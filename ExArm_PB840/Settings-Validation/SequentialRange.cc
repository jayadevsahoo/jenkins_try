#include "stdafx.h"


//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SequentialRange - Sequential Range.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class defines the operations needed by all of the Sequential
//  Settings, with respect to the control of their sequence.  These
//  operations include:  the testing of the legality of a specific
//  sequential value in relation to the sequence managed by instances of
//  this class, the accessing of the minumum and maximum constraints and
//  resolution, and the updating of the minimum and maximum constraints.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a central type for managing the operations of
//  sequential ranges.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class manages non-constant members for the maximum and minimum
//  values of this sequence, along with a constant member for the
//  resolution.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SequentialRange.ccv   25.0.4.0   19 Nov 2013 14:27:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc  Date:  26-Jan-2009   SCR Number: 6435
//  Project:  840S
//  Description:
//      Corrected sub-system identifier for SoftFault.
//
//  Revision: 003   By: sah  Date:  24-Apr-2000    DR Number: 5618
//  Project:  NeoMode
//  Description:
//      As part of the development of this class' unit test, I noticed
//      some unlabelled testable items.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SequentialRange.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SequentialRange(maxValue, maxContraintId, resolution,
//			     minConstraintId, minValue)  [Constructor]
//
//@ Interface-Description
//  Construct a sequential range with 'maxValue' and 'minValue' as the
//  absolute maximum and minimum values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The passed parameters, 'maxValue' and 'minValue', are used to initialize
//  this instance's constant members.
//---------------------------------------------------------------------
//@ PreConditions
//  (SequentialRange::IsLegalSequence(maxValue, resolution, minValue))
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SequentialRange::SequentialRange(const SequentialValue maxValue,
				 const SettingBoundId     maxConstraintId,
				 const SequentialValue resolution,
				 const SettingBoundId     minConstraintId,
				 const SequentialValue minValue)
				 : maxConstraintValue_(maxValue),
				   maxConstraintId_(maxConstraintId),
				   minConstraintValue_(minValue),
				   minConstraintId_(minConstraintId),
				   ABSOLUTE_MAX_(maxValue),
				   RESOLUTION_(resolution),
				   ABSOLUTE_MIN_(minValue)
{
  CALL_TRACE("SequentialRange(maxValue, resolution, minValue)");
  CLASS_PRE_CONDITION((SequentialRange::IsLegalSequence(ABSOLUTE_MAX_,
  						        RESOLUTION_,
  						        ABSOLUTE_MAX_)));
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SequentialRange()  [Destructor]
//
//@ Interface-Description
//  Destroy this range.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SequentialRange::~SequentialRange(void)
{
  CALL_TRACE("~SequentialRange()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isLegalValue(sequentialValue)  [const]
//
//@ Interface-Description
//  Is 'sequentialValue' a "legal" value within this sequential range.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
SequentialRange::isLegalValue(const SequentialValue sequentialValue) const
{
  CALL_TRACE("isLegalValue(sequentialValue)");

  // is 'sequentialValue' within the minimum and maximum range?...
  Boolean  isLegal = (sequentialValue >= ABSOLUTE_MIN_ &&
		      sequentialValue <= ABSOLUTE_MAX_);

  if (isLegal)
  {   // $[TI1]
    // is the value of 'sequentialValue' at a "resolution"
    // location?...$[TI1.1] (TRUE)  $[TI1.2] (FALSE)...
    isLegal = SequentialRange::IsLegalSequence(sequentialValue,
					       RESOLUTION_,
					       ABSOLUTE_MIN_);
  }   // $[TI2] -- 'sequentialValue' is NOT within the absolute range...

  return(isLegal);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  testValue(rProposedValue, rBoundStatus)  [const]
//
//@ Interface-Description
//  Check 'rProposedValue' against this range.  If 'rProposedValue' is
//  outside of this range's constraint values, 'rProposedValue' is "clipped"
//  to the nearest constraint, and 'rBoundStatus' is set to the violated
//  bound.  If no violation occurs, 'rBoundStatus' is returned in a
//  "no-violation" state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isLegalValue(rProposedValue))
//@ End-Method
//=====================================================================

void
SequentialRange::testValue(SequentialValue& rProposedValue,
			   BoundStatus&     rBoundStatus) const
{
  CALL_TRACE("testValue(rBoundState, rProposedValue)");

  // set the bound status to a "no-violation" state...
  rBoundStatus.reset();

  if (rProposedValue > maxConstraintValue_)
  {   // $[TI1]
    // the proposed value is above this range's maximum constraint,
    // therefore "clip" the proposed value and return the corresponding
    // ID of the maximum constraint upper bound...
    rProposedValue = maxConstraintValue_;
    rBoundStatus.setBoundStatus(UPPER_HARD_BOUND, maxConstraintId_);
  }   // $[TI2] -- 'rProposedValue' does NOT violate max. constraint...

  if (!rBoundStatus.isInViolation())
  {   // $[TI3]
    if (rProposedValue < minConstraintValue_)
    {   // $[TI3.1]
      // the proposed value is below this range's minimum constraint,
      // therefore "clip" the proposed value and return the corresponding
      // ID of the minimum constraint upper bound...
      rProposedValue = minConstraintValue_;
      rBoundStatus.setBoundStatus(LOWER_HARD_BOUND, minConstraintId_);
    }   // $[TI3.2] -- 'rProposedValue' does NOT violate min. constraint...
  }   // $[TI4] -- upper bound violated...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  increment(numClicks, rCurrValue, rBoundStatus)  [const]
//
//@ Interface-Description
//  Increment 'rCurrValue', within this range, by the number of clicks
//  given by 'numClicks'.  If this increment violates this range's specified
//  maximum constraint, the constraint information is returned in
//  'rBoundStatus', and 'rCurrValue' is clipped to the maximum.  If this
//  range's maximum constraint is NOT violated, 'rBoundStatus' is left in
//  the "no-violation" state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (isLegalValue(rCurrValue))
//---------------------------------------------------------------------
//@ PostConditions
//  (isLegalValue(rCurrValue))
//@ End-Method
//=====================================================================

void
SequentialRange::increment(const Uint       numClicks,
			   SequentialValue& rCurrValue,
			   BoundStatus&     rBoundStatus) const
{
  CALL_TRACE("increment(numClicks, rCurrValue, rBoundStatus)");
  SAFE_CLASS_PRE_CONDITION((isLegalValue(rCurrValue)));

  rCurrValue += (numClicks * RESOLUTION_);

  if (rCurrValue <= maxConstraintValue_)
  {   // $[TI1]
    rBoundStatus.reset();
  }
  else
  {   // $[TI2] -- upper bound violation...
    rCurrValue = maxConstraintValue_;
    rBoundStatus.setBoundStatus(UPPER_HARD_BOUND, maxConstraintId_);
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  decrement(numClicks, rCurrValue, rBoundStatus)  [const]
//
//@ Interface-Description
//  Decrement 'rCurrValue', within this range, by the number of clicks
//  given by 'numClicks'.  If this decrement violates this range's specified
//  minimum constraint, the ID of the violated constraint is returned, and
//  'rCurrValue' is clipped to the minimum.  If this range's minimum
//  constraint is NOT violated, 'NULL_SETTING_BOUND_ID' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (isLegalValue(rCurrValue))
//---------------------------------------------------------------------
//@ PostConditions
//  (isLegalValue(rCurrValue))
//@ End-Method
//=====================================================================

void
SequentialRange::decrement(const Uint       numClicks,
			   SequentialValue& rCurrValue,
			   BoundStatus&     rBoundStatus) const
{
  CALL_TRACE("decrement(numClicks, rCurrValue, rBoundStatus)");
  SAFE_CLASS_PRE_CONDITION((isLegalValue(rCurrValue)));

  rCurrValue -= ((SequentialValue)numClicks * RESOLUTION_);

  if (rCurrValue >= minConstraintValue_)
  {   // $[TI1]
    rBoundStatus.reset();
  }
  else
  {   // $[TI2] -- lower bound violation...
    rCurrValue = minConstraintValue_;
    rBoundStatus.setBoundStatus(LOWER_HARD_BOUND, minConstraintId_);
  }
}


//=====================================================================
//@ Method:  IsLegalSequence(intervalMax, intervalRes, intervalMin) [static]
//
//@ Interface-Description
//  Do 'intervalMax', 'intervalRes' and 'intervalMin' represent a "legal"
//  sequence.  For these values to represent a legal sequence the maximum
//  has to be greater-than the minimum, and there has to be some integer
//  multiple of 'intervalRes' between the minimum and the maximum.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
SequentialRange::IsLegalSequence(const SequentialValue intervalMax,
				 const SequentialValue intervalRes,
				 const SequentialValue intervalMin)
{
  CALL_TRACE("IsLegalSequence(intervalMax, intervalRes, intervalMin)");

  Boolean  isLegal = (intervalMax >= intervalMin);

  if (isLegal)
  {   // $[TI1]
    // calculate the total number of complete resolutions in the sequence...
    const Int  TOTAL_RESOLUTIONS = (intervalMax - intervalMin) / intervalRes;

    // calculate the maximum based on 'TOTAL_RESOLUTIONS'...
    const SequentialValue  CALCULATED_MAX =
			     intervalMin + (TOTAL_RESOLUTIONS * intervalRes);

    // $[TI1.1] (TRUE)  $[TI1.2] (FALSE)...
    isLegal = (intervalMax == CALCULATED_MAX);
  }   // $[TI2] -- 'intervalMax' is less-than 'intervalMin'...

  return(isLegal);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                 [static]
//
//@ Interface-Description
//  Report the software fault that occured within the source code
//  of this class.  The fault, indicated by 'softFaultID', occured
//  at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
SequentialRange::SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName,
			   const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, SEQUENTIAL_RANGE,
  			  lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Free-Function:  operator<<(ostr, sequentialRange)  [friend]
//
// Interface-Description
//  Dump the contents of 'sequentialRange' into 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Free-Function
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const SequentialRange& sequentialRange)
{
  CALL_TRACE("::operator<<(ostr, sequentialRange)");

  ostr << "SequentialRange {" << endl;
  ostr << "  ABSOLUTE_MAX_ = " << sequentialRange.ABSOLUTE_MAX_ << endl;
  ostr << "  RESOLUTION_   = " << sequentialRange.RESOLUTION_   << endl;
  ostr << "  ABSOLUTE_MIN_ = " << sequentialRange.ABSOLUTE_MIN_ << endl;
  ostr << '}' << endl;

  return(ostr);
}

#endif // defined(SIGMA_DEVELOPMENT)
