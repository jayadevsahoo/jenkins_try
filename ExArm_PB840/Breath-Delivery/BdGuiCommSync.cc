#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdGuiCommSync - implements the synchronizing of alarm and
//  ventilator status when Communications is restored between BD and GUI
//---------------------------------------------------------------------
//@ Interface-Description
//   The methods in this class are invoked via the callback mechanism
//   from the BdStatusTask which invokes appContext.dispatchEvent()
//   when a TASK_CONTROL_EVENT is received on the BD_STATUS_TASK_Q.
//   These methods in turn interface with the BdAlarms class to
//   coordinate the initialization of alarms and with the
//   VentAndUserEventStatus class to coordinate the initialization
//   of ventilator status and user event status.
//   The static methods GuiReadyCallback() and CommDownCallback() are
//   registered with the appContext object in Sys-Init from the BD
//   Status Task.  When a TASK_CONTROL_EVENT is received by the BD
//   Status Task, one of these methods is invoked based on the message
//   received.
//---------------------------------------------------------------------
//@ Rationale - required to synchronize the GUI with the alarm and
//   ventilator status maintained by the BD whenever communciations is
//   restored (on initialization or after Communicaitons Down)
//---------------------------------------------------------------------
//@ Implementation-Description
//  GuiReadyCallback() - If GUI state is on-line, then resynch alarm
//  status and user events.
//  CommDownCallback() - notifies BdAlarms and VentAndUserEventStatus
//  that communication is down.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a
//---------------------------------------------------------------------
//@ Restrictions
//    This is a static class; it is not instanciated.
//---------------------------------------------------------------------
//@ Invariants
//    none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdGuiCommSync.ccv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: syw     Date:  18-Nov-1999    DR Number: 5584
//  Project:  ATC
//  Description:
// 		Call TerminateManeuver() instead of CancelManeuver().
//
//  Revision: 007  By: yyy     Date:  22-Oct-1999    DR Number: 5458
//  Project:  ATC
//  Description:
//      Clear all the pending maneuval request when communicaiton
//		goes down.
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  syw    Date:  14-Oct-1998    DR Number: 5202
//       Project:  BiLevel (R8027)
//       Description:
//			Call CancelManeuverRequested() instead of CancelManeuver()
//			when comm is down.
//
//  Revision: 004  By:  yyy    Date:  28-Apr-1998    DR Number: None
//       Project:  BiLevel (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling of cancel pause event when communication
//			   goes down.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  sp    Date:  26-Feb-1997    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  kam    Date:  23-Oct-1995    DR Number:DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================
#include "BdGuiCommSync.hh"
#include "BreathMiscRefs.hh"
#include "VentObjectRefs.hh"
#include "IpcIds.hh"
#include "BdAlarmId.hh"
#include "SchedulerId.hh"

//@ Usage-Classes
#include "MsgQueue.hh"
#include "BdAlarms.hh"
#include "VentAndUserEventStatus.hh"
#include "GuiReadyMessage.hh"
#include "CommDownMessage.hh"
#include "Maneuver.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiReadyCallback 
//
//@ Interface-Description
//    This static method takes a reference to a GuiReadyMessage as an
//    input and returns nothing.  It is invoked when the GuiReadyMessage
//    is received by the BD Status Task.  The current Gui state is determined
//    by invoking the getState() method of GuiReadyMessage.  If the current
//    Gui State is ONLINE, this method invokes rBdAlarms.resyncAlarmStatus()
//    to post alarm status to Alarm-Analysis and VentAndUserEventStatus::
//    ResyncEventStatus() to send ventilator and user event status
//    to the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//    A switch is performed on the current Gui state.   If the state is
//    ONLINE, the method resyncAlarmStatus() in BdAlarms is invoked
//    to post alarm status to Alarm-Analysis and VentAndUserEventStatus::
//    ResyncEventStatus() is invoked to send vent and user event status
//    to the Gui.
//---------------------------------------------------------------------
//@ PreCondition
//    Gui State is valid
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
BdGuiCommSync::GuiReadyCallback(const GuiReadyMessage& rMessage)
{
    CALL_TRACE("BdGuiCommSync::GuiReadyCallback(const GuiReadyMessage& rMessage)");

    switch (rMessage.getState())
    {
        case STATE_ONLINE: // $[TI1]

            // GUI in ONLINE state
            // Resync Alarm Status
            RBdAlarms.resyncAlarmStatus();

            // Resync Vent and User Event Status
            VentAndUserEventStatus::ResyncEventStatus();
            break;

        case STATE_INIT: // $[TI2]
        case STATE_INOP: // $[TI3]
        case STATE_SERVICE: // $[TI4]
        case STATE_START: // $[TI5]
        case STATE_TIMEOUT: // $[TI6]
        case STATE_SST: // $[TI7]
            // Do nothing in these states 
            break;

        default:
	    // $[TI8]
            // Invalid state
            CLASS_ASSERTION (FALSE);
            break;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommDownCallback 
//
//@ Interface-Description
//    This static method takes a reference to a CommDownMessage as an
//    input and returns nothing.  It is invoked when the CommDownMessage
///   is received by the BD Status Task.
//
//    This method invokes rBdAlarms.SetAlarmCommState(),
//    Maneuver::TerminateManeuver() and
//    VentAndUserEventStatus::SetEventStatusCommState()
//    to notify BdAlarms and VentAndUserEventStatus that communications
//    is down and no status should be sent to Alarm-Analysis or the Gui.
//    The Maneuver::TerminateManeuver() will do the necessary clean up job on
//    the BD for the maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The method setAlarmCommState() in BdAlarms is invoked
//    to notify BdAlarms that alarm status should not be posted to
//    Alarm-Analysis, and the method SetEventStatusCommState() in
//    VentAndUserEventStatus is invoked to notify VentAndUserEventStatus
//    that ventilator and user event status should not be sent to
//    the GUI.  The TerminateManeuver() in Maneuver is invoked to do the
//    clean up work for the active maneuver due to the pause request.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void
BdGuiCommSync::CommDownCallback(const CommDownMessage& rMessage)
{
    CALL_TRACE("BdGuiCommSync::CommDownCallback(const CommDownMessage& rMessage)");
	UNUSED_SYMBOL(rMessage);
    VentAndUserEventStatus::SetEventStatusCommState (BdGuiCommSync::COMMS_DOWN);
    // $[BL04070] :l disable pause event request
 	// $[BL04071] :l terminate pause event and start inspiration
    // $[BL04072] :l disable pause event request
    // $[04220] :l terminate active manual expiratory pause
    // $[BL04008] :l disable pending pause event
    // $[BL04075] :l cancel pause request
	// $[BL04012] :l terminate active pause request
	// $[BL04075] :j cancel manual pause request
	// $[BL04078] :l terminate manual active inspirtory pause
	Maneuver::TerminateManeuver();
    RBdAlarms.setAlarmCommState (BdGuiCommSync::COMMS_DOWN);
   
    // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BdGuiCommSync::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BDGUICOMMSYNC,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//=====================================================================
//
//  Private Methods...
//
//=====================================================================


