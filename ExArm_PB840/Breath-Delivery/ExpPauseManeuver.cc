#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExpPauseManeuver - implements the handling of expiratory pause
//  meneuver.
//---------------------------------------------------------------------
//@ Interface-Description
// Handles the expiratory pause events that are originated by the UI.  At
// construction time, a maneuver object with the appropriate maneuver ID,
// and a stability data buffer are created.  Access methods are implemented
// to get the maneuver's state and the pressure stability state.  A method
// to set the clear request, to cancel the maneuver is implemented as well.
// When a expiratory pause is requested, the method handleExpPause shall
// determine the maneuver state, setup the maximum time allowed for pause
// to be initiated and starts the state machine.  The activate method
// setups the timer requirement for the maximum auto mode pause duration.
// The virtual function timeUpHappened also handles various timeout requirement
// to either cancel the manual or setups the maximum manual activated pause time.
//---------------------------------------------------------------------
//@ Rationale
// This class defines how expiratory pause works.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method handleExpPause() is invoked when a expiratory pause is started/
// stopped.  When expiratory pause key is released, only when the expiratory
// pause maneuver is active and we are in manual mode, we shall enable the
// PauseCompletionTrigger to tell the scheduler to complete the pause.  When
// starting a pause, a idle maneuver shall change to automatic pending state
// and ready for pause to start.  When a pending pause becomes activated,
// the maneuver state is set accordingly, the pause time is set to allow 2
// seconds of auto pause time.  When the 2 second auto pause has elapsed, and
// the user is still pressing the expiratory pause key, the pause transitions
// to manual mode.  The method newCycle_() is called every 5 ms.  While the
// pause is in active mode, if the user has requested to cancel the pause,
// then the maneuver shall be terminated accordingly, else it shall collect
// the airway pressure and calculate the pressure stability.  Once the pressure
// reached stability, the pause completion trigger shall be enabled and
// the maneuver shall transitions to idle pending state.  If in any cases,
// the timeout happened, then the pause completion trigger shall be enabled,
// the maneuver shall transitions to idle pending state.  The method clear()
// shall send a cancel command when the maneuver state is in pending or
// active state.  A queue message shall be send to GUI when the maneuver
// state is idle pending.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExpPauseManeuver.ccv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: gdc   Date:  29-Nov-2006   SCR Number: 6319
//  Project:  RESPM
//  Description:
//		Removed private data member clearRequested_ to use protected
//      data member with same name in Maneuver base class.
//
//  Revision: 009   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Refactored code for base class modifications to accomodate RM
//      maneuvers. Moved pressure stability buffer to its own class
//      that is now a member of this class. New method processUserRequest()
//      implements functionality of old Maneuver::DetermineManeuverStatus()
//      method in each maneuver class.
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By:  yyy    Date:  05-Nov-98    DR Number: 5252
//       Project:  Sigma (R8027)
//       Description:
//             Mapping SRS to code.
//
//  Revision: 006  By:  yyy    Date:  19-Oct-1998    DR Number: 5217
//       Project:  BiLevel
//       Description:
//      	Removed FORNOW statement.
//
//  Revision: 005  By:  syw    Date:  14-Oct-1998    DR Number: 5185
//       Project:  BiLevel
//       Description:
//      	Added handle in timeUpHappened() for the case when keyReleased_
//			is TRUE.  Terminate pause with RPauseCompletionTrigger and
//			cleanup.
//
//  Revision: 004  By:  yyy    Date:  13-Oct-1998    DR Number: 5189
//       Project:  Sigma (R8027)
//       Description:
//             Use the correct timer setting (the difference between
//			   Max pause time and actual time used for checking the
//			   pressure stability) for manual exp pause time.
//
//  Revision: 003  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added expiratory pause event.
//
//  Revision: 002  By:  syw    Date:  19-Aug-1997    DR Number: DCS 1656
//       Project:  Sigma (840)
//       Description:
//             Capitalize all global references per coding standard.
//
//  Revision: 001  By:  iv    Date:  27-Feb-1997    DR Number: DCS 1408, 1638
//       Project:  Sigma (840)
//       Description:
//             Initial version - per Breath Delivery formal code review
//             and unit test fixes.
//
//=====================================================================

#include "ExpPauseManeuver.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes
#include "IntervalTimer.hh"
#include "Trigger.hh"
#include "TriggersRefs.hh"
#include "TimerBreathTrigger.hh"
#include "MsgQueue.hh"
#include "BdQueuesMsg.hh"
#include "PhaseRefs.hh"
#include "ExpiratoryPausePhase.hh"
#include "StaticMechanicsBuffer.hh"
#include "SummationBuffer.hh"
#include "BreathRecord.hh"
#include "PhasedInContextHandle.hh"
#include "SettingId.hh"
#include "FlowPatternValue.hh"
#include "MainSensorRefs.hh"
#include "FlowSensor.hh"
#include "Btps.hh"
#include "CircuitCompliance.hh"
#include "BreathSet.hh"
#include "ManeuverTimes.hh"

//@ End-Usage

//@ Code...
//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================

//@ Constant MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE
// $[BL04008] k: The time elapsed since the pause was requested
static const Int32 MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE = 72000;

//@ Constant MAX_AUTO_TIME_EXCEED_SINCE_PAUSE_PHASE_START
// $[BL04008] k: The time elapsed since the pause was requested
static const Int32 MAX_AUTO_TIME_EXCEED_SINCE_PAUSE_PHASE_START = 3000;

//@ Constant MAX_EXP_PAUSE_INTERVAL
// $[BL04008] k: The time elapsed since the pause was requested
#ifdef E600_840_TEMP_REMOVED
extern Int32 MAX_EXP_PAUSE_INTERVAL;
#endif

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExpPauseManeuver
//
//@ Interface-Description
// The method takes a maneuverId and a reference to a IntervalTimer as
// arguments. It calls the base class constructors and initializes some
// data members.
//---------------------------------------------------------------------
//@ Implementation-Description
// It invokes the base class constructor with the expiratory pause
// maneuver id argument.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExpPauseManeuver::ExpPauseManeuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer) :
	Maneuver(maneuverId),
    TimerTarget(),
    rPauseTimer_(rPauseTimer),
    pauseTime_(0),
    pauseTimeId_(NULL_TIME),
    timeOut_(FALSE),
    keyReleased_(TRUE),
    audibleAnnunciatingNeeded_(TRUE),
	maneuverType_(PauseTypes::AUTO),
	pressureState_(PauseTypes::UNSTABLE),
	pressureStabilityBuffer_(StaticMechanicsBuffer::GetExhPressureBuffer(),
							 StaticMechanicsBuffer::GetExhPressBufferSize())
{
	// $[TI1]
	CALL_TRACE("ExpPauseManeuver::ExpPauseManeuver(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExpPauseManeuver()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExpPauseManeuver::~ExpPauseManeuver(void)
{
  CALL_TRACE("~ExpPauseManeuver()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
// $[04233]
// This method takes a reference to an IntervalTimer as an argument and has no
// return value. The expiratory pause maneuver is a client of the timer server
// that is responsible to notify (by invoking this function) the end of the
// current expiratory pause interval.
//---------------------------------------------------------------------
//@ Implementation-Description
// The IntervalTimer reference passed in, rTimer, is that of the cycle timer
// server for the expiratory pause maneuver.  The private data member pauseTimeId_
// is used to identify which timer period the maneuver is using.  If the
// MAX_PENDING_TIME (72 seconds passes without initiating a pause) is timeup,
// then we shall cancel this request.  If the MAX_AUTO_TIME (2 seconds passed
// without reaching pressure stability) then if the user has not released the
// expiratory pause key we shall transition to manual mode.  The MAX_MANUAL_TIME
// id and the corresponding time shall be set for the pause timer.  If the
// MAX_MANUAL_TIME (20 seconds passed for the activated maneuver) then we
// just set the timeOut_ flag to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
// &rTimer == &rPauseTimer_
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
ExpPauseManeuver::timeUpHappened(const IntervalTimer& rTimer)
{
    CALL_TRACE("BiLevelScheduler::timeUpHappened(const IntervalTimer& rTimer)");

    // make sure the caller is the proper server
    CLASS_PRE_CONDITION(&rTimer == &rPauseTimer_);

	// Stop the timer, but do not reset the currentTime_,
    rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);

	if (pauseTimeId_ == MAX_PENDING_TIME)
	{								    // $[TI1]
		// $[BL04070] :k disable pause event request
		// The time elapsed has exceeded 72 seconds without initiating
		// the pause phase, so wee have to cancel the exp. pause request.
		// CANCEL pause.
		// Inform Maneuver to cancel it.
		clear();
	}
	else if (pauseTimeId_ == MAX_AUTO_TIME)
	{								    // $[TI2]
		timeOut_ = TRUE;

		if (!keyReleased_)
		{							    // $[TI2.1]
			// When key is not released, then we shall drop to manual
			// mode.  then we should set the max time that allows the
			// expiratory pause to proceed (20 - 3 = 17 seconds).
		    // $[BL04072] :k disable pause event request
			maneuverType_ = PauseTypes::MANUAL;

			// adjust the target time
			pauseTime_ = MAX_EXP_PAUSE_INTERVAL -
   						  pauseTime_;
			pauseTimeId_ = MAX_MANUAL_TIME;
   			rPauseTimer_.setTargetTime(pauseTime_);

		    // start the timer to check the duration when the pause was
		    // requested
		   	rPauseTimer_.restart();

			// Reset the timeOut_ flag, so the newCycle() will not proceed
			// with this MAX_AUTO_TIME_EXCEED_SINCE_PAUSE_PHASE_START timeout.
			timeOut_ = FALSE;

			// The status shall be sent to GUI to communicate to
			// the user through UiEvent.cc so the appropriated prompt
			// can be displayed.
			RExpiratoryPauseEvent.setEventStatus(EventData::ACTIVE, EventData::MANUAL_PROMPT);
		}
		else
		{
			// $[TI2.2]
			((Trigger&)RPauseCompletionTrigger).enable();

			// User release the exp pause key,
			// so set the maneuver state to idle pending to
			// indicate the completion of the pause is
			// requested.
			maneuverState_ = PauseTypes::IDLE_PENDING;
		}
	}
	else if (pauseTimeId_ == MAX_MANUAL_TIME)
	{								    // $[TI3]
		timeOut_ = TRUE;
	}
	else
	{
        AUX_CLASS_ASSERTION_FAILURE(pauseTimeId_);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: handleExpPause
//
//@ Interface-Description
// The method takes a boolean event status as an argument and returns an enum
// for user event status.
//---------------------------------------------------------------------
//@ Implementation-Description
// The eventStatus argument is set to TRUE by the client, to start the pause.
// The eventStatus argument is set to FALSE by the client, to stop the pause.
//
// When required to start the pause, the following happens:
//		1. idle maneuverState_ transitions to pending.
//			setActiveManeuver()
//			set rPauseTimer_ with MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE
//		2. pending /active maneuverState_, simply ignore this request - do nothing
//
// When required to stop the pause, the following happens:
// 		1. pending maneuverState_, simply ignore this request - do nothing
//		2. active maneuverState_ and in manual mode, then
//			RPauseCompletionTrigger) gets enableed so the pause can be
//			cancelled at next BD cycle.
//---------------------------------------------------------------------
//@ PreCondition
//    The user event status is checked to be in the following range:
//    { REJECTED || COMPLETE || CANCEL || IDLE || ACTIVE || PENDING}
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
ExpPauseManeuver::handleExpPause(const Boolean eventStatus)
{
    CALL_TRACE("ExpPauseManeuver::handleExpPause(const Boolean eventStatus)");

	EventData::EventStatus rtnStatus = EventData::IDLE;
	
	if (eventStatus)
	{										// $[TI1]
		keyReleased_ = FALSE;
		if (maneuverState_ == PauseTypes::PENDING ||
			maneuverState_ == PauseTypes::ACTIVE)
		{									// $[TI1.1]
			// An exp pause was requested before.
			// Simply ignore this request - do nothing
			// an EventData::NO_ACTION_ACK is sending back to GUI.
			rtnStatus = EventData::NO_ACTION_ACK;
		}
		else if (maneuverState_ == PauseTypes::IDLE)
		{									// $[TI1.2]
			// maneuverState_ is IDLE so we allow the exp pause to
			// happen.
		    // adjust the target time
		    pauseTime_ = MAX_TIME_ALLOWED_TO_INITIATE_PAUSE_PHASE;
		    pauseTimeId_ = MAX_PENDING_TIME;
		   	rPauseTimer_.setTargetTime(pauseTime_);

		    // start the timer to check the duration when the pause was
		    // requested
		   	rPauseTimer_.restart();

			setActiveManeuver();

			// User pressed the exp pause key,
			// so set the the maneuverType_ to AUTO (default)
			maneuverType_ = PauseTypes::AUTO;
			maneuverState_ = PauseTypes::PENDING;
			clearRequested_ = FALSE;

			// This return status shall update the ExpiratoryPauseEvent's
			// eventStatus_ through UiEvent.cc (UiEvent::setEventStatus)
			// This return status shall be sent to GUI to communicate to
			// the user through UiEvent.cc (UiEvent::setEventStatus) too.
			rtnStatus = EventData::PENDING;

			// Reset the circular buffer for pressure and flow data.
			pressureStabilityBuffer_.reset();
		}
		else
		{									// $[TI1.3]
			// Do nothing
			// Note: we do not perform any special operation for IDLE_PENDING
			// due to that this status will be reset by the current running
			// scheduler to IDLE in the same 5 ms cycle.
            AUX_CLASS_ASSERTION( (maneuverState_ == PauseTypes::IDLE_PENDING) ||
                             	 (maneuverState_ == PauseTypes::ACTIVE_PENDING),
                             	 maneuverState_ );
		}
	}
	else
	{										// $[TI2]
		// Set the keyReleased_ flag
		keyReleased_ = TRUE;
		if (maneuverState_ == PauseTypes::PENDING)
		{									// $[TI2.1]
			// User released the exp pause key,
			rtnStatus = EventData::NO_ACTION_ACK;
		}
		else if (maneuverState_ == PauseTypes::ACTIVE)
		{									// $[TI2.2]
			if (maneuverType_ == PauseTypes::MANUAL)
			{								// $[TI2.2.1]
				// In manual state, and user release the EXP
				// PAUSE key so, it time to terminate the
				// pause.
				rtnStatus = EventData::ACTIVE;

				// Stop the timer, but do not reset the currentTime_,
			    // $[BL04074] :a terminate active pause
			    rPauseTimer_.stop();
			    rPauseTimer_.setCurrentTime(0);

				((Trigger&)RPauseCompletionTrigger).enable();

				// User release the exp pause key,
				// so set the maneuver state to idle pending to
				// indicate the completion of the pause is
				// requested.
				maneuverState_ = PauseTypes::IDLE_PENDING;
			}
			else
			{								// $[TI2.2.2]
				// Do nothing by informing GUI with NO_ACTION_ACK
				// status and let the 3 second time out to take care
				// of the rest.
				rtnStatus = EventData::NO_ACTION_ACK;
			}
		}
		else
		{									// $[TI2.3]
			// Do nothing
		}
	}

	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method is invoked by the currently running scheduler when the
//  maneuver is in auto pending mode.  Once the maneuver is activated,
//  we shall setup the maximum automatic pause time to allow the pause
//  to last.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
ExpPauseManeuver::activate(void)
{
    CALL_TRACE("ExpPauseManeuver::activate(void)");

	if (maneuverState_ == PauseTypes::PENDING)
	{									// $[TI1]
		timeOut_ = FALSE;
		pressureState_ = PauseTypes::UNSTABLE;
		audibleAnnunciatingNeeded_ = TRUE;

		if (maneuverType_ == PauseTypes::AUTO)
		{								// $[TI1.1]
			// adjust the target time
			pauseTimeId_ = MAX_AUTO_TIME;
			pauseTime_ = MAX_AUTO_TIME_EXCEED_SINCE_PAUSE_PHASE_START;
			rPauseTimer_.setTargetTime(pauseTime_);
		}
		else
		{
			AUX_CLASS_ASSERTION_FAILURE(maneuverType_);
		}

		// restart the BiLevel sync timer with appropriated target time.
		rPauseTimer_.restart();

		// Setup active maneuver state
		maneuverState_ = PauseTypes::ACTIVE;
	}
	else
	{									// $[TI2]
		AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: complete
//
//@ Interface-Description
//    This method takes no arguments and returns Boolean.  It is
//    invoked every time the expiratory pause completes.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Invokes clear() to complete the expiratory pause maneuver.
//    Returns TRUE to indicate there are no more pending expiratory
//    pause maneuvers.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
ExpPauseManeuver::complete(void)
{
    CALL_TRACE("ExpPauseManeuver::complete(void)");

	clear();

	return (TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: terminateManeuver
//
//@ Interface-Description
//    This method takes no arguments and returns nothing.  Issues
//	  cancel commands to deactivate the active maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Set clearRequested_ TRUE so the maneuver is cancelled
//    and the associated phase deactivated by the next BD cycle.
//    When the active maneuver is in pending state, invoke
//	  clear() method to cancel this maneuver immediately
//    since no pause phase has been involved.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
ExpPauseManeuver::terminateManeuver(void)
{
	if (maneuverState_ == PauseTypes::ACTIVE)
	{				 // $[TI1.2.1]
		// $[BL04012] :a terminate active pause request
		// $[BL04071] :a terminate active auto expiratory pause
		clearRequested_ = TRUE;
	}
	else if (maneuverState_ == PauseTypes::PENDING ||
			 maneuverState_ == PauseTypes::IDLE_PENDING ||
			 maneuverState_ == PauseTypes::ACTIVE_PENDING)
	{				 // $[TI1.2.2]
		// $[BL04008] :a disable pending pause event
		// $[BL04070] :a disable pending pause event
		clear();
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processUserEvent
//
//@ Interface-Description
//    This method takes a UiEvent reference as an argument and returns
//    a Boolean.  This method is invoked whenever the new UiEvent
//    occured and exp pause is the active maneuver.  A TRUE value is
//    returned if the new GUI event should proceed.  A FALSE value is
//    returned when the new GUI event should be rejected.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  For an inspiratory pause requested while the exp pause is
//    pending then call CancelManeuver() to terminate the exp pause and
//    return FALSE to discontinue processing of the insp pause
//    maneuver. If exp pause is active or idle pending then reject the
//    inspiratory pause event.  For an exp pause or manual insp
//    request, return TRUE to allow continued processing of the
//    request.  For a respiratory mechanics maneuver request, reject
//    the request.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
ExpPauseManeuver::processUserEvent(UiEvent &rUserEvent)
{
	Boolean continueNewEvent = TRUE;

	switch (rUserEvent.getId())
	{
	case EventData::EXPIRATORY_PAUSE:
	case EventData::MANUAL_INSPIRATION:
		continueNewEvent = TRUE;
		// Do nothing
		break;

	case EventData::INSPIRATORY_PAUSE:
		switch (maneuverState_)
		{
		case PauseTypes::PENDING:
			// Cancel Insp Pause when Exp pause status is pending
			// Set the Exp pause event status and communicate status to GUI
			// $[BL04070] :h disable pause event request
			CancelManeuver();
			continueNewEvent = FALSE;
			break;
		case PauseTypes::ACTIVE:
		case PauseTypes::IDLE_PENDING:
			// Complete (continue) Exp pause when Exp pause status is active
			// reject the Insp pause.
			// Set the event status and communicate status to GUI
			rUserEvent.setEventStatus(EventData::REJECTED);
			continueNewEvent = FALSE;
			break;
		default:
			AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
			break;
		}
		break;

	case EventData::NIF_MANEUVER:
	case EventData::P100_MANEUVER:
	case EventData::VITAL_CAPACITY_MANEUVER:
		// $[RM12033] All RM maneuver requests shall be rejected if any other pause or RM maneuver has already taken place during the same breath.
		rUserEvent.setEventStatus(EventData::REJECTED);
		continueNewEvent = FALSE;
		break;

	default:
		AUX_CLASS_ASSERTION_FAILURE(rUserEvent.getId());
		break;
	}

	return(continueNewEvent);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ExpPauseManeuver::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, EXPPAUSEMANEUVER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//    This method takes no arguments and returns nothing.  It is
//    invoked every BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method does all its function when it is in ACTIVE state.  It first
// checks if the user has requested to cancel this maneuver.  If it is, then
// we enable the immediate breath trigger and transition to IDLE_PENDING
// mode.  This will allow the scheduler to do all the necessary cleanup
// work at the next BD cycle.  During ACTIVE we need to monitor the airway
// pressure stability.  Once it is stable then an audible alarm is sent
// to GUI if the maneuver has lasted more than 500 ms.  Once an auto mode
// pause reached stability and the minimum time (500ms) has passed, then
// we enable the PauseCompletionTrigger so the scheduler can do whatever
// is necessary to terminate the expiratory pause.  If the stability
// never reaches and time up happened, then we also enable the
// PauseCompletionTrigger to force the scheduler to do the necessary
// cleanup work to terminate the expiratory pause.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ExpPauseManeuver::newCycle(void)
{
	CALL_TRACE("ExpPauseManeuver::newCycle_(void)");

	if ( maneuverState_ == PauseTypes::IDLE_PENDING )
	{
		Maneuver::Complete();
	}
	else if (maneuverState_ == PauseTypes::ACTIVE)
	{		  // $[TI1]
		if (clearRequested_)
		{		  // $[TI1.1.1]
			// Trigger an immediated breath to terminate the active
			// pause request.
			((Trigger&)RImmediateBreathTrigger).enable();
			maneuverState_ = PauseTypes::IDLE_PENDING;
			return;
		}		  // $[TI1.1.2]

		pressureStabilityBuffer_.updateBuffer(BreathRecord::GetAirwayPressure());

		PauseTypes::PpiState pressureState = PauseTypes::STABLE;

		// Calculate pressure stability only when it never reaches
		// stability.
		if (pressureState_ != PauseTypes::STABLE)
		{		  // $[TI1.2.1]
			pressureState = pressureStabilityBuffer_.calculatePressureStability();
		}		  // $[TI1.2.2]

		if (pressureState == PauseTypes::STABLE)
		{		  // $[TI1.3]
			if (audibleAnnunciatingNeeded_ && rPauseTimer_.getCurrentTime() >= 500)
			{		// $[TI1.3.1]
				// Pressure reached plateau, declare stability to GUI
				// $[BL01010] Once pressure stability has been detected an ...
				audibleAnnunciatingNeeded_ = FALSE;
				RExpiratoryPauseEvent.setEventStatus(EventData::AUDIO_ACK);
				pressureState_ = PauseTypes::STABLE;
			}		// $[TI1.3.2]

			// Pressure reached stability
			if (maneuverType_ == PauseTypes::AUTO)
			{		// $[TI1.3.3]
				if (rPauseTimer_.getCurrentTime() >= 500 &&
					RExpiratoryPauseEvent.getEventStatus() == EventData::ACTIVE)
				{		// $[TI1.3.3.1]
					if (keyReleased_)
					{	  // $[TI1.3.3.1.1]
						// $[BL04073] terminate active pause
						// If pressure stability is detected and more than
						// 0.5 seconds have elapsed since the start of the
						// pause, then terminate the pause
						// The scheduler should trigger the following to occure:
						// This call shall update the ExpiratoryPauseEvent's
						// eventStatus_ through UiEvent.cc (UiEvent::setEventStatus)
						// The status shall be sent to GUI to communicate to
						// the user through UiEvent.cc (UiEvent::setEventStatus) too.
						((Trigger&)RPauseCompletionTrigger).enable();
						rPauseTimer_.stop();
						rPauseTimer_.setCurrentTime(0);
						maneuverState_ = PauseTypes::IDLE_PENDING;
					}
					else
					{	  // $[TI1.3.3.1.2]
						CLASS_ASSERTION(pauseTimeId_ == MAX_AUTO_TIME);

						// Remember how much time had passed during stability checking
						pauseTime_ = rPauseTimer_.getCurrentTime();

						// Force a timeout to happen, so we will transition to
						// manual mode.
						rPauseTimer_.setTargetTime(0);
					}
				}		// $[TI1.3.3.2]
			}
			else if (maneuverType_ == PauseTypes::MANUAL &&
					 pauseTimeId_ == MAX_MANUAL_TIME && timeOut_)
			{		// $[TI1.3.4]
				((Trigger&)RPauseCompletionTrigger).enable();
				maneuverState_ = PauseTypes::IDLE_PENDING;
				CLASS_ASSERTION(RExpiratoryPauseEvent.getEventStatus() == EventData::ACTIVE);
			}		// $[TI1.3.5]
		}
		else
		{		  // $[TI1.4]
			if (timeOut_)
			{		// $[TI1.4.1]
				// the maneuverType_ == AUTO and
				// 	   timer = TIME_INTERVAL_TO_CHECK_STABLE_PL
				// 	   -> Pressure has not reached plateau yet
				// 	   The maneuver should terminate if the pressure does not
				// 	   stabilize within 2 seconds from the start of the pause.
				// the maneuverType_ == MANUAL and
				// 	   timer == MAX_AUTO_TIME_EXCEED_SINCE_PAUSE_PHASE_START
				//	   -> Exceed 20 seconds from the start of the pause phase.
				// 	   The maneuver should terminate if the time exceeded 10
				// 	   seconds from the start of the pause.
				// $[BL04073] terminate active pause
				// $[BL04074] :b terminate active pause
				((Trigger&)RPauseCompletionTrigger).enable();
				maneuverState_ = PauseTypes::IDLE_PENDING;
				pressureStabilityBuffer_.reset();

				AUX_CLASS_ASSERTION(RExpiratoryPauseEvent.getEventStatus() == EventData::ACTIVE,
									RExpiratoryPauseEvent.getEventStatus());
			}		// $[TI1.4.2]
		}
	}		  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
//    This method takes no arguments and returns nothing.  It is
//    invoked every time the expiratory pause is completed or terminated.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If the current pause is in PENDING or ACTIVE state, then we send
//    the message to GUI telling it that the pause has been cancelled.
//    If the current pause is in IDLE_PENDING state, then we post a
//    SEND_END_EXP_PAUSE_DATA message to the BD message queue to signal
//    to GUI that this is the end of expiratory pause.   Finally, we
//    initialize the data members to initial state.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ExpPauseManeuver::clear(void)
{
    CALL_TRACE("ExpPauseManeuver::clear(void)");

	if (maneuverState_ == PauseTypes::PENDING || maneuverState_ == PauseTypes::ACTIVE)
	{										// $[TI1]
		// This call shall update the ExpiratoryPauseEvent's
		// eventStatus_ through UiEvent.cc (UiEvent::setEventStatus)
		// The status shall be sent to GUI to communicate to
		// the user through UiEvent.cc (UiEvent::setEventStatus) too.
		RExpiratoryPauseEvent.setEventStatus(EventData::CANCEL);
	}
	else if (maneuverState_ == PauseTypes::IDLE_PENDING)
	{										// $[TI2]
		MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_END_EXP_PAUSE_DATA);
	}
	else
	{										// $[TI3]
		AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
	}

	maneuverState_ = PauseTypes::IDLE;
	maneuverType_ = PauseTypes::AUTO;
	pauseTimeId_ = NULL_TIME;
	clearRequested_ = FALSE;

	// Stop the timer, but do not reset the currentTime_,
    rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);
    ((Trigger&)RPauseCompletionTrigger).disable();

	pressureStabilityBuffer_.reset();

   	resetActiveManeuver();
}

