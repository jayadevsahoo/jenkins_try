#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: GuiEventTarget - A target for changes in the GUI state.
// Classes can specify this class as an additional parent class and register to
// be informed of state changes such as vent inoperative.
//---------------------------------------------------------------------
//@ Interface-Description
// This is a virtual base class.  Any classes which need to be informed of
// changes in the state of the GUI need to inherit from this class.  Then, when
// they register for state changes with the GuiEventRegistrar class, they will
// be informed of changes via the guiEventHappened() method.
//
// Examples of state changes include: vent inop, communications down,
// communications and failure in sending settings to Breath Delivery system.
//---------------------------------------------------------------------
//@ Rationale
// Used to implement the callback mechanism needed to communicate
// changes in the GUI state to objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// If classes need to be informed of GUI state changes they must do 3
// things: a) inherit from this class, b) define the
// guiEventHappened() method of this class and c) register for
// change notices via the GuiEventRegistrar class.  They will then
// be informed of changes via the guiEventHappened() method.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiEventTarget.ccv   25.0.4.0   19 Nov 2013 14:07:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  29-AUG-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "GuiEventTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened	[virtual]
//
//@ Interface-Description
// Called normally via GuiEventRegistrar when the GUI state has changed.  Passed
// the id of the state that just changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiEventTarget::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
        CALL_TRACE("GuiEventTarget::guiEventHappened(GuiApp::GuiAppEvent eventId)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiEventTarget()  [Default Constructor, Protected]
//
//@ Interface-Description
// The constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiEventTarget::GuiEventTarget(void)
{
        CALL_TRACE("GuiEventTarget::GuiEventTarget(void)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiEventTarget  [Destructor, Protected]
//
//@ Interface-Description
// Nothing to destruct!
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiEventTarget::~GuiEventTarget(void)
{
        CALL_TRACE("GuiEventTarget::~GuiEventTarget(void)");

        // Do nothing
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
GuiEventTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, GUIEVENTTARGET,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
