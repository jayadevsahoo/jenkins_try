#ifndef AlarmRender_IN
#define AlarmRender_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: AlarmRender - Low-level word wrapping routines for alarm displays.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmRender.inv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsWhiteSpaceChar_  [static]
//
//@ Interface-Description
//	Returns a boolean indicating whether 'testChar' is a whitespace
//  character, or not.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
 
inline Boolean
AlarmRender::IsWhiteSpaceChar_(const wchar_t testChar)
{
	CALL_TRACE("AlarmMsg::IsWhiteSpaceChar_(testChar)");
	return(testChar == L' '  ||  testChar == L'\t'  ||  testChar == L'\n'  ||
		   testChar == DELIMITER_CHAR_);
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


#endif // AlarmRender_IN 
