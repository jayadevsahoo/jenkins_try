
#ifndef BasedOnCategory_HH
#define BasedOnCategory_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  BasedOnCategory - .
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BasedOnCategory.hhv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  04-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//
//====================================================================

//@ Usage-Classes
//@ End-Usage


//@ Type:  BasedOnCategory
//
enum BasedOnCategory
{
  BASED_ON_NEW_PATIENT_VALUES,
  BASED_ON_ADJUSTED_VALUES,
  BASED_ON_ACCEPTED_VALUES
};


#endif // BasedOnCategory_HH 
