#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  BatchBoundedSetting - Batch Bounded Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides the setting's framework for the Batch, Bounded
//  Settings.  All batch, bounded settings ultimately are derived from this
//  setting.  This class overrides some of the virtual methods of its base
//  classes including those to:  update to a batch bounded setting's
//  new-patient value, and to fail an assertion when an attempt is made
//  to retrieve, or update to, a "default" value.  This class also
//  defines a new virtual method, with default behavior that can be overridden
//  by derived classes if need be.  This new virtual method provides a means
//  for getting this bounded setting's "accepted" and "adjusted" value.
//  These methods are overridden, if needed, by the derived bounded settings
//  to provide the specific behavior needed.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to establish the bounded setting's framework
//  for the rest of the settings to build upon.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchBoundedSetting.ccv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date: 21-Jan-2009    SCR Number: 6458
//  Project:  840S
//  Description:
//      Modified to use isNewlyApplicable() method to determine if
//		setting's applicability has changed from non-applicable to
//		an applicable state. Resolves problem of setting getting
//		into a changed state and not being able to get out of it when
//		even when the applicability reverts to non-applicable.
//
//  Revision: 010   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changeable IBW.
//
//  Revision: 009   By: sah    Date: 19-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added DEVELOPMENT-only code for debugging
//
//  Revision: 008   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  using SafetyPcvSettingValue's new, generic interface
//	*  as part of adding soft-bound to Tube I.D., the violation
//	   id is now more managed
//
//  Revision: 007   By: dosman Date:  24-Jun-1998    DCS Number: BILEVEL 66
//  Project:  BILEVEL
//  Description:
//  	Since the primary setting is being limited by the dependent's limit,
//  	warp the primary setting in the "same" direction it was trying to go
//  	to ensure that the primary setting is not limited any more than
//
//  Revision: 006   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 005   By: dosman Date:  24-Feb-1998    DCS Number: 
//  Project:  BILEVEL
//  Description:
//      Inital BiLevel version.
//      Ensure that absolute max/min values are updated during 
//      update to new patient mechanism.
//
//  Revision: 004   By: gdc    Date:  10-Apr-1998    DCS Number:  5069
//  Project:  COLOR
//  Description:
//	--- Merging Rev "Color" (1.16.1.0) into Rev "BiLevel" (1.16.2.0)
//	Using higher precision of two precisions when comparing for
//	equivalence in isChanged().
//
//  Revision: 003   By: sah    Date:  22-Sep-1997    DCS Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	Changed criteria of testing whether a setting is "changed", to
//	use the 'IsEquivalent()' function, instead of absolute equivalence.
//
//  Revision: 002   By: sah    Date:  07-May-1997    DCS Number: 2034
//  Project: Sigma (R8027)
//  Description:
//	Fixed problem with new-patient setup producing "clipped" tidal
//	volumes, by supplying one new method for resetting the minimum
//	and maximum constraints for each batch, bounded setting (see
//	'resetBoundConstraints()').
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "BatchBoundedSetting.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "AdjustedContext.hh"
#include "SafetyPcvSettingValues.hh"
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

//===================================================================
//
//  Public Method...
//
//===================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~BatchBoundedSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchBoundedSetting::~BatchBoundedSetting(void)
{
	CALL_TRACE("~BatchBoundedSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isChanged()  [const, virtual]
//
//@ Interface-Description
//  Has this setting been changed? When a batch setting's "adjusted"
//  value has been changed -- either auto-set or by the operator -- to a
//  value that is different than the setting's "accepted" value, or if 
//  the setting is now applicable in the adjusted context and it isn't
//  in the accepted context, this method will return TRUE, otherwise it
//  returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01038] -- is changed whenever the value is different than accepted...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean	BatchBoundedSetting::isChanged(void) const
{
	CALL_TRACE("isChanged()");

	Boolean  isChanged;

	if ( !getForcedChangeFlag_() )
	{	// $[TI1]
		// the forced-change flag is NOT set, therefore the "change" status
		// is based on whether the accepted and adjusted values are different...
		const BoundedValue  ACCEPTED_VALUE = getAcceptedValue();
		const BoundedValue  ADJUSTED_VALUE = getAdjustedValue();

		// compare using the finer resolution...
		const Precision COMPARE_PRECISION =
			MIN_VALUE(ACCEPTED_VALUE.precision,		// $[TI1.3]
					  ADJUSTED_VALUE.precision);	// $[TI1.4]

		// $[TI1.1] (TRUE)  $[TI1.2] (FALSE)...
		isChanged = !::IsEquivalent(ACCEPTED_VALUE.value, ADJUSTED_VALUE.value, COMPARE_PRECISION) ||
					isNewlyApplicable();
	}
	else
	{	// $[TI2] -- the override flag IS set, therefore return 'TRUE'.
		isChanged = TRUE;
	}

	return(isChanged);
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is NOT to be overridden by any of the
//  derived classes.  This is only used to initialize the Accepted Context
//  to some set of valid values, BEFORE a New-Patient Setup has been run.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This just returns the setting's Safety-PCV value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	BatchBoundedSetting::getDefaultValue(void) const
{
	CALL_TRACE("getDefaultValue()");

	return(SafetyPcvSettingValues::GetValue(getId()));
}	// $[TI1]


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getNewPatientValue()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be defined by ALL batch,
//  bounded settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  acceptTransition(settingId, newValue, currValue)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived classes
//  that respond to Main Control Setting transitions.  This version of
//  this virtual method is NEVER to be called -- it asserts that fact.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BatchBoundedSetting::acceptTransition(const SettingId::SettingIdType,
										  const DiscreteValue,
										  const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");
	AUX_CLASS_ASSERTION_FAILURE(getId());

	// NEVER gets this far...
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateToNewIbwValue()  [virtual]
//
//@ Interface-Description
// Update this setting for a changed IBW value. Called when the user
// changes the current patient's IBW setting.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method adjusts the bounded setting's constraints based on 
// the changed IBW. Settings values (e.g. tidal volume) constrain 
// changes to IBW only when they are applicable (e.g. Vt constrains
// IBW when in VC but not in PC). This method adjusts the setting's 
// constraints based on the new IBW and clips the adjusted value of 
// the setting to the new range defined by the changed IBW.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
	BatchBoundedSetting::updateToNewIbwValue(void)
{
	CALL_TRACE("updateToNewIbwValue()");

	// release prior constraints
	resetConstraints();

	// update the bounds based on the adjusted IBW
	updateConstraints_();

	// we want to clip to the hard bound so disable soft bounds
	// remove this override to clip to the soft-bound interval
	overrideAllSoftBoundStates_();

	BoundedValue newValue = BoundedValue(getAdjustedValue());

	// using this setting's constraints, clip the current adjusted value
	getBoundedRange().warpValue(newValue);

	// and store it back in the adjusted context
	setAdjustedValue(newValue);

	// update this setting's soft-bound states...
	updateAllSoftBoundStates_(NON_CHANGING);
}

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	BatchBoundedSetting::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							BATCH_BOUNDED_SETTING, lineNumber,
							pFileName, pPredicate);
}


//======================================================================
//
//  Protected Methods...
//
//======================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  BatchBoundedSetting(...)  [Constructor]
//
//@ Interface-Description
//  Construct a batch, bounded setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forwards on to 'BoundedSetting'.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchBoundedId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchBoundedSetting::BatchBoundedSetting(
										const SettingId::SettingIdType  settingId,
										const SettingId::SettingIdType* arrDependentSettingIds,
										const BoundedInterval*          pIntervalList,
										const SettingBoundId            maxConstraintId,
										const SettingBoundId            minConstraintId,
										const Boolean                   useEpsilonFactoring
										)
: BoundedSetting(settingId, arrDependentSettingIds,
				 pIntervalList, maxConstraintId,
				 minConstraintId, useEpsilonFactoring)
{
	CALL_TRACE("BatchBoundedSetting(settingId, arrDependentSettingIds, ...)");
	AUX_CLASS_PRE_CONDITION((SettingId::IsBatchBoundedId(settingId)),
							settingId);
}	// $[TI1]


//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  resolveDependentChange_(dependentId, isValueIncreasing)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02001] -- responses to dependent setting bound violations...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	BatchBoundedSetting::resolveDependentChange_(
												const SettingId::SettingIdType dependentId,
												const Boolean                  isValueIncreasing
												)
{
	CALL_TRACE("resolveDependentChange_(dependentId, isValueIncreasing)");

#if defined(SIGMA_DEVELOPMENT)
	const Boolean  IS_ADJUSTED_CONTEXT_DEBUG_ON =
		(Settings_Validation::IsDebugOn(ADJUSTED_CONTEXT));

	if ( Settings_Validation::IsDebugOn(BOUNDED_SETTING) )
	{
		Settings_Validation::TurnDebugOn(ADJUSTED_CONTEXT);
		cout << "RESOLVE-00:"
			<< "  dependent = " << SettingId::GetSettingName(dependentId)
		<< ", isIncr = " << isValueIncreasing
			<< endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	const BoundStatus*  pBoundStatus;

	Setting*  pDependentSetting = SettingsMgr::GetSettingPtr(dependentId);

	// initially notify the dependent setting of the primary setting's change...
	pBoundStatus = pDependentSetting->acceptPrimaryChange(getId());

	// used to determine, and return, whether a change to this primary
	// setting was made; anytime a dependent setting's change is restricted,
	// the primary setting requires change...
	const Boolean  DID_BOUND_VIOLATION_OCCUR = (pBoundStatus != NULL);

	if ( DID_BOUND_VIOLATION_OCCUR )
	{	// $[TI1] -- a dependent bound violation occurred...
		// get a reference to this primary setting's bound status...
		BoundStatus&  rBoundStatus = getBoundStatus_();

#if defined(SIGMA_DEVELOPMENT)
		if ( Settings_Validation::IsDebugOn(BOUNDED_SETTING) )
		{
			cout << "RESOLVE-01:"
				<< "  boundId = " << pBoundStatus->getBoundId()
			<< endl;
		}
#endif // defined(SIGMA_DEVELOPMENT)

		// set this primary setting's bound status with the dependent bound
		// violation information...
		rBoundStatus.setBoundStatus(pBoundStatus->getBoundState(),
									pBoundStatus->getBoundId());
		rBoundStatus.setViolationId(pBoundStatus->getViolatedSettingId());

		BoundedValue  newPrimaryValue;

		// Since the primary setting is being limited by the dependent's limit,
		// warp the primary setting in the "same" direction it was trying to go
		// to ensure that the primary setting is not limited any more than
		// necessary (BILEVEL DCS 66).
		BoundedRange::WarpDir  warpDirection =
			(isValueIncreasing) ? BoundedRange::WARP_UP	// $[TI1.3]
			: BoundedRange::WARP_DOWN;	// $[TI1.4]

		// re-calculate the primary setting's value, based on the dependent
		// setting's "clipped" value...
		newPrimaryValue = calcBasedOnSetting_(dependentId, warpDirection,
											  BASED_ON_ADJUSTED_VALUES);

		// store the new primary setting value...
		setAdjustedValue(newPrimaryValue);

		//===================================================================
		//  re-update the dependent setting...
		//===================================================================

		// re-notify the dependent setting of the primary setting's
		// re-calculation...
		pBoundStatus = pDependentSetting->acceptPrimaryChange(getId());

		if ( pBoundStatus != NULL )
		{	// $[TI1.1]
			// the primary setting was "rounded" when re-calculating itself based
			// on the "clipped" dependent setting, but "rounding" didn't back the
			// primary setting away from its bound-violating value, therefore
			// -- based on whether the primary setting is increasing in value, or
			// decreasing -- "warp" the newly calculated primary setting value
			// away from its violating value...
			warpDirection = (isValueIncreasing) ? BoundedRange::WARP_DOWN // $[TI1.1.1]
							: BoundedRange::WARP_UP;  // $[TI1.1.2]

#if defined(SIGMA_DEVELOPMENT)
			if ( Settings_Validation::IsDebugOn(BOUNDED_SETTING) )
			{
				cout << "RESOLVE-02:"
					<< "  boundId = " << pBoundStatus->getBoundId()
				<< endl;
			}
#endif // defined(SIGMA_DEVELOPMENT)

			// re-calculate the primary setting's value, based on the dependent
			// setting's "clipped" value -- this time "warping" against the
			// direction of change...
			newPrimaryValue = calcBasedOnSetting_(dependentId, warpDirection,
												  BASED_ON_ADJUSTED_VALUES);

			// store the new primary setting value...
			setAdjustedValue(newPrimaryValue);

			//===================================================================
			//  re-update the dependent setting, AGAIN...
			//===================================================================

			// re-re-notify the dependent setting of the primary setting's final
			// change...
			pDependentSetting->acceptPrimaryChange(getId());
		}	// $[TI1.2] -- no dependent bound was violated; re-calc worked...
	}	// $[TI2] -- no dependent bound was violated...

#if defined(SIGMA_DEVELOPMENT)
	if ( Settings_Validation::IsDebugOn(BOUNDED_SETTING) )
	{
		if ( !IS_ADJUSTED_CONTEXT_DEBUG_ON )
		{
			Settings_Validation::TurnDebugOff(ADJUSTED_CONTEXT);
		}
	}
#endif // defined(SIGMA_DEVELOPMENT)

	return(DID_BOUND_VIOLATION_OCCUR);
}


//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method:  calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [virtual, const]
//
//@ Interface-Description
//  A virtual method that is to be overridden by all settings that have
//  dependent settings.  This method is called by 'resolveDependentChange_()'
//  to recalculate this setting's value, based on the value of
//  'basedOnSettingId'.  The 'warpDirection' tells this method which direction
//  to round the calculated value to, and 'basedOnCategory' tells this
//  method where to get the values of the other settings needed for the
//  calculation.
//
//  This class's version of this method is NEVER to be called -- it asserts
//  this restriction.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (FALSE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
	BatchBoundedSetting::calcBasedOnSetting_(
											const SettingId::SettingIdType basedOnSettingId,
											const BoundedRange::WarpDir    warpDirection,
											const BasedOnCategory          basedOnCategory
											) const
{
	CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

	AUX_CLASS_ASSERTION_FAILURE((Uint32((getId() << 16) | basedOnSettingId)));

	//-------------------------------------------------------------------
	// the remaining block of code is NEVER run...
	//-------------------------------------------------------------------

	BoundedValue  dummyValue;

	dummyValue.value     = 0.0f;
	dummyValue.precision = ONES;

	return(dummyValue);
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  print_(ostr)  [const]
//
// Interface-Description
//  Print the contents of this bounded setting into 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//  Build message with setting contents inserted in it.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Ostream&
	BatchBoundedSetting::print_(Ostream& ostr) const
{
	CALL_TRACE("print_(ostr)");

	// forward to 'BoundedSetting'...
	BoundedSetting::print_(ostr);

	// add "adjusted" value...
	BoundedValue  adjustedValue = getAdjustedValue();

	ostr << "  Adjusted Value:  " << adjustedValue << endl;
	ostr << "  isChanged():     " << ((isChanged()) ? "TRUE" : "FALSE")
	<< endl;

	return(ostr);
}

#endif  // defined(SIGMA_DEVELOPMENT)
