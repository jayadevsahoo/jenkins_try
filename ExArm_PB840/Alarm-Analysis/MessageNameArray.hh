#ifndef MessageNameArray_HH
#define MessageNameArray_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MessageNameArray - derived from template class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/MessageNameArray.hhv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 003   By:   sah      Date: 10/19/99         DR Number:   5551
//      Project:   ATC
//      Description:
//         Added equivalence operators to allow array-based equivalence
//         tests.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "MessageName.hh"
#include "Array_MessageName_NUM_MESSAGE.hh"
//@ End-Usage


class MessageNameArray : public FixedArray(MessageName,NUM_MESSAGE) 
{

  public:

    MessageNameArray(void);
    MessageNameArray(const MessageNameArray& rArray); 
    MessageNameArray(const FixedArray(MessageName,NUM_MESSAGE)& rArray); 
    ~MessageNameArray(void);

    inline void   operator=(const MessageNameArray& rArray);   

    inline Boolean   operator==(const MessageNameArray& rArray) const;   
    inline Boolean   operator!=(const MessageNameArray& rArray) const;   

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

  protected:

  private:

};


// Inlined methods
#include "MessageNameArray.in"


#endif // MessageNameArray_HH 
