
#ifndef AveragedStabilityBuffer_HH
#define AveragedStabilityBuffer_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AveragedStabilityBuffer - class to chek the stability of data buffer.
//---------------------------------------------------------------------
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/AveragedStabilityBuffer.hhv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  20-Apr-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "SummationBuffer.hh"

//@ Usage-Classes
class Trigger;
class BreathPhase;
//@ End-Usage

class AveragedStabilityBuffer: public SummationBuffer
{
  public:
	AveragedStabilityBuffer(Real32 *pBuffer, Uint32 bufferSize);
	virtual ~AveragedStabilityBuffer(void);
  
	static void SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL, 
			      const char*       pPredicate = NULL);

	virtual void updateBuffer( const Real32 data) ;
	Boolean isStable( void);

  protected:

  private:
	AveragedStabilityBuffer(void);	    // Declared but not implemented
    AveragedStabilityBuffer(const AveragedStabilityBuffer&);    // Declared but not implemented
    void operator=(const AveragedStabilityBuffer&); // Declared but not implemented

	//@ Data-Member: counter_
	// A counter to ensure that the minimum requirement data has
	// been collected before declaring the buffer is stablized.
	Int32 counter_;

};


#endif // AveragedStabilityBuffer_HH 
