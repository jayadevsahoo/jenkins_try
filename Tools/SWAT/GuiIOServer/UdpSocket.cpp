#include "StdAfx.h"
#include "UdpSocket.h"
#include "UdpCommunication.h"

const UINT SWAT_GUI_COMMAND_UDP_PORT  = 7019;
const UINT SWAT_BD_COMMAND_UDP_PORT   = 7020;
const UINT SWAT_PC_UDP_PORT           = 7021;

UdpSocket::UdpSocket(void)
:PUdpComm_(NULL)
{
    PUdpComm_ = new UdpCommunication();
}

UdpSocket::~UdpSocket(void)
{
    if ( PUdpComm_ )
    {
        delete PUdpComm_;
    }
}


bool UdpSocket::Create()
{
    

    return PUdpComm_ != NULL;
}

bool UdpSocket::Connect(const CHAR * addr, UINT port)
{
    Close();
    return PUdpComm_->setup( UdpCommunication::UDP_BOTH, SWAT_PC_UDP_PORT, 
        addr, SWAT_GUI_COMMAND_UDP_PORT );
}

void UdpSocket::Close()
{
    PUdpComm_->stop();
}

INT UdpSocket::Send(const CHAR * sendbuf)
{
    DWORD bytesWritten = 0;

    return PUdpComm_->writeData( (Uint8*) sendbuf, strlen(sendbuf), &bytesWritten);
}

INT UdpSocket::Receive(CHAR * recvbuf, INT recvbuflen, INT maxMsToWait)
{
    DWORD bytesRead = 0;
    bool success = PUdpComm_->readData( (Uint8*) recvbuf, recvbuflen, &bytesRead, maxMsToWait );

    if ( success )
    {
        return bytesRead;
    }

    return -1;
}

bool UdpSocket::ReceiveReady()
{
    return true;
}