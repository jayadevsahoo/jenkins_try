
#ifndef VcmPsvPhase_HH
#define VcmPsvPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VcmPsvPhase - Vital Capacity Maneuver Pressure Supported
//                      Inspiratory Phase
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VcmPsvPhase.hhv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "PsvPhase.hh"

//@ End-Usage

class VcmPsvPhase : public PsvPhase
{
  public:
    VcmPsvPhase(void);
    virtual ~VcmPsvPhase(void);

    virtual void relinquishControl(const BreathTrigger& trigger) ;
    virtual void newBreath(void);

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;
  
  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;
	virtual void determineFlowAccelerationPercent_( void) ;
	virtual void enableExhalationTriggers_( void) ;

  private:
    VcmPsvPhase( const VcmPsvPhase&) ;			// not implemented...
    void   operator=( const VcmPsvPhase&) ;	// not implemented...

} ;


#endif // VcmPsvPhase_HH 
