
#ifndef NovRamDiscrete_HH
#define NovRamDiscrete_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  NovRamDiscrete - NOVRAM Discrete Value.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamDiscrete.hhv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' to non-inlined method.  Also, added new
//	private method, 'isValid_()'.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
#include "DiscreteValue.hh"
//@ End-Usage


class NovRamDiscrete
{
  public:
    NovRamDiscrete (void);
    ~NovRamDiscrete(void);

    NovRamStatus::InitializationId  initialize(void);
    NovRamStatus::VerificationId    verifySelf(void) const;

    inline Uint  getMaxDiscreteValue(void) const;

    DiscreteValue  getValue   (void) const;
    void           updateValue(const DiscreteValue newValue);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  private:
    NovRamDiscrete(const NovRamDiscrete&);	// not implemented...
    void  operator=(const NovRamDiscrete&);	// not implemented...

    inline Boolean  isValid_(void) const;

    //@ Data-Member:  discreteValue_
    // The NOVRAM discrete value.
    Uint  discreteValue_;

    //@ Data-Member:  MAX_DISCRETE_VALUE_
    // Constant that contains the maximum discrete setting value that can
    // be stored in this class.
    static const Uint  MAX_DISCRETE_VALUE_;

    //@ Data-Member:  ARR_DISCRETE_PATTERN_VALUES_
    // Constant array containing a mapping from discrete setting values
    // (i.e., 0 to N) to discrete NOVRAM bit patterns (e.g., 0x755aa55a)..
    static const Uint  ARR_DISCRETE_PATTERN_VALUES_[];
};


// Inlined methods...
#include "NovRamDiscrete.in"


#endif // NovRamDiscrete_HH 
