#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 2007, Nellcor Puritan Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendEventTarget
//---------------------------------------------------------------------
//@ Interface-Description
//  This class must be inherited by any class which needs to be informed
//  of Trend events.
//---------------------------------------------------------------------
//@ Rationale
//  To provide an interface through which derivatives can receive updates
//  about trend events.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Inheriting classes must do 2 things: 
//  a) Inherit from this class 
//  b) Register for change notices via the TrendEventRegistrar class
// 
//  Optionally, the derived class may overload the trendDataReady() method
//  or the trendManagerStatus() method as needed.
// 
//  They will then be informed of Trend events via the trendDataReady().
//  If more information on the status of the TrendDataMgr is needed, the
//  derived class is also free to overload the trendManagerStatus() method
//  to receive updates on the status.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//  This class should not be instantiated directly. 
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendEventTarget.ccv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  ksg    Date:  18-June-2007    SCR Number: 6237
//  Project:  Trend
//  Description: Initial version.
// 
//=====================================================================

#include "TrendEventTarget.hh"

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: trendDataReady
//
//@ Interface-Description
//  This method informs derived classes that the trend data request is 
//  ready. This method is overloaded in the derived classes.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendEventTarget::trendDataReady(TrendDataSet& rTrendDataSet)
{
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: trendManagerStatus
//
//@ Interface-Description
//  This method informs the derived classes of the TrendDataMgr status.
//  This method is overloaded by the derived classes.
//
//  The TrendDataMgr status can return the below: 
//  isEnabled - The manager is ready to process data request messages
//  isRunning - The manager is collecting trend data
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendEventTarget::trendManagerStatus( Boolean isEnabled, Boolean isRunning )
{
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendEventTarget()  [Default Constructor, Protected]
//
//@ Interface-Description
//  Constructs the TrendEventTarget object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendEventTarget::TrendEventTarget(void)	
{
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendEventTarget  [Destructor, Protected]
//
//@ Interface-Description
//  Nothing to destruct!
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendEventTarget::~TrendEventTarget(void)	
{
	// Do nothing
}
