#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdSystemStateHandler - A handle to the persistent BdSystemState
//  class
//---------------------------------------------------------------------
//@ Interface-Description
// The static class provides an interface to the persistent BdSystemState 
// class. It has methods to reset the BdSystemState object in NovRam, to 
// initialize its ram image, to update the NovRam object with its ram image,
// to update each individual data member in the NovRam object and to 
// interrogate the value of each member. 
//---------------------------------------------------------------------
//@ Rationale
//  The class provides a simple interface to the NovRam object BdSystemState.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All methods in this class are static. The class updates the object in 
//  NovRam indirectly to avoid waiting on a potential mutex. Rather then 
//  updating the NovRam object, a ram image is updated and a flag is set
//  indicating that NovRam update is required. A lower priority maintenance
//  task is responsible to detect this update request and then to write to 
//  NovRam.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/BdSystemStateHandler.ccv   10.7   08/17/07 09:34:04   pvcs  
//
//@ Modification-Log
//
//  Revision: 007  By:  gdc    Date:  12-Dec-2008    SCR Number: 5994
//  Project:  840S
//  Description:
//  	Removed isSafetyPcvActive accessor as dead code.
//
//  Revision: 006  By: rpr     Date:  24-Oct-2008    SCR Number: 6435
//  Project:  S840
//  Description:
//      Added UpdateCalibrateRequestStatus and IsCalibrateO2Requested to 
//		support +20 O2
//
//  Revision: 005  By: yab	   Date:  27-Feb-2002    DR Number: 5854 and 5888
//  Project:  VCP
//  Description:
//		The local copy of the BdSystemState_ class is used to get access to the
//      isApneaActive and SchedulerId fields instead of NovRam copy of the BdSystemState
//      for the IsApneaActive and GetSchedulerId functions.  Removed dead code.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 002  By:  iv    Date:  03-Feb-1997    DR Number: DCS 1297
//       Project:  Sigma (R8027)
//       Description:
//             Deleted O2 interval timer from Novram and added the method
//             StartNovRamUpdate_().
//
//  Revision: 001  By:  iv    Date:  16-Oct-1995    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BdSystemStateHandler.hh"
#include "NovRamManager.hh"
#include "BdQueuesMsg.hh"
#include "SchedulerId.hh"

//@ Usage-Classes
#include "MsgQueue.hh"
//@ End-Usage

//@ Code...

// Initialization of static class members
Boolean BdSystemStateHandler::ChangeInProgress_ = FALSE;
BdSystemState BdSystemStateHandler::BdSystemState_;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Reset
//
//@ Interface-Description
// The method takes no arguments and has no return value.
// This routine should run only during system initialization because of
// multiple thread execution issues with NovRamManager.	The method initializes
// the object BdSystemState that resides in NovRam.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method construct a default BdSystemState object and stores it in NovRam.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdSystemStateHandler::Reset(void)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::Reset(void)");

	BdSystemState bdState;
	NovRamManager::UpdateBdSystemState(bdState);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
// The method takes no arguments and has no return value.
// It copies the BdSystemState information from NovRam into a
// ram image.
//---------------------------------------------------------------------
//@ Implementation-Description
// A static NovRamManager method is used to get the information to
// a data member of this class.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BdSystemStateHandler::Initialize(void)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::Initialize(void)");

	BdSystemState_ = NovRamManager::GetBdSystemState();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpdateNovRam
//
//@ Interface-Description
// The method takes no arguments and has no return value.
// Since this method is called from only one lower priority task, it uses a
// semaphore to protect the integrity of the BdSystemState class. The
// method updates the NovRam object of BdSystemState with its ram image.
//---------------------------------------------------------------------
//@ Implementation-Description
// The data member ChangeInProgress_ is used to verify no interruption
// to the update of NovRam. The member BdSystemState_ is copied to the
// BdSystemState object in NovRam using a NovRamManager static method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
BdSystemStateHandler::UpdateNovRam(void)
{   // $[TI1]
    CALL_TRACE("BdSystemStateHandler::UpdateNovRam(void)");

    BdSystemState bdState;

    do
    {
	ChangeInProgress_ = FALSE;
        bdState = BdSystemState_;
    }
    while (ChangeInProgress_);	// $[TI2]

    NovRamManager::UpdateBdSystemState(bdState);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	UpdateO2RequestStatus  
//
//@ Interface-Description
// The method takes a Boolean argument and returns no value. It sets the 
// ram image of the BdSystemState object with the argument passed in and
// starts nov ram update.
//---------------------------------------------------------------------
//@ Implementation-Description
// The member BdSystemState_  is set with the argument	passed in.
// StartNovRamUpdate_ is called.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdSystemStateHandler::UpdateO2RequestStatus(const Boolean is100PercentO2Requested)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::UpdateO2RequestStatus(Boolean is100PercentO2Requested)");

	BdSystemState_.set100PercentO2Request(is100PercentO2Requested);
    StartNovRamUpdate_();
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	UpdateCalibrateRequestStatus  
//
//@ Interface-Description
// The method takes a Boolean argument and returns no value. It sets the 
// ram image of the BdSystemState object with the argument passed in and
// starts nov ram update.
//---------------------------------------------------------------------
//@ Implementation-Description
// The member BdSystemState_  is set with the argument	passed in.
// StartNovRamUpdate_ is called.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdSystemStateHandler::UpdateCalibrateRequestStatus(const Boolean isCalibrateO2Requested)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::UpdateCalibrateRequestStatus(Boolean isCalibrateO2Requested)");

	BdSystemState_.setCalibrateO2Request(isCalibrateO2Requested);
    StartNovRamUpdate_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	UpdateApneaActiveStatus  
//
//@ Interface-Description
// The method takes a Boolean argument and returns no value. It sets the 
// ram image of the BdSystemState object with the argument passed in and
// starts nov ram update.
//---------------------------------------------------------------------
//@ Implementation-Description
// The member BdSystemState_  is set with the argument	passed in.
// StartNovRamUpdate_ is called.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdSystemStateHandler::UpdateApneaActiveStatus(const Boolean isApneaActive)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::UpdateApneaActiveStatus(Boolean isApneaActive)");

	BdSystemState_.setApneaActiveStatus(isApneaActive);
    StartNovRamUpdate_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	UpdateSafetyPcvActiveStatus  
//
//@ Interface-Description
// The method takes a Boolean argument and returns no value. It sets the 
// ram image of the BdSystemState object with the argument passed in and
// starts nov ram update.
//---------------------------------------------------------------------
//@ Implementation-Description
// The member BdSystemState_  is set with the argument	passed in.
// StartNovRamUpdate_ is called.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdSystemStateHandler::UpdateSafetyPcvActiveStatus(const Boolean isSafetyPcvActive)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::UpdateSafetyPcvActiveStatus(Boolean isSafetyPcvActive)");

	BdSystemState_.setSafetyPcvActiveStatus(isSafetyPcvActive);
    StartNovRamUpdate_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	UpdateSchedulerId
//
//@ Interface-Description
// The method takes a SchedulerId::SchedulerIdValue argument and returns 
// no value. It sets the ram image of the BdSystemState object with the 
// argument passed in and starts nov ram update.
//---------------------------------------------------------------------
//@ Implementation-Description
// The member BdSystemState_  is set with the argument	passed in.
// StartNovRamUpdate_ is called.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdSystemStateHandler::UpdateSchedulerId(const SchedulerId::SchedulerIdValue id)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::UpdateSchedulerId(SchedulerId::SchedulerIdValue id)");

	BdSystemState_.setSchedulerId(id);
	StartNovRamUpdate_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	GetSchedulerId
//
//@ Interface-Description
// The method takes no argument and returns a variable of type 
// SchedulerId::SchedulerIdValue.
// The scheduler id is retrieved from the NovRam object BdSystemState
// and then returned to the caller.
//---------------------------------------------------------------------
//@ Implementation-Description
// A NovRamManager static access method is used to get the BdSystemState
// object. An access method of BdSystemState object is then used to 
// get the data member to be returned to the caller. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SchedulerId::SchedulerIdValue
BdSystemStateHandler::GetSchedulerId(void)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::GetSchedulerId(void)");

	return (BdSystemState_.getSchedulerId());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	Is100PercentO2Requested
//
//@ Interface-Description
// The method takes no argument and returns a variable of type Boolean.
// The 100 percent O2 request state is retrieved from the NovRam object
// BdSystemState and is returned to the caller.
//---------------------------------------------------------------------
//@ Implementation-Description
// A NovRamManager static access method is used to get the BdSystemState
// object. An access method of BdSystemState object is then used to
// get the data member to be returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
BdSystemStateHandler::Is100PercentO2Requested(void)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::Is100PercentO2Requested(void)");

	BdSystemState bdState = NovRamManager::GetBdSystemState();
	return (bdState.is100PercentO2Requested());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	IsCalibrateO2Requested
//
//@ Interface-Description
// The method takes no argument and returns a variable of type Boolean.
// The Calibrate O2 request state is retrieved from the NovRam object
// BdSystemState and is returned to the caller.
//---------------------------------------------------------------------
//@ Implementation-Description
// A NovRamManager static access method is used to get the BdSystemState
// object. An access method of BdSystemState object is then used to
// get the data member to be returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
BdSystemStateHandler::IsCalibrateO2Requested(void)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::IsCalibrateO2Requested(void)");

	BdSystemState bdState = NovRamManager::GetBdSystemState();
	return (bdState.isCalibrateO2Requested());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	IsApneaActive
//
//@ Interface-Description
// The method takes no argument and returns a variable of type Boolean.
// The apnea active state is retrieved from the NovRam object
// BdSystemState and then returned to the caller.
//---------------------------------------------------------------------
//@ Implementation-Description
// A NovRamManager static access method is used to get the BdSystemState
// object. An access method of BdSystemState object is then used to
// get the data member to be returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
BdSystemStateHandler::IsApneaActive(void)
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::IsApneaActive(void)");

	return (BdSystemState_.isApneaActive());
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BdSystemStateHandler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BDSYSTEMSTATEHANDLER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: 	StartNovRamUpdate_
//
//@ Interface-Description
// The method takes no argument and has no return value.
// Nov Ram update is initiated by posting a message to the BD Status task
// queue.
//---------------------------------------------------------------------
//@ Implementation-Description
// The data member ChangeInProgress_ is set to TRUE to verify no interruption
// to the update of NovRam. The message BdQueuesMsg::BD_NOVRAM_UPDATE_EVENT
// is put on the BD_STATUS_TASK_Q queue.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
BdSystemStateHandler::StartNovRamUpdate_(void) 
{	// $[TI1]
	CALL_TRACE("BdSystemStateHandler::StartNovRamUpdate_(void)");

	BdQueuesMsg bdMsg;
    ChangeInProgress_ = TRUE;
	bdMsg.event.eventType = BdQueuesMsg::BD_NOVRAM_UPDATE_EVENT;
	MsgQueue::PutMsg(::BD_STATUS_TASK_Q, bdMsg.qWord);
}



