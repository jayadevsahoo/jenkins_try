
//*****************************************************************************
//
//  AnalogInputThread.h - Header file for AnalogInputThread_t class.
//
//      This thread runs at the highest priority (0). It provides the fundamental
//      timing to the rest of the threads by sending a message with analog data
//      to the Servos Task every 2ms.
//
//      Because it is running at such a high priority, it must be very quick.
//      It must simply read the analog data, send the data on to the servos,
//      and then sleep for 2ms.
//
//*****************************************************************************

#ifndef ANALOG_INPUT_THREAD_H

#define ANALOG_INPUT_THREAD_H

#include "Sigma.hh"
#include "AnalogInputs.h"


class AnalogInputThread_t
{
    public:
        static void Initialize();                           // Called prior to starting the thread
        static void Thread();

        static uint32_t ThreadProcedure(void *ProcedureArg);

    protected:


    private:
        AnalogInputThread_t(void);                          // Not to be created
        ~AnalogInputThread_t(void);
};


#endif

