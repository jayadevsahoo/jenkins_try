#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimerModeTrigger - Triggers when corresponding Interval
//         Timer expires.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class triggers a change in the current mode of breathing,
//  to a new mode that is dependent upon the usage of the trigger. 
//  If an instance of this trigger is pointed to by the currently active
//  list of ModeTriggers and it is enabled, then its condition is 
//  evaluated every BD cycle.   When this trigger's interval timer
//  expires, the trigger is considered to have "fired".  
//---------------------------------------------------------------------
//@ Rationale
//  This class implements time dependent mode triggers.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains a boolean state variable to keep track of 
//  whether this trigger is enabled or not.  If the trigger is not 
//  enabled, it will always return a state of false.  If it is enabled, 
//  the trigger "fires" when the referenced interval timer expires. 
//  When a timer expires, timeUpHappened is called to update the 
//  data member timeUp_ of this trigger. 
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TimerModeTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 003  By:  sp    Date:  24-Jan-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 002  By:  iv    Date:  11-Jan-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Modified the method triggerCondition_() to replace the call
//             to disable the trigger with the assignment: timeUp_ = FALSE 
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "TimerModeTrigger.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimerModeTrigger
//
//@ Interface-Description
//      This constructor takes arguments of interval timer and trigger id.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ by calling ModeTrigger's constructor.
//	Initialize the reference rIntervalTimer_ to the argument iTimer.
//	Call the TimerTarget base class constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimerModeTrigger::TimerModeTrigger(IntervalTimer& iTimer, const Trigger::TriggerId id)
: TimerTarget(),
  rIntervalTimer_(iTimer),
  ModeTrigger(id)
{
  CALL_TRACE("TimerModeTrigger(IntervalTimer& iTimer, const Trigger::TriggerId id)");
  // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimerModeTrigger() 
//
//@ Interface-Description
//      Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimerModeTrigger::~TimerModeTrigger(void)
{
  CALL_TRACE("~TimerModeTrigger()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
//	This method takes a reference to the interval timer that has 
//  expired as a parameter, and has no return value.  This method is
//	used by interval timer to update the state of this trigger.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update timeUp_. The expired timer is stopped. 
//---------------------------------------------------------------------
//@ PreCondition
//      CLASS_PRE_CONDITION(&rIntervalTimer_ == &rTimer).
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
TimerModeTrigger::timeUpHappened(const IntervalTimer& rTimer)
{
  CALL_TRACE("timeUpHappened(const IntervalTimer& rTimer)");

  // $[TI1]
  CLASS_PRE_CONDITION(&rIntervalTimer_ == &rTimer);	
  timeUp_ = TRUE;
  rIntervalTimer_.stop();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disable
//
//@ Interface-Description
//     This method takes no parameters and has no return value.
//     This trigger becomes disabled, and is no longer able to detect the trigger
//     condition it is set up to monitor.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The state data member isEnabled_ and timeUp_ is set to false.
//     The rIntervalTimer_ is stopped.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//     none
//@ End-Method
//=====================================================================
void
TimerModeTrigger::disable(void)
{
  CALL_TRACE("disable(void)");
 
  // $[TI1]
  timeUp_ = FALSE;
  rIntervalTimer_.stop();
  isEnabled_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TimerModeTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, TIMERMODETRIGGER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//	This method takes no parameters and returns a boolean value.
//	This method returns true if the corresponding interval timer has
//	expired otherwise it  returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If timeUp occured then returns true and the trigger is disabled,
//	else returns false.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
TimerModeTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_(void)");
 
  Boolean rtnValue = FALSE;

  if (timeUp_)
  {
     // $[TI1]
     rtnValue = TRUE;
     timeUp_ = FALSE;
  }
  // $[TI2]


  // TRUE: $[TI3]
  // FALSE: $[TI4]
  return (rtnValue);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

