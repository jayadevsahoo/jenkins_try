#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: IntervalTimer - A general purpose server that is responsible to
// notify its client when a pre-set time interval has elapsed
//---------------------------------------------------------------------
//@ Interface-Description
//	Any object can start or stop an IntervalTimer, and set or extend
//	the duration of the timer.  IntervalTimers can be stopped, 
//	started or reset.  This class has no newCycle() method, so it
//	must be contained on a list in a TimerMediator in order to be
//	updated every BD cycle, when the timer is on.
//	Since the timer uses an Int32 to store its count, the count is limited
//	to the max value of an Int32 in milliseconds which is: 26.37 hours.
//---------------------------------------------------------------------
//@ Rationale
//	This class provides timer services to any object which may need
//	a timer. The interval is the duration, from the current cycle that
//	the timer needs to time in msec.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Timer has private data members that are used to determine if the
//	timer is on, to keep track of current time and to store the target
//	time. The method updateTimer has to be called every BD cycle to 
//	update the state of the timer. 
//	When the current time is equal to the target time, the timer 
//	notifies its client that the target time has elapsed.
//	The currentTime_ has to be pre-increment because in the main BDTask
//	loop, the TimerMediator is the the first one to be executed.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//	The pTargetPtr_ has to be set up via the setupTarget method before
//  the timer can be used.
//	The timer can not handle intervals greater than 26.37 hours.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/IntervalTimer.ccv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Code formatting changes only.
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  sp    Date:  29-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection Rework.
//
//  Revision: 004  By:  sp    Date:  27-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date:  15-Feb-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             added #include "BD_IO_Devices.hh" since CYCLE_TIME_MS was moved there.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "IntervalTimer.hh"

//@ Usage-Classes

#include "TimerTarget.hh"

#include "BD_IO_Devices.hh"

//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IntervalTimer 
//
//@ Interface-Description
//	Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize status_ and pTargetPtr_.  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
IntervalTimer::IntervalTimer(void)
{
   CALL_TRACE("IntervalTimer()");

   // $[TI1]
   status_ = OFF;
   pTargetPtr_ = NULL;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~IntervalTimer 
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
IntervalTimer::~IntervalTimer(void)
{
    CALL_TRACE("~IntervalTimer()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateTimer
//
//@ Interface-Description
//	This method takes no parameters and has no return value.
//	This method updates current elapsed time every BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the timer is on, currentTime_ is updated. If the new updated
//	currentTime >= targetTime_, then the timer expires by notifying
//	its client that the target time has elapsed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
IntervalTimer::updateTimer(void) 
{
	CALL_TRACE("IntervalTimer::updateTimer(void)");

	if ( status_ == ON)
	{
		// $[TI1]
		currentTime_ += CYCLE_TIME_MS;

		if (currentTime_ >= targetTime_)
		{
			//$[TI2]
			CLASS_PRE_CONDITION(pTargetPtr_!=NULL);
			pTargetPtr_ -> timeUpHappened(*this);
		}
		//$[TI3]
	}
	//$[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
IntervalTimer::SoftFault(const SoftFaultID  softFaultID,
			 const Uint32       lineNumber,
			 const char*        pFileName,
			 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, INTERVALTIMER,
                          lineNumber, pFileName, pPredicate);
}


//====================================================================
//
//  Private Methods...
//
//====================================================================

