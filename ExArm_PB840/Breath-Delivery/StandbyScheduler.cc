#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: StandbyScheduler - handles the ventilator state on powerup
// when the patient circuit is disconnected.
//---------------------------------------------------------------------
//@ Interface-Description
// The StandbyScheduler is derived from an IdleModeScheduler.
// It sets up the next breath, determines the next scheduler
// when control is released, and validates the transition from the
// previous scheduler when it takes control.
//---------------------------------------------------------------------
//@ Rationale
// On powerup, before a patient is connected to the ventilator, 
// a standby mode is in effect during which the ventilator waits for patient
// connection and/or new patient setup completion.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler implements all its functionality by defining all the
// protected pure virtual methods declared in its base class. 
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/StandbyScheduler.ccv   25.0.4.0   19 Nov 2013 14:00:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added case to reject RM maneuver requests during Standby mode.
//
//  Revision: 013  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 012  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling of inspiratory pause event.
//
//  Revision: 011  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 010  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 009  By:  iv    Date:  04-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Cleanup for code inspections.
//
//  Revision: 008  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 007 By:  iv   Date:   04-June-1996    DR Number: DCS 715 
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated the calls to the base class for takeControl() and
//             UpdateSchedulerId().
//             Eliminated the argument from the method validateScheduler_()
//
//  Revision: 006 By:  iv   Date:   15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 005 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674, 672 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//             Allow transition back from Svo and short power interruption to
//             standby in validateSchedule_().
//
//  Revision: 004 By:  iv    Date:  06-Dec-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated determineNextScheduler_() and validateScheduler_()
//             with a call VentAndUserEventStatus::PostEventStatus ()
//             to notify the BD state.
//
//  Revision: 003 By:  kam   Date:  06-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from validateScheduler_().
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "StandbyScheduler.hh"
#  include "ModeTriggerRefs.hh"
#  include "BreathMiscRefs.hh"
#  include "TriggersRefs.hh"
#  include "SchedulerRefs.hh"

//@ Usage-Classes
#  include "Trigger.hh"
#  include "BreathTriggerMediator.hh"
#  include "BreathSet.hh"
#  include "PhasedInContextHandle.hh"
#  include "PhaseInEvent.hh"
#  include "BdSystemStateHandler.hh"
#  include "VentAndUserEventStatus.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StandbyScheduler()  [Default Constructor]
//
//@ Interface-Description
// $[04119] $[04125] $[04130] $[04141]
// The constructor takes no arguments. It constructs the object with the proper id.
// The list of valid mode triggers, used by the mode trigger mediator is
// initialized as well. The triggers are ordered based on priority - the most urgent
// is first.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor invokes the base class constructor with the STANDBY id
// argument. It initializes the private data member pModeTriggerList_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	The number of elements on the trigger list is asserted
//  to be MAX_NUM_STANDBY_TRIGGERS and MAX_NUM_STANDBY_TRIGGERS to be less than
//  MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_STANDBY_TRIGGERS = 2;
StandbyScheduler::StandbyScheduler(void) :
						IdleModeScheduler(SchedulerId::STANDBY)
{
// $[TI1]
	CALL_TRACE("StandbyScheduler()");

	Int32 ii = 0;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RDiscAutoResetTrigger;
	pModeTriggerList_[ii] = (ModeTrigger*)NULL;

	CLASS_ASSERTION(ii == MAX_NUM_STANDBY_TRIGGERS &&
						MAX_NUM_STANDBY_TRIGGERS < MAX_MODE_TRIGGERS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~StandbyScheduler()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

StandbyScheduler::~StandbyScheduler(void)
{
  CALL_TRACE("~StandbyScheduler()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
StandbyScheduler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, STANDBYSCHEDULER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupBreath_
//
//@ Interface-Description
// This method is invoked by the base class method determineBreathPhase().
// It creates a new breath record. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The method uses the instance variable: RBreathSet to create a new
// breath  record.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
StandbyScheduler::setupBreath_(void)
{
// $[TI1]
	CALL_TRACE("StandbyScheduler::setupBreath_(void)");

	//Update the breath record
 	RBreathSet.newBreathRecord(SchedulerId::STANDBY,
 						::NULL_MANDATORY_TYPE,    // no mandatory type is specified
 						NON_MEASURED,        // breath type
 						BreathPhaseType::NON_BREATHING
 					);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineNextScheduler_
//
//@ Interface-Description
// This method is invoked by the base class method relinquishControl().  It
// accepts a TriggerId and a Boolean as arguments and has no return value.  If
// new patient setup has been completed, and reconnection detected, the next
// scheduler is set to be the set mode.  If new patient setup has not been
// completed, Sigma starts safety pcv mode of breathing. The standby scheduler
// may relinquish control to the Svo scheduler if Svo conditions are detected.
// To immediately start a breath upon standby reset, an immediate breath
// trigger is enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// The trigger id is checked to determine the next scheduler.  To determine the
// set mode, settings are phased-in using the handle PendingContextHandle.  The
// Boolean argument passed in is checked to determine whether vent setup has
// been completed.  If vent setup has been completed, the next scheduled mode
// is the set scheduler otherwise, it is set to safety pcv.  The immediate
// breath trigger is enabled, using the method enable() in the instance
// variable rImmediateBreathTrigger.  Once the next scheduler is determined, it
// is instructed to take control.
//---------------------------------------------------------------------
//@ PreCondition
// the trigger id is checked to be in the following range:
// DISCONNECT_AUTORESET, SVO
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
StandbyScheduler::determineNextScheduler_(const Trigger::TriggerId triggerId,
						const Boolean isVentSetupComplete) 
{
	CALL_TRACE("StandbyScheduler::determineNextScheduler_(\
				const Trigger::TriggerId triggerId,\
				const Boolean isVentSetupComplete)");

	BreathPhaseScheduler* pNextScheduler = NULL;

	// check if patient circuit reconnect:
	if( triggerId == Trigger::DISCONNECT_AUTORESET )
	{
	// $[TI1]
		// notify BD Status Task of patient connection
		VentAndUserEventStatus::PostEventStatus (EventData::PATIENT_CONNECT, EventData::ACTIVE);
		// check if vent setup has been completed:
		if(isVentSetupComplete)
		{
		// $[TI1.1]
			// phase-in the new vent settings (for start of inspiration):
			PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
			// Signal Event for swat
			swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

			// determine the set mode and start it:
			pNextScheduler = &BreathPhaseScheduler::EvaluateSetScheduler();
		}
		else // if vent setup not complete:
		{
		// $[TI1.2]
			pNextScheduler = (BreathPhaseScheduler*)&RSafetyPcvScheduler;
		}
	}
	else if(triggerId == Trigger::SVO)
	{
	// $[TI2]
		pNextScheduler = (BreathPhaseScheduler*)&RSvoScheduler;
	}
	else
	{
	// $[TI3]
		CLASS_PRE_CONDITION( (triggerId == Trigger::DISCONNECT_AUTORESET) ||
			(triggerId == Trigger::SVO) );
	}

	// disable all triggers on the breath trigger list.
	RBreathTriggerMediator.resetTriggerList();
	// enable an immediate breath trigger.
	((Trigger&)RImmediateBreathTrigger).enable();

	pNextScheduler->takeControl(*this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: validateScheduler_
//
//@ Interface-Description
// This method is invoked by the base class method takeControl().  It takes no
// arguments and has no return value. It checks to ensure the validity of the
// scheduler. The scheduler id is stored in persistent memory.
//---------------------------------------------------------------------
//@ Implementation-Description
// The id of the previous scheduler is checked for proper pre-conditions.
//---------------------------------------------------------------------
//@ PreCondition
//	The previous scheduler id is checked to be equal to POWER_UP || SVO.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
StandbyScheduler::validateScheduler_(void) 
{
// $[TI1]
	CALL_TRACE("StandbyScheduler::validateScheduler_(void)");

    SchedulerId::SchedulerIdValue id = PPreviousScheduler_->getId();	
 	CLASS_PRE_CONDITION( (id == SchedulerId::POWER_UP) ||
 	                     (id == SchedulerId::SVO) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[04215] $[01255]
// The argument of type EventId indicates which user event is active.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is rejected by
// invoking a call to the static method:
// BreathPhaseScheduler::RejectManualInspiration_(eventStatus).
// The alarm reset event is ignored, while the eventStatus argument value is checked for
// TRUE.
// The expiratory and inspiratory pause events are rejected by invoking a call to
// the static methods: BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus) and
// BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus) .
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
StandbyScheduler::reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus)
{
	CALL_TRACE("StandbyScheduler::reportEventStatus_(const EventData::EventId id,\
													 const Boolean eventStatus)");

	EventData::EventStatus rtnStatus = EventData::IDLE;

	switch (id)
	{
		case EventData::MANUAL_INSPIRATION:
		// $[TI1]
			rtnStatus =
					BreathPhaseScheduler::RejectManualInspiration_(eventStatus);
			break;

		case EventData::ALARM_RESET:
		// $[TI2]
			// ignore alarm reset
			CLASS_PRE_CONDITION(eventStatus);
			break;

		case EventData::EXPIRATORY_PAUSE:
		// $[TI3]
			rtnStatus =
					BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus);
			break;

		case EventData::INSPIRATORY_PAUSE:
		// $[TI4]
		// inspiratory pause shall not be active
		// $[BL04007]
			rtnStatus =
					BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus);
			break;

		case EventData::NIF_MANEUVER:
		case EventData::P100_MANEUVER:
		case EventData::VITAL_CAPACITY_MANEUVER:
			// $[RM12032] Reject RM maneuvers in Idle (Standby) mode
			rtnStatus = EventData::REJECTED;
			break;

		default:
		// $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(id);
	}	

	return (rtnStatus);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

