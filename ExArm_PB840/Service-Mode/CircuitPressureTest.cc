#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  CircuitPressureTest - checks the pressure transducers and
//	autozero solenoids.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class implements the circuit pressure test, which cross-checks
//	the inspiratory and expiratory pressure transducers at various
//	pressures and tests the autozero solenoids.  The Service Mode
//	version of the test contains extra steps to setup for EST, otherwise
//	it and the SST version are identical.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the circuit pressure test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs either the Service Mode or SST version
//	of the test, depending on ventilator state.  Private methods are
//	defined for each test phase.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/CircuitPressureTest.ccv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 09-Oct-1997    DR Number: DCS 2406
//       Project:  Sigma (840)
//       Description:
//       	Use new PSOL exercise procedure in runTest().
//
//  Revision: 003  By:  quf    Date: 16-Jun-1997    DR Number: DCS 2226
//       Project:  Sigma (840)
//       Description:
//       	Added autozero solenoid open/close during zero calibration,
//          increased zero threshold during autozero solenoid test.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "CircuitPressureTest.hh"

//@ Usage-Classes

#include "TaskControlAgent.hh"

#include "SmRefs.hh"
#include "SmPressureController.hh"
#include "SmExhValveController.hh"
#include "SmManager.hh"
#include "SmUtilities.hh"
#include "SmConstants.hh"

#include "Task.hh"

#include "ValveRefs.hh"
#include "MainSensorRefs.hh"
#include "PressureSensor.hh"
#include "Solenoid.hh"

#if defined(SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "SmUnitTest.hh"
# include "CircuitPressureTest_UT.hh"
# include "FakeIo.hh"
# include "FakeControllers.hh"
#endif

//@ End-Usage


//@ Code...


#ifdef SM_TEST
static SigmaState FakeSigmaState = STATE_SERVICE;
#endif  // SM_TEST

// from P/N 4-072218-00
const Real32 RESIDUAL_PRESSURE = 0.25;  // cmH2O


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: CircuitPressureTest()  [Default Constructor]
//
//@ Interface-Description
//  Constructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================
CircuitPressureTest::CircuitPressureTest(void)
{
	CALL_TRACE("CircuitPressureTest::CircuitPressureTest(void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~CircuitPressureTest
//
//@ Interface-Description
//	Destructor.
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
CircuitPressureTest::~CircuitPressureTest(void)
{
	CALL_TRACE("CircuitPressureTest::~CircuitPressureTest(void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTest
//
//@ Interface-Description
//  Performs circuit pressure test.  No argument, no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Assumptions: exh valve open, safety valve closed
//
//	If ventilator is in Service state:
//		Check that AC power is being used, and if not, prompt user to
//		connect AC, then recheck, and if AC is still not connected,
//		generate FAILURE and exit.
//		Prompt to ensure air and O2 are connected.
//		Prompt to remove insp filter and connect test circuit.  $[09047]
//		Open safety valve.
//		Exercise ("pop") the air and O2 psols.
//		Wait for system pressure to decay
//		Execute runTest_().
//	Else if ventilator is in SST state:
//		Open safety valve.
//		Wait for system pressure to decay.
//		Execute runTest_().
//-----------------------------------------------------------------------
//@ PreCondition
//	Ventilator state must be Service or SST.
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
void
CircuitPressureTest::runTest(void)
{
	CALL_TRACE("CircuitPressureTest::runTest(void)");

	CLASS_PRE_CONDITION(
		(TaskControlAgent::GetBdState() == STATE_SERVICE) ||
		(TaskControlAgent::GetBdState() == STATE_SST) );

#ifdef SM_TEST
	FakeSigmaState = STATE_SERVICE;
	char answer[80];
	printf("Run SST test? ");
	scanf("%s", answer);
	if (answer[0] == 'y' || answer[0] == 'Y') FakeSigmaState = STATE_SST;
	printf("fake sigma state = %s\n",
		(FakeSigmaState == STATE_SERVICE) ? "STATE_SERVICE" : "STATE_SST");
#endif  // SM_TEST


#if defined(SIGMA_UNIT_TEST)
	if (SmUnitTest::FakeSigmaState == STATE_SERVICE)
#elif defined(SM_TEST)
	if (FakeSigmaState == STATE_SERVICE)
#else
	if ( (TaskControlAgent::GetBdState() == STATE_SERVICE) )
#endif
	{
	// $[TI1]
		// For EST, perform EST setup steps first then proceed to
		// main part of test.

		Boolean operatorExit = FALSE;
		Boolean acDetected = TRUE;

#ifdef SIGMA_UNIT_TEST
		SmUtilities::TestResult acResult = CircuitPressureTest_UT::AcResult;
#else
		SmUtilities::TestResult acResult = RSmUtilities.checkAcPower();
#endif  // SIGMA_UNIT_TEST

		switch(acResult)
		{
			case SmUtilities::AC_CONNECTED:
			// $[TI1.1]
				// AC power is connected
				break;				

			case SmUtilities::AC_NOT_CONNECTED:
			// $[TI1.2]
				// Running on BPS power
				RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_FAILURE,
						SmStatusId::CONDITION_10);

				acDetected = FALSE;
				break;

			case SmUtilities::OPERATOR_EXIT:
			// $[TI1.3]
				operatorExit = TRUE;
				break;

			default:
			// $[TI1.4]
				CLASS_ASSERTION_FAILURE();
				break;
		}
		// end switch				
			
		if (!operatorExit && acDetected)
		{
		// $[TI1.5]
			if (!RSmUtilities.messagePrompt(
								SmPromptId::CONNECT_AIR_AND_O2_PROMPT) )
			{
			// $[TI1.5.1]
				// EXIT button was pressed
				operatorExit = TRUE;
			}
			// $[TI1.5.2]
			// implied else: ACCEPT key was pressed
		}
		
		// $[TI1.6]
		// implied else

		if (!operatorExit && acDetected)
		{
		// $[TI1.7]
			// prompt to remove insp filter & connect test ckt
			if (!RSmUtilities.messagePrompt(
								SmPromptId::REMOVE_INSP_FILTER_PROMPT) )
			{
			// $[TI1.7.1]
				// EXIT button was pressed
				operatorExit = TRUE;
			}
			else
			
			{
			// $[TI1.7.2]
				// ACCEPT key was pressed

				// open SV to relieve pressure due to psol "pops"
				RSmUtilities.openSafetyValve();
				RSmUtilities.popPsols();

				// wait for pressure to decay
				Task::Delay(1);

				// perform main part of test
				runTest_();
				// NOTE: safety valve is closed during runTest_()
			}
			// end else
		}
		// $[TI1.8]
		// implied else
	
		if (operatorExit)
		{
		// $[TI1.9]
			RSmManager.sendOperatorExitToServiceData();
		}
		// $[TI1.10]
		// implied else
	}
	else
	{
	// $[TI2]
		// For SST, open the SV to bleed system pressure then
		// run main part of test
		RSmUtilities.openSafetyValve();
		Task::Delay(1);

		// perform main part of test
		runTest_();
		// NOTE: safety valve is closed during runTest_()
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
CircuitPressureTest::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("CircuitPressureTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, CIRCUITPRESSURETEST,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTest_
//
//@ Interface-Description
//  Performs main part of circuit pressure test.  No argument, no return
//	value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: exh valve and safety valve both open.
//
//  $[09044]  $[09047]  $[10031]
//	Calibrate pressure transducers to atm pressure and verify DAC count
//	is in range.  FAILURE(s) if either zero calibration fails.
//	Build a 10 cmH2O system pressure.  FAILURE if pressure can't build
//	within timeout.
//	Cross-check transducers at 10 cmH2O.  FAILURE if insp and exp
//	pressures are not within range.
//	Activate autozero solenoids and verify insp and exh pressures are
//	near 0 cmH2O, then deactivate solenoids.  FAILURE(s) if insp
//	and/or exp pressures are not near 0.
//	Build a 50 cmH2O system pressure.  FAILURE if pressure can't build
//	within timeout.
//	Cross-check transducers at 50 cmH2O.  FAILURE if insp and exp
//	pressures are not within range.
//	Build a 100 cmH2O system pressure.  FAILURE if pressure can't build
//	within timeout.
//	Cross-check transducers at 100 cmH2O.  FAILURE if insp and exp
//	pressures are not within range.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
void
CircuitPressureTest::runTest_(void)
{
	CALL_TRACE("CircuitPressureTest::runTest_(void)");

	const Real32 LOW_TEST_PRESSURE = 10.0;  // cmH2O
	const Real32 MID_TEST_PRESSURE = 50.0;  // cmH2O

	// TODO E600 MS Change it back to MS when new exhalation valve code comes in.
	const Real32 HIGH_TEST_PRESSURE = 75.0;  // cmH2O
	//const Real32 HIGH_TEST_PRESSURE = 100.0;  // cmH2O

	Boolean inspZeroPassed = zeroTest_( DELIVERY);
	Boolean exhZeroPassed  = zeroTest_( EXHALATION);

	if (!inspZeroPassed)
	{
	// $[TI1]
		// insp zero out of range
		// Major Failure, Condition 1
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_1);
	}
	// $[TI2]
	// implied else: insp zero calib passed

	if (!exhZeroPassed)
	{
	// $[TI3]
		// exh zero out of range
		// Major Failure, Condition 2
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2);
	}
	// $[TI4]
	// implied else: exh zero calib passed

	RSmUtilities.closeSafetyValve();

	if (inspZeroPassed && exhZeroPassed)
	{
	// $[TI5]
	    RSmExhValveController.close();

		if ( !pressurizeSystemTest_ (LOW_TEST_PRESSURE) )
		{
		// $[TI5.1]
			// Can't pressurize system to 10 cmH2O
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_3);
		}
		else if ( !pressureTest_() )
		{
		// $[TI5.2]
			// Pressure cross-check failed @ 10 cmH2O
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_4);
		}
		else
		{
		// $[TI5.3]
			Boolean inspSolenoidPassed = openSolenoidTest_( DELIVERY);
			Boolean exhSolenoidPassed = openSolenoidTest_( EXHALATION);

			if (!inspSolenoidPassed)
			{
			// $[TI5.3.1]
				// bad insp autozero solenoid
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_5);
			}
			// $[TI5.3.2]
			// implied else: insp autozero solenoid passed

			if (!exhSolenoidPassed)
			{
			// $[TI5.3.3]
				// bad exh autozero solenoid
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_6);
			}
			// $[TI5.3.4]
			// implied else: exh autozero solenoid passed

			if (inspSolenoidPassed && exhSolenoidPassed)
			{
			// $[TI5.3.5]
				Boolean highPressurizationFailed = FALSE;
				Boolean highPressureCrossCheckFailed = FALSE;

				if ( !pressurizeSystemTest_ (MID_TEST_PRESSURE) )
				{
				// $[TI5.3.5.1]
					highPressurizationFailed = TRUE;
				}
				else if ( !pressureTest_() )
				{
				// $[TI5.3.5.2]
					highPressureCrossCheckFailed = TRUE;
				}
				else if ( !pressurizeSystemTest_ (HIGH_TEST_PRESSURE) )
				{
				// $[TI5.3.5.3]
					highPressurizationFailed = TRUE;
				}
				else if ( !pressureTest_() )
				{
				// $[TI5.3.5.4]
					highPressureCrossCheckFailed = TRUE;
				}
				// $[TI5.3.5.5]
				// implied else: passed at 50 and 100 cmH2O

				if (highPressurizationFailed)
				{
				// $[TI5.3.5.6]
					// unable to pressurize to 50 or 100 cmH2O
					// Major Failure, Condition 8
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_8);
				}
				// $[TI5.3.5.7]
				// implied else

				if (highPressureCrossCheckFailed)
				{
				// $[TI5.3.5.8]
					// cross-check failed at 50 or 100 cmH2O
					// Major Failure, Condition 7
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_7);
				}
				// $[TI5.3.5.9]
				// implied else
			}
			// $[TI5.3.6]
			// implied else: insp and/or exh autozero solenoid test failed
		}
		// end else

	   	RSmExhValveController.open();
	}
	// $[TI6]
	// implied else: insp and/or exh pressure transducer 0 cmH2O calibration
	// failed
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: zeroTest_
//
//@ Interface-Description
//	This method sets the 0 cmH2O offset for a pressure sensor and tests if
//	it is within spec.  The sensor side to be zeroed is passed as an argument.
//	Returns TRUE if zero offset is in spec, FALSE if not.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Take 8-sample average of pressure sensor DAC count.
//	If average count falls within spec, save count as zero offset and
//	return TRUE, else return FALSE	
//-----------------------------------------------------------------------
//@ PreCondition
//	Argument (sensor side to be zeroed) must be DELIVERY or EXHALATION.
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Boolean
CircuitPressureTest::zeroTest_(MeasurementSide side)
{
	CALL_TRACE("CircuitPressureTest::zeroTest_(MeasurementSide side)");
	
	CLASS_PRE_CONDITION( side == DELIVERY || side == EXHALATION );

	Boolean testPassed;
	Uint16 zeroCount;
	const Uint32 NUM_SAMPLES_AVERAGED = 8;

	if (side == DELIVERY)
	{
	// $[TI1]
		RInspAutozeroSolenoid.open();
		Task::Delay (0, 40);

		// average samples
		Uint32 sumCount = 0;
		for (Uint32 ctr = 0; ctr < NUM_SAMPLES_AVERAGED; ctr++)
		{
		// $[TI1.1]
			sumCount += RInspPressureSensor.getCount();

			Task::Delay(0, SM_CYCLE_TIME);
		}
		// end for

		zeroCount = (Uint16) (sumCount / NUM_SAMPLES_AVERAGED);

		RInspAutozeroSolenoid.close();
		Task::Delay (0, 40);
	}
	else
	{
	// $[TI2]
		RExhAutozeroSolenoid.open();
	  	Task::Delay (0, 40);

		Uint32 sumCount = 0;
		for (Uint32 ctr = 0; ctr < NUM_SAMPLES_AVERAGED; ctr++)
		{
		// $[TI2.1]
			sumCount += RExhPressureSensor.getCount();

			Task::Delay(0, SM_CYCLE_TIME);
		}
		// end for

		zeroCount = (Uint16)(sumCount / NUM_SAMPLES_AVERAGED);

		RExhAutozeroSolenoid.close();
		Task::Delay (0, 40);
	}
	// end else

	RSmManager.sendTestPointToServiceData(zeroCount);

	if (zeroCount < MIN_ZERO_COUNT || zeroCount > MAX_ZERO_COUNT)
	{
	// $[TI3]
		testPassed = FALSE;
	}
	else
	{
	// $[TI4]
		testPassed = TRUE;

		if (side == DELIVERY)
		{
		// $[TI4.1]
			RInspPressureSensor.setZeroOffset (zeroCount);
		}
		else
		{
		// $[TI4.2]
			RExhPressureSensor.setZeroOffset (zeroCount);
		}
		// end else
	}
	// end else

	return( testPassed);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: pressurizeSystemTest_
//
//@ Interface-Description
//  This method tests if system can be pressurized within a specified
//	timeout period.  Target pressure is passed in as an argument.
//	Returns TRUE if system can pressurize, FALSE if not.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: exh valve and safety valve both closed.
//
//	Calculate the pressurization timeout.
//  Pressurize the system to the passed-in target pressure value.
//	If the system cannot pressurize within the calculated timeout, then
//	return FALSE, else return TRUE.
////-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Boolean
CircuitPressureTest::pressurizeSystemTest_ (Real32 targetPressure)
{
	CALL_TRACE("CircuitPressureTest::pressurizeSystemTest_ (\
												Real32 targetPressure)");
	
	Boolean testPassed;
	Uint32 timeoutMs;
	Real32 flowRate;
	
	// Calculate pressurization timeout
#if defined(SIGMA_UNIT_TEST)
	if (SmUnitTest::FakeSigmaState == STATE_SERVICE)
#elif defined(SM_TEST)
	if (FakeSigmaState == STATE_SERVICE)
#else
	if ( (TaskControlAgent::GetBdState() == STATE_SERVICE) )
#endif  // SIGMA_UNIT_TEST
	{
	// $[TI1]
		// Service mode
		flowRate = 5.0;
		timeoutMs = RSmUtilities.pressureTimeout( STATE_SERVICE,
												  targetPressure,
												  flowRate );
	}
	else
	{
	// $[TI2]
		// SST
		flowRate = 10.0;
		timeoutMs = RSmUtilities.pressureTimeout( STATE_SST,
												  targetPressure,
												  flowRate );
	}
	// end else

#ifdef SIGMA_UNIT_TEST
	CircuitPressureTest_UT::FlowRate = flowRate;
#endif  // SIGMA_UNIT_TEST

	RSmPressureController.establishPressure(
							targetPressure, DELIVERY, EITHER, flowRate);

	if (RSmPressureController.waitForPressure(timeoutMs))
	{
	// $[TI3]
		testPassed = TRUE;

		// NOTE: if pressurization passes, insp and exh pressure readings
		// are sent to the GUI in pressureTest_()
  	}
	else
	{
	// $[TI4]
		testPassed = FALSE;

		// If pressurization fails, send the current insp and exh pressures
		// to the GUI
		RSmManager.sendTestPointToServiceData(RInspPressureSensor.getValue());
		RSmManager.sendTestPointToServiceData(RExhPressureSensor.getValue());
	}
	// end else

  	RSmPressureController.stopPressureController();

// keep safety and exh valves closed for pressureTest_()

	return( testPassed);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: pressureTest_
//
//@ Interface-Description
//	This method compares pressure readings of the insp and exh pressure
//	sensors.  No arguments, returns TRUE if pressure difference is within
//	spec, else returns FALSE.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: system still pressurized from pressurizeSystemTest_().
//
//	Read insp and exh pressure sensors.
//	If the difference is within spec, return TRUE, else return FALSE.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Boolean
CircuitPressureTest::pressureTest_(void)
{
	CALL_TRACE("CircuitPressureTest::pressureTest_(void)") ;
	
	Boolean testPassed;

	// wait for pressure to settle
    Task::Delay (0, 200);
	
	RSmManager.signalTestPoint();

	Real32 inspPressure = RInspPressureSensor.getValue();
	Real32 exhPressure = RExhPressureSensor.getValue();

	RSmManager.sendTestPointToServiceData(inspPressure);
	RSmManager.sendTestPointToServiceData(exhPressure);

	if ( ABS_VALUE(inspPressure - exhPressure) >
					(inspPressure + exhPressure) * REL_PRESSURE_ERROR +
												2 * ABS_PRESSURE_ERROR )
	{
	// $[TI1]
		testPassed = FALSE;
	}
	else
	{
	// $[TI2]
		testPassed = TRUE;
	}
	// end else

	return( testPassed);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: openSolenoidTest_
//
//@ Interface-Description
//	This method activates the autozero solenoid of the specified sensor
//	to make sure the pressure sensor reads near 0 cmH2O when solenoid
//	is activated and system is holding a pressure.  Returns TRUE if
//	solenoid test passes, FALSE if not.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: system still pressurized from pressurizeSystemTest_().
//
//	Activate the appropriate autozero solenoid and delay to allow activation.
//	Read the appropriate pressure sensor.
//	Deactivate the solenoid and delay to allow deactivation.
//	If pressure reading is in range, return TRUE, else return FALSE.
//-----------------------------------------------------------------------
//@ PreCondition
//  Argument must indicate DELIVERY or EXHALATION.
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Boolean
CircuitPressureTest::openSolenoidTest_ (MeasurementSide side)
{
	CALL_TRACE("CircuitPressureTest::openSolenoidTest_ (MeasurementSide side)");

	CLASS_PRE_CONDITION (side == DELIVERY || side == EXHALATION);

	const Real32 ZERO_THRESHOLD = ABS_PRESSURE_ERROR + RESIDUAL_PRESSURE;
	Real32 pressure;
	Boolean testPassed;

	if (side == DELIVERY)
	{
	// $[TI1]
	  	RInspAutozeroSolenoid.open();
	  	Task::Delay (0, 40);

		pressure = RInspPressureSensor.getValue();

	  	RInspAutozeroSolenoid.close();
	  	Task::Delay (0, 40);
	}
	else
	{
	// $[TI2]
  		RExhAutozeroSolenoid.open();
	  	Task::Delay (0, 40);
		
		pressure = RExhPressureSensor.getValue();

  		RExhAutozeroSolenoid.close();
	  	Task::Delay (0, 40);
	}
	// end else

	RSmManager.sendTestPointToServiceData(pressure);

	if ( ABS_VALUE(pressure) > ZERO_THRESHOLD )
	{
	// $[TI3]
		testPassed = FALSE;
	}
	else
	{
	// $[TI4]
		testPassed = TRUE;
	}
	// end else

	return( testPassed);
}
