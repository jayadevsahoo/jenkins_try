//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file DownloadSWDlg.h
//----------------------------------------------------------------------------


#pragma once
#include "afxcmn.h"


// CDownloadSWDlg dialog

class CDownloadSWDlg : public CDialog
{
	DECLARE_DYNAMIC(CDownloadSWDlg)

public:
	CDownloadSWDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDownloadSWDlg();

// Dialog Data
	enum { IDD = IDD_DOWNLOAD_SW_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonDwnBd();

	static UINT DownloadThread(LPVOID lp);
	


	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CProgressCtrl m_Progress_bar_BD;
	bool CreatePowerPCFile(CString strTempfile, CString strLoadFile, CString strIPAdd);
	CString m_strBD_IP;
	afx_msg void OnBnClickedButtonDwnGui();
	CString m_strGUI_IP;
	CProgressCtrl m_Progress_bar_GUI;
	afx_msg void OnBnClickedButtonBrowseBd();
	afx_msg void OnBnClickedButtonBrowseGui();
	afx_msg void OnBnClickedButtonDwnSw();
	afx_msg void OnBnClickedDwnBd();
	afx_msg void OnBnClickedDwnGui();

	void RefreshStatus();

	CString m_strBD_log;
	CString m_strGUI_log;
	afx_msg void OnBnClickedBdShell();
	afx_msg void OnBnClickedButtonDwnSw1();
	afx_msg void OnBnClickedLoadLastBd();
	afx_msg void OnBnClickedLoadLastGui();
	afx_msg void OnBnClickedCancel();
	void CreateTempFile(bool bBD);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonNovram();

	void CreateNovRamFiles();
	afx_msg void OnBnClickedButtonBurnBd();
	void CreateBurnFileBD(CString strBIN);
	void CreateBurnFileGUI(CString strBIN);
	void CreateBurnFilesOther();
	afx_msg void OnBnClickedButtonBurnGui();
};
