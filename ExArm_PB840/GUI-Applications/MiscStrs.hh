#ifndef MiscStrs_HH
#define MiscStrs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MiscStrs - Miscellaneous language-independent string repository
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MiscStrs.hhv   25.0.4.0   19 Nov 2013 14:08:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 108   By: rhj   Date: 05-Apr-2011    SCR Number: 6759
//  Project:  PROX
//  Description:
//       Added PROX_TOUCHABLE_SPONT_FV_RATIO_LABEL, 
//             PROX_TOUCHABLE_SPONT_FV_RATIO_MSG, 
//             PROX_TOUCHABLE_SPONT_MINUTE_VOL_LABEL,
//             PROX_TOUCHABLE_SPONT_MINUTE_VOL_MSG
// 
//  Revision: 107   By: rhj   Date: 13-Sept-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added flow results during leak test.  
//  
//  Revision: 106  By: rhj    Date: 10-Sept-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//       Added PROX_STARTUP_SMALL_MSG and PROX_STARTUP_LARGE_MSG
//
//  Revision: 105  By: rhj    Date: 10-Aug-2010   SCR Number: 6638
//  Project:  PROX
//  Description:
//       Added PROX_NOT_AVAILABLE_MSG
//
//  Revision: 104   By: rhj   Date: 16-July-2010  SCR Number: 6606
//  Project:  PROX
//  Description:
//		Added PROX_AUTO_ZERO_STATUS_MSG & PROX_PURGE_STATUS_MSG
// 
//  Revision: 103   By: rhj   Date: 16-July-2010  SCR Number: 6601
//  Project:  PROX
//  Description:
//		Added PROX_ERROR_MESSAGE_TITLE1 & PROX_ERROR_MESSAGE_TITLE2
// 
//  Revision: 102   By: mnr   Date: 04-May-2010  SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added more strings for Prox SST Pressure cross check test.
//
//  Revision: 101   By: mnr   Date: 03-May-2010  SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added PX_DISTAL_INSP_PRESSURE_TITLE and PX_WYE_PRESSURE_TITLE.
//	
//  Revision: 100 By: mnr   Date: 16-APR-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added/Updated strings for PROX SST.
//
//  Revision: 099  By: mnr   Date: 13-APR-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added more strings for PROX SST.
//
//  Revision: 098  By: rhj   Date: 22-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added PF symbol for prox flow.
//
//  Revision: 097  By: mnr   Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      More PROX related strings added.
//
//  Revision: 096  By: mnr   Date: 17-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      PROX related strings added.
//
//  Revision: 095   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 094   By: mnr    Date: 23-Feb-2010    SCR Number: 6556
//  Project:  NEO
//  Description:
//      PHILIPS_VALUE_TITLE string added.
// 
//  Revision: 093   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      New Software Options strings added.
//
//  Revision: 092   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      New Preset and Event strings added.
//
//  Revision: 091  By: gdc    Date:  18-Aug-2009   SCR Number: 6417
//  Project:  XB
//  Description:
//     Added Safety-Net disgnostic messages for revised BD monitor.
//
//  Revision: 090  By:  rhj    Date:  14-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added DLOG_EST_SINGLE_TEST_INITIATED and DLOG_TEST_ALL_TESTS_REQUIRED. 
//
//  Revision: 089  By:  rhj    Date:  04-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//		Added single test strings for EST. 
//
//  Revision: 088  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 087  By:  gdc    Date:  15-Jan-2009    SCR Number: 6311
//  Project:  840S
//  Description:
//	Removed maneuver button 'help' messages.
//
//  Revision: 086  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
// 
//  Revision: 085   By: rpr    Date: 11-Nov-2008    SCR Number: 6345
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 084   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 083   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 083   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 082   By: rhj   Date:  20-Jul-2007   SCR Number: 6383
//  Project:  Trend
//  Description:
//      Added Expiratory/Inspiratory Pause Event messages.
//
//  Revision: 081   By: gdc   Date:  20-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//        Added Trending related strings.
//
//  Revision: 080   By: rhj   Date:  20-Feb-2007    SCR Number: 6342
//  Project:  RESPM
//  Description:
//        Fixed minor problems with RM strings.
//
//  Revision: 079   By: rhj   Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//        Removed C20/C.
//
//  Revision: 078   By: rhj   Date:  29-Nov-2006    SCR Number: 6318
//  Project:  RESPM
//  Description:
//       Added RM pause pending and pause active messages.
//      
//  Revision: 077   By: rhj   Date:  05-May-2006    SCR Number: 6181
//  Project:  PAV4
//  Description:
//       Added Expiratory Sensitivity label (L/min).  
//
//  Revision: 076   By: rhj   Date:  27-Oct-2005    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       RESPM Project related changes. 
//   
//  Revision: 075   By: gdc   Date:  27-May-2005    SCR Number: 6170
//  Project:  NIV2
//  Description:
//       Removed Insp Too Long alarm for NIV. Added High Ti spont alert.
// 
//  Revision: 074   By: gdc   Date:  15-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//       added strings to support NIV
// 
//  Revision: 073   By: emccoy Date: 27-July-2004   SCR Number: 6138
//  Project:  PAV3
//  Description:
//       added support for ft/vte/kg
//
//  Revision: 072   By: erm   Date:  02-Jun-2003    DR Number: 6058
//  Project:  AVER
//  Description:
//      Added new string for SpaceLab monitor
//
//  Revision: 071   By: jja   Date:  28-May-2002    DR Number: 6010
//  Project:  VCP
//  Description:
//      Remove unused strings containing VALUE_TITLE and VALUE_CHANGED_TITLE
//
//  Revision: 070   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//      Add new string For Shadow Trace button  
//
//  Revision: 069   By: heatherw   Date:  07-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added the following strings to be used with Vti Alarm
//         TOUCHABLE_HIGH_INSP_MAND_TIDAL_VOL_MSG   
//
//  Revision: 068   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 067   By: heatherw   Date:  04-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added the following strings to be used with the different
//         combinations of VC+ MandType for new Vti Mand alarm.       
//         TOUCHABLE_INSP_MAND_TIDAL_VOL_LABEL
//         TOUCHABLE_INSP_MAND_TIDAL_VOL_UNIT;
//         TOUCHABLE_INSP_MAND_TIDAL_VOL_MSG 
//         TOUCHABLE_INSP_TIDAL_VOL_LABEL; 
//         TOUCHABLE_INSP_TIDAL_VOL_UNIT;
//         TOUCHABLE_INSP_TIDAL_VOL_MSG 
//         
//         HIGH_INH_SPONT_TIDAL_VOL_BUTTON_TITLE_SMALL;  
//         HIGH_INH_MAND_TIDAL_VOL_BUTTON_TITLE_SMALL;  
//         HIGH_INH_TIDAL_VOL_BUTTON_TITLE_SMALL;  
//         
//         INH_MAND_TIDAL_VOL_SLIDER_MIN_LABEL;  
//         INH_TIDAL_VOL_SLIDER_MIN_LABEL;
//
//         HIGH_INH_SPONT_VOL_HELP_MESSAGE;
//         HIGH_INH_MAND_VOL_HELP_MESSAGE;    
//
//  Revision: 066   By: quf    Date: 23-Jan-2002    DR Number: 5984
//  Project:  GUIComms
//  Description:
//	Added new strings for Serial Loopback Test.
//
//  Revision: 065   By: quf    Date: 10-Oct-2001    DR Number: 5962
//  Project:  GUIComms
//  Description:
//	Removed O2_SENSOR_DISABLED_MESSAGE and UPPER_SOFTWARE_SALES_LABEL.
//
//  Revision: 064   By: quf    Date: 17-Sep-2001    DR Number: 5943
//  Project:  GUIComms
//  Description:
//	Removed strings NONE_VALUE_TITLE and NONE_VALUE_CHANGED_TITLE.
//
//  Revision: 063   By: quf    Date: 13-Sep-2001    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Print implementation changes:
//	- Removed print configuration strings.
//	- Removed "CAPTURING PRINT DATA" string.
//
//  Revision: 062   By: sah   Date:  31-Oct-2000    DR Number: 5780
//  Project:  VTPC
//  Description:
//      Added new 'SST_NEO_FILTER_TEST_DATA_LABEL' to be used with a
//      NEONATAL run of SST.
//
//  Revision: 061   By: hct    Date: 09-OCT-2000    DCS Number:  5493
//  Project:  GuiComms
//  Description:
//      Added None title for ComPortConfigValue.
//
//  Revision: 060   By: gdc    Date: 20-Sep-2000    DR Number: 5769
//  Project:  GuiComms
//  Description:
//		Added 38400 baud strings.
//
//  Revision: 059   By: hct    Date: 14-FEB-2000    DR Number: 5493
//  Project:  GUIComms
//  Description:
//		Incorporated initial specifications for GUIComms Project.
//	NOTE: out of date sequence due to GuiComms, VC+ parallel development, 
//		and remerge process.  JJA 07 may 2002
//
//  Revision: 058  By: gdc	Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 057   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added new PAV-based compliance, resistance, elastance and work-
//         of-breathing index
//      *  added new PA spontaneous type value
//
//  Revision: 056  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new SPONT-based inspiratory time, inspiratory time ratio,
//         and rapid/shallow breathing index
//      *  added new VC+ mandatory type value
//      *  added new VS spontaneous type value
//      *  re-designed discrete setting value strings, to be formatted with
//         point-size and style at runtime; includes elimination of over
//         90 messages, renaming of messages, and reorganizing of this
//         file
//
//  Revision: 055  By: sah	Date: 18-Apr-2000   DCS Number:  5705
//  Project:  NeoMode
//  Description:
//      Added two new strings with the "above PEEP" phrase, but
//      formatted to be used as an auxillary title in a setting button.
//
//  Revision: 054  By: sah	Date: 11-Apr-2000   DCS Number:  5691
//  Project:  NeoMode
//  Description:
//      Renamed 'TOUCHABLE_MEAN_CIRC_*_UNIT' to 'TOUCHABLE_PRESSURE_*_UNIT', to
//      reduce confusion.  Grouped like ids with each other.
//
//  Revision: 053  By: sah	Date: 08-Mar-2000   DCS Number:  5683
//  Project:  NeoMode
//  Description:
//      Re-organized and re-named the Vent Config strings for more consistency.
//      Also, removed unneeded strings.
//
//  Revision: 052  By: hhd		Date: 17-Nov-1999   DCS Number:  5412
//  Project:  NeoMode
//  Description:
//		Added HUMID_VOLUME_VERIFY_SETTINGS_MESSAGE.
//
//  Revision: 051  By: btray    Date: 27-Jul-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Improvements made for display of data in Ventilator Configuration Subscreen. 
//
//  Revision: 050  By: btray    Date: 20-Jul-1999   DCS Number:  5424
//  Project:  NeoMode
//  Description:
//      Added strings for enabling software options from Development
//      Options Subscreen. 
//
//  Revision: 049  By: sah    Date: 30-Jun-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Initial NeoMode Project implementation added:
//      *  added new, neonatal-specific strings
//      *  merged units into VPDA help messages
//
//  Revision: 048   By: yyy      Date: 08/18/99         DR Number:   5513
//  Project:  ATC
//  Description:
//      Added new message for when alarm silence is active.
//
//  Revision: 047  By: hhd    Date:  21-Jun-1999   DR Number: 5376
//  Project:  ATC
//  Description:
//     Added new entry for Battery background error. 
//
//  Revision: 046  By: hhd    Date: 4-June-1999  DCS Number:  5418
//  Project:  ATC
//  Description:
//      Added 1 new string per DCS 5418.  Deleted a bunch of strings due to
//      code cleanup.
//
//  Revision: 045  By: hhd    Date: 27-May-1999  DCS Number:  5385
//  Project:  ATC
//  Description:
//      Added new strings PATIENT_TUBE_TYPE_BUTTON_TITLE3_SMALL and
//      TUBE_ID_BUTTON_TITLE3_SMALL.
//      Also, added SST_DATE_TIMESTAMP_LINE1, SST_DATE_TIMESTAMP_LINE2.  
//
//  Revision: 044  By: hhd    Date: 21-May-1999  DCS Number:  5391
//  Project:  ATC
//  Description:
//      Removed string STATUS_CCT_CAL_HUMID_CHANGED_WARNING.
//
//  Revision: 043  By: hhd    Date: 06-May-1999  DCS Number: 5381
//  Project:  ATC
//  Description:
//      Added TC_MM_UNITS_SMALL_2; Changed font style to Bold for 2 strings.
//
//  Revision: 042  By: hhd    Date: 05-May-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//      Added 'TUBE_TYPE_ET_TEXT' and 'TUBE_TYPE_TRACH_TEXT'. 
//
//  Revision: 041  By: sah    Date: 29-Apr-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//      Added 'VERIFY_SETTINGS_MESSAGE' and 'SAME_PATIENT_VERIFY_MESSAGE'
//      strings for use of the new verify-needed mechanism.
//
//  Revision: 040  By: yyy    Date: 16-Mar-1999  DCS Number: 5345
//  Project:  ATC
//  Description:
//	  Added warning message when exp. pause pressure is not stable.
//	  Added warning message when pause is canceled by alarm or patient
//	  trigger.
//
//  Revision: 039  By: hct        Date: 18-Feb-1999  DCS Number: 5322 
//  Project: ATC 
//  Description:
//      Added HIGH Pcomp misc string.
//
//  Revision: 038  By: hhd    Date: 03-Feb-1999  DCS Number: 5322
//  Project: ATC
//  Description:
//      Initial ATC revision
//      Added new string TOUCHABLE_HIGH_INSP_SPONT_TIDAL_VOL_MSG 
//
//  Revision: 037  By: dosman    Date: 02-Aug-1998  DCS Number: 5322
//  Project: ATC
//  Description:
//      Added BILEVEL_OPTIONS_LABEL 
//      as part of new access to string of software options
//      for manufacturing test.
//
//  Revision: 036  By: hhd    Date:  21-Jun-1999   DR Number: 5376
//  Project:  840 Cost-Reduce
//  Description:
//     Added new entry for Battery background error. 
//
//  Revision: 035  By: hct    Date: 20-Nov-1998  DCS Number: 5233
//  Project: BiLevel
//  Description:
//		Added messages for Spontaneous Inspired Tidal Volume
//
//  Revision: 034  By: syw    Date: 19-Nov-1998  DCS Number: 5218
//  Project: BiLevel
//  Description:
//		Added SER_NUM_TROUBLE_SHOOTING_BUTTON_TITLE3
//
//  Revision: 033  By: yyy    Date: 3-Nov-1998  DCS Number: 5256
//  Project: BiLevel (R8027)
//  Description:
//	  Removed unused compliance and resistance error titles
//	  Fixed per qual test results.
//
//  Revision: 032  By: syw      Date: 28-Oct-1998  DR Number: 5215
//    Project:  BiLevel
//    Description:
//	  	Changed CONFIGURATION_CODE_LABEL to VENTILATOR_OPTIONS_LABEL
//
//  Revision: 031  By: btray    Date: 28-Oct-1998  DCS Number: 5225
//  Project: BiLevel (R8027)
//  Description:
//	Removed unused MANEUVER_BUTTON_TITLE
//
//  Revision: 030  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/MiscStrs.hhv   1.169.1.0   07/30/98 10:16:12   gdc
//	   Added CONFIGURATION_CODE_LABEL, SOFTWARE_SALES_LABEL.
//
//  Revision: 029  By:  syw    Date: 27-Jul-1998    DR Number: DCS 5082
//       Project:  Sigma (840)
//       Description:
//            Added FS_NOT_COMPATIBLE_WITH_SOFTWARE
//
//  Revision: 028  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 027  By:  sah	Date:  08-May-1998    DR Number:  5041
//  Project:  Sigma (R8027)
//  Description:
//      Added two new system event codes, for tracking NOVRAM failures.
//
//  Revision: 026  By:  sah    Date:  15-Jan-1998    DR Number: 5004
//  Project:  Sigma (R8027)
//  Description:
//      Added 'SCROLLBAR_UP_ARROW' and 'SCROLLBAR_DN_ARROW', to eliminate
//      their corresponding Cheap Text strings from being defined within
//      the 'Scrollbar' class.
//
//  Revision: 025  By:  hhd		Date: 22-Oct-1997    DR Number: DCS 2288 
//       Project:  Sigma (840)
//       Description:
//			Restored SV_WYE_NOT_BLOCKED.
//
//  Revision: 024  By:  yyy    Date: 10-Oct-1997    DR Number: DCS 2452
//       Project:  Sigma (840)
//       Description:
//			Changed due to ExhValveCalibration modification.
//
//  Revision: 023	By:  hhd    Date: 09-OCT-1997    DR Number:   2288
//    Project:  Sigma (R8027)
//    Description:
//      Removed 'SV_WYE_NOT_BLOCKED' string ID (still in despite revision 18).
//		
//  Revision: 022  By:  yyy    Date:  08-Oct-97    DR Number: DCS 2203
//    Project:  Sigma (840)
//    Description:
//      Updated data for new BD audio test procedure.
//
//  Revision: 021  By:  yyy    Date:  08-Oct-1997    DR Number:   1865
//    Project:  Sigma (R8027)
//    Description:
//      Vent inop test error message cleanup.
//
//  Revision: 020  By:  yyy    Date:  01-Oct-97    DR Number: DCS 2204
//    Project:  Sigma (840)
//    Description:
//      Updated data label/error msg for battery test.
//
//  Revision: 019  By:  sah    Date:  30-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Replaced the system event message for a diagnostic log clearance,
//      with an event message for a task-control transition timeout.
//
//  Revision: 018  By:  hhd    Date:  30-SEP-1997    DR Number:   2288
//    Project:  Sigma (R8027)
//    Description:
//      Removed 'SV_WYE_NOT_BLOCKED' string ID, because it is not used.
//
//  Revision: 017  By:  yyy    Date:  25-SEP-1997    DR Number:   2410
//    Project:  Sigma (R8027)
//    Description:
//      Added Pressure transducer calibration related miscStrs.
//
//  Revision: 016  By:  yyy    Date:  24-SEP-1997    DR Number:   1861
//    Project:  Sigma (R8027)
//    Description:
//      Display more clear diagnostic information for all types of calibration
//		tests and vent inop test.
//
//  Revision: 015  By:  yyy    Date:  10-Sep-97    DR Number: 1923
//    Project:  Sigma (R8027)
//    Description:
//      Changed the name of communications Diagnostic log to the system
//		information log.
//
//  Revision: 014  By:  yyy    Date:  08-Sep-97    DR Number: 2439
//    Project:  Sigma (R8027)
//    Description:
//      Changed ML_CM_H2O_LABEL to ML_LABEL.
//
//  Revision: 013  By:  yyy    Date:  06-Aug-97    DR Number: 2130
//    Project:  Sigma (R8027)
//    Description:
//      Added SV_WYE_NOT_BLOCKED string.
//
//  Revision: 012  By:  yyy    Date:  30-JUL-97    DR Number: 2313, 2279
//    Project:  Sigma (R8027)
//    Description:
//      30-JUL-97 Added SM_FLOW_SENSOR_CAL_BUTTON_TITLE, and SM_TAU_CALIBRATION_BUTTON_TITLE
//		FLOW_SENSOR_CALIBRATION_SUBSCREEN_TITLE, TAU_CALIB_SUBSCREEN_TITLE,
//		TEMP_CHANGE_TOO_SMALL, TEMP_INCREASING, CANNOT_ACHIEVE_MIN_AIR_FLOW,
//		CANNOT_ACHIEVE_MIN_O2_FLOW, AIR_TAU_OOR, O2_TAU_OOR, AIR_OFFSET_OOR,
//		O2_OFFSET_OOR new strings.
//		13-AUG-97  Removed all references for TauCharacterization
//
//  Revision: 011  By:  yyy    Date:  30-JUL-97    DR Number: 2205
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for exh valve velocity transducer test.
//
//  Revision: 010  By:  yyy    Date:  30-JUL-97    DR Number: 2013
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for Nurse Call test.
//
//  Revision: 009  By:  yyy    Date:  30-JUL-97    DR Number: 2216
//    Project:  Sigma (R8027)
//    Description:
//      Added SV_LOW_EXPIRATORY_FILTER_DELTA_P new string.
//
//  Revision: 008  By:  yyy    Date:  22-JUNE-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      Renamed waveformsSubScreen related help message for translation.
//
//  Revision: 007  By:  hhd    Date:  25-JUNE-97    DR Number: 2233
//    Project:  Sigma (R8027)
//    Description:
//      Added BK_DATAKEY_SIZE_ERROR.
//
//  Revision: 006  By:  yyy    Date:  11-Jun-97    DR Number: 1988
//    Project:  Sigma (R8027)
//    Description:
//      Added EST_TEST_RESULT_HPA_UNIT, TITLE_CMH20_UNIT, TITLE_HPA_UNIT
//        and removed CALIB_SUB_SCREEN_TITLE
//
//  Revision: 005  By:  yyy    Date:  27-MAY-97    DR Number: 2042
//    Project:  Sigma (R8027)
//    Description:
//      Removed SV_SPEAKER_DISCONNECTED and SV_RESONANCE_TEST, changed
//        SV_SELF_TEST_FAILED to SV_SAAS_TEST_FAILED.
//
//  Revision: 004  By:  hhd    Date:  13-MAY-97    DR Number: 2085
//    Project:  Sigma (R8027)
//    Description:
//      Added DLOG_NMI_SHORT and DLOG_EXCEPTION_VECTOR.
//
//  Revision: 003  By: yyy      Date: 07-May-1997  DR Number: 2059
//    Project:  Sigma (R8027)
//    Description:
//      Removed unused message strings.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2004
//    Project:  Sigma (R8027)
//    Description:
//      Changed BK_COMPR_CHECKSUM to BK_COMPR_BAD_DATA.  Added BK_COMPR_UPDATE_SN,
//        and BK_COMPR_UPDATE_PM_HRS error strings.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//    Project:  Sigma (R8027)
//    Description:
//             Integration baseline.
//====================================================================

#include "GuiAppClassIds.hh"

class MiscStrs
{
public:
#include "MiscStrsDecl.hh"

private:
    // these methods are purposely declared, but not implemented...
    MiscStrs(void);                        // not implemented...
    ~MiscStrs(void);                    // not implemented...
    MiscStrs(const MiscStrs&);            // not implemented...
    void operator=(const MiscStrs&);    // not implemented...
};

#ifndef DEFINE_STRS
#include "GuiApp.hh"
#include "MiscStrsDef.hh"
#endif

#endif // MiscStrs_HH 

