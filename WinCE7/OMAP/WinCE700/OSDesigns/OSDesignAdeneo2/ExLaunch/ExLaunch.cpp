//*******************************************************************************
//
//  ExStartup.cpp

/////////////////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "winreg.h"
#include "resource.h"
#include "UtilityFunctions.h"
#include "AioDio.h"
#include "bsp_version.h"

static const int MAX_REGISTRY_LINE_SIZE = 250;

static const int MAX_MESSAGE_SIZE = 500;

static const int MAX_ERROR_CODE_SIZE = 6;

#define SPECIAL_BUTTON_COMB0	0xF5

#define SYSTEM_RESTART_EXE                          _T("\\Windows\\Restart.exe")
#define TECH_REG_FILE_NAME                          _T("\\Windows\\Tech.Reg")
#define CE_SHELL_EXE                                _T("\\Windows\\Explorer.exe")
#define NEWPORT_ID_FILE                             _T("\\Hard Disk\\NEWPORT.ID")
#define DOWNLOAD_SOURCE_DIRECTORY                   _T("\\Hard Disk\\")
#define EXSTARTUP_APPLICATION_FILE_NAME             _T("\\FlashFX Disk\\Startup\\ExStartup.exe")
#define EXSTARTUP_APPLICATION_CRC_FILE_NAME         _T("\\FlashFX Disk\\CrcFiles\\ExStartup.crc")
#define EXSTARTUP_APPLICATION_BACKUP_FILE_NAME      _T("\\FlashFX Disk2\\Startup\\ExStartup.exe")
#define EXSTARTUP_APPLICATION_BACKUP_CRC_FILE_NAME  _T("\\FlashFX Disk2\\CrcFiles\\ExStartup.crc")
#define EXSTARTUP_NEW_FILE_NAME						_T("\\FlashFX Disk\\Startup\\New\\ExStartup.exe")				

#define RK_PLATFORM_STARTUPDIR		                (TEXT("PLATFORM\\STARTUPDIRS"))
#define RV_FLASH_STARTUP_FOLDER		                (TEXT("StartupFolder1"))
#define RV_USB_STARTUP_FOLDER		                (TEXT("StartupFolder2"))
#define RV_SD_STARTUP_FOLDER		                (TEXT("StartupFolder3"))

// function prototypes
BOOL ExecuteFile(TCHAR *FileName, BOOL bWait);
BOOL IsCabFile(TCHAR *FileName);
BOOL IsVBFile(TCHAR *FileName);
BOOL RegisterFile(TCHAR *FileName);
BOOL ExecuteFile(TCHAR *FileName);
void GetCurrentDirectory(TCHAR *szDirectory);

// global variables
TCHAR	CopyPath[MAX_PATH];

BOOL CALLBACK MainWndProc(HWND hWnd, UINT wMsg, WPARAM wParam, LPARAM lParam)
{
    switch(wMsg)
    {
        default: break;

        // message handler for 3 ventilator model buttons' clicking event 
        case WM_COMMAND:
            
            switch(wParam)
            {
                default: break;
                case  IDOK:                       
                    PostQuitMessage(0); // send WM_QUIT message to exit the message loop
                    break;
            }
    }
	return FALSE;
}

//******************************************************************
//
//  ConvertStringToTchar - Convert ascii string to a TCHAR line.
//
//******************************************************************

void ConvertStringToTchar(char *Ascii, TCHAR *TChar)
{
    int Length = strlen(Ascii);

    for (int i=0; i<Length; i++)
    {
        TCHAR NewTChar = 0;
        NewTChar |= Ascii[i];
        TChar[i] = NewTChar;
    }
    TChar[Length] = 0;
}

//******************************************************************
//
//  SplitPath - Split a full file path name into its components.
//
//  The size_t parameters are the number of TCHAR elements in each
//  returned array.
//
//******************************************************************

BOOL SplitPath(LPCTSTR FullFilePathName,                                // Input filename with full path
               TCHAR *Dir, size_t DirSize,                              // Returned directory path. Input size limit on returned array.
               TCHAR *NameNoPath, size_t NameNoPathSize,                // Returned file name, no path, including the ext. Input size limit on returned array.
               TCHAR *BaseName, size_t BaseNameSize,                    // Returned file name, no path, excluding the ext. Input size limit on returned array.
               TCHAR *Ext, size_t ExtSize)                              // Returned ext. Input size limit on returned array.
{
    // If the name is empty, get out
    if (_tcslen(FullFilePathName) == 0)
    {
        return FALSE;
    }

    // find the beginning of the basename by finding the last '\'
    TCHAR *Temp = (TCHAR *)FullFilePathName;
    TCHAR *BeginBaseName = (TCHAR *)FullFilePathName;
    while ((Temp = _tcsstr(Temp, _T("\\"))) != 0)
    {
        Temp++;
        BeginBaseName = Temp;
    }

    // Return base name, period and ext (all one string)
    _tcsncpy(NameNoPath, BeginBaseName, NameNoPathSize-1);              // return name, including ext, to the caller
    NameNoPath[NameNoPathSize-1] = '\0';                                // make sure it's a valid string

    // Return directory path
    if (BeginBaseName == FullFilePathName)                              // no path, it's only a file name
    {
        _tcscpy(Dir, _T(""));                                           // Return empty string
    }
    else
    {
        _tcsncpy(Dir, FullFilePathName, BeginBaseName - FullFilePathName);  // return dir, up to the last \, to the caller
        Dir[BeginBaseName - FullFilePathName] = '\0';                   // make sure it's a valid string
    }

    // find the '.' to locate the ext and to delimit the base filename
    TCHAR *BeginExt;
    Temp = _tcsstr(BeginBaseName, _T("."));                             // Temp points to the period (if there)

    // Return base file name and ext as separate strings
    if (Temp)                                                           // was there a period?
    {
        BeginExt = Temp + 1;

        _tcsncpy(BaseName, BeginBaseName, Temp - BeginBaseName);        // return name, up to the period, to the caller
        BaseName[Temp - BeginBaseName] = '\0';                          // make sure it's a valid string

        _tcsncpy(Ext, BeginExt, ExtSize-1);                             // return name, no ext, to the caller
        Ext[ExtSize-1] = '\0';                                          // make sure it's a valid string
    }
    else                                                                // no period or ext
    {
        _tcsncpy(BaseName, BeginBaseName, BaseNameSize-1);              // return name, no ext, to the caller
        BaseName[BaseNameSize-1] = '\0';                                // make sure it's a valid string
        _tcscpy(Ext, _T(""));                                           // Empty ext
    }

    return TRUE;
}



//******************************************************************
//
//  FileExists - Return TRUE if the file exists, FALSE if not.
//
//******************************************************************

BOOL FileExists(LPCTSTR FileName)
{
    DWORD Attributes = GetFileAttributes(FileName);

    if ((Attributes == INVALID_FILE_ATTRIBUTES) && ((GetLastError() == ERROR_PATH_NOT_FOUND) || (GetLastError() == ERROR_FILE_NOT_FOUND)))
    {
        return FALSE;
    }

    return TRUE;
}


//******************************************************************
//
//  VerifyDirectory -
//          Ensure target directory exists. If it doesn't, create it.
//
//******************************************************************

void VerifyDirectory(TCHAR *filename)
{
	TCHAR dirname[MAX_PATH];
	TCHAR *pstart = filename;
	TCHAR *pend = _tcsstr(filename, _T("\\"));
	DWORD attr = 0;

	if(_tcslen(filename) <= 1) return;

	if(!(pend - pstart > 0))
	{ // doesn't start with a backslash
		_tcscpy(dirname, _T("\\"));
		pstart++;
	}

	while(true)
	{
		pend = _tcsstr(pstart, _T("\\"));

        if ((unsigned)(pend - pstart) > _tcslen(pstart))
		{  // last directory
			_tcscat(dirname, filename);
            if (! _tcsstr(dirname, _T(".")))
			{
                if (!FileExists(dirname))
				{
					RETAILMSG(TRUE, (_T("Creating directory: %s\r\n"), dirname));
					CreateDirectory(dirname, NULL);
				}
			}
			break;
		}
		else
		{
			_tcsncpy(dirname, filename, (pend - filename));
			dirname[pend - filename] = '\0';
            if (!FileExists(dirname))
			{
				RETAILMSG(TRUE, (_T("Creating directory: %s\r\n"), dirname));
				CreateDirectory(dirname, NULL);
			}
			_tcscpy(dirname, _T("\\"));
			pstart = pend;
			pstart++;
		}
	}
}




//******************************************************************
//
//
//
//******************************************************************

BOOL ExecuteFile(TCHAR *FileName, BOOL waitForCompletion)
{
	PROCESS_INFORMATION pi;

	if(IsCabFile(FileName))
	{
		RETAILMSG(TRUE, (_T("ADSCopy: installing CAB '%s'\r\n"), FileName));

		if(!CreateProcess(_T("\\Windows\\wceload.exe"), FileName, NULL, 0, FALSE, 0, 0, 0, NULL, &pi))
		{
			RETAILMSG(TRUE, (_T("ADSCopy: install failed: %s\r\nGetLastError returned: %d\r\n"), FileName, GetLastError()));
			return FALSE;
		}
	}
    else
    {
        if(IsVBFile(FileName))
        {
            RETAILMSG(TRUE, (_T("ADSCopy: starting VB app '%s'\r\n"), FileName));

            if(!CreateProcess(_T("\\Windows\\pvbload.exe"), FileName, NULL, 0, FALSE, 0, 0, 0, NULL, &pi))
            {
                RETAILMSG(TRUE, (_T("ADSCopy: failed to run: %s\r\nGetLastError returned: %d\r\n"), FileName, GetLastError()));
                return FALSE;
            }
        }
        else
        {
            TCHAR *p;

            // check for command line params
            p = _tcsstr(FileName, _T(" "));
            if(p == NULL)
            {
                RETAILMSG(TRUE, (_T("ADSCopy: starting app '%s'\r\n"), FileName));

                if(!CreateProcess(FileName, NULL, NULL, 0, FALSE, 0, 0, 0, NULL, &pi))
                {
                    RETAILMSG(TRUE, (_T("ADSCopy: failed to run: %s\r\nGetLastError returned: %d\r\n"), FileName, GetLastError()));
                    return FALSE;
                }
            }
            else
            {  // a space exists in the path.
                TCHAR *q = NULL;
                TCHAR params[MAX_PATH];

                // determine if there are quotes, which delimit the path
                q = _tcsstr(FileName, _T("\""));

                if(q == NULL)
                {   // no quotes
                    //the space separates the path and cmd line params
                    // copy the params
                    _tcscpy(params, p + 1);

                    // crop from filename
                    *p = '\0';
                }
                else
                {   // section between quotes is the path

                    q++;
                    q = _tcsstr(q, _T("\""));

                    if(q == NULL)
                    {
                        RETAILMSG(TRUE, (_T("ADSCopy: Invalid INF line: missing double quote\r\n")));
                        return FALSE;
                    }

                    q+=2;

                    // copy the params
                    _tcscpy(params, q);

                    // crop the filename, strip quotes
                    q-=2;
                    *q = '\0';
                    q = FileName;
                    q++;
                    _tcscpy(FileName, q);
                }

                RETAILMSG(TRUE, (_T("ADSCopy: starting app '%s' with command line '%s'\r\n"), FileName, params));

                if(!CreateProcess(FileName, params, NULL, 0, FALSE, 0, 0, 0, NULL, &pi))
                {
                    RETAILMSG(TRUE, (_T("ADSCopy: failed to run: %s\r\nGetLastError returned: %d\r\n"), FileName, GetLastError()));
                    return FALSE;
                }
            }
        }
    }

    if (waitForCompletion)
	{
        RETAILMSG(TRUE, (_T("ExStartup: Waiting for completion\r\n")));
		WaitForSingleObject(pi.hProcess, INFINITE);
	}

	return TRUE;
}


//******************************************************************
//
//
//
//******************************************************************

BOOL IsCabFile(TCHAR *FileName)
{
	LPCTSTR ptrPos;
	// Check for existence of '.cab'
	ptrPos = _tcsstr(FileName, _T(".cab"));

	if (ptrPos == NULL)
	{
		// lowercase not found, check for upper case
		ptrPos = _tcsstr(FileName, _T(".CAB"));
	}

	return !(ptrPos == NULL);
}


//******************************************************************
//
//
//
//******************************************************************

BOOL IsVBFile(TCHAR *FileName)
{
	LPCTSTR ptrPos;

	// Check for existence of '.vb'
	ptrPos = _tcsstr(FileName, _T(".vb"));

	if (ptrPos == NULL)
	{
		// lowercase not found, check for upper case
		ptrPos = _tcsstr(FileName, _T(".VB"));
	}

	return !(ptrPos == NULL);
}


//******************************************************************
//
//
//
//******************************************************************

void GetCurrentDirectory(TCHAR *szDirectory)
{
	TCHAR *p, *p2;

	// get current directory
	GetModuleFileName(NULL, szDirectory, MAX_PATH);

	// trim off the exe name
	p = _tcsstr(szDirectory, _T("\\"));

	if(p == NULL)
	{
		// no slash found - return root?
		_tcscpy(szDirectory, _T("\\"));

		return;
	}

	while( p != NULL)
	{
		p2 = p + 1;
		p = _tcsstr(p2, _T("\\"));
	}

	// null terminate to crop
	p2--;
	*p2 = '\0';

	RETAILMSG(TRUE, (_T("GetCurrentDirectory = %s\r\n"), szDirectory));

	return;
}

//******************************************************************
//
//  GetNextLine - Read a line from the file stream. Strip out lines
//      that are empty or contain only comments. Return false
//      if end of file or error.
//
//******************************************************************

bool GetNextLine(char *Line, FILE *Stream)
{
    while (true)
    {
        // clear buffer
        strcpy(Line, "");

        // checks for EOF
        if(fgets(Line, MAX_REGISTRY_LINE_SIZE, Stream) == NULL)
            return false;

        //sanity check
        if(ferror(Stream))
        {
            RETAILMSG(TRUE, (_T("ExStartup: Error Reading REG file\r\nGetLastError returned: %d\r\n"), GetLastError()));
            fclose(Stream);
            return false;
        }

        // strip leading spaces
        while(Line[0] == ' ')
            strcpy(Line, &Line[1]);

        if(strlen(Line) == 0)
			continue;

        switch(Line[0])
		{
			case ';':	// lines starting with a ";" are comments
			case 13:	// ignore empty lines (just a \r)
			case 10:	// ignore empty lines (just a \n)
				continue;
		}

        break;                                          // we got a line to use, quit the loop
    }

    return true;
}



//******************************************************************
//
//  ParseNextValue - Read lines until we find a valid value setting.
//      The format of a value setting line is:
//          "Value"=type:value
//          Type may be dword, binary, or string (case insensitive).
//          The value if dword is a hex value, if a string, don't use quotes.
//
//      Return false if end of file, error, or end of key.
//
//******************************************************************

bool ParseNextValue(TCHAR *Value, DWORD *ValueType, BYTE *ValueSetting, DWORD *ValueLength, FILE *Stream)
{
    char Line[MAX_REGISTRY_LINE_SIZE];

    while (true)
    {

        if (!GetNextLine(Line, Stream))
        {
            return false;
        }

        if ((Line[0] == '[') &&                             // The end of subkeys is "[]"
            (Line[1] == ']'))
        {
            return false;
        }

        if (Line[0] != '"')                                 // The subkey must start with a double quote
        {
            continue;
        }

        strtok(&Line[1], "\"");                             // Force the subkey starting at Line[1] to be a string.
                                                            // This also sets up strtok to return the token just past the double quote,
                                                            // which must be the equal sign.

        char *EqualSign = strtok(0, ":");                   // Find the colon, this will point us at the equal sign
        if (!EqualSign)                                     // If the colon wasn't found, then we don't have a valid value setting line
        {
           continue;
        }

        ConvertStringToTchar(&Line[1], Value);              // Return the value, converted to TCHAR

        if (EqualSign[0] != '=')                            // The next thing on the line must be the equal sign
        {
            continue;
        }

        char *Type = &EqualSign[1];

        char *SettingString = strtok(0, " ;\r\n");          // Find anything that ends the line

        if (!SettingString)
        {
            continue;
        }

        if (_stricmp(Type, "dword") == 0)
        {
            DWORD ConvertedValue = strtol(SettingString, NULL, 16);
            ValueSetting[0] = (BYTE)ConvertedValue & 0xff;
            ValueSetting[1] = (BYTE)(ConvertedValue >> 8) & 0xff;
            ValueSetting[2] = (BYTE)(ConvertedValue >> 16) & 0xff;
            ValueSetting[3] = (BYTE)(ConvertedValue >> 24) & 0xff;
            *ValueLength = sizeof(DWORD);
            *ValueType = REG_DWORD;
            RETAILMSG(TRUE, (_T("ExLaunch: Value: %s=dword:%lx\r\n"), Value, ConvertedValue));
            return true;
        }

        if (_stricmp(Type, "binary") == 0)
        {
            DWORD ConvertedValue = strtol(SettingString, NULL, 2);
            ValueSetting[0] = (BYTE)ConvertedValue & 0xff;
            ValueSetting[1] = (BYTE)(ConvertedValue >> 8) & 0xff;
            ValueSetting[2] = (BYTE)(ConvertedValue >> 16) & 0xff;
            ValueSetting[3] = (BYTE)(ConvertedValue >> 24) & 0xff;
            *ValueLength = sizeof(DWORD);
            *ValueType = REG_BINARY;

            TCHAR TCharValueString[MAX_REGISTRY_LINE_SIZE];
            ConvertStringToTchar(SettingString, TCharValueString);             // Convert the value string to a TCHAR string
            RETAILMSG(TRUE, (_T("ExLaunch: Value: %s=binary:%s\r\n"), Value, TCharValueString));

            return true;
        }

        if (_stricmp(Type, "string") == 0)
        {

            TCHAR TCharSettingString[MAX_REGISTRY_LINE_SIZE];
            ConvertStringToTchar(SettingString, TCharSettingString);        // Convert the value string to a TCHAR string
            wcscpy((TCHAR *)ValueSetting, TCharSettingString);
            *ValueLength = (wcslen(TCharSettingString)+1) * sizeof(TCHAR);  // we need to return the size of ValueLength plus the null terminator, wcslen only gives the string length
            *ValueType = REG_SZ;
            RETAILMSG(TRUE, (_T("ExLaunch: Value: %s=string:%s\r\n"), Value, TCharSettingString));
            return true;
        }
    }
}


//******************************************************************
//
//  ParseNextKey - Read lines until we find a valid major HKEY,
//      followed by the subkey. Both the major HKEY and the subkey
//      must be enclosed in brackets. At least one sub key is required!
//
//      The major HKEY can be one of the following:
//          HKEY_CLASSES_ROOT
//          HKEY_CURRENT_USER
//          HKEY_LOCAL_MACHINE
//          HKEY_USERS
//
//      The subkey follows the major key, separated by a backslash.
//
//      Example: [HKEY_LOCAL_MACHINE\Drivers\BuiltIn\MemoryConfig]
//
//      Return false if end of file or error.
//
//******************************************************************

bool ParseNextKey(HKEY *HKey, TCHAR *SubKey, FILE *Stream)
{
    char Line[MAX_REGISTRY_LINE_SIZE];

    while (true)
    {
        if (!GetNextLine(Line, Stream))
        {
            return false;
        }

        if (!strtok(Line, "\\"))                                // The backslash forms the end of the major key
        {
            continue;
        }

        char *SubKeyString = strtok(NULL, "]");             // The bracket forms the end of the subkey
        if (!SubKeyString)
        {
            continue;
        }
        ConvertStringToTchar(SubKeyString, SubKey);         // Convert the subkey string to a TCHAR string

        // Verify the matching HKEY
        if (strcmp(Line, "[HKEY_CLASSES_ROOT") == 0)
        {
            RETAILMSG(TRUE, (_T("ExLaunch: Major Key: [HKEY_CLASSES_ROOT]\r\n")));
            RETAILMSG(TRUE, (_T("ExLaunch: Subkey: %s\r\n"), SubKey));
            *HKey = HKEY_CLASSES_ROOT;
            return true;
        }
        if (strcmp(Line, "[HKEY_CURRENT_USER") == 0)
        {
            RETAILMSG(TRUE, (_T("ExLaunch: Major Key: [HKEY_CURRENT_USER]\r\n")));
            RETAILMSG(TRUE, (_T("ExLaunch: Subkey: %s\r\n"), SubKey));
            *HKey = HKEY_CURRENT_USER;
            return true;
        }
        if (strcmp(Line, "[HKEY_LOCAL_MACHINE") == 0)
        {
            RETAILMSG(TRUE, (_T("ExLaunch: Major Key: [HKEY_LOCAL_MACHINE]\r\n")));
            RETAILMSG(TRUE, (_T("ExLaunch: Subkey: %s\r\n"), SubKey));
            *HKey = HKEY_LOCAL_MACHINE;
            return true;
        }
        if (strcmp(Line, "[HKEY_USERS") == 0)
        {
            RETAILMSG(TRUE, (_T("ExLaunch: Major Key: [HKEY_USERS]\r\n")));
            RETAILMSG(TRUE, (_T("ExLaunch: Subkey: %s\r\n"), SubKey));
            *HKey = HKEY_USERS;
            return true;
        }

        RETAILMSG(TRUE, (_T("ExLaunch: Valid key not found\r\n"), SubKey));
        return false;
    }
}


//******************************************************************
//
//  ProcessRegFile - This expects registry entries in the form:
//
//  [HKEY_...\subkey\..\subkey]     <<---- key specification
//  "key0"=type:xyz                 <<---- value setting
//  "key1"=type:xyz                 <<---- value setting
//      .
//      .
//  "keyn"=type:xyz                 <<---- value setting
//  []                              <<---- end of value settings under the key specification
//
//  where type is dword, binary, or string. Multiple substrings are
//  allowed for a given
//
//  Example:
//  [HKEY_LOCAL_MACHINE\Software\Microsoft\Shell\AutoHide]
//    "Default"=dword:1          ; Enable Auto Hide
//  []
//
//  One or more value settings are allowed under a key.
//
//******************************************************************

bool ProcessRegFile(wchar_t * regfile)
{
    FILE *Stream;
    bool RegistryChanged = false;
    DWORD Error;

    // Open REG file
    if ((Stream = _wfopen(regfile, _T("rt"))) == NULL)
    {
        RETAILMSG(TRUE, (_T("ExLaunch: No Registry file found, skip registry update.\r\n")));

        return RegistryChanged;
    }

    while (!feof(Stream))
	{
        HKEY RootKey;
        TCHAR SubKey[MAX_REGISTRY_LINE_SIZE];

        // Get the HKEY and subkey
        if (!ParseNextKey(&RootKey, SubKey, Stream))
        {
            break;
        }

        // Multiple lines of values and settings are allowed
        while (true)
        {
            DWORD ValueType;
            TCHAR Value[MAX_REGISTRY_LINE_SIZE];
            BYTE  ValueSetting[MAX_REGISTRY_LINE_SIZE];
            DWORD ValueSettingLength;

            ZeroMemory(Value,sizeof(Value));

            ZeroMemory(ValueSetting,sizeof(ValueSetting));

            // Get the next value and setting
            if (!ParseNextValue(Value, &ValueType, ValueSetting, &ValueSettingLength, Stream))
            {
                break;
            }

            // Now we have a sub key and a value to work with...
            HKEY SubKeyHKey;
            DWORD Disposition;
            if (RegCreateKeyEx(RootKey, SubKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &SubKeyHKey, &Disposition) != ERROR_SUCCESS)
            {
                Error = GetLastError();
                RETAILMSG(TRUE, (_T("ExLaunch: Error %ld creating/opening registry key %s.\r\n"), Error, SubKey));
                break;
            }

            if (Disposition == REG_OPENED_EXISTING_KEY)                 // This is an existing key, we need to read the value and
                                                                        // if changing, write the new value
            {
                RETAILMSG(TRUE, (_T("ExLaunch: Opened existing registry key %s.\r\n"), SubKey));
            }
            else
            {
                RETAILMSG(TRUE, (_T("ExLaunch: New registry key %s.\r\n"), SubKey));
                RegistryChanged = true;
            }

            bool ValueChanged = false;

            DWORD QueryValueType;
            BYTE  QueryData[MAX_REGISTRY_LINE_SIZE];
            DWORD QueryDataLength = 0;

            // Query the value
            LONG ValueQueryResult = RegQueryValueEx(SubKeyHKey, Value, NULL, &QueryValueType, NULL, &QueryDataLength);

            if (ValueQueryResult == ERROR_FILE_NOT_FOUND)                       // This must be a new value
            {
                ValueChanged = true;
                RETAILMSG(TRUE, (_T("ExLaunch: New registry value %s = %s.\r\n"), Value, ValueSetting));
            }
            else                                                                // This is an existing value
            {
                if (ValueQueryResult != ERROR_SUCCESS)
                {
                    Error = GetLastError();
                    RETAILMSG(TRUE, (_T("ExLaunch: Error %ld reading the registry value %s.\r\n"), Error, Value));
                    if (RegCloseKey(SubKeyHKey) != ERROR_SUCCESS)
                    {
                        Error = GetLastError();
                        RETAILMSG(TRUE, (_T("ExStartup: Error %ld closing the registry key %s.\r\n"), Error, SubKey));
                    }
                    break;
                }

                RETAILMSG(TRUE, (_T("ExLaunch: Existing registry value %s.\r\n"), Value));

                // The previous query got the data size
                // Now read the value
                if (RegQueryValueEx(SubKeyHKey, Value, NULL, &QueryValueType, QueryData, &QueryDataLength) != ERROR_SUCCESS)
                {
                    Error = GetLastError();
                    RETAILMSG(TRUE, (_T("ExLaunch: Error %ld reading the registry value %s.\r\n"), Error, Value));
                    if (RegCloseKey(SubKeyHKey) != ERROR_SUCCESS)
                    {
                        Error = GetLastError();
                        RETAILMSG(TRUE, (_T("ExLaunch: Error %ld closing the registry key %s.\r\n"), Error, SubKey));
                    }
                    break;
                }

                if (ValueSettingLength != QueryDataLength)
                {
                    ValueChanged = true;
                }
                else
                {
                    // check byte by byte for a change
                    for (DWORD i=0; i<ValueSettingLength; i++)
                    {
                        if (ValueSetting[i] != QueryData[i])
                        {
                            ValueChanged = true;
                        }
                    }
                }
                if (ValueChanged)
                {
                    RETAILMSG(TRUE, (_T("ExLaunch: Changed registry value setting, old = %s, new = %s.\r\n"), QueryData, ValueSetting));
                }
                else
                {
                    RETAILMSG(TRUE, (_T("ExLaunch: Registry value unchanged.\r\n")));
                }
            }

            if (ValueChanged)                                           // something is changed, or it's new, so write the value
            {
                if (RegSetValueEx(SubKeyHKey, Value, NULL, ValueType, ValueSetting, ValueSettingLength) != ERROR_SUCCESS)
                {
                    Error = GetLastError();
                    RETAILMSG(TRUE, (_T("ExLaunch: Error %ld writing the registry value %s.\r\n"), Error, Value));
                    if (RegCloseKey(SubKeyHKey) != ERROR_SUCCESS)
                    {
                        Error = GetLastError();
                        RETAILMSG(TRUE, (_T("ExLaunch: Error %ld closing the registry key %s.\r\n"), Error, SubKey));
                    }
                    break;
                }
                RegistryChanged = true;
            }

            if (RegCloseKey(SubKeyHKey) != ERROR_SUCCESS)
            {
                Error = GetLastError();
                RETAILMSG(TRUE, (_T("ExLaunch: Error %ld closing the registry key %s.\r\n"), Error, SubKey));
                break;
            }
        }
    }

    // Only flush the registry if something changed
    if (RegistryChanged)
    {
        RETAILMSG(TRUE, (_T("ExLaunch: Flush registry.\r\n")));

#define FLUSH_KEYS_EXE              _T("\\Windows\\RegFlushKeys.exe")

        PROCESS_INFORMATION pi;
        CreateProcess(FLUSH_KEYS_EXE, NULL, NULL, 0, FALSE, 0, 0, 0, NULL, &pi);

        Sleep(3000);
    }

    return RegistryChanged;
}

//////////////////////////////////////////////////////////////////////////////
// void SetMouseCursorState(bool bEscape)
//      Edit registry file and either display or hide the mouse cursor
//      Modify [HKEY_LOCAL_MACHINE\Drivers\Display\Intel]
// bEscape : indicate if we need to turn on/off mouse cursor.
//          true - Turn the Cursor on
//          false - Turn the Cursor off
// Return:
//      Boolean indicates if the registry has been changed and Reboot is
//      required to get the correct mouse cursor display.
//////////////////////////////////////////////////////////////////////////////
#define ROOT_KEY                HKEY_LOCAL_MACHINE
#define KEY_MOUSE_CURSOR        _T("\\System\\GDI\\Drivers")
#define STRING_MOUSE_CURSOR     _T("Cursor")
#define VALUE_MOUSE_CURSOR_ON   _T("DWORD:1")
#define VALUE_MOUSE_CURSOR_OFF  _T("DWORD:0")

bool SetMouseCursorState(bool bEscape)
{
    HKEY hRootKey = ROOT_KEY;
    HKEY hSubKey;
    DWORD dwDisposition;
    DWORD dwError;
    bool bReboot = false;                                       // if reboot required


    DWORD ValueType;                                            // value is dword, should be 4
    BYTE  ValueSetting[MAX_REGISTRY_LINE_SIZE];                 // new value settings
    DWORD ValueSettingLength;                                   // value settings length, should be 4

    // new settings
    DWORD ConvertedValue = DWORD(bEscape);                      // DWORD:1
    ValueSetting[0] = (BYTE)ConvertedValue & 0xff;              // 0000
    ValueSetting[1] = (BYTE)(ConvertedValue >> 8) & 0xff;
    ValueSetting[2] = (BYTE)(ConvertedValue >> 16) & 0xff;
    ValueSetting[3] = (BYTE)(ConvertedValue >> 24) & 0xff;
    ValueSettingLength = sizeof(DWORD);                         // 4
    ValueType = REG_DWORD;                                      // 4

    // create the key, open it if it does exist.
    if (RegCreateKeyEx(hRootKey, KEY_MOUSE_CURSOR, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hSubKey, &dwDisposition) != ERROR_SUCCESS)
    {
        dwError = GetLastError();
        RETAILMSG(TRUE, (_T("ExLaunch: Error %ld creating/opening registry key %s.\r\n"), dwError, KEY_MOUSE_CURSOR));
        return false;
    }

    if (dwDisposition == REG_OPENED_EXISTING_KEY)                 // This is an existing key, we need to read the value and
                                                                  // if changing, write the new value
    {
        RETAILMSG(TRUE, (_T("ExLaunch: Open exist registry key %s.\r\n"), KEY_MOUSE_CURSOR));

        DWORD QueryValueType;
        BYTE  QueryData[MAX_REGISTRY_LINE_SIZE];
        DWORD QueryDataLength = 0;

        // Query the key value
        LONG ValueQueryResult = RegQueryValueEx(hSubKey, STRING_MOUSE_CURSOR, NULL, &QueryValueType, NULL, &QueryDataLength);

        if (ValueQueryResult == ERROR_FILE_NOT_FOUND)                       // This must be a new value
        {
            bReboot = true;
            RETAILMSG(TRUE, (_T("ExLaunch: New registry value %s = %s.\r\n"), STRING_MOUSE_CURSOR, ValueSetting));
        }
        else                                                                // This is an existing value
        {
            if (ValueQueryResult != ERROR_SUCCESS)                          // error
            {
                dwError = GetLastError();
                RETAILMSG(TRUE, (_T("ExLaunch: Error %ld reading the registry value %s.\r\n"), dwError, STRING_MOUSE_CURSOR));
                if (RegCloseKey(hSubKey) != ERROR_SUCCESS)
                {
                    dwError = GetLastError();
                    RETAILMSG(TRUE, (_T("ExLaunch: Error %ld closing the registry key %s.\r\n"), dwError, STRING_MOUSE_CURSOR));
                }

                return false;
            }

            // compare if the target value matches the setting value
            if((QueryDataLength != ValueSettingLength) || (QueryValueType != ValueType))    // different key
            {
                bReboot = true;
            }
            else                                                            // compare the value
            {
                ValueQueryResult = RegQueryValueEx(hSubKey, STRING_MOUSE_CURSOR, NULL, &QueryValueType, QueryData, &QueryDataLength);
                if (ValueQueryResult != ERROR_SUCCESS)                      // error
                {
                    dwError = GetLastError();
                    RETAILMSG(TRUE, (_T("ExLaunch: Error %ld reading the registry value %s.\r\n"), dwError, STRING_MOUSE_CURSOR));
                    if (RegCloseKey(hSubKey) != ERROR_SUCCESS)
                    {
                        dwError = GetLastError();
                        RETAILMSG(TRUE, (_T("ExLaunch: Error %ld closing the registry key %s.\r\n"), dwError, STRING_MOUSE_CURSOR));
                    }

                    return false;
                }

                for(DWORD i=0; i < QueryDataLength; i++)                    // check if target value is not matching the setting value
                {
                    if(QueryData[i] != ValueSetting[i])
                    {
                        bReboot = true;
                        break;
                    }
                }
            }
        }
    }
    else
    {                                                                       // new key
        bReboot = true;
        RETAILMSG(TRUE, (_T("ExLaunch: New registry key %s.\r\n"), STRING_MOUSE_CURSOR));
    }

    // check if we need to change the registry, or to reboot
    if(bReboot)
    {                                                           // need to change key/value, and reboot
        // change value
        if (RegSetValueEx(hSubKey, STRING_MOUSE_CURSOR, NULL, ValueType, ValueSetting, ValueSettingLength) != ERROR_SUCCESS)
        {
            dwError = GetLastError();
            RETAILMSG(TRUE, (_T("ExLaunch: Error %ld writing the registry value %s.\r\n"), dwError, STRING_MOUSE_CURSOR));
            if (RegCloseKey(hSubKey) != ERROR_SUCCESS)
            {
                dwError = GetLastError();
                RETAILMSG(TRUE, (_T("ExStartup: Error %ld closing the registry key %s.\r\n"), dwError, STRING_MOUSE_CURSOR));

                return false;
            }
        }

        RETAILMSG(TRUE, (_T("ExLaunch: Opened existing registry key %s.\r\n"), STRING_MOUSE_CURSOR));

        RegFlushKey(hSubKey);

        if (RegCloseKey(hSubKey) != ERROR_SUCCESS)
        {
            dwError = GetLastError();
            RETAILMSG(TRUE, (_T("ExLaunch: Error %ld closing the registry key %s.\r\n"), dwError, STRING_MOUSE_CURSOR));
            return false;
        }

        // now key/value has been changed. need to flush and reboot
        RETAILMSG(TRUE, (_T("ExLaunch: Registry key %s has been updated, rebooting...\r\n"), STRING_MOUSE_CURSOR));

        Sleep(6000);

        // reboot in main
    }

    return bReboot;
}


//******************************************************************
//
//  SetupForWinCeShell -
//      Start the watchdog handler,
//      execute an autoload.exe that may be on the flash stick
//      set the registry per the TECH_REG_FILE_NAME
//      turn on the mouse (if changed state, reset)
//      start the CE shell
//
//******************************************************************

static void SetupForWinCeShell()
{
    PROCESS_INFORMATION pi;
		
	// disable keyboard or load technical registry
    ProcessRegFile(TECH_REG_FILE_NAME);

    // Change the registry to show mouse cursor if necessary, returns true
    // is a change occurred, which requires a reset.
    if (!SetMouseCursorState(true))                             // No reset is necessary?
    {
        // Don't start the system normally, just start the desktop
        if (!CreateProcess(CE_SHELL_EXE, NULL, NULL, 0, FALSE, 0, 0, 0, NULL, &pi))
        {
            RETAILMSG(TRUE, (_T("ExLaunch: failed to load desktop\r\nGetLastError returned: %d\r\n"), GetLastError()));
        }
        else
        {
            RETAILMSG(TRUE, (_T("ExLaunch: Requested normal WinCE desktop.\r\nStarted...\r\n")));
        }
    }
    else
    {
		RETAILMSG(TRUE, (_T("ExLaunch: Self restarting\r\n")));

		if (!CreateProcess(SYSTEM_RESTART_EXE, NULL, NULL, 0, FALSE, 0, 0, 0, NULL, &pi))
		{
			RETAILMSG(TRUE, (_T("ExLaunch: Self restart failed\r\n")));
		}
    }
}


bool ExecuteStartup(HINSTANCE hInstance, HKEY hSubKey, LPTSTR ValueName)
{
	DWORD QueryValueType;
    BYTE  QueryData[MAX_REGISTRY_LINE_SIZE];
    DWORD QueryDataLength  = MAX_REGISTRY_LINE_SIZE;
	PROCESS_INFORMATION pi;
	DWORD lResult;
	bool success = FALSE;
	DWORD cRetry = 0;

	lResult = RegQueryValueEx(hSubKey, ValueName, 0, &QueryValueType, QueryData, &QueryDataLength);
    
	if ((lResult == ERROR_SUCCESS) && (QueryValueType == REG_SZ))
    {
        RETAILMSG(1, (TEXT("ExLaunch: Found [HKLM/%s]\r\n"), ValueName));
	    
        // we only need to validate ExStartup.exe file only if the ValueName is StartupFolder1
        // which contains the absolute path for the ExStartup application
        if(wcscmp(RV_FLASH_STARTUP_FOLDER,ValueName) == 0)
        {	
            bool NewStartupExists = false;
			if(FileExists(EXSTARTUP_NEW_FILE_NAME))
				NewStartupExists = true;
			
			TCHAR ExStartupApplicationName[_MAX_PATH] = {0};

            // validate ExStartup.exe file here
            // does the original file fail the CRC test? if it doesn't fail, continue the process
            if(!NewStartupExists && !ValidateFile(EXSTARTUP_APPLICATION_FILE_NAME, EXSTARTUP_APPLICATION_CRC_FILE_NAME))
            {
                // try to validate the file from the backup directory
                if(ValidateFile(EXSTARTUP_APPLICATION_BACKUP_FILE_NAME,EXSTARTUP_APPLICATION_BACKUP_CRC_FILE_NAME))
                {   
                    // restore the original file
                    if(!CopyFile(EXSTARTUP_APPLICATION_BACKUP_FILE_NAME,EXSTARTUP_APPLICATION_FILE_NAME,FALSE))
                    {
                        RETAILMSG(TRUE, (_T("ExLaunch: Failed to restore file: %s\r\nGetLastError returned: %d\r\n"), EXSTARTUP_APPLICATION_FILE_NAME, GetLastError()));
                    }

                    // restore the original file's crc file here
                    if(!CopyFile(EXSTARTUP_APPLICATION_BACKUP_CRC_FILE_NAME,EXSTARTUP_APPLICATION_CRC_FILE_NAME,FALSE))
                    {
                        RETAILMSG(TRUE, (_T("ExLaunch: Failed to restore Crc file: %s\r\nGetLastError returned: %d\r\n"), EXSTARTUP_APPLICATION_BACKUP_CRC_FILE_NAME, GetLastError()));
                    }

                    // the backup ExStartup.exe is good, use it
                    wcscpy(ExStartupApplicationName,EXSTARTUP_APPLICATION_BACKUP_FILE_NAME);
                }
                else
                {   
                    if ((FileExists(EXSTARTUP_APPLICATION_FILE_NAME)) || FileExists(EXSTARTUP_APPLICATION_BACKUP_FILE_NAME))	// executable file exists?
					{
						RETAILMSG(TRUE, (_T("ExLaunch: Failed to validate file: %s\r\n"), EXSTARTUP_APPLICATION_FILE_NAME));
						HWND hWndMain = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, MainWndProc);
    
						MSG WindowMessage;
						LONG GetMessageReturnValue;

						while((GetMessageReturnValue = GetMessage(&WindowMessage, hWndMain, 0, 0 )) != 0)
						{ 
							if (GetMessageReturnValue == -1)
							{
								// error in getting a valid window message
								RETAILMSG(TRUE, (_T("ExLaunch: cannot get window message,  error=%d\r\n"),GetLastError()));
								return false;  // return false if we fail to get a valid window message
							}
							else
							{
								TranslateMessage(&WindowMessage); 

								DispatchMessage(&WindowMessage); 
							}
						}
						
						if(hWndMain)
						{
							DestroyWindow(hWndMain);
						}
					}
					else
						RETAILMSG(TRUE, (_T("\r\nExLaunch: Startup Application missing!!!\r\n")));

					return false;   // we fail to validate ExStartup.exe, return false immediatelly
                }
            }
            else  // the original ExStartup.exe is good, we need to validate the backup copy, restore it if the backup copy is bad
            {
                if(!ValidateFile(EXSTARTUP_APPLICATION_BACKUP_FILE_NAME,EXSTARTUP_APPLICATION_BACKUP_CRC_FILE_NAME))
                {   
                    // restore the backup file
                    if(!CopyFile(EXSTARTUP_APPLICATION_FILE_NAME, EXSTARTUP_APPLICATION_BACKUP_FILE_NAME, FALSE))
                    {
                        RETAILMSG(TRUE, (_T("ExLaunch: Failed to restore file: %s\r\nGetLastError returned: %d\r\n"), EXSTARTUP_APPLICATION_BACKUP_FILE_NAME, GetLastError()));
                    }

                    // restore the backup file's crc file here
                    if(!CopyFile(EXSTARTUP_APPLICATION_CRC_FILE_NAME, EXSTARTUP_APPLICATION_BACKUP_CRC_FILE_NAME,FALSE))
                    {
                        RETAILMSG(TRUE, (_T("ExLaunch: Failed to restore Crc file: %s\r\nGetLastError returned: %d\r\n"), EXSTARTUP_APPLICATION_BACKUP_CRC_FILE_NAME, GetLastError()));
                    }
                }
             
               // the original ExStartup.exe is good, use it
               wcscpy(ExStartupApplicationName,EXSTARTUP_APPLICATION_FILE_NAME);
            }

            RETAILMSG(TRUE, (_T("ExLaunch: Execute %s\r\n"), ExStartupApplicationName));
		    if (!CreateProcess(ExStartupApplicationName, NULL, NULL, 0, FALSE, 0, 0, 0, NULL, &pi))
		    {
			    RETAILMSG(TRUE, (_T("ExLaunch: failed to execute %s\r\n"), (LPCTSTR)QueryData));
			}
		    else
		    {
			    success = TRUE;
		    }
        }
        else // other executable files get executed immediatelly without checking for Crc
        {
           if (FileExists((LPCTSTR)QueryData))                            // Is the executable file here
           {
			    RETAILMSG(TRUE, (_T("ExLaunch: Execute %s\r\n"), (LPCTSTR)QueryData));
			    if (!CreateProcess((LPCTSTR)QueryData, NULL, NULL, 0, FALSE, 0, 0, 0, NULL, &pi))
			    {
				    RETAILMSG(TRUE, (_T("ExLaunch: failed to execute %s\r\n"), (LPCTSTR)QueryData));
			    }
			    else
			    {
				    success = TRUE;
			    }
           }
        }
    }

	return success;
}

//******************************************************************
//
//  ReadSpecialComboButtonAtPowerup() -
//  This function shall return true if manual inflation and dimness
//  membrane buttons are both pressed at power up.
//  Otherwise, this funciton shall return false.
//
//******************************************************************
static bool ReadSpecialComboButtonAtPowerup(HANDLE hAIODriver, DWORD Combo)
{
    bool success = false;
	DWORD ButtonDataByte = 0;
    
    if(DeviceIoControl(hAIODriver, IOCTL_AIODIO_GET_BUTTON_FLAGS, NULL, 0, &ButtonDataByte, sizeof(DWORD), NULL, NULL))
	{
		RETAILMSG(TRUE, (_T("ExLaunch : ButtonDataByte: 0x%x\r\n"), ButtonDataByte));
		if(ButtonDataByte == Combo)
			success = true;
	}
	else
	{
		RETAILMSG(TRUE, (_T("Failed getting Power-ON Button Flags\r\n")));
	}

    return success;
}

//**************************************************************************************
//
//  ValidateExecutableFileInUSBStick() -
//  This function shall return true if a Newport.ID file and an executable file
//  (specified within Newport.ID file) are both verified as valid files
//  which are generated and encoded by NMI. Otherwise, this function shall return false
//
//**************************************************************************************
static bool ValidateExecutableFileInUSBStick(TCHAR* DecodedExecutableFileName)
{
    TCHAR ExecutableCrcFileName[_MAX_PATH] = {0};  

    TCHAR EncodedExecutableFileName[_MAX_PATH] = {0};
   
    wcscpy(DecodedExecutableFileName,DOWNLOAD_SOURCE_DIRECTORY);  // make sure we have a valid path in the USB flash stick

    wcscpy(ExecutableCrcFileName,DOWNLOAD_SOURCE_DIRECTORY);      // make sure we have a valid path in the USB flash stick

    wcscpy(EncodedExecutableFileName,DOWNLOAD_SOURCE_DIRECTORY);  // make sure we have a valid path in the USB flash stick

    // try to open the Newport.ID file
    HANDLE NewportIdFileHandle = CreateFile(NEWPORT_ID_FILE,
                                            GENERIC_READ,
                                            0,
                                            NULL,
                                            OPEN_EXISTING,
                                            FILE_ATTRIBUTE_NORMAL,
                                            NULL);

    if(NewportIdFileHandle == INVALID_HANDLE_VALUE)
    {
        RETAILMSG(TRUE, (_T("Error! Failed to open Newport.ID file.\r\n")));
    
        return false;
    }
    
    // find out the file size of Newport.ID file in bytes
    DWORD NewportIdFileSize = GetFileSize(NewportIdFileHandle,NULL);
   
    // empty file case
    if(NewportIdFileSize == 0)
    {
        RETAILMSG(TRUE, (_T("Error! Failed to read any content from the Newport.ID file\r\n")));

        CloseHandle(NewportIdFileHandle);

        return false;
    }

    DWORD BytesRead;

    DWORD BytesWritten;

    TCHAR FileName[_MAX_PATH] = {0};

    ReadFile(NewportIdFileHandle, FileName, NewportIdFileSize, &BytesRead, NULL);
   
    if(BytesRead != NewportIdFileSize)
    {
        RETAILMSG(TRUE, (_T("Error! Failed to read any content from the Newport.ID file\r\n")));

        CloseHandle(NewportIdFileHandle);

        return false;
    }

    wcscat(EncodedExecutableFileName,FileName);        // update the Executable file name here
    wcscat(EncodedExecutableFileName,L".exe");         // update the Executable file name here

    wcscat(DecodedExecutableFileName,FileName);        // update the decoded Executable file name here
    wcscat(DecodedExecutableFileName,L"_NMI.exe");     // update the decoded Executable file name here

    wcscat(ExecutableCrcFileName,FileName);            // update the Executable crc file name here
    wcscat(ExecutableCrcFileName,L".crc");             // update the Executable crc file name here

    // NewportIdFileHandle is no longer is neededed, close the file
    CloseHandle(NewportIdFileHandle);

    // open the executable file and its crc file
    HANDLE ExecutableFileHandle = CreateFile(EncodedExecutableFileName,
                                             GENERIC_READ,
                                             0,
                                             NULL,
                                             OPEN_EXISTING,
                                             FILE_ATTRIBUTE_NORMAL,
                                             NULL);

    HANDLE CrcFileHandle =        CreateFile(ExecutableCrcFileName,
                                             GENERIC_READ,
                                             0,
                                             NULL,
                                             OPEN_EXISTING,
                                             FILE_ATTRIBUTE_NORMAL,
                                             NULL);
    
    if(ExecutableFileHandle == INVALID_HANDLE_VALUE ||
       CrcFileHandle        == INVALID_HANDLE_VALUE)
    {
        RETAILMSG(TRUE, (_T("Error! Failed to open the executable file %s.\r\n"),EncodedExecutableFileName));
    
        return false;
    }

    // find out the file size of executable file in bytes
    DWORD ExecutableFileSize = GetFileSize(ExecutableFileHandle,NULL);
 
    // allocate memory to hold an encoded executable file
    BYTE *ReadBuffer =  new BYTE[ExecutableFileSize];

    if(!ReadBuffer)
    {
        RETAILMSG(TRUE, (_T("Error! Fail to allocate memory!\r\n")));
     
        CloseHandle(ExecutableFileHandle);
        CloseHandle(CrcFileHandle);

        return false;
    }

    BYTE DataBuffer;

    // read in the entire executable into memory
    ReadFile(ExecutableFileHandle,ReadBuffer,ExecutableFileSize,&BytesRead,NULL);

    if(BytesRead != ExecutableFileSize)
    {
        RETAILMSG(TRUE, (_T("Error! Fail to read the executable file!\r\n")));
      
        CloseHandle(ExecutableFileHandle);
        CloseHandle(CrcFileHandle);
        
        if(ReadBuffer)
        {
            delete []ReadBuffer;
        }

        return false;
    }

    DWORD StoredCrcInFile;

    // read in the crc stored in the crc file
    ReadFile(CrcFileHandle,&StoredCrcInFile,sizeof(StoredCrcInFile),&BytesRead,NULL);

    if(BytesRead != sizeof(StoredCrcInFile))
    {
        RETAILMSG(TRUE, (_T("Error! Fail to read CRC from a file!\r\n")));
     
        CloseHandle(ExecutableFileHandle);
        CloseHandle(CrcFileHandle);
    
        if(ReadBuffer)
        {
            delete []ReadBuffer;
        }

        return false;
    }
    
    // close the file handles here
    CloseHandle(ExecutableFileHandle);
    CloseHandle(CrcFileHandle);

    DWORD CalculatedCrc;                  // initial Crc value
    DWORD i;
    
     // allocate memory to hold an decoded executable file
    BYTE *WriteBuffer = new BYTE[ExecutableFileSize];

    if(!WriteBuffer)
    {
        RETAILMSG(TRUE, (_T("Error! Fail to allocate memory!\r\n")));

        if(ReadBuffer)
        {
            delete []ReadBuffer;
        }

        return false;
    }

    RETAILMSG(TRUE, (_T("Start to decode the executable file in the USB flash stick...\r\n")));
    // loop through the entire Readbuffer, decode one byte at a time and save the data to WriteBuffer
    for(i = 0; i < ExecutableFileSize; i++)
    {
        // rotate the high and lower 4 bits for each byte in a reverse direction used by ApplicationEncoder application 
        DataBuffer  = (ReadBuffer[i] >> 4); 
            
        DataBuffer |= (ReadBuffer[i] << 4);
    
        WriteBuffer[i] = DataBuffer;
    }
    
    // clean up
    if(ReadBuffer)
    {
        delete []ReadBuffer;
    }

    // calculate crc here
    CalculatedCrc = ::CalculateCrc(WriteBuffer,ExecutableFileSize);

    // compare calculated crc in momory with the crc stored in the crc file 
    if(CalculatedCrc != StoredCrcInFile)
    {
        RETAILMSG(TRUE, (_T("Error! An invalid NMI application is found in the USB flash stick!\r\n")));

        if(WriteBuffer)
        {
            delete []WriteBuffer;
        }

        return false;
    }

    HANDLE DecodedFileHandle =     CreateFile(DecodedExecutableFileName,
                                              GENERIC_WRITE,
                                              0,
                                              NULL,
                                              CREATE_ALWAYS,
                                              FILE_ATTRIBUTE_NORMAL,
                                              NULL);

    if(DecodedFileHandle == INVALID_HANDLE_VALUE)
    {
        RETAILMSG(TRUE, (_T("Error! Failed to create a decoded executable file %s.\r\n"),DecodedExecutableFileName));
    
        if(WriteBuffer)
        {
            delete []WriteBuffer;
        }

        return false;
    }

    
    // if we fall through here, it means that data in WriteBuffer is good and valid
    // we need to write it to a new file and execute it later
    WriteFile(DecodedFileHandle,WriteBuffer,ExecutableFileSize,&BytesWritten, NULL);

    // incorrect number of bytes written?
    if(BytesWritten != ExecutableFileSize)
    {
        if(WriteBuffer)
        {
            delete []WriteBuffer;
        }
        
        CloseHandle(DecodedFileHandle);

        RETAILMSG(TRUE, (_T("Error! Failed to create a decoded executable file %s.\r\n"),DecodedExecutableFileName));  
    }
    
    // clean up
    if(WriteBuffer)
    {
        delete []WriteBuffer;
    }
    
    // close the file handle here
    CloseHandle(DecodedFileHandle);

    // if we fall through here, it indicates the executable file is a valid file generated by NMI
 
    RETAILMSG(TRUE, (_T("Verification has completed, a valid NMI application is found in the USB flash stick!\r\n")));
     
    return true;
}


#include <Notify.h>
 
bool ClearRS232Notifications()
{
    HANDLE *hNotifications = NULL;
    DWORD NumNotifications;
    DWORD Count;
    DWORD BytesNeeded;
    CE_NOTIFICATION_INFO_HEADER *NIHeader;
    CE_NOTIFICATION_TRIGGER* Trigger;
    CE_USER_NOTIFICATION* UserNotification;
    void *pBuffer;
	bool Success = false;
    
    // First call CeGetUserNotifications to find out how many notifications are available
    // by passing in zero as the second argument
    CeGetUserNotificationHandles( hNotifications, 0, &NumNotifications );
    RETAILMSG( 0, (TEXT("Num Notifications available %d\r\n"), NumNotifications ));

    if(NumNotifications == 0)
		return Success;

	hNotifications = (HANDLE *)malloc( NumNotifications * sizeof( HANDLE *) );

    // Now get the notifications
    CeGetUserNotificationHandles( hNotifications, NumNotifications, &NumNotifications );

    for( Count = 0; Count < NumNotifications; Count++ )
    {
        // Now with a valid handle to a notification find out how many bytes are needed
        // to get the data
        CeGetUserNotification( hNotifications[Count], 0, &BytesNeeded, NULL );
        RETAILMSG( 0, (TEXT("%d Bytes needed %d\r\n"), Count, BytesNeeded ));
        pBuffer = malloc( BytesNeeded );
        // Error checking left for  you, but now we can get the data
        CeGetUserNotification( hNotifications[Count], BytesNeeded, &BytesNeeded, (BYTE *)pBuffer );
        NIHeader = (CE_NOTIFICATION_INFO_HEADER *)pBuffer;
        Trigger = NIHeader->pcent;
        UserNotification = NIHeader->pceun;
        
        RETAILMSG( 0, (TEXT("\tStatus %d\r\n"), NIHeader->dwStatus ));
        RETAILMSG( 0, (TEXT("\tTrigger Type %d"), Trigger->dwType ));
        switch( Trigger->dwType )
        {
        case CNT_EVENT:
            RETAILMSG( 0, (TEXT(" Event\r\n")));
            break;
		default:
			break;
        }
        
        RETAILMSG( 0, (TEXT("\tTrigger Event %d"), Trigger->dwEvent ));
        switch( Trigger->dwEvent )
        {
        case NOTIFICATION_EVENT_RS232_DETECTED           :
			RETAILMSG( 1, (TEXT("ExLaunch : RS232 Notification Event Detected\r\n")));
			CeClearUserNotification(hNotifications[Count]);
			/*if( !CeRunAppAtEvent(TEXT("repllog.exe"), NOTIFICATION_EVENT_NONE) )
					RETAILMSG( 1, (TEXT(" Failed to remove RS232-Detected notification\r\n")));*/
            Success = true;
            break;
		default:
            break;
        }
        free( pBuffer );
    }
    free( hNotifications );
	return Success;
}

#define MAX_NUM_USB_RETRY			5
#define	DELAY_BETWEEN_USB_RETRY		1000	// 1 second wait time before retrying to read USB

#define UI_DEV_INSTALL_FILE_NAME					_T("\\Hard Disk\\E600DevInstall.txt")
#define BD_DEV_INSTALL_FILE_NAME					_T("\\Storage_Card\\E600DevInstall.txt")
#define EXSTARTUP_DIR_NAME							_T("\\FlashFX Disk\\Startup")
#define PROGRAMFILES_DIR_NAME						_T("\\FlashFX Disk\\ProgramFiles")
#define SCREEN_CAPTURE_DIR_NAME						_T("\\FlashFX Disk\\ScreensCapture")

#define USB_APP_FILE_NAME							_T("\\Hard Disk\\ProgramFiles\\ExArm.exe")
#define SD_APP_FILE_NAME							_T("\\Storage_Card\\ProgramFiles\\ExArm.exe")
#define FLASH_APP_FILE_NAME							_T("\\FlashFX Disk\\ProgramFiles\\ExArm.exe")

#define USB_STARTUP_FILE_NAME						_T("\\Hard Disk\\Startup\\ExStartup.exe")
#define SD_STARTUP_FILE_NAME						_T("\\Storage_Card\\Startup\\ExStartup.exe")
#define FLASH_STARTUP_FILE_NAME						_T("\\FlashFX Disk\\Startup\\ExStartup.exe")

#define USB_ESC_FILE_NAME							_T("\\Hard Disk\\Escape.req")
#define SD_ESC_FILE_NAME							_T("\\Storage_Card\\Escape.req")

void DevInstall(DWORD BspType)
{
	BOOL bCopySuccess;

	if(!FileExists(PROGRAMFILES_DIR_NAME))
	{
		RETAILMSG(1, (TEXT("ExLaunch: ProgramFiles directory not found, creating ...")));
		if(CreateDirectory(PROGRAMFILES_DIR_NAME,NULL))
			RETAILMSG(1, (TEXT("successful\r\n")));
		else
			RETAILMSG(1, (TEXT("failed\r\n")));
	}

	if(!FileExists(EXSTARTUP_DIR_NAME))
	{
		RETAILMSG(1, (TEXT("ExLaunch: Startup directory not found, creating ...")));
		if(CreateDirectory(EXSTARTUP_DIR_NAME,NULL))
			RETAILMSG(1, (TEXT("successful\r\n")));
		else
			RETAILMSG(1, (TEXT("failed\r\n")));
	}

	if(BspType == BSP_E600_BD)
	{
		if(!FileExists(BD_DEV_INSTALL_FILE_NAME))
		{
			RETAILMSG(1, (TEXT("ExLaunch: BD_DEV_INSTALL_FILE_NAME not found")));
			return;
		}

		if(FileExists(SD_STARTUP_FILE_NAME))
		{
			RETAILMSG(1, (TEXT("ExLaunch: SD_STARTUP_FILE_NAME found")));
			if(FileExists(FLASH_STARTUP_FILE_NAME))
			{
				RETAILMSG(1, (TEXT("ExLaunch: Deleting FLASH_STARTUP_FILE_NAME ...\r\n")));
				bCopySuccess = DeleteFile(FLASH_STARTUP_FILE_NAME);
			}
			RETAILMSG(1, (TEXT("ExLaunch: Copying SD_STARTUP_FILE_NAME ...\r\n")));
			bCopySuccess = CopyFile(SD_STARTUP_FILE_NAME, FLASH_STARTUP_FILE_NAME,FALSE);
			if(!bCopySuccess)
				RETAILMSG(1, (TEXT("ExLaunch: SD_STARTUP_FILE_NAME failed\r\n")));
		}

		if(FileExists(SD_APP_FILE_NAME))
		{
			RETAILMSG(1, (TEXT("ExLaunch: SD_APP_FILE_NAME found")));
			if(FileExists(FLASH_APP_FILE_NAME))
			{
				RETAILMSG(1, (TEXT("ExLaunch: Deleting FLASH_APP_FILE_NAME ...\r\n")));
				bCopySuccess = DeleteFile(FLASH_APP_FILE_NAME);
			}
			RETAILMSG(1, (TEXT("ExLaunch: Copying SD_APP_FILE_NAME ...\r\n")));
			bCopySuccess = CopyFile(SD_APP_FILE_NAME, FLASH_APP_FILE_NAME,FALSE);
			if(!bCopySuccess)
				RETAILMSG(1, (TEXT("ExLaunch: SD_APP_FILE_NAME failed\r\n")));
		}
	}
	else if(BspType == BSP_E600_UI)
	{
		if(!FileExists(SCREEN_CAPTURE_DIR_NAME))
		{
			RETAILMSG(1, (TEXT("ExLaunch: ScreenCapture directory not found, creating ...")));
			if(CreateDirectory(SCREEN_CAPTURE_DIR_NAME,NULL))
				RETAILMSG(1, (TEXT("successful\r\n")));
			else
				RETAILMSG(1, (TEXT("failed\r\n")));
		}
	}
}


void DevInstallUI()
{
	if(FileExists(UI_DEV_INSTALL_FILE_NAME))
	{
		RETAILMSG(1, (TEXT("ExLaunch: Copying files from DOWNLOAD_SOURCE_DIRECTORY ... \r\n")));
		//Copy from src to dst
		BOOL bCopySuccess;

		if(FileExists(USB_STARTUP_FILE_NAME))
		{
			if(FileExists(FLASH_STARTUP_FILE_NAME))
			{
				bCopySuccess = DeleteFile(FLASH_STARTUP_FILE_NAME);
			}
			bCopySuccess = CopyFile(USB_STARTUP_FILE_NAME, FLASH_STARTUP_FILE_NAME,FALSE);
			if(!bCopySuccess)
				RETAILMSG(1, (TEXT("ExLaunch: Copy USB_STARTUP_FILE_NAME failed\r\n")));
		}

		bCopySuccess = CopyFile(L"\\Hard Disk\\ProgramFiles\\arial_1_30.ttf", L"\\FlashFX Disk\\ProgramFiles\\arial_1_30.ttf",FALSE);
		bCopySuccess = CopyFile(L"\\Hard Disk\\ProgramFiles\\arialbd.ttf", L"\\FlashFX Disk\\ProgramFiles\\arialbd.ttf",FALSE);
		bCopySuccess = CopyFile(L"\\Hard Disk\\ProgramFiles\\Medical.ttf", L"\\FlashFX Disk\\ProgramFiles\\Medical.ttf",FALSE);
		bCopySuccess = CopyFile(L"\\Hard Disk\\ProgramFiles\\msgothic.ttf", L"\\FlashFX Disk\\ProgramFiles\\msgothic.ttf",FALSE);
		bCopySuccess = CopyFile(L"\\Hard Disk\\ProgramFiles\\simsun.ttf", L"\\FlashFX Disk\\ProgramFiles\\simsun.ttf",FALSE);
		bCopySuccess = CopyFile(L"\\Hard Disk\\ProgramFiles\\English.str", L"\\FlashFX Disk\\ProgramFiles\\English.str",FALSE);
		bCopySuccess = CopyFile(L"\\Hard Disk\\ProgramFiles\\StringIds.h", L"\\FlashFX Disk\\ProgramFiles\\StringIds.h",FALSE);
		
		if(FileExists(FLASH_APP_FILE_NAME))
		{
			bCopySuccess = DeleteFile(FLASH_APP_FILE_NAME);
		}
		bCopySuccess = CopyFile(USB_APP_FILE_NAME, FLASH_APP_FILE_NAME,FALSE);
		if(!bCopySuccess)
			RETAILMSG(1, (TEXT("ExLaunch: Copying UI_APP_FILE_NAME failed\r\n")));
	}
}

bool DetectUSB()
{
	DWORD nRetry = 0;
	bool Found = false;

	RETAILMSG(1, (TEXT("ExLaunch: Detecting USB ... \r\n")));
	while(++nRetry < MAX_NUM_USB_RETRY)
	{
		Found = FileExists(DOWNLOAD_SOURCE_DIRECTORY);
		if(Found)
			break;
		Sleep(DELAY_BETWEEN_USB_RETRY);
	}
	if(Found)
		RETAILMSG(1, (TEXT("USB Present\r\n")));
	else
		RETAILMSG(1, (TEXT("USB Not Present\r\n")));
	return Found;
}


//******************************************************************
//
//
//
//******************************************************************

int WINAPI WinMain(HINSTANCE   hInstance,
                   HINSTANCE   hInstPrev,
                   LPTSTR      lpszCmdLine,
                   int         nCmdShow)
{
    HWND WndMain = NULL;
	bool Success;
	UINT8 Count = 0;

	RETAILMSG(TRUE, (_T("\r\n----- ExLaunch Starting -----\r\n")));

	HKEY hKey;

	HANDLE hAIODriver;
	LPCTSTR lpAIOFileName = TEXT("AIO1:");

	HBITMAP OldBitmap;
	HDC MemoryDc;
	HBITMAP BitmapHandle = NULL;
	BITMAP BitmapObjectInfo;
	HDC DisplayDc;

	do
	{
		Success = ClearRS232Notifications();
		if(Success)
			RETAILMSG(TRUE, (_T("ExLaunch : RS232 Notifications Cleared\r\n\r\n")));
	}while((++Count < 5) && (Success == false));

	if( Success && !CeRunAppAtEvent(TEXT("repllog.exe"), NOTIFICATION_EVENT_RS232_DETECTED) )
    {
		RETAILMSG( 1, (TEXT("ExLaunch : Failed to add RS232-Detected notification\r\n\r\n")));
    }

	hAIODriver= CreateFile( lpAIOFileName,  GENERIC_READ | GENERIC_WRITE,
		0, 0, OPEN_EXISTING, 0, 0);

	if(hAIODriver == INVALID_HANDLE_VALUE)
	{
		DWORD error = GetLastError();
		RETAILMSG(TRUE, (_T("ExLaunch : CreateFile AIO invalid handle, error %d\r\n"), error));
		return -1;
	}
	
	DWORD BspType = BSP_INVALID;
	if(DeviceIoControl(hAIODriver, IOCTL_AIODIO_GET_BSPTYPE, NULL, 0, &BspType, sizeof(DWORD), NULL, NULL))
		RETAILMSG(TRUE, (L"ExLaunch : BSP_TYPE = %d\r\n", BspType));
	else
		RETAILMSG(TRUE, (L"ExLaunch : Failed getting BSP_TYPE\r\n"));
	// Can set the brightness level here but needs to know the level for a specific model (AURA or HT70)
	// The goal is to have one OS for all products so it is not desirable to do it here
	//DWORD BacklightLevel = 0x0ff0;
	//DeviceIoControl(hAIODriver, IOCTL_AIODIO_SET_BRIGHTNESS, &BacklightLevel, sizeof(DWORD), NULL, 0, NULL, NULL);

	DEVMODE DevMode;
    memset(&DevMode, 0, sizeof (DevMode));
    DevMode.dmSize               = sizeof (DevMode);      
	DevMode.dmFields             = DM_DISPLAYORIENTATION;
	DevMode.dmDisplayOrientation = DMDO_90;

	if(BspType == BSP_E600_UI)
	{
		if (DISP_CHANGE_SUCCESSFUL != ChangeDisplaySettingsEx(NULL, &DevMode, NULL, CDS_RESET, NULL))
		{
			RETAILMSG(1, (L"ExLaunch : ChangeDisplaySettingsEx failed to set rotation angles (error=%d).\n", GetLastError()));
		}
		else
		{
			RETAILMSG(1, (L"ExLaunch : ChangeDisplaySettingsEx successfully set the  rotation angles to %d\r\n", DevMode.dmDisplayOrientation));
		}
	}

#if 1
	if(BspType != BSP_E600_BD)
	{
		WndMain = CreateWindow(L"STATIC",             // the window class name, use the pre-defined STATIC class to display text
							   L"",                   // The window title, no title text
							   WS_DLGFRAME,           // Window style as dialog window
							   CW_USEDEFAULT,         // Default screen position of upper left
							   CW_USEDEFAULT,         // corner of our window as x,y...
							   CW_USEDEFAULT,         // Default window size
							   CW_USEDEFAULT,         // .... 
							   NULL,                  // No parent window
							   NULL,                  // No menu
							   hInstance,             // Program Instance handle
							   NULL                   // No window creation data
							   ) ;

		if(!WndMain)
		{
			RETAILMSG(TRUE, (_T("Error creating a window to display a bitmap! Error code = %d\r\n"),GetLastError()));
		}
		else
		{ 
			BitmapHandle = LoadBitmap(hInstance, MAKEINTRESOURCE(IDB_BITMAP1));
		    
			if(BitmapHandle == NULL)
			{
				RETAILMSG(TRUE, (_T("Error loading bitmap! Error code = %d\r\n"),GetLastError()));
			}
			else
			{
				ShowWindow(WndMain,SW_SHOWNORMAL);  // to show the window
				UpdateWindow(WndMain);              // to paint the window
				DisplayDc = GetDC(WndMain);		
				MemoryDc = CreateCompatibleDC(DisplayDc);
				OldBitmap = (HBITMAP)SelectObject(MemoryDc, BitmapHandle);  // select the bitmap into memory
				GetObject(BitmapHandle, sizeof(BitmapObjectInfo), &BitmapObjectInfo);

				// display the bitmap
				BitBlt(DisplayDc, 0, 0, BitmapObjectInfo.bmWidth, BitmapObjectInfo.bmHeight, MemoryDc, 0, 0, SRCCOPY);

				// clean up
				SelectObject(MemoryDc, OldBitmap);
				DeleteDC(MemoryDc);
				ReleaseDC(WndMain,DisplayDc);
				DeleteObject(BitmapHandle);
			}
		}
	}

	if(hAIODriver && ReadSpecialComboButtonAtPowerup(hAIODriver, SPECIAL_BUTTON_COMB0))
    {
        CloseHandle(hAIODriver);

		int RetryCounter = 5;
        // try to access the USB flash stick for five times before concluding it is not physically connected
        while(--RetryCounter >= 0)
        {
            // if the special Newport.ID file doesn't exist in the USB flash stick, sleep 1 second and try again
            if(FileExists(NEWPORT_ID_FILE) == false)
            {
                Sleep(DELAY_BETWEEN_USB_RETRY);        
            }
            else
            {
                // Newport.Id exists in the USB flash stick            
                RETAILMSG(TRUE, (_T("Found the special Newport.ID file in the USB flash stick\r\n")));

                TCHAR DecodedExecutableFileName[_MAX_PATH] = {0};

                // check and verify if the USB flash stick contains a valid executable file and a valid Newport.ID file
                if(ValidateExecutableFileInUSBStick(DecodedExecutableFileName))
                {
                    RETAILMSG(TRUE, (_T("Start executing the application %s....\r\n"),DecodedExecutableFileName));
    
                    PROCESS_INFORMATION pi;
                    
                    // The executable file in the USB flash stick is valid, go ahead to execute it.
                    if (!CreateProcess(DecodedExecutableFileName, NULL, NULL, 0, FALSE, 0, 0, 0, NULL, &pi))
                    {
                        RETAILMSG(TRUE, (_T("Error! Failed to execute %s application in USB flash stick\r\n"),DecodedExecutableFileName));

                        break;          // do we fail to execute the application in the USB flash stick? if yes, break the while loop and continue on
                    }
                    else
                    {
                        RETAILMSG(TRUE, (_T("----- ExLaunch Finished ----- \r\n") ));
                       
                        return 0;
                    }
                }
                else
                {
                    break;            // do we fail to validate the application in the USB flash stick? if yes, break the while loop and continue on
                }
            }
        }// end of while loop
    }

	// try to open [HKLM\hardware\devicemap\touch] key
    if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_LOCAL_MACHINE, RK_PLATFORM_STARTUPDIR, 0, KEY_ALL_ACCESS, &hKey))
    {
		RETAILMSG(1, (TEXT("ExLaunch: Can't find [HKLM/%s]\r\n"), RK_PLATFORM_STARTUPDIR));
		return 0;
    }	
	
	BOOL USBFound = false;
	BOOL EscFound = false;

	//////////////////////////////////////////////////////////////////////////////
	DevInstall(BspType);

	if(BspType == BSP_E600_UI)
	{
		USBFound = DetectUSB();
		if(USBFound)
		{
			DevInstallUI();
			EscFound = FileExists(USB_ESC_FILE_NAME);
		}
	}
	else
	{
		EscFound = FileExists(SD_ESC_FILE_NAME);
	}

	if(EscFound)
	{
		RETAILMSG(1, (TEXT("ExLaunch: ESC file found ... going to desktop\r\n")));
		SetupForWinCeShell();
	}
	else
	{
	//////////////////////////////////////////////////////////////////////////////

	if(!ExecuteStartup(hInstance, hKey, RV_FLASH_STARTUP_FOLDER))
	{
		if(!ExecuteStartup(hInstance, hKey, RV_SD_STARTUP_FOLDER))
		{
			if(BspType == BSP_E600_BD)
			{
				SetupForWinCeShell();
			}
			else
			{
				bool Success = false;
				if(USBFound)
				{	
					Success = ExecuteStartup(hInstance, hKey, RV_USB_STARTUP_FOLDER);
				}
				if(!Success)
				{
					SetupForWinCeShell();
				}
			}
		}
	}
	}
		
	RegCloseKey(hKey);

#else
	PROCESS_INFORMATION pi;
	CreateProcess(CE_SHELL_EXE, NULL, NULL, 0, FALSE, 0, 0, 0, NULL, &pi);
#endif

	SignalStarted(_wtol(lpszCmdLine));

	// If necessary, destroy the dialog
    if (WndMain)
    {
        DestroyWindow(WndMain);
    }

    RETAILMSG(TRUE, (_T("----- ExLaunch Finished ----- \r\n\n") ));
    return 0;
}

