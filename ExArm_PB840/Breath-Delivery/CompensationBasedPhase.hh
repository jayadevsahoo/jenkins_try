#ifndef CompensationBasedPhase_HH
#define CompensationBasedPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CompensationBasedPhase - the base class for TcvPhase and PavPhase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/CompensationBasedPhase.hhv   10.7   08/17/07 09:35:48   pvcs  
//
//@ Modification-Log
//
//  Revision: 001   By: syw   Date:  21-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		Initial release.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "BreathPhase.hh"

//@ Usage-Classes

//@ End-Usage

class CompensationBasedPhase : public BreathPhase
{
  public:
    CompensationBasedPhase( void) ;
    virtual ~CompensationBasedPhase( void) ;

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

    virtual void newCycle( void) ;
    virtual void newBreath( void) ;
    virtual void relinquishControl(  const BreathTrigger& trigger) ;
    virtual inline Real32 getResistivePressure( void) ;
    virtual inline Real32 getElasticPressure( void) ;

  protected:

	virtual void determineResistivePressure_( const Real32 lungFlow) = 0 ;
	virtual void determineElasticPressure_( const Real32 lungVolume) = 0 ;
	virtual void updatePercentSupport_( void) ;
	
  	//@ Data-Member: resistivePressure_
  	// resistive pressure component of compensation
  	Real32 resistivePressure_ ;

  	//@ Data-Member: elasticPressure
  	// elastic pressure component of compensation
  	Real32 elasticPressure_ ;

	//@ Data-Member: percentSupport_
	// Percentage support setting.
	Real32 percentSupport_ ;
  
  private:
    CompensationBasedPhase( const CompensationBasedPhase&) ;	// not implemented...
    void   operator=( const CompensationBasedPhase&) ; 			// not implemented...

	void enableExhalationTriggers_( void) ;
	Real32 getFeedbackPressure_( void) ;
	inline Real32 calculatePressureError_(const Real32 pressureTarget, const Real32 pressure,
				const Real32 errorGain);

	//@ Data-Member: totalTrajLimitReached_
	// flag indicating whether the targeted pressure is
	// beyond the high pressure circuit setting
	Boolean totalTrajLimitReached_ ;

	//@ Data-Member: highCircuitPressure_
	// high circuit pressure setting
	Real32 highCircuitPressure_ ;

	//@ Data-Member: totalTrajLimit_
	// max pressure limit for breath
	Real32 totalTrajLimit_ ;

	//@ Data-Member: errorGain_
	// gain on error
	Real32 errorGain_ ;

	//@ Data-Member: desiredPressure_
	// to hold the desired pressure target
	Real32 desiredPressure_ ;

	//@ Data-Member: tubeId_
	// to hold the tube inner diameter
	Real32 tubeId_ ;

	//@ Data-Member: startingPeep_
	// starting pressure which will be used to calculate the peep
	Real32 startingPeep_ ;

	//@ Data-Member: loopCount_
	// number of BD cycle counter to evaluate the peep target at each cycle
	Int32 loopCount_ ;

	//@ Data-Member: peepTarget_
	// peep target pressure which is equivalent to the PEEP settings
	Real32 peepTarget_ ;

	//@ Data-Member: frozenElasticPressure_
	// elastic pressure when totalTrajLimitReached_ is set
	Real32 frozenElasticPressure_ ;

	//@ Data-Member: wf_
	// weighting factor to select feedback pressure
	Real32 wf_ ;
} ;

#include "CompensationBasedPhase.in"

#endif // CompensationBasedPhase_HH 
