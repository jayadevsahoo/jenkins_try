
#ifndef SettingPtr_HH
#define SettingPtr_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  SettingPtr -- Type Definition of 'SettingPtr'.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingPtr.hhv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//            Initial version 
//  
//====================================================================

// forward declaration...
class  Setting;


//@ Type:  SettingPtr
// A pointer to a setting.
typedef Setting*  SettingPtr;


#endif // SettingPtr_HH 
