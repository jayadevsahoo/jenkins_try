
#ifndef WaveformDataInfo_HH
#define WaveformDataInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  WaveformDataInfo - Waveform Data Manager class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/WaveformDataInfo.hhv   10.7   08/17/07 10:28:46   pvcs  
//
//@ Modification-Log
//
//  Revision: 004  By:  erm    Date:  3-May-2009    SCR Number: 6498
//  Project:  840S
//  Description:
//		increased the number of buffer elements to 150
// 
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//  
//  Revision: 001  By:  jhv    Date:  26-Jan-94    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "WaveformData.hh"
#include "PatientDataClassIds.hh"
#include "MutEx.hh"

//During s840 there were an 8 element over-run 
//increase to 8*3 round up to 30 -> 150 total
const Int32 NUM_TOTAL_WAVEFORM_DATA = 150;
class WaveformDataInfo
{
  public:
    WaveformDataInfo(void);
    void activate();
    void deactivate();
	static void Initialize();
	inline Boolean  isActive();
    void addData(const WaveformData *pData);
    SigmaStatus  getData(WaveformData *pData);

    static void  SoftFault(const SoftFaultID softFaultID, 
						   const Uint32      lineNumber, 
						   const char*       pFileName = NULL, 
						   const char*       pBoolTest = NULL); 

  private:
    ~WaveformDataInfo(void);   // not implemented.
    WaveformDataInfo(const WaveformDataInfo& wd); // not implemented ...
    void  operator=(const WaveformDataInfo&);	   // not implemented...


    //@Data-Member: buffer_[NUM_TOTAL_WAVEFORM_DATA];
    // a circular linked list of waveform breath data buffer.
    WaveformDataBuf buffer_[NUM_TOTAL_WAVEFORM_DATA];

    //@Data-Member: RDataLock_
    // RDataLock_ is a semaphore used to seriaize the waveform data pool.
    static MutEx	&RDataLock_;

    //@Data-Member: pTail_
    // pTail_ points to the tail end of waveform breath data.
    // Waveform data item is read from the tail end of the waveform buffer.
    WaveformDataBuf	*pTail_;

    //@Data-Member: pHead_
    // pHead_ points to the head of waveform breath data,
    // as a new waveform data arrives this head will move onto next buffer
    // to store a newly delivered data.
    WaveformDataBuf	*pHead_;

};

// include inline methods.
#include  "WaveformDataInfo.in"

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
#endif  // WaveformDataInfo_HH
