#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmAction - Alarm Action Info
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is used to store the information of the 'AlarmEvent'
//  class, to be used in the Alarm History Log.  By storing the alarm
//  event information into this structure, each alarm history entry
//  is 12 bytes smaller than if an 'AlarmEvent' instance was stored
//  directly.  This saves precious NOVRAM space.
//---------------------------------------------------------------------
//@ Rationale
//  Provides a compact storage structure for alarm events.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The first byte of this structure is shared between two, mutually-
//  exclusive variables:  'systemLevelId', which may contain the ID of
//  a system event (e.g., "correction" of this entry); and 'currentUrgency',
//  which contains the ID of the current urgency of this alarm event.  This
//  field should always be checked to see if this instance contains a
//  system event, or an actual alarm event.
//
//  $[00628] -- alarm diagnostic information...
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/AlarmAction.ccv   25.0.4.0   19 Nov 2013 14:18:46   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//  
//=====================================================================

#include "AlarmAction.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AlarmAction(alarmAction)  [Copy Constructor]
//
//@ Interface-Description
//  Construct an instance of 'AlarmAction', and initialize it to the
//  current state of 'alarmAction'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward on to this class' assignment operator.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  the entries of '*this' are equivalent to 'alarmAction'.
//@ End-Free-Function
//=====================================================================

AlarmAction::AlarmAction(const AlarmAction& alarmAction)
{
  CALL_TRACE("AlarmAction(alarmAction)");

  operator=(alarmAction);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AlarmAction()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default instance of 'AlarmAction'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

AlarmAction::AlarmAction(void)
{
  CALL_TRACE("AlarmAction()");

  currentUrgency = 0;
  baseMessage    = 0;
  analysisIndex  = 0;
  updateType     = 0;
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~AlarmAction()  [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

AlarmAction::~AlarmAction(void)
{
  CALL_TRACE("~AlarmAction()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(alarmAction)  [Assignment Operator]
//
//@ Interface-Description
//  Copy the contents of 'alarmAction' into this instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  the entries of '*this' are equivalent to 'alarmAction'.
//@ End-Free-Function
//=====================================================================

void
AlarmAction::operator=(const AlarmAction& alarmAction)
{
  CALL_TRACE("operator=(alarmAction)");

  // copy all of the items from 'alarmAction' into this alarm action...
  currentUrgency             = alarmAction.currentUrgency;
  baseMessage                = alarmAction.baseMessage;
  analysisIndex              = alarmAction.analysisIndex;
  updateType                 = alarmAction.updateType;
  whenOccurred               = alarmAction.whenOccurred;

  // TODO E600 MS this causes a crash. Investigate later.
  //arrCurrentAnalysisMessages = alarmAction.arrCurrentAnalysisMessages;
}   // $[TI1]


//=====================================================================
//
//  External Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Free-Function:  operator<<(ostr, alarmAction)  [Output Operator]
//
// Interface-Description
//  Dump to 'ostr' the current state of 'alarmAction'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Free-Function
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const AlarmAction& alarmAction)
{
  TimeOfDay  timeOfDay = alarmAction.whenOccurred.getTimeOfDay();

  ostr << "<"   << timeOfDay.month
       << "-"   << timeOfDay.date
       << "-"   << timeOfDay.year
       << " @ " << timeOfDay.hour
       << ":"   << timeOfDay.minute
       << ":"   << timeOfDay.second
       << ">: " << alarmAction.systemLevelId;

  return(ostr);
}

#endif // defined(SIGMA_DEVELOPMENT)
