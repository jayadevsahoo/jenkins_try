#pragma once

#include "resource.h"


// CLungSetupDlg dialog

class CLungSetupDlg : public CDialog
{
	DECLARE_DYNAMIC(CLungSetupDlg)

public:
	CLungSetupDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLungSetupDlg();

// Dialog Data
	enum { IDD = IDD_LUNG_SETUP_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_strLungsetup;
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
};
