#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================
 
//============================ C L A S S   D E S C R I P T I O N ====
//@  Class: BtatEventState - storage and maintenance utilities for 
//                           active BTAT alarms.
//---------------------------------------------------------------------
//@ Interface-Description
//  Organization of BtatEventStates in NOVRAM:
//  
//  There are 17 types of technical alarms that can be logged to NOVRAM.
//  Each alarm can have the state of enabled, not enabled, or empty.
//  These are designated by the enumerators BTAT_X_EN, BTAT_X_NOT_EN,
//  and EVENT_NAME_NULL.  An array of 17 elements containing the
//  OperatingParameterEventName enumerators is what we store in NOVRAM.
//  
//  The list stored in NOVRAM is the sequence of these states, in no
//  particular order.  The only limitation is that a not enabled
//  condition does not exist with its enabled state.
//
//  This is why the list needs to be searched for an existing entry
//  or its converse state.
//---------------------------------------------------------------------
//@ Rationale
//  NOVRAM interface class for storage and retrieval of active BTAT alarms.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The class provides utilities for search, deletion, insertion, and
//  reset functionalities for BTAT storage and retrieval.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/Safety-Net/vcssrc/BtatEventState.ccv   10.7   08/17/07 10:33:26   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001   By: by   Date: 12-Dec-1995  DR Number: None
//
//    Project:  Sigma (840)
//    Description:
//      Initial version.
//=====================================================================

#include "BtatEventState.hh"


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BtatEventState 
//
//@ Interface-Description
//  Constructor.
//  This constructor takes no argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes every element of arrBtatEvents_ to EVENT_NAME_NULL.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

BtatEventState::BtatEventState( void )
{
    // $[TI1]
    for (Uint8 btatEventNdx = 0; btatEventNdx < MAX_BTAT_EVENTS; ++btatEventNdx)
    {
        arrBtatEvents_[ btatEventNdx ] = EVENT_NAME_NULL;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BtatEventState
//
//@ Interface-Description
//  This constructor takes a reference to BtatEventState.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method takes a reference to BtatEventState class and 
//  copies the content of the reference to itself.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
 
BtatEventState::BtatEventState( const BtatEventState& rBtatEvents )
{
    // $[TI2]
    for ( Uint8 btatEventNdx = 0; btatEventNdx < MAX_BTAT_EVENTS; ++btatEventNdx )
    {
        arrBtatEvents_[ btatEventNdx ] = rBtatEvents.arrBtatEvents_[ btatEventNdx ];
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BtatEventState
//
//@ Interface-Description
//  Destructor.  Not implemented.
//---------------------------------------------------------------------
//@ Implementation-Description
//  N/A
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

BtatEventState::~BtatEventState( void ) 
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//  This method take a reference to BtatEventState object argument and
//  returns no value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method checks to be sure it is not copying itself.  If the
//  reference is not itself then it copies the contents of the
//  passed in reference to this object.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void 
BtatEventState::operator=( const BtatEventState& rBtatEvents )
{
    if ( this != &rBtatEvents )
    {
    // $[TI1]
        for ( Uint8 btatEventNdx = 0; btatEventNdx < MAX_BTAT_EVENTS; ++btatEventNdx )
        {
            arrBtatEvents_[ btatEventNdx ] = rBtatEvents.arrBtatEvents_[ btatEventNdx ];
        }
    }
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  findEntry
//
//@ Interface-Description
//  This method accept an enumeration of type OperatingParameterEventName
//  and returns an Int16.
//  This utility searches the BTAT array for the specified alarm type
//  and returns the location index of the alarm type when found,
//  otherwise a not found status is returned to its client.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Performs a sequential search through arrBtatEvents_ to find a match
//  for the specified alarm type.  If a match is found, the location
//  index of arrBtatEvents_ is returned, otherwise, a not found status
//  is returned to the client.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

Int16 
BtatEventState::findEntry( const OperatingParameterEventName entry ) const
{
    Int16 rtnEntry = NOT_FOUND;

    for (Uint8 btatEventNdx = 0; btatEventNdx < MAX_BTAT_EVENTS; ++btatEventNdx)
    {
        if ( arrBtatEvents_[ btatEventNdx ] == entry )
        {
        // $[TI1]
            rtnEntry = btatEventNdx;
            break;
        }
        // $[TI2]
    }

    return( rtnEntry );
}



#if defined( SIGMA_DEBUG )

#include <stdio.h>

void
BtatEventState::printBtat( void )
{
    for ( Uint8 btatNdx = 0; btatNdx < MAX_BTAT_EVENTS; ++btatNdx )
    {
        switch ( arrBtatEvents_[ btatNdx ] )
        {
            case BTAT_1_EN: 
                printf("BTAT_1_EN.\n");
                break;

            case BTAT_2_EN: 
                printf("BTAT_2_EN.\n");
                break;

            case BTAT_3_EN: 
                printf("BTAT_3_EN.\n");
                break;

            case BTAT_4_EN: 
                printf("BTAT_4_EN.\n");
                break;

            case NOT_BTAT_4_EN: 
                printf("BTAT_4_EN.\n");
                break;

            case BTAT_5_EN: 
                printf("BTAT_5_EN.\n");
                break;

            case BTAT_6_EN: 
                printf("BTAT_6_EN.\n");
                break;

            case NOT_BTAT_6_EN: 
                printf("NOT_BTAT_6_EN.\n");
                break;

            case BTAT_7_EN: 
                printf("BTAT_7_EN.\n");
                break;

            case NOT_BTAT_7_EN: 
                printf("NOT_BTAT_7_EN.\n");
                break;

            case BTAT_8_EN: 
                printf("BTAT_8_EN.\n");
                break;

            case NOT_BTAT_8_EN: 
                printf("NOT_BTAT_8_EN.\n");
                break;

            case BTAT_9_EN: 
                printf("BTAT_9_EN.\n");
                break;

            case BTAT_10_EN: 
                printf("BTAT_10_EN.\n");
                break;

            case BTAT_11_EN: 
                printf("BTAT_11_EN.\n");
                break;

            case BTAT_12_EN: 
                printf("BTAT_12_EN.\n");
                break;

            case BTAT_13_EN: 
                printf("BTAT_13_EN.\n");
                break;

            case BTAT_14_EN: 
                printf("BTAT_14_EN.\n");
                break;

            case BTAT_15_EN: 
                printf("BTAT_15_EN.\n");
                break;

            case BTAT_16_EN: 
                printf("BTAT_16_EN.\n");
                break;

            case BTAT_17_EN: 
                printf("BTAT_17_EN.\n");
                break;
          
            default:
                break;

        }
    }
}
#endif // defined( SIGMA_DEBUG )


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
BtatEventState::SoftFault( const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate )
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

    FaultHandler::SoftFault( softFaultID, SAFETY_NET, BTATEVENTSTATE,
                             lineNumber, pFileName, pPredicate);
}
