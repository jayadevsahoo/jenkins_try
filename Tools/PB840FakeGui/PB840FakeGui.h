
// PB840FakeGui.h : main header file for the PB840FakeGui application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CPB840FakeGuiApp:
// See PB840FakeGui.cpp for the implementation of this class
//

class CPB840FakeGuiApp : public CWinAppEx
{
public:
	CPB840FakeGuiApp();

	enum {
		XactionThread,
		NetworkAppThread
	};


// Overrides
public:
	virtual BOOL InitInstance();
	virtual BOOL ExitInstance();

	BOOL PostWorkerThreadMessage(Uint32 thread, Uint32 msg, WPARAM w, LPARAM l);

// Implementation
	BOOL  m_bHiColorIcons;

	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();

	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()

private:
	CWinThread* ThreadSettingXaction_;
	CWinThread* ThreadNetworkApp_;
};

extern CPB840FakeGuiApp theApp;


UINT SettingXactionThreadProc( LPVOID pParam );
UINT NetworkAppThreadProc( LPVOID pParam );