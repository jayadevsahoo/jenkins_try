#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendSelectMenuItems -  Trend parameter selection menu items
//---------------------------------------------------------------------
//@ Interface-Description
//  TrendSelectMenuItems is a SettingMenuItems class that provides the
//  trend parameter selection menu strings displayed by the
//  DiscreteSettingButton and the ScrollableMenu classes.
// 
//  TrendSelectMenuItems provides a single static method
//  GetTrendSelectMenuItems() that instantiates a single instance of
//  this class and returns a reference to it. During construction, this
//  class initializes the static DiscreteSettingButton::ValueInfo data
//  structure containing a single header and an array of strings for
//  each trend parameter setting value.
// 
//  This ValueInfo structure is referenced by DiscreteSettingButton
//  through the SettingMenuItems::getValueInfo method that returns a
//  reference to this static ValueInfo structure.
// 
//  The ScrollableMenu class retrieves the menu strings defined in
//  ValueInfo using the SettingMenuItems::getLogEntryInfo() method.
//---------------------------------------------------------------------
//@ Rationale
//	Needed to store and maintain trending parameter menu data.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  A static method GetTrendSelectMenuItems instantiates a single
//  instance of this class and returns a reference to it. The class
//  constructor is private so only this class can construct and
//  instance.
//---------------------------------------------------------------------
//@ Fault-Handling 
//	none 
//---------------------------------------------------------------------
//@ Restrictions 
//	none 
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendSelectMenuItems.ccv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: rpr    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 001  By: gdc    Date: 10-Jan-2007    SCR Number: 6237
//  Project:  TREND
//  Description:
//		Initial version.
//=====================================================================

#include "TrendSelectMenuItems.hh"
#include "DiscreteSettingButton.hh"
#include "TrendSelectValue.hh"
#include "MiscStrs.hh"

static Uint TrendMenuInfoMemory_[sizeof(DiscreteSettingButton::ValueInfo) +
								 sizeof(StringId) * (TrendSelectValue::TOTAL_TREND_SELECT_VALUES - 1)];

static DiscreteSettingButton::ValueInfo*  PMenuInfo_ =
	(DiscreteSettingButton::ValueInfo*)::TrendMenuInfoMemory_;

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendSelectMenuItems()  [private, constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the static DiscreteSettingButton::ValueInfo structure
//  with the strings defined for each trend parameter selection 
//  menu entry.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendSelectMenuItems::TrendSelectMenuItems(void)
:	SettingMenuItems(PMenuInfo_)
{
	PMenuInfo_->numValues = TrendSelectValue::TOTAL_TREND_SELECT_VALUES;

	PMenuInfo_->arrValues[TrendSelectValue::TREND_EMPTY] = MiscStrs::TREND_EMPTY_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_DYNAMIC_COMPLIANCE] = MiscStrs::TREND_DYNAMIC_COMPLIANCE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PAV_COMPLIANCE] = MiscStrs::TREND_PAV_COMPLIANCE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_STATIC_LUNG_COMPLIANCE] = MiscStrs::TREND_STATIC_LUNG_COMPLIANCE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PAV_ELASTANCE] = MiscStrs::TREND_PAV_ELASTANCE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_END_EXPIRATORY_FLOW] = MiscStrs::TREND_END_EXPIRATORY_FLOW_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PEAK_EXPIRATORY_FLOW_RATE] = MiscStrs::TREND_PEAK_EXPIRATORY_FLOW_RATE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PEAK_SPONTANEOUS_FLOW_RATE] = MiscStrs::TREND_PEAK_SPONTANEOUS_FLOW_RATE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_TOTAL_RESPIRATORY_RATE] = MiscStrs::TREND_TOTAL_RESPIRATORY_RATE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_I_E_RATIO] = MiscStrs::TREND_I_E_RATIO_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_NEGATIVE_INSPIRATORY_FORCE] = MiscStrs::TREND_NEGATIVE_INSPIRATORY_FORCE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_OXYGEN_PERCENTAGE] = MiscStrs::TREND_OXYGEN_PERCENTAGE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_P100] = MiscStrs::TREND_P100_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_END_EXPIRATORY_PRESSURE] = MiscStrs::TREND_END_EXPIRATORY_PRESSURE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_INTRINSIC_PEEP] = MiscStrs::TREND_INTRINSIC_PEEP_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PAV_INTRINSIC_PEEP] = MiscStrs::TREND_PAV_INTRINSIC_PEEP_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_TOTAL_PEEP] = MiscStrs::TREND_TOTAL_PEEP_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_MEAN_CIRCUIT_PRESSURE] = MiscStrs::TREND_MEAN_CIRCUIT_PRESSURE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PEAK_CIRCUIT_PRESSURE] = MiscStrs::TREND_PEAK_CIRCUIT_PRESSURE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PLATEAU_PRESSURE] = MiscStrs::TREND_PLATEAU_PRESSURE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SPONTANEOUS_RAPID_SHALLOW_BREATHING_INDEX] = MiscStrs::TREND_SPONTANEOUS_RAPID_SHALLOW_BREATHING_INDEX_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_DYNAMIC_RESISTANCE] = MiscStrs::TREND_DYNAMIC_RESISTANCE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PAV_RESISTANCE] = MiscStrs::TREND_PAV_RESISTANCE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_STATIC_AIRWAY_RESISTANCE] = MiscStrs::TREND_STATIC_AIRWAY_RESISTANCE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PAV_TOTAL_AIRWAY_RESISTANCE] = MiscStrs::TREND_PAV_TOTAL_AIRWAY_RESISTANCE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SPONTANEOUS_INSPIRATORY_TIME] = MiscStrs::TREND_SPONTANEOUS_INSPIRATORY_TIME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SPONTANEOUS_INSPIRATORY_TIME_RATIO] = MiscStrs::TREND_SPONTANEOUS_INSPIRATORY_TIME_RATIO_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_VITAL_CAPACITY] = MiscStrs::TREND_VITAL_CAPACITY_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_EXHALED_MINUTE_VOLUME] = MiscStrs::TREND_EXHALED_MINUTE_VOLUME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_EXHALED_SPONTANEOUS_MINUTE_VOLUME] = MiscStrs::TREND_EXHALED_SPONTANEOUS_MINUTE_VOLUME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_EXHALED_TIDAL_VOLUME] = MiscStrs::TREND_EXHALED_TIDAL_VOLUME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_EXHALED_SPONTANEOUS_TIDAL_VOLUME] = MiscStrs::TREND_EXHALED_SPONTANEOUS_TIDAL_VOLUME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_EXHALED_MANDATORY_TIDAL_VOLUME] = MiscStrs::TREND_EXHALED_MANDATORY_TIDAL_VOLUME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_INSPIRED_TIDAL_VOLUME] = MiscStrs::TREND_INSPIRED_TIDAL_VOLUME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_TOTAL_WORK_OF_BREATHING] = MiscStrs::TREND_TOTAL_WORK_OF_BREATHING_LABEL ;

	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_EXPIRATORY_SENSITIVITY] = MiscStrs::TREND_SET_EXPIRATORY_SENSITIVITY_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_RESPIRATORY_RATE] = MiscStrs::TREND_SET_RESPIRATORY_RATE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_PEAK_INSPIRATORY_FLOW] = MiscStrs::TREND_SET_PEAK_INSPIRATORY_FLOW_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_I_E_RATIO] = MiscStrs::TREND_SET_I_E_RATIO_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_OXYGEN_PERCENTAGE] = MiscStrs::TREND_SET_OXYGEN_PERCENTAGE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_PERCENT_SUPPORT] = MiscStrs::TREND_SET_PERCENT_SUPPORT_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_PEEP] = MiscStrs::TREND_SET_PEEP_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_PEEP_HIGH] = MiscStrs::TREND_SET_PEEP_HIGH_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_PEEP_LOW] = MiscStrs::TREND_SET_PEEP_LOW_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_INSPIRATORY_PRESSURE] = MiscStrs::TREND_SET_INSPIRATORY_PRESSURE_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_PRESSURE_SUPPORT_LEVEL] = MiscStrs::TREND_SET_PRESSURE_SUPPORT_LEVEL_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_FLOW_ACCELERATION_OVER_RISE_TIME] = MiscStrs::TREND_SET_FLOW_ACCELERATION_OVER_RISE_TIME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_FLOW_SENSITIVITY] = MiscStrs::TREND_SET_FLOW_SENSITIVITY_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_PRESSURE_SENSITIVITY] = MiscStrs::TREND_SET_PRESSURE_SENSITIVITY_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_PEEP_HIGH_TIME] = MiscStrs::TREND_SET_PEEP_HIGH_TIME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_TH_TL_RATIO] = MiscStrs::TREND_SET_TH_TL_RATIO_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_INSPIRATORY_TIME] = MiscStrs::TREND_SET_INSPIRATORY_TIME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_PEEP_LOW_TIME] = MiscStrs::TREND_SET_PEEP_LOW_TIME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_TIDAL_VOLUME] = MiscStrs::TREND_SET_TIDAL_VOLUME_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_SET_VOLUME_SUPPORT_LEVEL_VS] = MiscStrs::TREND_SET_VOLUME_SUPPORT_LEVEL_VS_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_PERCENT_LEAK] = MiscStrs::TREND_PERCENT_LEAK_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_LEAK] = MiscStrs::TREND_LEAK_LABEL ;
	PMenuInfo_->arrValues[TrendSelectValue::TREND_ILEAK] = MiscStrs::TREND_ILEAK_LABEL ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendSelectMenuItems()  [private,destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendSelectMenuItems::~TrendSelectMenuItems(void)
{
	CALL_TRACE("TrendSelectMenuItems::~TrendSelectMenuItems(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendSelectMenuItems
//
//@ Interface-Description
//	Constructs static instance of TrendSelectMenuItems and returns a
//	reference to it.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendSelectMenuItems& TrendSelectMenuItems::GetMenuItems(void)
{
	static TrendSelectMenuItems MenuItems_;

	return MenuItems_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
	TrendSelectMenuItems::SoftFault(const SoftFaultID  softFaultID,
									const Uint32       lineNumber,
									const char*        pFileName,
									const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDSELECTMENUITEMS,
							lineNumber, pFileName, pPredicate);
}
