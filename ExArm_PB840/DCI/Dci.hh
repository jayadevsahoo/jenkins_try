#ifndef	Dci_HH
# define Dci_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// ====================================================================
//@ Class: Dci - Sigma DCI Communications class
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/DCI/vcssrc/Dci.hhv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 019   By: mnr    Date: 15-Apr-2010    SCR Number: 6436  
//  Project: PROX
//  Description: 
//      encodeProxNormalFault_() added.
// 
//  Revision: 018   By: mnr    Date: 03-Mar-2010    SCR Number: 6436  
//  Project: PROX
//  Description: 
//      Modified to support PROX state (ON/OFF) and PROX alarm (FAULT/NORMAL).
// 
//  Revision: 017   By: mnr    Date: 24-Feb-2010    SCR Number: 6436  
//  Project: PROX
//  Description:
//      Modified to support PROX state (ON/OFF/FAULT).
//
//  Revision: 016    By: mnr   Date: 23-Feb-2010    SCR Number: 6556
//  Project: NEO
//  Description:
//  	Added isPhilips_() method.
//
//  Revision: 015   By: rhj    Date: 14-Oct-2009    SCR Number: 6544  
//  Project: XB2
//  Description:
//      Modified Leak Compensation enabled values from Enabled/Disabled to
//     ON/OFF.
//
//  Revision: 014   By: rpr    Date: 22-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support code review comments.
// 
//  Revision: 013   By: rpr    Date: 11-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 012   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 011   By: rhj   Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//        Removed C20/C and changed it as a reserve.
//
//  Revision: 010    By: rhj              Date: 29-Nov-2006  DR Number: 6301
//  Project: RESPM
//  Description:
//      Fixed Cstat and Rstat in the SNDA report and removed WOBtot,
//      WOBr, WOBe, and WOBpt.
//
//  Revision: 009    By: rhj              Date: 11-Jul-2006  DR Number: 5928
//  Project: RESPM
//  Description:
//      Added support for the new command SNDF, which generates 
//      a MISCF Report.
//
//  Revision: 008    By: ljs              Date: 8-Dec-2003  DR Number: 6127
//  Project: Baseline
//  Description:
//      Modified to make Serial Communications more robust by:
//      - Log error into NOVRAM if it was the first error of that type,
//        or if using an Engineering/Production DataKey.
//
//  Revision: 007    By: quf           Date: 26-JUN-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//  Revision: 006    By: hct           Date: 19-OCT-2000  DR Number: 5757
//  Project:  Baseline
//  Description:
//      Added functionality to support Marquette monitors.
//
//  Revision: 005    By: hct           Date: 15-DEC-1998  DR Number: 5292, 5314
//  Project: BiLevel
//  Description:
//      Add functionality to zero settings which are inapplicable for the
//      current mandatory type.
//
//  Revision: 004    By: hct              Date: 03-MAR-1998  DR Number: N/A
//  Project: BiLevel
//  Description:
//      Added Bilevel functionality.
//
//  Revision: 003    By: Gary Cederquist  Date: 11-DEC-1997  DR Number: 2640
//                                                           DR Number: 2674
//  Project: Sigma (R8027)
//  Description:
//      (DCS 2640) Added getPatientId_() to generate patient ID field 
//      containing the ventilator's serial number.
//      (DCS 2674) Added correctedEofIeRatio_() to generate compensated
//      E value when I>E for field 41.
//
//  Revision: 002    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//  Revision  1.0	By: jdm	Date: 7/26/95	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

# include "Sigma.hh"

//@ Usage-Classes
# include "BatchSettingValues.hh"	// for batch settings...
# include "DciClassIds.hh"
# include "DciDebug.hh"
# include "DciNumber.hh"
# include "DciString.hh"

# include "AlarmDci.hh"
# include "FaultHandler.hh"
# include "ModeValue.hh"
# include "SettingId.hh"

# include "SerialInterface.hh"

#include "VentTypeValue.hh"
#include "SettingsMgr.hh"
#include "Setting.hh"
#include "TubeTypeValue.hh"
#include "SupportTypeValue.hh"
#include "TriggerTypeValue.hh"
#include "MandTypeValue.hh"
#include "HumidTypeValue.hh"
#include "Fio2EnabledValue.hh"

#include "BatchBoundedSetting.hh"
#include "PatientCctTypeValue.hh"
#include "PatientDataTarget.hh"
#include "PatientDataRegistrar.hh"


//@ End-Usage

union DciQueueMessage;

				
class Dci : public PatientDataTarget
{
  public:
	Dci(void);
	~Dci(void);

	// magic numbers
	// NB	enumerated since they are used to allocate space
	enum
	{
		INPUT_BUFFER_SIZE = 400,
		REAL_INPUT_BUFFER_SIZE = (INPUT_BUFFER_SIZE+5),
		MAX_REPORT_LEN = 4000,
		SCRATCH_BUFFER_SIZE = 80,
		MISCA_FIELD_COUNT = (97 + 2 + 1)
	};

	// error codes for DCI error report
	enum	DciErrorCode
	{
		PARITY_ERROR = 7081,
		INPUT_BUFFER_OVERFLOW_ERROR = 7088,
		NON_SPECIFIC_ERROR = 7031,

		// should be last
		NUMBER_OF_DCI_ERRORS = 3
	};

	// structure definitions used in private definitions
	// and global declarations (need to be public)

	struct	DciAlarms	// Alarm data
	{
		DciString	highInspPres;		      // NORMAL, ALARM, RESET
		DciString	lowInspPres;		      // NORMAL, LOW, MEDIUM, HIGH, RESET
		DciString	lowPeepCpapPres;	      // --- not supported
		DciString	lowTidalVolume;		      // NORMAL, ALARM, RESET
		DciString	lowMinuteVolume;	      // NORMAL, ALARM, RESET
		DciString	highRespRate;		      // NORMAL, ALARM, RESET
		DciString	lowO2SupplyPres;	      // NORMAL, ALARM, RESET
		DciString	lowAirSupplyPres;	      // NORMAL, ALARM, RESET
		DciString	lowBattery;		          // Low Battery
		DciString	apnea;			          // NORMAL, ALARM, RESET
		DciString	ieRatio;		          // --- not supported
		DciString	exhValveLeak;		      // --- not supported
		DciString	lowO2Saturation;	      // --- not supported
		DciString	highO2Saturation;	      // --- not supported
		DciString	lowPulseRate;		      // --- not supported
		DciString	highPulseRate;		      // --- not supported
		DciString	oximeterStatus;		      // --- not supported "NOOP C"
		DciString	silence;		          // Alarm Silence ON, OFF
		DciString	apneaVentilation;         // Apnea Ventilation 
		DciString	disconnect;		          // Disconnect 
        DciString   occlusion;                // Occlusion

        DciString   highMinuteVolume;         // High Exhaled Minute Volume Limit
        DciString   highExhaledVolume;        // High Exhaled Tidal Volume Limit
        DciString   highO2Percent;            // High O2 Percent
        DciString   pVent;                    // High Ventilator Pressure 
        DciString   acPowerLoss;              // AC Power Loss
        DciString   inoperativeBattery;       // Inoperative Battery
        DciString   lowACPower;               // Low AC Power
        DciString   lowExhaledMandVolume;     // Low Exhaled Mandatory Tidal Volume Limit
        DciString   lowExhaledSpontVolume;    // Low Exhaled Spont Tidal Volume Limit
        DciString   compressorInop;           // Compressor Inoperative
        DciString   procedureError;           // Procedure Error
        DciString   volumeLimit;              // Volume Limit
        DciString   highInspiredSpontVolume;  // High Inspired Spont Volume Limit
        DciString   highInspiredMandVolume;   // High Inspired Mand Volume Limit
        DciString   highCompensationLimit;    // High Compensation Limit
        DciString   pavStartupTooLong;        // PAV Startup too long
        DciString   pavRCnotAssessed;         // PAV R & C not assessed
        DciString   volumeNotDeliveredVcPlus; // Volume not delivered (VC+)
        DciString   volumeNotDeliveredVS;     // Volume not delivered (VS)
        DciString   lowO2Percent;             // Low O2 Percent
        DciString   inspirationTooLong;       // Inspiration too long

        DciString   btat1;                    // Technical Malfunction (A5)  BTAT1
        DciString   btat2;                    // Technical Malfunction (A10) BTAT2
        DciString   btat3;                    // Technical Malfunction (A15) BTAT3
        DciString   btat4;                    // Technical Malfunction (A20) BTAT4
        DciString   btat5;                    // Technical Malfunction (A25) BTAT5
        DciString   btat6;                    // Technical Malfunction (A30) BTAT6
        DciString   btat7;                    // Technical Malfunction (A35) BTAT7
        DciString   btat8;                    // Technical Malfunction (A40) BTAT8
        DciString   btat9;                    // Technical Malfunction (A45) BTAT9
        DciString   btat10;                   // Technical Malfunction (A50) BTAT10
        DciString   btat11;                   // Technical Malfunction (A55) BTAT11
        DciString   btat12;                   // Technical Malfunction (A60) BTAT12
        DciString   btat13;                   // Technical Malfunction (A65) BTAT13
        DciString   btat14;                   // Technical Malfunction (A70) BTAT14
        DciString   btat15;                   // Technical Malfunction (A75) BTAT15
        DciString   btat16;                   // Technical Malfunction (A80) BTAT16
        DciString   btat17;                   // Technical Malfunction (A85) BTAT17
		DciString   proxAlarm;	 			  // Prox Alarm status
	};


	struct	DciMisc		// miscellaneous data
	{
		DciString	blank;			// blank
		DciString	softwareRevNum;	// XXXXX-XX-XXX
		DciString	estResult;		// PASSED or FAILED
		DciString	time;			// HH:MM
		DciString	patientId;		// --- not in release 1
		DciString	patientRoom;		// --- not in release 1
		DciString	date;			// MMM DD YYYY
		DciNumber	practitionerId;		// --- not in release 1
	};

	struct	DciPatient	// Patient data
	{
		DciNumber	respRate;		// bpm
		DciNumber	tidalVolume;		// liters
		DciNumber	minuteVolume;		// liters
		DciNumber	spontMinuteVolume;	// liters
		DciNumber	peakAirwayPres;		// cmH2O
		DciNumber	meanAirwayPres;		// cmH2O
		DciNumber	plateauPres;		// cmH2O
		DciNumber	o2Saturation;		// --- not in release 1
		DciNumber	pulseRate;		// --- not in release 1
		DciNumber	ieRatio;		// make E=1, if necessary
		DciNumber	ePortion;		// e portion of ieRatio

        DciNumber   deliveredO2; // Delivered O2 % to patient
        DciNumber   Vti;         // Delivered Volume (Liters)
        DciNumber   PEEPi;       // Estimated Intrinsic PEEP (cm/H20)
		DciNumber   WOBtot;		 // WOBtot
        DciNumber   Rtot;        // Estimated Total Resistance (cm/H2O)/ (L/S)
        DciNumber   Rpav;        // Estimated patient Resistance (cm/H2O)/ (L/S)
        DciNumber   Epav;        // Estimated Lung elastance (cm/H2O) /L
        DciNumber   Cpav;        // Estimated Lung Compliance (mL/cmH2O)
        DciNumber   spontInspPercent; // Spont Inspiratory % (Ti/Ttot)
        DciNumber   rapShadowIndx;    // Rapid/Shadow Breathing Index (F/Vt)
        DciNumber   normRapShadowIndx;// Normalized Rapid/Shadow Breathing Index (F/Vt/kg)
        DciNumber   endExhPress;        // Monitored end exp. pressure
        DciNumber   Vte;         // Exhaled Tidal Volume (mL)
        DciNumber   VeTot;       // Exhaled Minute Volume (L/min)
        DciNumber   TiSpont;     // Spontaneous Inspiratory Time (Sec)
        DciNumber   VeSpont;     // Exhaled Spontaneous Minute Volume (L/min)
		DciNumber   VteSpont;     // Exhaled Spontaneous Tidal Volume (L/Tidal)
        DciNumber   PEEP_I;      // Peep I   (Exp Pause)
        DciNumber   PEEP_Tot;    // Peep Tot (Exp Pause)
        DciNumber   Cstat;       // Static lung compliance (INSP PAUSE)
        DciNumber   Rstat;       // Static airway resistance (INSP PAUSE)
        DciNumber   Ppl;         // Plateau pressure (INSP PAUSE)
        DciString   highTiSpont; // High spontaneous inspiratory time
        DciNumber   Cdyn;        // Dynamic Compliance
        DciNumber   Rdyn;        // Dynamic Resistance
        DciNumber   PSF;         // Peak Spontaneous Flow
        DciNumber   PEF;         // Peak Expiratory Flow
        DciNumber   EEF;         // End Expiratory Flow
        DciNumber   NIF;         // Negative Inspiratory Force
        DciNumber   P100;        // P0.1 Pressure Change
        DciNumber   VC;          // Vital Capacity
		DciNumber   percentLeak; // %Leak (%)
		DciNumber   leak;		 // Leak Volume, Leak (L/min)
		DciNumber   iLeak;		 // Inspiratory Leak, iLeak (mL)
	};

	struct	DciSettings	// SNDA settings data
	{
		DciString	mode;			// CMV, SIMV, CPAP
		DciNumber	respRate;		// bpm
		DciNumber	tidalVolume;		// liters
		DciNumber	peakFlow;		// lpm
		DciNumber	o2;			// %
		DciNumber	presSensitivity;	// cmH2O
		DciNumber	peepCpap;		// cmH2O
		DciNumber	plateauTime;		// sec
		DciNumber	sighRate;		// --- not supported
		DciNumber	sighTidalVolume;	// --- not supported
		DciNumber	multipleSighCount;	// --- not supported
		DciNumber	sighHPL;		// --- not supported
		DciNumber	apneaInterval;		// sec
		DciNumber	apneaTidalVolume;	// liters
		DciNumber	apneaRespRate;		// bpm
		DciNumber	apneaPeakFlow;		// lpm
		DciNumber	apneaO2;		// %
		DciNumber	supportPres;		// cmH2O
		DciString	waveform;		// SQUARE, RAMP,SINE
		DciString	autoSighFeature;	// --- not supported
		DciString	nebulizerFeature;	// --- not supported
		DciString	suctionO2;		// ON, OFF
		DciNumber	printInterval;		// --- not supported
		DciNumber	dataAvgInterval;	// --- not supported
		DciNumber	chartInterval;		// --- not supported
		DciNumber	highInspPres;		// cmH2O
		DciNumber	lowInspPres;		// cmH20
		DciNumber	lowPeepCpap;		// --- not supported
		DciNumber	lowTidalVolume;		// liters
		DciNumber	lowMinuteVolume;	// liters
		DciNumber	highRespRate;		// bpm
		DciNumber	baseFlow;		// lpm
		DciNumber	flowSensitivity;	// lpm
		DciNumber	lowO2Saturation;	// --- not supported
		DciNumber	highO2Saturation;	// --- not supported
		DciNumber	lowPulseRate;		// --- not supported
		DciNumber	highPulseRate;		// --- not supported
		DciNumber	oximeterAvgInterval;	// --- not supported
		DciNumber	pcvControlPres;		// cmH2O
		DciNumber	pcvInspTime;		// sec
		DciNumber	pcvApneaControlPres;	// bpm
		DciNumber	pcvApneaInspTime;	// sec
		DciNumber	pcvIofIeRatio;		// inh portion of ratio
		DciNumber	pcvEofIeRatio;		// exh portion of ratio
		DciNumber	apneaPcvIofIeRatio;	// inh portion of ratio
		DciNumber	apneaPcvEofIeRatio;	// exh portion of ratio
		DciString	pcvControl;		// I-TIME, I/E

	};


	struct	DciSettingsF	         // SNDF settings data
	{
		DciString	mode;			 // A/C, SIMV, SPONT, BILEVEL
		DciString	respRate;		 // bpm
		DciString	tidalVolume;	 // liters
		DciString	peakFlow;		 // lpm
		DciString	o2;			     // %
		DciString	presSensitivity; // cmH2O
		DciString	peepCpap;		 // cmH2O
		DciString	plateauTime;	 // sec
		DciString	apneaInterval;	 // sec
		DciString	apneaTidalVolume;// liters
		DciString	apneaRespRate;	 // bpm
		DciString	apneaPeakFlow;	 // lpm
		DciString	apneaO2;		 // %
		DciString	supportPres;	 // cmH2O
		DciString	waveform;		 // SQUARE, RAMP,SINE
		DciString	suctionO2;		 // ON, OFF
		DciString	lowInspPres;	 // cmH20
		DciString	lowTidalVolume;	 // liters
		DciString	lowMinuteVolume; // liters
		DciString	highRespRate;	 // bpm
		DciString	baseFlow;		 // lpm
		DciString	flowSensitivity;	// lpm
		DciString	pcvControlPres;		// cmH2O
		DciString	pcvInspTime;		// sec
		DciString	pcvApneaControlPres;	// bpm
		DciString	pcvApneaInspTime;	// sec
		DciString	pcvIofIeRatio;		// inh portion of ratio
		DciString	pcvEofIeRatio;		// exh portion of ratio
		DciString	apneaPcvIofIeRatio;	// inh portion of ratio
		DciString	apneaPcvEofIeRatio;	// exh portion of ratio
		DciString	pcvControl;		// I-TIME, I/E

        DciString   ventType;           // NIV, INVASIVE
        DciString   mandatoryType;      // PC, VC, VC+
        DciString   spontType;          // NONE, PS, TC, VS, PA
        DciString   TriggerType;        // V-Trig, P-Trig
        DciString   humidificationType; // Non-Heated Exp Tube, Heated Exp Tube,
                                        // HME
        DciString   humidificationVol;  // Humidification Volume (Liters)
        DciString   O2sensor;           // Enabled, Disabled
        DciString   DSens;              // Disconnect sensitivity.
        DciString   tubeId;             // Tube Id(mm)
        DciString   tubeType;           // Tube Type (ET, TRACH)
        DciString   riseTimePercent;    // Rise Time %
        DciString   percentSupport;     // Percent Support
        DciString   eSens;              // Spontaneous Expiratory Sensitivity %
        DciString   ibw;                // Ideal body weight  (Kg)
        DciString   volumeSupport;      // Target Support Volume (mL)

        DciString   apneaFlowPattern;   // Apnea Flow Pattern (SQUARE, RAMP,SINE)
        DciString   apneaMandatoryType; // Apnea Mandatory Type
        DciString   highPpeak;          // Set Alarm setting High Inspiratory Pressure
        DciString   lowPpeak;           // Set Alarm setting Low Inspiratory Pressure
        DciString   highVeTot;          // Set Alarm setting High Exhaled Minute Volume Limit (L/min)
        DciString   lowVeTot;           // Set Alarm setting Low Exhaled Minute Volume Limit  (L/min)
        DciString   highVteMand;        // Set Alarm setting High Exhaled Tidal Volume Limit  (mL)
        DciString   lowVteMand;         // Set Alarm Setting Low Exhaled Mandatory Tidal Volume Limit (mL)
        DciString   highVteSpont;       // Set Alarm setting High Exhaled Tidal Volume Limit  (mL)
        DciString   lowVteSpont;        // Set Alarm setting Low Exhaled Spont Tidal Volume Limit (mL)
        DciString   highFtot;           // Set Alarm setting High Respiratory rate
        DciString   highVti;            // Set Alarm setting High Vti 

        DciString   highPEEPtime;       // High PEEP Time 
        DciString   lowPEEPtime;        // Low PEEP Time 
        DciString   Te;                 // Expiratory Time       
        DciString   PEEPh;              // High PEEP cmH2O
        DciString   PEEPl;              // Low PEEP cmH2O
        DciString   highSpontInspTimeLimit; // High Spontaneous Inspiratory Time Limit
        DciString   circuitType;        // Circuit Type (ADULT, PEDIATRIC, NEONATAL)
		DciString   leakState;			// Leak Compensation (Enable/Disable)
		DciString   proxState; 		    // Prox (ON/OFF)
	};

	static		void	Initialize(void);
	static		Dci&	GetDci(SerialInterface::PortNum portNum);

	static	void	SoftFault(const SoftFaultID	softFaultID,
					const	Uint32	lineNumber,
					const	char *	pFileName = NULL,
					const	char *	pPredicate = NULL);

	void	generateSNDA(void);
    void    generateSNDF(void);
	void	generateSNID(void);
	void	generateSER(void);
	void	generateSLMT(void);
	void	error(const char * pString, DciErrorCode code, Boolean b_log = TRUE);

			// used by getBatchSettings_()
	static  const	char *	PBlankStr6_;
	static  const	char *	PCmvStr6_;
	static  const	char *	PSimvStr6_;
	static  const	char *	PCpapStr6_;
	static  const	char *	PBilevlStr6_;

    static  const   char *  PACStr6_;
    static  const   char *  PSpontStr6_;


	static  const	char *	PSquareStr6_;
	static  const	char *	PRampStr6_;
	static  const	char *	PNoopCStr6_;
	static  const	char *	PITimeStr6_;
	static  const	char *	PIeRatioStr6_;
    static  const   char *  PETimeStr6_;
    static  const   char *  PNIVStr9_;
    static  const   char *  PInvasiveStr9_;
    static  const   char *  PPTrigerTypeStr6_;
    static  const   char *  PVTrigerTypeStr6_; 
    
    static  const   char *  PPCStr6_;
    static  const   char *  PVCStr6_;
    static  const   char *  PVCPlusStr6_;
    
    static  const   char *  PNoneStr6_;
    static  const   char *  PPSStr6_;
    static  const   char *  PTCStr6_;
    static  const   char *  PVSStr6_;
    static  const   char *  PPAStr6_;
    static  const   char *  PETStr6_;
    static  const   char *  PTRACHStr6_;
    static  const   char *  PNonHeatedExpTubeStr18_;
    static  const   char *  PHeatedExpTubeStr18_;
    static  const   char *  PHMEStr18_;
    static  const   char *  PO2EnabledStr9_;
    static  const   char *  PO2DisabledStr9_;
    static  const   char *  PDSensOFFStr6_;
    static  const   char *  PLeakEnabledStr6_;
    static  const   char *  PLeakDisabledStr6_;
	static  const   char *  PProxFaultStr6_;

		// *******   in DciLanguage.cc...
		// used by reportHeader_()
	static  const	char *	DciAId_;
	static  const	char *	DciSNIDId_;
	static  const	char *	DciSLMTId_;
	static  const	char *	DciSERId_;

    static  const   char *  DciFId_;

			// used by getDate_()
	static	const	char	MonthNames_[12][4];



			// used by encodeOnOff_()
	static  const	char *	POffStr6_;
	static  const	char *	POnStr6_;

			// used by encodeNormalAlarmReset_()
	static  const	char *	PNormalStr6_;
	static  const	char *	PAlarmStr6_;
	static  const	char *	PResetStr6_;

			// used by generateSER_()
	static  const	char *	PPassStr6_;


    static const    char * PHighUrgencyAlarmStr6_ ;
    static const    char * PMediumUrgencyAlarmStr6_ ;
    static const    char * PLowUrgencyAlarmStr6_ ;

    static const   char *  PpediatricCircuitStr9_;
    static const   char *  PadultCircuitStr9_ ;
    static const   char *  PneonatalCircuitStr9_;

    static const   char *  PAlertStr6_;

  protected:

  private:

	Dci(const Dci &);	// not implemented...
	void	operator=(const Dci &);	// not implemented...

	void 	output_(const char * pBuffer);
	void	getSndAData_(void);
    void    getSndFData_(void);
	void	getMiscData_(void);

	Boolean isPhilips_(void);

    void    getMiscDataF_(void);
    void    getBatchSettingsF_(void);

	char *	getTime_(void);
	char *	getDate_(void);
	char *	getPatientId_(void);
	char *	getSoftwareRevNum_(void);
	Real32	getApplicableBoundedValue_(BatchSettingValues &settings,
					SettingId::SettingIdType settingId);
	Boolean	isDiscreteValueApplicable_(BatchSettingValues &settings,
					SettingId::SettingIdType settingId);
	void	getBatchSettings_(void);
	char *	encodeOnOff_(DciString * pDciString,
					const Boolean status);
	Real32	computeIofIeRatio_(const Real32 ieRatio);
	Real32	computeEofIeRatio_(const Real32 ieRatio);
	Real32	correctedEofIeRatio_(const Real32 ieRatio);
	void	getPatientData_(void);
    void    getPatientDataF_(void);
	void	getAlarmStatus_(void);
    void    getAlarmStatusF_(void);

	char *	encodeNormaldciAlarm_(DciString * pDciString,
					const AlarmDci::AlarmDciStatus status);
    char *	encodeNormalAlarmReset_(DciString * pDciString,
					const AlarmDci::AlarmDciStatus status);
	char *  encodeNormalAlarmResetF_(DciString * pDciString,
                    const AlarmDci::AlarmDciStatus status);
	char *	encodeProxNormalFault_(DciString * pDciString,
					const AlarmDci::AlarmDciStatus status);


    char *  encodeSettingValues_(DciString * pDciString,
                    const Real32 value, const Precision precision);

    Real32  getApplicableBoundedValueF_(BatchSettingValues &settings, 
                    SettingId::SettingIdType settingId);

    // inherited from PatientDataTarget
    virtual void patientDataChangeHappened(
                     PatientDataId::PatientItemId patientDataId);

		// our copy of vent operational mode from BatchSettingValues
	ModeValue::ModeValueId ventMode_;

	// buffer filled with SNDA Alarm Analysis report data
	DciAlarms dciAlarm_;

	// buffer filled with SNDA miscellaneous report data
	DciMisc	dciMisc_;

	// buffer filled with SNDA Patient Data report data
	DciPatient dciPatient_;

	// buffer filled with SNDA Batch Settings report data
	DciSettings dciSetting_;


    // buffer filled with SNDF Alarm Analysis report data.
    DciAlarms dciAlarmF_;

    // buffer filled with SNDF miscellaneous report data
    DciMisc dciMiscF_;

    // buffer filled with SNDF Patient Data report data
    DciPatient dciPatientF_;

    // buffer filled with SNDF Batch Settings report data
    DciSettingsF dciSettingF_;

    // isNivInspTooLong_ -- set TRUE when patient-data received 
    // indicating NIV spont breath was truncated for exceeding 
    // the High Ti spont limit setting
    Boolean isNivInspTooLong_;

    // previousFIO2_IS_ENABLED -- save last enable/diable status of FIO2 sensor
	DiscreteValue previousFIO2_IS_ENABLED;

		// special ASCII character constants
	static	const	char	CR;		// control-M
	static	const	char	FIELD_DELIMITER;// comma


		// *******   in DciReport.cc...
	void	reportHeader_(const char* reportId, const char* header);
	void	reportATrailer_(void);
	void	reportSNIDTrailer_(void);
	void	reportSLMTTrailer_(void);
	void	reportSERTrailer_(void);
	void	initReport_(void);
	void	appendStringToReport_(const char * pString);
	void	appendNoCommaStringToReport_(const char * pString);
	void	float61_(void * pData);
	void	float62_(void * pData);
    void    float63_(void * pData);
	void	ieRatio62_(void * pData);
	void	int6_(void * pData);
	void	string2_(void * pData);
	void	string3_(void * pData);
	void	string4_(void * pData);
	void	string5_(void * pData);
	void	string6_(void * pData);
	void	string9_(void * pData);
	void	string12_(void * pData);
	void	string16_(void * pData);
	void	string18_(void * pData);

		// report generation output index, buffer,working buffer,
		// and flag for detecting control characters
	Int32	nextOut_;		// next output fill loc
	char	outputBuffer_[MAX_REPORT_LEN];
	char	scratchBuffer_[SCRATCH_BUFFER_SIZE];
	char    timeBuffer_[7];         // "HH:MM "
	char    dateBuffer_[13];        // "MMM DD YYYY "
	char    patientIdBuffer_[19];   // "840 XXXXXXXXXXXXXX"
	Boolean	ctrlChar_;
	Boolean isMarquette_;

        // The serial port associated with this instantiation of Dci.
	SerialInterface::PortNum portNum_;
  
};


// inlined methods...
#include "Dci.in"

#endif	// Dci_HH
