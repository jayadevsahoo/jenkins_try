//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Convert.h
//----------------------------------------------------------------------------


#ifndef CONVERT_H
#define CONVERT_H

#include"EpochTimeStamp.h"
#include"BreathDataPacket.hh"
#include"VtpcvStateDataPacket.hh"
#include"PavStartupDataPacket.hh"
#include"PavBreathDataPacket.hh"
#include"PavPlateauDataPacket.hh"



inline float convert(float x)
{
    union{
        float f;
        unsigned int i;
    } f;
    f.f = x;
    f.i = ntohl(f.i);

    return f.f;
}

/// @brief convert BreathType to string value.
/// 
/// @param x BreathType
/// 
/// @return  charactor string describing BreathType.
inline const char * convert(BreathType &x)
{

    switch(ntohl(x))
    {
        case CONTROL:                                  return "CONTROL";
        case ASSIST:                                   return "ASSIST";
        case SPONT:                                    return "SPONT";
        case NON_MEASURED:                             return "NON_MEASURED";
        default: return "";
    }
}

/// @brief convert PhaseType to string value.
/// 
/// @param x BreathPhaseType::PhaseType
/// 
/// @return charactor string describing PhaseType
inline const char * convert(BreathPhaseType::PhaseType &x)
{
    switch(ntohl(x))
    {
        case BreathPhaseType::EXHALATION:              return "EXHALATION";
        case BreathPhaseType::INSPIRATION:             return "INSPIRATION";
        case BreathPhaseType::NULL_INSPIRATION:        return "NULL_INSPIRATION";
        case BreathPhaseType::EXPIRATORY_PAUSE:        return "EXPIRATORY_PAUSE";
        case BreathPhaseType::INSPIRATORY_PAUSE:       return "INSPIRATORY_PAUSE";
        case BreathPhaseType::BILEVEL_PAUSE_PHASE:     return "BILEVEL_PAUSE_PHASE";
        case BreathPhaseType::PAV_INSPIRATORY_PAUSE:   return "PAV_INSPIRATORY_PAUSE";
        case BreathPhaseType::NIF_PAUSE:               return "NIF_PAUSE";
        case BreathPhaseType::P100_PAUSE:              return "P100_PAUSE";
        case BreathPhaseType::VITAL_CAPACITY_INSP:     return "VITAL_CAPACITY_INSP";
        case BreathPhaseType::NON_BREATHING:           return "NON_BREATHING";
        default: return "";
    };
}


/// @brief convertPhaseType
/// 
/// @param x float value
/// 
/// @return charactor string describing PhaseType
inline const char * convertPhaseType(float x)
{

    union
    {
        float f;
        unsigned int i;
    } f;

    f.f = x;
    f.i = ntohl(f.i);

    static char buffer[100];

    switch( (int)f.f )
    {
        case BreathPhaseType::EXHALATION:              return "EXHALATION";
        case BreathPhaseType::INSPIRATION:             return "INSPIRATION";
        case BreathPhaseType::NULL_INSPIRATION:        return "NULL_INSPIRATION";
        case BreathPhaseType::EXPIRATORY_PAUSE:        return "EXPIRATORY_PAUSE";
        case BreathPhaseType::INSPIRATORY_PAUSE:       return "INSPIRATORY_PAUSE";
        case BreathPhaseType::BILEVEL_PAUSE_PHASE:     return "BILEVEL_PAUSE_PHASE";
        case BreathPhaseType::PAV_INSPIRATORY_PAUSE:   return "PAV_INSPIRATORY_PAUSE";
        case BreathPhaseType::NIF_PAUSE:               return "NIF_PAUSE";
        case BreathPhaseType::P100_PAUSE:              return "P100_PAUSE";
        case BreathPhaseType::VITAL_CAPACITY_INSP:     return "VITAL_CAPACITY_INSP";
        case BreathPhaseType::NON_BREATHING:           return "NON_BREATHING";
        default: 
            return buffer;
    };
}



/// @brief convert SupportTypeValueId
/// 
/// @param x SupportTypeValue::SupportTypeValueId
/// 
/// @return charactor string describing SupportTypeValue
inline const char * convert(SupportTypeValue::SupportTypeValueId &x)
{
    switch(ntohl(x))
    {
        //case SupportTypeValue::OFF_SUPPORT_TYPE:       return "OFF_SUPPORT_TYPE";
        case SupportTypeValue::PSV_SUPPORT_TYPE:       return "PSV_SUPPORT_TYPE";
        case SupportTypeValue::ATC_SUPPORT_TYPE:       return "ATC_SUPPORT_TYPE";
        case SupportTypeValue::VSV_SUPPORT_TYPE:       return "VSV_SUPPORT_TYPE";
        case SupportTypeValue::PAV_SUPPORT_TYPE:       return "PAV_SUPPORT_TYPE";
//TODO e600		case SupportTypeValue::PA_VS_SUPPORT_TYPE:	   return "PA_VS_SUPPORT_TYPE";
        default: return "";
    };
}

/// @brief convert
/// 
/// @param x MandTypeValue::MandTypeValueId 
/// 
/// @return charactor string describing MandTypeValue
inline const char * convert(MandTypeValue::MandTypeValueId &x)
{
    switch(ntohl(x))
    {
        case MandTypeValue::PCV_MAND_TYPE:             return "PCV_MAND_TYPE";
        case MandTypeValue::VCV_MAND_TYPE:             return "VCV_MAND_TYPE";
        case MandTypeValue::VCP_MAND_TYPE:             return "VCP_MAND_TYPE";
        default: return "";
    };
}

/// @brief convert
/// 
/// @param x Trigger::TriggerId
/// 
/// @return charactor string describing Trigger
inline const char * convert(Trigger::TriggerId &x)
{
    switch(ntohl(x))
    {
        case Trigger::NULL_TRIGGER_ID:                 return "NULL_TRIGGER_ID";                                       
        case Trigger::DELIVERED_FLOW_EXP:              return "DELIVERED_FLOW_EXP";
        case Trigger::HIGH_CIRCUIT_PRESSURE_EXP:       return "HIGH_CIRCUIT_PRESSURE_EXP";
        case Trigger::HIGH_CIRCUIT_PRESSURE_INSP:      return "HIGH_CIRCUIT_PRESSURE_INSP";
        case Trigger::IMMEDIATE_BREATH:                return "IMMEDIATE_BREATH";
        case Trigger::IMMEDIATE_EXP:                   return "IMMEDIATE_EXP";
        case Trigger::NET_FLOW_EXP:                    return "NET_FLOW_EXP";
        case Trigger::NET_FLOW_INSP:                   return "NET_FLOW_INSP";
        case Trigger::PAUSE_PRESS:                     return "PAUSE_PRESS";
        case Trigger::PRESSURE_BACKUP_INSP:            return "PRESSURE_BACKUP_INSP"; 
        case Trigger::PRESSURE_INSP:                   return "PRESSURE_INSP"; 
        case Trigger::PRESSURE_EXP:                    return "PRESSURE_EXP"; 
        case Trigger::SIMV_TIME_INSP:                  return "SIMV_TIME_INSP";
        case Trigger::BL_LOW_TO_HIGH_INSP:             return "BL_LOW_TO_HIGH_INSP";
        case Trigger::BL_HIGH_TO_LOW_EXP:              return "BL_HIGH_TO_LOW_EXP";
        case Trigger::PEEP_REDUCTION_EXP:              return "PEEP_REDUCTION_EXP";
        case Trigger::ASAP_INSP:                       return "ASAP_INSP"; 
        case Trigger::OPERATOR_INSP:                   return "OPERATOR_INSP";
        case Trigger::TIME_INSP:                       return "TIME_INSP";
        case Trigger::PAUSE_TIMEOUT:                   return "PAUSE_TIMEOUT";
        case Trigger::PAUSE_COMPLETE:                  return "PAUSE_COMPLETE"; 
        case Trigger::BACKUP_TIME_EXP:                 return "BACKUP_TIME_EXP"; 
        case Trigger::HIGH_VENT_PRESSURE_EXP:          return "HIGH_VENT_PRESSURE_EXP"; 
        case Trigger::OSC_TIME_BACKUP_INSP:            return "OSC_TIME_BACKUP_INSP";
        case Trigger::SVC_TIME:                        return "SVC_TIME"; 
        case Trigger::PRESSURE_SVC:                    return "PRESSURE_SVC";
        case Trigger::OSC_TIME_INSP:                   return "OSC_TIME_INSP"; 
        case Trigger::SVC_COMPLETE:                    return "SVC_COMPLETE";
        case Trigger::PEEP_RECOVERY_MAND_INSP:         return "PEEP_RECOVERY_MAND_INSP";
        case Trigger::PEEP_RECOVERY:                   return "PEEP_RECOVERY";
        case Trigger::HIGH_PRESS_COMP_EXP:             return "HIGH_PRESS_COMP_EXP";
        case Trigger::LUNG_FLOW_EXP:                   return "LUNG_FLOW_EXP";
        case Trigger::LUNG_VOLUME_EXP:                 return "LUNG_VOLUME_EXP";
        case Trigger::ARM_MANEUVER:                    return "ARM_MANEUVER";
        case Trigger::DISARM_MANEUVER:                 return "DISARM_MANEUVER";
        case Trigger::P100_MANEUVER:                   return "P100_MANEUVER";
        case Trigger::P100_COMPLETE:                   return "P100_COMPLETE";
        case Trigger::APNEA:                           return "APNEA"; 
        case Trigger::APNEA_AUTORESET:                 return "APNEA_AUTORESET";
        case Trigger::DISCONNECT:                      return "DISCONNECT";
        case Trigger::DISCONNECT_AUTORESET:            return "DISCONNECT_AUTORESET";
        case Trigger::MANUAL_RESET:                    return "MANUAL_RESET";
        case Trigger::OCCLUSION:                       return "OCCLUSION";
        case Trigger::OCCLUSION_AUTORESET:             return "OCCLUSION_AUTORESET";
        case Trigger::SAFETY_PCV:                      return "SAFETY_PCV";
        case Trigger::SETTING_CHANGE_MODE:             return "SETTING_CHANGE_MODE";
        case Trigger::STARTUP:                         return "STARTUP";
        case Trigger::SVO:                             return "SVO";
        case Trigger::SVO_RESET:                       return "SVO_RESET";
        case Trigger::VENT_SETUP_COMPLETE:             return "VENT_SETUP_COMPLETE";                        
        default: return "";
    };
}

/// @brief convert
/// 
/// @param x VentTypeValue::VentTypeValueId
/// 
/// @return charactor string describing VentTypeValue
inline const char * convert(VentTypeValue::VentTypeValueId &x)
{
    switch(ntohl(x))
    {
        case VentTypeValue::INVASIVE_VENT_TYPE:        return "INVASIVE_VENT_TYPE";
        case VentTypeValue::NIV_VENT_TYPE:             return "NIV_VENT_TYPE";
        default: return "";
    };
}

/// @brief convert
/// 
/// @param x PauseTypes::ManeuverType
/// 
/// @return charactor string describing ManeuverType
inline const char * convert(PauseTypes::ManeuverType &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::MANUAL:                       return "MANUAL";
        case PauseTypes::AUTO:                         return "AUTO";
        default: return "";
    };
}

/// @brief convert
/// 
/// @param x PauseTypes::ManeuverState
/// 
/// @return charactor string describing ManeuverState
inline const char * convert(PauseTypes::ManeuverState &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::IDLE:                         return "IDLE";
        case PauseTypes::PENDING:                      return "PENDING";
        case PauseTypes::ACTIVE:                       return "ACTIVE";
        case PauseTypes::IDLE_PENDING:                 return "IDLE_PENDING";
        case PauseTypes::ACTIVE_PENDING:               return "ACTIVE_PENDING";
        default: return "";
    };
}

/// @brief convert
/// 
/// @param x PauseTypes::PedState
/// 
/// @return charactor string describing PedState
inline const char * convert(PauseTypes::PedState &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::PED_IDLE:                     return "PED_IDLE";
        case PauseTypes::PED_ACTIVE:                   return "PED_ACTIVE";
        case PauseTypes::PED_COMPLETE:                 return "PED_COMPLETE";
        default: return "";
    };
}

/// @brief convert
/// 
/// @param x PauseTypes::PpiState
/// 
/// @return charactor string describing PpiState
inline const char * convert(PauseTypes::PpiState &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::UNSTABLE:                     return "UNSTABLE";
        case PauseTypes::STABLE:                       return "STABLE";
        default: return "";
    };
}

/// @brief convert
/// 
/// @param x PauseTypes::ComplianceState
/// 
/// @return charactor string describing ComplianceState
inline const char * convert(PauseTypes::ComplianceState &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::VALID:                        return "VALID";
        case PauseTypes::OUT_OF_RANGE:                 return "OUT_OF_RANGE";
        case PauseTypes::CORRUPTED_MEASUREMENT:        return "CORRUPTED_MEASUREMENT";
        case PauseTypes::SUB_THRESHOLD_INPUT:          return "SUB_THRESHOLD_INPUT";
        case PauseTypes::UNAVAILABLE:                  return "UNAVAILABLE";
        case PauseTypes::EXH_TOO_SHORT:                return "EXH_TOO_SHORT";
        case PauseTypes::NOT_STABLE:                   return "NOT_STABLE";
        case PauseTypes::NOT_REQUIRED:                 return "NOT_REQUIRED";
        default: return "";
    };
}


/// @brief convert
/// 
/// @param x VtpcvState::Id 
/// 
/// @return charactor string describing VtpcvState
inline const char * convert(VtpcvState::Id &x)
{
    switch(ntohl(x))
    {
        case VtpcvState::STARTUP_VTPCV:                return "STARTUP_VTPCV";
        case VtpcvState::NORMAL_VTPCV:                 return "NORMAL_VTPCV";
        default: return "";
    };
}


/// @brief convert
/// 
/// @param x PavState::Id
/// 
/// @return charactor string describing PavState
inline const char * convert(PavState::Id &x)
{
    switch(ntohl(x))
    {
        case PavState::STARTUP:                        return "STARTUP";
        case PavState::CLOSED_LOOP:                    return "CLOSED_LOOP";
        default: return "";
    };
}


/// @brief convert
/// 
/// @param x Boolean
/// 
/// @return charactor string describing Boolean
inline const char * convert(Boolean &x)
{
    if(x==0)
    {
        return "FALSE";
    }
    return "TRUE";
}

/// @brief convert
/// 
/// @param x Uint32
/// 
/// @return charactor string describing Boolean
inline const char * convert(Uint32 &x)
{
	return "2010-05-05 12:00:00";
}


#endif
