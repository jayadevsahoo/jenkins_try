#ifndef BackgroundMaintApp_HH
#define BackgroundMaintApp_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================ H E A D E R   D E S C R I P T I O N ====
// Class: BackgroundMaintApp - Background Maintenance applications
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundMaintApp.hhv   25.0.4.0   19 Nov 2013 14:20:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//
//  Revision: 002  By: iv    Date:  14-Aug-1997    DR Number: DCS 2305
//  	Project:  Sigma (840)
//		Description:
//			Activate the maintenance task on the GUI CPU.
//
//  Revision: 001    By: by  Date: 12-Dec-1995   DR Number: None
//    Project: Sigma (840)
//    Description:
//       Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "SafetyNet.hh"

class BackgroundMaintApp
{

    public:
       
        enum BkMaintSemphCommands
        {
            TASK_RESUME,
            REQUEST_TO_SUSPEND,
            TASK_SUSPENDED
        };

#if defined( SIGMA_BD_CPU )
        static inline BkMaintSemphCommands RetrieveCurrentSemphStatus( void );
        static inline void SetUpdateCompressorOperationalTime( const Boolean );
        static void IssueNewSemphCommand( const BkMaintSemphCommands );
#endif //defined( SIGMA_BD_CPU )

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL, 
			       const char*       pPredicate = NULL );
  
        static void Initialize( void );
        static void Task( void );

    private:

        BackgroundMaintApp( void );  // not implemented
        ~BackgroundMaintApp( void );  // not implemented          

        BackgroundMaintApp( const BackgroundMaintApp& ); // not implemented
        void operator= ( const BackgroundMaintApp& );    // not implemented          
        static void UpdateSystemOperationalTime_( void );

#if defined( SIGMA_BD_CPU )
        static void UpdateCompressorOperationalTime_( void );

        //@ Data-Member: UpdateCompOperationalTime_ 
        //  indicates whether to update compressor operational time
        static Boolean UpdateCompOperationalTime_;

        //@ Data-Member: PrevCompElapsedTimerImage_ 
        //  stores the previous compressor elapsed timer image
        static Uint32 PrevCompElapsedTimerImage_;

        //@ Data-Member: CurrentBkMaintSemphStatus_
        //  stores the current task semaphore status
        static BkMaintSemphCommands CurrentBkMaintSemphStatus_;

        //@ Data-Member: IsTaskRunning_
        //  indicates whether the maintance task is currently running
        static Boolean IsTaskRunning_;

        //@ Data-Member: WasTaskSuspended_ 
        //  indicates whether the maintance task was suspended 
        static Boolean WasTaskSuspended_;
#endif //defined( SIGMA_BD_CPU )

        //@ Data-Member: CurrentSystemOperationalHours_ 
        //  indicates the current vent head operational hour 
        static Uint32 CurrentSystemOperationalHours_;

        //@ Data-Member: ElapsedMinSincePostUpdate_
        //  indicates the elapsed time since update to POST NOVRAM
        static Uint32 ElapsedMinSincePostUpdate_;
};

#include "BackgroundMaintApp.in"

#endif    // #ifndef BackgroundMaintApp_HH

