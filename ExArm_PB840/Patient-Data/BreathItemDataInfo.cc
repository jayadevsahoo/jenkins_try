#include "stdafx.h"

#if        defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  BreathItemDataInfo - Breath Data Item processing agent class.
//---------------------------------------------------------------------
//@ Interface-Description
//
//>Von
//   Description:
//   ------------
//   BreathItemDataInfo serves a base class for all the breath data
//   processing agent class. It handles several common aspects of
//   all breath data processing, maintains double buffer to coordinate
//   data access and data update amongst multiple tasks, notifies
//   other interested tasks when there is a breath item change or 
//   data timeout, and provides breath data access methods.
//
//   Class Features:
//   --------------------
//    1. Maintains a double breath data buffer, one to hold an active data
//       that is all ready to be accessed by other subsystems and the other to 
//       receive a new data from the network.
//    2. Inform interested subsystems about a breath item data change
//       and data timeout.
//    3. Provides various breath data access methods.
//
//   Class Key methods:
//   -----------------
//   1. modifyData_   -- modifies breath item data of this data agent
//                         and sends out a breath data change notification.
//   2. getBoundedValue  -- gets the active data value of this 
//                            bounded breath type data agent.
//   3. getDiscreteValue -- gets the active data value of this
//                           discrete breath type data agent.
//   4. getBreathDatum  --   gets the active data value of a given bounded
//           breath type data agent in a 'BoundedBreathDatum' structure.
//   5. isActive       --   determines whether an active data is available.
//---------------------------------------------------------------------
//@ Rationale
//     To capture all the common functionality of breath item
//     data processing within one class.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Data integrity of each breath item type is maintained by using double
//    data buffer. Initially buffer index value is set to indicate that 
//    there is no active data in the buffer, both network and active indexes
//    are set to zero until network starts delivering breath data from the
//    Breath Delivery board.
//    The 'network' indexes into the buffer array that is either receiving
//    data or waiting to be filled. The 'active' indexes into a 
//    buffer which is ready to be read by other tasks. As soon as, new
//    data is completely stored into a network buffer, the index_.active 
//    will be updated to a newer data and index_.network will be updated
//    to next buffer.
//    
//---------------------------------------------------------------------
//@ Fault-Handling
//    Follows the Sigma Standard contract programming model, 
//    class precoditions and class assertions are used to raise
//    a fatal software conditions.
//>Voff
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/BreathItemDataInfo.ccv   25.0.4.0   19 Nov 2013 14:17:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  30-Jan-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BreathItemDataInfo.hh"
#include "PatientDataMgr.hh"

const Int MAX_DOUBLE_BUFFER_INDEX = 2;
//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: modifyData_(void *pValue )  
//
//@ Interface-Description
//     BreathItemDataInfo::modifyData_() updates the active data to
//     be the data just received from the network.
//     Sends out breath data change notification for this specific
//     breath item data type.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This is a pure virtual function that should be overriden by its
//      subclasses for actual modification of the data. After 
//      newly received data is completely processed into a network buffer
//      by a subclass 'modifyData' method, this method is called to update
//      the buffer index.
//      This method is executed on the Network Application task only
//      and it has higher execution priority than other tasks
//      that reads breath data. It is critical that this method doesn't
//      get interruped by other consumer tasks.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
BreathItemDataInfo::modifyData_(void *)
{

  CALL_TRACE("BreathItemDataInfo::modifyData_()");
  SAFE_CLASS_PRE_CONDITION( index_.network < MAX_DOUBLE_BUFFER_INDEX);

  index_.active = index_.network;
  index_.network = (index_.network +1)%MAX_DOUBLE_BUFFER_INDEX;
  PatientDataMgr::MakeItemCalls(DATA_ID_); // $[TI1]
} 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BreathItemDataInfo( const PatientDataId::PatientItemId id)
//
//@ Interface-Description
//     The BreathItemDataInfo class constructor. 
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//     The network and active index of the buffer index is initially
//      set to zero. When these two indexes are same there is no active
//      data for this data type.
//--------------------------------------------------------------------- 
//@ PreCondition
//  ( PatientDataMgr::IsBreathItemId(id) )
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

BreathItemDataInfo::BreathItemDataInfo( const PatientDataId::PatientItemId id): DATA_ID_(id)
{
  CALL_TRACE("BreathItemDataInfo( const PatientDataId::PatientItemId id)");
  CLASS_PRE_CONDITION( PatientDataMgr::IsBreathItemId(id) );
                      
  index_.active = 0;
  index_.network = 0;
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathItemDataInfo()  [Destructor]
//
//@ Interface-Description
//    Destructor - does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

BreathItemDataInfo::~BreathItemDataInfo(void)
{
  CALL_TRACE("~BreathItemDataInfo()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BreathItemDataInfo::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA,
			  BREATH_ITEM_DATA_INFO, lineNumber,
			  pFileName, pPredicate);
}

#endif        //defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
