//----------------------------------------------------------------------------
//            Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------
/// @file
/// @brief Types used by the Telemetry class

#ifndef TELEMETRYTYPES_HH_
#define TELEMETRYTYPES_HH_

#define MAX_SIZE     10
#define MAX_SIGNALS  18
#define SIG_NAME_LEN 40

// This define is for using this with the BdSignal Python script for collecting
// data. see \PROTOTYPE\bdsignal
// Defining this will make the flat packet incompatible with SWAT.
// This should always be commented out in the baseline.
//#define SIGNAL_ANALYSIS

/// @brief Facilitates the association of signals to function pointers
class TelemetryPair
{
public:
	Real32 (*func)();
	char name[SIG_NAME_LEN];
};

/// @brief Smart packet, compatible with WaveformApp
class TelemetrySmartPacket {
public:
	Real32 data[MAX_SIGNALS][MAX_SIZE];
};

/// @brief For outputting all parameters.
/// SWAT (Software Automated Test) uses this packet for testing.
template <int NUMSIGS>
class TelemetryFlatPacket
{
public:
#ifndef SIGNAL_ANALYSIS
	struct DataSamples
	{
		Real32 s[MAX_SIZE];
	};

	DataSamples data[NUMSIGS];
#else
	Real32 data[MAX_SIZE][NUMSIGS];
#endif
};

/// @brief Used by the application to create sets of defaults to allow
///        users to make bulk changes instead of slot by slot.
template <typename SIGS>
class TelemetrySlotConfiguration
{
public:
	SIGS sigs[MAX_SIGNALS];
};

/// @brief Queue event for indicating to the thread that data is ready
///         to be sent.
union GenericQueueEvent
{
	struct
	{
		Int8   type:   8;
		Int32  data:  24;
	} event;

	Int32 qword;
};

#endif //TELEMETRYTYPES_HH_
