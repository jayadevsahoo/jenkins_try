#ifndef Sys_Init_HH
#define Sys_Init_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  Sys-Init - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/Sys_Init.hhv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sigma.hh"
#include "TraceMacros.hh"
//@ End-Usage

//@ Type: Semaphore
//  Semaphore identifier used for portability.
typedef  int Semaphore;

class Sys_Init
{
  public:
    enum InitClassId {
        ID_SYS_INIT              = 1,
        ID_VRTXHOOKS             = 2,
        ID_IPCTABLE              = 3,
        ID_USERCLOCK             = 4,
        ID_TASKTABLE             = 5,
        ID_INITCONDITIONS        = 6,
        ID_TASKCONTROL           = 7,
        ID_FSM                   = 8,
        ID_FSMSTATE              = 9,
        ID_TASKCONTROLQUEUEMSG   = 10,
        ID_CRITICALSECTION       = 11,
        ID_STACKCHECK            = 12,

        ID_BDFSM                 = 13,
        ID_BDSTART               = 14,
        ID_BDINOP                = 15,
        ID_BDONLINE              = 16,
        ID_BDSERVICE             = 17,
        ID_BDSST                 = 18,

        ID_COMMTASKAGENT         = 19,
        ID_TASKAGENT             = 20,
        ID_TASKAGENTMANAGER      = 21,
        ID_TASKCONTROLAGENT      = 22,
        ID_SERVICEMODEAGENT      = 23,

        ID_APPCONTEXT            = 24,
        ID_TASKCONTROLMESSAGE    = 25,
        ID_CHANGESTATEMESSAGE    = 26,
        ID_COMMDOWNMESSAGE       = 27,
        ID_COMMUPMESSAGE         = 28,
        ID_BDREADYMESSAGE        = 29,
        ID_GUIREADYMESSAGE       = 30,
        ID_TASKREADYMESSAGE      = 31,
        ID_SYNCCLOCKMESSAGE      = 32,
        ID_TASKCONTROLTIMER      = 33,
        ID_STARTUPTIMER          = 34,
        ID_TRANSITIONTIMER       = 35,
        ID_SYNCCLOCKTIMER        = 36,

        ID_GUIFSM                = 37,
        ID_GUISTART              = 38,
        ID_GUIINOP               = 39,
        ID_GUIONLINE             = 40,
        ID_GUISERVICE            = 41,
        ID_GUISST                = 42,
        ID_GUITIMEOUT            = 43,
        ID_GUIONLINEINIT         = 44,
        ID_GUISERVICEINIT        = 45,
        ID_GUISSTINIT            = 46,
   
        ID_BDFAILEDMESSAGE       = 47,

        NUM_INIT_CLASSES
    };
         
	static void InitializeSigma(void);
    static void InitializeTasks(void);
    static void Initialize(void);

    inline static Boolean  IsSystemInitialized(void);
    inline static Boolean  IsTaskingOn(void);

#if defined(SIGMA_DEVELOPMENT)
    static Int32           GetDebugLevel(const InitClassId initClassId);
 
    static void            SetDebugLevel(const InitClassId initClassId,
                                         const Int32 level);
 
    static void            SetDebugLevel(const Int32 level);

#endif  // defined(SIGMA_DEVELOPMENT)
 

    static void     SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL,
			      const char*       pPredicate = NULL);
    friend class UnitTestDriver;
    
  private:
    Sys_Init();                             // declared only
    ~Sys_Init();                            // declared only

    Sys_Init(const Sys_Init&);              // declared only
    void operator=(const Sys_Init&);        // declared only

    static void            InitializeSubsystems_();

    //@ Data-Member:  SystemInitialized_
    // Boolean set TRUE after subsystem and sys-init intialization
    static Boolean         SystemInitialized_;

    //@ Data-Member:  TaskingOn_
    // Boolean set TRUE after init task is created just before vrtx_go
    static Boolean         TaskingOn_;

    //@ Data-Member:  DebugLevel_
    // A static array of debug levels used by this subsystem's code.
    static Uint8   DebugLevel_[Sys_Init::NUM_INIT_CLASSES];

};

#include "Sys_Init.in"

#endif // Sys_Init_HH
