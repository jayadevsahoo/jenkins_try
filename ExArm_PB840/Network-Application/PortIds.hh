#ifndef	PortIds_HH
#define	PortIds_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: PortIds - List of InterProcess Communication (IPC) IDs
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/PortIds.hhv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  <jhv>    Date:  06-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================


enum PortId
{
	ANY_PORT_ID			= 0,
	BD_BOARD_TCPPORT 		= 7000,
	LOWEST_PORT_ID			= BD_BOARD_TCPPORT,
	BLOCK_IO_TCPPORT 		= 7001,
	WAVEFORM_UDPPORT 		= 7002,
	PATIENT_DATA_UDPPORT 	= 7003,
	SETTINGS_UDPPORT 		= 7004,
	ALARMS_UDPPORT 			= 7005,
	BROADCAST_UDPPORT  		= 7006,
	SERVICE_DATA_UDPPORT    = 7007,
    BD_DEBUG_SRC_UDPPORT0   = 7008,
    BD_DEBUG_DEST_UDPPORT0  = 7009,
    BD_DEBUG_SRC_UDPPORT1   = 7010,
    BD_DEBUG_DEST_UDPPORT1  = 7011,
    BD_DEBUG_SRC_UDPPORT2   = 7012,
    BD_DEBUG_DEST_UDPPORT2  = 7013,
    BD_DEBUG_SRC_UDPPORT3   = 7014,
    BD_DEBUG_DEST_UDPPORT3  = 7015,
	BD_ADDR_BROADCAST_PORT  = 7016,
	GUI_ADDR_BROADCAST_PORT = 7017,
    TOUCHED_POS_BROADCAST_PORT = 7018,	// Touched Position broadcast port
    SWAT_GUI_COMMAND_UDP_PORT  = 7019,	// SWAT Gui command server UDP port
    SWAT_BD_COMMAND_UDP_PORT   = 7020,	// BD script command server UDP port
    SWAT_PC_UDP_PORT           = 7021,	
	SWAT_PD_SIMULATION_UDP_PORT	= 7022,	// Patient Data simulation server UDP port
    HIGHEST_PORT_ID = SWAT_PD_SIMULATION_UDP_PORT
};  

#endif	// PortIds_HH
