#ifndef VgaGraphicsDriver_HH
#define VgaGraphicsDriver_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VgaGraphicsDriver -  Subsystem-Level class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaGraphicsDriver.hhv   25.0.4.0   19 Nov 2013 14:37:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: mnr   Date:  24-Sep-2010      SCR Number: 6672
//  Project:  XENA2
//  Description:
//   Methods added for supporting two VGA graphics chips based on 
//   hardware configuration.
//
//  Revision: 006  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//  Removed DELTA project single screen support.
//
//  Revision: 005  By:  gdc    Date:  07-APR-2008    SCR Number: 6407
//  Project:  TREND2
//  Description:
//  Removed redefinition of countof macro to resolve compiler warning.
//
//  Revision: 004   By: gdc    Date:  13-MAR-2001    DCS Number: 5788
//  Project: GuiComms
//  Description:
//  Corrected module ID VGA_REGISTERS_ID to VGA_ID.
//  
//  Revision: 003   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//  Implemented single screen compatibility.
//  
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//  Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001   By: sah    Date:  01-Aug-1997    DCS Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//  Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "OsUtil.hh"
#include "VgaDisplayId.hh"
#include "Task.hh"

//@ Usage-Classes
//@ End-Usage

class CDC;
class DisplayContext;

class VgaGraphicsDriver 
{
  public:

    //@ Type:  VgaModuleId
    // Id for all of this subsystem's modules.
    enum VgaModuleId
    {
      VGA_BLINK_ID = 0,
      VGA_DISPLAY_CONTEXT_ID = 1,
      VGA_DEVICE_ID = 2,
      VGA_FONT_ID = 3,
      VGA_FONT_DATA_ID = 4,
      VGA_LINE_ID = 5,
      VGA_GRAPHICS_CONTEXT_ID = 6,
      VGA_POINT_ID = 7,
      VGA_RECTANGLE_ID = 8,
      VGA_ID = 9,
      VGA_DIB_ID = 10,
      VGA_TRIANGLE_ID = 11,
      DISPLAY_CONTEXT_ID = 12,
      NULL_DISPLAY_CONTEXT_ID = 13,
      VGA_SCARLET_ID = 14,
      SCARLET_DISPLAY_CONTEXT = 15
    };

    static void  Initialize(void);

    static DisplayContext & GetUpperDisplayContext(void);
    static DisplayContext & GetLowerDisplayContext(void);

    static DisplayContext & GetDisplayContext(VgaDisplayId displayId);

  private:

	static CDC* ptrLowerCDC_;
	static CDC* ptrUpperCDC_;

    static DisplayContext & GetLegacyLowerDisplayContext_(void);
    static DisplayContext & GetScarletLowerDisplayContext_(void);

    static DisplayContext & GetLegacyUpperDisplayContext_(void);
    static DisplayContext & GetScarletUpperDisplayContext_(void);

	static void GetUpperLowerWndCDCptrs(void);

};


#endif // VgaGraphicsDriver_HH 
