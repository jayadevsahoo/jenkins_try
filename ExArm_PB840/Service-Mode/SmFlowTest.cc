#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SmFlowTest - Service Mode Flow Test
//---------------------------------------------------------------------
//@ Interface-Description
//
//	This class implements the remote flow test service functions, 
//  which establish specified flow rates of air or oxygen which are
//  then verified with an external test fixture (VIPER).
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the remote tests for flow characterization.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method establishFlow() is responsible for establishing a flow
//  of gas at the level specified in the test parameters.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//  This class should only be instantiated through the static methods
//  GetOxygenFlowTest() and GetAirFlowTest().
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmFlowTest.ccv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: quf    Date: 24-Apr-2000     DR Number: 5709
//  Project:  NeoMode
//  Description:
//	Added rampFlow_() to ramp the flow to the desired level rather
//	than setting the flow immediately to the desired level.
//
//  Revision: 004  By: dosman    Date: 26-Feb-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release.
//	Changed establishFlow() so that remote PC can re-measure desired values.
//	Traced SRS requirements to code.
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 01-Dec-1997    DR Number: DCS 2661
//       Project:  Sigma (840)
//       Description:
//			Fixed establishFlow() to read offsetted exh flow.
//
//  Revision: 001  By:  gdc    Date: 23-Sep-1997    DR Number: 2513
//       Project:  Sigma (R8027)
//       Description:
//           Initial version to support service/manufacturing test.
//
//=====================================================================

#include "SmFlowTest.hh"

//@ Usage-Classes

#include "SmManager.hh"
#include "SmConstants.hh"
#include "SmRefs.hh"
#include "MainSensorRefs.hh"
#include "VentObjectRefs.hh"
#include "ValveRefs.hh"
#include "SmUtilities.hh"
#include <stdio.h>   // for sscanf
#include "Task.hh"
#include "FlowSensor.hh"
#include "SmFlowController.hh"
#include "Psol.hh"
#include "ExhFlowSensor.hh"
#include "SigmaState.hh"
#include "TaskControlAgent.hh"
#include "SmGasSupply.hh"
#include "Compressor.hh"

#if defined SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"

// **** SmFlowTest_UT.hh MUST be the LAST of the includes! ****
# include "SmFlowTest_UT.hh"

#elif defined DCS_5709_UNIT_TEST
# include "FakeControllers.hh"
# include "Dcs5709.hh"

#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

static const Real32  COMPRESSOR_FILL_RATE = 54.0;  // lpm

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmFlowTest(GasType)  [Constructor]
//
//@ Interface-Description
//	Constructor.  Four arguments: gas type, inspiratory flow sensor
//	object reference, expiratory flow sensor object reference, and
//	psol object reference.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Use arguments to initialize private data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SmFlowTest::SmFlowTest(  const GasType   gasType
                       , FlowSensor &    flowSensor
                       , ExhFlowSensor_t & exhFlowSensor
                       , Psol &          psol )
    : gasType_(gasType)
    , rFlowSensor_(flowSensor)
    , rExhFlowSensor_(exhFlowSensor)
    , rPsol_(psol)
{
// $[TI1]
	CALL_TRACE("SmFlowTest::SmFlowTest(const GasType)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmFlowTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmFlowTest::~SmFlowTest(void)
{
	CALL_TRACE("SmFlowTest::~SmFlowTest(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAirFlowTest() 
//
//@ Interface-Description
//	Static function returns a reference to the SmFlowTest referencing
//  	the air flow test.
//  	On the first call, instantiates SmFlowTest and returns
//  	a reference to the object. Subsequent calls return reference to
//  	the instantiated SmFlowTest.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

const SmFlowTest &
SmFlowTest::GetAirFlowTest(void)
{
	CALL_TRACE("SmFlowTest::GetAirFlowTest(void)");

	static SmFlowTest flowTest(  AIR
                               , RAirFlowSensor
                               , RExhFlowSensor
                               , RAirPsol );

	// $[TI1]
	return flowTest;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetOxygenFlowTest() 
//
//@ Interface-Description
//	Static function returns a reference to the SmFlowTest referencing
//  	the oxygen flow test.
//  	On the first call, instantiates SmFlowTest and returns
//  	a reference to the object. Subsequent calls return reference to
//  	the instantiated SmFlowTest.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

const SmFlowTest &
SmFlowTest::GetOxygenFlowTest(void)
{
	CALL_TRACE("SmFlowTest::GetOxygenFlowTest(void)");

// E600
#if 0
	static SmFlowTest flowTest(  O2
                               , RO2FlowSensor
                               , RExhO2FlowSensor
                               , RO2Psol );
#endif
	static SmFlowTest flowTest(  AIR
                               , RAirFlowSensor
                               , RExhFlowSensor
                               , RAirPsol );

	// $[TI1]
	return flowTest;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: purgeCircuit()
//
//@ Interface-Description
//	Purge the circuit with the appropriate gas type.  No arguments,
//	no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[08057] Sigma shall purge the ventilator circuit with air when 
//  commanded by the PC.
//  $[08058] Sigma shall purge the ventilator circuit with O2 when
//  commanded by the PC.
//
//	Purge the circuit appropriately for the current system state and
//	gas type, then recharge the compressor if appropriate.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SmFlowTest::purgeCircuit(void) const
{
	CALL_TRACE("SmFlowTest::purgeCircuit(void)");

	// $[TI1]
	SigmaState  state = TaskControlAgent::GetMyState();
		RSmUtilities.flushTubing( gasType_, state);

	recharge_();
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: establishFlow()
//
//@ Interface-Description
//  This method attempts to establish a flow of the prescribed gas. This
//  test is initiated by the service/manufacturing PC to measure flow
//  accuracies with an external test fixture (VIPER). An external signal
//  is generated at the time this method measures the flow on the
//  inspiratory and expiratory sides, and reads the PSOL current 
//  required to establish the prescribed flow.
//	1 argument: pointer to string containing the prescribed flow.
//	No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[08059] Sigma shall establish the requested air flow when commanded
//  by the PC.
//  $[08060] Sigma shall establish the requested O2 flow when commanded
//  by the PC.
//  $[TC08000] Sigma shall re-measure the inspiratory flow, expiratory
//  flow, and PSOL current, during the Air and Oxygen Flow Tests,
//  when commanded by the PC.
//
//	Establish a test flow and allow it to stabilize.
//	Delay after stability is reached to allow alpha filtering of flows.
//	Read and display insp and exh flows (both alpha-filtered, exh flow is
//	offsetted) and psol command current.
//	Wait for user to press ACCEPT key or CLEAR key.
//	The prompt does not mention CLEAR.
//	It says to press ACCEPT key to stop flow.
//	However, if user presses the CLEAR key 
//	(which is really receipt of the remote signal)
//	then the vent re-reads and re-displays (re-sends)
//	insp and exh flows and psol command current.
//	The user can press CLEAR key repeatedly to re-measure values.
//	Wait for user to press ACCEPT key, then stop the flow.
//	If flow did not stabilize within the timeout, generate a FAILURE.
//	If appropriate, delay to recharge the compressor.
//
//	The prompt to the user says to press ACCEPT key to stop flow.
//	Because this is a remote test, 
//	and because we want to keep backward compatibility,
//	the prompt was not changed to mention CLEAR key.
//	If it is desired to make this test interactive,
//	then a separate test would need to be set up
//	because we cannot change the prompt to mention CLEAR key,
//	because the test needs to be backward compatible.
//	Old PC software would not know of the existence of any new
//	prompt that we create.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SmFlowTest::establishFlow( const char * const pParameters ) const
{
	CALL_TRACE("SmFlowTest::establishFlow(void)");

#if defined(SIGMA_UNIT_TEST)
	cout << "SmFT::eF beg" << endl;
#endif // defined(SIGMA_UNIT_TEST)

	Real32 flowRate = 0.0;
	sscanf( pParameters, "%f", &flowRate);

	// Set test gas to the test flow -- ramp the flow to prevent
	// pressure oscillation at high flow.
	rampFlow_( flowRate);

	// wait up to 2 seconds for stable flow
	Boolean flowTimeout =  !RSmFlowController.waitForStableFlow(2000);

	// delay for alpha-filtering of flow values
	Task::Delay( 0, 300);

	RSmManager.signalTestPoint(); // tell VIPER we're sampling

	//  send data points 
	Real32 inspFlow = rFlowSensor_.getFilteredValue();
	Real32 exhFlow  = rExhFlowSensor_.getFilteredValue();

	// read PSOL current
	Real32 psolMa   = rPsol_.getCurrent();

	RSmManager.sendTestPointToServiceData( inspFlow);
	RSmManager.sendTestPointToServiceData( exhFlow);
	RSmManager.sendTestPointToServiceData( psolMa);

#if defined(SIGMA_UNIT_TEST)
	cout << "SmFT::eF pO" << endl;
#endif // defined(SIGMA_UNIT_TEST)

	// The user may press ACCEPT key to stop flow, or 
	// CLEAR key to re-measure values, repeatedly.
	RSmManager.promptOperator(  
		SmPromptId::ACCEPT_TO_STOP_FLOW_PROMPT
                              , SmPromptId::ACCEPT_YES_AND_CLEAR_NO_ONLY);

#if defined(SIGMA_UNIT_TEST)
	cout << "SmFT::eF gOA" << endl;
#endif // defined(SIGMA_UNIT_TEST)

	// For the production vent software , we prompt the operator
	// and wait for a response, whereas for unit test
	// there is no interaction and we simulate input.
	// get operator keypress and process it
	SmPromptId::ActionId operatorAction =
	  RSmManager.getOperatorAction();

#if defined(SIGMA_UNIT_TEST)
	cout << "SmFT::eF bef while oA=" << operatorAction << endl;
#endif // defined(SIGMA_UNIT_TEST)

	while (operatorAction != SmPromptId::KEY_ACCEPT)
	{
	// $[TI5]
		// The user's action must be either CLEAR or ACCEPT
		// If it is not ACCEPT, then it must be CLEAR.
		CLASS_ASSERTION (operatorAction == SmPromptId::KEY_CLEAR);

		// Re-initialize the test point, which tells which data point we are sending, to zero.
		// Start sending data as if we are starting over.
		// Otherwise, data is just added to the end of the packet
		// to be sent to the PC, and we will be out of bounds of the array.
		RSmManager.resetTestPoint();

		// user is asking to retrigger and resample
		RSmManager.signalTestPoint(); // tell VIPER we're sampling

		//  send data points 
		Real32 inspFlow = rFlowSensor_.getFilteredValue();
		Real32 exhFlow  = rExhFlowSensor_.getFilteredValue();

		// read PSOL current
		Real32 psolMa   = rPsol_.getCurrent();

		RSmManager.sendTestPointToServiceData( inspFlow);
		RSmManager.sendTestPointToServiceData( exhFlow);
		RSmManager.sendTestPointToServiceData( psolMa);

		// then prompt user when to stop flow
		// ACCEPT key = user wants to terminate flow
		// CLEAR key = user wants to re-measure
		RSmManager.promptOperator(  SmPromptId::ACCEPT_TO_STOP_FLOW_PROMPT
			, SmPromptId::ACCEPT_YES_AND_CLEAR_NO_ONLY);
 
		operatorAction = RSmManager.getOperatorAction();

#if defined(SIGMA_UNIT_TEST)
		cout << "SmFT::eF in while oA=" << operatorAction << endl;
#endif // defined(SIGMA_UNIT_TEST)

	}
	// $[TI6], implied else

	// The user's action must be either CLEAR or ACCEPT.
	// We should only reach this point after receiving an ACCEPT.
	// ACCEPT means user wants to stop flow.
	CLASS_ASSERTION (operatorAction == SmPromptId::KEY_ACCEPT);
 
	// turn off gas
	RSmFlowController.stopFlowController();

	if ( flowTimeout )
	{
	// $[TI1]
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_1);

	}
	// $[TI2]
	// implied else

	if ( flowRate >= COMPRESSOR_FILL_RATE )
	{
	// $[TI3]
		recharge_();
	}
	// $[TI4]
	// implied else

#if defined(SIGMA_UNIT_TEST)
	cout << "SmFT::eF end" << endl;
#endif // defined(SIGMA_UNIT_TEST)

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 
void
SmFlowTest::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SMFLOWTEST,
		                      lineNumber, pFileName, pPredicate);

}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: recharge()
//
//@ Interface-Description
//  This method delays to allow time for the compressor to refill when
//  testing air flow using the compressor.  No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the gas type is air and no wall air is present and the compressor
//  is installed, then delay fixed compressor fill time.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SmFlowTest::recharge_(void) const
{
	// if using compressor air, wait for compressor to recharge
	if (    gasType_ == AIR
		&& !RSmGasSupply.isWallAirPresent()
		&& RCompressor.checkCompressorInstalled() )
	{
	// $[TI1]
#ifdef SIGMA_UNIT_TEST
		SmFlowTest_UT::CompressorRecharged = TRUE;
#else
		Task::Delay( 0, COMPRESSOR_FILL_TIME);
#endif  // SIGMA_UNIT_TEST
	}
	// $[TI2]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: rampFlow_()
//
//@ Interface-Description
//  This method ramps the closed-loop-controlled flow up to the
//	target flow, which is passed in as an argument. No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Ramp the flow until the target flow is reached.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SmFlowTest::rampFlow_( const Real32 targetFlow) const
{
	const Real32 FLOW_STEP = 4.0;	// Lpm
	Real32 rampedFlow;

	for (	rampedFlow = FLOW_STEP;
			rampedFlow < targetFlow;
			rampedFlow += FLOW_STEP )
	{
	// [$TI1]
		RSmFlowController.establishFlow( rampedFlow, DELIVERY, gasType_ );
		Task::Delay( 0, SM_CYCLE_TIME);
		
#ifdef DCS_5709_UNIT_TEST
		Dcs5709::RampedFlow = rampedFlow;
#endif  // DCS_5709_UNIT_TEST
	}

	RSmFlowController.establishFlow( targetFlow, DELIVERY, gasType_ );
}
