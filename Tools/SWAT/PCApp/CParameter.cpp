//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file CParameter.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "CParameter.h"
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOM.hpp>

#include<map>
#include<string>
#include<iterator>
#include<cstdlib>

using namespace XERCES_CPP_NAMESPACE;


//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// CParameter
//---------------------------
CParameter::CParameter ():
    m_iType (CParameter::UNDEF), m_pEnumMap (NULL), m_iVal (0), m_iMax (0),
    m_iMin (0) {
}

//---------------------------
// CParameter
//---------------------------
CParameter::CParameter (const DOMNode * ptr):
    m_iType (CParameter::UNDEF),
    m_pEnumMap (NULL),
    m_iVal (0),
    m_iMax (0),
    m_iMin (0),
    m_bMin (false),
    m_bMax (false),
    m_bVal (false) {

    const XMLCh *tempXMLCh, *tempXMLName, *tempXMLValue;
    XMLCh tempStr[100];

    DOMAttr *tempAttr;

    if (ptr->getNodeType () != DOMNode::ELEMENT_NODE) {
        return;
    }

    //
    // Set the name of the of the node
    //
    XMLString::transcode ("name", tempStr, 99);
    tempAttr = ((DOMElement *) ptr)->getAttributeNode (tempStr);

    if ((tempAttr == NULL) || ((tempXMLCh = tempAttr->getValue ()) == NULL)) {
        //Should throw an exception
        return;
    }

	
	char *typeStr = XMLString::transcode (tempXMLCh);

	if ((typeStr == NULL) || (setName (typeStr) == false)) {
		if(typeStr)
		{
			XMLString::release(&typeStr);
		}

		//Should throw an exception
		return;
	}
	XMLString::release(&typeStr);
	

    //
    // Set Attributes List of node
    //      
    DOMNamedNodeMap *pDOMNamedNodeMap = ptr->getAttributes ();

    if (pDOMNamedNodeMap == NULL) {
        return;
    }
	

    for (unsigned int i = 0; i < pDOMNamedNodeMap->getLength (); i++) {

        tempAttr = (DOMAttr *) pDOMNamedNodeMap->item (i);

        if (tempAttr == NULL) {
            continue;
        }

        tempXMLName = tempAttr->getName ();
        tempXMLValue = tempAttr->getValue ();

        if (tempXMLName != NULL && tempXMLValue != NULL) {

			char *pName =  XMLString::transcode (tempXMLName);
			char *pValue = XMLString::transcode (tempXMLValue);

			addAttribute ((const char *)pName, (const char *)pValue);
			
			XMLString::release(&pName);
			XMLString::release(&pValue);
		
        }
    }


    //
    // Set Attributes 
    //      
    const char *tempVal = getAttribute ("type");
    if (tempVal) {
        if (strcmp (tempVal, "enum") == 0) {
            setType (CParameter::ENUM);

        }
        else if (strcmp (tempVal, "float") == 0) {
            setType (CParameter::FLOAT);
        }
        else if (strcmp (tempVal, "int") == 0) {
            setType (CParameter::INT);
        }
    }

    tempVal = getAttribute ("max");
    if (tempVal) {
        m_bMax = true;
        if (m_iType == CParameter::ENUM) {
            /*ENUM's do not have max value */
        }
        else if (m_iType == CParameter::FLOAT) {
            m_fMax = (float)atof (tempVal);
        }
        else if (m_iType == CParameter::INT) {
            m_iMax = atoi (tempVal);
        }
    }

    tempVal = getAttribute ("min");
    if (tempVal) {
        m_bMin = true;
        if (m_iType == CParameter::ENUM) {
            /*ENUM's do not have min value */
        }
        else if (m_iType == CParameter::FLOAT) {
            m_fMin = (float)atof (tempVal);
        }
        else if (m_iType == CParameter::INT) {
            m_iMin = atoi (tempVal);
        }
    }

    tempVal = getAttribute ("default");
    if (tempVal) {

        m_bVal = true;
        if (m_iType == CParameter::ENUM) {
            m_iEnumVal = atoi (tempVal);
        }
        else if (m_iType == CParameter::FLOAT) {
            m_fVal = (float)atof (tempVal);
        }
        else if (m_iType == CParameter::INT) {
            m_iVal = atoi (tempVal);
        }
    }

    DOMNodeList *pDOMNodeList = ptr->getChildNodes ();
    if (pDOMNodeList) {
        for (unsigned int i = 0; i < pDOMNodeList->getLength (); i++) {
            DOMNode *pNode = pDOMNodeList->item (i);
            if (pNode != NULL) {
                char *typeStr = XMLString::transcode (pNode->getNodeName ());

                if (typeStr == NULL)
                    continue;

                if ((m_iType == CParameter::ENUM)
                    && (strcmp (typeStr, "enum") == 0)) {
                    addEnum (pNode);
                }
				XMLString::release(&typeStr);
            }
        }
    }





    tempVal = getAttribute ("id");
    if (tempVal != NULL) {
        m_iId = atoi (tempVal);
    }

	tempVal = getAttribute ("xid");
    if (tempVal != NULL) {
        m_iXid = atoi (tempVal);
    }




    return;
}

//---------------------------
// ~CParameter
//---------------------------
CParameter::~CParameter () {
    if (m_iType == CParameter::ENUM) {
        delete m_pEnumMap;
    }
}

//---------------------------
// getName
//---------------------------
const char *
CParameter::getName () const {
    return m_ParameterNameString.c_str ();
}

//---------------------------
// getAttribute
//---------------------------
const char *
CParameter::getAttribute (const char *pszName) const {
    std::map < std::string, std::string >::const_iterator cii;
    cii = m_ParameterAttributeMap.find (pszName);

    if (cii == m_ParameterAttributeMap.end ()) {
        return NULL;
    }

    return cii->second.c_str ();
}

//---------------------------
// getEnum
//---------------------------
bool 
CParameter::getEnum (const char *pszEnumName, int *piOut) const {
    std::map <std::string, int >::const_iterator cii;

    //This is not an enum type
    if (m_iType != CParameter::ENUM)
    {
        return false;
    }

    //Fine Enum.
    cii = (*m_pEnumMap).find (pszEnumName);

    if (cii == (*m_pEnumMap).end ()) {
        return false;
    }

    (*piOut) = cii->second;

    return true;
}

//---------------------------
// getEnumName
//---------------------------
const char *
CParameter::getEnumName (const int iIn) const {
    std::map < std::string, int >::const_iterator cii;

    if (m_iType != CParameter::ENUM)
    {
        return NULL;
    }
    for (cii = (*m_pEnumMap).begin (); cii != (*m_pEnumMap).end (); cii++) {
        if (cii->second == iIn) {
            return cii->first.c_str ();
        }
    }

    return NULL;
}

//---------------------------
// addEnum
//---------------------------
const bool
CParameter::addEnum (const char *pszName, const char *pszValue) {
    std::map < std::string, int >::const_iterator cii;

    //This is not an enum type
    if (m_iType != CParameter::ENUM) {
        return false;
    }

    //Make sure that there is not already an enum with given name.
    cii = (*m_pEnumMap).find (pszName);

    if (cii != (*m_pEnumMap).end ()) {
        return false;
    }

    (*m_pEnumMap)[pszName] = atoi (pszValue);

    return true;
}

//---------------------------
// addEnum
//---------------------------
const bool
CParameter::addEnum (const DOMNode * pNode) {
    std::map < std::string, int >::const_iterator cii;
    bool retval = false;

    //This is not an enum type
    if (m_iType != CParameter::ENUM) {
        return false;
    }


    XMLCh tempStr[100];
    //char *typeStr;

    DOMAttr *tempAttr;


    char *typeStr = XMLString::transcode (pNode->getNodeName ());
    if (typeStr==NULL || strcmp (typeStr, "enum") != 0) 
	{
		if(typeStr)
		{
			XMLString::release(&typeStr);
		}
        return false;
	}
	XMLString::release(&typeStr);
	

    DOMNamedNodeMap *pDOMNamedNodeMap = pNode->getAttributes ();

    if (pDOMNamedNodeMap->getLength () == 0) {
        return false;
    }

    char *pName  = NULL;
    char *pValue = NULL;

	try {

		XMLString::transcode ("name", tempStr, 99);
		tempAttr = (DOMAttr *) pDOMNamedNodeMap->getNamedItem (tempStr);
		if (tempAttr) {
			const XMLCh *tempXMLValue = tempAttr->getValue ();
			pName =XMLString::transcode (tempXMLValue);
		}

		XMLString::transcode ("value", tempStr, 99);
		tempAttr = (DOMAttr *) pDOMNamedNodeMap->getNamedItem (tempStr);
		if (tempAttr) {
			const XMLCh *tempXMLValue = tempAttr->getValue ();
			pValue = XMLString::transcode (tempXMLValue);
		}

		if (pName != 0 && pValue != 0) {
			retval = addEnum (pName, pValue);
		}

	}
	catch (...){}

	if(pName)
	{
		XMLString::release(&pName);
	}

	if(pValue)
	{
		XMLString::release(&pValue);
	}

    return retval;
}


//---------------------------
// setName
//---------------------------
const bool
CParameter::setName (const char *pszIn) {
    m_ParameterNameString = pszIn;
    return true;
}

//---------------------------
// addAttribute
//---------------------------
const bool
CParameter::addAttribute (const char *pszName, const char *pszValue) {
    std::map < std::string, std::string >::const_iterator cii;
    cii = m_ParameterAttributeMap.find (pszName);

    if (cii != m_ParameterAttributeMap.end ()) {
        return false;
    }

    m_ParameterAttributeMap[pszName] = pszValue;

    return true;
}

//---------------------------
// addOrUpdateAttribute
//---------------------------
const bool
CParameter::addOrUpdateAttribute (const char *pszName, const char *pszValue) {
    std::map < std::string, std::string >::iterator cii;
    cii = m_ParameterAttributeMap.find (pszName);

    if (cii != m_ParameterAttributeMap.end ()) {
        cii->second = pszValue;
        return true;
    }

    m_ParameterAttributeMap[pszName] = pszValue;

    if (strcmp (pszName, "default") == 0) {
        if (m_iType == CParameter::ENUM) {
            m_iVal = atoi (pszValue);
        }
        else if (m_iType == CParameter::FLOAT) {
            m_fVal = (float)atof (pszValue);
        }
        else if (m_iType == CParameter::INT) {
            m_iVal = atoi (pszValue);
        }
    }


    return true;
}

//---------------------------
// updateAttribute
//---------------------------
const bool
CParameter::updateAttribute (const char *pszName, const char *pszValue) {
    std::map < std::string, std::string >::iterator cii;
    cii = m_ParameterAttributeMap.find (pszName);

    if (cii == m_ParameterAttributeMap.end ()) {
        return false;
    }

    cii->second = pszValue;


    if (strcmp (pszName, "default") == 0) {
        if (m_iType == CParameter::ENUM) {
            int piOut;
            bool bSuccess = getEnum (pszValue, &piOut);
            if (bSuccess) {
                m_bVal = true;
                m_iVal = piOut;
            }
            else {
                m_bVal = false;
            }
        }
        else if (m_iType == CParameter::FLOAT) {
            m_fVal = (float)atof (pszValue);
        }
        else if (m_iType == CParameter::INT) {
            m_iVal = atoi (pszValue);
        }
    }



    return true;
}

//---------------------------
// setType
//---------------------------
const bool
CParameter::setType (const unsigned int iIn) {

    if (m_iType == CParameter::ENUM) {
        delete m_pEnumMap;
    }

    if (iIn == CParameter::ENUM) {
        m_pEnumMap = new std::map < std::string, int >;
        addOrUpdateAttribute ("type", "enum");
    }
    else if (iIn == CParameter::FLOAT) {
        addOrUpdateAttribute ("type", "float");
    }
    else if (iIn == CParameter::INT) {
        addOrUpdateAttribute ("type", "int");
    }
    else if (iIn == CParameter::INT) {
        addOrUpdateAttribute ("type", "undef");
    }

    m_iType = iIn;

    return true;
}

//---------------------------
// getTypeName
//---------------------------
const char *
CParameter::getTypeName () const {
    return getAttribute ("type");
}

//---------------------------
// getTypeId
//---------------------------
const int CParameter::getTypeId () const {
    return m_iType;
}

//---------------------------
// getId
//---------------------------
const int CParameter::getId () const {
    return m_iId;
}

//---------------------------
// getXid
//---------------------------
const int CParameter::getXid () const {
    return m_iXid;
}

//---------------------------
// getMaxInt
//---------------------------
const bool CParameter::getMaxInt (int *piMaxInt) const {
    if (m_bMax == false) {
        return false;
    }
    (*piMaxInt) = m_iMax;

    return true;
}

//---------------------------
// getMinInt
//---------------------------
const bool
CParameter::getMinInt (int *piMinInt) const {
    if (m_bMin == false) {
        return false;
    }
    (*piMinInt) = m_iMin;

    return true;
}

//---------------------------
// getDefaultInt
//---------------------------
const bool
CParameter::getDefaultInt (int *piDefaultInt) const {
    if (m_bVal == false) {
        return false;
    }
    (*piDefaultInt) = m_iVal;

    return true;
}

//---------------------------
// getMaxFloat
//---------------------------
const bool
CParameter::getMaxFloat (float *piMaxFloat) const {
    if (m_bMax == false) {
        return false;
    }
    (*piMaxFloat) = m_fMax;

    return true;
}

//---------------------------
// getMinFloat
//---------------------------
const bool
CParameter::getMinFloat (float *piMinFloat) const {
    if (m_bMin == false) {
        return false;
    }
    (*piMinFloat) = m_fMin;

    return true;
}

//---------------------------
// getDefaultFloat
//---------------------------
const bool
CParameter::getDefaultFloat (float *piDefaultFloat) const {
    if (m_bVal == false) {
        return false;
    }
    (*piDefaultFloat) = m_fVal;

    return true;
}

