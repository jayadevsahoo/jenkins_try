#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this ork may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: checksum.cc - POST Applications Checksum Tests
//
//---------------------------------------------------------------------
//@ Interface-Description
//  >Von
//
//  This module contains the individual checksum tests of the FLASH
//  eeprom, and the checksum test of the boot kernel eprom.  The information
//  for each test comes from externally defined structures located in
//  fixed locations in memory.
//
//  Free Functions:
//     ComputeChecksum
//     FlashChecksumTest
//     BootEpromTest
//  >Voff
//---------------------------------------------------------------------
//@ Rationale
//    This implements the APPLICATIONS checksum test as required by POST.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  >Von
//    There is 2MB of Flash on the BD CPU and 4MB FLASH memory on the
//    GUI that together contain the application software of the
//    ventilator.   POST and Safety-Net use an ADDITIVE checksum
//    system to verify the integrity of the application.
//
//    The POST checksum tests are divided into 2 sections.  The
//    BootEpromTest checksums only the Kernel EPROM, the
//    FlashChecksumTest verifies the application. The Flash tests
//    are broken up into many blocks because of how the watchdog test
//    interacts with this test.   By design, the watchdog test will
//    interrupt one of the Flash tests during one of its passes.
//    This allows reentry into the Flash checksum test instead of
//    restarting from the first block after the watchdog fires for
//    its test.
//
//    It is possible for one CPU to fail its checksum and force a
//    download, while the other CPU remains intact.  The bootstrap
//    downloader is designed to run independently on each CPU, allowing
//    an individual download of either the GUI or BD application from
//    the service laptop.  If the application (flash) checksum fails
//    on either CPU, the Kernel will invoke the bootstrap loader on
//    that CPU.
//
//    There is a header block at the beginning of Flash that contains
//    the amount of Flash that is programmed, as well as the checksum
//    table. POST will checksum the given amount of memory and will not
//    access portions of Flash that have not been programmed. The
//    header block is not included in the checksum.
//
//    The checksum test is broken into 16 separate tests, a test for
//    each of the checksumed blocks of code.   A simple additive
//    checksum algorithm of the contents of each location of memory is
//    used.
//
//    Breaking up this test into several tests was done for the
//    purposes of the watchdog timer test.  In an effort to speed the
//    Kernel testing, the second pass of the watchdog timer test will
//    bypass the  checksummed blocks that were computed in the first
//    pass.  At the start of each checksum  test, a pointer to the test
//    routine in progress is put into NOVRAM (lastTestInProgress).
//    The watchdog timer test will use this pointer to jump to the last
//    test that was in  progress when the watchdog timer hits.  The
//    added design complexity here can save up to 250ms when testing
//    the watchdog circuit.
//
//    If there are no checksum records defined then the Flash checksum
//    test fails and initiates a download.
//
//    The Flash checksum test initializes the Flash memory into read
//    mode since a processor reset could leave the memory in the
//    programming mode.  The downloader application in Kernel EPROM
//    is responsible for setting the Flash in program mode when it
//    downloads the application.
//
//  >Voff
//---------------------------------------------------------------------
//@ Fault Handling
//    Faults during POST cause the test in progress to fail.
//---------------------------------------------------------------------
//@ Restrictions
//    None
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/checksum.ccv   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 003    By: Gary Cederquist  Date: 09-SEP-1997  DR Number: 2471
//    Project: Sigma (R8027)
//    Description:
//       Conditionally included misc.hh for DEBUG only.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "kpost.hh"
#include "kernel.hh"
#include "postnv.hh"
#include "cktable.hh"
#include "checksum.hh"
#include "kernex.hh"
#include "stdio.h"
#include "string.h"
#include "CpuDevice.hh"
#include "Post_Library.hh"
#include "MemoryMap.hh"

#if defined(SIGMA_DEBUG)
# include "misc.hh"
#endif


//=====================================================================
//
//  Forward Declarations
//
//=====================================================================
static Boolean  ComputeChecksum(  const ChecksumStruct & rChecksumData
                                , Boolean strobeWatchdog);

static void     InitializeFlashInReadMode(void);

#if defined(SIGMA_UNIT_TEST)
static Criticality  ChecksumUnitTest(void);
#endif

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: AdditiveChecksum - perform a 32 bit additive checksum
//
//@ Interface-Description
//
//  This routine performs an 32 bit additive check sum over the specified
//  block of memory.  This function is optimized to checksum regions
//  that are a multiple of 64 bytes long.  It enables data caching for
//  the duration of the test and then disables it prior to exiting.
//  This function is not portable to different CPUs.
//
//  Note: This routine can only be used in supervisor mode.
//
//    Inputs:  AdditiveCheckSum(  Uint32 *starting_address
//                              , Uint32 number_of_bytes
//                              , Boolean strobeWatchdog
//                              , Uint32 *result)
//
//
//     starting_address  -  the beginning of the memory block to test
//     number_of_bytes   -  the number of bytes to test (multiple of 64)
//                          not greater than 512K bytes.
//     strobeWatchdog    -  when TRUE, will strobe watchdog during checksum
//     *result           -  location to which the result is written
//
//
//    Outputs: *result   -  the result of the CheckSum
//
//    Returns: TRUE - failed   FALSE - passed
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This routine performs a 32 bit additive Check Sum calculation with
//  loss of carryout information.  The routine validates the input
//  values and then runs a loop that adds each location to an accumulator.
//  The accumulator is a 32 bit register.  No attention is paid to
//  whether the accumulator overflows during this test.  The value that
//  remains in the accumulator at the end of the routine is written to
//  the specified location.  For this test to complete successfully,
//  the data cache must be operational.
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================

Boolean
AdditiveChecksum(  const Uint32 * pStart
                 , register Uint32 nbytes
                 , register Boolean strobeWatchdog
                 , Uint32 * pResult)
{
	//TODO E600 porting
/*

UNIT_TEST_POINT(POSTUT_16)

    if ( (nbytes % 64) || (nbytes > 0x80000) || (nbytes < 64) )
    {
       // $[TI1.1]
       return TRUE;
    }
    // $[TI1.2]

    register Uint32 checksum = 0;
    register const Uint32 * pEnd = pStart + nbytes/sizeof(Uint);
    register const Uint32 * pLoc = pStart;

    //  save the current cache control register
    register Uint32 cacr  = asm(" movec.l  cacr,d0 ");
    //  enable code and data cache
    asm(" move.l  #$80008000,d0 "," movec.l  d0,cacr");

    for ( ; pLoc < pEnd; )
    {
        if ( strobeWatchdog )
        {
            // $[TI2.1]
            CpuDevice::StrobeWatchdog();
        }
        // $[TI2.2]
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
        checksum += *pLoc++;
    }

    asm(" movec.l  `cacr`,cacr ");   // restore cacr

UNIT_TEST_POINT(POSTUT_16_1)
    *pResult = checksum;
*/

    return FALSE;   // success
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: ComputeChecksum
//
//@ Interface-Description
//  >Von
//    This computes an additive checksum of a block of memory.
//    This function provides an interface to the AdditiveChecksum
//    function which has been optimized for the 68040 architecture.
//    This function is the CPU portable interface to AdditiveChecksum.
//
//    Inputs:  pointer to a ChecksumStruct containing the address
//             of the memory block, its length and expected checksum
//
//    Outputs: TRUE - failed, FALSE - success
//
//    An checksumStruct is used to retrieve the length,
//    starting address, and expected checksum.  An additive checksum
//    is computed and compared to the expected checksum.
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

Boolean
ComputeChecksum(const ChecksumStruct & rChecksumRec, Boolean strobeWatchdog)
{
   Uint32 length   = rChecksumRec.length;
   Uint32 *pStart  = rChecksumRec.pStartAddress;
   Uint32 checksum = rChecksumRec.additiveChecksum;


   // Compute the checksum over the range.  Note: this routine is optimized
   // to operate over multiples of 64 bytes.  If the length is not a multiple
   // the status returns failed (0).

   Uint32 computedChecksum;
   Boolean failed = AdditiveChecksum(  pStart
                                     , length
                                     , strobeWatchdog
                                     , &computedChecksum );

   // $[TI1.1]  $[TI1.2]
   return ((computedChecksum != checksum) || failed);
}



//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: FlashChecksumTest
//
//@ Interface-Description
//  >Von
//    The applications software is verified by computing checksums
//    and comparing the results to a table of ChecksumStruct in Flash.
//    $[11036]
//
//    INPUTS:  NONE
//
//    OUTPUTS: PASS, MAJOR
//
//    This test calls ComputeChecksum with a reference to a
//    ChecksumStruct for the memory block under test.
//
//    The index is stored in ChecksumIndexInProgress in POST's NOVRAM.
//    This test is designed to be interrupted during the watchdog timer
//    test, as an expected watchdog will be generated sometime during
//    this test.  ChecksumIndexInProgress is retrieved from NOVRAM in
//    this case and continues testing where it was interrupted.
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    The Kernel software must be valid in order to perform the
//    checksum test of the application.  A successful execution of the
//    integer unit test and a small amount of Kernel DRAM memory is
//    required to compute a checksum.  The application software must
//    provide a checksum structure at a known location in Flash.
//
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

Criticality
FlashChecksumTest(void)
{
   // Flash may be initialized in program mode.  Set to read mode.
   InitializeFlashInReadMode();

#ifdef SIGMA_UNIT_TEST
   //  must return PASSED for unit test since the code runs in
   //  emulator memory - i.e. checksum test will not pass
   return ChecksumUnitTest();
#else
   // check input "parameters"
   if (   PEePromChecksums->numChecksums < 1
       || PEePromChecksums->numChecksums > NUMBER_OF_CHECKSUM_BLOCKS )
   {
       // $[TI1.1]
       return MAJOR;
   }
   // $[TI1.2]


   // Verify checksumIndexInProgress is valid.  If it is out of
   // range at this point an error has occurred.  Set the
   // checksumIndexInProgress back to zero so the next time this test
   // is run it will start over from block 0.

   // reference assigned for better readability
   Uint8 & ix = PKernelNovram->checksumIndexInProgress;
   const EepromStruct * pChecksums = PEePromChecksums;

   if (ix > NUMBER_OF_CHECKSUM_BLOCKS)
   {
      // $[TI2.1]
      ix = 0;
      return MAJOR;
   }
   // $[TI2.2]

   for (; ix < pChecksums->numChecksums; ix++)
   {
       if ( ComputeChecksum( pChecksums->checksumBlock[ix],
                             !PKernelNovram->testingWatchdog ) )
       {
           // $[TI3.1]
           ix = 0;
           return MAJOR;
       }
       // $[TI3.2]
   }

   //  Clear the checksumIndexInProgress only if the watchdog test
   //  is not active.  This will ensure the checksum test does not
   //  run again if it does finish all blocks during the first pass.
   if ( !PKernelNovram->testingWatchdog )
   {
       // $[TI4.1]
       ix = 0;
   }
   // $[TI4.2]

   return PASSED;
#endif // SIGMA_UNIT_TEST
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: BootEpromTest (void)
//
//@ Interface-Description
//  >Von
//    Computes a checksum of the BOOT EPROM and compares it to a value
//    at a known fixed location.  $[11011]
//
//    INPUTS:  NONE
//    OUTPUTS: PASSED, MAJOR
//
//    TECHNIQUE
//
//    The Sigma POST Kernel consists of a 512K byte EPROM that contains
//    The Kernel software.  The EPROM test consists of reading the
//    checksum from a fixed location at the end of the Kernel EPROM space.
//    The checksum operation over the entire EPROM should return the
//    value of the EPROM checksum added to itself.  A ChecksumStruct is
//    constructed and passed to the ComputeChecksum function.
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    Free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    The basic ability to read EPROM and compute a checksum using
//    stack memory is assumed.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

Criticality
BootEpromTest(void)
{
	//TODO E600 if needed!!
	return PASSED; //this is temp

/*
#ifdef SIGMA_DEVELOPMENT
   return PASSED;
#endif

#if defined( SIGMA_UNIT_TEST )
   return ChecksumUnitTest();
#elif defined( SIGMA_PRODUCTION )
   ChecksumStruct  checksumRec;

   checksumRec.length           = BOOT_PROM_LENGTH;
   checksumRec.pStartAddress    = (Uint32*)BOOT_PROM_BASE;
   checksumRec.additiveChecksum = *BOOT_PROM_CHECKSUM + *BOOT_PROM_CHECKSUM;

   if ( ComputeChecksum(checksumRec, TRUE) )
   {
      // $[TI1.1]
      return MAJOR;
   }
   // $[TI1.2]

   return PASSED;

#endif // SIGMA_PRODUCTION
*/
}

#ifdef SIGMA_UNIT_TEST

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: InitUnitTestChecksums (void) - UNIT TEST ONLY
//
//@ Interface-Description
//  >Von
//    Initializes the EepromStruct used by ChecksumUnitTest.
//
//    INPUTS:  NONE
//    OUTPUTS: NONE
//
//    TECHNIQUE
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    Free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

EepromStruct  UnitTestChecksums;


// Create some dummy checksum data for use by the checksum test.
// pStartAddress points to unused 4MB-256K memory

void InitUnitTestChecksums(void)
{
   EepromStruct * pUnitTestChecksums  = &UnitTestChecksums;
   pUnitTestChecksums->numChecksums = NUMBER_OF_CHECKSUM_BLOCKS;
   Uint32 & i = pUnitTestChecksums->numChecksums = 0;

   const Uint32   blockLength     = 0x40000;

   for (  Uint32 *pStartAddress=(Uint32*)0x40000
        ; pStartAddress < (Uint32*)0x80000
        ; pStartAddress += blockLength/sizeof(Uint32))
   {
      pUnitTestChecksums->checksumBlock[i].length           = blockLength;
      pUnitTestChecksums->checksumBlock[i].pStartAddress    = pStartAddress;
      pUnitTestChecksums->checksumBlock[i].additiveChecksum = 0x0;
      pUnitTestChecksums->checksumBlock[i].crc32Checksum    = 0xFFFFFFFF;

#ifdef E600_840_TEMP_REMOVED
      ClearMemory(pStartAddress, blockLength);
#endif
      pUnitTestChecksums->numChecksums++;
   }

}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: ChecksumUnitTest (void) - UNIT TEST ONLY
//
//@ Interface-Description
//  >Von
//    Computes a checksum of an initialized portion of DRAM for
//    unit testing the Additive Checksum function
//
//    INPUTS:  NONE
//    OUTPUTS: PASSED, MAJOR
//
//    TECHNIQUE
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    Free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    The basic ability to read EPROM and compute a checksum using
//    stack memory is assumed.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

Criticality
ChecksumUnitTest(void)
{
   InitUnitTestChecksums();
   EepromStruct * pUnitTestChecksums  = &UnitTestChecksums;

   if (   pUnitTestChecksums->numChecksums < 1
       || pUnitTestChecksums->numChecksums > NUMBER_OF_CHECKSUM_BLOCKS )
   {
       return MAJOR;
   }

   for (Uint32 ix=0; ix<pUnitTestChecksums->numChecksums; ix++)
   {
      if ( ComputeChecksum(pUnitTestChecksums->checksumBlock[ix],TRUE) )
      {
          return MAJOR;
      }
   }
   return PASSED;

}


#endif // SIGMA_UNIT_TEST


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: InitializeFlashInReadMode (void)
//
//@ Interface-Description
//
//    Puts flash memory in read mode.  At power up it is possible for
//    flash to be in program mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Writes a command code to each bank of flash
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

//  FLASH command to reset the block into read mode
#define READ_ARRAY   0x00ff00ff

//  FLASH memory structure - each block is a separately
//  programmable and must be set into the read mode by
//  InitializeFlashInReadMode.

//  Note: This structure must change if the flash layout changes.

struct FlashBlocks
{
    Uint32  block0[64*256];
    Uint32  block1[32*256];
    Uint32  block2[32*256];
    Uint32  block3[384*256];
    Uint32  block4[512*256];
    Uint32  block5[512*256];
    Uint32  block6[512*256];

    // second bank for GUI only
    Uint32  block7[64*256];
    Uint32  block8[32*256];
    Uint32  block9[32*256];
    Uint32  block10[384*256];
    Uint32  block11[512*256];
    Uint32  block12[512*256];
    Uint32  block13[512*256];
};

static FlashBlocks * const PStartFlash = (FlashBlocks*)FLASH_BASE;

static void
InitializeFlashInReadMode(void)
{
    // Vpp is disabled by a reset so there is no need to set it here

    // issue READ_ARRAY to each block and each interleave

    *PStartFlash->block0 = READ_ARRAY;
    *(PStartFlash->block0+1) = READ_ARRAY;
    *PStartFlash->block1 = READ_ARRAY;
    *(PStartFlash->block1+1) = READ_ARRAY;
    *PStartFlash->block2 = READ_ARRAY;
    *(PStartFlash->block2+1) = READ_ARRAY;
    *PStartFlash->block3 = READ_ARRAY;
    *(PStartFlash->block3+1) = READ_ARRAY;
    *PStartFlash->block4 = READ_ARRAY;
    *(PStartFlash->block4+1) = READ_ARRAY;
    *PStartFlash->block5 = READ_ARRAY;
    *(PStartFlash->block5+1) = READ_ARRAY;
    *PStartFlash->block6 = READ_ARRAY;
    *(PStartFlash->block6+1) = READ_ARRAY;

    if ( CpuDevice::IsGuiCpu() )
    {
        // $[TI1.1]
        *PStartFlash->block7 = READ_ARRAY;
        *(PStartFlash->block7+1) = READ_ARRAY;
        *PStartFlash->block8 = READ_ARRAY;
        *(PStartFlash->block8+1) = READ_ARRAY;
        *PStartFlash->block9 = READ_ARRAY;
        *(PStartFlash->block9+1) = READ_ARRAY;
        *PStartFlash->block10 = READ_ARRAY;
        *(PStartFlash->block10+1) = READ_ARRAY;
        *PStartFlash->block11 = READ_ARRAY;
        *(PStartFlash->block11+1) = READ_ARRAY;
        *PStartFlash->block12 = READ_ARRAY;
        *(PStartFlash->block12+1) = READ_ARRAY;
        *PStartFlash->block13 = READ_ARRAY;
        *(PStartFlash->block13+1) = READ_ARRAY;
    }
    // $[TI1.2]
}
