#ifndef TimerTarget_HH
#define TimerTarget_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
// Class: TimerTarget - Base class for all timer target objects.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TimerTarget.hhv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"

//@ Usage-Classes
class IntervalTimer;
//@ End-Usage


class TimerTarget 
{
  public:
    TimerTarget(void);
    virtual ~TimerTarget(void);
    virtual void timeUpHappened(const IntervalTimer& timer) = 0;
    static void SoftFault(const SoftFaultID softFaultID,
				  const Uint32      lineNumber,
				  const char*       pFileName  = NULL,
				  const char*       pPredicate = NULL);

  protected:

  private:
    TimerTarget(const TimerTarget&);          // Declared but not implemented
    void operator=(const TimerTarget&);   // Declared but not implemented

};


#endif // TimerTarget_HH 
