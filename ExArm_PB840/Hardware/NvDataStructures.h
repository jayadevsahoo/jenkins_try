
//#############################################################################
//
//  NvDataStructures.h - Header file containg structures for NvDataStoreThread_t class.
//
//#############################################################################

#ifndef NV_DATA_STRUCTURES_H

#define NV_DATA_STRUCTURES_H

#define NV_DATA_FILE_VERSION_SIZE               (18)

#define MAX_SETTINGS_ENTRIES                    (250)
#define MAX_CALIBRATION_ENTRIES                 (150)
#define MAX_CONFIGURATION_ENTRIES               (150)

#define NV_DATA_ENTRY_SIZE                      (8)


struct NvDataSettingsFile_t
{
    char        FileVersion[NV_DATA_FILE_VERSION_SIZE];
    uint32_t    NumberOfEntries;
    uint16_t    SettingsVersion;
    uint16_t    Spare;                              // This aligns the entries per the earlier stored data
    uint8_t     Entry[MAX_SETTINGS_ENTRIES][NV_DATA_ENTRY_SIZE];
    uint32_t    Crc;
};

struct NvDataCalFile_t
{
    char        FileVersion[NV_DATA_FILE_VERSION_SIZE];
    uint32_t    NumberOfEntries;
    uint32_t    Filler;                             // This aligns the entries per the earlier stored data
    uint8_t     Entry[MAX_CALIBRATION_ENTRIES][NV_DATA_ENTRY_SIZE];
    uint32_t    Crc;
};

struct NvDataConfigFile_t
{
    char        FileVersion[NV_DATA_FILE_VERSION_SIZE];
    uint32_t    NumberOfEntries;
    uint32_t    Filler;                             // This aligns the entries per the earlier stored data
    uint8_t     Entry[MAX_CONFIGURATION_ENTRIES][NV_DATA_ENTRY_SIZE];
    uint32_t    Crc;
};

// The dual store handles items that must be stored in CF and EEPROM, for example, the hour meters.
static const int MAX_DATA_DUAL_STORE_ENTRIES = 4;
static const int NV_DUAL_DATA_FILE_VERSION_SIZE = 8;

struct NvDataDualFile_t
{
    char        FileVersion[NV_DUAL_DATA_FILE_VERSION_SIZE];
    uint32_t    NumberOfEntries;
    uint32_t    Filler;                             // This aligns the entries per the earlier stored data
    uint8_t     Entry[MAX_DATA_DUAL_STORE_ENTRIES][NV_DATA_ENTRY_SIZE];
    uint32_t    Crc;
};



#endif

