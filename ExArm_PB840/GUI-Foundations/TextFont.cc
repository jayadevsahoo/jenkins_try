#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TextFont - The TextFont struct hides font data from the rest of
// the system.  
//---------------------------------------------------------------------
//@ Interface-Description
// This static class initializes font data.  Any classes needing font info can
// get it here by specifying the size and style of the font.  Valid sizes are
// 6, 8, 10, 12, 14, 18 and 24 point.  Valid styles are THIN, NORMAL and
// ITALIC.
//
// The Initialize() method must be called before using any other methods of
// this class.
//
// The other methods provide information about a specified font.  GetFont()
// returns a font handle, GetAscent() the "ascent" of a font, GetDescent()
// the "descent" of a font and GetWidth() the maximum width of a numeric
// character in the font.
//---------------------------------------------------------------------
//@ Rationale
// Insulates the NumericField and TextField classes from the handling
// of fonts.
//---------------------------------------------------------------------
//@ Implementation-Description
// Information about each font is stored in an array, AllFonts_[].  Each
// entry is a FontInfo structure.  The first member of the structure is
// a handle to a specific font and this handle is used in the Initialize()
// method to fill in the other members of the structure with the appropriate
// values such as the ascent, descent and width of the font.
//
// The other methods simply return FontInfo structure information based
// on a font size and style.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/TextFont.ccv   25.0.4.0   19 Nov 2013 14:11:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: gdc    Date:  03-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Improved font metrics.
// 
//  Revision: 013   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 012   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 011   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 010  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 009  By:  clw    Date:  28-May-98    DR Number:
//    Project:  Japanese
//    Description:
//      Updated GetStringSize method to support Japanese style.  Updated Initialize method
//      to set ascent, descent, etc. for Japanese fonts to the corresponding values for
//      the bold english font of the same point size.
//      Set up all sizes of Japanese fonts.
//		Set up simulated 16-point non-japanese fonts, all 3 styles.  These are currently
//      linked to 14-point fonts because we don't yet have the 16-point versions.
//		Added support for more Japanese fonts.  Japanese is now supported for all
//      sizes except 6 point.
//		Added support for Japanese fonts.  Note that currently the Japanese is not
//      supported on the SUN platform.  This initial release only adds 16-point
//      Japanese, and the second "b" set is really 14-point. It is installed for testing only. 
//
//  Revision: 008  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 007  By:  hhd	   Date:  30-Sep-97    DR Number: 2512
//    Project:  Sigma (R8027)
//    Description:
//		Compiled definition of method GetCommaHeightOffset() only for TARGET. 
//
//  Revision: 006  By:  hhd	   Date:  24-Sep-97    DR Number: 2512
//    Project:  Sigma (R8027)
//    Description:
//		Defined 'commaHeightOffset' value in the AllFont_ entries.
//		Added a query method for this value, to be used by the NumericField class .
//		Defined new constants.
//
//  Revision: 005  By:  hhd	   Date:  18-Sep-97    DR Number: 2512
//    Project:  Sigma (R8027)
//    Description:
//		Added a 'height' variable to hold the height of each font and
//		a query method for this value, to be used by the NumericField class .
//
//  Revision: 004  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 003  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 002  By: yyy      Date: 13-Jun-1995  DCR Number: 476
//    Project:  Sigma (R8027)
//    Description:
//      Added method GetStringSize() which will calculate the height and width of the
//      string passed to this method.  Due to the addition of this method, the SUN
//      version of FontInfo structure has one more field XFontStruc *xId.  This field
//      shall remember the font id for this structure and be used later by 
//      GetStringSize().
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "TextFont.hh"
#include "VgaFontData.hh"
#include "VgaRectangle.hh"
#include <iostream>
using namespace std;


//@ Code...

//
// Initialization of static data members...
//

const FontID TextFont::INVALID_FONT_ID = (FontID) -1;

// This is an arbitary x location passed to VGATextRect to ensure the x
// coordinate stays positive during the rendering.
const Uint16 TextFont::VGA_X_START = 100;

// This is an arbitary y location passed to VGATextRect to ensure the y
// coordinate stays positive during the rendering.
const Uint16 TextFont::VGA_Y_START = 100;

//
// A "4" is used to determine the width of the digit, because it is the largest
// digit.
//
const char TextFont::WIDTH_CHAR[2] = "4";

//
// A "|" is used to determine the height of the digit, because it is the
// tallest digit.
//
const char TextFont::HEIGHT_CHAR[2] = "|";

// AllFonts_ array: this array captures font information for each possible
// point size.  There are NUM_STYLE entries for each point size, one for
// each style (currently thin, normal, italic, and Japanese).
//
// $[00457] The font set shall support Bold, Italics and normal styles.
//

// ========================================================
// AllFonts_ initialization:
// Note that the id field (1st item) is set to INVALID_FONT_ID to indicate that
// a font is not supported.
// Note that FontInfo item 2 through 6 are initialized to 0.  But these items
// will be set to the appropriated value during TextFont::Initialize().  This
// info consists of ascent, descent, height width for the 
// supported font.

FontInfo TextFont::AllFonts_[] =
{
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 0 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 0 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 0 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 0 japanese A
    
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 1 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 1 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 1 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 1 japanese A
    
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 2 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 2 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 2 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 2 japanese A
    
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 3 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 3 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 3 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 3 japanese A
    
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 4 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 4 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 4 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 4 japanese A
    
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 5 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 5 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 5 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 5 japanese A
    
	{ VgaFontData::THIN_6_INDEX, 0, 0, 0, 0 },		// 6 thin
	{ VgaFontData::NORMAL_6_INDEX, 0, 0, 0, 0 },	// 6 normal
	{ VgaFontData::ITALIC_6_INDEX, 0, 0, 0, 0 },	// 6 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 6 japanese A
	
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 7 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 7 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 7 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 7 japanese A

	{ VgaFontData::THIN_8_INDEX, 0, 0, 0, 0 },		// 8 thin
	{ VgaFontData::NORMAL_8_INDEX, 0, 0, 0, 0 },	// 8 normal
	{ VgaFontData::ITALIC_8_INDEX, 0, 0, 0, 0 },	// 8 italic
    { VgaFontData::JAPANESE_8A_INDEX,0,0,0,0 },	// 8 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 9 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 9 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 9 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 9 japanese A

	{ VgaFontData::THIN_10_INDEX, 0, 0, 0, 0 },		// 10 thin
	{ VgaFontData::NORMAL_10_INDEX, 0, 0, 0, 0 },	// 10 normal
	{ VgaFontData::ITALIC_10_INDEX, 0, 0, 0, 0 },	// 10 italic
    { VgaFontData::JAPANESE_10A_INDEX,0,0,0,0 },	// 10 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 11 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 11 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 11 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 11 japanese A

	{ VgaFontData::THIN_12_INDEX, 0, 0, 0, 0 },		// 12 thin
	{ VgaFontData::NORMAL_12_INDEX, 0, 0, 0, 0 },	// 12 normal
	{ VgaFontData::ITALIC_12_INDEX, 0, 0, 0, 0 },	// 12 italic
    { VgaFontData::JAPANESE_12A_INDEX,0,0,0,0 },	// 12 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 13 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 13 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 13 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 13 japanese A

	{ VgaFontData::THIN_14_INDEX, 0, 0, 0, 0 },		// 14 thin
	{ VgaFontData::NORMAL_14_INDEX, 0, 0, 0, 0 },	// 14 normal
	{ VgaFontData::ITALIC_14_INDEX, 0, 0, 0, 0 },	// 14 italic
    { VgaFontData::JAPANESE_14A_INDEX, 0, 0, 0, 0 },	// 14 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 15 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 15 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 15 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 15 japanese A

    { VgaFontData::THIN_16_INDEX,0,0,0,0 },			// 16 thin
	{ VgaFontData::NORMAL_16_INDEX, 0, 0, 0, 0 },	// 16 normal 
    { VgaFontData::ITALIC_16_INDEX,0,0,0,0 },			// 16 italic
    { VgaFontData::JAPANESE_16A_INDEX, 0, 0, 0, 0 },	// 16 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 17 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 17 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 17 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 17 japanese A

	{ VgaFontData::THIN_18_INDEX, 0, 0, 0, 0 },		// 18 thin
	{ VgaFontData::NORMAL_18_INDEX, 0, 0, 0, 0 },	// 18 normal
	{ VgaFontData::ITALIC_18_INDEX, 0, 0, 0, 0 },	// 18 italic
    { VgaFontData::JAPANESE_18A_INDEX, 0, 0, 0, 0 },	// 18 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 19 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 19 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 19 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 19 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 20 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 20 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 20 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 20 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 21 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 21 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 21 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 21 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 22 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 22 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 22 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 22 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 23 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 23 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 23 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 23 japanese A

	{ VgaFontData::THIN_24_INDEX, 0, 0, 0, 0 },		// 24 thin
	{ VgaFontData::NORMAL_24_INDEX, 0, 0, 0, 0 },	// 24 normal
	{ VgaFontData::ITALIC_24_INDEX, 0, 0, 0, 0 },	// 24 italic
    { VgaFontData::JAPANESE_24A_INDEX,0,0,0,0 },	// 24 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 25 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 25 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 25 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 25 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 26 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 26 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 26 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 26 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 27 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 27 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 27 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 27 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 28 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 28 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 28 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 28 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 29 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 29 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 29 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 29 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 30 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 30 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 30 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 30 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 31 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 31 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 31 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 31 japanese A

	{ VgaFontData::THIN_32_INDEX, 0, 0, 0, 0 },		// 32 thin
	{ VgaFontData::NORMAL_32_INDEX, 0, 0, 0, 0 },	// 32 normal
	{ VgaFontData::ITALIC_32_INDEX, 0, 0, 0, 0 },	// 32 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 32 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 33 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 33 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 33 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 33 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 34 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 34 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 34 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 34 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 35 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 35 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 35 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 35 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 36 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 36 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 36 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 36 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 37 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 37 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 37 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 37 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 38 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 38 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 38 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 38 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 39 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 39 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 39 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 39 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 40 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 40 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 40 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 40 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 41 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 41 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 41 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 41 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 42 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 42 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 42 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 42 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 43 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 43 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 43 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 43 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 44 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 44 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 44 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 44 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 45 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 45 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 45 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 45 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 46 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 46 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 46 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 46 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 47 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 47 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 47 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 47 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 48 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 48 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 48 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 48 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 49 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 49 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 49 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 49 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 50 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 50 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 50 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 50 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 51 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 51 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 51 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 51 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 52 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 52 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 52 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 52 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 53 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 53 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 53 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 53 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 54 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 54 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 54 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 54 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 55 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 55 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 55 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 55 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 56 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 56 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 56 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 56 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 57 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 57 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 57 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 57 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 58 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 58 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 58 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 58 japanese A

    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 59 thin
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 59 normal
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 59 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 59 japanese A

	{ VgaFontData::THIN_60_INDEX, 0, 0, 0, 0 },		// 60 thin
	{ VgaFontData::NORMAL_60_INDEX, 0, 0, 0, 0 },	// 60 normal
	{ VgaFontData::ITALIC_60_INDEX, 0, 0, 0, 0 },	// 60 italic
    { TextFont::INVALID_FONT_ID,0,0,0,0 },	// 60 japanese A

};


// NumFonts initialization: is set to the number of enties in AllFonts_[].
// This does not indicate the number of available fonts - it only indicates
// the number of slots in AllFonts[], not how many slots are in use.

const Uint16 TextFont::NUM_FONTS =
			sizeof (TextFont::AllFonts_) / sizeof (TextFont::AllFonts_[0]);



//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize		[static]
//---------------------------------------------------------------------
//@ Interface-Description
// This function must be called during initialization.  It completes the
// initialization of the array-of-structs defined and initialized above.  The
// additional information consists of the maximum width, height and descent
// for each font size.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// For the supported font type, this function sets the appropriated values for
// the ascent, descent, width and height members of the AllFonts_ array. 
//
// It uses the VgaFont::TextRect() function to determine the screen size of a
// typical numeric digit.  For this purpose, a string containing the single
// digit "4" is sized, since 4 is the largest digit.  This information is used
// to set the width and ascent members of the FontInfo structs
// comprising the AllFonts_ array.  The descent member value is computed from
// the difference in height of the digit "4" and the "|" characters.  The height
// member value is the sum of the ascent and the descent. 
//---------------------------------------------------------------------
//@ PreCondition
//  Calculated font number should be equal to the (MAX_SIZE+1) * NUM_STYLE.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TextFont::Initialize(void)
{
	CALL_TRACE("TextFont::Initialize(void)");

	static Boolean isInitialized_ = FALSE;

	if (isInitialized_)
	{
		return;
	}
	isInitialized_ = TRUE;

	// The number of entries per font size is the number of styles.
	// MAX_SIZE is the largest point size supported.  The table must
	// include an entry for any possible combo of size and style.
	// Note that the number of sizes is MAX_SIZE+1 due to the 0 entry.
	CLASS_PRE_CONDITION(NUM_FONTS == ((MAX_SIZE+1) * NUM_STYLE ) );


	//
	// Loop thru the AllFonts_ array, filling out info not specified in
	// initializer.  This info consists of the screen width and height of a
	// typical digit and the maximum height of a descender.
	// The WidthChar_ is used to determine the digit size, because it is the largest
	// digit.  Its width is used to size a NumericField based on the max number of
	// digits.  The height of a descender for this font is calculated by determining
	// the height of a HEIGHT_CHAR character, and subtracting the height of the
	// WidthChar_. This is due to the fact that their difference in height 
	// represent the tallest descender.
	//

	for (Int i = 0; i < NUM_FONTS; i++)
	{	//													$[TI1]
		// Check if a FontID is specified
		Int fontId = TextFont::AllFonts_[i].id;
		if (fontId != TextFont::INVALID_FONT_ID)
		{	//												$[TI1.1]

			// Based on the structure of the AllFonts table, we can deduce that the
			//  current style is the remainder when the current index is divided
			//  by the number of styles.
			Int style = i % NUM_STYLE;

			// For japanese, the technique used below to calculate max width, height, and
			//  so on does not apply.  Therefore, we simply use the values calculated for
			//  the NORMAL (really bold) non-japanese font of the same point size.

			if (style == JAPANESE)
			{	//												$[TI1.1.1]

				// NOTE: this code assumes that the NORMAL (bold english) font exists,
				// and that the NORMAL style precedes the JAPANESE style in the
				// Style enum.
				SAFE_CLASS_ASSERTION(NORMAL<JAPANESE);
				CLASS_ASSERTION(AllFonts_[(i-JAPANESE)+NORMAL].id != INVALID_FONT_ID);
				
				TextFont::AllFonts_[i].width =
					TextFont::AllFonts_[(i-JAPANESE)+NORMAL].width;
					
				TextFont::AllFonts_[i].ascent =
					TextFont::AllFonts_[(i-JAPANESE)+NORMAL].ascent;

				TextFont::AllFonts_[i].descent =
					TextFont::AllFonts_[(i-JAPANESE)+NORMAL].descent;

				TextFont::AllFonts_[i].height =
					TextFont::AllFonts_[(i-JAPANESE)+NORMAL].height;
			}
			else
			{	//												$[TI1.1.2]
				// the improved method for determining font metrics uses metrics
				// initialized in VgaFontData	
				TextFont::AllFonts_[i].width   = VgaFontData::FontMetrics[fontId].width;
				TextFont::AllFonts_[i].height  = VgaFontData::FontMetrics[fontId].height;
				TextFont::AllFonts_[i].ascent  = VgaFontData::FontMetrics[fontId].ascent;
				TextFont::AllFonts_[i].descent = VgaFontData::FontMetrics[fontId].descent;
				TextFont::AllFonts_[i].maxAscent  = VgaFontData::FontMetrics[fontId].maxAscent;
				TextFont::AllFonts_[i].maxDescent = VgaFontData::FontMetrics[fontId].maxDescent;
				
			}
		}
		//													$[TI1.2]
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetFont		[static]
//
//@ Interface-Description
// This function figures out what font to use to render text based on the
// passed point "point" and "style", and returns the ID of that font.
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the "point" and "style" to find the index into the AllFonts_ array.
// The FontInfo structure at this index contains the fontId value which is
// then returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	Verify that the font value fetched from the AllFonts_ array is not
//	INVALID_FONT_ID.
//@ End-Method
//=====================================================================

FontID
TextFont::GetFont(TextFont::Size point, TextFont::Style style)
{
	CALL_TRACE("TextFont::GetFont(TextFont::Size point, TextFont::Style style)");

	//			$[TI1]
	Int fontIndex = getFontIndex_(point, style);
	
	FontID font = TextFont::AllFonts_[fontIndex].id;

	return (font);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAscent		[static]
//
//@ Interface-Description
// Returns the "ascent" of the specified font based on the passed point "point"
// and "style".  The ascent is the number of pixels above the baseline.
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the "point" and "style" to find the index into the AllFonts_ array.
// The FontInfo structure at this index contains the ascent value which is
// then returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	Verify that the font value fetched from the AllFonts_ array is not
//	INVALID_FONT_ID.
//@ End-Method
//=====================================================================

Int
TextFont::GetAscent(TextFont::Size point, TextFont::Style style)
{
	CALL_TRACE("TextFont::GetAscent(TextFont::Size point, TextFont::Style style)");

	//			$[TI1]
	Int ascent;
	Int fontIndex = getFontIndex_(point, style);
	
	FontID font = TextFont::AllFonts_[fontIndex].id;

	// font is -1 for unsupported fonts:	
	CLASS_ASSERTION(font != TextFont::INVALID_FONT_ID);
	
	ascent = TextFont::AllFonts_[fontIndex].ascent;

	return (ascent);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDescent		[static]
//
//@ Interface-Description
// Returns the "descent" of the specified font based on the passed point "point"
// and "style".  Returns the "descent" of the specified font.  The descent is
// the number of pixels that the text can descend below the baseline.
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the "point" and "style" to find the index into the AllFonts_ array.
// The FontInfo structure at this index contains the descent value which is
// then returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	Verify that the font value fetched from the AllFonts_ array is not
//	INVALID_FONT_ID.
//@ End-Method
//=====================================================================

Int
TextFont::GetDescent(TextFont::Size point, TextFont::Style style)
{
	CALL_TRACE("TextFont::GetDescent(TextFont::Size point, TextFont::Style style)");

	//			$[TI1]
	Int descent;
	Int fontIndex = getFontIndex_(point, style);
	
	FontID font = TextFont::AllFonts_[fontIndex].id;

	// font is -1 for unsupported fonts:	
	CLASS_ASSERTION(font != TextFont::INVALID_FONT_ID);

	descent = TextFont::AllFonts_[fontIndex].descent;

	return (descent);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetMaxAscent		[static]
//
//@ Interface-Description
// Returns the maximum "ascent" of the specified font based on the size
// and "style".  The ascent is the number of pixels above the baseline.
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the "point" and "style" to find the index into the AllFonts_ array.
// The FontInfo structure at this index contains the maxAscent value that is
// then returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	Verify that the font value fetched from the AllFonts_ array is not
//	INVALID_FONT_ID.
//@ End-Method
//=====================================================================

Int TextFont::GetMaxAscent(TextFont::Size point, TextFont::Style style)
{
	CALL_TRACE("GetMaxAscent(TextFont::Size point, TextFont::Style style)");

	Int fontIndex = getFontIndex_(point, style);

	FontID font = TextFont::AllFonts_[fontIndex].id;

	// font is -1 for unsupported fonts:	
	CLASS_ASSERTION(font != TextFont::INVALID_FONT_ID);

	return(TextFont::AllFonts_[fontIndex].maxAscent);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetMaxDescent		[static]                      
//
//@ Interface-Description
// Returns the maximum "descent" of the specified font based on the size
// and "style".  Returns the "descent" of the specified font.  The descent is
// the number of pixels that the text can descend below the baseline.
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the "point" and "style" to find the index into the AllFonts_ array.
// The FontInfo structure at this index contains the descent value which is
// then returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	Verify that the font value fetched from the AllFonts_ array is not
//	INVALID_FONT_ID.
//@ End-Method
//=====================================================================

Int TextFont::GetMaxDescent(TextFont::Size point, TextFont::Style style)
{
	CALL_TRACE("TextFont::GetMaxDescent(TextFont::Size point, TextFont::Style style)");

	Int fontIndex = getFontIndex_(point, style);

	FontID font = TextFont::AllFonts_[fontIndex].id;

	// font is -1 for unsupported fonts:	
	CLASS_ASSERTION(font != TextFont::INVALID_FONT_ID);

	return(TextFont::AllFonts_[fontIndex].maxDescent);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetWidth		[static]
//
//@ Interface-Description
// Returns the maximum width of a numeric digit in the specified font, based on
// the passed point "point" and "style".
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the "point" and "style" to find the index into the AllFonts_ array.
// The FontInfo structure at this index contains the width value which is
// then returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	Verify that the font value fetched from the AllFonts_ array is not
//	INVALID_FONT_ID.
//@ End-Method
//=====================================================================

Int
TextFont::GetWidth(TextFont::Size point, TextFont::Style style)
{
	CALL_TRACE("TextFont::GetWidth(TextFont::Size point, TextFont::Style style)");

	//			$[TI1]
	Int width;
	Int fontIndex = getFontIndex_(point, style);
	
	FontID font = TextFont::AllFonts_[fontIndex].id;

	// font is -1 for unsupported fonts:	
	CLASS_ASSERTION(font != TextFont::INVALID_FONT_ID);

	width = TextFont::AllFonts_[fontIndex].width;

	return (width);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetHeight		[static]
//
//@ Interface-Description
// Returns the height of a normal alphanumeric character in the 
// specified font, based on the passed "point" and "style".
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the maximum height of a numeric digit in the specified 
// font, based on the passed "point" and "style".
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	Verify that the font value fetched from the AllFonts_ array is not
//	INVALID_FONT_ID.
//@ End-Method
//=====================================================================
Int
TextFont::GetHeight(TextFont::Size point, TextFont::Style style)
{
	CALL_TRACE("TextFont::GetHeight(TextFont::Size point, TextFont::Style style)");

	Int height;
	Int fontIndex = getFontIndex_(point, style);
	
	FontID font = TextFont::AllFonts_[fontIndex].id;

	// font is -1 for unsupported fonts:	
	CLASS_ASSERTION(font != TextFont::INVALID_FONT_ID);

	height = TextFont::AllFonts_[fontIndex].height;

	return (height);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetStringSize		[static]
//
//@ Interface-Description
// Calculates the width and height of a text string in the specified font,
// based on the passed point "point" and "style".
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the "point" and "style" to find the index into the AllFonts_ array.
// Determine position of text string located within an rectangle.  The width of
// the rectangle can be calculated by the two upper cornor X coordinates.  The
// height of the rectangle can be calculate by the two left cornor Y
// coordinates.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
// None.
//@ End-Method
//=====================================================================
 
void
TextFont::GetStringSize(wchar_t *string, TextFont::Size point, Style style,
							Int32 &rWidth, Int32 &rHeight)
{
	CALL_TRACE("TextFont::GetStringSize(char *string, TextFont::Size point, "
						"Style style, &rWidth, &rHeight)");

	Int fontIndex = getFontIndex_(point, style);
	
	FontID font = AllFonts_[fontIndex].id;

	// font is -1 for unsupported fonts:	
	CLASS_ASSERTION(font != INVALID_FONT_ID);

	// This call sets Rect rect to the bounding box around the text
	// for the string.
	VgaRect rect = {0,0,0,0};

	if (style == JAPANESE)
	{	//			$[TI1]
		//VgaFont::TextRectJ(VGA_X_START, VGA_Y_START, font, string, &rect);  //TODO E600 port - Remove for compilation - need to check while implementing fonts
	}
	else
	{	//			$[TI2]
		VgaFontData::TextRect(VGA_X_START, VGA_Y_START, font, string, &rect);
	}

	rHeight = rect.y1 - rect.y0 + 1;
	rWidth  = rect.x1 - rect.x0 + 1;
	
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetStringSize		[static]
//
//@ Interface-Description
// Returns the font style character identifier for the specified style.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
// None.
//@ End-Method
//=====================================================================
 
wchar_t
TextFont::GetFontStyleChar(TextFont::Style textStyle)
{
	CALL_TRACE("GetFontStyleChar(TextFont::Style)");

	wchar_t styleChar = ' ';

	switch (textStyle)
	{
	case THIN:
		styleChar = L'T';
		break;
	case NORMAL:
		styleChar = L'N';
		break;
	case ITALIC:
		styleChar = L'I';
		break;
	case JAPANESE:
		styleChar = L'J';
		break;
	default:
		AUX_CLASS_ASSERTION_FAILURE(textStyle);
		break;
	}
	
	return styleChar;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TextFont::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, TEXT_FONT, lineNumber,
			  pFileName, pPredicate);
}
