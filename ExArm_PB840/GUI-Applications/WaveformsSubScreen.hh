#ifndef WaveformsSubScreen_HH
#define WaveformsSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: WaveformsSubScreen - The screen selected by pressing the
// Waveforms button on the Upper Screen.  Consists of two screens,
// the main screen displaying selected patient data waveforms with another
// screen to select which waveforms to display.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformsSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:46   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 023   By: rhj    Date: 22-Feb-2011   SCR Number: 6746 
//  Project:  PROX
//  Description:
//     Added proxManueverType_, isWaveformErased_, clearWaveform_,
//     isProxInStartupMode(), isProxInStartupMode_, isVentInNormalMode_
//  
//  Revision: 022   By: rhj    Date: 13-Jan-2010   SCR Number: 6631
//  Project:  PROX
//  Description:
//     Added isProxReady() method.
//  
//  Revision: 021   By: rhj   Date:  10-Sept-2010    SCR Number: 6631
//  Project:  PROX
//  Description:
//     Added isProxReady_.
//
//  Revision: 020   By: rhj   Date:  16-July-2010    SCR Number: 6593
//  Project:  PROX
//  Description:
//      Moved the proxAutoZeroPurgeStatusMsg_ to WaveformPlot class
//
//  Revision: 019   By: rhj   Date:  18-Mar-2010    SCR Number: 6436
//  Project:  PROX
//     Added a timer for the prox status 
// 
//  Revision: 018   By: rhj  Date: 26-Jan-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//     Modified to support prox related changes.  
// 
//  Revision: 017   By: erm  Date: 30-Oct-2008    SCR Number: 6441
//  Project:  840S
//  Description:
//   Added support for transmit waveform data to Serial Buffers
// 
//  Revision: 016   By: rhj  Date: 23-Jul-2007    SCR Number: 6386
//  Project:  Trend
//  Description:
//  Fixed a bug where the printing on this subscreen has stopped
//  working correctly. 
//
//  Revision: 015   By: gdc  Date: 10-May-2007    SCR Number: 6375
//  Project:  Trend
//  Description:
//  Added support for 30 second waveform for NIF.
//
//  Revision: 014   By: erm  Date: 23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//      added shadow trace enable/disable button
//
//  Revision: 013   By: heatherw Date: 09-Apr-2002    DR Number: 5809, 5811, 5881
//  Project:  VCP
//  Description:
//      Restored pause maneuver functionality back to waveform displays.
//
//  Revision: 012  By: heatherw Date:  28-Mar-2002    DR Number: 5922
//  Project:  VCP
//  Description:
//      Added private member isEndofExpPauseManeuver_. This flag is 
//      used to signal the end of an expiratory pause maneuver.
//
//  Revision: 011   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 010  By: quf     Date:  13-Sep-2001    DCS Number: 5493
//  Project:  GUIComms
//  Description:
//	Print implementation changes:
//	- cancelPrintRequest() now returns nothing.
//	- Added printDone().
//
//  Revision: 009   By: sah   Date:  08-Aug-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added display of PAV-based compliance, resistance and intrinsic
//         PEEP, during PAV breaths
//
//  Revision: 008   By: sah   Date:  08-Aug-2000    DR Number: 5313
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added support for new shadow traces
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//      *  changed to a (auxillary) state machine, with arrays of drawables
//         for each state; this significantly reduces the work of adding new
//         artifacts and states
//
//  Revision: 007  By: hct     Date:  11-APR-2000    DCS Number: 5493
//  Project:  GUIComms
//  Description:
//	GUIComms initial revision.
//
//  Revision: 006   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//      Optimized graphics library. Implemented Direct Draw mode for drawing
//       outside of BaseContainer refresh cycle.
//
//  Revision: 005  By: yyy    Date: 16-Mar-1999  DCS Number: 5345
//  Project:  ATC
//  Description:
//	  Added warning message when exp. pause pressure is not stable.
//	  Added warning message when pause is canceled by alarm or patient
//	  trigger.
//
//  Revision: 004  By: yyy    Date: 3-Nov-1998  DCS Number: 5256
//  Project: BiLevel (R8027)
//  Description:
//	  Removed unused compliance and resistance error titles
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/WaveformsSubScreen.hhv   1.16.1.0   07/30/98 10:22:56   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "GuiApp.hh"
#include "SubScreen.hh"
#include "PatientDataTarget.hh"
#include "ContextObserver.hh"
#include "BdEventTarget.hh"
#include "PrintTarget.hh"

//@ Usage-Classes

#include "BreathPhaseType.hh"
#include "DropDownSettingButton.hh"
#include "FlowVolumePlot.hh"
#include "NumericField.hh"
#include "NumericSettingButton.hh"
#include "PauseTypes.hh"
#include "PressureVolumePlot.hh"
#include "SettingPtr.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "TextField.hh"
#include "TimePlot.hh"
#include "TouchableText.hh"
#include "WaveformDataBuffer.hh"
#include "WaveformIntervalIter.hh"
#include "WobGraphic.hh"
#include "SettingPtr.hh"
#include "FlowVolumePlot.hh"
#include "SensorDataBuffer.hh"
#include "GuiTimerTarget.hh"
#include "GuiTimerRegistrar.hh"

//@ End-Usage

class SubScreenArea;
class TimePlotScaleSetting;


class WaveformsSubScreen : public SubScreen, public PatientDataTarget,
						   public ContextObserver, public BdEventTarget, public PrintTarget,
                           public GuiTimerTarget
{
public:
	WaveformsSubScreen(SubScreenArea* pSubScreenArea);
	~WaveformsSubScreen(void);

	void activate(void);
	void deactivate(void);

	Boolean update(void);
	Boolean updatePlot(void);

	void pauseRequested(void);
	void pauseCancelByAlarm(void);
	void pauseActive(BreathPhaseType::PhaseType phaseType);

	inline Boolean isOnWaveDisplay(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
									Boolean byOperatorAction);

	// PrintTarget virtuals
	virtual void cancelPrintRequest(void);
	virtual void updatePrintButton(void);
	virtual void transitionCaptureToPrint(void);
	virtual void printDone(void);

	// PatientDataTarget virtual method
	virtual void patientDataChangeHappened(
								PatientDataId::PatientItemId patientDataId);

	// ContextObserver virtual method...
	virtual void  batchSettingUpdate(
						  const Notification::ChangeQualifier qualifierId,
						  const ContextSubject*               pContextSubject,
						  const SettingId::SettingIdType      settingId
								    );
	virtual void  nonBatchSettingUpdate(
						  const Notification::ChangeQualifier qualifierId,
						  const ContextSubject*               pContextSubject,
						  const SettingId::SettingIdType      settingId
									   );

    
    Boolean  isInPavClosedLoop( void) const;
    
	// BdEventTarget virtual method...
	virtual void  bdEventHappened(EventData::EventId     eventId,
								  EventData::EventStatus eventStatus,
								  EventData::EventPrompt eventPrompt =
												EventData::NULL_EVENT_PROMPT);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
    void saveCurrentWaveformSettings(void);
    void restoreWaveformSettings();
    void setupNifSettings(void);
    void unFreezeWaveform(void);
    void setupVcSettings(void);
    void setupP100Settings(void);
	Boolean isProxInop(void);
    Boolean isProxInStartupMode(void);
    static SensorDataBuffer & GetSensorDataBuffer(void);
protected:
	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

private:
	// these methods are purposely declared, but not implemented...
	WaveformsSubScreen(void);						// not implemented...
	WaveformsSubScreen(const WaveformsSubScreen&);	// not implemented...
	void operator=(const WaveformsSubScreen&);		// not implemented...

	void layoutScreen_(void);
	void freeze_(void);
	void unFreeze_(void);
	void unFreezeButtons_(void);
	void setFreezingState_(Boolean isFreezing);
	void findLoopPlot_(void);
	void redrawPlot_(WaveformPlot &plot);

	void removeAuxInfo_(Drawable* arrDrawablePtrs[]);
	void addAuxInfo_   (Drawable* arrDrawablePtrs[]);

    void displayCompliance_(Boolean display);
    void displayResistance_(Boolean display);
    void displayPlateauPressure_( Boolean display);
    void updateSensorBufferViaWaveform_ (WaveformData *pData);

	enum AuxInfoState_
	{
		STANDARD_,		// no auxillary info needed...
		EXP_PAUSE_,		// PEEPi and PEEPtot displayed...
		NIF_PAUSE_,		// NIF pressure displayed...
		P100_PAUSE_,	// P100 pressure displayed...
		VC_PAUSE_,		// VC displayed...
		INSP_PAUSE_,	// Cstatic, Rstatic and Ppl displayed...
		VCP_ACTIVE_,	// VCP status message displayed...
		VS_ACTIVE_,   	// VS status message displayed...
		PAV_ACTIVE_		// Cpav, Rpav and PAV status displayed...
	};

	void  updateAuxInfoState_(const AuxInfoState_ newAuxInfoState,
							  const Boolean       forceStateChange = FALSE);

	AuxInfoState_  currAuxInfoState_;
	AuxInfoState_  lastAuxInfoState_;

	enum { MAX_EXP_PAUSE_ARTIFACTS_ = 12 };

	//@ Data-Member: arrExpPausePtrs_
	// Pointers to all of the drawables that make up the EXP PAUSE auxillary
	// information.
	Drawable*  arrExpPausePtrs_[MAX_EXP_PAUSE_ARTIFACTS_ + 1];


	enum { MAX_NIF_PAUSE_ARTIFACTS_ = 7 };

	//@ Data-Member: arrNifPausePtrs_
	// Pointers to all of the drawables that make up the NIF PAUSE auxillary
	// information.
	Drawable*  arrNifPausePtrs_[MAX_NIF_PAUSE_ARTIFACTS_ + 1];

	enum { MAX_P100_PAUSE_ARTIFACTS_ = 7 };

	//@ Data-Member: arrP100PausePtrs_
	// Pointers to all of the drawables that make up the P100 PAUSE auxillary
	// information.
	Drawable*  arrP100PausePtrs_[MAX_P100_PAUSE_ARTIFACTS_ + 1];

	enum { MAX_VC_PAUSE_ARTIFACTS_ = 7 };

	//@ Data-Member: arrSvcPausePtrs_
	// Pointers to all of the drawables that make up the VC PAUSE auxillary
	// information.
	Drawable*  arrSvcPausePtrs_[MAX_VC_PAUSE_ARTIFACTS_ + 1];


    enum { MAX_INSP_RESISTANCE_ARTIFACTS_ = 7 };

    //@ Data-Member: arrInspPauseResistancePtrs_
	// Pointers to all of the drawables that make up the INSP PAUSE auxillary
	// information for Resistance.
    Drawable* arrInspPauseResistancePtrs_[MAX_INSP_RESISTANCE_ARTIFACTS_ +1 ];

    enum { MAX_INSP_COMPLIANCE_ARTIFACTS_ = 6 };

    //@ Data-Member: arrInspPauseCompliancePtrs_
	// Pointers to all of the drawables that make up the INSP PAUSE auxillary
	// information for Compliance.
    Drawable* arrInspPauseCompliancePtrs_[MAX_INSP_COMPLIANCE_ARTIFACTS_ +1 ];

    enum { MAX_INSP_PLATEAU_ARTIFACTS_ = 5 };
    //@ Data-Member: arrInspPausePlateauPtrs_
	// Pointers to all of the drawables that make up the INSP PAUSE auxillary
	// information for Plateau.
    Drawable* arrInspPausePlateauPtrs_[MAX_INSP_PLATEAU_ARTIFACTS_ +1 ];

    enum { MAX_ALARM_CANCEL_ARTIFACTS_ = 2 };
    //@ Data-Member: arrAlarmCancelPtrs_
	// Pointer to information to be displayed on a cancelled pause maneuver 
    Drawable* arrAlarmCancelPtrs_[MAX_ALARM_CANCEL_ARTIFACTS_ +1 ];


	enum { MAX_PAV_ARTIFACTS_ = 12 };
	enum { MAX_VCP_ARTIFACTS_ = 1 };
	enum { MAX_VS_ARTIFACTS_  = 1 };
	
	//@ Data-Member: arrPavPtrs_
	// Pointers to all of the drawables that make up the PAV auxillary
	// information.
	Drawable*  arrPavPtrs_[MAX_PAV_ARTIFACTS_ + 1];

	//@ Data-Member: arrVcpPtrs_
	// Pointers to all of the drawables that make up the VCP auxillary
	// information.
	Drawable*  arrVcpPtrs_[MAX_VCP_ARTIFACTS_ + 1];

	//@ Data-Member: arrVsPtrs_
	// Pointers to all of the drawables that make up the VS auxillary
	// information.
	Drawable*  arrVsPtrs_[MAX_VS_ARTIFACTS_ + 1];

	//@ Type: staticMechanicState
	// This enum represents the state for pPi calculation.
	// qPat, pEexh, and pEd.
	enum ComplianceState
	{
		VALID = 0,
		STABILITY_PROBLEM = 1,
		TIMING_PROBLEM = 2,
		STABILITY_TIMING_PROBLEM = STABILITY_PROBLEM | TIMING_PROBLEM,
		INADEQUATE = 4
  	};

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.  Used only for
	// the plot setup screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member:: currentDataBuffer_
	// The waveform data buffer which stores current data, data that is
	// being collected in real-time from the Patient-Data subsystem.
	WaveformDataBuffer currentDataBuffer_;

	//@ Data-Member:: frozenDataBuffer_
	// The waveform data buffer which stores frozen data, the data which
	// was current at the time a freeze request was implemented.
	WaveformDataBuffer frozenDataBuffer_;

	//@ Data-Member:: timePlot1_
	// Time plot 1, which will display the data chosen by the user with
	// the Plot 1 Setup setting button.  This time plot is either displayed
	// over the full waveform display screen if it's the only time plot
	// or displayed at the top half of the screen if the second time plot
	// is displayed below.
	//
	// Time plot 1 always displays grid values below the X axis to denote
	// the time scale.
	TimePlot timePlot1_;

	//@ Data-Member:: timePlot2_
	// Time plot 2, which will display the data chosen by the user with
	// the Plot 2 Setup setting button.  Displayed below time plot 1.
	//
	// Time plot 2 does not display grid values below the X axis to denote
	// the time scale.
	TimePlot timePlot2_;

	//@ Data-Member::  wobGraphic_
	WobGraphic  wobGraphic_;

	//@ Data-Member:: pressureVolumePlot_
	// The Pressure vs. Volume loop plot.
	PressureVolumePlot pressureVolumePlot_;

	//@ Data-Member:: flowVolumePlot_
	// The Flow vs. Volume loop plot.
	FlowVolumePlot flowVolumePlot_;


	//@ Data-Member: currentFullPlotIter_
	// The waveform interval iterator which is used to iterate through the
	// waveform data which represents the full waveform plot(s) currently
	// displayed on-screen.
	WaveformIntervalIter currentFullPlotIter_;

	//@ Data-Member: currentSegmentIter_
	// The iterator used to access the latest segment of data received from
	// the Patient-Data subsystem.
	WaveformIntervalIter currentSegmentIter_;

	//@ Data-Member: frozenTimePlotIter_
	// An iterator which represents the frozen data to be displayed on the
	// time plots.
	WaveformIntervalIter frozenTimePlotIter_;

	//@ Data-Member: frozenLoopPlotIter_
	// An iterator which represents the frozen data to be displayed on the
	// loop plot.  It is the last full breath in the frozen data buffer.
	WaveformIntervalIter frozenLoopPlotIter_;

	//@ Data-Member: isOnWaveDisplay_;
	// Flag to indicate whether to display waveforms (TRUE) or the plot
	// setup screen.
	Boolean isOnWaveDisplay_;

	//@ Data-Member:  isStartOfWaveformFound_
	// Indicates whether the inspiration that triggers the beginning of
	// waveform plotting was found or not.  Testing whether this flag is FALSE
	// can also indicate that a full waveform is currently being displayed.
	Boolean  isStartOfWaveformFound_;

	//@ Data-Member:  isLastSampleExhalation_
	// In between updates where we read the latest waveform data from the
	// Patient-Data subsystem we need to know if the last sample received was
	// an exhalation so that we can trap for the beginning of breath triggers
	// (a transition from an exhalation to an inspiration).  This allows
	// us to detect the trigger that begins waveform plotting and the trigger
	// which ends loop plotting.
	Boolean  isLastSampleExhalation_;

	//@ Data-Member:  isFreezing_
	// A flag to show whether we are in the 'freezing' state, the state where
	// the user has requested a waveform freeze but the current waveform has
	// not yet reached the end of its trace.
	Boolean  isFreezing_;


	//@ Data-Member:  isInspiratoryPauseState_
	// A flag to show whether we are in the 'Inspiratory Pause' state, the state where
	// the user has requested a inspiratory pause 
	Boolean  isInspiratoryPauseState_;


	//@ Data-Member:  isExpiratoryPauseState_
	// A flag to show whether we are in the 'Expiratory Pause' state, the state where
	// the user has requested a expiratory pause 
	Boolean  isExpiratoryPauseState_;

	//@ Data-Member:  isNifPauseState_
	// A flag to show whether we are in the 'NIF Pause' state, the state where
	// the user has requested a NIF pause maneuver
	Boolean  isNifPauseState_;

	//@ Data-Member:  isP100PauseState_
	// A flag to show whether we are in the 'P100 Pause' state, the state where
	// the user has requested a P100 pause maneuver
	Boolean  isP100PauseState_;

	//@ Data-Member:  isSvcPauseState_
	// A flag to show whether we are in the 'SVC Pause' state, the state where
	// the user has requested a SVC pause maneuver
	Boolean  isSvcPauseState_;

	//@ Data-Member:  isLastSampleExhalation_
	// In between updates where we read the latest waveform data from the
	// Patient-Data subsystem we need to know if the last sample received was
	// an exhalation so that we can trap for the beginning of breath triggers
	// (a transition from an exhalation to an inspiration).  This allows
	// us to detect the trigger that begins waveform plotting and the trigger
	// which ends loop plotting.
	Boolean  isLastSampleExpiratoryPause_;

    //@ Data-Member: isPlateauPressureDisplayed_
	// Indicates whether we need to display the plateau pressure
	// data values which are used during the Inspiratory Pause maneuver.
	Boolean  isPlateauPressureDisplayed_;

	//@ Data-Member: areComplianceDisplayed_
	// Indicates whether we need to display the compliance patient
	// data values which are used during the Inspiratory Pause maneuver.
	Boolean  areComplianceDisplayed_;

	//@ Data-Member: areResistanceDisplayed_
	// Indicates whether we need to display the resistance patient
	// data values which are used during the Inspiratory Pause maneuver.
	Boolean  areResistanceDisplayed_;


	//@ Data-Member: isFrozen_
	// This indicates whether we are in the frozen state, where all waveform
	// plots display the same frozen data until waveforms are unfrozen.
	Boolean  isFrozen_;

	//@ Data-Member: lengthOfTimeWaveform_
	// This is the number of milliseconds of data currently displayed on the
	// time plots.  Currently varies between 3000 and 48000.
	Uint32 lengthOfTimeWaveform_;

	//@ Data-Member: plotSetupButton_
	// The button which allows the user to switch to the plot setup screen
	// from the waveform display screen.
	TextButton plotSetupButton_;

	//@ Data-Member: freezeButton_
	// The Freeze button which allows the user to request a waveform freeze.
	// This button also doubles as the Unfreeze button(!).  It either displays
	// the text 'FREEZE' or the text 'UNFREEZE' (flashing) depending on
	// the situation.
	TextButton freezeButton_;

	//@ Data-Member: printButton_
	// The button which allows the user to print a frozen waveform.
	TextButton printButton_;

	//@ Data-Member: timeScaleButton_
	// The button which adjust the time scale for the time plots.
	DiscreteSettingButton timeScaleButton_;

	//@ Data-Member: xPressureScaleButton_
	// The button which adjusts the pressure scale on a X-axis.  This button
	// adjusts the same setting as the 'yPressureScaleButton_' but two
	// buttons are needed because the two buttons need to be a different
	// size and have a different label.  This button is currently used
	// exclusively with the Pressure-Volume plot.
	DiscreteSettingButton xPressureScaleButton_;

	//@ Data-Member: yPressureScaleButton_
	// The button which adjusts the pressure scale on a Y-axis.  Used for
	// Pressure time plots.
	DiscreteSettingButton yPressureScaleButton_;

	//@ Data-Member: volumeScaleButton_
	// Adjusts the volume scale on both time plots and the Pressure-Volume plot.
	DiscreteSettingButton volumeScaleButton_;

	//@ Data-Member: volumeScaleButton_
	// Adjusts the volume scale on both time plots and the Flow-Volume plot.
	DiscreteSettingButton xVolumeScaleButton_;

	//@ Data-Member: flowScaleButton_
	// Adjusts the flow scale for time plots.
	DiscreteSettingButton flowScaleButton_;

	//@ Data-Member: baselineButton_
	// Adjusts the pressure baseline value on the Pressure-Volume plot.
	// The baseline value sets the position of the Y-axis on that plot.
	NumericSettingButton baselineButton_;

	//@ Data-Member: plot1SetupSettingButton_
	// The button which allows the user to choose the type of Plot 1.
	DropDownSettingButton  plot1SetupSettingButton_;

	//@ Data-Member: plot2SetupSettingButton_
	// The button which allows the user to choose the type of Plot 2.
	DropDownSettingButton  plot2SetupSettingButton_;

	//@ Data-Member: shadowEnableSettingButton_
	// The button which allows the user to enable/disable the shadow traces.
	DiscreteSettingButton  shadowEnableSettingButton_;

	//@ Data-Member: continueButton_
	// A Continue button to allow transition from the plot setup screen
	// back to the waveform display screen.
	TextButton continueButton_;

	//@ Data-Member: freezingText_;
	// The text which flashes the message 'FREEZING' at the top left of the
	// waveform display.
	TextField freezingText_;

	//@ Data-Member: printingText_;
	// The text which flashes the message 'PRINTING' at the top of the
	// waveform display.
	TextField printingText_;

	//@ Data-Member: capturingText_;
	// The text which flashes the message 'CAPTURING PRINT DATA' at the top
	// of the waveform display.
	TextField capturingText_;

	//@ Data-Member: intrinsicPeepSymbol_
	// Label for Intrinsic PEEP symbol.
	TouchableText intrinsicPeepSymbol_;

	//@ Data-Member: intrinsicPeepValue_
	// Numeric field for Intrinsic PEEP value.
	NumericField intrinsicPeepValue_;

	//@ Data-Member: intrinsicPeepUnit_
	// Label for Intrinsic PEEP unit.
	TextField intrinsicPeepUnit_;

	//@ Data-Member: intrinsicPeepMarkerText_
	// Label for intrinsicPeep marker.
	TextField intrinsicPeepMarkerText_;

	//@ Data-Member: totalPeepSymbol_
	// Label for Total PEEP symbol.
	TouchableText totalPeepSymbol_;

	//@ Data-Member: totalPeepValue_
	// Numeric field for Total PEEP value.
	NumericField totalPeepValue_;

	//@ Data-Member: totalPeepUnit_
	// Label for Total PEEP unit.
	TextField totalPeepUnit_;

	//@ Data-Member: totalPeepMarkerText_
	// Label for totalPeep marker.
	TextField totalPeepMarkerText_;

	//@ Data-Member: nifPressureSymbol_
	// Label for NIF Pressure symbol.
	TouchableText nifPressureSymbol_;

	//@ Data-Member: nifPressureValue_
	// Numeric field for NIF pressure value
	NumericField nifPressureValue_;

	//@ Data-Member: nifPressureUnit_
	// Label for NIF pressure unit.
	TextField nifPressureUnit_;

	//@ Data-Member: nifPressureMarkerText_
	// Label for NIF pressure marker.
	TextField nifPressureMarkerText_;

	//@ Data-Member: p100PressureSymbol_
	// Label for NIF Pressure symbol.
	TouchableText p100PressureSymbol_;

	//@ Data-Member: p100PressureValue_
	// Numeric field for P100 pressure value
	NumericField p100PressureValue_;

	//@ Data-Member: p100PressureUnit_
	// Label for P100 pressure unit.
	TextField p100PressureUnit_;

	//@ Data-Member: p100PressureMarkerText_
	// Label for P100 pressure marker.
	TextField p100PressureMarkerText_;

	//@ Data-Member: vitalCapacitySymbol_
	// Label for Vital Capacity symbol
	TouchableText vitalCapacitySymbol_;

	//@ Data-Member: vitalCapacityValue_
	// Numeric field for Vital Capacity value
	NumericField vitalCapacityValue_;

	//@ Data-Member: vitalCapacityUnit_
	// Label for Vital Capacity unit
	TextField vitalCapacityUnit_;

	//@ Data-Member: vitalCapacityMarkerText_
	// Label for Vital Capacity marker.
	TextField vitalCapacityMarkerText_;

	//@ Data-Member: plateauPressureMarkerText_
	// Label for plateau pressure marker.
	TouchableText plateauPressureMarkerText_;

	//@ Data-Member: isEndofExpPauseManeuver_
	// boolean flag for marking the end of
    // expiratory maneuver
    Boolean isEndofExpPauseManeuver_;

	//@ Data-Member: isEndofNifPauseManeuver_
	// boolean flag for marking the end of NIF maneuver
    Boolean isEndofNifPauseManeuver_;

	//@ Data-Member: isEndofP100PauseManeuver_
	// boolean flag for marking the end of P100 maneuver
    Boolean isEndofP100PauseManeuver_;

	//@ Data-Member: isEndofSvcPauseManeuver_
	// boolean flag for marking the end of SVC maneuver
    Boolean isEndofSvcPauseManeuver_;

	//@ Data-Member: plateauPressureSymbol_
	// label for plateau pressure symbol
	TouchableText plateauPressureSymbol_ ;

	//@ Data-Member: plateauPressureValue_
	// numeric field for plateau pressure value
	NumericField plateauPressureValue_ ;

	//@ Data-Member: plateauPressureUnit_
	// label for plateau pressure unit
	TextField plateauPressureUnit_ ;

	//@ Data-Member: lungComplianceMarkerText_
	// Label for Lung Compliance marker.
	TouchableText lungComplianceMarkerText_;

	//@ Data-Member: lungComplianceSymbol_
	// Label for Lung Compliance symbol.
	TouchableText lungComplianceSymbol_;

	//@ Data-Member: lungComplianceValue_
	// Numeric field for Lung Compliance value.
	NumericField lungComplianceValue_;

	//@ Data-Member: lungComplianceUnit_
	// Label for Lung Compliance unit.
	TextField lungComplianceUnit_;

	//@ Data-Member: airwayResistanceMarkerText_
	// Label for Lung Resistance marker.
	TouchableText airwayResistanceMarkerText_;

	//@ Data-Member: airwayResistanceSymbol_
	// Label for Lung Resistance symbol.
	TouchableText airwayResistanceSymbol_;

	//@ Data-Member: airwayResistanceValue_
	// Numeric field for Lung Resistance value.
	NumericField airwayResistanceValue_;

	//@ Data-Member: airwayResistanceUnit_
	// Label for Lung Resistance unit.
	TextField airwayResistanceUnit_;

	//@ Data-Member: pavStatusMsg_
	// Status message displaying the current state of PAV.
	TextField pavStatusMsg_;
  
	//@ Data-Member:  arePavSymbolsAllowed_
	// Indicates whether, while in 'PAV_ACTIVE' state, it is appropriate
	// for the PAV data symbols to be displayed.
	Boolean  arePavSymbolsAllowed_;

	//@ Data-Member:  arePavValuesAllowed_
	// Indicates whether, while in 'PAV_ACTIVE' state, it is appropriate
	// for the PAV data values to be displayed.
	Boolean  arePavValuesAllowed_;

	//@ Data-Member: vcpStatusMsg_
	// Status message displaying the current state of VCP.
	TextField vcpStatusMsg_;

	//@ Data-Member: vsStatusMsg_
	// Status message displaying the current state of VS.
	TextField vsStatusMsg_;

	//@ Data-Member: staticResistanceProblemLabel_
	// Diagnostic message for static resistance problem.
	TextField staticResistanceProblemLabel_;

	//@ Data-Member: staticComplianceProblemLabel_
	// Diagnostic message for static compliance problem.
	TextField staticComplianceProblemLabel_;

	//@ Data-Member: peepStabilityProblemLabel_
	// Diagnostic message for peep stability problem.
	TextField peepStabilityProblemLabel_;

	//@ Data-Member: staticResistanceStatusValue_
	// Static resistance status
	PauseTypes::ComplianceState staticResistanceStatusValue_;

	//@ Data-Member: staticComplianceStatusValue_
	// Static compliance status
	PauseTypes::ComplianceState staticComplianceStatusValue_;

	//@ Data-Member: staticResistanceNotAvailableText_
	// Label for static (INSP PAUSE) airway resistance value not available.
	TextField staticResistanceNotAvailableText_;

	//@ Data-Member: staticResistanceInadequateText_
	// Label for static (INSP PAUSE) airway resistance value is inadequate.
	TextField staticResistanceInadequateText_;

	//@ Data-Member: staticComplianceInadequateText_
	// Label for static (INSP_PAUSE) lung compliance value is inadequate.
	TextField staticComplianceInadequateText_;

	//@ Data-Member: pauseCancelByAlarmText_
	// Label for lung compliance value is inadequate
	TextField pauseCancelByAlarmText_;

	//@ Data-Member: isInErrorState_
	// Indicator of an any gui state other than NORMAL state.
	Boolean isInErrorState_;

	//@ Data-Member: isPlot1Updated_
	// This indicates whether we updated the time plot 1.
	Boolean  isPlot1Updated_;

	//@ Data-Member: isPlot2Updated_
	// This indicates whether we updated the time plot 2.
	Boolean  isPlot2Updated_;

	enum { MAX_DISPLAY_BUTTONS_ = 7 };

	//@ Data-Member: arrDisplayBtnPtrs_
	// All buttons that could be shown in waveforms display screen of
	// Waveforms Setup.
	SettingButton*  arrDisplayBtnPtrs_[MAX_DISPLAY_BUTTONS_ + 1];

	enum { MAX_SETUP_BUTTONS_ = 3 };

	//@ Data-Member: arrSetupBtnPtrs_
	// All buttons that could be shown in waveforms setup screen of
	// Waveforms Setup.
	SettingButton*  arrSetupBtnPtrs_[MAX_SETUP_BUTTONS_ + 1];

	//@ Data-Member: timeScaleAxisBitmap_
	// The bitmap for the time scale button
	Bitmap timeScaleAxisBitmap_;

	//@ Data-Member: timeScaleAxisBitmap_
	// The bitmap for the time scale button
	Bitmap xPressureScaleAxisBitmap_;

	//@ Data-Member: yPressureScaleAxisBitmap_
	// The bitmap for the y-axis pressure scale button
    Bitmap yPressureScaleAxisBitmap_;

	//@ Data-Member: volumeScaleAxisBitmap_
	// The bitmap for the volume scale button
	Bitmap volumeScaleAxisBitmap_;

    //@ Data-Member: volumeScaleAxisBitmap_
	// The bitmap for the volume scale button
	Bitmap xVolumeScaleAxisBitmap_;

	//@ Data-Member: flowScaleAxisBitmap_
	// The bitmap for the flow scale button
	Bitmap flowScaleAxisBitmap_;

    //@ Data-Member: plot1SaveSetting_
	// Stores the Plot1 setting.
    DiscreteValue plot1SaveSetting_;

    //@ Data-Member: plot2SaveSetting_
	// Stores the Plot2 setting.
    DiscreteValue plot2SaveSetting_;

    //@ Data-Member: pressurePlotScaleSaveSetting_
	// Stores the pressure plot scale setting.
    DiscreteValue pressurePlotScaleSaveSetting_;

    //@ Data-Member: volumePlotScaleSaveSetting_
	// Stores the volume plot scale setting.
    DiscreteValue volumePlotScaleSaveSetting_;

    //@ Data-Member: flowPlotScaleSaveSetting_
	// Stores the flow plot scale setting.
    DiscreteValue flowPlotScaleSaveSetting_;

    //@ Data-Member: timePlotScaleSaveSetting_
	// Stores the time plot scale setting.
    DiscreteValue timePlotScaleSaveSetting_;

    //@ Data-Member: shadowTraceEnableSaveSetting_
	// Stores the shadow trace enable save setting.
    DiscreteValue shadowTraceEnableSaveSetting_;


    //@ Data-Member: isNifWaveformIsFrozen_
	// Flag that indicates the waveform is freezing or frozen due to a NIF Maneuver.
    Boolean isNifWaveformFoisting_;

    //@ Data-Member: isVcWaveformIsFrozen_
	// Flag that indicates the waveform is freezing or frozen due to a VC Maneuver.
    Boolean isVcWaveformFoisting_;

    //@ Data-Member: isP100WaveformIsFrozen_
	// Flag that indicates the waveform is freezing or frozen due to a P100 Maneuver.
    Boolean isP100WaveformFoisting_;

    //@ Data-Member: updatePlot_
	// Flag that indicates whether waveform needs to be redrawn.
    Boolean updatePlot_;

    //@ Data-Member: disableSettingsUpdate_
	// Flag that indicates whether to do a waveform update due to a settings change.
    Boolean disableSettingsUpdate_;

	//@ Data-Member: isPressureLoopUpdated_
	// This indicates whether we updated the Pressure vs Volume Loop.
	Boolean  isPressureVolumeLoopUpdated_;

	//@ Data-Member: isVolumeLoopUpdated_
	// This indicates whether we updated the Flow vs Volume Loop.
	Boolean  isFlowVolumeLoopUpdated_;

	//@ Data-Member: previousSpontType_
    // Stores the previous Spont Type.
    DiscreteValue previousSpontType_;

	enum { MAX_WAVEFORM_PLOT_SETTING = 7 };
	
	//@ Data-Member: arrPlotSettingPtr_
	// Pointers to all of the plot settings.
	Setting * arrPlotSettingPtr_ [MAX_WAVEFORM_PLOT_SETTING + 1];

	//@ Data-Member: pTimeScaleSetting_
	// Pointers to the time plot scale setting
	TimePlotScaleSetting* pTimeScaleSetting_;

	//@ Data-Member: bufferEntry_
	// Hold Pressure and Flow data points
    SensorDataBuffer::SensorEntry   bufferEntry_;

	//@ Data-Member: startOnInsp_
	// Indicators for 1st cycle of insp
    Boolean startOnInsp_;

	//@ Data-Member: updateCount_
	// index into Presure and flow buffer
    Uint32 updateCount_;

	//@ Data-Member: isProxInop_
	// Is proximal board inoperative?
	Boolean isProxInop_;

	//@ Data-Member: isProxEnabled_
	// Is prox setting enabled?
	Boolean isProxEnabled_;

	//@ Data-Member: isProxDisplayTimerOn_
	// Is Prox display timer on?
	Boolean isProxDisplayTimerOn_;

	//@ Data-Member: isProxInStartupMode_
	// Is proximal board in startup mode
	Boolean isProxInStartupMode_;

	//@ Data-Member: clearWaveform_
	// A flag to clear the waveform 
    Boolean clearWaveform_;

	//@ Data-Member: isWaveformErased_
	// is the waveform screen is erased.
    Boolean isWaveformErased_ ;

	//@ Data-Member: proxManueverType_
	// stores the prox manuever type
    Uint  proxManueverType_;

	//@ Data-Member: isVentNotInStartupOrWaitForPT_
	// is the vent is not in startup nor waiting 
    // for patient connect?
    Boolean isVentNotInStartupOrWaitForPT_;

	//@ Data-Member: displayProxStartupMsgOnce
	// Used as a flag to prevent 
    // displaying the prox startup more than
    // once.
    Boolean displayProxStartupMsgOnce_;

};

// Inlined methods
#include "WaveformsSubScreen.in"

#endif // WaveformsSubScreen_HH 
