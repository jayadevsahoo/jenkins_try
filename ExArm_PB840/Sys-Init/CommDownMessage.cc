#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: CommDownMessage - Communications Down Message.
//---------------------------------------------------------------------
//@ Interface-Description
//  The CommDownMessage class is a TaskControlMessage indicating a
//  network communications failure.  CommTaskAgent::NetworkDown creates
//  and queues this message to the Task Control task when invoked
//  by Network-App.  Task Control receives this message and
//  processes it at its normal processing priority.  On the GUI, this
//  message indicates a communications failure with the BD.  On the BD,
//  this message indicates a communications failure with the GUI.
//  This class uses the AppContext callback mechanism to dispatch the 
//  message to the receiving task's callback function.
//---------------------------------------------------------------------
//@ Rationale
//  The CommDownMessage encapsulates communications failure event 
//  information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is a derived TaskControlMessage.  The base class new and
//  delete operators override the library operators to allocate memory
//  for the message in the global BufferPool.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/CommDownMessage.ccv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "CommDownMessage.hh"

//@ Usage-Classes
#include "AppContext.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommDownMessage [default constructor]
//
//@ Interface-Description
//  CommDownMessage default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class TaskControlMessage constructor with the classId
//  for this message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

CommDownMessage::CommDownMessage() 
    : TaskControlMessage(Sys_Init::ID_COMMDOWNMESSAGE)
{
    CALL_TRACE("CommDownMessage()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommDownMessage [copy constructor]
//
//@ Interface-Description
//  CommDownMessage copy constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class TaskControlMessage copy constructor with the
//  referenced CommDownMessage
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

CommDownMessage::CommDownMessage(const CommDownMessage& rMessage) 
    : TaskControlMessage(rMessage)
{
    CALL_TRACE("CommDownMessage(const CommDownMessage&)");
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CommDownMessage [destructor]
//
//@ Interface-Description
//  CommDownMessage destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

CommDownMessage::~CommDownMessage()
{
    CALL_TRACE("~CommDownMessage()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

const char*
CommDownMessage::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "CommDownMessage";
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch
//
//@ Interface-Description
//  The method dispatches this CommDownMessage to the callback
//  function registered with the specified AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the AppContext dispatch method with a reference to this
//  CommDownMessage.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
CommDownMessage::dispatch(const AppContext& context) const
{
    CALL_TRACE("dispatch(const AppContext&)");
    context.dispatch(*this);
    // $[TI1]
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
CommDownMessage::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_COMMDOWNMESSAGE, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================



