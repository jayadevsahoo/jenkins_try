#ifndef TrendDbEventReceiver_HH
#define TrendDbEventReceiver_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbEventReceiver
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbEventReceiver.hhv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  ksg    Date:  17-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//=====================================================================
//@ Usage-Classes
//@ End-Usage


#include "BdEventTarget.hh"         // GUI-Applications
#include "NovRamEventTarget.hh"     // GUI-Applications
									 
#include "TrendEvents.hh"
#include "TrendValue.hh"


class TrendDbEventReceiver :    public BdEventTarget,
								public NovRamEventTarget
{
public:

	TrendDbEventReceiver();
	~TrendDbEventReceiver();

	void registerTargets( void );
	void freezeEvents( void );
	void clearEvents( void );

	virtual void bdEventHappened( EventData::EventId eventId,
								  EventData::EventStatus eventStatus,
								  EventData::EventPrompt eventPrompt =
								  EventData::NULL_EVENT_PROMPT );

	virtual void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId updateId );

	void postEvent( TrendEvents::EventId event );
	void postEventValue( TrendEvents::EventId event, const TrendValue & value );

	inline Boolean isPatientConnected( void ) const;
	inline Boolean isPatientApnea( void ) const;
	inline Boolean isPatientCircuitOccluded( void ) const;
	inline void setPatientSetupComplete( Boolean patientSetupComplete );
	inline Boolean isPatientSetupComplete( void ) const;
	inline Uint32 getUserEvents( void ) const;
	inline Uint32 getAutoEvents( void ) const;
	inline const TrendValue & getEventValue( TrendEvents::EventId event ) const;

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


private:

	TrendDbEventReceiver( const TrendDbEventReceiver & );
	void operator=( const TrendDbEventReceiver & );
	inline void setPatientConnected( Boolean patientConnected );
	inline void setPatientApnea( Boolean patientConnected );
	inline void setPatientOcclusion( Boolean patientConnected );

	//@ Data-Member: patientConnected_
	// True when the patient is connected
	Boolean     isPatientConnected_;

	//@ Data-Member: patientOcclusion_
	// True when the patient circuit is occluded
	Boolean     isPatientCircuitOccluded_;

	//@ Data-Member: patientApnea_
	// True when the patient is in apnea
	Boolean     isPatientApnea_;

	//@ Data-Member: patientSetupComplete_
	// True once patient setup is completed
	Boolean     isPatientSetupComplete_;

	//@ Type: EventTypes
	// There are two type of Events User and Auto
	enum EventTypes
	{
		USER_EVENT = 0,
		AUTO_EVENT = 1,
		NUM_EVENT_TYPES = 2
	};

	//@ Type: EventSetData
	//  A struct to hold the events and their values (a event buffer)
	struct EventSetData
	{
		//@ Data-Member: eventbits_
		// Long value used to hold the Event bits
		Uint32  eventBits_[NUM_EVENT_TYPES];

		//@ Data-Member: eventValaue_
		// An Array to hold event values
		TrendValue eventValue_[TrendEvents::TOTAL_EVENT_IDS];
	};

	//@ Type: EventSets
	// Double buffer the events and values
	enum EventSets
	{
		EVENT_SET_A	= 0,
		EVENT_SET_B = 1,

		NUM_EVENT_SETS = 2
	};

	//@ Data-Member: frozenEventSet_
	//  Indicates which set of events and values sets is frozen
	//  The frozen set events and values are return via the "get" methods
	//  While the other set is updated via the "set" methods
	EventSets	frozenEventSet_;

	//@ Data-Member: eventSetData_
	//  The collection of events and their values double buffered
	EventSetData eventSetData_[ NUM_EVENT_SETS ];
	
};

// Inlined methods
#include "TrendDbEventReceiver.in"

#endif // TrendDbEventReceiver_HH
