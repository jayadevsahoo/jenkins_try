#ifndef PostDefs_HH
#define PostDefs_HH
 
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
//@ Filename:  PostDefs.hh - POST Global Definitions.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/PostDefs.hhv   25.0.4.0   19 Nov 2013 14:16:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc    Date:  07-APR-2008    SCR Number: 6407
//  Project:  TREND2
//  Description:
//	Removed redefinition of countof macro to resolve compiler warning.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================
 
 

// **** This macro generates a test point that can be used by the emulator
// **** for unit testing.  It does not cause any code insertions, it only
// **** defines a global label.
#ifdef SIGMA_UNIT_TEST
#define    UNIT_TEST_POINT(name)  /*lint -save -e525*/ asm(" xdef UT_" #name); asm("UT_"#name":"); /*lint -restore*/
#else
 
#define    UNIT_TEST_POINT(name)  //
#endif //SIGMA_UNIT_TEST
 
#ifdef SIGMA_DEBUG
#define    DEBUG_TEST_POINT(name) asm(" xdef DEBUG_" #name); asm("DEBUG_"#name":");
#endif //SIGMA_DEBUG
 
//TODO E600 remove asm. 
#define DISABLE_INTERRUPTS()  0;//asm(" or.w  #$0700,sr ")
#define ENABLE_INTERRUPTS()   0;//asm(" and.w #$f8ff,sr ")

// Criticality ranking of POST failures
// A post test return status is always one of these
enum  Criticality
{
    PASSED            = 0,
    MINOR             = 1,
    SERVICE_REQUIRED  = 2,
    MAJOR             = 3
};
 
// Types of CPU resets
enum  ShutdownState
{
    UNKNOWN     = 1,
    EXCEPTION   = 2,
    ACSWITCH    = 3,
    POWERFAIL   = 4,
    WATCHDOG    = 5,
    INTENTIONAL = 6
};
 
#endif // PostDefs_HH
