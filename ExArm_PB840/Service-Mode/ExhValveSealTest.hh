#ifndef ExhValveSealTest_HH
#define ExhValveSealTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  ExhValveSealTest - Test the exhalation valve seal
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ExhValveSealTest.hhv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

extern const Real32 EV_SEAL_TEST_LOW_FLOW;
extern const Real32 EV_SEAL_TEST_PRESSURE;
extern const Real32 EV_SEAL_TEST_ALERT_DELTA_P;


class ExhValveSealTest
{
  public:
    ExhValveSealTest( void);
    ~ExhValveSealTest( void);

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL, 
						   const char		*pPredicate = NULL) ;

	void runTest( void);
	  
  protected:

  private:
    ExhValveSealTest( const ExhValveSealTest&);	// not implemented...
    void operator=( const ExhValveSealTest&);	// not implemented...

	void checkEvMotorThermistor_( void);
} ;


#endif // ExhValveSealTest_HH 
