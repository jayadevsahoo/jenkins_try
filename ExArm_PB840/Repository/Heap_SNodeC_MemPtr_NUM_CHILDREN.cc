#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Heap_SNodeC_MemPtr_NUM_CHILDREN - Fixed Heap of Memory Blocks.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is the derived class of the abstract, size-independent memory
//  manager ('Heap').  Provides a means of dynamically managing
//  'NUM_CHILDREN' memory blocks.  These blocks may be reused many times --
//  given they're properly deallocated.  These blocks are long
//  word-aligned, and are big enough to contain an object of type 'SNodeC_MemPtr'.
//  Initialization and deinitialization of a heap's allocated memory
//  blocks is left to the client -- the memory blocks are raw memory
//  blocks.
//
//  An overloaded 'new' operator is provided that takes a heap as an
//  argument, and runs the specified constructor on the newly allocated
//  memory (see 'Heap' for the declaration and description of
//  this operator).
//---------------------------------------------------------------------
//@ Rationale
//  This will be used for data blocks (e.g., classes) that may
//  have a short life-span, and/or whose memory location may be
//  reused by other data blocks, of the same type, at a later time.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An internal array of 'Node' items is kept.  The array is
//  big enough to hold 'NUM_CHILDREN' blocks, with each block big enough to
//  contain an object of type 'SNodeC_MemPtr'.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The memory that is allocated from the heaps is raw memory -- the
//  memory is NOT initialized to any specific value.  It is left to
//  the client of the heaps to initialize the allocated memory (e.g.,
//  with a constructor) and, if necessary, deinitialize memory that
//  is being deallocated (e.g., with a destructor).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedHeap.C_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "Heap_SNodeC_MemPtr_NUM_CHILDREN.hh"

#if defined(SIGMA_DEBUG)
#  include "Foundation.hh"
#  include "Ostream.hh"
#endif  // defined(SIGMA_DEBUG)

//@ Usage-Classes...
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Heap_SNodeC_MemPtr_NUM_CHILDREN()  [Default Constructor]
//
//@ Interface-Description
//      Construct a full heap.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The address of the first node in the heap block, along with the
//      maximum number of blocks and the number of nodes per block, are
//      passed to the base class's constructor.
//---------------------------------------------------------------------
//@ PreConditions
//      none
//---------------------------------------------------------------------
//@ PostConditions
//      (isEmpty())
//      (getBlockSize() >= sizeof(SNodeC_MemPtr))
//      (getNumAvailable() == NUM_CHILDREN)
//@ End-Method
//=====================================================================

Heap_SNodeC_MemPtr_NUM_CHILDREN::Heap_SNodeC_MemPtr_NUM_CHILDREN(void)
			      : Heap(pMemBlock_, NUM_CHILDREN, BLOCK_NODES_)
{
  CALL_TRACE("Heap_SNodeC_MemPtr_NUM_CHILDREN()");

  AUX_CLASS_ASSERTION(sizeof(pMemBlock_) >= (sizeof(SNodeC_MemPtr) * NUM_CHILDREN),
		      ((sizeof(SNodeC_MemPtr) << 22) |	// top 10 bits...
		       (NUM_CHILDREN         << 12) |	// middle 10 bits...
		       sizeof(pMemBlock_)));	// bottom 12 bits...

#if defined(SIGMA_DEBUG)
  if (Foundation::IsDebugOn(::ABSTRACT_HEAP)  ||
      Foundation::IsDebugOn(::FIXED_HEAP))
  {
    cout << "Constructing a Fixed Heap:\n";
    cout << "  Memory Start:        " << (void*) pMemBlock_ << '\n';
    cout << "  Memory End:          "
	 << (void*)(pMemBlock_ + (BLOCK_NODES_ * NUM_CHILDREN)) << '\n';
    cout << "  Max. Blocks:         " << NUM_CHILDREN << '\n';
    cout << "  Block Size (bytes):  " << (BLOCK_NODES_ * sizeof(Heap::Node))
	 << '\n' << endl;
  }
#endif  // defined(SIGMA_DEBUG)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~Heap_SNodeC_MemPtr_NUM_CHILDREN()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this heap.  A heap is required to have no allocated
//  blocks when it destructs -- it must be empty.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (isEmpty())
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Heap_SNodeC_MemPtr_NUM_CHILDREN::~Heap_SNodeC_MemPtr_NUM_CHILDREN(void)
{
  CALL_TRACE("~Heap_SNodeC_MemPtr_NUM_CHILDREN()");
  CLASS_PRE_CONDITION((isEmpty()));

#if defined(SIGMA_DEBUG)
  if (Foundation::IsDebugOn(::ABSTRACT_HEAP)  ||
      Foundation::IsDebugOn(::FIXED_HEAP))
  {
    cout << "Destroying a Fixed Heap:\n";
    cout << "  Memory Start:        " << (void*) pMemBlock_ << '\n';
    cout << "  Memory End:          "
	 << (void*)(pMemBlock_ + (BLOCK_NODES_ * NUM_CHILDREN)) << '\n';
    cout << "  Max. Blocks:         " << NUM_CHILDREN << '\n';
    cout << "  Block Size (bytes):  " << (BLOCK_NODES_ * sizeof(Heap::Node))
	 << '\n' << endl;
  }
#endif  // defined(SIGMA_DEBUG)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                     [static]
//
//@ Interface-Description
//  Report the software fault that occured within the source code
//  of this class.  The fault, indicated by 'softFaultID', occured
//  at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
Heap_SNodeC_MemPtr_NUM_CHILDREN::SoftFault(const SoftFaultID softFaultID,
				const Uint32      lineNumber,
				const char*       pFileName,
				const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, FIXED_HEAP, lineNumber,
                          pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getFirstNode_()  [const, virtual]
//
//@ Interface-Description
//  This method overrides the base class's pure virtual method, and
//  returns a constant pointer to the first heap node.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

const Heap::Node*
Heap_SNodeC_MemPtr_NUM_CHILDREN::getFirstNode_(void) const
{
  CALL_TRACE("getFirstNode_()");

  return(pMemBlock_);
}   // $[TI1]


#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr)  [const, virtual]
//
// Interface-Description
//  This method overrides the base class's pure virtual method, and
//  dumps out the contents of this heap to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
Heap_SNodeC_MemPtr_NUM_CHILDREN::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "Heap_SNodeC_MemPtr_NUM_CHILDREN {\n";
  ostr << "  numMaxBlocks = " << getMaxBlocks() << '\n';
  ostr << "  blockSize    = " << getBlockSize() << '\n';
  ostr << "  numAllocated = " << getNumAllocated() << '\n';
  ostr << "  numAvailable = " << getNumAvailable() << '\n';
  ostr << "}\n";

  ostr.flush();

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
