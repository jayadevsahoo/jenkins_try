#ifndef NetFlowInspTrigger_HH
#define NetFlowInspTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: NetFlowInspTrigger - Detects patient effort during a flow 
//	  triggered exhalation.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/NetFlowInspTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By: rhj    Date: 07-Nov-2008      SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes:
//       Added net flow backup inspiratory trigger.
//       
//  Revision: 009 By: srp    Date: 28-May-2002   DR Number: 5909
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 008  By: syw     Date:  12-Dec-2000    DR Number: 5821
//  Project:  Metabolics
//  Description:
//		Obtain peak exh flow and time of occurance to be used to determine the
//			estimated time that the exh flow will reach base flow.
//		Hold off exhDryFlow < baseFlow + 1 condition by exhDryFlow >=
//			baseFlow + 1 at least once.
//		Set transientTime_ = 0.0 if the exh flow dips belows base flow and
//			recovers above base flow or the elapsed time is greater than
//			the estimated time that the exh flow will reach base flow.
//
//  Revision: 007  By: syw     Date:  5-Feb-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Updates for NeoMode
//
//
//  Revision: 006  By: syw     Date:  31-Oct-2000    DR Number: 5793
//  Project:  Metabolics
//  Description:
//		Changes due to new puffless controller implementaion.
//		Eliminate lungFlowConditionMet_, lungVolumeConditionMet_, and
//		exhAutozeroOccurred_.
//
//  Revision: 005  By: syw     Date:  02-Nov-1999    DR Number: 5573
//  Project:  ATC
//  Description:
//		Added lungFlowConditionMet_, exhAutozeroOccurred_, and
//		setLungFlowConditionMet().
//
//  Revision: 004  By: syw     Date:  14-Jun-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//		Added enable() and setLungVolumeConditionMet() method and
//		lungVolumeConditionMet_ data member.
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Trigger.hh"
//@ Usage-Classes
#include "PatientTrigger.hh"

//@ End-Usage

const Uint32 MAX_FLOW_PTS = 4 ;

class NetFlowInspTrigger : public PatientTrigger {
  public:
    NetFlowInspTrigger(const Trigger::TriggerId triggerId);
    virtual ~NetFlowInspTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

    virtual void enable(void);
	

	enum CycleState
	{
		TRANSIENT = 0,
		STEADY    = 1
	};

	Uint32 getCycleState( void);

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    NetFlowInspTrigger(const NetFlowInspTrigger&);		// not implemented...
    void   operator=(const NetFlowInspTrigger&);	// not implemented...

	//@ Data-Member: flowSlopeNegCount_ ;
	// number of conseutive cycles where flow slope is negative
	Uint32 flowSlopeNegCount_ ;

	//@ Data-Member: triggerFlow_[MAX_FLOW_PTS]
	// flow buffer used for triggering
	Real32 triggerFlow_[MAX_FLOW_PTS] ;
	
	//@ Data-Member: pressSlopeNegCount_
	// value indicates if pressure slope is negative
	Real32 pressSlopeNegCount_ ;
	
	//@ Data-Member: trigFlowCount_
	// counts number of times trigger event occurs
	Uint32 trigFlowCount_ ;
	
	//@ Data-Member: transientTime_
	// estimated transient duration
	Real32 transientTime_ ;

	//@ Data-Member: flowCondition1Met_
	// set TRUE when exhDryFlow > baseFlow
	Boolean flowCondition1Met_ ;

	//@ Data-Member: flowCondition2Met_
	// set TRUE when exhDryFlow > baseFlow + 1
	Boolean flowCondition2Met_ ;

	//@ Data-Member: exhFlowBelowBaseFlow_
	// set TRUE when exhFlow < base flow
	Boolean exhFlowBelowBaseFlow_ ;

	//@ Data-Member: timeToBaseFlowObtained_
	// set TRUE when time to base flow is calculated
	Boolean timeToBaseFlowObtained_ ;

	//@ Data-Member: elapsedTime_
	// time elapsed since trigger enabled (or exhalation time)
	Int32 elapsedTime_ ;

	//@ Data-Member: timeToBaseFlow_
	// estimated time for exh flow to reach base flow
	Real32 timeToBaseFlow_ ;

	//@ Data-Member: peakFlow_
	// maintains the last peak exh flow
	Real32 peakFlow_ ;

	//@ Data-Member: peakFlowTime_
	// maintains the time when peak flow obtained
	Real32 peakFlowTime_ ;

	//@ Data-Member: CycleState cycleState_
	// use to determine the current cycle state
	CycleState cycleState_ ;

	Real32 dOffset_;   //the delta between actual exh dry flow and offsetted exh dry flow


} ;

#endif // NetFlowInspTrigger_HH 
