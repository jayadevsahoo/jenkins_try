// SWATSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SWATSettingsDlg.h"
#include "Swat.h"


// CSWATSettingsDlg dialog

IMPLEMENT_DYNAMIC(CSWATSettingsDlg, CDialog)

CSWATSettingsDlg::CSWATSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSWATSettingsDlg::IDD, pParent)
	, m_bRecordSWinReport(true)
	, m_bAppendTimeInStatusWindow(FALSE)
	, m_bSendMail(FALSE)
	, m_nEditorType(0)
	, m_bLockReports(FALSE)
{

}

CSWATSettingsDlg::~CSWATSettingsDlg()
{
}

void CSWATSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Radio(pDX, IDC_RADIO_DECODA, m_nEditorType);
	DDX_Control(pDX, IDC_CHECK_RECORD_SW_IN_REPORT, m_Radio_SW);
	DDX_Check(pDX, IDC_CHECK_APPEND_TIME, m_bAppendTimeInStatusWindow);
	DDX_Check(pDX, IDC_CHECK_EMAIL, m_bSendMail);
	DDX_Check(pDX, IDC_CHECK_LOCK_REPORTS, m_bLockReports);
}


BEGIN_MESSAGE_MAP(CSWATSettingsDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSWATSettingsDlg::OnBnClickedOk)
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////


BOOL CSWATSettingsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CSwatApp *pApp = (CSwatApp *)AfxGetApp();
	if(pApp->m_strEditorPath.Find("Decoda") != -1)
	{
		m_nEditorType = 0;
	}
	else
	{
		m_nEditorType = 1;
	}
	

	if(pApp->m_strRecordSWinReport=="false")
		m_Radio_SW.SetCheck(0);
	else
		m_Radio_SW.SetCheck(1);

	m_bSendMail = pApp->m_bSendMail ;

	if(pApp->m_strLockPassReports == "true")
		m_bLockReports = 1;
	else
		m_bLockReports = 0;


	m_bAppendTimeInStatusWindow = pApp->m_bAppendTimeInStatusWindow;
	
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


// CSWATSettingsDlg message handlers

void CSWATSettingsDlg::OnBnClickedOk()
{
	CSwatApp *pApp = (CSwatApp *)AfxGetApp();
	UpdateData(TRUE);

	if(m_nEditorType == 0)
	{
		pApp->m_strEditorPath = pApp->m_strSWAT_DECODA_PATH;
	}
	else
	{
		pApp->m_strEditorPath = pApp->m_strSWAT_SCITE_PATH;
	}
	pApp->SaveRegistrySetting(SWAT_REG_PARAM_LUA_EDITOR, pApp->m_strEditorPath);


	int n = m_Radio_SW.GetCheck();
	if(n == 1)
		pApp->m_strRecordSWinReport = "true";  
	else if(n==0)
		pApp->m_strRecordSWinReport = "false";  


	pApp->SaveRegistrySetting(SWAT_REG_PARAM_RECORD_SW_VER, pApp->m_strRecordSWinReport);

	//AfxMessageBox(ch);

	char ch[200];
	pApp->m_bSendMail = m_bSendMail ? true : false;
	_itoa(m_bSendMail,ch , 10);
	pApp->SaveRegistrySetting(SWAT_SEND_EMAIL, ch);

	pApp->m_bAppendTimeInStatusWindow = m_bAppendTimeInStatusWindow ? true : false;

	_itoa(m_bAppendTimeInStatusWindow,ch , 10);
	pApp->SaveRegistrySetting(SWAT_REG_PARAM_APPEND_TIME, ch);


	if(m_bLockReports == 1)
		pApp->m_strLockPassReports = "true";
	else
		pApp->m_strLockPassReports = "false";

	pApp->SaveRegistrySetting(SWAT_SET_REPORT_READ_ONLY, pApp->m_strLockPassReports);
	



	OnOK();
}
