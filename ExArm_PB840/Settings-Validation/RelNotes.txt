
02-08-99  sah	Adding a dynamic applicability mechanism to replace all of the
		unmanageable applicability code in GUI-Apps.  Now GUI will only
		need to decide the size and location of a setting button, and
		this new mechanism will handle when its drawn. (DCS #5314)

		Also, as part of this new mechanism, introduced new BiLevel-
		specific settings. (DCS #5314)

		Added IBW-based bounds to Volume Limit, and tied alarm bar
		scaling to these constraints, rather than absolute min/max
		values.

01-11-99  sah	Made those 'SoftFault()' methods that are NOT called in
                PRODUCTION mode, DEVELOPMENT-only.

01-07-99  sah	Added new ATC settings.

01-06-99  sah	Added new applicability functionality that can be used by
		Settings and others (especially GUI-Apps).  (DCS #5214)

		Modified 'ContextId' type name and value names to more usable,
		shortened names.  (This affected DCI, Alarms and GUI-Apps, as
		well.)

10-20-98  sah	Final preparation for formal review.  Changed all 'SoftFault()'
		methods to be non-inlined, and obsoleted empty '.in' files that
		resulted.

08-06-98 dosman Merged Rev "Color" (1.68.1.0) into Rev "BiLevel" (1.68.2.0)

12-18-97  sah	Added logic to initiate a "sync" transaction when in SST state
		(DCS #470 -- NPostponed Sigma Features).

03-25-98  sah	Added soft-bound capability, along with stucturing cleanup.

12-18-97  sah	Added logic to initiate a "sync" transaction when in SST state
		(DCS #470 -- NPostponed Sigma Features).

12-08-97  sah	Modified dependency mechanism to distinguish dependencies based on
		"mode" (i.e., 'PRODUCTION' and 'DEVELOPMENT'), as well as, the
		"cpu" and "platform".  Also, added logic at end of this file to
		not try to include the '.makedepend.*' files if they don't exist.
