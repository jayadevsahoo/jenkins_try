#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PowerupScheduler - The scheduler that is active on power up
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for determining the next scheduler on power up,
// and for relinquishing control to that scheduler. A method is implemented to
// initialize the active mode triggers on the mode trigger list to be used by
// the mode trigger mediator. The same initialize method instructs this
// scheduler to take control. A startup trigger, valid in this mode, is
// guaranteed to always evaluate to true, thus ensuring an immediate transition
// from this scheduler to the scheduler evaluated by the method that
// relinquishes control. This scheduler is active only during the first BD
// cycle and therefore no breath sustaining requirements exist.  This scheduler
// collaborates with POST to get the type of the power interruption, which then
// determines the next scheduler.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates the rules associated with starting up the ventilator
// every time power is cycled or interrupted.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class implements methods to engage this scheduler as the first
// scheduler following power up. It also provides the mechanism to transfer
// control to the next valid scheduler so that the power up process is
// properly accomplished.
// The scheduler records itself in the breath record even though no spirometry
// or other breath parameters exist.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PowerupScheduler.ccv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 031   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 030   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added case to reject RM maneuvers during power up.
//
//  Revision: 029  By: syw     Date:  14-Jun-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Call the base class NewBreath() method instead of derived class.
//
//  Revision: 028  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 027  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling of inspiratory pause event.
//
//  Revision: 026  By: iv    Date:  12-Aug-1997    DR Number: DCS 2354
//  	Project:  Sigma (840)
//		Description:
//			In relinquishControl(), after a short power interruption, send circuit
//          connect active to gui for all cases except disconnect and standby.
//
//  Revision: 025  By: iv    Date:  11-Aug-1997    DR Number: DCS 2352
//  	Project:  Sigma (840)
//		Description:
//			In relinquishControl(), set the Standby required flag to true
//          for long power interruption. Notify alarm regarding startup disconnect
//          only if next scheduler is disconnect.
//
//  Revision: 024  By: syw   Date:  23-May-1997    DR Number: DCS 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 023  By:  iv    Date:  22-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review - fix unit
//             test.
//
//  Revision: 022  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 021 By:  iv   Date:   06-Feb-1997    DR Number: DCS 1737, 1738
//       Project:  Sigma (840)
//       Description:
//             Use the methods Get/SetIsStandbyRequired() to be used by the
//             svo scheduler if required.
//
//  Revision: 020  By:  iv    Date:  04-Feb-1997    DR Number: DCS 1697
//       Project:  Sigma (840)
//       Description:
//             Added a SVO trigger to the scheduler, so that when no gas supply
//             is present on powerup, the safety valve is not closed for 500 msec.
//
//  Revision: 019  By:  iv    Date:  25-Jan-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 018  By:  iv    Date:  20-Dec-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 017  By:  iv    Date:  03-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             relinquishControl(): Add the StandbyScheduler as another scheduler
//             that has to be considered after a short power interruption.
//             Cleanup for code inspection.
//
//  Revision: 016  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 015  By:  iv   Date:  13-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//          Changed class name from BDTasks to BdTasks.
//
//  Revision: 014 By:  iv   Date:   02-Aug-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Prepare for breathing by closing the safety valve only if the
//             next scheduler is not OSC or SVO.
//
//  Revision: 013 By:  iv   Date:   09-Jul-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Reset apnea and SafetyPcv active flags for ac switch,
//             short power interruption with no vent setup, and long power
//             interruption.
//
//  Revision: 012 By:  iv   Date:   21-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Added a call to notify the gui about patient connect during
//             short power interruption.
//
//  Revision: 011 By:  iv   Date:   16-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//		       Changed relinquishControl() to consider the case
//             where power is interrupted or an intentional reset is
//             caused during Service or SST.
//
//  Revision: 010 By:  iv   Date:   14-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed the check for sst major fault to call:
//               BdTasks::IsSstMajorFailure().
//
//  Revision: 009 By:  iv   Date:   04-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed logic of transition back to apnea.
//             Fixed call to base class takeControl() from takeControl().
//             Eliminated sysInitComplete flag.
//
//  Revision: 008 By:  iv   Date:   18-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added debbuging information and changed the check for
//             startupState == WATCHDOG to != POWERFAIL.
//
//  Revision: 007 By:  iv   Date:   15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 006 By:  iv   Date:   15-Feb-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 005 By:  iv   Date:  25-Jan-1996    DR Number: DCS 672
//       Project:  Sigma (R8027)
//       Description:
//            Changed relinquishControl() to disallow transition to Service
//            Mode after short power interruption when vent setup is complete.
//            Changed relinquishControl() to allow transition back to Svo, Osc,
//            and Disconnect after a short power interruption.
//
//  Revision: 004 By:  iv   Date:  13-Dec-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed CheckIfApneaPossible_() to CheckIfApneaPossible();
//             Added a check to SST Major flag to determine transition to
//             SVO.
//
//  Revision: 003 By:  kam   Date:  06-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from takeControl().
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem and
//             for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

# include "PowerupScheduler.hh"
# include "ModeTriggerRefs.hh"
# include "TriggersRefs.hh"
# include "TimersRefs.hh"
# include "SchedulerRefs.hh"
# include "BreathMiscRefs.hh"
# include "PhaseRefs.hh"
# include "BdDiscreteValues.hh"
# include "ValveRefs.hh"

//@ Usage-Classes
# include "Post.hh"
# include "ApneaInterval.hh"
# include "BdSystemStateHandler.hh"
# include "ModeTriggerMediator.hh"
# include "BreathTriggerMediator.hh"
# include "Trigger.hh"
# include "ModeTrigger.hh"
# include "BreathSet.hh"
# include "PhasedInContextHandle.hh"
# include "BreathPhase.hh"
# include "BdAlarms.hh"
# include "ServiceMode.hh"
# include "SmSwitchConfirmation.hh"
# include "BdTasks.hh"
# include "VentAndUserEventStatus.hh"
#include "Solenoid.hh"
//@ End-Usage

#ifdef SIGMA_DEBUG
#include "Ostream.hh"
#endif // SIGMA_DEBUG

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PowerupScheduler()  [Default Constructor]
//
//@ Interface-Description
// Constructs the class instance with the proper id. The list
// of valid mode triggers, used by the mode trigger mediator is
// initialized as well.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor takes no arguments. It invokes the base class
// constructor with the POWER_UP id argument. It initializes the private data
// member pModeTriggerList_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	The number of elements on the trigger list is asserted
//  to be MAX_NUM_POWERUP_TRIGGERS and MAX_NUM_POWERUP_TRIGGERS to be less than
//  MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_POWERUP_TRIGGERS = 2;
PowerupScheduler::PowerupScheduler(void) :
					BreathPhaseScheduler(SchedulerId::POWER_UP)
{
// $[TI1]
	CALL_TRACE("PowerupScheduler::PowerupScheduler(void)");

	Int32 ii = 0;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RStartupTrigger;
	pModeTriggerList_[ii] = (ModeTrigger*)NULL;

	CLASS_ASSERTION(ii == MAX_NUM_POWERUP_TRIGGERS &&
						MAX_NUM_POWERUP_TRIGGERS < MAX_MODE_TRIGGERS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PowerupScheduler()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PowerupScheduler::~PowerupScheduler(void)
{
  CALL_TRACE("~PowerupScheduler()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
// The method takes a const reference to a BreathTrigger type and has no return
// value.  The startup phase is registered as the current breath.
//---------------------------------------------------------------------
//@ Implementation-Description
// The static method in BreathPhase is called to register the startup
// breath phase reference rStartupPhase.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PowerupScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)
{
// $[TI1]
	CALL_TRACE("PowerupScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

	BreathPhase::SetCurrentBreathPhase((BreathPhase&)RStartupPhase, breathTrigger);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
// This method accepts a mode trigger reference and has no return value.  It
// checks the startup state, set by POST, and uses it as a basis to determine
// the next scheduler. The previous scheduler is retrieved from the nov ram
// object BdSystemState. Before the new mode is instructed to take control, all
// mode triggers on the current trigger list get reset.  If power was cycled by
// the power switch or via an intentional reset, control is given to the
// standby scheduler, unless sst is required or total loss of gas is detected,
// in which case SVO conditions are established.  If there was a short power
// interruption or any other powerup that was not a powerfail or not an ac
// switch or intentional reset event, and vent setup was completed, the next
// scheduler is set to the previous scheduler if the previous was one of the
// following: apnea, disconnect, occlusion, standby, or svo; otherwise, it is
// set to the set scheduler. The only exception is that if no gas supply
// present on power up, the next scheduler is set to SvoScheduler regardless of
// the previous scheduler. In that state Gui is notified of patient connect.
// If there was a short power interruption and vent setup has NOT been
// completed, the next scheduler is set to be the StandbyScheduler.  For long
// power interruption, if sst is not required, the disconnect scheduler is
// given control and the Alarms subsystem is notified of a startup disconnect
// condition.  For long power interruption, if sst is required, SVO conditions
// are established.  If the next scheduler is disconnect or standby, an
// immediate breath trigger is enabled and the safety valve is closed. If the
// next scheduler is occlusion or svo, an immediate breath trigger is enabled
// and the safety valve stays open. Otherwise, the safetyValveClosedPhase is
// set to be deliverd.
//---------------------------------------------------------------------
//@ Implementation-Description
// The interruption status, and the vent setup status are checked using Post
// static methods. A local variable is defined to point to the next scheduler -
// pNextScheduler.  pNextScheduler is assigned the address of the scheduler
// that is determined to be the next scheduler e.g. rStandbyScheduler.  The NOV
// RAM instance rBdSystemState is checked by the BdSystemStaetHandler for the
// previous scheduler which then is assigned, if required, to pNextScheduler.
// A class variable IsStandbyRequired_ is used (via Get/Set access methods) to
// signify the need to employ standby mode when Svo scheduler will relinquish
// control.  The immediate breath trigger is enabled, using the method enable()
// in the instance variable rImmediateBreathTrigger. Once the next scheduler is
// determined, it is instructed to take control.
//---------------------------------------------------------------------
//@ PreCondition
// the trigger id is checked to be STARTUP or SVO.
// The startup state and downTimeStatus are checked for the following:
//		startupState == ACSWITCH || WATCHDOG ||
//						(POWERFAIL &&
//                       downTimeStatus == DOWN_SHORT || DOWN_LONG)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PowerupScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{
	CALL_TRACE("PowerupScheduler::relinquishControl(const ModeTrigger& modeTrigger)");

    const Trigger::TriggerId triggerId = modeTrigger.getId();
	// the only trigger that collaborates with the Powerup Scheduler
	// is the StartupTrigger
	CLASS_PRE_CONDITION( Trigger::STARTUP == triggerId ||
	                     Trigger::SVO == triggerId );

	const ShutdownState startupState = Post::GetStartupState();
	const DowntimeStatus downTimeStatus = Post::GetDowntimeStatus();
	SchedulerId::SchedulerIdValue schedulerId = BdSystemStateHandler::GetSchedulerId();
	const Boolean isVentSetupComplete = PhasedInContextHandle::AreBdSettingsAvailable();
	BreathPhaseScheduler* pNextScheduler = NULL;

	// reset all mode triggers for this scheduler
	RModeTriggerMediator.resetTriggerList();

	if(startupState == ACSWITCH || startupState == INTENTIONAL)
	{
	// $[TI1]
		BdSystemStateHandler::UpdateApneaActiveStatus(FALSE);
		BdSystemStateHandler::UpdateSafetyPcvActiveStatus(FALSE);
		// $[00526]
		// power was cycled via the power switch
		// check if SST is required. If it does, start svo scheduler.
		if (BdTasks::IsSstRequired() || Trigger::SVO == triggerId)
		{
		// $[TI1.1]
		    BreathPhaseScheduler::SetIsStandbyRequired(TRUE);
			pNextScheduler = (BreathPhaseScheduler*)&RSvoScheduler;
		}
		else
		{
		// $[TI1.2]
			pNextScheduler = (BreathPhaseScheduler*)&RStandbyScheduler;
		}
	}
	else if( (startupState != POWERFAIL)
#ifndef SIGMA_UNIT_TEST
             ||
			 ( (startupState == POWERFAIL) &&
			   (downTimeStatus == DOWN_SHORT) )
#endif // SIGMA_UNIT_TEST
           )
	{
	// $[TI2]
		// $[00525] $[00530]
		if(isVentSetupComplete)
		{
		// $[TI2.1]
	        // Can not enter Service Mode from short power interruption
	        SmSwitchConfirmation::SetIsOkayToEnterSm(FALSE);
			SAFE_CLASS_ASSERTION(schedulerId != SchedulerId::SAFETY_PCV);
			// check for the active mode prior to the short power
			// interruption.
			if(schedulerId  == SchedulerId::SVO || Trigger::SVO == triggerId)
			{
			// $[TI2.1.4]
				pNextScheduler = (BreathPhaseScheduler*)&RSvoScheduler;
     			// notify BD Status Task of patient connection
		    	VentAndUserEventStatus::PostEventStatus (EventData::PATIENT_CONNECT, EventData::ACTIVE);
			}
			else if(schedulerId  == SchedulerId::APNEA)
			{
			// $[TI2.1.1]
				pNextScheduler = (BreathPhaseScheduler*)&RApneaScheduler;
     			// notify BD Status Task of patient connection
		    	VentAndUserEventStatus::PostEventStatus (EventData::PATIENT_CONNECT, EventData::ACTIVE);
			}
			else if(schedulerId  == SchedulerId::DISCONNECT)
			{
			// $[TI2.1.2]
				pNextScheduler = (BreathPhaseScheduler*)&RDisconnectScheduler;
			}
			else if(schedulerId  == SchedulerId::STANDBY)
			{
			// $[TI2.1.6]
				pNextScheduler = (BreathPhaseScheduler*)&RStandbyScheduler;
			}
			else if(schedulerId  == SchedulerId::OCCLUSION)
			{
			// $[TI2.1.3]
				pNextScheduler = (BreathPhaseScheduler*)&ROscScheduler;
     			// notify BD Status Task of patient connection
		    	VentAndUserEventStatus::PostEventStatus (EventData::PATIENT_CONNECT, EventData::ACTIVE);
			}
			else
			{
			// $[TI2.1.5]
				pNextScheduler = (BreathPhaseScheduler*)
							(&BreathPhaseScheduler::EvaluateSetScheduler());
     			// notify BD Status Task of patient connection
		    	VentAndUserEventStatus::PostEventStatus (EventData::PATIENT_CONNECT, EventData::ACTIVE);
			}
		}
		else  // !isVentSetupComplete
		{
		// $[TI2.2]
			BdSystemStateHandler::UpdateApneaActiveStatus(FALSE);
			BdSystemStateHandler::UpdateSafetyPcvActiveStatus(FALSE);
			// this covers the case of short power interruption, or intentional reset
			//  during service or sst.			
			pNextScheduler = (BreathPhaseScheduler*)&RStandbyScheduler;
		}
	}
	else if( (startupState == POWERFAIL)
#ifndef SIGMA_UNIT_TEST
             && (downTimeStatus == DOWN_LONG)
#endif // SIGMA_UNIT_TEST
           )
	{
	// $[00526]
	// $[TI3]
		BdSystemStateHandler::UpdateApneaActiveStatus(FALSE);
		BdSystemStateHandler::UpdateSafetyPcvActiveStatus(FALSE);
		// check if SST is required, if it does, set the
		// SvoScheduler as the next scheduler.
		if ( BdTasks::IsSstRequired() || Trigger::SVO == triggerId )
		{
		// $[TI3.1]
		    BreathPhaseScheduler::SetIsStandbyRequired(TRUE);
			pNextScheduler = (BreathPhaseScheduler*)&RSvoScheduler;
		}
		else
		{
		// $[TI3.2]
			pNextScheduler = (BreathPhaseScheduler*)&RDisconnectScheduler;
            // $[05166]
            //notify alarms on "Startup disconnect" alarm.
            RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_STARTUP_DISCONNECT);
		}
	}
	else
	{
	// $[TI4]
		CLASS_PRE_CONDITION( (startupState == ACSWITCH) ||
							 (startupState == INTENTIONAL) ||
							 (startupState == WATCHDOG) ||
							 ( (startupState == POWERFAIL) &&
							 ( (downTimeStatus == DOWN_SHORT) ||
							   (downTimeStatus == DOWN_LONG) ) ) );
	}

	SchedulerId::SchedulerIdValue nextSchedulerId = pNextScheduler->getId();
	if (nextSchedulerId == SchedulerId::STANDBY ||
		nextSchedulerId == SchedulerId::DISCONNECT)
	{
	// $[TI5]
		// enable an immediate breath trigger
		((Trigger&)RImmediateBreathTrigger).enable();
	  	RSafetyValve.close() ;
	}
	else if (nextSchedulerId == SchedulerId::OCCLUSION ||
			 nextSchedulerId == SchedulerId::SVO)
	{
	// $[TI6]
		// enable an immediate breath trigger
		((Trigger&)RImmediateBreathTrigger).enable();
	}
	else
	{
	// $[TI7]
		// Set SVC as the active BreathPhase to close the safety valve.
		// Since relinquishControl and SetCurrentBreathPhase() requires the trigger
		// causing the transition, send a reference to the immediate breath trigger.
		BreathPhase::SetCurrentBreathPhase((BreathPhase&)RSafetyValveClosePhase,
											(BreathTrigger&)RImmediateBreathTrigger);
		BreathPhase::NewBreath() ;
	}

	// instruct the next scheduler to take control:
	pNextScheduler->takeControl(*this);

#ifdef SIGMA_DEBUG
	cout << "BD: Down time status = ";
	if (downTimeStatus == DOWN_SHORT)
		cout << "DOWN_SHORT" << endl;
	else cout << "DOWN_LONG" << endl;

	cout << "Post startup state = ";
	if (startupState == ACSWITCH)
		cout << "ACSWITCH" << endl;
	else if (startupState == WATCHDOG)
		cout << "WATCHDOG" << endl;
	else if (startupState == POWERFAIL)
		cout << "POWERFAIL" << endl;
	else if (startupState == UNKNOWN)
		cout << "UNKNOWN" << endl;
	else if (startupState == EXCEPTION)
		cout << "EXCEPTION" << endl;
	else if (startupState == INTENTIONAL)
		cout << "INTENTIONAL" << endl;

	cout << "AreBdSettingsAvailable" << isVentSetupComplete << endl;
#endif // SIGMA_DEBUG
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
// This method can only work if the constructor for this object has
// initialized the mode trigger list. The mode trigger mediator is
// initialized to use the PowerupScheduler list of mode triggers.
// This scheduler is instructed to take control as part of the
// initialization process.
//---------------------------------------------------------------------
//@ Implementation-Description
// The trigger mediator instance: rModeTriggerMediator is used to set
// the active trigger list. The member function takeControl() is called
// from this method.
//---------------------------------------------------------------------
//@ PreCondition
//		pModeTriggerList_[0] == (ModeTrigger*)&RStartupTrigger
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PowerupScheduler::initialize(void)
{
// $[TI1]
	CALL_TRACE("PowerupScheduler::initialize(void)");

	CLASS_PRE_CONDITION(pModeTriggerList_[0] == (ModeTrigger*)&RSvoTrigger &&
	                    pModeTriggerList_[1] == (ModeTrigger*)&RStartupTrigger );
	RModeTriggerMediator.setActiveTriggerList(pModeTriggerList_);
	takeControl(*this);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
// A new breath record is created for the powerup scheduler. The mode
// triggers for this scheduler are enabled. The base class method for
// takeControl() is invoked to handle activities common to all schedulers.
//---------------------------------------------------------------------
//@ Implementation-Description
// The breath set instance rBreathSet is used to create a new breath record
// with the mode schduler id, the mandatory type and the breath type.  The
// member function enableTriggers_() is invoked to set the mode triggers to the
// appropriater state.
//---------------------------------------------------------------------
//@ PreCondition
// The schedulerId is checked for POWER_UP value.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PowerupScheduler::takeControl(const BreathPhaseScheduler& rScheduler)
{
// $[TI1]
	CALL_TRACE("PowerupScheduler::takeControl(const BreathPhaseScheduler& rScheduler)");

    // Invoke the base class takeControl method
    BreathPhaseScheduler::takeControl(rScheduler);

	SchedulerId::SchedulerIdValue schedulerId = rScheduler.getId();
	CLASS_PRE_CONDITION(schedulerId == SchedulerId::POWER_UP);

    // update the reference to the newly active breath scheduler
    BreathPhaseScheduler::SetCurrentScheduler_((BreathPhaseScheduler&)(*this));

	//Update the breath record
 	RBreathSet.newBreathRecord(SchedulerId::POWER_UP,
 						::NULL_MANDATORY_TYPE,    	// no mandatory type is specified
 						NON_MEASURED,    // breath type
 						BreathPhaseType::NON_BREATHING
 	);

	determineBreathPhase((BreathTrigger&)RImmediateBreathTrigger);
	enableTriggers_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
PowerupScheduler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, POWERUPSCHEDULER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// The method takes no arguments and returns no value.  The Powerup scheduler
// valid mode triggers are enabled whenever the scheduler takes control.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode trigger instances are accessed directly with a call to their
// enable() method. Only one trigger is enabled for this scheduler.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
PowerupScheduler::enableTriggers_(void) 
{
// $[TI1]
	CALL_TRACE("PowerupScheduler::enableTriggers_(void)");

	((Trigger&)RSvoTrigger).enable();
	((Trigger&)RStartupTrigger).enable();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[01255]
// The argument of type EventId indicates which user event is active.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is rejected by
// invoking a call to the static method:
// BreathPhaseScheduler::RejectManualInspiration_(eventStatus).
// The alarm reset event is ignored, while the eventStatus argument value is checked for
// TRUE.
// The expiratory and inspiratory pause events are rejected by invoking a call to
// the static methods: BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus) and
// BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus) .
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
PowerupScheduler::reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus)
{
	CALL_TRACE("PowerScheduler::reportEventStatus_(const EventData::EventId id,\
													 const Boolean eventStatus)");

	EventData::EventStatus rtnStatus = EventData::IDLE;

	switch (id)
	{
		case EventData::MANUAL_INSPIRATION:
		// $[TI1]
			rtnStatus =
					BreathPhaseScheduler::RejectManualInspiration_(eventStatus);
			break;

		case EventData::ALARM_RESET:
		// $[TI2]
			// ignore alarm reset
			CLASS_PRE_CONDITION(eventStatus);
			break;

		case EventData::EXPIRATORY_PAUSE:
		// $[TI3]
			rtnStatus =
					BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus);
			break;

		case EventData::INSPIRATORY_PAUSE:
		// $[TI4]
		// inspiratory pause shall not be active
		// $[BL04007]
			rtnStatus =
					BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus);
			break;

		case EventData::NIF_MANEUVER:
		case EventData::P100_MANEUVER:
		case EventData::VITAL_CAPACITY_MANEUVER:
			rtnStatus = EventData::REJECTED;
			break;


		default:
		// $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(id);
	}	
	return (rtnStatus);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


