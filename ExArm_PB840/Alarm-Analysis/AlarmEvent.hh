#ifndef AlarmEvent_HH
#define AlarmEvent_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AlarmEvent - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmEvent.hhv   25.0.4.0   19 Nov 2013 13:51:14   pvcs  $
//
//@ Modification-Log
//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//   Revision 002	By:   sah	   Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//	   Removed all referenced to newly-obsoleted class ('AlarmMessages').
//  
//   Revision 003   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

//@ Usage-Classes
#include "Alarm.hh"
#include "Alarm_Analysis.hh"
#include "MessageNameArray.hh"
#include "TimeStamp.hh"

class AlarmUpdate;
class AlarmAnnunciator;
struct AlarmAction;
//@ End-Usage

class AlarmEvent 
{
  public:

    AlarmEvent(MessageName baseMessage,
               const TimeStamp& rWhenOccurred,
               Alarm::UpdateType updateType);

    AlarmEvent(const AlarmEvent& rEvent);
    AlarmEvent(const AlarmUpdate& rUpdate);
    AlarmEvent(const AlarmAction& rAction);
    ~AlarmEvent();

    Boolean operator==(const AlarmEvent& rEntry) const;
    Boolean operator!=(const AlarmEvent& rEntry) const;
    Boolean operator< (const AlarmEvent& rEntry) const;
    Boolean operator> (const AlarmEvent& rEntry) const;

    AlarmAction makeAlarmAction(void);

    MessageName getUpdateTypeMessage   (void) const;
    MessageName getUrgencyMessage      (void) const;

    inline        MessageName        getBaseMessage    (void) const;
    inline const  MessageNameArray&  getAnalysisMessage(void) const;
    inline const  TimeStamp&         getTimeStamp      (void) const;
    inline Uint32 getAnalysisIndex   (void)   const;

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


  protected:

  private:

    AlarmEvent(); // Declared only
    void operator=(const AlarmEvent&); // Declared only.

    //@ Data-Member:    whenOccurred_
    // This attribute contains the time the event causing the Alarm History Log 
    // entry occurred.
    const TimeStamp whenOccurred_;

    //@ Data-Member:    currentUrgency_
    // The urgency associated with the Alarm History Log entry.
    const Alarm::Urgency currentUrgency_;

    //@ Data-Member:    baseMessage_
    // The text identifying the Alarm or alarm event associated with the
    // Alarm History Log entry.
    const MessageName baseMessage_;

    //@ Data-Member:    currentAnalysisMessage_
    // The analysis message associated with the Alarm History Log entry.
    const MessageNameArray currentAnalysisMessage_;

    //@ Data-Member:    analysisIndex_
    // Attribute analysisIndex_ indicates how many message enumerators are
    // contained in the currentAnalysisMessage_ array.
    const Uint32 analysisIndex_;

    //@ Data-Member:    updateType_
    // Attribute udpateType_ contains an enumerator indicating the reason for
    // the Alarm History Log entry.
    const Alarm::UpdateType updateType_;   


};

// Inlined methods
#include "AlarmEvent.in"

#endif // AlarmEvent_HH 
