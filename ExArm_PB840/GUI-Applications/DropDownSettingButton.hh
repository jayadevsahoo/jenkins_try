#ifndef DropDownSettingButton_HH
#define DropDownSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: DropDownSettingButton - A special type of DiscreteSettingButton
// that displays a drop-down menu, containing each of the allowable discrete
// values.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DropDownSettingButton.hhv   25.0.4.0   19 Nov 2013 14:07:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  09-Apr-2007    SCR Number:  6237
//  Project:  Trend
//  Description:
//      Added configurable drop down text size.
//
//  Revision: 001  By: sah     Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added dropdown buttons
//
//====================================================================

#include "DiscreteSettingButton.hh"

//@ Usage-Classes
#include "Box.hh"
//@ End-Usage

class DropDownSettingButton : public DiscreteSettingButton
{
public:
	DropDownSettingButton(Uint16 xOrg, Uint16 yOrg, Uint16 width,
						  Uint16 height, ButtonType buttonType, Uint8 bevelSize,
						  CornerType cornerType, BorderType borderType,
						  StringId titleText,
						  SettingId::SettingIdType settingId,
						  SubScreen *pFocusSubScreen,
						  StringId messageId, Uint valuePointSize,
						  Boolean isMainSetting = FALSE);
	~DropDownSettingButton(void);

	void setDropDownTextSize(const Int32 dropDownTextSize);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
								const SettingSubject*         pSubject);

	// Button virtual methods
	virtual void downHappened_(Boolean byOperatorAction);
	virtual void upHappened_(Boolean byOperatorAction);

	// SettingObserver virtual method
	virtual void  applicabilityUpdate(
									 const Notification::ChangeQualifier qualifierId,
									 const SettingSubject*               pSubject
									 );

private:
	// these methods are purposely declared, but not implemented...
	DropDownSettingButton(const DropDownSettingButton&);	// not implemented...
	void   operator=(const DropDownSettingButton&);	   // not implemented...

	enum
	{
		MAX_DROPDOWN_VALUES = 8
	};

	void  setValueString_(StringId       formatString,
						  TextField&     rTextField) const;

	//@ Data-Member: arrValueArea_
	// The container for each TextField in the drop down menu.
	Container  arrValueArea_[MAX_DROPDOWN_VALUES];

	//@ Data-Member: arrValueArea_
	// The TextField for each drop down menu item.
	TextField  arrValueFields_[MAX_DROPDOWN_VALUES];

	//@ Data-Member: arrValueState_
	// Boolean for current state (enabled or disabled) of each setting value
    Boolean  arrValueState_[MAX_DROPDOWN_VALUES];

	//@ Data-Member: dropDownBox_
	// The container for the drop down menu items in arrValueArea_.
	Container  dropDownBox_;

	//@ Data-Member: dropDownTextSize_
	// The size of the text in the drop down menu. Defaults to 10
	// points.
	Int32 dropDownTextSize_;

	//@ Data-Member: fieldHeight_
	// The height of each text field in the drop down menu.
	Int32 fieldHeight_;

	//@ Data-Member: isFirstUpdate_
	// Set TRUE before the drop-down menu is displayed for the first time
	// after a button press. Forces a complete rebuild of the menu.
	Boolean isFirstUpdate_;

	//@ Data-Member: highlightedValue_
	// The setting value that's currently highlighted in the menu
	Uint16 highlightedValue_;
};


#endif // DropDownSettingButton_HH 
