#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: WobGraphic - Displays a sliding-scale representation of PAV's
// patient work-of-breathing (WOBpt), along with indication as to the breakdown
// between resistive and elastive contributions to this total WOB (WOBtot).
// $[PA01012] -- The WOB plot shall be a graphical representation of 
// the estimated patient's work of breathing (WOB pt) and estimated total 
// (patient + ventilator) work of breathing (WOB tot) values referenced to 
// low (0-0.3), normal (0.3-0.7), above normal (0.7-1.0) and high (1.0-2.0) 
// ranges as depicted below. Included in the patient's WOB representation will 
// be the relative portions of resistance (R) and elastance (E). 
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WobGraphic.ccv   25.0.4.0   19 Nov 2013 14:08:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gfu    Date:  27-Jul-2004    SCR Number: 6133
//  Project:  PAV
//  Description:
//       Modified module per SDCR #6133.  Based on input from the 
//       Field Evaluations in Canada: the area on the WOB graphic 
//       which is colored red should be changed to another color as
//       red has a negative meaning. 
//    
//  Revision: 004   By: ccyang Date:  11-Sept-2003   SCR Number: 6144
//  Project:  NIV1
//  Description:
//       SDCR6114 update PA requirement tracing 
//   
//  Revision: 003   By: sah    Date:  27-Mar-2001    DCS Number: 5866
//  Project:  PAV
//  Description:
//      No longer draw extended WOBtot display when WOBtot value is greater-
//      than or equal-to the maximum scale value.
//
//  Revision: 002   By: sah    Date:  22-Jan-2001    DCS Number: 5835 & 5841
//  Project:  PAV
//  Description:
//      No longer display WOB data during Apnea, Safety-PCV, OSC and
//      Disconnect.
//
//  Revision: 001    By: sah    Date: 20-Nov-2000   DCS Number: 5805
//  Project:  PAV
//  Description:
//      Added for PAV data representation.
//
//=====================================================================

#include "WobGraphic.hh"
#include "MiscStrs.hh"
#include "BdGuiEvent.hh"

//@ Usage-Classes
#include "PatientDataRegistrar.hh"
#include "BreathDatumHandle.hh"
#include "BdEventRegistrar.hh"
#include "UpperSubScreenArea.hh" // PAV3_030415_LJS
#include "WaveformsSubScreen.hh" // PAV3_030415_LJS
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32  CONTAINER_WIDTH_  = 630;
static const Int32  CONTAINER_HEIGHT_ = 130;

static const Int32  WOB_LABEL_TEXT_HEIGHT_ = 14;		// WOB label (p=10)...

static const Int32  MIN_SCALE_X_ = 75;
static const Int32  MAX_SCALE_X_ = (MIN_SCALE_X_ + 495);

static const Int32  SCALE_CONTAINER_Y_ = ((CONTAINER_HEIGHT_ + 1) / 2);
static const Int32  SCALE_CONTAINER_HEIGHT_ = 8;
static const Int32  SCALE_CONTAINER_WIDTH_ =
							(MAX_SCALE_X_ - MIN_SCALE_X_ + 1);


//---------------------------------------------------------------------
//  Standard look of "WOBtot" display:
//
//     |----------|
//     |  WOBtot  |				standard WOBtot container and label
//     |----------|
//          \/					triangle pointer
//---------------------------------------------------------------------
static const Int32  TOT_WOB_TRIANGLE_SIZE_ = 6;	// width and height...
static const Int32  TOT_WOB_TRIANGLE_HALF_WIDTH_ =
											((TOT_WOB_TRIANGLE_SIZE_ + 1) / 2);
static const Int32  TOT_WOB_TRIANGLE_Y_    =
						   (SCALE_CONTAINER_Y_ - (TOT_WOB_TRIANGLE_SIZE_ + 1));

static const Int32  TOT_WOB_LABEL_CONTAINER_HEIGHT_ =
												  (WOB_LABEL_TEXT_HEIGHT_ + 1);
static const Int32  TOT_WOB_LABEL_CONTAINER_WIDTH_      = 60;
static const Int32  TOT_WOB_LABEL_CONTAINER_HALF_WIDTH_ =
								   ((TOT_WOB_LABEL_CONTAINER_WIDTH_ + 1) / 2);
static const Int32  STD_TOT_WOB_LABEL_CONTAINER_X_OFFSET_ =
									   -(TOT_WOB_LABEL_CONTAINER_HALF_WIDTH_);
static const Int32  STD_TOT_WOB_LABEL_CONTAINER_Y_      =
									 (TOT_WOB_TRIANGLE_Y_ -
									  TOT_WOB_LABEL_CONTAINER_HEIGHT_);


//---------------------------------------------------------------------
//  Extended look of "WOBtot" display:
//
//              |----------|
//              |  WOBtot  |		WOBtot container and label
//              |----------|
//                    |				vert line #1
//           ----------				horiz line
//           |						vert line #1
//          \/						triangle pointer
//---------------------------------------------------------------------
static const Int32  EXT_TOT_WOB_VLINE_HEIGHT_         =  2;
static const Int32  EXT_TOT_WOB_HLINE_LENGTH_         = 50;
static const Int32  EXT_TOT_WOB_LABEL_CONTAINER_X_OFFSET_ =
									   (STD_TOT_WOB_LABEL_CONTAINER_X_OFFSET_ +
										EXT_TOT_WOB_HLINE_LENGTH_);
static const Int32  EXT_TOT_WOB_LABEL_CONTAINER_Y_    =
										  (TOT_WOB_TRIANGLE_Y_ -
										   (2 * EXT_TOT_WOB_VLINE_HEIGHT_) -
										   TOT_WOB_LABEL_CONTAINER_HEIGHT_ - 1);
static const Int32  EXT_TOT_WOB_VLINE1_Y2_  = (TOT_WOB_TRIANGLE_Y_ - 1);
static const Int32  EXT_TOT_WOB_VLINE1_Y1_  = (EXT_TOT_WOB_VLINE1_Y2_ -
											   EXT_TOT_WOB_VLINE_HEIGHT_);
static const Int32  EXT_TOT_WOB_VLINE2_Y1_  = (EXT_TOT_WOB_VLINE1_Y1_ -
											   EXT_TOT_WOB_VLINE_HEIGHT_);


//---------------------------------------------------------------------
//  "WOBpt" display:
//
//		E		 R				E and R labels
//     |----------|
//     |****++++++|				WOBe/WOBr ratio box
//     |   WOBpt  |				WOBpt box and label
//     |----------|
//          \/					triangle pointer
//---------------------------------------------------------------------
static const Int32  PT_WOB_TRIANGLE_SIZE_ = 11;		// width and height...
static const Int32  PT_WOB_TRIANGLE_HALF_WIDTH_ =
										  ((PT_WOB_TRIANGLE_SIZE_ + 1) / 2);
static const Int32  PT_WOB_TRIANGLE_Y_    =
						 (SCALE_CONTAINER_Y_ - (PT_WOB_TRIANGLE_SIZE_ + 1));

static const Int32  PT_WOB_ER_TEXT_HEIGHT_    = 10;		// E/R text (p=8)...
static const Int32  PT_WOB_ER_BOX_HEIGHT_     = 10;		// WOBe/WOBr box...
static const Int32  PT_WOB_INFO_CONTAINER_HEIGHT_ =
											 ((PT_WOB_ER_BOX_HEIGHT_ + 1) +
											  (WOB_LABEL_TEXT_HEIGHT_ + 1));
static const Int32  PT_WOB_CONTAINER_HEIGHT_  =
											 ((PT_WOB_ER_TEXT_HEIGHT_ + 1) +
											  PT_WOB_INFO_CONTAINER_HEIGHT_);
static const Int32  PT_WOB_CONTAINER_WIDTH_   = 80;
static const Int32  PT_WOB_ER_BOX_WIDTH_      = (PT_WOB_CONTAINER_WIDTH_ - 2);
static const Int32  PT_WOB_CONTAINER_HALF_WIDTH_ =
										  ((PT_WOB_CONTAINER_WIDTH_ + 1) / 2);
static const Int32  PT_WOB_CONTAINER_Y_       =
							  (PT_WOB_TRIANGLE_Y_ - PT_WOB_CONTAINER_HEIGHT_);

// $[PA01012] low (0-0.3), normal (0.3-0.7), above normal (0.7-1.0) and high (1.0-2.0)
static const Real32  MIN_RANGE_VALUE_           = 0.0f;
static const Real32  LOW_RANGE_CUTOFF_VALUE_    = 0.3f;
static const Real32  NORMAL_RANGE_CUTOFF_VALUE_ = 0.7f;
static const Real32  HIGH_RANGE_CUTOFF_VALUE_   = 1.0f;
static const Real32  MAX_RANGE_VALUE_           = 2.0f;

static const Real32  RESOLUTION_ = 0.1f;

static const Int32  SCALE_VALUE_Y_       =
							(SCALE_CONTAINER_Y_ + SCALE_CONTAINER_HEIGHT_ + 2);
static const Int32  SCALE_VALUE_OFFSET_  = 9;
static const wchar_t*  SCALE_FORMAT_STRING_ = L"{p=10,y=11:%.1f}";  
static const wchar_t*  SCALE_UNIT_STRING_   = L"{p=8,y=11:J/L}";    


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WobGraphic()  [Constructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  SDCR #6133  Based on input from the Field Evaluation in Canada
//              the area on the WOB graphic which is colored red
//              should be changed to another color as red has a negative
//              meaning.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WobGraphic::WobGraphic(void) :
	totalWobLabel_(MiscStrs::WOB_GRAPHIC_TOTAL_LABEL,
				   MiscStrs::TOUCHABLE_TOTAL_WORK_OF_BREATHING_MSG),
	patientWobLabel_(MiscStrs::WOB_GRAPHIC_PATIENT_LABEL,
				     MiscStrs::TOUCHABLE_PATIENT_WORK_OF_BREATHING_MSG),
	elasticWobLabel_(MiscStrs::WOB_GRAPHIC_ELASTANCE_LABEL,
				     MiscStrs::TOUCHABLE_ELASTANCE_WORK_OF_BREATHING_MSG),
	resistiveWobLabel_(MiscStrs::WOB_GRAPHIC_RESISTANCE_LABEL,
				     MiscStrs::TOUCHABLE_RESISTANCE_WORK_OF_BREATHING_MSG),

	totalWobPointer_(0, 0, ::TOT_WOB_TRIANGLE_SIZE_, 0,
				     (::TOT_WOB_TRIANGLE_SIZE_ / 2), ::TOT_WOB_TRIANGLE_SIZE_),
	patientWobPointer_(0, 0, ::PT_WOB_TRIANGLE_SIZE_, 0,
					   (::PT_WOB_TRIANGLE_SIZE_ / 2), ::PT_WOB_TRIANGLE_SIZE_)
{
	CALL_TRACE("WobGraphic::WobGraphic(void)");

	graphicContainer_.setX(0);
	graphicContainer_.setWidth(::CONTAINER_WIDTH_);
	graphicContainer_.setHeight(::CONTAINER_HEIGHT_);
	addDrawable(&graphicContainer_);

	graphicTitleText_.setX(0);
	graphicTitleText_.setY(0);
	graphicTitleText_.setText(MiscStrs::WOB_GRAPHIC_TITLE);
	graphicTitleText_.setColor(Colors::WHITE);
	graphicContainer_.addDrawable(&graphicTitleText_);


	//-----------------------------------------------------------------
	//  Standard look of "WOBtot" display:
	//
	//     |----------|
	//     |  WOBtot  |				WOBtot container and label
	//     |----------|
	//          \/					triangle pointer
	//-----------------------------------------------------------------
	totalWobLabelContainer_.setHeight(::TOT_WOB_LABEL_CONTAINER_HEIGHT_);
	totalWobLabelContainer_.setWidth(::TOT_WOB_LABEL_CONTAINER_WIDTH_);
	totalWobLabelContainer_.setColor(Colors::LIGHT_BLUE);
	totalWobLabelContainer_.setFillColor(Colors::LIGHT_BLUE);

	totalWobLabel_.setY(0);
	totalWobLabel_.setColor(Colors::BLACK);
	totalWobLabelContainer_.addDrawable(&totalWobLabel_);
	totalWobLabel_.positionInContainer(::GRAVITY_CENTER);

	totalWobPointer_.setY(::TOT_WOB_TRIANGLE_Y_);
	totalWobPointer_.setColor(Colors::LIGHT_BLUE);
	totalWobPointer_.setFillColor(Colors::LIGHT_BLUE);

	//-----------------------------------------------------------------
	//  Extended look of "WOBtot" display:
	//
	//              |----------|
	//              |  WOBtot  |		WOBtot container and label
	//              |----------|
	//                    |				vert line #1
	//           ----------				horiz line
	//           |						vert line #1
	//          \/						triangle pointer
	//-----------------------------------------------------------------

	totalWobVertLine1_.setColor(Colors::LIGHT_BLUE);
	totalWobHorizLine_.setColor(Colors::LIGHT_BLUE);
	totalWobVertLine2_.setColor(Colors::LIGHT_BLUE);


	//-----------------------------------------------------------------
	//  "WOBpt" display:
	//
	//		E		 R				E and R labels
	//     |----------|
	//     |****++++++|				WOBe/WOBr ratio box
	//     |   WOBpt  |				WOBpt box and label
	//     |----------|
	//          \/					triangle pointer
	//-----------------------------------------------------------------
	patientWobContainer_.setY(::PT_WOB_CONTAINER_Y_);
	patientWobContainer_.setHeight(::PT_WOB_CONTAINER_HEIGHT_);
	patientWobContainer_.setWidth(::PT_WOB_CONTAINER_WIDTH_);
	patientWobContainer_.setColor(Colors::BLACK);
	patientWobContainer_.setFillColor(Colors::BLACK);

	elasticWobLabel_.setY(1);
	elasticWobLabel_.setColor(Colors::WHITE);
	patientWobContainer_.addDrawable(&elasticWobLabel_);
	elasticWobLabel_.positionInContainer(::GRAVITY_LEFT);

	resistiveWobLabel_.setY(1);
	resistiveWobLabel_.setColor(Colors::WHITE);
	patientWobContainer_.addDrawable(&resistiveWobLabel_);
	resistiveWobLabel_.positionInContainer(::GRAVITY_RIGHT);

	patientWobInfoContainer_.setX(0);
	patientWobInfoContainer_.setY(::PT_WOB_ER_TEXT_HEIGHT_ + 1);
	patientWobInfoContainer_.setHeight(::PT_WOB_INFO_CONTAINER_HEIGHT_);
	patientWobInfoContainer_.setWidth(::PT_WOB_CONTAINER_WIDTH_);
	patientWobInfoContainer_.setColor(Colors::WHITE);
	patientWobInfoContainer_.setFillColor(Colors::WHITE);
	patientWobContainer_.addDrawable(&patientWobInfoContainer_);

	patientWobElastanceBar_.setX(1);
	patientWobElastanceBar_.setY(1);
	patientWobElastanceBar_.setHeight(::PT_WOB_ER_BOX_HEIGHT_);
	
        // Changed the PatientWobElastanceBar color from COPPER to GOLDENROD - SDCR # 6133
	
	patientWobElastanceBar_.setColor(Colors::GOLDENROD);
	patientWobElastanceBar_.setFillColor(Colors::GOLDENROD);
	patientWobInfoContainer_.addDrawable(&patientWobElastanceBar_);

	patientWobResistanceBar_.setX(
		(patientWobElastanceBar_.getX() + patientWobElastanceBar_.getWidth())
							     );
	patientWobResistanceBar_.setY(1);
	patientWobResistanceBar_.setHeight(::PT_WOB_ER_BOX_HEIGHT_);
	patientWobResistanceBar_.setColor(Colors::MEDIUM_BLUE);
	patientWobResistanceBar_.setFillColor(Colors::MEDIUM_BLUE);
	patientWobInfoContainer_.addDrawable(&patientWobResistanceBar_);

	patientWobLabel_.setY(
		  (patientWobElastanceBar_.getY() + patientWobElastanceBar_.getHeight())
						 );
	patientWobLabel_.setColor(Colors::BLACK);
	patientWobInfoContainer_.addDrawable(&patientWobLabel_);
	patientWobLabel_.positionInContainer(::GRAVITY_CENTER);

	patientWobPointer_.setY(::PT_WOB_TRIANGLE_Y_);
	patientWobPointer_.setColor(Colors::WHITE);
	patientWobPointer_.setFillColor(Colors::WHITE);


	//-----------------------------------------------------------------
	// setup container for colored scale regions...
	//-----------------------------------------------------------------

	scaleContainer_.setX(::MIN_SCALE_X_);
	scaleContainer_.setY(::SCALE_CONTAINER_Y_);
	scaleContainer_.setHeight(::SCALE_CONTAINER_HEIGHT_);
	scaleContainer_.setWidth(::SCALE_CONTAINER_WIDTH_);
	scaleContainer_.setColor(Colors::BLACK);
	graphicContainer_.addDrawable(&scaleContainer_);

	const Int32  MIN_RANGE_CUTOFF_X_ =
								valueToPoint_(::LOW_RANGE_CUTOFF_VALUE_, FALSE);

	minRange_.setX(0);
	minRange_.setY(0);
	minRange_.setHeight(scaleContainer_.getHeight());
	minRange_.setWidth(MIN_RANGE_CUTOFF_X_);
	minRange_.setColor(Colors::YELLOW);
	minRange_.setFillColor(Colors::YELLOW);
	scaleContainer_.addDrawable(&minRange_);

	const Int32  NORMAL_RANGE_CUTOFF_X_ =
							valueToPoint_(::NORMAL_RANGE_CUTOFF_VALUE_, FALSE);

	normalRange_.setX(MIN_RANGE_CUTOFF_X_);
	normalRange_.setY(0);
	normalRange_.setHeight(scaleContainer_.getHeight());
	normalRange_.setWidth(NORMAL_RANGE_CUTOFF_X_ - MIN_RANGE_CUTOFF_X_);
	normalRange_.setColor(Colors::GREEN);
	normalRange_.setFillColor(Colors::GREEN);
	scaleContainer_.addDrawable(&normalRange_);

	const Int32  HIGH_RANGE_CUTOFF_X_ =
							valueToPoint_(::HIGH_RANGE_CUTOFF_VALUE_, FALSE);

	highRange_.setX(NORMAL_RANGE_CUTOFF_X_);
	highRange_.setY(0);
	highRange_.setHeight(scaleContainer_.getHeight());
	highRange_.setWidth(HIGH_RANGE_CUTOFF_X_ - NORMAL_RANGE_CUTOFF_X_);
	highRange_.setColor(Colors::YELLOW);
	highRange_.setFillColor(Colors::YELLOW);
	scaleContainer_.addDrawable(&highRange_);

	maxRange_.setX(HIGH_RANGE_CUTOFF_X_);
	maxRange_.setY(0);
	maxRange_.setHeight(scaleContainer_.getHeight());
	maxRange_.setWidth(::MAX_SCALE_X_ - HIGH_RANGE_CUTOFF_X_);
	
        // Changed the maxRange color from RED to WHITE - SDCR # 6133
	
	maxRange_.setColor(Colors::WHITE);
	maxRange_.setFillColor(Colors::WHITE);
	scaleContainer_.addDrawable(&maxRange_);


	//-----------------------------------------------------------------
	// setup scale value labels, and unit...
	//-----------------------------------------------------------------

	wchar_t  tmpString[TextField::MAX_STRING_LENGTH];

	minScaleValue_.setX(::MIN_SCALE_X_ - ::SCALE_VALUE_OFFSET_);
	minScaleValue_.setY(::SCALE_VALUE_Y_);
	swprintf(tmpString, ::SCALE_FORMAT_STRING_, ::MIN_RANGE_VALUE_);
	minScaleValue_.setText(tmpString);
	minScaleValue_.setColor(Colors::WHITE);
	graphicContainer_.addDrawable(&minScaleValue_);

	const Int32  LOW_CUTOFF_X = valueToPoint_(::LOW_RANGE_CUTOFF_VALUE_, TRUE);

	lowCutoffValue_.setX(LOW_CUTOFF_X - ::SCALE_VALUE_OFFSET_);
	lowCutoffValue_.setY(::SCALE_VALUE_Y_);
	swprintf(tmpString, ::SCALE_FORMAT_STRING_, ::LOW_RANGE_CUTOFF_VALUE_);
	lowCutoffValue_.setText(tmpString);
	lowCutoffValue_.setColor(Colors::WHITE);
	graphicContainer_.addDrawable(&lowCutoffValue_);

	const Int32  NORMAL_CUTOFF_X = valueToPoint_(::NORMAL_RANGE_CUTOFF_VALUE_,
												 TRUE);

	normalCutoffValue_.setX(NORMAL_CUTOFF_X - ::SCALE_VALUE_OFFSET_);
	normalCutoffValue_.setY(::SCALE_VALUE_Y_);
	swprintf(tmpString, ::SCALE_FORMAT_STRING_, ::NORMAL_RANGE_CUTOFF_VALUE_);
	normalCutoffValue_.setText(tmpString);
	normalCutoffValue_.setColor(Colors::WHITE);
	graphicContainer_.addDrawable(&normalCutoffValue_);

	const Int32  HIGH_CUTOFF_X = valueToPoint_(::HIGH_RANGE_CUTOFF_VALUE_,
											   TRUE);

	highCutoffValue_.setX(HIGH_CUTOFF_X - ::SCALE_VALUE_OFFSET_);
	highCutoffValue_.setY(::SCALE_VALUE_Y_);
	swprintf(tmpString, ::SCALE_FORMAT_STRING_, ::HIGH_RANGE_CUTOFF_VALUE_);
	highCutoffValue_.setText(tmpString);
	highCutoffValue_.setColor(Colors::WHITE);
	graphicContainer_.addDrawable(&highCutoffValue_);

	maxScaleValue_.setX(::MAX_SCALE_X_ - ::SCALE_VALUE_OFFSET_);
	maxScaleValue_.setY(::SCALE_VALUE_Y_);
	swprintf(tmpString, ::SCALE_FORMAT_STRING_, ::MAX_RANGE_VALUE_);
	maxScaleValue_.setText(tmpString);
	maxScaleValue_.setColor(Colors::WHITE);
	graphicContainer_.addDrawable(&maxScaleValue_);

	scaleUnit_.setX(::MAX_SCALE_X_ + 15);
	scaleUnit_.setY(::SCALE_VALUE_Y_);
	scaleUnit_.setText(::SCALE_UNIT_STRING_);
	scaleUnit_.setColor(Colors::WHITE);
	graphicContainer_.addDrawable(&scaleUnit_);


	// Of the four WOB data values displayed by this graphic, only WOBtot is
	// monitored; when the callback for WOBtot is activated, it is expected
	// that the other three WOB values are already updated...
	PatientDataRegistrar::RegisterTarget(PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM,
										 this);

	// register for BD event callbacks...
    BdEventRegistrar::RegisterTarget(EventData::OCCLUSION, this);
	BdEventRegistrar::RegisterTarget(EventData::PATIENT_CONNECT, this);
	BdEventRegistrar::RegisterTarget(EventData::SVO, this);
	BdEventRegistrar::RegisterTarget(EventData::VENT_INOP, this);
	BdEventRegistrar::RegisterTarget(EventData::APNEA_VENT, this);
	BdEventRegistrar::RegisterTarget(EventData::SAFETY_VENT, this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~WobGraphic  [Destructor]
//
//@ Interface-Description
// Destroys the alarm slider.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WobGraphic::~WobGraphic(void)
{
	CALL_TRACE("WobGraphic::~WobGraphic(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Virtual method inherited from Drawable.  Must be called every time the
// slider is about to be displayed.  Syncs the display of the slider with the
// current data in the Settings-Validation and Patient Data subsystems.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call our updateButtonPosition_() and updateValueDisplay_()
// methods to update the positions of the buttons and patient datum
// pointer.  Also, call the activate() method on the setting buttons
// in order to sync their displays with the current settings.
// $[NE01000] The alarm bars shall support a dynamic scaling ... 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WobGraphic::activate(void)
{
	CALL_TRACE("WobGraphic::activate(void)");

	const Int  LEFT_OVER_HEIGHT =
							(getHeight() - graphicContainer_.getHeight()) / 2;

	graphicContainer_.setY(LEFT_OVER_HEIGHT);
    
    // PAV3_030415_LJS - Remove WOBpt and WOBtot markers 
    // when not in PAV "CLOSED_LOOP" state.
    {
        const WaveformsSubScreen* screen_ptr = UpperSubScreenArea::GetWaveformsSubScreen();
    
        if (screen_ptr->isInPavClosedLoop())
        {}
        else // Not in CLOSED_LOOP.
        {
    		graphicContainer_.removeDrawable(&patientWobContainer_);
    		graphicContainer_.removeDrawable(&patientWobPointer_);
	    	graphicContainer_.removeDrawable(&totalWobLabelContainer_);
		    graphicContainer_.removeDrawable(&totalWobVertLine1_);
		    graphicContainer_.removeDrawable(&totalWobHorizLine_);
    		graphicContainer_.removeDrawable(&totalWobVertLine2_);
		    graphicContainer_.removeDrawable(&totalWobPointer_);
        }
    }
    
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Prepare for the deactivation of this slider.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Detach both the lower and upper setting ids from the Context Subject
//  then deactivate the lower the upper buttons on the slider.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WobGraphic::deactivate(void)
{
	CALL_TRACE("deactivate()");
    
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
// Called normally via the BdEventRegistrar when a BD event in which we are
// interested occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition                                              
//  none                                                      
//---------------------------------------------------------------------
//@ PostCondition                                             
//  none                                                      
//@ End-Method                                                
//=====================================================================
                                                              
void                                                          
WobGraphic::bdEventHappened(EventData::EventId, EventData::EventStatus,
						    EventData::EventPrompt)
{
	if (BdGuiEvent::GetEventStatus(EventData::APNEA_VENT) ==
														EventData::ACTIVE  ||
		BdGuiEvent::GetEventStatus(EventData::SAFETY_VENT) ==
														EventData::ACTIVE  ||
		BdGuiEvent::GetEventStatus(EventData::VENT_INOP) ==
														EventData::ACTIVE  ||
		BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) !=
														EventData::ACTIVE  ||
		BdGuiEvent::GetEventStatus(EventData::OCCLUSION) ==
														EventData::ACTIVE  ||
		BdGuiEvent::GetEventStatus(EventData::SVO) == EventData::ACTIVE)
	{	// $[TI1]
		// hide data display until next WOB update occurs...        
		totalWobLabelContainer_.setShow(FALSE);
		totalWobPointer_.setShow(FALSE);
		totalWobVertLine1_.setShow(FALSE);
		totalWobHorizLine_.setShow(FALSE);
		totalWobVertLine2_.setShow(FALSE);
		patientWobContainer_.setShow(FALSE);
		patientWobPointer_.setShow(FALSE);

		// next update will add drawable back...
		graphicContainer_.removeDrawable(&totalWobLabelContainer_);
		graphicContainer_.removeDrawable(&totalWobPointer_);
		graphicContainer_.removeDrawable(&totalWobVertLine1_);
		graphicContainer_.removeDrawable(&totalWobHorizLine_);
		graphicContainer_.removeDrawable(&totalWobVertLine2_);
		graphicContainer_.removeDrawable(&patientWobContainer_);
		graphicContainer_.removeDrawable(&patientWobPointer_);
	}
	else
	{	// $[TI2]
		// show data display...
		totalWobLabelContainer_.setShow(TRUE);
		totalWobPointer_.setShow(TRUE);
		totalWobVertLine1_.setShow(TRUE);
		totalWobHorizLine_.setShow(TRUE);
		totalWobVertLine2_.setShow(TRUE);
		patientWobContainer_.setShow(TRUE);
		patientWobPointer_.setShow(TRUE);
	}
}                                                             


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened
//
//@ Interface-Description
// Called when the patient datum value has changed.  The patient datum
// pointer's value and position are changed accordingly.  It is passed
// 'patientDataId', the patient data id for the value.
//---------------------------------------------------------------------
//@ Implementation-Description
// First, turn off hidePointer_ flag.  If the WobGraphic is not
// currently visible then we simply return.   Then call updateValueDisplay_().
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WobGraphic::patientDataChangeHappened(PatientDataId::PatientItemId)
{
	CALL_TRACE("patientDataChangeHappened(patientDataId)");
	
    BreathDatumHandle  totalWobHandle(
								PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM
									 );

	if (!totalWobHandle.getBoundedValue().timedOut)
	{	// $[TI1]
	       totalWobLabelContainer_.setFillColor(Colors::LIGHT_BLUE);
	       
		 
		BreathDatumHandle  patientWobHandle(
								PatientDataId::PATIENT_WORK_OF_BREATHING_ITEM
										   );
		BreathDatumHandle  elastanceWobHandle(
								PatientDataId::ELASTANCE_WORK_OF_BREATHING_ITEM
											 );

		const Real32  TOTAL_WOB_VALUE =
									totalWobHandle.getBoundedValue().data.value;
		const Real32  PATIENT_WOB_VALUE =
								  patientWobHandle.getBoundedValue().data.value;
		const Real32  ELASTANCE_WOB_VALUE =
							    elastanceWobHandle.getBoundedValue().data.value;

		//-----------------------------------------------------------------
		//  Setup location of Patient WOB info and pointer...
		//-----------------------------------------------------------------

		// x position of pointer is based on its parent container...
		const Int32  PATIENT_WOB_X         =
										valueToPoint_(PATIENT_WOB_VALUE, TRUE);
		const Int32  PATIENT_WOB_POINTER_X =
								(PATIENT_WOB_X - ::PT_WOB_TRIANGLE_HALF_WIDTH_);
		const Int32  PATIENT_WOB_INFO_X    =
							   (PATIENT_WOB_X - ::PT_WOB_CONTAINER_HALF_WIDTH_);

		patientWobPointer_.setX(PATIENT_WOB_POINTER_X);
		graphicContainer_.addDrawable(&patientWobPointer_);

		patientWobContainer_.setX(PATIENT_WOB_INFO_X);
		graphicContainer_.addDrawable(&patientWobContainer_);

		//-----------------------------------------------------------------
		//  Setup widths of WOBe and WOBr bars...
		//-----------------------------------------------------------------

		static const Int32  MINIMUM_BAR_WIDTH_ = 5; // pixels...

		if (ELASTANCE_WOB_VALUE < TOTAL_WOB_VALUE)
		{	// $[TI1.1]
			const Int32  ELAST_WOB_WIDTH =
							Int32(Real32(::PT_WOB_ER_BOX_WIDTH_) *
									(ELASTANCE_WOB_VALUE / TOTAL_WOB_VALUE));

			if (ELAST_WOB_WIDTH > MINIMUM_BAR_WIDTH_)
			{	// $[TI1.1.1]
				patientWobElastanceBar_.setWidth(ELAST_WOB_WIDTH);
			}
			else
			{	// $[TI1.1.2]
				patientWobElastanceBar_.setWidth(MINIMUM_BAR_WIDTH_);
			}
		}
		else
		{	// $[TI1.2]
			patientWobElastanceBar_.setWidth(::PT_WOB_ER_BOX_WIDTH_ -
														 MINIMUM_BAR_WIDTH_);
		}

		patientWobResistanceBar_.setX(
			patientWobElastanceBar_.getX() + patientWobElastanceBar_.getWidth()
									 );
		patientWobResistanceBar_.setWidth(
					::PT_WOB_ER_BOX_WIDTH_ - patientWobElastanceBar_.getWidth()
										 );

		//-----------------------------------------------------------------
		//  Setup location and configuration of Total WOB label and pointer...
		//-----------------------------------------------------------------

		static const Int32  MINIMUM_SEPERATION_ = 
					(::PT_WOB_CONTAINER_HALF_WIDTH_ +
					 ::TOT_WOB_LABEL_CONTAINER_HALF_WIDTH_ + 1); // pixels...

                 if (TOTAL_WOB_VALUE < ::MAX_RANGE_VALUE_)
                  {
                 	//No Blinking
                 	 totalWobLabelContainer_.setFillColor(Colors::LIGHT_BLUE);
                  }
                 else
                  {      // Start Blinking
                  	 totalWobLabelContainer_.setFillColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
                  }
                 	
		// x position of pointer is based on its parent container...
		const Int32  TOTAL_WOB_X = valueToPoint_(TOTAL_WOB_VALUE, TRUE);

		if (TOTAL_WOB_X > PATIENT_WOB_X)
		{	// $[TI1.3]
			const Int32  TOTAL_WOB_POINTER_X =
								(TOTAL_WOB_X - ::TOT_WOB_TRIANGLE_HALF_WIDTH_);

			totalWobPointer_.setX(TOTAL_WOB_POINTER_X);

			if ((TOTAL_WOB_X - PATIENT_WOB_X) >= MINIMUM_SEPERATION_)
			{	// $[TI1.3.1]
				totalWobLabelContainer_.setX(
										TOTAL_WOB_X + 
										::STD_TOT_WOB_LABEL_CONTAINER_X_OFFSET_
											);
				totalWobLabelContainer_.setY(::STD_TOT_WOB_LABEL_CONTAINER_Y_);

				graphicContainer_.removeDrawable(&totalWobVertLine1_);
				graphicContainer_.removeDrawable(&totalWobHorizLine_);
				graphicContainer_.removeDrawable(&totalWobVertLine2_);

				graphicContainer_.addDrawable(&totalWobPointer_);
				graphicContainer_.addDrawable(&totalWobLabelContainer_);
			}
			else
			{	// $[TI1.3.2]
				totalWobLabelContainer_.setX(
										TOTAL_WOB_X + 
										::EXT_TOT_WOB_LABEL_CONTAINER_X_OFFSET_
											);
				totalWobLabelContainer_.setY(::EXT_TOT_WOB_LABEL_CONTAINER_Y_);

				if (TOTAL_WOB_VALUE < ::MAX_RANGE_VALUE_)
				{	// $[TI1.3.2.1] -- extended WOBtot display can fit...
				
				       				        
					const Int32  LINE_X2 =
									(TOTAL_WOB_X + ::EXT_TOT_WOB_HLINE_LENGTH_);

					totalWobVertLine1_.setVertices(TOTAL_WOB_X,
												   ::EXT_TOT_WOB_VLINE1_Y1_,
												   TOTAL_WOB_X,
												   ::EXT_TOT_WOB_VLINE1_Y2_);
					totalWobHorizLine_.setVertices(TOTAL_WOB_X,
												   ::EXT_TOT_WOB_VLINE1_Y1_,
												   LINE_X2,
												   ::EXT_TOT_WOB_VLINE1_Y1_);
					totalWobVertLine2_.setVertices(LINE_X2,
												   ::EXT_TOT_WOB_VLINE2_Y1_,
												   LINE_X2,
												   ::EXT_TOT_WOB_VLINE1_Y1_);

					graphicContainer_.addDrawable(&totalWobPointer_);
					graphicContainer_.addDrawable(&totalWobVertLine1_);
					graphicContainer_.addDrawable(&totalWobHorizLine_);
					graphicContainer_.addDrawable(&totalWobVertLine2_);
					graphicContainer_.addDrawable(&totalWobLabelContainer_);
				}
				else
				{	// $[TI1.3.2.2] -- extended WOBtot display can NOT fit...
				
					graphicContainer_.removeDrawable(&totalWobPointer_);
					graphicContainer_.removeDrawable(&totalWobVertLine1_);
					graphicContainer_.removeDrawable(&totalWobHorizLine_);
					graphicContainer_.removeDrawable(&totalWobVertLine2_);
					graphicContainer_.removeDrawable(&totalWobLabelContainer_);
					
					
				}
			}
		}
		else
		{	// $[TI1.4]
			if (PATIENT_WOB_VALUE < ::MAX_RANGE_VALUE_)
			{	// $[TI1.4.1]
				// x position of pointer is based on its parent container...
				const Int32  TOTAL_WOB_X2         =
					valueToPoint_((PATIENT_WOB_VALUE + ::RESOLUTION_), TRUE);
				const Int32  TOTAL_WOB_POINTER_X2 = (TOTAL_WOB_X2 -
												::TOT_WOB_TRIANGLE_HALF_WIDTH_);

				totalWobLabelContainer_.setX(
										TOTAL_WOB_X2 + 
										::EXT_TOT_WOB_LABEL_CONTAINER_X_OFFSET_
											);
				totalWobLabelContainer_.setY(::EXT_TOT_WOB_LABEL_CONTAINER_Y_);

				const Int32  LINE_X2 =
								(TOTAL_WOB_X2 + ::EXT_TOT_WOB_HLINE_LENGTH_);

				totalWobVertLine1_.setVertices(TOTAL_WOB_X2,
											   ::EXT_TOT_WOB_VLINE1_Y1_,
											   TOTAL_WOB_X2,
											   ::EXT_TOT_WOB_VLINE1_Y2_);
				totalWobHorizLine_.setVertices(TOTAL_WOB_X2,
											   ::EXT_TOT_WOB_VLINE1_Y1_,
											   LINE_X2,
											   ::EXT_TOT_WOB_VLINE1_Y1_);
				totalWobVertLine2_.setVertices(LINE_X2,
											   ::EXT_TOT_WOB_VLINE2_Y1_,
											   LINE_X2,
											   ::EXT_TOT_WOB_VLINE1_Y1_);

				totalWobPointer_.setX(TOTAL_WOB_POINTER_X2);

				graphicContainer_.addDrawable(&totalWobLabelContainer_);
				graphicContainer_.addDrawable(&totalWobVertLine1_);
				graphicContainer_.addDrawable(&totalWobHorizLine_);
				graphicContainer_.addDrawable(&totalWobVertLine2_);
				graphicContainer_.addDrawable(&totalWobPointer_);
			}
			else
			{	// $[TI1.4.2]
				// there's not enough room to show a WOBtot pointer...
				graphicContainer_.removeDrawable(&totalWobLabelContainer_);
				graphicContainer_.removeDrawable(&totalWobVertLine1_);
				graphicContainer_.removeDrawable(&totalWobHorizLine_);
				graphicContainer_.removeDrawable(&totalWobVertLine2_);
				graphicContainer_.removeDrawable(&totalWobPointer_);
			}
		}        
	}
	else
	{	// $[TI2]
		// data has timed-out, remove until next update...
		graphicContainer_.removeDrawable(&patientWobContainer_);
		graphicContainer_.removeDrawable(&patientWobPointer_);
		graphicContainer_.removeDrawable(&totalWobLabelContainer_);
		graphicContainer_.removeDrawable(&totalWobVertLine1_);
		graphicContainer_.removeDrawable(&totalWobHorizLine_);
		graphicContainer_.removeDrawable(&totalWobVertLine2_);
		graphicContainer_.removeDrawable(&totalWobPointer_);        
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WobGraphic::SoftFault(const SoftFaultID  softFaultID,
					   const Uint32       lineNumber,
					   const char*        pFileName,
					   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMSLIDER,
									lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueToPoint_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int32
WobGraphic::valueToPoint_(const Real32  wobValue,
						  const Boolean isRelativeToGraphic) const
{
	CALL_TRACE("valueToPoint_(wobValue)");

	static const Int32  MAX_VALUE_INCREMENTS =
	  Int32(((::MAX_RANGE_VALUE_ - ::MIN_RANGE_VALUE_) / ::RESOLUTION_) + 0.5f);
	static const Int32  NUM_PIXELS_PER_INCREMENT =
		  ((::MAX_SCALE_X_ - ::MIN_SCALE_X_ + (MAX_VALUE_INCREMENTS / 2)) /
						  MAX_VALUE_INCREMENTS);

	// if the resulting 'x' value is to be relative to the entire graphic
	// container, rather than an internal container (e.g., scale container),
	// then initialize to an origin relative to the graphic container, otherwise
	// start at 1...
	Int32  xValue = (isRelativeToGraphic) ? (::MIN_SCALE_X_ + 1)	// $[TI1]
										  : 1;						// $[TI2]

	if (wobValue >= ::MAX_RANGE_VALUE_)
	{	// $[TI3]
		xValue += ::SCALE_CONTAINER_WIDTH_;
	}
	else if (wobValue > ::MIN_RANGE_VALUE_)
	{	// $[TI4]
		const Int32  NUM_INCREMENTS = Int32((wobValue / ::RESOLUTION_) + 0.5f);

		xValue += (NUM_INCREMENTS * NUM_PIXELS_PER_INCREMENT);
	}   // leave alone...

	return(xValue);
}
