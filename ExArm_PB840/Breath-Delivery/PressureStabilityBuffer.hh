#ifndef PressureStabilityBuffer_HH
#define PressureStabilityBuffer_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: PressureStabilityBuffer - class to check pressure stability 
//                                  during insp/exp pause maneuver
//---------------------------------------------------------------------
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureStabilityBuffer.hhv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial implementation. Moved pressure stability functions out
//      of Maneuver class into this new class so RM maneuvers are
//		burdened with this unrelated functionality.
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "PauseTypes.hh"
#include "AveragedStabilityBuffer.hh"

//@ Usage-Classes
class Trigger;
class BreathPhase;
//@ End-Usage

class PressureStabilityBuffer: public AveragedStabilityBuffer
{
  public:
	PressureStabilityBuffer(Real32 *pBuffer, Uint32 bufferSize);
	virtual ~PressureStabilityBuffer(void);

	PauseTypes::PpiState calculatePressureStability(void);
	void reset();
  
	static void SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL, 
			      const char*       pPredicate = NULL);

  protected:

  private:
	PressureStabilityBuffer(void);	    // Declared but not implemented
    PressureStabilityBuffer(const PressureStabilityBuffer&);    // Declared but not implemented
    void operator=(const PressureStabilityBuffer&); // Declared but not implemented

	//@ Data-Member: sCurrent_
	// Pressure rate = dp/dt, the slope of the pressure.
	Real32 sCurrent_;

	//@ Data-Member: rCurrent_
	// An intermediate pressure rate value, which is bounded
	// between 0 and 1000.
	Real32 rCurrent_;

	//@ Data-Member: rCurrent_1_
	// Most recent pressure rate.
	Real32 rCurrent_1_;

	//@ Data-Member: rCurrent_2_
	// Second to last recent pressure rate.
	Real32 rCurrent_2_;

	//@ Data-Member: pCurrent_1_
	// Current airway pressure
	Real32 pCurrent_1_;

};


#endif // PressureStabilityBuffer_HH 
