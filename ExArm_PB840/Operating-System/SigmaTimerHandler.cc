#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: SigmaTimerHandler.cc - 5ms Timer Interrupt Handler
//
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the handler for the system timer interrupting
//  the processor at 5ms intervals.  This handler performs three
//  functions:
//
//   1. Regulates the breath delivery cycle by posting a message
//      to the Breath Delivery queue every 5ms.
//   2. Notifies VRTX about a clock tick every 10ms.
//   3. Messages the Keyboard polling function every 10ms.
//
// Assumptions: The system timer has been initialized and started
//              during the boot process.
//---------------------------------------------------------------------
//@ Module-Decomposition
//  SigmaTimerHandler
//---------------------------------------------------------------------
//@ Rationale
//  This module contains the system 5ms timer handler.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  not applicable
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  not applicable
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/SigmaTimerHandler.ccv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By: Gary Cederquist Date: 08-Oct-1997 DR Number: 2549
//      Project:   Sigma   (R8027)
//      Description:
//         Externalized TickCount_ to support HiResTimer.
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================

#include "SigmaTypes.hh"
#ifdef E600_840_TEMP_REMOVED
#include <logio.h>
#include <vrtxil.h>
#endif
#include "Watchdog.hh"
#include "MemoryMap.hh"

#ifdef SIGMA_BD_CPU
#  include "IpcIds.hh"
#  include "TaskControlQueueMsg.hh"
#  include "BdQueuesMsg.hh"
#endif

#ifdef SIGMA_GUI_CPU
#  include "Keyboard.hh"
#endif

#ifdef SIGMA_UNIT_TEST
   extern void   ut_qpost(int queue, VRTX_MSG msg, int * err);
#  define sc_qpost ut_qpost
#endif

extern "C" void  user_timer();
extern "C" Int16 XS_TimeTick(void);

//=====================================================================
//
//   Global Data
//
//=====================================================================

//  BD cycle timer disabled initially - Breath-Delivery enables cycle
//  timer by setting TRUE
Boolean BdTimerEnabled = FALSE;     // BD timer disabled initially


//  Set by the XS_TimeTick() and read by the Stack Ware task
Int16   StackWareTimeoutCallFlag = 0;

//  free counter incremented each 5ms
static Uint TickCount_ = 0;

//=====================================================================
//
//   Static Data
//
//=====================================================================

#if defined(SIGMA_GUI_CPU) || defined(SIGMA_BD_CPU)
static const Uint8 LED_CYCLE[16] =
{
    (Uint8)~1
   ,(Uint8)~2
   ,(Uint8)~4
   ,(Uint8)~8
   ,(Uint8)~16
   ,(Uint8)~32
   ,(Uint8)~64
   ,(Uint8)~128
   ,(Uint8)~128
   ,(Uint8)~64
   ,(Uint8)~32
   ,(Uint8)~16
   ,(Uint8)~8
   ,(Uint8)~4
   ,(Uint8)~2
   ,(Uint8)~1
};
#else
static const Uint8 LED_CYCLE[16] =
{
    (Uint8)~0x81
   ,(Uint8)~0x42
   ,(Uint8)~0x24
   ,(Uint8)~0x18
   ,(Uint8)~0x18
   ,(Uint8)~0x24
   ,(Uint8)~0x42
   ,(Uint8)~0x81
   ,(Uint8)~0x81
   ,(Uint8)~0x42
   ,(Uint8)~0x24
   ,(Uint8)~0x18
   ,(Uint8)~0x18
   ,(Uint8)~0x24
   ,(Uint8)~0x42
   ,(Uint8)~0x81
};
#endif

// strobe watchdog for ~10 sec =  2000 * 5ms
static Uint32  WatchdogCountdown = 2000;

#define WRITE_CPU_LED(A)   (*DIAG_LED_REG = (A))


