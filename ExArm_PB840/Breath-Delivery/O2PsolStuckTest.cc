#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: O2PsolStuckTest - BD Safety Net Test for the detection of
//                           O2 PSOL stuck 
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes methods in the Psol class to obtain 
//  the value of the O2 PSOL command and the Sensor class to obtain 
//  the O2 flow reading.
//  It also interfaces with the GasSupply object to determine if oxygen
//  is available.
//---------------------------------------------------------------------
//@ Rationale
//  Used to determine if the O2 PSOL is stuck.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/O2PsolStuckTest.ccv   10.7   08/17/07 09:39:26   pvcs  
//
//@ Modification-Log
//
//  Revision: 005  By: iv     Date:  08-Jun-1999    DR Number: 5425
//  Project:  ATC
//  Description:
//      Added an offset to the highPsolCommandLimit.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code when fault is detected.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  19-Sep-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "O2PsolStuckTest.hh"
#include "MainSensorRefs.hh"
#include "MiscSensorRefs.hh"
#include "ValveRefs.hh"
#include "VentObjectRefs.hh"

//@ Usage-Classes
#include "FlowSensor.hh"
#include "LinearSensor.hh"
#include "GasSupply.hh"
#include "Psol.hh"

#if defined ( SIGMA_SAFETY_NET_DEBUG )
#include <stdio.h>
#endif // defined ( SIGMA_SAFETY_NET_DEBUG )
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: O2PsolStuckTest()  
//
//@ Interface-Description
//  Default Constructor.  Passes two arguments to the base class
//  constructor to define the error code and time criterion.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are
//  set by the base class constructor.  The background event id is
//  a unique identifier that is placed in the diagnostic log if the
//  background check detects a problem.  The maximum Cycles that the
//  criteria can be met before the Background subsystem is notified
//  is MAX_CYCLES_PSOL_STUCK.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

O2PsolStuckTest::O2PsolStuckTest( void ) :
SafetyNetTest( BK_O2_PSOL_STUCK, MAX_CYCLES_PSOL_STUCK )
{
    // $[TI1]
    CALL_TRACE("O2PsolStuckTest(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~O2PsolStuckTest() 
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

O2PsolStuckTest::~O2PsolStuckTest( void )
{
    CALL_TRACE("~O2PsolStuckTest(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no arguments and returns a BkEventName.
//  This method is responsible for detecting the O2 PSOL stuck
//  condition.
//  If the background check criteria have been met for the required
//  number of cycles, the backgndEventId for the class is returned;
//  otherwise BK_NO_EVENT is returned to indicate to the caller that
//  the Background subsystem should not be notified.
//
//  If the check has not already failed, the status of the oxygen
//  supply is checked.  If oxygen is available, the value of the
//  O2 flow signal and the value of the O2 PSOL command level are
//  retrieved from the flow sensor and the Psol objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first checks the status of the backgndCheckFailed_
//  data member.  If it is TRUE, the test is no longer run since
//  this Background event is not auto-resettable and the Background
//  subsystem only needs to be informed of an event once.  In this
//  case, BK_NO_EVENT is returned to the caller.
//
//  This method invokes GasSupply::isO2Present to determine if the
//  oxygen supply is available.  This check is not performed if there
//  is no oxygen supply as the loss of the O2 supply could look like
//  a PSOL stuck failure when it is not.
//
//  The method also calls the getValue() method for the O2 flow Sensor
//  flow signal.
//
//  The following criteria is tested:
//
//      Current O2 PSOL Command > High PSOL Command Limit
//      and
//      Current Desired O2 Flow <= MAX_PSOL_STUCK_TEST_FLOW_RATE 
//
//  If the condition is met, the numCyclesCriteriaMet_ data item is
//  incremented and compared to the maxCyclesCriteriaMet_ item to
//  determine if the background subsystem needs to be informed.
//
// $[06184]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
O2PsolStuckTest::checkCriteria( void )
{
    CALL_TRACE("O2PsolStuckTest::checkCriteria(void)") ;

    BkEventName rtnValue = BK_NO_EVENT;
    
    // Only do the check if it hasn't previously failed
    if ( ! backgndCheckFailed_ )
    {
    // $[TI1]
        
        // Only do the check if wall oxygen is available
        if ( RGasSupply.isO2Present() )
        {
        // $[TI1.1]

            DacCounts highPsolCommandLimit = (DacCounts)
			    ( ( QUICKEST_PSOL_COMMAND_GRADIENT * RO2FlowSensor.getValue()
				    + MAX_HIGH_PSOL_COMMAND ) * PSOL_CURRENT_TO_DAC_SCALE
				    +  PSOL_COMMAND_OFFSET );
            
            DacCounts currentO2PsolCommand = RO2Psol.getPsolCommand();
            Real32 currentDesiredO2Flow = getCurrentDesiredO2Flow_();

            if ( ( currentO2PsolCommand > highPsolCommandLimit ) &&
                 ( currentDesiredO2Flow <= MAX_PSOL_STUCK_TEST_FLOW_RATE ) )
            {
            // $[TI1.1.1]
                if ( ++numCyclesCriteriaMet_ >= maxCyclesCriteriaMet_ )
                {
                // $[TI1.1.1.1]
                    errorCode_ = currentO2PsolCommand;
                    backgndCheckFailed_ = TRUE;
                    rtnValue = backgndEventId_;
                }
                // $[TI1.1.1.2]
            }
            else   // criteria not met
            {
            // $[TI1.1.2]
                numCyclesCriteriaMet_ = 0;
            }
        }
        else  // no wall oxygen
        {
        // $[TI1.2]
            numCyclesCriteriaMet_ = 0;
        }
    }
    // $[TI2]

    return( rtnValue );
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
O2PsolStuckTest::SoftFault( const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, O2PSOLSTUCKTEST,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


