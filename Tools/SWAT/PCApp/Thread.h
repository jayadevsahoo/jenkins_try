//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Thread.h
//----------------------------------------------------------------------------


#ifndef THREAD_H
#define THREAD_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <tchar.h>


//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

/// Class to manage a thread.
class Thread
{
    public:

    /// @brief Thread constructor.
    /// 
    /// @param start_routine Function in which the new thread shall start running.
    /// @param arg           Argument to pass into function.
    Thread(void *(*start_routine)(void*), void *arg=NULL)
    {
        //pthread_create(&m_thread, NULL, start_routine, arg);
        //
            m_thread = CreateThread( 
            NULL,                   // default security attributes
            0,                      // use default stack size  
            (LPTHREAD_START_ROUTINE)start_routine,          // thread function name
            arg,                    // argument to thread function 
            0,                      // use default creation flags 
            &m_tid);                // returns the thread identifier 

    }

    /// @brief 
    /// join is called to wait for a thread to exit, and perform cleanup. 
    /// 
    /// @return An integer status.
    inline int  join()
    {
        return WaitForSingleObject(m_thread, INFINITE)==WAIT_OBJECT_0;
        //return pthread_join(m_thread, NULL);
    }

    /// @brief exit is to be called before a thread exits.
    static inline void exit()
    {
        //pthread_exit(NULL);
        ExitThread(0);
    }

    private:
    HANDLE m_thread;
    DWORD  m_tid;
};

#endif
