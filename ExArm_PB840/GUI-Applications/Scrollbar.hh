#ifndef Scrollbar_HH
#define Scrollbar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Scrollbar -  A generic vertical scrollbar, containing scroll up and
// down buttons and an "elevator" in between indicating the current position
// in the view and the relative amount of the total information currently
// being viewed.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/Scrollbar.hhv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date:  13-Mar-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//  Changed to base class to allow for derived TrendingScrollbar and
//  SettingScrollbar classes. Added getTargetType and isScrolling
//  methods. Added setWidth to support narrower SettingScrollbar.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"
#include "AdjustPanelTarget.hh"
#include "ButtonTarget.hh"

//@ Usage-Classes
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ScrollbarTarget;

class Scrollbar : 	public Container, 
					public AdjustPanelTarget,
					public ButtonTarget
{
public:
	Scrollbar(Uint16 height, Uint16 numDisplayRows, ScrollbarTarget *pTarget);
    ~Scrollbar(void);

	void setEntryInfo(Uint16 firstDisplayedEntry, Uint16 numEntries);
	void setScrollable(Boolean isScrollable);
	void activate(void);
	void deactivate(void);
	Boolean isScrolling(void) const;

	// Drawable virtual
	virtual void setWidth(Int width);
 
	// AdjustPanelTarget virtual methods
    virtual void adjustPanelKnobDeltaHappened(Int32 delta);
    virtual void adjustPanelLoseFocusHappened(void);
    virtual void adjustPanelAcceptPressHappened(void);
    virtual void adjustPanelClearPressHappened(void);
	virtual AdjustPanelTarget::TargetType getTargetType(void) const;

	// Inherited from ButtonTarget
	virtual void buttonDownHappened(Button* pButton, Boolean byOperatorAction);
	virtual void buttonUpHappened(Button* pButton, Boolean byOperatorAction);

	virtual void scroll(Int32 numRowsToScroll);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
protected:

	void adjustElevator_(void);

	//@ Data-Member: pTarget_
	// A pointer to the object which receives scrolling events (whenever the
	// user uses the scrollbar).
	ScrollbarTarget* pTarget_;

	//@ Data-Member: maxElevatorHeight_
	// The maximum height of the elevator (when the view is displaying the
	// total information).
	Uint16 maxElevatorHeight_;

	//@ Data-Member: elevatorButton_
	// The button which renders the elevator and grabs knob focus for scrolling.
	TextButton elevatorButton_;

	//@ Constant: NUM_DISPLAY_ROWS_
	// The number of "rows" of data that can be viewed in the object owning
	// the scrollbar.
	const Uint16 NUM_DISPLAY_ROWS_;

	//@ Data-Member: firstDisplayedEntry_
	// The index of the entry which is displayed on the first row of the
	// scrolling window.
	Int16 firstDisplayedEntry_;

	//@ Data-Member: numEntries_
	// The number of entries comprising the total displayable information.
	Uint16 numEntries_;

	//@ Data-Member: isScrollable_
	// Scrolling can be disabled by setting this flag to FALSE.  The up and
	// down buttons go into the flat state in this case until this flag is
	// reset to TRUE.
	Boolean isScrollable_;

	//@ Data-Member: buttonDeactivated_
	// Maintains the state of the elevator button. If the button is flattened
	// for any reason, this state is set TRUE, otherwise FALSE.
	Boolean buttonDeactivated_;

private:
    // these methods are purposely declared, but not implemented...
	Scrollbar(void);						// not implemented...
    Scrollbar(const Scrollbar&);			// not implemented...
    void   operator=(const Scrollbar&);		// not implemented...
};


#endif // Scrollbar_HH
