//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================

#ifndef __Battery_Info_hh__
#define __Battery_Info_hh__

#include "SigmaTypes.hh"
#include "SocketWrap.hh"

struct ExternalBatteryInfo
{
    Uint16  stateOfCharge;
    Uint16  remainingCapacity;
    Uint32  fullChargeCapacity;
    Int32   averageCurrent;
    Uint16  voltage;
    Uint16  temperature;
    bool    isInstalled;
    bool    isPowerSource;
    bool    isCharging;
    Int32   instantaneousCurrent;

    void convHtoN()
    {
        SocketWrap sock;
        stateOfCharge       = sock.HTONS(stateOfCharge);
        remainingCapacity   = sock.HTONS(remainingCapacity);
        fullChargeCapacity  = sock.HTONL(fullChargeCapacity);
        averageCurrent      = sock.HTONL(averageCurrent);
        voltage             = sock.HTONS(voltage);
        temperature         = sock.HTONS(temperature);
        instantaneousCurrent = sock.HTONL(instantaneousCurrent);
    }

    void convNtoH()
    {
        SocketWrap sock;
        stateOfCharge       = sock.NTOHS(stateOfCharge);
        remainingCapacity   = sock.NTOHS(remainingCapacity);
        fullChargeCapacity  = sock.NTOHL(fullChargeCapacity);
        averageCurrent      = sock.NTOHL(averageCurrent);
        voltage             = sock.NTOHS(voltage);
        temperature         = sock.NTOHS(temperature);
        instantaneousCurrent = sock.NTOHL(instantaneousCurrent);
    }
};

struct SystemBatteryInfo
{
    Real32  voltage;
    Uint32  status;
    Uint8   stateOfCharge;

    void convHtoN()
    {
        SocketWrap sock;
        voltage = sock.HTONF(voltage);
        status = sock.HTONL(status);
    }

    void convNtoH()
    {
        SocketWrap sock;
        voltage = sock.NTOHF(voltage);
        status = sock.NTOHL(status);
    }
};


struct BatteryInfo
{
    Uint32              magicNumber;
    ExternalBatteryInfo ebi;
    SystemBatteryInfo   sbi;

    void convHtoN()
    {
        SocketWrap sock;
        magicNumber = sock.HTONL(magicNumber);
        ebi.convHtoN();
        sbi.convHtoN();
    }

    void convNtoH()
    {
        SocketWrap sock;
        magicNumber = sock.NTOHL(magicNumber);
        ebi.convNtoH();
        sbi.convNtoH();
    }
};

const Uint32 BATTERYINFO_MAGIC_NUMBER   =   0x4E4D4245;

#endif //__Battery_Info_hh__
