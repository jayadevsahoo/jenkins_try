    LIST
;=====================================================================
; This is a proprietary work to which Nellcor Puritan Bennett
; corporation of California claims exclusive right.  No part of this
; work may be used, disclosed, reproduced, stored in an information
; retrieval system, or transmitted by any means, electronic,
; mechanical, photocopying, recording, or otherwise without the prior
; written permission of Nellcor Puritan Bennett Corporation of
; California.
;
;     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
;=====================================================================
;
; =========================== M O D U L E   D E S C R I P T I O N ====
;@ Filename: OsInit.s - Operating System Pre-Initialization
;---------------------------------------------------------------------
;@ Interface-Description
;  This is the entry point to the 840 application load image.  The 
;  boot code jumps here after initializing the device drivers.  It
;  performs initialization prior to VRTX initialization.
;---------------------------------------------------------------------
;@ Rationale
;  This module contains the "glue" logic between the boot code and 
;  VRTX initialization for the 840 system.
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Descriptions
;---------------------------------------------------------------------
;@ Fault-Handling
;  not applicable
;---------------------------------------------------------------------
;@ Restrictions
;  none
;---------------------------------------------------------------------
;@ Invariants
;  not applicable
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version-Information
; @(#) $Header:   /840/Baseline/Operating-System/vcssrc/OsInit.s_v   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
;
;@ Modification-Log
;
;  Revision: 004  By:  rpr      Date:  29-MAR-2011  SCR Number: 6757
;       Project:  PROX
;       Description:
;             Doubled _MasterStack_ stack size to 1024 bytes for >50%
;             engineering margin.  A lot more things added since 1997.
;             can cause stack overflow problems.
;
;  Revision: 003  By:  gdc      Date:  19-JUN-1997  DR Number: 2238
;       Project:  Sigma (R8027)
;       Description:
;             Doubled master state stack size to 512 bytes for >50%
;             engineering margin.
;
;  Revision: 002  By:  gdc      Date:  18-JUN-1997  DR Number: 2224
;       Project:  Sigma (R8027)
;       Description:
;             Allocated separate master state stack to resolve 
;             Application OS Boot Failure.
;
;  Revision: 001  By:  gdc      Date:  28-MAY-1997  DR Number: 2176
;       Project:  Sigma (R8027)
;       Description:
;             Initial version.
;=====================================================================
; 
;=====================================================================
 
 
;====================== P R O C E D U R E   D E S C R I P T I O N ====
;@ Procedure:  _OsInit
;
;@ Interface-Description
;  The procedure is entry point to the 840 application load image.  The
;  boot code branches here through the JumpTable after completing
;  device driver initialization.  Interrupts are masked during the boot
;  process since it initializes the EVT and device interrupts.  Upon
;  entry, the interrupts are still blocked.  The boot process runs with
;  interrupts blocked for less than 62.5ms to avoid a processor reset
;  by the watchdog timer.  Variable initialization and VRTX
;  initialization, however, consumes more than 62.5ms and must run with
;  interrupts enabled so the watchdog disabling interrupt installed
;  during POST can continue strobing the watchdog.  Therefore, this
;  function sets the IPL to level 2 prior to calling the variable
;  initialization function or VRTX initialization.  Control never
;  returns here after transferring control to VRTX initialization
;  (sys_init).
;
;  INPUT:    D2 = pointer to the boot item list
;            SR = 2700 (interrupts masked)
;
;  OUTPUT:   none
;
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Description
;---------------------------------------------------------------------
;@ PreCondition
;  none
;---------------------------------------------------------------------
;@ PostCondition
;  none
;@ End-Procedure
;=====================================================================

        NAME    OsInit

        INCLUDE kernel.inc
        XREF    __initcopy
        XREF    _sys_init

        SECTION zerovars,4,D
        DS.B    1024
_MasterStack_:

        SECTION code,4,C
        XDEF    _OsInit
        XDEF    _sys_start

; void OsInit(void)
_OsInit:
_sys_start:
;
;  upon entry, SR=2700  (interrupts masked)
;              D2 contains pointer to boot item list
;
        IFDEF    SIGMA_PRODUCTION    ; {
        move.l   d2,-(a7)            ; Save reg d2 containing VRTX
                                     ; boot item list pointer
        move.w   #$2200,sr           ; enable watchdog strobe interrupt
        jsr      __initcopy          ; copy ??INITDATA to vars
                                     ; zero out zervars
        move.l   (a7)+,d2            ; restore d2

        movea.l  #_MasterStack_,a0
        movec.l  a0,msp              ; set the master stack pointer

        move.w   #$3200,sr           ; enable master mode and watchdog strobe

        move.l   d2,-(a7)            ; pass the boot item list pointer
        jsr      _sys_init           ; to vrtx initialization

                                     ; // $[TI1]
        illegal                      ; never returns

        ELSEC                        ; } else {   // DEVELOPMENT

        move.l   d2,-(a7)            ; Save reg d2 containing VRTX
                                     ; boot item list pointer
        jsr      __initcopy          ; copy ??INITDATA to vars
                                     ; zero out zervars
        move.l   (a7)+,d2            ; restore d2

        move.w   #$2700,sr           ; enable watchdog strobe interrupt
        movec.l  a7,msp              ; set the master stack pointer
        move.w   #$3700,sr           ; enable master mode

        move.l   d2,-(a7)            ; pass the boot item list pointer
        jsr      _sys_init           ; to vrtx initialization

        ENDC                         ; }

        END
