// vtutils.h
//////////////////////////////////////////////////////////////////////

#if !defined _VENT_UTILS
#define _VENT_UTILS

#define MAX_FILE_NAME_LENGTH   12

#define TFTP_LISTEN_PORT       8000
#define TFTP_REPLY_PORT        8001
#define BOOTP_LISTEN_PORT      8002
#define BOOTP_REPLY_PORT       8003 

#define MAX_TFTP_CLIENTS       2

enum BOOTPOpCodes{
	BOOTP_MIN_OP  = 0x70000000,
	BOOTP_REQUEST = BOOTP_MIN_OP,
	BOOTP_REPLY   = 0x71000000,
	BOOTP_MAX_OP  = BOOTP_REPLY
}; // recieved from bootp_hh

enum TargetType{
	BD_BOARD,
	GUI_BOARD = 0x1000000,
};

typedef struct BMsg{
	unsigned int opCode;
	TargetType targetType;
	unsigned int clientIpAddr;
	unsigned int serverIpAddr;
	unsigned char etherAddr[8];  // dot form of address
	char bootFileName[MAX_FILE_NAME_LENGTH];
	char padding[188];  
}BOOTPMsg;

#endif
