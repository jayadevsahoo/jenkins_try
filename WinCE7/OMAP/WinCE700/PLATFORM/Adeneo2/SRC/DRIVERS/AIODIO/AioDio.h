
//*****************************************************************************
//
//  AioDioDriver.h - Header file for AioDioDriver_t class.
//
//*****************************************************************************
#include <winioctl.h>
#include <omap_mcspi_regs.h>

#ifndef AIO_DIO_H

#define AIO_DIO_H

//#define AIODIO_TESTING	1		//uncomment to enable testing

#define GPIO_BITS_PER_BANK      (0x1F)
#define OMAP_GPIO_BANK_COUNT	6
#define MAX_GPIO_COUNT          (32 * OMAP_GPIO_BANK_COUNT)
#define GPIO_BANK(x)            (x >> 5)
#define GPIO_BIT(x)             (x & GPIO_BITS_PER_BANK)


enum DIGITAL_PORT
{
    DIGITAL_PORT_0 = 0,
    DIGITAL_PORT_1,
    DIGITAL_PORT_2,
    DIGITAL_PORT_3,
    DIGITAL_PORT_4,
	DIGITAL_PORT_5,
    DIGITAL_PORT_6,
    DIGITAL_PORT_7,
	NUM_DIO_PORTS
};

#if BSP_AURA_PROTOTYPE

#define	LEFT_KEYPAD		0
#define RIGHT_KEYPAD	1
#define BOTH_KEYPADS	2

#endif

// number of input parameters
static const DWORD READ_COUNTER_INPUT_PARAMS		= 3;
static const DWORD READ_DIGITAL_INPUT_PARAMS		= 2;
static const DWORD WRITE_DIGITAL_OUTPUT_PARAMS		= 3;
static const DWORD READ_ANALOG_CHANNEL_PARAMS		= 2;
static const DWORD WRITE_DAC_MOTOR_PARAMS			= 1;
static const DWORD READ_BUTTONS_PARAMS				= 3;
static const DWORD SET_DIGITAL_IO_DIR_PARAMS		= 2;
static const DWORD READ_DIGITAL_IO_PARAMS			= 1;
static const DWORD WRITE_DIGITAL_IO_PARAMS			= 2;
static const DWORD WRITE_KEYPAD_LED_PARAMS			= 3;
static const DWORD READ_SENSOR_PARAMS				= 1;
static const DWORD READ_MULTIPLE_ANALOG_CHANNELS_PARAMS = 3;

#define IOCTL_AIODIO_READ_COUNTER_INPUT    \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0900, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_DIGITAL_INPUT       \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0901, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_WRITE_DIGITAL_OUTPUT      \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0902, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_ANALOG_CHANNEL       \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0903, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_WRITE_DAC_MOTOR      \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0904, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_INIT_HARDWARE  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0905, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_BUTTONS  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0906, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_SET_DIGITAL_IO_DIR  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0907, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_DIGITAL_IO  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0908, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_WRITE_DIGITAL_IO  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0909, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_SET_BRIGHTNESS  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0910, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_ENA_BACKLIGHT  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0911, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_DIS_BACKLIGHT  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0912, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_GET_NMI_EBOOT_VER  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0913, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_GET_NMI_OS_VER  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0914, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_GET_BUTTON_FLAGS  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0915, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_SUSPEND_POWER  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0916, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_DEINIT  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0917, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_KEYPAD \
	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0918, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_INIT_KEYPAD_I2C \
	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0919, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_INIT_KEYPAD_GPIO \
	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0920, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_DEINIT_KEYPAD  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0921, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_WRITE_KEYPAD_LED \
	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0922, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_RESET_KEYPAD \
	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0923, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_GET_BSPTYPE \
	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0924, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_SENSOR \
	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0925, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_ENCODER \
	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0926, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_MULTIPLE_ANALOG_CHANNELS \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0927, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_GET_BOOT_ERROR_CODE \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0928, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_SET_CODEC_GAIN \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0929, METHOD_BUFFERED, FILE_ANY_ACCESS)

#if AIODIO_TESTING
// For Testing Only ---- BEGIN ----
#define IOCTL_AIODIO_WRITE_BATTERY  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0950, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_BATTERY  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0951, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_WRITE_BATTERY_MUX  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0952, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_READ_BATTERY_MUX  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0953, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_WRITE_IO_POS  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0954, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_WRITE_CS  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0955, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_AIODIO_TEST_PHY  \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x0956, METHOD_BUFFERED, FILE_ANY_ACCESS)

// For Testing Only ---- END ----
#endif

class AioDio_t
{
    public:

        AioDio_t();
		~AioDio_t();
		// Read the specified counter input
        UINT32			ReadCounterInput(DWORD CounterInput0, DWORD CounterInput1, DWORD Port);

        // Read the specified digital input
        UINT32			ReadDigitalInput(DWORD DigitalInput, DWORD Port);

        // Write the specified digital output
        void			WriteDigitalOutput(DWORD DigitalOutput, DWORD Value, DWORD Port);

        // set up the AD7923 ADC and the digital I/O
        BOOL            InitializeAioDio(void);
		BOOL			InitializeAioDioE600BD();
		void            DeinitializeAioDio(void);

        // Read the specified A/D signal. Also sets the next signal to read. Returns true
        // if everything when ok. False if it fails. Failure would be if the requested
        // address doesn't match what was set as the "NextAnalogInput" on the previous call.
        bool            ReadAnalogInput(DWORD AnalogInput, DWORD NextAnalogInput, DWORD *Reading);
        // Write to the DACs.
        // The first version assumes the motor DAC is written first, followed by the Exhl valve
        bool            WriteAnalogOutput(DWORD Value);

		void			ReadButtons(DWORD FirstButton, DWORD NumButtons, DWORD Port, DWORD *ButtonPressedArray);

		// Read, write and set direction of bi-directional GPIOs
        UINT32          ReadDigitalIO(DWORD DigitalInput);
        void            WriteDigitalIO(DWORD DigitalOutput, DWORD Value);

        void            SetDirection(DWORD DigitalInputOutput, DWORD Direction);
		void			SetBrightness(DWORD data);
		void			DisableBacklight();
		void			EnableBacklight();
		bool			ReadSensor(DWORD cs, DWORD *pReadData);
		bool			SetCodecGain(DWORD Value);

#if AIODIO_TESTING
		bool			WriteBattery(DWORD subaddr, UCHAR *data, DWORD size, BYTE SlaveAddr);
		bool			ReadBattery(DWORD subaddr, UCHAR *data, DWORD size, BYTE SlaveAddr);
		bool			WriteBatteryMux(DWORD subaddr, UCHAR *data, DWORD size, BYTE SlaveAddr);
		bool			ReadBatteryMux(DWORD subaddr, UCHAR *data, DWORD size, BYTE SlaveAddr);
		void            WriteChipSelect(DWORD ChipSelectSetting);
        void            SetDioBitPosition(DWORD Position);
#endif

    protected:
        
#ifndef AIODIO_TESTING		
		// Set the chip select line as requested. Only one of the chip selects can
        // be set at a time.
        void            WriteChipSelect(DWORD ChipSelectSetting);
        void            SetDioBitPosition(DWORD Position);
#endif

		void			SetGpioBit(DWORD id);
		void			ClrGpioBit(DWORD id);
		void			SetGpioMultiBits(DWORD id, DWORD pattern);
		void			ClrGpioMultiBits(DWORD id, DWORD pattern);
		DWORD			GetGpioBit(DWORD id);

		DWORD			WriteSPI(OMAP_MCSPI_CHANNEL_REGS *pChannelRegs, void *pData, DWORD size, DWORD dwLength);
		DWORD			ReadSPI(OMAP_MCSPI_CHANNEL_REGS *pChannelRegs, void *pData, DWORD size, DWORD dwLength);
		DWORD			WriteReadSPI(OMAP_MCSPI_CHANNEL_REGS *pChannelRegs, void *pInData, void *pOutData, DWORD size, DWORD dwLength);

		DWORD			LastPortAccessed;			// knowing this allows using the FIFO on DAC writes

		CRITICAL_SECTION AioDioCriticalSection;     // make sure the digital I/O don't get stepped on by threads
		
		HANDLE			hGIOPort;
		HANDLE			hSPI3_CH0;
		HANDLE			hSPI3_CH1;
		HANDLE			hTled;
		
#if AIODIO_TESTING
		HANDLE		    hI2CPort;
#endif

};


#endif

