#!/bin/bash
ReviewId=NEW

function syntax()
{
    cat << EOF
$0 -[b|l|s <source version>|d <destination version>] [-r <review_id>] [-f filter]
Creates a new review or modifies an existing review with Code Collaborator. 

While creating a new review, the diffs are computed based between the latest commit (destination)
and the commit before that (source), by default. This behavior can be changed using command 
line options, but a code review with uncommitted versions is not allowed.

The command line options are parsed from left to right, and any option specified
last will take precedence over any specified first.

 -b             : Use the commit in dev_840 where this branch started as the source version. This is
                  useful if you havent yet merged from latest dev_840.
 -l             : Use the latest commit on dev_840 as the source. This can be useful if the code
                  review is started after the merge from latest dev_840.
 -s <commit id> : Use the commit id as the source version. Default is the commit before last (i.e. HEAD~1).
 -d <commit id> : Use the commit id as the destination version. Default is last commit (i.e. HEAD).
 -r <review id> : Specify the review id to which the diffs are to be submitted. The review id must be
                  a valid code review number at code collaborator, or the special value NEW. If not 
                  specified, a new review will be created (equivalent to NEW). Default is to create a 
                  new review.
 -f <file>      : Use <file> as a file name filter. By default, the script finds all differences between
                  the specified source and destination versions, and uploads the diffs. This option
                  allows one to only use a subset of those found. The file contents are file names, 
                  separated by space or new line.
 -F             : Creates a text file with the names of files that have changed between the specified
                  source and destination commits. This file can be used with -f option. This option does
                  *not* create a new review.
 -h             : Displays this help.

When invoked without any parameter, the behavior is that of '$0 -r NEW', creating a new review for the
files part of last commit.

EOF
    exit
}

# We should get these values before getopts since they are used in
# the -b and -l options
CurrentBranch=`git rev-parse --abbrev-ref HEAD`
CurrentCommit=`git rev-parse --short HEAD`
LastCommit=`git rev-parse --short HEAD~1`
LatestDev840Commit=`git ls-remote origin dev_840 | cut -c -7`
Dev840RootCommit=`git merge-base origin/dev_840 $CurrentBranch | cut -c -7`
CreateFileListOnly=0

while getopts ":hblr:s:d:f:F" opt; do
  case $opt in
    b)
      SourceCommit=$Dev840RootCommit
      echo "Will use dev_840 BASE as source"
      ;;
    l)
      SourceCommit=$LatestDev840Commit
      echo "Will use dev_840 LATEST as source"
      ;;
    s)
      SourceCommit=$OPTARG
      ;;
    d)
      DestCommit=$OPTARG
      ;;
    r)
      ReviewId=$OPTARG
      ;;
    f)
      FilterFile=$OPTARG
      ;;
    F)
      CreateFileListOnly=1
      ;;
    h)
        syntax
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done


if [ "$SourceCommit" = "" ]; then
    SourceCommit=$LastCommit
fi
if [ "$DestCommit" = "" ]; then
    DestCommit=$CurrentCommit
fi
if [ "$FilterFile" != "" ]; then
    if [ -f $FilterFile ]; then
        filesForReview=`cat $FilterFile`
        #if no filter set, the filesForReview will be "", which
        #means all files
    fi
fi

if [ $CreateFileListOnly -eq 1 ]; then
    git diff --name-only $SourceCommit $DestCommit > filelist.$SourceCommit.$DestCommit.txt
    cp -f filelist.$SourceCommit.$DestCommit.txt filelist.txt
else
    ccollab addgitdiffs $ReviewId $SourceCommit $DestCommit -- $filesForReview
fi


