#include "stdafx.h"
// topbasewindow.cpp : implementation file
//
#include "ExArm.h"
#include "MsgQue.h"
#include "TopBaseWindow.h"
#include "GUICommThread.h"
#include "ButtonsThread.h"

#include "..\SWAT\SWATCommandServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// Indexed by MembraneButtons_t
static const bool MembraneButtonRepeats[NUMBER_OF_MEMBRANE_BUTTONS] =
{
    false,                   // MEMBRANE_BUTTON_UP
    false,                   // MEMBRANE_BUTTON_DOWN
    false,                  // MEMBRANE_BUTTON_ACCEPT
    false,                  // MEMBRANE_BUTTON_CANCEL
    false,                  // MEMBRANE_BUTTON_MAN_BREATH
    false,                  // MEMBRANE_BUTTON_SILENCE_RESET
    false,                  // MEMBRANE_BUTTON_DIMNESS
    //true,                   // MEMBRANE_BUTTON_ON_OFF
    //false                   // MEMBRANE_BUTTON_STORE_BMP
};


TopBaseWindow_t::TopBaseWindow_t(UINT IDTemplate, CWnd* ParentPtr /*=NULL*/)
	: CDialog(IDTemplate, ParentPtr),MsgQue_t(IDTemplate, ParentPtr)
{
	//{{AFX_DATA_INIT(TopBaseWindow_t)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	//InitializeCriticalSection(&cs);

	ReadHandle	= NULL;
	WriteHandle	= NULL;

}


void TopBaseWindow_t::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(TopBaseWindow_t)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(TopBaseWindow_t, CDialog)
	//{{AFX_MSG_MAP(TopBaseWindow_t)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

UINT SWATServerThreadProc(LPVOID pParam)
{
	SWATCommandServer commandServer;
	commandServer.ListenTo( "32274" );

	return 0;
}

BOOL TopBaseWindow_t::OnInitDialog()
{
	uint16_t MaxMailEntrySize	= (uint16_t)GUI_THREAD_MESSAGE_SIZE;
    uint16_t Entries            = GUI_THREAD_MAX_MESSAGES;
	LPCWSTR Name				= NULL;
	
	CDialog::OnInitDialog();

	SetWindowPos(	NULL,
					ViewPortDimensions.left+100,
					ViewPortDimensions.top,
					(ViewPortDimensions.right-ViewPortDimensions.left)+100,
					(ViewPortDimensions.bottom-ViewPortDimensions.top)+25,
					SWP_NOZORDER);


	MSGQUEUEOPTIONS MessageQueueOptions;


    // Create the message queue and get the read handle
    MessageQueueOptions.dwSize = sizeof(MessageQueueOptions);
    MessageQueueOptions.dwFlags = 0;
    MessageQueueOptions.dwMaxMessages = Entries;
    MessageQueueOptions.cbMaxMessage = MaxMailEntrySize;
    MessageQueueOptions.bReadAccess = true;

    ReadHandle = CreateMsgQueue(_T("TopBaseWindow"), &MessageQueueOptions);
    if (!ReadHandle)
    {
        ErrVal = GetLastError();
        DEBUG_ASSERT(false);
    }

    // Set up to get a write handle
    MessageQueueOptions.bReadAccess = false;
    WriteHandle = OpenMsgQueue(OpenProcess(0, FALSE, GetCurrentProcessId()), ReadHandle, &MessageQueueOptions);
    DEBUG_ASSERT(WriteHandle);
    if (!WriteHandle)
    {
        ErrVal = GetLastError();
        DEBUG_ASSERT(false);
    }

    MaxEntrySize = MaxMailEntrySize;

	// start a SWAT thread to listen to client and process COMMANDs
	AfxBeginThread(SWATServerThreadProc, NULL);
	
    return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void TopBaseWindow_t::OnDestroy()
{
    CloseMsgQueue(WriteHandle);
    CloseMsgQueue(ReadHandle);

	CDialog::OnDestroy();

	// TODO: Add your message handler code here

}

//#############################################################################
//
//  TopLevelWindow_t::MembraneButtonPressed - User has pressed a membrane button.
//
//  NOTE: This handles button presses that need to a user interface response.
//      For real time response (such as Man Breath) the button state is obtained
//      directly by the affected thread.
//
//#############################################################################

/*
void TopBaseWindow_t::BaseMembraneButtonPressed(MembraneButtons_t Button, LPARAM RepeatCount)
{
    switch (Button)
    {
        case MEMBRANE_BUTTON_UP:
        case MEMBRANE_BUTTON_DOWN:
        case MEMBRANE_BUTTON_ACCEPT:
        case MEMBRANE_BUTTON_CANCEL:
        case MEMBRANE_BUTTON_SILENCE_RESET:
        case MEMBRANE_BUTTON_DIMNESS:
        case MEMBRANE_BUTTON_MAN_BREATH:                        // Man breath gets handled immediately by breath thread
            PostMessage(WM_MEMBRANE_BUTTON_PRESSED, Button, RepeatCount);
            break;

        // When on/off is pressed, we need to handle immediately. Use Send not Post of the message.
        case MEMBRANE_BUTTON_ON_OFF:
            SendMessage(WM_MEMBRANE_BUTTON_PRESSED, Button, RepeatCount);
            break;

// This stuff isn't normally used. Definitely not used in production!
        case MEMBRANE_BUTTON_STORE_BMP:
#if ALLOW_SAVE_WINDOWS
            ActiveScreen->SaveWindow();
#else
#if DUMP_REAL_TIME_DATA
            if (!DUMP_REAL_TIME_DATA_DumpingRtData)
            {
                DUMP_REAL_TIME_DATA_CapturingRtData = false;
                DUMP_REAL_TIME_DATA_DumpingRtData = true;
                DUMP_REAL_TIME_DATA_DumpIndex = DUMP_REAL_TIME_DATA_StoreIndex;
//                RETAILMSG(true, (_T("<<<<CAPTURE NEXT BREATH>>>>\r\n")));
            }
#endif
#endif
            break;

        default:
            break;
    }

    // Let the screen know about the activity
    PostMessage(WM_USER_ACTIVITY, 0, 0);
}



//#############################################################################
//
//  TopLevelWindow_t::MembraneButtonReleased - User has released a membrane button.
//
//#############################################################################

void TopBaseWindow_t::BaseMembraneButtonReleased(MembraneButtons_t Button)
{
    switch (Button)
    {
        case MEMBRANE_BUTTON_UP:
        case MEMBRANE_BUTTON_DOWN:
        case MEMBRANE_BUTTON_ACCEPT:
        case MEMBRANE_BUTTON_CANCEL:
            PostMessage(WM_MEMBRANE_BUTTON_RELEASED, Button, 0);
            break;

        case MEMBRANE_BUTTON_MAN_BREATH:
        case MEMBRANE_BUTTON_SILENCE_RESET:
        case MEMBRANE_BUTTON_DIMNESS:
        case MEMBRANE_BUTTON_STORE_BMP:
            break;

        // When on/off is released, we need to handle immediately. Use Send not Post of the message.
		case MEMBRANE_BUTTON_ON_OFF:
            SendMessage(WM_MEMBRANE_BUTTON_RELEASED, Button, 0);
			break;

        default:
            break;
    }

    // Let the screen know about the activity
    PostMessage(WM_USER_ACTIVITY, 0, 0);
}
*/

LRESULT TopBaseWindow_t::WindowProc(
   UINT message,
   WPARAM wParam,
   LPARAM lParam 
)
{
	return CDialog::WindowProc(message, wParam, lParam);
}

/*
tony: extracted from ProcessMembraneButtons,re-used by TopLevelWindow_t::OnMembraneButton
handle one membrane button event
*/
void TopBaseWindow_t::ProcessMembraneBtn(int keyId, bool btnPressed, BOOL SendToWaveScreen/*=FALSE*/)
{
	if (btnPressed)
    {
        if (!ButtonPreviouslyPressed[keyId])
        {
            MembraneButtonPressed((MembraneButtons_t)keyId, 1,SendToWaveScreen);
            ButtonPreviouslyPressed[keyId] = true;
            if (MembraneButtonRepeats[keyId])                       // if this is a repeat button, init the repeat sequence
            {
                RepeatCount = 1;
                RepeatDelay = INITIAL_REPEAT_DELAY;
            }
        }
        if (MembraneButtonRepeats[keyId])                           // repeat on subsequent timer ticks as needed
        {
            if (--RepeatDelay <= 0)
            {
                RepeatCount++;                                  // increment every 200ms

                MembraneButtonPressed((MembraneButtons_t)keyId, RepeatCount,SendToWaveScreen);  // repeat the button press
                RepeatDelay = REPEAT_DELAY;
            }
        }
        //break;                                                  // allow only one button to be handled at a time
    }
    else
    {
         if (ButtonPreviouslyPressed[keyId])
        {
            MembraneButtonReleased((MembraneButtons_t)keyId,SendToWaveScreen);
            ButtonPreviouslyPressed[keyId] = false;
        }
    }
}

//#############################################################################
//
//  TopBaseWindow_t::ProcessMembraneButtons - Query and Handle the Membrane Buttons
//
//#############################################################################
void TopBaseWindow_t::ProcessMembraneButtons(BOOL SendToWaveScreen)
{
    bool ButtonPressed[NUMBER_OF_MEMBRANE_BUTTONS];

    ButtonsThread.GetButtonsPressed(ButtonPressed, NUMBER_OF_MEMBRANE_BUTTONS);

    // handle buttons
    for (int i=0; i<NUMBER_OF_MEMBRANE_BUTTONS; i++)
    {
		ProcessMembraneBtn(i, ButtonPressed[i], SendToWaveScreen);
		
		if(ButtonPressed[i])
			break;                                                  // allow only one button to be handled at a time
    }

}
//#############################################################################
//
//  TopBaseWindow_t::SendMail - Used by threads to send a message to this task.
//
//      Timeout not implemented at this time
//
//#############################################################################

bool TopBaseWindow_t::SendMail(uint8_t *MessagePtr, uint16_t MessageSize, int32_t Timeout)
{
    bool EntryFree = false;

	if (WriteHandle)
	{
		if (WriteMsgQueue(WriteHandle, MessagePtr, MessageSize, Timeout, 0))
		{
			return true;
		}
		else
		{
			ErrVal = GetLastError();
			return false;
		}
	}
	else
		return false;
}


//#############################################################################
//
//  TopBaseWindow_t::GetMail -  Used by this task to retrieve messages from the
//                           mailbox.
//
//#############################################################################

bool TopBaseWindow_t::GetMail(uint8_t *MessagePtr, uint32_t Timeout)
{
    uint32_t BytesRead;
    uint32_t Flags;

	try
		{
		if (ReadHandle)
		{
			if (ReadMsgQueue(ReadHandle, MessagePtr,  MaxEntrySize, &BytesRead, Timeout, &Flags))
			{
				return true;
			}
			else
			{
				ErrVal = GetLastError();
				if (ErrVal != ERROR_TIMEOUT)
				{
					ErrVal = ERROR_TIMEOUT;
				}
				return false;
			}
		}
		else
			return false;
		}
	catch(...)
		{
		return false;
		}
}


//#############################################################################
//
//  MailboxThread_t::GetMail -  Used by this task to retrieve messages from the
//                           mailbox.
//
//#############################################################################

bool TopBaseWindow_t::GetMail(uint8_t *MessagePtr, uint32_t *BytesRead, uint32_t Timeout)
{
    uint32_t Flags;

	if (ReadHandle)
	{
		if (ReadMsgQueue(ReadHandle, MessagePtr,  MaxEntrySize, BytesRead, Timeout, &Flags))
		{
			return true;
		}
		else
		{
			ErrVal = GetLastError();
			if (ErrVal != ERROR_TIMEOUT)
			{
				ErrVal = ERROR_TIMEOUT;
			}
			return false;
		}
	}
	else
		return false;
}
void TopBaseWindow_t::HandleIntraVentMsg(IntraVentMsgIds_t msg, uint8_t* data)
{
	
}
