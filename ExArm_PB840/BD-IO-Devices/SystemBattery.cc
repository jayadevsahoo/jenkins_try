#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@  Class: SystemBattery - implements the functionality required for
//                          interfacing with the system battery
//---------------------------------------------------------------------
//@ Interface-Description
//  This class implements the interface to the system battery.  It provides 
//  the ablitity to interrogate whether or not the battery is installed. 
//  Every cycle, this class will  determine if  the battery is charged within 
//  the acceptable voltage range, and illuminate the LED accordingly.  This 
//  class also messages the Alarm-Analysis subsystem with the installation 
//  status of the battery if the battery  is installed.
//---------------------------------------------------------------------
//@ Rationale
//  This class implements the algorithm for interfacing with the system 
//  battery.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This system battery can be in any of the following statuses: initialize,
//  charging, charged, on battery power, and fault.
//  If a system battery is installed and in normal operation, it may
//  be in either of the following states: inoperative, low, and good. 
//  A privated data member keeps track of the system battery's state.
//  Another data member maintains the elapsed time in the current status.
//  The input data to this class includes the charged and charging status
//  lines, battery current and voltage.  With these information,
//  each status monitor can determine whether to stay in the current status
//  or declare a new status.
//  The following state machine is implemented:
//
//  Current status     transition  Next status
//  ==============     ==========  ===========
//      STARTUP          --1-->    NOT_INSTALLED
//      STARTUP          --2-->    CHARGING
//      STARTUP          --3-->    CHARGED
//      STARTUP          --4-->    ON_BATTERY_POWER
//      STARTUP          --5-->    BPS_FAULT
//      STARTUP          --6-->    BPS_FAULT
//
//      CHARGING         --3-->    CHARGED
//      CHARGING         --4-->    ON_BATTERY_POWER
//      CHARGING         --5-->    BPS_FAULT
//      CHARGING         --6-->    BPS_FAULT
//      CHARGING         --9-->    BPS_FAULT
//  
//      CHARGED          --2-->    CHARGING
//      CHARGED          --4-->    ON_BATTERY_POWER
//      CHARGED          --5-->    BPS_FAULT
//      CHARGED          --6-->    BPS_FAULT
//  
//      ON_BATTERY_POWER --7-->    CHARGING
//      ON_BATTERY_POWER --8-->    CHARGED
//      ON_BATTERY_POWER --5-->    BPS_FAULT
//                         
//      BPS_FAULT        --!4->    BPS_FAULT + not on battery power
//      BPS_FAULT        --4-->    BPS_FAULT + on battery power
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  Since the SystemBattery class contains BinaryIndicator objects, it
//  has to be constructed after the construction of the BinaryIndicator class. 
//	The newCycle method of this class must be invoked
//  after the newCycle method for the SensorMediator.  The initialize
//  method of this class must be invoked before the initialize method of
//  of the PowerSource class since the PowerSource initialization depends
//  upon the status of the battery.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//@ (#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/SystemBattery.ccv   10.7   08/17/07 09:32:06   pvcs  
//
//@ Modification-Log
//
//  Revision: 015  By:  mnr    Date:   16-Feb-2009    SCR Number: 6472
//       Project:  Baseline
//       Description:
//          Requirements tracing updated as per code review action item.
// 
//  Revision: 014  By:  mnr    Date:   13-Feb-2009    SCR Number: 6472
//       Project:  Baseline
//       Description:
//          Comments upated for changes in previous revision.
// 
//  Revision: 013  By:  mnr    Date:   13-Feb-2009    SCR Number: 6472
//       Project:  Baseline
//       Description:
// 			Increased the MAX_BATTERY_CHARGE_HOURS to 20 for new Extended
// 			Battery.
// 
//  Revision: 012  By:  jja    Date:   18-Apr-2000    DR Number: 5668
//       Project:  NeoMode
//       Description:
//			Swap order of evaluating transition5_() and transition6_()
//			to flag an open in the battery before a bad LED.
//
//  Revision: 011  By:  syw    Date:   05-Apr-2000    DR Number: 5541
//       Project:  NeoMode
//       Description:
//			Initialize currentBpsQuality_ to NONE from GOOD thus allowing
//			newCycle() to update quality.
//
//  Revision: 010  By:  iv    Date:   03-May-1999    DR Number: DCS 5376
//       Project:  ATC
//       Description:
//			Added diagnostic code logs for BPS events.
//
//  Revision: 009 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 008  By:  syw	    Date:   14-Sep-1998    DR Number: none
//       Project:  BiLevel
//       Description:
//			BiLevel initial release.
//			Added second argument to pVentStatusCallback.
//
//  Revision: 007  By:  iv    Date:   18-Nov-1997    DR Number: DCS 2610
//       Project:  Sigma (R8027)
//       Description:
//			Turn off BPS Ready LED when BPS quality is low. Added a call
//          in initialStatusNotification() to pBatteryAlarmCallBack_() so that
//          low battery alarm notifications are updated after communication
//          recovery.
//
//  Revision: 006  By:  syw    Date:  08-Sep-1996    DR Number: DCS 2280
//       Project:  Sigma (R8027)
//       Description:
//			Eliminate PowerSource comments.
//
//  Revision: 005  By: iv    Date:  08-Aug-1997    DR Number: DCS 2032, 1474 
//      Project:  Sigma (840)
//              Description:
//                      Added testable items per DRs.
//
//  Revision: 004  By: iv    Date:  08-Aug-1997    DR Number: DCS 2032, 1474 
//      Project:  Sigma (840)
//              Description:
//                      Update per new battery spec.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: DCS 1656
//      Project:  Sigma (840)
//              Description:
//                      Capitalize all global references per coding standard.
//
//  Revision: 002  By:  iv    Date:  09-May-1997    DR Number: DCS 2041, 2038
//       Project:  Sigma (840)
//       Description:
//             Fixed battery to issue correct alarms and GUI notifications
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "SystemBattery.hh"

//@ Usage-Classes
#include "RegisterRefs.hh"
#include "MiscSensorRefs.hh"
#include "VentObjectRefs.hh"
#include "MathUtilities.hh"
#include "Background.hh"
#include "PowerSource.hh"
#include "ExternalBattery.hh"

//@ End-Usage


//@ Code...

//@ Constant: SYSBATT_VOLTSENSOR_VOLT_MIN
// system battery voltage sensor min voltage
const Real32 SYSBATT_VOLTSENSOR_VOLT_MIN  = 02.285714F;

//@ Constant: SYSBATT_VOLTSENSOR_VOLT_MAX
// system battery voltage sensor max voltage
const Real32 SYSBATT_VOLTSENSOR_VOLT_MAX  = 04.000000F;

//@ Constant: SYSBAT_VOLTSENSOR_ENG_MIN
// system battery voltage sensor engineering min
const Real32 SYSBATT_VOLTSENSOR_ENG_MIN   = 08.000000F;

//@ Constant: SYSBAT_VOLTSENSOR_ENG_MAX
// system battery voltage sensor engineering max
const Real32 SYSBATT_VOLTSENSOR_ENG_MAX   = 14.000000F;

// For relevant informartion see Requirement Specification:
// Backup Power Source No. 70520-45
// Analog Interface PCBA No. 70550-45

//@ Constant: BATTERY_CHARGING_MASK
// mask to read the battery charging bit data
static const Uint8 BATTERY_CHARGING_MASK = 0x02;
 
//@ Constant: BATTERY_CHARGED_MASK
// mask to read the battery charged bit data
static const Uint8 BATTERY_CHARGED_MASK = 0x04;
  
//@ Constant: POWER_MODE_MASK 
// mask to read the battery charged bit data
static const Uint8 POWER_MODE_MASK = 0x01;

//@ Constant: OPEN_CIRCUIT_VOLTAGE 
// battery disconnected voltage threshold
static const Real32 OPEN_CIRCUIT_VOLTAGE = 14.0F;

//@ Constant: TWO_MINUTE_RUN_TIME_VOLTAGE
// Threshold to declare low battery when on battery power
static const Real32 TWO_MINUTE_RUN_TIME_VOLTAGE = 10.5F;

//@ Constant: LOW_BATTERY_VOLTAGE
// Threshold to declare low battery when NOT on battery power
static const Real32 LOW_BATTERY_VOLTAGE = 10.0F;

//@ Constant: BATTERY_CHARGED_VOLTAGE
// Threshold to declare battery charged when NOT on battery power
static const Real32 BATTERY_CHARGED_VOLTAGE = 13.53F;

//@ Constant: DISCONNECT_CURRENT_THRESHOLD 
// discharge current threshold
const Real32 DISCONNECT_CURRENT_THRESHOLD = 1.5F;

//@ Constant: MAX_BPS_MODEL_1_VOLTAGE
// maximum BPS model 1 voltage 
const Real32 MAX_BPS_MODEL_1_VOLTAGE = 1.0F;

//@ Constant: MIN_BPS_MODEL_1_VOLTAGE
// minimum BPS model 1 voltage 
const Real32 MIN_BPS_MODEL_1_VOLTAGE = 0.5F;

//@ Constant: ONE_SYSTEM_BATTERY_SECOND 
// one system battery minute at 20 MS resolution
static const Uint8 ONE_SYSTEM_BATTERY_SECOND = 50;

//@ Constant: ONE_SYSTEM_BATTERY_MINUTE 
// one system battery minute at 20 MS resolution
static const Uint32 ONE_SYSTEM_BATTERY_MINUTE = 60 * 50;

//@ Constant: ONE_SYSTEM_BATTERY_HOUR
// one system battery hour at 20 MS resolution
static const Uint32 ONE_SYSTEM_BATTERY_HOUR = 60 * 60 * 50;

//@ Constant: MAX_BATTERY_CHARGE_HOURS
// max number of hours that battery can be allowed to charge
static const Uint32 MAX_BATTERY_CHARGE_HOURS = 20;

//@ Constant: MAX_TRANS1_COUNT
// counter for number of transitions
static const Int8 MAX_TRANS1_COUNT = 2;

//@ Constant: MAX_TRANS2_COUNT
// counter for number of transitions
static const Int8 MAX_TRANS2_COUNT = 4;

//@ Constant: MAX_TRANS3_COUNT
// counter for number of transitions
static const Int8 MAX_TRANS3_COUNT = 4;

//@ Constant: MAX_TRANS4_COUNT
// counter for number of transitions
static const Int8 MAX_TRANS4_COUNT = 1;

//@ Constant: MAX_TRANS5_COUNT
// counter for number of transitions
static const Int8 MAX_TRANS5_COUNT = 4;

//@ Constant: MAX_TRANS6_COUNT
// counter for number of transitions
static const Int8 MAX_TRANS6_COUNT = 4;

//@ Constant: MAX_TRANS7_COUNT
// counter for number of transitions
static const Int8 MAX_TRANS7_COUNT = 1;

//@ Constant: MAX_TRANS8_COUNT
// counter for number of transitions
static const Int8 MAX_TRANS8_COUNT = 2;

//@ Constant: MAX_TRANS9_COUNT
// counter for number of transitions
static const Int8 MAX_TRANS9_COUNT = 1;

static const Uint16 VOLT_TABLE_SIZE = 20;

// TODO E600 
// The following calibration table is tested on VENT 39 in CTC
// compare the state of charge measured between AC connected
// and AC not connected, there's a big difference. We must think
// about another better method to measure the calibration table.

// Discharging voltage table from 5% to 100% ( step 5% )
static const Real32 DISCHARGING_VOLT_TABLE[VOLT_TABLE_SIZE] = { 
    10.7712F, 11.0423F, 11.2041F, 11.3762F, 11.4747F,
    11.5640F, 11.6767F, 11.7478F, 11.8121F, 11.8919F, 
    11.9562F, 12.0132F, 12.0805F, 12.1532F, 12.2014F, 
    12.2416F, 12.3101F, 12.3468F, 12.3740F, 12.4160F };

// Charging voltage table from 5% to 100% ( step 5% )
static const Real32 CHARGING_VOLT_TABLE[VOLT_TABLE_SIZE] = {
    12.2502F, 12.5453F, 12.7451F, 12.8730F, 12.9724F,
    13.0521F, 13.1206F, 13.1819F, 13.2315F, 13.2757F,
    13.3137F, 13.3468F, 13.3782F, 13.4058F, 13.4310F,
    13.4530F, 13.4741F, 13.4916F, 13.5103F, 13.5258F };

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SystemBattery()  [Constructor]
//
//@ Interface-Description
//
//  This constructor takes no argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  Initialize the battery charged, charging, and power mode binary
//  indicators.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
SystemBattery::SystemBattery( void ) 
    : voltageBufferIndex_(0)
{
    // $[TI1]
    CALL_TRACE("SystemBattery::SystemBattery(void)");

    memset( voltageBuffer_, 0, sizeof(voltageBuffer_) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SystemBattery()  [Destructor]
//
//@ Interface-Description
//  N/A
//---------------------------------------------------------------------
//@ Implementation-Description
//  N/A
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
SystemBattery::~SystemBattery(void)
{
    CALL_TRACE("SystemBattery::~SystemBattery(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize()
//
//@ Interface-Description
//
//  This method takes no arguments and has no return value.
//  It sets up the SystemBattery object to an initial known state.
//  This method is invoked during subsystem initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  The battery status is initialized to STARTUP.  The power mode bit
//  is set to high so that the power supply can draw from either AC or DC.
//  All other data members are intialized.
//---------------------------------------------------------------------
//@ PreCondition
//   ONE_SYSTEM_BATTERY_MINUTE == 60 * ONE_SYSTEM_BATTERY_SECOND &&
//   ONE_SYSTEM_BATTERY_HOUR == 60 * ONE_SYSTEM_BATTERY_MINUTE
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
SystemBattery::initialize( void )
{
    CALL_TRACE("SystemBattery::initialize( void )");

    // $[TI1]
    CLASS_ASSERTION(ONE_SYSTEM_BATTERY_MINUTE == 60 * ONE_SYSTEM_BATTERY_SECOND);
    CLASS_ASSERTION(ONE_SYSTEM_BATTERY_HOUR == 60 * ONE_SYSTEM_BATTERY_MINUTE);

    // set power mode to AC/DC
    setPowerModeOn_();

    // initialize VBATT sample buffers
    resetVbatt_();

    // initialize all status data members
    currentBpsStatusTimer_ = 0;

    transition_1_Counter_ = 0;
    transition_2_Counter_ = 0;
    transition_3_Counter_ = 0;
    transition_4_Counter_ = 0;
    transition_5_Counter_ = 0;
    transition_6_Counter_ = 0;
    transition_7_Counter_ = 0;
    transition_8_Counter_ = 0;
    transition_9_Counter_ = 0;

    newState_ = FALSE;
    onBattery_ = FALSE;
    previouslyOnBattery_ = FALSE;
    bpsStatus_ = STARTUP;
    currentBpsQuality_ = NONE;
    notificationRequired_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//
//  This method accepts no argument and returns no value.
//  This method is invoked to determine and process changes in BPS
//  status and quality.  Any change in BPS is relayed to Alarms-Analysis
//  Subsystem and vent status LEDs are commanded to reflect current
//  conditions.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  Each ONE_SYSTEM_BATTERY_SECOND the current battery status is monitored
//  through the use of a switch statement. When a change is detected to 
//  the bps quality, the alarm syb-system is notified.
//  The current battery status timer is incremented each cycle. 
//  The bps current and voltae are sampled each cycle.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
SystemBattery::newCycle( void )
{
    CALL_TRACE("SystemBattery::newCycle( void )");

    RSystemBatteryVoltage.updateValue();

    // increment BPS status timer first before main loop
    // so that main loop is not executed at time 0
    ++currentBpsStatusTimer_;

    // accumulator VBATT samples
    sampleVbatt_();

    /////////////////////////////////////////////////////////////////////
    // check for changes in BPS status once every second
    /////////////////////////////////////////////////////////////////////
    if ( 0 == currentBpsStatusTimer_ % ONE_SYSTEM_BATTERY_SECOND )
    {
    // $[TI1]

        // preserve old BPS quality
        BpsQuality oldBpsQuality = currentBpsQuality_;

        ///////////////////////////////////////////////////////////////
        // invoke the appropriate monitoring method base on BPS status
        ///////////////////////////////////////////////////////////////
        switch ( bpsStatus_ )
        {
            case STARTUP:
            // $[TI1.1]
                monitorStartupState_();
                break;

            case CHARGING:
            // $[TI1.2]
                monitorChargingState_();
                break;

            case CHARGED:
            // $[TI1.3]
                monitorChargedState_();
                break;

            case ON_BATTERY_POWER:
            // $[TI1.4]
                monitorOnBatteryPowerState_();
                break;

            case BPS_FAULT:
            // $[TI1.5]
                monitorBpsFaultState_();
                break;

            case NOT_INSTALLED:
            // $[TI1.6]
                monitorNotInstalledState_();
                break;

            default:
                CLASS_ASSERTION ( ( bpsStatus_ == NOT_INSTALLED ) ||
                                  ( bpsStatus_ == CHARGED ) ||
                                  ( bpsStatus_ == CHARGING ) ||
                                  ( bpsStatus_ == STARTUP ) ||
                                  ( bpsStatus_ == ON_BATTERY_POWER ) ||
                                  ( bpsStatus_ == BPS_FAULT ) );
                break;
        }

        // Notify Alarms of any change in Battery Quality
        if ( oldBpsQuality != currentBpsQuality_ )
        {
        // $[TI1.7]
            pBatteryAlarmCallBack_( currentBpsQuality_, FALSE );
        }
        // $[TI1.8]

        // save the average Vbatt to voltage buffer
        Real32 vBatt = calculateAvgVbatt_(); 
        saveVoltageToBuffer_( vBatt );

        // reset VBATT accumulators
        resetVbatt_();
    }
    // $[TI2]

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialStatusNotification
//
//@ Interface-Description
//
//  This method takes no arguments and has no return value. It is
//  invoked after system initialization to notify Alarms of initial
//  battery status.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  Data member notificationRequired_ is used to force notification of 
//  BPS status and quality to Alarms-Analysis.
//
//  $[05122]
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
SystemBattery::initialStatusNotification( void )
{
    CALL_TRACE("SystemBattery::initialStatusNotification(void)");

    // $[TI1]
    notificationRequired_ = TRUE;
    pBatteryAlarmCallBack_( currentBpsQuality_, TRUE );

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SystemBattery::SoftFault(const SoftFaultID softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, SYSTEMBATTERY,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: monitorStartupState_ 
//
//@ Interface-Description
//
//  This method takes no parameters and returns no value.
//  This method monitors BPS when it is in startup state. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  This implementation follows the state transition as specified in the file header.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
SystemBattery::monitorStartupState_( void )
{
    CALL_TRACE("SystemBattery::monitorStartupState_( void )");

    if ( newState_  || notificationRequired_ )
    {
    // $[TI1]
        newState_ = FALSE;
        notificationRequired_ = FALSE;
    }
    // $[TI2]

    if ( RPowerSource.getSource() == PowerSource::AC_POWER )
    {
        bpsStatus_ = CHARGING;
    }
    else if ( RPowerSource.isExternalBatteryConnected() == TRUE )
    {
        if ( !RExternalBattery.isPowerSource() )
        {
            bpsStatus_ = ON_BATTERY_POWER;
        }
    } 
    else
    {
        bpsStatus_ = ON_BATTERY_POWER;
    }

    if ( transition1_() )
    {
    // $[TI3]
        bpsStatus_ = NOT_INSTALLED;
    }
    else if ( transition2_() )
    {
    // $[TI4]
        bpsStatus_ = CHARGING;
    }
    else if ( transition3_() )
    {
    // $[TI5]
        bpsStatus_ = CHARGED;
    }
    else if ( transition4_() )
    {
    // $[TI6]
        bpsStatus_ = ON_BATTERY_POWER;
    }
    else if ( transition6_() || transition5_() )
    {
    // $[TI7]
        bpsStatus_ = BPS_FAULT;
    }
    // $[TI8]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: monitorChargingState_ 
//
//@ Interface-Description
//
//  This method takes no parameters and returns no value.
//  This method monitors BPS when it is in charging state.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  This implementation follows the state transition as specified in the file header.
//  If the average battery voltage >= LOW_BATTERY_VOLTAGE, then the
//  current quality is set to LOW. 
//  Note that all transitions are mutualy exclusive except from transition 9.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
SystemBattery::monitorChargingState_( void )
{
    CALL_TRACE("SystemBattery::monitorChargingState_(void)");

    Real32 vBatt = calculateAvgVbatt_(); 

    if ( newState_  || notificationRequired_ )
    {
    // $[TI1]
        pAlarmCallBack_( BdAlarmId::BDALARM_NOT_INTERNAL_BATTERY_POWER, FALSE );
        pVentStatusCallBack_( EventData::ON_BATTERY_POWER, EventData::CANCEL, EventData::NULL_EVENT_PROMPT );
        currentBpsStatusTimer_ = 0;
        newState_ = FALSE;
        notificationRequired_ = FALSE;
    }
    // $[TI2]

    if ( transition3_() )
    {
    // $[TI3]
        bpsStatus_ = CHARGED;
    }
    else if ( transition4_() )
    {
    // $[TI4]
        bpsStatus_ = ON_BATTERY_POWER;
    }
    else if ( transition6_() || transition5_() )
    {
    // $[TI5]
        bpsStatus_ = BPS_FAULT;
    }
    // $[TI6]
  
    if ( transition9_() )
    {
    // $[TI7]
        bpsStatus_ = BPS_FAULT;
    }// $[TI8]

    if ( vBatt < LOW_BATTERY_VOLTAGE )
    {
    // $[TI9]
        currentBpsQuality_ = LOW;
    }
    else
    {
    // $[TI10]
        currentBpsQuality_ = GOOD;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: monitorChargedState_ 
//
//@ Interface-Description
//
//  This method takes no parameters and returns no value.
//  This method monitors BPS when it is in charged state. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  This implementation follows the state transition as specified in the file header.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
SystemBattery::monitorChargedState_( void )
{
    CALL_TRACE("SystemBattery::monitorChargedState_(void)");

    if ( newState_  || notificationRequired_ )
    {
    // $[TI1]
        currentBpsQuality_ = GOOD;
        pAlarmCallBack_( BdAlarmId::BDALARM_NOT_INTERNAL_BATTERY_POWER, FALSE );
        pVentStatusCallBack_( EventData::ON_BATTERY_POWER, EventData::CANCEL, EventData::NULL_EVENT_PROMPT );
        pVentStatusCallBack_( EventData::BATTERY_BACKUP_READY, EventData::ACTIVE, EventData::NULL_EVENT_PROMPT );
        newState_ = FALSE;
        notificationRequired_ = FALSE;
    }
    // $[TI2]

    if ( transition2_() )
    {
    // $[TI3]
        bpsStatus_ = CHARGING;
    }
    else if ( transition4_() )
    {
    // $[TI4]
        bpsStatus_ = ON_BATTERY_POWER;
    }
    else if ( transition6_() || transition5_() )
    {
    // $[TI5]
        bpsStatus_ = BPS_FAULT;
    }
    // $[TI6]
}


//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: monitorOnBatteryPowerState_ 
//
//@ Interface-Description
//
//  This method takes no parameters and returns no value.
//  This method monitors BPS when it is in on battery power.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This implementation follows the state transition as specified in the file header.
//  If the bps voltage is less than TWO_MINUTE_RUN_TIME_VOLTAGE, the bps
//  quality is considerd low.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

void
SystemBattery::monitorOnBatteryPowerState_( void )
{
    CALL_TRACE("SystemBattery::monitorOnBatteryPowerState_(void)");

    Real32 vBatt = calculateAvgVbatt_(); 

    if ( newState_  || notificationRequired_ )
    {
    // $[TI1]
        pAlarmCallBack_( BdAlarmId::BDALARM_INTERNAL_BATTERY_POWER, FALSE );
        pVentStatusCallBack_( EventData::ON_BATTERY_POWER, EventData::ACTIVE, EventData::NULL_EVENT_PROMPT );
        pVentStatusCallBack_( EventData::BATTERY_BACKUP_READY, EventData::ACTIVE, EventData::NULL_EVENT_PROMPT );
        newState_ = FALSE;
        notificationRequired_ = FALSE;
    }
    // $[TI2]

    if ( transition5_() )
    {
    // $[TI3]
        bpsStatus_ = BPS_FAULT;
    }
    else if ( transition7_() )
    {
    // $[TI4]
        bpsStatus_ = CHARGING;
    }
    else if ( transition8_() )
    {
    // $[TI5]
        bpsStatus_ =  CHARGED;
    }
    // $[TI6]

    if ( vBatt < TWO_MINUTE_RUN_TIME_VOLTAGE && currentBpsQuality_ != LOW)
    {
    // $[TI7]
        // less than 2 minutes of operational time left
        currentBpsQuality_ = LOW;
        Background::LogDiagnosticCodeUtil(::BK_BPS_EVENT_INFO, LESS_THAN_TWO_MINUTES_EVENT);
    }
    // $[TI8]
}

//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: monitorBpsFaultState_ 
//
//@ Interface-Description
//  The method accepts no arguments and does not return a value. 
//  This method sets the status data members to BPS fault condition.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This implementation follows the state transition as specified in the file header.
//  Note that since this is a terminal state, currentBpsQuality_ is used
//  to qualify initial notification rather than newState_.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------
void
SystemBattery::monitorBpsFaultState_( void )
{
    CALL_TRACE("SystemBattery::monitorBpsFaultState_(void)");

    if ( currentBpsQuality_ != INOPERATIVE || notificationRequired_ )
    {
    // $[TI1]
        currentBpsQuality_ = INOPERATIVE;
        pAlarmCallBack_( BdAlarmId::BDALARM_NON_FUNCTIONAL_BATTERY, FALSE );
        notificationRequired_ = FALSE;
    }
    // $[TI2]

    // invoke check for transition4. Do not check the return value
    transition4_();
    if ( onBattery_ && !previouslyOnBattery_ )
    {
    // $[TI3]
        pAlarmCallBack_( BdAlarmId::BDALARM_INTERNAL_BATTERY_POWER, FALSE );
        pVentStatusCallBack_( EventData::ON_BATTERY_POWER, EventData::ACTIVE, EventData::NULL_EVENT_PROMPT );
        previouslyOnBattery_ = TRUE;
    }
    else if ( !onBattery_ && previouslyOnBattery_ )
    {
    // $[TI4]
        pAlarmCallBack_( BdAlarmId::BDALARM_NOT_INTERNAL_BATTERY_POWER, FALSE );
        pVentStatusCallBack_( EventData::ON_BATTERY_POWER, EventData::CANCEL, EventData::NULL_EVENT_PROMPT );
        previouslyOnBattery_ = FALSE;
    }
    // $[TI5]
}


//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: monitorNotInstalledState_ 
//
//@ Interface-Description
//  The method accepts no arguments and does not return a value. 
//  This method sets the status data members to BPS fault condition.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method set bpsFaultDeclared_ to TRUE, currentBpsQuality_ to
//  INOPERATIVE, and lastly, bpsStatus_ to BPS_FAULT.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------
void
SystemBattery::monitorNotInstalledState_( void )
{
    CALL_TRACE("SystemBattery::monitorNotInstalledState_(void)");

    if ( newState_  || notificationRequired_ )
    {
    // $[TI1]
        currentBpsQuality_ = NONE;
        // initialize all status data members
        newState_ = FALSE;
        notificationRequired_ = FALSE;
    }
    // $[TI2]
}

//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: getOldestVoltageInBuffer_ 
//
//@ Interface-Description
//  The method accepts no arguments and returns a Real32 voltage. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method returns the oldest voltage sample in buffer, if
//  the oldest voltage is 0, the first voltage of buffer will be 
//  returned. 
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------
Real32 
SystemBattery::getOldestVoltageInBuffer_( void )
{
    Real32 oldestVoltage = voltageBuffer_[voltageBufferIndex_];

    // data in buffer less than 1 minute
    if ( oldestVoltage == 0 )
    {
        // use the very beginning one as the oldest
        oldestVoltage = voltageBuffer_[0];
    }

    return oldestVoltage;
}

//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: getLatestVoltageInBuffer_ 
//
//@ Interface-Description
//  The method accepts no arguments and returns a Real32 voltage. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method returns the latest voltage sample in buffer, if there
//  is no sample has been filled to buffer, 0 will be returned.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------
Real32 
SystemBattery::getLatestVoltageInBuffer_( void )
{
    int latestIndex = (voltageBufferIndex_ 
                        + VOLTAGE_BUFFER_SIZE - 1) % VOLTAGE_BUFFER_SIZE;

    return voltageBuffer_[latestIndex];
}

//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: getRecentVoltageChange_ 
//
//@ Interface-Description
//  The method accepts no arguments and returns a Real32 voltage. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method returns the differences of the most recent 2 voltage 
//  samples. 
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------
Real32
SystemBattery::getRecentVoltageChange_( void )
{
    int latestIndex = (voltageBufferIndex_ 
                        + VOLTAGE_BUFFER_SIZE - 1) % VOLTAGE_BUFFER_SIZE;
    int indexBeforeLatest = (voltageBufferIndex_ 
                        + VOLTAGE_BUFFER_SIZE - 2) % VOLTAGE_BUFFER_SIZE;
    
    return voltageBuffer_[latestIndex] - voltageBuffer_[indexBeforeLatest];
}

//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: saveVoltageToBuffer_ 
//
//@ Interface-Description
//  The method accepts a Real32 voltage argument and does not return a value. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Voltage buffer is a fixed size buffer, if the buffer is already full
//  the oldest voltage in buffer will be overwrited.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------
void
SystemBattery::saveVoltageToBuffer_( Real32 volt )
{
    voltageBuffer_[voltageBufferIndex_] = volt;

    voltageBufferIndex_ ++;

    // move index to begining if exceed the maximum
    voltageBufferIndex_ %= VOLTAGE_BUFFER_SIZE;
}

//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: transition1_ 
//
//@ Interface-Description
//
//  The method accepts no arguments and returns a Boolean. 
//  This method determines if its transition has been fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A transition is implemented by evaluating a compound condition. Once
//  the condition evaluates to TRUE for a number of times specified for this
//  transition, the transition is considered TRUE and the method returns a TRUE
//  Boolean value. 
//  When the transition is fired, all counters are reset and the newState_ variable
//  is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

Boolean
SystemBattery::transition1_( void )
{
    CALL_TRACE("SystemBattery::transition1_( void )");
#if 0//E600
    Boolean returnVal = FALSE; 
    Real32 bpsModelVoltage = RSystemBatteryModel.getValue();

    if ( ( bpsModelVoltage < MIN_BPS_MODEL_1_VOLTAGE ) ||
         ( bpsModelVoltage > MAX_BPS_MODEL_1_VOLTAGE ) )
    {
    // $[TI1]
        transition_1_Counter_++;
    }
    // $[TI2]

    if ( transition_1_Counter_  >= MAX_TRANS1_COUNT )
    {
    // $[TI3]
        resetAllTransCounters_();
        newState_ = TRUE;
        returnVal = TRUE;
    }
    // $[TI4]

    return( returnVal );
#endif
	return FALSE;
}


//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: transition2_ 
//
//@ Interface-Description
//
//  The method accepts no arguments and returns a Boolean. 
//  This method determines if its transition has been fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A transition is implemented by evaluating a compound condition. Once
//  the condition evaluates to TRUE for a number of times specified for this
//  transition, the transition is considered TRUE and the method returns a TRUE
//  Boolean value. 
//  When the transition is fired, all counters are reset and the newState_ variable
//  is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

Boolean
SystemBattery::transition2_( void )
{
        CALL_TRACE("SystemBattery::transition2_( void )");

    Boolean returnVal = FALSE; 
    Real32 one_mV = 0.001;
    Real32 latestVoltage = getLatestVoltageInBuffer_();
    
    if ( PowerSource::AC_POWER == RPowerSource.getSource() 
        && (latestVoltage <  BATTERY_CHARGED_VOLTAGE)
        && (latestVoltage - getOldestVoltageInBuffer_() > one_mV) )
    {
        transition_2_Counter_++;
    }
    // $[TI2]

    if ( transition_2_Counter_ >= MAX_TRANS2_COUNT )
    {
    // $[TI3]
        resetAllTransCounters_();
        newState_ = TRUE;
        returnVal = TRUE;
    }
    // $[TI4]

    return( returnVal );
}
         
//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: transition3_ 
//
//@ Interface-Description
//
//  The method accepts no arguments and returns a Boolean. 
//  This method determines if its transition has been fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A transition is implemented by evaluating a compound condition. Once
//  the condition evaluates to TRUE for a number of times specified for this
//  transition, the transition is considered TRUE and the method returns a TRUE
//  Boolean value. 
//  When the transition is fired, all counters are reset and the newState_ variable
//  is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

Boolean
SystemBattery::transition3_( void )
{
    CALL_TRACE("SystemBattery::transition3_( void )");

    Boolean returnVal = FALSE; 

    // We have already reached to the CHARGED voltage
    if( getLatestVoltageInBuffer_() >= BATTERY_CHARGED_VOLTAGE )
    {
        transition_3_Counter_++;
    }
    
    // $[TI2]

    if ( transition_3_Counter_ >= MAX_TRANS3_COUNT )
    {
    // $[TI3]
        resetAllTransCounters_();
        newState_ = TRUE;
        returnVal = TRUE;
    }
    // $[TI4]

    return( returnVal );
}
         

//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: transition4_ 
//
//@ Interface-Description
//
//  The method accepts no arguments and returns a Boolean. 
//  This method determines if its transition has been fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A transition is implemented by evaluating a compound condition. Once
//  the condition evaluates to TRUE for a number of times specified for this
//  transition, the transition is considered TRUE and the method returns a TRUE
//  Boolean value. 
//  When the transition is fired, all counters are reset and the newState_ variable
//  is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

Boolean
SystemBattery::transition4_( void )
{
    CALL_TRACE("SystemBattery::transition4_( void )");

    Boolean returnVal = FALSE; 

    Real32 hundred_mV = 0.1;

    if ( PowerSource::AC_POWER != RPowerSource.getSource() 
        && getRecentVoltageChange_() < -hundred_mV )
    {
        transition_4_Counter_++;
        onBattery_ = TRUE;
    }
    else
    {
        onBattery_ = FALSE;
    }

    if ( transition_4_Counter_ >= MAX_TRANS4_COUNT )
    {
    // $[TI3]
        resetAllTransCounters_();
        newState_ = TRUE;
        returnVal = TRUE;
        Background::LogDiagnosticCodeUtil(::BK_BPS_EVENT_INFO, ON_BATTERY_POWER_EVENT);
    }
    // $[TI4]

    return( returnVal );
}


//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: transition5_ 
//
//@ Interface-Description
//
//  The method accepts no arguments and returns a Boolean. 
//  This method determines if its transition has been fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A transition is implemented by evaluating a compound condition. Once
//  the condition evaluates to TRUE for a number of times specified for this
//  transition, the transition is considered TRUE and the method returns a TRUE
//  Boolean value. 
//  When the transition is fired, all counters are reset and the newState_ variable
//  is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

Boolean
SystemBattery::transition5_( void )
{
        CALL_TRACE("SystemBattery::transition5_( void )");

    Boolean returnVal = FALSE; 
#if 0//E600
    if ( ( bpsCharging_.getState() == BinaryIndicator::OFF &&
           bpsCharged_.getState() == BinaryIndicator::OFF ) ||
         ( bpsCharging_.getState() == BinaryIndicator::ON &&
           bpsCharged_.getState() == BinaryIndicator::ON &&
           calculateAvgIbatt_() < DISCONNECT_CURRENT_THRESHOLD &&
           currentBpsQuality_ != LOW ) ) 
    {
    // $[TI1]
         transition_5_Counter_++;
    }
    // $[TI2]

    if ( transition_5_Counter_ >= MAX_TRANS5_COUNT )
    {
    // $[TI3]
        resetAllTransCounters_();
        newState_ = TRUE;
        returnVal = TRUE;
        Background::LogDiagnosticCodeUtil(::BK_BPS_EVENT_INFO, BAD_LED_EVENT);
    }
    // $[TI4]
#endif
    return( returnVal );
}

//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: transition6_ 
//
//@ Interface-Description
//
//  The method accepts no arguments and returns a Boolean. 
//  This method determines if its transition has been fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A transition is implemented by evaluating a compound condition. Once
//  the condition evaluates to TRUE for a number of times specified for this
//  transition, the transition is considered TRUE and the method returns a TRUE
//  Boolean value. 
//  When the transition is fired, all counters are reset and the newState_ variable
//  is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

Boolean
SystemBattery::transition6_( void )
{
    CALL_TRACE("SystemBattery::transition6_( void )");

    Boolean returnVal = FALSE;

	Real32 voltage = calculateAvgVbatt_();

    if ( voltage >= OPEN_CIRCUIT_VOLTAGE )
    {
    // $[TI1]
         transition_6_Counter_++;
    }
    // $[TI2]

    if ( transition_6_Counter_ >= MAX_TRANS6_COUNT )
    {
    // $[TI3]
        resetAllTransCounters_();
        newState_ = TRUE;
        returnVal = TRUE;
        Background::LogDiagnosticCodeUtil(::BK_BPS_EVENT_INFO, OPEN_CIRCUIT_EVENT);
    }
    // $[TI4]

    return( returnVal );
}


//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: transition7_ 
//
//@ Interface-Description
//
//  The method accepts no arguments and returns a Boolean. 
//  This method determines if its transition has been fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A transition is implemented by evaluating a compound condition. Once
//  the condition evaluates to TRUE for a number of times specified for this
//  transition, the transition is considered TRUE and the method returns a TRUE
//  Boolean value. 
//  When the transition is fired, all counters are reset and the newState_ variable
//  is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

Boolean
SystemBattery::transition7_( void )
{
    CALL_TRACE("SystemBattery::transition7_( void )");

    Boolean returnVal = FALSE; 
    Real32 hundred_mV = 0.1;

    if ( PowerSource::AC_POWER == RPowerSource.getSource()
        && getRecentVoltageChange_() > hundred_mV )
    {
    // $[TI1]
         transition_7_Counter_++;
    }
    // $[TI2]

    if ( transition_7_Counter_ >= MAX_TRANS7_COUNT )
    {
    // $[TI3]
        resetAllTransCounters_();
        newState_ = TRUE;
        returnVal = TRUE;
        Background::LogDiagnosticCodeUtil(::BK_BPS_EVENT_INFO, ON_BATTERY_POWER_EVENT_RESET);
    }
    // $[TI4]

    return( returnVal );
}

//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: transition8_ 
//
//@ Interface-Description
//
//  The method accepts no arguments and returns a Boolean. 
//  This method determines if its transition has been fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A transition is implemented by evaluating a compound condition. Once
//  the condition evaluates to TRUE for a number of times specified for this
//  transition, the transition is considered TRUE and the method returns a TRUE
//  Boolean value. 
//  When the transition is fired, all counters are reset and the newState_ variable
//  is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

Boolean
SystemBattery::transition8_( void )
{
    CALL_TRACE("SystemBattery::transition8_( void )");

    // same as transition3
    return transition3_();
}


//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: transition9_ 
//
//@ Interface-Description
//
//  The method accepts no arguments and returns a Boolean. 
//  This method determines if its transition has been fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  A transition is implemented by evaluating a compound condition. Once
//  the condition evaluates to TRUE for a number of times specified for this
//  transition, the transition is considered TRUE and the method returns a TRUE
//  Boolean value. 
//  When the transition is fired, all counters are reset and the newState_ variable
//  is set to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

Boolean
SystemBattery::transition9_( void )
{
    CALL_TRACE("SystemBattery::transition9_( void )");

    Boolean returnVal = FALSE; 

	// $[05175] Maximum charge time allowed for an inoperational battery before declaring alarm
    if ( currentBpsStatusTimer_ > MAX_BATTERY_CHARGE_HOURS * ONE_SYSTEM_BATTERY_HOUR )
    {
    // $[TI1]
         transition_9_Counter_++;
    }
    // $[TI2]

    if ( transition_9_Counter_ >= MAX_TRANS9_COUNT )
    {
    // $[TI3]
        resetAllTransCounters_();
        newState_ = TRUE;
        returnVal = TRUE;
    }
    // $[TI4]

    return( returnVal );
}

    
//=========================== M E T H O D  D E S C R I P T I O N ======
//@ Method: resetAllTransCounters_
//
//@ Interface-Description
//
//  The method accepts no arguments and returns no value. 
//  It resets all transition flags to FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  All transition counters are set to 0.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//---------------------------------------------------------------------

void
SystemBattery::resetAllTransCounters_( void )
{
        CALL_TRACE("SystemBattery::resetAllTransCounters_( void )");

    // $[TI1]
    transition_1_Counter_ = 0;
    transition_2_Counter_ = 0;
    transition_3_Counter_ = 0;
    transition_4_Counter_ = 0;
    transition_5_Counter_ = 0;
    transition_6_Counter_ = 0;
    transition_7_Counter_ = 0;
    transition_8_Counter_ = 0;
    transition_9_Counter_ = 0;
}

Uint8
SystemBattery::getStateOfCharge( void )
{
    Real32 volt0 = getLatestVoltageInBuffer_();
    Real32 stateOfCharge = 100.0F / VOLT_TABLE_SIZE;

    if ( volt0 != 0 )
    {
        const Real32* pVoltTable = (PowerSource::AC_POWER == 
            RPowerSource.getSource()) ? CHARGING_VOLT_TABLE : DISCHARGING_VOLT_TABLE;

        Uint32 index2 = 0;

        for ( index2 = 0; index2 < VOLT_TABLE_SIZE; ++ index2 )
        {
            if ( pVoltTable[index2] > volt0 )
            {
                break;
            }
        }

        Uint32 index = index2 - 1;
        index = (index < 0) ? 0 : index;
        index2 = (index > VOLT_TABLE_SIZE -1) ? VOLT_TABLE_SIZE -1 : index2;

        if ( index == index2 )
        {
            stateOfCharge *= (index + 1);
        }
        else
        {
            Real32 volt1 = pVoltTable[index];
            Real32 volt2 = pVoltTable[index2];

            stateOfCharge *= index2;
            stateOfCharge += (volt0 - volt1) / (volt2 - volt1) 
                                             * (100.0F / VOLT_TABLE_SIZE);
        }
   }

    return static_cast<Uint8>(stateOfCharge+0.5);
}
