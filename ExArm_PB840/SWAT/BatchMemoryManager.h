//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
/// @file BatchMemoryManager.h
///
/// @brief
/// Class definition for BatchMemoryManager


#ifndef BATCHMEMORYMANAGER_H
#define BATCHMEMORYMANAGER_H

namespace swat{

    /// @class BatchMemoryManager
    ///
    /// BatchMemoryManager is a allocator/deallocator
    /// <ol>
    /// <li> Garentees O(1) lockfree deallocation.
    /// <li> Limits number of allocations allowed before an allocation will fail.
    /// <li> Recycles previous allocations in order to limit the number of re-allocations.
    ///    <ul>
    ///       <li>Always favor recyling memory that meets the size requirments.
    ///       <li>If size requirments are not meet, recycle the largest free slot by
    ///           increasing size.
    ///    </ul>
    /// </ol>
    ///
    /// BatchMemoryManager was written to address the issue were a high priority thread
    /// will need to free memory deterministacly without locking.
    ///
    /// There are no garentees on the allocation of new memory when it comes to locking
    /// of blocking.
    ///
    class BatchMemoryManager{

        private:

            struct SlotMemoryHeader
            {
                unsigned int m_iSlotId;
                bool         m_bfreeSlot;
                unsigned int m_iBatchLength;
            };

            SlotMemoryHeader **m_pSlotMemoryHeaderArray;
            int      m_iSlotLength;
            unsigned int      m_iSlotMask;
            unsigned int      m_iSlotMin;

        public:
            /// @brief Constructor
            /// @param iSlotLength Max number of allocations that are allowed.
            /// @param iSlotMask Mask to optimize memory allignment allowing grater reuse.
            /// @param iSlotMin Mask to optimize possibility of memory reuse.
            BatchMemoryManager(unsigned int iSlotLength,unsigned int iSlotMask=0x7,unsigned int iSlotMin=1);

            /// @brief Destructor
            ~BatchMemoryManager();

            /// @brief Memory allocator
            /// @param iLength Number of bytes to allocate.
            /// @return A pointer to memory allocated.
            void * allocate(unsigned int iLength);

            /// @brief Memory deallocator
            /// @param pIn Pointer to memory allocated by BatchMemoryManager::allocate
            static void free(void *pIn);


    };

}
#endif
