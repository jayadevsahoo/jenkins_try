//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MiscStrs - Language-independent repository for
// miscellaneous strings displayed on the ventilator screens.
//---------------------------------------------------------------------
//@ Interface-Description
// This class exists for encapsulation purposes only.  All language dependent
// strings that can be displayed on the ventilator (except for prompt strings,
// stored in the PromptStrs class) are static public members of this class and
// can be accessed globally.
//---------------------------------------------------------------------
//@ Rationale
// Convenient encapsulator for strings and provides for language independence.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//
// $[00638] The language required to be supported at initial ...
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MiscStrs.ccv   25.0.4.0   19 Nov 2013 14:08:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By:   Boris Kuznetsov      Date: 03-May-2003       DR Number: 6036
//  Project:   Russian
//  Description:
//      Addded include file for PolishMiscStrs
//
//  Revision: 010   By:   S. Peters      Date: 16-Oct-2002       DR Number: 6029
//  Project:   Polish
//  Description:
//      Addded include file for PolishMiscStrs
//
//  Revision: 009   By:   sah      Date: 31-Aug-99       DR Number:   5517
//  Project:   ATC
//  Description:
//      Removed obsoleted requirement.
//
//  Revision: 008  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//   Revision 007   By:  hhd	Date: 09-Apr-1998        DR Number:  
//   Project:   Sigma   (R8027)
//   Description:
//		Included JapaneseMiscStrs.in for Japanese version.	
//
//   Revision 006   By:  sah    Date: 15-Jan-1998        DR Number:  5004
//   Project:   Sigma   (R8027)
//   Description:
//       Removed include of now-obsoleted 'Symbols.hh'.

//  Revision: 005  By:  yyy    Date:  27-MAY-97    DR Number: 2042
//    Project:  Sigma (R8027)
//    Description:
//      Removed SV_SPEAKER_DISCONNECTED and SV_RESONANCE_TEST, changed
//		SV_SELF_TEST_FAILED to SV_SAAS_TEST_FAILED.
//
//  Revision: 004  By:  hhd    Date:  13-MAY-97    DR Number: 2085
//    Project:  Sigma (R8027)
//    Description:
//      Added DLOG_NMI_SHORT and DLOG_EXCEPTION_VECTOR.
//
//  Revision: 003  By: yyy      Date: 07-May-1997  DR Number: 2059
//    Project:  Sigma (R8027)
//    Description:
//      Removed unused message strings.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2004
//    Project:  Sigma (R8027)
//    Description:
//      Changed BK_COMPR_CHECKSUM to BK_COMPR_BAD_DATA.  Added BK_COMPR_UPDATE_SN,
//		and BK_COMPR_UPDATE_PM_HRS error strings.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================
#define DEFINE_STRS
#include "MiscStrs.hh"
#include "Sigma.hh"

//@ Usage-Classes
#ifdef SIGMA_ASIA
	#include "AsiaMiscStrs.in"
#elif SIGMA_EUROPE
	#include "EuropeMiscStrs.in"
#elif SIGMA_CHINESE
	#include "ChineseMiscStrs.in"
#elif SIGMA_ENGLISH
	#include "EnglishMiscStrs.in"
#elif SIGMA_FRENCH
	#include "FrenchMiscStrs.in"
#elif SIGMA_GERMAN
	#include "GermanMiscStrs.in"
#elif SIGMA_ITALIAN
	#include "ItalianMiscStrs.in"
#elif SIGMA_JAPANESE
	#include "JapaneseMiscStrs.in"
#elif SIGMA_POLISH
	#include "PolishMiscStrs.in"
#elif SIGMA_PORTUGUESE
	#include "PortugueseMiscStrs.in"
#elif SIGMA_RUSSIAN
	#include "RussianMiscStrs.in"
#elif SIGMA_SPANISH
	#include "SpanishMiscStrs.in"
#endif

//@ End-Usage

