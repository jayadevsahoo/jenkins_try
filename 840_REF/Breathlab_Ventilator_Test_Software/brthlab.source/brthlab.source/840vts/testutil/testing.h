/*****************************************************************************/
/*   Module Name:     Testing.h                                          
/*   Purpose:   840 Ventilator Final Test                               
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*             <75359-93>   
/*      
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 Removed the Metabolic Port Test     	Rhomere Jimenez   05 Oct,2007  
/*
/*	 The Error codes in ErrorMessage 
/* 	 functions are changed.    				Dwarkanath(CGS)   12 Dec,2000  
/*
/*   Removed obsolete symbols               Dave Richardson   21 Nov, 2002
/*
/*   Removed code that generates storage,
/*    changed to externals
/*
/*   Revised prototypes to match parameters
/*    used in external functions
/*
/*   $Revision::   1.2         $                                          
/*
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/testutil/testing.h-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:46   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   21 Nov 2002 10:23:00   Richardson
 * Updated per SCR # 29

/*****************************************************************************/


#ifndef _TESTING_H
#define _TESTING_H

/*                     
 * Run Modes for Tests 
 */                     

#define MIN_RUN_MODE       				 0
#define RUN_MODE_NORMAL     			 0
#define RUN_MODE_FORCE_PASS 			 1
#define RUN_MODE_FORCE_FAIL 			 2
#define RUN_MODE_SKIP       			 4
#define MAX_RUN_MODE        			 3


#define NO_TEST_ERROR                    2
#define UNABLE_TO_OPEN_FILE_ERROR        18
#define WRITING_TO_FILE_ERROR            21
#define COM_PTS_ERROR				     38
#define COM_840_ERROR				     39
#define PRESSURE_TEST_FAILURE_ERROR      40
#define ATM_PRESS_TEST_FAILURE_ERROR     41
#define GAS_FLOW_TEST_FAILURE_ERROR		 42

/*
 * Comment  the following macro when the test skipped  result is not required in the report
 */


#define SKIP_IN_REPORT 1





/*
 *  System Panel constants
 */
#define STAT_STOPPED 0
#define STAT_RUNNING 1
#define STAT_LOOPING 2

/*
 * Reports Constants
 */

#define RPT_ALL 0
#define RPT_PASSONLY 1
#define RPT_FAILONLY 2
#define RPT_NONE 3

#define PRINT_TO_PRN      0
#define PRINT_TO_FILE     1 
#define FULL_REPORT       0
#define SUMMARY_REPORT    1

/*
 *Definition of different test result states 
 */
#define TEST_CLEAR 0
#define TEST_RUNNING 1
#define TEST_PASS 2
#define TEST_FAIL 3
#define TEST_SKIP 4
#define TEST_LOOPING 5
#define TEST_ERROR 6
#define TEST_ABORT 7

/*                       
/* Spec Values for Tests 
 */
#define SPEC_LOG    11      /* Assume PASS, but really just log the data */
#define SPEC_NONE   12      /* Assume PASS, but really do nothing */

/*
 *Definition for button enable/disable
 */
 
#define ENABLE 0
#define DISABLE 1

/*
 *TestNumber
 */
 
#define TEST_1 0
#define TEST_2 1
#define TEST_3 2
#define TEST_4 3
#define TEST_5 4
#define TEST_6 5
#define TEST_7 6
#define TEST_8 7
#define TEST_9 8
#define TEST_10 9
#define TEST_11 10
#define TEST_12 11


#define DEFAULT_DIR "c:\\brthlab\\840vts"  
#undef  FALSE
#undef  TRUE 
#define TEST_UUT 0
#define NUM_OF_LANG 6
#define NUM_OF_TESTS 12
#define MAX_TESTS	11
#define MIN_TESTS	 0

/*
 * Enumerated Variables
 */
typedef enum {FALSE, TRUE}Boolean;
typedef enum {FAIL, PASS, SKIP, AGAIN, ABORT}Status;
typedef struct Data_Rec {
    Status  result;         /* Whether test passed                      */
    char    *outBuffer;     /* For output messages from the test        */
    double  testLimit1;
    double  testLimit2;
    int 	loopCount;
    short	sRunMode;        /* How to Execute                           */
	char    *outBuff_localized;
	
} tTestData;

/*
 * Extern  Variables
 */
extern tTestData tDataRec[NUM_OF_TESTS];
extern char *TestName[NUM_OF_LANG][NUM_OF_TESTS]; 
extern int  gErrorFlag;

/*
 * Global variables
 */
extern int iCurrentTest;
extern int SST; // Flag which controls if the "Run SST" prompt is displayed
extern char *gDatabaseBuffer;

/*
 * Test functions
 */

void InitDataRecBuffer(void);
void CurrentTest(int index);
void InitTestBuffers(tTestData *dataRec);
void RunSingleTest(void);
void RunTestUUT(void);
void LoopTest(void);
int  WriteRunDB(char *serialNum);
int  WriteStepDB(void);
int  SetupDB(void);
int  RunSequence(int *failStop, char *serialNum, char *revNum); 
void GetReportFileName(void);
void FreeTestBuffers(tTestData *dataRec);
int  RemoveRenameOldFiles(void); // Modified the statement due to Critical Observation.
void CreateNewDBFiles(void); // Modified the statement due to Critical Observation.
int  WriteRunDB(char *serialNum);
void ErrorMessage(int errorcode, ...);


/*
 * Report functions
 */
void ChangeReportScreenOption(int newVal);
void ChangeReportFileOption(int newVal);
int  WriteTestResult(int testIndex);
char *StrDuplicate(char *s);
int  WriteStopReason( int abortFlag, int failStop);
int  WriteSeqHeader(char *serialNum, char *uutRevNum);  
int  InitTestReport(void);
int  StatusReportString(int sts);
int  ReportStringToFile(void);

 
#endif



