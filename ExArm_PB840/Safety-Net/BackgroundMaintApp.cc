#include "stdafx.h"

//=============================================================================
//  This is a proprietary work to which Puritan-Bennett corporation of
//  California claims exclusive right.  No part of this work may be used,
//  disclosed, reproduced, stored in an information retrieval system, or
//  transmitted by any means, electronic, mechanical, photocopying,
//  recording, or otherwise without the prior written permission of
//  Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=============================================================================

//============================ C L A S S   D E S C R I P T I O N ============
//@ Class: BackgroundMaintApp - Background Maintenance applications
//-----------------------------------------------------------------------------
//@ Interface-Description
//  This class contains the functionality for performing the background
//  maintenance operations.  Contained in this class is the application
//  Task(), a standard do-forever task that is spawned during System
//  Initialization.  The task runs continously, but put itself to sleep
//  for a fixed amount of time after a sequence of maintenance task
//  is completed.
//
//  A BackgroundMaintApp exists only on the BD CPU.
//-----------------------------------------------------------------------------
//@ Rationale
//  This class contains the functionality for performing the background
//  maintenance operations.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  This class provides the following functionalities:
//  1. Updates the System Operational time in NOVRAM and DataKey for BD CPU.
//  2. Updates the compressor's elapsed time for the BD CPU.
//-----------------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//-----------------------------------------------------------------------------
//@ Restrictions
//  None
//-----------------------------------------------------------------------------
//@ Invariants
//  None
//-----------------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundMaintApp.ccv   25.0.4.0   19 Nov 2013 14:20:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By: gdc    Date:  25-Feb-2009   SCR Number: 6019
//    Project:  S840
//    Description:
//		Modified to increase data key update reliability. Requirment annotation
//		only.
//
//  Revision: 009  By: hhd    Date:  18-Aug-1999    DR Number: DCS 5513
//    Project:  ATC (840)
//    Description:
//		Modified so the following events will be logged into NovRam instead of
//		sent to the BackgroundFaultHanlder Queue:
//			BK_COMPR_ELAPSED_TIMER,
//			BK_COMPR_BAD_DATA and
//			BK_DATAKEY_UPDATE_FAIL
//
//  Revision: 008  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007    By: iv   Date: 11-Mar-1998    DR Number: DCS 5041
//    Project: Sigma (840)
//    Description:
//       Added error code for compressor time update.
//
//  Revision: 006  By: syw   Date:  25-Nov-1997    DR Number: DCS 2654
//  	Project:  Sigma (840)
//		Description:
//			Move TaskMonitor::Report() to outside of the semaphore check loop so it
//			can report back independent of the semaphore status.  Removed
//			TIMING_TEST CODE.
//
//  Revision: 005  By: iv    Date:  14-Aug-1997    DR Number: DCS 2305
//  	Project:  Sigma (840)
//		Description:
//			Activate the maintenance task on the GUI CPU.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003   By: syw    Date: 07-May-1997   DR Number: DCS 2004
//    Project:  Sigma (R8027)
//    Description:
//		Log BK_COMPR_BAD_DATA background error if Compressor::UpdateTotalElapsedTime
//		returns BAD_CHECKSUM or	UPDATE_FAILED.
//
//  Revision: 002    By: iv    Date: 22-Apr-1997    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Check if data key is installed before reading its content.
//        This change is required as part of the ventObjects code review.
//
//  Revision: 001    By: by    Date: 11-Nov-1996    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//=============================================================================

#include "BackgroundMaintApp.hh"

//@ Usage-Classes
#include "Background.hh"

#if defined( SIGMA_BD_CPU )
#include "Compressor.hh"
// E600 BDIO #include "CompressorTimer.hh"
#include "DataKey.hh"
#endif // defined( SIGMA_BD_CPU )

#include "MsgTimer.hh"
#include "NovRamManager.hh"
#include "Post.hh"
#include "StackCheck.hh"
#include "Task.hh"
#include "TaskMonitor.hh"
#include "VentObjectRefs.hh"
#include "TaskControlAgent.hh"

//@ Constant: NUM_OF_MINUTES_IN_HOUR
//  number of minutes in a hour
static const Uint8 NUM_OF_MINUTES_IN_HOUR = 60;

//@ Constant: BK_MAINTENACE_CYCLE_TIME
//  Background Maintenance Task cycle time 60 second
static const Uint8 BK_MAINTENACE_CYCLE_TIME = 60;

//////////////////////////////////////////////////////////////////////////////
//    Initialize all status data members
//////////////////////////////////////////////////////////////////////////////
#if defined( SIGMA_BD_CPU )
Boolean BackgroundMaintApp::UpdateCompOperationalTime_ = TRUE;
Uint32 BackgroundMaintApp::PrevCompElapsedTimerImage_ = 0;
Boolean BackgroundMaintApp::IsTaskRunning_ = FALSE;
BackgroundMaintApp::BkMaintSemphCommands
BackgroundMaintApp::CurrentBkMaintSemphStatus_ = TASK_RESUME;
Boolean BackgroundMaintApp::WasTaskSuspended_ = FALSE;
#endif // defined( SIGMA_BD_CPU )

Uint32 BackgroundMaintApp::CurrentSystemOperationalHours_ = 0;
Uint32 BackgroundMaintApp::ElapsedMinSincePostUpdate_ = 0;


//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: Initialize
//
//@ Interface-Description
//  This method accepts no arguement and returns no value.
//  This method tests for a compressor timer running too fast.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  The compressor timer is reset to 0 on power-up.  If it reads more than
//  a few seconds when this method is executed, it is running too fast and
//  a Background compressor timer event is reported.
//-----------------------------------------------------------------------------
//@ PreCondition
//  None
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void
BackgroundMaintApp::Initialize( void )
{
#if defined( SIGMA_BD_CPU )
    if ( TRUE == RCompressor.isInstalled() )
    {
    // $[TI1]

#ifndef SIGMA_UNIT_TEST
        PrevCompElapsedTimerImage_ = 0; // E600 BDIO RCompressorTimer.readActiveCounter();
#endif // ifndef SIGMA_UNIT_TEST

        ////////////////////////////////////////////////////////////////
        // Verify that the compressor elapse timer is reset correctly.
        // The compressor timer is reset to 0 at system initialize.
        // If it reads more than 30 seconds by the time this initialize
        // method is executed, something is wrong with the compressor
        // timer.
        ////////////////////////////////////////////////////////////////
        if ( PrevCompElapsedTimerImage_ > BK_MAINTENACE_CYCLE_TIME / 2 )
        {
        // $[TI1.1]
            // set error code to indicate initialization problem.
            Uint16 errorCode =  0x8000;
            Background::LogDiagnosticCodeUtil( BK_COMPR_ELAPSED_TIMER, errorCode );
        }
        // $[TI1.2]

    }
    // $[TI2]
#endif // defined( SIGMA_BD_CPU )

}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method:  Task
//
//@ Interface-Description
//  This method accepts no arguement and returns no value.
//
//  This is the Background Maintance Task.  This task is responsible for
//  updating the vent head operational time to NOVRAM and the DataKey.
//  If a compressor is installed, its operational time is also updated.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  This is an independent task that runs on a periodic schedule.  This task
//  can be suspended by another task that shares the serial read/write port.
//  Once a request to suspend task is detected, if the task is not currently
//  running, it is suspended immediately until it recieves a resume command.
//  If the task is currently running, it will suspend itself after the current
//  cycle is completed.
//
//  $[06084] $[06182]
//-----------------------------------------------------------------------------
//@ PreCondition
//  None
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void
BackgroundMaintApp::Task( void )
{
	//TODO E600 this is temp, remove when implemented
	while(1)
	{
		RETAILMSG( 0, (_T("BackgroundMaintApp::Task\r\n")) );
		Task::Delay(1);
	}
	//TODO E600 enable when ready
/*
#ifndef SIGMA_UNIT_TEST
    while ( TRUE )  // forever
    {
#endif // ifndef SIGMA_UNIT_TEST

		TaskMonitor::Report();

#if defined( SIGMA_BD_CPU )
        if ( ( TASK_RESUME == CurrentBkMaintSemphStatus_ ) ||
             ( REQUEST_TO_SUSPEND == CurrentBkMaintSemphStatus_ ) )
        {
        // $[TI1]

            IsTaskRunning_ = TRUE;
            UpdateSystemOperationalTime_();
            UpdateCompressorOperationalTime_();
            IsTaskRunning_ = FALSE;

            if ( REQUEST_TO_SUSPEND == CurrentBkMaintSemphStatus_ )
            {
            // $[TI1.1]
                CurrentBkMaintSemphStatus_ = TASK_SUSPENDED;
                WasTaskSuspended_ = TRUE;
            }
            // $[TI1.2]
        } // if
        // $[TI2]
#else
		UpdateSystemOperationalTime_();
#endif //defined( SIGMA_BD_CPU )

#ifndef SIGMA_UNIT_TEST
		Task::Delay( BK_MAINTENACE_CYCLE_TIME, 0 );
#endif // ifndef SIGMA_UNIT_TEST

#ifndef SIGMA_UNIT_TEST
    } // while
#endif // ifndef SIGMA_UNIT_TEST
*/
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IssueNewSemphCommand
//
//@ Interface-Description
//
//  This method accepts a BkMaintSemphCommands enumeration and
//  returns no value.
//  This method implements a request to resume or suspend this
//  task.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This method implements a request to resume or suspend this
//  task.  When a quest to resume is received, this task is resumed
//  immediately.  When a quest to suspend is received and this
//  task in not currently running it is suspended immediately.
//  Otherwise, the client must wait until this current cycle is
//  completed before a task suspended status is granted.
//
//  A semaphore for this task is need to prevent interference with
//  Service-Mode (EST) tests.  When this task is running, it could
//  potentially corrupt test process and result in EST.  Hence this
//  task is suspended by Service-Mode when those tests are executed.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
#if defined( SIGMA_BD_CPU )
void
BackgroundMaintApp::IssueNewSemphCommand
(
    const BkMaintSemphCommands newCommand
)
{
    CLASS_PRE_CONDITION( TaskControlAgent::GetMyState() == STATE_SERVICE );

    switch ( newCommand )
    {
        case TASK_RESUME:
        // $[TI1]
            CurrentBkMaintSemphStatus_ = TASK_RESUME;
            break;

        case REQUEST_TO_SUSPEND:
        // $[TI2]

            if ( FALSE == IsTaskRunning_ )
            {
            // $[TI2.1]
                CurrentBkMaintSemphStatus_ = TASK_SUSPENDED;
                WasTaskSuspended_ = TRUE;
            }
            else
            {
            // $[TI2.2]
                CurrentBkMaintSemphStatus_ = REQUEST_TO_SUSPEND;
            }
            break;

        case TASK_SUSPENDED: // this command should not be issued externally
        default:
        // $[TI3]
            SAFE_CLASS_ASSERTION( FALSE );
            break;
    }

}
#endif //defined( SIGMA_BD_CPU )


//=============================================================================
//
//  Protected Methods...
//
//=============================================================================

//=============================================================================
//
//  Private Methods...
//
//=============================================================================

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: UpdateSystemOperationalTime_
//
//@ Interface-Description
//  This method accepts no argument and returns no value.
//
//  This method is responsible for updating the vent head operational time
//  to NOVRAM, POST NOVRAM, and the DataKey.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  If data key is installed, this utility calls a method in DataKey class, to
//  update the vent head operational time to the NOVRAM and Data Key Device.
//  Once an hour, it updates the POST NOVRAM with the current vent head
//  operational time.  Any failure to update the vent head operational time
//  detected is reported to Safety-Net subsystem immediately.
//-----------------------------------------------------------------------------
//@ PreCondition
//  None
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================

void
BackgroundMaintApp::UpdateSystemOperationalTime_( void )
{

#if defined( SIGMA_BD_CPU )
#ifndef SIGMA_UNIT_TEST

    ///////////////////////////////////////////////////////////////////////////
    // Method updateVentHeadOperationalTime() in DataKey class will update
    // the operational time stored in NOVRAM every time it is invoked and
    // update the operational time stored in the Data Key device once every
    // ten hours.
    ///////////////////////////////////////////////////////////////////////////
    if ( RDataKey.isInstalled() )
    {
    // $[TI5]
	    SigmaStatus updateOperationalHourStatus
    	    = RDataKey.updateVentHeadOperationalTime();

	    if ( FAILURE == updateOperationalHourStatus )
	    {
	    // $[TI1]
 	       Background::LogDiagnosticCodeUtil( BK_DATAKEY_UPDATE_FAIL );

	    }
	    // $[TI2]
    } // $[TI6]
	//  else do nothing ---
	//  $[LC08002] System operational time shall not accumulate in NOVRAM
	//             or in the data key when there is no data key device detected
	//             at power up.

#endif // ifndef SIGMA_UNIT_TEST
#endif //defined( SIGMA_BD_CPU )

    if ( ElapsedMinSincePostUpdate_ >= NUM_OF_MINUTES_IN_HOUR )
    {
    // $[TI3]

#if defined( SIGMA_BD_CPU )
        // retrieve total operational minutes from NovRam
        CurrentSystemOperationalHours_
            = NovRamManager::GetSystemOperationalMinutes()
            / NUM_OF_MINUTES_IN_HOUR;
#endif //defined( SIGMA_BD_CPU )

#if defined( SIGMA_GUI_CPU )
        CurrentSystemOperationalHours_ = NovRamManager::GetSystemOperationalHours();
#endif //defined( SIGMA_GUI_CPU )

        Post::SetOperationalTime( CurrentSystemOperationalHours_ );
        // reset elapsed time counter
        ElapsedMinSincePostUpdate_ = 0;
    }
    else
    {
    // $[TI4]
        // increment elapsed time counter
        ++ElapsedMinSincePostUpdate_;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: UpdateCompressorOperationalTime_
//
//@ Interface-Description
//  This method accepts no argument and returns no value.
//
//  This method is responsible for updating the compressor operational time.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  If a compressor is installed and the EST compressor test is not currently
//  running, this method updates the compressor operational time.
//  The compressor timer is tested for running too fast.
//  Any failure to update the vent head operational detected is reported
//  to Safety-Net subsystem immediately.
//-----------------------------------------------------------------------------
//@ PreCondition
//  None
//-----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================
#if defined( SIGMA_BD_CPU )
void
BackgroundMaintApp::UpdateCompressorOperationalTime_( void )
{
// E600 BDIO
#if 0
    CompressorEeprom::errorStatus compressorUpdateStatus;
    Uint32 currentCompressorElapsedTime = 0;

    ////////////////////////////////////////////////////////////////
    // UpdateCompOperationalTime_ is a flag toggled by
    // Service-Mode when EST performs compressor tests, the
    // compressor operational time updates must be disabled so
    // that it does not interfere with compressor test
    ////////////////////////////////////////////////////////////////
    if ( ( TRUE == RCompressor.isInstalled() ) &&
         ( TRUE == UpdateCompOperationalTime_ ) )
    {
    // $[TI1]
        compressorUpdateStatus = Compressor::UpdateTotalElapsedTime();

        if ( CompressorEeprom::BAD_CHECKSUM == compressorUpdateStatus ||
             CompressorEeprom::UPDATE_FAILED == compressorUpdateStatus)
        {
        // $[TI1.1]
            Background::LogDiagnosticCodeUtil( BK_COMPR_BAD_DATA, (Uint16)compressorUpdateStatus );
        }
        // $[TI1.2]

        currentCompressorElapsedTime = 0; // E600 BDIO RCompressorTimer.readActiveCounter();

        if ( TRUE == WasTaskSuspended_)
        {
        // $[TI1.3]
            PrevCompElapsedTimerImage_ = currentCompressorElapsedTime;
            WasTaskSuspended_ = FALSE;
        }
        // $[TI1.4]

        if ( ( currentCompressorElapsedTime - PrevCompElapsedTimerImage_ )
             >
             ( 2 * BK_MAINTENACE_CYCLE_TIME ) )
        {
        // $[TI1.5]
            Uint16 errorCode = (Uint16)(currentCompressorElapsedTime - PrevCompElapsedTimerImage_);
            // format code to place a 0 at the MSB to indicate elapsed time update error.
            errorCode &= 0x7FFF;
            Background::LogDiagnosticCodeUtil( BK_COMPR_ELAPSED_TIMER, errorCode );
        }
        // $[TI1.6]

        PrevCompElapsedTimerImage_ = currentCompressorElapsedTime;
    }
    // $[TI2]
// E600 BDIO
#endif
}
#endif //defined( SIGMA_BD_CPU )


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
BackgroundMaintApp::SoftFault( const SoftFaultID  softFaultID,
                               const Uint32       lineNumber,
                               const char*        pFileName,
                               const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault( softFaultID, SAFETY_NET, BACKGROUNDMAINTAPP,
                           lineNumber, pFileName, pPredicate);
}
