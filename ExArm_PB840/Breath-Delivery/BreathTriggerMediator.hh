
#ifndef BreathTriggerMediator_HH
#define BreathTriggerMediator_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: BreathTriggerMediator - Keeps applicable lists of breath triggers.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathTriggerMediator.hhv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: rhj   Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes:
//       Increase the number of NUM_EXP_TRIGGERS.
// 
//  Revision: 012   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Add RM phase trigger lists.
//
//  Revision: 011   By: syw   Date:  19-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added pav pause triggers handle.
//
//  Revision: 010  By: syw     Date:  15-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added new triggers.
//
//  Revision: 009  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//		ATC initial release.
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 008  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Bilevel
//       Description:
//             Bilevel initial version
//
//  Revision: 007  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Incremented number of exhalation triggers by 1.
//
//  Revision: 006  By:  sp    Date:  9-Jan-1997    DR Number: DCS 1339
//       Project:  Sigma (R8027)
//       Description:
//             Add one more trigger to expiratory pause list.
//
//  Revision: 005  By:  sp    Date:  31-Oct-1996    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 004  By:  iv    Date:  07-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Incremented the number of breath triggers for the exp phase
//             list.
//
//  Revision: 003  By:  iv    Date:  20-Jul-1996    DR Number: DCS 10017
//       Project:  Sigma (R8027)
//       Description:
//             Increased the number of breath triggers for the pause phase
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes
class BreathTrigger;
//@ End-Usage

//@ Constant: NUM_EXP_TRIGGERS
//number of triggers on expiratory list
static const Int32 NUM_EXP_TRIGGERS = 21;

//@ Constant: NUM_INSP_TRIGGERS
//number of triggers on inspiratory list
static const Int32 NUM_INSP_TRIGGERS = 12;

//@ Constant: NUM_VCM_INSP_TRIGGERS
//number of triggers on Vital Capacity Maneuver inspiratory list
static const Int32 NUM_VCM_INSP_TRIGGERS = 14;

//@ Constant: NUM_EXP_PAUSE_TRIGGERS
//number of triggers on expiratory pause list
static const Int32 NUM_EXP_PAUSE_TRIGGERS = 7;

//@ Constant: NUM_INSP_PAUSE_TRIGGERS
//number of triggers on inspiratory pause list
static const Int32 NUM_INSP_PAUSE_TRIGGERS = 7;

//@ Constant: NUM_PAV_INSP_PAUSE_TRIGGERS
//number of triggers on inspiratory pause list
static const Int32 NUM_PAV_INSP_PAUSE_TRIGGERS = 4;

//@ Constant: NUM_NIF_PAUSE_TRIGGERS
//number of triggers on NIF pause list
static const Int32 NUM_NIF_PAUSE_TRIGGERS = 7;

//@ Constant: NUM_NON_BREATHING_TRIGGERS
//number of non breathing phase triggers
static const Int32 NUM_NON_BREATHING_TRIGGERS = 7;

class BreathTriggerMediator
{
  public:
	enum BreathListName
	{
		EXP_LIST, 
		INSP_LIST, 
		VCM_INSP_LIST, 
		EXP_PAUSE_LIST,
		INSP_PAUSE_LIST, 
		PAV_INSP_PAUSE_LIST, 
		NIF_PAUSE_LIST,
		NON_BREATHING_LIST,
		NULL_LIST
	};

    BreathTriggerMediator(void);
    ~BreathTriggerMediator(void);

    void resetTriggerList(void);

    inline void changeBreathListName(const BreathListName newList); 
    void newCycle(void);

    static void SoftFault(const SoftFaultID softFaultID,
		   const Uint32      lineNumber,
		   const char*       pFileName  = NULL,
		   const char*       pPredicate = NULL);

  protected:

  private:
    BreathTriggerMediator(const BreathTriggerMediator&);  // Declared but not implemented
    void operator=(const BreathTriggerMediator&);  // Declared but not implemented

    Boolean checkActiveTriggers_(void);
    void changeListPtr_(void);

    //@ Data-Member: pExpList_
    //active during exp
    BreathTrigger* pExpList_[NUM_EXP_TRIGGERS];	

    //@ Data-Member: pInspList_
    //active during insp
    BreathTrigger* pInspList_[NUM_INSP_TRIGGERS];	

    //@ Data-Member: pVcmInspList_
    //active during vital capacity maneuver insp phase
    BreathTrigger* pVcmInspList_[NUM_VCM_INSP_TRIGGERS];	

    //@ Data-Member: pExpPauseList_
    // active during expiratory pause
    BreathTrigger* pExpPauseList_[NUM_EXP_PAUSE_TRIGGERS];

    //@ Data-Member: pInspPauseList_
    // active during inspiratory pause
    BreathTrigger* pInspPauseList_[NUM_INSP_PAUSE_TRIGGERS];

    //@ Data-Member: pPavInspPauseList_
    // active during pav inspiratory pause
    BreathTrigger* pPavInspPauseList_[NUM_PAV_INSP_PAUSE_TRIGGERS];

    //@ Data-Member: pNifPauseList_
    // active during NIF pause
    BreathTrigger* pNifPauseList_[NUM_NIF_PAUSE_TRIGGERS];

    //@ Data-Member: pNonBreathingList_
    // active during svo, disconnect, powerup, etc.
    BreathTrigger* pNonBreathingList_[NUM_NON_BREATHING_TRIGGERS];

    //@ Data-Member: pActiveBreathList_
    //current breath list in use
    BreathTrigger** pActiveBreathList_;	

    //@ Data-Member: nextList_
    //name of next list to activate
    BreathListName nextList_; 

#ifdef SIGMA_UNIT_TEST
public:
   void setActiveTriggerList(BreathListName newList);
   void write(void);
#endif // SIGMA_UNIT_TEST
};

// Inlined methods
#include "BreathTriggerMediator.in"

#endif // BreathTriggerMediator_HH 
