#ifndef Debouncer_HH
#define Debouncer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Debouncer - implementation of a debounced signal.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/Debouncer.hhv   10.7   08/17/07 09:29:06   pvcs  
//
//@ Modification-Log
//
//  Revision: 005  By:  erm    Date:  14-Aug-2009    DR Number: 6424
//       Project:  840
//       Description:
//			Modified updateValue to include resetting of the state counter
// 
//  Revision: 004  By:  syw    Date:  10-Nov-1999    DR Number: 5578
//       Project:  840
//       Description:
//			Fixed return type of getDelayCounts() to Int32.
//
//  Revision: 003  By:  syw    Date:  27-Oct-1999    DR Number: 5556
//       Project:  840
//       Description:
//			Added delayCounts_ and methods to support delaying of debouncing.
//
//  Revision: 002  By:  syw    Date:  16-Aug-1999    DR Number: 5511
//       Project:  840
//       Description:
//			Added initialize() method
//
//  Revision: 001  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//             Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

//@ End-Usage

class Debouncer {
  public:
  	Debouncer( const Uint32 debounceCycles, const Uint32 initValue, const Int32 delayCounts = 0) ;
    ~Debouncer( void) ;

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;
	inline void updateValue( Uint32 newValue, Boolean updateCount = FALSE) ;
	inline void initialize( Uint32 newValue) ;
	inline Uint32 getDebouncedValue( void) ;
	inline Int32 getDelayCounts( void) ;
	Boolean isReset( void) ;
	
  protected:

  private:
    Debouncer( void) ;						// not implemented...
    Debouncer( const Debouncer&) ;  	// not implemented...
    void   operator=( const Debouncer&) ; 	// not implemented...

    //@ Data-Member: debounceCounts_
    // number of cycles to debounce
    Uint32 debounceCounts_ ;

    //@ Data-Member: counts_
    // current cycles in debounce
    Uint32 counts_ ;

    //@ Data-Member: currValue_
    // the current value
    Uint32 currValue_ ;

    //@ Data-Member: debouncedValue_
    // the current debounced value
    Uint32 debouncedValue_ ;

    //@ Data-Member: delayCounts_
    // number of cycles to delay initial debouncing
    Int32 delayCounts_ ;

} ;

#include "Debouncer.in"

#endif // Debouncer_HH 



