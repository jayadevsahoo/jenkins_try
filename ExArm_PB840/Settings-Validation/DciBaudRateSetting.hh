 
#ifndef DciBaudRateSetting_HH
#define DciBaudRateSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  DciBaudRateSetting - DCI Baud Rate Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DciBaudRateSetting.hhv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005    By: mnr   Date: 03-Mar-2010  SCR Number: 6460
//  Project: NEO
//  Description:
//  Enhanced SCR 6460 fix to revert to user selection (if any).
// 
//  Revision: 004    By: erm   Date: 5-Nov-2008  DR Number: 6441
//  Project: 840S
//  Description:
//  Added support for RT waveform to serial port
// 
//  Revision: 004   By: hct    Date: 03-DEC-1999    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class DciBaudRateSetting : public BatchDiscreteSetting
{
  public:
    DciBaudRateSetting(SettingId::SettingIdType settingId);
    virtual ~DciBaudRateSetting(void);

	// Setting virtual
	virtual void  setAdjustedValue(const SettingValue& newAdjustedValue);
    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;
    virtual Boolean isEnabledValue(const DiscreteValue value) const; 

    virtual const BoundStatus*  acceptPrimaryChange(
				    const SettingId::SettingIdType primaryId
						   );

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    DciBaudRateSetting(void);	// not implemented...
    DciBaudRateSetting(const DciBaudRateSetting&);	// not implemented...
    void  operator=(const DciBaudRateSetting&);	        // not implemented...

    //@ Data-Member:  settingId_
    // Setting Id for a comm port's baud rate setting.
	SettingId::SettingIdType  settingId_;

};


#endif // DciBaudRateSetting_HH 
