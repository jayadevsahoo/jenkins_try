#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VentStartSubScreen - Subscreen displayed in the Lower Screen
// on startup of normal ventilation.  Allows operator to choose Same Patient,
// New Patient or Short Self Test.
//---------------------------------------------------------------------
//@ Interface-Description
// The Vent Startup subscreen is displayed on power-up of the ventilator (after
// the successful completion of POST when this is not a power fail recovery and
// the service mode button was not pressed) and allows the operator to choose
// between continuing with the settings for the patient that was attached at
// the time of the previous power-down (Same Patient option) or the setting up
// of a new patient (New Patient option) or the running of Short Self Test (SST
// option).
//
// There is a dedicated button for each option and the operator must press a
// Continue button after pressing either the New Patient or SST option button
// in order to select the option.  The Same Patient option is selected via the
// Accept key.  Selecting an option causes transition to a different subscreen.
//
// There are no methods that modify the operation of VentStartSubScreen.
// Once created, VentStartSubScreen operates without needing intervention
// from other objects.  Only methods that are part of the GUI-Application
// framework are defined.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// buttonDownHappened() and buttonUpHappened() are called when any button in
// the subscreen is pressed.  Timer events (specifically the subscreen setting
// change timeout event) are passed into timerEventHappened() and Adjust Panel
// events are communicated via the various adjustPanel?Happened() methods.
// Changes in GUI App states result in a call to guiEventHappened().
//---------------------------------------------------------------------
//@ Rationale
// Implements all the functionality for the Vent Startup subscreen in a single
// class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The main operation of this class is to wait for button down and up events,
// received via buttonUpHappened() and buttonDownHappened().  This class
// registers as a target of all four buttons and it basically waits for button
// down and up events to occur.  Pressing down the New Patient or Short Self
// Test button causes the Continue button to be added to display.  Continue
// button events are then caught by the button callback methods.  The main
// functionality of this class is concentrated in those methods.
//
// An important member is 'currentOption_' which tracks which option, if any,
// the user has proposed (i.e. tracks which option button, if any, is currently
// pressed down).
//
// Even though only the Same Patient option needs to use the Adjust Panel, all
// three options grab the Adjust Panel focus.  This is because we
// want to pop up any option button if another object grabs the Adjust Panel
// focus so that we can tidy up the Prompt Area.  Basically, any object that
// needs to display high priority prompts (ones that highlight the prompt area)
// should grab the Adjust Panel focus (normally, all objects that use high
// priority prompts also use the Adjust Panel though, in this case, we don't
// really use the Adjust Panel but we do want to display highlighted prompts,
// hence the oddity)!
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VentStartSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  modified to use new names of string identifiers
//      *  mapped missing TC requirement
//
//  Revision: 013   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 012   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 011  By: hhd    Date: 07-June-1999  DCS Number: 5423 
//  Project:  ATC
//  Description:
//		Used ARROW_ICON instead for setting verification icon and eliminated
//  the icon background box.
//
//  Revision: 010  By: hhd    Date: 05-May-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//		Added background for flashing icons.
//
//  Revision: 009  By: sah    Date: 29-Apr-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//	Supporting new verification-needed mechanism, whereby certain
//      setting buttons that are deemed "ultra"-important will flash an
//      icon, and this subscreen will flash the same icon along with a
//      message.
//
//  Revision: 008  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 007  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/VentStartSubScreen.ccv   1.62.1.0   07/30/98 10:22:38   gdc
//
//  Revision: 006  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 005  By:  yyy    Date:  25-SEP-1997    DR Number:   2385
//    Project:  Sigma (R8027)
//    Description:
//      Display diagnostic message for flow sensor calibration status.
//
//  Revision: 004  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 003  By:  yyy    Date:  30-JUL-97    DR Number: 2313
//    Project:  Sigma (R8027)
//    Description:
//      30-JUL-97 Added diagnostic string information for Tau characterization.
//		13-AUG-97  Removed all references for TauCharacterization
//
//  Revision: 002  By: yyy      Date: 15-May-1997  DR Number: 2066
//    Project:  Sigma (R8027)
//    Description:
//      Removed all checking for patient circuit type which is handled
//		by Service-Mode now.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "GuiApp.hh"
#include "MiscStrs.hh"
#include "VentStartSubScreen.hh"
#include "NovRamManager.hh"
#include "CommTaskAgent.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "BdEventRegistrar.hh"
#include "BdGuiEvent.hh"
#include "GuiTimerId.hh"
#include "GuiEventRegistrar.hh"
#include "GuiTimerRegistrar.hh"
#include "LowerScreen.hh"
#include "PromptArea.hh"
#include "ResultEntryData.hh"
#include "SettingContextHandle.hh"
#include "Sound.hh"
#include "TaskControlAgent.hh"
#include "TubeTypeValue.hh"
#include "Image.hh"

//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_X_ = 28;
static const Int32 BUTTON_WIDTH_  = 99;
static const Int32 BUTTON_HEIGHT_ = 39;
static const Int32 BUTTON_BORDER_ = 3;
static const Int32 SAME_PATIENT_BUTTON_Y_ = 63;
static const Int32 NEW_PATIENT_BUTTON_Y_ = 112;
static const Int32 SST_BUTTON_Y_ = 173;
static const Int32 MESSAGE_X_ = 169;
static const Int32 DIVIDING_LINE_X_ = 12;
static const Int32 DIVIDING_LINE_Y_ = 161;
static const Int32 DIVIDING_LINE_LENGTH_ = 603;
static const Int32 CONTINUE_BUTTON_X_ = 547;
static const Int32 CONTINUE_BUTTON_Y_ = 209;
static const Int32 CONTINUE_BUTTON_WIDTH_ = 84;
static const Int32 CONTINUE_BUTTON_HEIGHT_ = 34;

static const Int32 SAME_PATIENT_MSG_Y_ = ::SAME_PATIENT_BUTTON_Y_;
static const Int32 SAME_PATIENT_SHIFTED_MSG1_Y_ = ::SAME_PATIENT_MSG_Y_ - 10;
static const Int32 SAME_PATIENT_SHIFTED_MSG2_Y_ =
										::SAME_PATIENT_SHIFTED_MSG1_Y_ + 18;



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VentStartSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the VentStartSubScreen.  Passed a pointer to the subscreen
// area which creates it (the lower subscreen area). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates the various graphics needed for the subscreen (buttons, text fields,
// title area).  Sizes, positions and colors the subscreen.  Adds all permanent
// graphics to the container and registers for callbacks from all buttons.
//
// $[01034] All of these buttons must be of the select type ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentStartSubScreen::VentStartSubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	samePatientButton_(BUTTON_X_, SAME_PATIENT_BUTTON_Y_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_,
			Button::DARK, BUTTON_BORDER_, Button::SQUARE,
			Button::NO_BORDER, MiscStrs::SAME_PATIENT_BUTTON_TITLE),
	newPatientButton_(BUTTON_X_, NEW_PATIENT_BUTTON_Y_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_,
			Button::LIGHT, BUTTON_BORDER_, Button::SQUARE,
			Button::NO_BORDER, MiscStrs::NEW_PATIENT_BUTTON_TITLE),
	sstButton_(BUTTON_X_, SST_BUTTON_Y_, BUTTON_WIDTH_,
			BUTTON_HEIGHT_, Button::DARK, BUTTON_BORDER_,
			Button::SQUARE, Button::NO_BORDER,
			MiscStrs::SHORT_SELF_TEST_BUTTON_TITLE),
	continueButton_(CONTINUE_BUTTON_X_, CONTINUE_BUTTON_Y_,
			CONTINUE_BUTTON_WIDTH_, CONTINUE_BUTTON_HEIGHT_,
			Button::LIGHT, BUTTON_BORDER_, Button::SQUARE,
			Button::NO_BORDER, MiscStrs::CONTINUE_BUTTON_TITLE),
	samePatientMessage_(MiscStrs::SAME_PATIENT_MESSAGE),
	samePatientVerifyMessage_(MiscStrs::SAME_PATIENT_VERIFY_MESSAGE),
	samePatientVerifyIcon_(Image::RCheckArrowIcon),
	newPatientMessage_(MiscStrs::NEW_PATIENT_MESSAGE),
	sstMessage_(MiscStrs::SHORT_SELF_TEST_MESSAGE),
	titleArea_(MiscStrs::VENT_START_TITLE, SubScreenTitleArea::SSTA_CENTER),
	dividingLine_(DIVIDING_LINE_X_, DIVIDING_LINE_Y_,
		DIVIDING_LINE_X_ + DIVIDING_LINE_LENGTH_ - 1,
		DIVIDING_LINE_Y_),
	currentOption_(NO_OPTION_)
{
	CALL_TRACE("VentStartSubScreen::VentStartSubScreen(void)");

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Add the sub-screen title and the dividing line
	addDrawable(&titleArea_);
	dividingLine_.setColor(Colors::WHITE);
	addDrawable(&dividingLine_);

	// Position and add miscellaneous text
	samePatientMessage_.setX(MESSAGE_X_);
	samePatientMessage_.setY(SAME_PATIENT_BUTTON_Y_);
	samePatientMessage_.setColor(Colors::WHITE);
	samePatientVerifyMessage_.setX(MESSAGE_X_ + 22);
	samePatientVerifyMessage_.setY(SAME_PATIENT_SHIFTED_MSG2_Y_);
	samePatientVerifyMessage_.setColor(Colors::WHITE);
	samePatientVerifyMessage_.setShow(FALSE);
	DiscreteValue tubeType =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
													SettingId::TUBE_TYPE);		 
	BoundedValue settingValue = 
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
													SettingId::TUBE_ID);
	Real32 tubeId = settingValue.value;
	static wchar_t tmpBuf[256];   
	swprintf(tmpBuf, MiscStrs::SAME_PATIENT_VERIFY_MESSAGE, 
		tubeId, getTubeTypeText_(tubeType));
	samePatientVerifyMessage_.setText(tmpBuf);

	samePatientVerifyIcon_.setX(MESSAGE_X_);
	samePatientVerifyIcon_.setY(SAME_PATIENT_SHIFTED_MSG2_Y_ + 11);
	samePatientVerifyIcon_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
	samePatientVerifyIcon_.setShow(FALSE);
	newPatientMessage_.setX(MESSAGE_X_);
	newPatientMessage_.setY(NEW_PATIENT_BUTTON_Y_);
	newPatientMessage_.setColor(Colors::WHITE);
	sstMessage_.setX(MESSAGE_X_);
	sstMessage_.setY(SST_BUTTON_Y_);
	sstMessage_.setColor(Colors::WHITE);
	addDrawable(&samePatientMessage_);
	addDrawable(&samePatientVerifyMessage_);
	addDrawable(&samePatientVerifyIcon_);
	addDrawable(&newPatientMessage_);
	addDrawable(&sstMessage_);

	// Add the option buttons and register for callbacks to all buttons
	// $[01007] Vent Startup shall provide same/new patient and SST choices.
	addDrawable(&samePatientButton_);
	addDrawable(&newPatientButton_);
	addDrawable(&sstButton_);
	samePatientButton_.setButtonCallback(this);
	newPatientButton_.setButtonCallback(this);
	sstButton_.setButtonCallback(this);
	continueButton_.setButtonCallback(this);

	BdEventRegistrar::RegisterTarget(EventData::SST_CONFIRMATION, this);
	BdEventRegistrar::RegisterTarget(EventData::PATIENT_CONNECT, this);

	// Register for GUI state change callbacks.
	GuiEventRegistrar::RegisterTarget(this);			
															// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VentStartSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys VentStartSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VentStartSubScreen::~VentStartSubScreen(void)
{
	CALL_TRACE("VentStartSubScreen::~VentStartSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary display setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// We start the subscreen setting change timer and layout the screen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::activate(void)
{
	CALL_TRACE("VentStartSubScreen::activate(void)");

	// Start a timer to timeout inactivity
	GuiTimerRegistrar::StartTimer(
						GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT, this);

	// No option currently active
	currentOption_ = NO_OPTION_;

	// Layout screen
	layoutScreen_();

	// Clear the default subscreen 
	LowerScreen::RLowerScreen.getLowerSubScreenArea()->setDefaultSubScreen(NULL, NULL);
															// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Takes no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// We remove any prompts that we may have posted and release the Adjust Panel
// focus (which ensures that the Continue button is removed from this container
// and that all option buttons are in the up position).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::deactivate(void)
{
	CALL_TRACE("VentStartSubScreen::deactivate(void)");

	// Remove any of our prompts.
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
									NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
									NULL_STRING_ID);

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL);
															// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when any button in Vent Startup is pressed down.  Passed a pointer to
// the button as well as 'byOperatorAction', a flag which indicates whether the
// operator pressed the button down or whether the setToDown() method was
// called.
//
// Displays the Continue button if the New Patient or SST buttons were pressed.
// Grabs the Adjust Panel focus if any option button was pressed.  If the
// Continue button was pressed then we proceed to either New Patient Setup or
// to SST (depending on which option is currently selected).
//---------------------------------------------------------------------
//@ Implementation-Description
// Checks which button was pressed down.  If it was one of the option buttons
// then make sure the other two option buttons are in the up position (one of
// them could currently be in the down position), grab the Adjust Panel focus,
// display appropriate prompts as well as the Continue button (if New Patient
// or SST selected).
//
// If the Continue button was pressed down then check which option is
// currently active and inform LowerScreen of the chosen option.
//
// $[01025] When a user touches an instant action button, the appearance ...
// $[01026] When touched and released, an instant action button shall ...
// $[01030] Button in same selection group should be deselected on selection
//			of another button in the same group
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::buttonDownHappened(Button *pButton, Boolean)
{
	CALL_TRACE("VentStartSubScreen::buttonDownHappened(Button *pButton, "
												"Boolean)");

	// Was the Same Patient button pressed?
	if (pButton == &samePatientButton_)
	{
															// $[TI1]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this);

		// Remember that this is the currently selected option
		currentOption_ = SAME_PATIENT_;

		// Make sure Continue button is not displayed.
		removeDrawable(&continueButton_);
		continueButton_.setToUp();

		// Set Primary prompt to "Press ACCEPT if OK."
		// $[01012] GUI should prompt user to press Accept if Same Patient
		//			option is chosen
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_HIGH, PromptStrs::PRESS_ACCEPT_IF_OK_P);

		// Set Advisory prompt to "Retains previous patient settings and data."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
				PromptArea::PA_HIGH, PromptStrs::RETAINS_PREVIOUS_SETTINGS_A);
	}
	// Was it the New Patient button that was pressed?
	else if (pButton == &newPatientButton_)
	{
															// $[TI2]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this);

		// Remember that this is the currently selected option
		currentOption_ = NEW_PATIENT_;

		// $[01009] When the user chooses the New Patient Setup sequence...
		//          the GUI shall transition to New Patient Setup

		// Inform the lower screen to begin new patient setup
		LowerScreen::RLowerScreen.startNewPatientSetup();
	}
	// Maybe it was the Short Self Test button?
	else if (pButton == &sstButton_)
	{													
															// $[TI3]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this);

		// Remember that this is the currently selected option
		currentOption_ = SHORT_SELF_TEST_;

		// Make sure Continue button is not displayed.
		removeDrawable(&continueButton_);
		continueButton_.setToUp();

		// Notify Breath-Delivery that the SST option has been chosen
		BdGuiEvent::RequestUserEvent(EventData::SST_CONFIRMATION); 
	
		// Clear all high priority prompts (we will wait for a response
		// from the BdGuiEvent object before displaying appropriate prompts).
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
	}
	else
	{
		// Unknown button pressed
		CLASS_ASSERTION(FALSE);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in Vent Startup is pressed up.  Passed a pointer to
// the button as well as 'byOperatorAction', a flag which indicates whether the
// operator pressed the button up or whether the setToUp() method was
// called.
//
// If the operator pressed a button up then we clear the Adjust Panel focus.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the operator initiated the event then clear the Adjust Panel focus (which
// results in the Continue button, if displayed, being removed, as well as any
// high priority prompts).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::buttonUpHappened(Button *, Boolean byOperatorAction)
{
	CALL_TRACE("VentStartSubScreen::buttonUpHappened(Button *pButton, "
												"Boolean byOperatorAction)");

	// Check if the event is operator initiated
	if (byOperatorAction)
	{
															// $[TI1]
		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL);
	}
															// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called when a change in the GUI App state happens.  Passed the id of the
// state which changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call the layoutScreen_() method.  Ignore the state change if vent startup
// is complete.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::guiEventHappened(GuiApp::GuiAppEvent
#ifndef FORNOW
eventId
#endif // FORNOW
)
{
	CALL_TRACE("VentStartSubScreen::guiEventHappened(GuiApp::GuiAppEvent)");


#ifndef FORNOW  // Back door code to allow access to Vent Setup etc. even if
				// BD is not connected or comms is down.
if (eventId == GuiApp::SERVICE_READY)
{
	samePatientButton_.setShow(SettingContextHandle::IsSamePatientValid());
	samePatientMessage_.setShow(SettingContextHandle::IsSamePatientValid());
	newPatientButton_.setShow(TRUE);
	newPatientMessage_.setText(MiscStrs::NEW_PATIENT_MESSAGE);
	sstButton_.setShow(TRUE);
	sstMessage_.setText(MiscStrs::SHORT_SELF_TEST_MESSAGE);
	return;
}
#endif // ifndef FORNOW
															// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when a timer that we started runs out, in this case the screen
// inactivity timer.  We are passed an enum id identifying the timer.
// We must reset the subscreen to its startup state.
//---------------------------------------------------------------------
//@ Implementation-Description
// We reset the subscreen by deactivating and reactivating it.  We only react
// to timers if this subscreen is visible.  There is a small chance that this
// timer event might arrive after we have been deactivated so we ignore it.
// $[01311] If the vent startup sequence is partially completed, and the user ...
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the screen inactivity timer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("VentStartSubScreen::timerEventHappened("
									"GuiTimerId::GuiTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId ==
								GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT);

	// React to event only if we're visible.
	if (isVisible())
	{
															// $[TI1]
		// Reactivate this sub-screen
		deactivate();
		activate();
	}
															// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting of the Same Patient setup.  Tell the Lower Screen of the event and
// let it handle the activation of normal operation and put the operator
// straight into the Apnea Setup subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the Same Patient is the currently selected option then simply inform the
// Lower Screen of the event, else make the Invalid Entry sound.
// $[01216] The invalid entry sound shall be produced whenever ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("VentStartSubScreen::adjustPanelAcceptPressHappened(void)");

	SAFE_CLASS_ASSERTION(currentOption_ != NO_OPTION_);

	// Inform the Lower Screen of the operator's choice
	if (currentOption_ == SAME_PATIENT_)
	{
		// Update Vent startup state.
		GuiApp::SetVentStartupSequenceComplete(TRUE);
															// $[TI1]
		LowerScreen::RLowerScreen.samePatientSetupAccepted();
	}
	else
	{
															// $[TI2]
		// Make the Invalid Entry sound
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is called when the operator presses the Clear key.
// We make an Invalid Entry sound as the Clear key is always invalid in
// this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Tell the sound system to make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("VentStartSubScreen::adjustPanelClearPressHappened(void)");

	Sound::Start(Sound::INVALID_ENTRY);		// Make the Invalid Entry sound
															// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelLoseFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is called when this subscreen is about to lose the focus of
// the Adjust Panel.  Pop up whichever button is currently selected
// and reset any prompts and remove the Continue button (if displayed).
//---------------------------------------------------------------------
//@ Implementation-Description
// We make sure all buttons go up and that the Continue button and the
// high priority prompts are removed.  The current option is set to
// NO_OPTION_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::adjustPanelLoseFocusHappened(void)
{
	CALL_TRACE("VentStartSubScreen::adjustPanelLoseFocusHappened(void)");


	// Make sure Continue button is not left on subscreen and all option buttons
	// are in the Up state
	switch (currentOption_)
	{
	case SAME_PATIENT_:
															// $[TI1]
		samePatientButton_.setToUp();
		break;

	case NEW_PATIENT_:
															// $[TI2]
		newPatientButton_.setToUp();
		removeDrawable(&continueButton_);
		continueButton_.setToUp();
		break;

	case SHORT_SELF_TEST_:
			
		// $[TI3]
		// Notify Breath-Delivery that the SST option has been cancelled
		BdGuiEvent::RequestUserEvent(EventData::SST_CONFIRMATION,
															EventData::STOP);
		sstButton_.setToUp();
		break;

	case NO_OPTION_:
	default:																										// $[TI4]
															// $[TI4]
		// Ignore if no option is selected.
		break;
	}

	currentOption_ = NO_OPTION_;		// No option is now selected

	// Remove the high priority prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
												NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
												NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
											PromptArea::PA_HIGH, NULL_STRING_ID);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
// Called to notify this object when Breath-Delivery events that we've
// registered for occur.
//---------------------------------------------------------------------
//@ Implementation-Description
// For SST_CONFIRMATION Bd event, we ignore any events received if the
// SST option is not currently the chosen option (may have been cancelled
// just before an outstanding SST BD event is received on the GUI-App
// input queue).  Else we display the appropriated prompts to respond
// to passed BD event.
// For PATIENT_CONNECT we only display the VentStartup screen when we
// are in PatientSetupMode.
//
// $[01018] When the user selects the Run Short-Self-Test sequence...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
VentStartSubScreen::bdEventHappened(EventData::EventId eventId,
					                EventData::EventStatus eventStatus,
									EventData::EventPrompt eventPrompt)
{
	CALL_TRACE("VentStartSubScreen::bdEventHappened(EventData::EventId eventId, "
					                "EventData::EventStatus eventStatus)");

	// Check which BD event was received
	switch (eventId)
	{
	case EventData::SST_CONFIRMATION:
		
															// $[TI1]
		// Ignore any events received if the SST option is not currently
		// the chosen option (may have been cancelled just before an
		// outstanding SST BD event is received on the GUI-App input queue).
		if (SHORT_SELF_TEST_ == currentOption_)
		{
															// $[TI2]
			switch (eventStatus)
			{
			case EventData::PENDING:
															// $[TI3]
				// Pending: means the Test button is being held down as
				// the SST option was chosen.  Tell user to release it.

				// Set Primary prompt to "Release Test button."
				LowerScreen::RLowerScreen.getPromptArea()->setPrompt(
								PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
								PromptStrs::RELEASE_TEST_BUTTON_P);
		
				// Set Advisory prompt to "Test button may be stuck."
				LowerScreen::RLowerScreen.getPromptArea()->setPrompt(
								PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
								PromptStrs::TEST_BUTTON_STUCK_A);
		
				// Set Secondary prompt to "To cancel: touch SST button."
				LowerScreen::RLowerScreen.getPromptArea()->setPrompt(
								PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
								PromptStrs::SST_CANCEL_S);
				break;
	
			case EventData::ACTIVE:
															// $[TI4]
				// Active: means the Test button is not being held down as
				// the SST option was chosen.  Tell user to press it within
				// 5 seconds to begin SST.

				// Set Primary prompt to "Ensure no patient is attached."
				LowerScreen::RLowerScreen.getPromptArea()->setPrompt(
								PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
								PromptStrs::ENSURE_NO_PATIENT_ATTACHED_P);
		
				// Set Advisory prompt to "SST will begin."
				LowerScreen::RLowerScreen.getPromptArea()->setPrompt(
								PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
								PromptStrs::SST_WILL_BEGIN_A);
		
				// Set Secondary prompt to "To confirm: press Test button
				// within 5 seconds.  To cancel: touch SST button."
				LowerScreen::RLowerScreen.getPromptArea()->setPrompt(
								PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
								PromptStrs::SST_CONFIRM_CANCEL_S);
				break;
	
			case EventData::CANCEL:
			case EventData::REJECTED:
															// $[TI5]
				// Cancel: the user did not press Test button within 5 seconds.
				// Make an invalid entry sound and deactivate the SST option.

				Sound::Start(Sound::INVALID_ENTRY);

				// Clear the Adjust Panel focus to deactivate SST option.
				AdjustPanel::TakeNonPersistentFocus(NULL);

				break;
			}
		}
															// $[TI6]
		break;
		
	case EventData::PATIENT_CONNECT:
															// $[TI7]
	    if (LowerScreen::RLowerScreen.isInPatientSetupMode())
    	{													
															// $[TI8]
			// Layout screen
			layoutScreen_();
		}													
															// $[TI9]
		break;

	default:
		CLASS_ASSERTION(FALSE);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_		[private]
//
//@ Interface-Description
// Lays out the screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Checks the various gui states, if GUI and BD are online, if ventilation
// is allowed etc., and determines what buttons and messages should be
// displayed.
// $[01283] If the operator exits from SST before it is complete, ...
// $[01310] The control allowing Same patient and New patient ...
// $[01309] The control allowing SST to be run shall be hidden if a ...
// $[TC01005] When the previous same patient setting included 'PA' or 'TC'...
// $[NE01011] When an acceptance of an SST Setup includes ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::layoutScreen_(void)
{
	CALL_TRACE("VentStartSubScreen::layoutScreen_(void)");

	// initially have all conditionally-displayed items hidden...
	samePatientButton_.setShow(FALSE);
	samePatientMessage_.setShow(FALSE);
	samePatientVerifyMessage_.setShow(FALSE);
	samePatientVerifyIcon_.setShow(FALSE);
	newPatientButton_.setShow(FALSE);
	sstButton_.setShow(FALSE);

#ifdef FORNOW // FORNOW ignore the fact that SST hasn't been run
printf("VentStartSubScreen, IsSettingsLockedOut()=%d, IsVentInopMajorFailure()=%d\n",
		GuiApp::IsSettingsLockedOut(), GuiApp::IsVentInopMajorFailure());
printf("    GetSerialNumberInfoState()=%d, IsBdAndGuiFlashTheSame()=%d\n",
		GuiApp::GetSerialNumberInfoState(), GuiApp::IsBdAndGuiFlashTheSame());
printf("	isInPatientSetupMode() = %d\n", LowerScreen::RLowerScreen.isInPatientSetupMode());
#endif // FORNOW
	// Display all possible system malfunction messages.
	// BD goes vent Inop.
	if (GuiApp::IsSettingsLockedOut()
#ifndef SIGMA_DEVELOPMENT // FORNOW ignore the flash table.
		//|| !GuiApp::IsBdAndGuiFlashTheSame()
		//|| GuiApp::SERIAL_NUMBER_OK != GuiApp::GetSerialNumberInfoState()  //TODO E600 - Code is commented - Hacked-5 - To get patient button - for GUI First_Normal_Vent_Scr
#endif // SIGMA_DEVELOPMENT
	   )
	{
															// $[TI1]
		// Display New Patient message explaining why New Patient option
		// is not available
		newPatientMessage_.setText(MiscStrs::NEW_PATIENT_MALFUNCTION_MESSAGE);
	
		// Remove the SST button and message from display
		// If settings are locked out remove both buttons also
		if (GuiApp::IsSettingsLockedOut())
		{
															// $[TI2]
			if (GuiApp::IsGuiPostFailed())
			{
															// $[TI3]
				sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_GUI_POST_FAIL_MESSAGE);
			}
			else if (GuiApp::IsBdPostFailed())
			{
															// $[TI4]
				sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_BD_POST_FAIL_MESSAGE);
			}
			else if (GuiApp::IsGuiBackgroundFailed())
			{
															// $[TI5]
				sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_GUI_BG_FAIL_MESSAGE);
			}
			else if (GuiApp::IsBdBackgroundFailed())
			{
															// $[TI6]
				sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_BD_BG_FAIL_MESSAGE);
			}
			else if (GuiApp::IsServiceRequired())
			{
															// $[TI7]
				if (GuiApp::IsVentInopMajorFailure())
				{
															// $[TI8]
					sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_VENT_INOP_FAIL_MESSAGE);
				}
				else if (GuiApp::IsBdVentInopTestInProgress())
				{
															// $[TI9]
					sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_VENT_INOP_TEST_IN_PROGRESS_MESSAGE);
				}
				else if (GuiApp::IsExhValveCalRequired())
				{
															// $[TI10]
					sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_EXH_CAL_REQD_MESSAGE);
				}
				else if (GuiApp::IsFlowSensorInfoRequired())
				{
															// $[TI11]
					sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_FLOW_SENSOR_INFO_REQD_MESSAGE);
				}
				else if (!GuiApp::IsFlowSensorCalPassed())
				{
															// $[TI11.5]
					sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_FLOW_SENSOR_CAL_REQD_MESSAGE);
				}
				else
				{	// Must be the EST failed/alert
															// $[TI12]
					sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_EST_MESSAGE);
				}
			}
			else if (GuiApp::GetGuiState() == STATE_TIMEOUT)
			{
															// $[TI13]
				sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_GUI_TIMEOUT_MESSAGE);
			}
			else if (GuiApp::GetGuiState() == STATE_INOP)
			{
				if (STATE_FAILED == TaskControlAgent::GetBdState())
				{
															// $[TI14.1]
					sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_TUV_FAILED_GUI_INOP_MESSAGE);
				}
				else
				{
															// $[TI14]
					sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_GUI_INOP_MESSAGE);
				}
			}
			else if (GuiApp::GetBdState() == STATE_INOP)
			{
															// $[TI15]
				sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_BD_INOP_MESSAGE);
			}
			else
			{
															// $[TI16]
				sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_LOCKOUT_MESSAGE);
			}
		}
#ifndef SIGMA_DEVELOPMENT // FORNOW ignore the flash table.
		// If flash tables do not match, remove both buttons.
		else if (!GuiApp::IsBdAndGuiFlashTheSame())
		{												
			sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_BAD_FLASH_TABLE);
		}
		// If Serial numbers do not match, remove both buttons.
		else if (GuiApp::SERIAL_NUMBER_OK != GuiApp::GetSerialNumberInfoState())
		{
															// $[TI17]
			sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_BAD_SERIAL_NUMBER);
		}
#endif // SIGMA_DEVELOPMENT
		else
		{
			CLASS_ASSERTION_FAILURE();
		}
	}
	else
	{	// Displaying patient related messages.
															// $[TI18]
		// Patient setting is allowed if SST was passed or was overridden
		Boolean patientSetupAllowed = TRUE
#ifndef SIGMA_DEVELOPMENT // FORNOW ignore the fact that SST hasn't been run
									//&& (!GuiApp::IsSstFailure()) //TODO E600 - Hacked-6 - commented : Assuming: - SST check is by-passed - for GUI First_Normal_Vent_Scr
#endif // SIGMA_DEVELOPMENT
				;

		if (patientSetupAllowed)
		{ 
													// $[TI19]
			// Display the Same Patient button depending on if there are
			// same patient settings available or not.
			const Boolean  IS_SAME_PATIENT_VALID =
									SettingContextHandle::IsSamePatientValid();

			if (IS_SAME_PATIENT_VALID)
			{										// $[TI19.1]
				samePatientButton_.setShow(TRUE);
				samePatientMessage_.setShow(TRUE);

				const Applicability::Id  TUBE_TYPE_APPLIC =
					SettingContextHandle::GetSettingApplicability(
												ContextId::ACCEPTED_CONTEXT_ID,
												SettingId::TUBE_ID
																 );

				if (TUBE_TYPE_APPLIC == Applicability::CHANGEABLE)
				{								   	// $[TI19.1.1]
					// shift up the same patient message, to fit in a new
					// line...
					samePatientMessage_.setY(SAME_PATIENT_SHIFTED_MSG1_Y_);
					samePatientVerifyMessage_.setShow(TRUE);
					samePatientVerifyIcon_.setShow(TRUE);
				}
				else
				{									// $[TI19.1.2]
					samePatientMessage_.setY(SAME_PATIENT_MSG_Y_);
				}
			}										// $[TI19.2]

			// Display the New Patient button and correct message
			newPatientButton_.setShow(TRUE);
			newPatientMessage_.setText(MiscStrs::NEW_PATIENT_MESSAGE);
		}
		else
		{
												   	// $[TI20]
			// Display New Patient message explaining why New Patient option
			// is not available
			newPatientMessage_.setText(MiscStrs::NEW_PATIENT_NO_VENT_MESSAGE);
		}

		// Now for the SST. 
		// If the patient is or ever was connected remove both buttons and
		// indicate that patient connection is the reason for this.
		// $[01308] The control allowing SST to be run shall be hidden if a ...
		if (BdGuiEvent::GetEventStatus(EventData::PATIENT_CONNECT) !=
												EventData::IDLE)
		{
															// $[TI21]
			sstMessage_.setText(MiscStrs::SST_NOT_ALLOWED_PATIENT_MESSAGE);
		}
		else
		{											
															// $[TI22]
			// Both systems online, patient is not connected and no
			// settings lockout, show the both buttons.
			sstButton_.setShow(TRUE);
			sstMessage_.setText(MiscStrs::SHORT_SELF_TEST_MESSAGE);
		}
	}

	// The Main Settings Area should match the visibility of the Same
	// Patient button.
	LowerScreen::RLowerScreen.getMainSettingsArea()->setBlank(
							! samePatientButton_.getShow());
	
	// If we're in Settings lockout mode then we need to display special prompts
	if (GuiApp::IsSettingsLockedOut())
	{													
															// $[TI23]
		// Set Primary prompt to nothing
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, NULL_STRING_ID);
		// Set Advisory prompt to "Settings have been locked out."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_LOW, PromptStrs::SETTINGS_LOCKED_OUT_A);
	}
	else if (newPatientButton_.getShow())
	{													
															// $[TI24]
		// If the New Patient button is showing display appropriate prompts.

		// Set Primary prompt to "Make a selection"
		LowerScreen::RLowerScreen.getPromptArea()->setPrompt(
							PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
							PromptStrs::TOUCH_A_BUTTON_P);
	
		// Set Advisory prompt to "Select BEFORE attaching patient!"
		LowerScreen::RLowerScreen.getPromptArea()->setPrompt(
							PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
							PromptStrs::SELECT_BEFORE_ATTACHING_PATIENT_A);
	}
	else
	{												
															// $[TI25]
		// If the New Patient button is not visible then make sure we're
		// displaying no prompts in the prompt area

		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									PromptArea::PA_LOW, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									PromptArea::PA_LOW, NULL_STRING_ID);
	}

	// Reset the screen state as we do when we lose Adjust Panel focus
	adjustPanelLoseFocusHappened();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTubeTypeText_  [private]
//
//@ Interface-Description
//  Given a DiscreteValue tube type, this method returns the display text for the
//  associated tube type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Depending on the passed in value, return the corresponding text value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
StringId 
VentStartSubScreen::getTubeTypeText_(DiscreteValue tubeType)
{
	CALL_TRACE("VentStartSubScreen::getTubeTypeText_(void)");

	switch (tubeType)
	{
		case TubeTypeValue::ET_TUBE_TYPE:		// $[TI1.1]
			return(MiscStrs::STATUS_TUBE_TYPE_ET_TEXT);
		case TubeTypeValue::TRACH_TUBE_TYPE:	// $[TI1.2]
			return(MiscStrs::STATUS_TUBE_TYPE_TRACH_TEXT);
		default:							 	// $[TI1.3]
			return(MiscStrs::EMPTY_STRING);
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
VentStartSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, VENTSTARTSUBSCREEN,
									lineNumber, pFileName, pPredicate);
}

