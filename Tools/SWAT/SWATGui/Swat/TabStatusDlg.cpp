// TabStatusDlg.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "swat.h"
#include "TabStatusDlg.h"


// CTabStatusDlg dialog

IMPLEMENT_DYNAMIC(CTabStatusDlg, CDialog)

CTabStatusDlg::CTabStatusDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTabStatusDlg::IDD, pParent)
	, m_bFreeze(false)
{

}

CTabStatusDlg::~CTabStatusDlg()
{
}

void CTabStatusDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_BOX, m_lbColor);
}


BEGIN_MESSAGE_MAP(CTabStatusDlg, CDialog)
	ON_BN_CLICKED(IDC_CHECK_FREEZE, &CTabStatusDlg::OnBnClickedCheckFreeze)
	ON_BN_CLICKED(IDC_BUTTON1, &CTabStatusDlg::OnBnClickedExpand)
	ON_LBN_DBLCLK(IDC_LIST_BOX, &CTabStatusDlg::OnLbnDblclkListBox)
	ON_BN_CLICKED(IDC_BUTTON_COPY, &CTabStatusDlg::OnBnClickedButtonCopy)
	ON_COMMAND(ID_STATUS_COPYSELECTED, &CTabStatusDlg::OnStatusCopyselected)
	ON_WM_CONTEXTMENU()
	ON_BN_CLICKED(IDC_BUTTON_STATUS_CLEAR, &CTabStatusDlg::OnBnClickedButtonStatusClear)
	ON_BN_CLICKED(IDC_BUTTON_FIND, &CTabStatusDlg::OnBnClickedButtonFind)
	ON_EN_CHANGE(IDC_EDIT_FIND, &CTabStatusDlg::OnEnChangeEditFind)
	ON_BN_CLICKED(IDC_BUTTON_FIND_UP, &CTabStatusDlg::OnBnClickedButtonFindUp)
	ON_BN_CLICKED(IDC_BUTTON_CPY, &CTabStatusDlg::OnBnClickedButtonCpyID)
	ON_BN_CLICKED(IDC_BUTTUN_OPEN, &CTabStatusDlg::OnBnClickedButtunOpen)
END_MESSAGE_MAP()


// CTabStatusDlg message handlers
//actual test stauts data for the script execution
void CTabStatusDlg::UpdateTestStatus(CString &str, int nMsgType)
{
	if(!IsWindow(m_hWnd)) return;

	int nTotalCount = m_lbColor.GetCount();

	if(nTotalCount > 10000    &&   !m_bFreeze)
	{
		m_lbColor.SetRedraw(FALSE); 
		for (int i=0;i < nTotalCount-3000; i++)
		{
			m_lbColor.DeleteString( 0 );
		} 
		m_lbColor.InsertString(0, "Open expanded view for log prior to it.");
	}

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	str = " "+str;
	str.TrimRight();

	CString strHTML, strUpper(str);
	strUpper.MakeUpper();
	if( (nMsgType == REPORT_TEST_FAIL) || (nMsgType == REPORT_WARNING) || (nMsgType == REPORT_ERROR) ||  (nMsgType == REPORT_RED))
	{
		strHTML = "<font size=\"2\" color=\"#ff0000\">" + str + "</font><br />\r\n";
		str.Replace("&", "&&");
		m_lbColor.InsertString(-1, str, RGB(150, 55, 50));
		if(pApp->m_SessionFile.m_pStream) pApp->m_SessionFile.WriteString("r:"+str+"\n");

		
	}
	else if((nMsgType == REPORT_TEST_PASS) || (nMsgType == REPORT_GREEN))
	{
		strHTML = "<font size=\"2\" color=\"##009900\">" +str+ "</font><br /> \r\n";
		str.Replace("&", "&&");
		m_lbColor.InsertString(-1, str, RGB(20, 150, 75));
		if(pApp->m_SessionFile.m_pStream)pApp->m_SessionFile.WriteString("g:"+str+"\n");

		
	}
	else
	{
		strHTML = str+"<br /> \r\n";
		str.Replace("&", "&&");
		m_lbColor.InsertString(-1, str);
		if(pApp->m_SessionFile.m_pStream)pApp->m_SessionFile.WriteString("n:"+str+"\n");
	}

	pApp->m_SWAT_test_log_file.WriteString(strHTML);
	pApp->m_SWAT_test_log_file.Flush();

	if(str.Find("Completed session") != -1)//Draw a horizontal line
	{
		pApp->m_SWAT_test_log_file.WriteString("<hr /> \r\n");

		pApp->m_SessionFile.WriteString("--RESULTS--\r\n");

		pApp->AddSessionTestResultsToFile();
		pApp->m_SessionFile.Close();

	}

	pApp->m_SWAT_test_log_file.Flush();

	if(!m_bFreeze)
	{
		m_lbColor.SetTopIndex(m_lbColor.GetCount()-1);
	}

	m_lbColor.SetRedraw(TRUE); 

}

void CTabStatusDlg::ClearTestStatus()
{
	LOG_MESSAGE(LM_INFO,"Enter %s", __FUNCTION__);
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	int nCount = m_lbColor.GetCount();

	m_lbColor.SetRedraw(FALSE); 
  
	for (int I = nCount - 1; I >= 0; I--)
	{
		m_lbColor.DeleteString(I);
	}

	m_lbColor.SetRedraw(TRUE); 

	pApp->m_SWAT_test_log_file.Close();

	LOG_MESSAGE(LM_INFO,"Deleting %s", SWAT_TEST_LOG_FILE);
	DeleteFile(SWAT_TEST_LOG_FILE);

	pApp->m_SWAT_test_log_file.Open( SWAT_TEST_LOG_FILE, CFile::shareDenyWrite | CFile::modeCreate | CFile::modeWrite | CFile::typeText );
	
	pApp->m_SWAT_test_log_file.WriteString("Mime-Version: 1.0; \r\n");
	pApp->m_SWAT_test_log_file.WriteString("Content-Type: text/html; charset=\"ISO-8859-1\";  \r\n");
	pApp->m_SWAT_test_log_file.WriteString("Content-Transfer-Encoding: 7bit; \r\n\r\n");

	pApp->m_SWAT_test_log_file.WriteString("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"    \r\n");
	pApp->m_SWAT_test_log_file.WriteString("\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">      \r\n");
	pApp->m_SWAT_test_log_file.WriteString("<html xmlns=\"http://www.w3.org/1999/xhtml\"> \r\n\r\n");

	
	pApp->m_SWAT_test_log_file.WriteString("<span style=\"font-family: Lucida Console;\"> \r\n <font size = \"2\">");



	LOG_MESSAGE(LM_INFO,"Exit %s", __FUNCTION__);

}


BOOL CTabStatusDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_lbColor.SetHorizontalExtent(1000);  

	GetDlgItem(IDC_BUTTUN_OPEN)->EnableWindow(TRUE);


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTabStatusDlg::OnBnClickedCheckFreeze()
{
	if(BST_CHECKED == IsDlgButtonChecked(IDC_CHECK_FREEZE))
		m_bFreeze  = true;
	else
		m_bFreeze = false;

	
	return;
}

void CTabStatusDlg::OnBnClickedExpand()
{
	ShellExecute(0, "open", "IExplore.exe", SWAT_TEST_LOG_FILE, 0, SW_SHOW);
}






void CTabStatusDlg::OnLbnDblclkListBox()
{
	CSwatApp *pApp = (CSwatApp *)AfxGetApp();

	CString strLine;
	m_lbColor.GetText(m_lbColor.GetCurSel(),strLine);
//	strLine = ">>>...\\TestScripts\\SEP\\00_sep_libs\\Req\\sep_req_ventset.lua:873: GLOBALS are locked -- <<";
	CString strRight = strLine.Right(3);

	CString strLeft = strLine.Left(3);

	if((strLeft.Find(">>")!=-1) && (strRight.Find("<<")!=-1 ))
	{//it's a lua error with file name and line number

		int nStart = strLine.Find(':',6);
		if(nStart == -1 )return; //error does not contain file name

		CString strFileName = strLine.Left(nStart);
		strFileName = strFileName.Right(strFileName.GetLength()-4);
		if(strFileName.GetAt(2) != '\\')//don't know why but sometimes lua omits \ from the path, insert it here
		{
			strFileName.Insert(2,'\\'); 
		}
		strFileName.Replace("\\", "\\\\");
		
		if(strFileName.Left(2) == "..")//for this kinda file name: ...\TestScripts\SEP\00_sep_libs\Req\sep_req_ventset.lua:873: GLOBALS are locked -- <<
		{
			strFileName.Replace("..", pApp->m_strTestScriptPath.Left(2));
		}

		CString str4Line = strLine.Right(strLine.GetLength() - nStart-1);
		int nC = str4Line.Find(':');
		str4Line = str4Line.Left(nC);


		CString strCmd;
		if(pApp->m_strEditorPath.Find("Decoda.exe") != -1)
		{//decoda
			strFileName.Replace("\\\\", "\\");
			strCmd.Format("%s:%s", "\""+strFileName+"\"", str4Line);  //strCmd = "s:\\testscripts\\xyz.lua:144" 
			ShellExecute(0, "open", pApp->m_strEditorPath, strCmd, 0, SW_SHOW);
		}
		else 
		{//Scite
			strFileName.Replace("\\\\", "\\");
			strFileName.Replace("\\", "\\\\");
			strCmd.Format("\"-open:%s\" -goto:%s", strFileName, str4Line);  //strCmd = "-open:s:\\testscripts\\xyz.lua" -goto:144 
			ShellExecute(0, "open", pApp->m_strEditorPath, strCmd, 0, SW_SHOW);
		}
		
	}
	else if (strLine.Left(10).Find("Loading: ") > 0)
	{
		if(strLine.Find("Error")!= -1 ) return;

		CString strFileName = strLine.Right(strLine.GetLength() - 10);
		CString str4Line = "0";

		CString strCmd;
		if(pApp->m_strEditorPath.Right(9).CompareNoCase("Scite.exe") == 0)
		{//scite
			strFileName.Replace("\\\\", "\\");
			strCmd.Format("%s", strFileName);  //strCmd = "s:\\testscripts\\xyz.lua:144" 
			ShellExecute(0, "open", pApp->m_strEditorPath, strCmd, 0, SW_SHOW);
		}
		else 
		{//decoda
			strFileName.Replace("\\\\", "\\");
			strCmd.Format("%s:%s", "\""+strFileName+"\"", str4Line);  //strCmd = "s:\\testscripts\\xyz.lua:144" 
			ShellExecute(0, "open", pApp->m_strEditorPath, strCmd, 0, SW_SHOW);	
		}


	}
	else if (strLine.Left(9).CompareNoCase(" Running:") == 0)
	{
		if(strLine.Find("Error")!= -1 ) return;

		CString strFileName = strLine.Right(strLine.GetLength() - 10);
		CString str4Line = "0";

		CString strCmd;
		if(pApp->m_strEditorPath.Right(9).CompareNoCase("Scite.exe") == 0)
		{//scite
			strFileName.Replace("\\\\", "\\");
			strCmd.Format("%s", strFileName);  //strCmd = "s:\\testscripts\\xyz.lua:144" 
			ShellExecute(0, "open", pApp->m_strEditorPath, strCmd, 0, SW_SHOW);
		}
		else 
		{//decoda
			strFileName.Replace("\\\\", "\\");
			strCmd.Format("%s:%s", "\""+strFileName+"\"", str4Line);  //strCmd = "s:\\testscripts\\xyz.lua:144" 
			ShellExecute(0, "open", pApp->m_strEditorPath, strCmd, 0, SW_SHOW);	
		}


	}
	else if (strLine.Find("ftp://192.168.0.2:3021/SWAT")> 0)
	{
		if(pApp->PingVent())
		{
			ShellExecute(0, "open", "c:\\windows\\explorer.exe", "ftp://192.168.0.2:3021/SWAT", 0, SW_SHOW);
		}

	}

	else if (strLine.Find("Report file read only")> 0)
	{
		CString strFileName = strLine.Right(strLine.GetLength()-strLine.Find('>')-2);
		BOOL b = SetFileAttributes( strFileName, GetFileAttributes(strFileName) & ~FILE_ATTRIBUTE_READONLY); 

		if(b)
		{
			AfxMessageBox("Report file unlocked : "+strFileName);
		}
		else
		{
			AfxMessageBox("Error occured unlocking : "+strFileName, MB_OK|MB_ICONERROR);
		}
	}

}

void CTabStatusDlg::OnBnClickedButtonCopy()
{
	for (int i=0;i < m_lbColor.GetCount();i++)
	{
	   m_lbColor.SetSel(i);
	}


	m_lbColor.DoCopy();

	for (int i=0;i < m_lbColor.GetCount();i++)
	{
	   m_lbColor.SetSel(i, 0);
	}


}

void CTabStatusDlg::OnStatusCopyselected()
{
	// TODO: Add your command handler code here
}


void CTabStatusDlg::OnContextMenu(CWnd* pWnd, CPoint pt)
{
	// convert from screen to control coordinate
	CPoint pt1( pt );
	m_lbColor.ScreenToClient( &pt1 );
	BOOL bOutside;
	// check if you have clicked on an item
	UINT nItem = m_lbColor.ItemFromPoint( pt1, bOutside );
	if ( !bOutside )
	{
		// yes, select the item
		m_lbColor.SetCurSel( nItem );
		// do some other stuff
		
	}
}
void CTabStatusDlg::OnBnClickedButtonStatusClear()
{
	int nCount = m_lbColor.GetCount();
	for (int i=0;i < nCount;i++)
	{
	   m_lbColor.DeleteString(0);
	}
}

void CTabStatusDlg::OnEnChangeEditFind()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	CString strFind;
	GetDlgItem(IDC_EDIT_FIND)->GetWindowTextA(strFind);
	if(strFind.IsEmpty())
	{
		GetDlgItem(IDC_BUTTON_FIND)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_FIND_UP)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_BUTTON_FIND)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_FIND_UP)->EnableWindow(TRUE);
	}
}




void CTabStatusDlg::OnBnClickedButtonFind()
{
	int nCount = m_lbColor.GetCount();
	if(nCount < 1) return;

	int nStart = m_lbColor.GetCurSel();
//	if(nStart == -1 ) nStart = 0;
	
	UpdateData(1);
	CString strFind;
	GetDlgItem(IDC_EDIT_FIND)->GetWindowTextA(strFind);
	strFind.MakeUpper();


	CString str;
	for(int n(nStart+1); n < nCount; n++)
	{
		m_lbColor.GetText(n, str);
		str.MakeUpper();
		if(str.Find(strFind) != -1)
		{
			m_lbColor.SetCurSel(n);
			return;
		}
	}
	CString strMsg;
	strMsg.Format("Reached end of list. '%s' not found. Do you want to start from the beginning?",strFind);
	if(IDYES == AfxMessageBox(strMsg, MB_YESNO))
	{
		CString str;
		for(int n(0); n < nCount; n++)
		{
			m_lbColor.GetText(n, str);
			str.MakeUpper();
			if(str.Find(strFind) != -1)
			{
				m_lbColor.SetCurSel(n);
				return;
			}
		}
	}
}

void CTabStatusDlg::OnBnClickedButtonFindUp()
{
	int nCount = m_lbColor.GetCount();
	if(nCount < 1) return;

	int nStart = m_lbColor.GetCurSel();
//	if(nStart == -1 ) nStart = 0;
	
	UpdateData(1);
	CString strFind;
	GetDlgItem(IDC_EDIT_FIND)->GetWindowTextA(strFind);
	strFind.MakeUpper();


	CString str;
	for(int n(nStart-1); n > 1; n--)
	{
		m_lbColor.GetText(n, str);
		str.MakeUpper();
		if(str.Find(strFind) != -1)
		{
			m_lbColor.SetCurSel(n);
			return;
		}
	}
	CString strMsg;
	strMsg.Format("Reached end of list. '%s' not found. Do you want to start from the end again?",strFind);
	if(IDYES == AfxMessageBox(strMsg, MB_YESNO))
	{
		CString str;
		for(int n(nCount-1); n > 1; n--)
		{
			m_lbColor.GetText(n, str);
			str.MakeUpper();
			if(str.Find(strFind) != -1)
			{
				m_lbColor.SetCurSel(n);
				return;
			}
		}
	}
}

void CTabStatusDlg::OnBnClickedButtonCpyID()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	if(m_lbColor.GetCount() < 1) return;

	if(m_lbColor.GetCurSel()== -1)
	{
		AfxMessageBox("Please select a line in the log window to copy ID from.");
		return;
	}

	CString strLine;
	m_lbColor.GetText(m_lbColor.GetCurSel(),strLine);
	if(strLine.IsEmpty()) return; 

	//get ID from the line, if possible
	//15:48:55:90 ID_WAVEFORM_LAYOUT_BUTTON : Control_down : DOWN

	int nIDstart = strLine.Find("ID_");
	if(nIDstart != -1)
	{
		int nIDstop = strLine.Find(" ", nIDstart+1);
		strLine = strLine.Mid(nIDstart, nIDstop-nIDstart);
	}

	//copy strLine to the clipboard
	HGLOBAL hMem;
	hMem = GlobalAlloc(GMEM_FIXED, strLine.GetLength()+1);
	if(hMem == NULL) { 
		pApp->m_SWAT_log_file.WriteString("GlobalAlloc failed.");
		pApp->m_SWAT_log_file.Flush();
		return;
	}
	strcpy((char *) GlobalLock(hMem), strLine.GetBuffer());
	strLine.ReleaseBuffer();
	GlobalUnlock(hMem);
	::OpenClipboard(m_hWnd);
	EmptyClipboard();
	SetClipboardData(CF_TEXT, hMem);
	CloseClipboard();
}

//to open the status file generated by pApp->m_SessionFile
void CTabStatusDlg::OnBnClickedButtunOpen()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if((pApp->m_bScriptRunning) || (pApp->m_bRecording))
	{
		AfxMessageBox("Not available during script execution or recording.");
		return;
	}

	CFileDialog dlg(TRUE, NULL,NULL,0, "Saved log (log)|*.log|", NULL);
	dlg.m_ofn.lpstrInitialDir = "C:\\SWAT\\Temp\\log";

	if(dlg.DoModal() == IDCANCEL)
		return;

	CWaitCursor wait;

	CStdioFile file( dlg.GetPathName(), CFile::modeRead | CFile::typeText );
	CString str;

	OnBnClickedButtonStatusClear();

	//load the status log
	while(file.ReadString(str))
	{
		if(str.Find("--RESULTS--") != -1) break;

		if(str.Left(2).CompareNoCase("r:") == 0)
		{
			str = str.Right(str.GetLength()-2);
			m_lbColor.InsertString(-1, str, RGB(150, 55, 50));
		}
		else if(str.Left(2).CompareNoCase("g:") == 0)
		{
			str = str.Right(str.GetLength()-2);
			m_lbColor.InsertString(-1, str, RGB(20, 150, 75));
		}
		else if(str.Left(2).CompareNoCase("n:") == 0)
		{
			str = str.Right(str.GetLength()-2);
			m_lbColor.InsertString(-1, str);
		}
	}
	//now load the test results in the list box
	pApp->ReadSessionTestResultsFromFile(&file);

}
