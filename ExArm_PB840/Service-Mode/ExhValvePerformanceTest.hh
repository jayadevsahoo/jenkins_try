#ifndef ExhValvePerformanceTest_HH
#define ExhValvePerformanceTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  ExhValvePerformanceTest - Test exhalation valve performance
//	against the calibration table
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ExhValvePerformanceTest.hhv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "ExhValveCalibration.hh"

//@ End-Usage

//@ Constant: EV_PERF_TEST_NUM_TEST_POINTS
// # of test points
const Int32 EV_PERF_TEST_NUM_TEST_POINTS = 3;

class ExhValvePerformanceTest
{
  public:
    ExhValvePerformanceTest( void);
    ~ExhValvePerformanceTest( void);

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL, 
						   const char		*pPredicate = NULL) ;
	void runTest( void);
	  
  protected:

  private:
    ExhValvePerformanceTest( const ExhValvePerformanceTest&);  // not implemented...
    void operator=( const ExhValvePerformanceTest&);  // not implemented...

	//@ Data-Member: evTestPressure_[]
	// array of test pressures
	Real32 evTestPressure_[EV_PERF_TEST_NUM_TEST_POINTS];
};


#endif // ExhValvePerformanceTest_HH 
