
#ifndef TaskMonitorQueueMsg_HH
#define TaskMonitorQueueMsg_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: TaskMonitorQueueMsg - Task monitor queue message.
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/Safety-Net/vcssrc/TaskMonitorQueueMsg.hhv   25.0.4.0   19 Nov 2013 14:20:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001   By: sah   Date: 26-Mar-1997   DR Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//	Initial version to coding standards (Integration baseline).
//
//=====================================================================

#include "Sigma.hh"
#include "IpcIds.hh"
#include "InterTaskMessage.hh"
#include "SafetyNet.hh"


class TaskMonitorQueueMsg
{
  public:
    //@ Type: TaskMonitorMsgId  
    // Message id for TaskMonitor.
    enum TaskMonitorMsgId 
    { 
      // this ID is used by external subsystems (therefore don't change it)...
      TASK_MONITOR_MSG = FIRST_TASK_MONITOR_MSG, 

      // these IDs are only used internally; "PING" ID value must be same as
      // 'TASK_MONITOR_MSG'...
      PING_MSG_ID = TASK_MONITOR_MSG,
      RESPONSE_MSG_ID
    };

    TaskMonitorQueueMsg(const TaskMonitorMsgId msgId);
    inline TaskMonitorQueueMsg(const Int32 rawMsg);
    ~TaskMonitorQueueMsg(void);

    inline Int32             getRawMsg(void) const;
    inline TaskMonitorMsgId  getMsgId (void) const;

    inline IpcId  getRespondingTaskId(void) const;

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);

  private:
    // these methods are purposely declared, but not implemented...
    TaskMonitorQueueMsg(const TaskMonitorQueueMsg&);
    TaskMonitorQueueMsg(void);
    void  operator=(const TaskMonitorQueueMsg&);

    //@ Type: TaskMonMsg_
    // Message for TaskMonitor.
    union TaskMonMsg_
    {
      //@ Data-Member:  taskMsg
      // Contains the full message in the form of an ID and some data.
      InterTaskMessage  taskMsg;

      //@ Data-Member:  rawMsg
      // Contains the full message in the form of a 32-bit integer.
      Int32  rawMsg;
    };

    //@ Data-Member:  msg_
    // Contains the message in the TaskMonMsg_ union.
    TaskMonMsg_  msg_;
};


#include "TaskMonitorQueueMsg.in"


#endif // TaskMonitorQueueMsg_HH
