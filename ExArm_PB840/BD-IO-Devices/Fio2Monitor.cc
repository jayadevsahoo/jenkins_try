#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Fio2Monitor - implements the functionality required for 
//                       interfacing with the FiO2 monitor.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class implements the interface to the O2 sensor.
//  Every cycle, this class will compare the measured O2% with the 
//  target O2 mixture in use, and determine if the delivered O2% is
//  within the acceptable range.  This class notifies the Alarm-Analysis 
//  subsystem of the current status of the O2 monitoring.
//---------------------------------------------------------------------
//@ Rationale
//  This class implements the algorithm for monitoring the delivered O2%.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since the changes in the inspiratory pressure are expected to be much
//  faster than the response of the sensor, a 1.2 second inspiratory
//  pressure buffer is used to digitally slow it down to the same level
//  as that of the sensor for effective pressure compensation. 
//  A 1.0 second running average of the raw ADC counts of O2 sensor
//  channel is used to minimize noises and spikes.  Every cycle, after
//  both the inspiratory and O2 sensor sample buffers are filled, the
//  delivered O2% is calculated and compared to the current tolerance.
//  For the first hour, a +/- 12% tolerance is used and a +/- 17%
//  tolerance is used for the first four minutes after the target O2%
//  is changed.  For operation after the first hour, a +/- 7% tolerance
//  is used for normal operation and a +/- 12% tolerance is used for
//  the first four minutes after the target O2% is changed.  Any
//  calculated delivered O2% above the tolerance is declared as a HIGH
//  O2 and below the tolerance is declared a LOW O2 condition.
//  $[00401]
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @ (#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/Fio2Monitor.ccv   10.7   08/17/07 09:29:40   pvcs  
//
//@ Modification-Log
//
//  Revision: 018 By: rpr    Date: 26-Aug-2009   SCR Number: 6524
//  Project:  PROX
//	Description:
//		Controlled the BD_FIO2_CYCLE_EVENT to occur every ONE_FIO2_CYCLE_TIME.
//      when the sensor has not been calibrated.  Before this event occurred 
//      every 20 ms.
// 
//  Revision: 017 By: rpr    Date: 17-Dec-2008   SCR Number: 5956
//  Project:  S840
//	Description:
//		First time through newCycle() update alarm status.  Fixes issues when
//		power failures occur.
//
//  Revision: 016 By: syw    Date: 21-Sep-1999   DR Number: 5532
//  Project:  ATC
//	Description:
//		Use PendingContextHandle to get SettingId::FIO2_ENABLED status instead of
//		PhasedInContextHandle in newCycle().
//
//  Revision: 015 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 014 By: iv     Date: 13-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code report for background failure.
//
//  Revision: 013  By:  iv    Date:  09-Mar-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Changed currentlyInBreathingMode_() to include BiLevel scheduler.
//
//  Revision: 012 By: iv     Date: 13-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code report for background failure.
//
//  Revision: 011  By: iv     Date:  16-Dec-1997    DR Number: DCS 2701
//  	Project:  Sigma (840)
//        Description:
//		    Added code to make sure that a bound violation of the FiO2 sensor
//          can not be cleared by a 100% O2 calibration.  Disabling the Fio2 monitor
//          still clears any BTAT 8 device alert.
//
//  Revision: 010  By: syw    Date:  16-Dec-1997    DR Number: DCS 2685
//  	Project:  Sigma (840)
//        Description:
//			Added code to re-initialize and enable O2 sensor channel range test
//          in method calibrateFio2Sensor(). Fixed logic in newCycle() method 
//		    to reset background failure in pSafetyNetSensorData_ when the monitor
//		    is disabled.
//
//  Revision: 009  By: syw    Date:  24-Oct-1997    DR Number: DCS 2001
//  	Project:  Sigma (840)
//        Description:
//			Added requirement traceability.  Removed hardware document numbers
//			since they are traced to [00401].
//
//  Revision: 008  By: iv    Date:  16-Sep-1997    DR Number: DCS 2504
//  	Project:  Sigma (840)
//        Description:
//             Update NovRam only after calibration.
//
//  Revision: 007  By: syw   Date:  25-Jun-1997    DR Number: DCS 1926
//  	Project:  Sigma (840)
//        Description:
//             Do not report background error when SafetyNetSensorData
//             indicates background check failed.
//
//  Revision: 006  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 005  By: syw    Date: 28-Apr-1997    DR Number: DCS 1996
//      Project:  Sigma (840)
//        Description:
//          Added requirement tracing.
//
//  Revision: 004  By:  iv    Date:  23-Apr-1997   DR Number: 1954, 1956, 1927 
//       Project:  Sigma (840)
//       Description:
//             Fixed tolerances for low and high FIO2 readings.
//             Fixed transmission to GUI of oor o2 measurements.
//
//  Revision: 003  By:  by    Date:  15-Mar-1997   DR Number: 1891 
//       Project:  Sigma (840)
//       Description:
//             Fixed the reset back to normal condition in newCycle().
//
//  Revision: 002  By:  by    Date:  15-Mar-1997   DR Number: 1834 
//       Project:  Sigma (840)
//       Description:
//             After the user enables the O2 monitor, re-initialize, and
//             enable the SafetyNetSensorData object for O2 sensor channel.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996   DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "Fio2Monitor.hh"
#include "MainSensorRefs.hh"
#include "PressureSensor.hh"

#include "BdQueuesMsg.hh"
#include "MsgQueue.hh"
#include "IpcIds.hh"
#include "RegisterRefs.hh"
#include "VentObjectRefs.hh"
#include "MiscSensorRefs.hh"
#include "NovRamManager.hh"
#include "TaskControlAgent.hh"
#include "MathUtilities.hh"
#include "Background.hh"
#include "SchedulerId.hh"
#include "PhasedInContextHandle.hh"
#include "PendingContextHandle.hh"
#include "BreathPhaseScheduler.hh"
#include "Barometer.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//@ Constant: MONITOR_INSTALLED_MASK
//  mask to read the monitor installed bit data
static const Uint16 MONITOR_INSTALLED_MASK = 0x0800;

//@ Constant: MIN_O2_PERCENTAGE_TOLERANCE
// Minimum acceptable O2 percentage 
static const Real32 MIN_O2_PERCENTAGE_TOLERANCE = 18.0F;
 
//@ Constant: NORMAL_O2_DELIVERY_TOLERANCE 
//  Maximum acceptable O2 error percentage from target percentage
//  After 4 minutes of a change in target O2% by user 
static const Real32 NORMAL_O2_DELIVERY_TOLERANCE = 7.0F;
 
//@ Constant: WARMUP_O2_DELIVERY_TOLERANCE 
// Maximum acceptable O2 error percentage from target percentage
// after a change of target O2% by user
static const Real32 WARMUP_O2_DELIVERY_TOLERANCE = 12.0F;
 
//@ Constant: MAX_O2_DELIVERY_TOLERANCE 
// Maximum acceptable O2 error percentage from target percentage
// after a change of target O2% by user during warm-up period
static const Real32 MAX_O2_DELIVERY_TOLERANCE = 17.0F;
 
//////////////////////////////////////////////////////////////////////////////
// These are alpha constants are for a sampling rate of 20MS frequency
//////////////////////////////////////////////////////////////////////////////
//@ Constant: INSP_PRES_ALPHA1 
//  inpiratory pressure alpha filter 1 constant 
static const Real32 INSP_PRES_ALPHA1 = 0.994858611F;
 
//@ Constant: INSP_PRES_ALPHA2 
//  inpiratory pressure alpha filter 2 constant 
static const Real32 INSP_PRES_ALPHA2 = 0.983739837F;
 
//@ Constant: INSP_PRES_ALPHA3 
//  inpiratory pressure alpha filter 3 constant 
static const Real32 INSP_PRES_ALPHA3 = 0.947368421F;

//@ Constant: ONE_FIO2_SECOND 
//  One Fio2 second based on a 20 MS resolution 
static const Uint16 ONE_FIO2_SECOND = 50;

//@ Constant: ONE_FIO2_MINUTE
//  One Fio2 minute based on a 20 MS resolution 
static const Uint16 ONE_FIO2_MINUTE = 60 * ONE_FIO2_SECOND;

//@ Constant: ONE_FIO2_HOUR
//  One Fio2 hour based on a 20 MS resolution 
static const Uint32 ONE_FIO2_HOUR = 60 * ONE_FIO2_MINUTE;

//@ Constant: ONE_FIO2_CYCLE_TIME 
//  One Fio2 cycle time based on a 20 MS resolution 
static const Uint16 ONE_FIO2_CYCLE_TIME = ONE_FIO2_SECOND;

//@ Constant: TIME_REQUIRED_TO_FILL_IP_BUFFER 
//  number of cycles required to fill both the 
//  inspiratory (1.2sec) and O2 sample buffers (1.0sec)
static const Uint16 TIME_REQUIRED_TO_FILL_IP_BUFFER = MAX_INSP_PRES_SAMPLES;

//@ Constant: MAX_100_PERCENT_O2_COUNT 
//  Maximum O2 sensor raw count resolution 
// TODO E600 MS Come up with more accurate numbers when available
static const Uint16 MAX_100_PERCENT_O2_COUNT = 4000;

//@ Constant: MIN_O2_SENSOR_COUNT_RESOLUTION
//  Minimum O2 sensor raw count resolution 
// TODO E600 MS Come up with more accurate numbers when available
static const Uint16 MIN_O2_SENSOR_COUNT_RESOLUTION = 10;

//@ Constant: MAX_TOLERANT_O2_PERCENT_READING 
//  Maximum allowable O2% reading before a FiO2 sensor is declared a failure 
static const Real32 MAX_TOLERANT_O2_PERCENT_READING = 108.0F;

//@ Constant: MAX_HIGH_FIO2_ALARM_SETTING 
//  Maximum allowable O2% setting before the high FiO2 alarm is disabled 
static const Real32 MAX_HIGH_FIO2_ALARM_SETTING = 93.0F;

//@ Static Data-Member: AlarmResetOccurred_ 
// Indicates whether the operator has pressed the Alarm Reset switch
Boolean Fio2Monitor::AlarmResetOccurred_ = FALSE;
             
//@ Static Data-Member: IsInstalled_
// Indicates whether the monitor is installed
Boolean Fio2Monitor::IsInstalled_ = FALSE;
             
//@ Static Data-Member: IsEnabled_
// Indicates whether the monitor is enabled
Boolean Fio2Monitor::IsEnabled_ = FALSE;
             
//@ Static Data-Member: TargetO2Percent_ 
// Indicates the current user set O2 percent 
Real32 Fio2Monitor::TargetO2Percent_ = 21.0F;             

//@ Static Data-Member: PrevTargetO2Percent_
// Indicates the previous user set O2 percent
Real32 Fio2Monitor::PrevTargetO2Percent_ = 21.0F;

Real32 Fio2Monitor::CurrentO2PercentUpperBound_ = 0.0F;
Real32 Fio2Monitor::CurrentO2PercentLowerBound_ = 0.0F;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Fio2Monitor()  [Constructor]
//
//@ Interface-Description
//  This constructor takes an enum for the ADC channel id and a pointer
//  to the SafetyNetSensorData as arguments.  The argument values are
//  passed to the base class, Sensor, constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method initializes the monitorInstalled_ BinaryIndicator,
//  the three FilteredValues inspPresAlphaFilter1_, inspPresAlphaFilter2_,
//  and inspPresAlphaFilter3_.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

Fio2Monitor::Fio2Monitor( const AdcChannels::AdcChannelId AdcId) // E600 BDIO,
// E600 BDIO                          SafetyNetSensorData *pData)
: 
             inspPresAlphaFilter1_( INSP_PRES_ALPHA1 ),
             inspPresAlphaFilter2_( INSP_PRES_ALPHA2 ),
             inspPresAlphaFilter3_( INSP_PRES_ALPHA3 ), // E600 BDIO,
             Sensor( AdcId /*, pData*/ )
{
    // $[TI1]
    CALL_TRACE("Fio2Monitor::Fio2Monitor( const AdcChannels::AdcChannelId, \
                                          SafetyNetSensorData *pData )" );

    // retrieve FiO2 sensor calibration information from Nov-RAM
    getFio2CalInfoFromNovRam_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Fio2Monitor()  [Destructor]
//
//@ Interface-Description
//  Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

Fio2Monitor::~Fio2Monitor(void)
{
     CALL_TRACE("Fio2Monitor::~Fio2Monitor(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//  Invoked every BD secondary cycle. When the Fio2 sensor is enabled
//  every second it determines the status of the O2% delivered to the
//  patient.  This method takes no parameters and has no return value.
//  $[03067] $[05096] $[05097] $[05148] $[05149] $[05179] $[05180] 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The delivered O2% is compared to the value in use by the 
//  Breath-Delivery subsystem to determine if the delivered Fio2
//  is high normal or low.  Once this has been determined, the alarm
//  analysis subsystem is notified if the status has changed.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Fio2Monitor::newCycle( void )
{

    CALL_TRACE("Fio2Monitor::newCycle(void)");

    BdQueuesMsg fio2Msg;

    // continuously take inspiratory pressure
    takeInspPresSample_();

    // continuously take O2 sensor samples
    takeO2SensorSample_();

    // increament the first hour power-up timer
    ++timeSincePowerup_;

    // increment the four (4) minute timer
    ++fourMinuteChangeO2PercentageTimer_;


    // Determine if the the Fio2 Monitor annunciation is enabled/disabled
    Boolean isEnabled = PendingContextHandle::GetDiscreteValue( SettingId::FIO2_ENABLED);

    // First time through force update of alarm status
	// fixes resyncing issues when a power failure occurs
    // Check if the enabled status has changed
    if ( ( isEnabled != IsEnabled_ ) || (timeSincePowerup_ == 1) )
    {
    // $[TI1]

        // If status has gone from enabled to disabled and the
        // fio2Status is not normal, reset it to normal and cancel
        // outstanding alarm events.
        if ( ! isEnabled && ( fio2AlarmStatus_ != NORMAL ) )
        {
        // $[TI1.1]
            fio2AlarmStatus_ = NORMAL;

            // send FIO2 Normal message to alarm analysis subsystem

            pAlarmCallBack_(BdAlarmId::BDALARM_FIO2_NORMAL, FALSE);
        }
        // $[TI1.2]
       
        if ( ! isEnabled && isO2SensorOor_ )
        {
        // $[TI1.3]
            isO2SensorOor_ = FALSE;

			// TODO E600 MS put back in when ported.
            //Background::ReportBkEvent( BK_O2_SENSOR_OOR_RESET );
		    //pSafetyNetSensorData_->setBackgndCheckFailed( FALSE );
        }
        // $[TI1.4]

        if ( ! isEnabled )
		{
        // $[TI1.5]
		    // disable O2 sensor channel range test
			// TODO E600 MS put back in when ported.
		    //pSafetyNetSensorData_->setCheckedInSensorNewCycle( FALSE );
        }
		else
		{
        // $[TI1.6]
		    // re-initialize and enable O2 sensor channel range test

			// TODO E600 MS put back in when ported.
            //pSafetyNetSensorData_->resetNumCyclesOor();
		    //pSafetyNetSensorData_->setCheckedInSensorNewCycle( TRUE );
		    //pSafetyNetSensorData_->setBackgndEventReported( FALSE );
		}
        IsEnabled_ = isEnabled;
    }

    // $[TI2]
#if E600_TO_CHECK
    if ( ( pSafetyNetSensorData_->getBackgndCheckFailed() ) &&
	     ( ! isO2SensorOor_ ) )
	{
    // $[TI3]
        isO2SensorOor_ = TRUE;
    }
#endif
    // $[TI4]

    if ( IsInstalled_ && IsEnabled_ )
    {
    // $[TI5]
        ++fio2AlarmTimer_;
    }
    // $[TI6]

    if ( ( TRUE == stillInWarmUpPeriod_ ) && ( timeSincePowerup_ > ONE_FIO2_HOUR ) )
    {
    // $[TI7]
        stillInWarmUpPeriod_ = FALSE;
    }
    // $[TI8]
    if ( FALSE == IsEquivalent( TargetO2Percent_, PrevTargetO2Percent_, ONES ) )
    {
    // $[TI9]
        // O2% setting increase or decrease or unchanged ?
        if ( TargetO2Percent_ > PrevTargetO2Percent_ )
        {
        // $[TI9.1]
            if ( stillInWarmUpPeriod_ )
            {
            // $[TI9.1.1]
                targetO2status_ = WARM_UP_PERIOD_O2_INCREASED;
            }
            else
            {
            // $[TI9.1.2]
                targetO2status_ = TARGET_O2_INCREASED;
            }
        }
        else 				// TargetO2Percent_ < PrevTargetO2Percent_
        {
        // $[TI9.2]
            if ( stillInWarmUpPeriod_ )
            {
            // $[TI9.2.1]
                targetO2status_ = WARM_UP_PERIOD_O2_DECREASED;
            }
            else
            {
            // $[TI9.2.2]
                targetO2status_ = TARGET_O2_DECREASED;
            }
        }

        // update previous target O2% with current O2%
        PrevTargetO2Percent_ = TargetO2Percent_;

        // start the four (4) minuter timer
        fourMinuteChangeO2PercentageTimer_ = 0;
    }
    // $[TI10]
    if ( fourMinuteChangeO2PercentageTimer_ > ( 4 * ONE_FIO2_MINUTE ) )
    {
    // $[TI11]
        if ( stillInWarmUpPeriod_ )
        {
        // $[TI11.1]
            targetO2status_ = WARM_UP_PERIOD_O2_UNCHANGED;
        }
        else
        {
        // $[TI11.2]
            targetO2status_ = TARGET_O2_UNCHANGED;
        }
    }
    // $[TI12]

    // set-up current O2 percentage upper and lower bounds
    switch( targetO2status_ )
    {
        case TARGET_O2_INCREASED:
        // $[TI13]
            CurrentO2PercentUpperBound_ = NORMAL_O2_DELIVERY_TOLERANCE;
            CurrentO2PercentLowerBound_ = WARMUP_O2_DELIVERY_TOLERANCE;
            break;

        case TARGET_O2_DECREASED:
        // $[TI14]
            CurrentO2PercentUpperBound_ = WARMUP_O2_DELIVERY_TOLERANCE;
            CurrentO2PercentLowerBound_ = NORMAL_O2_DELIVERY_TOLERANCE;
            break;

        case WARM_UP_PERIOD_O2_INCREASED:    
        // $[TI22]
            CurrentO2PercentUpperBound_ = WARMUP_O2_DELIVERY_TOLERANCE;
            CurrentO2PercentLowerBound_ = MAX_O2_DELIVERY_TOLERANCE;
            break;

        case WARM_UP_PERIOD_O2_DECREASED:    
        // $[TI15]
            CurrentO2PercentUpperBound_ = MAX_O2_DELIVERY_TOLERANCE;
            CurrentO2PercentLowerBound_ = WARMUP_O2_DELIVERY_TOLERANCE;
            break;

        case WARM_UP_PERIOD_O2_UNCHANGED: 
        // $[TI16]
            CurrentO2PercentUpperBound_ = WARMUP_O2_DELIVERY_TOLERANCE;
            CurrentO2PercentLowerBound_ = WARMUP_O2_DELIVERY_TOLERANCE;
            break;

        case TARGET_O2_UNCHANGED: 
        // $[TI17]
            CurrentO2PercentUpperBound_ = NORMAL_O2_DELIVERY_TOLERANCE;
            CurrentO2PercentLowerBound_ = NORMAL_O2_DELIVERY_TOLERANCE;
            break;
            
        default:
            CLASS_ASSERTION(
                ( TARGET_O2_INCREASED == targetO2status_ ) ||
                ( TARGET_O2_DECREASED == targetO2status_ ) ||
                ( WARM_UP_PERIOD_O2_INCREASED == targetO2status_ ) ||
                ( WARM_UP_PERIOD_O2_DECREASED == targetO2status_ ) ||
                ( WARM_UP_PERIOD_O2_UNCHANGED == targetO2status_ ) ||
                ( TARGET_O2_UNCHANGED == targetO2status_ ) );
            break;
    }

    if ( ( IsInstalled_ ) && ( IsEnabled_ ) &&
         ( 0 == ( fio2AlarmTimer_ % ONE_FIO2_CYCLE_TIME ) ) &&
         ( timeSincePowerup_ >= TIME_REQUIRED_TO_FILL_IP_BUFFER ) &&
         ( FALSE == isO2SensorOor_ ) )
    {
    // $[TI19]

        calculateO2Percentage_();

        if ( ( fio2AlarmStatus_ != NORMAL ) &&
             ( ( TRUE == AlarmResetOccurred_ ) ||
               ( ( currentO2Percentage_ >=
                   TargetO2Percent_ - CurrentO2PercentLowerBound_ ) &&
                   currentO2Percentage_ > MIN_O2_PERCENTAGE_TOLERANCE &&
                 ( ( currentO2Percentage_ <=
                     TargetO2Percent_ + CurrentO2PercentUpperBound_ ||
                     TargetO2Percent_ >= MAX_HIGH_FIO2_ALARM_SETTING ) ) ) ) )
        {
        // $[TI19.1]
            AlarmResetOccurred_ = FALSE;

            fio2AlarmStatus_ = NORMAL;

            // send FIO2 Normal message to alarm analysis subsystem


			pAlarmCallBack_(BdAlarmId::BDALARM_FIO2_NORMAL, FALSE);
        }
        else if ( ( fio2AlarmStatus_ != HIGH ) &&
                  ( TRUE == currentlyInBreathingMode_() ) &&
                  ( currentO2Percentage_ > 
                    TargetO2Percent_ + CurrentO2PercentUpperBound_ ) &&
                  ( TargetO2Percent_ < MAX_HIGH_FIO2_ALARM_SETTING ) )
        {
        // $[TI19.2]

            fio2AlarmStatus_ = HIGH;

            // send FIO2 High message to alarm analysis subsystem
            pAlarmCallBack_(BdAlarmId::BDALARM_FIO2_HIGH, FALSE);
        }
        else if ( ( fio2AlarmStatus_ != LOW ) &&
                  ( TRUE == currentlyInBreathingMode_() ) &&
                  ( ( currentO2Percentage_ < 
                      TargetO2Percent_ - CurrentO2PercentLowerBound_ ) ||
                    ( currentO2Percentage_ <= MIN_O2_PERCENTAGE_TOLERANCE ) ) )
        {
        // $[TI19.3]

            fio2AlarmStatus_ = LOW;

            // send FIO2 Low message to alarm analysis subsystem

            pAlarmCallBack_(BdAlarmId::BDALARM_FIO2_LOW, FALSE);
        }
        // $[TI19.4]
    }  
    else if ( TRUE == isO2SensorOor_ &&
			( 0 == ( fio2AlarmTimer_ % ONE_FIO2_CYCLE_TIME ) ) &&
              timeSincePowerup_ >= TIME_REQUIRED_TO_FILL_IP_BUFFER )
    {
    // $[TI20]
	    // current O2% is set to -1 to nofity GUI not to display O2%
        currentO2Percentage_ = -1.0F;
        // Notified BD Status Task a new calculated delivered O2 % is ready
	    // So that it may be sent over to be displayed on the GUI
        fio2Msg.event.eventType = BdQueuesMsg::BD_FIO2_CYCLE_EVENT;
        CLASS_ASSERTION (MsgQueue::PutMsg (BD_STATUS_TASK_Q, fio2Msg.qWord) == Ipc::OK);
    } 
    // $[TI21] //  not installed or O2 sensor is disabled, do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize()
//
//@ Interface-Description
//
//  This method takes no arguments and has no return value.  It sets up
//  the Fio2Monitor object to an initial known state.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  The monitor status is unconditionally set to normal.  The Boolean
//  data member IsInstalled_ is set based on the installation status of
//  the monitor. 
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Fio2Monitor::initialize( void )
{
    CALL_TRACE("Fio2Monitor::initialize( void )");

    // initialize inspiratory pressure circular buffer 
    inspPresBuffer_.currentNdx = 0;
    inspPresBuffer_.numSamplesInBuffer = 0;

	Uint16 tmpNdx;
    for ( tmpNdx = 0; tmpNdx < MAX_INSP_PRES_SAMPLES; ++tmpNdx )
    {
	    // $[TI3]
        inspPresBuffer_.sampleBuffer_[ tmpNdx ] = 0;
    }

    // initialize O2 sensor circular buffer
    o2SensorSamples_.currentNdx = 0;
    o2SensorSamples_.sampleAccumulator = 0;
    o2SensorSamples_.currentRunningAvg = 0;
    o2SensorSamples_.bufferFull = FALSE;

    for ( tmpNdx = 0; tmpNdx < MAX_O2_SENSOR_SAMPLES; ++tmpNdx )
    {
	    // $[TI4]
        o2SensorSamples_.o2SensorRawCountBuffer[ tmpNdx ] = 0;
    }

    // initialize all three alpha filters
    inspPresAlphaFilter1_.initValues( 0.0F, 0.0F, 0.0F, 0.0F );
    inspPresAlphaFilter2_.initValues( 0.0F, 0.0F, 0.0F, 0.0F );
    inspPresAlphaFilter3_.initValues( 0.0F, 0.0F, 0.0F, 0.0F );

    // initialize Fio2Monitor timers
    fio2AlarmTimer_ = 0;
    timeSincePowerup_ = 0;
    fourMinuteChangeO2PercentageTimer_ = 0;

    // initialize O2 % deviance tolerance
    stillInWarmUpPeriod_ = TRUE;
    targetO2status_ = WARM_UP_PERIOD_O2_UNCHANGED;
    CurrentO2PercentUpperBound_ = WARMUP_O2_DELIVERY_TOLERANCE;
    CurrentO2PercentLowerBound_ = WARMUP_O2_DELIVERY_TOLERANCE;

    // initialize the previous target O2% to the current
    PrevTargetO2Percent_ = TargetO2Percent_;

    // initialize current O2 percentage to target O2 
    currentO2Percentage_ = TargetO2Percent_;

    SAFE_CLASS_ASSERTION( fio2SensorCalInfo_.countAt100Percent
	                      != fio2SensorCalInfo_.countAt0Percent );

    // calculate the O2 sensor gain in terms of cmH2O per count
    o2SensorGain_ = fio2SensorCalInfo_.calInspPresAt100Percent
                  / (Real32) ( ( fio2SensorCalInfo_.countAt100Percent -
                                 fio2SensorCalInfo_.countAt0Percent ) );

	// TODO E600 MS determine whether O2 sensor is always installed.
    if ( TRUE /*monitorInstalled_.getState() == BinaryIndicator::OFF*/ )
    {
    // $[TI1]
        IsInstalled_ = TRUE;
        fio2AlarmStatus_ = NORMAL;
	    isO2SensorOor_ = FALSE;
    }
    else
    {
    // $[TI2]
        IsInstalled_ = FALSE;
        fio2AlarmStatus_ = NOT_INSTALLED;
	    isO2SensorOor_ = TRUE;
    }

    // Determine if the the Fio2 Monitor annunciation is enabled/disabled
    IsEnabled_ = PhasedInContextHandle::GetDiscreteValue( SettingId::FIO2_ENABLED );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialStatusNotification
//
//@ Interface-Description
//  This method takes no arguments and has no return value. It is invoked
//  after system initialization and whenever communications is restored
//  to notify Alarms of initial Fio2 Monitor status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The private data members IsEnabled_ and fio2AlarmStatus_ are used to
//  determine if the FIO2 Monitor is installed, enabaled,  and what FIO2 
//  indication should be sent to Alarms.
//  The Alarm-Analysis subsystem is then notified of the status.
//---------------------------------------------------------------------
//@ PreCondition
//  fio2AlarmStatus_ must have a valid value
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
Fio2Monitor::initialStatusNotification( void )
{
    CALL_TRACE("Fio2Monitor::initialStatusNotification(void)");

    switch ( fio2AlarmStatus_ )
    {
        case NORMAL:
            // $[TI1]
            // send messages O2 Monitor Installed
            // and FIO2 Normal to Alarm-Analysis subsystem
            pAlarmCallBack_(BdAlarmId::BDALARM_O2_MONITOR_INSTALLED, TRUE);
            pAlarmCallBack_(BdAlarmId::BDALARM_FIO2_NORMAL, TRUE);
            break;

        case HIGH:
            // $[TI2]
            // send messages O2 Monitor Installed
            // and FIO2 High to Alarm-Analysis subsystem
            pAlarmCallBack_(BdAlarmId::BDALARM_O2_MONITOR_INSTALLED, TRUE);

            // If the monitor is enabled, send the high alarm status; otherwise
            // send normal status as the operator 
            if (IsEnabled_)
            {
            // $[TI2.1]
                pAlarmCallBack_(BdAlarmId::BDALARM_FIO2_HIGH, TRUE);
            }
            else
            {
            // $[TI2.2]
                pAlarmCallBack_(BdAlarmId::BDALARM_FIO2_NORMAL, TRUE);
            }
            break;
        
        case LOW:
            // $[TI3]
            // send messages O2 Monitor Installed
            // and FIO2 Low to Alarm-Analysis subsystem
            pAlarmCallBack_(BdAlarmId::BDALARM_O2_MONITOR_INSTALLED, TRUE);

            // If the monitor is enabled, send the low alarm status; otherwise
            // send normal status as the operator 
            if (IsEnabled_)
            {
            // $[TI3.1]
                pAlarmCallBack_(BdAlarmId::BDALARM_FIO2_LOW, TRUE);
            }
            else
            {
            // $[TI3.2]
                pAlarmCallBack_(BdAlarmId::BDALARM_FIO2_NORMAL, TRUE);
            }
            break;

        case NOT_INSTALLED:
            // $[TI4]
            // send message O2 Monitor Not Installed
            pAlarmCallBack_(BdAlarmId::BDALARM_NOT_O2_MONITOR_INSTALLED, TRUE);
            break;

        default:
            CLASS_ASSERTION( (fio2AlarmStatus_ == NORMAL) ||
                                  (fio2AlarmStatus_ == HIGH) ||
                                  (fio2AlarmStatus_ == LOW) ||
                                  (fio2AlarmStatus_ == NOT_INSTALLED) );
    }
}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method: calibrateFio2Sensor()
//
//@ Interface-Description
//  This method accepts a Real32 argument and no argument is returned.
//-----------------------------------------------------------------------------
//@ Implementation-Description
//  This method is intented to be called at the termination of a 2 minute
//  100% O2 suction.  The raw ADC counts at 100% is recorded and analyzed
//  for failure.  If the raw count value is acceptable then the FiO2
//  calibration data is updated internally.  Lastly, the Fio2 calibration
//  data in the NovRamManager is updated with these new values.  
//
//  $[04317] $[06168]
//-----------------------------------------------------------------------------
//@ PreCondition
//  ( mixSetting >= 21.0F ) && ( mixSetting <= 100.0F )
//-----------------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=============================================================================

void
Fio2Monitor::calibrateFio2Sensor( const Real32 mixSetting )
{
    CALL_TRACE("Fio2Monitor::calibrateFio2Sensor( const Real32 mixSetting )");

    CLASS_PRE_CONDITION( ( mixSetting >= 21.0F ) && ( mixSetting <= 100.0F ) );

    Real32 totalPressure = 0.0F;

    Uint16 o2CountsAt100Percent = o2SensorSamples_.currentRunningAvg;

	// TODO E600 MS put the safety net stuff back in later.
    if ( o2CountsAt100Percent - fio2SensorCalInfo_.countAt0Percent 
             > MIN_O2_SENSOR_COUNT_RESOLUTION  &&
         o2CountsAt100Percent <= MAX_100_PERCENT_O2_COUNT  /*&&
		 ! pSafetyNetSensorData_->getBackgndCheckFailed()*/ )
    {
    // $[TI1]
        //////////////////////////////////////////////////////////////////////
        // Ptotal = Patm * 70.317 + Pg_filtered3
        //////////////////////////////////////////////////////////////////////

        totalPressure = RAtmosphericPressureSensor.getValue() 
                      + inspPresAlphaFilter3_.getFilteredValue();

        //////////////////////////////////////////////////////////////////////
        // PpO2_cal = ( u2 * (100 - mix_setting) + mix_setting / 100 ) * Ptotal
        //////////////////////////////////////////////////////////////////////

        fio2SensorCalInfo_.calInspPresAt100Percent
            = ( ( 0.981F * ( 100.0F - mixSetting ) + mixSetting ) / 100.0F )
            * totalPressure;
    
        fio2SensorCalInfo_.countAt100Percent = o2CountsAt100Percent;

        o2SensorGain_ = fio2SensorCalInfo_.calInspPresAt100Percent
                      / (Real32) ( ( fio2SensorCalInfo_.countAt100Percent -
                                     fio2SensorCalInfo_.countAt0Percent ) );

        // store calibration into NOVRAM for FiO2 sensor
        updateFio2CalInfoToNovRam_();

        if ( TRUE == isO2SensorOor_ )
        {
        // $[TI1.1]
            isO2SensorOor_ = FALSE;
		    // re-initialize report status

			// TODO E600 MS Put back when ported...
		    //pSafetyNetSensorData_->setBackgndEventReported( FALSE );

            if ( TRUE == IsEnabled_ )
            {
            // $[TI1.1.1]
				// TODO E600 MS put back in when ported.
                //Background::ReportBkEvent( BK_O2_SENSOR_OOR_RESET );
            }
            // $[TI1.1.2]
        }
        // $[TI1.2]
    }
    else
    {
    // $[TI2]
        isO2SensorOor_ = TRUE;

        if ( TRUE == IsEnabled_ )
        {
        // $[TI2.1]
            Uint16 errorCode = o2CountsAt100Percent;
            //specify counts in error code
            errorCode |= 0x4000; 
			// TODO E600 MS put back in when ported.
            //Background::ReportBkEvent( BK_O2_SENSOR_OOR, errorCode );
        }                
        // $[TI2.2]
    }
}


#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
//----------------------------------------------------------------------------
// updateValue
//----------------------------------------------------------------------------
void Fio2Monitor::updateValue(void)
{
    Sensor::updateValue();
    Sensor::updateVirtualValue();
}
#endif // defined (SIGMA_BD_CPU)
#endif // INTEGRATION_TEST_ENABLE


#ifdef SIGMA_DEVELOPMENT 

#include <stdio.h>

void
Fio2Monitor::displayFio2MonitorInfo( void )
{
    printf("\nCurrent Fio2Monitor Information:\n");

    switch ( fio2AlarmStatus_ )
    {
        case NOT_INSTALLED:
            printf("fio2AlarmStatus_ is NOT_INSTALLED.\n");
            break;

        case NORMAL:
            printf("fio2AlarmStatus_ is NORMAL.\n");
            break;

        case HIGH:
            printf("fio2AlarmStatus_ is HIGH.\n");
            break;

        case LOW:
            printf("fio2AlarmStatus_ is LOW.\n");
            break;

        default:
            printf("fio2AlarmStatus_ is error.\n");
            break;
    }

    printf("IsInstalled_ = %d.\n", IsInstalled_ );

    printf("IsEnabled_ = %d.\n", IsEnabled_ );

    printf("stillInWarmUpPeriod_ = %d.\n", stillInWarmUpPeriod_ );

    printf("isO2SensorOor_ = %d.\n", isO2SensorOor_ );

    printf("inspPresBuffer_.numSamplesInBuffer = %d.\n",
            inspPresBuffer_.numSamplesInBuffer );

    printf("inspPresBuffer_.currentNdx = %d.\n",
            inspPresBuffer_.currentNdx );

    printf("inspPresAlphaFilter1_.filteredValue = %8.6f.\n",
            inspPresAlphaFilter1_.getFilteredValue() );

    printf("inspPresAlphaFilter2_.filteredValue = %8.6f.\n",
            inspPresAlphaFilter2_.getFilteredValue() );

    printf("inspPresAlphaFilter3_.filteredValue = %8.6f.\n",
            inspPresAlphaFilter3_.getFilteredValue() );

    printf("AtmosphericPressureSensor = %8.4f.\n",
            RAtmosphericPressureSensor.getValue() );

    printf("o2SensorGain_ = %8.6f.\n", o2SensorGain_ );

    printf("current partial O2 pressure = %8.4f.\n", getValue() );

    printf("currentO2Percentage_ = %8.6f.\n", currentO2Percentage_ );

    printf("calInspPresAt100Percent = %8.4f.\n",
            fio2SensorCalInfo_.calInspPresAt100Percent );

    printf("fio2SensorCalInfo_.countAt100Percent = %d.\n",
            fio2SensorCalInfo_.countAt100Percent );

    printf("fio2SensorCalInfo_.countAt0Percent = %d.\n",
            fio2SensorCalInfo_.countAt0Percent );

    printf("o2SensorSamples_.currentNdx = %d.\n", o2SensorSamples_.currentNdx );
    printf("o2SensorSamples_.bufferFull = %d.\n", o2SensorSamples_.bufferFull );

    printf("o2SensorSamples_.currentRunningAvg = %d.\n",
            o2SensorSamples_.currentRunningAvg );

    printf("targetO2status_ = 0x%X.\n", targetO2status_ );

    printf("fourMinuteChangeO2PercentageTimer_ = %ld.\n",
            fourMinuteChangeO2PercentageTimer_ );

    printf("fio2AlarmTimer_ = %ld.\n", fio2AlarmTimer_ );
    printf("timeSincePowerup_ = %ld.\n", timeSincePowerup_ );

    printf("TargetO2Percent_ = %6.3f.\n", TargetO2Percent_ );
    printf("PrevTargetO2Percent_ = %6.3f.\n", PrevTargetO2Percent_ );

    printf("CurrentO2PercentUpperBound_ = %6.3f.\n", CurrentO2PercentUpperBound_);
    printf("CurrentO2PercentLowerBound_ = %6.3f.\n", CurrentO2PercentLowerBound_);
}

#endif // SIGMA_DEVELOPMENT 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Fio2Monitor::SoftFault( const SoftFaultID  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, FIO2MONITOR,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: rawToEngValue_()
//
//@ Interface-Description
//  This method takes one argument, ADC raw counts, and returns a Real32.
//  This is a virtual method that converts the raw ADC counts to
//  its respective engineering value, O2%.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Return the running average value.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

Real32
Fio2Monitor::rawToEngValue_( const AdcCounts count ) const
{
    CALL_TRACE("Fio2Monitor::rawToEngValue_( const AdcCounts count)");
     
    CLASS_PRE_CONDITION( count <= MAX_COUNT_VALUE );
          
    //////////////////////////////////////////////////////////////////
    // PpO2 = ((Count - Count0%) / (Count100% - Count0%)) * PpO2Cal //
    //////////////////////////////////////////////////////////////////

    // $[TI1]
    return( ( o2SensorSamples_.currentRunningAvg 
              - fio2SensorCalInfo_.countAt0Percent ) * o2SensorGain_ );
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
  
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFio2CalInfoFromNovRam_()
//
//@ Interface-Description
//  This method accepts no argument and returns no value.
//
//  This method retrieves FiO2 Monitor calibration information 
//  via Fio2CalInfo from NOVRAM and updates the corresponding data
//  members in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Fio2Monitor::getFio2CalInfoFromNovRam_( void )
{
    CALL_TRACE("Fio2Monitor::getFio2CalInfoFromNovRam_( void )");

    // $[TI1]
    Fio2CalInfo fio2CalInfo;

    // retrieve calibration info from NOVRAM for FiO2 sensor
    NovRamManager::GetFio2CalInfo( fio2CalInfo );

    fio2SensorCalInfo_ = fio2CalInfo.getNovRamFio2CalInfo();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFio2CalInfoToNovRam_()
//
//@ Interface-Description
//  This method accepts no argument and returns no value.
//
//  This method updates the NOVRAM with the current Fio2Monitor
//  calibration information via Fio2CalInfo object.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Fio2Monitor::updateFio2CalInfoToNovRam_( void )
{
    CALL_TRACE("Fio2Monitor::updateFio2CalInfoToNovRam_( void )");

    // $[TI1]
    Fio2CalInfo fio2CalInfo;

    fio2CalInfo.setNovRamFio2CalInfo( fio2SensorCalInfo_ );

    // store calibration into NOVRAM for FiO2 sensor
    NovRamManager::UpdateFio2CalInfo( fio2CalInfo );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeInspPresSample_ 
//
//@ Interface-Description
//  This private method accepts no argument and return a void.
//  Since the changes in the inspiratory pressure are expected to be much
//  faster than the response of the sensor, a 1.2 second inspiratory
//  pressure buffer is used to digitally slow it down to the same level
//  as that of the sensor for effective pressure compensation.  This 
//  method is responsible for storing and maintaining the Pi buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is called every BD cycle.  It verifies that the
//  inspiratory pressure buffer is full.  The earliest Pi is used in
//  conjuction with 3 sequential alpha filters.  The earliest Pi is 
//  over written with a current Pi is cycle after the buffer is full.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Fio2Monitor::takeInspPresSample_( void )
{
    CALL_TRACE("Fio2Monitor::takeInspPresSample_( void )");

    // re-adjust the sample count and the earlist sample index
    if ( inspPresBuffer_.numSamplesInBuffer >= MAX_INSP_PRES_SAMPLES )
    {
    // $[TI1]
        ///////////////////////////////////////////////////////////////////
        // re-calculate the three alpha filters
        ///////////////////////////////////////////////////////////////////
        // Pg_filtered1( t ) = INSP_PRES_ALPHA1 * Pg_filtered1( t - 1 ) 
        //                   + ( 1 - INSP_PRES_ALPHA1 ) * Pg_delayed( t )
        ///////////////////////////////////////////////////////////////////

        inspPresAlphaFilter1_.updateValues(
            inspPresBuffer_.sampleBuffer_[ inspPresBuffer_.currentNdx ]
                                           );

        ////////////////////////////////////////////////////////////////////
        // Pg_filtered2( t ) = INSP_PRES_ALPHA2 * Pg_filtered2( t - 1 ) 
        //                   + ( 1 - INSP_PRES_ALPHA2 ) * Pg_filtered1( t )
        ////////////////////////////////////////////////////////////////////

        inspPresAlphaFilter2_.
            updateValues( inspPresAlphaFilter1_.getFilteredValue() );

        ////////////////////////////////////////////////////////////////////
        // Pg_filtered3( t ) = INSP_PRES_ALPHA3 * Pg_filtered3( t - 1 ) 
        //                   + ( 1 - INSP_PRES_ALPHA3 ) * Pg_filtered2( t )
        ////////////////////////////////////////////////////////////////////

        inspPresAlphaFilter3_.
            updateValues( inspPresAlphaFilter2_.getFilteredValue() );
    }
    else
    {
    // $[TI2]
        ++(inspPresBuffer_.numSamplesInBuffer);
    }

    // record an inspiratory pressure sample
    inspPresBuffer_.sampleBuffer_[ inspPresBuffer_.currentNdx ] 
        = RInspPressureSensor.getValue();

    // increment the buffer index
    ++(inspPresBuffer_.currentNdx);

    if ( inspPresBuffer_.currentNdx >= MAX_INSP_PRES_SAMPLES )
    {
    // $[TI3]
        inspPresBuffer_.currentNdx = 0;
    }
    // $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeO2SensorSample_ 
//
//@ Interface-Description
//  This method accepts no argument and returns no value.
//  This method is responsible for maintaining a 1 second running 
//  average of the raw ADC counts of the O2 sensor channel.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A circular buffer is used to maintain a one second running average
//  of the raw ADC O2 sensor counts.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Fio2Monitor::takeO2SensorSample_( void )
{
    CALL_TRACE("Fio2Monitor::takeO2SensorSample_( void )");

    if ( o2SensorSamples_.currentNdx >= MAX_O2_SENSOR_SAMPLES )
    {
    // $[TI1]
        o2SensorSamples_.currentNdx = 0;
        o2SensorSamples_.bufferFull = TRUE;
    }
    // $[TI2]

    Uint16 currentNdx = o2SensorSamples_.currentNdx;

    if ( TRUE == o2SensorSamples_.bufferFull )
	{
    // $[TI3]
        o2SensorSamples_.sampleAccumulator
            -= o2SensorSamples_.o2SensorRawCountBuffer[ currentNdx ];
    }
    // $[TI4]

    o2SensorSamples_.o2SensorRawCountBuffer[ currentNdx ] = getCount() ;

    o2SensorSamples_.sampleAccumulator
        += o2SensorSamples_.o2SensorRawCountBuffer[ currentNdx ];

    ++(o2SensorSamples_.currentNdx);

    if ( TRUE == o2SensorSamples_.bufferFull )
    {
    // $[TI5]
        o2SensorSamples_.currentRunningAvg
            = o2SensorSamples_.sampleAccumulator / MAX_O2_SENSOR_SAMPLES;
    }
	else
	{
    // $[TI6]
        o2SensorSamples_.currentRunningAvg
            = o2SensorSamples_.sampleAccumulator / o2SensorSamples_.currentNdx;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateO2Percentage_ 
//
//@ Interface-Description
//  This method accepts no argument and returns no value.
//
//  This method is responsible for calculating the current delivered
//  O2 percentage.  The delivered O2% is calculated only if BD is
//  currently in a breathing mode.  If a reading of greater than
//	MAX_TOLERANT_O2_PERCENT_READING is derived, the O2 sensor is declared OOR.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses inspiratory pressure compensation to calculate
//  the current O2% delivery.  When the BD is not in breathing mode,
//  the calculated O2% is set to the target O2% to avoid any false
//  alarms.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
Fio2Monitor::calculateO2Percentage_( void )
{
    CALL_TRACE("Fio2Monitor::calculateO2Percentage_( void )");

    BdQueuesMsg fio2Msg;

    if ( TRUE == currentlyInBreathingMode_() )
    {
    // $[TI1]
        ////////////////////////////////////////////////////////////////
        // O2% = 100 * PpO2 / Ptotal
        //     = 100 * PpO2 / ( Patm * 70.317 + Pg_filtered3 )
        ////////////////////////////////////////////////////////////////
		
		// TODO E600 MS Determine if this should change based on the e600
		// algorithm
        currentO2Percentage_ = (Real32) ( ( 100.0F * getValue() )
                             / ( RAtmosphericPressureSensor.getValue()
                             + inspPresAlphaFilter3_.getFilteredValue() ) );

        // possible FiO2 sensor failure, disable FiO2 monitoring
        if ( currentO2Percentage_ > MAX_TOLERANT_O2_PERCENT_READING )
        {
        // $[TI1.1]
            isO2SensorOor_ = TRUE;

            if ( TRUE == IsEnabled_ )
            {
            // $[TI1.1.1]
                Uint16 errorCode = Uint16(currentO2Percentage_);
                //specify engineering units in error code
                errorCode |= 0x8000; 
				// TODO E600 MS put back in when ported.
                //Background::ReportBkEvent( BK_O2_SENSOR_OOR, errorCode );
            }
            // $[TI1.1.2]
        }
        // $[TI1.2]
    }
    else
    {
    // $[TI2]
        currentO2Percentage_ = TargetO2Percent_;
    }

    // Notified BD Status Task a new calculated delivered O2 % is ready
	// So that it may be sent over to be displayed on the GUI
    fio2Msg.event.eventType = BdQueuesMsg::BD_FIO2_CYCLE_EVENT;
    CLASS_ASSERTION (MsgQueue::PutMsg (BD_STATUS_TASK_Q, fio2Msg.qWord) == Ipc::OK);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: currentlyInBreathingMode_
//
//@ Interface-Description
//  This method accepts no argument and returns a Boolean. 
//  This method interrogates BD to determine if it is currently in
//  breathing mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method will return a Boolean indicating whether the
//  system is currently in a breathing mode or not.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
 
Boolean
Fio2Monitor::currentlyInBreathingMode_( void )
{
    SchedulerId::SchedulerIdValue currentSchedulerId;
    Boolean rtnStatus = FALSE;

    BreathPhaseScheduler&
    rCurrentScheduler = BreathPhaseScheduler::GetCurrentScheduler();

    currentSchedulerId = rCurrentScheduler.getId();

	if ( currentSchedulerId >= SchedulerId::FIRST_BREATHING_SCHEDULER &&
         currentSchedulerId <= SchedulerId::LAST_BREATHING_SCHEDULER)
    {
    // $[TI1]
        rtnStatus = TRUE;
    }
    // $[TI2]

    return( rtnStatus );
}










