#include "stdafx.h"
#if	defined(SIGMA_GUI_CPU) || defined(SIGMA_DEVELOPMENT)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PDTimerRegistrar - The central registration, control, and
// dispatch point for all Patient Data timer events. All the Timer events
// that are required by the Patient Data Manager subsystem are queued
// on the GuiApp's User Annuncition Queue and handled by the GuiApp 
// task.  The GuiApp task passes along timer events directed
// to the Patient Data Manager subsystem by calling the
// PDTimerRegistrar::TimerEventHappened() method.
//---------------------------------------------------------------------
//@ Interface-Description
//    A PDtimerTarget or its subclass object can register itself
//    with the PDTimerRegistrar class to start a timer event and
//    receive a timer event when a specified time interval passes.
//---------------------------------------------------------------------
//@ Rationale
// This class is necessary to register, set, and centrally dispatch timer
// events which are received from the Operating-System subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class doesn't get instantiated, the class provides static methods
// only. The class is implemented with the TimerArray_[] as the
// centerpiece. 
// This is an array of private PDTimerRecord records -- one for each
// unique timer id.  The PDTimerId acts as the index into the TimerArray_[].
// The RegisterTarget() method is used to initialize the pointer
// slots of the TimerArray_[] -- this is done on-the-fly as needed.
//
// Timer events are communicated to the registrar via the
// TimerEventHappened() method.  This then calls the
// timerEventHappened() of the object which is registered as a target
// of the particular timer event.
//---------------------------------------------------------------------
//@ Fault-Handling
// Class assertions and pre-conditions are used to verify the
// validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// PDTimerRegistrar allows only one object at a time to be registered
// for a particular timer.  "Un-registering" is not allowed.
// If you register for a timer you stay registered for it until 
// another object registers for the same timer.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PDTimerRegistrar.ccv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: rhj    Date: 09-March-2009    SCR Number: 6454
//  Project:  840S
//  Description:
//      Added an inspiratory tidal volume alarm timer.
//  
//  Revision: 007   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 006  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 005  By: rhj   Date:  07-Nov-2008  SCR Number: 6435
//  Project:  840S
//  Description:
//      Changed the inspired tidal volume's timer to 
//      INSP_TIDAL_VOL_TIMEOUT_INTERVAL.
// 
//  Revision: 004  By: rhj   Date:  03-Oct-2006  SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added Cdyn, Rdyn, C20/c and PSF timers.
//
//  Revision: 003  By: hct   Date:  19-Jan-99    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added ATC functionality.
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  26-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "PDTimerRegistrar.hh"

//@ Usage-Classes
#include "PDTimerId.hh"
#include "PDTimerTarget.hh"
#include "IpcIds.hh"
#include "MsgTimer.hh"
//@ End-Usage

//@ Code...

//
// Word-aligned memory pools for static member data.
//
static Uint TimerArrayMemory_ [
		((sizeof(PDTimerRegistrar::PDTimerRecord)  + sizeof(Uint) - 1)
		 / sizeof(Uint)) * PDTimerId::NUM_TIMER_IDS];

//
// Initializations of static member data.
//
PDTimerRegistrar::PDTimerRecord *PDTimerRegistrar::TimerArray_ =
					(PDTimerRegistrar::PDTimerRecord *) ::TimerArrayMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PDTimerRecord()  [Default Constructor]
//
//@ Interface-Description
// Constructs and initializes the private PDTimerRecord structure within
// PDTimerRegistrar.  Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The constructor doesn't get called but it exists to satisfy
//  the compiler reference.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PDTimerRecordInRegistrar::PDTimerRecord(void) :
	msgTimer(0, 0, 0)
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimerEventHappened
//
//@ Interface-Description
// Called due to a timer event message from the operating system (comes
// via a timer task).  It is passed the event message sent by the timer
// task.  We decode the timer id from this message.  The object associated
// with this timer is then informed of the event by calling the object's
// corresponding timerEventHappened() method.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is the central dispatch point for all Patient Data timer events.
// The 'timerEventHappened' method on a registered target object for
// a given timer id is called.
// timerId is used as an index into TimerArray_[] to find the target
// object.
//---------------------------------------------------------------------
//@ PreCondition
// The target object for the given 'timerId' must exist and timer has to
// activated.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PDTimerRegistrar::TimerEventHappened(UserAnnunciationMsg &eventMsg)
{
	PDTimerId::PDTimerIdType timerId =
					(PDTimerId::PDTimerIdType)eventMsg.timerParts.timerId;

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < PDTimerId::NUM_TIMER_IDS);
	CLASS_PRE_CONDITION(TimerArray_[timerId].pTarget != NULL);
	CLASS_PRE_CONDITION (TimerArray_[timerId].isActive);

	TimerArray_[timerId].pTarget->timerEventHappened(timerId);
	//$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterTarget [public, static]
//
//@ Interface-Description
// Objects needing to know when timer events occur use this method to
// register themselves for timer notices.  The parameters are as follows:
// >Von
//	timerId		The enum id of the timer associated with the target object.
//	pTarget		ptr to the object to be notified when the timer event given by
//				'timerId' occurs.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Registers the given target with the associated with the given 'timerId' by
// storing the given pointer into the appropriate slot of the TimerArray_[].
//---------------------------------------------------------------------
//@ PreCondition
// timerId must be valid and pTarget must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PDTimerRegistrar::RegisterTarget(PDTimerId::PDTimerIdType timerId,
								  PDTimerTarget* pTarget)
{
	CALL_TRACE("PDTimerRegistrar::RegisterTarget("
	                            "PDTimerId::PDTimerIdType timerId, "
	                            "PDTimerTarget* pTarget)");

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < PDTimerId::NUM_TIMER_IDS);
	CLASS_PRE_CONDITION(pTarget != NULL);

	// Cancel an existing timer, if any.
	CancelTimer(timerId);
	SAFE_CLASS_ASSERTION(FALSE == TimerArray_[timerId].isActive);

	// Register the new target.
	TimerArray_[timerId].pTarget = pTarget;	
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartTimer [public, static]
//
//@ Interface-Description
// Starts the specified timer.  It is passed 'timerId', the enum id of
// the timer associated with the target object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Starts the corresponding timer as long as something is registered
// for it.
//---------------------------------------------------------------------
//@ PreCondition
//	The timerId must be valid and a timer target must have been 
//  registered.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PDTimerRegistrar::StartTimer(PDTimerId::PDTimerIdType timerId)
{
	CALL_TRACE("PDTimerRegistrar::StartTimer("
									"PDTimerId::PDTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < PDTimerId::NUM_TIMER_IDS);
	CLASS_PRE_CONDITION(TimerArray_[timerId].pTarget != NULL);

	// Set the timer going
	TimerArray_[timerId].msgTimer.set();
	TimerArray_[timerId].isActive = TRUE;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelTimer [public, static]
//
//@ Interface-Description
//  Cancels the specified timer.  It is passed 'timerId', the enum id of
//  the timer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Cancels the MsgTimer pointed to by timerId and marks the timer as
//  inactive.
//---------------------------------------------------------------------
//@ PreCondition
//	timerId must be a valid timer.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PDTimerRegistrar::CancelTimer(PDTimerId::PDTimerIdType timerId)
{
	CALL_TRACE("PDTimerRegistrar::CancelTimer("
									"PDTimerId::PDTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId >= 0 && timerId < PDTimerId::NUM_TIMER_IDS);
	if (TimerArray_[timerId].pTarget != NULL); // $[TI1]
	{
		TimerArray_[timerId].msgTimer.cancel();
		TimerArray_[timerId].isActive = FALSE;
	} // Else $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [public, static]
//
//@ Interface-Description
// Called at startup by the Patient Data subsystem in order to initialize
// static member data.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Performs one-time initialization of static member data.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PDTimerRegistrar::Initialize(void)
{
	CALL_TRACE("PDTimerRegistrar::Initialize(void)");
#if	defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
	UserAnnunciationMsg timerEventMsg;		// 32 bits of data that each
											// MsgTimer will put on our input
											// queue to identify timeout event

	timerEventMsg.timerParts.eventType = UserAnnunciationMsg::PATIENT_DATA_TIMER_EVENT;
#endif	//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

	// First, clear all flags and target pointers.
	for (Int32 i = 0; i < PDTimerId::NUM_TIMER_IDS; i++)
	{
		TimerArray_[i].pTarget = NULL;
		TimerArray_[i].isActive = FALSE; 
	}

#	define TIMER_Q_ID	USER_ANNUNCIATION_Q

#if	defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
	// Create a timer for the Patient Data Management subsystem's spontaneous
	// exhaled tidal volume time out, 2 minutes.
	timerEventMsg.timerParts.timerId  = 
 	(GuiTimerId::GuiTimerIdType) PDTimerId::SPONT_TIDAL_VOL_TIMER;
	new (&(TimerArray_[PDTimerId::SPONT_TIDAL_VOL_TIMER].msgTimer))
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 
				TIDAL_VOLUME_TIMEOUT_INTERVAL, MsgTimer::ONE_SHOT);

	// Create a timer for the Patient Data Management Subsystem's mandatory
	// tidal volume time out, 2 minutes.
	timerEventMsg.timerParts.timerId  = 
 	(GuiTimerId::GuiTimerIdType) PDTimerId::MAND_TIDAL_VOL_TIMER;
	new (&(TimerArray_[PDTimerId::MAND_TIDAL_VOL_TIMER].msgTimer))
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 
				TIDAL_VOLUME_TIMEOUT_INTERVAL, MsgTimer::ONE_SHOT);

	// Create a timer for the Patient Data Management subsystem's 
	// inspired tidal volume time out, 2 minutes.
	timerEventMsg.timerParts.timerId  = 
 	(GuiTimerId::GuiTimerIdType) PDTimerId::INSP_TIDAL_VOL_TIMER;
	new (&(TimerArray_[PDTimerId::INSP_TIDAL_VOL_TIMER].msgTimer))
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 
				INSP_TIDAL_VOL_TIMEOUT_INTERVAL, MsgTimer::ONE_SHOT);

	// timer for the dynamic compliance data, 2 minutes.
	timerEventMsg.timerParts.timerId  = 
	(GuiTimerId::GuiTimerIdType) PDTimerId::DYNAMIC_COMPLIANCE_TIMER;
	new (&(TimerArray_[PDTimerId::DYNAMIC_COMPLIANCE_TIMER].msgTimer))
	MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 
			 DYNAMIC_MECHANICS_TIMEOUT_INTERVAL, MsgTimer::ONE_SHOT);

	// timer for the dynamic resistance data, 2 minutes.
	timerEventMsg.timerParts.timerId  = 
	(GuiTimerId::GuiTimerIdType) PDTimerId::DYNAMIC_RESISTANCE_TIMER;
	new (&(TimerArray_[PDTimerId::DYNAMIC_RESISTANCE_TIMER].msgTimer))
	MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 
			 DYNAMIC_MECHANICS_TIMEOUT_INTERVAL, MsgTimer::ONE_SHOT);

	// timer for the C20/C data, 2 minutes.
	timerEventMsg.timerParts.timerId  = 
	(GuiTimerId::GuiTimerIdType) PDTimerId::C20_TO_C_RATIO_TIMER;
	new (&(TimerArray_[PDTimerId::C20_TO_C_RATIO_TIMER].msgTimer))
	MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 
			 DYNAMIC_MECHANICS_TIMEOUT_INTERVAL, MsgTimer::ONE_SHOT);

	// timer for the peak spontaneous flow data, 2 minutes.
	timerEventMsg.timerParts.timerId  = 
	(GuiTimerId::GuiTimerIdType) PDTimerId::PEAK_SPONT_INSP_FLOW_TIMER;
	new (&(TimerArray_[PDTimerId::PEAK_SPONT_INSP_FLOW_TIMER].msgTimer))
	MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 
			 DYNAMIC_MECHANICS_TIMEOUT_INTERVAL, MsgTimer::ONE_SHOT);

	// Create a timer for the Patient Data Management subsystem's 
	// inspired tidal volume alarm time out, 2 minutes.
	timerEventMsg.timerParts.timerId  = 
 	(GuiTimerId::GuiTimerIdType) PDTimerId::INSP_TIDAL_VOL_ALARM_TIMER;
	new (&(TimerArray_[PDTimerId::INSP_TIDAL_VOL_ALARM_TIMER].msgTimer))
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 
				INSP_TIDAL_VOL_TIMEOUT_INTERVAL, MsgTimer::ONE_SHOT);


#if defined(SIGMA_DEVELOPMENT)  && defined(SIGMA_UNIT_TEST)
	timerEventMsg.timerParts.timerId  = 
 	(GuiTimerId::GuiTimerIdType) PDTimerId::WAVEFORM_DATA_TIMER;
	new (&(TimerArray_[PDTimerId::WAVEFORM_DATA_TIMER].msgTimer)) 
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 10000,
					MsgTimer::PERIODIC);

	timerEventMsg.timerParts.timerId  = 
 	(GuiTimerId::GuiTimerIdType) PDTimerId::END_BREATH_DATA_TIMER;
	new (&(TimerArray_[PDTimerId::END_BREATH_DATA_TIMER].msgTimer)) 
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 30000,
					MsgTimer::PERIODIC);

	timerEventMsg.timerParts.timerId  = 
 	(GuiTimerId::GuiTimerIdType) PDTimerId::REALTIME_DATA_TIMER;
	new (&(TimerArray_[PDTimerId::REALTIME_DATA_TIMER].msgTimer)) 
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 40000,
					MsgTimer::PERIODIC);

	timerEventMsg.timerParts.timerId  = 
 	(GuiTimerId::GuiTimerIdType) PDTimerId::AVERAGE_DATA_TIMER;
	new (&(TimerArray_[PDTimerId::AVERAGE_DATA_TIMER].msgTimer)) 
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 25000,
					MsgTimer::PERIODIC);

	timerEventMsg.timerParts.timerId  = 
 	(GuiTimerId::GuiTimerIdType) PDTimerId::PEEND_DATA_TIMER;
	new (&(TimerArray_[PDTimerId::PEEND_DATA_TIMER].msgTimer)) 
		MsgTimer(TIMER_Q_ID, timerEventMsg.qWord, 60000,
					MsgTimer::PERIODIC);
#endif //defined(SIGMA_DEVELOPMENT) && defined(SIGMA_UNIT_TEST)
#endif //defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
	//$[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
PDTimerRegistrar::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("PDTimerRegistrar::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, PATIENT_DATA, 
				PATIENT_DATA_TIMER_REGISTRAR, lineNumber,
										pFileName, pPredicate);
}

#endif	// defiend(SIGMA_GUI_CPU) || defined(SIGMA_GUI_DEVELOPMENT)
