#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdSystemState - Keeps track on the state of the system for recovery purposes
//---------------------------------------------------------------------
//@ Interface-Description
// A NovRam object, implemented as a double buffer to keep BD information
// in synch through power interruptions. It provides services such as a
// access methods, copy constructor, and assignment operator.
//---------------------------------------------------------------------
//@ Rationale
// Information that has to be preserved through power interruption is stored
// in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The class stores information to indicate if apnea is active, if safetyPcv is
// active, the current scheduler id, and whether or not 100% O2 is requested.
// It provides access methods for these variables, a default constructor that
// assigns default values for these variables, a copy constructor, and an
// assignment operator.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdSystemState.ccv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
// 
// 
//  Revision: 008   By: rpr    Date: 23-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support code review comments.
// 
//  Revision: 006  By: rpr     Date:  24-Oct-2008    DR Number: 6435
//  Project:  S840
//  Description:
//      Added isCalibrateO2Requested_ to support +20 O2
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per unit test peer review
//             Changed testable item in constructor.
//
//  Revision: 003  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 002  By:  iv    Date:  15-Jan-1996    DR Number: DCS 1297
//       Project:  Sigma (R8027)
//       Description:
//             Deleted O2 interval timer from Novram.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BdSystemState.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdSystemState()  [Default Contstructor]
//
//@ Interface-Description
// Default constructor, takes no arguments. Initializes data members to
// default values.
//---------------------------------------------------------------------
//@ Implementation-Description
// Set defaults values as follows:
// SchedulerId is set for safety pcv, 100 percent O2 request is set to false,
// apnea active is set to false, safety pcv active is set to true.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdSystemState::BdSystemState(void)
{	// $[TI1]
  CALL_TRACE("BdSystemState()");

	schedulerId_ = SchedulerId::SAFETY_PCV;
	is100PercentO2Requested_ = FALSE;
	isCalibrateO2Requested_ = FALSE;
	isApneaActive_ = FALSE;
	isSafetyPcvActive_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdSystemState()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdSystemState::~BdSystemState(void)
{
	CALL_TRACE("~BdSystemState()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdSystemState  [Copy Constructor]
//
//@ Interface-Description
// Copy constructor. Accepts a const BsSystemState& as argument.
// It copies all data members from the class passed in to the  constructed 
// object members.
//---------------------------------------------------------------------
//@ Implementation-Description
// The members of the object passed in are copied to the members of the
// constructed object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdSystemState::BdSystemState(const BdSystemState& rBdState)
{	// $[TI2]
	CALL_TRACE("BdSystemState::BdSystemState(const BdSystemState& rBdState)");

	schedulerId_ = rBdState.schedulerId_;
	is100PercentO2Requested_ = rBdState.is100PercentO2Requested_;
	isCalibrateO2Requested_ = rBdState.isCalibrateO2Requested_;
	isApneaActive_ = rBdState.isApneaActive_;
	isSafetyPcvActive_ = rBdState.isSafetyPcvActive_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=  [Assignment Operator]
//
//@ Interface-Description
// Assignment operator.  Accepts a const BsSystemState& as argument and 
// has no return value.
// Copies all data members from the class passed in to the members 
// of this object.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the object passed in is not the reference of this object then the 
// members of the passed in object are copied to the members of this
// object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdSystemState::operator=(const BdSystemState& rBdState)
{	// $[TI1]
	if (this != &rBdState)
	{
	// $[TI1.1]
		schedulerId_ = rBdState.schedulerId_;
		is100PercentO2Requested_ = rBdState.is100PercentO2Requested_;
		isCalibrateO2Requested_ = rBdState.isCalibrateO2Requested_;
		isApneaActive_ = rBdState.isApneaActive_;
		isSafetyPcvActive_ = rBdState.isSafetyPcvActive_;
	} // $[TI1.2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BdSystemState::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BDSYSTEMSTATE,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//=====================================================================
//
//  Private Methods...
//
//=====================================================================







