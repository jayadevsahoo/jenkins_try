#include "stdafx.h"
//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2010, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Class:  VsetInterface
//---------------------------------------------------------------------
//@ Interface-Description
//  VsetInterface is a SerialInterface that handles communications and
//  command processing for the VSET command protocol. This protocol
//  allows for an external controller to set vent settings through the
//  serial communications port.
//---------------------------------------------------------------------
//@ Rationale
//  Provides VSET command processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness of 
//  parameterized data. Incoming commands are verified for correctness
//  and an error report issued for commands in error.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//  @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/VsetInterface.ccv   25.0.4.0   19 Nov 2013 14:12:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  22-FEB-2010    SCR Number: 6663
//  Project:  API/MAT
//  Description:
//	Initial version.
//=====================================================================

#include "Sigma.hh"
#include "VsetInterface.hh"
#include <stdio.h>

//@ Usage-Classes
#include "Dci.hh"

#include "IpcIds.hh"
#include "SerialPort.hh"

#include "TaskInfo.hh"
#include "SoftwareOptions.hh"
#include "OsTimeStamp.hh"
#include "VsetServer.hh"
#include "SerialQueue.hh"
#include "Task.hh"
//@ End-Usage

//@ Code...
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VsetInterface [constructor]
//
//@ Interface-Description
//  Constructor for the VsetInterface class. Instantiates the VentSet 
//  protocol interface on the specified SerialPort.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VsetInterface::VsetInterface( SerialPort & rSerialPort,
                              SerialInterface::PortNum portNum ) :
SerialInterface( rSerialPort, portNum ),
rDci_(Dci::GetDci(portNum)),
rVsetServer_(VsetServer::GetVsetServer(portNum_))
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VsetInterface [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VsetInterface::~VsetInterface()
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startPrint
//
//@ Interface-Description
//  Someone's trying to print on the DCI interface. Cancel the 
//  operation and continue.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
VsetInterface::startPrint()
{
    return TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCurrentPrinter_
//
//@ Interface-Description
//  Someone's trying to assign the printer to a DCI interface. Do
//  nothing so the assignment has no real effect.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VsetInterface::setCurrentPrinter_()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendNak_
//
//@ Interface-Description
//  Writes a NAK packet with the specified reason code to the serial
//  port.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
VsetInterface::sendNak_(Int reason)
{
    char msg[3];
    msg[0] = '\x15';
    msg[1] = reason;
    msg[2] = '\0';
    write(msg, strlen(msg), 0);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequests_
//
//@ Interface-Description
//  Reads and processes serial command input from the serial port using
//  the VentSet command protocol and packet interface.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Issues one logio read to retrieve as much data as possibe from the 
//  logio buffer and writes this data to a SerialQueue. A state machine
//  then processes this data, checking the validity of the data using 
//  the embedded checksum. If the packet is valid, this method calls
//  processCmd_ to process the encapsulated command line.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VsetInterface::processRequests_(void)
{

    static const unsigned char SYNC1_CHAR = 0xAA;
    static const unsigned char SYNC2_CHAR= 0xAB;
    static const Uint16 MAX_PKT_SIZE = 1500;
    static const Uint16 SOM_INDEX = 4;
    static const Uint16 CHKSUM_LENGTH = 4;

    Uint state = 1;
    Uint msgBytesRemaining = 0;
    Uint msgBytesRead = 0;
    unsigned char msg[MAX_PKT_SIZE];
    unsigned char& sync1 = msg[0];
    unsigned char& sync2 = msg[1];
    unsigned char& nbf1 = msg[2];
    unsigned char& nbf2 = msg[3];

    isPacketProtocolEnabled_ = TRUE;

    OsTimeStamp msgTimer;
    msgTimer.invalidate();


    while ( !areSettingsChanged_ )
    {
        // idle function as lowest priority task
        Task::Yield();    

		// TODO E600 put this in a common location
		static const Uint32 LOGIO_CHARBUF_SIZE = 200;
        unsigned char portBuffer[LOGIO_CHARBUF_SIZE];
        Uint bytesRead = read(portBuffer, sizeof( portBuffer ), LOGIO_CHARBUF_SIZE/2);     // get contents of logio buffer

        for ( int ix=0; ix<bytesRead; ix++ )
        {
            rxQueue_.insert( portBuffer[ix] );  // this could be placed in ISR
        }

        if ( rxQueue_.inUseCount() <= 0 )
        {
            // nothing in the receive queue
            static const Int MSG_TIMEOUT_MS = 2000;
            if ( msgTimer.isValid() && msgTimer.getPassedTime() > MSG_TIMEOUT_MS )
            {
                rSerialPort_.reset();
                sendNak_(NAK_TIMEOUT);
                msgTimer.invalidate();
                state = 1;
            }
            continue;
        }

        while ( rxQueue_.inUseCount() > 0 )
        {
            switch ( state )
            {
            case 1:
                rxQueue_.peek(&sync1, 1);
                if ( sync1 == SYNC1_CHAR )
                {
                    msgTimer.now();
                    state = 2;
                }
                break;
            case 2:
                rxQueue_.peek(&sync2, 1);
                if ( sync2 == SYNC2_CHAR )
                {
                    state = 3;
                }
                else
                {
                    state = 1;
                }
                break;
            case 3:
                rxQueue_.peek(&nbf1, 1);
                state = 4;
                break;
            case 4:
                rxQueue_.peek(&nbf2, 1);
                msgBytesRemaining = nbf1 * 256 + nbf2;
                msgBytesRead = 0;
                state = 5;
                break;
            case 5:
                rxQueue_.peek(&msg[SOM_INDEX + msgBytesRead], 1);
                if ( ++msgBytesRead == msgBytesRemaining )
                {
                    char* cmd = (char*)(msg) + SOM_INDEX;
                    if ( !isChecksumValid_(msg, SOM_INDEX + msgBytesRead) )
                    {
                        // an echo command returns a formatted packet with the
                        // command following to "ECHO" to be returned to the 
                        // transmitter
                        if ( !strncmp(cmd, "ECHO", 4 ) )
                        {
                            cmd[msgBytesRead - CHKSUM_LENGTH] = '\0';
                            write(cmd + 5, strlen(cmd + 5), 0);  // skip over "ECHO "
                        }
                        else
                        {
							rSerialPort_.reset();
                            sendNak_(NAK_CHECKSUM_ERROR);
                            #if 0
                            write(msg,msgBytesRead+4,0);
                            char buf[100];
                            rSerialPort_.dumpRegs(buf);
                            write(buf,strlen(buf),15000);
                            #endif
                        }
                    }
                    else
                    {
                        // overwrite CKS to null terminate command string
                        cmd[msgBytesRead - CHKSUM_LENGTH] = '\0';
                        processCmd_(cmd);
                    }

                    // restart the state machine
                    msgTimer.invalidate();
                    state = 1;
                }
                break;
            default:
                AUX_CLASS_ASSERTION_FAILURE(state)
                break;
            }
            rxQueue_.remove();
        }
    }
    isPacketProtocolEnabled_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  processCmd_
//
//@ Interface-Description
//  Decodes and processes the command line parameter. Decodes the 
//  VSET or SNDF commands and dispatches them to the appropriate server.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
VsetInterface::processCmd_(const char* cmd)
{
    if ( !strncmp( cmd, "VSET", 4 ) )
    {
        rVsetServer_.processCmd( cmd );
    }
    else if ( !strncmp( cmd, "SNDF", 4 ) )
    {
        rDci_.generateSNDF();
    }
    else
    {
        sendNak_(NAK_UNRECOGNIZED);
    }
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void    
VsetInterface::SoftFault(  const SoftFaultID softFaultID
                           , const Uint32      lineNumber
                           , const char *      pFileName
                           , const char *      pPredicate )
{
    FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
                            GUI_Serial_Interface::DCI_INTERFACE_CLASS, 
                            lineNumber, pFileName, pPredicate);
}
