
#ifndef BitArray_NUM_BATCH_SETTING_IDS_HH
#define BitArray_NUM_BATCH_SETTING_IDS_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BitArray_NUM_BATCH_SETTING_IDS - Fixed Array of 'NUM_BATCH_SETTING_IDS' Bits.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedBitArray.H_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "SettingId.hh"
#include "SettingId.hh"

//@ Usage-Classes
#include "BitArray.hh"
//@ End-Usage


class BitArray_NUM_BATCH_SETTING_IDS : public BitArray
{
  public:
    BitArray_NUM_BATCH_SETTING_IDS(const BitArray_NUM_BATCH_SETTING_IDS& bitArray);
    BitArray_NUM_BATCH_SETTING_IDS(void);
    virtual ~BitArray_NUM_BATCH_SETTING_IDS(void);

    inline void  operator=(const BitArray_NUM_BATCH_SETTING_IDS& bitArray);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);

  protected:
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    //@ Data-Member:  pElems_
    // An array of enough integers to contain 'NUM_BATCH_SETTING_IDS' bits.
    Uint  pElems_[(NUM_BATCH_SETTING_IDS + ::BITS_PER_LWORD - 1) / ::BITS_PER_LWORD];
};


// Inlined methods...
#include "BitArray_NUM_BATCH_SETTING_IDS.in"


#endif  // BitArray_NUM_BATCH_SETTING_IDS_HH
