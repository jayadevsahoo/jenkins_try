#ifndef INSP_VALVE_CALIBRATION_H

#define INSP_VALVE_CALIBRATION_H

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: InspValveCalibrationTable_t - Implements inspiratory valve calibration.
//***********************************************************************

#include "CalibrationTable.h"

static const uint16_t NUM_SAMPLE_AVG			= 3;
static const DWORD FDEL_CAL_WAIT_TIME			= 100;	//30;
static const Flow_t INSP_HIGH_LOW_LIMIT			= 20;		// At or above this level, flow is considered high flow
static const Flow_t INSP_HIGH_FLOW_CAL_STEP		= 5;		// During high flow calibration, this is a step increment
static const Flow_t INSP_CAL_MIN_FLOW			= 100.0;

//TODO E600 VM: make this enum a class membed because it seems to be
// specific to this class only
enum FdelCalState_t            /* define states of inspiration flow calibration */
{
    FD_INIT,
    FD_ZERO,
    FD_CHECK,
    FD_WAIT,
    FD_DONE,
	FD_SAVE
};

class InspValveCalibrationTable_t : public CalibrationTable_t
{
	friend class CircuitCalibrationTable_t;
	friend class O2SensorCalibrationTable_t;
	friend class ExhValveCalibrationTable_t;
	friend class ServosThread_t;
	friend class BreathServos_t;

public:	
	InspValveCalibrationTable_t(CalibrationID_t CalID, AnalogOutputs_t Output);
	CalibrationStatus_t	Calibrate(Flow_t Flow, bool DataValid);
	uint16_t GetMinFlowDac() {return MinFlowDac; }
	uint16_t SendFlow(Flow_t Flow);
	uint16_t GetPsolLiftoff();

protected:
    friend class EngineeringServoThread_t;

private:
	bool DoCalibration(Flow_t Flow, bool DataValid);

	AnalogOutputs_t						AnalogOutput;
	FdelCalState_t						FdelCalState;
	uint16_t							FlowDacCommand;
	DWORD								PreviousTickCount;
	bool								FdelCalDone;
	uint16_t							MinFlowDac;
	uint16_t							LiftoffValue;

	Flow_t FlowLevel;
	uint16_t FlowIndex;
	uint16_t SampleCount;
	Flow_t	FlowSum;
	bool MinFlowDacCaptured;
	uint16_t ZeroFlowCount;
				
	uint16_t DacCount[INSP_VALVE_SUBTABLE_SIZE];
	Flow_t FlowLPM[INSP_VALVE_SUBTABLE_SIZE];
	Flow_t InFlow[NUM_SAMPLE_AVG];
};

#endif