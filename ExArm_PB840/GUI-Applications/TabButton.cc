#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TabButton - A SubScreenButton which displays a "tab" around
// it when depressed.  Used for the buttons in the Upper and Lower
// Screen Select areas.
//---------------------------------------------------------------------
//@ Interface-Description
// TabButton's are created in both the Lower and Upper Screen Select Areas.
// They are used to activate subscreens (they inherit from SubScreenButton).
// They display a white border around the bottom, left and right of the button
// when pressed down so as to give the impression that they are connected, like
// a tab, to the subscreen which they've selected which is located immediately
// above.
//
// There are two methods which are provided for extra control of
// tab buttons.  updateDisplay() is a virtual method and is provided
// for the use of derived classes.  There is a need for some
// specialized tab buttons due to the fact that the content of some
// tab buttons changes due to external events.  Calling updateDisplay()
// is the method by which other objects inform tab buttons to react
// to these events and update their display, if needed.
//
// The second method is the protected setTopLineBoxColor_() used
// by the MoreAlarmsTabButton to change the color of the area at the
// top of the tab button.  This area has to match the background color
// of the selected subscreen which is almost always medium grey but,
// in the case of the More Alarms subscreen, is black.  Hence, this
// method.
//---------------------------------------------------------------------
//@ Rationale
// Groups together the display of the tab and the subscreen selection
// mechanism as a useful class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The tab is displayed by using the external border feature of the Button
// class.  The border is displayed in solid white.  The only other thing that
// needs to be done is to blank out the top part of the border so as to create
// the 3-sided border.  We use the topLineBox_ member to do this.  When the tab
// button is pressed down the border is displayed along with the topLineBox_.
// When it is pressed up the border and topLineBox_ are removed.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TabButton.ccv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 004  By: gdc    Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "TabButton.hh"

//@ Usage-Classes
#include "VgaGraphicsDriver.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TabButton()  [Constructor]
//
//@ Interface-Description
// Constructs the Tab Button. Passed the following arguments:
// >Von
//	xOrg			The X co-ordinate of the button.
//	yOrg			The Y co-ordinate of the button.
//	width			Width of the button.
//	height			Height of the button.
//	buttonType		Type of the button, either LIGHT, LIGHT_NON_LIT or DARK.
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					ROUND or SQUARE.
//	title			The string displayed as the button's title.
//	pSubScreen		The subscreen which is selected by pressing this button.
//>Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Passes all arguments on to the inherited SubScreenButton and
// positions, sizes and color topLineBox_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TabButton::TabButton(Uint16 xOrg, Uint16 yOrg, Uint16 width, Uint16 height,
			Button::ButtonType buttonType, Uint8 bevelSize,
			Button::CornerType cornerType,
			StringId title, SubScreen *pSubScreen) :
	SubScreenButton(xOrg, yOrg, width, height, buttonType, bevelSize,
					cornerType, Button::BORDER, title, pSubScreen),
	topLineBox_(bevelSize, 0, width - 2 * bevelSize, bevelSize)
{
	CALL_TRACE("TabButton::TabButton(Uint16 xOrg, Uint16 yOrg, Uint16 width, "
			"Uint16 height, "
			"ButtonType buttonType, Uint8 bevelSize, CornerType cornerType, "
			"StringId title, SubScreen *pSubScreen)");

	// Color top line to match the default color of subscreen backgrounds
	setTopLineBoxColor_(Colors::MEDIUM_BLUE);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TabButton  [Destructor]
//
//@ Interface-Description
// Destroys a Tab Button.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TabButton::~TabButton(void)
{
	CALL_TRACE("TabButton::~TabButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay  [public, virtual]
//
//@ Interface-Description
// Provided for derived classes.  Triggers any needed update of the
// button's display.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty implementation.  This virtual function should be redefined by
//  any derived classes which require dynamic changes in the appearance
//  of the button contents.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TabButton::updateDisplay(void)
{
	CALL_TRACE("TabButton::updateDisplay(void)");

	// Do nothing.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downHappened_
//
//@ Interface-Description
// Called when this button is pressed down.  Passed 'byOperatorAction',
// a flag which indicates whether the operator pressed the button down
// or whether the setToDown() method was called.  Causes the associated
// subscreen to be activated and the connecting tab to be displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the corresponding method in SubScreenButton allowing it to
// do the subscreen activation.  Then we display the tab border using
// a white surrounding border and a box to blank out the top
// part of the surround.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TabButton::downHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("TabButton::downHappened_(Boolean byOperatorAction)");

	SubScreenButton::downHappened_(byOperatorAction);

	// Make border of button be white
	setBorderColor(Colors::FRAME_COLOR);

	// Add grey connecting line
	addDrawable(&topLineBox_);							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: upHappened_
//
//@ Interface-Description
// Called when this button is pressed up.  Passed 'byOperatorAction',
// a flag which indicates whether the operator pressed the button up
// or whether the setToUp() method was called.  Causes the associated
// subscreen to be deactivated and the connecting tab to be removed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the corresponding method in SubScreenButton allowing it to
// do the subscreen deactivation.  Then we undisplay the tab border by
// removing the button border and box.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TabButton::upHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("TabButton::upHappened_(Boolean byOperatorAction)");

	SubScreenButton::upHappened_(byOperatorAction);

	// Remove white border
	setBorderColor(Colors::NO_COLOR);

	// Remove grey connecting line
	removeDrawable(&topLineBox_);						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTopLineBoxColor_
//
//@ Interface-Description
// Sets the color of this tab button's "top line".  Passed the new color
// color of the line.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the color of this tab button's "top line".  By default, the
//  TabButton constructor initializes the "top line" color to match
//  the default color of the subscreen backgrounds.  This provides an override
//  for those buttons which have subscreens with different background
//  colors.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TabButton::setTopLineBoxColor_(Colors::ColorS boxColor)
{
	CALL_TRACE("TabButton::setTopLineBoxColor_(boxColor)");

	// Set the color (and make sure it fills the area).
	topLineBox_.setColor(boxColor);
	topLineBox_.setFillColor(boxColor);					// $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TabButton::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TABBUTTON,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
