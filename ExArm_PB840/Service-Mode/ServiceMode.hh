#ifndef ServiceMode_HH
#define ServiceMode_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceMode - SubSystem definitions
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ServiceMode.hhv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By: sah    Date: 22-Sep-1999     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      No longer need to store local copy of 'SoftwareOptions', now can
//      user global, static interface.
//
//  Revision: 011  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 010  By:  syw    Date: 05-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//			BiLevel initial version.  Added GetSoftwareOptions() and
//			GetSoftwareOptions_() methods.
//
//  Revision: 009  By:  quf    Date: 21-Nov-1997    DR Number: DCS 2634
//       Project:  Sigma (840)
//       Description:
//			GUI novram serial number update now occurs within
//			ServiceMode::BdReadyCallback() (formerly done within
//			GuiApp::BdReadyCallback()).  CheckSerialNumber_() is now
//			defined for both BD and GUI.
//
//  Revision: 008  By:  quf    Date: 13-Nov-1997    DR Number: DCS 2631
//       Project:  Sigma (840)
//       Description:
//			Fixed IsServiceRequired() so that IS_EST_FAILURE is not
//			affected by use of demo data key, removed DemoDataKey_.
//
//  Revision: 007  By:  quf    Date: 14-Oct-1997    DR Number: DCS 2289
//       Project:  Sigma (840)
//       Description:
//			Added DataKeyType_.
//
//  Revision: 006  By:  quf    Date: 06-Oct-1997    DR Number: DCS 2451
//       Project:  Sigma (840)
//       Description:
//			Added methods CalculateIncreasedPriority(), IncreasePriority(),
//			and RestorePriority() to allow a temporary increase of the
//			SM Manager Task priority.
//
//  Revision: 005  By:  gdc    Date: 30-Sep-1997    DR Number: DCS 2513/2483
//       Project:  Sigma (840)
//       Description:
//			Provided callback methods to handle TaskControlMessages on
//          BD for enhanced communication failure detection/recovery
//          and to retrieve GUI FLASH serial number. Corrected initialization
//          of GUI SerialNumber object to use function static for 
//          controlled initialization.
//
//  Revision: 004  By:  quf    Date: 26-Sep-1997    DR Number: DCS 2525
//       Project:  Sigma (840)
//       Description:
//			Changed enum name from EvCalStatus to TestStatus and added
//			FAILED status ID, fixed all references to name.
//
//  Revision: 003  By:  quf    Date: 02-Jul-1997    DR Number: DCS 2107
//       Project:  Sigma (840)
//       Description:
//            Added method GetExhValveCalStatus(), which returns an enum
//            having one of three values: NEVER_RUN, IN_PROGRESS, or PASSED.
//            This will be used this to keep the EST button flat only for the
//            NEVER_RUN case.  IsExhValveCalRequired() and IsServiceRequired()
//            were modified to use the new three-state calibration-in-progress
//            novram flag.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//		Project:  Sigma (R8027)
//		Description:
//			Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "ServiceModeClassId.hh"

//@ Usage-Classes

class SerialNumber;
class BdReadyMessage;
class GuiReadyMessage;
class CommDownMessage;
class ChangeStateMessage;

#if defined(SIGMA_GUI_CPU)
# include "Configuration.hh"
# include "CrcUtilities.hh"
# include "CalInfoFlashSignature.hh"
#endif  // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_BD_CPU)
# include "SettingId.hh"

#endif  // defined(SIGMA_BD_CPU)

//@ End-Usage


#if defined(SIGMA_BD_CPU)

extern const Int32 SM_CYCLE_TIME;
extern const Real32 EV_COUNTS_TO_MA;

#endif  // defined(SIGMA_BD_CPU)


//@ Type: gasType
//  Gas used
enum GasType {
		AIR, O2, EITHER
};

//@ Type: measurementSide
//  Side measured for control
enum MeasurementSide {
		DELIVERY, EXHALATION
};


class ServiceMode
{
  public:
	enum TestStatus
	{
		NEVER_RUN = 0,
		IN_PROGRESS,
		FAILED,
		PASSED
	};

	static void  Initialize( void) ;

	static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL, 
						   const char		*pPredicate = NULL) ;

// ***** BD/GUI common methods *****
	static inline Boolean IsServiceComplete( void) ;
	static inline void SetServiceComplete( const Boolean value) ;
	static Boolean IsServiceRequired( void) ;
	static void ChangeStateCallback(const ChangeStateMessage &rMessage) ;
	static const SerialNumber& GetGuiSerialNumber(void) ;

// ***** BD-only public methods *****
#if defined(SIGMA_BD_CPU)

	static void SettingEventHappened(SettingId::SettingIdType id) ;
	static inline Boolean IsFlowSensorInfoValid( void) ;
	static Boolean IsExhValveCalRequired( void) ;
	static TestStatus GetExhValveCalStatus( void) ;
	static inline Boolean IsVentInopTestInProgress( void) ;
	static Boolean IsSstRequired( void) ;
	
	static inline void SetServiceRequired( const Boolean value) ;
	static inline void SetFlowSensorInfoValid( const Boolean value) ;
	static inline void SetExhValveCalValid( const Boolean value) ;
	static inline void SetVentInopTestInProgress( const Boolean value) ;
	static void CommDownCallback(const CommDownMessage &rMessage) ;
	static void GuiReadyCallback(const GuiReadyMessage &rMessage) ;

	static void CalculateIncreasedPriority(void);
	static void IncreasePriority(void);
	static void RestorePriority(void);

#endif  // defined(SIGMA_BD_CPU)

// ***** GUI-only public methods *****
#if defined(SIGMA_GUI_CPU)

	static void BdReadyCallback(const BdReadyMessage &rMessage) ;
	static inline Boolean IsDataKeyInstalled( void) ;
	
	static Boolean AreBdAndGuiFlashTheSame( void) ;
	
	static inline CalInfoFlashSignature::Signature GetExhValveSignature( void) ;
	static inline CrcValue GetExhValveCrc( void) ;

	static inline Uint32 GetAirFlowSensorSN( void) ;
	static inline CrcValue GetAirFlowSensorCrc( void) ;

	static inline Uint32 GetO2FlowSensorSN( void) ;
	static inline CrcValue GetO2FlowSensorCrc( void) ;

	static inline Uint32 GetExhFlowSensorSN( void) ;
	static inline CrcValue GetExhFlowSensorCrc( void) ;

	static inline Uint32 GetExhO2FlowSensorSN( void) ;
	static inline CrcValue GetExhO2FlowSensorCrc( void) ;

#endif  // defined(SIGMA_GUI_CPU)

  protected:

  private:
	ServiceMode( void) ;					// not implemented...
	~ServiceMode( void) ;					// not implemented...
	ServiceMode( ServiceMode&) ;		// not implemented...
	void operator=( const ServiceMode&) ;	// not implemented...

	static SerialNumber& GetGuiSerialNumber_(void) ;
	static void CheckSerialNumber_(void);

// ***** BD/GUI common private data members *****
	//@ Data-Member: Boolean ServiceComplete_
	// Service Mode complete flag
	static Boolean ServiceComplete_ ;

// ***** BD-only private data members *****
#if defined(SIGMA_BD_CPU)

	//@ Data-Member: Boolean FlowSensorInfoValid_
	// flow sensor calibration info valid flag
	static Boolean FlowSensorInfoValid_ ;

	//@ Data-Member: Boolean ExhValveCalValid_
	// exh valve calibration info valid flag
	static Boolean ExhValveCalValid_ ;

	//@ Data-Member: Boolean VentInopTestInProgress_
	// vent inop test-in-progress flag
	static Boolean VentInopTestInProgress_ ;

	//@ Data-Member: IncreasedPriority_
	// increased priority of service mode manager task
	static Int32 IncreasedPriority_ ;

#endif  // defined(SIGMA_BD_CPU)

#if defined(SIGMA_GUI_CPU)

// ***** GUI-only private data members *****

	//@ Data-Member: Boolean DataKeyInstalled_
	// data key installed flag
	static Boolean DataKeyInstalled_ ;

	//@ Data-Member: CalInfoFlashSignature::Signature ExhValveSignature_
	// exh valve signature
	static CalInfoFlashSignature::Signature ExhValveSignature_ ;

	//@ Data-Member: CrcValue ExhValveCrc_
	// exh valve checksum
	static CrcValue ExhValveCrc_ ;

	//@ Data-Member: Uint32 AirFlowSensorSN_
	// air flow sensor serial #
	static Uint32 AirFlowSensorSN_ ;

	//@ Data-Member: CrcValue AirFlowSensorCrc_
	// air flow sensor checksum
	static CrcValue AirFlowSensorCrc_ ;

	//@ Data-Member: Uint32 O2FlowSensorSN_
	// O2 flow sensor serial #
	static Uint32 O2FlowSensorSN_ ;

	//@ Data-Member: CrcValue O2FlowSensorCrc_
	// O2 flow sensor checksum
	static CrcValue O2FlowSensorCrc_ ;

	//@ Data-Member: Uint32 ExhFlowSensorSN_
	// exh flow sensor serial #, air data
	static Uint32 ExhFlowSensorSN_ ;

	//@ Data-Member: CrcValue ExhFlowSensorCrc_
	// exh flow sensor checksum, air data
	static CrcValue ExhFlowSensorCrc_ ;

	//@ Data-Member: Uint32 ExhO2FlowSensorSN_
	// exh flow sensor serial #, O2 data
	static Uint32 ExhO2FlowSensorSN_ ;

	//@ Data-Member: CrcValue ExhO2FlowSensorCrc_
	// exh flow sensor checksum, O2 data
	static CrcValue ExhO2FlowSensorCrc_ ;

#endif  // defined(SIGMA_GUI_CPU)

} ;


// Inlined methods...
#include "ServiceMode.in"

#endif // ServiceMode_HH 
