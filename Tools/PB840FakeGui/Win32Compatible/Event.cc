#include "StdAfx.h"
#include "Event.hh"


HANDLE Event::EventLookup_[] = { NULL };


Event::Event(void)
{
}

Event::~Event(void)
{
}


void Event::Initialize()
{
	for( Uint32 i = 0; i < MAX_EVENT_ID; ++ i )
	{
		EventLookup_[i] = CreateEvent(NULL, FALSE, FALSE, NULL);
	}
}



HANDLE Event::Query(Uint32 eventId)
{
	ASSERT( eventId >= 0 && eventId < MAX_EVENT_ID );

	return EventLookup_[eventId];
}