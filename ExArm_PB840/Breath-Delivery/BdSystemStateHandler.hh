#ifndef BdSystemStateHandler_HH
#define BdSystemStateHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdSystemStateHandler - A handle to the persistent BdSystemState
// class.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/BdSystemStateHandler.hhv   10.7   08/17/07 09:34:06   pvcs  
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  12-Dec-2008    SCR Number: 5994
//  Project:  840S
//  Description:
//  	Removed isSafetyPcvActive accessor as dead code.
//
//  Revision: 005  By: rpr     Date:  24-Oct-2008    SCR Number: 6435
//  Project:  S840
//  Description:
//      Added UpdateCalibrateRequestStatus and IsCalibrateO2Requested to 
//		support +20 O2
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 002  By:  iv    Date:  03-Feb-1997    DR Number: DCS 1297
//       Project:  Sigma (R8027)
//       Description:
//             Deleted O2 interval timer from Novram and added the method
//             StartNovRamUpdate_().
//
//  Revision: 001  By:  iv    Date:  16-Oct-1995    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "BdSystemState.hh"
#include "SchedulerId.hh"

//@ Usage-Classes
class BreathPhase;
//@ End-Usage


class BdSystemStateHandler {
  public:
    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
	static void Reset(void);
	static void Initialize(void);
	static void UpdateNovRam(void);
	static void UpdateO2RequestStatus(const Boolean is100PercentO2Requested); 
	static void UpdateCalibrateRequestStatus(const Boolean isCalibrateO2Requested); 
	static void UpdateSchedulerId(const SchedulerId::SchedulerIdValue id);
	static void UpdateApneaActiveStatus(const Boolean isApneaActive);
	static void UpdateSafetyPcvActiveStatus(const Boolean isSafetyPcvActive);
	static SchedulerId::SchedulerIdValue GetSchedulerId(void);
	static Boolean Is100PercentO2Requested(void);
	static Boolean IsCalibrateO2Requested(void);
	static Int32 GetO2RequestElapsedTime(void);
	static Boolean IsApneaActive(void);

  protected:

  private:
    BdSystemStateHandler(const BdSystemStateHandler&);	// not implemented...
    void   operator=(const BdSystemStateHandler&);	// not implemented...
    BdSystemStateHandler(void);	// not implemented...
    ~BdSystemStateHandler(void); // not implemented...

   static void StartNovRamUpdate_(void);

	//@ Data-Member BdSystemState_
	// local copy of the object, used to update the nov ram object
    static BdSystemState BdSystemState_;

	//@ Data-Member ChangeInProgress_
	// local variable, used as semaphore to disallow nov ram updates 
    static Boolean ChangeInProgress_;
};



#endif // BdSystemStateHandler_HH 
