#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  MinuteVolumeSetting - (Set) Minute Volume Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the minute volume (in
//  liters) that would be delivered with respiratory rate and tidal
//  volumes settings.  This class inherits from 'BatchBoundedSetting' and
//  provides the specific information needed for representation of
//  minute volume.  This information includes the interval and range of
//  this setting's values, this setting's response to a change of its tracked
//  settings (see 'valueUpdate()'), and this batch setting's new-patient
//  value (see 'getNewPatientValue()').
//
//  This class cannot be directly changed by the operator, yet it is not
//  fixed.  This setting is strictly changed via changes to respiratory
//  rate and tidal volume, via the Setting Observer/Subject Framework.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/MinuteVolumeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  changed this setting from a "dependent setting" of peak
//         insp flow, to using the Observer/Subject mechanism as a
//         "tracking setting"; as a tracking setting, this now needs to
//         override some 'SettingObserver' methods ('valueUpdate()',
//         'doRetainAttachment()' and 'settingObserverInit()'), and no
//         longer needs dependent and transition methods
//         ('acceptPrimaryChange()' and 'acceptTransition()')
//
//  Revision: 002   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//=====================================================================

#include "MinuteVolumeSetting.hh"
#include "MandTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[CL02004] The setting's range ...
// $[CL02005] The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    0.0f,		// absolute minimum...
    0.0f,		// unused...
    TENTHS,		// unused...
    NULL
  };
static const BoundedInterval  NODE2_ =
  {
    10.0f,		// change-point...
    0.01f,		// resolution...
    HUNDREDTHS,		// precision...
    &::LAST_NODE_
  };
static const BoundedInterval  NODE1_ =
  {
    100.0f,		// change-point...
    0.1f,		// resolution...
    TENTHS,		// precision...
    &::NODE2_
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    1000.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::NODE1_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: MinuteVolumeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Construct an instance of an inspiratory time setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

MinuteVolumeSetting::MinuteVolumeSetting(void)
        : BatchBoundedSetting(SettingId::MINUTE_VOLUME,
			      Setting::NULL_DEPENDENT_ARRAY_,
			      &::INTERVAL_LIST_,
			      NULL_SETTING_BOUND_ID,	// maxConstraintId...
			      NULL_SETTING_BOUND_ID)	// minConstraintId...
{
  CALL_TRACE("MinuteVolumeSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~MinuteVolumeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

MinuteVolumeSetting::~MinuteVolumeSetting(void)
{
  CALL_TRACE("~MinuteVolumeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is only applicable (viewable) if both respiratory rate
//  and tidal volume are applicable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
MinuteVolumeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pRespRate =
			SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);
  const Setting*  pTidalVolume =
			SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

  const Applicability::Id  RESP_RATE_APPLIC_ID =
				   pRespRate->getApplicability(qualifierId);
  const Applicability::Id  TIDAL_VOL_APPLIC_ID =
				pTidalVolume->getApplicability(qualifierId);

  return((RESP_RATE_APPLIC_ID != Applicability::INAPPLICABLE  &&
          TIDAL_VOL_APPLIC_ID != Applicability::INAPPLICABLE)
	  ? Applicability::VIEWABLE		// $[TI1]
	  : Applicability::INAPPLICABLE);	// $[TI2]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
MinuteVolumeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  // calculate minute volume's new-patient value using tidal volume's
  // and respiratory rate's new-patient values...
  const BoundedValue  NEW_PATIENT = calcBasedOnSetting_(
  						SettingId::TIDAL_VOLUME,
						BoundedRange::WARP_NEAREST,
						BASED_ON_NEW_PATIENT_VALUES
						       );

  return(NEW_PATIENT);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to value changes of tidal volume and respiratory rate, by
//  re-calculating current value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[CL02004]    -- equal to (Vt * f * 0.001)
//  $[02022]\h\   -- from-PC-to-VC transition rules
//  $[VC02006]\d\ -- from-VC-to-VC+ transition rules
//  $[VC02006]\j\ -- from-PC-to-VC+ transition rules
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::TIDAL_VOLUME  ||
//   pSubject->getId() == SettingId::RESP_RATE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
MinuteVolumeSetting::valueUpdate(
			    const Notification::ChangeQualifier qualifierId,
			    const SettingSubject*               pSubject
			        )
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");
  SAFE_AUX_CLASS_PRE_CONDITION((pSubject->getId() == SettingId::TIDAL_VOLUME  ||
				pSubject->getId() == SettingId::RESP_RATE),
			       pSubject->getId());

  if (qualifierId == Notification::ADJUSTED)
  {  // $[TI1] -- tidal volume's or resp rate's adjusted value changed...
    BoundedValue  newMinuteVolume;

    newMinuteVolume = calcBasedOnSetting_(pSubject->getId(),
					  BoundedRange::WARP_NEAREST,
					  BASED_ON_ADJUSTED_VALUES);

    // store the value...
    setAdjustedValue(newMinuteVolume);
  }  // $[TI2] -- don't care about their accepted value changes...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
MinuteVolumeSetting::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the values of tidal volume and respiratory
//  rate, therefore this virtual method is overridden to provide a point
//  during initialization to attach to those (subject) settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
MinuteVolumeSetting::settingObserverInit(void)
{
  CALL_TRACE("settingObserverInit()");

  attachToSubject_(SettingId::TIDAL_VOLUME, Notification::VALUE_CHANGED);
  attachToSubject_(SettingId::RESP_RATE, Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
MinuteVolumeSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      const Real32  MINUTE_VOLUME_VALUE =
			      BoundedValue(getAcceptedValue()).value;
      const Real32  CALC_MINUTE_VOLUME_VALUE =
			  calcBasedOnSetting_(SettingId::TIDAL_VOLUME,
					      BoundedRange::WARP_NEAREST,
					      BASED_ON_ACCEPTED_VALUES).value;

      isValid = (MINUTE_VOLUME_VALUE == CALC_MINUTE_VOLUME_VALUE);

      if (!isValid)
      {
	const Setting*  pTidalVolume =
		      SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
	const Setting*  pRespRate =
		       SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);

	cout << "\nINVALID Minute Volume:\n";
	cout << "CALC = " << CALC_MINUTE_VOLUME_VALUE << endl;
	cout << *this << *pTidalVolume << *pRespRate << endl;
      }
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
MinuteVolumeSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
    {
      const Real32  MINUTE_VOLUME_VALUE =
			      BoundedValue(getAdjustedValue()).value;
      const Real32  CALC_MINUTE_VOLUME_VALUE =
			  calcBasedOnSetting_(SettingId::TIDAL_VOLUME,
					      BoundedRange::WARP_NEAREST,
					      BASED_ON_ADJUSTED_VALUES).value;

      isValid = (MINUTE_VOLUME_VALUE == CALC_MINUTE_VOLUME_VALUE);

      if (!isValid)
      {
	const Setting*  pTidalVolume =
		      SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
	const Setting*  pRespRate =
		       SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);

	cout << "\nINVALID Minute Volume:\n";
	cout << "CALC = " << CALC_MINUTE_VOLUME_VALUE << endl;
	cout << *this << *pTidalVolume << *pRespRate << endl;
      }
    }
  }

  return(isValid);
}

#endif // defined (SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
MinuteVolumeSetting::SoftFault(const SoftFaultID  softFaultID,
			       const Uint32       lineNumber,
			       const char*        pFileName,
			       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  MINUTE_VOLUME_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  ((basedOnSettingId == SettingId::TIDAL_VOLUME  ||
//    basedOnSettingId == SettingId::RESP_RATE)  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
MinuteVolumeSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
				    ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  AUX_CLASS_PRE_CONDITION((basedOnSettingId == SettingId::TIDAL_VOLUME  ||
			   basedOnSettingId == SettingId::RESP_RATE),
			  basedOnSettingId);

  const Setting*  pRespRate =
			SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);
  const Setting*  pTidalVolume =
			SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

  Real32  respRateValue;
  Real32  tidalVolumeValue;

  switch (basedOnCategory)
  {
  case BASED_ON_NEW_PATIENT_VALUES :		// $[TI1]
    // get the "new-patient" values of each of the settings...
    respRateValue    = BoundedValue(pRespRate->getNewPatientValue()).value;
    tidalVolumeValue = BoundedValue(pTidalVolume->getNewPatientValue()).value;
    break;

  case BASED_ON_ADJUSTED_VALUES :		// $[TI2]
    // get the "adjusted" values of each of the settings...
    respRateValue    = BoundedValue(pRespRate->getAdjustedValue()).value;
    tidalVolumeValue = BoundedValue(pTidalVolume->getAdjustedValue()).value;
    break;

  case BASED_ON_ACCEPTED_VALUES :
    // fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
    // get the "accepted" values of each of the settings...
    respRateValue    = BoundedValue(pRespRate->getAcceptedValue()).value;
    tidalVolumeValue = BoundedValue(pTidalVolume->getAcceptedValue()).value;
    break;
#endif // defined(SIGMA_DEVELOPMENT)

  default :
    // unexpected 'basedOnCategory' value..
    AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
    break;
  };

  BoundedValue  minuteVolume;

  minuteVolume.value = (tidalVolumeValue * 0.001 * respRateValue);

  // place minute volume on a "click" boundary...
  getBoundedRange().warpValue(minuteVolume, warpDirection, FALSE);

  return(minuteVolume);
}
