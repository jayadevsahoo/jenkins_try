#if !defined(AFX_INFOSET_H__617D124C_54A5_11D3_9C97_004005519CCD__INCLUDED_)
#define AFX_INFOSET_H__617D124C_54A5_11D3_9C97_004005519CCD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InfoSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInfoSet DAO recordset

class CInfoSet : public CDaoRecordset
{
public:
	CInfoSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CInfoSet)

// Field/Param Data
	//{{AFX_FIELD(CInfoSet, CDaoRecordset)
	long	m_ID;
	CString	m_Info1;
	CString	m_Info2;
	CString	m_Info3;
	CString	m_Info4;
	CString	m_Info5;
	long	m_LastSeq;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFOSET_H__617D124C_54A5_11D3_9C97_004005519CCD__INCLUDED_)
