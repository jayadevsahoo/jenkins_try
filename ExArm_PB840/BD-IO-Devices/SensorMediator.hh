#ifndef SensorMediator_HH
#define SensorMediator_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SensorMediator - Moderates the updating of the sensor values.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/SensorMediator.hhv   25.0.4.0   19 Nov 2013 13:54:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 008 By: syw    Date: 07-Apr-1998   DR Number: 5065
//  	Project:  Sigma (R8027)
//			Changed order of processing sensors for NEW_SENSOR.
//			NEW_SENSOR needs temperature sensors to be updated before flow
//			sensors are updated.
//
//  Revision: 007 By: syw    Date: 07-Jul-1997   DR Number: 2283
//  	Project:  Sigma (R8027)
//			Sample all flow temperature sensors during 5 ms loop.
//
//  Revision: 006 By: syw    Date: 13-Mar-1997   DR Number: 1780, 1827
//  	Project:  Sigma (R8027)
//			Eliminate EXH_DRY_FLOW_SENSOR,
//
//  Revision: 005 By: syw    Date: 25-Feb-1997   DR Number: DCS 1795
//  	Project:  Sigma (R8027)
//			Moved exh flow temperature sensors to 5 ms primary.
//
//  Revision: 004  By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 003  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Redefined NUM_SENSORS and added NUM_SUBMUX_SENSORS to accomodate
//			sub mux design.  Added *pSubMuxSensors_ data member.
//
//  Revision: 002  By: kam    Date:  03-Oct-1995    DR Number: DCS 562
//       Project:  Sigma (R8027)
//       Description:
//          Updated for BD Safety Net Interface -- made SafetyNetTestMediator
//          a friend of this class
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

class SafetyNetTestMediator;
class Sensor;

//@ End-Usage


class SensorMediator 
{
	public:

		enum PrimarySensorGroupId
		{
#ifdef NEW_SENSOR
			INSP_PRESSURE_SENSOR,
			EXH_PRESSURE_SENSOR,
			AC_LINE_VOLTAGE,
			O2_TEMP_SENSOR,
			AIR_TEMP_SENSOR,
			EXH_GAS_TEMP_SENSOR,
			O2_FLOW_SENSOR,
			AIR_FLOW_SENSOR,
			EXH_FLOW_SENSOR,
#else
			INSP_PRESSURE_SENSOR,
			EXH_PRESSURE_SENSOR,
			EXH_DRV_PRESSURE_SENSOR,
			O2_FLOW_SENSOR,
			AIR_FLOW_SENSOR,
			EXH_FLOW_SENSOR,
			O2_TEMP_SENSOR,
			AIR_TEMP_SENSOR,
			// TODO E600_LL: to be determined if secondary cycle is nec.
			AIR_PRESS_SENSOR,
			O2_PRESS_SENSOR,
			//EXH_GAS_TEMP_SENSOR,
#endif // NEW_SENSOR

			MAX_PRIMARY_SENSORS
			
		};
#if 0 // TODO E600_LL: to be determined if secondary cycle is nec.
		enum SecondarySensorGroupId
		{
			ATMOSPHERIC_PRESSURE_SENSOR,
			O2_PSOL_CURRENT,
			AIR_PSOL_CURRENT,
			EXH_MOTOR_CURRENT,
			SAFETY_VALVE_CURRENT,
			FIO2_MONITOR,
			EXH_VOICE_COIL_TEMP_SENSOR,
			EXH_HEATER_TEMP_SENSOR,
			POWER_FAIL_CAP_VOLTAGE,
			DC_10V_SENTRY,
			DC_15V_SENTRY,
			DC_NEG_15V_SENTRY,
			DC_5V_GUI,
			DC_12V_GUI,
			DC_5V_VENT_HEAD,
			DC_12V_SENTRY,
			SYSTEM_BATTERY_VOLTAGE,
			SYSTEM_BATTERY_CURRENT,
			ALARM_CABLE_VOLTAGE,
			SAFETY_VALVE_SWITCH_SIDE,

			MAX_SECONDARY_SENSORS
		};
#endif
		SensorMediator( void) ;
		~SensorMediator( void) ;

		static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;
    	void newCycle( void) const ;
		void newSecondaryCycle( void) const ;

    	friend class SafetyNetTestMediator;
  
	protected:

	private:

    	SensorMediator( const SensorMediator&) ;      // not implemented...
	    void   operator=( const SensorMediator&) ;    // not implemented...

    	//@ Data-Member: *pPrimarySensorsGroup_
    	// A set of pointers to sensors updated
		// by the sensor mediator once every BD cycle
    	Sensor *pPrimarySensorsGroup_[ MAX_PRIMARY_SENSORS ] ;

    	//@ Data-Member: *pSecondarySensorsGroup_
    	// A set of pointers to sensors updated
		// by the sensor mediator every secondary cycle
		// TODO E600_LL: to be determined if secondary cycle is nec.
    	//Sensor *pSecondarySensorsGroup_[ MAX_SECONDARY_SENSORS ] ;
};


#endif // SensorMediator_HH 
