#ifndef AlarmUpdateBuffer_HH
#define AlarmUpdateBuffer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmUpdateBuffer - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmUpdateBuffer.hhv   25.0.4.0   19 Nov 2013 13:51:18   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 01/03/97         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.  Removed fromCpu handling.
//  
//   Revision 003   By:   hct      Date: 02/10/97         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.  Don't need AlarmComm anymore.
//  
//   Revision 004   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"


//@ Usage-Classes
#include "Alarm.hh"
#include "AlarmUpdateList.hh"

//@ End-Usage

class AlarmUpdateBuffer
{

  public:

    AlarmUpdateBuffer(const AlarmUpdateList& rAlarmUpdateList,
                      Alarm::UpdateType updateType,
                      Boolean           cancelScreenLockStatus,
                      Boolean           noPatientDataDisplayStatus,
                      Boolean           noGuiSettingChangesStatus,
                      Boolean           ventInopLedOnStatus);
    AlarmUpdateBuffer(const AlarmUpdateBuffer& rBuff);

    ~AlarmUpdateBuffer(void);

    inline Boolean operator==(const  AlarmUpdateBuffer& rBuff) const;
    inline Boolean operator!=(const  AlarmUpdateBuffer& rBuff) const;

    inline Alarm::UpdateType getUpdateType(void) const;
    inline const AlarmUpdateList& getAlarmUpdateList(void) const;
    inline Boolean getCancelScreenLockStatus(void) const;
    inline Boolean getNoPatientDataDisplayStatus(void) const;
    inline Boolean getNoGuiSettingChangesStatus(void) const;
    inline Boolean getVentInopLedOnStatus(void) const;

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

  protected:

  private:

    AlarmUpdateBuffer(void); // not implemented
    void   operator=(const AlarmUpdateBuffer&); // not implemented

    //@ Data-Member:    alarmUpdateList_
    // Holds the list of AlarmUpdate objects.
    AlarmUpdateList alarmUpdateList_;

    //@ Data-Member:    cancelScreenLockStatus_
    // For an Alarm Update, holds the Boolean value of 
    // Alarm::GetCancelScreenLockStatus().
    Boolean cancelScreenLockStatus_;

    //@ Data-Member:    noPatientDataDisplayStatus_
    // For an Alarm Update, holds the Boolean value of 
    // Alarm::GetNoPatientDataDisplayStatus().
    Boolean noPatientDataDisplayStatus_;

    //@ Data-Member:    noGuiSettingChangesStatus_
    // For an Alarm Update, holds the Boolean value of 
    // Alarm::GetNoGuiSettingsChangesStatus().
    Boolean noGuiSettingChangesStatus_;

    //@ Data-Member:    ventInopLedOnStatus_
    // For an Alarm Update, holds the Boolean value of 
    // Alarm::GetVentInopLedOnStatus().
    Boolean ventInopLedOnStatus_;

    //@ Data-Member:    updateType_
    // For an Alarm Update, holds the value of the Alarm attribute of the same 
    // name.
    Alarm::UpdateType updateType_;

};


// Inlined methods
#include "AlarmUpdateBuffer.in"


#endif // AlarmUpdateBuffer_HH 
