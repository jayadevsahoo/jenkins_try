//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================
#ifndef __EBM_bq34z110__
#define __EBM_bq34z110__

#include "ExternalBattery.hh"
#include "I2C.h"

// 
// Implements the External Battery Module interface using TI bq34z110
// chip. It uses I2C-based communication.
//
class EBM_bq34z110
	: public ExternalBattery, public I2C
{
public:
	static EBM_bq34z110* getInstance();

	// Returns an unsigned short value of the prediccted remaining battery
	// capacity expressed as a percentage of FullChargeCapacity(), with a 
	// range of 0 to 100%.
	bool readStateOfCharge( Uint16& socPercent );

	// Returns the compensated battery capacity remaining. Units are 1 mAh 
	// per bit.
	bool readRemainingCapacity( Uint16& remainingCapacity );

	// Returns the compensated capacity of the battery when fully charged.
	// Units are 1 mAh per bit except if X10 mode is selected. In X10 mode,
	// units are 10 mAh per bit. FullChargeCapacity() is updated at regular
	// intervals, as specified by the Impedance Track algorithm.
	bool readFullChargeCapacity( Uint32& fullChargeCapacity );

	// Returns an unsigned short value of the measured cell-pack voltage 
	// in mV with a range of 0 V to 65535 mV.
	bool readVoltage( Uint16& voltage );

	// Returns a signed integer value that is the average current flow 
	// through the sense resistor. It is updated every 1 second. Units are 
	// 1 mA per bit except if X10 mode is selected. In X10 mode, units
	// are 10 mA per bit.
	bool readAverageCurrent( Int32& averageCurrent );

    // Returns the instantaneous current measured by the gauge
    bool readInstantaneousCurrent( Int32& instCurrent );

	// Returns an unsigned integer value of the temperature in units of 0.1K
	// measured by the gas gauge and has a range of 0 to 6553.5K.
	bool readTemperature( Uint16& temperature );

private:

	// Private constructor for Singleton class
	EBM_bq34z110(HANDLE FileHandle, uint8_t BusAddress, 
		I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout = 50);
	~EBM_bq34z110(void);

	// Read the Pack Configuration
	bool readPackConfiguration_( Uint16& packConfiguration );

    bool control_( Uint16 cntlSubCommand, Uint16& data );
    bool control_( Uint16 cntlSubCommand );

	// Singleton instance of EBM bq34z110
	static EBM_bq34z110* instance_;

	// If current Battery configuration is X10 mode
	bool     isX10Mode_;
};

#endif // __EBM_bq34z110__