#ifndef Fio2Monitor_HH
#define Fio2Monitor_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: Fio2Monitor - implements the functionality required for 
// interfacing with the FiO2 monitor.
//---------------------------------------------------------------------
// @ Version-Information
// @ (#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/Fio2Monitor.hhv   10.7   08/17/07 09:29:42   pvcs  
//
// @ Modification-Log
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
// TODO E600_LL:  this class will need to be worked on in the future

#include "Sigma.hh"
#include "Sensor.hh"
#include "AdcChannels.hh"
#include "BD_IO_Devices.hh"
//TODO E600_LL #include "BdAlarmId.hh"
#include "BinaryIndicator.hh"
#include "Fio2CalInfo.hh"
#include "ProcessedValues.hh"

//@ Usage-Classes
//@ End-Usage

//  defines the size of the inspiratory pressure
//  circular buffer; 1.2 seconds of samples
static const Uint16 MAX_INSP_PRES_SAMPLES = 60;

//  defines the size of the sampled O2 sensor buffer 
static const Uint16 MAX_O2_SENSOR_SAMPLES = 50;

class Fio2Monitor : public Sensor 
{
	public:

		enum Fio2Status { NOT_INSTALLED, NORMAL, HIGH, LOW };

		Fio2Monitor( const AdcChannels::AdcChannelId);
// TODO E600_LL	                 SafetyNetSensorData *pData );

		~Fio2Monitor( void );

		static inline void SetTargetMix( const Real32 newMix);
		static inline Boolean IsInstalled( void );
		static inline void SetAlarmResetStatus( void );

		static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL,
							   const char*       pPredicate = NULL );

		Boolean inline isFio2SensorOor( void ) const;

		void initialize( void );
		void newCycle( void );

		inline void registerAlarmCallBack( PAlarmCallBack pAlarmCallBack );
		inline Real32 getCurrentCalculatedO2Percent( void ) const;

		void initialStatusNotification( void );

		void calibrateFio2Sensor( const Real32 mixSetting );

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
        virtual void updateValue( void);
#endif // defined (SIGMA_BD_CPU)
#endif // INTEGRATION_TEST_ENABLE

		struct InspPresBuffer 
		{
			Uint16 currentNdx;
			Uint16 numSamplesInBuffer;
			Real32 sampleBuffer_[ MAX_INSP_PRES_SAMPLES ];
		};

		struct O2SensorSampleBuffer
		{
			Uint16    currentNdx;
			Uint32    sampleAccumulator;
			Uint16    currentRunningAvg;
			Uint16    o2SensorRawCountBuffer[ MAX_O2_SENSOR_SAMPLES ];
			Boolean   bufferFull;
		};

#ifdef SIGMA_DEVELOPMENT
void displayFio2MonitorInfo( void );
#endif // SIGMA_DEVELOPMENT

	protected:

		virtual Real32 rawToEngValue_( const AdcCounts count) const ;

	private:

    	enum TargetO2Status { TARGET_O2_UNCHANGED,
							  WARM_UP_PERIOD_O2_UNCHANGED,
							  WARM_UP_PERIOD_O2_INCREASED,
							  WARM_UP_PERIOD_O2_DECREASED,
	                          TARGET_O2_INCREASED,
						      TARGET_O2_DECREASED };
														 
		Fio2Monitor( void );    					// not implemented
		Fio2Monitor( const Fio2Monitor& );          // not implemented
		void operator=( const Fio2Monitor& );       // not implemented
		void getFio2CalInfoFromNovRam_( void );
		void updateFio2CalInfoToNovRam_( void );
		void takeInspPresSample_( void );
		void takeO2SensorSample_( void );
		void calculateO2Percentage_( void );
		Boolean currentlyInBreathingMode_( void );

		//@ Data-Member: TargetO2Percent_
		// Indicates the target value for delivered O2 percent.
		// Set by the Breath-Delivery subsystem.
		static Real32 TargetO2Percent_;

		//@ Data-Member: PrevTargetO2Percent_
		// Indicates the previous target value for delivered O2 percent.
		// Set by the Breath-Delivery subsystem.
		static Real32 PrevTargetO2Percent_;

		//@ Data-Member: IsInstalled_
		// indicates whether the monitor is installed
		static Boolean IsInstalled_;

		//@ Data-Member: IsEnabled_
		// indicates whether the monitor is enabled
		static Boolean IsEnabled_;

		//@ Data-Member: isO2SensorOor_
		// indicates whether the O2 sensor has been detected as OOR
		Boolean isO2SensorOor_;

		//@ Data-Member: AlarmResetOccurred_ 
		// indicates whether the operator has pressed the Alarm Reset switch 
		static Boolean AlarmResetOccurred_;

		//@ Data-Member: stillInWarmUpPeriod_ 
		// Indicates whether the FiO2 sensor is still
		// in the first hour of warm up period
		Boolean stillInWarmUpPeriod_;

		//@ Data-Member: fio2AlarmStatus_
		// indicates the alarm status of the delivered Fio2
		Fio2Status fio2AlarmStatus_;

		//@ Data-Member: pAlarmCallBack_
		// pointer to the function called when an Fio2 Alarm is detected
		PAlarmCallBack pAlarmCallBack_;

		//@ Data-Member: o2SensorGain_ 
		//  slope of the linear O2 sensor 
		Real32 o2SensorGain_;

		//@ Data-Member: currentO2Percentage_
		//  last calculated O2 percentage 
		Real32 currentO2Percentage_;

		//@ Data-Member: fio2AlarmTimer_ 
		//  Fio2Monitor object alarm timer 
		Uint32 fio2AlarmTimer_;

		//@ Data-Member: timeSincePowerup_
		//  store the time since power-up
		Uint32 timeSincePowerup_;

		//@ Data-Member: fourMinuteChangeO2PercentageTimer_
		//  four (4) minute timer to countdown change in target O2%
		Uint16 fourMinuteChangeO2PercentageTimer_;

		//@ Data-Member: targetO2status_
		//  indicates the change in the O2 status (increase/decrease percentage)
		TargetO2Status targetO2status_;

		//@ Data-Member: fio2SensorCalInfo_ 
		//  struct which stores all the fiO2 calibration data
		Fio2SensorCalInfo fio2SensorCalInfo_;

		//@ Data-Member: inspPresAlaphaFilter1_ 
		//  alpha filter 1 for the inspiratory pressure delay 
		ProcessedValues inspPresAlphaFilter1_;

		//@ Data-Member: inspPresAlphaFilter2_ 
		//  alpha filter 2 for the inspiratory pressure delay 
		ProcessedValues inspPresAlphaFilter2_;

		//@ Data-Member: inspPresAlphaFilter3_ 
		//  alpha filter 3 for the inspiratory pressure delay 
		ProcessedValues inspPresAlphaFilter3_;

		//@ Data-Member: inspPressureBuffer_ 
		//  a circular inspiratory pressure buffer
		InspPresBuffer inspPresBuffer_;

		//@ Data-Member: o2SensorSampleBuffer_ 
		//  a circular buffer of O2 sensor sample
		O2SensorSampleBuffer o2SensorSamples_;

		//@ Data-Member: CurrentO2PercentUpperBound_
		// indicates the current O2% upper bound (target + tolerance)
		static Real32 CurrentO2PercentUpperBound_;

		//@ Data-Member: CurrentO2PercentLowerBound_
		// indicates the current O2% lower bound (target - tolerance)
		static Real32 CurrentO2PercentLowerBound_;
};

// Inlined methods...
#include "Fio2Monitor.in"

#endif  //Fio2Monitor_HH
