#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: PatientDataTarget - A target for changes to Patient Data
// values. Classes can specify this class as an additional parent class and
// register to be informed of changes to specific pieces of patient data.
//---------------------------------------------------------------------
//@ Interface-Description
// Any classes which need to be informed of changes to patient data in the
// Patient Data subsystem need to multiply inherit from this class.  Then, when
// they register for patient data changes with the PatientDataRegistrar class,
// they will be informed of changes via the patientDataChangeHappened() method.
//---------------------------------------------------------------------
//@ Rationale
// Needed to implement the callback mechanism used to communicate
// changes to patient data.
//---------------------------------------------------------------------
//@ Implementation-Description
// If classes need to be informed of patient data changes they must do 3
// things: a) multiply inherit from this class, b) define the
// patientDataChangeHappened() method of this class and c) register for
// change notices via the PatientDataRegistrar class.  They will then
// be informed of changes via the patientDataChangeHappened() method.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PatientDataTarget.ccv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "PatientDataTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened
//
//@ Interface-Description
// This method should be redefined in derived classes.  It is via this method
// that patient data changes are communicated.  It is passed one parameter,
// 'patientDataId', which identifies the patient datum that has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PatientDataTarget::patientDataChangeHappened(
						PatientDataId::PatientItemId patientDataId)
{
	CALL_TRACE("PatientDataTarget::patientDataChangeHappened("
						"PatientDataId::PatientItemId patientDataId)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PatientDataTarget()  [Default Constructor, Protected]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PatientDataTarget::PatientDataTarget(void)
{
	CALL_TRACE("PatientDataTarget::PatientDataTarget(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PatientDataTarget  [Destructor, Protected]
//
//@ Interface-Description
// The destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PatientDataTarget::~PatientDataTarget(void)
{
	CALL_TRACE("PatientDataTarget::~PatientDataTarget(void)");

	// Do nothing
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
PatientDataTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, PATIENTDATATARGET,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
