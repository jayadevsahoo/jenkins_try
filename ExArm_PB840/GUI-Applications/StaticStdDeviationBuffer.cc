#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: StaticStdDeviationBuffer - circular buffer for static standard
//		deriviation storage.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class provides static methods to obtain the pre-allocated buffers for
//	computing standard deriviation.
//---------------------------------------------------------------------
//@ Rationale
//	To encapsulate the static standard deriviation buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The buffers are statically created when a "get" method is called.  The
//	number of elements of each buffer is predetermined and can not be changed.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/GUI-Applications/vcssrc/StaticStdDeviationBuffer.ccv   10.7   08/17/07 10:09:00   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By:  sph	   Date:  08-Feb-2000    DCS Number:  5504
//  Project:  NeoMode
//  Description:
//      Added recent-activity range to alarm bars whereby the standard deviation 
//      the last N consecutive breaths is displayed.
//
//  Revision: 001  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//=====================================================================

#include "StaticStdDeviationBuffer.hh"

//@ Usage-Classes
#include "AlarmSlider.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSummationBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the time
//		buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticStdDeviationBuffer::GetSummationBuffer( Int32 Idx)
{
	CALL_TRACE("StaticStdDeviationBuffer::GetSummationBuffer( Int32 Idx)") ;
	
	// $[TI1]

	static Real32 SummationBuffer[AlarmSlider::MAX_SLIDER_ID][STD_BUFFER_SIZE] ;

	AUX_CLASS_PRE_CONDITION((Idx >=0 && Idx < AlarmSlider::MAX_SLIDER_ID), Idx);
	return( &SummationBuffer[Idx][0]) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetDeviationBuffer()  
//
//@ Interface-Description
//		This method has no arguments and returns a pointer to the time
//		buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Allocate the buffer when called for the first time, otherwise
//		return address of buffer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
Real32 *
StaticStdDeviationBuffer::GetDeviationBuffer( Int32 Idx)
{
	CALL_TRACE("StaticStdDeviationBuffer::GetDeviationBuffer( Int32 Idx)") ;
	
	// $[TI1]

	static Real32 DeviationBuffer[AlarmSlider::MAX_SLIDER_ID][STD_BUFFER_SIZE] ;

	AUX_CLASS_PRE_CONDITION((Idx >=0 && Idx < AlarmSlider::MAX_SLIDER_ID), Idx);
	return( &DeviationBuffer[Idx][0]) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
StaticStdDeviationBuffer::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, STATICSTDDEVIATIONBUFFER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================






