#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ScrollbarTarget -  A target class for receiving scrollbar
// manipulation events.  Classes derive from this class and use it to
// learn of scrolling interactions with a Scrollbar object.
//---------------------------------------------------------------------
//@ Interface-Description
// Any classes which use a Scrollbar object must derive from this class.
// This is in order to be informed of a user's interactions with the
// Scrollbar object.  When the user "scrolls" the target object's
// scrollHappened() method is called.
//---------------------------------------------------------------------
//@ Rationale
// Used to implement the callback mechanism needed to communicate user's
// Scrollbar interactions.
//---------------------------------------------------------------------
//@ Implementation-Description
// Classes which create a Scrollbar object must do two things: 1) derive from
// this class and 2) define the scrollHappened() method of this class.
// Those classes will then be automatically informed of any interactions
// with the Scrollbar object.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// There is no provision for an object creating more than one Scrollbar object
// and then being able to distinguish which Scrollbar was scrolled when
// scrollHappened() is called.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollbarTarget.ccv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ScrollbarTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: scrollHappened
//
//@ Interface-Description
// Called when the associated Scrollbar object is scrolled by the user.
// Passed the new 'firstDisplayedEntry', indicating the point scrolled to.
// See Scrollbar for more informatio on this parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollbarTarget::scrollHappened(Uint16)
{
	CALL_TRACE("ScrollbarTarget::scrollHappened(Uint16 firstDisplayedEntry)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScrollbarTarget()  [Protected constructor]
//
//@ Interface-Description
// The constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScrollbarTarget::ScrollbarTarget(void)
{
	CALL_TRACE("ScrollbarTarget::ScrollbarTarget(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScrollbarTarget()  [Protected destructor]
//
//@ Interface-Description
// ScrollbarTarget destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScrollbarTarget::~ScrollbarTarget(void)
{
	CALL_TRACE("ScrollbarTarget::~ScrollbarTarget(void)");
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ScrollbarTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SCROLLBARTARGET,
  				lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
