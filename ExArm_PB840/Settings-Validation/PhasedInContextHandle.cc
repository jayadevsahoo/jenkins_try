#include "stdafx.h"

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PhasedInContextHandle -  Handle to the Phased-In Settings.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides an external interface to the Settings-Validation
//  Subsystem, for the functionality relating to the "phased-in" settings.
//  This class provides static methods for phasing-in the pending settings
//  from various breathing modes (i.e., normal, Safety-PCV, etc.), and
//  getting the values of the currently phased-in settings.
//
//  The purpose of this class is to provide an interface for all of the
//  other subsystems, that need to interact with the phased-in settings.
//  This class provides a clear demarcation line between the functionality
//  of this subsystem's internal classes, and the functionality needed by
//  other subsystems.
//
//  This class is only available on the Breath-Delivery CPU.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a clear border between the Settings-Validation
//  Subsystem, and the subsystems that need functionality relating to the
//  phased-in settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All of the interface methods are set up as static, non-inline methods.
//  The fact that the methods are static, allows the other subsystems to
//  have access to the needed functionality, without needing access to an
//  instance of the class; direct calls to these methods could be made.
//  By making all of the methods non-inline, the client's dependency on
//  the classes and types needed for the implementation only is eliminated.
//  In other words, the non-inline methods allow for the header files needed
//  for the implementation to be included in the source file and NOT the
//  header file.
//
//  None of these methods are set-up for multiple thread capability.  It is
//  the responsibility of the Phased-In Context to protect the "read" threads
//  against the "update" thread.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PhasedInContextHandle.ccv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah   Date:  30-Sep-1999    DR Number: 5457
//  Project:  NeoMode
//  Description:
//	Removed 'SPECIAL CASE' used for BD's PEEP references.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added "special" code for handling queries to PEEP when in BiLevel
//	   mode:  to ensure that Breath-Delivery, et al, refer to the proper
//	   "PEEP", the PEEP-low value is returned in BiLevel
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PhasedInContextHandle.hh"
#include "ModeValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "PhasedInContext.hh"
#include "PendingContextHandle.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AreBdSettingsAvailable(void)  [static]
//
//@ Interface-Description
//  Are there previous settings available, at this time.  This does NOT
//  indicate whether they are, in fact, phased-in.  When a new-patient
//  batch first is received on the BD side, there is a small window of
//  time when the patient settings are "available", but not phased-in.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
PhasedInContextHandle::AreBdSettingsAvailable(void)
{
  CALL_TRACE("AreBdSettingsAvailable()");

  const PhasedInContext&  rPhasedInContext =
  					ContextMgr::GetPhasedInContext();

  return(rPhasedInContext.arePatientSettingsAvailable());
}  // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PhaseInSafetyPcvSettings(void)  [static]
//
//@ Interface-Description
//  Phase-in all of the Safety-PCV setting values at this time.  This is
//  called upon entry into safe-mode breathing.  All of the Safety-PCV
//  setting values are "phased-in" immediately.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward to the Phased-In Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PhasedInContextHandle::PhaseInSafetyPcvSettings(void)
{
  CALL_TRACE("PhaseInSafetyPcvSettings()");

  ContextMgr::GetPhasedInContext().phaseInSafetyPcvSettings();
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PhaseInPendingBatch(phaseInState)  [static]
//
//@ Interface-Description
//  Phase-in any of the pending BD settings that are to be phased in
//  during the current breath phase given by 'phaseInState'.
//
//  If a Vent-Startup update is currently pending in the Pending Context,
//  this will phase-in ALL of the setting from the Pending Context,
//  immediately.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward to the Phased-In Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PhasedInContextHandle::PhaseInPendingBatch(
			      const PhaseInEvent::PhaseInState phaseInState
					  )
{
  CALL_TRACE("PhaseInPendingBatch(phaseInState)");

  ContextMgr::GetPhasedInContext().phaseInPendingSettings(phaseInState);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetBoundedValue(boundedId)  [static]
//
//@ Interface-Description
//  Return a copy of the value of the bounded setting indicated by
//  'boundedId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward to the Phased-In Context.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
PhasedInContextHandle::GetBoundedValue(
				  const SettingId::SettingIdType boundedId
				      )
{
  CALL_TRACE("GetBoundedValue(boundedId)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdBoundedId(boundedId)));

  return(ContextMgr::GetPhasedInContext().getBoundedValue(boundedId));
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetDiscreteValue(discreteId)  [static]
//
//@ Interface-Description
//  Return a copy of the value of the discrete setting indicated by
//  'discreteId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward to the Phased-In Context.
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteValue
PhasedInContextHandle::GetDiscreteValue(
				  const SettingId::SettingIdType discreteId
				       )
{
  CALL_TRACE("GetDiscreteValue(discreteId)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdDiscreteId(discreteId)));

  return(ContextMgr::GetPhasedInContext().getDiscreteValue(discreteId));
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetBoundedValue(boundedId, boundedValue)  [static]
//
// Interface-Description
//  Set the value of the bounded setting identified by 'boundedId', to
//  the value given by 'boundedValue'.
//---------------------------------------------------------------------
// Implementation-Description
//  Forward on to the Phased-In Context.
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsBdBoundedId(boundedId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
PhasedInContextHandle::SetBoundedValue(
				  const SettingId::SettingIdType boundedId,
				  const BoundedValue&            boundedValue
				      )
{
  CALL_TRACE("SetBoundedValue(boundedId, boundedValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdBoundedId(boundedId)));

  ContextMgr::GetPhasedInContext().setBoundedValue(boundedId,
						   boundedValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetDiscreteValue(discreteId, discreteValue)  [static]
//
// Interface-Description
//  Set the value of the discrete setting identified by 'discreteId', to
//  the value given by 'discreteValue'.
//---------------------------------------------------------------------
// Implementation-Description
//  Forward on to the Phased-In Context.
//---------------------------------------------------------------------
// PreCondition
//  (SettingId::IsBdDiscreteId(discreteId))
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
PhasedInContextHandle::SetDiscreteValue(
				const SettingId::SettingIdType discreteId,
				DiscreteValue                  discreteValue
				       )
{
  CALL_TRACE("SetDiscreteValue(discreteId, discreteValue)");
  SAFE_CLASS_PRE_CONDITION((SettingId::IsBdDiscreteId(discreteId)));

  ContextMgr::GetPhasedInContext().setDiscreteValue(discreteId,
  						    discreteValue);
}

#endif // defined(SIGMA_DEVELOPMENT)


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	          [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//	and 'lineNumber' are essential pieces of information.  The
//	'pFileName' and 'pPredicate' strings may be defaulted in the macro
//	to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PhasedInContextHandle::SoftFault(const SoftFaultID  softFaultID,
			         const Uint32       lineNumber,
			         const char*        pFileName,
			         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  PHASED_IN_CONTEXT_HANDLE, lineNumber, pFileName,
			  pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
