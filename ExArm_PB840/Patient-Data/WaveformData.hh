#ifndef WaveformData_HH
#define WaveformData_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Struct: WaveformData - data structure defined for a waveform 
//    data packet.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/WaveformData.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//   
//  Revision: 006   By:   rhj   Date: 20-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added proxCircuitPressure to the waveform packet.
// 
//  Revision: 005   By:   rhj   Date: 23-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added proxManuever to the waveform packet.
// 
//  Revision: 004   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//        added fields that indicate whether this data is part of an
//         Apnea breath, or part of an "error" state (e.g., OSC,
//         disconnect, etc.)
//
//  Revision: 003   By: sah   Date:  11-Sep-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added assignment operator to allow for easier expansion
//         of data items
//      *  added lung pressure, lung flow and lung volume for use with
//         shadow traces
//      *  added a PEEP-recovery flag, mode value, and a spontaneous type
//         value for use in determining when to draw shadow traces
//
//  Revision: 002  By:  gdc    Date:  24-JUN-1998  DR Number: 
//       Project:  Color
//       Description:
//             Added breathType for colored waveforms.
//
//  Revision: 001  By:  jhv    Date:  17-Jan-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================
#include "SocketWrap.hh"
#include "BreathPhaseType.hh"
#include "BreathType.hh"
#include "DiscreteValue.hh"

//@Type: WaveformData
struct WaveformData
{
	inline void  operator=(const WaveformData& waveformData);

	inline void convHtoN();
	inline void convNtoH();

	Real32		circuitPressure;
	Real32		lungPressure;
	Real32  	netFlow;
	Real32		lungFlow;
	Real32		netVolume;
	Real32		lungVolume;
	BreathPhaseType::PhaseType	bdPhase;
	BreathType	breathType;
	Boolean		isPeepRecovery;
    Boolean		isApneaActive;
	Boolean		isErrorState;
    Boolean		isAutozeroActive;
	DiscreteValue modeSetting ;
	DiscreteValue supportTypeSetting ;
	Uint8       proxManuever;
	Real32		proxCircuitPressure;
};

inline void
WaveformData::operator=(const WaveformData& waveformData)
{
	circuitPressure    = waveformData.circuitPressure;
	lungPressure       = waveformData.lungPressure;
	netFlow            = waveformData.netFlow;
	lungFlow           = waveformData.lungFlow;
	netVolume          = waveformData.netVolume;
	lungVolume         = waveformData.lungVolume;
	bdPhase            = waveformData.bdPhase;
	breathType         = waveformData.breathType;
	isPeepRecovery     = waveformData.isPeepRecovery;
	isAutozeroActive   = waveformData.isAutozeroActive;
	modeSetting        = waveformData.modeSetting;
	supportTypeSetting = waveformData.supportTypeSetting;
    isApneaActive      = waveformData.isApneaActive;
   	isErrorState       = waveformData.isErrorState; 
	proxManuever       = waveformData.proxManuever;         
	proxCircuitPressure       = waveformData.proxCircuitPressure;         
}	// $[TI1]

inline void
WaveformData::convHtoN()
{
	SocketWrap sock;
	circuitPressure = sock.HTONF(circuitPressure);
	lungPressure = sock.HTONF(lungPressure);
	netFlow = sock.HTONF(netFlow);
	lungFlow = sock.HTONF(lungFlow);
	netVolume = sock.HTONF(netVolume);
	lungVolume = sock.HTONF(lungVolume);
	bdPhase            = (BreathPhaseType::PhaseType)sock.HTONL((Uint32)bdPhase);
	breathType         = (BreathType)sock.HTONL((Uint32)breathType);
	//isPeepRecovery is (uchar) no need to convert
	//isAutozeroActive is (uchar) no need to convert
	modeSetting        = (DiscreteValue)sock.HTONS(modeSetting);
	supportTypeSetting = (DiscreteValue)sock.HTONS(supportTypeSetting);
    //isApneaActive is (uchar) no need to convert
   	//isErrorState is (uchar) no need to convert 
	//proxManuever is u char do not convert
	proxCircuitPressure = sock.HTONF(proxCircuitPressure);
}

inline void
WaveformData::convNtoH()
{
	SocketWrap sock;
	circuitPressure = sock.NTOHF(circuitPressure);
	lungPressure = sock.NTOHF(lungPressure);
	netFlow = sock.NTOHF(netFlow);
	lungFlow = sock.NTOHF(lungFlow);
	netVolume = sock.NTOHF(netVolume);
	lungVolume = sock.NTOHF(lungVolume);
	bdPhase            = (BreathPhaseType::PhaseType)sock.NTOHL((Uint32)bdPhase);
	breathType         = (BreathType)sock.NTOHL((Uint32)breathType);
	//isPeepRecovery is (uchar) no need to convert
	//isAutozeroActive is (uchar) no need to convert
	modeSetting        = (DiscreteValue)sock.NTOHS(modeSetting);
	supportTypeSetting = (DiscreteValue)sock.NTOHS(supportTypeSetting);
    //isApneaActive  is (uchar) no need to convert
   	//isErrorState is (uchar) no need to convert 
	//proxManuever is uchar do not convert
	proxCircuitPressure = sock.NTOHF(proxCircuitPressure);
}

struct WaveformDataBuf
{
	WaveformData data;
	WaveformDataBuf *pNext;
};

#endif // WaveformData_HH 
