#ifndef PbTimerTest_HH
#define PbTimerTest_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1998, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
// Class: PbTimerTest - Power on self test for PB (EPLD based) timers.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/PbTimerTest.hhv   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 21-Oct-1998  DCS Number: 5311
//    Project: 840 Cost Reduction
//    Description:
//       Initial version
//
//=====================================================================

#include "kpost.hh"

class PbTimerTest
{
  public:
	static Criticality Test(void);

  private:
	static Boolean TestRegisters_(void);
	static Boolean TestRegister_(volatile Uint8 * pControlReg, 
				 		   		 volatile Uint32 * pPreloadReg,
				 		   		 volatile Uint32 * pCounterReg,
				 		   		 Uint32 initCount);
	static Boolean TestInterrupts_(void);
	static Boolean TestAccuracy_(void);
	static void    DisableTimers_(void);

	// TODO E600 removed the keyword interrupt from the following four functions
	static void InterruptHandler_(void);
	static void Av3Handler_(void);
	static void Av4Handler_(void);
	static void Av6Handler_(void);
};

#endif 	// PbTimerTest_HH
