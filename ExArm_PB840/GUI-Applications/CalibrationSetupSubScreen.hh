#ifndef CalibrationSetupSubScreen_HH
#define CalibrationSetupSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CalibrationSetupSubScreen - Activated by selecting Exhalation Valve
// Calibration button on the the Service lower subScreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/CalibrationSetupSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $
//
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//  Revision: 003  By: yyy      Date: 19_Aug-1997  DR Number: 2340
//    Project:  Sigma (R8027)
//    Description:
//    Qualified adjustPanelClearPressHappened() to handle the CLEAR key
//	  only when running EXH_VALVE_CALIB_TEST_ID test.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2035
//    Project:  Sigma (R8027)
//    Description:
//      Added new promot "Connect AC" index.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"
#include "GuiTimerTarget.hh"
#include "AdjustPanelTarget.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "GuiTestManager.hh"
#include "ServiceStatusArea.hh"
#include "SmPromptId.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
class TestResult;
//@ End-Usage

class CalibrationSetupSubScreen : public SubScreen, public GuiTimerTarget,
							public AdjustPanelTarget,
							public GuiTestManagerTarget 
{
public:
	CalibrationSetupSubScreen(SubScreenArea *pSubScreenArea);
	~CalibrationSetupSubScreen(void);

	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);
	virtual void adjustPanelRestoreFocusHappened(void);

	// GuiTestMangerTarget virtual methods
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);



	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	enum CalConditionId
	{
		MAX_CONDITION_ID = 20 
	};

	enum CalScreenId
	{
		CALIBRATION_END,
		CALIBRATION_FAILED,
		CALIBRATION_PROMPT_ACCEPTED_CLEARED,
		CALIBRATION_SETUP,
		CALIBRATION_START_ACCEPTED,
		MAX_CAL_SCREENS
	};

	enum CalPromptId
	{
		EXH_CAL_CONNECT_AC_PROMPT,
		EXH_CAL_REMOVE_INSP_FILTER_PROMPT,
		EXH_CAL_CONNECT_AIR_PROMPT,
		EXH_CAL_PRESSURE_SENSOR_ALERT_PROMPT,
		MAX_CAL_PROMPTS
	};

	enum TestStateId
	{
		PROMPT_ACCEPT_BUTTON_PRESSED,
		START_BUTTON_PRESSED,
		UNDEFINED_TEST_STATE,
		MAX_TEST_STATES
	};

	// these methods are purposely declared, but not implemented...
	CalibrationSetupSubScreen(void);						// not implemented...
	CalibrationSetupSubScreen(const CalibrationSetupSubScreen&);	// not implemented...
	void operator=(const CalibrationSetupSubScreen&);		// not implemented...

	void layoutScreen_(CalScreenId calScreenId);
	
	void setPromptTable_(void);
	void setErrorTable_(void);
	void startTest_(SmTestId::ServiceModeTestId currentTestId);
	void nextTest_(void);
	void setupForRestartCalTest_(void);

	//@ Data-Member: calStatus_
	// A container for displaying the current Exhalation Calibration status.
	ServiceStatusArea calStatus_;

	//@ Data-Member: calWarningMsg_
	// A TextField objects used to display calibration warning message.
	// lower screen
	TextField calWarningMsg_;

	//@ Data-Member: errDisplayArea_
	// An array of TextField objects used to display test conditions in the
	// lower screen
	TextField errDisplayArea_[MAX_CONDITION_ID];
	
	//@ Data-Member: conditionText_
	// Text for error condition prompts
	StringId conditionText_[MAX_CONDITION_ID];

	//@ Data-Member: errorHappened_
	// Flag that indicates status of test
	Int	errorHappened_;
	
	//@ Data-Member: errorIndex_
	// Keep tracks of index into TextFields in the error display area
	Int	errorIndex_;
	
	//@ Data-Member: keyAllowedId_
	Int keyAllowedId_;

	//@ Data-Member: userKeyPressedId_
	// Hold the operator pressed key Id.
	SmPromptId::ActionId userKeyPressedId_;

	//@ Data-Member: promptId_
	// The prompt id.
	SmPromptId::PromptId promptId_[MAX_CAL_PROMPTS];
	
	//@ Data-Member: promptName_
	// The prompts.
	StringId promptName_[MAX_CAL_PROMPTS];

	//@ Data-Member: startTestButton_
	// The StartTest button is used to start the Exhalation Valve Calibration testing.
	TextButton startTestButton_;
	
	//@ Data-Member: testStateId_
	// The test status identifier.
	TestStateId testStateId_;
	
	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: guiTestMgr_ 
	// Object instance of GuiTestManager
	GuiTestManager guiTestMgr_;

	//@ Data-Member: currentTestId_
	// Object instance of GuiTestManager
	SmTestId::ServiceModeTestId currentTestId_;

};

#endif // CalibrationSetupSubScreen_HH 
