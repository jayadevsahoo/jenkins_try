/*****************************************************************************/
/*   Module Name:    DETFUN.h                                            
/*   Purpose:       Functions called by com detection routines to display connection 
/*                  information and to handle the cancel system.                                                         
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Added handler for messages back to     Dave Richardson   19 Nov, 2002
/*   panel containing list of images for
/*   software download
/*    
/*   $Revision::   1.2      $                                                
/*
/*   $LOG$
/*
/*****************************************************************************/

/*****************************************************************************
 *   THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS               
 *   EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,           
 *   STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY      
 *   MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE       
 *   PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.                             
 *   Copyright (c) 1995-2002, Puritan-Bennett                                                  
 *   All rights reserved.                                                                                                                                                * 
 *****************************************************************************************/

#ifndef _DETFUN_H_
#define _DETFUN_H_

void Register_Splash_CB(void);
void Register_Logon_CB(void);
void Register_Sys_CB(void);
void Register_Setup_CB(void);
void Register_ImageSetup_CB(void);
void StopPTSDetect(void);
void Stop840Detect(void);
void StopDetect(void);
void ResetDetect(void);

#endif //_DETFUN_H_
