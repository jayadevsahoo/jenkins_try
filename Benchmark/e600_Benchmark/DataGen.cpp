#include "StdAfx.h"
#include "DataGen.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

// data samples generated from a bitmap image
int DataSamples[] = {
	97,99,101,103,105,107,109,112,114,116,118,121,123,125,127,129,131,134,136,138,140,142,144,146,148,150,152,154,155,157,159,160,162,163,164,165,166,167,167,166,165,163,159,151,135,115,105,100,97,96,95,94,93,93,92,92,91,91,91,91,91,91,91,92,92,92,92,92,93,93,93,93,93,94,94,94,91,84,77,69,62,54,48,42,36,31,27,24,21,20,21,24,28,34,42,50,60,68,79,89,99,108,117,126,134,141,147,153,158,162,165,168,170,170,171,171,171,171,170,170,169,167,166,165,163,161,159,157,156,153,151,149,147,144,142,140,137,135,132,129,127,124,122,119,116,114,111,108,106,104
};

// data sample length, how many data in this array
const int DataSampleLen = sizeof(DataSamples)/sizeof(int);

DataGen::DataGen(void)
{
	srand( GetTickCount() );
	dataIndex = 0;
}

DataGen::~DataGen(void)
{
}

int DataGen::get( int range )
{
	if ( USE_DATA_SAMPLE )
	{
		dataIndex = dataIndex % DataSampleLen;
		int data = DataSamples[dataIndex];
		dataIndex ++;
		return data;
	}
	return (rand() % range + 1);
}