#pragma once


class Event
{
public:

	enum
	{
		EVENT_SETTING_XACTION,		// event for setting transaction thread start/stop
		MAX_EVENT_ID
	};


	Event(void);
	~Event(void);


	// initialize the event table
	static void Initialize();

	// get a system event HANDLE by event ID
	static HANDLE Query( Uint32 eventId );

private:

	// an array to store all event handles
	static HANDLE EventLookup_[Event::MAX_EVENT_ID];
};
