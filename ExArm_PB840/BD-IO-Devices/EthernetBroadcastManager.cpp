#include "stdafx.h"
#include "EthernetBroadcastManager.h"
#include "ExceptionHandler.h"
#include "GlobalObjects.h"
#include "BDIORefs.hh"
#include "BDIOTest.h"

static bool bBroadcastEnabled = false;
static int const TOTAL_NUMBER_OF_CHANNELS = 18;

// global instance of the EthernetBroadcastManager
EthernetBroadcastManager_t EthernetBroadcastManager;

static SOCKET	Socket = 0;
static struct sockaddr_in SockAddr;
static EthernetBroadcastMsg SendMsg_;
static CRITICAL_SECTION EthernetBroadcastCriticalSection;

#define BD_ACK_DATA							L"OK"
#define BD_PING_DATA						L"PING"

#define NETWORK_BYTE_ORDER					1
#define COMPUTE_MSG_CHECKSUM				1

struct EthernetResponseMsg_t
{
	UINT16		msgId;
	UINT16		pktSize;
	UINT		seqNumber;	// = 0;
	wchar_t		Response[4];
	UINT		CheckSum;
};
struct EthernetPingMsg_t
{
	UINT16		msgId;
	UINT16		pktSize;
	UINT		seqNumber;	// = 0;
	wchar_t		Ping[8];
	UINT		CheckSum;
};

static EthernetResponseMsg_t ResponseMsg;
static EthernetPingMsg_t PingMsg;

#define FILL_RESPONSE_MSG \
	ResponseMsg.msgId = BD_RESPONSE_MSG_ID; \
	ResponseMsg.pktSize = sizeof(ResponseMsg.Response); \
	ResponseMsg.seqNumber = 0; \
	wcscpy_s(ResponseMsg.Response, sizeof(ResponseMsg.Response) / 2, BD_ACK_DATA);

//*****************************************************************************
//  Helper Functions  
//*****************************************************************************
UINT CalMsgChecksum(char *pData, UINT *pChecksum, int Size)
{
	UINT Checksum = 0;
	char *pChecksumMsg = pData;

	for(int i = 0; i < Size; i++)
	{
		Checksum += *pChecksumMsg++;
	}
	
	if(pChecksum)
		*pChecksum = Checksum;
	
	return Checksum;
}

//*****************************************************************************
void SwapNetworkByteOrder(UINT *pSrc, UINT *pDst, int Size)
{
	UINT *puiSrc = (UINT *)pSrc;
	UINT *puiDst = (UINT *)pDst;

	int MsgLongSize = Size / 4;
	for(int i = 0; i < MsgLongSize; i++)
	{
		*puiDst++ = htonl(*puiSrc++);
	}
}

//*****************************************************************************
void SendMsg(char *pMsg, int Size)
{
	UINT *pChecksum = (UINT *)(pMsg + Size - sizeof(UINT));

#if COMPUTE_MSG_CHECKSUM
	CalMsgChecksum(pMsg, pChecksum, Size - sizeof(UINT));
#else
	*pChecksum = 0;
#endif

#if NETWORK_BYTE_ORDER
	// swap the entire message except the checksum
	SwapNetworkByteOrder((UINT *)pMsg, (UINT *)pMsg, Size);
#endif

	if(sendto(Socket,(char *)pMsg, Size, 0,
				(sockaddr *)(&SockAddr), sizeof(SockAddr)) < 0)
	{
		RETAILMSG(0, (_T("SendMsg: error (%d)\r\n"), GetLastError()));
	}
	else
	{
		RETAILMSG(0, (_T("SendMsg: Success\r\n")));
	}
}

//*****************************************************************************
//  EthernetBroadcastThread_t::EthernetBroadcastManager_t  
//*****************************************************************************
EthernetBroadcastManager_t::EthernetBroadcastManager_t()
: slotIndex_(0),
sendMsgIndex_(1),
collectMsgIndex_(0),
//Socket(0),
isTransmitInProgress_(false)
{
	//initialize();
	SocketInitialized = false;
	InitializeCriticalSection(&EthernetBroadcastCriticalSection);
}

EthernetBroadcastManager_t::~EthernetBroadcastManager_t()
{
	closesocket(Socket);
}

void
EthernetBroadcastManager_t::collect(ConvertedA2dReadings_t A2dData) 
{
    if (slotIndex_ < EthernetBroadcastMsg::NUM_SLOTS)
	{
		msg_[collectMsgIndex_].signalData[0][slotIndex_] = A2dData.AirFlow;
		msg_[collectMsgIndex_].signalData[1][slotIndex_] = A2dData.O2Flow;
		msg_[collectMsgIndex_].signalData[2][slotIndex_] = A2dData.ExhalationFlow;
		msg_[collectMsgIndex_].signalData[3][slotIndex_] = A2dData.AirPressure;
		msg_[collectMsgIndex_].signalData[4][slotIndex_] = A2dData.O2Pressure;
		msg_[collectMsgIndex_].signalData[5][slotIndex_] = A2dData.InspPressure;
		msg_[collectMsgIndex_].signalData[6][slotIndex_] = A2dData.ExpPressure;
		msg_[collectMsgIndex_].signalData[7][slotIndex_] = A2dData.AirwayPressure;
		msg_[collectMsgIndex_].signalData[8][slotIndex_] = A2dData.ExhDrivePressure;
		msg_[collectMsgIndex_].signalData[9][slotIndex_] = A2dData.SupplyPressure;
		msg_[collectMsgIndex_].signalData[10][slotIndex_] = A2dData.FiO2;
		msg_[collectMsgIndex_].signalData[11][slotIndex_] = A2dData.FilteredAirFlow;
		msg_[collectMsgIndex_].signalData[12][slotIndex_] = A2dData.FilteredO2Flow;
		msg_[collectMsgIndex_].signalData[13][slotIndex_] = A2dData.FilteredExhFlow;
		msg_[collectMsgIndex_].signalData[14][slotIndex_] = A2dData.FilteredInspPressure;
		msg_[collectMsgIndex_].signalData[15][slotIndex_] = A2dData.FilteredExpPressure;
		msg_[collectMsgIndex_].signalData[16][slotIndex_] = A2dData.FilteredAirwayPressure;
		msg_[collectMsgIndex_].signalData[17][slotIndex_] = A2dData.FilteredExhDrivePressure;
		
		// collect other signals here up to [NUM_SIGNALS-1][slotIndex_]
		slotIndex_++;
	}
	if (slotIndex_ >= EthernetBroadcastMsg::NUM_SLOTS)
	{
		// avoid changing buffers if we are in the middle of transmitting
		if (!isTransmitInProgress_)
		{
			int temp = sendMsgIndex_ ;
			sendMsgIndex_ = collectMsgIndex_ ;
			collectMsgIndex_ = temp ;
			isTransmitInProgress_ = true;		
			send();
			slotIndex_ = 0;
		}
		else
		{
			RETAILMSG(TRUE, (_T("Ethernet TX lagging\r\n")));
		}
	}
}

void
EthernetBroadcastManager_t::collect(ConvertedA2dReadings_t A2dData, RawA2dReadings_t RawA2dReadings, CommandValues_t CommandValues) 
{
#if 0
// For E600
	if (slotIndex_ < EthernetBroadcastMsg::NUM_SLOTS)
	{
		msg_[collectMsgIndex_].signalData[0][slotIndex_] = A2dData.O2Flow;
		msg_[collectMsgIndex_].signalData[1][slotIndex_] = A2dData.AirFlow;
		msg_[collectMsgIndex_].signalData[2][slotIndex_] = A2dData.ExhalationFlow;
		msg_[collectMsgIndex_].signalData[3][slotIndex_] = A2dData.InspPressure;
		msg_[collectMsgIndex_].signalData[4][slotIndex_] = A2dData.ExpPressure;
		msg_[collectMsgIndex_].signalData[5][slotIndex_] = A2dData.ExhDrivePressure;
		msg_[collectMsgIndex_].signalData[6][slotIndex_] = CommandValues.DesiredAirFlow;
		msg_[collectMsgIndex_].signalData[7][slotIndex_] = CommandValues.DesiredO2Flow;
		msg_[collectMsgIndex_].signalData[8][slotIndex_] = CommandValues.DesiredPressure;
		msg_[collectMsgIndex_].signalData[9][slotIndex_] = RawA2dReadings.O2Flow.Value;
		msg_[collectMsgIndex_].signalData[10][slotIndex_] = A2dData.AirwayPressure;	// Bar Press in E600 Eng
		msg_[collectMsgIndex_].signalData[11][slotIndex_] = CommandValues.AirDac;
		msg_[collectMsgIndex_].signalData[12][slotIndex_] = CommandValues.O2Dac;
		msg_[collectMsgIndex_].signalData[13][slotIndex_] = CommandValues.ExhalationDAC;
		msg_[collectMsgIndex_].signalData[14][slotIndex_] = RawA2dReadings.AirTemp.Value;
		msg_[collectMsgIndex_].signalData[15][slotIndex_] = RawA2dReadings.ExhaleFlow.Value;
		msg_[collectMsgIndex_].signalData[16][slotIndex_] = RawA2dReadings.AirFlow.Value;
		msg_[collectMsgIndex_].signalData[17][slotIndex_] = RawA2dReadings.O2Temp.Value;
        
       // collect other signals here up to [NUM_SIGNALS-1][slotIndex_]
		slotIndex_++;
	}
#else
// For PB880
	if (slotIndex_ < EthernetBroadcastMsg::NUM_SLOTS)
	{
		msg_[collectMsgIndex_].signalData[0][slotIndex_] = A2dData.O2Flow;					// O2 Flow
		msg_[collectMsgIndex_].signalData[1][slotIndex_] = A2dData.AirFlow;					// Air Flow
		msg_[collectMsgIndex_].signalData[2][slotIndex_] = A2dData.ExhalationFlow;			// Exh. Flow
		msg_[collectMsgIndex_].signalData[3][slotIndex_] = A2dData.InspPressure;			// Insp Pressure
		msg_[collectMsgIndex_].signalData[4][slotIndex_] = A2dData.ExpPressure;				// Exp. Pressure
		msg_[collectMsgIndex_].signalData[5][slotIndex_] = CommandValues.ExhalationDAC;		// Exh Valve Command
		msg_[collectMsgIndex_].signalData[6][slotIndex_] = CommandValues.DesiredAirFlow;	// Desired Air Flow
		msg_[collectMsgIndex_].signalData[7][slotIndex_] = CommandValues.DesiredO2Flow;		// Desired O2 Flow
		msg_[collectMsgIndex_].signalData[8][slotIndex_] = CommandValues.DesiredPressure;	// Desired Pressure
		msg_[collectMsgIndex_].signalData[9][slotIndex_] = RawA2dReadings.O2Flow.Value;		// ADC O2 Flow
		msg_[collectMsgIndex_].signalData[10][slotIndex_] = RawA2dReadings.AirFlow.Value;	// ADC Air Flow
		msg_[collectMsgIndex_].signalData[11][slotIndex_] = CommandValues.AirDac;			// DAC Air Psol
		msg_[collectMsgIndex_].signalData[12][slotIndex_] = CommandValues.O2Dac;			// DAC O2 Psol
		msg_[collectMsgIndex_].signalData[13][slotIndex_] = A2dData.ExhDrivePressure;		// Exh Drive Pressure	
		msg_[collectMsgIndex_].signalData[14][slotIndex_] = RawA2dReadings.O2Temp.Value;	// Temp O2 Flow
		msg_[collectMsgIndex_].signalData[15][slotIndex_] = RawA2dReadings.AirTemp.Value;	// Temp Air Flow
		msg_[collectMsgIndex_].signalData[16][slotIndex_] = RawA2dReadings.ExhaleFlow.Value;// ADC Exh Flow
		msg_[collectMsgIndex_].signalData[17][slotIndex_] = CommandValues.Phase;			// Phase
#endif        
       // collect other signals here up to [NUM_SIGNALS-1][slotIndex_]
		slotIndex_++;
	}

	if (slotIndex_ >= EthernetBroadcastMsg::NUM_SLOTS)
	{
		// avoid changing buffers if we are in the middle of transmitting
		if (!isTransmitInProgress_)
		{
			int temp = sendMsgIndex_ ;
			sendMsgIndex_ = collectMsgIndex_ ;
			collectMsgIndex_ = temp ;
			isTransmitInProgress_ = true;		
			send();
			slotIndex_ = 0;
		}
		else
		{
			RETAILMSG(TRUE, (_T("Ethernet TX lagging\r\n")));
		}
	}
}

void EthernetBroadcastManager_t::SendDebug(float * pData) 
{
	if (slotIndex_ < EthernetBroadcastMsg::NUM_SLOTS)
	{
		msg_[collectMsgIndex_].signalData[0][slotIndex_] = pData[0];
		msg_[collectMsgIndex_].signalData[1][slotIndex_] = pData[1];
		msg_[collectMsgIndex_].signalData[2][slotIndex_] = pData[2];
		msg_[collectMsgIndex_].signalData[3][slotIndex_] = pData[3];
		msg_[collectMsgIndex_].signalData[4][slotIndex_] = pData[4];
		msg_[collectMsgIndex_].signalData[5][slotIndex_] = pData[5];
		msg_[collectMsgIndex_].signalData[6][slotIndex_] = pData[6];
		msg_[collectMsgIndex_].signalData[7][slotIndex_] = pData[7];
		msg_[collectMsgIndex_].signalData[8][slotIndex_] = pData[8];
		msg_[collectMsgIndex_].signalData[9][slotIndex_] = pData[9];
		msg_[collectMsgIndex_].signalData[10][slotIndex_] = pData[10];
		msg_[collectMsgIndex_].signalData[11][slotIndex_] = pData[11];
		msg_[collectMsgIndex_].signalData[12][slotIndex_] = pData[12];
		msg_[collectMsgIndex_].signalData[13][slotIndex_] = pData[13];
		msg_[collectMsgIndex_].signalData[14][slotIndex_] = pData[14];
		msg_[collectMsgIndex_].signalData[15][slotIndex_] = pData[15];
		msg_[collectMsgIndex_].signalData[16][slotIndex_] = pData[16];
		msg_[collectMsgIndex_].signalData[17][slotIndex_] = pData[17];
		
		// collect other signals here up to [NUM_SIGNALS-1][slotIndex_]
		slotIndex_++;
	}

	if (slotIndex_ >= EthernetBroadcastMsg::NUM_SLOTS)
	{
		// avoid changing buffers if we are in the middle of transmitting
		if (!isTransmitInProgress_)
		{
			int temp = sendMsgIndex_ ;
			sendMsgIndex_ = collectMsgIndex_ ;
			collectMsgIndex_ = temp ;
			isTransmitInProgress_ = true;		
			send();
			slotIndex_ = 0;
		}
		else
		{
			RETAILMSG(TRUE, (_T("Ethernet TX lagging\r\n")));
		}
	}
}


void EthernetBroadcastManager_t::SendPacket(char *pPacket, int Size)
{
	EnterCriticalSection(&EthernetBroadcastCriticalSection);

	SendMsg(pPacket, Size);

	LeaveCriticalSection(&EthernetBroadcastCriticalSection);
}


int EthernetBroadcastManager_t::initialize()
{
	memset(msg_, 0, sizeof(msg_));
	for (UINT i=0; i<NUM_MSG_BUFFERS; ++i)
	{
		msg_[i].msgId = BD_DEBUG_MSG_ID;
		msg_[i].pktSize = sizeof(msg_[0].signalData);
	}

	if (Socket == 0)
	{
		WSADATA wsaData = {0};
		int iResult = 0;
	
		// Initialize Winsock
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			wprintf(L"WSAStartup failed: %d\n", iResult);
			return -1;
		}
		
		//Create a UDP socket
		if((Socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) 
		{
			RETAILMSG(TRUE, (_T("socket: error (%d)\r\n"), Socket));
			return 1;
		}
#if 0
		nOptiontValue = 1;		
	
		//Set socket options to broadcast
		if(setsockopt(Socket, SOL_SOCKET, SO_BROADCAST, 
			reinterpret_cast<char *>(&nOptiontValue), 
			sizeof (nOptiontValue)) < 0) 
		{
			closesocket(Socket);
			RETAILMSG(TRUE, (_T("setsockopt: error (%d)\r\n"), GetLastError()));
			return 2;
		}
#else
		SOCKADDR_IN sadr;
		int rc;
	
		// Fill in socket address structure
		memset(&sadr, 0, sizeof(sadr));
		sadr.sin_family = AF_INET;
		//192.168.0.3
		sadr.sin_addr.s_addr = htonl(0xC0A80003); //broadcast address
		sadr.sin_port = htons(50001);

		// Bind address to socket
		rc = bind(Socket, (struct sockaddr *)&sadr, sizeof(sadr));
		if(rc == SOCKET_ERROR)
		{
			return 2;
		}
#endif
		//Set broadcast address
		SockAddr.sin_family = AF_INET;
		SockAddr.sin_addr.s_addr = htonl(0xffffffff); //broadcast address
		SockAddr.sin_port = htons(EthernetBroadcastManager_t::UDP_PORT_ID);  
	}
	SocketInitialized = true;
	return 0;
}

#if 0
void EthernetBroadcastManager_t::SwapNetworkByteOrder(UINT *pSrc, UINT *pDst, int Size)
{
	char *pchSrc = (char *)pSrc;
	UINT *puiSrc = (UINT *)pSrc;
	UINT *puiDst = (UINT *)pDst;

	int MsgLongSize = (Size / 4) - 1;
	for(int i = 0; i < MsgLongSize; i++)
	{
		*puiDst++ = htonl(*puiSrc++);
	}
#if COMPUTE_MSG_CHECKSUM
	CalMsgChecksum(pchSrc, (UINT *)puiDst, Size - sizeof(UINT));
#else
	*puiDst = 0;
#endif
}
#endif

void EthernetBroadcastManager_t::send()
{	
	EnterCriticalSection(&EthernetBroadcastCriticalSection);

	if(bBroadcastEnabled)
	{
		char *pSendMsg = (char *)&SendMsg_;
#if COMPUTE_MSG_CHECKSUM
		CalMsgChecksum(pSendMsg, &msg_[sendMsgIndex_].CheckSum, sizeof(*msg_) - sizeof(UINT));
#else
		msg_[sendMsgIndex_].CheckSum = 0;
#endif				

#if NETWORK_BYTE_ORDER
		// swap the entire message except the checksum
		SwapNetworkByteOrder((UINT *)&msg_[sendMsgIndex_], (UINT *)&SendMsg_, sizeof(*msg_));
#else
		pSendMsg = (char *)&msg_[sendMsgIndex_];
#endif

		if(sendto(Socket,(char *)pSendMsg, sizeof(*msg_)+1, 0,
				(sockaddr *)&SockAddr, sizeof(SockAddr)) < 0)
		{
			//closesocket(Socket);
			//Error
			RETAILMSG(TRUE, (_T("sendto: error (%d)\r\n"), GetLastError()));
		}
		else
		{
			//RETAILMSG(TRUE, (_T("sendto: Success\r\n")));
		}
	}
	isTransmitInProgress_ = false;

	LeaveCriticalSection(&EthernetBroadcastCriticalSection);
}
//////////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////////

//*****************************************************************************
//
//  EthernetBroadcastRXThread_t::EthernetBroadcastRXThread_t constructor
//
//*****************************************************************************
void EthernetBroadcastRXThread_t::Initialize()
{
	if(EthernetBroadcastManager.initialize() == 0)
	{
		DEBUGMSG(1, (L"EthernetBroadcastRXThread_t: BroadcastManager init successfully\r\n"));
	}

	ResponseMsg.msgId = BD_RESPONSE_MSG_ID;
	ResponseMsg.pktSize = sizeof(ResponseMsg.Response);
	ResponseMsg.seqNumber = 0;
	//ResponseMsg.Response = BD_ACK_DATA;
	wcscpy_s(ResponseMsg.Response, sizeof(ResponseMsg.Response) / 2, BD_ACK_DATA);
	//CalMsgChecksum((char *)&ResponseMsg, &ResponseMsg.CheckSum, sizeof(ResponseMsg) - sizeof(UINT));

	PingMsg.msgId = BD_PING_MSG_ID;
	PingMsg.pktSize = sizeof(PingMsg.Ping);
	PingMsg.seqNumber = 0;
	//PingMsg.Ping = BD_PING_DATA;
	wcscpy_s(PingMsg.Ping, sizeof(PingMsg.Ping) / 2, BD_PING_DATA);
	//CalMsgChecksum((char *)&PingMsg, &PingMsg.CheckSum, sizeof(PingMsg) - sizeof(UINT));
}

#if 0
void EthernetBroadcastRXThread_t::SwapNetworkByteOrder(UINT *pSrc, UINT *pDst, int Size)
{
	char *pchSrc = (char *)pSrc;
	UINT *puiSrc = (UINT *)pSrc;
	UINT *puiDst = (UINT *)pDst;

	int MsgLongSize = (Size / 4) - 1;
	for(int i = 0; i < MsgLongSize; i++)
	{
		*puiDst++ = htonl(*puiSrc++);
	}
#if COMPUTE_MSG_CHECKSUM
	CalMsgChecksum(pchSrc, (UINT *)puiDst, Size - sizeof(UINT));
#else
	*puiDst = 0;
#endif
}
#endif

//*****************************************************************************
//
//  EthernetBroadcastThread_t::ProcessRXCommand
//  
//  Process Ethernet messages
//*****************************************************************************
void EthernetBroadcastRXThread_t::ProcessRXCommand(char RxBuf[], int Size)
{
	char *pSendMsg = NULL;
	int SizeToSend = 0;
	UINT16 Command = *(UINT16 *)RxBuf;
	UINT16 *pData = (UINT16 *)(RxBuf + 2);
	bool bSendResponse = false;
	bool bExit = false;
		
	switch(Command)
	{
	case PC_PING_MSG_ID :
		PingMsg.msgId = BD_PING_MSG_ID;
		PingMsg.pktSize = sizeof(PingMsg.Ping);
		PingMsg.seqNumber = 0;
		wcscpy_s(PingMsg.Ping, sizeof(PingMsg.Ping) / 2, BD_PING_DATA);
		
		pSendMsg = (char *)&PingMsg;
		SizeToSend = sizeof(EthernetPingMsg_t);
		break;
	case PC_DISABLE_BROADCAST_MSG_ID:
		bBroadcastEnabled = false;
		bSendResponse = true;
		break;
	case PC_ENABLE_BROADCAST_MSG_ID:
		bBroadcastEnabled = true;
		bSendResponse = true;		
		break;
	case PC_CALIBRATION_MSG_ID:
		BdioTest.SetCalType((BdioCalType_t)*pData);
		bSendResponse = true;
		break;
	case PC_TEST_MSG_ID:
		BdioTest.SetTestType((BdioTestType_t)*pData);
		bSendResponse = true;
		break;
	case PC_CAL_CANCEL_MSG_ID:
		BdioTest.CancelCal((BdioCalType_t)*pData);
		bSendResponse = true;
		break;
	case PC_SEND_SETTINGS_MSG_ID:
		BdioTest.ProcessRxSettingsPacket(RxBuf);
		bSendResponse = true;
		break;
	case PC_REQUEST_SETTINGS_MSG_ID:
		BdioTest.ProcessTxSettingsPacket(RxBuf);
		bExit = true;
		break;
	case PC_SEND_SETTINGS_DEBUG_MSG_ID:
		BdioTest.ProcessRxSettingsDebugPacket(RxBuf);
		bSendResponse = true;
		break;
	default:
		return;
	}

	if(bExit)
	{
		return;
	}

	if(bSendResponse)
	{
		FILL_RESPONSE_MSG
		pSendMsg = (char *)&ResponseMsg;
		SizeToSend = sizeof(EthernetResponseMsg_t);
	}

	EnterCriticalSection(&EthernetBroadcastCriticalSection);

	UINT *pChecksum = (UINT *)(pSendMsg + SizeToSend - sizeof(UINT));

#if COMPUTE_MSG_CHECKSUM
	CalMsgChecksum(pSendMsg, pChecksum, Size - sizeof(UINT));
#else
	*pChecksum = 0;
#endif

#if NETWORK_BYTE_ORDER
	// swap the entire message including the checksum
	SwapNetworkByteOrder((UINT *)pSendMsg, (UINT *)pSendMsg, SizeToSend);
#endif

	if(sendto(Socket,(char *)pSendMsg, SizeToSend, 0,
				(sockaddr *)(&SockAddr), sizeof(SockAddr)) < 0)
	{
		RETAILMSG(TRUE, (_T("sendto: error (%d)\r\n"), GetLastError()));
	}
	else
	{
		RETAILMSG(TRUE, (_T("ProcessRXCommand: Success\r\n")));
	}
	LeaveCriticalSection(&EthernetBroadcastCriticalSection);
}
//*****************************************************************************
//
//  EthernetBroadcastThread_t::ThreadProcedure -  
//
//*****************************************************************************
uint32_t EthernetBroadcastRXThread_t::ThreadProcedure()
{	
	int rc;
	char RxBuf[300];
	//SOCKADDR_IN FromSocketAddr;
	//int FromSocketLen;

	EthernetBroadcastRXThread_t::Initialize();
	for(;;)
	{
		//rc = recvfrom(Socket, RxBuf, sizeof(RxBuf), 0, (SOCKADDR *)&FromSocketAddr, &FromSocketLen);
		rc = recv(Socket, RxBuf, sizeof(RxBuf), 0);
		if(rc == SOCKET_ERROR)
		{
			RETAILMSG(TRUE, (_T("Error receiving from Ethernet, Error = %d\r\n"), GetLastError()));
			break;
		}
		else
		{
			RETAILMSG(TRUE, (_T("Success receiving from Ethernet\r\n")));
			ProcessRXCommand(RxBuf, rc);
		}
	}
	return 0;
}
//#############################################################################
//
//  ThreadStartUp - This function gives the Win32 function ::CreateThread an
//      address to be passed as the startup location.
//
//#############################################################################
void EthernetBroadcastRXThread_t::Thread()
{
	__try
	{
		uint32_t ThreadResult = ThreadProcedure();
		UNUSED_SYMBOL(ThreadResult);
	}
	__except(ExceptionHandler.EvaluateException(GetExceptionInformation()))
	{
		// Get exception info
		int32_t Code = _exception_code();
		UNUSED_SYMBOL(Code); 
	}
}

                                                                                                                                                               
