#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Drawable - Represents an object that occupies an area on the screen.
//---------------------------------------------------------------------
//@ Interface-Description
// The abstract Drawable class is the most basic piece of the screen
// management framework.  All objects that appear on the screen are
// derived from Drawable.  In addition, the Container class (which
// manages groups of screen objects) is derived from Drawable.  This
// class is abstract - it cannot be instantiated.  It is provided to serve
// as a base class for all Drawable objects.
//
// The Drawable class maintains an area which is specified relative to the
// origin of its parent Container.  This coordinate system is used by most
// methods.  Some member functions are supplied which instead specify the area
// relative to the origin of the screen.  This is to facilitate certain
// operations, such as handling of touch-related events, that must deal with
// the screen as a whole.
//
// The Drawable class is "aware" of the screen management framework.  It
// knows which Container it is in, and it also knows which BaseContainer
// (top-level Container) it is in.  It knows how to append itself to a
// Container and how to remove itself from a Container.  When changed,
// it is capable of registering with its BaseContainer for screen
// update.  It knows its color.  It also specifies a pure virtual draw()
// method which must be provided in derived classes if they are to be
// instantiated.
//
// A Drawable can determine if it has been touched, and it provides methods for
// receiving the touch-related notifications touch(), release(), and leave().
// Any object which inherits from Drawable may define touch dependent behavior
// by overriding these functions.  touch() is called when the Drawable is first
// touched, leave() is called when the finger slides out of the Drawable's
// area, and release() is called when the finger is lifted from the touch
// screen while within the Drawable's screen area.
//---------------------------------------------------------------------
//@ Rationale
// This object is designed to encapsulate the capabilities and knowledge
// needed by every object which can be drawn on the screen.  A simple
// Area is not enough by itself, because an Area does not have a parent.
// This class contains an area, but also knows enough about its parent's
// coordinate system and the screen management framework to figure out
// where it truly resides on the screen.  It also encapsulates all the
// functionality required to be touchable.
//---------------------------------------------------------------------
//@ Implementation-Description
// The Drawable class is abstract -- it is never instantiated.
//
// The Drawable class maintains several data members used by the screen
// management framework, including area_, isVisible_, isShown_,
// isSensitive_, color_, pParentContainer_, and pBaseContainer_.
//
// The area_ member records where a Drawable lies within its parent
// Container.  This includes a width and height, plus X and Y position
// relative to the upper left corner of the parent Container.  All of
// the functions which deal in absolute screen coordinates calculate
// them each time they are required, using recursive calls which travel
// up the containment tree.
//
// The isVisible_ attribute records whether or not the Drawable is
// displayed on the screen.  This is not publicly settable.  It is
// updated by the public evaluateVisible() method and set by the
// protected setVisible_() method.  Do not confuse this with the
// isShown_ attribute described below.  Application calls are made to
// the isVisible() method to check visibility.
//
// The isShown_ attribute is controlled by the application via the
// public methods setShow() and getShow().  This attribute specifies
// whether or not the object is logically supposed to be visible, not
// whether it is ACTUALLY visible (see isVisible_).
//
// The isSensitive_ attribute specifies if the Drawable is touch
// sensitive.  The findTouchedDrawable() method will not report a hit
// unless the Drawable's isSensitive_ attribute is set.  Note that the
// derived class Container redefines the findTouchedDrawable() method to
// behave differently.  If the Container is not touch sensitive, it calls
// the findTouchedDrawable() methods of its contained Drawables, thus
// implementing a recursive search through the containment tree.  This
// attribute is set to FALSE at construction time.  Derived classes
// (such as Button) may set this flag differently, because it is a
// protected member.  No public methods are provided to manipulate or
// test this variable.
//
// The color_ attribute specifies the color to use when rendering the
// Drawable.  The setColor() method is virtual and provides a default
// implementation, but derived classes which do not have an intrinsic
// color (e.g.  Container) or have special requirements (e.g.  TextField)
// should redefine setColor().
//
// The pParentContainer_ attribute points to the Container that
// contains the Drawable, if any.  The public appendTo() method is used
// to set this attribute; the getParentContainer() method gives access
// to the pointer.
//
// The pBaseContainer_ attribute points to the BaseContainer that
// contains the Drawable, if any.  The public evaluateBase() method
// determines which BaseContainer the Drawable is in; the
// getBaseContainer() method gives access to the pointer.
//
// In order to manage the redrawing of the screen, all non-abstract classes
// which inherit from Drawable must have a draw() function.  The draw()
// function's responsibility is to render the Drawable on the screen.  The
// draw() method should only be called via the "framework"; it relies on the
// framework to ensure that the object is visible and that its isShown_ member
// is true. Calls via the framework originate in the draw() method of the
// object's parent Container.  An exception is BaseContainer, whose draw() is
// called from the repaint() method.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Drawable.ccv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube I.D. to 
//		new patient value when spontaneous type changed to PAV.
//		Changed getColor() to const method.
//
//  Revision: 008   By: gdc    Date:  25-Jan-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Removed SUN prototype code. Added drawOverlay and removeOverlay to
//  support trending cursor line drawn over direct draw plot. Added
//  vertical positioning gravity for positionInContainer.
//
//  Revision: 007   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 006   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Added "GRAVITY_RIGHT" and "GRAVITY_LEFT" support in 'positionInContainer()'
//      as part of the adding of the "above PEEP" phrase at the right edge
//      of some setting buttons.
//
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project:  NeoMode
//  Description:
//      Optimized VGA library for Neonatal project to use BitBlt and Windows
//      Bitmap structures.
//
//  Revision: 004   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 003  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 002  By: yyy      Date: 15-May-1995  DCR Number: 1957
//    Project:  Sigma (R8027)
//    Description:
//      Bug in appendTo():when a drawable is set show to FALSE first, then appended
//      to a container, then set show to TRUE, this drawable will not be displayed
//      correctly.  It is due to the addToChangedList() will only be called when
//      the drawable is visible.  However, since this drawable's isChanged_ flag
//      is set to FALSE, this will force the isChanged_ set to TRUE.  The conflict
//      between this causes the drawable not be repainted correctly.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Drawable.hh"

//@ Usage-Classes
#include "Container.hh"

#include "BaseContainer.hh"

//@ End-Usage

// Initialize static data members
const Int16 Drawable::MAX_TREE_DEPTH = 20;

//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Drawable
//
//@ Interface-Description
// Default constructor which initializes a Drawable object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all data members.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Drawable::Drawable(void) :
	isSensitive_(FALSE),
	isOpaque_(FALSE),
	color_(Colors::BLACK),
	pBaseContainer_(NULL),
	area_(0,0,0,0),
	isHighlighted_(FALSE),
	highlightColor_(Colors::DARK_RED),
	pParentContainer_(NULL),
	pBaseWhereLastDrawn_(NULL),
	isChanged_(FALSE),
	isVisible_(FALSE),
	isShown_(TRUE),
	lastDrawnArea_(0,0,0,0),
	pStoredImage_(NULL)
{
	CALL_TRACE("Drawable::Drawable(void)");

	//												$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Drawable 
//
//@ Interface-Description
// Shall never be called.  Drawable instance shall never be destroyed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	A drawable shall never be destroyed for production software.  If the
//	destruction of the drawable indeed happened, then CLASS_ASSERTION shall
//	be executed.
//---------------------------------------------------------------------
//@ PreCondition
//  Always causes CLASS_PRE_CONDITION.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Drawable::~Drawable(void)
{
	CALL_TRACE("Drawable::~Drawable()");

#if defined SIGMA_UNIT_TEST
	//	This code will be executed only when the drawable has been changed.
	//	In case the drawable is destroyed, the changedDrawable_ list and the
	//	areaList in the	BaseContainer need be updated by using the repaint()
	//	method.  To do this a two step processes is required.
	//	First to remember the current drawable's baseContainer address.
	//
	BaseContainer *pOldBaseContainer=NULL;
	
	pOldBaseContainer = pBaseContainer_;

	removeFromParent();
	
	//
	//	Second to repaint the screen.
	//
	//	After the drawable is removed from it parent, the screen needs to
	//	be updated.  The repaint() call shall remove the referenced drawable
	//	from the changedDrawables_ list.  The areaList shall be updated to
	//	reflect the removal of the drawabale's area.
	//
	if (pOldBaseContainer != NULL)
	{
		pOldBaseContainer->repaint();
	}	
#endif	// SIGMA_UNIT_TEST

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setX [virtual]
//
//@ Interface-Description
// Sets the "x" position of the top left corner of the Drawable.  This
// value is specified relative to the origin of the parent Container.
//---------------------------------------------------------------------
//@ Implementation-Description
// First, the object is marked as needing redraw by a call to the changing()
// method.  The position change is then implemented by a call to the setX()
// method of the area_ member.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Drawable::setX(Int x)
{
	CALL_TRACE("Drawable::setX(Int)");

	changing();	//										$[TI1]
	area_.setX(x);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setY [virtual]
//
//@ Interface-Description
// Sets the "y" position of the top left corner of the Drawable.  This
// value is specified relative to the origin of the parent Container.
//---------------------------------------------------------------------
//@ Implementation-Description
// First, the object is marked as needing redraw by a call to the changing()
// method.  The position change is then implemented by a call to the setY()
// method of the area_ member.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Drawable::setY(Int y)
{
	CALL_TRACE("Drawable::setY(Int)");

	changing();	//										$[TI1]
	area_.setY(y);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getX
//
//@ Interface-Description
// Returns the X position of the top left corner of this Drawable, relative to
// the origin of the parent Container.
//---------------------------------------------------------------------
//@ Implementation-Description
// The position is determined by a call to the getX1() method of the
// area_ member.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int
Drawable::getX(void) const
{
	CALL_TRACE("Drawable::getX(void) const");
	return area_.getX1();	//							$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getY
//
//@ Interface-Description
// Returns the Y position of the top left corner of this Drawable, relative to
// the origin of the parent Container.
//---------------------------------------------------------------------
//@ Implementation-Description
// The position is determined by a call to the getY1() method of the
// area_ member.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int
Drawable::getY(void) const
{
	CALL_TRACE("Drawable::getY(void) const");
	return area_.getY1(); 	//							$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRealX
//
//@ Interface-Description
// Returns the X location of the top left corner of this Drawable in
// absolute (screen) coordinates.
//
// This method uses an internal static variable to monitor the depth of
// recursion; for this reason, it should not be called reentrantly.
//---------------------------------------------------------------------
//@ Implementation-Description
// The absolute screen location is determined by a recursive series of calls
// travelling up and back down the containment tree.  Recursively adds the X
// coordinate of this object to the real X coordinate of its parent.  The
// recursion bottoms out when it finds a Drawable with no base Container.
//---------------------------------------------------------------------
//@ PreCondition
//  This drawable must be in a BaseContainer (pBaseContainer_ can't be NULL).
// The recursion depth must be <= MAX_TREE_DEPTH.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int
Drawable::getRealX(void) const
{
	CALL_TRACE("Drawable::getRealX(void) const");

	CLASS_PRE_CONDITION(pBaseContainer_ != NULL);

	Int xVal;

	if (NULL == pParentContainer_)
	{	//												$[TI1]
		xVal = getX();
	}
	else
	{	//												$[TI2]
		xVal = getX() + pParentContainer_->getRealX();
	}

	return(xVal);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRealY
//
//@ Interface-Description
// Returns the Y location of the top left corner of this Drawable in
// absolute (screen) coordinates.
//
// This method uses an internal static variable to monitor the depth of
// recursion; for this reason, it should not be called reentrantly.
//---------------------------------------------------------------------
//@ Implementation-Description
// The absolute screen location is determined by a recursive series of calls
// travelling up and back down the containment tree.  Recursively adds the Y
// coordinate of this object to the real Y coordinate of its parent.  The
// recursion bottoms out when it finds a Drawable with no base Container.
//---------------------------------------------------------------------
//@ PreCondition
// This drawable must be in a BaseContainer (pBaseContainer_ can't be NULL).
// The recursion depth must be <= MAX_TREE_DEPTH.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int
Drawable::getRealY(void) const
{
	CALL_TRACE("Drawable::getRealY(void) const");

	CLASS_PRE_CONDITION(pBaseContainer_ != NULL);

	Int yVal;

	if (NULL == pParentContainer_)
	{	//												$[TI1]
		yVal = getY();
	}
	else
	{	//												$[TI2]
		yVal = getY() + pParentContainer_->getRealY();
	}

	return(yVal);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setWidth [virtual]
//
//@ Interface-Description
// Sets the width of the Drawable to the given "width".  The position of
// the top left corner is not changed.  This object is marked as
// needing redraw.
//---------------------------------------------------------------------
//@ Implementation-Description
// First, the object is marked as needing redraw by a call to the changing()
// The size change is then implemented by a call to the setWidth() method
// of the area_ member.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
Drawable::setWidth(Int width)
{
	CALL_TRACE("Drawable::setWidth(Int)");
	changing();

	area_.setWidth(width);	//							$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHeight [virtual]
//
//@ Interface-Description
// Sets the height of the Drawable to the given "height".  The position
// of the top left corner is not changed.  This object is marked as
// needing redraw.
//---------------------------------------------------------------------
//@ Implementation-Description
// First, the object is marked as needing redraw by a call to the changing()
// The size change is then implemented by a call to the setHeight() method
// of the area_ member.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Drawable::setHeight(Int height)
{
	CALL_TRACE("Drawable::setHeight(Int)");

	changing();
	area_.setHeight(height);	//						$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getWidth
//
//@ Interface-Description
// Returns the width of the Drawable.
//---------------------------------------------------------------------
//@ Implementation-Description
// The request is delegated to the area_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int
Drawable::getWidth(void) const
{
	CALL_TRACE("Drawable::getWidth(void) const");
	return area_.getWidth();	//						$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getHeight
//
//@ Interface-Description
// Returns the height of the Drawable.
//---------------------------------------------------------------------
//@ Implementation-Description
// The request is delegated to the area_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int
Drawable::getHeight(void) const
{
	CALL_TRACE("Drawable::getHeight(void) const");
	return area_.getHeight();	//						$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: overlapsRealArea
//
//@ Interface-Description
// Checks to see whether or not this object overlaps with the given
// "realArea", which is in absolute (screen) coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
// Constructs a temporary "real area" for this Drawable and then uses
// Area::overlapsArea() to check if the given "realArea" overlaps.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean  
Drawable::overlapsRealArea(const Area& realArea) const
{
	CALL_TRACE("Drawable::overlapsRealArea(const Area&) const");

	//												$[TI1], [TI2]
	Area myRealArea = area_;
	
	myRealArea.setX(getRealX());
	myRealArea.setY(getRealY());

	return myRealArea.overlapsArea(realArea);
}
	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: findTouchedDrawable [virtual]
//
//@ Interface-Description
// This method is used by the Touch class to determine if the screen location
// of the last touch event falls within or overlaps this Drawable.
// It returns a pointer to this object if: it overlaps with the given
// "touchArea" AND this object is displayed, AND this object is
// touch-sensitive.  Otherwise, it returns NULL.  The "touchArea" must be
// in relative coordinates, based on the origin of the parent Container.
// The numHits parameter is a reference to the total number of Drawables
// hit.
//---------------------------------------------------------------------
//@ Implementation-Description
// This function checks for sensitivity and uses Area::overlapsArea() to
// do the overlap check.  If both tests return TRUE then returns the
// "this" pointer.
//
// Note that the pointer returned by this method has the following type:
// "const Drawable *".  In order for the calling method to use this
// pointer to call a non-const method of this object, such as the touch()
// method, it must first cast away the constness of the pointer.
//
// This function could have returned a Boolean "hit or not hit" value,
// instead of returning "this".  It could be assumed that the caller
// knows which object it called the function on.  However this function
// is designed to be part of a recursive set of calls to any of the
// virtual findTouchedDrawable() functions.  Some of the other versions
// of this function might return a pointer to a Drawable other than itself.
// An example of this is the redefined version of this method for the
// Container class, which searches thru its "children" and returns a
// pointer to the touched child.
//
// NOTE: if another shown Drawable is on top of this one, and is not touch
// sensitive - it will not prevent this one from being touched, even though
// this one could be obscured by the one on top of it.
//--------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
const Drawable *
Drawable::findTouchedDrawable(const Area& touchArea,
								Int& numHits) const
{
	CALL_TRACE("Drawable::findTouchedDrawable(const Area& touchArea, "
				"Int& numHits) const");

	if (isVisible_ && isSensitive_ && area_.overlapsArea(touchArea))
	{	//												$[TI1]
		//
		// This object is on-screen, sensitive, and hit.
			
		numHits++;	// increment the reference to the total number of
					// Drawables hit.
		if (numHits == 1)
		{	//											$[TI1.1]			
			return this;
		}
		//												$[TI1.2]
	}
	//													$[TI2]
	
	return NULL;
}
		

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: touch [virtual]
//
//@ Interface-Description
// This method is called by the Touch object when this object has been touched.
// "Touch" as used here means that the user's finger has made contact; separate
// leave() and release() methods are called when the user's finger slides out
// of the object or is lifted from the object.  The two parameters are the
// X and Y coordinates of the touch. 
//
// This method does nothing.  If a sub-class wishes to implement a specific
// behavior, it must redefine this (virtual) function.
//---------------------------------------------------------------------
//@ Implementation-Description
// Called by the Touch object when a touch-releated event has occurred, and its
// search for the touched Drawable (using the findTouchedDrawable() method)
// determies that this is the one that was touched.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Drawable::touch(Int, Int)
{
	CALL_TRACE("Drawable::touch(Int, Int)");

	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: leave [virtual]
//
//@ Interface-Description
// This method is called by the Touch object when the user's finger slides out
// of this Drawable. The two parameters are the X and Y coordinates of the
// touch.
//
// This method does nothing.  If a sub-class wishes to implement a specific
// behavior, it must redefine this (virtual) function.
//---------------------------------------------------------------------
//@ Implementation-Description
// Called by the Touch object when a transition occurs from a touch
// location within this object to a touch location outside this object.
// This call signifies that this object is losing the "touch focus".
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Drawable::leave(Int, Int)
{
	CALL_TRACE("Drawable::leave(Int, Int)");
	
	// do nothing
}
	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: release [virtual]
//
//@ Interface-Description
// This method is called by the Touch object when the user's finger is lifted
// off of this Drawable. The two parameters are the X and Y coordinates of the
// touch.
//
// Note: the paradigm of touch-screen use that has been adopted by Sigma
// dictates that actions triggered using the touch screen should usually
// occur on release.  Rationale: if the user accidentally touches the wrong
// button, the finger can be "dragged" out of the button without being
// lifted, thus avoiding the triggering of the action associated with the
// button.
//
// This method does nothing.  If a sub-class wishes to implement a specific
// behavior, it must redefine this (virtual) function.
//---------------------------------------------------------------------
//@ Implementation-Description
// Called by the Touch object when a release event has occurred, when
// prior to the release this object was being touched. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Drawable::release(void)
{
	CALL_TRACE("Drawable::release(void)");

	// do nothing
}
	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate [virtual]
//
//@ Interface-Description
// Virtual function which allows derived Drawables to update their contents as
// needed.  This method does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// Empty implementation for base class.  It will be redefined in the Gui-Apps.
// sub-classes.  This method is provided to collaborate with the Gui-Apps.
// Any container in the Gui-Apps,Application, can redefine this method
// to update the display of all the drawables within this container.
//
// There is also a deactive method in the Gui-Apps.  This method is used to
// erase the display of the drawables within the parent container.  However,
// due to the design of the Gui-Apps, deactive shall only be used with the
// sub-screen class.  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Drawable::activate(void)
{
	CALL_TRACE("Drawable::activate(void)");

	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: appendTo [virtual]
//
//@ Interface-Description
// Appends this object to the given "pNewParentContainer".  If it is
// initially in a different parent container, it is removed from that
// parent before being added to the new one.
//---------------------------------------------------------------------
//@ Implementation-Description
// Removes this object from its current container (if any), and adds it to the
// new parent container.  Calls the evaluateVisible() method to update the
// isVisible_ member.
//
// Avoid the formation of the cyclic container list by checking through the
// newParentContainer's pParentContainer_ chain to make sure that the
// newParentContainer is not contained by this drawable already.
//
// If the base Container has changed, then we need to tell the new base
// Container that this Drawable needs to be redrawn.  Most of the time the
// BaseContainer will either be unchanged, or will have changed from NULL to
// either GuiFoundation::GetLowerBaseContainer() or
// GuiFoundation::GetUpperBaseContainer(); rarely, if ever, from one to the other.
//---------------------------------------------------------------------
//@ PreCondition
// The given "pNewParentContainer" must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
Drawable::appendTo(Container* pNewParentContainer)
{
	CALL_TRACE("Drawable::appendTo(Container *)");
	CLASS_PRE_CONDITION((pNewParentContainer != NULL) &&
						(this != pNewParentContainer));

	Container *pParent;
	BaseContainer *pBase;
	//
	// If new parent Container is the same as the current 
	// parent Container, then we're done.
	//
	if (pParentContainer_ == pNewParentContainer)
	{	//											$[TI1]
		return;
	}
	//												$[TI2]

	//
	// Avoid the formation of a cyclic container list.
	// Checke through the newParentContainer's pParentContainer_ chain to
	// make sure that the newParentContainer is not contained by this drawable
	// already.
	//
	pParent = pNewParentContainer;
	pBase = ((Drawable *) pNewParentContainer)->pBaseContainer_;
	
	while (pParent != NULL && pParent != pBase)
	{	//											$[TI5]
		CLASS_ASSERTION(pParent != this)

		pParent = ((Drawable *) pParent)->pParentContainer_;
	}
	//												$[TI6]
	//
	// Remove this object from its existing parent Container, if any.  If there
	// is a parent, this will result in a call to changing(), marking this
	// object for redraw.
	//
	removeFromParent();

	//
	// Assign a new parent Container and append this object to it.
	//
	pParentContainer_ = pNewParentContainer;
	pNewParentContainer->setChild_(this);
	evaluateVisible();

	BaseContainer *pOldBaseContainer = pBaseContainer_;
	evaluateBase();

	// If this Drawable was not previously in a BaseContainer, or if it
	// was previously in a different BaseContainer:
	//
	if (pBaseContainer_ != NULL && (pOldBaseContainer != pBaseContainer_))
	{	//											$[TI3]
		//
		// Add this object to the changed Drawables list of
		// the base Container.
		//
		// NOTE: If the object was visible in the old BaseContainer, and is now
		// visible in the new one, it will now be on the changed drawable lists
		// of both old and new BaseContainers.  In repaint() of OLD
		// BaseContainer: if object is visible in NEW BaseContainer, will add
		// bogus realArea of object in NEW BaseContainer to areaList because
		// calls to getRealX() reference new parent...  This will cause some
		// additional drawing on the old BaseContainer, but will not cause any
		// problems.  The performance hit of code to prevent this is not
		// justified, considering that moving objects between BaseContainers
		// should be rare to nonexistant.

		if (isVisible())
		{	//											$[TI3.1]
			pBaseContainer_->addToChangedList(this);
			
			// Setting isChanged_ to true prevents this object from being added
			// again to the BaseContainer's list of changed Drawables
			// (changedDrawables_).  While the changing() method (called above for
			// the old BaseContainer) also can set the isChanged_ flag for this
			// object, it did so only if the object was visible in the old
			// BaseContainer, so isChanged might not have been set.

			if (! isChanged_)
			{	//										$[TI3.1.1]
				// Drawable was not previously visible
				isChanged_ = TRUE;

				// This object was not previously visible in the new or old
				// BaseContainer.  Setting pBaseWhereLastDrawn_ = NULL here
				// prevents its lastDrawnArea_ from being added to the list of
				// areas to redraw at the next repaint of either BaseContainer.
				// Refer to the BaseContainer::repaint() method.
				pBaseWhereLastDrawn_ = NULL;
			}
			//											$[TI3.1.2]
		}
		//												$[TI3.2]
	}
	//													$[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: removeFromParent [virtual]
//
//@ Interface-Description
// Removes this Drawable from its parent Container, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this Drawable has a parent, then call the parent Container's
// removeChild_() function and invalidate the pointer to the parent
// Container.  Update the visibility status (should become not visible)
// and update the pointer to base container (should go to NULL).
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Drawable::removeFromParent(void)
{
	CALL_TRACE("Drawable::removeFromParent(void)");

	if (pParentContainer_ != NULL)
	{	//												$[TI1]
		changing();
		pParentContainer_->removeChild_(this);
		pParentContainer_ = NULL;

		// You might think it would be sufficient to simply set isVisible_ to
		// false here, since that is all that is done by the evaluateVisible()
		// method.  However, if this method is being called from derived class
		// Container, then Container's redefined version of evaluateVisible()
		// will be called; it also updates the visibility status of all of its
		// children.
		//
		evaluateVisible();
		SAFE_CLASS_ASSERTION(isVisible_ == FALSE);

		// You might think it would be sufficient to simply set pBaseContainer_
		// to NULL here, since that is all that is done by the
		// evaluateBase() method.  However, if this method is being called
		// from derived class Container, then Container's redefined version of
		// evaluateBase() will be called; it also updates the pBaseContainer_
		// members of all of its children. 
		//
		evaluateBase();
		SAFE_CLASS_ASSERTION(pBaseContainer_ == NULL);
	}
	//													$[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateBase [virtual]
//
//@ Interface-Description
// Determines the base Container for this object. 
//---------------------------------------------------------------------
//@ Implementation-Description
// If a parent container exists, simply sets the pBaseContainer of this
// object to the pBaseContainer of its parent (which may be NULL). If
// there is no parent container, sets the pBaseContainer to NULL.
//---------------------------------------------------------------------
//@ PreCondition
//  This drawable must not be a BaseContainer (pBaseContainer_).
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Drawable::evaluateBase(void)
{
	CALL_TRACE("Drawable::evaluateBase(void)");
	CLASS_PRE_CONDITION(this != pBaseContainer_);

	if (pParentContainer_ != NULL)
	{	//												$[TI1]
		pBaseContainer_ = pParentContainer_->getBaseContainer();
	}
	else
	{	//												$[TI2]
		pBaseContainer_ = NULL;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setColor [virtual]
//
//@ Interface-Description
// Sets the intrinsic color of the object to the given "color".
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the color_ member and marks this object as needing redraw.
// This virtual function should be redefined by sub-classes which have
// special requirements or which don't have an intrinsic color.
//---------------------------------------------------------------------
//@ PreCondition   
//  color >= 0 && color < Colors::NUM_COLORS
//  (NO_COLOR is invalid)
//---------------------------------------------------------------------
//@ PostCondition  
//  none
//@ End-Method
//=====================================================================

// Sun-specific comments: to simulate flashing (implemeted by periodically
// remapping colors for the target), this method maintains a static collection
// of flashing Drawables, and toggles their visibility on an X-Windows timer
// callback.

void
Drawable::setColor(Colors::ColorS color)
{
	CALL_TRACE("Drawable::setColor(Colors::ColorS)");

	CLASS_PRE_CONDITION( color >= 0 && color < Colors::NUM_COLORS );

	changing();		//									$[TI1]
	color_ = color;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setShow
//
//@ Interface-Description
// Sets the "shown" status of the Drawable.  If the passed "isShown" parameter
// is false, the Drawable will be hidden.  If it is true, it will be displayed
// if possible.  If a "shown" Drawable is contained in a hidden Container,
// it will not be drawn.  Whether or not a Drawable will be drawn can be
// determined by the application by calling its public isVisible() method.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets the internal isShown_ value to the given "isShown" value.  Calls
// evaluateVisible() to update the isVisible_ state.  That is, just because an
// object is logically shown, does not imply that the object is actually drawn
// since it's parent Container could be hidden.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Drawable::setShow(Boolean isShown)
{
	CALL_TRACE("Drawable::setShow(Boolean)");

	if (isShown != isShown_)	// shown status is changing
	{	//												$[TI1]
		
		// Save the drawable's current isVisible_ state before the passed in
		// isShown takes effect.
		Boolean oldIsVisible = isVisible_;

		if (oldIsVisible)
		{	//											$[TI1.1]
			// If the drawable was previously visible, it's isShown_ state is
			// being set to False.  The transition of isShown_ state from TRUE
			// to FALSE means changing() should be executed. This will cause
			// its lastDrawnArea to be saved, thus causing it to be erased at
			// the next repaint.
			//
			// Note that this call must be done before the call to
			// evaluateVisible() below, because that call will overwrite
			// the old isVisible_.  After that call, the changing() method
			// will not work correctly, because it depends upon the 
			// isVisible_ flag.

			changing();
		}
		//												$[TI1.2]
		
		//
		// Update internal show state and evaluate whether we are actually
		// visible or not.
		//
		isShown_ = isShown;
		evaluateVisible();

		// If drawable was not previously visible:
		if (!oldIsVisible)
		{	//											$[TI1.3]
			// if the drawable is now visible
			if (isVisible_)
			{	//										$[TI1.3.1]
				// Drawable has transitioned from not visible to visible.
				// Note that changing() will log the lastDrawnArea_,
				// but it will really get the new area.  However, adding this
				// area to the area list in repaint() does no harm, because it
				// is the same as the new area.  No additional screen area will
				// be redrawn as a result.

				changing();
			}
			//											$[TI1.3.2]		
		}
		//												$[TI1.4]
	}
	//													$[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateVisible [virtual]
//
//@ Interface-Description
// Determines the visibility status of this object.  This is not the same as
// the "shown" status; in order to be visible, a Drawable must be shown, AND
// its parent Container must be visible.  In short, the user can set the
// "shown" status, but cannot set the "visible" status; it will be set true by
// this method only if all levels of the containment tree above the Drawable
// are shown and visible.
//---------------------------------------------------------------------
//@ Implementation-Description
// The visibility is determined by the existence and visibility of the
// parent Container as well as the isShown_ status of the drawable.  This
// is based on the assumption that the parent's isVisible_ member is
// up-to-date.  There is no series of recursive calls here.
//
// Note that this method is redefined in the Container class to use recursive
// calls to update the visibility status of its childern, but not its parents;
// in general, evaluateVisible() assumes that all levels of the containment
// tree above it have valid isVisible_ settings, but it updates all levels
// below it.
//
// This method is redefined in the BaseContainer class (which has no parent)
// to always return TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Drawable::evaluateVisible(void)
{
	CALL_TRACE("Drawable::evaluateVisible(void)");

	//												$[TI1], $[TI2]
	isVisible_ = isShown_ && (pParentContainer_ != NULL) &&
							 pParentContainer_->isVisible();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: changing
//
//@ Interface-Description
// This function is called when the appearance of this object is about to be
// changed, or the object is about to be hidden, moved, added to a Container,
// or removed from a Container.  A pointer to this object is added to a list of
// changed Drawables maintained by the current BaseContainer; this determines
// which parts of the screen will need to be regenerated at the next screen
// repaint.
//
// This method also saves the current (pre-change) real (absolute) area of the
// object so that the redraw engine can properly erase its old image at the
// next update of the screen.
//
// If the object is not visible, or if it has already been logged to the
// changed list, this method does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this object is visible and has not already been marked as being changed,
// then add its pointer to the changed list.  Then convert the current
// (relative) area into real (absolute) coordinates and save the data into
// lastDrawnArea_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Drawable::changing(void)
{
	CALL_TRACE("Drawable::changing(void)");

	//
	// Check the isChanged_ flag to make sure we only save the current real
	// area once per repaint pass.
	//
	if ( !isChanged_ && isVisible_ && !pBaseContainer_->isDirectDrawEnabled() )
	{	//												$[TI1]
		isChanged_ = TRUE;
		
		CLASS_ASSERTION(pBaseContainer_ != NULL);

		//
		// Store the current area to real (absolute) coordinates
		// in lastDrawnArea_.
		//
		lastDrawnArea_.setX(getRealX());
		lastDrawnArea_.setY(getRealY());
		lastDrawnArea_.setWidth(getWidth());
		lastDrawnArea_.setHeight(getHeight());

		pBaseWhereLastDrawn_ = pBaseContainer_; 

		//
		// Add this changed object to the list of changed Drawables
		// maintained by the base Container, unless this object is
		// the base Container itself.
		//
		pBaseContainer_->addToChangedList(this);
	}
	//													$[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw [pure virtual]
//
//@ Interface-Description
// Renders this Drawable on the screen.  It does not need to erase its old
// image (saved in the private "last drawn area" member) because that is
// handled by the screen update "framework"; the BaseContainer::repaint()
// method tracks the areas of all old images, and Container::draw() takes care
// of erasing them if needed.
//
// Note that the framework has the responsibility of making sure that an
// object is visible and shown before it calls the draw() method; these
// items are not checked within a draw() method.
//
// This is a pure virtual method that must be defined in a derived class if
// that class is to be instantiated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  No implementation - the method is pure virtual.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

//@ not implemented

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawDirect
//
//@ Interface-Description
// Renders this Drawable directly on the screen. This method sets up the
// context for drawing this drawable by setting the clip area to the
// area of the base container (ie. no clipping). This function can only 
// be called when the Direct Draw mode is enabled which locks the 
// BaseContainer's display and graphics contexts. After establishing the 
// context, the draw() method is called to actually render the object to 
// the display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Drawable::drawDirect(void)
{
	CLASS_PRE_CONDITION( pBaseContainer_ && pParentContainer_ 
						&& pBaseContainer_->isDirectDrawEnabled() );

	if (isVisible_)
	{
		GraphicsContext & rGc = pBaseContainer_->getGraphicsContext();
		DisplayContext & rDc = pBaseContainer_->getDisplayContext();

		Int  x1 = pParentContainer_->getRealX();
		Int  y1 = pParentContainer_->getRealY();
		Int  x2 = x1 + pParentContainer_->getWidth() - 1;
		Int  y2 = y1 + pParentContainer_->getHeight() - 1;

		rDc.setClip( rGc, x1, y1, x2, y2 );

		draw( Area( x1, y1, x2, y2) );
		//								$[TI1]
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawOverlay
//
//@ Interface-Description
// Renders this Drawable in the emulated overlay plane. The drawable
// is rendered directly in the screen buffer with drawDirect, but 
// contents of the frame buffer underneath the area of this drawable 
// are first stored in off-screen memory so they can be restored when 
// this drawable is moved or removed. This method allows for a pseudo
// non-destructive write operation into the frame buffer. However, if
// the underlying data is changed before the overlay graphic has a 
// chance to recover the stored image from off-screen memory, the 
// recovered image will be the "old" image stored when the overlay
// graphic was first drawn. This method is required and used by the 
// application for drawing and moving a cursor over graphical data that
// is drawn directly into the frame buffer for which there are no
// saved drawables in the base container's drawable list that could be
// used to refresh the data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  ( pBaseContainer_ && pBaseContainer_->isDirectDrawEnabled() )
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Drawable::drawOverlay(void)
{
  CLASS_PRE_CONDITION( pBaseContainer_ && 
	                   pBaseContainer_->isDirectDrawEnabled() );

  GraphicsContext &rGc = pBaseContainer_->getGraphicsContext();
  DisplayContext &rDc = pBaseContainer_->getDisplayContext();

  Int  width = getWidth();
  Int  height = getHeight();
  Int  x1 = getRealX();
  Int  y1 = getRealY();

  pStoredImage_ = rDc.allocateImage(width * height);
  AUX_CLASS_ASSERTION(pStoredImage_ != NULL, width * height);

  // save the image currently on the screen
  rDc.storeImage( pStoredImage_, x1, y1, width, height );


  // now draw the overlay object on the screen
  Int  baseX1 = pBaseContainer_->getX();
  Int  baseY1 = pBaseContainer_->getY();
  Int  baseX2 = baseX1 + pBaseContainer_->getWidth() - 1;
  Int  baseY2 = baseY1 + pBaseContainer_->getHeight() - 1;

  rDc.setClip( rGc, baseX1, baseY1, baseX2, baseY2 );

  draw( Area( baseX1, baseY1, baseX2, baseY2) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: removeOverlay
//
//@ Interface-Description
// Removes the overlay graphic (drawable) by recovering the underlay 
// image that was saved when this graphic was drawn (see drawOverlay).
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
// ( pBaseContainer_ && pParentContainer_ 
//   && pBaseContainer_->isDirectDrawEnabled() )
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Drawable::removeOverlay(void)
{
  CLASS_PRE_CONDITION( pBaseContainer_ && pParentContainer_ 
	                  && pBaseContainer_->isDirectDrawEnabled() );

  GraphicsContext &rGc = pBaseContainer_->getGraphicsContext();
  DisplayContext &rDc = pBaseContainer_->getDisplayContext();

  Int  width = getWidth();
  Int  height = getHeight();
  Int  x1 = getRealX();
  Int  y1 = getRealY();

  if (pStoredImage_ != NULL)
  {
	  rDc.recoverImage(x1, y1, pStoredImage_, width, height);
	  rDc.freeImage(pStoredImage_);
  }

  pStoredImage_= NULL;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
// If called, this method generates a run-time error.  Do not use.
//---------------------------------------------------------------------
//@ Implementation-Description
// In order for the Sigma collection classes to work when the collection
// consists of references to objects of this class, the operator== method must
// be defined.  If called, it will generate a run-time error.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
Drawable::operator==(const Drawable&) const
{
	CALL_TRACE("Drawable::operator==(const Drawable&) const");

	CLASS_ASSERTION(FALSE);
	return FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
// If called, this method generates a run-time error.  Do not use.
//---------------------------------------------------------------------
//@ Implementation-Description
// In order for the Sigma collection classes to work when the collection
// consists of references to objects of this class, the operator!= method must
// be defined.  If called, it will generate a run-time error.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
Drawable::operator!=(const Drawable&) const
{
	CALL_TRACE("Drawable::operator!=(const Drawable&) const");

	CLASS_ASSERTION(FALSE);
	return FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHighlighted [virtual]
//
//@ Interface-Description
// Sets the "highlight" attribute of the object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Updates the private highlighted_ member.
// This virtual function may be redefined by sub-classes which have
// special requirements.
//---------------------------------------------------------------------
//@ PreCondition   
//  none
//---------------------------------------------------------------------
//@ PostCondition  
//  none
//@ End-Method
//=====================================================================

void
Drawable::setHighlighted(Boolean var)
{
	CALL_TRACE("setHighlighted(void");
	// $[TI1]
	changing();
	isHighlighted_ = var;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setHighlightColor [virtual]
//
//@ Interface-Description
//  Sets the "highlight" color of the object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Updates the private highlightColor_ member.
//---------------------------------------------------------------------
//@ PreCondition   
//  none
//---------------------------------------------------------------------
//@ PostCondition  
//  none
//@ End-Method
//=====================================================================

void
Drawable::setHighlightColor(Colors::ColorS color)
{
	CALL_TRACE("setHighlightColor(Colors::ColorS)");
	// $[TI1]
	changing();
	highlightColor_ = color;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getColor [virtual]
//
//@ Interface-Description
//  Returns the highlight color if the field is highlighted,
//  otherwise returns the private color_.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the private highlightColor_ when isHighlighted_ is TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Colors::ColorS 
Drawable::getColor(void) const
{
	CALL_TRACE("getColor(void");

	if ( isHighlighted_ )
	{													// $[TI1.1]
		return highlightColor_;
	}
	else
	{													// $[TI1.2]
		return color_;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: positionInContainer [virtual]
//
//@ Interface-Description
//  Positions the drawable in its parent container as specified by the
//  gravity parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Repositions the object in its parent container using setX and setY.
//---------------------------------------------------------------------
//@ PreCondition
//  pParentContainer_ not NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Drawable::positionInContainer(const Gravity gravity)
{
	CALL_TRACE("positionInContainer()");

	CLASS_PRE_CONDITION( pParentContainer_ );

	switch (gravity)
	{
	case ::GRAVITY_LEFT :
		// $[TI1]
		setX(2);
		break;
	case ::GRAVITY_CENTER :
		// $[TI2]
		setX((pParentContainer_->getWidth() - getWidth()) / 2);
		break;
	case ::GRAVITY_RIGHT :
		// $[TI3]
		// the "- 2" bias is to ensure a right spacing of at least 2 pixels,
		// for aesthetic purposes...
		setX(pParentContainer_->getWidth() - getWidth() - 2);
		break;
	case ::GRAVITY_TOP :
		setY(2);
		break;
	case ::GRAVITY_VERTICAL_CENTER :
		setY((pParentContainer_->getHeight() - getHeight()) / 2);
		break;
	case ::GRAVITY_BOTTOM :
		setY(pParentContainer_->getHeight() - getHeight());
		break;
	default :
		// unexpected 'gravity' value...
		AUX_CLASS_ASSERTION_FAILURE( gravity );
		break;
	};
}


#if defined SIGMA_UNIT_TEST

//=====================================================================
//
// Public methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getIsSensitive_
//
//@ Interface-Description
// This function returns the value of the isSensitive_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The address of isSensitive_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
Drawable::getIsSensitive_(void)
{
	CALL_TRACE("Drawable::getIsSensitive_(void");

	return(isSensitive_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setIsSensitive_
//
//@ Interface-Description
// This function returns the value of the isSensitive_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The address of isSensitive_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
Drawable::setIsSensitive_(Boolean isSensitive)
{
	CALL_TRACE("Drawable::setIsSensitive_(Boolean isSensitive");

	isSensitive_ = isSensitive;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getIsOpaque_
//
//@ Interface-Description
// This function returns the value of the isOpaque_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The value of isOpaque_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
Drawable::getIsOpaque_(void)
{
	CALL_TRACE("Drawable::getIsOpaque_(void");

	return(isOpaque_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPBaseContainer_
//
//@ Interface-Description
// This function returns the address of the pBaseContainer_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The address of pBaseContainer_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

BaseContainer* 
Drawable::getPBaseContainer_(void)
{
	CALL_TRACE("Drawable::getPBaseContainer_(void");

	return(pBaseContainer_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getArea_
//
//@ Interface-Description
// This function returns the address of the area_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The address of area_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Area* 
Drawable::getArea_(void)
{
	CALL_TRACE("Drawable::getArea_(void");

	return(&area_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPParentContainer_
//
//@ Interface-Description
// This function returns the address of the pParentContainer_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The address of pParentContainer_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Container* 
Drawable::getPParentContainer_(void)
{
	CALL_TRACE("Drawable::getPParentContainer_(void");

	return(pParentContainer_);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getIsChanged_
//
//@ Interface-Description
// This function returns the value of the isChanged_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The value of isChanged_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
Drawable::getIsChanged_(void)
{
	CALL_TRACE("Drawable::getIsChanged_(void");

	return(isChanged_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getIsVisible_
//
//@ Interface-Description
// This function returns the value of the isVisible_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The value of isVisible_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
Drawable::getIsVisible_(void)
{
	CALL_TRACE("Drawable::getIsVisible_(void");

	return(isVisible_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getIsShown_
//
//@ Interface-Description
// This function returns the value of the isShown_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The value of isShown_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
Drawable::getIsShown_(void)
{
	CALL_TRACE("Drawable::getIsShown_(void");

	return(isShown_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLastDrawnArea_
//
//@ Interface-Description
// This function returns the address of the lastDrawnArea_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The address of LastDrawnArea_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Area* 
Drawable::getLastDrawnArea_(void)
{
	CALL_TRACE("Drawable::getLastDrawnArea_(void");

	return(&lastDrawnArea_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPBaseWhereLastDrawn_
//
//@ Interface-Description
// This function returns the address of the pBaseWhereLastDrawn_.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// The address of pBaseWhereLastDrawn_ is returned.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

BaseContainer* 
Drawable::getPBaseWhereLastDrawn_(void)
{
	CALL_TRACE("Drawable::getPBaseWhereLastDrawn_(void");

	return(pBaseWhereLastDrawn_);
}

#endif	// SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SoftFault  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void 
Drawable::SoftFault(const SoftFaultID  softFaultID,
					const Uint32       lineNumber,
					const char*        pFileName,
					const char*        pPredicate)  
{
	CALL_TRACE("Drawable::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, DRAWABLE,
							lineNumber, pFileName, pPredicate);
}
