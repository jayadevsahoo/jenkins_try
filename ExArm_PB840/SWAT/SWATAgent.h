////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		SWATAgent.h

Purpose:
	This file defines class SWATAgent, SWAT agent class is used for implement
	the following interfaces, ISWATServer, IAutomationRegistrar.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 23-JUL-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/
#pragma once

#include "SWATInterface.h"

class ScreenCaptureMgr;

class SWATAgent 
	: public ISWATService				// Provide SWAT service
{
private:
    ScreenCaptureMgr* screenCaptureMgr_;
	
	static SWATAgent* instance_;
	// private constructor
	SWATAgent();

public:

	// static expose ISWATService  
	static ISWATService* GetService();
    static void          Initialize();


	/////////////////////////////// Implement ISWATService Interface ///////////////////////////////// 
	// Simulate a touch on location at [x,y]
	virtual SWATStatus SimulateTouch(int x, int y);

	// Simulate a push/release on a bezel key
	virtual SWATStatus SimulateBezelKey(int keyId, bool onOff);

	// simulate knob rotates
	virtual SWATStatus SimulateKnob(int clicks);

	// Capture screen and save to file 'fileName'
	virtual SWATStatus CaptureScreen(const char* fileName);

	// Return screen capture queue size.
	virtual SWATStatus QueryScreenshotQueueSize(int& queueSize);	

	// Remove all screen capture files.
	virtual SWATStatus ClearScreenshots();

	virtual SWATStatus QueryScreenshotPath(int index, char* scPath);

};
