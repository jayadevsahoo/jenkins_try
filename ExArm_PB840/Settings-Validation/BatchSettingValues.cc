#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BatchSettingValues - Manager of the Batch Setting Values.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class manages the values of Batch Settings.  Instances of this
//  class can be used to store all of the batch setting values.  This
//  class provides methods to query and set any of the batch setting
//  values.  It also has two copy methods to do copies between instances
//  of this class:  'operator=()' which copies ALL values and does NOT
//  generate any callbacks; and 'copyFrom()' which copies only those
//  values that are different between the two instances and generates
//  a callback for each value copied.
//
//  This class inherits from 'SettingValueMgr' which provides the callback
//  functionality.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a general-purpose class for managing, as a set,
//  batch setting values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Internally, instances of this class contain arrays of type 'BatchValue'.
//  'BatchValue' is a union of the three different types of batch values:
//  bounded, discrete and sequential values.  This array is big enough to
//  store all of the batch setting values, where the setting IDs are used
//  as indices into this array.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchSettingValues.ccv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: sah    Date: 19-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added "AUX" assertions for better debugging
//
//  Revision: 003   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added "AUX" assertions for better debugging
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "BatchSettingValues.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BatchSettingValues(batchValues)  [Copy Constructor]
//
//@ Interface-Description
//  Construct a batch setting value's instance, using 'batchValues'
//  to initialize this new instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchSettingValues::BatchSettingValues(const BatchSettingValues& batchValues)
				       : SettingValueMgr(NULL)
{
  CALL_TRACE("BatchSettingValues(batchValues)");

  // copy all of the values from 'batchValues' into this instance...
  operator=(batchValues);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BatchSettingValues(pInitCallBack)  [Constructor]
//
//@ Interface-Description
//  Construct a batch setting value's instance, using 'pInitCallback'
//  as the pointer to the value-change callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchSettingValues::BatchSettingValues(
			SettingValueMgr::ChangeCallbackPtr pInitCallback
				      )
			: SettingValueMgr(pInitCallback)
{
  CALL_TRACE("BatchSettingValues(pInitCallback)");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BatchSettingValues()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default batch setting value's instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchSettingValues::BatchSettingValues(void)
				       : SettingValueMgr(NULL)
{
  CALL_TRACE("BatchSettingValues()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BatchSettingValues(void)  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this batch setting values instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchSettingValues::~BatchSettingValues(void)
{
  CALL_TRACE("~BatchSettingValues()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSettingValue(settingId)  [const]
//
//@ Interface-Description
//  Return the setting value that is given by 'settingId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchId(SettingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
BatchSettingValues::getSettingValue(
				const SettingId::SettingIdType settingId
				   ) const
{
  CALL_TRACE("getSettingValue(settingId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBatchId(settingId)), settingId);

  return(arrBatchValues_[settingId]);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getBoundedValue(boundedId)  [const]
//
//@ Interface-Description
//  Return the bounded setting value and precision, that is given by
//  'boundedId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
BatchSettingValues::getBoundedValue(
				const SettingId::SettingIdType boundedId
				   ) const
{
  CALL_TRACE("getBoundedValue(boundedId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBatchBoundedId(boundedId)), boundedId);

  return(arrBatchValues_[boundedId]);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getDiscreteValue(discreteId)  [const]
//
//@ Interface-Description
//  Return the discrete setting value that is given by 'discreteId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteValue
BatchSettingValues::getDiscreteValue(
				const SettingId::SettingIdType discreteId
				    ) const
{
  CALL_TRACE("getDiscreteValue(discreteId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBatchDiscreteId(discreteId)),
			  discreteId);

  return(arrBatchValues_[discreteId]);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSequentialValue(sequentialId)  [const]
//
//@ Interface-Description
//  Return the sequential setting value that is given by 'sequentialId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchSequentialId(sequentialId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SequentialValue
BatchSettingValues::getSequentialValue(
				const SettingId::SettingIdType sequentialId
				    ) const
{
  CALL_TRACE("getSequentialValue(sequentialId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBatchSequentialId(sequentialId)),
			  sequentialId);

  return(arrBatchValues_[sequentialId]);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setSettingValue(settingId, newValue)
//
//@ Interface-Description
//  Set the value of the setting represented by 'settingId',
//  to 'newValue'.  This will activate this instance's callback, if
//  any, to indicate that the setting setting identified by 'settingId'
//  has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchId(settingId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BatchSettingValues::setSettingValue(const SettingId::SettingIdType settingId,
				    const SettingValue&            newValue)
{
  CALL_TRACE("setSettingValue(settingId, newValue)");
  CLASS_PRE_CONDITION((SettingId::IsBatchId(settingId)));

  // copy 'newValue' as the new setting value of the setting identified by
  // 'settingId'...
  arrBatchValues_[settingId] = newValue;

  // activate this instance's callback, if any, to indicate that a setting's
  // value has changed...
  activateCallback_(settingId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setBoundedValue(boundedId, newValue)
//
//@ Interface-Description
//  Set the value of the bounded setting represented by 'boundedId',
//  to 'newValue'.  This will activate this instance's callback, if
//  any, to indicate that the bounded setting identified by 'boundedId'
//  has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BatchSettingValues::setBoundedValue(const SettingId::SettingIdType boundedId,
				    const BoundedValue&            newValue)
{
  CALL_TRACE("setBoundedValue(boundedId, newValue)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBatchBoundedId(boundedId)),
			  boundedId);

  // copy 'newValue' as the new bounded value of the bounded setting
  // identified by 'boundedId'...
  arrBatchValues_[boundedId] = newValue;

  // activate this instance's callback, if any, to indicate that a setting's
  // value has changed...
  activateCallback_(boundedId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setDiscreteValue(discreteId, newValue)
//
//@ Interface-Description
//  Set the value of the discrete setting represented by 'discreteId',
//  to 'newValue'.  This will activate this instance's callback, if
//  any, to indicate that the discrete setting identified by 'discreteId'
//  has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BatchSettingValues::setDiscreteValue(const SettingId::SettingIdType discreteId,
				     const DiscreteValue            newValue)
{
  CALL_TRACE("setDiscreteValue(discreteId, newValue)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBatchDiscreteId(discreteId)),
			  discreteId);

  // copy 'newValue' as the new discrete value of the discrete setting
  // identified by 'discreteId'...
  arrBatchValues_[discreteId] = newValue;

  // activate this instance's callback, if any, to indicate that a setting's
  // value has changed...
  activateCallback_(discreteId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setSequentialValue(sequentialId, newValue)
//
//@ Interface-Description
//  Set the value of the sequential setting represented by 'sequentialId',
//  to 'newValue'.  This will activate this instance's callback, if
//  any, to indicate that the sequential setting identified by 'sequentialId'
//  has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBatchSequentialId(sequentialId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BatchSettingValues::setSequentialValue(
				const SettingId::SettingIdType sequentialId,
			        const SequentialValue          newValue
				      )
{
  CALL_TRACE("setSequentialValue(sequentialId, newValue)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBatchSequentialId(sequentialId)),
			  sequentialId);

  // copy 'newValue' as the new sequential value of the sequential setting
  // identified by 'sequentialId'...
  arrBatchValues_[sequentialId] = newValue;

  // activate this instance's callback, if any, to indicate that a setting's
  // value has changed...
  activateCallback_(sequentialId);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(batchValues)
//
//@ Interface-Description
//  Copy all of the batch setting values from 'batchValues' into here.
//  No callbacks are generated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method will be used for fast copies, with no callbacks, therefore
//  speed is of the essence.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BatchSettingValues::operator=(const BatchSettingValues& batchValues)
{
  CALL_TRACE("operator=(batchValues)");

  if (this != &batchValues)
  {   // $[TI1]
    Uint32 idx;

    // for each batch setting id...
    for (idx = SettingId::LOW_BATCH_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++)
    {
      // copy the value, identified by 'idx', from 'batchValues' into this
      // instance...
      arrBatchValues_[idx] = batchValues.arrBatchValues_[idx];
    }
  }   // $[TI2] -- assignment to oneself...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  copyFrom(batchValues)
//
//@ Interface-Description
//  Copy all of the CHANGED batch setting values from 'batchValues'
//  into here.  Each value copied into this instance will activate the
//  registered callback, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method will be used for copies, with callbacks, therefore speed
//  is not as critical as the assignment operator.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BatchSettingValues::copyFrom(const BatchSettingValues& batchValues)
{
  CALL_TRACE("copyFrom(batchValues)");

  if (this != &batchValues)
  {   // $[TI1]
    Uint32  idx;

    // for each bounded, BD batch setting...
    for (idx = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
    {   // $[TI1.1] -- ALWAYS enters this branch...
      const SettingId::SettingIdType  ID = (SettingId::SettingIdType)idx;

      if (getBoundedValue(ID) != batchValues.getBoundedValue(ID))
      {   // $[TI1.1.1]
	// The bounded value from this instance identified by 'ID' is
	// different than the corresponding bounded value from
	// 'batchValues', therefore copy it into this instance, thereby
	// activating the callback...
	setBoundedValue(ID, batchValues.getBoundedValue(ID));
      }   // $[TI1.1.2] -- no change in the value of the 'ID' setting...
    }

    // for each discrete, BD batch setting...
    for (idx = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
    {   // $[TI1.2] -- ALWAYS enters this branch...
      const SettingId::SettingIdType  ID = (SettingId::SettingIdType)idx;

      if (getDiscreteValue(ID) != batchValues.getDiscreteValue(ID))
      {   // $[TI1.2.1]
	// The discrete value from this instance identified by 'ID' is
	// different than the corresponding discrete value from
	// 'batchValues', therefore copy it into this instance, thereby
	// activating the callback...
	setDiscreteValue(ID, batchValues.getDiscreteValue(ID));
      }   // $[TI1.2.2] -- no change in the value of the 'ID' setting...
    }

    // for each bounded, non-BD batch setting...
    for (idx = SettingId::LOW_BATCH_NONBD_BOUNDED_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_NONBD_BOUNDED_ID_VALUE; idx++)
    {   // $[TI1.3] -- ALWAYS enters this branch...
      const SettingId::SettingIdType  ID = (SettingId::SettingIdType)idx;

      if (getBoundedValue(ID) != batchValues.getBoundedValue(ID))
      {   // $[TI1.3.1]
	// The bounded value from this instance identified by 'ID' is
	// different than the corresponding bounded value from
	// 'batchValues', therefore copy it into this instance, thereby
	// activating the callback...
	setBoundedValue(ID, batchValues.getBoundedValue(ID));
      }   // $[TI1.3.2] -- no change in the value of the 'ID' setting...
    }

    // for each discrete, non-BD batch setting...
    for (idx = SettingId::LOW_BATCH_NONBD_DISCRETE_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_NONBD_DISCRETE_ID_VALUE; idx++)
    {   // $[TI1.4] -- ALWAYS enters this branch...
      const SettingId::SettingIdType  ID = (SettingId::SettingIdType)idx;

      if (getDiscreteValue(ID) != batchValues.getDiscreteValue(ID))
      {   // $[TI1.4.1]
	// The discrete value from this instance identified by 'ID' is
	// different than the corresponding discrete value from
	// 'batchValues', therefore copy it into this instance, thereby
	// activating the callback...
	setDiscreteValue(ID, batchValues.getDiscreteValue(ID));
      }   // $[TI1.4.2] -- no change in the value of the 'ID' setting...
    }

    // for each sequential, non-BD batch setting...
    for (idx = SettingId::LOW_BATCH_NONBD_SEQUENTIAL_ID_VALUE;
	 idx <= SettingId::HIGH_BATCH_NONBD_SEQUENTIAL_ID_VALUE; idx++)
    {   // $[TI1.5] -- ALWAYS enters this branch...
      const SettingId::SettingIdType  ID = (SettingId::SettingIdType)idx;

      if (getSequentialValue(ID) != batchValues.getSequentialValue(ID))
      {   // $[TI1.5.1]
	// The sequential value from this instance identified by 'ID' is
	// different than the corresponding sequential value from
	// 'batchValues', therefore copy it into this instance, thereby
	// activating the callback...
	setSequentialValue(ID, batchValues.getSequentialValue(ID));
      }   // $[TI1.5.2] -- no change in the value of the 'ID' setting...
    }
  }   // $[TI2] -- assignment to oneself...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BatchSettingValues::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  BATCH_SETTING_VALUES, lineNumber, pFileName,
			  pPredicate);
}


#if defined(SIGMA_DEVELOPMENT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Free-Function:  operator<<(ostr, batchValues)  [friend]
//
// Interface-Description
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const BatchSettingValues& batchValues)
{
  CALL_TRACE("operator<<(ostr, batchValues)");

  Uint  idx;

  for (idx = SettingId::LOW_BATCH_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_ID_VALUE; idx++)
  {
    const SettingId::SettingIdType  SETTING_ID = (SettingId::SettingIdType)idx;

    ostr << "[" << SettingId::GetSettingName(SETTING_ID) << "] = ";

    if (SettingId::IsBatchBoundedId(SETTING_ID))
    {
      ostr << BoundedValue(batchValues.arrBatchValues_[idx]);
    }
    else if (SettingId::IsBatchDiscreteId(SETTING_ID))
    {
      ostr << DiscreteValue(batchValues.arrBatchValues_[idx]);
    }
    else if (SettingId::IsBatchSequentialId(SETTING_ID))
    {
      ostr << SequentialValue(batchValues.arrBatchValues_[idx]);
    }
    else
    {
      // unexpected setting category...
      AUX_FREE_ASSERTION_FAILURE(idx, SETTINGS_VALIDATION,
      				 BATCH_SETTING_VALUES);
    }

    ostr << endl;
  }

  return(ostr);
}

#endif // defined(SIGMA_DEVELOPMENT)
