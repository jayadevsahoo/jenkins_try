#ifndef GuiStart_HH
#define GuiStart_HH
 
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  GuiStart - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiStart.hhv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "Sys_Init.hh"
#include "FsmState.hh"
//@ End-Usage

class GuiStart : public FsmState
{
  public:
    GuiStart();
    ~GuiStart();

    const char* nameOf(void) const;
    SigmaState getSigmaState(void) const;
    void activate(void) const;
    void receiveBdReady(const BdReadyMessage& message) const;
    void receiveStartupTimer(const StartupTimer& message) const;

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);
 
  private:
    GuiStart(const GuiStart&);  // not implemented
    void operator=(const GuiStart&);         // not implemented
};
 

#endif // GuiStart_HH
