#ifndef ConfigurationAgent_HH
#define ConfigurationAgent_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ConfigurationAgent - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ConfigurationAgent.hhv   25.0.4.0   19 Nov 2013 13:54:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: mnr    Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX firmware revision and serial number being sent to GUI via
//		BdReadyMsg.
//
//  Revision: 003  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002  By:  syw    Date: 30-Jan-1997    DR Number: DCS 1555
//      Project:   Sigma   (R8027)
//      Description:
//         Added bdNovRamSerialNumber_ data member and getBdNovRamSerialNumber()
//		   method.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================

#include "BD_IO_Devices.hh"

// Usage-Classes

#include "SerialNumber.hh"
#include "CrcUtilities.hh"
#include "CalInfoFlashSignature.hh"
#include "SoftwareRevNum.hh"
//#include "CompressorEeprom.hh"
#include "BigSerialNumber.hh"

// End-Usage

class ConfigurationAgent
{
  public:

   	ConfigurationAgent(void);
   	~ConfigurationAgent(void);

   	ConfigurationAgent(const ConfigurationAgent&);

   	static void SoftFault( const SoftFaultID softFaultID,
							  const Uint32      lineNumber,
							  const char*       pFileName  = NULL,
							  const char*       pPredicate = NULL );

	inline const SerialNumber & getFlashSerialNumber( void) const ;

	inline CalInfoFlashSignature::Signature getExhValveSignature( void) const ;
	inline CrcValue getExhValveCrc( void) const ;

	inline Uint32 getAirFlowSensorSN( void) const ;
	inline CrcValue getAirFlowSensorCrc( void) const ;

	inline Uint32 getO2FlowSensorSN( void) const ;
	inline CrcValue getO2FlowSensorCrc( void) const ;

	inline Uint32 getExhFlowSensorSN( void) const ;
	inline CrcValue getExhFlowSensorCrc( void) const ;

	inline Uint32 getExhO2FlowSensorSN( void) const ;
	inline CrcValue getExhO2FlowSensorCrc( void) const ;

	inline const char *getSoftwareRevNum( void) const ;
	inline const SerialNumber & getCompressorSerialNumber( void) const ;

	inline const SerialNumber & getBdNovRamSerialNumber( void) const ;

	inline const BigSerialNumber & getProxFirmwareRev( void) const ;

	inline const Uint32 & getProxSerialNum(void) const;

	inline const Boolean & getIsProxInstalled(void) const;

#ifdef SIGMA_DEBUG

	void dumpDataMembers( void) const ;

#endif // SIGMA_DEBUG

  private:
   	void operator=(const ConfigurationAgent&);

	//@ Data-Member: flashSerialNumber_
	// serial number stored in flash
	SerialNumber flashSerialNumber_ ;

	//@ Data-Member:::Signature exhValveSignature_
	// exh valve signature
	CalInfoFlashSignature::Signature exhValveSignature_ ;

	//@ Data-Member: exhValveCrc_
	//exh valve Crc value
	CrcValue exhValveCrc_ ;

	//@ Data-Member: airFlowSensorSN_
	// air serial number
	Uint32 airFlowSensorSN_ ;

	//@ Data-Member: airFlowSensorCrc_
	// air flow sensor Crc value
	CrcValue airFlowSensorCrc_ ;

	//@ Data-Member: o2FlowSensorSN_
	// o2 serial number
	Uint32 o2FlowSensorSN_ ;

	//@ Data-Member: o2FlowSensorCrc_
	// o2 flow sensor Crc value
	CrcValue o2FlowSensorCrc_ ;

	//@ Data-Member: exhFlowSensorSN_
	// exh serial number
	Uint32 exhFlowSensorSN_ ;

	//@ Data-Member: exhFlowSensorCrc_
	// exh flow sensor Crc value
	CrcValue exhFlowSensorCrc_ ;

	//@ Data-Member: softwareRevNum_[SoftwareRevNum::REV_NUM_SIZE]
	// stores the software revision number for the BD CPU
	char softwareRevNum_[SoftwareRevNum::REV_NUM_SIZE] ;

	//@ Data-Member: compressorSerialNumber_
	// serial number stored in compressor eeprom
	SerialNumber compressorSerialNumber_ ;

	//@ Data-Member: bdNovRamSerialNumber_
	// serial number that is stored in novram
	SerialNumber bdNovRamSerialNumber_ ;

	//@ Data-Member: proxFirmwareRev_
	// prox main revision string that is stored in novram
	BigSerialNumber proxFirmwareRev_;

	//@ Data-Member: proxSerialNum_
	// PROX serial number
	Uint32 proxSerialNum_ ;

	//@ Data-Member: isProxInstall_
	// is a prox board installed in system
	Boolean isProxInstalled_;

} ;

#include "ConfigurationAgent.in"

#endif // ConfigurationAgent_HH



