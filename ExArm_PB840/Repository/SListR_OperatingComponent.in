
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SListR_OperatingComponent - Abstract Singly-Linked List of 'OperatingComponent' References.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListR.I_v   25.0.4.0   19 Nov 2013 14:03:44   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//====================================================================
//
//  Friend Functions...
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  operator<<(ostr, sList)  [Friend Function]
//
// Interface-Description
//  Dump out the contents of 'sList' to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

inline Ostream&
operator<<(Ostream& ostr, const SListR_OperatingComponent& sList)
{
  CALL_TRACE("::operator<<(ostr, sList)");
  return(sList.print_(ostr));
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEmpty()  [const]
//
//@ Interface-Description
//  Is this list empty?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline Boolean
SListR_OperatingComponent::isEmpty(void) const
{
  CALL_TRACE("isEmpty()");
  return(rPtrList_.isEmpty());  // $[TI1]
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SListR_OperatingComponent(pList)  [Constructor]
//
//@ Interface-Description
//  Construct an empty list of references, using 'pPtrList' as the
//  internal reference list for this instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The 'rPtrList_' alias can NOT be used from within this constructor,
//  because the list of references will NOT be constructed until the
//  derived class's constructor is run.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

inline
SListR_OperatingComponent::SListR_OperatingComponent(SListC_MemPtr* pList)
					   : rPtrList_(*pList)
{
  CALL_TRACE("SListR_OperatingComponent()");
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isLegalNode_(pNode)  [const]
//
//@ Interface-Description
//  Is the node pointed to by 'pNode' currently allocated from this
//  list's heap?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline Boolean
SListR_OperatingComponent::isLegalNode_(const SNodeC_MemPtr* pNode) const
{
  CALL_TRACE("isLegalNode_(pNode)");
  return(rPtrList_.isLegalNode_(pNode));  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getFirst_()  [const]
//
//@ Interface-Description
//  Return a constant pointer to the first node in this list.  If
//  the list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline const SNodeC_MemPtr*
SListR_OperatingComponent::getFirst_(void) const
{
  CALL_TRACE("getFirst_() const");
  return(rPtrList_.getFirst_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrent_()  [const]
//
//@ Interface-Description
//  Return a constant pointer to the current node in this list.  If
//  the list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline const SNodeC_MemPtr*
SListR_OperatingComponent::getCurrent_(void) const
{
  CALL_TRACE("getCurrent_() const");
  return(rPtrList_.getCurrent_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getFirst_()
//
//@ Interface-Description
//  Return a non-constant pointer to the first node in this list.  If
//  the list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline SNodeC_MemPtr*
SListR_OperatingComponent::getFirst_(void)
{
  CALL_TRACE("getFirst_()");
  return(rPtrList_.getFirst_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrent_()
//
//@ Interface-Description
//  Return a non-constant pointer to the current node in this list.  If
//  the list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline SNodeC_MemPtr*
SListR_OperatingComponent::getCurrent_(void)
{
  CALL_TRACE("getCurrent_()");
  return(rPtrList_.getCurrent_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setCurrent_(pNewCurr)
//
//@ Interface-Description
//  Set this list's current pointer to 'pNewCurr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (isLegalNode_(pNewCurr))
//---------------------------------------------------------------------
//@ PostConditions
//  (pNewCurr == getCurrent_())
//@ End-Method
//=====================================================================

inline void
SListR_OperatingComponent::setCurrent_(SNodeC_MemPtr* pNewCurr)
{
  CALL_TRACE("setCurrent_(pNewCurr)");
  SAFE_CLASS_PRE_CONDITION((isLegalNode_(pNewCurr)));
  rPtrList_.setCurrent_(pNewCurr);
}  // $[TI1]


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isFull()  [const]
//
//@ Interface-Description
//  Is this list full?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline Boolean
SListR_OperatingComponent::isFull(void) const
{
  CALL_TRACE("isFull()");
  return(rPtrList_.isFull());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isAtFirst()  [const]
//
//@ Interface-Description
//  Is this list not empty, and is the current node also the first
//  node in this list?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline Boolean
SListR_OperatingComponent::isAtFirst(void) const
{
  CALL_TRACE("isAtFirst()");
  return(rPtrList_.isAtFirst());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isAtLast()  [const]
//
//@ Interface-Description
//  Is this list not empty, and is the current node also the last
//  node in this list?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline Boolean
SListR_OperatingComponent::isAtLast(void) const
{
  CALL_TRACE("isAtLast()");
  return(rPtrList_.isAtLast());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getNumItems()  [const]
//
//@ Interface-Description
//  Return the number of items currently in this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline Uint32
SListR_OperatingComponent::getNumItems(void) const
{
  CALL_TRACE("getNumItems()");
  return(rPtrList_.getNumItems());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getMaxItems()  [const]
//
//@ Interface-Description
//  Return the maximum number of items allowed in this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline Uint32
SListR_OperatingComponent::getMaxItems(void) const
{
  CALL_TRACE("getMaxItems()");
  return(rPtrList_.getMaxItems());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  goFirst()
//
//@ Interface-Description
//  Move the current pointer to the first node in this list, and
//  return 'SUCCESS'.  If the list is empty, then 'FAILURE' is
//  returned, and the list is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  ((goFirst() == SUCCESS) ? isAtFirst() : isEmpty())
//@ End-Method
//=====================================================================

inline SigmaStatus
SListR_OperatingComponent::goFirst(void)
{
  CALL_TRACE("goFirst()");
  return(rPtrList_.goFirst());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  goNext()
//
//@ Interface-Description
//  Move the current pointer to the next node in this list, and
//  return 'SUCCESS'.  If the list is empty or the current node is
//  at the end of the list, then 'FAILURE' is returned, and the list
//  is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  ((goNext() == SUCCESS) ? !isAtFirst() : (isEmpty() || isAtLast())
//@ End-Method
//=====================================================================

inline SigmaStatus
SListR_OperatingComponent::goNext(void)
{
  CALL_TRACE("goNext()");
  return(rPtrList_.goNext());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  insert(newItem, insertPlace)
//
//@ Interface-Description
//  Insert 'newItem' before/after the current list node.  If
//  'insertPlace' is 'AFTER_CURRENT' then insert after, else insert
//  before. (If the list is empty, then 'insertPlace' is ignored.)
//---------------------------------------------------------------------
//@ Implementation-Description
//  Insert a pointer to 'newItem' into this list's reference list.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//---------------------------------------------------------------------
//@ PostConditions
//  (currentItem() == newItem)
//@ End-Method
//=====================================================================

inline void
SListR_OperatingComponent::insert(OperatingComponent&                newItem,
			     const InsertionPlace insertPlace)
{
  CALL_TRACE("insert(newItem, insertPlace)");
  SAFE_CLASS_PRE_CONDITION((!isFull()));  // checked by 'rPtrList_'...
  void*  pMem = (void*)&newItem;  // to avoid "non-const reference" error...
  rPtrList_.insert(pMem, insertPlace);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  append(newItem)
//
//@ Interface-Description
//  Append 'newItem' to the end of this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Append a pointer to 'newItem' to the end of this list's reference
//  list.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//---------------------------------------------------------------------
//@ PostConditions
//  (isAtLast()  &&  currentItem() == newItem)
//@ End-Method
//=====================================================================

inline void
SListR_OperatingComponent::append(OperatingComponent& newItem)
{
  CALL_TRACE("append(newItem)");
  SAFE_CLASS_PRE_CONDITION((!isFull()));  // checked by 'rPtrList_'...
  void*  pMem = (void*)&newItem;  // to avoid "non-const reference" error...
  rPtrList_.append(pMem);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  prepend(newItem)
//
//@ Interface-Description
//  Prepend 'newItem' to the front of this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Prepend a pointer to 'newItem' to the front of this list's reference
//  list.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//---------------------------------------------------------------------
//@ PostConditions
//  (isAtFirst()  &&  currentItem() == newItem)
//@ End-Method
//=====================================================================

inline void
SListR_OperatingComponent::prepend(OperatingComponent& newItem)
{
  CALL_TRACE("prepend(newItem)");
  SAFE_CLASS_PRE_CONDITION((!isFull()));  // checked by 'rPtrList_'...
  void*  pMem = (void*)&newItem;  // to avoid "non-const reference" error...
  rPtrList_.prepend(pMem);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeCurrent()
//
//@ Interface-Description
//  Remove the current node from the list, and return 'SUCCESS'.  If
//  the list is empty, then return 'FAILURE', and the list is left
//  unchanged.  When a node is removed, the "next" node is set as the
//  "current" node.  If the removed node is the "last" node in the list,
//  the "current" node is set to the "previous" node.  If the removed
//  node is the only node in this list, the list is left empty.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If this list is not empty, then remove the current node, otherwise
//  return 'FAILURE'.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline SigmaStatus
SListR_OperatingComponent::removeCurrent(void)
{
  CALL_TRACE("removeCurrent()");
  return((!isEmpty()) ? rPtrList_.removeCurrent()	// $[TI1]
		      : FAILURE);			// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  clearList()
//
//@ Interface-Description
//  Clear out this list, and leave this list as an empty list.  The
//  referenced objects are not destroyed by this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

inline void
SListR_OperatingComponent::clearList(void)
{
  CALL_TRACE("clearList()");
  rPtrList_.clearList();
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  currentItem()  [const]
//
//@ Interface-Description
//  Return a constant reference to the current item in this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Get the reference pointer in the current node, then cast and
//  dereference that pointer.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isEmpty())
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline const OperatingComponent&
SListR_OperatingComponent::currentItem(void) const
{
  CALL_TRACE("currentItem() const");
  SAFE_CLASS_PRE_CONDITION((!isEmpty()));  // checked by 'rPtrList_'...
  return(*((const OperatingComponent*)(rPtrList_.currentItem())));  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  currentItem()
//
//@ Interface-Description
//  Return a constant reference to the current item in this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Get the reference pointer in the current node, then cast and
//  dereference that pointer.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isEmpty())
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline OperatingComponent&
SListR_OperatingComponent::currentItem(void)
{
  CALL_TRACE("currentItem()");
  SAFE_CLASS_PRE_CONDITION((!isEmpty()));  // checked by 'rPtrList_'...
  return(*((OperatingComponent*)(rPtrList_.currentItem())));  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(sList)  [Assignment Operator]
//
//@ Interface-Description
//  Copy the items of 'sList' into this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (getMaxItems() == sList.getMaxItems())
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline void
SListR_OperatingComponent::operator=(const SListR_OperatingComponent& sList)
{
  CALL_TRACE("operator=(sList)");
  SAFE_CLASS_PRE_CONDITION((getMaxItems() == sList.getMaxItems()));
  rPtrList_ = sList.rPtrList_;
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator==(sList)  [const]
//
//@ Interface-Description
//  Is this list equivalent to 'sList'?  Two lists are equivalent if
//  they have the same number of nodes, and each of the referenced
//  objects in one list is equivalent to the corresponding referenced
//  object in the other list.
//  (Same as 'isEquivTo(sList, EQUIV_OBJECTS)'.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline Boolean
SListR_OperatingComponent::operator==(const SListR_OperatingComponent& sList) const
{
  CALL_TRACE("operator==(sList)");
  return(isEquivTo(sList, EQUIV_OBJECTS));  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator!=(sList)  [const]
//
//@ Interface-Description
//  Is this list NOT equivalent to 'sList'?  Two lists are equivalent
//  if they have the same number of nodes, and each of the referenced
//  objects in one list is equivalent to the corresponding referenced
//  object in the other list.
//  (Same as '!isEquivTo(sList, EQUIV_OBJECTS)'.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

inline Boolean
SListR_OperatingComponent::operator!=(const SListR_OperatingComponent& sList) const
{
  CALL_TRACE("operator!=(sList)");
  return(!isEquivTo(sList, EQUIV_OBJECTS));  // $[TI1]
}
