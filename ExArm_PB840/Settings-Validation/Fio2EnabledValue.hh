
#ifndef Fio2EnabledValue_HH
#define Fio2EnabledValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Fio2EnabledValue - Values of the FIO2-Enabled Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Fio2EnabledValue.hhv   25.0.4.0   19 Nov 2013 14:27:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: rpr    Date:  05-Feb-2009    SCR Number: 6435
//  Project: 840S
//  Description:
//		Modified to support neonatal Plus 20 O2.
// 
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct Fio2EnabledValue
{
  //@ Type:  Fio2EnabledValueId 
  // All of the possible values of the FIO2-Enabled Setting.
  enum Fio2EnabledValueId
  {
    // $[02274] -- values of the range of FIO2-Enabled Setting...
    NO_FIO2_ENABLED,
    YES_FIO2_ENABLED,
	CALIBRATE_FIO2,

    TOTAL_FIO2_ENABLED_VALUES
  };
};


#endif // Fio2EnabledValue_HH 
