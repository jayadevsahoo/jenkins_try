#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NV_MemoryStorageThread - Nonvolatile memory storage thread
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides execution context for the NV_MemoryStorage class
//  objects.
//
//  When the Initialize method is called during system initialization,
//  (prior to starting the various threads), NV_MemoryStorage class objects
//  are instantiated.
//  Once this thread is running, it provides execution context for those
//  objects by calling their CheckForRequiredStorageUpdate method.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides an interface to this subsystem, without unduly
//  exposing the internals and dependencies.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ Fault-Handling
//  If memory is not available for allocation, a fault will be thrown.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//

#include "NonVolatileMemory.hh"
#include "PostNovram.hh"
#include "NV_MemoryStorageThread.hh"
#include "NV_MemoryStorage.hh"
#include "BD_IO_Devices.hh"
#include "DebugAssert.h"
#include "Task.hh"

NV_MemoryStorage *  NV_MemoryStorageThread::pNV_MemoryStorage_[MAX_NUMBER_OR_NV_STORAGE_AREAS];
bool                NV_MemoryStorageThread::initialized_ = false;

static const Uint32 STORAGE_UPDATE_PERIOD_SEC = 2;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Shutdown()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void NV_MemoryStorageThread::Shutdown()
{
    int i;

    // Delete the various NV memory allocations
    for (i=0; i<MAX_NUMBER_OR_NV_STORAGE_AREAS; i++)
    {
        delete pNV_MemoryStorage_[i];
		pNV_MemoryStorage_[i] = NULL;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()
//
//@ Interface-Description
//  This allocates the NV_MemoryStorage objects and ties them to the global
//  pointers used in the rest of the system.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void NV_MemoryStorageThread::Initialize()
{
    initialized_ = false;

    int i;

    // Init the array of pointers to the various NV memory
    for (i=0; i<MAX_NUMBER_OR_NV_STORAGE_AREAS; i++)
    {
        pNV_MemoryStorage_[i] = 0;
    }

    i = 0;

    // Create each NV store
    // App NOVRAM
    pNV_MemoryStorage_[i] = new NV_MemoryStorage((void **)&NonVolatileMemory::P_INSTANCE_,
                                                NonVolatileMemory::MAX_BYTES_AVAILABLE_,
                                                "AppNovRam",
                                                true);
    DEBUG_ASSERT(++i < MAX_NUMBER_OR_NV_STORAGE_AREAS);

    // POST NOVRAM
    pNV_MemoryStorage_[i] = new NV_MemoryStorage((void **)&PPostNovram,
                                                POST_NOVRAM_LENGTH,
                                                "PostNovRam",
                                                true);
    DEBUG_ASSERT(++i < MAX_NUMBER_OR_NV_STORAGE_AREAS);

    // Flash serial number and the rest of the cal info
    pNV_MemoryStorage_[i] = new NV_MemoryStorage((void **)&FLASH_SERIAL_NO_ADDR,
                                                SIZE_OF_FLASH_SERIAL_NO_ADDR,
                                                "SnAddrFlash",
                                                true);
    DEBUG_ASSERT(++i < MAX_NUMBER_OR_NV_STORAGE_AREAS);


/*
    >>>> To add new memory store, create a new instance of NV_MemoryStorage with corresponding
         pointer address, size and file name. If the memory is not based on a pointer,
         the constructor for NV_MemoryStorage will need to be overloaded.

    pNV_MemoryStorage_[i] = new NV_MemoryStorage((void **)&NonVolatileMemory::P_INSTANCE_,
                                                NonVolatileMemory::MAX_BYTES_AVAILABLE_,
                                                _T("AppNovRam"),
                                                true);
    DEBUG_ASSERT(++i < MAX_NUMBER_OR_NV_STORAGE_AREAS);
*/
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UpdateStorageObjects()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void NV_MemoryStorageThread::UpdateStorageObjects()
{
    int i;

    // Delete the various NV memory allocations
    for (i=0; i<MAX_NUMBER_OR_NV_STORAGE_AREAS; i++)
    {
        if (pNV_MemoryStorage_[i] != 0)
        {
            pNV_MemoryStorage_[i]->CheckForStorageUpdate_();
        }
    }
}





//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Thread()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void NV_MemoryStorageThread::Thread()
{
    while (true)
    {
        UpdateStorageObjects();
        Task::Delay(STORAGE_UPDATE_PERIOD_SEC);
    }
}




