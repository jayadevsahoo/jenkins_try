
#ifndef VgaDisplayId_HH
#define VgaDisplayId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ==== 
//@ Filename: VgaDisplayId.hh - VGA display ID.
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaDisplayId.hhv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001   By: sah   Date: 01-Aug-1997   DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//      Initial version to coding standards (Integration baseline).
//
//===================================================================== 

//@ Type:  VgaDisplayId
//
enum VgaDisplayId
{
  LOWER_VGA_DISPLAY      =  0,
  UPPER_VGA_DISPLAY      =  1,
  NUM_VGA_DISPLAYS       =  2
};


#endif // VgaDisplayId_HH
