#ifndef InitiateReboot_HH
#define InitiateReboot_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename:  InitiateReboot - Functions to initiate a system reboot.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/InitiateReboot.hhv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//
//====================================================================

//@ Usage-Classes
#include "kpost.hh"
//@ End-Usage


//@ Begin-Free-Declarations
extern void  InitiateRebootHandler(void);
extern void  InitiateReboot(ShutdownState state=EXCEPTION);
//@ End-Free-Declarations


#endif // InitiateReboot_HH 
