 
#ifndef TubeTypeSetting_HH
#define TubeTypeSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TubeTypeSetting - Tube Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TubeTypeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah   Date:  06-Jan-1999    DR Number:  5314
//  Project:  ATC
//  Description:
//     Added new 'getApplicability()' method.
//
//  Revision: 001   By: sah   Date:  06-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting.
//  
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class TubeTypeSetting : public BatchDiscreteSetting
{
  public:
    TubeTypeSetting(void);
    virtual ~TubeTypeSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32 lineNumber,
			   const char*  pFileName  = NULL, 
			   const char*  pPredicate = NULL);

  private:
    TubeTypeSetting(const TubeTypeSetting&);	// not implemented...
    void  operator=(const TubeTypeSetting&);		// not implemented...
};


#endif // TubeTypeSetting_HH 
