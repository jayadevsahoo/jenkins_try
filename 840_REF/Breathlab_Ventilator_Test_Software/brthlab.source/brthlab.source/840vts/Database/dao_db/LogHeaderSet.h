#if !defined(AFX_LOGHEADERSET_H__617D124D_54A5_11D3_9C97_004005519CCD__INCLUDED_)
#define AFX_LOGHEADERSET_H__617D124D_54A5_11D3_9C97_004005519CCD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LogHeaderSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogHeaderSet DAO recordset

class CLogHeaderSet : public CDaoRecordset
{
public:
	CLogHeaderSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CLogHeaderSet)

// Field/Param Data
	//{{AFX_FIELD(CLogHeaderSet, CDaoRecordset)
	long	m_ID;
	CString	m_FileName;
	CString	m_BDSN;
	CString	m_GUISN;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogHeaderSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGHEADERSET_H__617D124D_54A5_11D3_9C97_004005519CCD__INCLUDED_)
