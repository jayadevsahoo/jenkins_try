#include <cvidef.h>

/*============================================================================*/
/*                        L a b W i n d o w s / C V I                         */
/*----------------------------------------------------------------------------*/
/*    Copyright (c) National Instruments 1987-1999.  All Rights Reserved.     */
/*----------------------------------------------------------------------------*/
/*                                                                            */
/* Title:       easytab.h                                                     */
/* Purpose:     Implements tab dialogs                                        */
/*                                                                            */
/*============================================================================*/

#ifndef EASY_TAB_HEADER
#define EASY_TAB_HEADER


#ifdef _CVI_
#pragma EnableLibraryRuntimeChecking
#endif

#include <userint.h>
#include "toolbox.h"

#ifdef __cplusplus
    extern "C" {
#endif

#define EVENT_TAB_CHANGED               6000    /* eventData1 = panel of newly active tab, eventData2 = panel of previously active tab */

    /*  EasyTab_SetAttribute and EasyTab_GetAttribute attributes */
#define ATTR_EASY_TAB_ACTIVE_PANEL      3800
#define ATTR_EASY_TAB_ORIENTATION       3801
#define ATTR_EASY_TAB_STRETCH_TO_FIT    3802
#define ATTR_EASY_TAB_BTN_GAP_SIZE      3803    /* -1 == much a possible when ATTR_EASY_TAB_STRETCH_TO_FIT is TRUE */
#define ATTR_EASY_TAB_META_FONT         3804
#define ATTR_EASY_TAB_META_FONT_LENGTH  3805
#define ATTR_EASY_TAB_LABEL_COLOR       3806
#define ATTR_EASY_TAB_BG_COLOR          3807
#define ATTR_EASY_TAB_VISIBLE           3808

    /*  EasyTab_SetTabAttribute and EasyTab_GetTabAttribute attributes */
#define ATTR_EASY_TAB_VISIBLE           3808    /* same as above ATTR_EASY_TAB_VISIBLE */
#define ATTR_EASY_TAB_DIMMED            3900
#define ATTR_EASY_TAB_LABEL_TEXT        3901
#define ATTR_EASY_TAB_LABEL_TEXT_LENGTH 3902
#define ATTR_EASY_TAB_LABEL_BG_COLOR    3903



        /* Values for the ATTR_EASY_TAB_ORIENTATION attribute: */
#define VAL_EASY_TAB_BTNS_ON_TOP        0
#define VAL_EASY_TAB_BTNS_ON_BOTTOM     1
#define VAL_EASY_TAB_BTNS_ON_LEFT       2
#define VAL_EASY_TAB_BTNS_ON_RIGHT      3

        /* Values for the whichBounds parameter to EasyTab_SetBounds() and EasyTab_GetBounds() */
#define VAL_EASY_TAB_INTERIOR_BOUNDS    1
#define VAL_EASY_TAB_EXTERIOR_BOUNDS    2


int CVIFUNC EasyTab_Create(int panel, int top, int left, CtrlCallbackPtr callback, void *callbackData);
int CVIFUNC EasyTab_ConvertFromCanvas(int panel, int canvas);

int CVIFUNC_C EasyTab_LoadPanels(int panel, int easyTabCtrl, int row, char *fileName, void *module, ...);
int CVIFUNC_C EasyTab_AddPanels(int panel, int easyTabCtrl, int row, ...);

int CVIFUNC_C EasyTab_SetAttribute(int panel, int easyTabCtrl, int attribute, ...);
int CVIFUNC EasyTab_GetAttribute(int panel, int easyTabCtrl, int attribute, void *value);

int CVIFUNC_C EasyTab_SetTabAttribute(int panel, int easyTabCtrl, int tabPanelHandle, int attribute, ...);
int CVIFUNC EasyTab_GetTabAttribute(int panel, int easyTabCtrl, int tabPanelHandle, int attribute, void *value);

int CVIFUNC EasyTab_SetBounds(int panel, int easyTabCtrl, int whichBounds, Rect bounds);
int CVIFUNC EasyTab_GetBounds(int panel, int easyTabCtrl, int whichBounds, Rect *bounds);
int CVIFUNC EasyTab_AutoSize(int panel, int easyTabCtrl);




int CVIFUNC EasyTab_GetPanelList(int panel, int easyTabCtrl, ListType *list);       /* seldom needed, no function panel */   
    
    /* OBSOLETE FUNCTION: Do not use for new code                   */
    /* special values for numCtrls parameter to EasyTab_AddSheet()  */
#define VAL_TAB_SHEET_FROM_LIST         -1
#define VAL_TAB_SHEET_FROM_PANEL_HANDLE -2
#define VAL_TAB_SHEET_FROM_FILE         -3
int     CVIFUNC EasyTab_AddSheet(int panel, int easyTabCtrl, char *sheetName, void *module, int row, int numCtrls, ...);



    /*  These functions are exported for the use of the user protection module (ckeztab.obj) only. */
int CVIFUNC EasyTab_SetAttributeFromParmInfo(int panel, int tabCtrl, int attribute, va_list parmInfo);
int CVIFUNC EasyTab_LoadPanelsFromParmInfo(int panel, int tabCtrl, int row, char *fileName, void *module, va_list parmInfo);
int CVIFUNC EasyTab_AddPanelsFromParmInfo(int panel, int tabCtrl, int row, va_list parmInfo);
int CVIFUNC EasyTab_SetTabAttributeFromParmInfo(int panel, int easyTabCtrl, int tabPanelHandle, int attribute, va_list parmInfo);




#ifdef __cplusplus
    }
#endif

#endif /* EASY_TAB_HEADER */
