#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: PressureXducerCalibrationSubScreen - Activated by selecting Atm
// pressure Calibration in the Service Lower Other Screen after user
// press/release the Lower Other screen tab button.
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen is activated by selecting Atm pressure Calibration
// button in the ServiceLowerOtherScreen.  Only one instance of this
// class is created by LowerSubScreenArea.  This class uses the
// batch settings sub-screen interface. Settings changes are registered
// with the valueUpdateHappened() method which activates the ACCEPT
// key allowing the acceptHappened() method to process the change.
// It also defines the activate() and deactivate() methods.
// No Atm pressure calibration testing shall start until the successful 
// Settings transaction message event from Persistent-Objects is received.
//
//---------------------------------------------------------------------
//@ Rationale
// Implement the complete functionality of the Atm pressure Calibration in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The implementation is quite simple.  Upon activating this subscreen
// we immediately query the Service-Mode of the current atm pressure
// value by calling DoTest(), passing the acquired value to Setting-Validation
// subsystem.  With the successful Settings transaction message event we allow
// the operator to adjust the desired atm pressure through setting button.
// When the operator confirms of the adjusted setting, and the successful
// Settings transaction message event is received, we pass this operator
// adjusted setting to Service-Mode to set the atm pressure.  
//
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in serviceLowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/PressureXducerCalibrationSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
// 		Removed SUN prototype code.
//
//  Revision: 006   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 005  By:  hhd	   Date:  24-Feb-1999    DCS Number:  5326
//  Project:  ATC
//  Description:
//    Added buttonUpHappened() method to fix the above DCS.		
//
//  Revision: 004  By:  btray	   Date:  24-Feb-1999    DCS Number:  5309
//  Project:  ATC
//  Description:
//	Changed the X coordinate of the position from where the failure
//	string originates. This is necessary for the correct centering
//	of this string for the different languages.
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  yyy    Date:  01-Oct-97    DR Number: 2410
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "PressureXducerCalibrationSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "PromptStrs.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceLowerScreenSelectArea.hh"
#include "ServiceUpperScreen.hh"

#if defined FAKE_SM
#	include "FakeServiceModeManager.hh"
#   define SmManager FakeServiceModeManager
#else		
#	include "SmManager.hh"
#endif	// FAKE_SM

#include "AdjustPanel.hh"
#include "MathUtilities.hh"
#include "PromptArea.hh"
#include "SettingContextHandle.hh"
#include "Sound.hh"
#include "SubScreenArea.hh"
#include "SmTestId.hh"
#include "ContextSubject.hh"
//@ End-Usage

//@ Code...


// Initialize static constants.
static const Uint16 SETTING_BUTTON_WIDTH_ = 180;
static const Uint16 SETTING_BUTTON_HEIGHT_ = 46;
static const Uint8  BUTTON_BORDER_ = 3;
static const Uint16 SETTING_BUTTON_X_ = 
			( SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH - SETTING_BUTTON_WIDTH_ ) / 2;
static const Uint16 SETTING_BUTTON_Y_ = 
			( SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT - SETTING_BUTTON_HEIGHT_ ) / 2;

static const Int32 NUMERIC_VALUE_CENTER_X_ = 80;
static const Int32 NUMERIC_VALUE_CENTER_Y_ = 26;

static const Int32 ERR_DISPLAY_AREA_X1_ = 115;
static const Int32 ERR_DISPLAY_AREA_Y1_ = 250;
static const Int32 ERR_DISPLAY_ROW_WIDTH_ = 20;
static const Int32 STATUS_LABEL_X_ = 379;
static const Int32 STATUS_LABEL_Y_ = 0;
static const Int32 STATUS_WIDTH_ = 255;
static const Int32 STATUS_HEIGHT_ = 31;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureXducerCalibrationSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the More Settings subscreen.  Passed a pointer to the subscreen
// area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various buttons displayed in the subscreen and adds them
// to the container.  Registers for callbacks from each button and each
// setting.
//
// $[07075] The Atmospheric pressure transducer calibration subscreen shall
// provide a display of ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureXducerCalibrationSubScreen::PressureXducerCalibrationSubScreen(SubScreenArea *pSubScreenArea) :
		BatchSettingsSubScreen(pSubScreenArea),
		titleArea_(MiscStrs::PRESSURE_XDUCER_SETUP_SUBSCREEN_TITLE),
		pressureSettingButton_(SETTING_BUTTON_X_,SETTING_BUTTON_Y_,
				SETTING_BUTTON_WIDTH_,		SETTING_BUTTON_HEIGHT_,
				Button::LIGHT, 				BUTTON_BORDER_,
				Button::ROUND, 				Button::NO_BORDER,
				TextFont::EIGHTEEN,
                NUMERIC_VALUE_CENTER_X_, 	NUMERIC_VALUE_CENTER_Y_,
                MiscStrs::BAROMETRIC_PRESSURE_BUTTON_TITLE_SMALL,
				MiscStrs::MMHG_UNITS_SMALL,
                SettingId::ATM_PRESSURE, 	this,
                NULL),
		currentScreenId_(PRESSURE_CAL_INIT),
		currentTestId_(SmTestId::TEST_NOT_START_ID),
		measuredAdjustedPressure_(0.0),
		guiTestMgr_(this),
		calStatus_(STATUS_LABEL_X_, STATUS_LABEL_Y_,
						STATUS_WIDTH_, STATUS_HEIGHT_,
						MiscStrs::EXH_V_CAL_STATUS_LABEL,
						NULL_STRING_ID),
		errorHappened_(FALSE),
		errorIndex_(0),
		keyAllowedId_(-1),
		userKeyPressedId_(SmPromptId::NULL_ACTION_ID)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::PressureXducerCalibrationSubScreen(SubScreenArea "
													"*pSubScreenArea)");
	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Add the title area
	addDrawable(&titleArea_);

	// Add status line text objects to main container
	addDrawable(&calStatus_);
	
	guiTestMgr_.setupTestResultDataArray();
	guiTestMgr_.setupTestCommandArray();

	setErrorTable_();

	for (int i=0; i<MAX_CONDITION_ID; i++)
	{
		errDisplayArea_[i].setColor(Colors::WHITE);
		errDisplayArea_[i].setY(ERR_DISPLAY_AREA_Y1_+(i*ERR_DISPLAY_ROW_WIDTH_));
		errDisplayArea_[i].setX(ERR_DISPLAY_AREA_X1_);

		errDisplayArea_[i].setShow(FALSE);
		addDrawable(&errDisplayArea_[i]);
	}
													// $[TI1]

	addDrawable(&pressureSettingButton_);
	pressureSettingButton_.setButtonCallback(this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureXducerCalibrationSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys PressureXducerCalibrationSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureXducerCalibrationSubScreen::~PressureXducerCalibrationSubScreen(void)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::~PressureXducerCalibrationSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by BatchSettingsSubScreen when the operator presses the Accept 
// key. We accept the settings and inform GuiApp that we are now waiting 
// on SettingTransaction event.
//---------------------------------------------------------------------
//@ Implementation-Description
// Make the "Accept" sound, clear the Adjust Panel focus, accept the
// adjusted settings, inform the GuiApp that we are waiting for the
// setting transaction event.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::acceptHappened(void)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::acceptHappened(void)");

	// Clear the Adjust Panel focus, make the Accept sound, store the
	// current settings, accept the adjusted settings and deactivate
	// this subscreen.
	Sound::Start(Sound::ACCEPT);
	AdjustPanel::TakeNonPersistentFocus(NULL);

	// Hide the lower screen select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

	// Set Primary prompt to "Please Wait...."
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_PLEASE_WAIT_P);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_LOW, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, NULL_STRING_ID);

	// Accept Service Mode settings
	SettingContextHandle::AcceptBatch();

	GuiApp::SetUpdateCompletedBatchProcessFlag();

												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: executeSettingTransactionHappened
//
//@ Interface-Description
// Called when a successful setting transaction event occured.
// Passed no parameters.  
//---------------------------------------------------------------------
//@ Implementation-Description
// If we were doing a arm pressure read test then we are ready to perform
// setting atm pressure through the setting button.  If we were doing
// setting atm pressure then we are ready to perform atm pressure
// calibration test with the operator accepted atm pressure.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::executeSettingTransactionHappened(void)
{
	CALL_TRACE("PressureXducerCalibrationSubScreenq::executeSettingTransactionHappened(void)");

	const ContextSubject*  pSubject =
								getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

	BoundedValue boundedDatum;
    char valueString[20];

	boundedDatum = pSubject->getSettingValue(SettingId::ATM_PRESSURE);
	measuredAdjustedPressure_ = boundedDatum.value;

	// Stuff the numeric field's value into valueString
	sprintf(valueString, "%.*f", 
			DecimalPlaces(boundedDatum.precision), 
			measuredAdjustedPressure_);

	if (currentScreenId_ == PRESSURE_CAL_SETTING_PRE_SET)
	{										// $[TI2.1]
		layoutScreen_(PRESSURE_CAL_SETUP);
	}
	else if (currentScreenId_ == PRESSURE_CAL_SETUP)
	{										// $[TI2.2]
		layoutScreen_(PRESSURE_CAL_EXECUTING);

		// Let the service mode  manager knows the type of Service mode.
		SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

		// Start the test.
		startTest_(SmTestId::ATMOS_PRESSURE_CAL_ID, FALSE, valueString);
	}										// $[TI2.3]
}


//============================ m e t h o d   d e s c r i p t i o n ====
//@ Method: processTestPrompt_
//
//@ Interface-Description
//  Process the prompt command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Do nothing, ignore all prompts.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::processTestPrompt(Int command)");

	// Do nothing
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//  Process the test result status given by Service Mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dispatch the given command and make the corresponding layoutScreen_
//  call to update the screen display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::processTestResultStatus_"
								"(Int resultStatus Int testResultId)");
	switch (resultStatus)
	{
	case SmStatusId::TEST_END:			// $[TI1]
		GuiApp::SetUpdateCompletedBatchProcessFlag();

		if (currentTestId_ == SmTestId::ATMOS_PRESSURE_READ_ID)
		{								// $[TI1.1]
			if (errorHappened_)
			{							// $[TI1.1.1]
				CLASS_ASSERTION_FAILURE();
			}
			else
			{							// $[TI1.1.2]
				layoutScreen_(PRESSURE_CAL_SETTING_PRE_SET);
			}
		}
		else if (currentTestId_ == SmTestId::ATMOS_PRESSURE_CAL_ID)
		{								// $[TI1.2]
			if (errorHappened_)
			{							// $[TI1.2.1]
				// Update the AtmPresXducerCalStatusFlag.
				GuiApp::SetAtmPresXducerCalStatusFlag(FALSE);
			}
			else
			{							// $[TI1.2.2]
				// Update the AtmPresXducerCalStatusFlag.
				GuiApp::SetAtmPresXducerCalStatusFlag(TRUE);
			}
				
			layoutScreen_(PRESSURE_CAL_SETTING_PRE_SET);
			ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea()->updateVentTestSummaryScreen();
		}
		else
		{
			CLASS_ASSERTION_FAILURE();
		}
		break;

	case SmStatusId::TEST_START:		// $[TI2]
		break;

	case SmStatusId::TEST_ALERT:		// $[TI3]
	case SmStatusId::TEST_FAILURE:		// $[TI4]
		errorHappened_ = TRUE;
		break;

	case SmStatusId::TEST_OPERATOR_EXIT:// $[TI5]
		errorHappened_ = TRUE;
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//  Process test result condition passed by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Display the text for the corresponding result condition on the screen
//  and update the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//  Result condition has to be in the range [0, MAX_CONDITION_ID]
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::processTestResultCondition(Int resultCondition)");

	SAFE_CLASS_ASSERTION((resultCondition >= 0) && (resultCondition <= MAX_CONDITION_ID));

	if (resultCondition > 0)
	{						// $[TI1]
		errDisplayArea_[errorIndex_].setText(conditionText_[resultCondition-1]);
		errDisplayArea_[errorIndex_].setShow(TRUE);
			
		errorIndex_++;
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);
	}						// $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//  Process keyAllowed command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Remember the expected user key response ID.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::processTestKeyAllowed(Int command)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::processTestKeyAllowed(Int command)");

	// Remember the expected user key response ID
	keyAllowedId_ = command;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  Process data given by Service-Mode
//---------------------------------------------------------------------
//@ Implementation-Description
//  Save the measured datum into measuredAdjustedPressure_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::processTestData(Int dataIndex, TestResult *pResult)");

	measuredAdjustedPressure_ = pResult->getTestDataValue();
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the Calibration
// process.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call GuiManager's virtual method to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::adjustPanelRestoreFocusHappened(void)");

	guiTestMgr_.restoreTestPrompt();
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
//  Called by LowerSubScreenArea just before this subscreen is displayed
//  on the screen.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Display the necessary screen for running the ATMOS_PRESSURE_READ_ID
// test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_()");

	//
	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	guiTestMgr_.registerTestResultData();
	guiTestMgr_.registerTestCommands();

	// Let the service mode  manager knows the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

	layoutScreen_(PRESSURE_CAL_UPDATING);
	
	// Start the test.
	startTest_(SmTestId::ATMOS_PRESSURE_READ_ID);
	errorHappened_ = FALSE;
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears the Adjust Panel focus in case we had it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("deactivateHappened_()");

	// deactivate setting button...
	pressureSettingButton_.deactivate();

	// Clear the Primary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);
				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdateHappened_
//
//@ Interface-Description
// Called when a setting has changed.  If this screen is visible then update
// the PRESSURE_XDUCER_CAL_PROPOSED_SUBSCREEN_TITLE.  Passed the following
// parameters:
// >Von
//	settingId	The enum id of the setting which changed.
//	contextId	The context in which the change happened, either
//				ContextId::ACCEPTED_CONTEXT_ID or
//				ContextId::ADJUSTED_CONTEXT_ID.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// We first check if we're visible.  We ignore all setting changes when this
// subscreen is not active.  If this is the first setting change then we
// display the PRESSURE_XDUCER_CAL_PROPOSED_SUBSCREEN_TITLE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier,
					  const ContextSubject*
														)
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");

	if (currentScreenId_ == PRESSURE_CAL_SETTING_PRE_SET)
	{													// $[TI2.1]
		return;
	}													// $[TI2.2]

	// If the setting has been changed and the screen was in the
	// current state then go to the proposed state.
	if (transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED)
	{													// $[TI3.1]
		// Display proposed title
		titleArea_.setTitle(MiscStrs::PRESSURE_XDUCER_CAL_PROPOSED_SUBSCREEN_TITLE);
		calStatus_.setShow(FALSE);
		
		// Change the Secondary prompt to explain the use of the ACCEPT key
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_PROCEED_CANCEL_S);
	}
	// If the setting has been adjusted back to its original value and now
	// all settings are unchanged then reset the screen
	else if (transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT)
	{													// $[TI3.2]
		// Display current title
		titleArea_.setTitle(MiscStrs::PRESSURE_XDUCER_SETUP_SUBSCREEN_TITLE);
		calStatus_.setShow(TRUE);

		// Reset the Secondary prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);
	}													// $[TI3.3]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// Sets the subscreen into one of four display configurations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If screen id is PRESSURE_CAL_UPDATING, we force an atm pressure
//  read from Service mode in addition to display the appropriat screen.
//  If screen id is PRESSURE_CAL_SETTING_PRE_SET, we inform the Setting-Validation
//  to set the ATM_PRESSURE setting ID with the value from atm pressure
//  read test.
//  If screen id is PRESSURE_CAL_SETUP, we are ready to perform setting button
//  setting.
//  If screen id is PRESSURE_CAL_EXECUTING, we perform the atm pressure
//  calibration with the accepted atm pressure setting value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::layoutScreen_(CalScreenId calScreenId)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::layoutScreen_(CalScreenId calScreenId)");

	currentScreenId_ = calScreenId;

	switch (currentScreenId_)
	{
	case PRESSURE_CAL_UPDATING:				// $[TI1]
		// Display current title
		titleArea_.setTitle(MiscStrs::PRESSURE_XDUCER_SETUP_SUBSCREEN_TITLE);

		pressureSettingButton_.setShow(FALSE);

		// Hide the lower screen select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

		// Set all prompts
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_TESTING_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, NULL_STRING_ID); 
		break;

	case PRESSURE_CAL_SETTING_PRE_SET:				// $[TI2]
		calStatus_.setShow(TRUE);

		if (GuiApp::IsAtmPresXducerCalSuccessful())
		{											// $[TI2.1]
			calStatus_.setStatus(MiscStrs::ATM_PRESSURE_CALIBRATED_MSG);
		}
		else
		{											// $[TI2.2]		
			calStatus_.setStatus(MiscStrs::ATM_PRESSURE_NOT_CALIBRATED_MSG);
		}

		// Tells Settings-Validation that we are about to adjust batch settings.
		// Pass the measured pressure on to the Atm pressure Volume setting
		SettingContextHandle::AdjustAtmPressureCal(measuredAdjustedPressure_);

		// Tells Settings-Validation that we are about to accept batch settings.
		SettingContextHandle::AcceptAtmPressureCal();
		break;
		
	case PRESSURE_CAL_SETUP:						// $[TI3]
		// Tells Settings-Validation that we are about to adjust batch settings.
		SettingContextHandle::AdjustBatch();
		setCurrentSettingsFlag_(TRUE);

		// Hide the lower screen select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

		// Display current title
		titleArea_.setTitle(MiscStrs::PRESSURE_XDUCER_SETUP_SUBSCREEN_TITLE);

		// Activate setting buttons
		pressureSettingButton_.activate();

		// Grab focus at this point so the ACCEPT key can be processed
		AdjustPanel::TakeNonPersistentFocus(this, TRUE, FALSE);

		// Clear the Primary prompt
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_HIGH, NULL_STRING_ID);

		// Set Primary prompt to "Select and Adjust settings."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);

		// Set Secondary prompt to tell user to press OTHER SCREENS button to cancel
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_PROCEED_CANCEL_S);
		break;

	case PRESSURE_CAL_EXECUTING:					// $[TI4]
		pressureSettingButton_.setShow(FALSE);
	
		// Make sure that nothing in this subscreen keeps focus.
		AdjustPanel::TakeNonPersistentFocus(NULL);

		// Display current calibration title
		titleArea_.setTitle(MiscStrs::PRESSURE_XDUCER_CAL_SUBSCREEN_TITLE);
		calStatus_.setShow(TRUE);


		// Hide the lower screen select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

		// Set Primary prompt to "Please Wait...."
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_PLEASE_WAIT_P);

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_LOW, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, NULL_STRING_ID);
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setErrorTable_
//
//@ Interface-Description
// 	Sets the error text in the error table.  Any condition that arises
//	has a unique id which is used as an index into this table of condition
//	text.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assign condition text (non-cheap text) to error table.  Different 
//	conditions will arise depending whether it's the Exhalation Calibration
//	or the Calibration Info Duplication process.  Therefore, different
//	sets of error conditions are used according to which test is in progress.
//	We also initialize the error display area's show flag.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::setErrorTable_(void)
{
	Int testIndex = 0;

	CALL_TRACE("PressureXducerCalibrationSubScreen::setErrorTable_(void)");

	// Reset status flag
	errorHappened_ = 0;
	errorIndex_ = 0;

	if (currentTestId_ == SmTestId::ATMOS_PRESSURE_CAL_ID)
	{											// $[TI1]
		conditionText_[testIndex++] = MiscStrs::ATM_PRESSURE_TRANSDUCER_OOR;
	}											// $[TI2]

	// Make sure total test count is within the maximum allowed.
	CLASS_ASSERTION(testIndex <= MAX_CONDITION_ID);

	for (int i=0; i < MAX_CONDITION_ID; i++)
	{											// $[TI3]
		errDisplayArea_[i].setShow(FALSE);
	}											// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//	Preparation step for a test to start.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the error table.  Display prompts and status.
//	Finally, tell Service Mode Manager to do the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId,
				Boolean testRepeat, const char * const pParameters)
{
	CALL_TRACE("PressureXducerCalibrationSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)");

	currentTestId_ = currentTestId;
	
	// Display the overall testing status on the service status area
	calStatus_.setShow(TRUE);

	if (currentTestId_ == SmTestId::ATMOS_PRESSURE_READ_ID)
	{											// $[TI1]
		calStatus_.setStatus(MiscStrs::ATM_PRESSURE_UPDATING_MSG);
	}
	else
	{											// $[TI2]
		calStatus_.setStatus(MiscStrs::EXH_V_CAL_RUNNING_MSG);
	}

	setErrorTable_();

	// Set all prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_TESTING_P);
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, NULL_STRING_ID); 


	// Start the test.
	SmManager::DoTest(currentTestId, testRepeat, pParameters);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when the Pressure Setting button in PressureXducerCalibrationSubScreen 
// is pressed up.  Passed a pointer to the button as well as 'byOperatorAction', 
// a flag which indicates whether the operator pressed the button up or whether
// the setToUp() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the subscreen is invisible, simply return.
// If the Pressure Setting button is pressed up, we update the prompt area
// with the appropriate prompts.  If an unidentified button is pressed up,
// generate a class assertion.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::buttonUpHappened(Button *pButton, Boolean)
{
    CALL_TRACE("buttonUpHappened(pButton, )");

    // Don't do anything if this subscreen isn't displayed
    if (!isVisible())
    {													// $[TI1]
        return;
    }													// $[TI2]

    // Check for one of the timing buttons being pressed up
    if (pButton == &pressureSettingButton_)
    {													// $[TI3]
		// Set Primary prompt to "Select and Adjust settings."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);

		// Set Secondary prompt to tell user to press OTHER SCREENS button to cancel
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_PROCEED_CANCEL_S);
    }													
	 else
    {
        // Unknown button
        CLASS_ASSERTION_FAILURE();			// $[TI4]
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
PressureXducerCalibrationSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
			PRESSUREXDUCERCALIBRATIONSUBSCREEN, lineNumber, pFileName, pPredicate);
}

