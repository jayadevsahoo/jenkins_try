/*****************************************************************************/
/*   Module Name:    sysmenu.h                                     
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.2      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _SYSMENU_H_
#define _SYSMENU_H_

void SetSysMenuMode(int Mode);
void SetTestButtonMode(int state);
void SetSysPanelMessage(char* cbuff);

enum SysPanelStates{
	SYS_ALL_ON=0,
	SYS_ALL_OFF,
	SYS_HELP,
	SYS_SETUP,
	SYS_TEST,
	SYS_PTS,
	SYS_840,
	SYS_REPORT,
	SYS_EXIT
};

enum TestButtonStates{
	TB_NORMAL=0,
	TB_RUNNING,
	TB_DEVICE_NOT_DETECTED,
	TB_RUNNING_SINGLE,
	TB_PAUSED,
	TB_STOPPED
};

#endif // #ifndef _SYSMENU_H_
