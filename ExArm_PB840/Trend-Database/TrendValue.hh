#ifndef TrendValue_HH
#define TrendValue_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation 
//=====================================================================

//=====================================================================
// Class: TrendValue.hh - One Trend data value/point
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendValue.hhv   25.0.4.0   19 Nov 2013 14:34:42   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  mjf    Date:  05-MAR-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
//=====================================================================
//@ Usage-Classes
//@ End-Usage


#include "DataValue.hh"           // Patient-Data
#include "Trend_Database.hh"


// forward delcare

// Type for one Trend value
struct TrendValue
{
	DataValue   data;
	Boolean     isValid;
};


#endif // TrendValue_HH
