#ifndef AlarmBroadcast_HH
#define AlarmBroadcast_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Type: AlarmBroadcast - derived from template class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmBroadcast.hhv   25.0.4.0   19 Nov 2013 13:51:14   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   571
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "TimeDay.hh"
#include "AlarmName.hh"
#include "MessageName.hh"

class AlarmUpdate;
//@ End-Usage

struct AlarmBroadcast
{
  public:

    AlarmBroadcast();
    AlarmBroadcast(const AlarmBroadcast&);
    ~AlarmBroadcast();
    void operator=(const AlarmUpdate& rAlarmUpdate);

	void convHtoN(void);
	void convNtoH(void);

    static void          SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName = NULL,
								   const char*        pPredicate = NULL);

    //@ Data-Member:    whenOccurred
    // When the alarm changed state.
    TimeOfDay           whenOccurred;

    //@ Data-Member:    name
    // Enumerator uniquely identifying an alarm.
    Int32               name;

    //@ Data-Member:    baseMessage
    // This is the enumerator which identifies the text associated with the
    // Alarm Name/Base Message column of the Alarm Matrices.
    Int16               baseMessage;

    //@ Data-Member:    updateType
    // Instance variable used in determining the reason for the reevaluation
    // of an Alarm.
    Int8                updateType;

    //@ Data-Member:    currentUrgency
    // Current Urgency of the alarm.
    Int8                currentUrgency;

    //@ Data-Member:    currentPriority
    // Current priority of the alarm.
    Int16               currentPriority;

    //@ Data-Member:    analysisIndex
    // Attribute analysisIndex tracks the number of analysis messages in the
    // currentAnalysisMessage array.
    Int8                analysisIndex;

    //@ Data-Member:    remedyIndex
    // Attribute remedyIndex tracks the number of remedy messages in the
    // currentRemedyMessage array.
    Int8                remedyIndex;

    //@ Data-Member:    currentAnalysisMessage
    // Array containing the message enumerators for the analysis messages.
    Int16         currentAnalysisMessage[NUM_MESSAGE];

    //@ Data-Member:    currentRemedyMessage
    // Array containing the message enumerators for the remedy messages.
    Int16         currentRemedyMessage[NUM_MESSAGE];

  private:
    //operator=(const AlarmBroadcast&); // Declared only

};


#endif // AlarmBroadcast_HH 
