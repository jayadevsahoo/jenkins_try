#ifndef PavStartupDataPacket_HH
#define PavStartupDataPacket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct : PavStartupDataPacket - structure definition of patient
//   data packet containing PAV data that are only updated when PAV
//   is starting up.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PavStartupDataPacket.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: sah    Date:  12-Mar-2001    DR Number: 5847
//	Project:  PAV
//	Description:
//		Used to send PAV state prior to first PAV breath.
//
//====================================================================

#include "PavState.hh"

// @Type: PavStartupDataPacket
struct   PavStartupDataPacket
{
	PavState::Id  pavState;

	void convHtoN()

	{
		SocketWrap sock;
		pavState = (PavState::Id)sock.HTONL((Uint32)pavState);
	}

	void convNtoH()
	{
		SocketWrap sock;
		pavState = (PavState::Id)sock.NTOHL((Uint32)pavState);
	}
};

#endif // PavStartupDataPacket_HH 
