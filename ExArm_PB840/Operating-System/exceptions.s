;=====================================================================
; This is a proprietary work to which Nellcor Puritan Bennett
; corporation of California claims exclusive right.  No part of this
; work may be used, disclosed, reproduced, stored in an information
; retrieval system, or transmitted by any means, electronic,
; mechanical, photocopying, recording, or otherwise without the prior
; written permission of Nellcor Puritan Bennett Corporation of
; California.
;
;     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
;=====================================================================
;
; =========================== M O D U L E   D E S C R I P T I O N ====
;@ Filename: exceptions.s - Sigma Exception Handlers
;---------------------------------------------------------------------
;@ Interface-Description
;  This module contains the assembly level exception handlers for the
;  840 application.  These handlers process all exceptions for the
;  840 application except for device interrupts and the VRTX trap
;  exception which are handled directly by the device driver and VRTX 
;  operating system.  Exception::InstallHandlers() installs these 
;  handlers during 840 initialization.  When installed, these handlers
;  replace the exception handlers used during POST.  
;
;  This module contains the following exception handler entry points:
;
;    SigmaTrapHandler    - handler for TRAP #1 instructions.
;    SigmaExceptionCatch - handler for unexpected exceptions
;    SigmaFlineCatch     - handler for F-Line Opcode exceptions
;    SigmaLevel7Catch    - handler for level 7 autovector exceptions
;
;---------------------------------------------------------------------
;@ Rationale
;  This module contains the assembly level exception handlers.
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Description
;---------------------------------------------------------------------
;@ Fault-Handling
;  not applicable
;---------------------------------------------------------------------
;@ Restrictions
;  none
;---------------------------------------------------------------------
;@ Invariants
;  not applicable
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version-Information
; @(#) $Header:   /840/Baseline/Operating-System/vcssrc/exceptions.s_v   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
;
;@ Modification-Log
;
;    Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
;       Project:   Sigma   (R8027)
;       Description:
;          Initial version.
; 
;=====================================================================
 
	NAME	exceptions
;
; 840 Exception Handler Entry Points
;
    XDEF    _SigmaTrapHandler
    XDEF    _SigmaExceptionCatch
    XDEF    _SigmaFlineCatch
    XDEF    _SigmaLevel7Catch
;
; 840 Virtual Exception Vector Table
; 
    XDEF    _SigmaVirtualEvt
;
; 840 TRAP Functions
;
    XREF    _InitiateRebootHandler__Fv
    XREF    _SetProcIpl
;
; 840 Exception Processing Functions
;
    XREF    _Decode__9ExceptionSFPC19ExceptionStackFrame
    XREF    _DecodeAutovector7__9ExceptionSFCUc
    XREF    _PowerFail__9ExceptionSFv

;
;  I/O Register Locations
;
IO_REGISTER_1               EQU  $FFBE8000
NMI_SOURCE                  EQU  $FFBE8008

;
;  BD NMI Source Register Bit Assignments
;
POWER_FAIL_BIT              EQU  2


;=======================================================================
;
;  Data Section
;
;=======================================================================

	SECTION zerovars,4,R

_SigmaVirtualEvt:    ds.l   256

;====================== P R O C E D U R E   D E S C R I P T I O N ====
;@ Procedure:  _SigmaTrapHandler
;
;@ Interface-Description
;  This interrupt handler catches the 840 application defined trap
;  exception (TRAP #1).  The 840 application uses this trap to perform
;  functions in supervisor mode that cannot be done in user mode
;  without causing a privilege violation.  Currently, two trap
;  functions are defined for the 840 application: the
;  InitiateRebootHandler() function to block interrupts and cause a
;  processor reset, and the SetProcIpl() function used to set the
;  interrupt processing level.  This trap handler is invoked by
;  executing the TRAP #1 instruction.
;
;  INPUTS:   D0 is set to the desired function code 
;               1 = InitiateRebootHandler()
;               2 = SetProcIpl()
;
;            D1 is the parameter value passed to the function.
;
;  OUTPUTS:  D0 - return code from the processing function
;
;
;  Exception::InstallHandlers() installs this handler in the the EVT
;  during 840 application initialization.
;
;  Note:  Be aware that MRI "C" convention allows D0/D1/A0/A1 to be 
;         modified by functions without saving or restoring these
;         registers.  This trap handler assumes that the invoking
;         procedure is aware of this and has saved these registers
;         prior to the TRAP if they must be preserved.
;  
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Description
;---------------------------------------------------------------------
;@ PreCondition
;
;---------------------------------------------------------------------
;@ PostCondition
;
;@ End-Procedure
;=====================================================================

; interrupt void SigmaTrapHandler()
; {
    SECTION code,4,C

    ALIGN    4
_SigmaTrapHandler:

    cmpi.l   #1,d0                   ; function #1         // $[TI1.1]
    bne.s    notFunc1          
    jsr      _InitiateRebootHandler__Fv
    rte

notFunc1:
    cmpi.l   #2,d0                   ; function #2         // $[TI1.2]
    bne.s    notFunc2          
    jsr      _SetProcIpl
    rte

notFunc2:                            ; others illegal      // $[TI1.3]
    illegal

; }


;====================== P R O C E D U R E   D E S C R I P T I O N ====
;@ Procedure:  SigmaLevel7Catch
;
;@ Interface-Description
;  This interrupt handler catches and processes the Autovector level 7 
;  (NMI) interrupt.  It examines the NMI source register to determine
;  if a power fail is active.  If it is, Exception::PowerFail() is
;  called to process the power failure.  Otherwise, the handler calls
;  Exception::DecodeAutovector7() to decode and log the exception.
;
;  Exception::InstallHandlers() installs this handler in the the EVT
;  during 840 application initialization.
;---------------------------------------------------------------------
;@ Implementation-Description
;
;---------------------------------------------------------------------
;@ PreCondition
;
;---------------------------------------------------------------------
;@ PostCondition
;
;@ End-Procedure
;=====================================================================
 
; interrupt void SigmaLevel7Catch()
; {
    SECTION code,4,C

    ALIGN    4
_SigmaLevel7Catch:
    IFDEF    SIGMA_BD_CPU             ; { 

    btst.b   #POWER_FAIL_BIT,NMI_SOURCE
    beq.s    level7Generic            ; not a power fail interrupt
                                      ;                        // $[TI1.1]
    jsr      _PowerFail__9ExceptionSFv
    illegal                           ; should not return
    ENDC                              ; }

level7Generic:
                                      ;                        // $[TI1.2]
    movem.l  d0/d1/a0/a1,-(sp)        ; save regs not saved by C++

    move.b   NMI_SOURCE,d0
    move.l   d0,-(sp)                 ; nmi source on stack
   
    jsr      _DecodeAutovector7__9ExceptionSFCUc

    IFDEF    SIGMA_PRODUCTION         ; { // SIGMA_PRODUCTION
    illegal                           ; should not return
    ELSEC                             ; } else { 
    addq.l   #4,sp                    ; nmi source

    movem.l  (sp)+,d0/d1/a0/a1

    subq.l   #8,sp                    ; allocate 4 word exception stack
    movem.l  d0/a0,-(sp)
    move.w   $16(sp),d0               ; get original format/vector
    andi.w   #$0fff,d0                ; mask off format nibble
    lea.l    _SigmaVirtualEvt,a0      ; get evt entry prior to sigma init
    move.l   0(a0,d0.w),$a(sp)        ; handler pc -> exception stack
    beq.s    level7NullHandler        ; no xtrace exception handler

    move.w   sr,$8(sp)                ; current sr -> exception stack
    move.w   d0,$e(sp)                ; format 0/vector -> exception stack
    movem.l  (sp)+,d0/a0
    rte                               ; "returns" to vt exception handler
                                      ; which returns from original exception
    
level7NullHandler:                    ; this should never be executed TBD
    movem.l  (sp)+,d0/a0
    addq.w   #8,sp                    ; remove exception stack frame
    rte                               ; return from original exception
    ENDC                              ; } // SIGMA_DEVELOPMENT

; }

 
;====================== P R O C E D U R E   D E S C R I P T I O N ====
;@ Procedure:  _SigmaFlineCatch
;
;@ Interface-Description
;  This procedure handles "Unimplemented F-Line Opcode" exception for
;  840 application.  
;
;  The unimplemented floating-point exception and the F-line illegal
;  instruction share the same vector, this exception handler uses the
;  stack frame format ($0 for $2) to distinguish between the two.
;
;  For unimplemented floating-point instructions, this handler calls
;  HandoffException_() to hand off the exception to the run-time
;  library for software emulation of the unimplemented floating 
;  point instruction.  F-Line illegal instructions are handled as
;  "normal" exceptions using the SigmaExceptionCatch() function.
;
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Description
;---------------------------------------------------------------------
;@ PreCondition
;  none
;---------------------------------------------------------------------
;@ PostCondition
;  none
;@ End-Procedure
;=====================================================================

; interrupt void SigmaFlineCatch()
; {
;
    SECTION code,4,C

    ALIGN    4
_SigmaFlineCatch:
    move.l   d0,-(sp)                 ; save temporary

    move.w   $A(sp),d0                ; format/vector offset
    andi.l   #$f000,d0                ; isolate format
    beq.s    frameFormat0            
                                      ;                   // $[TI1.1]
                                      ; unimp fline opcode
    move.l   (sp)+,d0                 ; restore temporary
    bra.s    HandoffException_

frameFormat0:
                                      ;                   // $[TI1.2]
                                      ; F-Line illegal instruction
    move.l   (sp)+,d0                 ; restore temporary
    bra.s    _SigmaExceptionCatch   ; handle as a "normal" exception
; }

;====================== P R O C E D U R E   D E S C R I P T I O N ====
;@ Procedure:  _SigmaExceptionCatch
;
;@ Interface-Description
;  This procedure handles all "standard" exceptions of interest to the
;  840 application.  This includes all exceptions except for the level
;  7 autovector handled by SigmaLevel7Catch(), TRAP #0 handled by VRTX,
;  the F-Line Opcode handled by SigmaFlineHandler() and TRAP #1 handled
;  by SigmaTrapHandler().  Exception::InstallHandlers() installs this
;  handler and the other handlers in the exception vector table for the
;  appropriate exception vectors.  
;
;  When an exception occurs, the MC68040 generates an exception call to
;  this function.  This function augments the exception stack with the
;  contents of the general purpose registers and control registers,
;  then calls the Exception::Decode() to log the exception in the
;  System Diagnostic Log (NOVRAM).
;
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Description
;---------------------------------------------------------------------
;@ PreCondition
;  none
;---------------------------------------------------------------------
;@ PostCondition
;  none
;@ End-Procedure
;=====================================================================

; interrupt void SigmaExceptionCatch()
; {
    ALIGN    4
_SigmaExceptionCatch:
    movem.l  d0-d7/a0-a7,-(sp)

    move.l   usp,a0
    move.l   a0,-(sp)

    movec.l  sfc,d0
    movec.l  dfc,d1
    movec.l  vbr,d2
    movec.l  cacr,d3
    movec.l  msp,d4
    movec.l  isp,d5
    movem.l  d0-d5,-(sp)              ; save control registers

    movec.l  tc,d0
    movec.l  itt0,d1
    movec.l  itt1,d2
    movec.l  dtt0,d3
    movec.l  dtt1,d4
    movec.l  mmusr,d5
    movec.l  urp,d6
    movec.l  srp,d7
    movem.l  d0-d7,-(sp)              ; save MMU registers

    move.l   sp,-(sp)                 ; the stack frame parameter
    jsr      _Decode__9ExceptionSFPC19ExceptionStackFrame  

    IFDEF    SIGMA_PRODUCTION
    illegal                           ; should not return in production

    ELSEC                             ; { // SIGMA_DEVELOPMENT
;
;  for DEVELOPMENT, the exception decoder returns so we can hand off
;  the exception to the xtrace exception handler so the user can
;  diagnose the exception interactively
;
    addq.l   #4,sp                    ; restore stack
    move.l   sp,d0
    addi.l   #60,d0                   ; skip usp..srp
    move.l   d0,sp
    movem.l  (sp)+,d0-d7/a0-a7        ; restore data/address regs
    bra.s    HandoffException_        ; give the exception to xtrace
    ENDC                              ; } // SIGMA_DEVELOPMENT
                                      ;                       // $[TI1]
; }

;====================== P R O C E D U R E   D E S C R I P T I O N ====
;@ Procedure:  HandoffException_
;
;@ Interface-Description
;  The procedure hands off processing of unimplemented floating-point
;  instructions to the VRTX run-time library by retrieving the vector
;  that was originally stored in the EVT prior to installation of
;  the 840 exception handler, storing this address in a four-word
;  "throw away" stack frame and returning (via RTE) to the RTL
;  exception handler.  
;
;  This function restores all registers prior to the RTE so the RTL
;  exception handler processes the exception as if it were invoked
;  directly.  It is not possible to branch directly to the RTL
;  exception handler without affecting the contents of the registers
;  since at least two registers are required to retrieve the handler's
;  address and branch to it.  Therefore we use the "throw away" stack
;  frame method.
;
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Description
;---------------------------------------------------------------------
;@ PreCondition
;  none
;---------------------------------------------------------------------
;@ PostCondition
;  none
;@ End-Procedure
;=====================================================================

; void HandoffException_()
; {
    ALIGN    4
HandoffException_:
    subq.l   #8,sp                    ; allocate 4 word exception stack
    movem.l  d0/a0,-(sp)
    move.w   $16(sp),d0               ; get original format/vector
    andi.w   #$0fff,d0                ; mask off format nibble
    lea.l    _SigmaVirtualEvt,a0      ; get evt entry prior to sigma ev init
    move.l   0(a0,d0.w),$a(sp)        ; handler pc -> exception stack
    beq.s    exceptionNullHandler     ; no rtl exception handler
                                      ;                       // $[TI1.1]
                                      ; build exception stack for vt 
    move.w   sr,$8(sp)                ; current sr -> exception stack
    move.w   d0,$e(sp)                ; format 0/vector -> exception stack
    movem.l  (sp)+,d0/a0
    rte                               ; "returns" to rtl exception handler
                                      ; which returns from original exception
    
exceptionNullHandler:
                                      ;                       // $[TI1.2]
    movem.l  (sp)+,d0/a0
    addq.w   #8,sp                    ; remove exception stack frame
    rte                               ; return from the original exception
; }

	END
