
#ifndef PEEPDataInfo_HH
#define PEEPDataInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PEEPDataInfo - Manages PEEP intrinsic and PEEP total data 
//                while the breath pause key is down .
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/PEEPDataInfo.hhv   10.7   08/17/07 10:27:58   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  06-Feb-95    DR Number: 
//    Project:  Sigma (R8027)
//    Description:
//        Initial version. 
//
//====================================================================

#include     "OsTimeStamp.hh"
#include     "WaveformData.hh"
#include     "PatientDataClassIds.hh"

class PEEPDataInfo
{
  //@ Friend : PatientDataMgr.
  //  Allows PatientDataMgr class to access private methods.
  friend  class PatientDataMgr;

  public:
    PEEPDataInfo();
    virtual ~PEEPDataInfo(void);                 
    inline   Boolean isPauseKeyPressed();

    static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL);

  private:
    PEEPDataInfo(const PEEPDataInfo&);   // not implemented.
    void operator=(const PEEPDataInfo&); // not implemented.

    void updateData_(WaveformData *pWaveData);
    void startPause_( const Real32 endExpPressure);
    inline   void stopPause_();

   //@ Data-Member: paused_
   // This flag indicates whether the breath pause key is down.
    Boolean    paused_;

  //@ Data-Member: initialPeEnd_
  // Contains an End Expiratory Pressure after the pause key was pressed
  // initially.
    Real32     initialPeEnd_;

  //@ Data-Member updateTime_ 
  // This variable contains the last time when PEEP intrinsic and
  // PEEP total values updated.
    OsTimeStamp  updateTime_;

 //@ Data-Member DELAY_TIME_
 //  Defines a time interval before a new PEEP Data gets computed.
 	static const Uint DELAY_TIME_;
};


// Inlined methods...
#include "PEEPDataInfo.in"

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
#endif // PEEPDataInfo_HH 
