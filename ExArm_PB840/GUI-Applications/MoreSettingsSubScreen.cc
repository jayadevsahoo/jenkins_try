#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MoreSettingsSubScreen - The subscreen selected by the
// More Settings button in the Lower Other Screens subscreen.
// Allows adjusting of the Expiratory Sensitivity, Humidification Type and
// O2 Sensor Enable settings.
//---------------------------------------------------------------------
//@ Interface-Description
// MoreSettingsSubScreen contains any of the various vent settings that do not
// belong in Vent/Apnea/Alarm setup but which should be easily accessible to
// the user while the ventilator is operating.  These are, currently,
// Expiratory Sensitivity, Humidification Type and O2 Sensor Enable.
//
// It is the responsibility of this class to ensure that only currently
// applicable settings are displayed.  Humidification Type and O2 Sensor Enable
// are always applicable but Expiratory Sensitivity is applicable only when the
// breath mode is SIMV or SPONT.
//
// The subscreen contains a setting button for each of the above settings.  If
// a settings is modified a Proceed button is displayed.  This button must be
// pressed down and the Accept key pressed in order to accept and apply the
// settings changes.
//
// The activateHappened()/deactivateHappened() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// acceptHappened() method is called by the Proceed button when the settings
// are being accepted. Timer events (specifically the
// subscreen setting change timeout event) are passed into timerEventHappened()
// and Adjust Panel events are communicated via the various Adjust Panel
// "happened" methods.  Settings events result in a call to
// valueUpdate().  GUI state changes cause stateChangeHappened()
// to be called.
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the More Settings subscreen in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The implementation is quite simple.  Three setting buttons are displayed
// (except in the case where the breath mode is A/C when the Expiratory
// Sensitivity button is hidden).  This subscreen registers as a target of each
// of the three settings.  If any of the settings are modified the subscreen is
// informed (via valueUpdate()) and it will then display the Proceed
// button.
//
// If the Proceed button is pressed down and the Accept key is pressed
// then the Proceed button will call the acceptHappened() method which
// will accept the setting changes and deactivate the subscreen.
//
// The subscreen is also registered as a target of the subscreen inactivity
// timer.  If the timer runs out this is detected in timerEventHappened() and
// the subscreen is reset.
//
// $[01057]
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MoreSettingsSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 018   By: rhj    Date: 28-Apr-2009    SCR Number: 6474
//  Project:  840S2
//  Description:
//	    Moved the applicability algorithm of the Leak Comp Enable 
//      button to the LeakCompEnabledSetting class.
//
//  Revision: 017   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S2
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
// 
//  Revision: 016   By: rpr    Date: 01-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Moved initialization of previousfio2Enabled_ from constructor to 
//		activateHappened_.  Fixes issues when sensor is disabled and vent is turned
// 		off.  On startup sensor is reinitialized to enabled causing previousfio2Enabled_
//		to have the wrong state due to it being initialized too soon.		
// 
//  Revision: 015   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 014   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 013   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//      Modified to support Leak Compensation.
//       
//  Revision: 012   By: sah    Date: 09-Jun-2000     DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  eliminated unneeded include
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//
//  Revision: 011  By:  hhd	   Date:  16-Nov-1999    DCS Number:  5327, 5412
//  Project:  NeoMode
//  Description:
//	Modified as part of a general cleanup effort for Neo-Mode (5327).
//	Modified to add attention icon to Humid. Volume button when Humid. Type changes (5412).	
//
//  Revision: 010  By:  hhd	   Date:  06-May-1999    DCS Number:  5381
//  Project:  ATC
//  Description:
//  	Used TC_MM_UNITS2_SMALL instead.
//
//  Revision: 009  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 008  By: 	btray   Date:  08-Feb-1999    DCS Number: 5316
//  Project:  ATC
//  Description:
// 	Fix for DCS # 5316. Centering for value displayed within disconnect sensitivity button. 
//
//  Revision: 007  By:  hhd	   Date:  03-Feb-1999    DCS Number: 
//  Project:  ATC
//  Description:
//    Fixed bug.
//
//  Revision: 006  By:  hhd	   Date:  02-Feb-1999    DCS Number: 
//  Project:  ATC
//  Description:
//		Modified so Humid. Volume button is displayed on the condition
//		of ATC Option installation.
//		Cleaned up.
//	
//  Revision: 005  By:  hhd	   Date:  28-Jan-1999    DCS Number: 
//  Project:  ATC
//  Description:
//		Modified to fix problem with wrong labels on button, etc.
//
//  Revision: 004  By:  hhd	   Date:  27-Jan-1999    DCS Number: 
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  10-Sep-97    DR Number: 2169
//    Project:  Sigma (R8027)
//    Description:
//      Removed obsolete SRS references.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "MoreSettingsSubScreen.hh"

//@ Usage-Classes
#include "AcceptedContext.hh"
#include "AcceptedContextHandle.hh"
#include "AdjustPanel.hh"
#include "AdjustedContext.hh"
#include "ContextMgr.hh"
#include "Fio2EnabledValue.hh"
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "HumidTypeValue.hh"
#include "Image.hh"
#include "KeyHandlers.hh"
#include "LeakCompEnabledValue.hh"
#include "LowerScreen.hh"
#include "MandTypeValue.hh"
#include "MiscStrs.hh"
#include "PromptArea.hh"
#include "Setting.hh"
#include "SettingContextHandle.hh"
#include "SoftwareOptions.hh"
#include "Sound.hh"
#include "SubScreenArea.hh"
#include "SupportTypeValue.hh"
#include "TubeTypeValue.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_GAP_ = 4;
static const Int32 BUTTON_HEIGHT_ = 46;
static const Int32 BUTTON_WIDTH_ = 162;
static const Int32 BUTTON_BORDER_ = 3;
static const Int32 BUTTON_X_ = 
		( ::LOWER_SUB_SCREEN_AREA_WIDTH - BUTTON_WIDTH_ * 3 
			- BUTTON_GAP_ * 2 ) / 2;
static const Int32 BUTTON_Y1_ = 62;
static const Int32 BUTTON_Y2_ = BUTTON_Y1_ + BUTTON_HEIGHT_ + BUTTON_GAP_;
static const Int32 BUTTON_Y3_ = BUTTON_Y2_ + BUTTON_HEIGHT_ + BUTTON_GAP_;
static const Int32 NUMERIC_VALUE_CENTER_X_ = 68;
static const Int32 NUMERIC_VALUE_CENTER_Y_ = 26;

static Uint  HumidTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		    (sizeof(StringId) * (HumidTypeValue::TOTAL_HUMIDIFIER_TYPES - 1)))];
static Uint  Fio2EnabledInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
	(sizeof(StringId) * (Fio2EnabledValue::TOTAL_FIO2_ENABLED_VALUES - 1)))];
static Uint  TubeTypeInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		    (sizeof(StringId) * (TubeTypeValue::TOTAL_TUBE_TYPE_VALUES - 1)))];

static DiscreteSettingButton::ValueInfo*  PHumidTypeInfo_ =
				(DiscreteSettingButton::ValueInfo*)::HumidTypeInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PFio2EnabledInfo_ =
				(DiscreteSettingButton::ValueInfo*)::Fio2EnabledInfoMemory_;
static DiscreteSettingButton::ValueInfo*  PTubeTypeInfo_ =
				(DiscreteSettingButton::ValueInfo*)::TubeTypeInfoMemory_;

static Uint  LeakCompEnabledInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
	(sizeof(StringId) * (LeakCompEnabledValue::TOTAL_LEAK_COMP_ENABLED_VALUES - 1)))];

static DiscreteSettingButton::ValueInfo*  PLeakCompEnabledInfo_ =
				(DiscreteSettingButton::ValueInfo*)::LeakCompEnabledInfoMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MoreSettingsSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the More Settings subscreen.  Passed a pointer to the subscreen
// area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various buttons displayed in the subscreen and adds them
// to the container.  Registers for callbacks from each button and each
// setting.
//
// $[01034] All of these buttons must be of the select type ...
// $[01055] Multiple setting changes are allowed before accepting ...
// $[01117] Display the following batch setting buttons ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MoreSettingsSubScreen::MoreSettingsSubScreen(SubScreenArea *pSubScreenArea) :
	BatchSettingsSubScreen(pSubScreenArea),
	disconnectionSensitivityButton_(
			BUTTON_X_ + 2 * (BUTTON_WIDTH_ + BUTTON_GAP_),
			BUTTON_Y1_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::DISCONNECTION_SENS_BUTTON_TITLE_SMALL,
			MiscStrs::PERCENT_UNITS_SMALL_MORE_SETTINGS_SUB_SCREEN,
			SettingId::DISCO_SENS, this,
			MiscStrs::DISCONNECTION_SENSITIVITY_HELP_MESSAGE),
	humidificationTypeButton_(
			BUTTON_X_,
			BUTTON_Y1_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER,
			MiscStrs::HUMID_TYPE_BUTTON_TITLE,
			SettingId::HUMID_TYPE, this,
			MiscStrs::HUMID_TYPE_HELP_MESSAGE, 10),
	humidifierVolumeButton_(
			BUTTON_X_,
			BUTTON_Y2_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::HUMID_VOLUME_BUTTON_TITLE_SMALL,
			MiscStrs::TC_ML_UNITS_SMALL,
			SettingId::HUMID_VOLUME, this,
			MiscStrs::HUMID_VOLUME_HELP_MESSAGE),
	o2SensorEnableButton_(
			BUTTON_X_ + 1 * (BUTTON_WIDTH_ + BUTTON_GAP_),
			BUTTON_Y1_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER,
			MiscStrs::O2_SENSOR_BUTTON_TITLE,
			SettingId::FIO2_ENABLED, this,
			MiscStrs::O2_SENSOR_HELP_MESSAGE, 18),
	tubeTypeButton_(
			BUTTON_X_ + BUTTON_WIDTH_ + BUTTON_GAP_,
			BUTTON_Y2_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER,
			MiscStrs::TUBE_TYPE_BUTTON_TITLE2_SMALL,
			SettingId::TUBE_TYPE, this,
			MiscStrs::TUBE_TYPE_HELP_MESSAGE, 18),
	tubeIdButton_(
			BUTTON_X_ + 2 * (BUTTON_WIDTH_ + BUTTON_GAP_),
			BUTTON_Y2_,
			BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
			Button::ROUND, Button::NO_BORDER, TextFont::EIGHTEEN,
			NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
			MiscStrs::TUBE_ID_BUTTON_TITLE2_SMALL,
			MiscStrs::TC_MM_UNITS2_SMALL,
			SettingId::TUBE_ID, this,
			MiscStrs::TUBE_ID_HELP_MESSAGE),
	leakCompEnableButton_(
		    BUTTON_X_ + 2 * (BUTTON_WIDTH_ + BUTTON_GAP_),
			BUTTON_Y3_,
			BUTTON_WIDTH_,
			BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_, Button::ROUND, 
			Button::NO_BORDER, 
			MiscStrs::LEAK_COMP_BUTTON_TITLE,
			SettingId::LEAK_COMP_ENABLED, this,
			MiscStrs::LEAK_COMP_HELP_MESSAGE, 18),
	titleArea_(NULL_STRING_ID, SubScreenTitleArea::SSTA_CENTER),
	verifySettingsIcon_(Image::RCheckArrowIcon),
	verifySettingsMsg_(MiscStrs::HUMID_VOLUME_VERIFY_SETTINGS_MESSAGE)
{
	CALL_TRACE("MoreSettingsSubScreen::MoreSettingsSubScreen(SubScreenArea "
													"*pSubScreenArea)");

	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Add the title area
	addDrawable(&titleArea_);

	// set up values for humidification type....
	::PHumidTypeInfo_->numValues = HumidTypeValue::TOTAL_HUMIDIFIER_TYPES;
	::PHumidTypeInfo_->arrValues[HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER] =
							MiscStrs::HUMID_TYPE_NON_HEATED_VALUE;
	::PHumidTypeInfo_->arrValues[HumidTypeValue::HEATED_TUBING_HUMIDIFIER] =
							MiscStrs::HUMID_TYPE_HEATED_TUBING_VALUE;
	::PHumidTypeInfo_->arrValues[HumidTypeValue::HME_HUMIDIFIER] =
							MiscStrs::HUMID_TYPE_HME_VALUE;
	humidificationTypeButton_.setValueInfo(::PHumidTypeInfo_);


	// set up values for FiO2 Disable/Enable....
	::PFio2EnabledInfo_->numValues = Fio2EnabledValue::TOTAL_FIO2_ENABLED_VALUES;
	::PFio2EnabledInfo_->arrValues[Fio2EnabledValue::NO_FIO2_ENABLED] =
							MiscStrs::O2_SENSOR_DISABLED_VALUE;
	::PFio2EnabledInfo_->arrValues[Fio2EnabledValue::YES_FIO2_ENABLED] =
							MiscStrs::O2_SENSOR_ENABLED_VALUE;
	::PFio2EnabledInfo_->arrValues[Fio2EnabledValue::CALIBRATE_FIO2] =
							MiscStrs::O2_SENSOR_CALIBRATION_VALUE;
	o2SensorEnableButton_.setValueInfo(::PFio2EnabledInfo_);


	// set up values for tube type....
	::PTubeTypeInfo_->numValues = TubeTypeValue::TOTAL_TUBE_TYPE_VALUES;
	::PTubeTypeInfo_->arrValues[TubeTypeValue::ET_TUBE_TYPE] =
							MiscStrs::TUBE_TYPE_ET_VALUE;
	::PTubeTypeInfo_->arrValues[TubeTypeValue::TRACH_TUBE_TYPE] =
							MiscStrs::TUBE_TYPE_TRACH_VALUE;
	tubeTypeButton_.setValueInfo(::PTubeTypeInfo_);

	// set up values for Leak Comp Disable/Enable....
	::PLeakCompEnabledInfo_->numValues = LeakCompEnabledValue::TOTAL_LEAK_COMP_ENABLED_VALUES;
	::PLeakCompEnabledInfo_->arrValues[LeakCompEnabledValue::LEAK_COMP_DISABLED] =
							MiscStrs::LEAK_COMP_DISABLE_VALUE;
	::PLeakCompEnabledInfo_->arrValues[LeakCompEnabledValue::LEAK_COMP_ENABLED] =
							MiscStrs::LEAK_COMP_ENABLE_VALUE;
	leakCompEnableButton_.setValueInfo(::PLeakCompEnabledInfo_);
	leakCompEnableButton_.setShowViewable(TRUE);

	// Setting the image, position, color of the VerifySettings icon.
	// This icon is an arrow shown near the bottom of the screen, with some text
	// illustrating the significance of the attention icon.
  	verifySettingsIcon_.setX(BUTTON_X_ + 4);
    verifySettingsIcon_.setY(BUTTON_Y2_ + BUTTON_HEIGHT_ + BUTTON_GAP_ + 2);
    verifySettingsIcon_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
    addDrawable(&verifySettingsIcon_);

	// Setting the text, position, color of the VerifySettings message.
	// This shows an arrow near the bottom of the lower screen, with some text
	// illustrating the significance of the attention icon.
    verifySettingsMsg_.setX(verifySettingsIcon_.getX() + 22);
    verifySettingsMsg_.setY(verifySettingsIcon_.getY() - 2);
    verifySettingsMsg_.setColor(Colors::WHITE);
    addDrawable(&verifySettingsMsg_);

	humidifierVolumeButton_.setButtonCallback(this);
	humidifierVolumeButton_.setVerifyNeeded(TRUE);

	Uint  idx;

	idx = 0u;
	arrSettingButtonPtrs_[idx++] = &humidificationTypeButton_;
	arrSettingButtonPtrs_[idx++] = &o2SensorEnableButton_;
	arrSettingButtonPtrs_[idx++] = &disconnectionSensitivityButton_;
	arrSettingButtonPtrs_[idx++] = &tubeTypeButton_;
	arrSettingButtonPtrs_[idx++] = &tubeIdButton_;
	arrSettingButtonPtrs_[idx++] = &humidifierVolumeButton_;
	arrSettingButtonPtrs_[idx++] = &leakCompEnableButton_;
	arrSettingButtonPtrs_[idx]   = NULL;

	AUX_CLASS_ASSERTION((idx <= MORE_SETTINGS_MAX_BUTTONS_), idx);

	// Add the buttons to the subscreen
	for (idx = 0u; arrSettingButtonPtrs_[idx] != NULL; idx++ )
	{
		addDrawable(arrSettingButtonPtrs_[idx]);
	}

	previousfio2Enabled_ = Fio2EnabledValue::YES_FIO2_ENABLED;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MoreSettingsSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys MoreSettingsSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MoreSettingsSubScreen::~MoreSettingsSubScreen(void)
{
	CALL_TRACE("MoreSettingsSubScreen::~MoreSettingsSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when certain buttons on the More Settings Sub-Screen are 
// pressed down.
// Passed a pointer to the button as well as 'isByOperatorAction', 
// a flag that indicates whether the operator pressed the button down 
// or the setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the humidification volume button is no longer in the verification
// state then stop flashing of the verification arrow icon.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreSettingsSubScreen::buttonDownHappened(Button *pButton,
										  Boolean isByOperatorAction)
{
	if ( pButton == &humidifierVolumeButton_ )
	{
		if ( !humidifierVolumeButton_.isInVerifyState() )
		{
			verifySettingsIcon_.setColor(Colors::BLACK);
		}
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(Uint(pButton));
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by the Proceed button when the operator presses the Accept key
// when the Proceed button is pressed down.  We accept the settings and
// display this screen with the new settings displayed as the current
// settings.
//---------------------------------------------------------------------
//@ Implementation-Description
// Make the "Accept" sound, clear the Adjust Panel focus, accept the
// adjusted settings, deactivate this subscreen and inform the Lower
// screen which Apnea settings, if any, were corrected as a result
// of the user's settings changes.
//
// $[01056] When batch settings are being setup ventilator settings
//			will not be affected
// $[01118] User must select Proceed and press Accept key to accept settings.
// $[01257] The Accept key shall be the ultimate confirmation step for all ...
// $[01258] The GUI shall accept all adjusted settings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreSettingsSubScreen::acceptHappened(void)
{
	CALL_TRACE("MoreSettingsSubScreen::acceptHappened(void)");

	if ( areSettingsCurrent_() )
	{													// $[TI1.1]
		Sound::Start(Sound::INVALID_ENTRY);
	}
	else
	{													// $[TI1.2]
		// Clear the Adjust Panel focus, make the Accept sound, store the
		// current settings, accept the adjusted settings and deactivate
		// this subscreen.
		Sound::Start(Sound::ACCEPT);

		DiscreteValue fio2Enabled =
			SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
												  SettingId::FIO2_ENABLED);

		AdjustedContext&  rAdjustedContext = ContextMgr::GetAdjustedContext();

		fio2Enabled = rAdjustedContext.getDiscreteValue(SettingId::FIO2_ENABLED); 
		// if calibration selected 
		if (Fio2EnabledValue::CALIBRATE_FIO2 == fio2Enabled)
		{
			// change the setting to enabled
			AdjustedContext&  rAdjustedContext = 
				ContextMgr::GetAdjustedContext();
			rAdjustedContext.setDiscreteValue(SettingId::FIO2_ENABLED, 
												previousfio2Enabled_); 
			GuiApp::RKeyHandlers.getHundredPercentO2Handler().calibrateO2PressHappened();
		}
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);
	
		ApneaCorrectionStatus apneaCorrectionStatus;
		apneaCorrectionStatus = SettingContextHandle::AcceptBatch();
		activate();			// Reactive this subscreen

		previousfio2Enabled_ = SettingContextHandle::GetSettingValue(
				ContextId::ACCEPTED_CONTEXT_ID,
				SettingId::FIO2_ENABLED);

		//
		// The settings subsystem may have auto-adjusted some apnea
		// parameters upon acceptance of the settings above.  Pass the
		// correction status to the Lower Screen so that it can decide
		// whether or not to auto-launch the apnea setup screen.
		//
		LowerScreen::RLowerScreen.reviewApneaSetup(apneaCorrectionStatus);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when a timer that we started runs out, in this case the subscreen
// setting change timer.  We are passed an enum id identifying the timer.
// We must reset the subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// We reset the subscreen by deactivating and reactivating it.  We also
// clear the Adjust Panel focus.
//
// We only react to timers if this subscreen is visible.  There is a small
// chance that this timer event might arrive just as we are being deactivated,
// in which case we ignore it.
// $[01318] If the MoreSettings subscreen is displayed and the user has not ...
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the subscreen setting change timout.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreSettingsSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("MoreSettingsSubScreen::timerEventHappened("
										"GuiTimerId::GuiTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId ==
								GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT);

	if (isVisible())
	{													// $[TI1]
		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Reactivate this sub-screen
		deactivate();
		activate();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
MoreSettingsSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
			MORESETTINGSSUBSCREEN, lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Starts the subscreen setting change timer.  Tells the Settings-Validation
// subsystem to begin adjusting settings.  Calls the activate() method
// of each setting button in order to sync their displays with the current
// setting values.  Displays some prompts to guide the operator.
//
// $[01119] Expiratory sensitivity button visible only when ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreSettingsSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_(void)");

	// Begin adjusting settings
	SettingContextHandle::AdjustBatch();

	titleArea_.setTitle(MiscStrs::CURRENT_MORE_SETTINGS_TITLE);

	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);

	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);

	// Activate all setting buttons...
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::activate);

	verifySettingsIcon_.setShow(FALSE);
	verifySettingsMsg_.setShow(FALSE);

	const DiscreteValue LEAK_COMP_ENABLED_VALUE = 
		AcceptedContextHandle::GetDiscreteValue(SettingId::LEAK_COMP_ENABLED);

	if (LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED)
	{
	     disconnectionSensitivityButton_.setUnitsText(MiscStrs::DISCO_L_PER_MIN_UNITS_SMALL);
		 disconnectionSensitivityButton_.setMessageId(MiscStrs::DISCONNECTION_SENSITIVITY_L_PER_MIN_HELP_MESSAGE);
	}
	else
	{
		 disconnectionSensitivityButton_.setUnitsText(MiscStrs::PERCENT_UNITS_SMALL_MORE_SETTINGS_SUB_SCREEN);
		 disconnectionSensitivityButton_.setMessageId(MiscStrs::DISCONNECTION_SENSITIVITY_HELP_MESSAGE);
	}



	// Save the current FIO2 enabled setting.
	previousfio2Enabled_ =  SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::FIO2_ENABLED);

}								// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed and clears the Adjust Panel
// focus in case we had it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreSettingsSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("MoreSettingsSubScreen::deactivateHappened_()");

	// Deactivate all buttons
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::deactivate);
}			// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  valueUpdateHappened_
//
//@ Interface-Description
//  Called when a settings value is updated.  Passed in a transition id,
//  a qualifier id, and pointer to the ContextSubject.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Change the screen title and the secondary prompt according to whether
//  change is from the current value to the proposed value or from the 
//  proprosed value back to the original value.  Finally, a message 
//  regarding disabling of O2 Sensor is displayed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreSettingsSubScreen::valueUpdateHappened_(
							  const TransitionId_                 transitionId,
							  const Notification::ChangeQualifier qualifierId,
							  const ContextSubject*               pSubject
						)
{
    CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");


	if (transitionId == BatchSettingsSubScreen::CURRENT_TO_PROPOSED)
	{
													// $[TI1]
		// Display proposed title
		titleArea_.setTitle(MiscStrs::PROPOSED_MORE_SETTINGS_TITLE);

		// Change the Secondary prompt to explain the use of the Proceed
		// button
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_PROCEED_CANCEL_S);
	}
	else if (transitionId == BatchSettingsSubScreen::PROPOSED_TO_CURRENT)
	{													// $[TI2]
		// Display current title
		titleArea_.setTitle(MiscStrs::CURRENT_MORE_SETTINGS_TITLE);

		// Reset the Secondary prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);
	}													// $[TI3]


    const DiscreteValue LEAK_COMP_ENABLED_VALUE =
		  SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
    											SettingId::LEAK_COMP_ENABLED);

	// When Leak Comp is enabled, display disconnect sensitivity's unit as
	// L/min. If it is disabled, display disconnect sensitivity's unit as
	// a percent.
	if (LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED)
	{
		 disconnectionSensitivityButton_.setUnitsText(MiscStrs::DISCO_L_PER_MIN_UNITS_SMALL);
		 disconnectionSensitivityButton_.setMessageId(MiscStrs::DISCONNECTION_SENSITIVITY_L_PER_MIN_HELP_MESSAGE);
	}
	else
	{
		 disconnectionSensitivityButton_.setUnitsText(MiscStrs::PERCENT_UNITS_SMALL_MORE_SETTINGS_SUB_SCREEN);
		 disconnectionSensitivityButton_.setMessageId(MiscStrs::DISCONNECTION_SENSITIVITY_HELP_MESSAGE);
	}
	
	if (SettingsMgr::GetSettingPtr(SettingId::HUMID_VOLUME)->isChanged()
		&& SettingsMgr::GetSettingPtr(SettingId::HUMID_TYPE)->isChanged())
	{
		verifySettingsIcon_.setShow(TRUE);
		verifySettingsMsg_.setShow(TRUE);
		if (humidifierVolumeButton_.isInVerifyState())
		{
			verifySettingsIcon_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
		}
		else
		{
			verifySettingsIcon_.setColor(Colors::BLACK);
		}
	}
	else
	{
		verifySettingsIcon_.setShow(FALSE);
		verifySettingsMsg_.setShow(FALSE);
	}
}  
