#include "stdafx.h"
////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		SWATAgent.cpp

Purpose:
	This file is implementation file of SWATAgent class.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 23-JUL-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version. 
	
END OF REVISIONS

*******************************************************************************/

// for function pow
#include <math.h>

#include "SWATAgent.h"
#include "Sigma.hh"
#include "ScreenCaptureMgr.h"
#include "UserAnnunciationMsg.hh"
#include "KeyPanel.hh"
#include "IpcIds.hh"
#include "MsgQueue.hh"

static Uint SWATAgentMem[(sizeof(SWATAgent)+sizeof(Uint) -1)/sizeof(Uint)];
static Uint ScreenCaptureMgrMem[(sizeof(ScreenCaptureMgr)+sizeof(Uint) -1)/sizeof(Uint)];

// initialize the only one object to NULL ptr
SWATAgent* SWATAgent::instance_ = NULL;

SWATAgent::SWATAgent()
{
	new (ScreenCaptureMgrMem) ScreenCaptureMgr();
    screenCaptureMgr_ = (ScreenCaptureMgr*)ScreenCaptureMgrMem;
    screenCaptureMgr_->initialize();
}

void SWATAgent::Initialize()
{
    // call once to initialize
    GetService();
}

ISWATService* SWATAgent::GetService()
{
	// singleton object
	if ( NULL == instance_ )
    {
        instance_ = (SWATAgent*)SWATAgentMem;
        new (instance_) SWATAgent();
    }

	return dynamic_cast<ISWATService*>(instance_);
}

// Push button at location [x,y], no effect if already pushed
SWATStatus SWATAgent::SimulateTouch(int x, int y)
{
#if defined (WIN32) || defined(_WIN32_WCE)

#if defined (WIN32)
	// Get the Main Frame window
	HWND hMainFrameWnd = AfxGetApp()->m_pMainWnd->GetSafeHwnd();

	// Get the client area of Main Frame window
	RECT	clientRect;
	GetClientRect( hMainFrameWnd, &clientRect );
	
	// Convert the LeftTop point to Screen Coordinate
	POINT ptClientLeftTopScrn = { clientRect.left, clientRect.top };
	ClientToScreen( hMainFrameWnd, &ptClientLeftTopScrn );
	
	int offset = 2;

	// Convert the screen
	x += (ptClientLeftTopScrn.x + offset);
	y += (ptClientLeftTopScrn.y + offset);

#endif

	// Send mouse event to x,y
	SetCursorPos(x, y);
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);

    return SWAT_SUCCESS;
#elif
    
    return SWAT_SERVER_NOT_IMPLEMENTED;

#endif
}

// Simulate a push/release on a bezel key
SWATStatus SWATAgent::SimulateBezelKey(int keyId, bool onOff)
{
	// Make sure the keyId is defined in PB880
	if ( keyId >= 0 && keyId < KeyPanel::KeyId::NUMBER_OF_KEYS )
	{
		// convert the on/off to UserAnnunciationMsg::KeyAction type
		UserAnnunciationMsg::KeyAction keyAction = onOff ?
			UserAnnunciationMsg::KEY_PRESS : UserAnnunciationMsg::KEY_RELEASE;

		UserAnnunciationMsg keyMsg;								// appl message
		MsgQueue	userInputQueue(USER_ANNUNCIATION_Q);		// appl queue

		keyMsg.qWord = 0;
		keyMsg.keyParts.eventType = UserAnnunciationMsg::INPUT_FROM_KEY;
		keyMsg.keyParts.action  = keyAction;
		keyMsg.keyParts.keyCode = keyId;

		userInputQueue.putMsg((Uint32)keyMsg.qWord);

		return SWAT_SUCCESS;
	}
    
	return SWAT_ERROR;
}

/*
  SetKnobDelta is defined in GUI-IO-Devices\KnobTask.cc, no declaration for
  it, I declare it here as `extern`to let compiler link to it directly.
    */
extern void SetKnobDelta( Int16 delta );


SWATStatus SWATAgent::SimulateKnob(int clicks)
{
    SetKnobDelta( clicks );

    return SWAT_SUCCESS;
}
  
SWATStatus SWATAgent::CaptureScreen(const char* fileName)
{
    std::string sFileName = fileName;

    // windows file name does not care about the UPPER,lower case,
    // but ScreenCaptureMgr is case sensitive, we convert the file name
    // to lower case on Windows/WCE system.
#if defined (WIN32) || defined(_WIN32_WCE)

    std::transform( sFileName.begin(), sFileName.end(),
                                            sFileName.begin(), tolower );

#endif

	// initiate to capture whole screen by default
	int x		= 0;
	int y		= 0;
	int width	= 0;
	int height	= 0;

#if defined ( WIN32 )
	// Get the Main Frame window
	HWND hMainFrameWnd = AfxGetApp()->m_pMainWnd->GetSafeHwnd();

	// Get the client area of Main Frame window
	RECT	clientRect;
	GetClientRect( hMainFrameWnd, &clientRect );
	
	// Convert the LeftTop point to Screen Coordinate
	POINT ptClientLeftTopScrn = { clientRect.left, clientRect.top };
	ClientToScreen( hMainFrameWnd, &ptClientLeftTopScrn );

	int offset		 = 2;
	int	spliterWidth = 7;

	x		=	ptClientLeftTopScrn.x + offset;
	y		=	ptClientLeftTopScrn.y + offset;
	width	=	640;
	height	=	480 * 2 + spliterWidth;

#endif

    TCHAR wFileName[MAX_PATH];
    memset( wFileName, 0, sizeof(wFileName) );
    size_t convertedSize = mbstowcs( wFileName, sFileName.c_str(), sizeof(wFileName) );
    if ( convertedSize == (size_t)-1 )
    {
        return SWAT_ERROR;
    }

    return screenCaptureMgr_->saveScreenToFile(x, y, width, height, wFileName);
}

SWATStatus SWATAgent::QueryScreenshotQueueSize(int& queueSize)
{
    return screenCaptureMgr_->getBufferSize( queueSize );
}

SWATStatus SWATAgent::ClearScreenshots()
{
    bool clearOK = screenCaptureMgr_->clear();

    return clearOK ? SWAT_SUCCESS : SWAT_SS_CLEAR_FAILED;
}

SWATStatus SWATAgent::QueryScreenshotPath(int index, char* scPath)
{
    TCHAR fileName[MAX_PATH];
    memset( fileName, 0, sizeof(fileName) );

    int nStatus = screenCaptureMgr_->queryScreenshotFileName( index, fileName );
    
    // the index out of range
    if ( -1 == nStatus )
    {
        return SWAT_SS_INDEX_OUT_OF_RANGE;
    }

    char fileNameA[MAX_PATH];
    memset( fileNameA, 0, sizeof(fileNameA) );
    size_t size = wcstombs( fileNameA, fileName, sizeof(fileNameA) );
    if ( size == (size_t)-1 )
    {
        return SWAT_ERROR;
    }

    strcpy( scPath, fileNameA );

    return SWAT_SUCCESS;
}
