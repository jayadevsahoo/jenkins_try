//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ScrollableLog - Generic scrollable log class which allows
// the viewing of information in a tabular form.
//---------------------------------------------------------------------
//@ Interface-Description
// A ScrollableLog is used to display a log of information.  The information is
// presented in tabular form, as a table with a set number of rows and columns.
// A scrollbar is provided because, typically, the number of entries in the log
// is often more than the number of rows than can be displayed in the table.
// All scrolling is handled by ScrollableLog, not by its creator.  All the
// creator must provide is a method by which the ScrollableLog can retrieve the
// contents of any "cell" in the table.
//
// On construction the ScrollableLog class is told how many rows and columns
// will be in the table that it displays. It is also passed a pointer to a
// LogTarget object.  This object is usually the object which creates the
// ScrollableLog.  It is through this LogTarget pointer that this class
// retrieves the textual content of any cell in the log's table.  The textual
// content can be specified in a number of different ways, either as specific
// cheap text or generic text that is auto-formatted within ScrollableLog, thus
// relieving the application of some of the burden of text formatting.
//
// To begin displaying a scrollable log the minimum that must be done is:
// construct the ScrollableLog, call setColumnInfo() once for each column of
// the ScrollableLog, call setEntryInfo() to specify the number of entries in
// the log and call activate() before it will be displayed.
//
// Entries in the log can be selected by having optional select buttons appear
// at the left of the first column.  Only one displayed row can be selected at
// any one time.  Selection is indicated by the row being highlighted.  The
// application has control over the selected row by calling setSelectedEntry()
// and clearSelectedEntry().  Only entries that are displayed onscreen can be
// selected.  Changes by the user to the selected entry are communicated to the
// application via the LogTarget pointer's currentLogEntryChangeHappened()
// method.  The user can be given the ability to select entries via
// setUserSelectable().  The default is that ScrollableLog is not user
// selectable.
//
// If the log is scrolled by the user ScrollableLog automatically retrieves the
// contents of any newly displayed rows by calling LogTarget's
// getLogEntryColumn() method.  If the application needs to communicate changes
// in the log's contents to ScrollableLog then it can call any one of
// updateEntry(), updateColumn() or updateEntryColumn().  ScrollableLog can
// then retrieve the contents of any updated cells by again calling
// getLogEntryColumn().
//
// The application can choose whether the log has a scrollbar or, if the log
// has a scrollbar, whether to allow or disallow scrolling by the user.  On
// construction of ScrollableLog the last parameter determines whether the log
// will display a scrollbar.  If the application needs to display tabular
// information but does not need scrolling ability then the last construction
// parameter should be set to FALSE.  When the application does need scrolling
// then often the application may need to temporarily prevent the user from
// scrolling.  It can do this via setUserScrollable().  When passed a FALSE
// parameter both scroll buttons on the scrollbar enter the flat state, thereby
// disallowing user interaction.  See scrollHappened() for more info.
//
// Because of the large amount of memory needed to create a ScrollableLog
// object (c. 150K) other objects should avoid creating private instances
// of this class and instead use the New() method.  This method constructs
// a ScrollableLog in a shared memory area and returns a pointer to that
// area.  Care should be taken by objects to clear all references to this
// pointer once the object is finished displaying the ScrollableLog due
// to the fact that another object may want to use the shared ScrollableLog.
//---------------------------------------------------------------------
//@ Rationale
// Scrollable logs are used in numerous screens so it made sense to create a
// generic ScrollableLog class to handle most/all of the scrolling
// functionality.
//---------------------------------------------------------------------
//@ Implementation-Description
// The basic building block of ScrollableLog is the LogCell class.  A LogCell
// object is used for display of each "cell" in the log, and each LogCell
// object has auto-formatting text features to make log entry display easier.
//
// A Scrollbar object is used to handle scrolling.  ScrollableLog inherits from
// ScrollbarTarget and any user interactions on the scrollbar generate calls to
// the inherited scrollHappened() method.
//
// An array of Buttons is used to handle the selection/deselection of log
// entries.  ScrollableLog is a ButtonTarget of each button and reacts to the
// user selecting/deselecting a button via the buttonDownHappened() and
// buttonUpHappened() methods.
//
// ScrollableLog is passed a LogTarget pointer on construction.  It is from
// this pointer that it retrieves the contents of each cell via the
// getLogEntryColumn().  When the user selects a new entry or deselects the
// current entry the LogTarget method currentLogEntryChangeHappened() is
// called.
//
// The most complicated implementation detail deals with making ScrollableLog
// do single row scrolling as fast as possible.  The simple, but inefficient,
// way to do it is to refresh the contents of all LogCells when the scroll
// occurs.  A faster method is to move the position of all LogCells by one row
// and then all that is needed is to refresh the contents of the row that has
// newly appeared.  This is the method that is used here.
//
// To begin with, all LogCells with a row index of 0 are displayed on row 0 of
// ScrollableLog.  If a single line scroll occurs this changes.  All LogCells
// with a row index of 0 will now be displayed either on row 1 or on the last
// row of ScrollableLog (depending on the direction of the scroll).  Similarly
// for all rows of LogCells, e.g. the last row of LogCells will now either be
// displayed on the previous row or on row 0.  This complicates the coding
// due to the necessity to continually convert between a row number of
// ScrollableLog and the row index of the LogCells which render that row.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// None of the construction parameters can subsequently be changed after
// construction e.g. you cannot change the number of rows displayed by
// ScrollableLog after construction.  ScrollableLog can currently handle
// up to a maximum of 12 rows and 6 columns.  This can be extended later if
// needed.
//
// ScrollableLog is very memory hungry (see Description section above) and
// private instances of this object should be avoided.  Instead the New()
// method should be called to construct a shared ScrollableLog object.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollableLog.ccv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 004  By:  gdc	   Date:  09-Nov-1998    DCS Number: 5265
//  Project:  BiLevel 840
//  Description:
//		Fixed problem in updateSelectButtons_() caused by previous
//		modification to use setShow() to show or not show the select
//		buttons. The setShow() that replaced addDrawable() did not
//		account for the number of entries in the log.
//
//  Revision: 003  By:  gdc	   Date:  23-Oct-1998    DCS Number: 5224
//  Project:  BiLevel 840
//  Description:
//		Modified to allow log to be visible prior to activation. Using
//		setShow() instead of addDrawable() in updateSelectButtons_() 
//		so the select buttons are only added to the drawable list in
//		the activate() method. This allows the activate() method to 
//		control the display sequence of the drawables. This corrects
//		a problem where the select buttons disappeared because they
//		were added to the beginning of the drawable list via the 
//		updateSelectButtons_() method instead of to the end where they
//		would be displayed on top of the preceeding drawables.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  06-JUN-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ScrollableLog.hh"

//@ Usage-Classes
//@ End-Usage

#  if defined(SIGMA_DEVELOPMENT)
     // for development only, locate this module's static data into
     // development unit's extended memory...
#    pragma options -NZstacks
#  endif //defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//		Static Data...
//
//=====================================================================

static Uint ScrollableLogMemory_[(sizeof (ScrollableLog) + sizeof (Uint) - 1)
															/ sizeof (Uint)];

// Initialize static constants.
static const Int32 TITLE_HEIGHT_ = 17;
static const Int32 TITLE_SEPARATOR_HEIGHT_ = 2;
static const Int32 LOG_Y_OFFSET_ = TITLE_HEIGHT_ + TITLE_SEPARATOR_HEIGHT_;
static const Int32 BUTTON_BORDER_ = 2;
static const Int32 BUTTON_MAX_HEIGHT_ = 35;
static const Int32 BUTTON_MAX_WIDTH_ = 35;
static const Int32 BUTTON_GAP_ = 3;
static const Int32 MIN_ROW_HEIGHT_ = 2;
static const Int32 MIN_COLUMN_WIDTH_ = 2;

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScrollableLog()  [Constructor]
//
//@ Interface-Description
// Constructs a ScrollableLog.  Passed the following parameters:
// >Von
//	pTarget				A LogTarget pointer through which cell contents are
//						retrieved and user select events are communicated.
//	numDisplayRows		Number of rows displayed by the log.
//	numDisplayColumns	Number of columns displayed by the log.
//	rowHeight			The height in pixels of a row (includes the one pixel
//						of the horizontal dividing line between rows).
//	displayScrollbar	Pass FALSE if you do not want ScrollableLog to have
//						a scrollbar.  Defaults to TRUE.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize all data members.  Setup the selectRowButtons_[] array to
// point to each select button.  Register for callbacks to each button.
// Setup the colors of drawables that need it.
//---------------------------------------------------------------------
//@ PreCondition
// The pTarget parameter must be non-null.  The number of rows and columns
// must be within the maximum limits.  The row height must be 2 or greater.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScrollableLog::ScrollableLog(LogTarget *pTarget, Uint16 numDisplayRows,
						Uint16 numDisplayColumns, Uint16 rowHeight,
						Boolean displayScrollbar) :
	pTarget_(pTarget),
	titleSeparator_(0, TITLE_HEIGHT_, 1, TITLE_SEPARATOR_HEIGHT_),
	scrollbar_(rowHeight * numDisplayRows - 1, numDisplayRows, this),
	NUM_DISPLAY_ROWS_(numDisplayRows),
	NUM_DISPLAY_COLUMNS_(numDisplayColumns),
	ROW_HEIGHT_(rowHeight),
	firstDisplayedEntry_(0),
	numEntries_(0),
	selectedEntry_(-1),
	firstCellRow_(0),
	displayScrollbar_(displayScrollbar),
	isUserScrollable_(TRUE),
	isUserSelectable_(FALSE),
	selectRow0Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow1Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow2Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow3Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow4Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow5Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow6Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow7Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow8Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow9Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow10Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER),
	selectRow11Button_(2, 2, BUTTON_MAX_WIDTH_, BUTTON_MAX_HEIGHT_,
				Button::LIGHT, BUTTON_BORDER_, Button::ROUND,
				Button::NO_BORDER)
{
	CALL_TRACE("ScrollableLog::ScrollableLog(void)");

	CLASS_PRE_CONDITION(pTarget != NULL);
	CLASS_PRE_CONDITION(ROW_HEIGHT_ >= MIN_ROW_HEIGHT_);
	CLASS_PRE_CONDITION((0 < NUM_DISPLAY_ROWS_)
							&& (NUM_DISPLAY_ROWS_ <= MAX_DISPLAY_ROWS_));
	CLASS_PRE_CONDITION((0 < NUM_DISPLAY_COLUMNS_)
							&& (NUM_DISPLAY_COLUMNS_ <= MAX_DISPLAY_COLUMNS_));

	// Setup the selectRowButtons_ array
	selectRowButtons_[0]  = &selectRow0Button_;
	selectRowButtons_[1]  = &selectRow1Button_;
	selectRowButtons_[2]  = &selectRow2Button_;
	selectRowButtons_[3]  = &selectRow3Button_;
	selectRowButtons_[4]  = &selectRow4Button_;
	selectRowButtons_[5]  = &selectRow5Button_;
	selectRowButtons_[6]  = &selectRow6Button_;
	selectRowButtons_[7]  = &selectRow7Button_;
	selectRowButtons_[8]  = &selectRow8Button_;
	selectRowButtons_[9]  = &selectRow9Button_;
	selectRowButtons_[10] = &selectRow10Button_;
	selectRowButtons_[11] = &selectRow11Button_;
	SAFE_CLASS_ASSERTION(11 == MAX_DISPLAY_ROWS_ - 1);

	// Make this object be a button target of all displayable row select buttons
	Int32 row;
	for (row = 0; row < NUM_DISPLAY_ROWS_; row++)
	{
		selectRowButtons_[row]->setButtonCallback(this);

		// Set the color of horizontal lines while we're at it
		horizLines_[row].setColor(Colors::LIGHT_BLUE);
	}
	// Last one should be white
	horizLines_[NUM_DISPLAY_ROWS_ - 1].setColor(Colors::WHITE);

	// Set the color of vertical lines and column titles
	vertLines_[0].setColor(Colors::WHITE);
	columnTitles_[0].setColor(Colors::WHITE);
	Int32 col;
	for (col = 1; col < NUM_DISPLAY_COLUMNS_; col++)
	{
		vertLines_[col].setColor(Colors::LIGHT_BLUE);
		columnTitles_[col].setColor(Colors::WHITE);
	}
	vertLines_[col].setColor(Colors::WHITE);		// Extra one at end

	// Set color of title separator
	titleSeparator_.setColor(Colors::WHITE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScrollableLog()  [Destructor]
//
//@ Interface-Description
// ScrollableLog destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScrollableLog::~ScrollableLog(void)
{
	CALL_TRACE("ScrollableLog::~ScrollableLog(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: New			[static]
//
//@ Interface-Description
// Returns a pointer to a newly created ScrollableLog.  Passed the same
// arguments as the constructor, i.e.:
// >Von
//	pTarget				A LogTarget pointer through which cell contents are
//						retrieved and user select events are communicated.
//	numDisplayRows		Number of rows displayed by the log.
//	numDisplayColumns	Number of columns displayed by the log.
//	rowHeight			The height in pixels of a row (includes the one pixel
//						of the horizontal dividing line between rows).
//	displayScrollbar	Pass FALSE if you do not want ScrollableLog to have
//						a scrollbar.  Defaults to TRUE.
// >Voff
// The ScrollableLog is created in a shared memory area so objects should be
// aware that New() destroys the ScrollableLog created by the last call to
// New(). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Construct a ScrollableLog object using placement new in the static memory
// pool ScrollableLogMemory_ and returns a pointer to this memory.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScrollableLog *
ScrollableLog::New(LogTarget *pTarget, Uint16 numDisplayRows,
						Uint16 numDisplayColumns, Uint16 rowHeight,
						Boolean displayScrollbar)
{
	CALL_TRACE("ScrollableLog::New(LogTarget *pTarget, Uint16 numDisplayRows, "
						"Uint16 numDisplayColumns, Uint16 rowHeight, "
						"Boolean displayScrollbar)");

	// Construct new ScrollableLog in memory pool
	new (::ScrollableLogMemory_)
		ScrollableLog(pTarget, numDisplayRows, numDisplayColumns, rowHeight,
									displayScrollbar);

	// Return pointer to memory pool
	return ((ScrollableLog *)::ScrollableLogMemory_);		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setColumnInfo
//
//@ Interface-Description
// This method must be called once for each column before ScrollableLog is
// first displayed.  Used for setting the column title, its width and
// some text formatting information used in conjunction with the results
// from LogTarget's getLogEntryColumn() method in setting the contents
// of a LogCell.  See LogCell's setText() method for more information.
// Passed the following parameters:
// >Von
//	columnNumber		The column number to which the other parameters apply.
//						Numbered from 0 to (numDisplayColumns - 1).
//	title				The title of the column, e.g. "Time" or "Test Name".
//						The title is centered above the column and can actually
//						be wider than the column.
//	width				The width of the column in pixels (includes the one
//						pixel of the vertical dividing line between columns).
//	alignment			This parameter and the following ones are defaulted.
//						If the application always specifies the contents of
//						this column with an actual cheap text string then these
//						parameters need not be set.  This parameter specifies
//						the alignment of this column if the auto-formatting
//						text feature is used.  Can be LEFT, RIGHT or CENTER.
//						Defaults to LEFT.
//	margin				The number of pixels margin to leave when aligning
//						the text.  Not used if "alignment" is CENTER.
//	fontStyle			The style of the auto-formatted text, normal, thin
//						or italic.  Defaults to TextFont::NORMAL.
//	oneLineFontSize		The font size if only one line of text is specified
//						when auto-formatting.  Defaults to TextFont::TWELVE.
//	twoLineFontSize		The font size to use if two lines of text are
//						specified.  Defaults to TextFont::TWELVE.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Mostly store the passed in parameters in the columnInfos_[] array.
// For the column title we formulate cheap text and store the result
// in the columnTitles_[] array of TextFields.
//---------------------------------------------------------------------
//@ PreCondition
// The ScrollableLog must currently be invisible.  The columnNumber must
// be within range (0 to numDisplayColumns - 1 inclusive) and the width
// must be 2 pixels or greater.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
ScrollableLog::setColumnInfo(Uint16 columnNumber, StringId title,
				Uint16 width, Alignment alignment,
				Uint16 margin, TextFont::Style fontStyle,
				TextFont::Size oneLineFontSize, TextFont::Size twoLineFontSize)
{
	CALL_TRACE("ScrollableLog::setColumnInfo(Uint16 columnNumber, StringId title, "
				"Uint16 width, Alignment alignment, "
				"Uint16 margin, TextFont::Style fontStyle, "
				"TextFont::Size oneLineFontSize, TextFont::Size twoLineFontSize)");

	CLASS_PRE_CONDITION(!isVisible());	// Not allowed to update while visible
	CLASS_PRE_CONDITION((0 <= columnNumber)
									&& (columnNumber < NUM_DISPLAY_COLUMNS_));
	CLASS_PRE_CONDITION(MIN_COLUMN_WIDTH_ <= width);

	wchar_t tmpBuffer[256] = {0};

	// Make cheap text out of the column title and set this to be the contents
	// of the column title TextField
	swprintf(tmpBuffer, L"{p=10,y=13:%s}", title);
	columnTitles_[columnNumber].setText(tmpBuffer);

	// Store the rest of the column info for later
	columnInfos_[columnNumber].width		   = width;
	columnInfos_[columnNumber].alignment	   = alignment;
	columnInfos_[columnNumber].margin		   = margin;
	columnInfos_[columnNumber].fontStyle	   = fontStyle;
	columnInfos_[columnNumber].oneLineFontSize = oneLineFontSize;
	columnInfos_[columnNumber].twoLineFontSize = twoLineFontSize;
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Must be called just before ScrollableLog is to be displayed.  Its job
// is to layout the scrollable log using the construction parameter
// information, setColumnInfo() data and any other variables passed to
// other methods.
//---------------------------------------------------------------------
//@ Implementation-Description
// For each column we must position each column title centered above the
// column, size and position each LogCell object in cells_[] and position
// the vertical lines separating each column.  For each row we must
// position each horizontal line separating the rows and position the
// select buttons in the last column of each row.  Other miscellaneous
// graphic positioning is also done.  Finally we call refreshAll_() to
// update each LogCell object with correct log information.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::activate(void)
{
	CALL_TRACE("ScrollableLog::activate(void)");

	Uint16 currColX = 1;		// Current column X position (col 0 X is 1)

	// First row of cells should be displayed on first row at the start!
	firstCellRow_ = 0;

	Int32 col;
	// Setup each column according to the info passed to setColumnInfo()
	for (col = 0; col < NUM_DISPLAY_COLUMNS_; col++)
	{
		// Center the column title above the column.
		columnTitles_[col].setX(currColX +
				(columnInfos_[col].width - columnTitles_[col].getWidth()) / 2);
		addDrawable(&columnTitles_[col]);

		// Set the width and height of each cell in the display part of the log
		// to be one less than the column width and row height (need to reserve
		// a pixel for the lines drawn to the left and below the cell).
		for (int row = 0; row < NUM_DISPLAY_ROWS_; row++)
		{
			cells_[row][col].setX(currColX);
			cells_[row][col].setY(LOG_Y_OFFSET_ + row * ROW_HEIGHT_);
			cells_[row][col].setWidth(columnInfos_[col].width - 1);
			cells_[row][col].setHeight(ROW_HEIGHT_ - 1);
			addDrawable(&cells_[row][col]);
		}

		// Setup the vertical lines before each cell
		vertLines_[col].setStartPoint(currColX - 1, LOG_Y_OFFSET_);
		vertLines_[col].setEndPoint(currColX - 1, LOG_Y_OFFSET_ +
											NUM_DISPLAY_ROWS_ * ROW_HEIGHT_ - 1);

		// The X position of the next column depends on the width of this
		// column.
		currColX += columnInfos_[col].width;

		addDrawable(&vertLines_[col]);
	}

	// Display closing vertical line 
	vertLines_[col].setStartPoint(currColX - 1, LOG_Y_OFFSET_);
	vertLines_[col].setEndPoint(currColX - 1, LOG_Y_OFFSET_ +
										NUM_DISPLAY_ROWS_ * ROW_HEIGHT_);
	addDrawable(&vertLines_[col]);

	// currColX is now the X position where we should place the scrollbar
	// and where the horizontal lines should end.  Let's position the
	// horizontal lines accordingly.
	Int32 row = 0;
	for (row = 0; row < NUM_DISPLAY_ROWS_; row++)
	{
		horizLines_[row].setStartPoint(0,
								LOG_Y_OFFSET_ + (row + 1) * ROW_HEIGHT_ - 1);
		horizLines_[row].setEndPoint(currColX - 1,
								LOG_Y_OFFSET_ + (row + 1) * ROW_HEIGHT_ - 1);
		addDrawable(&horizLines_[row]);
	}

	// The two white vertical lines at the edges of the log must be displayed
	// above the grey horizontal lines so bring those two lines higher in
	// the display tree.
	removeDrawable(&vertLines_[0]);
	removeDrawable(&vertLines_[NUM_DISPLAY_COLUMNS_]);
	addDrawable(&vertLines_[0]);
	addDrawable(&vertLines_[NUM_DISPLAY_COLUMNS_]);

	// If scrollbar needs displaying then do it!
	if (displayScrollbar_)
	{													// $[TI1]
		scrollbar_.setX(currColX);
		scrollbar_.setY(TITLE_HEIGHT_ + TITLE_SEPARATOR_HEIGHT_);
		addDrawable(&scrollbar_);
		scrollbar_.activate();
		titleSeparator_.setWidth(currColX + scrollbar_.getWidth());

		// Extend the last horizontal line below the last row to go beneath
		// the scrollbar
		horizLines_[NUM_DISPLAY_ROWS_ - 1].setEndPoint(
						currColX + scrollbar_.getWidth() - 1,
						LOG_Y_OFFSET_ + NUM_DISPLAY_ROWS_ * ROW_HEIGHT_ - 1);
	}
	else
	{													// $[TI2]
		titleSeparator_.setWidth(currColX);
	}

	// Set size of whole ScrollableLog container
	setWidth(titleSeparator_.getWidth());
	setHeight(TITLE_HEIGHT_ + TITLE_SEPARATOR_HEIGHT_ +
									ROW_HEIGHT_ * NUM_DISPLAY_ROWS_);

	// Add title separator
	addDrawable(&titleSeparator_);

	// Row by row format and add the buttons to the first column cell container
	for (row = 0; row < NUM_DISPLAY_ROWS_; row++)
	{
		selectRowButtons_[row]->setWidth( columnInfos_[0].width - BUTTON_GAP_*2);
		selectRowButtons_[row]->setHeight( ROW_HEIGHT_ - BUTTON_GAP_*2);
		selectRowButtons_[row]->setX(BUTTON_GAP_);
		selectRowButtons_[row]->setY(BUTTON_GAP_);
		cells_[row][0].addDrawable(selectRowButtons_[row]);
	}		

	// Now decide whether to show row select buttons or not.
	updateSelectButtons_();

	// Update all cells with the latest log contents.
	refreshAll_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::deactivate(void)
{
	CALL_TRACE("deactivate(void)");

	// $[TI1]
	clearSelectedEntry();
	scrollbar_.deactivate();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setEntryInfo
//
//@ Interface-Description
// Sets "entry info", i.e. firstDisplayedEntry (the log entry number that
// should be displayed on the first row of the log) and numEntries (the total
// number of entries in the log).  Usually firstDisplayedEntry is set to 0.
// For logs whose number of entries do not change once ScrollableLog
// is displayed this method needs only to be called once before display.
// If it is called while ScrollableLog is displayed it will cause a complete
// refresh of ScrollableLog.  Note that any currently selected entry will
// be deselected.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the parameters.  Make sure firstDisplayedEntry_ is valid.
// If the scrollbar is displayed pass on the information.  Then, if
// ScrollableLog is visible, refresh the whole log.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::setEntryInfo(Uint16 firstDisplayedEntry, Uint16 numEntries)
{
	CALL_TRACE("ScrollableLog::setEntryInfo(Uint16 firstDisplayedEntry, Uint16 numEntries)");

	// Store new numbers
	firstDisplayedEntry_ = firstDisplayedEntry;
	numEntries_ = numEntries;
	selectedEntry_ = -1;

	// Make sure firstDisplayedEntry_ is valid
	if (numEntries_ <= NUM_DISPLAY_ROWS_)
	{													// $[TI1]
		firstDisplayedEntry_ = 0;
	}
	else if (firstDisplayedEntry_ + NUM_DISPLAY_ROWS_ > numEntries_)
	{													// $[TI2]
		firstDisplayedEntry_ = numEntries_ - NUM_DISPLAY_ROWS_;
	}													// $[TI3]

	// Pass on the info to the scrollbar, if displayed
	if (displayScrollbar_)
	{													// $[TI4]
		scrollbar_.setEntryInfo(firstDisplayedEntry_, numEntries_);
	}													// $[TI5]

	// Got's to do a full refresh if we're visible
	if (isVisible())
	{													// $[TI6]
		refreshAll_();
	}													// $[TI7]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSelectedEntry
//
//@ Interface-Description
// Sets the new selected entry.  If the selected entry is not currently
// displayed on the log then the first displayed entry of the log is set
// so as to ensure this.
//---------------------------------------------------------------------
//@ Implementation-Description
// If there already is a selected entry then unhighlight it.  Store
// the new selected entry.  If the selected entry is already onscreen
// then simply highlight it.  If the selected entry is offscreen then
// we have to decide what the first displayed entry should be so as to
// display the selected entry.
//
// The first thing to check is whether the selected entry is one off the
// top or bottom row of the log.  If it is then we just scroll one so that
// it is now onscreen.  Otherwise if the selected entry is on the first
// or last page of entries then display the whole first or last page of
// entries.  Otherwise just display the selected entry as the middle entry
// of ScrollableLog.
//
// Note: do not do any highlighting/unhighlighting if ScrollableLog is
// not visible.
//---------------------------------------------------------------------
//@ PreCondition
// The selected entry number must be less than numEntries_.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::setSelectedEntry(Uint16 newSelectedEntry)
{
	CALL_TRACE("ScrollableLog::setSelectedEntry(Uint16 newSelectedEntry)");

	Uint16 newFirstDisplayedEntry;

	CLASS_PRE_CONDITION(newSelectedEntry < numEntries_);

	// Only react to call if it's a new current entry
	if (newSelectedEntry != selectedEntry_)
	{													// $[TI1]
		// If there's already a selected entry then unhighlight that row
		if ((-1 != selectedEntry_) && isVisible())
		{												// $[TI2]
			unhighlightRow_(selectedEntry_ - firstDisplayedEntry_);
		}												// $[TI3]

		// Store the new selected entry
		selectedEntry_ = newSelectedEntry;

		// If the new selected entry is offscreen then put it onscreen
		if ((selectedEntry_ < firstDisplayedEntry_)
			|| (selectedEntry_ >= firstDisplayedEntry_ + NUM_DISPLAY_ROWS_))
		{												// $[TI4]
			// Set newFirstDisplayedEntry so that selectedEntry_ is onscreen.

			// If the selected entry is one entry off the top
			// of the log then make it the first displayed entry (emulates
			// scrolling).
			if (firstDisplayedEntry_ == selectedEntry_ + 1)
			{											// $[TI5]
				newFirstDisplayedEntry = selectedEntry_;
			}
			// If the selected entry is one entry off the bottom of
			// the log then make it the last displayed entry (emulates
			// scrolling).
			else if (firstDisplayedEntry_ + NUM_DISPLAY_ROWS_ ==
														selectedEntry_)
			{											// $[TI6]
				newFirstDisplayedEntry =
									selectedEntry_ - NUM_DISPLAY_ROWS_ + 1;
														
			}
			// If the selected entry is on the first page of the log then
			// make 0 the first displayed entry.
			else if (selectedEntry_ < NUM_DISPLAY_ROWS_)
			{											// $[TI7]
				// Selected entry is on first page of entries.  Set
				// first displayed entry to first entry.
				newFirstDisplayedEntry = 0;
			}
			// If the selected entry is on the last page of the log then
			// make the last entry in the log the last displayed entry.
			else if (selectedEntry_ + NUM_DISPLAY_ROWS_ >= numEntries_)
			{											// $[TI8]
				// Selected entry is on last page of entries.  Set
				// first displayed entry so that whole last page is
				// displayed.
				newFirstDisplayedEntry = numEntries_ - NUM_DISPLAY_ROWS_;
			}
			// Selected entry is somewhere in the middle of the log.  Set
			// first displayed entry so that the selected entry is displayed
			// approximately in the middle row.
			else
			{											// $[TI9]
				// Set first displayed entry so that selected
				// entry is approximately centered.
				newFirstDisplayedEntry =
									selectedEntry_ - NUM_DISPLAY_ROWS_ / 2;
			}
			
			// Pass on the info about new first displayed entry to the
			// scrollbar, if displayed
			if (displayScrollbar_)
			{											// $[TI10]
				scrollbar_.setEntryInfo(newFirstDisplayedEntry, numEntries_);
			}											// $[TI11]
		
			// Refresh the log accordingly (use scrollHappened() to do it
			// because it has the intelligence to do efficient scrolling)
			if (isVisible())
			{											// $[TI12]
				scrollHappened(newFirstDisplayedEntry);
			}											// $[TI13]

			// Make sure new first displayed entry is stored
			firstDisplayedEntry_ = newFirstDisplayedEntry;
		}
		else if (isVisible())
		{												// $[TI14]
			// Newly elected row is onscreen, simply highlight it
			highlightRow_(selectedEntry_ - firstDisplayedEntry_);
		}												// $[TI15]
	}													// $[TI16]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearSelectedEntry
//
//@ Interface-Description
// Unhighlights (clears) the currently selected entry, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
// If a selected entry is set and ScrollableLog is visible then
// unhighlight the selected entry and store -1 as the new selected entry
// (indicates no selected entry).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::clearSelectedEntry(void)
{
	CALL_TRACE("ScrollableLog::clearSelectedEntry(void)");

	// Only react to call if a selected entry exists
	if (-1 != selectedEntry_)
	{													// $[TI1]
		// If log is displayed then unhighlight the selected row.
		if (isVisible())
		{												// $[TI2]
			unhighlightRow_(selectedEntry_ - firstDisplayedEntry_);
		}												// $[TI3]

		// Clear the currently selected entry
		selectedEntry_ = -1;
	}													// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSelectedEntry
//
//@ Interface-Description
// Gets the currently selected entry.  Passed a reference to this value,
// rSelectedEntry, which will be set to the currently selected entry, if any.
// If there is no currently selected entry then the return value of this method
// will be FAILURE, otherwise SUCCESS is returned and rSelectedEntry will be
// set.
//---------------------------------------------------------------------
//@ Implementation-Description
// As above.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SigmaStatus
ScrollableLog::getSelectedEntry(Uint16 &rSelectedEntry)
{
	CALL_TRACE("ScrollableLog::getSelectedEntry(Uint16 &rSelectedEntry)");

	// Return FAILURE if there's no selected entry
	if (selectedEntry_ < 0)
	{													// $[TI1]
		return (FAILURE);
	}													// $[TI2]

	// Set selectedEntry and return
	rSelectedEntry = selectedEntry_;
	return (SUCCESS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateEntry
//
//@ Interface-Description
// This method should be called by the application when the contents of a log
// entry changes.  Passed the number of the changed entry.  If the entry is
// currently being displayed ScrollableLog will update the appropriate row with
// the new contents.  Otherwise the call is ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
// If ScrollableLog is currently being displayed and changedEntry is onscreen
// then call refreshRow_() to update the correct row.
//---------------------------------------------------------------------
//@ PreCondition
// The changed entry number must be less than numEntries_.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::updateEntry(Uint16 changedEntry)
{
	CALL_TRACE("ScrollableLog::updateEntry(Uint16 changedEntry)");

	CLASS_PRE_CONDITION(changedEntry < numEntries_);

	// Refresh the changed entry row as long as it's displayed on screen
	if (isVisible() && (changedEntry >= firstDisplayedEntry_)
			&& (changedEntry < firstDisplayedEntry_ + NUM_DISPLAY_ROWS_))
	{													// $[TI1]
		refreshRow_(changedEntry - firstDisplayedEntry_);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateColumn
//
//@ Interface-Description
// This method should be called by the application when the contents of a
// particular column across the whole log changes.  Passed the number of the
// changed column.  If ScrollableLog is displayed it will update all the cells
// in the specified column.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call refreshCell_() for all cells in the specified column if ScrollableLog
// is visible.
//---------------------------------------------------------------------
//@ PreCondition
// The changed column number must be less than NUM_DISPLAY_COLUMNS_.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::updateColumn(Uint16 columnNumber)
{
	CALL_TRACE("ScrollableLog::updateColumn(Uint16 columnNumber)");

	CLASS_PRE_CONDITION(columnNumber < NUM_DISPLAY_COLUMNS_);

	// Refresh the changed entry row as long as it's displayed on screen
	if (isVisible())
	{													// $[TI1]
		for (int row = 0; row < NUM_DISPLAY_ROWS_; row++)
		{
			refreshCell_(row, columnNumber);
		}
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateEntryColumn
//
//@ Interface-Description
// Should be called when the contents of a single column of a single
// entry in the log changes.  If that cell is visible onscreen then it
// is updated.  Passed the changed entry number and column number of the cell.
//---------------------------------------------------------------------
//@ Implementation-Description
// If ScrollableLog is visible and the changed entry number is onscreen
// then call refreshCell_().
//---------------------------------------------------------------------
//@ PreCondition
// changedEntry and columnNumber must be valid (within range).
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::updateEntryColumn(Uint16 changedEntry, Uint16 columnNumber)
{
	CALL_TRACE("ScrollableLog::updateEntryColumn(Uint16 changedEntry, Uint16 columnNumber)");

	CLASS_PRE_CONDITION((changedEntry < numEntries_) &&
						(columnNumber < NUM_DISPLAY_COLUMNS_));

	// Refresh the changed entry column as long as it's displayed on screen
	if (isVisible() && (changedEntry >= firstDisplayedEntry_)
			&& (changedEntry < firstDisplayedEntry_ + NUM_DISPLAY_ROWS_))
	{													// $[TI1]
		refreshCell_(changedEntry - firstDisplayedEntry_, columnNumber);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setUserScrollable
//
//@ Interface-Description
// Passed a boolean which determines whether the user is allowed to
// use the scrollbar to scroll the log or not.
//---------------------------------------------------------------------
//@ Implementation-Description
// Pass boolean straight on to scrollbar_.  It is responsible for scrolling.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::setUserScrollable(Boolean isUserScrollable)
{
	CALL_TRACE("ScrollableLog::setUserScrollable(Boolean isUserScrollable)");

	// Pass the flag on to the scrollbar.
	scrollbar_.setScrollable(isUserScrollable);			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setUserSelectable
//
//@ Interface-Description
// Passed a boolean which determines whether the user is allowed to
// select entries of the log or not.  Essentially sets the visibility
// of the row select buttons.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the new value of isUserSelectable_ and, if ScrollableLog is visible,
// call updateSelectButton_() to display/remove the row select buttons.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::setUserSelectable(Boolean isUserSelectable)
{
	CALL_TRACE("ScrollableLog::setUserSelectable(Boolean isUserSelectable)");

	// Store the new flag
	isUserSelectable_ = isUserSelectable;

	// If log is visible then update the display immediately
	if (isVisible())
	{													// $[TI1]
		updateSelectButtons_();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: scrollHappened
//
//@ Interface-Description
// Called by ScrollableLog's Scrollbar object when the user initiates a
// scrolling action via one of the scrollbar's scroll buttons.  Passed
// an integer specifying what the new first displayed entry of the log
// should be.  We must update the display accordingly and inform the
// application of a deselection event if a selected row scrolled off the
// screen (selected rows can only be onscreen).
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the new first displayed entry.  Then check to see if a single
// row down scroll occurred.  If so, then shift all rows of LogCells
// in the cells_[] array up a row in round robin fashion, i.e. the last
// cells_[] row is moved to where the second last cells_[] row is ...
// row 2 is moved to where row 1 is, row 1 is moved to where row 0 is
// and row 0 is moved to where the last row was.  Increment firstCellRow_
// and then refresh the last row.
//
// If a single row up scroll occurred then do the following: shift all rows of
// LogCells in the cells_[] array down a row in round robin fashion, i.e. row 0
// of cells_[] is moved to where row 1 is, row 1 is moved to where row 2 is,
// ... and the last cells_[] row is moved to where the row 0 was.  Decrement
// firstCellRow_ and then refresh the first row.
//
// If a multiple row scroll occurred then don't do anything fancy, just
// refresh the whole ScrollableLog.
//
// If a selected entry ended up scrolling off the screen then deselect it
// and inform pTarget_ of the event.
//
// Note that the round robin shifting of the LogCells in cells_[] is applied
// also to the row select buttons in selectRowButtons_[].
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Uint DirectDrawTime;

void
ScrollableLog::scrollHappened(Uint16 firstDisplayedEntry)
{
	CALL_TRACE("ScrollableLog::scrollHappened(Uint16 firstDisplayedEntry)");

	// Store old and new first displayed entry
	Uint16 oldFirstDisplayedEntry_ = firstDisplayedEntry_;
	firstDisplayedEntry_ = firstDisplayedEntry;

	// Check to see if a "one row down" scroll occurred
	if (firstDisplayedEntry_ == oldFirstDisplayedEntry_ + 1)
	{													// $[TI1]

		setDirectDraw(TRUE);

		// define the area within this Container to scroll
		Area area( 0, LOG_Y_OFFSET_ + ROW_HEIGHT_, 
			getWidth() - scrollbar_.getWidth() - 2, 
			LOG_Y_OFFSET_ + NUM_DISPLAY_ROWS_ * ROW_HEIGHT_ - 2 );

		// scroll area up one row
		scroll(area, 0, -ROW_HEIGHT_);

		// Increment firstCellRow_
		firstCellRow_ = (firstCellRow_ + 1) % NUM_DISPLAY_ROWS_;

		// reset the Y position of each row of cells
		Uint cellRow = firstCellRow_;
		for (Uint row = 0; row < NUM_DISPLAY_ROWS_; row++)
		{												
		  for (Uint col = 0; col < NUM_DISPLAY_COLUMNS_; col++)
		  {
			cells_[cellRow][col].setY(LOG_Y_OFFSET_ + row * ROW_HEIGHT_);
		  }
		  cellRow = ++cellRow % NUM_DISPLAY_ROWS_;
		}												

		// Refresh bottom row
		refreshRow_(NUM_DISPLAY_ROWS_ - 1);

		// direct draw the last row to stay in sync with scrolled area
	    cellRow = displayRowToCellRow_(NUM_DISPLAY_ROWS_-1);

		for (Uint drawcol = 0; drawcol < NUM_DISPLAY_COLUMNS_; drawcol++)
		{
		  cells_[cellRow][drawcol].drawDirect();
		}

		setDirectDraw(FALSE);
	}
	// Check to see if a "one row up" scroll occurred
	else if (oldFirstDisplayedEntry_ == firstDisplayedEntry_ + 1)
	{													// $[TI4]
		setDirectDraw(TRUE);

		// define the area within this Container to scroll
		Area area( 0, LOG_Y_OFFSET_,
			getWidth() - scrollbar_.getWidth() - 2, 
			LOG_Y_OFFSET_ + (NUM_DISPLAY_ROWS_-1) * ROW_HEIGHT_ - 2 );

		// scroll area down one row
		scroll(area, 0, ROW_HEIGHT_);

		// Decrement firstCellRow_
		firstCellRow_ = (firstCellRow_ - 1 + NUM_DISPLAY_ROWS_) % NUM_DISPLAY_ROWS_;

		// reset the Y position of each row of cells
		Uint cellRow = firstCellRow_;
		for (Uint row = 0; row < NUM_DISPLAY_ROWS_; row++)
		{												
		  for (Uint col = 0; col < NUM_DISPLAY_COLUMNS_; col++)
		  {
			cells_[cellRow][col].setY(LOG_Y_OFFSET_ + row * ROW_HEIGHT_);
		  }
		  cellRow = ++cellRow % NUM_DISPLAY_ROWS_;
		}												

		// Refresh top row
		refreshRow_(0);

		// direct draw the first row to stay in sync with scrolled area
	    cellRow = displayRowToCellRow_(0);

		for (Uint drawcol = 0; drawcol < NUM_DISPLAY_COLUMNS_; drawcol++)
		{
		  cells_[cellRow][drawcol].drawDirect();
		}

		setDirectDraw(FALSE);
	}
	else
	{													
		// multi-row scroll is not implemented
		CLASS_ASSERTION_FAILURE();
	}

	// Check if a selected entry just scrolled offscreen.  If it did then
	// we must deselect it
	if ((-1 != selectedEntry_) &&
			((selectedEntry_ < firstDisplayedEntry_)
				|| (selectedEntry_ >= firstDisplayedEntry_ + NUM_DISPLAY_ROWS_)))
	{													// $[TI8]
		// Uh oh, selected entry scrolled offscreen.  Inform target of
		// change and deselect it.  In case target calls, say,
		// clearSelectedEntry() selectedEntry_ must be set to -1 before
		// informing target so that selectedEntry_ will have a legal value
		// (it's current value is illegal because it is offscreen).
		Int16 deselectedEntry = selectedEntry_;
		selectedEntry_ = -1;
		pTarget_->currentLogEntryChangeHappened(deselectedEntry, FALSE);
	}													// $[TI9]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when one of the row select buttons is pressed down.  If it
// is pressed down by the operator then highlight the selected entry
// and inform the application of the event via LogTarget's
// currentLogEntryChangeHappened() method.  Any previously selected
// entry is unhighlighted.
//---------------------------------------------------------------------
//@ Implementation-Description
// Ignore event if byOperatorAction is FALSE. If there already is a
// selected entry unhighlight it.  Search through the selectRowButtons_[]
// array for the pointer that matches the pButton parameter.  The array index
// tells us which row was selected.  Go highlight the row and inform
// pTarget_ of selection event.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{
	CALL_TRACE("ScrollableLog::buttonDownHappened(Button *pButton, Boolean byOperatorAction)");

	// Only react to down events that come from the operator
	if (byOperatorAction)
	{													// $[TI1]
		// User selected a new row. If there's already a selected entry
		// on-screen then unhighlight that row.
		if ((selectedEntry_ >= firstDisplayedEntry_) &&
			(selectedEntry_ < firstDisplayedEntry_ + NUM_DISPLAY_ROWS_))
		{												// $[TI2]
			unhighlightRow_(selectedEntry_ - firstDisplayedEntry_);
		}												// $[TI3]
	
		// Find which row is selected
		for (Uint cellRow = 0; cellRow < NUM_DISPLAY_ROWS_; cellRow++)
		{
			if (pButton == selectRowButtons_[cellRow])
			{											// $[TI4]
				// Found selected "cell" row.  We have to relate this "cell"
				// row back to a "display" row number.
				Uint displayRow = cellRowToDisplayRow_(cellRow);

				// Highlight display row
				highlightRow_(displayRow);
	
				// Store new selection.
				selectedEntry_ = firstDisplayedEntry_ + displayRow;

				// Inform target of change.
				pTarget_->currentLogEntryChangeHappened(selectedEntry_, TRUE);

				break;		// Finished processing
			}											// $[TI5]
		}
	}													// $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when one of the row select buttons is deselected.  If it
// is pressed up by the operator then unhighlight the selected entry
// and inform the application of the deselection event via LogTarget's
// currentLogEntryChangeHappened() method.
//---------------------------------------------------------------------
//@ Implementation-Description
// Ignore event if byOperatorAction is FALSE.  Unhighlight
// the currently selected row and inform pTarget_ of deselection event.
// Store the fact that there is no currently selected entry.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::buttonUpHappened(Button *, Boolean byOperatorAction)
{
	CALL_TRACE("ScrollableLog::buttonUpHappened(Button *pButton, Boolean byOperatorAction)");

	// Only react to up events that come from the operator
	if (byOperatorAction)
	{													// $[TI1]
		SAFE_CLASS_ASSERTION(selectedEntry_ != -1);

		// Unhighlight currently selected row.
		unhighlightRow_(selectedEntry_ - firstDisplayedEntry_);

		// Inform target of change.  In case target calls, say,
		// clearSelectedEntry() selectedEntry_ must be set to -1 before
		// informing target so that selectedEntry_ will have a legal value
		// (it's current value is illegal because it is offscreen).
		selectedEntry_ = -1;
		pTarget_->currentLogEntryChangeHappened(selectedEntry_, FALSE);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: refreshAll_		[Private]
//
//@ Interface-Description
// Updates all the cells in ScrollableLog according to the current log
// information.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call refreshRow_() successively for each row in ScrollableLog.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::refreshAll_(void)
{
	// Refresh the full log by refreshing all the rows
	for (int row = 0; row < NUM_DISPLAY_ROWS_; row++)
	{
		refreshRow_(row);
	}													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: refreshRow_		[Private]
//
//@ Interface-Description
// Refreshes all the cells in a single row.  Passed the row number.
//---------------------------------------------------------------------
//@ Implementation-Description
// For each column call refreshCell_().
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::refreshRow_(Uint16 row)
{
	// Refresh row by refreshing all cells in the row
	for (int col = 0; col < NUM_DISPLAY_COLUMNS_; col++)
	{
		refreshCell_(row, col);
	}													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: refreshCell_
//
//@ Interface-Description
// Refreshes the contents of a single cell.  Passed the row and column
// number of the cell.
//---------------------------------------------------------------------
//@ Implementation-Description
// First relate the display row number back to the index of the row in
// the cells_[] array that renders that row.
//
// If the cell is on a row beyond the end of the log then make sure the
// cell is empty.  Change the colors of the cell depending on whether
// it is on a selected row or not.  If it's the last cell on the row
// then decide whether to show a row select button there.  Retrieve
// the text content of the cell from pTarget_ and pass on the info
// along with information from the columnInfos_[] array to the correct
// LogCell object in cells_[].
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::refreshCell_(Uint16 row, Uint16 col)
{
	Boolean isCheapText = FALSE;
	StringId string1 = NULL_STRING_ID;
	StringId string2 = NULL_STRING_ID;
	StringId string3 = NULL_STRING_ID;
	StringId string1Message = NULL_STRING_ID;
	Uint16 entryNumber = firstDisplayedEntry_ + row;
	Uint16 cellRow;

	// Calculate what row in the cells_[] array corresponds to the row on
	// the display
	cellRow = displayRowToCellRow_(row);

	// First, check if cell is on a row that corresponds to a log entry
	if (entryNumber >= numEntries_)
	{													// $[TI1]
		// Row should be displayed empty
		cells_[cellRow][col].setText(TRUE, NULL_STRING_ID);
		cells_[cellRow][col].setFillColor(Colors::MEDIUM_BLUE);

		// If we're the first column then make sure the selection button is
		// not displayed.
		if (col == 0)
		{												// $[TI2]
			selectRowButtons_[cellRow]->setShow(FALSE);
		}												// $[TI3]
	}
	else
	{													// $[TI4]
		// Check if the cell is on a row that's selected.
		if (selectedEntry_ == entryNumber)
		{												// $[TI5]
			// It is so highlight it.
			cells_[cellRow][col].setFillColor(Colors::WHITE);
			cells_[cellRow][col].setContentsColor(Colors::BLACK);
	
			// If we're the first column and it's a user selectable log then
			// display a button in the down position
			if (isUserSelectable_ && (col == 0))
			{											// $[TI6]
				selectRowButtons_[cellRow]->setShow(TRUE);
				selectRowButtons_[cellRow]->setToDown();
			}											// $[TI7]
		}
		else
		{												// $[TI8]
			// Not selected so render in normal color
			cells_[cellRow][col].setFillColor(Colors::MEDIUM_BLUE);
			cells_[cellRow][col].setContentsColor(Colors::WHITE);
	
			// If we're the first column and it's a user selectable log then
			// display a button in the down position
			if (isUserSelectable_ && (col == 0))
			{											// $[TI9]
				selectRowButtons_[cellRow]->setShow(TRUE);
				selectRowButtons_[cellRow]->setToUp();
			}											// $[TI10]
		}

		// Ask the log target for the contents of the entry's column
		pTarget_->getLogEntryColumn(firstDisplayedEntry_ + row, col,
						isCheapText, string1, string2, string3,
						string1Message);

		// Set the contents of the cell
		if (NULL_STRING_ID == string2)
		{												// $[TI11]
			// string2 is not used so pass oneLineFontSize as the required
			// font size.
			cells_[cellRow][col].setText(isCheapText,
						string1, string2, string3,
						columnInfos_[col].alignment,
						columnInfos_[col].fontStyle,
						columnInfos_[col].oneLineFontSize,
						columnInfos_[col].margin,
						string1Message);
		}
		else
		{												// $[TI12]
			// string2 is used so pass twoLineFontSize as the required
			// font size.
			cells_[cellRow][col].setText(isCheapText,
						string1, string2, string3,
						columnInfos_[col].alignment,
						columnInfos_[col].fontStyle,
						columnInfos_[col].twoLineFontSize,
						columnInfos_[col].margin,
						string1Message);
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateSelectButtons_		[Private]
//
//@ Interface-Description
// Updates the display of select buttons according to where ScrollableLog
// is currently selectable by the user and which row, if any, is selected.
//---------------------------------------------------------------------
//@ Implementation-Description
// If ScrollableLog is not user selectable then make sure all select buttons
// are removed from the display.  Otherwise, ensure that a select button
// is displayed on each row.  Hide the select button on rows which have
// no log entries displayed.  Set the up or down state of the button
// according to whether the button is on a selected row or not.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::updateSelectButtons_(void)
{
	CALL_TRACE("ScrollableLog::updateSelectButtons_(void)");

	// Decide whether to display buttons or not
	if (isUserSelectable_)
	{													// $[TI1]
		Uint16 currentEntryNumber;		// The number of the entries corresponding
										// to the current row on the log
		Uint16 cellRow;
	
		// Row by row show each select button
		for (int row = 0; row < NUM_DISPLAY_ROWS_; row++)
		{
			// Relate the current display row back to the cell row.  This will
			// be the index of the button that is displayed on the current
			// row.
			cellRow = displayRowToCellRow_(row);

			// Calculate the entry number corresponding to the current row.
			currentEntryNumber = firstDisplayedEntry_ + row;

			// Set the show flag of the current button based on the
			// current entry number.  If the current entry number is beyond
			// the end of the entry log then don't show the button, else do.
			selectRowButtons_[cellRow]->setShow(
											currentEntryNumber < numEntries_);
														// $[TI2], $[TI3]

			// If the button is going to be displayed on the row that is the
			// the currently selected row then make sure the button is in the
			// down (selected) state, otherwise make sure it's up.
			if (selectedEntry_ == currentEntryNumber)
			{											// $[TI4]
				selectRowButtons_[cellRow]->setToDown();
			}
			else
			{											// $[TI5]
				selectRowButtons_[cellRow]->setToUp();
			}
		}
	}
	else
	{													// $[TI6]
		// Row by row remove the buttons from the screen
		for (int row = 0; row < NUM_DISPLAY_ROWS_; row++)
		{
			selectRowButtons_[row]->setShow(FALSE);
		}		
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: highlightRow_		[Private]
//
//@ Interface-Description
// Highlights the specified row (displayed as black text on a white
// background).  Make sure the select button on the last column is in the down
// position.  Passed the row number to highlight.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calculate what row in the cells_[] array corresponds to the requested row
// number.  Cycle through the cells on the row and set the colors.  Set the
// accompanying select button to the down state.
//---------------------------------------------------------------------
//@ PreCondition
// Row number must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::highlightRow_(Uint16 rowNumber)
{
	CALL_TRACE("ScrollableLog::highlightRow_(Uint16 rowNumber)");

	SAFE_CLASS_PRE_CONDITION((rowNumber >= 0) && (rowNumber < NUM_DISPLAY_ROWS_));

	// Calculate what row in the cells_[] array corresponds to rowNumber
	Uint16 cellRow = displayRowToCellRow_(rowNumber);

	// Highlight each cell of the row
	for (int col = 0; col < NUM_DISPLAY_COLUMNS_; col++)
	{
		cells_[cellRow][col].setFillColor(Colors::WHITE);
		cells_[cellRow][col].setContentsColor(Colors::BLACK);
	}

	// Set the row select button to the down position (doesn't matter if it's
	// not displayed).
	selectRowButtons_[cellRow]->setToDown();			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: unhighlightRow_
//
//@ Interface-Description
// Unhighlights the specified row (displayed as white text on a transparent
// background).  Make sure the select button on the first column is in the up
// position.  Passed the row number to unhighlight.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calculate what row in the cells_[] array corresponds to the requested row
// number.  Cycle through the cells on the row and set the colors.  Set the
// accompanying select button to the up state.
//---------------------------------------------------------------------
//@ PreCondition
// Row number must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::unhighlightRow_(Uint16 rowNumber)
{
	CALL_TRACE("ScrollableLog::unhighlightRow_(Uint16 rowNumber)");

	SAFE_CLASS_PRE_CONDITION((rowNumber >= 0) && (rowNumber < NUM_DISPLAY_ROWS_));

	// Calculate what row in the cells_[] array corresponds to rowNumber
	Uint16 cellRow = displayRowToCellRow_(rowNumber);

	// Unhighlight each cell of the row
	for (int col = 0; col < NUM_DISPLAY_COLUMNS_; col++)
	{
		cells_[cellRow][col].setFillColor(Colors::MEDIUM_BLUE);
		cells_[cellRow][col].setContentsColor(Colors::WHITE);
	}

	// Set the row select button to the up position (doesn't matter if it's
	// not displayed).
	selectRowButtons_[cellRow]->setToUp();				// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFirstDisplayedEntry
//
//@ Interface-Description
// Returns the first displayed entry, the entry displayed on row 0 of
// ScrollableLog.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply returns firstDisplayedEntry_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Uint16
ScrollableLog::getFirstDisplayedEntry(void)
{
	CALL_TRACE("ScrollableLog::getFirstDisplayedEntry(void)");

	return (firstDisplayedEntry_);						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayRowToCellRow_		[Private]
//
//@ Interface-Description
// Converts the index of a row on the display to the row index into
// the cells_[] array that contains the LogCells that render the display row.
//---------------------------------------------------------------------
//@ Implementation-Description
// The cell row is offset from the display row by 'firstCellRow_'.  So
// just add firstCellRow_ to display row.  Return the result modula
// NUM_DISPLAY_ROWS_ so as to keep the row number between 0 and
// NUM_DISPLAY_ROWS_ - 1 inclusive.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Uint16
ScrollableLog::displayRowToCellRow_(Uint16 displayRow)
{
	CALL_TRACE("ScrollableLog::displayRowToCellRow_(Uint16 displayRow)");

	return ((displayRow + firstCellRow_) % NUM_DISPLAY_ROWS_);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: cellRowToDisplayRow_
//
//@ Interface-Description
// Converts the index of a row in the cells_[] array to the display row
// that the cell row renders.
//---------------------------------------------------------------------
//@ Implementation-Description
// The display row is a negative offset from the cell row by 'firstCellRow_'.
// So just subtract firstCellRow_ from cell row.  Return the result plus
// NUM_DISPLAY_ROWS_ and modula NUM_DISPLAY_ROWS_ so as to keep the row number
// between 0 and NUM_DISPLAY_ROWS_ - 1 inclusive.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Uint16
ScrollableLog::cellRowToDisplayRow_(Uint16 cellRow)
{
	CALL_TRACE("ScrollableLog::cellRowToDisplayRow_(Uint16 cellRow)");

	return ((cellRow - firstCellRow_ + NUM_DISPLAY_ROWS_) % NUM_DISPLAY_ROWS_);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ScrollableLog::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SCROLLABLELOG,
  				lineNumber, pFileName, pPredicate);
}

