#ifndef  BdPost_HH
#define  BdPost_HH

#ifdef SIGMA_BD_CPU
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
// Filename: BdPost.hh - Breath Delivery CPU Power On Self Tests
//---------------------------------------------------------------------
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 05-MAY-1999  DR Number: 5203
//    Project: 840 ATC
//    Description:
//       Implemented the PrevPostFailureTests to allow entry into 
//		 service mode after a previous POST failure. The PrevPostFailureTests
//		 skips all POST tests except for the service switch test that
//		 provides entry into service mode. POST bypasses the normal test
//		 sequence if a previous POST failure exists since running the 
//		 tests again will result in another POST failure which would not
//		 allow entry into service mode where a better diagnosis of the 
//		 problem can occur.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//
//=====================================================================

#include "Post.hh"


class BdPost : public Post
{
  public:
    static const PostTestItem * GetTestItems(void);

  private:
    BdPost();         // not implemented
    ~BdPost();        // not implemented

    static const PostTestItem ShortPostTests_[];
    static const PostTestItem LongPostTests_[];
    static const PostTestItem PrevPostFailureTests_[];
};



#endif // SIGMA_BD_CPU
#endif // BdPost_HH
