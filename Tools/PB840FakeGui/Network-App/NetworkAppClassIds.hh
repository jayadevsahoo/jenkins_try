
#ifndef NetworkAppClassIds_HH
#define NetworkAppClassIds_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Filename:  NetworkAppClassIds - Ids of all of the Network Application
//            subsytem Classes.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/NetworkAppClassIds.hhv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 000  By:  jhv    Date:  06-16-95    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version 
//  
//====================================================================


//@ Type:  NetworkAppClassId
enum NetworkAppClassId {
  NETWORK_APP_CLASS,
  NETWORK_SOCKET_CLASS,
  RECV_SOCKET_CLASS,
  BLOCKIO_SOCKET_CLASS,
  SEND_SOCKET_CLASS,
  BROADCAST_SOCKET_CLASS,
  SOCKET_WRAP_CLASS,
  NUM_NETWORK_APP_CLASSES
};


#endif // NetworkAppClassIds_HH 
