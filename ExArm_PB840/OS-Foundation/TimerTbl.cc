#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimerTbl - A table of MsgTimers that other tasks utilize to 
//  track time and receive notification when the timer expires.
//---------------------------------------------------------------------
//@ Interface-Description
//  A class that manages a table of MsgTimer entries, and includes a 
//  Task that checks the Timers and sends a message when the time is up.
//---------------------------------------------------------------------
//@ Rationale
//  TimerTbl class provides a mechanism for tracking timed events, and then
//  notifying the proper owner that the timer has expired.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Utilizes a static array to store the table of MsgTimer entries.
//  Time is tracked by the TimerTbl::MsgTimerTask.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre and
//  post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/TimerTbl.ccv   25.0.4.0   19 Nov 2013 14:15:20   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 004   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003    By: syw  Date: 09-APR-1998  DR Number: 5048
//    Project: Sigma (R8027)
//    Description:
//       Eliminate TIMING_TEST code.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.  Corrected TI number.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "TimerTbl.hh"
#include "Sys_Init.hh"
#include "Task.hh"
#include "MsgQueue.hh"
#include "IpcIds.hh"
#include "MsgTimer.hh"
#include "TaskMonitor.hh"

#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#  include <stdio.h>
#endif // SIGMA_DEVELOPMENT

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================
 
TimerTbl::TimerEntry TimerTbl::MsgTimerTable_[];
Int32 TimerTbl::MsgTimerCount_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  One-time initialization routine to run placement new on
//  static data members. Initializes MsgTimerCount_ to zero.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A reference to the mutual-exclusion semaphore is used
//  to protect the static data of the TimerTbl class.
//---------------------------------------------------------------------
//@ PreCondition
//  ((sizeof(TimerTbl::RMutEx_) >= sizeof(MutEx))).
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
TimerTbl::Initialize(void)
{
  TimerTbl::MsgTimerCount_ = 0;
  				// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AddMsgTimer_
//
//@ Interface-Description
//  Adds a timer message to the timer table. Takes as input the qId,
//  msg, msec, and eventType. Returns the id of the timer entry in to
//  the TimerTbl. Entry zero is unused.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the data in the timer table for this entry.   
//  Runs in single threaded mode.
//---------------------------------------------------------------------
//@ PreCondition
//  (Sys_Init::IsSystemInitialized() == FALSE)
//  (TimerTbl::MsgTimerCount_ < (Int32)TimerTbl::MAX_NUM_MSGTIMERS)
//  SAFE_CLASS_PRE_CONDITION(msec >= (Int32)MsgTimer::MIN_MSEC_REQUEST)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int32 
TimerTbl::AddMsgTimer_(const Int32 qId, const Int32 msg, const Int32 msec, 
	MsgTimer::EventType eventType)
{
	CLASS_PRE_CONDITION(Sys_Init::IsSystemInitialized() == FALSE);
	CLASS_PRE_CONDITION(TimerTbl::MsgTimerCount_ < (Int32)TimerTbl::MAX_NUM_MSGTIMERS);
	SAFE_CLASS_PRE_CONDITION(msec >= (Int32)MsgTimer::MIN_MSEC_REQUEST);
	TimerTbl::MsgTimerCount_++;  // entry 0 is unused
	TimerEntry *pEntry = &TimerTbl::MsgTimerTable_[TimerTbl::MsgTimerCount_];
	pEntry->qId = qId;
	pEntry->msg = msg;
 	pEntry->msec = ( msec + ( (Int32)TimerTbl::TASK_MSEC_INTERVAL - 1 ) ) 
	              / (Int32)TimerTbl::TASK_MSEC_INTERVAL;
	pEntry->eventType = eventType;
	pEntry->isSet = FALSE;
	// need to convert interval to task tic interval.
	pEntry->ticsLeft = pEntry->msec;
	return TimerTbl::MsgTimerCount_;		// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsSet_
//
//@ Interface-Description
//  Takes an Int32 that identifies the MsgTimer and return whether
//  or not it is set.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Check entry to see if it is active. Call TimerTbl::isSet.
//---------------------------------------------------------------------
//@ PreCondition
//  (TimerTbl::IsValidId(msgTimerId) == TRUE)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
TimerTbl::IsSet_(const Int32 msgTimerId)
{
	CLASS_PRE_CONDITION( TimerTbl::IsValidId_(msgTimerId) );
	Boolean ret = TimerTbl::MsgTimerTable_[msgTimerId].isSet;
	return ( ret );			// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Set_
//
//@ Interface-Description
//  Takes an Int32 that identifies the MsgTimer and activates that
//  Timer entry. Returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the timer to FALSE retrieve and store the tick value then set 
//  the timer to TRUE to activate it.
//  The MsgTimerTask has the highest priority of any task in the table.
//  By first setting the timer to FALSE this insures that the timer entry
//  will not be accessed or changed before it is ready.
//---------------------------------------------------------------------
//@ PreCondition
//  (TimerTbl::IsValidId(msgTimerId) == TRUE)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimerTbl::Set_(const Int32 msgTimerId)
{
	CLASS_PRE_CONDITION( TimerTbl::IsValidId_(msgTimerId) );
	TimerTbl::MsgTimerTable_[msgTimerId].isSet = FALSE;
	TimerTbl::MsgTimerTable_[msgTimerId].ticsLeft =
				TimerTbl::MsgTimerTable_[msgTimerId].msec;
	TimerTbl::MsgTimerTable_[msgTimerId].isSet = TRUE;
					// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Set_
//
//@ Interface-Description
//  Takes an Int32 that identifies the MsgTimer and an updated millisecond
//  count that activates the timer entry using the new timer count.
//  Returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the timer to FALSE compute the new tick value and store it then set 
//  the timer to TRUE to activate it.
//  The MsgTimerTask has the highest priority of any task in the table.
//  By first setting the timer to FALSE this insures that the timer entry
//  will not be accessed or changed before it is ready.
//---------------------------------------------------------------------
//@ PreCondition
//  (TimerTbl::IsValidId(msgTimerId) == TRUE)
//  (msec >= (Int32)MsgTimer::MIN_MSEC_REQUEST)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimerTbl::Set_(const Int32 msgTimerId, const Int32 msec)
{
  CLASS_PRE_CONDITION( TimerTbl::IsValidId_(msgTimerId) );
  CLASS_PRE_CONDITION(msec >= (Int32)MsgTimer::MIN_MSEC_REQUEST);
  TimerTbl::MsgTimerTable_[msgTimerId].isSet = FALSE;
  TimerTbl::MsgTimerTable_[msgTimerId].msec = 
            ( msec + ( (Int32)TimerTbl::TASK_MSEC_INTERVAL - 1 ) ) 
	      / (Int32)TimerTbl::TASK_MSEC_INTERVAL;
  TimerTbl::MsgTimerTable_[msgTimerId].ticsLeft =
            TimerTbl::MsgTimerTable_[msgTimerId].msec;
  TimerTbl::MsgTimerTable_[msgTimerId].isSet = TRUE;

	//Set_(msgTimerId);
					// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Cancel_
//
//@ Interface-Description
//  Takes an Int32 that identifies the MsgTimer and de-activates that
//  timer entry. Returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the timer to FALSE thus disabling the timer.
//---------------------------------------------------------------------
//@ PreCondition
//  (TimerTbl::IsValidId(msgTimerId) == TRUE)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimerTbl::Cancel_(const Int32 msgTimerId)
{
	CLASS_PRE_CONDITION( TimerTbl::IsValidId_(msgTimerId) );
	TimerTbl::MsgTimerTable_[msgTimerId].isSet = FALSE;
					// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MsgTimerTask
//
//@ Interface-Description
//  Tracks time for expiration and posts a message to the corresponding
//  queue. Returns nothing because it is a task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Awakens every TASK_MSEC_INTERVAL then decrements tic counters 
//  for entries that are active. If an entry has less than zero tics 
//  then send a message to the appropriate message queue. Reset periodic
//  timers and disable one shot timers.
//
//  Note: The timer task guarantees that when a timer expires it will be
//  >= to the interval specified by the user. The greatest margin of error
//  is TASK_MSEC_INTERVAL - 1.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TimerTbl::MsgTimerTask(void)
{
  TimerEntry *pEntry;

  for(;;)
  {					// $[TI1]
    Task::Delay(0,TimerTbl::TASK_MSEC_INTERVAL);

    for (int Id=1;Id<=TimerTbl::MsgTimerCount_;Id++)
    {
      pEntry = &TimerTbl::MsgTimerTable_[Id];
      if (pEntry->isSet == FALSE)
      {					// $[TI1.1]
        continue;
      }
      else
      {					// $[TI1.2]
        if (--pEntry->ticsLeft < 0)
        {				// $[TI1.2.1]
          MsgQueue::PutMsg(pEntry->qId,pEntry->msg);
          pEntry->ticsLeft = pEntry->msec;
          if (pEntry->eventType == MsgTimer::ONE_SHOT)
          {				// $[TI1.2.1.1]
            pEntry->isSet = FALSE;
          }				// $[TI1.2.1.2]
        }		// $[TI1.2.2]
      }
    } // End of For
    TaskMonitor::Report();  // Sends periodic message to Task-Monitor.

  } // End of While
  
}

#ifdef SIGMA_DEVELOPMENT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PrintTimerTable
//
//@ Interface-Description
//  none
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TimerTbl::PrintTimerTable(void)
{
    cout << "MESSAGE TIMER TABLE:" << endl;
    for (int Id=1;Id<=TimerTbl::MsgTimerCount_;Id++)
    {
        TimerEntry *pEntry = &TimerTbl::MsgTimerTable_[Id];
	cout << Id << " ";
	cout << "QId=" << (int) pEntry->qId << " ";
	cout << "QMsg=" << (int) pEntry->msg << " ";
	cout << "isSet=" << ((pEntry->isSet == FALSE) ? "FALSE " : "TRUE ");
	cout << "Type=" <<  ((pEntry->eventType == MsgTimer::ONE_SHOT) 
             ? "ONE_SHOT " : "PERIODIC ");
	cout << "Time(msec)=" 
             << (int) pEntry->msec * TimerTbl::TASK_MSEC_INTERVAL << " " << endl;
    }
    printf("\n");
}


#endif // SIGMA_DEVELOPMENT


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds its sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TimerTbl::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
    FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::TIMER_TBL,
                            lineNumber, pFileName, pPredicate);
}
