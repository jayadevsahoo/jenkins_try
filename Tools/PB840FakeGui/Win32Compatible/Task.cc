#include "StdAfx.h"
#include "Task.hh"

Task::Task(void)
{
}

Task::~Task(void)
{
}


void Task::Delay(const Uint32 seconds, const Uint32 mils)
{
	Sleep(seconds * 1000 + mils);
}

Boolean Task::IsTaskingOn()
{
	return true;
}