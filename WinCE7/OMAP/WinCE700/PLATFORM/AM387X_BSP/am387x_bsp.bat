REM --------------------------------------------------------------------------
REM Build Environment
REM --------------------------------------------------------------------------

REM Always copy binaries to flat release directory
set WINCEREL=1
REM Generate .cod, .lst files
set WINCECOD=1

REM ----OS SPECIFIC VERSION SETTINGS----------

if "%SG_OUTPUT_ROOT%" == "" (set SG_OUTPUT_ROOT=%_PROJECTROOT%\cesysgen) 

set _PLATCOMMONLIB=%_PLATFORMROOT%\common\lib
set _KITLLIBS=%SG_OUTPUT_ROOT%\oak\lib

set BSP_WCE=1

REM This is to be used in COMMON_TI_V1\COMMON_TI\BOOT\SDHC\sdhc.c
set BSP_AM387X=1
set BSP_NOTOUCH=1

REM TI BSP builds its own ceddk.dll. Setting this IMG var excludes default CEDDK from the OS image.
rem set IMGNODFLTDDK=1
