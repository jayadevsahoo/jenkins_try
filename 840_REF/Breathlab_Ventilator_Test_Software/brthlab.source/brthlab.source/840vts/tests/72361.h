/*****************************************************************************/
/*   Module Name:    72361.h                                           
/*   Purpose:      Purpose                                 
/*                                                                          
/*   Modification History:
/*
/*   Description of Change			        By			      Date 
/*   ---------------------					--------		  ------
/*   Removed the Metabolic Port Test.       Rhomere Jimenez   05 Oct, 2007
/*
/*	 All the functions dependent on 		Hamsavally(CGS)	  17 Oct, 2000
/*   test executive test data and	        		 
/*	 error structure are removed and 
/*	 equivalent functionality is 
/*	 included.
/*                                                                           
/*   Added Serial Loopbak Test              Dave Richardson   18 Nov, 2002
/*   (Manual Tests Other)
/*
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.2      $    
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
/*****************************************************************************/

#ifndef _72361_H
#define _72361_H

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include "840Cmds.h"  // VENTDATA  typedef
#include "testing.h"


/***********************************************/
/****************** DEFINE'S *******************/
/***********************************************/
#define AIR_FLUSH          33 // for GasFlowPerformanceTest() & FlushGas()
#define O2_FLUSH           32 // for GasFlowPerformanceTest() & FlushGas()
#define GAS_TIMEOUT        10 // for GasFlowPerformanceTest() & FlushGas()
#define NO_PROMPT          -2 // for OpenSeq & DEMOMODE:CircuitPressureTest()


#define VOLUME_TIMEOUT     50 // for VolumePerformanceTest()

#define CP_TEST_POINTS      3 // for CircuitPressureTest()
#define CP_RETURN_POINTS   10 // for CircuitPressureTest()

#define TEST_TIMEOUT      180 // for AtmospherePressureTest()
//#define USE_DEMO_TIMEOUT	 
#define DEMO_TIMEOUT        5 // for DEMOMODE:multiple tests
#define MAX_TRIES_ALLOWED  10 // for multiple tests
#define O2_WAIT_TIME       25 // for FIO2PerformanceTest()



/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/* Setup/Cleanup  Functions */
void LogStatus(char *timeStamp, char *fmt, ...);
void LogError( char *Title, char* ErrMsg);
void AppErr(int ErrorNo, double reading);
void ShutDown(void) ;

/* Test Functions */
void PostRevision( void  );
void AtmospherePressureTest(  void   );
void BreathDeliveryAlarmTest(void);
void GuiAlarmTest( void   );
void CircuitPressureTest( void     );
void VolumePerformanceTest( void);
void PressureControlPerformanceTest(void);
void PEEPPerformanceTest( void          );
void FIO2PerformanceTest( void);
void GasFlowPerformanceTest(int selection);

int ProcessManualTestOptional(int panel);
int ProcessManualTestElectrical(int panel);
int ProcessManualTestGround(int panel);
int ProcessManualTestRegulator(int panel);
int ProcessManualTestEST_SST(int panel);
int ProcessManualTestOther(int panel);
int ProcessManualTestPanel(int panel);
void ManualPerformanceTest(void);
int CVICALLBACK ManualPnlCB(int panel, int event, void *callbackData,
		int eventData1, int eventData2);

/* Gas,Volume Support Functions */
int VerifyVolume(double btps, double spiro,
                 int currentVolumeTest, int log);

/* instruction panels Support Functions */
void SetPerfPanelFIO2(    int whichOne);
void SetPerfPanelPeep(    int whichOne);
void SetPerfPanelPressure(int whichOne);
void SetPerfPanelVolume(  int whichOne);
void OpenSeq( void );
int StartSeq( void );
void StopSeq( void );


#endif // #ifndef _72361_H
