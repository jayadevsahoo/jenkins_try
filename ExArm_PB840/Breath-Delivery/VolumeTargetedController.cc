#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmtted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VolumeTargetedController - Implements a volume targeted trajectory
//				controller.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class provides services to control the pressure trajectory such 
//	that a specified volume is delivered.  Methods are provided to reset
//	the controller and to determine the pressure trajectory.
//---------------------------------------------------------------------
//@ Rationale
//	This class implements the algorithms to control pressure in the
//	patient circuit to achieve the specified volume.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The controller is invoked each volume targeted breath (VTPC or VS) 
//	to determine the targeted pressure.  The reset method is called by
//	the VolumeTargetedManager class.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance is allowed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/VolumeTargetedController.ccv   10.7   08/17/07 09:45:34   pvcs  
//
//@ Modification-Log
//
//  Revision: 007   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 006  By:  gdc    Date:  10-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - removed VC+ low inspiratory alarm event
//
//  Revision: 005  By:  gfu    Date:  30-May-2002    DR Number: 6011
//  Project:  VTPC
//  Description:
//	Removed declaration of low insp pressure alarm condition for VS breaths.
//
//  Revision: 004  By:  jja    Date:  27-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//		Added reset target pressure when transite to VTPC startup process.
//
//  Revision: 003  By:  gfu    Date:  19-Mar-2002    DR Number: 5996
//  Project:  VTPC
//  Description:
//		Added declaration of compliance limited alarm
//
//  Revision: 002  By:  quf    Date:  24-Jan-2001    DR Number: 5790
//  Project:  VTPC
//  Description:
//		Added handling of blocked wye during volume targeted breaths.
//
//  Revision: 001  By:  syw    Date:  03-Sep-2000    DR Number: 5755
//       Project:  VTPC
//       Description:
//             Initial version
//
//=====================================================================

#include "VolumeTargetedController.hh"

#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "MathUtilities.hh"
#include "PhasedInContextHandle.hh"
#include "Peep.hh"
#include "BdAlarms.hh"
#include "VolumeTargetedManager.hh"
#include "CircuitCompliance.hh"
#include "BreathSet.hh"
#include "PhaseRefs.hh"
#include "VcvPhase.hh"
  
//
//@ Code...

#ifdef SIGMA_PAV_UNIT_TEST
extern BdAlarmId::BdAlarmIdType BdAlarmId_UT ;
#endif // SIGMA_PAV_UNIT_TEST			

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VolumeTargetedController()  
//
//@ Interface-Description
//		Default Constructor.  Data members are initialized.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Data members are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

VolumeTargetedController::VolumeTargetedController(void)
{
	CALL_TRACE("VolumeTargetedController::VolumeTargetedController(void)") ;

	// $[TI1]
	reset() ;

	cPatientFiltered_ = 0.0 ;
	cPatient_ = 0.0 ;
	prevTarget_ = 0.0 ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VolumeTargetedController()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

VolumeTargetedController::~VolumeTargetedController(void)
{
	CALL_TRACE("VolumeTargetedController::~VolumeTargetedController(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPressureTrajectory()  
//
//@ Interface-Description
//	This method has volume target, alpha, the proportional gain, and the
//	lower limit as arguments and returns the calculated pressure trajectory.
//  This method is called to get the pressure trajectory required to deliver
//	the volume target.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The patient compliance is calculated and then filtered using an alpha
//	filter.  The filtered value is then used to determine the pressure error
//	based on the volume error.  The pressure trajectory is calculated
//	based on proportional feedback of the pressure error.  Limits on the
//	change of the pressure trajectory are made and alarms are notified.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
VolumeTargetedController::getPressureTrajectory(
								const Real32 volumeTarget,
								const Real32 alpha,
								const Real32 kp,
								const Real32 lowerLimit)
{
	CALL_TRACE("VolumeTargetedController::getPressureTrajectory( \
								const Real32 volumeTarget, \
								const Real32 alpha, \
								const Real32 kp, \
								const Real32 lowerLimit)") ;
								
	const Real32 DELTA_P = RVolumeTargetedManager.getDeltaPLimited();
	const Real32 INSP_VOLUME = RVolumeTargetedManager.getInspLungVolumeLimited() ;
	
	// $[VC24003]
	cPatient_ = INSP_VOLUME / DELTA_P ;

	const Real32 IBW_VALUE =
				PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value ;

	const Real32 MIN_C = RVolumeTargetedManager.getMinPatientCompliance();
	const Real32 MAX_C = RVolumeTargetedManager.getMaxPatientCompliance();

	Real32 newTarget ;

	Boolean volumeChanged = !IsEquivalent( prevVolumeTarget_, volumeTarget, HUNDREDTHS) ;

	Real32 error = 1000.0 ;
	
	// $[VC24004]
	Real32 exhVolume = RVolumeTargetedManager.getExhLungVolume() ;
	
	if(RVolumeTargetedManager.getVtiLastBreath())
	{
		newTarget = prevTarget_;
	}	
	else if ( (breathNumber_ >= 5 &&
			(exhVolume < 0.25 * INSP_VOLUME - 15.0) &&
			!volumeChanged) ||
			(cPatient_ < MIN_C) )
	{
		// $[TI5]
		suspectCounter_++ ;
		newTarget = prevTarget_ ;
	}
 	else
	{
		// $[TI6]
		suspectCounter_ = 0 ;

		// $[VC24005]
		if (cPatient_ > MAX_C)
		{
			// $[TI8]
			cPatient_ = MAX_C ;
		}
		// $[TI9]

		if (resetOccurred_)
		{
			// $[TI10]
			// $[VC24011]
			cPatientFiltered_ = cPatient_ ;
			prevTarget_ = DELTA_P ;
			resetOccurred_ = FALSE ;
		}
		// $[TI11]

		// $[VC24006]
		cPatientFiltered_ = alpha * cPatientFiltered_ + (1.0F - alpha) * cPatient_ ;

                
		// $[VC24007]
		error = (volumeTarget - INSP_VOLUME) / cPatientFiltered_ ;

		Real32 limit = 0.0 ;

		// $[VC24009]
		if (breathNumber_ < 5 || volumeChanged)
		{
			// $[TI12]
			if (IBW_VALUE < 15.0)
			{
				// $[TI13]
				limit = 3.0 ;
			}
			else if (IBW_VALUE >= 15.0 && IBW_VALUE < 25.0)
			{
				// $[TI14]
				limit = 6.0 ;
			}
			else
			{
				// $[TI15]
				limit = 10.0 ;
			}
		}
		else
		{
			// $[TI16]
			limit = 3.0 ;
		}

		if (kp * error > limit)
		{
			// $[TI17]
			error = limit / kp ;
		}
		else if (kp * error < -limit)
		{
			// $[TI18]
			error = -limit / kp ;
		}
		// $[TI19]
	
		// $[VC24008]
		newTarget = prevTarget_ + kp * error ;
	}
 
        // $[VC24022]
        // Get inspiratory time from previous breath and calculate tube compliance.
        // getComplianceVolume is called with 1.0 as the delta pressure value so it 
        // is not applied to the computation per the requirement.
        Int32 inspTime = (Int32) (RBreathSet.getLastBreathRecord())->getInspTime();
        Real32 cTubing = RCircuitCompliance.getComplianceVolume (1.0, inspTime, 
             		    CircuitCompliance::TOTAL_COMPLIANCE, FALSE); 
        Real32 factor = RVcvPhase.getComplLimitFactorValue(); 

        if (cPatientFiltered_ < (cTubing/factor))
        {
	       	// Declare compliance limit alarm and use previous target
            	RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_VOLUME_LIMIT);
               	newTarget = prevTarget_ ;
        }
 
	Real32 hcp = PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS).value ;
	Real32 peep = RPeep.getActualPeep() ;

	// $[VC24010]
	if (newTarget > hcp - 3.0 - peep)
	{	
		// $[TI20]
		newTarget = hcp - 3.0F - peep ;

		if (RVolumeTargetedManager.getTestBreathState() == VolumeTargetedManager::VT_BASED)
		{
			// $[TI25]
			RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_VCP_VOLUME_NOT_DELIVERED) ;
#ifdef SIGMA_PAV_UNIT_TEST
			BdAlarmId_UT = BdAlarmId::BDALARM_VCP_VOLUME_NOT_DELIVERED ;
#endif // SIGMA_PAV_UNIT_TEST			
		}
		else if (RVolumeTargetedManager.getTestBreathState() == VolumeTargetedManager::VS_BASED)
		{
			// $[TI26]
			RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_VS_VOLUME_NOT_DELIVERED) ;
#ifdef SIGMA_PAV_UNIT_TEST
			BdAlarmId_UT = BdAlarmId::BDALARM_VS_VOLUME_NOT_DELIVERED ;
#endif // SIGMA_PAV_UNIT_TEST			
		}
		else
		{
			AUX_CLASS_ASSERTION_FAILURE( RVolumeTargetedManager.getTestBreathState()) ;
		}
	}
	else if (newTarget < lowerLimit)
	{
		// $[TI21]
		newTarget = lowerLimit ;

	}
	// $[TI22]
	
	prevTarget_ = newTarget ;
	prevVolumeTarget_ = volumeTarget ;

	// $[VC24004]
	if (volumeChanged)
	{
		// $[TI23]
		breathNumber_ = 0 ;
	}
	else
	{
		// $[TI24]
		breathNumber_++ ;
	}
	
	return( newTarget) ;
}								
	
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VolumeTargetedController::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, VOLUMETARGETEDCONTROLLER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================


