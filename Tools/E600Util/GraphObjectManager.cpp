#include "StdAfx.h"
#include "GraphObjectManager.h"

GraphObjectManager::GraphObjectManager(void)
{
	MemDC_ID_Base_ = 0;
}

GraphObjectManager::~GraphObjectManager(void)
{
}

GraphObjectManager* GraphObjectManager::getInstance()
{
	if ( Instance_ == NULL)
	{
		Instance_ = new GraphObjectManager;
	}

	return Instance_;
}

MEMDC_OBJ_ID GraphObjectManager::createMemDCObj( CDC* pDC, int width, int height )
{
	MEMDC_OBJ memDCObj;
	ZeroMemory( &memDCObj, sizeof(MEMDC_OBJ) );

	CDC* pMemDC = new CDC;
	pMemDC->CreateCompatibleDC( pDC );

	CBitmap* pBitmap = new CBitmap;
	pBitmap->CreateCompatibleBitmap( memDCObj.pMemDC, width, height );

	memDCObj.pOldBmp = pMemDC->SelectObject( pBitmap );
	memDCObj.pBmp = pBitmap;
	memDCObj.pMemDC = pMemDC;
	memDCObj.width = width;
	memDCObj.height = height;
	Map_MemDC_Objs_[MemDC_ID_Base_] = memDCObj;

	return MemDC_ID_Base_ ++;
}

void GraphObjectManager::releaseMemDCObj( MEMDC_OBJ_ID objId )
{
	MEMDC_OBJ memDCObj = Map_MemDC_Objs_[ objId ];
	Map_MemDC_Objs_.erase( objId );
	CDC* pMemDC = memDCObj.pMemDC;
	CBitmap* pBitmap = memDCObj.pBmp;

	pMemDC->SelectObject( memDCObj.pOldBmp );
	pMemDC->DeleteDC();
	pBitmap->DeleteObject();

	delete pMemDC;
	delete pBitmap;
}

GraphObjectManager* GraphObjectManager::Instance_ = NULL;
