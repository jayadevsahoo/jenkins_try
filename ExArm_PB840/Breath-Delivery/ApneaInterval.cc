#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ApneaInterval - manages changes to the actual apnea interval.
//---------------------------------------------------------------------
//@ Interface-Description
// The ApneaInterval object controls the apnea timer to manage the actual
// apnea interval, and the apnea timer elapsed time. This class is able to
// check for apnea conditions, but is not responsible to detect apnea
// triggering.
// The actual apnea interval in use may be different from the setting value
// because of extensions made due to expiratory pause or mode change.
//---------------------------------------------------------------------
//@ Rationale
// The actual apnea interval is not always the same as the set apnea interval.
// This function is responsible to provide access to the actual apnea
// interval, and to handle temporary interval duration changes for mode changes
// and apnea events. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Stores for the actual apnea interval, for a flag that signals a mode change
// request, and for a constant that specifies the difference between the set
// apnea interval value and the actual assigned value - are provided.
// Functionality is provided for:
//	-- BD secondary cycle activity.
//     Every cycle the apnea interval is checked for new set value.
//	-- Start of a new phase. At start of inspiration the set apnea interval is
// phase-in unconditionaly, at start of exhalation, the apnea interval is extended
// if there exist a request for extension. In both cases the apnea timer gets updated.
//	-- Apnea interval management. Unconditional extension service is provided.
//	-- Extension for mode change is implemented - using the APNEA_DELAY constant.
//	-- Access methods are provided for the apnea interval, to change the apnea interval
// elapsed time, to change the actual apnea interval, and to set the actual apnea interval.
//	-- A service to check for apnea conditions is provided.
// When the set apnea interval is equal to the breath period - apnea should
// not be declared. This is implemented by making sure that the breath period exceeded
// the apnea interval by a threshold DELTA_. 
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class may be created
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ApneaInterval.ccv   25.0.4.0   19 Nov 2013 13:59:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 011   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added new RM maneuver breath phase types.
//
//  Revision: 010   By: syw   Date:  15-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Changes to accommodate PAV_INSPIRATORY_PAUSE enum.
//
//  Revision: 009  By: healey     Date:  29-Jan-1999    DR Number: 5317
//  Project:  ATC
//  Description:
//      Added extending actualApneaInterval_ by adding DELTA_ when 
//      apnea interval time left is less than APNEA_DELAY.
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By:  yyy    Date: 06-Jan-1998     DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//          Initial release for Bilevel.
//
//  Revision: 006  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 005  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal Breath Delivery code review.
//
//  Revision: 004 By:  iv   Date:   09-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Since the introduction of peep recovery phase, spontaneous mode
//             always starts with inspiration and there is no need to have the
//             noInspiration flag. The flag was eliminated together with the
//             previous change to set the interval temporarily to 10 seconds.
//
//  Revision: 003 By:  iv   Date:   05-June-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Modified newExhalation_() to check for spont scheduler after
//             power up. In that case, the apnea interval is temporary adjusted
//             to 10 seconds.
//
//  Revision: 002 By:  iv   Date:   15-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

# include "ApneaInterval.hh"
# include "BreathMiscRefs.hh"

//@ Usage-Classes
# include "BreathSet.hh"
# include "BreathRecord.hh"
# include "PhasedInContextHandle.hh"
# include "PendingContextHandle.hh"
# include "BreathPhaseScheduler.hh"
#include "SettingConstants.hh"
//@ End-Usage

//@ Code...


//@ Constant: APNEA_DELAY
// 5 seconds delay
static const Int32 APNEA_DELAY = 5000; // milliseconds

// $[04121] Static member initialization
const Int32 ApneaInterval::DELTA_ = 350; // milliseconds

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ApneaInterval()  [Default Contstructor]
//
//@ Interface-Description
// Construct the object
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ApneaInterval::ApneaInterval(void)
: isModeChangeRequest_(FALSE),
	isApneaDetectionOff_(FALSE)
{
// $[TI1]
	CALL_TRACE("ApneaInterval(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
	//@ Method: ~ApneaInterval()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ApneaInterval::~ApneaInterval(void)
{
  CALL_TRACE("~ApneaInterval()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle(void)
//
//@ Interface-Description
// $[02070]
// Every new cycle the set value of the apnea interval is checked. If the
// set value is greater than the actual value, the new value is phased-in
// and the apnea timer is set for a new target time. If apnea detection
// is being turned off then turn off the apnea timer. If apnea detection
// is off and is now being turned on then set new target time and restart
// the apnea timer.
//---------------------------------------------------------------------
//@ Implementation-Description
// The member actualApneaInterval_ is compared to the set value derived from
// the PhasedInContext instance. The new value for the actual interval is
// used to set a new target for the instance of the apnea timer RApneaTimer.
// $[LC02001]\f\ if Ta is OFF, upon exiting CPAP, reeanble apnea detection immediately
// $[LC04001]\b\ with Ta=OFF, apnea detection shall be disabled
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================


void
ApneaInterval::newCycle(void)
{
	CALL_TRACE("newCycle(void)");

	Int32 apneaIntervalSetValue =
	 (Int32)(PhasedInContextHandle::GetBoundedValue(SettingId::APNEA_INTERVAL).value);

	if	( !isApneaDetectionOff_ )
	{   
		if ( apneaIntervalSetValue == DEFINED_UPPER_ALARM_LIMIT_OFF ) // ON to OFF
		{
			isApneaDetectionOff_ = TRUE;
			RApneaTimer.stop();
			// even though apnea detection is turned OFF, we set actualApneaInterval_ 
			// to a large number (DEFINED_UPPER_ALARM_LIMIT_OFF) so the breath 
			// schedulers and triggers see a large number in calculating apnea interval 
			// times that can be factored out of their calculations
			actualApneaInterval_ = apneaIntervalSetValue + DELTA_;
		}
		else if ( actualApneaInterval_ - DELTA_ < apneaIntervalSetValue ) // ON to longer ON
		{
			// $[04121] The previous interval is smaller than the new interval 
			// so phase in the value for the new interval immediately:
			actualApneaInterval_ = apneaIntervalSetValue + DELTA_;

			// update the apnea interval timer with the extended interval,
			// but don't restart timer:
			RApneaTimer.setTargetTime(actualApneaInterval_);
		}
		// else -- ON to shorter ON - new interval is smaller than current interval 
		// - phase in on next inspiration - no action required here
	}
	else  
	{
		if ( apneaIntervalSetValue != DEFINED_UPPER_ALARM_LIMIT_OFF )  // OFF to ON
		{
			isApneaDetectionOff_ = FALSE;
			actualApneaInterval_ = apneaIntervalSetValue + DELTA_;
			RApneaTimer.setTargetTime(actualApneaInterval_);
			RApneaTimer.restart();
		}
		// else - OFF to OFF - do nothing
	}
	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newPhase
//
//@ Interface-Description
// This method services any client that is responsible for starting a new
// phase. The method takes an argument of type BreathPhaseType::PhaseType
// and has no return value.
// A call to process new inspiration is made for phase type equal INSPIRATION.
// A call to process new exhalation is made for phase type equal EXHALATION. 
//---------------------------------------------------------------------
//@ Implementation-Description
// A simple if-else statement is implemented to determine whether to invoke
// the newInspiration or the newExhalation method.
//---------------------------------------------------------------------
//@ PreCondition
//	The value of the argument - phase is checked for the proper value.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaInterval::newPhase(const BreathPhaseType::PhaseType phase)
{
	CALL_TRACE("ApneaInterval::newPhase(const BreathPhaseType::PhaseType phase, const Boolean noInspirationPhase)");

	if(phase == BreathPhaseType::INSPIRATION)
	{
	// $[TI1]
		newInspiration_();
	}
	else if(phase == BreathPhaseType::EXHALATION)
	{
	// $[TI2]
		newExhalation_();
	}
	else
	{
	// $[TI3]
		CLASS_PRE_CONDITION((phase == BreathPhaseType::INSPIRATION) ||
								(phase == BreathPhaseType::EXHALATION));
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: extendIntervalForModeChange
//
//@ Interface-Description
// This method takes no arguments and has no return value. It extends the apnea
// interval only if current phase is exhalation. If current phase is
// INSPIRATION or NON_BREATHING, it sets a flag to signal a future change.
// To extend the apnea interval, the time left for apnea detection is
// calculated (with reference to the start of exalation). If time left for
// detection is less than a fixed minimum value, the interval is extended to
// that minimum.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[04231] $[04248]
// A switch statement is implemented for the three possible breath phases.
// The private member isModeChangeRequested_ is set to TRUE during inspiration or
// non breathing.
// The current breath record is used to get the current inspiration time.
// If time for apnea detection is less than APNEA_DELAY, when measured from
// start of exhalatation, the apnea interval is extended. A new Target time is
// set for the apnea timer instance RApneaTimer. 
//---------------------------------------------------------------------
//@ PreCondition
// (currentPhase == BreathPhaseType::INSPIRATION) ||
//	(currentPhase == BreathPhaseType::EXHALATION) ||
//	(currentPhase == BreathPhaseType::INSPIRATORY_PAUSE) ||
//	(currentPhase == BreathPhaseType::PAV_INSPIRATORY_PAUSE) ||
//	(currentPhase == BreathPhaseType::EXPIRATORY_PAUSE) ||
//	(currentPhase == BreathPhaseType::NIF_PAUSE) ||
//	(currentPhase == BreathPhaseType::P100_PAUSE) ||
//	(currentPhase == BreathPhaseType::VITAL_CAPACITY_INSP) ||
//	(currentPhase == BreathPhaseType::BILEVEL_PAUSE_PHASE) ||
//	(currentPhase == BreathPhaseType::NON_BREATHING)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
ApneaInterval::extendIntervalForModeChange(void)
{
	CALL_TRACE("extendIntervalForModeChange(void)");

	BreathPhaseType::PhaseType currentPhase = BreathRecord::GetPhaseType();

	switch(currentPhase)
	{
		case BreathPhaseType::INSPIRATION:
		// fall through to next case
		case BreathPhaseType::NON_BREATHING:
		// $[TI1]
			//flag the request for apnea interval extension 
			isModeChangeRequest_ = TRUE;
		break;
		
		case BreathPhaseType::EXHALATION:
		{
			// $[TI2]
			//determine the inspiration duration for the current breath:
			Int32 inspirationDuration =
				(Int32)BreathRecord::GetInspiratoryTime();

			//if time left for apnea detection ( when measured from start of 
			// exhalation ) is less than APNEA_DELAY time,
			//extend the apnea interval by the difference plus DELTA_.
			if (!isApneaDetectionOff_ && (actualApneaInterval_ - inspirationDuration < APNEA_DELAY))
			{
			// $[TI2.1]
				actualApneaInterval_ = APNEA_DELAY + inspirationDuration + DELTA_;
				RApneaTimer.setTargetTime(actualApneaInterval_);
			}
			// $[TI2.2]

			// reset the mode transition flag:
			isModeChangeRequest_ = FALSE;
 		}
 		break;

		case BreathPhaseType::INSPIRATORY_PAUSE:
		case BreathPhaseType::PAV_INSPIRATORY_PAUSE:
		case BreathPhaseType::EXPIRATORY_PAUSE:
		case BreathPhaseType::NIF_PAUSE:
		case BreathPhaseType::P100_PAUSE:
		case BreathPhaseType::VITAL_CAPACITY_INSP:
		case BreathPhaseType::BILEVEL_PAUSE_PHASE:
			// do nothing
		break;

	default:
		AUX_CLASS_ASSERTION_FAILURE( currentPhase );
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: extendIntervalForPeepReduction
//
//@ Interface-Description
// This method takes no arguments and has no return value. It extends the apnea
// interval only if current phase is Exhalation and BlTransitionState is changing
// from high to low.  To extend the apnea interval, the time left for apnea
// detection is calculated (with reference to the start of exhalation). If time
// left for detection is less than a fixed minimum value, the interval is
// extended to that minimum.
//---------------------------------------------------------------------
//@ Implementation-Description
// A switch statement is implemented for the exhalation breath phases.
// The current breath record is used to get the current breath duration time.
// If time for apnea detection is less than APNEA_DELAY, when measured from
// start of exhalation, the apnea interval is extended. A new Target time is
// set for the apnea timer instance RApneaTimer. 
//---------------------------------------------------------------------
//@ PreCondition
// (currentPhase == BreathPhaseType::INSPIRATION) ||
//	(currentPhase == BreathPhaseType::EXHALATION) ||
//	(currentPhase == BreathPhaseType::NON_BREATHING)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
ApneaInterval::extendIntervalForPeepReduction(void)
{
	CALL_TRACE("extendIntervalForPeepReduction(void)");

	BreathPhaseType::PhaseType currentPhase = BreathRecord::GetPhaseType();

	switch(currentPhase)
	{
		case BreathPhaseType::EXHALATION:
		{
		// $[TI1]
		    // Get the blTransitionState from the BreathPhaseScheduler.
	    	const BreathPhaseScheduler::BlTransitionState blTransitionState =
    				BreathPhaseScheduler::GetBlPeepTransitionState();

			if (blTransitionState == BreathPhaseScheduler::HIGH_TO_LOW)
			{
			// $[TI1.1]
				//determine the breath duration for the current breath:
    	        Int32  breathDuration = (Int32)(RBreathSet.getCurrentBreathRecord())->getBreathDuration();

				// if time left for apnea detection ( when measured from start of 
				// exhalation ) is less than APNEA_DELAY time,
				// extend the apnea interval by the difference plus DELTA_.
				if (!isApneaDetectionOff_ && (actualApneaInterval_ - breathDuration < APNEA_DELAY))
				{
				// $[TI1.1.1]
					actualApneaInterval_ = APNEA_DELAY + breathDuration + DELTA_;
					RApneaTimer.setTargetTime(actualApneaInterval_);
				}
				// $[TI1.1.2]
			}
			// $[TI1.2]
 		}
 		break;
		default:
		// $[TI2]
			CLASS_PRE_CONDITION( (currentPhase == BreathPhaseType::INSPIRATION) ||
				(currentPhase == BreathPhaseType::EXHALATION) ||
				(currentPhase == BreathPhaseType::NON_BREATHING) );
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ApneaInterval::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, APNEAINTERVAL,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newInspiration_(void)
//
//@ Interface-Description
// The method takes no arguments and has no return value. The actual apnea
// interval is set and the apnea timer is loaded with the new interval and then
// restarts.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[04122]
// The PhasedInContextHandle class is used to get the set apnea interval value
// which is then assigned to the member actualApneaInterval_ and loaded to
// the apnea timer object: RApneaTimer. 
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
ApneaInterval::newInspiration_(void)
{
// $[TI1]
	CALL_TRACE("newInspiration_()");

	Int32 apneaIntervalSetValue =
  	 (Int32)(PhasedInContextHandle::GetBoundedValue(SettingId::APNEA_INTERVAL).value);

	// $[04118] $[04121] 
  	actualApneaInterval_ = apneaIntervalSetValue + DELTA_;
    // update the apnea interval timer:
    RApneaTimer.setTargetTime(actualApneaInterval_);

	isApneaDetectionOff_ = (apneaIntervalSetValue == DEFINED_UPPER_ALARM_LIMIT_OFF);

	if (isApneaDetectionOff_)
	{
		RApneaTimer.stop();
	}
	else
	{
		// start the timer:
		RApneaTimer.restart();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newExhalation_
//
//@ Interface-Description
// This method is called at the start of every exhalation. It takes no 
// arguments. It extends
// the apnea interval only if a request for a mode change has been made. To
// extend the apnea interval, the time left for apnea detection is calculated
// (with reference to the start of exhalation). If time left for detection is
// less than a fixed minimum value, the interval is extended to that minimum
// plus DELTA_.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[04231] $[04248]
// The private member isModeChangeRequest_ is checked to detect any previous
// mode change request. The current breath record is used to get the current
// inspiration time. If time for apnea detection is less than APNEA_DELAY,
// the apnea interval is extended. A new Target time is set for the apnea timer
// instance RApneaTimer. 
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ApneaInterval::newExhalation_(void)
{
	CALL_TRACE("ApneaInterval::newExhalation_(void)");

	if (isModeChangeRequest_)
	{
		//determine the inspiration duration for the current breath:
		Int32 inspirationDuration =	(Int32)BreathRecord::GetInspiratoryTime();
	
		//if time left for apnea detection is less than APNEA_DELAY time,
		//extend the apnea interval by the difference.
		if(!isApneaDetectionOff_ && (actualApneaInterval_ - inspirationDuration < APNEA_DELAY))
		{
		// $[TI1.1]
			actualApneaInterval_ = APNEA_DELAY + inspirationDuration + DELTA_;
			RApneaTimer.setTargetTime(actualApneaInterval_);
		}
		// $[TI1.2]

		// reset the mode transition flag:
		isModeChangeRequest_ = FALSE;
	}
	// $[TI2]
}		 		

