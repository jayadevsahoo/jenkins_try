
#ifndef Plot2TypeValue_HH
#define Plot2TypeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Plot2TypeValue - Values of the Plot 2 Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Plot2TypeValue.hhv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah   Date:  21-Dec-1998    DR Number: 5314
//  Project:  ATC
//  Description:
//     As part of fixing this DCS, I noticed a typo below.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct Plot2TypeValue
{
  //@ Type:  Plot2TypeValueId
  // All of the possible values of the Plot 2 Type Setting.
  enum Plot2TypeValueId
  {
    //-----------------------------------------------------------------
    //  NOTE:  the values of Plot #2's 'PRESSURE_VS_TIME', 'VOLUME_VS_TIME',
    //         and 'FLOW_VS_TIME' enumerators must match the corresponding
    //         values of Plot #1; various implementations expect this.
    //-----------------------------------------------------------------

    // $[01151] -- values of the range of Plot #2 Type Setting...
    PRESSURE_VS_TIME	= 0,
    VOLUME_VS_TIME	= 1,
    FLOW_VS_TIME	= 2,
    WOB_GRAPHIC, 
    NO_PLOT2,
    
    TOTAL_PLOT2_TYPES  
  };
};


#endif // Plot2TypeValue_HH
 
