#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====

//@ Class: KeyPanel - Routes events from off-screen keys to application objects.
//---------------------------------------------------------------------
//@ Interface-Description
// The purpose of this all-static class is to provide a framework for the
// triggering of application code in response to pressing or releasing any of
// the off-screen keys provided by the GUI hardware.  While all off-screen keys
// are received here, the Accept and Clear key events are processed elsewhere,
// by the AdjustPanel class.
//
// The KeyPanel class receives its input (key event notification) from outside
// the GUI-Foundations subsystem.  The external caller calls the
// KeyPressHappened() method when a key is pressed.  The external caller calls
// the KeyReleaseHappened() method when a key is released.  The KeyPanel class
// routes the events for each off-screen key to the appropriate object.  That
// object is known as the target.  There can be at most one target object for
// each off-screen key.  To be a target, an object must be derived from the
// KeyPanelTarget class.
//
// A KeyPanelTarget-derived object becomes a target for a given
// off-screen key by calling the SetCallBack() method of KeyPanel,
// passing a pointer to itself and the key index of the given key as
// parameters.  The previous target is not notified that it is no longer
// the target.
//
// Once a callback target is registered for a given key, pressing that
// key causes the keyPanelPressHappened() method of the target to be
// called.  When the key is released, the keyPanelReleaseHappened()
// method is called.  The key index is passed as a parameter.
//
// This class also provides a method allowing the application to control
// the LED indicators associated with off-screen keys.  It is the
// responsibility of the application to use this method; LEDs are not
// automatically turned on or off as a part of keypress processing in
// this class.
//---------------------------------------------------------------------
//@ Rationale
// The KeyPanel class encapsulates the mechanism for registering objects which
// must be notified of key events, the interface to external parts of the
// system which detect key events, the decoding of the off-screen key events,
// and the mechanism for notifying the appropriate registered object of a key
// event.  It also encapsulates the mechanism for controlling the key LEDs.
// This frees the application of the need to deal with these details.
//
// A single controlling class for all off-screen keys was determined to
// be simpler than multiple instances of a (hypothetical) OffscreenKey
// class (one for each key), since the set of keys is finite and stable.
//---------------------------------------------------------------------
//@ Implementation-Description
// The user of this class specifies individual keys with the enum KeyId, which
// lists each of the keys.  This class maintains an array of pointers to
// KeyPanelTarget-derived objects, with one entry for every member of the KeyId
// enum.  KeyPanelTarget pointers are installed in this array by calling the
// SetCallback() method.  The KeyPressHappened() method is called when any
// off-screen "TOUCH" key events are detected by external subsystems.  The
// KeyReleaseHappened() method is called when any off-screen "RELEASE" key
// events are detected by external subsystems; it decodes the key ID (based on
// the KeyId enum), and calls the appropriate method of the KeyPanelTarget
// based on the key ID.  DecodeGuiIoKey_ method decodes the GUI I/O key data in
// the passed GuiKeys enum "guiIoKeyCode" into KeyPanel KeyId field.
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/KeyPanel.ccv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 006   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 005  By: clw      Date: 20-Mar-1996   DCR Number: 1950
//    Project:  Sigma (R8027)
//    Description:
//      Added a new method EncodeGuifKey_() to translate guif's key enum
//      into GUI-IO-Devices' enum.  This is needed for sound
//      enable/disable.
//
//  Revision: 004  By: yyy      Date: 03-Jan-1996   DCR Number: 643
//    Project:  Sigma (R8027)
//    Description:
//      Removed the CLASS_PRE_CONDITION(KeyEnabled_[keyCode]) so the GUI-Apps can switch on and off
//      keypanel LEDs when the user has pressed the screen lock key.  Note: the removed code was not
//      in the previously-inspected code, so this does not represent a diff...
//
//  Revision: 003  By: yyy      Date: 17-Jul-1995   DCR Number: 1951
//    Project:  Sigma (R8027)
//    Description:
//      Replaced RIGHT_SPARE_KEY with INSP_PAUSE_KEY.
//
//  Revision: 002  By: yyy      Date: 06-Jul-1995   DCR Number: 1952
//    Project:  Sigma (R8027)
//    Description:
//      Modified due to the addition of KeyEnabled_[] static variable, which controls
//      the enabling/disabling of a specific key functions.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================
#include "KeyPanel.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "KeyPanelTarget.hh"
#include "Sound.hh"
#include "Led.hh"
//@ End-Usage

//@ Code...

//
// Static data member initializations.
//
KeyPanelTarget *KeyPanel::KeyTargets_[] = { NULL };
Boolean KeyPanel::KeyEnabled_[] = { TRUE };


// This is the only place in the software which maps all of of the GUI I/O 
// keyboard keys that may be pressed. From the KeysTask driver values between zero to 
// NUMBER_OF_GUI_KEYS are sent along the message queue. KeyMapPass[] maps each of those values to
// respective KeyId's. Order of the KeyId's are very important here. Software logically
// supports 15 buttons which is contrained by NUMBER_OF_GUI_KEYS from the hardware driver.
// No values are posted by the KeyTask driver above NUMBER_OF_GUI_KEYS. 

static KeyPanel::KeyId	KeyMapPass[] =
{
	 KeyPanel::KeyId::ACCEPT_KEY,				//	Maps to 0 
	 KeyPanel::KeyId::CLEAR_KEY,				//	Maps to 1 
	 KeyPanel::KeyId::HOME_KEY,					//	Maps to 2 .
	 KeyPanel::KeyId::MAN_INSP_KEY,				//	Maps to 3 .
	 KeyPanel::KeyId::INSP_PAUSE_KEY,			//	Maps to 4.
	 KeyPanel::KeyId::EXP_PAUSE_KEY,			//	Maps to 5.
	 KeyPanel::KeyId::ALARM_RESET_KEY,			//	Maps to 6.
	 KeyPanel::KeyId::ALARM_SILENCE_KEY,		//	Maps to 7
	 KeyPanel::KeyId::HUNDRED_PERCENT_O2_KEY ,	//	Maps to 8
	 KeyPanel::KeyId::INFO_KEY ,				//	Maps to 9.
	 KeyPanel::KeyId::ALARM_VOLUME_KEY ,		//	Maps to 10.
	 KeyPanel::KeyId::SCREEN_BRIGHTNESS_KEY ,	//	Maps to 11.
	 KeyPanel::KeyId::SCREEN_CONTRAST_KEY ,		//	Maps to 12.
	 KeyPanel::KeyId::SCREEN_LOCK_KEY ,			//	Maps to 13
	 KeyPanel::KeyId::LEFT_SPARE_KEY ,			//	Maps to 14 
};

KeyPanel::KeyId	*keyMapping;



//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [static]
//
//@ Interface-Description
// This method initializes the KeyPanel class.  This consists of setting
// the callback target for each key to NULL and setting the keyEnabled_
// Boolean array to TRUE.
//---------------------------------------------------------------------
//@ Implementation-Description
// Just sets all the key callback targets to NULL and sets the keyEnabled_
// Boolean array to TRUE via a call to EnableAllKeys(). This also enables
// key sounds for each key.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
KeyPanel::Initialize(void)
{
	CALL_TRACE("KeyPanel::Initialize(void)");

	keyMapping = KeyMapPass;

	//													$[TI1]
	for (Int i = 0; i < NUMBER_OF_KEYS; i++)
	{
		KeyTargets_[i] = NULL;
	}
	EnableAllKeys();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetCallback [static]
//
//@ Interface-Description
// Makes the KeyPanelTarget-derived object pointed to by the passed "pTarget"
// pointer the target callback object for the off-screen key specified by the
// passed "keyCode".  If it is desired to have no callback, the passed
// value for pTarget can be NULL.
//---------------------------------------------------------------------
//@ Implementation-Description
// Stores "pTarget" in the "keyCode" slot of the KeyTargets_[] array.
// Note that any existing target object callback is overwritten; the
// old target object is not informed.
//---------------------------------------------------------------------
//@ PreCondition
// The given "keyCode" must be valid.
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void
KeyPanel::SetCallback(KeyId keyCode, KeyPanelTarget* pTarget)
{
	CALL_TRACE("KeyPanel::SetCallback(KeyId keyCode, KeyPanelTarget *)");
	CLASS_PRE_CONDITION((keyCode >= 0)
							&& (keyCode < NUMBER_OF_KEYS));

	SAFE_CLASS_ASSERTION((keyCode != ACCEPT_KEY) && (keyCode != CLEAR_KEY))
	
	//													$[TI1]
	KeyTargets_[keyCode] = pTarget;		// can be NULL
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetLight [static]
//
//@ Interface-Description
// Sets the LED for the off-screen key specified by the passed "keyCode" to on
// or off, based on the value of the passed Boolean "isIlluminated".  If the
// specified off-screen key does not have an LED, then an error occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method calls the SetState() method of the static Led class to command
// the appropriate LED to turn on or off.  The mapping between the ID of the
// keycode and the corresponding LED is done here.
//---------------------------------------------------------------------
//@ PreCondition
// The given "keyCode" must be valid.  The corresponding KeyEnabled_ array
// element has to be enables.
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
KeyPanel::SetLight(KeyId keyCode, Boolean isIlluminated)
{
	CALL_TRACE("KeyPanel::SetLight(KeyId keyCode, Boolean)");
	CLASS_PRE_CONDITION((keyCode >= 0)
							&& (keyCode < NUMBER_OF_KEYS));

	// Based on the passed keyCode, turn the appropriate LED on or off.
	// Only certain keys have an LED, so make sure the caller didn't
	// specify one that has no LED.
	//
	switch (keyCode)
	{											// $[TI1]
	case SCREEN_LOCK_KEY:						// $[TI1.1]
		if (isIlluminated)
		{										// $[TI1.1.1]
			Led::SetState(Led::SCREEN_LOCK, Led::ON_STATE);
		}
		else
		{										// $[TI1.1.2]
			Led::SetState(Led::SCREEN_LOCK, Led::OFF_STATE);
		}
		break;

	case ALARM_SILENCE_KEY:						// $[TI1.2]
		if (isIlluminated)
		{										// $[TI1.2.1]
			Led::SetState(Led::ALARM_SILENCE, Led::ON_STATE);
		}
		else
		{										// $[TI1.2.2]
			Led::SetState(Led::ALARM_SILENCE, Led::OFF_STATE);
		}
		break;

	case HUNDRED_PERCENT_O2_KEY:				// $[TI1.3]
		if (isIlluminated)
		{										// $[TI1.3.1]
			Led::SetState(Led::HUNDRED_PERCENT_O2, Led::ON_STATE);
		}
		else
		{										// $[TI1.3.2]
			Led::SetState(Led::HUNDRED_PERCENT_O2, Led::OFF_STATE);
		}
		break;

	default:
		CLASS_ASSERTION(FALSE);		// caller blew it
		break;
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: KeyPressHappened
//
//@ Interface-Description
// Process the GUI I/O key data in the passed "guiIoKeyCode" when the offscreen
// key is pressed by the user and key is enabled.  The Accept and Clear keys
// are logically grouped with the Adjust Panel, so notification of key events
// for those two keys is forwarded to the AdjustPanel class.  Otherwise, if
// there is a registered target for the pressed key code, then the callback
// method of the target is called as appropriate.  If for this key there is no
// registered target, an invalid entry sound is generated.
//---------------------------------------------------------------------
//@ Implementation-Description
// Translates the GuiKeys enum field into KeyPanel KeyId field.
//
// Execute the offscreen key function only when the corresponding KeyEnabled_
// element for the offscreen key is enabled.  If an Accept key is pressed then
// forward the execution to AdjustPanel class's AcceptKeyPressHappened()
// method.  If the Clear key is pressed then forward the execution to
// AdjustPanel class's ClearKeyPressHappened() method.
//
// Otherwise, if there is a callback target for this key, executes the
// keyPanelPressHappened() method of the target. If not generate an invalid
// entry sound.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
// None.
//@ End-Method
//=====================================================================

void
KeyPanel::KeyPressHappened(GuiKeys guiIoKeyCode)
{
	CALL_TRACE("KeyPanel::KeyPressHappened(GuiKeys guiIoKeyCode)");

	KeyPanel::KeyId keyCode;

	// Translates the GuiKeys enum "guiIoKeyCode" into KeyPanel KeyId keyCode.
	keyCode = DecodeGuiIoKey_(guiIoKeyCode);

	if (KeyEnabled_[keyCode])
	{	//													$[TI1]
		if (ACCEPT_KEY == keyCode)
		{	//												$[TI1.1]
			//
			// Forward the accept key events to the AdjustPanel.
			//
			AdjustPanel::AcceptKeyPressHappened();
		}
		else if (CLEAR_KEY == keyCode)
		{	//												$[TI1.2]
			//
			// Forward the clear key events to the AdjustPanel.
			//
			AdjustPanel::ClearKeyPressHappened();
		}
		else if (KeyTargets_[keyCode] != NULL)
		{	//												$[TI1.3]
			//
			// There is a registered target associated with this key, so
			// call the appropriate callback routine.
			//
			KeyTargets_[keyCode]->keyPanelPressHappened(keyCode);
		}
		else
		{	//												$[TI1.4]
			// There is no focus for the Key so make the Invalid Entry sound.

#if defined SIGMA_UNIT_TEST
			printf("INVALID_ENTRY sound is generated\n");
#endif	// 	SIGMA_UNIT_TEST

			Sound::Start(Sound::INVALID_ENTRY);
		}
	}
	//														$[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: KeyReleaseHappened
//
//@ Interface-Description
// Process the GUI I/O key data in the passed "guiIoKeyCode" when the offscreen
// key is pressed by the user and key is enabled.  The Accept is logically
// grouped with the Adjust Panel, so notification of key events for the Accept
// key is forwarded to the AdjustPanel class.  Otherwise, if there is a
// registered target for the press key code, then the callback method of the
// target is called as appropriate.
//---------------------------------------------------------------------
//@ Implementation-Description
// Translates the GuiKeys enum field into KeyPanel KeyId field.
//
// Execute the offscreen key function only when the corresponding KeyEnabled_
// element for the offscreen key is enabled.  If an Accept key is released then
// forward the execution to AdjustPanel class's AcceptKeyReleaseHappened()
// method.  If the Clear key is released then do nothing.
//
// Otherwise, if there is a callback target for this key, executes the
// keyPanelReleaseHappened() methods of the target, else do nothing.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

void
KeyPanel::KeyReleaseHappened(GuiKeys guiIoKeyCode)
{
	CALL_TRACE("KeyPanel::KeyReleaseHappened(GuiKeys guiIoKeyCode)");

	KeyPanel::KeyId keyCode;

	// Translates the GuiKeys enum "guiIoKeyCode" into KeyPanel KeyId keyCode.
	keyCode = DecodeGuiIoKey_(guiIoKeyCode);

	if (KeyEnabled_[keyCode])
	{	//													$[TI1]
		if (ACCEPT_KEY == keyCode)
		{	//												$[TI1.1]
			//
			// Forward the accept key events to the AdjustPanel.
			//
			AdjustPanel::AcceptKeyReleaseHappened();
		}
		else if (CLEAR_KEY == keyCode)
		{	//												$[TI1.2]
			// Do nothing
		}
		else if (KeyTargets_[keyCode] != NULL)
		{	//												$[TI1.3]
			//
			// There is a registered target associated with this key, so
			// call the appropriate callback routine.
			//
			KeyTargets_[keyCode]->keyPanelReleaseHappened(keyCode);
		}
		else
		{	//												$[TI1.4]
			// There is a NO registered target associated with this key, so
			// do nothing.
		}
	}
	//														$[TI2]
}



//=====================================================================
//
// Private methods
//
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DecodeGuiIoKey_
//
//@ Interface-Description
// Decodes the GUI I/O key data in the passed "guiIoKeyCode".
// Translates the GuiKeys enum field into KeyPanel KeyId field.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
// guiIoKeyCode < NUMBER_OF_KEYS
//@ End-Method
//=====================================================================
KeyPanel::KeyId
KeyPanel::DecodeGuiIoKey_(GuiKeys guiIoKeyCode)
{
	CALL_TRACE("KeyPanel::DecodeGuiIoKey_(GuiKeys guiIoKeyCode)");
	CLASS_PRE_CONDITION(guiIoKeyCode < NUMBER_OF_KEYS)

	if(guiIoKeyCode < NUMBER_OF_KEYS)
	{
		KeyPanel::KeyId keyCode = keyMapping[guiIoKeyCode];
		return(keyCode);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EncodeGuifKey_
//
//@ Interface-Description
// Translates the key id data in the passed guifKeyCode, of type 
// enum KeyPanel::KeyId, into the GUI-IO-Devices enum GuiKeys.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
GuiKeys
KeyPanel::EncodeGuifKey_(KeyPanel::KeyId guifKeyCode)
{
    CALL_TRACE("KeyPanel::EncodeGuifKey_(KeyPanel::KeyId guifKeyCode)");

	for(Uint16 index = 0; index < KeyPanel::KeyId:: NUMBER_OF_KEYS ; index++)
	{
		if(guifKeyCode == keyMapping[index])
			return GuiKeys(index);
	}
	
	CLASS_ASSERTION(FALSE);		// caller blew it
}	
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the fault
//  macros ('CLASS_PRE_CONDITION()', etc.).  The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
KeyPanel::SoftFault(const SoftFaultID  softFaultID,
					const Uint32       lineNumber,
					const char*        pFileName,
					const char*        pPredicate)
{
	CALL_TRACE("KeyPanel::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, KEYPANEL,
							lineNumber, pFileName, pPredicate);
}
