#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: CursorSettingButton - A setting button that displays a
// formatted time.
//---------------------------------------------------------------------
//@ Interface-Description 
// This class is a SettingButton that adds functionality to the base
// class by displaying a time value in its interior. This class is
// instantiated within the TrendCursorSlider to allow the user to select
// a single record (sample) within a trend dataset. The time stamp for
// the selected trend record is provided to this button using the
// setTime() method.
//---------------------------------------------------------------------
//@ Rationale
// Contains the functionality for adjusting a setting and displaying
// a formatted time in a button.
//---------------------------------------------------------------------
//@ Implementation-Description 
// See Interface-Description. 
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/CursorSettingButton.ccv   25.0.4.0   19 Nov 2013 14:07:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By:  rhj    Date:  30-JAN-07    SCR Number: 6237
//  Project:  Trend
//  Description:
//  	Initial Release
//=====================================================================

#include "CursorSettingButton.hh"
#include "Colors.hh"
#include "MiscStrs.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SettingConstants.hh"
#include "SettingSubject.hh"
#include "TimeStamp.hh"
#include "TrendTimeScaleValue.hh"
#include "TextUtil.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_X_ = 0;
static const Int32 BUTTON_Y_ = 0;
static const Int32 BUTTON_WIDTH_ = 64;
static const Int32 BUTTON_HEIGHT_ = 22;
static const Int32 BUTTON_BORDER_ = 1;

static const Int32 BUTTON_TEXT_X_ = 0; 
static const Int32 BUTTON_TEXT_Y_ = 6; 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CursorSettingButton()  [Constructor]
//
//@ Interface-Description
// Creates an Cursor Setting Button. Passed the following arguments:
// >Von
//	settingId		The setting whose value the button modifies.
//  messageId		The help message displayed in the Message Area when
//                  this button is pressed.
//  pFocusSubScreen The SubScreen that gains focus when this button is
//                  pressed.
// >Voff 
//---------------------------------------------------------------------
//@ Implementation-Description
// Passes parameters along to SettingButton and then sets up the button
// text field that displays the time.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CursorSettingButton::CursorSettingButton(SettingId::SettingIdType settingId, 
										 StringId messageId,
										 SubScreen * pFocusSubScreen
										) 
:   SettingButton(BUTTON_X_, BUTTON_Y_, BUTTON_WIDTH_, BUTTON_HEIGHT_,
				  Button::LIGHT, BUTTON_BORDER_,
				  Button::ROUND, Button::NO_BORDER,
				  MiscStrs::EMPTY_STRING,
				  settingId, pFocusSubScreen, messageId,
				  FALSE, ::NULL_STRING_ID, ::GRAVITY_RIGHT),
	buttonText_()
{
	CALL_TRACE("CursorSettingButton(Uint16 xOrg, "
			   "Uint16 yOrg, SettingId::SettingIdType settingId, "
			   "StringId messageId)");

	// Position and size the time Stamp field

	// "- -" is displayed for an invalid/uninitialized time stamp
	buttonText_.setText(L"{p=12,x=20:- -}" );  
	buttonText_.setX(BUTTON_TEXT_X_);
	buttonText_.setY(BUTTON_TEXT_Y_);
	buttonText_.setColor(Colors::BLACK );

	addLabel( &buttonText_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CursorSettingButton  [Destructor]
//
//@ Interface-Description
// CursorSettingButton destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CursorSettingButton::~CursorSettingButton(void)
{
	CALL_TRACE("~CursorSettingButton(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setTime
//
//@ Interface-Description
//  Sets the time displayed by this button to the specified TimeStamp.
//  Flashes the displayed time if the Boolean isCurrentTime parameter is
//  TRUE.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses TextUtil::FormatTime to format the specified time stamp. 
//  $[TR01103] The Trend Graph Cursor button shall display the time...
//  $[TR01104] The Trend Graph Cursor button shall display the time as
//  "HH:MM:SS" for all timescales.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void CursorSettingButton::setTime(const TimeStamp & rTimeStamp, Boolean isCurrentTime)
{
	CALL_TRACE("setTime(const TimeStamp&, Boolean)");

	if (rTimeStamp.isInvalid())
	{
		buttonText_.setText(L"{p=12,x=20:- -}");
	}
	else
	{
		wchar_t tmpBuffer[64];
		TextUtil::FormatTime(tmpBuffer, L"{p=10: %H:%M:%S}", rTimeStamp);
		buttonText_.setText(tmpBuffer);
	}

	if (isCurrentTime)
	{
		buttonText_.setColor(Colors::BLACK_ON_LIGHT_GREY_BLINK);
	}
	else
	{
		buttonText_.setColor(Colors::BLACK);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [protected, virtual]
//
//@ Interface-Description 
//  Called when the associated setting has changed. We do nothing here
//  because the TrendCursorSlider drives the update to the displayed
//  time by calling setTime(). This method is the definition for
//  the pure virtual declared in SettingButton.
//---------------------------------------------------------------------
//@ PreCondition 
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CursorSettingButton::updateDisplay_(Notification::ChangeQualifier qualifierId,
										 const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void CursorSettingButton::SoftFault(const SoftFaultID  softFaultID,
									const Uint32       lineNumber,
									const char*        pFileName,
									const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							CURSORSETTINGBUTTON, lineNumber, pFileName, pPredicate);
}
