#include "stdafx.h"
#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimedBoundedDataInfo - Data processing agent class for
//  	bounded patient data types with a timeout constraint.
//---------------------------------------------------------------------
//@ Interface-Description:
//>Von
//  Adds timeout capability to a bounded breath item type. This class
//  inherits all of the bounded breath item processing features from
//  its parent class, 'BoundedDataInfo'. Most of the data processing
//  and data buffering is done by the parent class.
//  This class is also a subclass of the PDTimerTarget class and it
//  inherits Timer interface capability from the parent class.
//
//  Class Key Methods:
//  ---------------------------------
//  modifyData_  - modifies the bounded breath data and notifies other
//                subsystems of the data change. Restarts a data timeout
//                interval.
//
//  timerEventHappened - is called to notify  2 minute pass
//                  since last data update. The data is marked timedout and
//           breath item data change notice on the data type is sent out.
//---------------------------------------------------------------------
//@ Rationale
//   The purpose of this class is to encapsulate all the data and 
//   behaviors that are necessary for timing out some of the Patient
//   data items.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Since the patient data manager doesn't run as a separate task,
//   it cannot utilize system timer service directly. Instead,
//   the class uses GuiApp task's UserAnnunciation queue to get timer
//   events. The GuiApp task monitors timer event messages directed
//   to the Patient Data Management Subsystem and passes the event information
//   to the Patient Data Management Subsystem.
//   Eventually, this timer information is passed to a TimedBoundedDataInfo object
//   through the timerEventHappened method on the GuiApp task thread. 
//   The patient data is delivered to the Patient Data Management Subsystem
//   by the Network Application task and updated on its task thread.
//   To avoid a race condition between the GUIApp task and Network
//   Application task for timing out of a patient data item, a data item is
//   timed out within a mutex protection only if full two minutes has passed after
//   a data update.
//   
//   Whenever data is modified, a timer wait period gets restarted by 
//   calling the PDTimerRegistrar::StartTimer method.
//   These timers are created to have 2 minute wait interval after
//   each call to PDTimerRegistrar::StartTimer method.
//   
//   A timeout notice from a PD timer comes through the 
//   timerEventHappened virtual method, this method overrides
//   the parent class 'PDTimerTarget' method.
//>Voff
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//     Relies GUI task for the timer event message delivery.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/TimedBoundedDataInfo.ccv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 003   By: sah   Date:  20-Oct-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      * modified to use a static array of ids of those data items
//        that timeout based on the 'TIDAL_VOLUME_TIMEOUT_INTERVAL'
//        timer
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  23-Jan-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "TimedBoundedDataInfo.hh"
#include "PatientDataMgr.hh"
#include "PDTimerRegistrar.hh"

//====================================================================
//
//  Static Member Definitions...
//
//====================================================================
static Uint Bmem[(sizeof(MutEx)+sizeof(Uint) -1)/sizeof(Uint)];
MutEx &TimedBoundedDataInfo::RTimeLock_ = *(( MutEx *) Bmem);

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()  
//
//@ Interface-Description
//	  Initialize the static member objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
TimedBoundedDataInfo::Initialize()
{
	(void) new (&RTimeLock_) 
        MutEx(::PATIENT_DATA_TIMEOUT_MT);	// $[TI1]
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimedBoundedDataInfo(PatientDataId::PatientItemId id,const Real32 min,
//  const Real32 mid, const Real32 max, const Real32 changePoint,
//  const Precision lowerP, const Precision higherP,
//  PDTimerId::PDTimerIdType timerId))  [constructor]
//---------------------------------------------------------------------
//@ Interface-Description
//     Initializes member variables to initial values.
//     Register this object as a PD Timer target object so this
//     object can utilize GUI App timer events.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Most of the inherited members are initialized by the parent
//   class, BoundedDataInfo, constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimedBoundedDataInfo::TimedBoundedDataInfo( const PatientDataId::PatientItemId id,
    const Real32 min, const Real32 max, const Real32 changePoint,
	const Precision lowerP, const Precision higherP,
    PDTimerId::PDTimerIdType timerId):
    BoundedDataInfo(id, min, max, changePoint, lowerP, higherP),
    TIMER_ID_(timerId)
{
	CALL_TRACE("TimedBoundedDataInfo()");

	PDTimerRegistrar::RegisterTarget(timerId, this);
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimedBoundedDataInfo() [ destructor ]
//@ Interface-Description
//   Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimedBoundedDataInfo:: ~TimedBoundedDataInfo()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: modifyData_(void *pValue) [ virtual ]
//
//@ Interface-Description
//    Updates the actual data of this object after processing the raw
//    data to an acceptable value. Kicks off a new timeout wait cycle
//    for the data.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Data is processed and saved by the parent class's modifyData_ 
//     method. The 'TIMER_ID_' indicates a timer id allocated for
//     this agent, starts a full time out cycle by calling the
//     PDTimerRegistrar::StartTimer method.
//
//     Data is updated within a MutEx protection to avoid a race
//     condition where the GUIApp task tries to timeout an active data
//     and the NetworkApp task comes in and updates the data at the same
//     time. Which will generate a timeout condition on a brand new data.
//	   By synchronizing updating and timing out activitiy within a same
//     MutEx enforces each action to be atomic.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
TimedBoundedDataInfo::modifyData_(void *pValue)
{
    CALL_TRACE("TimedBoundedDataInfo::modifyData_(DataValue value)");


    BoundedDataInfo::modifyData_(pValue);
    RTimeLock_.request();
    updateTime_.now();
    RTimeLock_.release();
    PDTimerRegistrar::StartTimer( TIMER_ID_ );
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened(PDTimerId::PDTimerIdType timerId) [virtual]
//
//@ Interface-Description
//    This method is called when this data hasn't been updated last 
//    2 minutes. The method times out the data and sends out a data
//    change notice. If the Spontaneous Exhaled Tidal Volume data type
//    gets timed out, the Spontaneous Exhaled Minute Tidal Volume data
//    also gets timed out.
//---------------------------------------------------------------------
//@ Implementation-Description
//    TimedBoundedDataInfo class is a subclass of the PDTimerTarget
//    class and it provides its own timerEventHappened virtual method
//    to receive a 2 minute timer event notice.
//
//    In order to avoid race coditions between different priority
//    tasks that updates and times out a timed bounded data item,
//    code is protected within a MutEx lock and a data update time
//    is checked before a data gets timed out.
//---------------------------------------------------------------------
//@ PreCondition
//	 ( timerId == TIMER_ID_ )
//---------------------------------------------------------------------
//@ PostCondition
//    none.
//=====================================================================
void
TimedBoundedDataInfo::timerEventHappened(PDTimerId::PDTimerIdType  timerId)
{
    CALL_TRACE("timerEventHappened(timedId)");
    CLASS_PRE_CONDITION( timerId == TIMER_ID_ );

    RTimeLock_.request();

    if (updateTime_.getPassedTime()>=TIDAL_VOLUME_TIMEOUT_INTERVAL) // $[TI1]
    {
    	RTimeLock_.release();

    	setDataTimeout();

		const PatientDataId::PatientItemId ID = BreathItemDataInfo::getDataId();

    	if (ID == PatientDataId::EXH_SPONT_TIDAL_VOL_ITEM) // $[TI2]
    	{
			// this array is used to identify those spontaneous data items
			// that are to be based on the 'TIDAL_VOLUME_TIMEOUT_INTERVAL'
			// timer, with 'EXH_SPONT_TIDAL_VOL_ITEM'...
			static const PatientDataId::PatientItemId  ARR_DEPENDENT_IDS_[] =
			{
				PatientDataId::EXH_SPONT_MINUTE_VOL_ITEM,
				PatientDataId::SPONT_RATE_TO_VOLUME_RATIO_ITEM,
				PatientDataId::SPONT_PERCENT_TI_ITEM,
				PatientDataId::SPONT_INSP_TIME_ITEM,
				PatientDataId::PAV_LUNG_COMPLIANCE_ITEM,
				PatientDataId::PAV_PATIENT_RESISTANCE_ITEM,
				PatientDataId::TOTAL_AIRWAY_RESISTANCE_ITEM,
				PatientDataId::PAV_LUNG_ELASTANCE_ITEM,
				PatientDataId::PAV_PEEP_INTRINSIC_ITEM,
				PatientDataId::ELASTANCE_WORK_OF_BREATHING_ITEM,
				PatientDataId::RESISTIVE_WORK_OF_BREATHING_ITEM,
				PatientDataId::PATIENT_WORK_OF_BREATHING_ITEM,
				PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM,

				PatientDataId::NULL_PATIENT_DATA_ITEM
			};

			for (Uint idx = 0u; ARR_DEPENDENT_IDS_[idx] !=
										PatientDataId::NULL_PATIENT_DATA_ITEM;
				 idx++)
			{
				PatientDataMgr::GetDataAgent(ARR_DEPENDENT_IDS_[idx])->
															setDataTimeout();
			}
    	}
        //else $[TI3]
    }
    else // $[TI4]
    {
    	RTimeLock_.release();
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDataTimeout(Boolean sendNotice) [virtual]
//
//@ Interface-Description
//    Time out the data of this patient data item agent.
//    Send out a data change notice to other interested subsystems.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Use the RTimeLock_ mutex to protect a critical section of
//     the data update code since a timer driven GUIAPP task and
//     NetworkApp task with a higher task priority can create a race
//     condition.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//	 none.
//@ End-Method
//=====================================================================
void
TimedBoundedDataInfo::setDataTimeout(void)
{
    RTimeLock_.request();

    BoundedDataInfo::setDataTimeout();

    RTimeLock_.release();
	// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TimedBoundedDataInfo::SoftFault(const SoftFaultID  softFaultID,
							    const Uint32       lineNumber,
							    const char*        pFileName,
							    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA,
  			TIMED_BOUNDED_DATA_INFO, lineNumber, pFileName,
			pPredicate);
}

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
