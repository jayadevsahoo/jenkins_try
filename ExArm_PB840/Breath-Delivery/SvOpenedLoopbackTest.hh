#ifndef SvOpenedLoopbackTest_HH
#define SvOpenedLoopbackTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: SvOpenedLoopbackTest - BD Safety Net Test for the detection
//                           of open safety valve loopback current OOR
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SvOpenedLoopbackTest.hhv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  by    Date:  07-Nov-1996    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "SafetyNetTest.hh"

//@ Usage-Classes
#include "Background.hh"
//@ End-Usage


class SvOpenedLoopbackTest : public SafetyNetTest
{

    public:

        SvOpenedLoopbackTest( void );
        ~SvOpenedLoopbackTest( void );

        static void SoftFault( const SoftFaultID softFaultID,
					   const Uint32      lineNumber,
					   const char*       pFileName  = NULL,
					   const char*       pPredicate = NULL );

        virtual BkEventName checkCriteria( void );
  
    protected:

    private:

        // these methods are purposely declared, but not implemented...
        SvOpenedLoopbackTest( const SvOpenedLoopbackTest& ); // not implemented...
        void operator=( const SvOpenedLoopbackTest& );       // not implemented...
};


#endif // SvOpenedLoopbackTest_HH 
