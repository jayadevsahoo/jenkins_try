/*****************************************************************************/
/*   Module Name:    pts2000.h                                          
/*   Purpose:      Function prototypes for communications with the PTS 2000                                
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _PTS2000
#define _PTS2000

#define DEFAULT_MSEC_SAMPLING_RATE 10

void   SetPort(int port);
int    GetPort(void);
char  *GetPTSSerial(void);
char  *GetPTSVersion(void);
double GetPTSVersionNumber(void);
char  *GetPTSCalDate(void);
void   SetPTSNextCalDate(char *CalDate, char *NextCalDate);
char  *GetPTSNextCalDate(void);

int EstablishCom(void);
int EstablishComm(void);

int ZeroHighPressure(void);
int ZeroLowPressure(void);

int AdjOxy21(void);
int AdjOxy100(void);

int fnGetPTSHiFlow(double *dData);
int fnGetPTSLoFlow(double *dData);
int fnGetPTSHiPsr(double *dData);
int fnGetPTSLoPsr(double *dData);
int fnGetPTSPctO2(double *dData);
int fnGetPTSBarom(double *dData);
int fnGetPTSHiFlowTempr(double *dData);
int fnGetPTSLoFlowTempr(double *dData);

int BarPressureCmd(int bSetting);
int SetPTSSampleRate(int nMsecRate);
int SetFlowContinuousCmd(int nGasType);
int GetContinuousData(double *dData);
int StopContinuousData(void);
int StartHighFlowContinuous(void);

#endif // #ifndef _PTS2000
