#include "stdafx.h"
//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsConfig.cc
/// @brief Implementation of GainsConfig class
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "GainsConfigFuncPtrs.hh"
#include "GainsManagerControllerWrapper.hh"
#include "SigmaTypes.hh"

using namespace GainsManagerNS;

//--------------------------------------------------------------------
/// Constructor
//--------------------------------------------------------------------
GainsConfigFuncPtrs::GainsConfigFuncPtrs(void)
: m_setptr(0),
  m_getptr(0)
{
} // Constructor

//--------------------------------------------------------------------
// Constructor
//--------------------------------------------------------------------
GainsConfigFuncPtrs::GainsConfigFuncPtrs( GainsNames name)
: GainsConfig( name ),
  m_setptr(0),
  m_getptr(0)
{

} // Constructor

//--------------------------------------------------------------------
// Copy constructor
//--------------------------------------------------------------------
GainsConfigFuncPtrs::GainsConfigFuncPtrs(const GainsConfigFuncPtrs& rhs )
{
    (*this) = rhs;
} // Copy constructor

//--------------------------------------------------------------------
// Destructor
//--------------------------------------------------------------------
GainsConfigFuncPtrs::~GainsConfigFuncPtrs(void)
{

} // Destructor

//--------------------------------------------------------------------
// operator=
//--------------------------------------------------------------------
GainsConfigFuncPtrs& GainsConfigFuncPtrs::operator=(const GainsConfigFuncPtrs &rhs)
{
    if( this != &rhs )
    {
        GainsConfig::operator=(rhs);

        m_setptr       = rhs.m_setptr;
        m_getptr       = rhs.m_getptr;
    }

	return (*this);
} // operator=

//--------------------------------------------------------------------
// distribute
//--------------------------------------------------------------------
GainsConfig::GainsConfigOpStatus GainsConfigFuncPtrs::distribute(void)
{
    if( m_setptr != 0 )
    {
        if( isChanged() == TRUE )
        {
            m_setptr( getGain() );
            setChanged( FALSE );
        }
        return GainsConfig::SUCCESS;
    }

    return GainsConfig::FAILURE;
} // distribute

//--------------------------------------------------------------------
// load
//--------------------------------------------------------------------
GainsConfig::GainsConfigOpStatus GainsConfigFuncPtrs::load(void)
{
    if( m_getptr != 0 )
    {
        setGain( m_getptr() );
        return GainsConfig::SUCCESS;
    }

    return GainsConfig::FAILURE;
} // load

//--------------------------------------------------------------------
// setup
//--------------------------------------------------------------------
GainsConfig::GainsConfigOpStatus GainsConfigFuncPtrs::setup(void)
{
    GainsNames nm = getGainName();
    GainsConfigOpStatus ret = GainsConfig::SUCCESS;

    switch( nm )
    {
        case PEEP_KI:
            m_setptr = GainsManagerControllerWrapper::setPeepKi;
            m_getptr = GainsManagerControllerWrapper::getPeepKi;
            break;
        case FLOW_A_FACTOR:
            m_setptr = GainsManagerControllerWrapper::setAFactor;
            m_getptr = GainsManagerControllerWrapper::getAFactor;
            break;
        case FLOW_B_FACTOR:
            m_setptr = GainsManagerControllerWrapper::setBFactor;
            m_getptr = GainsManagerControllerWrapper::getBFactor;
            break;
        case FLOW_C_FACTOR:
            m_setptr = GainsManagerControllerWrapper::setCFactor;
            m_getptr = GainsManagerControllerWrapper::getCFactor;
            break;
        case FLOW_D_FACTOR:
            m_setptr = GainsManagerControllerWrapper::setDFactor;
            m_getptr = GainsManagerControllerWrapper::getDFactor;
            break;
        case PRESSURE_KP:
            m_setptr = GainsManagerControllerWrapper::setPressureKp;
            m_getptr = GainsManagerControllerWrapper::getPressureKp;
            break;
        case PRESSURE_KI:
            m_setptr = GainsManagerControllerWrapper::setPressureKi;
            m_getptr = GainsManagerControllerWrapper::getPressureKi;
            break;
		case PID_KP:
            m_setptr = GainsManagerControllerWrapper::setPidKp;
            m_getptr = GainsManagerControllerWrapper::getPidKp;
            break;
        case PID_KI:
            m_setptr = GainsManagerControllerWrapper::setPidKi;
            m_getptr = GainsManagerControllerWrapper::getPidKi;
            break;
        case PID_KD:
            m_setptr = GainsManagerControllerWrapper::setPidKd;
            m_getptr = GainsManagerControllerWrapper::getPidKd;
            break;
        case PCV_PED_KP:
        case PCV_ADU_KP:
        case PCV_PED_KI:
        case PCV_ADU_KI:
        default:
            ret = GainsConfig::FAILURE;
            break;
    }

    return (ret);
}

