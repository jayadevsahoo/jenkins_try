#ifndef IbwSettingButton_HH
#define IbwSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: IbwSettingButton - A setting button used for modifying
// the Ideal Body Weight setting.  Needs to display the setting's value
// twice both in kilos and in pounds.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/IbwSettingButton.hhv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SettingButton.hh"

//@ Usage-Classes
#include "NumericField.hh"
//@ End-Usage

class IbwSettingButton : public SettingButton
{
public:
	IbwSettingButton(Uint16 xOrg, Uint16 yOrg);
	~IbwSettingButton(void);

	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
                                const SettingSubject*         pSubject);


private:
	// these methods are purposely declared, but not implemented...
	IbwSettingButton(const IbwSettingButton&);		// not implemented...
	void   operator=(const IbwSettingButton&);	// not implemented...

	//@ Data-Member: kiloValue_
	// The Numeric Field which displays the IBW value in kilos.
	NumericField kiloValue_;

	//@ Data-Member: poundText_
	// The Text Field which displays the IBW value in pounds
	TextField poundText_;

	//@ Data-Member: unitsText_
	// The Text Field that displays "kg" units
	TextField unitsText_;

};

#endif // IbwSettingButton_HH 
