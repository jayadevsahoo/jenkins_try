#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OscBiLevelPhase - Implements OSC test breath during BiLevel.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from PressureBasedPhase class.  No public
//		methods are defined by this class since base class methods are used.
//		Protected virtual methods are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
// 		This class implements the algorithms for delivering the OSC recovery
//		inspiration breath when BiLevel is active.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The data members values from the base class are defined	in the pure
//		virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OscBiLevelPhase.ccv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//      Added support for new 'PEEP_LOW' and 'PEEP_HIGH_TIME' settings.
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  10-Mar-1998    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             BiLevel Initial version
//
//=====================================================================

#include "OscBiLevelPhase.hh"

#include "TriggersRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "SettingId.hh"
#include "PhasedInContextHandle.hh"
#include "BreathTrigger.hh"
#include "BD_IO_Devices.hh"

//@ End-Usage

// $[04295]
const Uint32 MAX_OSC_BILEVEL_INSP_TIME = 1000 ;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OscBiLevelPhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Call base class constructor.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

OscBiLevelPhase::OscBiLevelPhase( void)
 : PressureBasedPhase()  	// $[TI1]
{
	CALL_TRACE("OscBiLevelPhase::OscBiLevelPhase( void)") ;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OscBiLevelPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

OscBiLevelPhase::~OscBiLevelPhase(void)
{
	CALL_TRACE("OscBiLevelPhase::~OscBiLevelPhase(void)") ;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
OscBiLevelPhase::SoftFault( const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
		   			 const char*        pFileName,
		   			 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, OSCBILEVELPHASE,
                          	 lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineEffectivePressureAndBiasOffset_
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to determine the
//		effective pressure and the bias offset. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		The biasOffset = 0.  The effective pressure is the sum of
//		inspPress and biasOffset_ where inspPress is the difference
//		between peep high and low.
//		$[04295]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscBiLevelPhase::determineEffectivePressureAndBiasOffset_( void)
{
	CALL_TRACE("OscBiLevelPhase::determineEffectivePressureAndBiasOffset_( void)") ;

	Real32 inspPress = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_HIGH).value -
						PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_LOW).value ;

  	// $[TI1]
	biasOffset_ = 0.0F ;
	effectivePressure_ = inspPress + biasOffset_ ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineTimeToTarget_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to	determine the time to
//      target.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The time to target is 2/3 * inspiratory time.  The insp time is the
//		minimum of the set insp time or	MAX_OSC_BILEVEL_INSP_TIME.
//		$[04295]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscBiLevelPhase::determineTimeToTarget_( void)
{
	CALL_TRACE("OscBiLevelPhase::determineTimeToTarget_( void)") ;

  	// $[TI1]

	Real32 inspTime = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_HIGH_TIME).value ;
	
 	inspTimeMs_ = MIN_VALUE( inspTime, MAX_OSC_BILEVEL_INSP_TIME) ;
	
	timeToTarget_ = 2.0F / 3.0F * inspTimeMs_ ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineFlowAccelerationPercent_( void)
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to	determine the flow
//      acceleration percent.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Obtain fap from phased in settings.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscBiLevelPhase::determineFlowAccelerationPercent_( void)
{
	CALL_TRACE("OscBiLevelPhase::determineFlowAccelerationPercent_( void)") ;
	
  	// $[TI1]

	fap_ = PhasedInContextHandle::GetBoundedValue( SettingId::FLOW_ACCEL_PERCENT).value ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExhalationTriggers_()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called by the base class, PressureBasePhase, to	enable exhalation
//      triggers.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		ImmediateExpTrigger is enabled one BD cycle before the inspiratory
// 		time has expired.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
OscBiLevelPhase::enableExhalationTriggers_( void)
{
	CALL_TRACE("OscBiLevelPhase::enableExhalationTriggers_( void)") ;
	
	if (elapsedTimeMs_ >= inspTimeMs_ - CYCLE_TIME_MS)
	{
	  	// $[TI1.1]
   		((BreathTrigger&)RImmediateExpTrigger).enable() ;
   	} 	// implied else $[TI1.2]
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================













