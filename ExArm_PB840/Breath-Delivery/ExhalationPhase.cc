#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhalationPhase - Implements the exhalation phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  Virtual
//		methods are implemented to initialize the ExhalationPhase, to deliver
//		the flow needed to maintain peep while controlling the exhalation
//		valve each BD cycle, and to allow for internal clean up and graceful
//		termination of the breath phase.
//---------------------------------------------------------------------
//@ Rationale
//      This class implements the algorithms during exhalation phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The PeepController class is used to control the PSOLs and exhalation
//		valve to control the pressure to Peep.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//      Only one instance of this class can be created.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExhalationPhase.ccv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 030   By:   rhj    Date: 25-Oct-2010      SCR Number:   6694   
//  Project:  PROX
//  Description:
//       Removed the isEquivalent logic.
//
//  Revision: 029   By:   rhj    Date: 10-May-2010      SCR Number:   6436   
//  Project:  PROX
//  Description:
//       Optimize leak comp related changes by minimizing the use of 
//       pow function calls.
//
//  Revision: 028   By:   rhj    Date: 12-Mar-2009      SCR Number:   6491   
//  Project:  840S2
//  Description:
//       Enhance the calculation of leak add to determine the bigger 
//       leak rate corresponding to the actual delivered peep in the 
//       previous exhalation.  
//
//  Revision: 027   By:   rhj    Date: 19-Jan-2009      SCR Number:   6452   
//  Project:  840S
//  Description:
//       Changed the way to retreive the disconnect sensitivity setting
//       by using RLeakCompMgr.getDiscoSensitivity() instead of using the
//       PhasedInContextHandle::GetBoundedValue() method.
//              
//  Revision: 026   By:   rhj    Date: 07-Nov-2008      SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 025   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 024   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Set flow target to minimum purge flow when P100 maneuver is pending.
//
//  Revision: 023  By: syw     Date:  08-Sep-1999    DR Number: 5515
//  Project:  Baseline
//  Description:
//		Initialize pressure controller's integrator with desired flow.
//
//  Revision: 022  By: syw     Date:  26-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//		Implement new exhalation phase algorithm. 
//
//  Revision: 021  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 020  By:  syw   Date:  16-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (840)
//       Description:
//			Resume base flow to flowTarget if BreathRecord::GetIsPeepTransition() is
//			TRUE.
//
//  Revision: 019  By: syw   Date:  14-Apr-1998    DR Number: none
//  	Project:  Sigma (840)
//		Description:
//          Bilevel initial version.
//			To reduce autocycling, set desiredFlow_ to flowTarget_ if
//			the current breath is a mand breath during BiLevel.
//
//  Revision: 018  By: syw   Date:  06-Nov-1997    DR Number: 2609
//  	Project:  Sigma (840)
//		Description:
//			Added 100 ms to pressure condition.  Added back
//			exhFlow <= peakExhFlow_ * 0.9F && elapsedTimeMs_ > 25 flow condition.
//
//  Revision: 017  By: syw   Date:  24-Sep-1997    DR Number: 2519
//  	Project:  Sigma (840)
//		Description:
//			added back 100 ms (!autozero) condition for pressure condition.
//
//  Revision: 016  By: syw   Date:  15-Sep-1997    DR Number: 2422
//  	Project:  Sigma (840)
//		Description:
//			deleted some of the flow pulsing conditions and pressure condition.
//
//  Revision: 015  By: syw   Date:  15-Sep-1997    DR Number: 2430
//  	Project:  Sigma (840)
//		Description:
//			Added requirement traceability.
//
//	Revision: 014  By:  syw    Date:  28-Jul-1997    DR Number: DCS 2327
//      Project:  Sigma (R8027)
//      Description:
//			Set/reset the temperature sensors alpha when conditions are met.
//
//	Revision: 013  By:  syw    Date:  03-Jul-1997    DR Number: DCS 2282
//      Project:  Sigma (R8027)
//      Description:
//			Removed calls to RExhFlowSensor.setSpiroCalTemp()
//
//  Revision: 012  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 011  By:  iv     Date:  07-Apr-1997    DR Number: DCS 1780, 1827
//       Project:  Sigma (840)
//       Description:
//     		Set exh flow sensor calibration temperature to 32, 35 C.
//
//  Revision: 010  By:  syw    Date:  27-Mar-1997    DR Number: DCS 1780, 1827
//       Project:  Sigma (840)
//       Description:
//     		Set exh flow sensor calibration temperature to 30, 35 C.
//
//  Revision: 009  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780, 1827
//       Project:  Sigma (R8027)
//       Description:
//			Changed RExhDryFlowSensor.getValue() to RExhFlowSensor.getDryValue().
//
//  Revision: 008 By: syw    Date: 30-Sep-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added another condition in isFlowPulseCondition_() for meeting flow
//			pulsing.
//
//  Revision: 007 By: syw    Date: 02-Aug-1996   DR Number: 10031
//  	Project:  Sigma (R8027)
//		Description:
//			Move call of rPressureXducerAutozero.newCycle() to CycleTimer::SignalNewCycle()
//
//  Revision: 006 By: syw    Date: 12-Jul-1996   DR Number: 1077
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate shutoff of base flow during pressure triggering.
//
//  Revision: 005 By: syw    Date: 03-Jul-1996   DR Number: 1073
//  	Project:  Sigma (R8027)
//		Description:
//			Call rPressureController.setInitIntegratorValue() in
//			relinquishControl().
//
//  Revision: 004 By: syw    Date: 26-Feb-1996   DR Number: DCS 622
//  	Project:  Sigma (R8027)
//		Description:
//			Call ExhValveEController every newCycle so that its elapsed time
//			is the same as this class.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ExhalationPhase.hh"

#include "ControllersRefs.hh"
#include "MainSensorRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "TemperatureSensor.hh"
#include "PhasedInContextHandle.hh"
#include "BdDiscreteValues.hh"
#include "ExhFlowSensor.hh"
#include "PressureXducerAutozero.hh"
#include "FlowController.hh"
#include "PatientTrigger.hh"
#include "Peep.hh"
#include "PeepController.hh"
#include "PressureController.hh"
#include "ManeuverRefs.hh"
#include "P100Maneuver.hh"
#include "LeakCompEnabledValue.hh"
#include "PhasedInContextHandle.hh"
#include "BreathRecord.hh"
#include "Peep.hh"
#include "LeakCompMgr.hh"
//@ End-Usage

//@ Code...

// $[04310]
//@ Constant: PURGE_FLOW
// flow during exhalation with pressure triggering
extern const Real32 PURGE_FLOW;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhalationPhase()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

ExhalationPhase::ExhalationPhase(void)
 : BreathPhase(BreathPhaseType::EXHALATION),
   leakAdd_(0.0F),
   deltaLeakAdd_(0.0F)		// $[TI1]
{
	CALL_TRACE("ExhalationPhase::ExhalationPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhalationPhase()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

ExhalationPhase::~ExhalationPhase(void)
{
	CALL_TRACE("ExhalationPhase::~ExhalationPhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle( void)
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called every BD cycle during exhalation to deliver the flow needed to
//      maintain peep while controlling the exhalation valve to peep level.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set the alpha constant of the temperature sensor when the specific
//		conditions are met.  Call RPeepController.updatePsolAndExhalationValves()
//		to control to peep.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
ExhalationPhase::newCycle( void)
{
	CALL_TRACE("ExhalationPhase::newCycle( void)") ;

    Real32 exhalationFlow = RExhFlowSensor.getDryValue() ;

    if (exhalationFlow < flowTarget_ + 5 && elapsedTimeMs_ >= 200)
    {
	   	// $[TI1]
    	TemperatureSensor::SetAlpha( TRIGGER_ALPHA) ;
    }  	// implied else $[TI2]

	// $[04278]
	updateFlowControllerFlags_() ;
	
	RPeepController.updatePsolAndExhalationValves( elapsedTimeMs_) ;
	
    elapsedTimeMs_ += CYCLE_TIME_MS ;	
}	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called at the beginning of each exhalation to update the parameters
//      needed for exhalation.  
//---------------------------------------------------------------------
//@ Implementation-Description
//      The data members are initialized.  Patient triggers are enabled.
//		newBreath() methods are called for
//		the peep controller and for the autozero maneuver since autozeros
//		are executed at the beginning of exhalation.
//      If leak compensation is enabled, flow target is compensated
//      for leak loss.
// 		$[04060] $[04193] $[04194] 
//---------------------------------------------------------------------
//@ PreCondition
//		triggerType_ == TriggerTypeValue::FLOW_TRIGGER
//			|| triggerType_ == TriggerTypeValue::PRESSURE_TRIGGER
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ExhalationPhase::newBreath( void)
{
	CALL_TRACE("ExhalationPhase::newBreath( void)") ;
	
    DiscreteValue triggerType = PhasedInContextHandle::GetDiscreteValue( SettingId::TRIGGER_TYPE) ;

	// $[LC24026] Calculate baseflow based on trigger type. 
    if (triggerType == TriggerTypeValue::FLOW_TRIGGER)
    {
		if (RP100Maneuver.getManeuverState() == PauseTypes::PENDING)
		{
		  // $[RM24009] P100: Set the flow target to purge flow
			flowTarget_ = PURGE_FLOW;
		}
		else
		{
			Real32 flowSensitivity = PhasedInContextHandle::GetBoundedValue(SettingId::FLOW_SENS).value;
			flowTarget_ = flowSensitivity + BASE_FLOW_MIN ;
		}
    }
    else if (triggerType == TriggerTypeValue::PRESSURE_TRIGGER)
    {
	   	// $[TI2]
    	flowTarget_ = PURGE_FLOW ;
    }
    else
    {
    	CLASS_PRE_CONDITION( triggerType == TriggerTypeValue::FLOW_TRIGGER
        			|| triggerType == TriggerTypeValue::PRESSURE_TRIGGER) ;
    }

	const Real32 DISCO_SENS_VALUE = RLeakCompMgr.getDiscoSensitivity();


	// If Leak Compensation is enabled, calculate the additional flow to 
	// compensate the leak.
	const DiscreteValue MODE_VALUE = PhasedInContextHandle::GetDiscreteValue(SettingId::MODE);

	const BoundedValue PEEP_LOW_VALUE = PhasedInContextHandle::GetBoundedValue(SettingId::PEEP_LOW);
	const BoundedValue PEEP_HIGH_VALUE = PhasedInContextHandle::GetBoundedValue(SettingId::PEEP_HIGH);

	if (RLeakCompMgr.isEnabled())
	{

		// To determine the bigger leak rate corresponding to the actual delivered peep in the previous exhalation.  
		// One is added to set peep because the actual measured peep is higher than the set peep by one.
		Real32 peep = MAX_VALUE(RPeep.getActualPeep() + 1.0f, RLeakCompMgr.getAvgSteadyExpPressure());
		if (peep <= 0.5F)
		{
            peep = 0.5F;
		}


		// Compensate bias flow when autocylcing occurs with too much leak.
		// $[LC24027]
		if ((RBreathData.getExhaledTidalVolume() < 1.0F) &&
			(MODE_VALUE != ModeValue::BILEVEL_MODE_VALUE))
		{
			deltaLeakAdd_ = deltaLeakAdd_ + 1.0F;

			// Maximum limit addition to base flow
			if (deltaLeakAdd_ > 5.0F)
			{
				deltaLeakAdd_ = 5.0F;
			}
			

		}
		else
		{
			deltaLeakAdd_ = 0.0F;
		}

		//TODO E600 changed 0.5 to 0.5f to avoid compiler confusion
		Real32 sqrPeep = (Real32)( pow(peep, 0.5f));
        // $[LC24025] -- The desired flow reference for the bias flow, 
		// delivered during the exhalation phase...
		leakAdd_ = ((RLeakCompMgr.getFinalK1Gain() * sqrPeep) 
					 + RLeakCompMgr.getK2Gain() * (sqrPeep * peep )) + deltaLeakAdd_;  

		if ( (MODE_VALUE == ModeValue::BILEVEL_MODE_VALUE) &&
			  BreathRecord::IsHighPeep())
		{
			//TODO E600 changed 0.5 to 0.5f to avoid compiler confusion
            Real32 maxLeakAdd = (DISCO_SENS_VALUE *(Real32) (pow((PEEP_HIGH_VALUE/MAX_VALUE(PEEP_LOW_VALUE,0.3F)) , 0.5f)));
			if (leakAdd_ >  maxLeakAdd)
			{
				leakAdd_ = maxLeakAdd;
			}
		}
		// Limit the amount of leak added to flow target based
		// on disconnect sensitivity
		if (leakAdd_ > DISCO_SENS_VALUE )
		{
			leakAdd_ = DISCO_SENS_VALUE;
		}

        // Do not add leak to a P100 Maneuver
		if (RP100Maneuver.getManeuverState() != PauseTypes::PENDING)
		{
			flowTarget_ = flowTarget_ + leakAdd_;
		}

	}
	else
	{
		leakAdd_ = 0.0F;
		deltaLeakAdd_ = 0.0F;
	}


    BreathPhase::EnablePatientTriggers( BreathPhaseType::EXHALATION) ;

	// PressureXducerAutozero.newBreath() must be called before RPeepController.newBreath()
    RPressureXducerAutozero.newBreath() ;
	RPeepController.newBreath( RPeep.getActualPeep(), flowTarget_) ;

    elapsedTimeMs_ = 0 ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
//      This method takes a BreathTrigger& as an argument and has no return
//      value.  This method is called at the beginning of the next phase to
//      wrap up the current breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize the alpha constant of the temperature sensor.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ExhalationPhase::relinquishControl( const BreathTrigger& trigger)
{
	CALL_TRACE("BreathPhase::relinquishControl( BreathTrigger& trigger)") ;
	
   	// $[TI1]
	UNUSED_SYMBOL(trigger);	

// E600 BDIO    RExhGasTemperatureSensor.enableFilter();
        
   	TemperatureSensor::SetAlpha( NOMINAL_ALPHA) ;

	Real32 desiredFlow = RAirFlowController.getDesiredFlow() + RO2FlowController.getDesiredFlow() ;
	
   	RPressureController.setInitIntegratorValue( desiredFlow) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ExhalationPhase::SoftFault( const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
				 			const char*        pFileName,
		   					const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, EXHALATIONPHASE,
                             lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFlowControllerFlags_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to set the controller shutdown flag of the flow controller to
//		TRUE.  This method overloads the base class method.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set flags to true.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
ExhalationPhase::updateFlowControllerFlags_( void)
{
	CALL_TRACE("ExhalationPhase::updateFlowControllerFlags_( void)") ;
	
   	// $[TI1]
	RO2FlowController.setControllerShutdown( TRUE) ;
	RAirFlowController.setControllerShutdown( TRUE) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================






