
#ifndef TaskMonitor_HH
#define TaskMonitor_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: TaskMonitor - Monitor system tasks and strobe the watchdog.
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/Safety-Net/vcssrc/TaskMonitor.hhv   25.0.4.0   19 Nov 2013 14:20:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004   By: sah   Date: 09-Sep-1997   DR Number: 2278
//  Project:  Sigma (R8027)
//  Description:
//	Fixed problem of inaccurate period monitoring by re-doing the
//	internal monitoring mechanism.
//
//  Revision: 003   By: sah   Date: 14-Jul-1997   DR Number: 2293
//  Project:  Sigma (R8027)
//  Description:
//	Allow ability to disable (default) or enable task monitoring
//	during DEVELOPMENT.
//
//  Revision: 002   By: sah   Date: 16-Apr-1997   DR Number: 1963
//  Project:  Sigma (R8027)
//  Description:
//	Used this opportunity to reduce future confusion by changing
//	'counter' to 'pingCounter'.
//
//  Revision: 001   By: sah   Date: 26-Mar-1997   DR Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//	Initial version to coding standards (Integration baseline).
//
//=====================================================================

#include "Sigma.hh"
#include "Task.hh"
#include "IpcIds.hh"
#include "SafetyNet.hh"
#include "OsTimeStamp.hh"

class MsgQueue;
class TaskMonitorQueueMsg;

//Global Monitor constants:
#define NETWORK_APP_MONITOR_PERIOD	2000	//millisecond

//  Structure for holding the Task information for monitored tasks.
struct MonTaskEntry
{
  const char*   pTaskName;
  Uint          taskId;
  IpcId         queueId;
  Int           expectedPeriod; // expected time between responses...
  Int           maximumPeriod;  // maximum allowed time between responses...
  Boolean       activePingState;// flag indicating a ping-state is active...
  OsTimeStamp   elapsedTime;    // time since last response...
};


class TaskMonitor
{
  public:
    static void  Initialize(void);
    static void  MonitorTask(void);
    static void  Report(void);

#ifdef SIGMA_DEVELOPMENT
    static void  Print(const Int32 taskId = -1);
#endif // SIGMA_DEVELOPMENT

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);

#if defined(SIGMA_DEVELOPMENT)
    //@ Data-Member: DoNotMonitor
    //  Provide ability to disable (default) or enable task monitoring
    //  during DEVELOPMENT.
    static Boolean  DoNotMonitor;
#endif // defined(SIGMA_DEVELOPMENT)
   
  private:
    TaskMonitor(const TaskMonitor&);          // not implemented...
    TaskMonitor(void);                        // not implemented...
    ~TaskMonitor(void);                       // not implemented...
    void  operator=(const TaskMonitor&);      // not implemented...

    static void  SendPingMsg_(void);
    static void  RcvRespMsg_ (void);
    static void  FaultTester_(void);

    static MonTaskEntry*  GetEntryId_(const IpcId taskId);

    //@ Data-Member: MonTaskTable_
    //  Holds the task list.
    static MonTaskEntry  MonTaskTable_[TaskInfo::MAX_NUM_TASKS];

    //@ Data-Member: TaskNum_
    //  Holds the number of tasks monitored.
    static Uint  NumTasksMonitored_;

    //@ Constant: MONITOR_CYCLE_TIME_
    // The time, in milliseconds, that makes up each cycle of the task
    // monitor.
    static const Uint  MONITOR_CYCLE_TIME_;
};


#endif // TaskMonitor_HH
