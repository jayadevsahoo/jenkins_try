
// PB840SettingsView.cpp : implementation of the CPB840SettingsView class
//

#include "stdafx.h"
#include "PB840FakeGui.h"

#include "PB840SettingsDoc.h"
#include "PB840SettingsView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPB840SettingsView

IMPLEMENT_DYNCREATE(CPB840SettingsView, CView)

BEGIN_MESSAGE_MAP(CPB840SettingsView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CPB840SettingsView::OnFilePrintPreview)
	ON_WM_CREATE()
	ON_WM_SIZE()	
END_MESSAGE_MAP()

// CPB840SettingsView construction/destruction

CPB840SettingsView::CPB840SettingsView()
{
	// TODO: add construction code here

}

CPB840SettingsView::~CPB840SettingsView()
{
}

BOOL CPB840SettingsView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CPB840SettingsView drawing

void CPB840SettingsView::OnDraw(CDC* /*pDC*/)
{
	CPB840SettingsDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CPB840SettingsView printing


void CPB840SettingsView::OnFilePrintPreview()
{
	AFXPrintPreview(this);
}

BOOL CPB840SettingsView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CPB840SettingsView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CPB840SettingsView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CPB840SettingsView::OnRButtonUp(UINT nFlags, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CPB840SettingsView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
}


// CPB840SettingsView diagnostics

#ifdef _DEBUG
void CPB840SettingsView::AssertValid() const
{
	CView::AssertValid();
}

void CPB840SettingsView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CPB840SettingsDoc* CPB840SettingsView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPB840SettingsDoc)));
	return (CPB840SettingsDoc*)m_pDocument;
}
#endif //_DEBUG


// CPB840SettingsView message handlers

int CPB840SettingsView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create Setting Grid
	SettingGrid_.CreateGrid( WS_CHILD | WS_VISIBLE, CRect(0, 0, 0, 0), this, 1234 );

	return 0;
}

void CPB840SettingsView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	SettingGrid_.MoveWindow( 0, 0, cx, cy );
}


void CPB840SettingsView::OnInitialUpdate()
{
	CPB840SettingsDoc* pSettingDoc = GetDocument();

	SettingXmitData settingData;

	TCHAR buf[256];

	for ( Uint32 idx = 0; idx < ::NUM_BD_SETTING_IDS; ++ idx )
	{
		// read setting data from doc successfully
		if ( pSettingDoc->GetSettingElement( idx, settingData ) )
		{
			CUGCell cell;
			SettingGrid_.GetCell( SETTING_COLUMN_VALUE, idx, &cell );
			Uint32 settingId = cell.GetParam();
			
			const SettingEntry& entry = SettingTable::GetSettingEntryById( settingId );

			if ( entry.type == SET_DISCRETE )
			{
				// search the table
				for ( Uint32 i = 0; i < DiscreteTableSize; ++ i )
				{
					const DiscreteGroup& discreteGroup = DiscreteTable_[i];
					if ( discreteGroup.id == settingId )
					{
						const DiscretePair* currentDiscretePair = discreteGroup.pairs;

						while ( currentDiscretePair->display != NULL
							|| currentDiscretePair->value != 0 )
						{
							if ( currentDiscretePair->value == settingData.discreteValue )
							{
								cell.SetText( currentDiscretePair->display );
							}
							currentDiscretePair ++;
						}
						break;
					}
				}
			}
			else 
			{
				CString fmt;
				int scale = settingData.boundedPrec >= 0 ? 1 : abs(settingData.boundedPrec);
				fmt.Format( "%%.%df", scale );

				sprintf_s( buf, sizeof(buf), fmt, settingData.boundedValue );
				cell.SetText( buf );

				{
					CUGCell precisionCell;
					SettingGrid_.GetCell( SETTING_COLUMN_PRECISION, idx, &precisionCell );

					// lookup the display according to the boundedPrec value
					for ( Uint32 i = 0; i < PrecisionTableSize; ++ i )
					{
						if ( PrecisionTable_[i].prec == settingData.boundedPrec )
						{
							precisionCell.SetText( PrecisionTable_[i].display );
							break;
						}
					}
					SettingGrid_.SetCell( SETTING_COLUMN_PRECISION, idx, &precisionCell );
					SettingGrid_.RedrawCell( SETTING_COLUMN_PRECISION, idx );
				}
			}

			SettingGrid_.SetCell( SETTING_COLUMN_VALUE, idx, &cell );
			SettingGrid_.RedrawCell( SETTING_COLUMN_VALUE, idx );
		}
	}
}