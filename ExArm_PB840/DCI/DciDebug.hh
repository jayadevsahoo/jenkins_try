#ifndef	DciDebug_HH
#define	DciDebug_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ====================================================================
//@ Filename: DciDebug - Sigma DCI Communications class debug tools
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/DCI/vcssrc/DciDebug.hhv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision  1.0	By: jdm	Date: 9/x/95	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

	// setup unit test environment
#ifdef	SIGMA_UNIT_TEST
#define		DCI_DEBUG
#define		INIT_DUMMY_DATA		// maybe not??
#endif	// SIGMA_UNIT_TEST

	// enforce proper compiler switch usage
#if defined(VCONSOLE_IO) && ! defined(SIGMA_DEVELOPMENT)
#	error Must define SIGMA_DEVELOPMENT to use VCONSOLE_IO
#elif defined(DCI_DEBUG) && ! defined(SIGMA_DEVELOPMENT)
#	error Must define SIGMA_DEVELOPMENT to use DCI_DEBUG
#elif defined(INIT_DUMMY_DATA) && ! defined(SIGMA_DEVELOPMENT)
#	error Must define SIGMA_DEVELOPMENT to use INIT_DUMMY_DATA
#endif	// ! SIGMA_DEVELOPMENT


#ifdef	VCONSOLE_IO	// for testing without normal serial driver
	extern	const	Int32	DCI_GO_MSG;	// defined in test driver code
	extern	const	Int32	DCI_STOP_MSG;	// defined in test driver code
#endif	// VCONSOLE_IO

	// not-so-elegant debug tool
#ifdef	DCI_DEBUG	// enable debug output
#	include <stdio.h>

#	define	DBG(string)		printf(string)
#	define	DBG1(string, a)		printf(string, a)
#	define	DBG2(string, a, b)	printf(string, a, b)
#	define	DBG3(string, a, b, c)	printf(string, a, b, c)

#else			// disable debug output
#	define	DBG(string)		;
#	define	DBG1(string, a)		;
#	define	DBG2(string, a, b)	;
#	define	DBG3(string, a, b, c)	;
#endif	// DCI_DEBUG

#endif	// DciDebug_HH

