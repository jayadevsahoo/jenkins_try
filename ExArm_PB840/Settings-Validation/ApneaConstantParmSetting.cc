#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  ApneaConstantParmSetting - Apnea Constant Parameter Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates which timing parameter
//  remains constant during a change of apnea respiratory rate.  This setting
//  actually remains constant to a value of "apnea inspiratory time", but
//  resides in the "setting space" so that other settings don't need to
//  "know" that it is a constant setting.  This class inherits from 
//  'BatchDiscreteSetting' and provides the specific information needed for
//  the representation of apnea constant-during-rate-change.  This
//  information includes the value that this setting can have (see
//  'ConstantParmValue.hh'), this setting's role in Transition Rules
//  (see 'acceptTransition()'), and this batch setting's new-patient value.
//
//  Since this setting is a constant setting, it overrides 
//  'calcNewValue()' to ensure that that method is never called for this
//  setting (i.e., these methods assert an error if called).
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaConstantParmSetting.ccv   25.0.4.0   19 Nov 2013 14:27:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Cleaned up incomplete comment.
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed now-unneeded 'calcNewValue()'
//
//  Revision: 003   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getNewPatientValue() 
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "ApneaConstantParmSetting.hh"
#include "ConstantParmValue.hh"
#include "MandTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ApneaConstantParmSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Apnea Constant Parm Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaConstantParmSetting::ApneaConstantParmSetting(void)
	  : BatchDiscreteSetting(SettingId::APNEA_CONSTANT_PARM,
				 Setting::NULL_DEPENDENT_ARRAY_, 1u)
{
  CALL_TRACE("ApneaConstantParmSetting()");
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~ApneaConstantParmSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaConstantParmSetting::~ApneaConstantParmSetting(void)
{
  CALL_TRACE("~ApneaConstantParmSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaConstantParmSetting::getApplicability(
			      const Notification::ChangeQualifier qualifierId
					  ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pApneaMandType =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

  DiscreteValue  apneaMandTypeValue;

  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    apneaMandTypeValue = pApneaMandType->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    apneaMandTypeValue = pApneaMandType->getAdjustedValue();
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }

  return((apneaMandTypeValue == MandTypeValue::PCV_MAND_TYPE)
	   ? Applicability::VIEWABLE		// $[TI3]
	   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Returns the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns a constant.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaConstantParmSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  // $[02041] The setting's new-patient value ...

  return(DiscreteValue(ConstantParmValue::INSP_TIME_CONSTANT));    // $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaConstantParmSetting::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          APNEA_CONSTANT_PARM_SETTING, lineNumber, pFileName,
                          pPredicate);
}
