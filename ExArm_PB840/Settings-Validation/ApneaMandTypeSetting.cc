#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  ApneaMandTypeSetting - Apnea Mandatory Type Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates what type of mandatory
//  breaths (i.e., "volume-controlled" or "pressure-controlled") are to be
//  applied to the patient during apnea ventilation.  This setting is also
//  an Apnea Main Control Setting, and, therefore, activates the Transition
//  Rules of the apnea settings that are dependent on this setting's
//  transitions.  This class inherits from 'BatchDiscreteSetting' and provides
//  the specific information needed for the representation of apnea mandatory
//  type.  This information includes the set of values that this setting can
//  have (see 'MandTypeValue.hh'), this setting's activation of Transition
//  Rules (see 'calcTransition()'), and this batch setting's new-patient
//  value.
//
//  Because this is a Main Control Setting, the changing of this setting is
//  done differently than the other settings.  The direct changing of a Main
//  Control Setting does not update the settings that are dependent on its
//  value -- unlike those settings that have dependent settings and override
//  'calcDependentValues_()'.  The updating of the "dependent" settings of
//  Main Control Settings is done by the Adjusted Context AFTER all of the
//  Main Control Settings have been changed and approved by the operator.
//  This update is done via the Transition Rules defined for this setting
//  (see 'calcTransition()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaMandTypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 007  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added 'isEnabledValue()' for disabling new 'VC+' value
//      *  eliminated apnea minute volume, and apnea min insp flow
//         settings from transition rules (they're now tracking
//         settings)
//      *  modified 'getNewPatientValue()' to use 'PC' or 'VC' if non-apnea
//         counterpart is set to 'VC+'
//
//  Revision: 006   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Corrected incomplete comment.
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  simplified 'calcTransition()' method implementation
//
//  Revision: 004   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getNewPatientValue() 
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merging Rev "Color" (1.21.1.0) into Rev "BiLevel" (1.21.2.0)
//	Changed new-patient value to use its non-apnea counterpart's
//	value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Inital version.
//
//=====================================================================

#include "ApneaMandTypeSetting.hh"
#include "MandTypeValue.hh"
#include "PatientCctTypeValue.hh"
#include "ApneaTidalVolSetting.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"VC", MandTypeValue::VCV_MAND_TYPE},
	{"PC", MandTypeValue::PCV_MAND_TYPE},
	{NULL , NULL}
};


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ApneaMandTypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02071] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaMandTypeSetting::ApneaMandTypeSetting(void)
	        : BatchDiscreteSetting(SettingId::APNEA_MAND_TYPE,
				       Setting::NULL_DEPENDENT_ARRAY_,
				       MandTypeValue::TOTAL_MAND_TYPES)
{
  CALL_TRACE("ApneaMandTypeSetting()");
	setLookupTable(ParameterLookup_);
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~ApneaMandTypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaMandTypeSetting::~ApneaMandTypeSetting(void)
{
  CALL_TRACE("~ApneaMandTypeSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Apnea mandatory type is ALWAYS changeable.
//
//  $[01095] -- apnea control settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaMandTypeSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isEnabledValue()  [const, virtual]
//
//@ Interface-Description
//  Is this setting's value given by 'value', a currently-enabled value?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
Boolean
ApneaMandTypeSetting::isEnabledValue(const DiscreteValue value) const
{
  Boolean  isEnabled;

  switch (value)
  {
  case MandTypeValue::VCV_MAND_TYPE :
  case MandTypeValue::PCV_MAND_TYPE :		// $[TI1]
    // these standard values are ALWAYS enabled...
    isEnabled = TRUE;
    break;

  case MandTypeValue::VCP_MAND_TYPE :		// $[TI2]
    // VTPC not supported for apnea...
    isEnabled = FALSE;
    break;

  case MandTypeValue::TOTAL_MAND_TYPES :
  default :
    // unexpected mand type value...
    AUX_CLASS_ASSERTION_FAILURE(value);
    break;
  }
 
  return(isEnabled);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcTransition(newValue, currValue)  [virtual]
//
//@ Interface-Description
//  The purpose of this method is to inform the settings effected
//  by a state transition of this setting. So they can adjust
//  their value(s) appropriately.
//
//  This method deals with state transitions where-as dependent settings
//  calculations typically deal with bounded settings and incremental
//  changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (currValue == MandTypeValue::PCV_MAND_TYPE  ||
//   currValue == MandTypeValue::VCV_MAND_TYPE)  &&
//  (newValue == MandTypeValue::PCV_MAND_TYPE  ||
//   newValue == MandTypeValue::VCV_MAND_TYPE)  &&
//  (newValue != currValue)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaMandTypeSetting::calcTransition(const DiscreteValue newValue,
				     const DiscreteValue currValue)
{
  CALL_TRACE("calcTransition(newValue, currValue)");

  switch (currValue)
  {
  case MandTypeValue::VCV_MAND_TYPE :	// $[TI1]
    {  // $[02023] -- rules for the transition from VCV...
      // check for unexpected "to" value...
      AUX_CLASS_ASSERTION((newValue == MandTypeValue::PCV_MAND_TYPE), newValue);

      SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_PRESS)->
				acceptTransition(getId(), newValue, currValue);

      //=================================================================
      // activate the transition rule of apnea inspiratory time; this is NOT
      // a formal requirement, but an implementation needed to solve
      // apnea inspiratory time's resolution differences between VCV (20ms)
      // and PCV (10ms)...
      //=================================================================

      SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME)->
				acceptTransition(getId(), newValue, currValue);
    }
    break;

  case MandTypeValue::PCV_MAND_TYPE :	// $[TI2]
    {  // $[02024] -- rules for the transition from PCV...
      // check for unexpected "to" value...
      AUX_CLASS_ASSERTION((newValue == MandTypeValue::VCV_MAND_TYPE), newValue);

      //=================================================================
      // activate the transition rule of apnea apnea tidal volume...
      //=================================================================

      SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME)->
				acceptTransition(getId(), newValue, currValue);

      //=================================================================
      // activate the transition rules of apnea peak inspiratory flow; this
      // must be done AFTER apnea tidal volume is activated...
      //=================================================================

      SettingsMgr::GetSettingPtr(SettingId::APNEA_PEAK_INSP_FLOW)->
				acceptTransition(getId(), newValue, currValue);

      //=================================================================
      // activate the transition rule of apnea inspiratory time; this must
      // be done AFTER all of the apnea VCV settings are activated...
      //=================================================================

      SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME)->
				acceptTransition(getId(), newValue, currValue);


      //-----------------------------------------------------------------
      // Go back and ensure the apnea tidal volume value is still within a valid
      // range. At low IBW, sometimes inspiratory time can compute to below
      // 0.2 seconds then warped back to the minimum of 0.2. This adjustment
      // requires that apnea tidal volume be re-computed to ensure its within range.
      //-----------------------------------------------------------------
      ApneaTidalVolSetting* tidalVolSet = (ApneaTidalVolSetting*)SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);
      tidalVolSet->correctForMinimumTi();

      //=================================================================
      // finally, activate the transition rules of apnea expiratory time
      // and apnea I:E ratio; these must be done AFTER apnea inspiratory
      // time has been activated...
      //=================================================================

      SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME)->
				acceptTransition(getId(), newValue, currValue);
      SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO)->
				acceptTransition(getId(), newValue, currValue);
    }
    break;

  default :
    // unexpedted "from" mandatory type value...
    AUX_CLASS_ASSERTION_FAILURE(currValue);
    break;
  };
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaMandTypeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  // $[02072] The setting's new-patient value ...
  const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

  const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

  DiscreteValue  newPatientValue;

  switch (MAND_TYPE_VALUE)
  {
  case MandTypeValue::PCV_MAND_TYPE :
  case MandTypeValue::VCV_MAND_TYPE :	// $[TI1]
    newPatientValue = MAND_TYPE_VALUE;
    break;
  case MandTypeValue::VCP_MAND_TYPE :	// $[TI2]
    {  // VC+ not supported in apnea, set based on circuit type...
      const Setting*  pCctType =
			SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

      const DiscreteValue  CCT_TYPE_VALUE = pCctType->getAdjustedValue();

      switch (CCT_TYPE_VALUE)
      {
      case PatientCctTypeValue::ADULT_CIRCUIT :
      case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	// $[TI2.1]
	newPatientValue = MandTypeValue::VCV_MAND_TYPE;
	break;
      case PatientCctTypeValue::NEONATAL_CIRCUIT :	// $[TI2.2]
	newPatientValue = MandTypeValue::PCV_MAND_TYPE;
	break;
      default :
	// unexpected circuit type...
	AUX_CLASS_ASSERTION_FAILURE(CCT_TYPE_VALUE);
	break;
      };
    }
    break;
  default :
    // unexpected non-apnea mand type...
    AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
    break;
  };

  return(newPatientValue);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaMandTypeSetting::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                          APNEA_MAND_TYPE_SETTING, lineNumber, pFileName,
                          pPredicate);
}
