#ifndef Fio2CalInfo_HH
#define Fio2CalInfo_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: Fio2CalInfo - This class is the interface between the
//                      Fio2Monitor and the NovRamManager for
//                      retrieval and updates of calibration
//                      information.
//---------------------------------------------------------------------
// @ Version-Information
//
// @ Modification-Log
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  by    Date:  05-Jan-1995    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"
#include "Fio2SensorCalInfo.hh"

class Fio2CalInfo
{
    public:

        Fio2CalInfo( void );
        ~Fio2CalInfo( void );

        Fio2CalInfo( const Fio2CalInfo& rFio2CalInfo );
        void operator=( const Fio2CalInfo& rFio2CalInfo );        

        static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL,
							   const char*       pPredicate = NULL );

        inline void 
    	setNovRamFio2CalInfo( const Fio2SensorCalInfo fio2SensorCalInfo );

        inline const Fio2SensorCalInfo& getNovRamFio2CalInfo( void ) const;

    protected:

    private:

        //@ Data-Member: fio2CalInfo_ 
        //  structure for storing the
        //  FiO2 sensor calibration data
        Fio2SensorCalInfo fio2SensorCalInfo_;

};

// Inlined methods...
#include "Fio2CalInfo.in"

#endif  //Fio2CalInfo_HH
