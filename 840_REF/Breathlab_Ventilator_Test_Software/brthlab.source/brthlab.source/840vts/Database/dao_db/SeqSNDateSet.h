#if !defined(AFX_SEQSNDATESET_H__485A29D0_4B66_425E_A031_6809586D529E__INCLUDED_)
#define AFX_SEQSNDATESET_H__485A29D0_4B66_425E_A031_6809586D529E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SeqSNDateSet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSeqSNDateSet DAO recordset

class CSeqSNDateSet : public CDaoRecordset
{
public:
	CSeqSNDateSet(CDaoDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSeqSNDateSet)

// Field/Param Data
	//{{AFX_FIELD(CSeqSNDateSet, CDaoRecordset)
	long	m_ID;
	COleDateTime	m_time;
	CString	m_User;
	long	m_P_F;
	CString	m_UUTPN;
	CString	m_UUTBDSN;
	CString	m_UUTBDRunTime;
	CString	m_UUTRev;
	CString	m_BDPost;
	CString	m_GUISN;
	CString	m_GUIPost;
	CString	m_Saas;
	CString	m_CompSN;
	CString	m_CompRunTime;
	CString	m_PTSSN;
	CString	m_PTSVer;
	CString	m_PTSLastCal;
	CString	m_PTSNextCal;
	BOOL	m_PTSOutofCal;
	CString	m_VentAsset;
	CString	m_WorkOrder;
	//}}AFX_FIELD

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSeqSNDateSet)
	public:
	virtual CString GetDefaultDBName();		// Default database name
	virtual CString GetDefaultSQL();		// Default SQL for Recordset
	virtual void DoFieldExchange(CDaoFieldExchange* pFX);  // RFX support
	//}}AFX_VIRTUAL

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEQSNDATESET_H__485A29D0_4B66_425E_A031_6809586D529E__INCLUDED_)
