10-05-2007

Files I've changed:
strings\
   Idb.c

tests\
   version.h

TEST OPTION DELETE EXHALATION TEST

TOptionsCallback     cpvtmain.c - only debug

// MetabolicPortTest
static char cb_E_Ldb_Test6_Running[] = "Exhalation Port Test running";
static char cb_S_Ldb_Test6_Running[] = "Ejecutando prueba del puerto de espiraci�n";
static char cb_F_Ldb_Test6_Running[] = "Test Port expiratoire en cours";
static char cb_G_Ldb_Test6_Running[] = "Exspirationsanschlu�-Test l�uft";
static char cb_I_Ldb_Test6_Running[] = "Test porta esalazione in corso";
static char cb_P_Ldb_Test6_Running[] = "Teste de porta de exala��o em execu��o";

static char cb_E_Ldb_vent_timeout[] = "Ventilator timeout";
static char cb_S_Ldb_vent_timeout[] = "Tiempo de espera de ventilador";
static char cb_F_Ldb_vent_timeout[] = "D�lai ventilateur �coul�";
static char cb_G_Ldb_vent_timeout[] = "Beatmungsger�t Zeit�berschreitung";
static char cb_I_Ldb_vent_timeout[] = "Timeout ventilatore";
static char cb_P_Ldb_vent_timeout[] = "Tempo de espera do ventilador";

static char cb_E_Ldb_metport_error[] = "Exhalation Port Test error";
static char cb_S_Ldb_metport_error[] = "Error de la prueba del puerto de espiraci�n";
static char cb_F_Ldb_metport_error[] = "Erreur test Port expiratoire";
static char cb_G_Ldb_metport_error[] = "Exspirationsanschlu�-Test Fehler";
static char cb_I_Ldb_metport_error[] = "Errore test porta esalazione";
static char cb_P_Ldb_metport_error[] = "Erro do teste de porta de exala��o";

static char cb_E_Ldb_metport_block_error[] = "Blockage error: Do you wish to retry?";
static char cb_S_Ldb_metport_block_error[] = "Error de bloqueo: �Desea volver a intentarlo?";
static char cb_F_Ldb_metport_block_error[] = "Erreur de blocage: Voulez-vous r�essayer?";
static char cb_G_Ldb_metport_block_error[] = "Blockierung: Erneut versuchen?";
static char cb_I_Ldb_metport_block_error[] = "Errore di blocco: Riprovare?";
static char cb_P_Ldb_metport_block_error[] = "Erro de bloqueio: deseja repetir?";

static char cb_E_Ldb_met_port_failure[] = "Exhalation port failure";
static char cb_S_Ldb_met_port_failure[] = "Fallo en el puerto de espiraci�n";
static char cb_F_Ldb_met_port_failure[] = "D�fail. port expiratoire";
static char cb_G_Ldb_met_port_failure[] = "Exspirationsanschlu� Fehler";
static char cb_I_Ldb_met_port_failure[] = "Errore porta esalazione";
static char cb_P_Ldb_met_port_failure[] = "Falha na porta de exala��o";

static char cb_E_Ldb_met_port_rdg[] = "Exhalation port reading:";
static char cb_S_Ldb_met_port_rdg[] = "Lectura del puerto de espiraci�n:";
static char cb_F_Ldb_met_port_rdg[] = "Lecture port expiratoire:";
static char cb_G_Ldb_met_port_rdg[] = "Exspirationsanschlu� Me�wert:";
static char cb_I_Ldb_met_port_rdg[] = "Dati porta esalazione:";
static char cb_P_Ldb_met_port_rdg[] = "Leitura da porta de exala��o:";



static char cb_E_Ldb_Met_Port_Title[] = "Exhalation Port Test";
static char cb_S_Ldb_Met_Port_Title[] = "Prueba del puerto de espiraci�n";
static char cb_F_Ldb_Met_Port_Title[] = "Test Port expiratoire";
static char cb_G_Ldb_Met_Port_Title[] = "Exspirationsanschlu�-Test";
static char cb_I_Ldb_Met_Port_Title[] = "Test porta esalazione";
static char cb_P_Ldb_Met_Port_Title[] = "Teste de porta de exala��o";

// Met1
static char cb_E_Ldb_Block_Port[] = "Block the exhalation exhaust port on the 840 Ventilator.";
static char cb_S_Ldb_Block_Port[] = "Bloquear el puerto de espiraci�n de salida del ventilador 840.";
static char cb_F_Ldb_Block_Port[] = "Bloquer port expiratoire sur ventilateur 840.";
static char cb_G_Ldb_Block_Port[] = "Exspirationsausla� am Beatmungsger�t 840 blockieren.";
static char cb_I_Ldb_Block_Port[] = "Bloccare la porta di esalazione del ventilatore 840.";
static char cb_P_Ldb_Block_Port[] = "Bloquear porta de escape de exala��o no Ventilador 840.";

static char cb_E_Ldb_Connect_Met_Port[] = "Connect the metabolic port from the 840 Ventilator or the stopper Luer fitting to the low flow inlet on the PTS 2000.";
static char cb_S_Ldb_Connect_Met_Port[] = "Conecte el puerto metab�lico del ventilador 840 o el accesorio obturador Luer a la entrada de flujo bajo del PTS 2000.";
static char cb_F_Ldb_Connect_Met_Port[] = "Connecter port m�tabolique du ventilateur 840 ou raccord Luer du bouchon � l'entr�e Petit d�bit du PTS 2000.";
static char cb_G_Ldb_Connect_Met_Port[] = "Den Luer-Anschlu� mit dem Einla� f�r niedrigen Flow am PTS 2000 verbinden.";
static char cb_I_Ldb_Connect_Met_Port[] = "Collegare la porta metabolica del ventilatore 840 o il raccordo del fermo Luer all'ingresso a basso flusso del PTS 2000.";
static char cb_P_Ldb_Connect_Met_Port[] = "Conectar a porta metab�lica do Ventilador 840 ou do encaixe do tipo Luer do tamp�o � entrada de fluxo baixo no PTS 2000.";

// Met2
static char cb_E_Ldb_PROCESSING[] = "PROCESSING";
static char cb_S_Ldb_PROCESSING[] = "PROCESANDO";
static char cb_F_Ldb_PROCESSING[] = "TRAITEMENT EN COURS";
static char cb_G_Ldb_PROCESSING[] = "VERARBEITUNG";
static char cb_I_Ldb_PROCESSING[] = "ELABORAZIONE IN CORSO";
static char cb_P_Ldb_PROCESSING[] = "PROCESSANDO";

// Met3
static char cb_E_Ldb_Remove_block[] = "Remove block on the 840 Ventilator exhalation exhaust port.";
static char cb_S_Ldb_Remove_block[] = "Retirar el bloqueo del puerto de espiraci�n del ventilador 840.";
static char cb_F_Ldb_Remove_block[] = "Lib�rer port expiratoire du ventilateur 840.";
static char cb_G_Ldb_Remove_block[] = "Exspirationsausla� am Beatmungsger�t 840 deblockieren.";
static char cb_I_Ldb_Remove_block[] = "Sbloccare la porta di esalazione del ventilatore 840.";
static char cb_P_Ldb_Remove_block[] = "Remover bloqueio da porta de exaust�o de exala��o do Ventilador 840.";


