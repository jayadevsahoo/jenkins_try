;//=====================================================================
;// This is a proprietary work to which Puritan-Bennett corporation of
;// California claims exclusive right.  No part of this work may be used,
;// disclosed, reproduced, sorted in an information retrieval system, or
;// transmitted by any means, electronic, mechanical, photocopying,
;// recording, or otherwise without the prior written permission of
;// Puritan-Bennett Corporation of California.
;//
;//            Copyright (c) 1995, Puritan-Bennett Corporation
;//=====================================================================
;
;
;//============================ C L A S S     D E S C R I P T I O N ====
;//@ Filename: wdtdisable.s - Watchdog timer disable interrupt handler
;//
;//---------------------------------------------------------------------
;//@ Interface-Description
;//  >Von
;//  Free-Functions:
;//
;//  WatchdogDisableInterruptHandler - Strobes watchdog when interrupt occurs
;//
;//---------------------------------------------------------------------
;//@ Rationale
;//---------------------------------------------------------------------
;//@ Implementation-Description
;//  
;//  If the watchdog timer test passes, it installs this handler to 
;//  process interrupts from the 82C54 timer which is set to interrupt
;//  the CPU every 8.19 milliseconds.  This handler simply strobes the 
;//  watchdog timer every time an interrupt occurs.  It does this through
;//  the remainder of POST, VRTX and application initialization after
;//  which the application Task Monitor begins strobing the watchdog.
;//  At the end of pass 2 of the watchdog timer test, only the timer 
;//  counter (counter 0) used to strobe the watchdog is active.
;//  >Voff
;//
;//---------------------------------------------------------------------
;//@ Fault-Handling
;//  The POST exception handler processes faults for this module.
;//---------------------------------------------------------------------
;//@ Restrictions
;//---------------------------------------------------------------------
;//@ Invariants
;//    None
;//---------------------------------------------------------------------
;//@ End-Preamble
;//
;//@ Version
;// @(#) $Header:   /840/Baseline/Kernel/vcssrc/wdtdisable.s_v   25.0.4.0   19 Nov 2013 14:13:58   pvcs  $
;//
;//@ Modification-Log
;//
;//
;//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;//    Project: Sigma (R8027)
;//    Description:
;//       Added testable item annotations.
;//
;//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;//    Project: Sigma (R8027)
;//    Description:
;//       Initial version
;//
;//=====================================================================
;

    INCLUDE   kernel.inc

;//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;//@ Free-Function: WatchdogDisableInterruptHandler
;//
;//@ Interface-Description
;//  This interrupt simply strobes the watchdog through POST phase 2
;//  and through VRTX and application initialization.  
;//---------------------------------------------------------------------
;//@ Implementation-Description
;//  DisableWatchdog sets up the 82C54 and autovector 3 to invoke this
;//  interrupt function when the 82C54 counter 0 expires.  This 
;//  function writes to the watchdog strobe location to prevent the
;//  watchdog timer from expiring.  This function must NOT USE ANY
;//  STACK variables since it executes during initialization of the MMU.
;//---------------------------------------------------------------------
;//@ PreCondition
;//    none
;//---------------------------------------------------------------------
;//@ PostCondition
;//    none
;//@ End-Free-Function
;//=====================================================================
	SECTION code,4,C

    XDEF    _WatchdogDisableInterruptHandler__Fv

; interrupt WatchdogDisableInterruptHandler() {
_WatchdogDisableInterruptHandler__Fv:

	clr.b	AUTOVEC3_RESET_ADR     ; clear the interrupt
	STROBE_WATCHDOG
	rte

; // $[TI1]
; }
    END
