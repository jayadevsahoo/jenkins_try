#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@  Class: UDPSocket - Class definition that provides 
//   interface to a network socket that utilizes a UDP communication
//   protocol.
//---------------------------------------------------------------------
//@ Interface-Description
//    The UDPSocket class provides interfaces to a UDP socket
//    that can broadcast data and receive UDP data packets.
//---------------------------------------------------------------------
//@ Rationale
//   This class enables network data broadcasting on the sigma
//   subnet during the download phase. Broadcasting capability relies
//	 on a UDP communication protocol and all other network socket 
//   classes only support TCP/IP communication method.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The UDPSocket is a UDP socket and its communication mode
//   is set to non-blocking mode. This is to avoid an Application
//   task which broadcasts a network message has to be put into
//   a sleep state while lower level Stack-Ware code completes
//   transmitting data to the network successfully. In a non-blocking
//   IO mode, a transmit socket library call returns immediately
//   after all the user data is copied into the Stack-Ware system
//   buffer. Unlike other TCP/IP socket types, a network message
//   header and its data are sent out within one UDP packet just by
//   calling the 'sendto' library call once.
//   A UDP socket should be initialized not to route while the system's
//   IP address still set to zero. A UDP socket can be used to send out
//   broadcast messages even before a legitimate IP address is assigned
//   to the system.
//---------------------------------------------------------------------
//@ Fault-Handling
//	Standard class pre-conditions and assertion macros are utilized
//  to endure software correctness.
//---------------------------------------------------------------------
//@ Restrictions
//    The UDPSocket is designed mainly to support broadcasting
//    capability. If an application wants to use it as a generic
//    UDP socket, it can be done by overriding reInit method and
//    sendMsg method.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/UDPSocket.ccv   25.0.4.0   19 Nov 2013 14:02:52   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  dd-mmm-yy    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "UDPSocket.hh"
#include "Download.hh"

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UDPSocket(void)  [Default Constructor]
//
//@ Interface-Description
//    Initializes all the instance variables.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Let the NetSocket class constructor do all the 
//   instance variable initialization.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UDPSocket::UDPSocket(void) : NetSocket()
{
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~UDPSocket(void)  [Destructor]
//
//@ Interface-Description
//   Default Destructor. Close the socket if it is opened.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 none.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UDPSocket::~UDPSocket(void)
{
	if ( opened() ) // $[TI1]
	{
		NetSocket::close();
	} // else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: avoidRouting(void) 
//
//@ Interface-Description
//		Set this socket option not to use UDP/IP routing scheme.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Until the real IP address for the target machine is established
//	    set the socket option not to use UDP/IP routing.
//---------------------------------------------------------------------
//@ PreCondition
//	 ( opened() )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int
UDPSocket::avoidRouting(void)
{
int  soopts = 1;

	CLASS_PRE_CONDITION( opened() );

	if (::setsockopt(socketId_, SOL_SOCKET, SO_DONTROUTE,
			(caddr_t) &soopts, sizeof(soopts)) < 0) // $[TI1]
	{ 
		Download::ReportError("UDPSocket::avoidRouting():setsockopt(SO_DONTROUTE)", errno);
		return(-1);
	} // else $[TI2]
	return(socketId_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: open(PortId  portId, int )
//
//@ Interface-Description
//		Creates a UDP socket that can be used to broadcast data and
//	    receive broadcast data. 
//---------------------------------------------------------------------
//@ Implementation-Description
//    Call the createSocket_ method of the 'NetSocket' parent class to
//    create a new UDP socket.
//	  The socket will be created as a blocking socket as a default.
//	  Make the socket to be be able to broadcast data.
//	  A UDP socket is not used a contact point to open up new
//	  TCP/IP socket, therefore pass a zero as number of Listen argument.
//---------------------------------------------------------------------
//@ PreCondition
//   Shouldn't reopen an already open state socket.
//	 (! opened() )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int
UDPSocket::open(PortId  portId, int )
{
int  soopts = 1;

	CLASS_PRE_CONDITION( !opened() );

    if ((socketId_ = createSocket_(SOCK_DGRAM, portId, 0))<0) // $[TI1]
    {
	    return(-1);
    } // else $[TI2]

	if (::setsockopt(socketId_, SOL_SOCKET, SO_BROADCAST,
			(caddr_t) &soopts, sizeof(soopts)) < 0) // $[TI3]
	{ 
		Download::ReportError("UDPSocket::open():setsockopt(SO_BROADCAST)", errno);
		return(-1);
	} // else $[TI4]
	return(socketId_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: computeBroadcastAddr(Uint32 hostIP) 
//
//@ Interface-Description
//		Compute a broadcast IP address based on the given host IP 
//      address.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
UDPSocket::computeBroadcastAddr(Uint32 hostIP)
{
	if ( hostIP  == INADDR_ANY ) // $[TI1]
   	{
		hostIP =  (Uint32)INADDR_BROADCAST;
	}
#if	defined(SIGMA_TARGET)
	else if ( inClassA(hostIP)) // $[TI2]
	{
		hostIP &= CLASSA_NET_ADDR_MASK;
		hostIP |= (INADDR_BROADCAST &  ~CLASSA_NET_ADDR_MASK );
	}
	else if( inClassB(hostIP)) // $[TI3]
	{
		hostIP &= CLASSB_NET_ADDR_MASK;
		hostIP |= (INADDR_BROADCAST &  ~CLASSB_NET_ADDR_MASK );
	}
#endif //defined(SIGMA_TARGET)
	else // $[TI4]
		 // any other type of IP address, treat it like C class
	{
		hostIP &= CLASSC_NET_ADDR_MASK;
		hostIP |= (Uint32)(INADDR_BROADCAST &  ~CLASSC_NET_ADDR_MASK );
	} 
	destAddr_.in.sin_addr.s_addr = htonl(hostIP);
	destAddr_.in.sin_family = AF_INET;

#if		defined(SIGMA_UNIT_TEST)
{
unsigned char *cptr = (unsigned char *)&destAddr_.sa.sa_data[0];
printf("UDPSocket::computeBroadcastAddr: address %x %d%d %u.%u.%u.%u\n",
		hostIP, cptr[0], cptr[1],cptr[2],cptr[3], cptr[4], cptr[5]);
}
#endif //defined(SIGMA_UNIT_TEST)
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: broadcastData( char *buf, Uint nbytes, PortId portId)
//
//@ Interface-Description
//    This method is called to broadcast a network message with
//    a 'nbytes' of data to a given port id. Any node on the network
//    can open up a UDP socket at the given port id and receive
//    the sent data.  The method returns number of data bytes that 
//    are successfully transmitted. The method returns -1 if any
//    communication error.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    Sets the destination UDP port in a network byte order in
//    the 'broadcastAddr' socket address. This IP address portion
//    of the broadcast address has been already initialized within
//    the reInit method.  This broadcast address is used as a
//    destination address when the network data is actually transmitted
//    at a lower level routine.
//    The reason why the method returns -1 instead of a softfault
//    if the socket is not open is that these sockets can be closed
//    by the Network Application task if it detects a communication
//    loss state independently.
//--------------------------------------------------------------------- 
//@ PreCondition
//	 ( nbytes <= MAX_BROADCAST_MSG_SIZE );
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
UDPSocket::broadcastData( char *buf, Uint nbytes, PortId portId)
{
	// $[TI1]
	CLASS_PRE_CONDITION( nbytes <= MAX_BROADCAST_MSG_SIZE );
	
	destAddr_.in.sin_port = htons(portId);
	return( sendData( buf, nbytes));
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  readDataFrom(void *buf, int nbytes, sockaddr *from)
//
//@ Interface-Description
//    The readDataFrom method receives 'nbytes' of network data
//    from this socket into the 'buf' memory area. The method returns
//    a byte count of the received data. The address of the source
//    socket will be put into the from pointer.
//    If any communication error, the method returns -1. All the
//    socket library call errors are examined and logged internally.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    If the receive operation fails due to the lack of TCP/IP 
//    IO buffer, the method waits 20 milliseconds before it tries
//    to read again. Retry count for the no system buffer is set 
//    twice as many as the normal retry count.
// 
//    For a blocked io socket, the receiveFrom_ method would not return
//    until it receives 'nbytes' of data or a communication error.
//    For a non-block  mode socket, the receiveFrom_ method reads only
//    available bytes of data accumulated in the socket. 
//
//    For non-blocking mode socket, the errno 'EWOULDBLOCK' or
//    'EAGAIN' means that there is no data accumulated in the socket
//    input buffer. In this case, the method returns 0 value after
//    retries.
//--------------------------------------------------------------------- 
//@ PreCondition
// 		( buf && nbytes )
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
UDPSocket::readDataFrom(void *buf, int nbytes, sockaddr *from)
{
int retry;
Int count;
int noBufferRetry;

	CLASS_PRE_CONDITION( buf && nbytes );

	if (!opened()) // $[TI1]
	{
		return(-1);
	} // else $[TI2]

	retry = DOWNLOAD_COMM_RETRY;
	noBufferRetry =  DOWNLOAD_COMM_RETRY * 2;
	while((count=receiveFrom_((char *)buf, nbytes, from))< 0) // $[TI3]
	{
		if ( errno == ENOBUFS || errno == EWOULDBLOCK || errno == EAGAIN ) // $[TI3.1]
		{
			Download::ReportError("UDPSocket::readDataFrom():receiveFrom_() errno",
					errno);
		 	if ( --noBufferRetry < 0 ) // $[TI3.1.1]
		 	{
				return(-1);
			} // else $[TI3.1.2]
			continue;
		}
		else if ( --retry <  0 ) // $[TI3.2]
		{
			Download::ReportError(
				"UDPSocket::readDataFrom():receiveFrom_() DOWNLOAD_COMM_RETRY", errno);
			return(-1);
		} // else $[TI3.3]
	} // $[TI4]
	return(count);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: transmit_( char *buf, Int nbytes)
//
//@ Interface-Description
//    transmit the data onto the given socket connection.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    Just call the 'sendto' socket library to transmit the data and
//    used the 'broadcastAddr' socket address as a destination address.
//   
//--------------------------------------------------------------------- 
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
UDPSocket::transmit_( char *buf, Int nbytes)
{
Int ret;

	if (!opened()) // $[TI1]
	{
		return -1;
	} // else $[TI2]

	ret = ::sendto( getSocketId(), (char *) buf, nbytes, SO_BROADCAST,
				&destAddr_.sa, sizeof (struct sockaddr));
	return(ret);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  receiveFrom_( char *buf, Int nbytes, sockaddr *from)
//
//@ Interface-Description
//    Receives data from this TCP/IP socket connection into the
//    'buf' memory up to 'nbytes', and the data source socket address
//    is put into the 'from' memory pointer on return.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    Simply call the recvfrom socket library routine with the from
//    address set to NULL to indicate that this is a TCP/IP connection
//    not a UDP communication.
//--------------------------------------------------------------------- 
//@ PreCondition
// 		(from)
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
UDPSocket::receiveFrom_(char *buf, Int nbytes, sockaddr *from)
{
Int fromlen = sizeof(from);

	CLASS_PRE_CONDITION(from);

	if (!opened()) // $[TI1]
	{
		return -1;
	} // else $[TI2]
	return(::recvfrom(getSocketId(),(char *)buf,nbytes,0,from,&fromlen));
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  receive_( char *buf, Int nbytes)
//
//@ Interface-Description
//    Receives data from this TCP/IP socket connection into the
//    'buf' memory up to 'nbytes'.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    Simply call the recvfrom socket library routine with the from
//    address set to NULL to indicate that this is a TCP/IP connection
//    not a UDP communication.
//--------------------------------------------------------------------- 
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
UDPSocket::receive_(char *buf, Int nbytes)
{
struct sockaddr from;
Int fromlen = sizeof(from);

	if (!opened()) // $[TI1]
	{
		return -1;
	} // else $[TI2]
	return(::recvfrom(getSocketId(),(char *)buf,nbytes,0,&from,&fromlen));
}


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  callReceive( char *buf, Int nbytes)
//
//@ Interface-Description
//    Provides access to the protected method
//    receive_(char *buf, Int nbytes).
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int
UDPSocket::callReceive(char *buf, Int nbytes)
{
	return(receive_(buf, nbytes));
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  callReceiveFrom( char *buf, Int nbytes, sockaddr *from)
//
//@ Interface-Description
//    Provides access to the protected method
//    receiveFrom_(char *buf, Int nbytes, sockaddr *from).
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int
UDPSocket::callReceiveFrom(char *buf, Int nbytes, sockaddr *from)
{
	return(receiveFrom_(buf, nbytes, from));
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: callTransmit_( char *buf, Int nbytes)
//
//@ Interface-Description
//    Provides access to the protected method transmit_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int
UDPSocket::callTransmit( char *buf, Int nbytes)
{
	return(transmit_(buf, nbytes));
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSocketIdOnly(Int socketId)
//
//@ Interface-Description
//    Set the SocketId_ to the specified value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
UDPSocket::setSocketIdOnly( Int socketId )
{
	socketId_ = socketId;
}
#endif // SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
UDPSocket::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  DownloadFault::SoftFault(softFaultID, DOWNLOAD,
  	UDPSOCKET_CLASS, lineNumber, pFileName, pPredicate);
}
