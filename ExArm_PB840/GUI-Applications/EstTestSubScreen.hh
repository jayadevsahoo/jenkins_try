#ifndef EstTestSubScreen_HH
#define EstTestSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: EstTestSubScreen - he subscreen selected by the EST button in
// the Lower Other Screens subscreen.  Allows conducting EST test.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/EstTestSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: rhj   Date:  04-Apr-2009    SCR Number: 6495 
//  Project:  840S2
//  Description:
//      Added single test mode.
//
//  Revision: 012   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 011  By:  gdc	   Date:  23-Nov-1998    DCS Number: 5173
//  Project:  BiLevel
//  Description:
//		Changed to deactivate scrollbar and data select buttons when 
//		the start test button has been pressed. This prevents the 
//		scrollbar and start test buttons from being pressed at the 
//		same time. (DCS 5173)
//
//  Revision: 010  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 009  By:  yyy    Date: 02-Oct-1997    DR Number: DCS 2214
//  Project:  Sigma (840)
//  Description:
//  	Added a overrideCounter_ to accumulate number of times after the user
//      requested to override the EST alert based on persistent objects.  When
//		the counter reaches to 2 we shall update the overall status for the
//		override operation.
//
//  Revision: 008  By:  yyy    Date: 01-Oct-1997    DR Number: DCS 2204
//       Project:  Sigma (840)
//       Description:
//            Added PromptId DISCONNECT_AIR_AND_O2_PROMPT for revised
//            EST Battery Test.
//
//  Revision: 007  By:  yyy    Date:  16-Sep-1997    DCS Number: 2213
//  Project:  Sigma (R8027)
//  Description:
//		Added EXECUTE_NO_ACT_PROMPT_STATE, NO_ACT_PAUSE_REQUEST_STATE
//		and NO_ACT_PAUSE_PROMPT_STATE to handle the knob and keyboard tests.
//		These two tests do not require ACCEPT key input to notify service
//		mode that a request had been serviced.
//
//  Revision: 003  By:  yyy    Date:  30-JUL-97    DR Number: 2205
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for exh valve velocity transducer test.
//
//  Revision: 002  By:  yyy    Date:  30-JUL-97    DR Number: 2013
//    Project:  Sigma (R8027)
//    Description:
//      Updated the error for Nurse Call test.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"
#include "GuiTimerTarget.hh"
#include "AdjustPanelTarget.hh"
#include "LogTarget.hh"
#include "NovRamEventTarget.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "Array_ResultTableEntry_MAX_TEST_ENTRIES.hh"
#include "GuiTestManager.hh"
#include "NovRamManager.hh"
#include "NovRamUpdateManager.hh"
#include "ResultTableEntry.hh"
#include "ScrollableLog.hh"
#include "ServiceStatusArea.hh"
#include "SmPromptId.hh"
#include "SmStatusId.hh"
#include "SmTestId.hh"
#include "SubScreenTitleArea.hh"
#include "TemplateMacros.hh"
#include "TextButton.hh"
#include "UpperSubScreenArea.hh"
#include "EstResultSubScreen.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class EstTestSubScreen : public SubScreen, public GuiTimerTarget,
							public AdjustPanelTarget,
							public LogTarget,
							public NovRamEventTarget,
							public GuiTestManagerTarget
{
public:
	EstTestSubScreen(SubScreenArea *pSubScreenArea);
	~EstTestSubScreen(void);

	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// Redefine LogTarget methods
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
						Boolean &rIsCheapText, StringId &rString1,
						StringId &rString2, StringId &rString3,
						StringId &rString1Message);

	virtual void currentLogEntryChangeHappened(
						Uint16 changedEntry, Boolean isSelected);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelRestoreFocusHappened(void);
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);
				
	// NovRamEventTarget virtual methods
	virtual void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
											updateId);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// GuiTestManagerTarget virtual method
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int testResultId, Int resultStatus);
	virtual void processTestResultCondition(Int resultCondition);

	void clearHighlightedTestItem(void);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	EstTestSubScreen(void);						// not implemented...
	EstTestSubScreen(const EstTestSubScreen&);	// not implemented...
	void operator=(const EstTestSubScreen&);		// not implemented...

	//@ Type: estScreenId
	// Lists the possible screen layout scenarios.
	enum estScreenId
	{
		EST_NO_SCREEN,
		EST_SETUP_SCREEN,			// Initial screen set to previous EST result.
		EST_READY_TO_START_SCREEN,	// The START button is pressed
		EST_START_SCREEN,			// do the rest of EST test.
		EST_EXIT_SCREEN,			// exit the all the EST test.
		EST_TEST_NO_SCREEN		
	};

	void layoutScreen_(estScreenId newScreenId);
	void setEstTestNameTable_(void);
	void setEstTestPromptTable_(void);
	void setEstTestStatusTable_(void );
	void formatStatusOutcome_(ResultEntryData resultData, wchar_t *tmpBuffer);  
	void startTest_(void);
	void nextTest_(void);
	void repeatOrNextTest_(void);
	void estTestPause2Seconds_(void);
	void displayPausingRequest_(void);
	void setupTestResultOnUpperScreen_(void);
	void setupUpperScreenLayout_(int estCurrentTestIndex);
	void displayPrompt_(void);
	void displayAndReviewEstResults_(int firstDisplayItem);
	void displayEstFinishedScreen(void);
	void displayEstResultOutcome_(void);
	Int getNextState_(Int newEvent);
	void executeState_(void);
	void setupFallbackState_(Int newEvent);
	void startSingleTest_(void);

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member: startTestButton_
	// The StartTest button is used for start the EST Service mode testing.
	TextButton startTestButton_;

	//@ Data-Member: exitButton_
	// The Exit button is used for exiting the EST test mode.
	TextButton exitButton_;

	//@ Data-Member: repeatButton_
	// The Repeat button is used for repeating the current EST test.
	TextButton repeatButton_;

	//@ Data-Member: nextButton_
	// The Next button is used for proceeding to the next EST test.
	TextButton nextButton_;

	//@ Data-Member: overrideButton_
	// The Override button is used to override the minor failure found in the
	//  EST test mode.
	TextButton overrideButton_;

	//@ Data-Member: pCurrentButton_
	// The pCurrentButton_ is used for remembering the previously button
	// which was pressed by the user.
	Button *pCurrentButton_;

	//@ Data-Member: estStatus_
	// A container for displaying the current EST status.
	ServiceStatusArea estStatus_;

	//@ Data-Member: log_
	// A ScrollableLog field for handling the table display.
	ScrollableLog log_;

	//@ Data-Member: pEstResultSubScreen_
	// Address points to the EstResultSubScreen.
	EstResultSubScreen *pEstResultSubScreen_;

	//@ Data-Member: pUpperSubScreenArea_
	// Address points to UpperSubScreenArea
	UpperSubScreenArea *pUpperSubScreenArea_;
	
	//@ Type: estTestId
	// Lists the possible EST test ID.
	enum estTestId
	{
		EST_NO_TEST_ID=-1,
		EST_TEST_START_ID,
		EST_TEST_CIRCUIT_PRESSURE_ID = EST_TEST_START_ID,
		EST_TEST_FS_CROSS_CHECK_ID,
		EST_TEST_GAS_SUPPLY_ID,
		EST_TEST_SM_LEAK_ID,
		EST_TEST_GUI_KEYBOARD_ID,
		EST_TEST_GUI_KNOB_ID,
		EST_TEST_GUI_LAMP_ID,
		EST_TEST_BD_LAMP_ID,
		EST_TEST_GUI_AUDIO_ID,
		EST_TEST_GUI_NURSE_CALL_ID,
		EST_TEST_BD_AUDIO_ID,
		EST_TEST_SAFETY_SYSTEM_ID,
		EST_TEST_EXH_VALVE_SEAL_ID,
		EST_TEST_EXH_VALVE_ID,
		EST_TEST_EXH_HEATER_ID,
		EST_TEST_GENERAL_ELECTRONICS_ID,
		EST_TEST_GUI_TOUCH_ID,
		EST_TEST_GUI_SERIAL_PORT_ID,
		EST_TEST_BATTERY_ID,
		EST_TEST_MAX
	};

	//@ Type: estPromptId
	// Lists the possible EST test ID.
	enum estPromptId
	{
		EST_TEST_REMOVE_INSP_FILTER_PROMPT,
		EST_TEST_UNBLOCK_INLET_PORT_PROMPT,
		EST_TEST_BLOCK_INLET_PORT_PROMPT,
		EST_TEST_DISCONNECT_AIR_PROMPT,
		EST_TEST_DISCONNECT_O2_PROMPT,
		EST_TEST_CONNECT_AIR_PROMPT,
		EST_TEST_CONNECT_AIR_IF_AVAIL_PROMPT,
		EST_TEST_CONNECT_O2_PROMPT,
		EST_TEST_DISCNT_TUBE_BFR_EXH_FLTR_PROMPT,
		EST_TEST_CONCT_TUBE_BFR_EXH_FLTR_PROMPT,
		EST_TEST_ATTACH_GOLD_STD_TEST_TUBING_PROMPT,
		EST_TEST_CONNECT_WALL_AIR_PROMPT,
		EST_TEST_KEY_ACCEPT_PROMPT,
		EST_TEST_KEY_CLEAR_PROMPT,
		EST_TEST_KEY_INSP_PAUSE_PROMPT,
		EST_TEST_KEY_EXP_PAUSE_PROMPT,
		EST_TEST_KEY_MAN_INSP_PROMPT,
		EST_TEST_KEY_HUNDRED_PERCENT_O2_PROMPT,
		EST_TEST_KEY_INFO_PROMPT,
		EST_TEST_KEY_ALARM_RESET_PROMPT,
		EST_TEST_KEY_ALARM_SILENCE_PROMPT,
		EST_TEST_KEY_ALARM_VOLUME_PROMPT,
		EST_TEST_KEY_SCREEN_BRIGHTNESS_PROMPT,
		EST_TEST_KEY_SCREEN_CONTRAST_PROMPT,
		EST_TEST_KEY_SCREEN_LOCK_PROMPT,
		EST_TEST_KEY_LEFT_SPARE_PROMPT,
		EST_TEST_TURN_KNOB_LEFT_PROMPT,
		EST_TEST_TURN_KNOB_RIGHT_PROMPT,
		EST_TEST_NORMAL_PROMPT,
		EST_TEST_VENTILATOR_INOPERATIVE_PROMPT,
		EST_TEST_SAFETY_VALVE_OPEN_PROMPT,
		EST_TEST_LOSS_OF_GUI_PROMPT,
		EST_TEST_BATTERY_BACKUP_READY_PROMPT,
		EST_TEST_COMPRESSOR_READY_PROMPT,
		EST_TEST_COMPRESSOR_OPERATING_PROMPT,
		EST_TEST_COMPRESSOR_MOTOR_ON_PROMPT,
		EST_TEST_COMPRESSOR_MOTOR_OFF_PROMPT,
		EST_TEST_HUNDRED_PERCENT_O2_PROMPT,
		EST_TEST_ALARM_SILENCE_PROMPT,
		EST_TEST_SCREEN_LOCK_PROMPT,
		EST_TEST_HIGH_ALARM_PROMPT,
		EST_TEST_MEDIUM_ALARM_PROMPT,
		EST_TEST_LOW_ALARM_PROMPT,
		EST_TEST_ON_BATT_PWR_PROMPT,
		EST_TEST_SOUND_PROMPT,
		EST_TEST_NURSE_CALL_TEST_PROMPT,
		EST_TEST_NURSE_CALL_ON_PROMPT,
		EST_TEST_NURSE_CALL_OFF_PROMPT,
		EST_TEST_CONNECT_AC_PROMPT,
		EST_TEST_CONNECT_AIR_AND_O2_PROMPT,
		EST_TEST_DISCONNECT_AIR_AND_O2_PROMPT,
		EST_TEST_UNPLUG_AC_PROMPT,
		
		NUM_EST_TEST_PROMPT_MAX
	};

	//@ Type: estRowId
	// Lists the the number of rows can be displayed
	enum estRowId
	{
		ROW_1,
		ROW_2,
		ROW_3,
		ROW_4,
		ROW_5,
		ROW_6,
		ROW_7,
		ROW_8,
		EST_DISPLAY_ROW_MAX
	};

	//@ Type: estRowId
	// Lists the the number of columns can be displayed
	enum estColId
	{
		COL_5_DATA,
		COL_1_TIME,
		COL_2_TEST,
		COL_3_DIAG_CODE,
		COL_4_RESULT,
		EST_DISPLAY_COL_MAX
	};

	//@ Type: estEventId
	// Lists the possible state events.
	enum estEventId
	{
		UNDEFINED_EVENT=-1,

		// Button initiated event
		START_EVENT				=0,
		EXIT_EVENT				=1,
		REPEAT_EVENT			=2,
		NEXT_EVENT				=3,
		OVERRIDE_EVENT			=4,
		SINGLE_EVENT            =5,

		// Keyboard initiated event
		ACCEPT_EVENT			=6,
		CLEAR_EVENT				=7,
		NO_KEY_EVENT			=8,

		// Knob initiated event
		KNOB_EVENT				=9,

		// Service mode initiated event
		USER_PROMPT_EVENT		=10,
		END_ALERT_EVENT			=11,
		END_FAILURE_EVENT		=12,
		USER_END_EVENT			=13,
		
		// Timer initiated event
		TIMEOUT_EVENT			=14,

		// Program automatically initiated event
		AUTO_END_OF_TEST_EVENT	=15,
		AUTO_EVENT				=16,
		EST_EVENT_NUM
	};
	
	//@ Type: estStateId
	// Lists the possible states.
	enum estStateId
	{
		UNDEFINED_NEXT_TEST					=-1,
		INITIAL_STATE						=0,
		START_STATE							=1,
		READY_TO_TEST_STATE					=2,
		DO_TEST_STATE						=3,
		PROMPT_STATE						=4,
		END_ALERT_STATE						=5,
		END_FAILURE_STATE					=6,
		TEST_PASS_STATE						=7,
		EXECUTE_PROMPT_STATE				=8,
		EXIT_REQUEST_STATE					=9,
		REPEAT_STATE						=10,
		NEXT_STATE							=11,
		OVERRIDE_STATE						=12,
		DO_NEXT_STATE						=13,
		END_OF_EST_STATE					=14,
		PAUSE_REQUEST_STATE					=15,
		EXIT_BEFORE_PROMPT_STATE			=16,
		END_EXIT_ALERT_STATE				=17,
		END_EXIT_FAILURE_STATE				=18,
		END_EXIT_TEST_STATE					=19,
		RESTART_EST_STATE					=20,
		EXEC_OVERRIDE_STATE					=21,
		EXECUTE_EXIT_EST_STATE				=22,
		EXIT_EST_STATE						=23,
		EXIT_FALLBACK_STATE					=24,
		EXECUTE_REPEAT_STATE				=25,
		EXECUTE_NO_ACT_PROMPT_STATE			=26,
		NO_ACT_PAUSE_REQUEST_STATE			=27,
		NO_ACT_PAUSE_PROMPT_STATE			=28,
		START_SINGLE_STATE                  =29,
		BD_PROMPT_STATE                     =30,
		EST_STATE_NUM
	};

	//@ Data-Member: testManager_
	// An object which look after rendering the service mode test.
	GuiTestManager testManager_;
	
	//@ Data-Member: reviewMode_
	// The flag indicates if we are in test result review mode.
	Boolean reviewMode_;
	
	//@ Data-Member: estCurrentTestIndex_
	// The currently processing EST test item ids.
	Int estCurrentTestIndex_;
	
	//@ Data-Member: maxServiceModeTest_
	// The maximum number of test in the EST test item ids.
	Int maxServiceModeTest_;
	
	//@ Data-Member: estTestId_
	// An array which stores the EST test item ids.
	SmTestId::ServiceModeTestId estTestId_[EST_TEST_MAX];

	//@ Data-Member: estTestName_
	// An array which stores the EST test item names text.
	StringId estTestName_[EST_TEST_MAX];

	//@ Data-Member: estTestPromptId_
	// An array which stores the EST test prompt ids.
	SmPromptId::PromptId estTestPromptId_[NUM_EST_TEST_PROMPT_MAX];
	
	//@ Data-Member: estTestPromptName_
	// An array which stores the prompt test item names text.
	StringId estTestPromptName_[NUM_EST_TEST_PROMPT_MAX];

	//@ Data-Member: estResultTable_
	// An array which stores the EST ResultTableEntry information.
	FixedArray(ResultTableEntry,MAX_TEST_ENTRIES) estResultTable_;
	
	//@ Data-Member: estStatusDecodingTable_
	// An array which stores the decoding information from Service Mode to
	// Persistent object.
	Int estStatusDecodingTable_[SmStatusId::NUM_TEST_DATA_STATUS];

	//@ Data-Member: estTestResultTitle_
	// An array which stores the EST test item result title text.
	StringId estTestResultTitle_[EST_TEST_MAX];

	//@ Data-Member: clearLogEntry_
	// An boolean flag indicating to keep or clear the log entry.
	Boolean clearLogEntry_;

	//@ Data-Member: savedPromptName_
	// Hold the Service Mode prompt name.
	StringId savedPromptName_;

	//@ Data-Member: promptId_
	// Hold the Service Mode prompt ID.
	Int promptId_;

	//@ Data-Member: keyAllowedId_
	// Hold the Service Mode key combinations allowed for the operator.
	SmPromptId::KeysAllowedId keyAllowedId_;

	//@ Data-Member: conditionId_
	// Hold the Service Mode condition ID.
	Int conditionId_;

	//@ Data-Member: userKeyPressedId_
	// Hold the operator pressed key Id.
	SmPromptId::ActionId userKeyPressedId_;

	//@ Data-Member: isThisEstTestCompleted_
	// Hold the test completed information.
	Boolean isThisEstTestCompleted_;

	//@ Data-Member: mostSevereStatusId_
	// Hold the most severed status ID.
	Int mostSevereStatusId_;

	//@ Data-Member: overallStatusId_
	// Hold the overall status ID.
	Int overallStatusId_;

	//@ Data-Member:  previousState_
	// Previous state.
	Int previousState_;
	
	//@ Data-Member:  nextState_
	// Current state.
	Int nextState_;
	
	//@ Data-Member:  fallBackState_
	// FallBack state.
	Int fallBackState_;
	
	//@ Data-Member: overrideRequested_
	// Flag to Hold the override request
	Boolean	overrideRequested_;
	
	//@ Data-Member: overrideCounter_
	// Counter for holding override novRam callbacks counts.
	Int32	overrideCounter_;
	
	//@ Data-Member: repeatTestRequested_
	// Flag to Hold the repeat request
	Boolean	repeatTestRequested_;
	
	//@ Data-Member: estStateTable_
	// State table for EST test.
	Int16 estStateTable_[EST_STATE_NUM][EST_EVENT_NUM];

	//@ Data-Member:  RecursionCount_
	// Recursion Count_.
	Int16 RecursionCount_;
	
	//@ Data-Member:  newEvent_
	// New event.
	Int newEvent_;

	//@ Data-Member: singleTestButton_
	// The single test button is used to execute one test.
	TextButton singleTestButton_;


	//@ Data-Member: isSingleTestMode_
	// To determine whether we are in single test mode or not
	Boolean isSingleTestMode_;

	//@ Data-Member: showBdTestPrompt_
	// To determine whether to display BD test prompt.
	Boolean showBdTestPrompt_;

	//@ Data-Member: showAirO2ConnectedPrompt_
	// To determine whether "Ensure air and O2 are connected"
    // prompt is currently displayed
	Boolean showAirO2ConnectedPrompt_;


	//@ Data-Member: wasBdTestPromptShown_
	// A flag indicating whether Bd tests prompt 
	// was shown. 
	Boolean wasBdTestPromptShown_;

};

#endif // EstTestSubScreen_HH 
