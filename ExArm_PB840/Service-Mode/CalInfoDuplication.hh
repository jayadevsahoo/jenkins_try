#ifndef CalInfoDuplication_HH
#define CalInfoDuplication_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CalInfoDuplication - Copy the CalInfoFlashBlock from BD to GUI
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/CalInfoDuplication.hhv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 11-Nov-1997    DR Number: DCS 2621
//       Project:  Sigma (840)
//       Description:
//			useDefaultEvTable() is now compiled all the time, i.e. no longer
//			conditionally compiled using the SIGMA_DEVELOPMENT and
//			SIGMA_PSUEDO_OFFICIAL compile flags.
//
//  Revision: 003  By:  quf    Date: 29-Aug-1997    DR Number: DCS 2435
//       Project:  Sigma (840)
//       Description:
//            Added useDefaultEvTable() to create a default exh valve
//            calibration table for SIGMA_DEVELOPMENT and SIGMA_PSUEDO_OFFICIAL
//            builds only.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  syw    Date:  25-Apr-1996    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "ServiceMode.hh"

#include "NetworkMsgId.hh"

//@ Usage-Classes
//@ End-Usage

class CalInfoDuplication
{
  public:

	CalInfoDuplication( void) ;
    ~CalInfoDuplication( void) ;

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL, 
						   const char		*pPredicate = NULL) ;

	static void Initialize( void) ;
	static void ProcessCalInfoFlashDataMsg( XmitDataMsgId, void *pData, Uint) ;

	void useDefaultEvTable( void) ;

#if defined(SIGMA_BD_CPU)
	void sendCalInfoFlashData( void) ;

#else	  
	void execute( void) ;

#endif // defined(SIGMA_BD_CPU)
	
  protected:

  private:
    CalInfoDuplication( const CalInfoDuplication&) ;	// not implemented...
    void operator=( const CalInfoDuplication&) ;		// not implemented...

#if defined(SIGMA_GUI_CPU)
	Uint32 programFlash_( void) ;
	void requestBdFlashData_( void) ;

	//@ Data-Member: NumBytesReceived_
	// number of bytes received by GUI from BD
	static Uint32 NumBytesReceived_ ;

#endif // defined(SIGMA_GUI_CPU)

} ;


#endif // CalInfoDuplication_HH 
