#ifndef TrendDbSettingRecord_HH
#define TrendDbSettingRecord_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbSettingRecord
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSettingRecord.hhv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  ksg    Date:  18-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
// 
//=====================================================================

#include "TrendDbRecord.hh"

class TrendDbSettingRecord : public TrendDbRecord
{
public:

	TrendDbSettingRecord( void );
	virtual ~TrendDbSettingRecord( void );
	virtual Int16 size( void ) const;
	virtual Uint32 getSeqNumber( void ) const;
	virtual void setSeqNumber( Uint32 seqNumber );
	virtual const TimeOfDay & getTimeOfDay( void ) const;
	virtual void setTimeOfDay( const TimeOfDay & timeOfDay );
	virtual const TrendValue & getTrendValue( Int8 slotNumber ) const;
	virtual void setTrendValue( Int8 slotNumber, const TrendValue & value );
	virtual void setChecksum( void );

private:

	//@Data-Member: Schema
	// This struct represents a single setting id record
	struct Schema
	{
		Uint32      seqNumber;
		TimeOfDay   timeOfDay;
		Boolean     isValidArray[ TrendDataMgr::TREND_SETTING_MAX_SLOTS ];
		DataValue   dataValueArray[ TrendDataMgr::TREND_SETTING_MAX_SLOTS ];
		Uint16      checkSum;
	};
};

#endif // TrendDbSettingRecord_HH
