#include "stdafx.h"

#if  defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PEEPDataInfo - Manages PEEP intrinsic and PEEP total patient 
//        data during a breath pause phase.
//---------------------------------------------------------------------
//@ Interface-Description
//  
//>Von
//    Description:
//    ------------
//    This class computes PEEP intrinsic and PEEP total values while the
//    pause key is pressed. During a pause period, this class updates
//    PEEP intrinsic and PEEP total value at every 300 millisecond
//    interval based on the current circuit pressure that comes in
//    a waveform data.
//
//    Class Features:
//    ---------------
//      1. Maintains a state that indicates whether the pause key is down
//         or not.
//      2. Maintains the end of expiratory pressure at the beginning of 
//         a breath pause phase, to compute intrinsic PEEP value.
//      3. Maintains the last PEEP value update time.
//      4. Updates new PEEP values.
//    
//     Class Key methods:
//     ------------------
//     1. "updateData_"  - updates PEEP intrinsic and PEEP total patient data.
//     2. "startPause_"  - starts a breath Paused state and records an
//  initial End of Expiratory pressure at the beginning of a pause.
//     3. "stopPause_"  - stop a breath pause phase.
//
//     Class Interfaces:
//     -----------------
//       PatientDataMgr class interfaces with this class to keep
//       track of PEEP data during a pause. PatientDataMgr class
//       starts and stops a breath pause state based on the data
//       packet type it receives from the Breath Delivery subsystem.
//       Actual PEEP data is maintained with two breath item data 
//       processing agents, one for the 'PEEP_INTRINSIC' type and
//       one for the 'PEEP_TOTAL' type. Other subsystems access
//     these PEEP data values through these agents.
//---------------------------------------------------------------------
//@  Rationale
//   Encapsulate all the data necessary to compute a PEEP intrinsic
//   and PEEP total value within this class.
//---------------------------------------------------------------------
//@  Implementation-Description
//   When a PeEnd patient data packet arrives, Patient Data Manager
//   starts a breath pause phase. This class keeps track of last
//   PEEP data modification time and it only computes new PEEP
//   intrinsic and total values every 200 milli seconds.
//   Newly computed PEEP intrinsic and total values are maintained
//   within each corresponding type data processing agent.
//---------------------------------------------------------------------
//@  Fault-Handling
//     Uses Standard Sigma CLASS_ASSERTION and CLASS_PRE_CONDITION
//     to handle software faults.
//---------------------------------------------------------------------
//@  Restrictions
//       The PatientDataMgr class should be initialized before
//       this class is used. OsTimeStamp class should be functional.
//>Voff
//---------------------------------------------------------------------
//@  Invariants
//---------------------------------------------------------------------
//@  End-Preamble
//
//---------------------------------------------------------------------
//@  Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PEEPDataInfo.ccv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@  Modification-Log
//
//  Revision: 006  By: dph   Date:  16-Jan-99    DR Number: 5575
//  Project:  NeoMode
//  Description:
//      Set PEEP intrinsic and total peep to 0.0 if values are
//      less than 0.0.
//
//  Revision: 005  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//--Break in Revision numbers--
//
//  Revision: 001  By:  jhv    Date:  16-Feb-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//  
//   Revision 002  By:  hct    Date: 04/23/97         DR Number:  1479
//      Project:   Sigma   (R8027)
//      Description:
//         Add requirements 03067, 03068, 03071 and 03072 to updateData_.  
//         These requirements were already added to the Unit Test Spec.
//
//   Revision 003  By:  hct    Date: 04/23/97         DR Number:  1479
//      Project:   Sigma   (R8027)
//      Description:
//         Add requirements 03069, 03070 and 03073 to updateData_.  
//         These requirements were already added to the Unit Test Spec.
//
//  Revision: 004  By:  hct    Date:  21-AUG-97    DR Number: 1479
//       Project:  Sigma (R8027)
//       Description:
//             Change 03067 to 03074 per review rework.  
//
//=====================================================================
#include "PEEPDataInfo.hh"
#include "PatientDataMgr.hh"
#include "BoundedDataInfo.hh"



//@  Code...
// Static member definition.
const Uint PEEPDataInfo::DELAY_TIME_ = 300;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@  Method: PEEPDataInfo()  [Default Constructor]
//
//@  Interface-Description
//     The breath phase is initially not paused.
//---------------------------------------------------------------------
//@  Implementation-Description
//---------------------------------------------------------------------
//@  PreCondition
//none
//---------------------------------------------------------------------
//@  PostCondition
//none
//@  End-Method
//=====================================================================

PEEPDataInfo::PEEPDataInfo(void)
{
    CALL_TRACE("PEEPDataInfo()");
    paused_ = FALSE;
    updateTime_.invalidate();
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@  Method: ~PEEPDataInfo()  [Destructor]
//
//@  Interface-Description
//     Does nothing.
//---------------------------------------------------------------------
//@  Implementation-Description
//---------------------------------------------------------------------
//@  PreCondition
//none
//---------------------------------------------------------------------
//@  PostCondition
//none
//@  End-Method
//=====================================================================

PEEPDataInfo::~PEEPDataInfo(void)
{
    CALL_TRACE("~PEEPDataInfo()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@  Method: updateData_( WaveformData &pWaveData )
//
//@  Interface-Description
//    Computes a new PEEP intrinsic and PEEP total value based on
//    the current circuit pressure data of a new waveform data.
//    PEEP value update is done once every 300 milliseconds.
//    Whenever PEEP intrinsic and total values are modified, notify
//    other subsytems of the PEEP breath item change.
//    An intrinsic PEEP value is a difference between the current circuit
//    pressure and an initial Expiratory pressure when the pause key was
//    pressed at first. Total PEEP is a current circuit pressure.
//    $[03074], $[03068], $[03069], $[03070], $[03071], $[03072], $[03073]
//---------------------------------------------------------------------
//@  Implementation-Description
//   The last update time is maintained in the updateTime_ member. 
//   The actual PEEP data is kept within the Breath Item Data Processing 
//   objects that Patient Data Manager maintains. These objects are
//   obtained from the PatientDataMgr Class through  the 'GetDataAgent'
//   method. Each PEEP data is passed to its data type processing object via
//   'modifyData_' and it updates the data and sends out a data change
//   notice to other subsystems.
//---------------------------------------------------------------------
//@  PreCondition
//   none
//---------------------------------------------------------------------
//@  PostCondition
// none
//@  End-Method
//=====================================================================
void
PEEPDataInfo::updateData_(WaveformData *pWaveData)
{
BreathItemDataInfo *pDataManager;
Real32 rNumber;


    CALL_TRACE("updateData(pWaveData)");
    SAFE_CLASS_PRE_CONDITION(paused_);

    if ( updateTime_.getPassedTime() >= DELAY_TIME_) // $[TI1]
    {
		pDataManager=PatientDataMgr::GetDataAgent(PatientDataId::PEEP_INTRINSIC_ITEM);
		rNumber =  pWaveData->circuitPressure - initialPeEnd_;
		
		if (rNumber < 0.0)
		{// $[TI1.1]
			rNumber = 0.0;
		}// $[TI1.2]

		pDataManager->modifyData_( (void *)&rNumber );

		pDataManager=PatientDataMgr::GetDataAgent( PatientDataId::PEEP_TOTAL_ITEM );

		rNumber = pWaveData->circuitPressure;

		if (rNumber < 0.0)
		{// $[TI1.3]
			rNumber = 0.0;
		}// $[TI1.4]

		pDataManager->modifyData_( (void *)&rNumber );
		updateTime_.now();
    }
    //else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  startPause_(const Real32  endExpPressure)
//
//@ Interface-Description
//   Start a breath pause phase and record an  initial Expiratory circuit
//   pressure. Record current time as a breath pause start time.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//   Get the End Of Expiratory Pressure breath data agent object
//   from the patient data manager and let it process the data.
//--------------------------------------------------------------------- 
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
PEEPDataInfo::startPause_(const Real32 endExpPressure )
{
BoundedDataInfo *pDataInfo;

    CALL_TRACE("startPause_()");
    if ( paused_ )// $[TI0.1]
    {
           return;
    }
    //else  $[TI1]
    paused_ = TRUE;
    updateTime_.now();
    pDataInfo = (BoundedDataInfo *) PatientDataMgr::GetDataAgent
    (PatientDataId::END_EXP_PRESS_ITEM);
    initialPeEnd_ = pDataInfo->processData_(endExpPressure);
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//. Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//. Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//. Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//. PreCondition
//      none 
//---------------------------------------------------------------------
//. PostCondition 
//      none 
//. End-Method 
//===================================================================== 

void
PEEPDataInfo::SoftFault(const SoftFaultID  softFaultID,
					    const Uint32       lineNumber,
					    const char*        pFileName,
					    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA, PEEP_DATA_INFO,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

#endif//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
