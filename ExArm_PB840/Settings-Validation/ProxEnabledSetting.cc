#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Covidien
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  ProxEnabledSetting - Prox Enabled Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the whether the optional
//  Prox is enabled.  This class inherits from 'BatchDiscreteSetting'
//  and provides the specific information needed for the representation of
//  the Prox's activation.  This information includes the set of values
//  that this setting can have (see 'ProxEnabledValue.hh'), and this batch
//  setting's new-patient value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ProxEnabledSetting.ccv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc   Date: 21-Jan-2010     SCR Number: 6733
//  Project:  PROX
//  Description:
//      Added VCO2 study code for development reference.
//
//  Revision: 008   By: rhj   Date: 25-Oct-2010     SCR Number: 6603
//  Project:  PROX
//  Description:
//      Moved the TrendDataMgr::PostEvent functionality to the 
//      TrendDbSettingRepository class.
//
//  Revision: 007   By: rhj   Date: 29-Sept-2010     SCR Number: 6681 
//  Project:  PROX
//  Description:
//      Modified to automatically enable PROX only if the previous 
//      setting was NIV or BILEVEL.
//
//  Revision: 006   By: rhj   Date: 10-Aug-2010     SCR Number: 6601
//  Project:  PROX
//  Description:
//      Added disableProx method.
//
//  Revision: 005   By: rhj   Date: 28-July-2010     SCR Number: 6624
//  Project:  PROX
//  Description:
//      Changed the default setting from disabled to enabled.
//
//  Revision: 004   By: mnr   Date: 13-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Log Trending event for New Patient value as well.
//
//  Revision: 003   By: mnr   Date: 10-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Unused code removed, compilation error fixed.
//
//  Revision: 002   By: mnr   Date: 10-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      if NIV or BILEVEL and PROX is ENABLED, log PROX_DISABLED to Trending.
//
//  Revision: 001   By: rhj    Date:  25-Jan-2010    SCR Number: 6436
//  Project: PROX
//  Description:
//		Initial version.
//=====================================================================

#include "ProxEnabledSetting.hh"
#include "SettingsMgr.hh"
#include "MandTypeValue.hh"
#include "VentTypeValue.hh"
//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SupportTypeValue.hh"
#include "SettingContextHandle.hh"
#include "ModeValue.hh"
#include "SoftwareOptions.hh"
#include "PatientCctTypeValue.hh"
#include "AcceptedContextHandle.hh"
#include "ProxEnabledValue.hh"
#include "TrendDataMgr.hh"
#include "TrendEvents.hh"

//@ End-Usage

//@ Code...


//====================================================================
//
//  Static Data Definitions...
//
//====================================================================


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ProxEnabledSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[PX02010] -- Setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ProxEnabledSetting::ProxEnabledSetting(void)
	: BatchDiscreteSetting(SettingId::PROX_ENABLED,
						   Setting::NULL_DEPENDENT_ARRAY_,
						   ProxEnabledValue::TOTAL_PROX_ENABLED_VALUES)
{
	CALL_TRACE("ProxEnabledSetting()");

	disableProx_ = FALSE;
}  


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~ProxEnabledSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ProxEnabledSetting::~ProxEnabledSetting(void)   
{
	CALL_TRACE("~ProxEnabledSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	ProxEnabledSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id  applicabilityId = Applicability::CHANGEABLE; 

	const DiscreteValue VENT_TYPE_VALUE = 
		AcceptedContextHandle::GetDiscreteValue(SettingId::VENT_TYPE);

	const DiscreteValue MODE_VALUE = 
		AcceptedContextHandle::GetDiscreteValue(SettingId::MODE);

    // Do not allow the user to change the setting.
	// $[PX02004] 
	if (VENT_TYPE_VALUE == VentTypeValue::NIV_VENT_TYPE ||
		MODE_VALUE == ModeValue::BILEVEL_MODE_VALUE )
	{
		applicabilityId = Applicability::VIEWABLE;
	}

#if defined(VCO2_STUDY)
	applicabilityId = Applicability::CHANGEABLE;
#endif

	return(applicabilityId);
}	


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[PX02001] The setting's new-patient value ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	ProxEnabledSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	DiscreteValue  newPatientValue;


	const DiscreteValue VENT_TYPE_VALUE = 
		SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();

	const DiscreteValue MODE_VALUE = 
		SettingsMgr::GetSettingPtr(SettingId::MODE)->getAdjustedValue();

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();

    // Do not allow the user to change the setting.
	// $[PX02004] 
	if (VENT_TYPE_VALUE == VentTypeValue::NIV_VENT_TYPE ||
		MODE_VALUE == ModeValue::BILEVEL_MODE_VALUE ||
		CIRCUIT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT ||
		disableProx_)
	{
        newPatientValue = ProxEnabledValue::PROX_DISABLED;	
	}
	else
	{
        newPatientValue = ProxEnabledValue::PROX_ENABLED;	

	}

#if defined(VCO2_STUDY)
	if (VENT_TYPE_VALUE == VentTypeValue::NIV_VENT_TYPE ||
        disableProx_)
    {
        newPatientValue = ProxEnabledValue::PROX_DISABLED;	
    }
    else
    {
        newPatientValue = ProxEnabledValue::PROX_ENABLED;	
    }
#endif

	return(newPatientValue);
}  

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on 
//  mode, and vent type setting.  
//---------------------------------------------------------------------
//@ Implementation-Description
//
//@ PreCondition
//  (settingId == SettingId::VENT_TYPE)
//  (settingId == SettingId::MODE)
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
	ProxEnabledSetting::acceptTransition(
										const SettingId::SettingIdType settingId,
										const DiscreteValue            newValue,
										const DiscreteValue        currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	AUX_CLASS_PRE_CONDITION((settingId == SettingId::VENT_TYPE) ||
							(settingId == SettingId::MODE), 
							settingId);

	const DiscreteValue  CIRCUIT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAcceptedValue();

	const DiscreteValue VENT_TYPE_VALUE =
		SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();

	const DiscreteValue MODE_VALUE =
		SettingsMgr::GetSettingPtr(SettingId::MODE)->getAdjustedValue();

	const DiscreteValue VENT_TYPE_OLD_VALUE =
		SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAcceptedValue();

	const DiscreteValue MODE_OLD_VALUE =
		SettingsMgr::GetSettingPtr(SettingId::MODE)->getAcceptedValue();


	if (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT )
	{
        DiscreteValue  newPatientValue;

		if (MODE_VALUE == ModeValue::BILEVEL_MODE_VALUE || 
			VENT_TYPE_VALUE == VentTypeValue::NIV_VENT_TYPE || 
			disableProx_)
		{
			newPatientValue = ProxEnabledValue::PROX_DISABLED;
			setAdjustedValue( (SettingValue) newPatientValue );   
		}
		else
		{
            if (MODE_OLD_VALUE == ModeValue::BILEVEL_MODE_VALUE || 
                VENT_TYPE_OLD_VALUE == VentTypeValue::NIV_VENT_TYPE)
            {
    			newPatientValue = ProxEnabledValue::PROX_ENABLED;
    			setAdjustedValue( (SettingValue) newPatientValue );   
            }

		}

	}

#if defined(VCO2_STUDY)
	if (VENT_TYPE_VALUE == VentTypeValue::NIV_VENT_TYPE ||
        disableProx_)
    {
        DiscreteValue newPatientValue = ProxEnabledValue::PROX_DISABLED;
        setAdjustedValue( (SettingValue) newPatientValue );   
    }
    else
    {
        DiscreteValue newPatientValue = ProxEnabledValue::PROX_ENABLED;
        setAdjustedValue( (SettingValue) newPatientValue );   
    }
#endif

	// activation of Transition Rule will be italicized if value has changed
	setForcedChangeFlag();
}



//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ProxEnabledSetting::SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							PROX_ENABLED_SETTING, lineNumber, pFileName,
							pPredicate);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  disableProx() 
//
//@ Interface-Description
//  This method simply forces prox to be disabled. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the flag disableProx_ to true.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void ProxEnabledSetting::disableProx()
{
	disableProx_ = TRUE;
}


