/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2007. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  ADDUSERPNL                      1
#define  ADDUSERPNL_USERNAME             2       /* callback function: Adduser_Callback */
#define  ADDUSERPNL_PASSWORD             3       /* callback function: Adduser_Callback */
#define  ADDUSERPNL_PASSWORD_2           4       /* callback function: Adduser_Callback */
#define  ADDUSERPNL_OK_BUTTON            5
#define  ADDUSERPNL_CANCEL_BUTTON        6
#define  ADDUSERPNL_ERROR                7
#define  ADDUSERPNL_DECORATION           8
#define  ADDUSERPNL_TEXTMSG              9
#define  ADDUSERPNL_TEXTMSG_2            10
#define  ADDUSERPNL_TEXTMSG_3            11

#define  ADMINPANEL                      2
#define  ADMINPANEL_DECORATION_2         2
#define  ADMINPANEL_DECORATION_4         3
#define  ADMINPANEL_DECORATION_6         4
#define  ADMINPANEL_DECORATION_7         5
#define  ADMINPANEL_DECORATION_8         6
#define  ADMINPANEL_DECORATION_9         7
#define  ADMINPANEL_PICTURE_14           8
#define  ADMINPANEL_DECORATION           9
#define  ADMINPANEL_TEXTMSG_2            10

#define  ALARMPANEL                      3
#define  ALARMPANEL_DONE                 2
#define  ALARMPANEL_YES                  3
#define  ALARMPANEL_NO                   4
#define  ALARMPANEL_TEXT_BOX3            5
#define  ALARMPANEL_TEXT_BOX2            6
#define  ALARMPANEL_TEXT_BOX1            7
#define  ALARMPANEL_DECORATION           8
#define  ALARMPANEL_DECORATION_2         9

#define  AUTODETECT                      4
#define  AUTODETECT_ACCEPT               2       /* callback function: AcceptAutoDetect */
#define  AUTODETECT_AUTODET_STATUS       3

#define  BASEPANEL                       5       /* callback function: BaseCallback */

#define  COMP_PANEL                      6       /* callback function: CompPanelCB */
#define  COMP_PANEL_COMP_SERIAL          2
#define  COMP_PANEL_COMP_DONE            3
#define  COMP_PANEL_COMP_TIME            4
#define  COMP_PANEL_COMP_CANCEL          5
#define  COMP_PANEL_OLDINFO              6
#define  COMP_PANEL_DECORATION           7

#define  COMSETTING                      7       /* callback function: ComSettingPanel */
#define  COMSETTING_AUTODET_ENABLE_PTS   2       /* callback function: AutoDetectEnablePTS */
#define  COMSETTING_COM_PORT_PTS         3       /* callback function: ComPortPTS */
#define  COMSETTING_BAUD_PTS             4       /* callback function: BaudRatePTS */
#define  COMSETTING_AUTO_DETECT_PTS      5       /* callback function: AutoDetectPTS */
#define  COMSETTING_AUTODET_ENABLE_840   6       /* callback function: AutoDetectEnabled840 */
#define  COMSETTING_COM_PORT_840         7       /* callback function: ComPort840 */
#define  COMSETTING_BAUD_840             8       /* callback function: BaudRate840 */
#define  COMSETTING_AUTO_DETECT_840      9       /* callback function: AutoDetect840 */
#define  COMSETTING_DECORATION           10
#define  COMSETTING_DECORATION_3         11
#define  COMSETTING_TEXTMSG_2            12
#define  COMSETTING_TEXTMSG_3            13
#define  COMSETTING_TEXTMSG_5            14
#define  COMSETTING_TEXTMSG              15

#define  DB_CONF                         8
#define  DB_CONF_MAXUSERS_MSG            2
#define  DB_CONF_PACKDB                  3       /* callback function: DB_Conf_CB */
#define  DB_CONF_TEXTMSG_2               4
#define  DB_CONF_PICTURE                 5

#define  LANGCONF                        9
#define  LANGCONF_TEXTBOX                2
#define  LANGCONF_LANG_SEL               3       /* callback function: LanguageSelect */
#define  LANGCONF_LANG_MSG               4

#define  LOGGINOPTS                      10
#define  LOGGINOPTS_CHECKBOX_1           2       /* callback function: DataLogCB */
#define  LOGGINOPTS_CHECKBOX_2           3       /* callback function: DataLogCB */
#define  LOGGINOPTS_CHECKBOX_3           4       /* callback function: DataLogCB */
#define  LOGGINOPTS_CHECKBOX_4           5       /* callback function: DataLogCB */

#define  LOGONPANEL                      11      /* callback function: LogonPanel_Callback */
#define  LOGONPANEL_OPERATORS            2       /* callback function: Logon_Callback */
#define  LOGONPANEL_EXIT_BUTTON          3       /* callback function: secondquit */
#define  LOGONPANEL_PTS_CAL_BUTTON       4       /* callback function: Logon_Callback */
#define  LOGONPANEL_VENT_CAL_BUTTON      5       /* callback function: Logon_Callback */
#define  LOGONPANEL_LOGIN_BUTTON         6       /* callback function: Logon_Callback */
#define  LOGONPANEL_PTS2000_BOX          7
#define  LOGONPANEL_PTS2000_BOX_2        8
#define  LOGONPANEL_PTS2000_LBL          9
#define  LOGONPANEL_PTS2000_LBL_2        10
#define  LOGONPANEL_PICTURE              11
#define  LOGONPANEL_VERSION_LBL          12
#define  LOGONPANEL_840_GUISERIAL        13
#define  LOGONPANEL_840_BDSERIAL         14
#define  LOGONPANEL_PTS_SERIAL_LBL_2     15
#define  LOGONPANEL_CAL_LAST_LBL_2       16
#define  LOGONPANEL_SW_VERSION_LBL_2     17
#define  LOGONPANEL_CAL_LAST_LBL_3       18
#define  LOGONPANEL_PICTURE_2            19
#define  LOGONPANEL_PTS2000_LBL_3        20
#define  LOGONPANEL_840_LBL_4            21
#define  LOGONPANEL_PTS_SERIAL_LBL       22
#define  LOGONPANEL_SW_VERSION_LBL       23
#define  LOGONPANEL_840_GUISERIAL_LBL    24
#define  LOGONPANEL_840_BDSERIAL_LBL     25
#define  LOGONPANEL_PTS2000_LBL_5        26
#define  LOGONPANEL_CAL_LAST_LBL         27
#define  LOGONPANEL_CAL_NEXT_LBL         28

#define  LOOPPANEL                       12
#define  LOOPPANEL_MAXNUMBER             2
#define  LOOPPANEL_UNTILFAIL             3
#define  LOOPPANEL_OK                    4       /* callback function: LoopCallback */
#define  LOOPPANEL_CANCEL                5       /* callback function: LoopCallback */
#define  LOOPPANEL_DECORATION            6

#define  MANUALPNL                       13      /* callback function: ManualPnlCB */
#define  MANUALPNL_WORKORDER             2
#define  MANUALPNL_VENTASSET             3
#define  MANUALPNL_GROUNDRES             4
#define  MANUALPNL_VOLTAGE               5
#define  MANUALPNL_FRWDLEAK              6
#define  MANUALPNL_REVLEAK               7
#define  MANUALPNL_GRNDISOL              8
#define  MANUALPNL_DCVOLTS               9
#define  MANUALPNL_ACVOLTS               10
#define  MANUALPNL_AIRREG                11
#define  MANUALPNL_O2REG                 12
#define  MANUALPNL_EST_PF                13
#define  MANUALPNL_SST_PF                14
#define  MANUALPNL_OK                    15
#define  MANUALPNL_DECORATION_2          16
#define  MANUALPNL_DECORATION_3          17
#define  MANUALPNL_TEXTMSG_3             18
#define  MANUALPNL_TEXTMSG_2             19
#define  MANUALPNL_DCVUNITS              20
#define  MANUALPNL_ACVUNITS              21
#define  MANUALPNL_DECORATION_4          22
#define  MANUALPNL_TEXTMSG_4             23
#define  MANUALPNL_DECORATION_5          24
#define  MANUALPNL_TEXTMSG_7             25
#define  MANUALPNL_TEXTMSG               26
#define  MANUALPNL_FRWDLEAK_UNITS        27
#define  MANUALPNL_REVLEAK_UNITS         28
#define  MANUALPNL_GRND_UNITS            29
#define  MANUALPNL_TEXTMSG_5             30
#define  MANUALPNL_TEXTMSG_6             31
#define  MANUALPNL_VOLTUNITS             32
#define  MANUALPNL_DECORATION            33
#define  MANUALPNL_TEXTMSG_8             34

#define  PASSWDPNL                       14
#define  PASSWDPNL_PASSWORD              2       /* callback function: Passwd_Callback */
#define  PASSWDPNL_OK_BUTTON             3       /* callback function: Passwd_Callback */
#define  PASSWDPNL_CANCEL_BUTTON         4       /* callback function: Passwd_Callback */
#define  PASSWDPNL_USERNAME              5
#define  PASSWDPNL_DECORATION            6
#define  PASSWDPNL_ERROR                 7

#define  PERF_PANEL                      15
#define  PERF_PANEL_DONE                 2
#define  PERF_PANEL_ABORT                3
#define  PERF_PANEL_BODY_WEIGHT          4
#define  PERF_PANEL_TRIGGER              5
#define  PERF_PANEL_PEEP                 6
#define  PERF_PANEL_AC_TITLE             7
#define  PERF_PANEL_MANTYPE              8
#define  PERF_PANEL_O2                   9
#define  PERF_PANEL_P                    10
#define  PERF_PANEL_PSENS                11
#define  PERF_PANEL_TPL                  12
#define  PERF_PANEL_VMAX                 13
#define  PERF_PANEL_FSET                 14
#define  PERF_PANEL_PI                   15
#define  PERF_PANEL_VT                   16
#define  PERF_PANEL_TI                   17
#define  PERF_PANEL_WAVEFORM             18
#define  PERF_PANEL_INST_BOX             19
#define  PERF_PANEL_TESTTITLE            20
#define  PERF_PANEL_O2READING            21
#define  PERF_PANEL_PANEL_TIMER          22      /* callback function: Flasher */
#define  PERF_PANEL_VMAX_L               23
#define  PERF_PANEL_PICTURE              24
#define  PERF_PANEL_DECORATION           25
#define  PERF_PANEL_DECORATION_2         26
#define  PERF_PANEL_FREQ_F               27
#define  PERF_PANEL_O2PERCENT            28
#define  PERF_PANEL_AC_MIN               29
#define  PERF_PANEL_AC                   30
#define  PERF_PANEL_VT_TITLE             31
#define  PERF_PANEL_TI_S                 32
#define  PERF_PANEL_VMAX_TITLE           33
#define  PERF_PANEL_TPL_TITLE            34
#define  PERF_PANEL_TPL_S                35
#define  PERF_PANEL_P_PER                36
#define  PERF_PANEL_P_TITLE              37
#define  PERF_PANEL_PSENS_CM             38
#define  PERF_PANEL_VTRIG_MIN            39
#define  PERF_PANEL_PSENS_H20            40
#define  PERF_PANEL_O2_TITLE             41
#define  PERF_PANEL_O2_PER               42
#define  PERF_PANEL_PEEP_TITLE           43
#define  PERF_PANEL_PEEP_CM              44
#define  PERF_PANEL_PEEP_H20             45
#define  PERF_PANEL_PI_TITLE             46
#define  PERF_PANEL_PI_CM                47
#define  PERF_PANEL_PI_H20               48
#define  PERF_PANEL_TI_TITLE             49
#define  PERF_PANEL_VMAX_MIN             50
#define  PERF_PANEL_PSENS_TITLE          51
#define  PERF_PANEL_VTRIG_L              52
#define  PERF_PANEL_VT_ML                53
#define  PERF_PANEL_TESTCOUNT            54
#define  PERF_PANEL_O2TIMER              55      /* callback function: O2TimerCB */

#define  PTSPANEL                        16
#define  PTSPANEL_CALIBRATE              2       /* callback function: CmdAutoZero */
#define  PTSPANEL_RESET                  3       /* callback function: CmdPTSReset */
#define  PTSPANEL_DECORATION_2           4
#define  PTSPANEL_PICTURE_14             5
#define  PTSPANEL_DECORATION             6
#define  PTSPANEL_DECORATION_3           7
#define  PTSPANEL_DECORATION_4           8
#define  PTSPANEL_DECORATION_5           9
#define  PTSPANEL_DECORATION_6           10
#define  PTSPANEL_DECORATION_7           11
#define  PTSPANEL_DECORATION_8           12
#define  PTSPANEL_DECORATION_9           13
#define  PTSPANEL_PICTURE                14
#define  PTSPANEL_TEXTMSG_3              15
#define  PTSPANEL_TEXTMSG_4              16
#define  PTSPANEL_TEXTMSG_2              17

#define  REPORTOPTS                      17
#define  REPORTOPTS_STDCSE               2
#define  REPORTOPTS_TESTRESULTS          3
#define  REPORTOPTS_DIAGLOG              4
#define  REPORTOPTS_INFOLOG              5
#define  REPORTOPTS_ALARMLOG             6
#define  REPORTOPTS_TESTLOG              7
#define  REPORTOPTS_OK                   8
#define  REPORTOPTS_EXIT                 9
#define  REPORTOPTS_DECORATION           10

#define  SELECTRPT                       18
#define  SELECTRPT_PREVIEW               2
#define  SELECTRPT_EXIT                  3
#define  SELECTRPT_DELETE                4
#define  SELECTRPT_LISTRECORDS           5
#define  SELECTRPT_DECORATION            6
#define  SELECTRPT_TXTPREVIEW            7
#define  SELECTRPT_TXTDELETE             8
#define  SELECTRPT_TXTCANCEL             9

#define  SPLASH                          19
#define  SPLASH_SEARCHCANCEL             2       /* callback function: SplashCallback */
#define  SPLASH_PICTURE                  3
#define  SPLASH_EVALMSG                  4
#define  SPLASH_COMSTATUS                5
#define  SPLASH_COPYRIGHT                6
#define  SPLASH_COPYRIGHT_2              7
#define  SPLASH_SWVERSION                8
#define  SPLASH_SW_LBL                   9

#define  SYSPANEL                        20
#define  SYSPANEL_CMD_HELP               2       /* callback function: SysPanelCallback */
#define  SYSPANEL_CMD_ADMIN              3       /* callback function: SysPanelCallback */
#define  SYSPANEL_CMD_REPORT             4       /* callback function: SysPanelCallback */
#define  SYSPANEL_CMD_EXIT               5       /* callback function: SysPanelCallback */
#define  SYSPANEL_CMD_TESTS              6       /* callback function: SysPanelCallback */
#define  SYSPANEL_CMD_840                7       /* callback function: SysPanelCallback */
#define  SYSPANEL_CMD_PTS                8       /* callback function: SysPanelCallback */
#define  SYSPANEL_STATUS                 9
#define  SYSPANEL_USERNAME               10
#define  SYSPANEL_CLOCKTIMER             11      /* callback function: MainClockTimer */
#define  SYSPANEL_SYSTEM_PANEL           12
#define  SYSPANEL_SYS_TT_TIMER           13      /* callback function: ToolTipTimer */
#define  SYSPANEL_TIME                   14
#define  SYSPANEL_DATE                   15
#define  SYSPANEL_DECORATION             16

#define  SYSSTATPNL                      21
#define  SYSSTATPNL_DONE                 2
#define  SYSSTATPNL_TEXT_BOX1            3
#define  SYSSTATPNL_DECORATION           4
#define  SYSSTATPNL_DECORATION_2         5
#define  SYSSTATPNL_TEXTMSG              6

#define  TESTOPTS                        22
#define  TESTOPTS_MANUALTEST             2       /* callback function: TOptionsCallback */
#define  TESTOPTS_APTEST                 3       /* callback function: TOptionsCallback */
#define  TESTOPTS_BDATEST                4       /* callback function: TOptionsCallback */
#define  TESTOPTS_GUIATEST               5       /* callback function: TOptionsCallback */
#define  TESTOPTS_CPRESTEST              6       /* callback function: TOptionsCallback */
#define  TESTOPTS_AIRFLOWTEST            7       /* callback function: TOptionsCallback */
#define  TESTOPTS_OXYFLOWTEST            8       /* callback function: TOptionsCallback */
#define  TESTOPTS_FIRMWARETEST           9       /* callback function: TOptionsCallback */
#define  TESTOPTS_VOLPERF                10      /* callback function: TOptionsCallback */
#define  TESTOPTS_PRESPERF               11      /* callback function: TOptionsCallback */
#define  TESTOPTS_PEEPPERF               12      /* callback function: TOptionsCallback */
#define  TESTOPTS_FIO2PERF               13      /* callback function: TOptionsCallback */
#define  TESTOPTS_STOPONFAILS            14      /* callback function: TOptionsCallback */
#define  TESTOPTS_ENABLEALL              15      /* callback function: TOptionsAllCB */
#define  TESTOPTS_DECORATION             16
#define  TESTOPTS_TEXTMSG                17
#define  TESTOPTS_TEXTMSG_2              18

#define  TESTPANEL                       23
#define  TESTPANEL_TESTUUT_DEV           2       /* callback function: MainPanelCallback */
#define  TESTPANEL_PAUSE_BTN             3       /* callback function: MainPanelCallback */
#define  TESTPANEL_STOP_BTN              4       /* callback function: MainPanelCallback */
#define  TESTPANEL_RUNTEST_DEV           5       /* callback function: MainPanelCallback */
#define  TESTPANEL_LOOPTEST_DEV          6       /* callback function: MainPanelCallback */
#define  TESTPANEL_SEQUENCEDISPLAY       7       /* callback function: MainPanelCallback */
#define  TESTPANEL_TESTDISPLAY           8       /* callback function: MainPanelCallback */
#define  TESTPANEL_SYSTEMSTATUS          9
#define  TESTPANEL_STATUS_ACK            10
#define  TESTPANEL_PICTURE_3             11
#define  TESTPANEL_PICTURE_4             12
#define  TESTPANEL_PICTURE_5             13
#define  TESTPANEL_PICTURE_6             14
#define  TESTPANEL_PICTURE_7             15
#define  TESTPANEL_PICTURE_8             16
#define  TESTPANEL_PICTURE_9             17
#define  TESTPANEL_PICTURE_10            18
#define  TESTPANEL_PICTURE_11            19
#define  TESTPANEL_PICTURE_12            20
#define  TESTPANEL_PICTURE_1             21
#define  TESTPANEL_PICTURE_2             22
#define  TESTPANEL_PICTURE_14            23
#define  TESTPANEL_TEXTMSG_3             24
#define  TESTPANEL_SYSMESSAGE            25
#define  TESTPANEL_RESULT_5              26
#define  TESTPANEL_RESULT_6              27
#define  TESTPANEL_RESULT_7              28
#define  TESTPANEL_RESULT_8              29
#define  TESTPANEL_RESULT_9              30
#define  TESTPANEL_RESULT_10             31
#define  TESTPANEL_RESULT_11             32
#define  TESTPANEL_RESULT_12             33
#define  TESTPANEL_PROMPT_AREA           34
#define  TESTPANEL_TEXTMSG_4             35
#define  TESTPANEL_DECORATION            36
#define  TESTPANEL_DECORATION_3          37
#define  TESTPANEL_DECORATION_1          38
#define  TESTPANEL_DECORATION_4          39
#define  TESTPANEL_DECORATION_5          40
#define  TESTPANEL_DECORATION_6          41
#define  TESTPANEL_DECORATION_7          42
#define  TESTPANEL_DECORATION_8          43
#define  TESTPANEL_RESULT_1              44
#define  TESTPANEL_RESULT_2              45
#define  TESTPANEL_RESULT_3              46
#define  TESTPANEL_RESULT_4              47
#define  TESTPANEL_TEXTMSG_2             48
#define  TESTPANEL_DECORATION_2          49
#define  TESTPANEL_TEXTMSG_5             50

#define  USERCONFIG                      24
#define  USERCONFIG_USERLIST             2       /* callback function: UserConfig */
#define  USERCONFIG_ADDUSER              3       /* callback function: UserConfig */
#define  USERCONFIG_DELETEUSER           4       /* callback function: UserConfig */
#define  USERCONFIG_USERPWD              5       /* callback function: UserConfig */
#define  USERCONFIG_MAXUSERS_MSG         6
#define  USERCONFIG_TEXTMSG_3            7
#define  USERCONFIG_TEXTMSG_4            8
#define  USERCONFIG_TEXTMSG_5            9

#define  _840PANEL                       25
#define  _840PANEL_CAPDIAGLOG            2       /* callback function: Set840Callback */
#define  _840PANEL_CLRDIAGLOG            3       /* callback function: Set840Callback */
#define  _840PANEL_SETCOMPHOURS          4       /* callback function: Set840Callback */
#define  _840PANEL_SETCOMPSERIAL         5       /* callback function: Set840Callback */
#define  _840PANEL_DECORATION_2          6
#define  _840PANEL_DECORATION_3          7
#define  _840PANEL_DECORATION_4          8
#define  _840PANEL_DECORATION_5          9
#define  _840PANEL_DECORATION_6          10
#define  _840PANEL_DECORATION_7          11
#define  _840PANEL_DECORATION_8          12
#define  _840PANEL_DECORATION_9          13
#define  _840PANEL_PICTURE_14            14
#define  _840PANEL_DECORATION            15
#define  _840PANEL_PICTURE               16
#define  _840PANEL_CAPTURE_LOGS_LABEL    17
#define  _840PANEL_CLEAR_LOGS_LABEL      18
#define  _840PANEL_RESET_COMP_HRS_LABEL  19
#define  _840PANEL_SET_COMP_SERNUM_LABEL 20
#define  _840PANEL_TEXTMSG_2             21


     /* Menu Bars, Menus, and Menu Items: */

#define  MENU                            1
#define  MENU_FILE                       2
#define  MENU_FILE_LOGOUT                3       /* callback function: CBFileMenuLogout */
#define  MENU_FILE_SEPARATOR             4
#define  MENU_FILE_EXIT                  5       /* callback function: CBFileMenuExit */
#define  MENU_EDIT                       6
#define  MENU_EDIT_CALIBRAT              7       /* callback function: CBEditMenuCalibrate */
#define  MENU_VIEW                       8
#define  MENU_VIEW_TESTCONT              9       /* callback function: CBViewMenuTest */
#define  MENU_VIEW_SCREEN                10      /* callback function: CBViewMenu840 */
#define  MENU_VIEW_PTS2000S              11      /* callback function: CBViewMenuPTS */
#define  MENU_VIEW_SEPARATOR_2           12
#define  MENU_VIEW_REPORT                13      /* callback function: CBMenuReport */
#define  MENU_OPTIONS                    14
#define  MENU_OPTIONS_SETUP              15      /* callback function: CBMenuSetup */
#define  MENU_HELP                       16
#define  MENU_HELP_CONTENTS              17      /* callback function: CBMenuHelpContents */
#define  MENU_HELP_ABOUT                 18      /* callback function: CBMenuHelpAbout */


     /* Callback Prototypes: */ 

int  CVICALLBACK AcceptAutoDetect(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Adduser_Callback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK AutoDetect840(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK AutoDetectEnabled840(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK AutoDetectEnablePTS(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK AutoDetectPTS(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK BaseCallback(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK BaudRate840(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK BaudRatePTS(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK CBEditMenuCalibrate(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK CBFileMenuExit(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK CBFileMenuLogout(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK CBMenuHelpAbout(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK CBMenuHelpContents(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK CBMenuReport(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK CBMenuSetup(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK CBViewMenu840(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK CBViewMenuPTS(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK CBViewMenuTest(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK CmdAutoZero(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CmdPTSReset(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ComPort840(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ComPortPTS(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CompPanelCB(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ComSettingPanel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DataLogCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK DB_Conf_CB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Flasher(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK LanguageSelect(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK LogonPanel_Callback(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Logon_Callback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK LoopCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK MainClockTimer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK MainPanelCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ManualPnlCB(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK O2TimerCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Passwd_Callback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK secondquit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK Set840Callback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SplashCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK SysPanelCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK ToolTipTimer(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK TOptionsAllCB(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK TOptionsCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK UserConfig(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
