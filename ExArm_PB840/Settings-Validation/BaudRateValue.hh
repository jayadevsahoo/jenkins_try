
#ifndef BaudRateValue_HH
#define BaudRateValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BaudRateValue - Values of Serial Baud Rate Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BaudRateValue.hhv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date:  20-Sep-2000    DR Number: 5769
//  Project: GUIComms
//  Description:
//		Added single screen compatibility.  Added 38400 baud rate.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct BaudRateValue
{
  //@ Type:  BaudRateValueId
  // All of the possible values of the DCI Baud Rate Setting.
  enum BaudRateValueId
  {
    // $[01191] -- values of the range of DCI Baud Rate Setting...
    // $[07046] -- values of the range of Service Baud Rate Setting...
    BAUD_1200_VALUE,
    BAUD_2400_VALUE,
    BAUD_4800_VALUE,
    BAUD_9600_VALUE,
    BAUD_19200_VALUE,
    BAUD_38400_VALUE,
	SERVICE_MODE_TOTAL_BAUD_RATES = BAUD_38400_VALUE,

    TOTAL_BAUD_RATES
  };
};


#endif // BaudRateValue_HH 
