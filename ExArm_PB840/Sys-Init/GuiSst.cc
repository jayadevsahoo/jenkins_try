#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: GuiSst - GUI State: SST.
//---------------------------------------------------------------------
//@ Interface-Description
//  The GuiSst class is a FsmState used by the GuiFsm finite state
//  machine.  The class represents the state that GUI Task Control
//  enters upon completing a GUI transition to the SST state.  It
//  contains methods for processing events while in this state.
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the GUI SST state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  GuiSst is derived from the FsmState class.  It overrides the
//  virtual method receiveBdReady to provide state specific behavior
//  when it receives a BdReadyMessage.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiSst.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

//TODO E600 added GUI only
#if defined(SIGMA_GUI_CPU)

#include "GuiSst.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "GuiFsm.hh"
#include "GuiSstInit.hh"
#include "Reset.hh"
#include "TaskControlAgent.hh"
#include "BdReadyMessage.hh"
#include "TaskAgentManager.hh"
#include "GuiOnlineInit.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiSst [default constructor]
//
//@ Interface-Description
//  GuiSst default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiSst::GuiSst()
{
    CALL_TRACE("GuiSst()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiSst [destructor]
//
//@ Interface-Description
//  GuiSst class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiSst::~GuiSst()
{
    CALL_TRACE("~GuiSst()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

const char*
GuiSst::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "GuiSst";
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with
//  this GuiOnline state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the ONLINE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SigmaState
GuiSst::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_SST;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveBdReady
//
//@ Interface-Description
//  The receiveBdReady method processes a BdReadyMessage received
//  from BD task control by GUI Task-Control in the GuiSst state.
//  It updates the local state of the BD from the message contents.
//  For BD in the SST state, it forwards the BdReadyMessage 
//  to the applications and transitions back through SST-INIT to
//  resynchronize the processors.  For any other BD state, this method 
//  resets the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes Fsm::SetBdState to set the local state of the
//  BD to the state contained in the BdReadyMessage.  
//  TaskAgentManager::Send forwards the BdReadyMessage to
//  each managed task.  Reset::Initiate() resets the GUI.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
GuiSst::receiveBdReady(const BdReadyMessage& message) const
{
    CALL_TRACE("receiveBdReady(const BdReadyMessage&)");
 
    FsmState::SetBdState(message);

    switch (message.getState())
    {
        case STATE_SST:   // $[TI1]
            TaskAgentManager::Send(message);
            GuiFsm::GetGuiSstInitState().startTransition();
            break;
        case STATE_INOP:    // $[TI2]
        case STATE_ONLINE:
			// Check flag to verify GUI was NOT previously in
			// Service Mode before returning to ONLINE state.
            TaskAgentManager::Send(message);
            GuiFsm::GetGuiOnlineInitState().startTransition();
			break;
        case STATE_SERVICE:
        case STATE_START:
        case STATE_TIMEOUT:
        default:
            Reset::Initiate(INTENTIONAL, Reset::GUISST);
            break;
    }
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
GuiSst::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_GUISST,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

#endif //defined(SIGMA_GUI_CPU)


