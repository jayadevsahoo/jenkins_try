
#ifndef SoftwareRevNum_HH
#define SoftwareRevNum_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SoftwareRevNum - Software Revision Number.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Build/vcssrc/SoftwareRevNum.hhv   25.0.4.0   19 Nov 2013 14:01:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 002  By: sah    Date:  07-Apr-2000    DR Number: 5700
//  Project:  NeoMode
//  Description:
//      Added 'SOFTWARE_REL_TYPE' field.
//
//  Revision: 001  By: sah    Date:  17-Dec-1997    DR Number: None
//  Project:  Sigma (R8027)
//  Description:
//	Initial version (Integration baseline).
//  
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage


/// @class SoftwareRevNum
/// @detail Abstracts the software revision number. A software revision number
/// has 4 parts - major, minor, test, dev. The policy of incrementing any part
/// is external to this class.
class SoftwareRevNum
{
public:
	//@ Constant:  REV_NUM_SIZE 
	// Software revision number size (in chars).
	enum { REV_NUM_SIZE = 45 };

	///@detail Return the full software revision number as 
	///a string. E.g. return is "1.2.3.4"
	const char* GetAsFullString() const;

	///@detail Return a short software revision number. The
	///short version does not include 'test' and the 'dev' parts.
	///e.g. return is "1.2"
	const char* GetAsShortString() const;

	///@detail Returns the major revision number.
	/// E.g. return 1
	const Uint16 GetMajorNumber() const;
	///@detail Returns the major revision number.
	/// E.g. return 2
	const Uint16 GetMinorNumber() const;

	static const SoftwareRevNum& GetInstance();

protected:
	SoftwareRevNum();
	SoftwareRevNum(const SoftwareRevNum &rVersionNumber);
	void operator=(const SoftwareRevNum &rVersionNumber);

};


struct SoftwareRelType
{
  //@ Type:  Id
  // Type of software release.
  enum Id
  {
    DEVELOPMENT,	// daily build/internal release...
    PRE_PRODUCTION,	// DCO (X01) release...
    PRODUCTION 		// CO release...
  };

  //@ Constant:  SOFTWARE_REL_TYPE
  // Software's release type.
  static const Id  SOFTWARE_REL_TYPE;
};

#endif // SoftwareRevNum_HH 
