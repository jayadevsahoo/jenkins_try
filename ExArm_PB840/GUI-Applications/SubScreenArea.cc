#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SubScreenArea - An area in which subscreens can be
// displayed.  Both the Upper and the Lower screens contain SubScreenArea's.
//---------------------------------------------------------------------
//@ Interface-Description
// This class provides subscreen activation/deactivation functionality as well
// as default sizing, coloring, and the exclusivity of subscreen buttons.  The
// main interface methods are activateSubScreen() (which displays a particular
// subscreen) and deactivateSubScreen() (which removes the current subscreen
// from the display) and can be used by any object needing to display/undisplay
// a subscreen.
//
// There is the notion of a "default" subscreen, a subscreen which is
// activated if no other subscreen is active.  This subscreen can be set
// via the setDefaultSubScreen() method and, if needed to be specially
// activated, there is also an activateDefaultSubScreen() method
// provided.
//
// Because subscreen areas are responsible for handling some key presses
// (from the external Key Panel) SubScreenArea multiply inherits from
// KeyPanelTarget so that Key Panel events can be received (hence the
// empty keyPanelPressHappened() and keyPanelReleaseHappened() methods).
//
// Objects can also test if a subscreen is currently being displayed via
// the isCurrentSubScreen() method and access the button which activated
// the current subscreen by calling getCurrentSubScreenButton().
//---------------------------------------------------------------------
//@ Rationale
// Both the Upper and Lower subscreen areas have very similar functionality and
// their commonality has been grouped together in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
// SubScreenArea is a container that contains at most one subscreen,
// i.e., the active (or current) subscreen.  It stores pointers to the
// active and default subscreens and to the active and default subscreen
// buttons, buttons which are used to activate their respective
// subscreen (though some subscreens have no associated subscreen
// button, e.g. the Help subscreen).
//
// Subscreens are activated (displayed) via the activateSubScreen() call.  This
// causes any previously active subscreen to be deactivated.  Subscreens are
// deactivated either directly by deactivateSubScreen() or indirectly by using
// activateSubScreen() to activate another subscreen.  If there is no active
// subscreen when deactivateSubScreen() is called then the default subscreen,
// if set, is activated.  Subscreen buttons (of subscreens which are
// deactivated) are set into the up position.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// SubScreenArea should not be instantiated directly, it should be inherited
// from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SubScreenArea.ccv   25.0.4.0   19 Nov 2013 14:08:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc	   Date:  21-Jan-2009    SCR Number: 6456
//  Project:  840S
//  Description:
//      Fixed breath timing diagram not switching to correct type when 
//      mode is changed and then main setting button pressed.
//
//  Revision: 005  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 004  By:  gdc    Date:  23-Oct-1998    DCS Number: 5224
//  Project:  BiLevel 840
//  Description:
//      Change activateSubScreen to activate the sub-screen before 
//		making it visible. Allows for better optimization during 
//		sub-screen activation.
// 
//  Revision: 003  By:  gdc    Date:  01-Oct-1998    DCS Number: 5192
//  Project:  BiLevel
//  Description:
//      Reverted to code used prior to Color version since the BiLevel
//      implementation for the maneuver sub-screen was incompatible with 
//      the Color implementation of this class. Added the method 
//      restorePreviousSubScreen to implement the functionality to restore 
//		the sub-screen that was active prior to activating a default sub-screen.
//
//  Revision: 002  By:  gdc    Date:  29-Jun-1998    DCS Number:
//  Project:  Color
//  Description:
//      Initial version.
//      Modified to automatically associate the sub-screen button
//      with the sub-screen and set it to the DOWN position when
//      the sub-screen is activated and set it UP when the sub-screen
//      is deactivated. The higher level application no longer needs
//      to associate the button here and set it down too.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SubScreenArea.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "SubScreen.hh"
#include "SubScreenButton.hh"

//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SubScreenArea()  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor simply initializes all members to NULL.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SubScreenArea::SubScreenArea(void) :
		pCurrentSubScreen_(NULL),
		pDefaultSubScreen_(NULL),
		pCurrentButton_(NULL),
		pDefaultButton_(NULL),
		pPreviousSubScreen_(NULL),
		pPreviousButton_(NULL),
		previousActivatedByOperator_(FALSE)
{
	CALL_TRACE("SubScreenArea::SubScreenArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SubScreenArea()  [Destructor]
//
//@ Interface-Description
// Destroys a SubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SubScreenArea::~SubScreenArea(void)
{
	CALL_TRACE("SubScreenArea::~SubScreenArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateSubScreen
//
//@ Interface-Description
// This method activates (displays) a subscreen and is passed a pointer to the
// SubScreen which needs to be activated and the SubScreenButton which is
// activating it (this parameter can be NULL if the screen is being activated
// by another source, e.g. by an off-screen key).
//---------------------------------------------------------------------
//@ Implementation-Description
// The principal actions of this method are to deactivate any current
// subscreen, to remove it from the subscreen area container, to activate the
// new subscreen, add it to the container and clear the Adjust Panel focus.
//
// $[01030] Button in same selection group should be deselected ...
// $[01076] Only one subscreen allowed in Lower Subscreen Area ...
// $[01147] Only one subscreen allowed in Upper Subscreen Area ...
// $[07011] Only one subscreen allowed in Lower Subscreen Area ...
// $[07021] Only one subscreen allowed in Upper Subscreen Area ...
//---------------------------------------------------------------------
//@ PreCondition
// The SubScreen pointer must be non-NULL
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenArea::activateSubScreen(SubScreen *pSubScreen,
								SubScreenButton *pButton,
								Boolean activatedByOperator)
{
	CALL_TRACE("activateSubScreen(SubScreen *,SubScreenButton *)");

	CLASS_PRE_CONDITION(pSubScreen != NULL);

	activatedByOperator_ = activatedByOperator;

	// Set any current button to up, $[01030]
	if (pCurrentButton_)
	{													// $[TI1]
		pCurrentButton_->setToUp();
	}													// $[TI2]

	// If the current subscreen is a different sub-screen than the one to activate,
	// remove any current subscreen from the subscreen area and deactivate it
	if ( pCurrentSubScreen_ && (pCurrentSubScreen_ != pSubScreen) )
	{													// $[TI3]
		if (pCurrentSubScreen_->isMainSetting() != pSubScreen->isMainSetting())
		{												// $[TI3.1]
			// If current subScreen is associated with main setting
			// and the new subScreen is not or vice versa,
			// then initialize (reset values to the adjusted context)
			// the main setting members.
			if (pCurrentSubScreen_->isMainSetting())
			{
				pCurrentSubScreen_->initializeMainSetting();
			}
			else
			{
				pSubScreen->initializeMainSetting();
			}
		}												// $[TI3.2]

		removeDrawable(pCurrentSubScreen_);
		pCurrentSubScreen_->deactivate();
	}													// $[TI4]

	// Store new current subscreen and button
	pCurrentSubScreen_ = pSubScreen;
	pCurrentButton_ = pButton;

	if (pCurrentButton_)
	{													// $[TI5]
		pCurrentButton_->setToDown();
	}													// $[TI6]

	// Activate new subscreen ...
	pCurrentSubScreen_->activate();

	// We activate the sub-screen before adding it to the sub-screen
	// area to improve performance. Many functions are short circuited 
	// if the sub-screen is not visible.

	// The sub-screen's activate method may actually deactivate the
	// sub-screen (eg. Maneuver Sub-Screen) and set the current sub 
	// screen pointer NULL so we check it here before adding it to 
	// the sub-screen area.

	if ( pCurrentSubScreen_ )
	{													// $[TI7]
		addDrawable(pCurrentSubScreen_);
	}													// $[TI8]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateSubScreen
//
//@ Interface-Description
// No parameters are passed.  This method is called whenever the current
// subscreen needs deactivation, usually when a subscreen button is pressed up
// by the operator or another one (not the current one) is pressed down.
//---------------------------------------------------------------------
//@ Implementation-Description
// The deactivated screen must be removed from the subscreen container list and
// deactivated.  Any current subscreen button is set to up.  If there is a
// default subscreen set then this subscreen is now activated.
//
// $[01148] Safety PCV screen must be displayed automatically if ...
// $[07044] The user shall be allowed to escape from the Service Mode ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenArea::deactivateSubScreen(void)
{
	CALL_TRACE("deactivateSubScreen(void)");

	// If there is a subscreen active, remove it from the display
	if (pCurrentSubScreen_)
	{													// $[TI1]
		removeDrawable(pCurrentSubScreen_);

		// Deactive the subscreen
		pCurrentSubScreen_->deactivate();
		pCurrentSubScreen_ = NULL;
	
		// If the current subscreen has an associated subscreen button
		// then set it to up
		if (pCurrentButton_)
		{												// $[TI2]
		pCurrentButton_->setToUp();
			pCurrentButton_ = NULL;
		}												// $[TI3]
	}													// $[TI4]

	// If there is a default subscreen then activate it and set any
	// associated default button to the down state
	if (pDefaultSubScreen_)
	{													// $[TI5]
		activateSubScreen(pDefaultSubScreen_, pDefaultButton_);
	}													// $[TI8]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: refreshActiveSubScreen
//
//@ Interface-Description
// Redraws the currrent active subscreen
//---------------------------------------------------------------------
//@ Implementation-Description
// The principal actions of this method are to deactivate any current
// subscreen, to remove it from the subscreen area container, to activate the
// new subscreen, add it to the container and clear the Adjust Panel focus.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenArea::refreshActiveSubScreen(void)
{
	CALL_TRACE("refreshActiveSubScreen(void)");

	if (pCurrentSubScreen_)
	{
		SubScreen *ptempSubScreen = pCurrentSubScreen_;
		SubScreenButton *ptempCurrentButton_ = pCurrentButton_;
		deactivateSubScreen();
		// if breath timing screen is not active redraw sbuscreen otherwise
		// just use default screen (blank)
		if (!ptempSubScreen->isMainSetting())
			activateSubScreen(ptempSubScreen, ptempCurrentButton_);
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDefaultSubScreen
//
//@ Interface-Description
// This method sets a default subscreen, a subscreen that will be activated
// if no other subscreen is activated.  The method is passed a pointer to
// SubScreen which will be activated and the SubScreenButton which
// activates it (if any).  This button will be automatically selected if
// the default subscreen is activated.
//---------------------------------------------------------------------
//@ Implementation-Description
// The information is simply stored.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenArea::setDefaultSubScreen(SubScreen *pDefaultSubScreen,
									SubScreenButton *pDefaultButton)
{
	CALL_TRACE("setDefaultSubScreen(SubScreen*, SubScreenButton*)");

	// Store new defaults
	pDefaultSubScreen_ = pDefaultSubScreen;
	pDefaultButton_ = pDefaultButton;					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateDefaultSubScreen
//
//@ Interface-Description
// Activates the default subscreen.  Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply calls the activateSubScreen() method to activate the  default
// subscreen and sets the default subscreen button (if any) into the
// down state.
//---------------------------------------------------------------------
//@ PreCondition
// The default subscreen must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenArea::activateDefaultSubScreen(void)
{
	CALL_TRACE("activateDefaultSubScreen(void)");

	CLASS_PRE_CONDITION(pDefaultSubScreen_ != NULL);

	pPreviousSubScreen_ = pCurrentSubScreen_;
	pPreviousButton_ = pCurrentButton_;
	previousActivatedByOperator_ = activatedByOperator_;

	// Activate default subscreen and button
	activateSubScreen(pDefaultSubScreen_, pDefaultButton_);

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: restorePreviousSubScreen
//
//@ Interface-Description
// Restores the sub-screen and button that was active when the default
// sub-screen was activated.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply calls the activateSubScreen() method with the sub-screen and
// button pointers stored prior to activating the default sub-screen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SubScreenArea::restorePreviousSubScreen(void)
{
	if ( pPreviousSubScreen_ )
	{ // $[TI1.1]
		activateSubScreen(pPreviousSubScreen_, 
			  			  pPreviousButton_, 
						  previousActivatedByOperator_);
	} // $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetCurrentSubScreenButton
//
//@ Interface-Description
// Restores the sub-screen and button that was active when the default
// sub-screen was activated.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply calls the activateSubScreen() method with the sub-screen and
// button pointers stored prior to activating the default sub-screen.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SubScreenArea::resetCurrentSubScreenButton(void)
{
	if (pCurrentButton_)
	{												// $[TI2]
		pCurrentButton_->setToUp();
		pCurrentButton_ = NULL;
	}												// $[TI3]

	// If there is a default subscreen then activate it and set any
	// associated default button to the down state
	if (pDefaultSubScreen_ && pCurrentSubScreen_ == pDefaultSubScreen_)
	{													// $[TI5]
		activateSubScreen(pDefaultSubScreen_, pDefaultButton_);
	}													// $[TI8]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SubScreenArea::SoftFault(const SoftFaultID  softFaultID,
						 const Uint32       lineNumber,
						 const char*        pFileName,
						 const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SUBSCREENAREA,
							lineNumber, pFileName, pPredicate);
}


