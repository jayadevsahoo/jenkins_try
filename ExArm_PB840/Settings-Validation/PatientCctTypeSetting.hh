 
#ifndef PatientCctTypeSetting_HH
#define PatientCctTypeSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  PatientCctTypeSetting - Patient circuit type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PatientCctTypeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      * changed 'isEnabledValue()' to a public method, to provide
//        support for the new drop-down menus
//
//  Revision: 004   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Incorporated initial specifications for NeoMode Project.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "BatchDiscreteSetting.hh"
//@ End-Usage


class PatientCctTypeSetting : public BatchDiscreteSetting
{
  public:
    PatientCctTypeSetting(void);
    virtual ~PatientCctTypeSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual Boolean isEnabledValue(const DiscreteValue value) const;

    virtual SettingValue  getNewPatientValue(void) const;

    static void   SoftFault(const SoftFaultID softFaultID,
			    const Uint32      lineNumber,
			    const char*       pFileName  = NULL, 
			    const char*       pPredicate = NULL);

  private:
    // not implemented...
    PatientCctTypeSetting(const PatientCctTypeSetting&);
    void  operator=(const PatientCctTypeSetting&);
};


#endif // PatientCctTypeSetting_HH 
