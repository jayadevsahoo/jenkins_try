# Appends a blank space to each array entry. This is necessary because
# when we later search for strings we need them to match exactly. e.g.
# when we search for "EST_CANCEL" we don't want to find "EST_CANCEL_TEST"
# but only "EST_CANCEL".
{
  print $0 " "
}
