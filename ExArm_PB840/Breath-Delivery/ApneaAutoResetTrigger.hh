#ifndef ApneaAutoResetTrigger_HH
#define ApneaAutoResetTrigger_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: ApneaAutoResetTrigger - Monitors the system for the specific 
//	conditions that cause auto reset of apnea to be detected.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ApneaAutoResetTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  iv    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
 
#  include "Breath_Delivery.hh"

#  include "ModeTrigger.hh"

//@ Usage-Classes
//@ End-Usage


class ApneaAutoResetTrigger : public ModeTrigger
{
  public:
    ApneaAutoResetTrigger(void);
    virtual ~ApneaAutoResetTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);


  protected:
    virtual Boolean triggerCondition_(void);

  private:
    ApneaAutoResetTrigger(const ApneaAutoResetTrigger&);    // Declared but not implemented
    void operator=(const ApneaAutoResetTrigger&);   // Declared but not implemented

};


#endif // ApneaAutoResetTrigger_HH 
