#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmAndStatusArea - A container for displaying alarms and status
//									messages.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container class.
// This class contains AlarmMsg container objects, textual fields, and lines.
// This class also inherits timer callbacks (timerEventHappened()) via the
// abstract GuiTimerTarget class as well as setting change callbacks
// via the ContextObserver class.
//
// The current display configuration for this area is determined by the
// setDisplayMode() method and the number of current alarms.
//
// During normal GUI operation, this area relies on the data provided by the
// Alarms subsystem for the current alarm messages.  During normal GUI and
// Service GUI operation, this area depends on periodic timer callbacks to
// dynamically update the date/time readout in the status portion of the
// display.  Events from the Alarm-Analysis subsystem should result in
// a call to updateAlarmMsgDisplay() in order that we may display the new
// alarms.
//
// The area is divided horizontally in two halves.  The upper half displays
// only alarm messages whereas the lower half displays either an additional
// alarm message (if more that one alarm is currently active) or status
// information including the current circuit and humidification types and
// the current date and time.
// The novRamUpdateEventHappened() is called whenever the NovRamManger issures 
// an REAL_TIME_CLOCK_UPDATE event to update the date/time change to the log.
//---------------------------------------------------------------------
//@ Rationale
// For both the normal GUI and the Service GUI, this class is dedicated
// to managing the display of the second from the top portion of the
// Upper Screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// To support normal GUI operations, this area continuously displays
// one of the following configurations.
// >Von
//	1.	Status readout only.
//	2.	One alarm message and the status readout.
//	3.	The two highest urgency alarm messages with no status readout.
//	4.	Seamless merge with MoreAlarmsSubScreen (no status, up to two
//		alarm messages).
// >Voff
// An alarm message is a complete summary of a current alarm condition
// and the status readout displays the current date/time and current
// circuit and humidification types along with any warnings associated
// with those settings.
//
// When updated alarm messages are available, an external message must be sent
// to this class to force the internal alarms display processing to occur
// (i.e., somebody must call the updateAlarmMsgDisplay() method).  The
// updateAlarmMsgDisplay() method takes care of querying the Alarms subsystem
// for the number of current alarms.  Based on the number of current alarms,
// the private showAlarmMsg1_(), hideAlarmMsg1_(), showAlarmMsg2_(),
// hideAlarmMsg2_() methods do the work needed to show/hide/update the
// AlarmMsg1_ and AlarmMsg2_ member objects.
//
// To support the status readout, the current date/time display is updated via
// a timer callback which is set to fire several times a minute to keep the
// time display reasonably accurate.  The timer callback and associated
// processing is handled by timerEventHappened().  Changes to the circuit
// and humidification types are updated via callbacks from the Settings-
// Validation subsystem.
//
// Activation of the MoreAlarmsSubScreen sets this area to its "more
// alarms" display mode.  In this case, there are several graphics
// changes which cause the AlarmAndStatusArea to appear to "merge" with
// the adjacent UpperSubScreenArea.  Specifically, white "subscreen
// border" lines appear around the top, left, and right edges of the
// AlarmAndStatusArea, the white subscreen border line which normally
// separates the UpperSubScreenArea from the AlarmAndStatusArea is set
// to black, and the entire AlarmAndStatusArea has a black background to
// match the black background of the MoreAlarmsSubScreen.
//
// The various display objects are added to the Container when it is
// constructed.  The display configurations are implemented by
// hiding/showing the contained objects in the proper combinations.
//---------------------------------------------------------------------
//@ Fault-Handling
// Checks for out-of-range enum values with assertions.
//---------------------------------------------------------------------
//@ Restrictions
// None.  In particular, there is no restriction in changing from one
// display mode to another at any time.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmAndStatusArea.ccv   25.0.4.0   19 Nov 2013 14:07:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 021   By: gdc   Date:  26-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 020  By: gdc   Date:  21-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS6170 - NIV2
//      Modified per formal code review.
//  
//  Revision: 019  By: gdc   Date:  15-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Changes to support NIV with High Spont Insp Limit setting.
//    
//  Revision: 018  By: gdc   Date:  10-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Process high insp time alert based on breath trigger from BD.
// 
//  Revision: 017  By: gdc   Date:  01-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Changed High Ti,spont alert timeout to 15 seconds.
//
//  Revision: 016   By: gdc   Date:  30-May-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Added 30 second timeout for High Spontaneous Inspiratory Alert.
//
//  Revision: 015   By: gdc   Date:  27-May-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS 6170 - NIV2
//      Removed Insp Too Long alarm for NIV. Added High Ti spont alert.
//
//  Revision: 014   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  modified to use new names of string identifiers
//
//  Revision: 013  By: sah    Date: 30-Jun-1999   DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Initial NeoMode Project implementation added.
//
//  Revision: 012  By: hhd    Date: 4-June-1999  DCS Number:  5418
//  Project:  ATC
//  Description:
//		Modified so tube info. isn't displayed at vent startup time when the Same
//	Patient data is valid.  Also, simplify code when necessary.
//
//  Revision: 011  By: hhd    Date: 21-May-1999  DCS Number:  5391
//  Project:  ATC
//  Description:
//		Removed all references to string STATUS_CCT_CAL_HUMID_CHANGED_WARNING. 
//
//  Revision: 010  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 009  By:  gdc	   Date:  27-Jan-1998    DCS Number:  5322
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 008  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 007  By:  clw    Date:  21-May-1998    DCS Number: 
//  Project:  Sigma (R8027)
//  Description:
//      Modified japanese-only code to tune screen location.
//
//  Revision: 006  By:  clw    Date:  13-May-1998    DCS Number: 
//  Project:  Sigma (R8027)
//  Description:
//      Added japanese-only code to implement a different format for the date/time display.
//
//  Revision: 005  By:  yyy    Date:  08-Sep-1997    DCS Number: 2269
//  Project:  Sigma (R8027)
//  Description:
//      Used REAL_TIME_CLOCK_UPDATE the newly added callbacks registered for real-time
//		clock changes to update the timer.
//
//  Revision: 004  By:  yyy    Date:  04-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Registered for DATE/TIME settings to capture the setting change
//		events and update the date/time accordingly.
//
//  Revision: 003  By:  sah    Date:  28-Jul-1997    DCS Number: 2320
//  Project:  Sigma (R8027)
//  Description:
//      Now using 'AlarmStateManger' for retrieving the list of current
//		alarms.  Also, eliminated some PRODUCTION-mode warnings.
//	
//
//  Revision: 002  By:  sah    Date:  23-Jun-1997    DCS Number: 2214
//  Project:  Sigma (R8027)
//  Description:
//      Changed calling of 'getResultOfCurrRun()' to 'getResultId()'.
//      Also, changed some logic to incorporate cases when the
//      overall status is INCOMPLETE, and removed unneeded assertion
//      (done by client code).
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "AlarmAndStatusArea.hh"
#include "Sigma.hh"
#include <string.h>
#include "PatientDataRegistrar.hh"
#include "BreathDatumHandle.hh"
#include "BreathType.hh"
#include "GuiTimerRegistrar.hh"

#include "NovRamEventRegistrar.hh"
#include "NovRamManager.hh"
#include "ResultEntryData.hh"
#include "TestResultId.hh"
#include "TextUtil.hh"

//@ Usage-Classes
#include "SettingContextHandle.hh"
#include "AlarmAnnunciator.hh"
#include "GuiTimerId.hh"
#include "HumidTypeValue.hh"
#include "MiscStrs.hh"
#include "PatientCctTypeValue.hh"
#include "SortIterC_AlarmUpdate.hh"
#include "TimeStamp.hh"
#include "AlarmStateManager.hh"
#include "TubeTypeValue.hh"
#include "VentTypeValue.hh"
#include "MathUtilities.hh"
#include "ModeValue.hh"
#include "ServiceMode.hh"

//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 65;
static const Int32 CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 90;
static const Int32 FIXED_BORDER_X_ = 0;
static const Int32 FIXED_BORDER_Y_ = 87;
static const Int32 FIXED_BORDER_WIDTH_ = 640;
static const Int32 FIXED_BORDER_HEIGHT_ = 3;
static const Int32 TOP_BORDER_X_ = 0;
static const Int32 TOP_BORDER_Y_ = 0;
static const Int32 TOP_BORDER_WIDTH_ = 640;
static const Int32 TOP_BORDER_HEIGHT_ = 2;
static const Int32 LEFT_BORDER_X_ = 0;
static const Int32 LEFT_BORDER_Y_ = 0;
static const Int32 LEFT_BORDER_WIDTH_ = 3;
static const Int32 LEFT_BORDER_HEIGHT_ = 90;
static const Int32 RIGHT_BORDER_X_ = 637;
static const Int32 RIGHT_BORDER_Y_ = 0;
static const Int32 RIGHT_BORDER_WIDTH_ = 3;
static const Int32 RIGHT_BORDER_HEIGHT_ = 90;
static const Int32 ALARM_MSG_X_ = 0;
static const Int32 ALARM_MSG1_Y_ = 0;
static const Int32 ALARM_MSG2_Y_ = 44;
static const Int32 STATUS_AREA_Y_ = ALARM_MSG2_Y_;
static const Int32 DATE_TIME_X_ = 470;
static const Int32 DATE_TIME_Y_ = 38 - 14;
static const Int32 HIGH_SPONT_INSP_TIME_ALERT_TIMEOUT = 15000;



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmAndStatusArea  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.  Registers for timer callback for
// the date/time status display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmAndStatusArea::AlarmAndStatusArea(void) :
	displayMode_(AASA_MODE_NORMAL),
	fixedBorder_(FIXED_BORDER_X_, FIXED_BORDER_Y_,
						FIXED_BORDER_WIDTH_, FIXED_BORDER_HEIGHT_),
	alarmTopBorder_(TOP_BORDER_X_, TOP_BORDER_Y_,
						TOP_BORDER_WIDTH_, TOP_BORDER_HEIGHT_),
	alarmLeftBorder_(LEFT_BORDER_X_, LEFT_BORDER_Y_,
						LEFT_BORDER_WIDTH_, LEFT_BORDER_HEIGHT_),
	alarmRightBorder_(RIGHT_BORDER_X_, RIGHT_BORDER_Y_,
						RIGHT_BORDER_WIDTH_, RIGHT_BORDER_HEIGHT_),
	ventTypeLabel_(MiscStrs::STATUS_VENT_TYPE_NIV_LABEL,
						MiscStrs::TOUCHABLE_VENT_TYPE_NIV_MSG),
	highSpontInspTimeAlert_(MiscStrs::HIGH_SPONT_INSP_TIME_ALERT,
						MiscStrs::TOUCHABLE_HIGH_SPONT_INSP_TIME_MSG)
{
	CALL_TRACE("AlarmAndStatusArea::AlarmAndStatusArea(void)");

    // Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Set default background color
	setFillColor(Colors::EXTRA_DARK_BLUE);

	// Set color of fixed subscreen area border, then add it to the collection.
	fixedBorder_.setColor(Colors::FRAME_COLOR);
	fixedBorder_.setFillColor(Colors::FRAME_COLOR);
	addDrawable(&fixedBorder_);

	// Position and add the alarm msg containers, initially as hidden
	// objects.
	alarmMsg1_.setX(ALARM_MSG_X_);
	alarmMsg1_.setY(ALARM_MSG1_Y_);
	alarmMsg2_.setX(ALARM_MSG_X_);
	alarmMsg2_.setY(ALARM_MSG2_Y_);

	addDrawable(&alarmMsg1_);
	alarmMsg1_.setShow(FALSE);
	addDrawable(&alarmMsg2_);
	alarmMsg2_.setShow(FALSE);

	// Set colors of the alarm borders, then add them to the collection as
	// hidden objects.
	alarmTopBorder_.setColor(Colors::FRAME_COLOR);
	alarmTopBorder_.setFillColor(Colors::FRAME_COLOR);
	alarmLeftBorder_.setColor(Colors::FRAME_COLOR);
	alarmLeftBorder_.setFillColor(Colors::FRAME_COLOR);
	alarmRightBorder_.setColor(Colors::FRAME_COLOR);
	alarmRightBorder_.setFillColor(Colors::FRAME_COLOR);

	addDrawable(&alarmTopBorder_);
	alarmTopBorder_.setShow(FALSE);
	addDrawable(&alarmLeftBorder_);
	alarmLeftBorder_.setShow(FALSE);
	addDrawable(&alarmRightBorder_);
	alarmRightBorder_.setShow(FALSE);

	// Setup the status area container.
	statusArea_.setY(STATUS_AREA_Y_);
	statusArea_.setWidth(CONTAINER_WIDTH_);
	statusArea_.setHeight(CONTAINER_HEIGHT_ - STATUS_AREA_Y_);
	addDrawable(&statusArea_);

	// Add appropriate drawables to the status area.
	// Add the date/time string.
	dateTimeText_.setX(DATE_TIME_X_);
	dateTimeText_.setY(DATE_TIME_Y_);
	dateTimeText_.setColor(Colors::WHITE);
	statusArea_.addDrawable(&dateTimeText_);

	// Now for the Circuit and Humidification Type stuff
	circuitTypePrompt_.setText(MiscStrs::STATUS_CCT_TYPE_LABEL);
	circuitTypePrompt_.setColor(Colors::WHITE);
	circuitTypeText_.setColor(Colors::WHITE);
	humidificationTypePrompt_.setText(MiscStrs::STATUS_HUMID_TYPE_LABEL);
	humidificationTypePrompt_.setColor(Colors::WHITE);
	humidificationTypeText_.setColor(Colors::WHITE);
	ventTypeLabel_.setColor(Colors::YELLOW);
	ventTypeLabel_.setShow(FALSE);
	highSpontInspTimeAlert_.setColor(Colors::YELLOW);
	highSpontInspTimeAlert_.setShow(FALSE);
	tubeTypePrompt_.setText(MiscStrs::STATUS_TUBE_TYPE_LABEL);
	tubeTypePrompt_.setColor(Colors::WHITE);
	tubeTypePrompt_.setShow(FALSE);
	tubeTypeText_.setColor(Colors::WHITE);
	tubeTypeText_.setShow(FALSE);
	tubeIdPrompt_.setText(MiscStrs::STATUS_TUBE_ID_LABEL);
	tubeIdPrompt_.setColor(Colors::WHITE);
	tubeIdPrompt_.setShow(FALSE);
	tubeIdText_.setColor(Colors::WHITE);
	tubeIdText_.setShow(FALSE);
	statusArea_.addDrawable(&circuitTypePrompt_);
	statusArea_.addDrawable(&circuitTypeText_);
	statusArea_.addDrawable(&humidificationTypePrompt_);
	statusArea_.addDrawable(&humidificationTypeText_);
	statusArea_.addDrawable(&ventTypeLabel_);
	statusArea_.addDrawable(&highSpontInspTimeAlert_);
	statusArea_.addDrawable(&tubeTypePrompt_);
	statusArea_.addDrawable(&tubeTypeText_);
	statusArea_.addDrawable(&tubeIdPrompt_);
	statusArea_.addDrawable(&tubeIdText_);

	// We do not display current Circuit or Humidification Type if SST has
	// not been run at least once
	ResultEntryData overallSstResultData;

	// Get the overall testing status
	NovRamManager::GetOverallSstResult(overallSstResultData);
	TestResultId overallResult = overallSstResultData.getResultId();

	if (overallResult < PASSED_TEST_ID)
	{													// $[TI1]
		// don't show these textual prompts if SST hasn't completed...
		circuitTypePrompt_.setShow(FALSE);
		humidificationTypePrompt_.setShow(FALSE);
	}													// $[TI2]

	// Register myself as a callback target for wall clock events, then kick
	// off the "forever" wall clock timer.
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::WALL_CLOCK, this);
	GuiTimerRegistrar::StartTimer(GuiTimerId::WALL_CLOCK);
	timerEventHappened(GuiTimerId::WALL_CLOCK);	// initialize date/time strings

	// Attach itself to Context Subject
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID,
					 Notification::BATCH_CHANGED);

	// Register all logs with NovRam for receiving event updates
	// for changes to the date/time setting changes
	NovRamEventRegistrar::RegisterTarget(NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE,
										this);

	// Register for change in patient data
	PatientDataRegistrar::RegisterTarget(PatientDataId::IS_NIV_INSP_TOO_LONG_ITEM, this);

	isSpontInspTimeLimitActive_ = 
			SettingContextHandle::GetSettingApplicability(
				ContextId::ACCEPTED_CONTEXT_ID, SettingId::HIGH_SPONT_INSP_TIME)
		    == Applicability::CHANGEABLE;
	updatePtCctTypeDisplay_();
	updateHumidTypeDisplay_();
	updateTCTubeDisplay_(TRUE);
	updateNivLabel_(TRUE);
	updateHighSpontInspTimeAlert_();


	// Make sure display of alarm messages is up to date
	updateAlarmMsgDisplay();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmAndStatusArea  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Detach itself from the Setting Subject.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AlarmAndStatusArea::~AlarmAndStatusArea(void)
{
	CALL_TRACE("AlarmAndStatusArea::~AlarmAndStatusArea(void)");

	detachFromSubject_(ContextId::ACCEPTED_CONTEXT_ID,
					   Notification::BATCH_CHANGED);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDisplayMode
//
//@ Interface-Description
// Sets the AlarmAndStatusArea into one of its two display modes.  The
// 'displayMode' is the new display mode value (enum), one of
// AlarmAndStatusArea::AASA_MODE_NORMAL or AlarmAndStatusArea::AASA_MODE_MORE_ALARMS.
// When the More Alarms subscreen is displayed we must change our look so as to
// "join up" with that subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// In normal display mode (AASA_MODE_NORMAL), the "subscreen border" which
// graphically merges this area with the MoreAlarmsSubScreen is hidden.
// The normal subscreen border color is set to white and the background
// color of the area is set to extra dark gray.
//
// In "more alarms" display mode (AASA_MODE_MORE_ALARMS), the "subscreen
// border" is shown, normal subscreen border color is set to black, and
// the background color of the area is set to black to match the
// MoreAlarmsSubScreen.  This makes the AlarmAndStatusArea look like it
// is contiguously connected to the active MoreAlarmsSubScreen.
//
// There are no restrictions and no error checking on changing from
// one mode to a different mode.
//
// $[01146] The status display shall consist of ...
// $[01187] More Alarms subscreen should merge with Alarm and Status Area.
// $[01327] When displaying zero or one alarm analysis messages, the ...
//---------------------------------------------------------------------
//@ PreCondition
// The given displayMode value must be one of the expected enum values.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmAndStatusArea::setDisplayMode(DisplayMode displayMode)
{
	CALL_TRACE("AlarmAndStatusArea::setDisplayMode(displayMode)");

	// Check for duplication
	if (displayMode_ == displayMode)
	{													// $[TI1]
		return;
	}													// $[TI2]

	switch (displayMode_ = displayMode)
	{
	case AASA_MODE_NORMAL:								// $[TI3]
		// Normal mode, More Alarms subscreen is not being displayed.

		// Display the status area if alarm message 2 is not displayed.
		if (! alarmMsg2_.getShow())
		{												// $[TI4]
			statusArea_.setShow(TRUE);
		}												// $[TI5]

		// Hide the "More Alarms" border objects and update the colors of the
		// normal border and background.
		alarmTopBorder_.setShow(FALSE);
		alarmLeftBorder_.setShow(FALSE);
		alarmRightBorder_.setShow(FALSE);
		fixedBorder_.setShow(TRUE);
		setFillColor(Colors::EXTRA_DARK_BLUE);
		break;

	case AASA_MODE_MORE_ALARMS:							// $[TI6]
		// More Alarms subscreen is being displayed.

		// Show the "More Alarms" border objects and update the colors of the
		// normal border and background.  Hide the status area.
		alarmTopBorder_.setShow(TRUE);
		alarmLeftBorder_.setShow(TRUE);
		alarmRightBorder_.setShow(TRUE);
		fixedBorder_.setShow(FALSE);
		statusArea_.setShow(FALSE);
		setFillColor(Colors::BLACK);
		break;

	default:
		CLASS_ASSERTION(FALSE);		// Should never get here
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateAlarmMsgDisplay
//
//@ Interface-Description
// Queries the Alarms software for the number of alarms and configures
// the alarm message display accordingly.
//---------------------------------------------------------------------
//@ Implementation-Description
// Based on the number of current alarms, calls the private
// showAlarmMsg1_(), hideAlarmMsg1_(), showAlarmMsg2_(),
// hideAlarmMsg2_() methods to do the work needed to show/hide/update the
// alarmMsg1_ and alarmMsg2_ containers.
//
// $[01138] Two highest priority messages displayed in order.
// $[01143] When alarm condition is corrected alarm message shall disappear.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmAndStatusArea::updateAlarmMsgDisplay(void)
{
	CALL_TRACE("AlarmAndStatusArea::updateAlarmMsgDisplay(void)");

	Int32 numCurrentAlarms = -1;

	// Fetch current alarm list from AlarmAnnunciation subsystem, then determine
	// how many entries there are in the list.
	const AlarmUpdateSortedList& currentAlarmList =
										AlarmStateManager::GetCurrAlarmList();
	numCurrentAlarms = currentAlarmList.getNumItems();
	CLASS_ASSERTION(numCurrentAlarms >= 0);

	switch (numCurrentAlarms)
	{
	case 0:												// $[TI1]
		hideAlarmMsg2_();
		hideAlarmMsg1_();
		break;

	case 1:												// $[TI2]
		hideAlarmMsg2_();
		showAlarmMsg1_();
		break;

	default:											// $[TI3]
		showAlarmMsg1_();
		showAlarmMsg2_();
		break;
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the Wall Clock timer fires.  We must refresh the date/time
// fields whenever the date/time fields are visible.
//---------------------------------------------------------------------
//@ Implementation-Description
// Determines the current wall clock time using a TimeStamp object, then
// passes the wall clock time to the TextUtil::FormatTime() routine for
// conversion to raw C string format.  This raw C string is then
// converted to Cheap Text format and then rendered for display.
//
// $[01144] GUI shall update status display at least once per minute.
// $[01145] No compensation for Daylight Savings time needed.
//---------------------------------------------------------------------
//@ PreCondition
// The given 'timerId' must be the WALL_CLOCK value.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmAndStatusArea::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("AlarmAndStatusArea::timerEventHappened(timerId)");

	switch (timerId)
	{
	case GuiTimerId::WALL_CLOCK:						// $[TI1]
		{
			// Refresh the visible time/date fields.
			TimeStamp timeNow;
			wchar_t      cheapText[100];  

			TextUtil::FormatTime(cheapText, MiscStrs::TIME_DATE_TITLE_FORMAT,
																	timeNow);
			dateTimeText_.setText(cheapText);
		}
		break;

	case GuiTimerId::HIGH_SPONT_INSP_TIME_ALERT_TIMER:
		{
			if (alertTimer_.getPassedTime() >= HIGH_SPONT_INSP_TIME_ALERT_TIMEOUT)
			{
			  isNivInspTooLong_ = FALSE;
			  updateHighSpontInspTimeAlert_();
			}
		}
		break;

	default:
		CLASS_ASSERTION(FALSE);
		break;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened
//
//@ Interface-Description
// Handles update event notification for patient data fields.  The
// 'patientDataId' identifies which patient data value changed so that
// we know which data value to update.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// The given 'patientDataId' value must be one of the id's associated
// with the patient data fields in this area.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void 
AlarmAndStatusArea::patientDataChangeHappened(
								PatientDataId::PatientItemId patientDataId)
{
	CALL_TRACE("patientDataChangeHappened(patientDataId)");

	if (patientDataId == PatientDataId::IS_NIV_INSP_TOO_LONG_ITEM)
	{
	  BreathDatumHandle dataHandle(patientDataId);
	  isNivInspTooLong_ = dataHandle.getDiscreteValue().data;
	  updateHighSpontInspTimeAlert_();
	}
	else
	{
	  AUX_CLASS_ASSERTION_FAILURE(patientDataId);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateTCTubeDisplay_
//
//@ Interface-Description
// Update tube type and tube id in the Alarm Status area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Parse the flag which indicates if it's vent start up time.  The display
// of tube I.D. and type only occurs under either of the following condition:
//	1) It's Vent Startup time and Same Patient is valid and TUBE_ID/TYPE setting
//		is applicable.
//	2) It's not Vent Startup time and TUBE_ID/TYPE setting id is applicable.
//  We set all relevant display string's Show flag to TRUE.  We compose the
//  display strings using the corresponding setting values.  
//  Otherwise, we only display empty, blank strings.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
AlarmAndStatusArea::updateTCTubeDisplay_(Boolean isVentStartUp)
{

	if ( (isVentStartUp && SettingContextHandle::IsSamePatientValid()) ||
		 !isVentStartUp)
	{	   // $[TI1]
		// Check for display applicability of this settingId.  If this setting id is
		// not applicable, don't display its value.
		Boolean isApplicable =  SettingContextHandle::GetSettingApplicability(
								ContextId::ACCEPTED_CONTEXT_ID, SettingId::TUBE_ID)
									!= Applicability::INAPPLICABLE;

		if ( isApplicable )
		{	 // $[TI1.1]
			tubeTypePrompt_.setShow(TRUE);
			tubeTypeText_.setShow(TRUE);
			tubeIdPrompt_.setShow(TRUE);
			tubeIdText_.setShow(TRUE);
	
			DiscreteValue tubeType =
				SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,		 
													SettingId::TUBE_TYPE);
			switch (tubeType)
			{
				case TubeTypeValue::ET_TUBE_TYPE:		// $[TI1.1.1]
					tubeTypeText_.setText(MiscStrs::STATUS_TUBE_TYPE_ET_VALUE);
					break;
	
				case TubeTypeValue::TRACH_TUBE_TYPE:	// $[TI1.1.2]
					tubeTypeText_.setText(MiscStrs::STATUS_TUBE_TYPE_TRAC_VALUE);
					break;
			
				default:							 	// $[TI1.1.3]
					tubeTypeText_.setText(MiscStrs::EMPTY_STRING);
					break;
			}

			BoundedValue settingValue = 
				SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
													SettingId::TUBE_ID);
	   		Real32 tubeId = settingValue.value;
			wchar_t tubeIdStr[50];  
			swprintf(tubeIdStr, MiscStrs::STATUS_TUBE_ID_STRING_FORMAT, tubeId);
			tubeIdText_.setText(tubeIdStr);
		}	// Setting is applicable
		else 
		{	// $[TI1.2]
			tubeIdPrompt_.setShow(FALSE);
			tubeIdText_.setText(MiscStrs::EMPTY_STRING);
			tubeTypePrompt_.setShow(FALSE);
			tubeTypeText_.setText(MiscStrs::EMPTY_STRING);
		}
	}
	else if (isVentStartUp && !SettingContextHandle::IsSamePatientValid())
	{		 // $[TI2]
		// No same patient, so make sure tube info is not shown.
		tubeIdPrompt_.setShow(FALSE);
		tubeIdText_.setText(MiscStrs::EMPTY_STRING);
		tubeTypePrompt_.setShow(FALSE);
		tubeTypeText_.setText(MiscStrs::EMPTY_STRING);
	}
}			

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateNivLabel_
//
//@ Interface-Description
// Update vent type label in the Alarm Status area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Display of the vent type NIV label only occurs under either of the 
// following condition:
//   1) It's Vent Startup time and Same Patient is valid and vent type 
//      is NIV.
//   2) It's not Vent Startup time and vent type is NIV.
// 
// This NIV label is displayed in the same location as the tube-type and
// tube-id fields, but since the these settings are only applicable during
// invasive ventilation, the fields will never be displayed at the same
// time.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
AlarmAndStatusArea::updateNivLabel_(Boolean isVentStartUp)
{
  if ( (isVentStartUp && SettingContextHandle::IsSamePatientValid()) ||
       !isVentStartUp)
  {	     // $[TI1]
    DiscreteValue ventType = SettingContextHandle::GetSettingValue(
		ContextId::ACCEPTED_CONTEXT_ID, SettingId::VENT_TYPE); 
    ventTypeLabel_.setShow( VentTypeValue::NIV_VENT_TYPE == ventType );
  }
  else if (isVentStartUp && !SettingContextHandle::IsSamePatientValid())
  {		   // $[TI2]
    // No same patient, so don't show vent-type (NIV) info
    ventTypeLabel_.setShow(FALSE);
  }
}                       

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateHighSpontInspTimeAlert_
//
//@ Interface-Description
// Update the high spontaneous inspiratory time alert in the 
// Alarm Status area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Display of the high inspiratory time alert when the following 
// conditions occur:
//   1) vent type is NIV and the last end-of-breath patient data 
//      packet indicates the high inspiratory time limit was exceeded
//      and the breath terminated.
// 
// This alert is displayed in the same location as the tube-type and
// tube-id fields, but since the these settings are only applicable during
// invasive ventilation, the fields will never be displayed at the same
// time.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
AlarmAndStatusArea::updateHighSpontInspTimeAlert_(void)
{
  if ( isSpontInspTimeLimitActive_ && isNivInspTooLong_ )
  {
	highSpontInspTimeAlert_.setShow(TRUE);
	alertTimer_.now();
	GuiTimerRegistrar::StartTimer(
		GuiTimerId::HIGH_SPONT_INSP_TIME_ALERT_TIMER, this);
  }
  else
  {
	highSpontInspTimeAlert_.setShow(FALSE);
  }
}                       


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateHumidTypeDisplay_
//
//@ Interface-Description
// Update Humidification type in the Alarm Status area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Update the display according to the humidification type.  Also, 
// display a warning due to the humidification type change only when
// TC is not an option [01146].
//
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
AlarmAndStatusArea::updateHumidTypeDisplay_()
{
	DiscreteValue humidType =
				SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
													SettingId::HUMID_TYPE);

 	// Make sure humidification type prompt is shown
 	humidificationTypePrompt_.setShow(TRUE);
 	switch (humidType)
 	{
 		case HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER:   // $[TI1]
			humidificationTypeText_.
						setText(MiscStrs::STATUS_HUMID_TYPE_NON_HEATED_VALUE);
			break;

		case HumidTypeValue::HEATED_TUBING_HUMIDIFIER:		  // $[TI2]

			humidificationTypeText_.
						setText(MiscStrs::STATUS_HUMID_TYPE_HEATED_VALUE);
			break;

		case HumidTypeValue::HME_HUMIDIFIER:					  // $[TI3]

			humidificationTypeText_.
						setText(MiscStrs::STATUS_HUMID_TYPE_HME_VALUE);
			break;

		default:
			CLASS_ASSERTION(FALSE);
			break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePtCctTypeDisplay_
//
//@ Interface-Description
//  Update Patient Circuit Type on Alarm Status area.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Update the display according to the changed circuit type.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
AlarmAndStatusArea::updatePtCctTypeDisplay_()
{
	DiscreteValue patientCctType = 
				SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
													SettingId::PATIENT_CCT_TYPE);

	// Make sure circuit type prompt is shown
	circuitTypePrompt_.setShow(TRUE);

	// Circuit Type changed so display the new value
  	switch (patientCctType)
	{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT:	// $[TI1]
			circuitTypeText_.setText(MiscStrs::STATUS_CCT_TYPE_PEDIATRIC_VALUE);
			break;

	 	case PatientCctTypeValue::ADULT_CIRCUIT:		// $[TI2]
	 		circuitTypeText_.setText(MiscStrs::STATUS_CCT_TYPE_ADULT_VALUE);
	 		break;

	 	case PatientCctTypeValue::NEONATAL_CIRCUIT:		// $[TI3]
	 		circuitTypeText_.setText(MiscStrs::STATUS_CCT_TYPE_NEONATAL_VALUE);
	 		break;

	 	default:
	 		circuitTypeText_.setText(MiscStrs::EMPTY_STRING);
	 		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: batchSettingUpdate
//
//@ Interface-Description
// Called when the Circuit type, Humidification Type, Tube Id, Tube Type,
// Mode, Support Type or the DATE/TIME related sas changed. Passed the
// following parameters:
// >Von
//	settingId	The enum id of the setting which changed.
//	contextId	The context in which the change happened, either
//				ACCEPTED_CONTEXT_ID or ADJUSTABLE_CONTEXT_ID.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Only react to changes in the Accepted context.  Update the appropriate
// graphics according to which setting change and what the new (current) value
// of that setting is.
//---------------------------------------------------------------------
//@ PreCondition
// 	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
AlarmAndStatusArea::batchSettingUpdate(
							 const Notification::ChangeQualifier qualifierId,
							 const ContextSubject*               pSubject,
							 const SettingId::SettingIdType		 settingId
									  )
{
	CALL_TRACE("batchSettingUpdate(qualifierId, pSubject, settingId)");

	ResultEntryData overallSstResultData;

	// Get the overall testing status
	NovRamManager::GetOverallSstResult(overallSstResultData);

	// Only react to changes to the accepted context, and ignore changes
	// while SST has not completed.
	if ( (qualifierId == Notification::ACCEPTED)
		 && (PASSED_TEST_ID <= overallSstResultData.getResultId()))
	{													// $[TI1]
	  switch (settingId)
	  {
	  case SettingId::PATIENT_CCT_TYPE :
		  updatePtCctTypeDisplay_();
		  break;
	  case SettingId::HUMID_TYPE :
		  updateHumidTypeDisplay_();
		  break;
	  case SettingId::TUBE_TYPE :
	  case SettingId::TUBE_ID :
	  case SettingId::SUPPORT_TYPE :
	  case SettingId::MODE :
		  updateTCTubeDisplay_();
		  break;
	  case SettingId::VENT_TYPE :
		  updateNivLabel_();
		  break;
	  case SettingId::NUM_BATCH_IDS :
		  updatePtCctTypeDisplay_();
		  updateHumidTypeDisplay_();
		  updateTCTubeDisplay_();
		  updateNivLabel_();
		  break;
	  default:
		// do nothing
		break;
	  }

	  // process setting changes that affect High Spont Insp Time Alert
	  switch (settingId)
	  {
	  case SettingId::VENT_TYPE :
	  case SettingId::MODE :
	  case SettingId::NUM_BATCH_IDS :
		  isSpontInspTimeLimitActive_ = 
			SettingContextHandle::GetSettingApplicability(
				ContextId::ACCEPTED_CONTEXT_ID, SettingId::HIGH_SPONT_INSP_TIME)
		    == Applicability::CHANGEABLE;
		  if (!isSpontInspTimeLimitActive_)
		  {
			isNivInspTooLong_ = FALSE;
		  }
		  updateHighSpontInspTimeAlert_();
		  break;
	  default:
		  // do nothing
		  break;
	  }
	}													// $[TI2]
	// else ignore adjusted context

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: novRamUpdateEventHappened
//
//@ Interface-Description
// Called by NovRam manager, this function updates the display with
// the latest, real-time data in the log.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call timerEventHappened to process it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmAndStatusArea::novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId updateTypeId)
{
	// update our display.
	if (updateTypeId == NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE)
	{ 		// $[TI1.1]
		timerEventHappened(GuiTimerId::WALL_CLOCK);
	}		// $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: showAlarmMsg1_
//
//@ Interface-Description
// Displays the highest urgency alarm analysis message in the first
// alarm slot (alarmMsg1_).
//---------------------------------------------------------------------
//@ Implementation-Description
// Makes the alarmMsg1_ container object visible.  Fetches the first
// AlarmUpdate object from the Alarms subsystem and passes it to the
// alarmMsg1_ object.  The alarmMsg1_ object takes care of the low-level
// alarm message rendering details.
//---------------------------------------------------------------------
//@ PreCondition
// There must be at least one current alarm in the alarm update list.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmAndStatusArea::showAlarmMsg1_(void)
{
	CALL_TRACE("AlarmAndStatusArea::showAlarmMsg1_(void)");

	// Show first alarm container.
	alarmMsg1_.setShow(TRUE);

	// Fetch the first AlarmUpdate object from the Alarm-Annunciation
	// subsystem and pass it to the AlarmMsg display object.
	SortedIterC(AlarmUpdate) alarmUpdates(AlarmStateManager::GetCurrAlarmList());
	SAFE_CLASS_PRE_CONDITION(alarmUpdates.getNumItems() >= 1);

#if defined(SIGMA_DEVLOPMENT)
	SigmaStatus status =
#endif // defined(SIGMA_DEVLOPMENT)

	alarmUpdates.goFirst();

#if defined(SIGMA_DEVLOPMENT)
	SAFE_CLASS_ASSERTION(SUCCESS == status);
#endif // defined(SIGMA_DEVLOPMENT)

	alarmMsg1_.updateDisplay(alarmUpdates.currentItem());
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hideAlarmMsg1_
//
//@ Interface-Description
// Hides the alarmMsg1_ container object which occupies the first alarm
// slot.
//---------------------------------------------------------------------
//@ Implementation-Description
// Makes the alarmMsg1_ container object invisible.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmAndStatusArea::hideAlarmMsg1_(void)
{
	CALL_TRACE("AlarmAndStatusArea::hideAlarmMsg1_(void)");

	// Hide 1st alarm container.
	alarmMsg1_.setShow(FALSE);							// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: showAlarmMsg2_
//
//@ Interface-Description
// Displays the second highest urgency alarm analysis message in the
// second alarm slot (alarmMsg2_), hiding the status info fields.
//---------------------------------------------------------------------
//@ Implementation-Description
// Makes the alarmMsg2_ container object visible and makes the status
// info fields invisible.  Fetches the second AlarmUpdate object from
// the Alarms subsystem and passes it to the alarmMsg2_ object.  The
// alarmMsg2_ object takes care of the low-level alarm message rendering
// details.
//---------------------------------------------------------------------
//@ PreCondition
// There must be at least two current alarms in the alarm update list.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmAndStatusArea::showAlarmMsg2_(void)
{
	CALL_TRACE("AlarmAndStatusArea::showAlarmMsg2_(void)");

	// Show second alarm container and hide status area
	alarmMsg2_.setShow(TRUE);
	statusArea_.setShow(FALSE);

	// Fetch the second AlarmUpdate object from the Alarm-Annunciation
	// subsystem and pass it to the AlarmMsg display object.
	SortedIterC(AlarmUpdate) alarmUpdates(AlarmStateManager::GetCurrAlarmList());
	SAFE_CLASS_PRE_CONDITION(alarmUpdates.getNumItems() >= 2);

	alarmUpdates.goFirst();
	alarmUpdates.goNext();

	alarmMsg2_.updateDisplay(alarmUpdates.currentItem());
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hideAlarmMsg2_
//
//@ Interface-Description
// Hides the alarmMsg2_ container object which occupies the second alarm
// slot and redisplay the status fields. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Makes the alarmMsg2_ container object invisible.  If we're in normal
// display mode, then make the status info (date/time) fields visible.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
AlarmAndStatusArea::hideAlarmMsg2_(void)
{
	CALL_TRACE("AlarmAndStatusArea::hideAlarmMsg2_(void)");

	// Hide second alarm container and show the status area
	// if we're in "normal" display mode.
	alarmMsg2_.setShow(FALSE);
	if (AASA_MODE_NORMAL == displayMode_)
	{													// $[TI1]
		statusArea_.setShow(TRUE);
	}													// $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
AlarmAndStatusArea::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)  
{
	CALL_TRACE("AlarmAndStatusArea::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, ALARMANDSTATUSAREA,
							lineNumber, pFileName, pPredicate);
}
