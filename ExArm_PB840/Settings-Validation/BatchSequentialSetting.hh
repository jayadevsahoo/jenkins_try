
#ifndef BatchSequentialSetting_HH
#define BatchSequentialSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BatchSequentialSetting - Batch Sequential Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchSequentialSetting.hhv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.5.1.0) into Rev "BiLevel" (1.5.2.0)
//	Added new constructor parameters that may be called by derived
//	class to disable bounds for this sequential setting.
//
//  Revision: 002   By: sah    Date:  07-May-1997    DCS Number: 2034
//  Project: Sigma (R8027)
//  Description:
//	As a result of this DCS, I'm making sure the same problem can't
//	happen with sequential settings, by supplying one new method
//	for resetting the minimum and maximum constraints for each batch,
//	sequential setting (see 'resetBoundConstraints()').
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "BasedOnCategory.hh"

//@ Usage-Classes
#include "SequentialSetting.hh"
//@ End-Usage


class BatchSequentialSetting : public SequentialSetting 
{
  public:
    virtual ~BatchSequentialSetting(void);

    virtual Boolean  isChanged(void) const;

    virtual SettingValue  getDefaultValue   (void) const;
    virtual SettingValue  getNewPatientValue(void) const = 0;

    virtual void  acceptTransition(const SettingId::SettingIdType settingid,
                                   const DiscreteValue            newvalue,
                                   const DiscreteValue            currvalue);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    BatchSequentialSetting(
		     const SettingId::SettingIdType  settingId,
		     const SettingId::SettingIdType* arrDependentSettingIds,
		     SequentialRange&                rSequentialRange,
                     const Boolean                  useWrappingMode = FALSE);

    virtual Boolean  resolveDependentChange_(
			const SettingId::SettingIdType dependentId,
			const Boolean                  isValueIncreasing
					    );

    virtual SequentialValue  calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BasedOnCategory          basedOnCategory
					        ) const;

#if defined(SIGMA_DEVELOPMENT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_DEVELOPMENT)

  private:
    // not implemented...
    BatchSequentialSetting(const BatchSequentialSetting&);
    BatchSequentialSetting(void);
    void operator=(const BatchSequentialSetting&);
};


#endif // BatchSequentialSetting_HH 
