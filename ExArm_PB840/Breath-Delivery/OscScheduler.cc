#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OscScheduler - Active scheduler during Occlusion Status Cycling
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for scheduling breath phases during Occlusion
// Status Cycling.  This class is also responsible for relinquishing control to
// the next valid scheduler and for assuming control when occlusion has been
// detected.  Any setting changes made to the mode during Osc are used to deliver
// the "normal" breath during OSC.  This scheduler is responsible for
// enabling the appropriate mode triggers while Osc is active.  If occlusion is
// detected while this scheduler is active, the scheduler will reset the breath
// phase to the SVO state.  All user events, except alarm reset are disabled
// during Osc, and hence are ignored/rejected by this scheduler.  Upon
// relinquishing control, the previous scheduler will be restored, unless that
// scheduler is apnea, and due to setting changes, apnea is no longer possible.
// If control is relinquished due to an alarm reset, the set scheduler will
// take control.
//---------------------------------------------------------------------
//@ Rationale
// This class implements the algorithm for breath phase scheduling
// during Occlusion Status Cycling.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class implements methods for breath and mode transitions during
// occlusion status cycling.  The Osc Scheduler is responsible for:
// --Determining the next BreathPhase when a breath trigger becomes active.
// --Initiating the activities required for the proper start of a new
//  breath:
//    phase in settings
//    Start a new BreathRecord.
//    Setting up breath triggers appropriate for the new phase.
//    Phase out the previous breath phase.
// --Relinquishing control when a mode trigger becomes active.  Activities
//  while control is relinquished include:
//    Disabling out of date breath triggers.
//    Determining which is the next valid scheduler, and instructing
//    the next scheduler to take control.
// --Taking control when the previous scheduler relinquishes control.
	//  Activities while taking control include:
//    Register self with the BreathPhaseScheduler object.
//    Activate the mode trigger list.
//    Set data member indicating that this is the first cycle of Osc.
//    Reset counter for number of breaths delivered during Osc.
// During the first cycle of Osc, the O2 mix must be set to 100% O2 (if available),
// and Peep must be set to 0.  At any time during Osc, if occlusion is
// detected again, the scheduler must open the safety valve and start
// the occlusion cycle again.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OscScheduler.ccv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 028   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added handling of new RM breath phases and disarm maneuver trigger.
//
//  Revision: 027   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added pav pause handle during mode transition.
//
//  Revision: 026  By: jja     Date:  17-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added VTPC handle during occlusion.
//
//  Revision: 025  By: syw     Date:  14-Jun-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Call the base class NewBreath() method instead of derived class.
//
//  Revision: 024  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 023  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling of inspiratory pause event, invoked ROscBiLevelPhase.
//
//  Revision: 022  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 021  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 020  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review for BD Schedulers.
//
//  Revision: 019  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 018  By:  iv    Date:  25-Jan-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 017  By:  iv    Date:  20-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fixed class precondition in determineBreathPhase() for case =
//             NON_BREATHING and trigger id is IMMEDIATE_BREATH.
//
//  Revision: 016  By:  iv    Date:  18-Dec-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 015  By:  iv    Date:  04-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Cleanup for code inspection.
//
//  Revision: 014  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 013  By:  sp    Date:  26-Sep-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed PeepRecoveryTrigger to PeepRecoveryInspTrigger.
//
//  Revision: 012 By:  iv   Date:   06-Sep-1996    DR Number: DCS 10053. 10035
//       Project:  Sigma (R8027)
//       Description:
//             Enabled peep recovery trigger on transition out of occlusion
//             - in determineNextScheduler_()
//
//  Revision: 011 By:  iv   Date:   02-Aug-1996    DR Number: DCS 10035, 1186
//       Project:  Sigma (R8027)
//       Description:
//             Incorporated PEEP recovery by updating the set peep value
//             when relinquishing control.
//
//  Revision: 010 By:  iv   Date:   15-July-1996    DR Number: DCS 10010
//       Project:  Sigma (R8027)
//       Description:
//             Enhance a class pre-condition in determineBreathPhase()
//             where trigger is immediate breath trigger. Transitions are
//             possible from AC, SPONT, SIMV, SAFETY_PCV, and OCCLUSION
//             in addition to SVO.
//
//  Revision: 009 By:  iv   Date:   12-July-1996    DR Number: DCS 951
//       Project:  Sigma (R8027)
//       Description:
//             Eliminate the transition back to Standby.
//
//  Revision: 008 By:  iv   Date:   04-June-1996    DR Number: DCS 951
//       Project:  Sigma (R8027)
//       Description:
//             For the NON_BREATHING case in method deterineBreathPhase()
//             added a check:  else if(triggerId == Trigger::IMMEDIATE_BREATH)
//             to detect a transition from SVO scheduler.
//             Corrected a problem with transition back to apnea - the transition
//             is based on persistent memory information for IsApneaActice() and not
//             just the previous scheduler.
//             Fixed call to base class takeControl() from takeControl().
//             Changed the logic for the transition back to SafetyPcv or Standby.
//
//  Revision: 007 By:  iv   Date:   15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 006 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674, 672 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//             Allow transition back from Svo and short power interruption
//             to occlusion in takeControl_().
//
//  Revision: 005 By:  iv   Date:  26-Jan-1996    DR Number:DCS 672 
//       Project:  Sigma (R8027)
//       Description:
//             Fixed a typo in startInspiration_ that caused Vcv phase to
//             start instead of Pcv phase. Fixed a missing bracket typo in
//             determineNextScheduler_().
//
//  Revision: 004 By:  kam   Date:  01-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Modified interface for reporting ventilator status.  Vent
//             and user event status are posted to the BD Status task
//             via the method VentAndUserEventStatus::PostEventStatus().
//             The BD status task is then responsible for transmitting
//             the status information to the GUI through NetworkApps.
//
//             Also, updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from takeControl().
//
//  Revision: 003 By:  iv   Date:  13-Dec-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed CheckIfApneaPossible_() to CheckIfApneaPossible()
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem
//             and for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "OscScheduler.hh"
#include "BreathMiscRefs.hh"
#include "BdDiscreteValues.hh"
#include "PhaseRefs.hh"
#include "TriggersRefs.hh"
#include "SchedulerRefs.hh"
#include "ModeTriggerRefs.hh"

//@ Usage-Classes
#include "BreathSet.hh"
#include "BreathPhase.hh"
#include "BreathTriggerMediator.hh"
#include "UiEvent.hh"
#include "OscTimeInspTrigger.hh"
#include "OcclusionTrigger.hh"
#include "ModeTrigger.hh"
#include "ModeTriggerMediator.hh"
#include "TimerBreathTrigger.hh"
#include "ApneaInterval.hh"
#include "O2Mixture.hh"
#include "Peep.hh"
#include "PhasedInContextHandle.hh"
#include "BdAlarms.hh"
#include "VentAndUserEventStatus.hh"
#include "BdSystemStateHandler.hh"
#include "InspPauseManeuver.hh"
#include "ExpPauseManeuver.hh"
#include "NifManeuver.hh"
#include "P100Maneuver.hh"
#include "VitalCapacityManeuver.hh"
#include "ManeuverRefs.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

//@constant: SVO_DURATION
//$[04204] maximum duration for safe state phase
static const Int32 SVO_DURATION = 15000;  //15 seconds

//@constant: MAX_EXH_DURATION
//$[04209] $[04207] maximum duration of exhalation during OSC
static const Int32 MAX_EXH_DURATION = 5000;   //5 seconds

//@constant: OSC_EXH_DURATION
//$[04207] $[04209] normal duration of exhalation during OSC
static const Int32 OSC_EXH_DURATION = 2500;  //2.5 seconds

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OscScheduler()
//
//@ Interface-Description
//  Default Constructor -- The list of mode triggers is initialized.
//  The order in which the triggers appear on the list is important.
// The triggers are ordered based on priority - the most urgent
// is first. The list also guarantees that when a trigger fires - the triggers
// that follow it do not have to be processed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The mode trigger list is initialized. The id is passed to the
//  base class constructor. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
// The number of elements on the trigger list is asserted
// to be MAX_NUM_OSC_TRIGGERS and MAX_NUM_OSC_TRIGGERS to be less than
// MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_OSC_TRIGGERS = 4;
OscScheduler::OscScheduler(void) : BreathPhaseScheduler(SchedulerId::OCCLUSION)
{
// $[TI1]
  CALL_TRACE("OscScheduler()");

  Int32 ii = 0;
  //Set up list of mode triggers for this scheduler
  //$[04141] $[04211] $[04212]
  // The triggers are ordered in order of their severity
  pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
  pModeTriggerList_[ii++] = (ModeTrigger*)&ROcclusionTrigger;
  pModeTriggerList_[ii++] = (ModeTrigger*)&ROcclusionAutoResetTrigger;
  pModeTriggerList_[ii++] = (ModeTrigger*)&RManualResetTrigger;
  pModeTriggerList_[ii] = (ModeTrigger*)NULL;

  CLASS_ASSERTION(ii == MAX_NUM_OSC_TRIGGERS &&
						MAX_NUM_OSC_TRIGGERS < MAX_MODE_TRIGGERS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OscScheduler()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OscScheduler::~OscScheduler(void)
{
  CALL_TRACE("~OscScheduler()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
//    This method takes a BreathTrigger reference as a parameter.  The 
//    active BreathPhase may be changed as a result of this method.  
//    There is no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
// The activities handled by this method are dependent on the current breath
// phase. The current breath phase is retrieved from the BreathPhase object.
// Before any phase starts, the BreathTriggerMediator is used to reset the
// old phase trigger list, and to set the new phase trigger list,  the old 
// phase relinquishes control, and the new phase registers itself with the 
// BreathPhase object. New settings are phased in - when relevant
// A switch statement is implemented for the various breath phases which are:
// NON_BREATHING, EXHALATION, INSPIRATION, EXPIRATORY_PAUSE, and INSPIRATORY_PAUSE.
// Inspiration begins by calling the method startInspiration_().
//---------------------------------------------------------------------
//@ PreCondition
// For each case in the switch statement, the valid range of triggers is
// checked. The control flags: firstOcclusion_ and occlusionDetected_ are also
// checked within the relevant context. The breath phase is checked to have
// a value within the following range: {NON_BREATHING, INSPIRATION, EXHALATION}
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OscScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)
{
	CALL_TRACE("OscScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

  Boolean apneaBreath = FALSE;

  const Trigger::TriggerId triggerId = breathTrigger.getId();

  // A pointer to the current breath phase
  BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
  // determine the current phase type (e.g. EXHALATION, INSPIRATION)
  const BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();

  switch (phase)
  {
    case BreathPhaseType::NON_BREATHING:
	// $[TI1]
	  // Trigger::PRESSURE_SVC - is the id of the trigger that is responsible for
	  // terminating the SVO phase and starting the SVC phase when a pressure threshod
	  // is reached.
	  // Trigger::SVC_TIME - is the id of the trigger that is responsible for starting
	  // the SVC phase when SVO_DURATION or MAX_EXH_DURATION elapses.
	  // Trigger::OSC_TIME_INSP - is the id of the trigger that is responsible for
	  // terminating the osc exhalation (when S.V. is open) based on OSC_EXH_DURATION
	  // elapsed time and pressure threshold.
      if ((triggerId == Trigger::PRESSURE_SVC) ||
      		(triggerId == Trigger::SVC_TIME) ||
      		(triggerId == Trigger::OSC_TIME_INSP))
      {
      // $[TI1.1]
        //$[04204]
        //Since Svo and Svc phases share the non-breathing trigger list.
        // The breath list name is changed to set the nextList variable to a value other
        // then NONE
        RBreathTriggerMediator.resetTriggerList();
		RBreathTriggerMediator.changeBreathListName(
											BreathTriggerMediator::NON_BREATHING_LIST);
	
        // $[04205] Set SVC as the active BreathPhase.
        pBreathPhase->relinquishControl(breathTrigger);
        BreathPhase::SetCurrentBreathPhase((BreathPhase&) RSafetyValveClosePhase,
        	breathTrigger);
		BreathPhase::NewBreath() ;
      }
      else if(triggerId == Trigger::SVC_COMPLETE)
      {
      // $[TI1.2]
        // $[04205] $[04210] It is time to start the special Osc Pcv Inspiration.
        //Reset the active triggers and set the inspiratory list to active.
        //It is not necessary to enable any Breath Triggers at this point, since the
        //only trigger causing a transition from this phase is the immediate trigger.
        RBreathTriggerMediator.resetTriggerList();
        RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::INSP_LIST);

        //The OscPcvPhase must become the active BreathPhase.
        pBreathPhase->relinquishControl(breathTrigger);
        BreathPhase::SetCurrentBreathPhase((BreathPhase&)ROscPcvPhase, breathTrigger);
		//create a new breath record for this inspiration
		RBreathSet.newBreathRecord(SchedulerId::OCCLUSION,
						::NULL_MANDATORY_TYPE,	//no mandatory type is specified
						NON_MEASURED,	//no spirometry is performed
						BreathPhaseType::INSPIRATION);
		// start the new breath phase
		BreathPhase::NewBreath() ;
        //Monitor for occlusion detection during the delivery of Osc breaths
        ROcclusionTrigger.enable();
      }
      // IMMEDIATE_BREATH is possible on transition back from SVO or occlusion detection
      // following alarm reset
      else if(triggerId == Trigger::IMMEDIATE_BREATH)
      {
      // $[TI1.3]
          SchedulerId::SchedulerIdValue id = PPreviousScheduler_->getId();
          // When alarm reset is pressed during Osc non breathing
          // phase, and occlusion condition persists, a transition back to
          // occlusion with immediate breath trigger will follow.
          // The SVO scheduler is always followed by an immediate trigger  
          CLASS_PRE_CONDITION(id != SchedulerId::DISCONNECT &&
          	                  id != SchedulerId::STANDBY);
          restartOcclusion_(pBreathPhase, breathTrigger);
      }		
      else
      {
      // $[TI1.4]
        CLASS_ASSERTION_FAILURE();
      }  
    break;

    case BreathPhaseType::INSPIRATION:
    case BreathPhaseType::INSPIRATORY_PAUSE:
    case BreathPhaseType::BILEVEL_PAUSE_PHASE: 
    case BreathPhaseType::PAV_INSPIRATORY_PAUSE:
    case BreathPhaseType::VITAL_CAPACITY_INSP:
        
	// $[TI2]
    // inspiratory pause shall not be active
    // $[BL04007]
	//The class precondition verifies the following compound conditions:
	// 1) Immediate trigger is valid only when occlusion is detected.
	// 2) High circuit pressure or high vent pressure are only valid for a breath
	//    number greater than 1.
	// 3) Immediate exp trigger is only valid when no occlusion is detected.
	
	//Note that high circuit pressure and high vent pressure do not cause
	//occlusion to reset, however, they do cause the inspiration to end.
	CLASS_PRE_CONDITION(
		( (triggerId == Trigger::IMMEDIATE_BREATH) &&
		  (firstOcclusion_ || occlusionDetected_) )
		||
		( ( (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) ||    
            (triggerId == Trigger::HIGH_VENT_PRESSURE_EXP) )  &&
          (numBreathsDelivered_ >= 1) )
        ||
        ( !(firstOcclusion_ || occlusionDetected_) &&
          (triggerId == Trigger::IMMEDIATE_EXP) ) );
      if ((occlusionDetected_) || (firstOcclusion_))
      {
      // $[TI2.1]
        // $[04204] If this trigger was set due to an occlusion being detected while
        // delivering the Osc breath, the scheduler must reset to the SVO phase.
        // The scheduler must also be reset to the SVO phase if Occlusion is just
        // beginning.
		//$[04208]
        restartOcclusion_(pBreathPhase, breathTrigger);
      }
      else
      {
      // $[TI2.2]
		//Phase in new settings (for start of exhalation)
		PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_EXHALATION);

#ifdef INTEGRATION_TEST_ENABLE
		// Signal Event for swat
		swat::EventManager::signalEvent(swat::EventManager::START_OF_EXH);
#endif // INTEGRATION_TEST_ENABLE

        //The active trigger list must be changed to the exhalation list.
        RBreathTriggerMediator.resetTriggerList();
        RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);

		//$04207]
        //enable triggers for ending the OSC exhalation phase
        ROscTimeInspTrigger.restartTimer(OSC_EXH_DURATION);
        ROscTimeInspTrigger.enable();
        ROscTimeBackupInspTrigger.restartTimer(MAX_EXH_DURATION);
        ROscTimeBackupInspTrigger.enable();

        //Occlusion exhalation becomes the active BreathPhase.
        // $[04296] $[04207]
        pBreathPhase->relinquishControl(breathTrigger);
        BreathPhase::SetCurrentBreathPhase((BreathPhase&)ROscExhPhase, breathTrigger);
		//Update the breath record
		(RBreathSet.getCurrentBreathRecord())->newExhalation();
		// start the delivery of the new breath
		BreathPhase::NewBreath() ;
      }
    break;

	case BreathPhaseType::EXHALATION:
	case BreathPhaseType::EXPIRATORY_PAUSE:
    case BreathPhaseType::NIF_PAUSE:
    case BreathPhaseType::P100_PAUSE:
	// $[TI3]
		// phase in new settings.
		PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
		// Signal Event for swat
		swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

		if (triggerId == Trigger::IMMEDIATE_BREATH)
		{
		// $[TI3.1]
			//The only way an immediate trigger can fire during exhalation is if an
			//occlusion was detected.
			CLASS_PRE_CONDITION ( (firstOcclusion_ || occlusionDetected_) );

			// $[04204] This trigger was set due to an occlusion being detected, either
			// while delivering the OSC exhalation or during normal ventilation.
			// The scheduler must be reset to the SVO phase.
			//$[04208] $[04298]
			restartOcclusion_(pBreathPhase, breathTrigger);
		}
		else if ( (triggerId == Trigger::OSC_TIME_INSP) ||
                    (triggerId == Trigger::OSC_TIME_BACKUP_INSP) )
		{
		// $[TI3.2]
			//a complete breath has just been delivered.
			numBreathsDelivered_ ++;
			if (numBreathsDelivered_ >= 2)
			{
			// $[TI3.2.1]
				//$[04135] $[04139]
				//Two breaths have been successfully delivered. The criteria for auto-reset
				//of occlusion mode have been met.  Enable the Occlusion auto-reset trigger
				//to cause a reset to the normal mode of breathing.
				((Trigger&)ROcclusionAutoResetTrigger).enable();
				// set the breath list to EXP_LIST (no change)
		        RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);
			}
			else //numBreathsDelivered_ == 1
			{
		    // $[TI3.2.2]
				//$[04137] $[04295] $[04210] 
				//Determine the currently defined ventilatory mode, by checking the
				//previous scheduler.  If apnea was the previous scheduler,
				//determine if it is still possible to enter apnea.
				if ( BdSystemStateHandler::IsApneaActive() &&
					 BreathPhaseScheduler::CheckIfApneaPossible() )
				{
				// $[TI3.2.2.1]
					apneaBreath = TRUE;
				}// $[TI3.2.2.2], implied else, schedulerId != APNEA, apneaBreath = FALSE.
				startInspiration_(apneaBreath, breathTrigger, pBreathPhase);
			} //end of (numBreathsDelivered_ == 1)
		}
		else if (triggerId == Trigger::DISARM_MANEUVER)
		{
			// $[RM12094] A P100 maneuver shall be canceled...
			// $[RM12077] A NIF maneuver shall be canceled...
		    // The armed RM maneuver was canceled due to occlusion, 
		    // resume exhalation phase (no change)
			RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);
		}
		else
		{
		// $[TI3.3]
	  	    //The triggerId should be a legal Id for Occlusion.  The only way the immediate
	  	    // trigger can fire is if this is the first cycle in occlusion, or occlusion was
	  	    // detected in the middle of the OscExhalation.
			AUX_CLASS_ASSERTION_FAILURE(triggerId);
		}
	break;

	default:
	// $[TI4]
        AUX_CLASS_ASSERTION_FAILURE(phase);
  } //end of switch
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
//	This method is called by a mode trigger when its condition has
//	evaluated to true.  This method takes a reference to the trigger
//  that just fired as a parameter and has no return value.  This method
//  instructs the next scheduler to take control, or, if an occlusion
//  was detected, resets this scheduler to restart occlusion.
//  This method interfaces with BdAlarms to post the Occlusion reset
//  condition, which is then passed to the Alarm-Analysis subsystem, if the
//  disconnect condition has manually or automatically reset.
//  The BD Status task is also notified of the Occlusion status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the trigger id of the mode trigger that has fired is occlusion then the
//	scheduler must be flagged to restart occlusion.  This is accomplished by
//	setting the private data member occlusionDetected_ to true. The immediate
//	breath trigger must be enabled, signalling the current breath phase to end
//	so the transition to the SVO phase can be made.  If the trigger id is not
//	occlusion, then it is necessary to change to another scheduler.  This
//	method calls the private method determineNextScheduler that will
//	instruct the correct breath phase scheduler to take control. Before the new
//	mode is instructed to take control, all mode triggers on the current
//	trigger list get reset.  rBdAlarms.postBdAlarm() is invoked to notify Alarms
//  when the Occlusion condition has reset and VentAndUserEventStatus::PostEventStatus()
//  is invoked to notify the BD Status task of the end of Occlusion.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
OscScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{
	CALL_TRACE("OscScheduler::relinquishControl(const ModeTrigger& modeTrigger)");
	const Boolean isVentSetupComplete = PhasedInContextHandle::AreBdSettingsAvailable();

	const Trigger::TriggerId triggerId = modeTrigger.getId();

	//if occlusion has been detected again, then restart OSC
    //$[04139] $[04208] $[04298] 
	if (triggerId == Trigger::OCCLUSION)
	{
	// $[TI1]
		occlusionDetected_ = TRUE;
		//ensure that the next BreathTrigger to fire is the immediate trigger
		RBreathTriggerMediator.resetTriggerList();
		((BreathTrigger&)RImmediateBreathTrigger).enable();
		// Set the mode triggers for that scheduler. Required by the ModeTriggerMediator
		// interface
		RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);
	}
	else
	{
	// $[TI2]
		// The occlusion trigger needs explicit enabling since it is on the occlusion
		// scheduler list (the only scheduler that can be re-entered)
		ROcclusionTrigger.enable();
		// reset all mode triggers for this scheduler
		RModeTriggerMediator.resetTriggerList();
		determineNextScheduler_(triggerId, isVentSetupComplete);

		//notify alarms of severe occlusion reset
		// $[05167]
		RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NOT_SEVERE_OCCLUSION);

		// notify BD Status task of exit from Occlusion
		VentAndUserEventStatus::PostEventStatus (EventData::OCCLUSION, EventData::CANCEL);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
//	Called when it is necessary to transition to Occlusion.  This
//	method takes a reference to the active BreathPhaseScheduler as
//	a parameter, and has no return value.  Mode triggers that
//  correspond to this scheduler are enabled. Note that BdSystemState is NOT
//  updated with the scheduler id for occlusion.    This method also
//  interfaces with BdAlarms to post the occlusion condition
//  which is then passed to the Alarm-Analysis subsystem.
//  The BD Status task is also notified of the Occlusion status.
//  The base class method for takeControl() is invoked to
//  handle activities common to all schedulers.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method must make the transition to OSC mode by registering
//  this scheduler as the currently active one, and updating the
//	BD state with this scheduler. The mode triggers stored on this
//	scheduler's list are set to the appropriate states by calling
//	enableTriggers_().  The private data member firstOcclusion_ must be
//	set to true, and numBreathsDelivered must be reset to 0.
//  rBdAlarms.postBdAlarm() is called to log the Occlusion condition
//  and VentAndUserEventStatus::PostEventStatus() is invoked to notify the
//  BD Status task of the start of Occlusion.
//---------------------------------------------------------------------
//@ PreCondition
//	The scheduler's id is checked to be in the following range:
//	AC, SIMV, SPONT, APNEA, SAFETY_PCV, POWER_UP, SVO
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
OscScheduler::takeControl(const BreathPhaseScheduler& scheduler) 
{
	CALL_TRACE("OscScheduler::takeControl(BreathPhaseScheduler& scheduler)");

    // Invoke the base class takeControl method
    BreathPhaseScheduler::takeControl(scheduler);
    
	// disable the following triggers
	((BreathTrigger&)RAsapInspTrigger).disable();
	((BreathTrigger&)RPeepRecoveryMandInspTrigger).disable();

	// $[BL04070] :c disable pause event request
	// $[BL04072] :c disable pause event request
	// $[BL04008] :c disable pending pause event
	// $[BL04075] :c cancel pause request
	// $[BL04012] :c terminate active pause request
	// $[BL04078] :c terminate manual active inspirtory pause
	Maneuver::CancelManeuver();

	// cancel any mode or rate change that may have been initialized during 
	// the previous mode
	BreathPhaseScheduler::ModeChange_ = FALSE;	
	BreathPhaseScheduler::RateChange_ = FALSE;

	SchedulerId::SchedulerIdValue schedulerId = scheduler.getId();
	// update the reference to the newly active breath scheduler 
	BreathPhaseScheduler::SetCurrentScheduler_((BreathPhaseScheduler&)(*this));

	//The BreathRecord is not being updated with the new scheduler id, so that
	// the identity of the scheduler that originated this breath is maintained. 
	// until a new breath starts.
	
    // update the BD state, in case of power interruption
   	BdSystemStateHandler::UpdateSchedulerId(getId()); 

	// Set the mode triggers for that scheduler.
	enableTriggers_();	
	RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);

	//There is no need to enable the immediate trigger, as it should have been
	//enabled by the scheduler that is relinquishing control.
	//Setting firstOcclusion_ true will cause the scheduler to activate the
	//SVO phase when the breath trigger fires.
	firstOcclusion_ = TRUE;
	//occlusion was not detected during OSC
	occlusionDetected_ = FALSE;
	numBreathsDelivered_ = 0;

	//notify alarms of severe occlusion
	// $[05167]
	RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_SEVERE_OCCLUSION);

	// notify BD Status task of Occlusion
	VentAndUserEventStatus::PostEventStatus (EventData::OCCLUSION, EventData::ACTIVE);

	// $[04125] $[04130]  
 	CLASS_PRE_CONDITION(
		(schedulerId == SchedulerId::AC) ||
		(schedulerId == SchedulerId::SIMV) ||
		(schedulerId == SchedulerId::SPONT) ||
		(schedulerId == SchedulerId::BILEVEL) ||
		(schedulerId == SchedulerId::APNEA) ||
		(schedulerId == SchedulerId::POWER_UP) ||
		(schedulerId == SchedulerId::SVO) ||
		(schedulerId == SchedulerId::SAFETY_PCV) );
} 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
OscScheduler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, OSCSCHEDULER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[01255] $[05016]
// The argument of type EventId indicates which user event is active.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is rejected by
// invoking a call to the static method:
// BreathPhaseScheduler::RejectManualInspiration_(eventStatus).
// The alarm reset event is accepted by invoking a call to the static method:
// BreathPhaseScheduler::AcceptAlarmReset_(eventStatus).
// The expiratory and inspiratory pause events are rejected by invoking a call to
// the static methods: BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus) and
// BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus) .
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
OscScheduler::reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus)
{
	CALL_TRACE("OscScheduler::reportEventStatus_(const EventData::EventId id,\
													 const Boolean eventStatus)");

	EventData::EventStatus rtnStatus = EventData::IDLE;

	switch (id)
	{
		case EventData::MANUAL_INSPIRATION:
		// $[TI1] $[04211]
			rtnStatus =
					BreathPhaseScheduler::RejectManualInspiration_(eventStatus);
			break;

		case EventData::ALARM_RESET:
		// $[TI2]
			rtnStatus =
					BreathPhaseScheduler::AcceptAlarmReset_(eventStatus);
			break;

		case EventData::EXPIRATORY_PAUSE:
		// $[TI3] $[04211]
			rtnStatus =
					BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus);
			break;

		case EventData::INSPIRATORY_PAUSE:
		// $[TI4]
        // inspiratory pause shall not be active
	    // $[BL04007]
			rtnStatus =
					BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus);
			break;

		case EventData::NIF_MANEUVER:
			rtnStatus = BreathPhaseScheduler::RejectNifManeuver_(eventStatus);
			break;

		case EventData::P100_MANEUVER:
			rtnStatus = BreathPhaseScheduler::RejectP100Maneuver_(eventStatus);
			break;

		case EventData::VITAL_CAPACITY_MANEUVER:
			rtnStatus = BreathPhaseScheduler::RejectVitalCapacityManeuver_(eventStatus);
			break;

		default:
		// $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(id);
	}	

	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineNextScheduler_
//
//@ Interface-Description
//  This method takes a trigger id and a Boolean indicating whether or not vent
//  setup was completed and returns no value.  It is called when this scheduler
//  is relinquishing control to determine what scheduler should take control
//  next.  If the trigger is manual reset, and vent setup completed, then the
//  next scheduler is determined by the setting for mode.  If the trigger is
//  autoreset, and apnea is active and possible, then the next scheduler is
//  set to the apnea scheduler. If apnea is not active, the next scheduler is the
//  set scheduler.  If vent setup didn't complete, the next scheduler is set to
//  be safety pcv.  If the trigger is SVO, the next scheduler is set to be
//  SvoScheduler.
//  If the next scheduler is svo, reset all current breath triggers, enable the
//  immediate trigger and set the actual peep value to the current valid peep
//  setting.  If the next scheduler is not svo and the current breath phase is
//  SafeStatePhase, reset all current breath triggers, enable the immediate
//  trigger, and set up the safetyValveClosedPhase as the active phase.  If the
//  next scheduler is not svo and the current breath phase is inspiration or
//  exhalation, disable OSC time triggers, and enable the peep recovery trigger
//  to establish peep as soon as the breath is not restricted.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A local variable is defined to point to the next scheduler -
//  pNextScheduler.  pNextScheduler is assigned the address of the scheduler
//  that is determined to be the next scheduler e.g. rSvoScheduler.
//  The immediate breath trigger is enabled, using the method enable() in the
//  instance variable rImmediateBreathTrigger. Other triggers that are used are:
//  rOscTimeInspTrigger, rOscTimeBackupInspTrigger, and rPeepRecoveryInspTrigger.
//  Once the next scheduler is determined, it is instructed to take control.
//---------------------------------------------------------------------
//@ PreCondition
//	The Triggerid of the trigger causing the change must be either SVO,
//	OCCLUSION_AUTORESET or MANUAL_RESET.
// 	If the next scheduler is not SVO and the phase is not NON_BREATHING
//	then the breath phase is checked to be either safe state phase or
//	safety valve close phase.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
OscScheduler::determineNextScheduler_(const Trigger::TriggerId triggerId,
						const Boolean isVentSetupComplete)
{
	CALL_TRACE("OscScheduler::determineNextScheduler_(const Trigger::TriggerId triggerId, \
												Boolean isVentSetupComplete)");

	// A pointer to the current breath phase
	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
	BreathPhaseScheduler* pNextScheduler = NULL;
    const BreathPhaseType::PhaseType phase = BreathRecord::GetPhaseType();	 

	// check if occlusion is reset:
	if( (triggerId == Trigger::OCCLUSION_AUTORESET) ||
        (triggerId == Trigger::MANUAL_RESET) )
	{
	// $[TI1]
		// recovery from occlusion is to the current set mode, or apnea if
		// it was previously active and still possible:
		if(isVentSetupComplete)
		{
		// $[TI1.1]
			// phase-in the new vent settings (for start of inspiration):
			PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
			// Signal Event for swat
			swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

			RPeep.setActualPeep(PhasedInContextHandle::GetBoundedValue( SettingId::PEEP).value);
			pNextScheduler = &BreathPhaseScheduler::EvaluateSetScheduler();
			// if auto reset, check if should transfer control to apnea scheduler:
			if( (triggerId == Trigger::OCCLUSION_AUTORESET) &&
				BdSystemStateHandler::IsApneaActive() &&
				BreathPhaseScheduler::CheckIfApneaPossible() )
			{
			// $[TI1.1.1]
				pNextScheduler = (BreathPhaseScheduler*)&RApneaScheduler;
			}// $[TI1.1.2] implied else
		}
		else // if vent setup not complete:
		{
		// $[TI1.2]
			pNextScheduler = (BreathPhaseScheduler*)&RSafetyPcvScheduler;
		}
	}
	else if(triggerId == Trigger::SVO)
	{
	// $[TI2]
		//If the new mode is SVO, enable the immediate trigger to phase in the new mode
		//right away.
		pNextScheduler = (BreathPhaseScheduler*)&RSvoScheduler;
		RBreathTriggerMediator.resetTriggerList();
		((BreathTrigger&)RImmediateBreathTrigger).enable();
		RPeep.setActualPeep(PhasedInContextHandle::GetBoundedValue( SettingId::PEEP).value);
	}
	else
	{
	// $[TI3]
		CLASS_PRE_CONDITION( (triggerId == Trigger::OCCLUSION_AUTORESET) ||
			(triggerId == Trigger::MANUAL_RESET) ||
			(triggerId == Trigger::SVO) );
	}

	if (pNextScheduler != (BreathPhaseScheduler*)&RSvoScheduler)
	{
	// $[TI4]
		if (pBreathPhase == (BreathPhase*)&RSafeStatePhase)
		{
		// $[TI4.1]
			RBreathTriggerMediator.resetTriggerList();
			//Set SVC as the active BreathPhase to close the safety valve.  Since
			// relinquishControl and SetCurrentBreathPhase() requires the trigger causing
			// the transition, send a reference to the immediate breath trigger.  There
			// is no need to update the breath record since the phase is still
			// NON_BREATHING.
			pBreathPhase->relinquishControl((BreathTrigger&)RImmediateBreathTrigger);
			BreathPhase::SetCurrentBreathPhase((BreathPhase&)RSafetyValveClosePhase,
												(BreathTrigger&)RImmediateBreathTrigger);
			// start the delivery of the new breath
			BreathPhase::NewBreath() ;
		}
		else if (phase != BreathPhaseType::NON_BREATHING)
 		{
 		// $[TI4.2]
			((BreathTrigger&)ROscTimeInspTrigger).disable();
			((BreathTrigger&)ROscTimeBackupInspTrigger).disable();
			// enable the peep recovery trigger to establish peep as soon
			// as the breath is not restricted
			((BreathTrigger&)RPeepRecoveryInspTrigger).enable();
		}
		else
		{
		// $[TI4.3]
			// there is no specific check for safety valve closed phase since no trigger
			// has to be enabled in that case
			CLASS_PRE_CONDITION( (phase != BreathPhaseType::NON_BREATHING) ||
								(pBreathPhase == (BreathPhase*)&RSafeStatePhase) ||
								(pBreathPhase == (BreathPhase*)&RSafetyValveClosePhase) );
		}
	}// $[TI5]
	pNextScheduler->takeControl(*this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// The method takes no arguments and returns no value.  A request not to enable
// the Occlusion trigger is issued when this scheduler is taking
// control.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The OcclusionTrigger is disabled to prevent another immediate
//  detection of occlusion during the safety valve open and closed
//  states. It will be enabled at a later phase of OSC.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
OscScheduler::enableTriggers_(void) 
{
// $[TI1]
	CALL_TRACE("OscScheduler::enableTriggers_()");

	// The occlusion mode trigger needs to be disabled for the first 2 phases
	// of OSC.
	((ModeTrigger&)ROcclusionTrigger).setIsEnableRequested(FALSE);
	// Only the Svo trigger should be enabled
	((ModeTrigger&)RSvoTrigger).setIsEnableRequested(TRUE);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: restartOcclusion_
//
//@ Interface-Description
//  This method is called whenever an occlusion is detected during OSC
//	or when a breath trigger fires for the first time since OSC
//  became active.  This method takes no parameters and has no return
//	type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[04204]
//  Regardless of why this method was invoked, the OcclusionTrigger
//  must be disabled to prevent another immediate detection and restart.
//  The active BreathPhase is changed to the SVO state, the appropriate
//  triggers are enabled and the active trigger list is changed.  The
//  data member numBreathsDelivered_ is reset to 0.  The occlusionDetected_
//  flag must be reset to detect another occlusion.
//	If this method is invoked for the first time into occlusion, then
//	the Peep level is set to 0, the O2 mix is set to 100% O2, a new
//	BreathRecord is updated with the new schedulerId, and the
//	firstOcclusion_ flag is reset to false.
//---------------------------------------------------------------------
//@ PreCondition
//	numBreathsDelivered_ <= 2
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
OscScheduler::restartOcclusion_(BreathPhase* pBreathPhase,
											const BreathTrigger& breathTrigger)
{
  CALL_TRACE("restartOcclusion_(BreathPhase* pBreathPhase, const BreathTrigger breathTrigger)");

	//the occlusion trigger must be disabled during the first two phases of OSC to
	//prevent an immediated restart from triggering.
	ROcclusionTrigger.disable();

	//update the breath triggers for the non breathing phases of OSC
    RBreathTriggerMediator.resetTriggerList();
    RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::NON_BREATHING_LIST);

	//If this is the first time into OSC, or if a normal breath was in progress while in OSC
	//then the duration of the safety valve open state must have a SVO_DURATION
	//Otherwise, it has a MAX_EXH_DURATION second time limit.
	if ((firstOcclusion_) || (numBreathsDelivered_ >= 1))
	{
	// $[04298] $[04209]
	// $[TI1]
	    RSvcTimeTrigger.restartTimer(SVO_DURATION);
   	    ((Trigger&)RPressureSvcTrigger).enable();
	}
	else if (numBreathsDelivered_ == 0)
	{
	// $[TI2]
		RSvcTimeTrigger.restartTimer(MAX_EXH_DURATION);
		ROscTimeInspTrigger.restartTimer(OSC_EXH_DURATION);
		ROscTimeInspTrigger.enable();
	}
	else
	{
	// $[TI3]
		CLASS_PRE_CONDITION(numBreathsDelivered_ <= 2)
	}
	//$[04209]
   	RSvcTimeTrigger.enable();


    //Reset the OcclusionDetected_ flag for use again.
    occlusionDetected_ = FALSE;

    //Reset the successful breath counter
    numBreathsDelivered_ = 0;

    //The SVO phase needs to be set to the active phase
    pBreathPhase->relinquishControl(breathTrigger);
    BreathPhase::SetCurrentBreathPhase((BreathPhase&)RSafeStatePhase, breathTrigger);
	//start a new BreathRecord, and set the scheduler to the OscScheduler.
 	RBreathSet.newBreathRecord(SchedulerId::OCCLUSION,
					::NULL_MANDATORY_TYPE,    // no mandatory type is specified
					NON_MEASURED, //no spirometry is performed
					BreathPhaseType::NON_BREATHING);
	//Deliver the new breath phase 
	BreathPhase::NewBreath() ;
	//The first time occlusion is started, Peep must be set to 0, and
	//the O2 mix must be set to 100% O2.
	if (firstOcclusion_)
	{
	// $[TI4]
		RO2Mixture.determineO2Mix(*this, (BreathPhase&)RSafeStatePhase);
		RPeep.setActualPeep(0.0);
		firstOcclusion_ = FALSE;
 	}
 	// $[TI5]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startInspiration_ 
//
//@ Interface-Description
//	$[04295] $[04137]
//	The method takes a breath trigger reference, a breath phase pointer, and a
//	// Boolean indicating whether apnea breath is due as arguments. When the
//	next phase is determined to be inspiration, this method is called to
//	perform all the standard procedures required to start inspiration:
//	-	Change the active breath trigger list.
//	-	Instruct the current breath phase to relinquish control and setup
//		the new breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method collaborates with the global objects: RBreathSet,
// RBreathTriggerMediator, and with the handle PendingContextHandle to
// accomplish its responsibilities.
//---------------------------------------------------------------------
//@ PreCondition
// Make sure the mandatory type value is within range.
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
OscScheduler::startInspiration_(const Boolean apneaBreath,
				const BreathTrigger& breathTrigger, BreathPhase* pBreathPhase)
{
  CALL_TRACE("OscScheduler::startInspiration_(const Boolean apneaBreath,\
					 const BreathTrigger& breathTrigger, BreathPhase* pBreathPhase)");

  // variable to store setting values: 
  DiscreteValue mandatoryType = 0;
  // The breath phase to be delivered:
  BreathPhase *pPhase = NULL;

  //Reset currently active triggers and setup triggers that are active during inspiration:
  RBreathTriggerMediator.resetTriggerList();
  RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::INSP_LIST);

  // phase-out current breath phase
  pBreathPhase->relinquishControl(breathTrigger);

  if (apneaBreath)     // apnea breath
  {
  // $[TI1]
    //get the mandatory type
    mandatoryType = PhasedInContextHandle::GetDiscreteValue(SettingId::APNEA_MAND_TYPE);

    //  register new phase and prepare to deliver a mandatory inspiration:
	switch (mandatoryType)
	{
		case MandTypeValue::PCV_MAND_TYPE :
			// $[TI1.2]
			pPhase = (BreathPhase*)&RApneaPcvPhase ;
	    	break ;
	    	
		case MandTypeValue::VCV_MAND_TYPE :
			// $[TI1.1]
			pPhase = (BreathPhase*)&RApneaVcvPhase ;
			break ;
			
		default :
			// unexpected mand type...
			AUX_CLASS_ASSERTION_FAILURE(mandatoryType) ;
			break ;
	}
  }
  else      // normal breath
  {
  // $[TI2]
    // get the mandatory type
    mandatoryType = PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);
	
    //  register new phase and prepare to deliver a mandatory inspiration:
	switch (mandatoryType)
	{
		case MandTypeValue::PCV_MAND_TYPE :
		{
			// $[TI2.2]
			const DiscreteValue  MODE_VALUE =
					PhasedInContextHandle::GetDiscreteValue(SettingId::MODE) ;

			if(MODE_VALUE == ModeValue::BILEVEL_MODE_VALUE)
			{
				// $[TI2.2.1]
				pPhase = (BreathPhase*)&ROscBiLevelPhase ;
			}
			else
			{
				// $[TI2.2.2]
				pPhase = (BreathPhase*)&RPcvPhase ;
			}
		}
		break ;
		    
		case MandTypeValue::VCV_MAND_TYPE :	// $[TI2.1]
			pPhase = (BreathPhase*)&RVcvPhase;
			break ;
			
		case MandTypeValue::VCP_MAND_TYPE :	// $[TI2.3]
			// $[VC24021]
			// $[VC24019]
			pPhase = (BreathPhase*)&RVcvPhase ;
			mandatoryType = MandTypeValue::VCV_MAND_TYPE ;
			break ;
			
		default :
			// unexpected mand type...
			AUX_CLASS_ASSERTION_FAILURE(mandatoryType) ;
			break ;
	}
  }
  // initialize the new breath phase.
  BreathPhase::SetCurrentBreathPhase(*pPhase, breathTrigger);
  //start a new BreathRecord
  RBreathSet.newBreathRecord(SchedulerId::OCCLUSION, mandatoryType,
   								NON_MEASURED, BreathPhaseType::INSPIRATION);
	BreathPhase::NewBreath() ;
}

