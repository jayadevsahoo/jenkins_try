
#ifndef PendingContext_HH
#define PendingContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: PendingContext - Context for Settings Pending Phase-In.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PendingContext.hhv   25.0.4.0   19 Nov 2013 14:27:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "SettingId.hh"

//@ Usage-Classes
#include "BdSettingValues.hh"
#include "SettingXmitData.hh"
#include "BoundedValue.hh"
#include "DiscreteValue.hh"
#include "SettingCallbackMgr.hh"
#include "TemplateMacros.hh"
#include "BitArray_NUM_BD_SETTING_IDS.hh"
//@ End-Usage


class PendingContext
{
  public:
    PendingContext(void);
    ~PendingContext(void);

#if defined(SIGMA_DEVELOPMENT)
    inline Boolean  isSettingPending(
				  const SettingId::SettingIdType settingId
				    ) const;

    inline void  setVentStartupFlag(void);
#endif // defined(SIGMA_DEVELOPMENT)

    inline void  clearPendingFlag(const SettingId::SettingIdType settingId);

    inline Boolean  isVentStartupPending      (void) const;
    inline void     ventStartupPhaseInComplete(void);

    inline BoundedValue   getBoundedValue (
				    const SettingId::SettingIdType boundedId
					  )  const;
    inline DiscreteValue  getDiscreteValue(
				    const SettingId::SettingIdType discreteId
					  )  const;

#if defined(SIGMA_DEVELOPMENT)
    void  setBoundedValue (const SettingId::SettingIdType boundedId,
			   const BoundedValue&            newValue);
    void  setDiscreteValue(const SettingId::SettingIdType discreteId,
			   const DiscreteValue            newValue);
#endif // defined(SIGMA_DEVELOPMENT)

    inline const FixedBitArray(NUM_BD_SETTING_IDS)&  getPendingFlags(void) const;
    inline const BdSettingValues&  getPendingValues(void) const;

    void  processTransmission(const SettingsXactionId xactionId,
			      const BdSettingValues&  xmittedBdValues);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

    //@ Type:  BufferedValues
    // Contains the Vent-Startup-Pending Flag and pointers to the
    // pending values and flags.  This allows for the double-buffering of
    // the flags and values.  The Vent-Startup-Pending Flag indicates
    // whether a Vent-Startup update is currently pending a phase-in.
    struct BufferedValues
    {
      Boolean                             ventStartupPendingFlag;
      FixedBitArray(NUM_BD_SETTING_IDS)*  pPendingFlags;
      BdSettingValues*                    pPendingBdValues;
    };

    //@ Type:  PendingBufferId
    // Identifiers for the two buffers.
    enum PendingBufferId
    {
      BUFFER0,
      BUFFER1,

      NUM_BUFFERS
    };

  private:
    PendingContext(const PendingContext&);	// not implemented...
    void  operator=(const PendingContext&);	// not implemented...

    static void  RegisterSettingReceipt_(
				  const SettingId::SettingIdType settingId
				        );

    inline PendingContext::PendingBufferId  getUpdateBufferId_(void) const;

    inline const BufferedValues*  getReadBufferPtr_  (void) const;
    inline BufferedValues*        getReadBufferPtr_  (void);
    inline BufferedValues*        getUpdateBufferPtr_(void);

    inline void  acceptUpdateBuffer_(void);

    // Data-Member:  currReadBuffer_
    // This is used to manage which of the two buffers is the current
    // "READ" buffer, and, therefore, which is the current "WRITE" buffer.
    Uint  currReadBuffer_;

    //================================================================
    // Members for Buffer 0...
    //================================================================

    // Data-Member:  pendingBdValues0_
    // The pending phase-in values of each of the BD settings.
    BdSettingValues  pendingBdValues0_;

    // Data-Member:  pendingFlags0_
    // Bit flags to keep track of which settings are waiting to be phased-in.
    FixedBitArray(NUM_BD_SETTING_IDS)  pendingFlags0_;

    //================================================================
    // Members for Buffer 1...
    //================================================================

    // Data-Member:  pendingBdValues1_
    // The pending phase-in values of each of the BD settings.
    BdSettingValues  pendingBdValues1_;

    // Data-Member:  pendingFlags1_
    // Bit flags to keep track of which settings are waiting to be phased-in.
    FixedBitArray(NUM_BD_SETTING_IDS)  pendingFlags1_;

    //================================================================
    // Buffered Value Storage...
    //================================================================

    // Data-Member:  pendingValueBuffer_
    // The pending phase-in values of each of the BD settings.
    BufferedValues  arrPendingInfo_[NUM_BUFFERS];

    //================================================================
    // Static members...
    //================================================================

    //@ Data-Member:  ArrPendingIds_
    // Static array of the ids of the settings that are being received.
    static SettingId::SettingIdType   ArrPendingIds_[::NUM_BD_SETTING_IDS];

    //@ Data-Member:  NumPendingIds_
    // Static integer to indicate how many settings just received; this
    // number corresponds to how many pending ids are in 'ArrPendingIds_'.
    static Uint   NumPendingIds_;
};


// Inlined methods...
#include "PendingContext.in"


#endif // PendingContext_HH 
