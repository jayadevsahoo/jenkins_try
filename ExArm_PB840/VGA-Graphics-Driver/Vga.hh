#ifndef Vga_HH
#define Vga_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Vga - Interface class for VGA register functions.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/Vga.hhv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 004   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 003   By: srp    Date:  28-May-2002    DCS Number: 5905
//  Project: VCP
//  Description: 
//	Indentation of data structure.
//
//  Revision: 002   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//====================================================================

#include "VgaGraphicsDriver.hh"
#include "VgaDisplayId.hh"

//@ Usage-Classes
#include "RgbQuad.hh"
#include "VgaDevice.hh"
#include "MutEx.hh"
//@ End-Usage


class Vga 
{
public:
    enum DisplayRegPort
    {
        SRX           = 0x3c4,
        SRDATA        = 0x3c5,

        GRX           = 0x3ce,
        GRDATA        = 0x3cf,

        ARX           = 0x3c0,
        ARDATA        = 0x3c0,  // same as ARX

        VSE           = 0x3c3,

        FCR           = 0x3da,
        ST01          = 0x3da,

        CRX           = 0x3d4,
        CRDATA        = 0x3d5,

        MSR           = 0x3c2,
        ST00          = 0x3c2,

        DACMASK       = 0x3c6,
        DACRX         = 0x3c7,
        DACWX         = 0x3c8,
        DACDATA       = 0x3c9,

        XRX           = 0x3d6,
        XRDATA        = 0x3d7,

        DISPLAY_MEM   = 0x100000
    };

    enum DisplayReg32Port
    {
        DR00          = 0x83d0,
        DR01          = 0x87d0,
        DR02          = 0x8bd0,
        DR03          = 0x8fd0,
        DR04          = 0x93d0,
        DR05          = 0x97d0,
        DR06          = 0x9bd0,
        DR07          = 0x9fd0,
        DR08          = 0xa3d0,
        DR09          = 0xa7d0,
        DR0A          = 0xabd0,
        DR0B          = 0xafd0,
        DR0C          = 0xb3d0
    };

    enum AttributeRegIndex
    {
        AR00  = 0x00,
        AR01  = 0x01,
        AR02  = 0x02,
        AR03  = 0x03,
        AR04  = 0x04,
        AR05  = 0x05,
        AR06  = 0x06,
        AR07  = 0x07,
        AR08  = 0x08,
        AR09  = 0x09,
        AR0A  = 0x0a,
        AR0B  = 0x0b,
        AR0C  = 0x0c,
        AR0D  = 0x0d,
        AR0E  = 0x0e,
        AR0F  = 0x0f,
        AR10  = 0x10,
        AR11  = 0x11,
        AR12  = 0x12,
        AR13  = 0x13,
        AR14  = 0x14
    };

    enum GraphicsRegIndex
    {
        GR00  = 0x0,
        GR01  = 0x1,
        GR02  = 0x2,
        GR03  = 0x3,
        GR04  = 0x4,
        GR05  = 0x5,
        GR06  = 0x6,
        GR07  = 0x7,
        GR08  = 0x8
    };

    enum SequencerRegIndex
    {
      SR00 	= 0x0,
      SR01 	= 0x1,
      SR02 	= 0x2,
      SR03 	= 0x3,
      SR04 	= 0x4,
      SR07 	= 0x7
    };

	enum CrtRegIndex
	{
	  CR00	= 0x00,
	  CR01	= 0x01,
	  CR02	= 0x02,
	  CR03	= 0x03,
	  CR04	= 0x04,
	  CR05	= 0x05,
	  CR06	= 0x06,
	  CR07	= 0x07,
	  CR08	= 0x08,
	  CR09	= 0x09,
	  CR0A	= 0x0a,
	  CR0B	= 0x0b,
	  CR0C	= 0x0c,
	  CR0D	= 0x0d,
	  CR0E	= 0x0e,
	  CR0F	= 0x0f,
	  CR10	= 0x10,
	  CR11	= 0x11,
	  CR14	= 0x14,
	  CR15	= 0x15,
	  CR16	= 0x16,
	  CR17	= 0x17,
	  CR18	= 0x18,
	  CR22	= 0x22,
	  CR24	= 0x24,
	  CR26	= 0x26
	};
	enum ExtensionRegIndex
	{
	  XR00	= 0x00,
	  XR01	= 0x01,
	  XR02	= 0x02,
	  XR03	= 0x03,
	  XR04	= 0x04,
	  XR05	= 0x05,
	  XR06	= 0x06,
	  XR07	= 0x07,
	  XR08	= 0x08,
	  XR0B	= 0x0b,
	  XR0C	= 0x0c,
	  XR0D	= 0x0d,
	  XR0E	= 0x0e,
	  XR0F	= 0x0f,
	  XR10	= 0x10,
	  XR11	= 0x11,
	  XR14	= 0x14,
	  XR15	= 0x15,
	  XR16	= 0x16,
	  XR17	= 0x17,
	  XR18	= 0x18,
	  XR19	= 0x19,
	  XR1A	= 0x1a,
	  XR1B	= 0x1b,
	  XR1C	= 0x1c,
	  XR1D	= 0x1d,
	  XR1E	= 0x1e,
	  XR1F	= 0x1f,
	  XR24	= 0x24,
	  XR25	= 0x25,
	  XR26	= 0x26,
	  XR28	= 0x28,
	  XR29	= 0x29,
	  XR2B	= 0x2b,
	  XR2C	= 0x2c,
	  XR2D	= 0x2d,
	  XR2E	= 0x2e,
	  XR2F	= 0x2f,
	  XR30	= 0x30,
	  XR31	= 0x31,
	  XR32	= 0x32,
	  XR33	= 0x33,
	  XR3A	= 0x3a,
	  XR3B	= 0x3b,
	  XR3C	= 0x3c,
	  XR3D	= 0x3d,
	  XR3E	= 0x3e,
	  XR3F	= 0x3f,
	  XR40	= 0x40,
	  XR44	= 0x44,
	  XR45	= 0x45,
	  XR4F	= 0x4f,
	  XR50	= 0x50,
	  XR51	= 0x51,
	  XR52	= 0x52,
	  XR53	= 0x53,
	  XR54	= 0x54,
	  XR55	= 0x55,
	  XR56	= 0x56,
	  XR57	= 0x57,
	  XR58	= 0x58,
	  XR59	= 0x59,
	  XR5A	= 0x5a,
	  XR5B	= 0x5b,
	  XR5C	= 0x5c,
	  XR5D	= 0x5d,
	  XR5E	= 0x5e,
	  XR5F	= 0x5f,
	  XR60	= 0x60,
	  XR61	= 0x61,
	  XR62	= 0x62,
	  XR63	= 0x63,
	  XR64	= 0x64,
	  XR65	= 0x65,
	  XR66	= 0x66,
	  XR67	= 0x67,
	  XR68	= 0x68,
	  XR6C	= 0x6c,
	  XR6E	= 0x6e,
	  XR6F	= 0x6f,
	  XR70	= 0x70,
	  XR72	= 0x72,
	  XR73	= 0x73,
	  XR7D	= 0x7d,
	  XR7E	= 0x7e,
	  XR7F	= 0x7f
	};

	enum ColorMapFlag
	{
	  CM_READ_ONLY = 1,
	  CM_ALLOCATED = 2
	};

	static Vga & GetUpperVga(void);
	static Vga & GetLowerVga(void);
	static Vga & GetVga(VgaDisplayId id);

	void  storeColor(Byte index, const RgbQuad & rgb, Boolean readOnly=TRUE);
	void  getColor(Byte index, RgbQuad & rgb);
	void  allocateColor(Byte & index, const RgbQuad & rgb);
    void  initMode79(void);
	void  setDisplayStartAddress(Byte * startAddress);

	inline Uint  getFrameCount(void) const;

    inline Byte * getPDisplayMemory(void) const;
    inline Uint  getMemorySize(void) const;
	inline void  lock(void);
	inline void  unlock(void);

    inline void  setReg     (DisplayRegPort regId, Uint8 regData);
    inline Uint8 getReg     (DisplayRegPort regId);
    inline void  setReg     (DisplayReg32Port regId, Uint32 regData);
    inline Uint  getReg     (DisplayReg32Port regId);
    inline void  setReg     (ExtensionRegIndex regIndex, Uint8 regData);
    inline void  setReg     (GraphicsRegIndex regIndex, Uint8 regData);
    inline void  setReg     (CrtRegIndex regIndex, Uint8 regData);
    inline void  setReg     (SequencerRegIndex regIndex, Uint8 regData);
    inline void  setReg     (AttributeRegIndex regIndex, Uint8 regData);
    inline void  selectAttributeIndexReg(void);

    inline void  accessDisplay   (void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:

	Vga( Uint vgaBaseAddress, Int32 mutexId );
	~Vga(void);

    void  initializePalette_(void);
	Boolean isSameVideo_( const RgbQuad & rgb, const RgbQuad & key ) const;
	Uint  getMemorySize_(void);

    //@ Data-Member:  pVgaBase_
    //  Contains the address of the VGA - volatile since it references i/o regs
    volatile Byte * const pVgaBase_;
	
    //@ Data-Member:  pDisplayMemory_
    //  Contains the base address of the VGA display memory
    Byte * const pDisplayMemory_;
	
    //@ Data-Member:  memorySize_
    //  Contains the size of the VGA display memory
    Uint memorySize_;
	
    //@ Data-Member:  frameCount_
    //  Contains the number of display frames contained in VGA memory
    Uint frameCount_;
	
	//@ Data-Member:  mutex_
	//  Mutex for gating access to the VGA display and registers 
	MutEx  mutex_;

	//@ Data-Member:  colormapCache_
	//  Cached RGB values stored in the VGA palette
	RgbQuad  colormapCache_[256];

};


// Inlined methods...
#include "Vga.in"


#endif // Vga_HH 
