#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: ScreenCapture - Capture pixel data from VGA memory 
//---------------------------------------------------------------------
//@ Interface-Description
//  This base class contains the methods used to copy pixel data from VGA
//	subsystem RAM to main RAM for subsequent transmission to a printer
//	via a GUI serial port.  Note that this is a base class and is not
//	intended to be directly instantiated -- only derived classes
//	should be instantiated, e.g AlarmStatusCapture.
//---------------------------------------------------------------------
//@ Rationale
//  Provides capability for 840 screen dump to a printer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The SerialInterface::Task instantiates this class when the ventilator
//  transitions to a normal (ie. non-SERVICE) state and a communication
//  port setting is set to PRINTER.  When the PRINT button is pressed, this
//  class captures screen data from the VGA framebuffer and converts it
//	to HP PCL format for output to a compatible printer.
//
//	The method captureData() is used to read color pixel data from
//	VGA RAM, convert it to B/W printer pixel data via filterPixel_()
//	(which is pure virtual in this class and must be defined in derived
//	classes), then store the converted pixel data in main RAM.
//
//	The method copyRasterLine() is used to copy one raster line of printer
//	pixel data to a designated buffer, for subsequent transmission to
//	a printer.
//
//	The method resetByteCount() is used to reset the count of printer
//	pixel bytes prior to the first call to copyRasterLine() during
//	a print job.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness of 
//  parameterized data.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/ScreenCapture.ccv   25.0.4.0   19 Nov 2013 14:12:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: quf         Date: 26-Sep-2001  DR Number: 5942
//  Project: GuiComms
//  Description:
//  Code/comment/header cleanup.
//
//  Revision: 001    By: gdc         Date: 23-Jan-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//=====================================================================

#include "Sigma.hh"
#include "ScreenCapture.hh"
#include <string.h>  // for memcpy

//@ Usage-Classes
#include "DisplayContext.hh"
//@ End-Usage

//@ Code...

static const Int32 BITS_PER_UINT32 = 32;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScreenCapture [constructor]
//
//@ Interface-Description
//  Constructor for the ScreenCapture class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
ScreenCapture::ScreenCapture( Uint32* pRasterData, 
							  Uint32  sizeofRasterData,
							  DisplayContext& rDisplayContext,
							  Int32 x0, Int32 y0,
	                          Uint32 xWidth, Uint32 yHeight )
	: pRasterData_(pRasterData)
	, rDisplayContext_(rDisplayContext)
    , x0_(x0)
	, y0_(y0)
    , xWidth_(xWidth)
    , yHeight_(yHeight)
    , sizeofRasterLine_((xWidth_ + 31) / 32 * sizeof(Uint32))
    , sizeofBitmap_(sizeofRasterLine_ * yHeight_)
{
// $[TI1]
  CLASS_ASSERTION(pRasterData);
  CLASS_ASSERTION(sizeofBitmap_ == sizeofRasterData);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScreenCapture [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
ScreenCapture::~ScreenCapture()
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: captureData
//
//@ Interface-Description
//  Capture raster data from the VGA subsystem RAM, converting from VGA
//	(color) pixels to printer (B/W) pixels and storing the converted
//	pixels in main RAM.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ScreenCapture::captureData(void)
{
  rDisplayContext_.lock();
  rDisplayContext_.accessDisplay();

  // Get the dimensions and coordinates for the waveform subscreen.
  Int32 displayWidth = rDisplayContext_.getWidth();   // 640
  CLASS_ASSERTION(displayWidth == xWidth_);

  Int32 y1 = y0_ + yHeight_ - 1;
  Int32 row;

  Uint32 compressedData = 0;

  Uint32* pRasterData = pRasterData_;
  const Byte* pDisplayMemory = rDisplayContext_.getDisplayFramePtr();
  const Byte* pDisplayMemoryIterator;

  // Iterate through all rows in the graphics area.
  for (Int32 y = y0_; y <= y1; y++)
  {
  // $[TI1]
	row = y;

    pDisplayMemoryIterator = pDisplayMemory + (row * displayWidth + x0_);
    Uint32 x;

	// Iterate through all pixels in the row.
    for (x = 0; x < xWidth_; x++)
    {
	// $[TI1.1]

      // If the shift register is full - transfer to storage buffer
      if (x && (x % BITS_PER_UINT32 == 0))
      {
	  // $[TI1.1.1]
        *pRasterData++ = compressedData;
        compressedData = 0;
      }
	  // $[TI1.1.2]

      compressedData <<= 1;
	  compressedData |= filterPixel_(*pDisplayMemoryIterator++);
    }
	// $[TI1.2]

    // If the shift register is full - transfer to storage buffer else
    // fill shift register with zeros and then transfer to storage buffer.
    if (x % BITS_PER_UINT32 == 0)
    {
	// $[TI1.3]
      *pRasterData++ = compressedData;
    }
    else
    {
	// $[TI1.4]
      *pRasterData++ = compressedData << (BITS_PER_UINT32 - x % BITS_PER_UINT32);
    }
  }
  // $[TI2]

  rDisplayContext_.unlock();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: copyRasterLine
//
//@ Interface-Description
//  This method copies a line of printer pixel data to the specified
//	buffer. Returns the number of bytes actually copied, where 1 byte
//	stores 8 printer pixels.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Call copyRasterData_(), passing the pointer to the storage buffer
//	and the size of a printer raster line in bytes.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int32
ScreenCapture::copyRasterLine(void *pDest)
{
// $[TI1]
  return copyRasterData_(pDest, sizeofRasterLine_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: copyRasterData_
//
//@ Interface-Description
//  This method copies the specified number of bytes of printer pixel
//	data to the specified destination buffer. It returns the number
//	of bytes actually copied, where 1 byte stores 8 printer pixels
//	(1 bit per printer pixel).
//---------------------------------------------------------------------
//@ Implementation-Description
//	byteCount_ is an offset into the printer pixel data and it is
//	incremented each time this method is called.  Therefore, the first
//	call to this method must be preceded by a call to resetByteCount()
//	so that copying will begin at the start of the printer pixel data.
//	Subsequent calls to copyRasterData_() will copy printer pixel data
//	from the position that the last call finished at.
//	The final call to this method will return zero, indicating
//	all printer pixel data was copied.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int32
ScreenCapture::copyRasterData_(void* pDest, Uint32 nbytes)
{
  if (byteCount_ + nbytes > sizeofBitmap_)
  {
  // $[TI1]
	nbytes = sizeofBitmap_ - byteCount_;
  }
  // $[TI2]

  memcpy(pDest, (Byte*)pRasterData_ + byteCount_, nbytes);

  byteCount_ += nbytes;

  return nbytes;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetByteCount
//
//@ Interface-Description
//  Reset the byte count.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ScreenCapture::resetByteCount(void)
{
// $[TI1]
  byteCount_ = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void    
ScreenCapture::SoftFault(  const SoftFaultID softFaultID
                           , const Uint32      lineNumber
                           , const char *      pFileName
                           , const char *      pPredicate )
{
    FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
		GUI_Serial_Interface::SCREEN_CAPTURE_CLASS, 
		lineNumber, pFileName, pPredicate);
}
