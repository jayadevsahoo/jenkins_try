#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ProcessedValues - Filtering and slope calculation.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class is used for signal processing (filtering and
//      slope) of sensor data.  Methods exist for initializing,
//      updating, and reading processed values.  Methods are implemented
//		to update the filtered, slope, and filtered slope values; to return
//		the filtered value; to return the slope value; to return the filtered
//		slope value; to redefine the alpha of the filter; and to initializes the
//		filtered, slope, filtered slope	value, and previous values.
//---------------------------------------------------------------------
//@ Rationale
//      This class is used to perform signal processing on sensor data.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The method updateValues is used to update the processed
//      values.  The various access methods return the calculated
//      values.  When each sensor is read, its processed values
//      should be updated.  This will ensure that the filtered
//      and/or slope values are updated only once per cycle.
//
//      The BD I/O subsystem initialization invokes the sensor's initValues
//      method.  This ensures the integrity of all processed values.  It takes
//      two samples for the slope values to be valid, one during initialization
//      and one when breath delivery first reads the sensors.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      This class should always be associated with a sensor class (to
//		be initialized and continuously updated)
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ProcessedValues.ccv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003  By: syw    Date:  27-Mar-1996    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Round to 0.0 if newFilteredValue < ZER0_VALUE to prevent underflow.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Fix bug in the constructor where alpha_ was used in the
//			CLASS_PRE_CONDITION of alpha_.  alpha should be used.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "ProcessedValues.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessedValues()
//
//@ Interface-Description
//      Constructor.  It takes one argument, alpha.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The alpha is stored and remaining private data are initialized
//      to zero.
//---------------------------------------------------------------------
//@ PreCondition
//      0 <= alpha_ <= 1
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

ProcessedValues::ProcessedValues( const Real32 alpha)
{
	CALL_TRACE("ProcessedValues::ProcessedValues( const Real32 alpha)") ;

    // $[TI1]
	CLASS_PRE_CONDITION( alpha >= 0.0 && alpha <= 1.0) ;
	
    alpha_ = alpha;
    prevValue_ = 0.0;
    filteredValue_ = 0.0;
    slopeValue_ = 0.0;
    filteredSlopeValue_ = 0.0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ProcessedValues()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//        None
//---------------------------------------------------------------------
//@ PreCondition
//        None
//---------------------------------------------------------------------
//@ PostCondition
//        None
//@ End-Method
//=====================================================================

ProcessedValues::~ProcessedValues( void)
{
	CALL_TRACE("ProcessedValues::~ProcessedValues( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValues()
//
//@ Interface-Description
//      This method takes one argument, currValue, and updates slopeValue,
//      filteredSlopeValue, and filteredValue.  It returns nothing.  This
//      method is called every new cycle by the Sensor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The new slope value is the difference between the previous
//      and current value.  The new filtered value is calculated 
//      from the current value, the previous filtered value and alpha.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

void
ProcessedValues::updateValues( const Real32 currValue)
{
	CALL_TRACE("ProcessedValues::updateValues( const Real32 currValue)") ;

	const Real32 ZERO_VALUE = 0.001F ;
	
    Real32 newFilteredValue = filteredValue_ * alpha_ + currValue * (1 - alpha_) ;

    if (newFilteredValue < ZERO_VALUE)
    {
    	// $[TI1.1]
    	newFilteredValue = 0.0F ;
    }  	// $[TI1.2]
    	
    filteredSlopeValue_ = newFilteredValue - filteredValue_ ;
    filteredValue_ = newFilteredValue ;
    slopeValue_ = currValue - prevValue_ ;  
    prevValue_ = currValue ;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ProcessedValues::SoftFault( const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;

    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, PROCESSEDVALUES,
                             lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//=====================================================================
//
//  Private Methods...
//
//=====================================================================



