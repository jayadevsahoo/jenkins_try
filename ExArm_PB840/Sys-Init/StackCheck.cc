#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: StackCheck - Runtime Stack Checking Facility
//---------------------------------------------------------------------
//@ Interface-Description
//  StackCheck is a static class that maintains information about
//  each stack allocated during the task creation process.  Statistics
//  are maintained for the stack size and location of each task created 
//  under VRTX.  In production, the Background Task insures a runtime
//  safety margin for each stack by calling this class to check stack
//  usage.
//---------------------------------------------------------------------
//@ Rationale
//  This class contains stack statistics for all tasks created
//  under VRTX.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The VrtxHooks class messages this class with the stack locations
//  and sizes prior to the task's execution.  StackCheck initializes
//  the stack area with a "unique" pattern which is used later to
//  determine how much the the stack area the executing task has
//  used.  The Margin() function returns the minimum available 
//  margin found for all stacks maintained by this class.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/StackCheck.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "StackCheck.hh"
#include <stdio.h>
#include <string.h>

//@ Usage-Classes
#include "Task.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

const Uint MAX_NUM_TASKS = 30;
const unsigned char       STACK_MAGIC_COOKIE = 0xEF;
StackCheck::TaskRecord    StackCheck::TaskData_[MAX_NUM_TASKS];
Int32                     StackCheck::TaskCount_;

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


#ifdef SIGMA_INT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
// Method: Print - INTEGRATION TEST ONLY
//
// Interface-Description
//  This static function prints the stack
//  utilization table for all VRTX tasks.  It determines the "high 
//  water mark" for each stack based on the first location from the
//  beginning of the stack that does not contain the magic cookie 
//  value.
//---------------------------------------------------------------------
// Implementation-Description
//  Uses the TaskData_ set earlier by the Register function to find
//  the stack areas.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
StackCheck::Print(void)
{
    for (int i = 0; i < TaskCount_; i++)
    {
        unsigned char* pStack     = TaskData_[i].pUStackTop;
        Uint32         stackSize  = TaskData_[i].userStackSize;
        unsigned char  taskId     = TaskData_[i].taskId;
        const char*    taskName;


        if (taskId > 0 && taskId <= Task::GetTaskCount()) 
        {
            taskName = Task::GetTaskInfo(taskId).getName();
        }
        else
        {
            taskName = "unknown";
        }
        printf("-%10s: ID (%3d), ", taskName, taskId);

        for (int j=0; j<stackSize; j++, pStack++) 
        {
            if (*pStack != STACK_MAGIC_COOKIE) break;
        }

        printf("User (%6d/%6d), ", stackSize-j, stackSize);


        //  supervisor stack usage
        pStack     = TaskData_[i].pSStackTop;
        stackSize  = TaskData_[i].supvStackSize;

        for (j=0; j<stackSize; j++, pStack++) 
        {
            if (*pStack != STACK_MAGIC_COOKIE) break;
        }

        printf("Supv (%4d/%4d)", stackSize-j, stackSize);

        printf("\n");
    }
}
#endif // SIGMA_INT_TEST


#ifdef SIGMA_INT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
// Method: Task - INTEGRATION TEST ONLY
//
// Interface-Description
//  This static function periodically prints the task stack utilization
//  by calling the StackCheck::Print method.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
StackCheck::Task(void)
{
    for (;;)
    {
        StackCheck::Print();
        Task::Delay(60);
    }
}
#endif // SIGMA_INT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Register
//
//@ Interface-Description
//  This method receives a task ID, a 
//  pointer and size for the task's user mode stack, and a pointer 
//  and size for the task's supervisor mode stack.  It retains this
//  information and uses it to intialize the stack memory to a know 
//  (STACK_MAGIC_COOKIE) value.  VrtxHooks::TCreateHook calls this 
//  function during the task's creation prior to moving the contents
//  of the temporary stack to the task's supervisor stack.  Later
//  when the Print method is called, the stacks are examined to 
//  find the first location within each stack that does not contain
//  the magic cookie value.  This location represents the "high water"
//  mark for the stack.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Stores the task information in TaskData_ array indexed by the
//  TaskCount_ which is incremented for each call.  
//---------------------------------------------------------------------
//@ PreCondition
//  TaskCount_ < MAX_NUM_TASKS
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


void
StackCheck::Register( unsigned char taskId,
                        unsigned char * pUStackTop, 
                        Int32 userStackSize,
                        unsigned char * pSStackTop, 
                        Int32 supvStackSize )
{
    CLASS_PRE_CONDITION(TaskCount_ < MAX_NUM_TASKS);
    TaskData_[TaskCount_].taskId    = taskId;
    TaskData_[TaskCount_].pUStackTop = pUStackTop;
    TaskData_[TaskCount_].pSStackTop = pSStackTop;
    TaskData_[TaskCount_].userStackSize = userStackSize;
    TaskData_[TaskCount_].supvStackSize = supvStackSize;
    if (pUStackTop != NULL)
    {
        memset(pUStackTop, STACK_MAGIC_COOKIE, userStackSize);
    }
    if (pSStackTop != NULL)
    {
        memset(pSStackTop, STACK_MAGIC_COOKIE, supvStackSize);
    }
    ++TaskCount_;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Margin
//
//@ Interface-Description
//  This static function returns the minimum margin for 
//  all stacks registered with the class.  It determines the "high 
//  water mark" for each stack based on the first location from the
//  beginning of the stack that does not contain the magic cookie 
//  value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the TaskData_ set earlier by the Register function to find
//  the stack areas.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Int32
StackCheck::Margin(void)
{
    Int32 minMargin = MAX_INT32_VALUE;

    for (Int32 i = 0; i < TaskCount_; i++)
    {
        unsigned char* pStack     = TaskData_[i].pUStackTop;
        Uint32         stackSize  = TaskData_[i].userStackSize;

        //  task may not have a user stack - ie. supv mode task
        if (stackSize > 0)  // $[TI1]
        {
			Int32 j;
            //  user stack usage
            for (j=0; j<stackSize; j++, pStack++) 
            {
                if (*pStack != STACK_MAGIC_COOKIE) 
                { // $[TI1.1] 
                    break;  
                } // $[TI1.2]
            }
    
            if (j < minMargin) 
            {   // $[TI1.3]
                minMargin = j;
            }   // $[TI1.4]
        }
        // else $[TI2]

        //  supervisor stack usage
        pStack     = TaskData_[i].pSStackTop;
        stackSize  = TaskData_[i].supvStackSize;

		Int32 j;
        for (j=0; j<stackSize; j++, pStack++) 
        {
            if (*pStack != STACK_MAGIC_COOKIE) 
            {    // $[TI5]
                 break;
            }    // $[TI6]
        }

        if (j < minMargin) 
        {   // $[TI3]
            minMargin = j;
        }   // $[TI4]
    }

    return minMargin;
}


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//. Method: GetTaskData
//
//. Interface-Description
//---------------------------------------------------------------------
//. Implementation-Description
//---------------------------------------------------------------------
//. PreCondition
//  (index < TaskCount_)
//---------------------------------------------------------------------
//. PostCondition
//  none
//. End-Method
//=====================================================================

const StackCheck::TaskRecord&
StackCheck::GetTaskData(const Int32 index)
{
    AUX_CLASS_ASSERTION( index < TaskCount_, index );

    return TaskData_[index];
}

//============================ M E T H O D   D E S C R I P T I O N ====
//. Method: GetTaskCount
//
//. Interface-Description
//---------------------------------------------------------------------
//. Implementation-Description
//---------------------------------------------------------------------
//. PreCondition
//  none
//---------------------------------------------------------------------
//. PostCondition
//  none
//. End-Method
//=====================================================================

Int32
StackCheck::GetTaskCount(void)
{
    return TaskCount_;
}

#endif  // SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
StackCheck::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*,
                         const char*)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SystemSoftFault(softFaultID, SYS_INIT,
                                Sys_Init::ID_STACKCHECK,
                                lineNumber);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================
