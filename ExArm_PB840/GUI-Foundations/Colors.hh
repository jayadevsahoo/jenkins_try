#ifndef Colors_HH
#define Colors_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Colors - This class defines the colors used for the system.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Colors.hhv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By: mnr    Date: 03-Mar-2010    SCR Number: 6555
//  Project: NEO
//  Description:
//      Added YELLOW_ON_MEDIUM_BLUE_BLINK_SLOW.
// 
//  Revision: 014   By: mnr    Date: 23-Feb-2010    SCR Number: 6555
//  Project:  NEO
//  Description:
//      Added YELLOW_ON_BLACK_BLINK_SLOW.
//
//  Revision: 013   By: rhj    Date: 10-Dec-2009    SCR Number: 6523
//  Project:  NEO
//  Description:
//      Added BLACK_ON_LIGHT_BLUE_BLINK.
//
//  Revision: 012   By: gdc    Date: 21-May-2007   SCR Number: 6330
//  Project:  Trend
//	Description:
//		Removed SUN prototype code.
//
//  Revision: 011   By: gdc    Date: 01-Feb-2007   SCR Number: 6237
//  Project:  Trend
//	Description:
//		Trend project related changes. Removed SUN prototype code.
//
//  Revision: 010 By: srp    Date: 28-May-2002   DR Number: 5906
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 009   By: hlg    Date: 04-Sep-01   DR Number: 5940
//  Project:  GuiComms
//  Description:
//      Added private method InitializeMap_.
//
//  Revision: 008  By: sah    Date: 01-Mar-2000   DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Update from NeoMode
//
//  Revision: 007   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 006  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 005  By: yyy      Date: 28-Aug-1996  DCR Number: 516
//    Project:  Sigma (R8027)
//    Description:
//      Adjusted gray scale to avoid vertical lines on lower half of displays
//
//  Revision: 004  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 003  By: mpm      Date: 14-Aug-1995  DCR Number: 521
//    Project:  Sigma (R8027)
//    Description:
//      Setup of VGA colors changed in order to solve DCS #521, Noise on GUI Screens.
//
//  Revision: 002  By: mpm      Date: 26-Jul-1995  DCR Number: 521
//    Project:  Sigma (R8027)
//    Description:
//      Modified colors to get better VGA rendition, especially to rid screen
//      of annoying vertical lines that appeared because of Extra Dark Grey
//      color.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================


//@ Usage-Classes
 
#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "VgaDisplayId.hh" 
#include "RgbQuad.hh"
//@ End-Usage

typedef Uint8 Pixel;

class Colors 
{
public:
	//@ Type: ColorS
	// This enum contains all valid colors used in the application.  These
	// values range from -1 to 15; all except the -1 consist of indeces into
	// the color palette.  The color palette contains 16 values, each of which
	// can range from 0 to 255. Each of the values in the color palette
	// consists of an index into the color table, which specifies 255 RGB
	// values (see below).
	//
	// Note: if the values assigned in this enum are changed, it may be
	// necessary to make corresponding changes in the Image.cc file, because
	// the initializers for color bitmaps include explicit numeric color
	// values -- they don't refer to this enum.
	enum ColorS
	{
		NO_COLOR	= -1,
		DARK_RED     = 0,
		
		WHITE		= 1, 
		LIGHT_BLUE	= 2, 
		MEDIUM_BLUE	= 3, 
		DARK_BLUE	= 4, 
		EXTRA_DARK_BLUE	= 5, 
		BLACK		= 6,
		MIDNIGHT_BLUE= 7,

		YELLOW      = 8,
		RED 		= 9,
		GREEN     	= 10,

		COPPER      = 16,

		// The blink colors are continuously remapped between 2 different
		// colors by a task dedicated to this purpose.  Everything rendered
		// on the screen in these colors will flash.  The flash rate and
		// colors are reflected in the names below.
		BLACK_ON_LIGHT_GREY_BLINK		= 11, 
		GOLDENROD						= 12, 
		WHITE_ON_EXTRA_DARK_GREY_BLINK	= 13, 
		WHITE_ON_BLACK_BLINK_FAST		= 14, 
		BLACK_ON_YELLOW_BLINK_SLOW		= 15,
		BLACK_ON_LIGHT_BLUE_BLINK       = 17,
		YELLOW_ON_MEDIUM_BLUE_BLINK_SLOW = 18,
		FRAME_COLOR = 1,
		
		NUM_COLORS = 19
	};

	static void Initialize(void);
	static RgbQuad toStandardColor(ColorS color);
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:
	// these methods are purposely declared, but not implemented...
	Colors(void);					// not implemented...
    Colors(const Colors&);			// not implemented...
    ~Colors(void);					// not implemented...
    void operator=(const Colors&);	// not implemented...

	static void InitializeMap_( VgaDisplayId displayIndex);

};

#endif // Colors_HH
