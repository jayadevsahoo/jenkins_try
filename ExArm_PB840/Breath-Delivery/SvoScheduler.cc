#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SvoScheduler - Safety Valve Open scheduler
//---------------------------------------------------------------------
//@ Interface-Description
// The SvoScheduler is responsible to determine the next breath phase when invoked
// by a breath trigger, and to determine the next scheduler when invoked
// by a mode trigger. When a new breath phase is due, the scheduler starts the
// safe state phase. When the scheduler takes control it cancels pending pause
// requests.
// Upon relinquishing control, the next scheduler to be phased in is evaluated
// and may be either apnea, or the user set scheduler or safety pcv (if vent setup
// has not been completed yet).
// During svo, user events are either ignored or rejected, and setting change
// events are ignored. The only mode trigger active during svo is the svo reset
// trigger.
//---------------------------------------------------------------------
//@ Rationale
// Svo is a safe mode of breathing in which no active breath delivery takes
// place and the airways are open to allow free flow of gas to and from the
// patient. This mode is needed when active breath activity can no longer be
// supported.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler is using methods to perform the following activities:
// Determine the next breath phase, relinquish control, take control,
// enable mode triggers, report user event status, and handle setting
// events.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SvoScheduler.ccv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 028   By: gdc   Date:  23-Feb-2007    SCR Number: 6307
//  Project:  RESPM
//  Description:
//		Added missing RM requirement numbers.
//
//  Revision: 027   By: gdc   Date:  22-Oct-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added case to reject RM maneuver request during SVO. Using new
//      Maneuver::CancelManeuver() to cancel any active maneuver when
//      entering SVO.
//
//  Revision: 026  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//			Change call to VentAndUserEventStatus::PostEventStatus() to
//			REventFilter.postEventStatus() to filter the svo annunciation.
//
//  Revision: 025  By: syw     Date:  14-Jun-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Call the base class NewBreath() method instead of derived class.
//
//  Revision: 024  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 023  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling of inspiratory pause event.
//
//  Revision: 022  By:  iv    Date:  03-Sep-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 021  By: iv    Date:  11-Aug-1997    DR Number: DCS 2352
//  	Project:  Sigma (840)
//		Description:
//			In relinquishControl(), added a check for startupState != POWERFAIL
//          to determine what the next scheduler should be.
//
//  Revision: 020  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 019  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 018  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 017 By:  iv   Date:   06-Feb-1997    DR Number: DCS 1737
//       Project:  Sigma (840)
//       Description:
//             Use the methods Get/SetIsStandbyRequired() in relinquishControl()
//             to determine whether or not the standby scheduler is to be invoked.
//
//  Revision: 016  By:  iv    Date:  01-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 015  By:  iv    Date:  04-Dec-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Cleanup for code inspections.
//
//  Revision: 014  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 013 By:  iv   Date:   13-Nov-1996    DR Number: DCS 1541
//       Project:  Sigma (R8027)
//       Description:
//		       In relinquishControl(), changed the condition that determines that
//             SafetyPcvScheduler is the next scheduler.
//
//  Revision: 012 By:  iv   Date:   22-Aug-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//		       In relinquishControl(), eliminated assertion for !isVentSetupComplete
//             when going back to Standby or SafetyPcv
//
//  Revision: 011 By:  iv   Date:   20-Jul-1996    DR Number: DCS 10016, 10015
//       Project:  Sigma (R8027)
//       Description:
//		       In relinquishControl(), fixed transition back to the previous scheduler
//             that is based on the phased-in settings.
//
//  Revision: 010 By:  iv   Date:   04-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fixed call to base class takeControl() from takeControl().
//             Call newBreathRecord() in class BreathSet only if the previous scheduler
//             was not SVO.
//             Changed mode transition logic: always go back to the previous scheduler
//             unless it was PowerupScheduler; If apnea was the last 'ACTIVE' scheduler,
//             and it is still possible - go back to apnea; If vent setup has not been
//             completed, go back to SafetyPcvScheduler if it is 'ACTIVE', otherwise,
//             go back to StandbyScheduler; If the next scheduler is Standby, Disconnect,
//             or Occlusion, enable the immediate breath trigger and close the safety valve.
//
//  Revision: 009 By:  iv   Date:   10-May-1996    DR Number: DCS 989
//       Project:  Sigma (R8027)
//       Description:
//		       Added the assertion in relinquishControl():
//              BreathPhase::GetCurrentBreathPhase() ==	(BreathPhase *)&rSafetyValveClosePhase );
//
//  Revision: 008 By:  iv   Date:   15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 007 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 006 By:  iv   Date:  25-Jan-1996    DR Number: DCS 672, 645
//       Project:  Sigma (R8027)
//       Description:
//             Changed relinquishControl() to allow transition to disconnect
//             scheduler if ventsetup is not complete.
//             Changed relinquishControl() and takeControl() to allow transition
//             back to disconnect or occlusion - if vent setup is complete.
//
//  Revision: 005 By:  iv   Date:  08-Jan-1996    DR Number: DCS 645
//       Project:  Sigma (R8027)
//       Description:
//             Moved the evaluation of SchedulerId from within the "if" statement in
//             relinquishControl(...) to a location before the "if" statement.
//
//  Revision: 004 By:  kam   Date:  13-Dec-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Modified interface for reporting ventilator status.  Vent
//             and user event status are posted to the BD Status task
//             via the method VentAndUserEventStatus::PostEventStatus().
//             The BD status task is then responsible for transmitting
//             the status information to the GUI through NetworkApps.
//
//             Also, updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from takeControl().
//
//  Revision: 003 By:  iv   Date:  13-Dec-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed CheckIfApneaPossible_() to CheckIfApneaPossible()
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem
//             and for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "SvoScheduler.hh"
#  include "ModeTriggerRefs.hh"
#  include "BreathMiscRefs.hh"
#  include "SchedulerRefs.hh"
#  include "TriggersRefs.hh"
#  include "PhaseRefs.hh"
#  include "ValveRefs.hh"
#  include "BdDiscreteValues.hh"

//@ Usage-Classes
#  include "Trigger.hh"
#  include "BreathSet.hh"
#  include "BreathPhase.hh"
#  include "ModeTriggerMediator.hh"
#  include "ModeTrigger.hh"
#  include "BreathTriggerMediator.hh"
#  include "TimerBreathTrigger.hh"
#  include "PendingContextHandle.hh"
#  include "PhasedInContextHandle.hh"
#  include "PhaseInEvent.hh"
#  include "BdSystemStateHandler.hh"
#  include "Solenoid.hh"
#  include "Post.hh"
#  include "BdAlarms.hh"
#  include "Maneuver.hh"
#  include "EventFilter.hh"
#  include "BDIORefs.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SvoScheduler()  [Default Constructor]
//
//@ Interface-Description
//		Constructor.
// $[04119] $[04125] $[04130] $[04141]
// The constructor takes no arguments. It constructs the class instance with
// the proper id.  The list of valid mode triggers, used by the mode trigger
// mediator is initialized as well.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor invokes the base class constructor with the SVO id argument.
// It initializes the private data member pModeTriggerList_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	The number of elements on the trigger list is asserted
//  to be MAX_NUM_SVO_TRIGGERS and MAX_NUM_SVO_TRIGGERS to be less than
//  MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_SVO_TRIGGERS = 1;
SvoScheduler::SvoScheduler(void) : BreathPhaseScheduler(SchedulerId::SVO)
{
// $[TI1]
	CALL_TRACE("SvoScheduler::SvoScheduler(void)");

	Int32 ii = 0;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoResetTrigger;
	pModeTriggerList_[ii] = (ModeTrigger*)NULL;

	CLASS_ASSERTION(ii == MAX_NUM_SVO_TRIGGERS &&
						MAX_NUM_SVO_TRIGGERS < MAX_MODE_TRIGGERS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SvoScheduler()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SvoScheduler::~SvoScheduler(void)
{
  CALL_TRACE("~SvoScheduler()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
// This method accepts a breath trigger as an argument, and returns no value.
// Activities performed here include:
// change the active breath trigger list, disable the triggers on the
// current list, instruct the current breath phase to relinquish control,
// and setup the new breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[04223]
// The instance RBreathTriggerMediator is used to reset the
// old phase trigger list, and to set the new phase trigger list,  the old
// phase relinquishes control, and the new phase registers itself with the
// BreathPhase object.
// The BreathPhase static method SetCurrentBreathPhase() is used to set the
// current instance of the breath phase: rSafeStatePhase.
// inspiratory pause shall not be active
// $[BL04007]
//---------------------------------------------------------------------
//@ PreCondition
// The breath trigger id is checked to be equal to IMMEDIATE_BREATH.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SvoScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)
{
// $[TI1]
	CALL_TRACE("SvoScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

	// A pointer to the current breath phase
	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();

	const Trigger::TriggerId triggerId = ((const Trigger&)breathTrigger).getId();
	//Check the validity of the triggers.
	CLASS_PRE_CONDITION(triggerId == Trigger::IMMEDIATE_BREATH);

	// Reset currently active triggers and setup triggers that are active during
	//svo :
	RBreathTriggerMediator.resetTriggerList();
	RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::NON_BREATHING_LIST);
	// disable the immediate trigger on the list:
	((Trigger&)RImmediateBreathTrigger).disable();

	// phase-out current breath phase
	pBreathPhase->relinquishControl(breathTrigger);

	// register new phase :
	BreathPhase::SetCurrentBreathPhase((BreathPhase&)RSafeStatePhase,
												breathTrigger);
	// Update the breath record only if the scheduler id stored in the current
	// breath record is not SVO
	const BreathRecord* pCurrentRecord = RBreathSet.getCurrentBreathRecord();
	SchedulerId::SchedulerIdValue schedulerId = pCurrentRecord->getSchedulerId();
	if (schedulerId != SchedulerId::SVO)
	{
	// $[TI1.1]
	 	RBreathSet.newBreathRecord(SchedulerId::SVO,
 						::NULL_MANDATORY_TYPE,    // no mandatory type is specified
 						NON_MEASURED,    // breath type
						BreathPhaseType::NON_BREATHING );
	}
	// $[TI1.2]
	// deliver the breath:
	BreathPhase::NewBreath() ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
// This method accepts a mode trigger reference as an argument and has no
// return value.  If the previous scheduler is disconnect, occlusion or
// standby, the next scheduler is set equal to the previous. If previous mode
// was apnea and it is still possible, the next scheduler is set to apnea else
// it is set to the set mode.  If new patient setup has not been completed,
// Sigma starts safety pcv breathing.  If the next mode is a breathing mode,
// the safety valve closes for 500 milliseconds before inspiration starts.
// Before the next scheduler takes control, it resets all mode triggers on its
// list.  The BD Status task is also notified of the start of SVO.
//---------------------------------------------------------------------
//@ Implementation-Description
// To determine the set mode, settings are phased-in using the handle
// PhasedInContextHandle.  The PhasedInContextHandle is used to get the vent
// setup complete status.  which is used to determine whether to employ safety
// pcv breathing.  The previous scheduler is checked using the static member
// PPreviousScheduler_.
// The SafetyValveClosePhase is phased-in only for a breathing mode.
// Once the next scheduler is determined, it is instructed to take
// control.  REventFilter.postEventStatus() is invoked to send SVO
// status to the BD Status task.
//---------------------------------------------------------------------
//@ PreCondition
// The trigger id is checked to be SVO_RESET.  The previous scheduler id as
// well as the next scheduler id are checked to be within valid ranges.  If
// previous scheduler is apnea, vent setup complete is asserted to be true.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SvoScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{
	CALL_TRACE("SvoScheduler::relinquishControl(const ModeTrigger& modeTrigger)");

	BreathPhaseScheduler* pNextScheduler;
	const Boolean isVentSetupComplete = PhasedInContextHandle::AreBdSettingsAvailable();
	const ShutdownState startupState = Post::GetStartupState();
	const Trigger::TriggerId triggerId = ((const Trigger&)modeTrigger).getId();

	CLASS_PRE_CONDITION(triggerId == Trigger::SVO_RESET);

	// recovery from svo is to the current set mode, or apnea

	// reset all mode triggers for this scheduler
	RModeTriggerMediator.resetTriggerList();

	// notify the BD Status task of exit from SVO
	REventFilter.postEventStatus( EventFilter::SVO, EventData::CANCEL) ;

	SchedulerId::SchedulerIdValue previousSchedulerId = PPreviousScheduler_->getId();
	// phase-in the new vent settings (for start of inspiration):
	PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

	if(previousSchedulerId == SchedulerId::DISCONNECT)
	{
	// $[TI1]
		pNextScheduler = (BreathPhaseScheduler*)&RDisconnectScheduler;
	}
	else if(previousSchedulerId == SchedulerId::OCCLUSION)
	{
	// $[TI2]
		pNextScheduler = (BreathPhaseScheduler*)&ROscScheduler;
	}
	else if(previousSchedulerId == SchedulerId::STANDBY)
	{
	// $[TI3]
		pNextScheduler = (BreathPhaseScheduler*)&RStandbyScheduler;
	}
	else if ( previousSchedulerId == SchedulerId::APNEA ||
			 BdSystemStateHandler::IsApneaActive() )
	{
	// $[TI4]
		CLASS_ASSERTION(isVentSetupComplete);
		if (BreathPhaseScheduler::CheckIfApneaPossible())
		{
		// $[TI4.1]
			pNextScheduler = (BreathPhaseScheduler*)&RApneaScheduler;
		}
		else
		{
		// $[TI4.2]
			pNextScheduler = &BreathPhaseScheduler::EvaluateSetScheduler();
		}
	}
	else if( previousSchedulerId == SchedulerId::POWER_UP &&
             TRUE == BreathPhaseScheduler::GetIsStandbyRequired() )
	{
	// $[TI9]
	    if (startupState != POWERFAIL)
	    {
        // $[TI9.1]
            pNextScheduler = (BreathPhaseScheduler*)&RStandbyScheduler;
	    }
	    else
	    {
        // $[TI9.2]
            // $[05166]
            //notify alarms on "Startup disconnect" alarm.
            RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_STARTUP_DISCONNECT);
            pNextScheduler = (BreathPhaseScheduler*)&RDisconnectScheduler;
	    }
	}
	else if (!isVentSetupComplete)
	{
	// $[TI5]
        pNextScheduler = (BreathPhaseScheduler*)&RSafetyPcvScheduler;
	}
	else
	{
	// $[TI6]
		CLASS_ASSERTION(isVentSetupComplete);
		pNextScheduler = &BreathPhaseScheduler::EvaluateSetScheduler();
	}

	CLASS_PRE_CONDITION( previousSchedulerId >= SchedulerId::FIRST_SCHEDULER &&
					 previousSchedulerId <= SchedulerId::TOTAL_SCHEDULER_IDS &&
					 previousSchedulerId != SchedulerId::SVO );

	SchedulerId::SchedulerIdValue nextSchedulerId = pNextScheduler->getId();

	// Close the safety valve, disable all active triggers and set up the trigger to
	// time the settling time.
	RBreathTriggerMediator.resetTriggerList();
	// Make sure current breath is the SafetStatePhase
	CLASS_PRE_CONDITION(BreathPhase::GetCurrentBreathPhase() ==
										(BreathPhase *)&RSafeStatePhase );

	((BreathPhase&)RSafeStatePhase).relinquishControl(
										(BreathTrigger&)RImmediateBreathTrigger);

	if (nextSchedulerId == SchedulerId::STANDBY ||
		nextSchedulerId == SchedulerId::DISCONNECT ||
		nextSchedulerId == SchedulerId::OCCLUSION )
	{
	// $[TI7]
		// enable an immediate breath trigger
		((Trigger&)RImmediateBreathTrigger).enable();
	  	RSafetyValve.close() ;
	}
	else
	{
	// $[TI8]
		// Set SVC as the active BreathPhase to close the safety valve.
		// Since relinquishControl and SetCurrentBreathPhase() requires the trigger
		// causing the transition, send a reference to the immediate breath trigger.
		BreathPhase::SetCurrentBreathPhase((BreathPhase&)RSafetyValveClosePhase,
											(BreathTrigger&)RImmediateBreathTrigger);
		BreathPhase::NewBreath() ;
	}
    BreathPhaseScheduler::SetIsStandbyRequired(FALSE);
	pNextScheduler->takeControl(*this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
// The method accepts a breath phase scheduler reference as an argument.  It is
// invoked when the previous scheduler is relinquishing control. The scheduler
// registers with the BreathPhaseScheduler as the current scheduler, and the
// mode triggers relevant to that mode are set to be enabled.  The BD Status
// task is notified of the start of SVO. The base class method for
// takeControl() is invoked to handle activities common to all schedulers.  The
// expiratory pause and inspiratory pause events are canceled, the asap trigger,
// and the peep recovery mand insp trigger are disabled, the static members for mode
// and rate change are set to false.
// The scheduler id in the bd system state object in nov ram is set to SVO.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler reference passed as an argument is used to get the id of the
// current scheduler.  The static method of BreathPhaseScheduler is used to
// register the new scheduler.  REventFilter.postEventStatus() is
// invoked to send SVO status to the BD Status task.  The objects
// RExpiratoryPauseEvent and RInspiratoryPauseEvent are used to cancel the pause
// event. the object rAsapInspTrigger is used to disable the trigger. The class
// BdSystemStateHandler is used to set the scheduler id in the nov ram object
// for BdSystemState.
//---------------------------------------------------------------------
//@ PreCondition
//	The scheduler's id is checked to be any valid id except SVO.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SvoScheduler::takeControl(const BreathPhaseScheduler& scheduler)
{
	CALL_TRACE("SvoScheduler::takeControl(const BreathPhaseScheduler& scheduler)");

    // Invoke the base class takeControl method
    BreathPhaseScheduler::takeControl(scheduler);

	// disable the following triggers
	((BreathTrigger&)RAsapInspTrigger).disable();
	((BreathTrigger&)RPeepRecoveryMandInspTrigger).disable();

	// $[BL04070] :d disable pause event request
	// $[BL04072] :d disable pause event request
	// $[BL04071] :d terminate active auto expiratory pause
	// $[04220] :d terminate active manual expiratory pause
	// $[BL04008] :d disable pending pause event
	// $[BL04075] :d cancel pause request
	// $[BL04012] :d terminate active pause request
	// $[BL04078] :d terminate manual active inspirtory pause
	// $[RM12077] cancel a NIF maneuver if SVO is detected
	// $[RM12062] cancel a VC maneuver if SVO is detected
	// $[RM12094] cancel a P100 maneuver if SVO is detected
	Maneuver::CancelManeuver();

	// cancel any mode or rate change that may have been initialized during
	// the previous mode
	BreathPhaseScheduler::ModeChange_ = FALSE;
	BreathPhaseScheduler::RateChange_ = FALSE;

	SchedulerId::SchedulerIdValue schedulerId = scheduler.getId();

	CLASS_PRE_CONDITION( schedulerId >= SchedulerId::FIRST_SCHEDULER &&
					 	 schedulerId <= SchedulerId::TOTAL_SCHEDULER_IDS &&
					 	 schedulerId != SchedulerId::SVO );

	// update the reference to the newly active breath scheduler
    BreathPhaseScheduler::SetCurrentScheduler_((BreathPhaseScheduler&)(*this));

    //The BreathRecord is not being updated with the new scheduler id, so that
    // the identity of the scheduler that originated this breath is maintained.

    // update the BD state, in case of power interruption
   	BdSystemStateHandler::UpdateSchedulerId(getId());

	// Set the mode triggers for that scheduler.
	enableTriggers_();
	RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);

	// notify the BD Status task of SVO
	REventFilter.postEventStatus( EventFilter::SVO, EventData::ACTIVE) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SvoScheduler::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, SVOSCHEDULER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// The method takes no arguments and returns no value.  A request to enable
// SvoScheduler valid mode triggers is issued whenever the scheduler takes
// control.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode trigger instances are accessed directly with a call to their
// setIsEnableRequested() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SvoScheduler::enableTriggers_(void)
{
// $[TI1]
	CALL_TRACE("SvoScheduler::enableTriggers_(void)");

	((ModeTrigger&)RSvoResetTrigger).setIsEnableRequested(TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[05016]
// The argument of type EventId indicates which user event is active.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is rejected by
// invoking a call to the static method:
// BreathPhaseScheduler::RejectManualInspiration_(eventStatus).
// The alarm reset event is ignored, while the eventStatus argument value is checked for
// TRUE.
// The expiratory and inspiratory pause events are rejected by invoking a call to
// the static methods: BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus) and
// BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus) .
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
SvoScheduler::reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus)
{
	CALL_TRACE("SvoScheduler::reportEventStatus_(const EventData::EventId id,\
													 const Boolean eventStatus)");

	EventData::EventStatus rtnStatus = EventData::IDLE;

	switch (id)
	{
		case EventData::MANUAL_INSPIRATION:
		// $[TI1]
			rtnStatus =
					BreathPhaseScheduler::RejectManualInspiration_(eventStatus);
			break;

		case EventData::ALARM_RESET:
		// $[TI2]
			// ignore alarm reset
			CLASS_PRE_CONDITION(eventStatus);
			break;

		case EventData::EXPIRATORY_PAUSE:
		// $[TI3]
			rtnStatus =
					BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus);
			break;

		case EventData::INSPIRATORY_PAUSE:
		// $[TI4]
		// inspiratory pause shall not be active
		// $[BL04007]
			rtnStatus =
					BreathPhaseScheduler::RejectInspiratoryPause_(eventStatus);
			break;

		case EventData::NIF_MANEUVER:
		case EventData::P100_MANEUVER:
		case EventData::VITAL_CAPACITY_MANEUVER:
			// $[RM12032] Reject RM maneuvers in SVO mode
			rtnStatus = EventData::REJECTED;
			break;

		default:
		// $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(id);
	}	

	return (rtnStatus);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================





