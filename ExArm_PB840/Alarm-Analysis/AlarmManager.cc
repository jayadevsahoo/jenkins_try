#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmManager - Direct Operating Parameter and Alarm Analysis clusters.
//---------------------------------------------------------------------
//@ Interface-Description
// When an OperatingCondition changes state, or a timer expires indicating time
// to examine an OperatingCondition or OperatingGroup, an event associated with
// the enumerated name of the condition or group is added to the Alarm task
// OperatingParameterEventQueue.  When the OS awakens the Alarm task, the 
// Operating Parameter Analysis cluster will begin processing the events 
// contained in the queue.  Once the Operating Parameter hierarchy has been 
// reevaluated, the Alarm Analysis cluster will reevaluate the Alarm objects 
// which were marked during the evaluation of the Operating Parameter Hierarchy.
//
// The output of this processing is an AlarmUpdateBuffer which is sent to the
// Alarm Annunciation cluster.
//
// AlarmManager is also responsible for initiating processing associated with 
// the operator pressing the Alarm Reset key.
//
// AlarmManager handles resetting Patient Data Alarms when an alarm causing
// Loss of Patient Data Monitoring auto-resets.
//
// The Operating Parameter Hierarchy and the Alarm objects are constructed via
// the placement new operator in AlarmManager::Initialize.  As part of this
// process, the priority and urgency information in the Alarm Matrices is 
// assigned to the appropriate AlarmInformation objects.
//---------------------------------------------------------------------
//@ Rationale
// The single instance of AlarmManager (per CPU) is responsible for directing
// the processing in the Operating Parameter Analysis cluster and the Alarm
// Analysis cluster.  Output from the processing is handed off to the 
// AlarmAnnunciator (the director of processing for the Alarm Annunciation
// cluster) on the GUI CPU.
//---------------------------------------------------------------------
//@ Implementation-Description
// When the OS detects that the Alarm task OperatingParameterEventQueue is not 
// empty it schedules the Alarm task to run.  When the Alarm-Analysis subsystem 
// is awakened, it begins processing by examining the messages on the 
// OperatingParameterEventQueue.  The messages represent either 
// OperatingParameterEventName enumerators or pointers to ViolationHistory 
// objects.  The OperatingParameterEventName enumerators are used to determine
// which OperatingCondition objects need to be evaluated or indicate Manual 
// Reset.  The ViolationHistory pointers indirectly point to OperatingGroup 
// objects which need to be evaluated.  All events must be handled 
// in a single cycle of Alarm processing.
//
// The Operating Parameter cluster is messaged to evaluate the Operating
// Parameter Hierarchy.  The Operating Parameter Hierarchy is an inverted
// forest where the OperatingCondition objects are the leaves and Operating
// Group objects are the branches.  All ancestor AlarmOperand objects associated
// with the AlarmOperand objects directly associated with the events in the 
// Alarm task Queue are marked as needing to be updated.  Then, marked 
// AlarmOperand objects are evaluated, and if any OperatingGroup that augments 
// or causes an alarm changes state, those alarms are marked as needing to be
// updated.
//
// When all AlarmOperand objects have been evaluated, the marked alarms are
// processed by the Alarm Analysis cluster.  The current annunciation
// information for the Alarm is cleared.  The Alarm object's augmentations and
// associated OperatingGroup objects are examined in order to build a current
// annunciation.  If an augmentation is active, the information referenced by
// that augmentation is merged into the current alarm annunciation.
//
// Finally, an AlarmUpdateBuffer is built and stored in the AlarmUpdateBuffer 
// list on the GUI_CPU.  The AlarmAnnunciator class in the Alarm Annunciation 
// cluster is messaged to update the Current Alarm Log and Alarm History Log 
// with the new alarm annunciation information.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  There are no instantiations of this class.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmManager.ccv   25.0.4.0   19 Nov 2013 13:51:14   pvcs  $
//
//@ Modification-Log
//  
//   Revision 001   By:   hct    Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct    Date: 05/22/95         DR Number:   392
//      Project:   Sigma   (R8027)
//      Description:
//         Changed Technical Alarm text as per changes to SRS.
//  
//   Revision 003   By:   hct    Date: 05/22/95         DR Number:   7
//      Project:   Sigma   (R8027)
//      Description:
//         Changed Non-Technical Alarm text and structure as per changes to SRS.
//  
//   Revision 004   By:   hct    Date: 05/31/95         DR Number:   470
//      Project:   Sigma   (R8027)
//      Description:
//         Added default/break statements to those switch statements from which
//         they were missing.
//  
//   Revision 005   By:   hct    Date: 06/01/95         DR Number:   472
//      Project:   Sigma   (R8027)
//      Description:
//         Moved the call to register for end-of-breath events 
//         (PatientDataMgr::RegisterEndOfBreath()) so the call is 
//         conditionally compiled for the GUI cpu only.
//  
//   Revision 006   By:   hct    Date: 08/02/95         DR Number:   502
//      Project:   Sigma   (R8027)
//      Description:
//         Removed the call to rAlarm.getIsDependent() from
//         CreateAlarmUpdateList_().  DCI reports need to know all active
//         alarms regardless of whether they are dependent augmentations or
//         not.  Therefore, all active alarms must be sent to the 
//         AlarmAnnunciation cluster.  The check for dependent alarms will be
//         the responsibility of the AlarmAnnunciation cluster.
//  
//   Revision 007   By:   hct    Date: 08/08/95         DR Number:   571
//      Project:   Sigma   (R8027)
//      Description:
//         Added single event ALARM_BROADCAST_EN and call to 
//         AlarmBroadcast::BroadcastAlarms() to support broadcast of 
//         Alarm information.
//  
//   Revision 008   By:   hct    Date: 08/23/95         DR Number:   550
//      Project:   Sigma   (R8027)
//      Description:
//         Added call to AlarmManager::ROperandNameList_.append(C155) to 
//         cause the parent AlarmOperand objects of OperatingCondition C155
//         to be evaluated.
//         When an alarm causing Loss of Patient Data Monitoring occurs on
//         one CPU, the alarms based on Patient Data on the other CPU are not
//         disabled.  When the alarm causing Loss of Patient Data Monitoring
//         autoresets, the active alarms based on Patient Data on the other
//         CPU are not reset.
//
//   Revision 009   By:   hct    Date: 01/11/96         DR Number:   649
//      Project:   Sigma   (R8027)
//      Description:
//         Changed conditional compilation label from SIGMA_DEVELOPMENT to
//         SIGMA_SIMULATOR.
//  
//   Revision 010   By:   hct    Date: 03/22/96         DR Number:   795
//      Project:   Sigma   (R8027)
//      Description:
//         Integrate with TaskMonitor.
//  
//   Revision 011   By:   gbs    Date: 05/17/96         DR Number:   996
//      Project:   Sigma   (R8027)
//      Description:
//         Modify [TI] numbers.
//  
//   Revision 012   By:   gbs    Date: 05/17/96         DR Number:   995
//      Project:   Sigma   (R8027)
//      Description:
//         Fix CALL_TRACE and add passed parameter comments.
//         These changes do not affect Unit Test.
//  
//   Revision 013   By:   hct    Date: 05/23/96         DR Number:   648
//      Project:   Sigma   (R8027)
//      Description:
//         Changed DISCONNECT alarms analysis message to "No ventilation.".
//  
//   Revision 014   By:   hct    Date: 05/31/96         DR Number:  1032
//      Project:   Sigma   (R8027)
//      Description:
//         Added callback routine for averaged patient data.  Added call to 
//         Patient-Data subsystem to register callback routine.
//  
//   Revision 015   By:   hct    Date: 07/29/96         DR Number:  1185
//      Project:   Sigma   (R8027)
//      Description:
//         The Apnea alarm is affected by Loss of Patient Data Monitoring;
//         however, the reset and detection of the alarm during LPDM is
//         handled by the Breath-Delivery subsystem so it should not be
//         handled like the other Patient-Data alarms.  To accomplish this,
//         Alarm::patientDataAlarm_ is set to FALSE in the constructor call
//         for the Apnea alarm.  This change does not affect the unit test.
//  
//   Revision 016   By:   hct    Date: 07/29/96         DR Number:  1028
//      Project:   Sigma   (R8027)
//      Description:
//         The INSPIRATION TOO LONG alarm does not escalate in urgency when
//         a mandatory breath occurs between violation SPONT breaths.  The code
//         was counting all breaths  for escalation and de-escalation instead
//         of looking at SPONT breaths only.  This change does not affect the
//         Unit Test.
//  
//   Revision 017   By:   hct    Date: 08/02/96         DR Number:  10027
//      Project:   Sigma   (R8027)
//      Description:
//         Moved DISCONNECT, STANDBY DISCONNECT and SEVERE OCCLUSION alarms
//         to the GUI CPU.  Since the MANUAL RESET event is not sent to the 
//         BD CPU, when the RESET key was pressed, the TYPE column in the
//         Alarm History Log was showing AUTORESET instead of MANUAL RESET.
//  
//   Revision 018   By:   hct    Date: 08/07/96         DR Number:  1201
//      Project:   Sigma   (R8027)
//      Description:
//         Remove 1 second time delay for detection/reset of supply alarms.
//  
//   Revision 019   By:   hct    Date: 08/13/96         DR Number:  1206
//      Project:   Sigma   (R8027)
//      Description:
//         Removed functionality that allow processing of multiple alarm
//         events in one cycle of Alarm-Analysis subsystem processing.
//         If two events which canceled each other out were received and
//         processed in the same round of processing, it is possible an alarm
//         condition would have gone unreported.
//  
//   Revision 020   By:   hct      Date: 08/26/96         DR Number: 10041
//      Project:   Sigma   (R8027)
//      Description:
//         Changed processing for Update() so all AlarmUpdateBuffers (up to
//         NUM_BUFF) are processed in one pass before GUI-Apps is called to 
//         update the displays.  Made ALARM_BROADCAST_EN a separate event type
//         so alarm broadcast events wouldn't cause an update to be sent to
//         the GUI to enhance throughput.
//  
//   Revision 021   By:   hct      Date: 09/09/96         DR Number:  1209
//      Project:   Sigma   (R8027)
//      Description:
//         Changed Alarm setup as per changes to SRS to 
//         properly annunciate PSOL Stuck Closed / Supply alarms.
//  
//   Revision 022   By:   hct      Date: 10/30/96         DR Number:  1487
//      Project:   Sigma   (R8027)
//      Description:
//         Moved HIGH and LOW FiO2 Alarm detection to GUI_CPU.
//  
//   Revision 023   By:   hct      Date: 11/06/96         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.
//  
//   Revision 024   By:   hct    Date: 11/11/96         DR Number:  1530
//      Project:   Sigma   (R8027)
//      Description:
//         The end-of-breath data packet contains the breath type for the new
//         breath instead of the previous breath.  Added callback 
//         ReceiveRealtimeData to store the breath type in the static
//         data of method GetBreathType.
//
//   Revision 025   By:   hct    Date: 12/06/96         DR Number:  1508, 1535
//      Project:   Sigma   (R8027)
//      Description:
//         Removed check for the Alarm being active from the check of 
//         ConditionAugmenations in UpdateAlarms_.
//
//   Revision 026   By:   hct    Date: 12/18/96         DR Number:  1611
//      Project:   Sigma   (R8027)
//      Description:
//         Use new previous breath type from End-of-Breath data packet.
//
//   Revision 027   By:   hct    Date: 01/16/97         DR Number:  1603
//      Project:   Sigma   (R8027)
//      Description:
//         VentStatus::VentStatusService() obsoleted; use
//         VentStatus::DirectVentStatusService() instead.
//
//   Revision 028   By:   hct    Date: 01/29/97         DR Number:  1654
//      Project:   Sigma   (R8027)
//      Description:
//         Added alarm message "Power loss & return occurred with a
//         pre-existing DEVICE ALERT." and "Check Alarm Log.  EST required."
//         to BTAT5.
//  
//   Revision 029   By:   hct    Date: 01/31/97         DR Number:   505
//      Project:   Sigma   (R8027)
//      Description:
//         Updated urgency/priorities to match changes to the SRS.
//  
//   Revision 030   By:   hct    Date: 02/24/97         DR Number:  1794
//      Project:   Sigma   (R8027)
//      Description:
//         Swapped analysis/remedy messages for BTAT4 and BTAT7.
//  
//   Revision 030   By:   hct    Date: 02/24/97         DR Number:  1794
//      Project:   Sigma   (R8027)
//      Description:
//         Swapped analysis/remedy messages for BTAT4 and BTAT7.
//  
//   Revision 031   By:   hct    Date: 02/26/97         DR Number:  1793
//      Project:   Sigma   (R8027)
//      Description:
//         Delete WEAK BATTERY alarm and change analysis messages for 
//         INOPERATIVE BATTERY and LOW BATTERY
//  
//   Revision 032   By:   hct    Date: 04/23/97         DR Number:  1992
//      Project:   Sigma   (R8027)
//      Description:
//         Requirements 5047 and 5079 have been merged into other
//         requirements in the matrices and deleted.
//  
//   Revision 033   By:   hct    Date: 24 APR 1997      DR Number:  1994
//      Project:   Sigma   (R8027)
//      Description:
//         Deleted requirement number 05147.
//  
//   Revision 034   By:   hct    Date: 02 JUN 1997      DR Number:  1544
//      Project:   Sigma   (R8027)
//      Description:
//         Added Dependent Augmentation link from AC POWER LOSS to LOW
//         AC POWER so that LOW AC POWER alarm is not annunciated during 
//         loss of AC power.
//  
//   Revision 035   By:   hct    Date: 06/19/97         DR Number:  2097
//      Project:   Sigma   (R8027)
//      Description:
//         Change analysis messages for HIGH Pvent.
//  
//   Revision 036   By:   hct    Date: 08/06/97         DR Number:  2350
//      Project:   Sigma   (R8027)
//      Description:
//         Add C140 to G29004 so that G29004 will be evaluated when the
//         COMPRESSOR INOPERATIVE alarm is detected.
//  
//   Revision 037   By:   hct    Date: 08/27/97         DR Number:  2322
//      Project:   Sigma   (R8027)
//      Description:
//         Add analysis message and remedy message for BTAT 8.
//  
//   Revision 038   By:   hct    Date: 09/16/97         DR Number:  2490
//      Project:   Sigma   (R8027)
//      Description:
//         Corrected APNEA alarm augmentations.
//  
//   Revision 039   By:   hct    Date: 09/16/97         DR Number:  2503
//      Project:   Sigma   (R8027)
//      Description:
//         Changed messages for BTAT 6 (Touch screen blocked).
//  
//   Revision 040   By:   hct    Date: 09/29/97         DR Number:  1475
//      Project:   Sigma   (R8027)
//      Description:
//         Add VOLUME LIMIT alarm.
//  
//   Revision 041   By:   hct    Date: 11/03/97         DR Number:  2599
//      Project:   Sigma   (R8027)
//      Description:
//         Change Low and High O2 alarms per DCS.
//  
//   Revision 042   By:   hct    Date: 12/16/97         DR Number:  2688
//      Project:   Sigma   (R8027)
//      Description:
//         Add hysteresis to LSP alarms.
//  
//   Revision 043	By:   hct	   Date: 01/12/98       DR Number: 5001
//      Project:   Sigma   (R8O27)
//      Description:
//		   Added call to Alarms::ListActivePDResetAlarmConditions() in
//                 ManualResetAlarms().
//
//   Revision 044	By:   sah	   Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//		   Removed all referenced to newly-obsoleted class ('AlarmMessages').
//
//   Revision 045	By:   syw	   Date: 04/09/98       DR Number: 5048
//      Project:   Sigma   (R8O27)
//      Description:
//		   Eliminate TIMING_TEST code.
//
//   Revision 046   By:   hct    Date: 06/23/98         DR Number:  5109
//      Project:   Sigma   (R8027)
//      Description:
//         Changed High Circuit Pressure alarm per DCS.
//  
//   Revision 047   By:   hct    Date: 08/25/98         DR Number:  5131
//      Project:   Sigma   (R8027)
//      Description:
//         Changed HIGH and LOW FIO2 alarms per DCS.  If the LOW FIO2 
//         event is received, clear the HIGH FIO2 condition.  If the 
//         HIGH FIO2 event is received, clear the LOW FIO2 condition.
//  
//   Revision 048   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 049   By:   hct      Date: 01/13/99       DR Number: 5322
//      Project:   Sigma   (R8O27)
//      Description:
//         Add ATC functionality.
//  
//   Revision 050   By:   sah      Date: 03/15/99       DR Number: 5314
//      Project:   ATC
//      Description:
//         Settings' callback/monitoring mechanism changed to an observer/
//         subject framework.  Changed this class to use the new mechanism.
//
//   Revision 051   By:   hct      Date: 03/23/99       DR Number: 5314
//      Project:   ATC
//      Description:
//         Move instantiation of RInstance_ to InitMemory().
//
//   Revision 052   By:   hct      Date: 24/JUN/99       DR Number: 5450
//      Project:   ATC
//      Description:
//         Add if statement to Settings' observer/subject framework to avoid
//         updates on adjustments and just use accepted values.
//  
//   Revision 053   By:   yyy      Date: 08/18/99         DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified to handle alarm lock and break through.
//
//   Revision 054   By:   hct      Date: 08/27/99         DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified Disconnect alarm so it does not break through.
//
//   Revision 055   By:   sah      Date: 08/27/99       DR Number: 5513
//      Project:   ATC
//      Description:
//         Modified BTAT #8's primary msg from 'DEVICE ALERT' to 'O2 SENSOR',
//         thereby requiring the need of a new message name.
//
//   Revision 056   By:   hct    Date: 08/27/99         DR Number:  5513
//      Project:   NewAlarms
//      Description:
//         Removed de-escalation from Patient-Data alarms.
//  
//  Revision: 057   By: syw   Date:  07-Aug-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//      Handle PAV alarms.
//
//  Revision: 058   By: hct   Date:  11-Aug-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      Create PAV alarms.
//
//  Revision: 059   By: hct   Date:  30-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      Create VTPC alarms.
//
//  Revision: 060   By: hct   Date:  02-OCT-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      Add new dependent augmentations to VTPC alarms.
//
//  Revision: 061   By: hct   Date:  16-OCT-2000    DR Number: 5755, 5313
//  Project:  VTPC & PAV
//  Description:
//      Review rework.
//
//  Revision: 062   By:   hct    Date: 24-OCT-2000      DR Number: 5755
//  Project:  VTPC
//  Description:
//      Differentiate between spont and mand breaths for VTPC alarms.
//
//  Revision: 063   By:   heatherw Date: 8-MAR-2002      DR Number: 5790
//  Project:  VTPC
//  Description:
//      Added Vti Spont alarm for Volume support (A385).
//      Changed Inspired Volume alarm A340 to become inpired Spont
//      alarm for pressure support. Vti Spont Pressure (A380).
//
//   Revision: 064  By:   erm      Date: 11-MAR-2002    DR Number: 5790
//   Project:  VTPC
//   Description:
//        Added support for mand inspired Alarm
//
//   Revision: 065  By:   erm      Date: 25-MAR-2002    DR Number: 5790
//   Project:  VTPC
//   Description:
//        Added support for mand inspired Alarm to be examined after END-OF-BREATH
//
//   Revision: 066  By:   ljstar   Date: 20-Aug-2003    DR Number: 6086
//   Project:  PAV3
//   Description:
//        SDCR 6086
//
//   Revision: 067  By:   ljstar   Date: 20-Aug-2003    DR Number: 6086
//   Project:  PAV3
//   Description:
//        SDCR 6086
//
//   Revision: 068  By:   ljstar   Date: 21-Aug-2003    DR Number: 6086
//   Project:  PAV3
//   Description:
//        SDCR 6086, cleaned up comments, corrected preexisting typos related to A385
//
//   Revision: 069  By:   ljstar   Date: 22-Aug-2003    DR Number: 6086
//   Project:  PAV3
//   Description:
//        SDCR 6086: For PAV Project, modified remedy msgs for Pcomp (A345) and Vti-spont (A380),
//        so that there are different versions for spontaneous types TC and PAV.
//
//   Revision: 070  By:   gdc      Date: 21-Feb-2005    DR Number: 6144
//   Project:  NIV1
//   Description:
//        Modifications to support NIV.
//        Removed obsolete low insp pressure alarm.
//        Added new low insp (Ppeak) pressure alarm.
//        Changed insp too long alarm to low urgency for NIV.
//
//   Revision: 071  By:   gdc      Date: 23-Feb-2005    DR Number: 6144
//   Project:  NIV1
//   Description:
//        DCS 6144 - NIV1
//        Code review rework. Added Low Ppeak alarm requirement numbers.
//
//   Revision: 072  By:   gdc      Date: 10-June-2005    DR Number: 6170
//   Project:  NIV1
//   Description:
//        DCS 6170 - NIV2
//        Removed INSP TOO LONG alarm when in NIV.
//
//   Revision: 073   By:   rhj    Date: 05-March-2007      DR Number: 6359
//   Project:  RESPM
//   Description:
//        Resolve merge conflict for RM to Baseline 
//
//   Revision: 074  By:  gdc    Date:  10-Dec-2008    SCR Number: 6068
//   Project:  840S
//   Description:
//	 Removed SUN prototype code.
//
//   Revision: 075  By:  rhj    Date: 15-Jan-2009      SCR Number: 6452  
//   Project:  840S
//        Add an additional remedy message to Circuit Disconnect alarm.		 
//
//  Revision: 076   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//  
//  Revision: 077   By: rhj    Date: 23-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added Prox alarm.
// 
//  Revision: 078   By: rhj    Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added PROX_INOPERATIVE_REMEDY_MSG.
// 
//=====================================================================
#include "Sigma.hh"
#include "AlarmManager.hh"

#include "AlarmResponses.hh"
#include "AlarmQueueMsg.hh"
#include "TaskMonitor.hh"
#include "NetworkApp.hh"

//@ Usage-Classes
#ifdef SIGMA_GUI_CPU
#include "Alarm.hh"
#include "AlarmAnnunciator.hh"
#include "AlarmBroadcastArray.hh"
#include "AlarmInformation.hh"
#include "AlarmOperand.hh"
#include "Array_MessageName_NUM_MESSAGE.hh"
#include "BreathType.hh"
#include "CalcFunctions.hh"
#include "ConditionAugmentation.hh"
#include "ConditionEvaluations.hh"
#include "CountConstraint.hh"
#include "DependentAugmentation.hh"
#include "DependentInformation.hh"
#include "OperatingComponent.hh"
#include "PatientDataId.hh"
#include "PatientDataMgr.hh"
#include "TemporalConstraint.hh"
#include "VHMCurrBreathCountCardinal.hh"
#include "VHMPrevBreathCountCardinal.hh"
#include "VHMCurrBreathCountDur.hh"
#include "VHMEventCountDur.hh"
#include "VHMTemporal.hh"
#include "MsgTimer.hh"
#include "Heap_OperatingCondition_NUM_OPCOND.hh"
#include "Heap_OperatingComponent_NUM_OPCOMP.hh"
#include "Heap_OperatingGroup_NUM_OPGROUP.hh"
#include "Heap_CountConstraint_NUM_CONST.hh"
#include "Heap_TemporalConstraint_NUM_CONST.hh"
#include "Heap_Alarm_MAX_NUM_ALARMS.hh"
#include "Heap_AlarmInformation_NUM_ALARMINFO.hh"
#include "Heap_DependentInformation_NUM_DEPINFO.hh"
#include "Heap_ConditionAugmentation_NUM_CONDAUG.hh"
#include "Heap_DependentAugmentation_NUM_DEPAUG.hh"
#include "Heap_VHMCurrBreathCountCardinal_NUM_VHM.hh"
#include "Heap_VHMPrevBreathCountCardinal_NUM_VHM.hh"
#include "Heap_VHMCurrBreathCountDur_NUM_VHM.hh"
#include "Heap_VHMEventCountDur_NUM_VHM.hh"
#include "Heap_VHMTemporal_NUM_VHM_T.hh"
#include "SettingSubject.hh"

#include "EventData.hh"
#include "LeakCompEnabledValue.hh"
#include "ContextId.hh"
#include "SettingContextHandle.hh"
#include "MiscBdEventHandler.hh"
#include "SettingsMgr.hh"
#include "BoundedValue.hh"
#include "Setting.hh"
#include "DiscoSensSetting.hh"
#include "MemoryHandle.hh"
#endif // SIGMA_GUI_CPU

#ifdef SIGMA_BD_CPU
#  include "VentStatus.hh"
#endif // SIGMA_BD_CPU


//@ End-Usage

//@ Code...
#ifdef SIGMA_GUI_CPU
  static Uint 
      pOperandNameListMemory[(sizeof(OperandNameList) + sizeof(Uint) - 1) / 
                              sizeof(Uint)];
  OperandNameList& AlarmManager::ROperandNameList_ = 
      *((OperandNameList*)::pOperandNameListMemory);


  static Uint 
      pAlarmUpdateListMemory[(sizeof(AlarmUpdateList) + sizeof(Uint) - 1) / 
                              sizeof(Uint)];
  AlarmUpdateList& AlarmManager::RAlarmUpdateList_ =
      *((AlarmUpdateList*)::pAlarmUpdateListMemory);


  static Uint 
      pCheckAlarmListMemory[(sizeof(CheckAlarmList) + sizeof(Uint) - 1) / 
                             sizeof(Uint)]; 
  CheckAlarmList& AlarmManager::RCheckAlarmList_ =
      *((CheckAlarmList*)::pCheckAlarmListMemory);


  // OperatingCondition
  static Uint pFHOC_
      [(sizeof(FixedHeap(OperatingCondition,NUM_OPCOND)) + sizeof(Uint) 
      - 1) / sizeof(Uint)];
  FixedHeap(OperatingCondition,NUM_OPCOND)& rFHOC_ = 
      *((FixedHeap(OperatingCondition,NUM_OPCOND)*)::pFHOC_);

  // OperatingComponent
  static Uint pFHOK_
      [(sizeof(FixedHeap(OperatingComponent,NUM_OPCOMP)) + sizeof(Uint) 
      - 1) / sizeof(Uint)];
  FixedHeap(OperatingComponent,NUM_OPCOMP)& rFHOK_ = 
      *((FixedHeap(OperatingComponent,NUM_OPCOMP)*)::pFHOK_);

  // OperatingGroup
  static Uint pFHOG_
      [(sizeof(FixedHeap(OperatingGroup,NUM_OPGROUP)) + sizeof(Uint) - 1)
      / sizeof(Uint)];
  FixedHeap(OperatingGroup,NUM_OPGROUP)& rFHOG_ = 
      *((FixedHeap(OperatingGroup,NUM_OPGROUP)*)::pFHOG_);

  // CountConstraint
  static Uint pFHCC_
      [(sizeof(FixedHeap(CountConstraint,NUM_CONST)) + sizeof(Uint) - 1)
      / sizeof(Uint)];
  FixedHeap(CountConstraint,NUM_CONST)& rFHCC_= 
      *((FixedHeap(CountConstraint,NUM_CONST)*)::pFHCC_);

  // TemporalConstraint
  static Uint pFHTC_
      [(sizeof(FixedHeap(TemporalConstraint,NUM_CONST)) + sizeof(Uint) -
      1) / sizeof(Uint)];
  FixedHeap(TemporalConstraint,NUM_CONST)& rFHTC_ = 
      *((FixedHeap(TemporalConstraint,NUM_CONST)*)::pFHTC_);

  // Alarm
  static Uint pFHA_
      [(sizeof(FixedHeap(Alarm,MAX_NUM_ALARMS)) + sizeof(Uint) - 1)
      / sizeof(Uint)];
  FixedHeap(Alarm,MAX_NUM_ALARMS)& rFHA_ = 
      *((FixedHeap(Alarm,MAX_NUM_ALARMS)*)::pFHA_);

  // AlarmInformation
  static Uint pFHAI_
      [(sizeof(FixedHeap(AlarmInformation,NUM_ALARMINFO)) + sizeof(Uint) - 1)
      / sizeof(Uint)];
  FixedHeap(AlarmInformation,NUM_ALARMINFO)& rFHAI_ = 
      *((FixedHeap(AlarmInformation,NUM_ALARMINFO)*)::pFHAI_);

  // DependentInformation
  static Uint pFHDI_
      [(sizeof(FixedHeap(DependentInformation,NUM_DEPINFO)) + sizeof(Uint)
      - 1) / sizeof(Uint)];
  FixedHeap(DependentInformation,NUM_DEPINFO)& rFHDI_ = 
      *((FixedHeap(DependentInformation,NUM_DEPINFO)*)::pFHDI_);

  // ConditionAugmentation;
  static Uint pFHCA_
      [(sizeof(FixedHeap(ConditionAugmentation,NUM_CONDAUG)) + sizeof(Uint)
      - 1) / sizeof(Uint)];
  FixedHeap(ConditionAugmentation,NUM_CONDAUG)& rFHCA_ = 
      *((FixedHeap(ConditionAugmentation,NUM_CONDAUG)*)::pFHCA_);

  // DependentAugmentation
  static Uint pFHDA_
      [(sizeof(FixedHeap(DependentAugmentation,NUM_DEPAUG)) + sizeof(Uint)
      - 1) / sizeof(Uint)];
  FixedHeap(DependentAugmentation,NUM_DEPAUG)& rFHDA_ = 
      *((FixedHeap(DependentAugmentation,NUM_DEPAUG)*)::pFHDA_);

  // VHMCurrBreathCountCardinal
  static Uint pFHVHMCBCC_
      [(sizeof(FixedHeap(VHMCurrBreathCountCardinal,NUM_VHM)) + 
      sizeof(Uint) - 1) / sizeof(Uint)];
  FixedHeap(VHMCurrBreathCountCardinal,NUM_VHM)& rFHVHMCBCC_ = 
      *((FixedHeap(VHMCurrBreathCountCardinal,NUM_VHM)*)::pFHVHMCBCC_);


  // VHMPrevBreathCountCardinal
  static Uint pFHVHMPBCC_
      [(sizeof(FixedHeap(VHMPrevBreathCountCardinal,NUM_VHM)) + 
      sizeof(Uint) - 1) / sizeof(Uint)];
  FixedHeap(VHMPrevBreathCountCardinal,NUM_VHM)& rFHVHMPBCC_ = 
      *((FixedHeap(VHMPrevBreathCountCardinal,NUM_VHM)*)::pFHVHMPBCC_);


  // VHMCurrBreathCountDur
  static Uint pFHVHMCBCD_
      [(sizeof(FixedHeap(VHMCurrBreathCountDur,NUM_VHM)) + sizeof(Uint)
      - 1) / sizeof(Uint)];
  FixedHeap(VHMCurrBreathCountDur,NUM_VHM)& rFHVHMCBCD_ = 
      *((FixedHeap(VHMCurrBreathCountDur,NUM_VHM)*)::pFHVHMCBCD_);


  // VHMEventCountDur
  static Uint pFHVHMECD_
      [(sizeof(FixedHeap(VHMEventCountDur,NUM_VHM)) + sizeof(Uint)
      - 1) / sizeof(Uint)];
  FixedHeap(VHMEventCountDur,NUM_VHM)& rFHVHMECD_ = 
      *((FixedHeap(VHMEventCountDur,NUM_VHM)*)::pFHVHMECD_);


  // VHMTemporal
  static Uint pFHVHMT_
      [(sizeof(FixedHeap(VHMTemporal,NUM_VHM_T)) + sizeof(Uint)
      - 1) / sizeof(Uint)];
  FixedHeap(VHMTemporal,NUM_VHM_T)& rFHVHMT_ = 
      *((FixedHeap(VHMTemporal,NUM_VHM_T)*)::pFHVHMT_);


  // instance of this class...
  static Uint  pInstanceMemory_
		  [(sizeof(AlarmManager) + sizeof(Uint) - 1) / sizeof(Uint)];
  AlarmManager&  AlarmManager::RInstance_ = *(AlarmManager*)::pInstanceMemory_;


#endif // SIGMA_GUI_CPU

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//      Method called during system initialization to instantiate objects for
//      the Alarm subsystem to model the information in the Alarm matrices.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::Initialize(void)
{
  CALL_TRACE("Initialize(void)");

#ifdef SIGMA_GUI_CPU
  InitMemory();
  InitHierarchy();

  // FINALLY:  Once all the groups have been added, initialize topLevelGroups. 
  AlarmOperand::InitGroups();

  // Register with the PatientDataMgmt subsystem to receive notification when
  // end-of-breath data is available.
  PatientDataMgr::RegisterEndOfBreath(AlarmManager::SendEndOfBreathEvent);

  // Register with the PatientDataMgmt subsystem to receive notification when
  // averaged patient data is available.
  PatientDataMgr::RegisterAverageData(AlarmManager::SendAveragedDataEvent);

  ::BreathType breathType = AlarmManager::GetBreathType();
#endif // SIGMA_GUI_CPU

  // This will happen on both CPUs.
  // Register with the Ethernet communication link for callback when Operating 
  // Parameter Events are sent over the Ethernet link.
  ::RegisterForXmit(SEND_EVENT_MSG_ID, AlarmManager::ReceiveEvent);

  // $[TI1]
}


#ifdef SIGMA_GUI_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmManager(void)
//
//@ Interface-Description
//      Construct a default instance of this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Attach to the setting subjects that need to be monitored.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

AlarmManager::AlarmManager(void)
{
  CALL_TRACE("AlarmManager(void)");

  // Monitor the accepted values of the APNEA_O2_PERCENT, OXYGEN_PERCENT
  // TRIGGER_TYPE and VENT_TYPE settings.  This monitoring is used to 
  // generate events for the OperatingParameterEventQueue.
  attachToSubject_(SettingId::TRIGGER_TYPE, Notification::VALUE_CHANGED);
  attachToSubject_(SettingId::APNEA_O2_PERCENT, Notification::VALUE_CHANGED);
  attachToSubject_(SettingId::OXYGEN_PERCENT, Notification::VALUE_CHANGED);
  attachToSubject_(SettingId::VENT_TYPE, Notification::VALUE_CHANGED);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmManager(void)
//
//@ Interface-Description
//      Destruct an instance of this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

AlarmManager::~AlarmManager(void)
{
  CALL_TRACE("~AlarmManager(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitMemory
//
//@ Interface-Description
//      Method called during system initialization to allocate all necessary memory for 
//      the Alarm subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::InitMemory(void)
{
  CALL_TRACE("InitMemory(void)");

  // construct the one instance of this class using 'RInstance_'...
  new (&AlarmManager::RInstance_) AlarmManager();

  CLASS_ASSERTION(sizeof(::pOperandNameListMemory) >= 
      sizeof(OperandNameList));
  new (::pOperandNameListMemory) OperandNameList;

  CLASS_ASSERTION(sizeof(::pAlarmUpdateListMemory) >= 
      sizeof(AlarmUpdateList));
  new (::pAlarmUpdateListMemory) AlarmUpdateList;

  CLASS_ASSERTION(sizeof(::pCheckAlarmListMemory) >= 
      sizeof(CheckAlarmList));
  new (::pCheckAlarmListMemory) CheckAlarmList;

  // OperatingCondition
  CLASS_ASSERTION(sizeof(::pFHOC_) >=
      sizeof(FixedHeap(OperatingCondition,NUM_OPCOND)));
  new (::pFHOC_) FixedHeap(OperatingCondition,NUM_OPCOND);

  // OperatingComponent
  CLASS_ASSERTION(sizeof(::pFHOK_) >=
      sizeof(FixedHeap(OperatingComponent,NUM_OPCOMP)));
  new (::pFHOK_) FixedHeap(OperatingComponent,NUM_OPCOMP);

  // OperatingGroup
  CLASS_ASSERTION(sizeof(::pFHOG_) >=
      sizeof(FixedHeap(OperatingGroup,NUM_OPGROUP)));
  new (::pFHOG_) FixedHeap(OperatingGroup,NUM_OPGROUP);

  // CountConstraint
  CLASS_ASSERTION(sizeof(::pFHCC_) >=
      sizeof(FixedHeap(CountConstraint,NUM_CONST)));
  new (::pFHCC_) FixedHeap(CountConstraint,NUM_CONST);

  // TemporalConstraint
  CLASS_ASSERTION(sizeof(::pFHTC_) >=
      sizeof(FixedHeap(TemporalConstraint,NUM_CONST)));
  new (::pFHTC_) FixedHeap(TemporalConstraint,NUM_CONST);

  // Alarm
  CLASS_ASSERTION(sizeof(::pFHA_) >=
      sizeof(FixedHeap(Alarm,MAX_NUM_ALARMS)));
  new (::pFHA_) FixedHeap(Alarm,MAX_NUM_ALARMS);

  // AlarmInformation
  CLASS_ASSERTION(sizeof(::pFHAI_) >=
      sizeof(FixedHeap(AlarmInformation,NUM_ALARMINFO)));
  new (::pFHAI_) FixedHeap(AlarmInformation,NUM_ALARMINFO);

  // DependentInformation
  CLASS_ASSERTION(sizeof(::pFHDI_) >=
      sizeof(FixedHeap(DependentInformation,NUM_DEPINFO)));
  new (::pFHDI_) FixedHeap(DependentInformation,NUM_DEPINFO);

  // ConditionAugmentation;
  CLASS_ASSERTION(sizeof(::pFHCA_) >=
      sizeof(FixedHeap(ConditionAugmentation,NUM_CONDAUG)));
  new (::pFHCA_) FixedHeap(ConditionAugmentation,NUM_CONDAUG);

  // DependentAugmentation
  CLASS_ASSERTION(sizeof(::pFHDA_) >=
      sizeof(FixedHeap(DependentAugmentation,NUM_DEPAUG)));
  new (::pFHDA_) FixedHeap(DependentAugmentation,NUM_DEPAUG);

  // VHMCurrBreathCountCardinal
  CLASS_ASSERTION(sizeof(::pFHVHMCBCC_) >=
      sizeof(FixedHeap(VHMCurrBreathCountCardinal,NUM_VHM)));
  new (::pFHVHMCBCC_) FixedHeap(VHMCurrBreathCountCardinal,NUM_VHM);


  // VHMPrevBreathCountCardinal
  CLASS_ASSERTION(sizeof(::pFHVHMPBCC_) >=
      sizeof(FixedHeap(VHMPrevBreathCountCardinal,NUM_VHM)));
  new (::pFHVHMPBCC_) FixedHeap(VHMPrevBreathCountCardinal,NUM_VHM);


  // VHMCurrBreathCountDur
  CLASS_ASSERTION(sizeof(::pFHVHMCBCD_) >=
      sizeof(FixedHeap(VHMCurrBreathCountDur,NUM_VHM)));
  new (::pFHVHMCBCD_) FixedHeap(VHMCurrBreathCountDur,NUM_VHM);


  // VHMEventCountDur
  CLASS_ASSERTION(sizeof(::pFHVHMECD_) >=
      sizeof(FixedHeap(VHMEventCountDur,NUM_VHM)));
  new (::pFHVHMECD_)FixedHeap(VHMEventCountDur,NUM_VHM);


  // VHMTemporal
  CLASS_ASSERTION(sizeof(::pFHVHMT_) >=
      sizeof(FixedHeap(VHMTemporal,NUM_VHM_T)));
  new (::pFHVHMT_) FixedHeap(VHMTemporal,NUM_VHM_T);

  // $[TI1]
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitHierarchy
//
//@ Interface-Description
//      Method called during system initialization to instantiate objects and
//      create the Operating parameter hierarchy for the Alarm subsystem to
//      model the information in the Alarm matrices.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::InitHierarchy(void)
{
   CALL_TRACE("InitHierarchy(void)");

   //==========================================================
   //
   //   Operating Condition definitions
   //
   //   The following allocates memory for each condition,
   //   calls its constructor, and adds the conditions to the
   //   static condition list.
   //
   //==========================================================

   OperatingCondition* pC5   = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c5,  C5);
      AlarmOperand::AddOperand(pC5);

   OperatingCondition* pC10  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c10,   C10);
      AlarmOperand::AddOperand(pC10);

   OperatingCondition* pC15  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c15,   C15);
      AlarmOperand::AddOperand(pC15);

   OperatingCondition* pC30  = new (::rFHOC_)
      OperatingCondition(ConditionEvaluations::c30,   C30);
      AlarmOperand::AddOperand(pC30);

   OperatingCondition* pC35  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c35,   C35);
      AlarmOperand::AddOperand(pC35);

   OperatingCondition* pC40  = new (::rFHOC_)
      OperatingCondition(ConditionEvaluations::c40,   C40);
      AlarmOperand::AddOperand(pC40);

   OperatingCondition* pC45  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c45,   C45);
      AlarmOperand::AddOperand(pC45);

   OperatingCondition* pC50  = new (::rFHOC_)
      OperatingCondition(ConditionEvaluations::c50,   C50);
      AlarmOperand::AddOperand(pC50);

   OperatingCondition* pC55  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c55,   C55);
      AlarmOperand::AddOperand(pC55);

   OperatingCondition* pC60  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c60,   C60);
      AlarmOperand::AddOperand(pC60);

   OperatingCondition* pC65  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c65,   C65);
      AlarmOperand::AddOperand(pC65);

   OperatingCondition* pC70  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c70,   C70);
      AlarmOperand::AddOperand(pC70);

   OperatingCondition* pC75  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c75,   C75);
      AlarmOperand::AddOperand(pC75);

   OperatingCondition* pC80  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c80,   C80);
      AlarmOperand::AddOperand(pC80);

   OperatingCondition* pC85  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c85,   C85);
      AlarmOperand::AddOperand(pC85);

   OperatingCondition* pC90  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c90,   C90);
      AlarmOperand::AddOperand(pC90);

   OperatingCondition* pC95  = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c95,   C95);
      AlarmOperand::AddOperand(pC95);

   OperatingCondition* pC100 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c100,  C100);
      AlarmOperand::AddOperand(pC100);

   OperatingCondition* pC105 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c105,  C105);
      AlarmOperand::AddOperand(pC105);

   OperatingCondition* pC110 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c110,  C110);
      AlarmOperand::AddOperand(pC110);

   OperatingCondition* pC135 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c135,  C135);
      AlarmOperand::AddOperand(pC135);

   OperatingCondition* pC136 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c136,  C136);
      AlarmOperand::AddOperand(pC136);

   OperatingCondition* pC140 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c140,  C140);
      AlarmOperand::AddOperand(pC140);

   OperatingCondition* pC145 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c145,  C145);
      AlarmOperand::AddOperand(pC145);

   OperatingCondition* pC150 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c150,  C150);
      AlarmOperand::AddOperand(pC150);

   OperatingCondition* pC155 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c155,  C155);
      AlarmOperand::AddOperand(pC155);

   OperatingCondition* pC170 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c170,  C170);
      AlarmOperand::AddOperand(pC170);

   OperatingCondition* pC190 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c190,  C190);
      AlarmOperand::AddOperand(pC190);

   OperatingCondition* pC193 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c193,  C193);
      AlarmOperand::AddOperand(pC193);

   OperatingCondition* pC195 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c195,  C195);
      AlarmOperand::AddOperand(pC195);

   OperatingCondition* pC200 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c200,  C200);
      AlarmOperand::AddOperand(pC200);

   OperatingCondition* pC205 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c205,  C205);
      AlarmOperand::AddOperand(pC205);

   OperatingCondition* pC210 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c210,  C210);
      AlarmOperand::AddOperand(pC210);

   OperatingCondition* pC215 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c215,  C215);
      AlarmOperand::AddOperand(pC215);

   OperatingCondition* pC220 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c220,  C220);
      AlarmOperand::AddOperand(pC220);

   OperatingCondition* pC225 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c225,  C225);
      AlarmOperand::AddOperand(pC225);

   OperatingCondition* pC260 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c260,  C260);
      AlarmOperand::AddOperand(pC260);

   OperatingCondition* pC261 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c261,  C261);
      AlarmOperand::AddOperand(pC261);

   OperatingCondition* pC262 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c262,  C262);
      AlarmOperand::AddOperand(pC262);

   OperatingCondition* pC263 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c263,  C263);
      AlarmOperand::AddOperand(pC263);

   OperatingCondition* pC265 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c265,  C265);
      AlarmOperand::AddOperand(pC265);

   OperatingCondition* pC270 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c270,  C270);
      AlarmOperand::AddOperand(pC270);

   OperatingCondition* pC290 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c290,  C290);
      AlarmOperand::AddOperand(pC290);

   OperatingCondition* pC295 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c295,  C295);
      AlarmOperand::AddOperand(pC295);

   OperatingCondition* pC300 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c300,  C300);
      AlarmOperand::AddOperand(pC300);

   OperatingCondition* pC305 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c305,  C305);
      AlarmOperand::AddOperand(pC305);

   OperatingCondition* pC315 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c315,  C315);
      AlarmOperand::AddOperand(pC315);

   OperatingCondition* pC320 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c320,  C320);
      AlarmOperand::AddOperand(pC320);

   OperatingCondition* pC321 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c321,  C321);
      AlarmOperand::AddOperand(pC321);

   OperatingCondition* pC325 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c325,  C325);
      AlarmOperand::AddOperand(pC325);

   OperatingCondition* pC330 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c330,  C330);
      AlarmOperand::AddOperand(pC330); 

   OperatingCondition* pC335 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c335,  C335);
      AlarmOperand::AddOperand(pC335); 

   OperatingCondition* pC345 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c345,  C345);
      AlarmOperand::AddOperand(pC345); 

   OperatingCondition* pC355 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c355,  C355);
      AlarmOperand::AddOperand(pC355); 

   OperatingCondition* pC360 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c360,  C360);
      AlarmOperand::AddOperand(pC360); 

   OperatingCondition* pC365 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c365,  C365);
      AlarmOperand::AddOperand(pC365); 

   OperatingCondition* pC366 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c366,  C366);
      AlarmOperand::AddOperand(pC366); 

   OperatingCondition* pC380 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c380,  C380);
      AlarmOperand::AddOperand(pC380);

   OperatingCondition* pC385 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c385,  C385);
      AlarmOperand::AddOperand(pC385); 

   OperatingCondition* pC390 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c390,  C390);
      AlarmOperand::AddOperand(pC390); 

   OperatingCondition* pC400 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c400,  C400);
   AlarmOperand::AddOperand(pC400); 

   OperatingCondition* pC901 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c901,  C901);
   AlarmOperand::AddOperand(pC901);

   OperatingCondition* pC902 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c902,  C902);
   AlarmOperand::AddOperand(pC902);

   OperatingCondition* pC1000 = new (::rFHOC_) 
      OperatingCondition(ConditionEvaluations::c1000,  C1000);
   AlarmOperand::AddOperand(pC1000);

   //=======================================================
   //
   //   Alarm Group Definitions that do not have constraints
   //   or VHM's
   //
   //=======================================================

   OperatingGroup* pG500   = new (::rFHOG_)
      OperatingGroup( G500);
      AlarmOperand::AddOperand(pG500);

   OperatingGroup* pG1000  = new (::rFHOG_)
      OperatingGroup( G1000);
      AlarmOperand::AddOperand(pG1000);

   OperatingGroup* pG1500  = new (::rFHOG_)
      OperatingGroup( G1500);
      AlarmOperand::AddOperand(pG1500);

   OperatingGroup* pG2000  = new (::rFHOG_)
      OperatingGroup( G2000);
      AlarmOperand::AddOperand(pG2000);

   OperatingGroup* pG2500  = new (::rFHOG_)
      OperatingGroup( G2500);
      AlarmOperand::AddOperand(pG2500);

   OperatingGroup* pG3000  = new (::rFHOG_)
      OperatingGroup( G3000);
      AlarmOperand::AddOperand(pG3000);

   OperatingGroup* pG3500  = new (::rFHOG_)
      OperatingGroup( G3500);
      AlarmOperand::AddOperand(pG3500);

   OperatingGroup* pG4000  = new (::rFHOG_)
      OperatingGroup( G4000);
      AlarmOperand::AddOperand(pG4000);

   OperatingGroup* pG4500  = new (::rFHOG_)
      OperatingGroup( G4500);
      AlarmOperand::AddOperand(pG4500);

   OperatingGroup* pG4501  = new (::rFHOG_)
      OperatingGroup( G4501);
      AlarmOperand::AddOperand(pG4501);

   OperatingGroup* pG4502  = new (::rFHOG_)
      OperatingGroup( G4502);
      AlarmOperand::AddOperand(pG4502);

   OperatingGroup* pG4503  = new (::rFHOG_)
      OperatingGroup( G4503);
      AlarmOperand::AddOperand(pG4503);

   OperatingGroup* pG5000  = new (::rFHOG_)
      OperatingGroup( G5000);
      AlarmOperand::AddOperand(pG5000);

   OperatingGroup* pG5001  = new (::rFHOG_)
      OperatingGroup( G5001);
      AlarmOperand::AddOperand(pG5001);

   OperatingGroup* pG5500  = new (::rFHOG_)
      OperatingGroup( G5500);
      AlarmOperand::AddOperand(pG5500);

   OperatingGroup* pG5501  = new (::rFHOG_)
      OperatingGroup( G5501);
      AlarmOperand::AddOperand(pG5501);

   OperatingGroup* pG5502  = new (::rFHOG_)
      OperatingGroup( G5502);
      AlarmOperand::AddOperand(pG5502);

   OperatingGroup* pG5503  = new (::rFHOG_)
      OperatingGroup( G5503);
      AlarmOperand::AddOperand(pG5503);

   OperatingGroup* pG6000  = new (::rFHOG_)
      OperatingGroup( G6000);
      AlarmOperand::AddOperand(pG6000);

   OperatingGroup* pG6001  = new (::rFHOG_)
      OperatingGroup( G6001);
      AlarmOperand::AddOperand(pG6001);

   OperatingGroup* pG6500  = new (::rFHOG_)
      OperatingGroup( G6500);
      AlarmOperand::AddOperand(pG6500);

   OperatingGroup* pG6501  = new (::rFHOG_)
      OperatingGroup( G6501);
      AlarmOperand::AddOperand(pG6501);

   OperatingGroup* pG6502  = new (::rFHOG_)
      OperatingGroup( G6502);
      AlarmOperand::AddOperand(pG6502);

   OperatingGroup* pG7500  = new (::rFHOG_)
      OperatingGroup( G7500);
      AlarmOperand::AddOperand(pG7500);

   OperatingGroup* pG8000  = new (::rFHOG_)
      OperatingGroup( G8000);
      AlarmOperand::AddOperand(pG8000);

   OperatingGroup* pG8500  = new (::rFHOG_)
      OperatingGroup( G8500);
      AlarmOperand::AddOperand(pG8500);

   OperatingGroup* pG11000 = new (::rFHOG_)
      OperatingGroup( G11000);
      AlarmOperand::AddOperand(pG11000);

   OperatingGroup* pG20500 = new (::rFHOG_)
      OperatingGroup( G20500);
      AlarmOperand::AddOperand(pG20500);

   OperatingGroup* pG20505 = new (::rFHOG_)
      OperatingGroup( G20505);
      AlarmOperand::AddOperand(pG20505);

   OperatingGroup* pG20508 = new (::rFHOG_)
      OperatingGroup( G20508);
      AlarmOperand::AddOperand(pG20508);

   OperatingGroup* pG21500 = new (::rFHOG_)
      OperatingGroup( G21500);
      AlarmOperand::AddOperand(pG21500);

   OperatingGroup* pG22000 = new (::rFHOG_)
      OperatingGroup( G22000);
      AlarmOperand::AddOperand(pG22000);

   OperatingGroup* pG22500 = new (::rFHOG_)
      OperatingGroup( G22500);
      AlarmOperand::AddOperand(pG22500);

   OperatingGroup* pG22560 = new (::rFHOG_)
      OperatingGroup( G22560);
      AlarmOperand::AddOperand(pG22560);

   OperatingGroup* pG22550 = new (::rFHOG_)
      OperatingGroup( G22550);
      AlarmOperand::AddOperand(pG22550);

   OperatingGroup* pG22551 = new (::rFHOG_)
      OperatingGroup( G22551);
      AlarmOperand::AddOperand(pG22551);

   OperatingGroup* pG22552 = new (::rFHOG_)
      OperatingGroup( G22552);
      AlarmOperand::AddOperand(pG22552);

   OperatingGroup* pG22530 = new (::rFHOG_)
      OperatingGroup( G22530);
      AlarmOperand::AddOperand(pG22530);

   OperatingGroup* pG22531 = new (::rFHOG_)
      OperatingGroup( G22531);
      AlarmOperand::AddOperand(pG22531);

   OperatingGroup* pG22532 = new (::rFHOG_)
      OperatingGroup( G22532);
      AlarmOperand::AddOperand(pG22532);

   OperatingGroup* pG22533 = new (::rFHOG_)
      OperatingGroup( G22533);
      AlarmOperand::AddOperand(pG22533);

   OperatingGroup* pG22534 = new (::rFHOG_)
      OperatingGroup( G22534);
      AlarmOperand::AddOperand(pG22534);

   OperatingGroup* pG22535 = new (::rFHOG_)
      OperatingGroup( G22535);
      AlarmOperand::AddOperand(pG22535);

   OperatingGroup* pG22536 = new (::rFHOG_)
      OperatingGroup( G22536);
      AlarmOperand::AddOperand(pG22536);

   OperatingGroup* pG22537 = new (::rFHOG_)
      OperatingGroup( G22537);
      AlarmOperand::AddOperand(pG22537);

   OperatingGroup* pG22538 = new (::rFHOG_)
      OperatingGroup( G22538);
      AlarmOperand::AddOperand(pG22538);

   OperatingGroup* pG22539 = new (::rFHOG_)
      OperatingGroup( G22539);
      AlarmOperand::AddOperand(pG22539);

   OperatingGroup* pG22540 = new (::rFHOG_)
      OperatingGroup( G22540);
      AlarmOperand::AddOperand(pG22540);

   OperatingGroup* pG22520 = new (::rFHOG_)
      OperatingGroup( G22520);
      AlarmOperand::AddOperand(pG22520);

   OperatingGroup* pG22510 = new (::rFHOG_)
      OperatingGroup( G22510);
      AlarmOperand::AddOperand(pG22510);

   OperatingGroup* pG23000 = new (::rFHOG_)
      OperatingGroup( G23000);
      AlarmOperand::AddOperand(pG23000);

   OperatingGroup* pG24002 = new (::rFHOG_)
      OperatingGroup( G24002);
      AlarmOperand::AddOperand(pG24002);

   OperatingGroup* pG24100 = new (::rFHOG_)
      OperatingGroup( G24100);
      AlarmOperand::AddOperand(pG24100);

   OperatingGroup* pG24300 = new (::rFHOG_)
      OperatingGroup( G24300);
      AlarmOperand::AddOperand(pG24300);

   OperatingGroup* pG25500 = new (::rFHOG_)
      OperatingGroup( G25500);
      AlarmOperand::AddOperand(pG25500);

   OperatingGroup* pG26500 = new (::rFHOG_)
      OperatingGroup( G26500);
      AlarmOperand::AddOperand(pG26500);

   OperatingGroup* pG27000 = new (::rFHOG_)
      OperatingGroup( G27000);
      AlarmOperand::AddOperand(pG27000);

   OperatingGroup* pG28001 = new (::rFHOG_)
      OperatingGroup( G28001);
      AlarmOperand::AddOperand(pG28001);

   OperatingGroup* pG28002 = new (::rFHOG_)
      OperatingGroup( G28002);
      AlarmOperand::AddOperand(pG28002);

   OperatingGroup* pG28003 = new (::rFHOG_)
      OperatingGroup( G28003);
      AlarmOperand::AddOperand(pG28003);

   OperatingGroup* pG28004 = new (::rFHOG_)
      OperatingGroup( G28004);
      AlarmOperand::AddOperand(pG28004);

   OperatingGroup* pG28005 = new (::rFHOG_)
      OperatingGroup( G28005);
      AlarmOperand::AddOperand(pG28005);

   OperatingGroup* pG28501 = new (::rFHOG_)
      OperatingGroup( G28501);
      AlarmOperand::AddOperand(pG28501);

   OperatingGroup* pG28502 = new (::rFHOG_)
      OperatingGroup( G28502);
      AlarmOperand::AddOperand(pG28502);

   OperatingGroup* pG29000 = new (::rFHOG_)
      OperatingGroup( G29000);
      AlarmOperand::AddOperand(pG29000);

   OperatingGroup* pG29002 = new (::rFHOG_)
      OperatingGroup( G29002);
      AlarmOperand::AddOperand(pG29002);

   OperatingGroup* pG29003 = new (::rFHOG_)
      OperatingGroup( G29003);
      AlarmOperand::AddOperand(pG29003);

   OperatingGroup* pG29500 = new (::rFHOG_)
      OperatingGroup( G29500);
      AlarmOperand::AddOperand(pG29500);

   OperatingGroup* pG30500 = new (::rFHOG_)
      OperatingGroup( G30500);
      AlarmOperand::AddOperand(pG30500);

   OperatingGroup* pG31000 = new (::rFHOG_)
      OperatingGroup( G31000);
      AlarmOperand::AddOperand(pG31000);

   OperatingGroup* pG31500 = new (::rFHOG_)
      OperatingGroup( G31500);
      AlarmOperand::AddOperand(pG31500);

   OperatingGroup* pG32000 = new (::rFHOG_)
      OperatingGroup( G32000);
      AlarmOperand::AddOperand(pG32000);

   OperatingGroup* pG32001 = new (::rFHOG_)
      OperatingGroup( G32001);
      AlarmOperand::AddOperand(pG32001);

   OperatingGroup* pG32002 = new (::rFHOG_)
      OperatingGroup( G32002);
      AlarmOperand::AddOperand(pG32002);

   OperatingGroup* pG32003 = new (::rFHOG_)
      OperatingGroup( G32003);
      AlarmOperand::AddOperand(pG32003);

   OperatingGroup* pG32004 = new (::rFHOG_)
      OperatingGroup( G32004);
      AlarmOperand::AddOperand(pG32004);

   OperatingGroup* pG32005 = new (::rFHOG_)
      OperatingGroup( G32005);
      AlarmOperand::AddOperand(pG32005);

   OperatingGroup* pG33500 = new (::rFHOG_)
      OperatingGroup( G33500);
      AlarmOperand::AddOperand(pG33500);

   OperatingGroup* pG34500 = new (::rFHOG_)
      OperatingGroup( G34500);
      AlarmOperand::AddOperand(pG34500);

   OperatingGroup* pG35500 = new (::rFHOG_)
      OperatingGroup( G35500);
      AlarmOperand::AddOperand(pG35500);

   OperatingGroup* pG36000 = new (::rFHOG_)
      OperatingGroup( G36000);
      AlarmOperand::AddOperand(pG36000);

   OperatingGroup* pG36500 = new (::rFHOG_)
      OperatingGroup( G36500);
      AlarmOperand::AddOperand(pG36500);

   OperatingGroup* pG36600 = new (::rFHOG_)
      OperatingGroup( G36600);
      AlarmOperand::AddOperand(pG36600);

   OperatingGroup* pG38000 = new (::rFHOG_)
      OperatingGroup( G38000);
      AlarmOperand::AddOperand(pG38000);

   OperatingGroup* pG38500 = new (::rFHOG_)
      OperatingGroup( G38500);
      AlarmOperand::AddOperand(pG38500);
  
   OperatingGroup* pG39000 = new (::rFHOG_)
      OperatingGroup( G39000);
      AlarmOperand::AddOperand(pG39000);

   OperatingGroup* pG40000 = new (::rFHOG_) 
	 OperatingGroup( G40000);
   AlarmOperand::AddOperand(pG40000);
      
   OperatingGroup* pG100000 = new (::rFHOG_)
      OperatingGroup( G100000);
      AlarmOperand::AddOperand(pG100000);

   //======================================================
   //   ALARM DEFINITIONS
   //======================================================





   //======================================================
   //
   //   A5:   Technical Malfunction (BTAT1) 
   //   $[05048]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA5Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 10, NO_VENTILATION_MSG, 
      PROVIDE_OTHER_VENTILATION_MSG, AlarmResponses::VILedOnPatDataOff,
      AlarmResponses::VILedOffPatDataOn);


   Alarm* pA5 = new (::rFHA_)             // Define the alarm
      Alarm(A5, *pA5Info, *pG500,  OPERAND_NAME_NULL, FALSE,
            DEVICE_ALERT_MSG, TRUE);

   Alarm::AddAlarm(pA5);                  // Add the alarm



   // Create the operating component for this alarm 
   OperatingComponent* pKG500_C30 = new (::rFHOK_)
      OperatingComponent(*pC30, OperatingComponent::OR, FALSE);

   
   pC30->addParent(*pG500);               // Assign a parent group to the condition

   pG500->addChild(*pKG500_C30);          // Link the operating group to the parent


   pG500->setCausedAlarm(pA5);            // Set the caused state




   //======================================================
   //
   //   A10:   Technical Malfunction (BTAT2)
   //   $[05049]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA10Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 290, 
      VENTILATION_CONTINUES_BD_SPIRO_MSG, REPLACE_VENTILATOR_SERVICE_MSG, 
      AlarmResponses::BdAlarmOnGuiSetOff,
      AlarmResponses::BdAlarmOffGuiSetOn);

   Alarm* pA10 = new (::rFHA_)             // Define the alarm
      Alarm(A10, *pA10Info, *pG1000,
             OPERAND_NAME_NULL,  
            FALSE, DEVICE_ALERT_MSG, TRUE);

   Alarm::AddAlarm(pA10);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG1000_C35 = new (::rFHOK_)
      OperatingComponent(*pC35, OperatingComponent::OR, FALSE);

   
   pC35->addParent(*pG1000);               // Assign a parent group to the condition

   pG1000->addChild(*pKG1000_C35);         // Link the operating group to the parent


   pG1000->setCausedAlarm(pA10);           // Set the caused state


   //======================================================
   // 
   //   A15:   Technical Malfunction (BTAT3)
   //   $[05050]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA15Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 250, BD_NOT_AFFECTED_MSG,
      SERVICE_REQUIRED_MSG);

   Alarm* pA15 = new (::rFHA_)               // Define the alarm
      Alarm(A15, *pA15Info, *pG1500,  OPERAND_NAME_NULL,  
            FALSE, DEVICE_ALERT_MSG, TRUE);

   Alarm::AddAlarm(pA15);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG1500_C40 = new (::rFHOK_)
      OperatingComponent(*pC40, OperatingComponent::OR, FALSE);

   
   pC40->addParent(*pG1500);               // Assign a parent group to the condition

   pG1500->addChild(*pKG1500_C40);         // Link the operating group to the parent


   pG1500->setCausedAlarm(pA15);           // Set the caused state


   //======================================================
   //
   //   A20:   Technical Malfunction (BTAT4)
   //   $[05051]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA20Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 320, VENTILATION_CONTINUES_MSG, 
      REPLACE_VENTILATOR_SERVICE_MSG, AlarmResponses::LOILedOnBdAlarmOn,
      AlarmResponses::LOILedOffBdAlarmOff);

   Alarm* pA20 = new (::rFHA_)             // Define the alarm
      Alarm(A20, *pA20Info, *pG2000,  OPERAND_NAME_NULL, 
            FALSE, DEVICE_ALERT_MSG, TRUE);

   Alarm::AddAlarm(pA20);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG2000_C45 = new (::rFHOK_)
      OperatingComponent(*pC45, OperatingComponent::OR, FALSE);

   
   pC45->addParent(*pG2000);               // Assign a parent group to the condition

   pG2000->addChild(*pKG2000_C45);         // Link the operating group to the parent
                                           

   pG2000->setCausedAlarm(pA20);           // Set the caused state


   //======================================================
   //
   //   A25:   Technical Malfunction (BTAT5)
   //   $[05052]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA25Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 95, AC_LOSS_DEVICE_ALERT_MSG, 
            CHECK_ALARM_LOG_MSG);

   Alarm* pA25 = new (::rFHA_)             // Define the alarm
      Alarm(A25, *pA25Info, *pG2500,  OPERAND_NAME_NULL, 
            FALSE, DEVICE_ALERT_MSG, TRUE);

   Alarm::AddAlarm(pA25);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG2500_C50 = new (::rFHOK_)
      OperatingComponent(*pC50, OperatingComponent::OR, FALSE);

   
   pC50->addParent(*pG2500);               // Assign a parent group to the condition

   pG2500->addChild(*pKG2500_C50);         // Link the operating group to the parent


   pG2500->setCausedAlarm(pA25);           // Set the caused state


   //======================================================
   //
   //   A30:   Technical Malfunction (BTAT6)
   //   $[05053]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA30Info = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 150, SCREEN_BLOCK_ANALYSIS_MSG, 
      SCREEN_BLOCK_REMEDY_MSG);

   Alarm* pA30 = new (::rFHA_)             // Define the alarm
      Alarm(A30, *pA30Info, *pG3000,  OPERAND_NAME_NULL,
            FALSE, SCREEN_BLOCK_MSG, TRUE);

   Alarm::AddAlarm(pA30);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG3000_C55 = new (::rFHOK_)
      OperatingComponent(*pC55, OperatingComponent::OR, FALSE);

   
   pC55->addParent(*pG3000);               // Assign a parent group to the condition

   pG3000->addChild(*pKG3000_C55);         // Link the operating group to the parent


   pG3000->setCausedAlarm(pA30);           // Set the caused state



   //======================================================
   //
   //   A35:   Technical Malfunction (BTAT7)
   //   $[05054]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA35Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 330, NO_BD_STATUS_MSG, 
      CHECK_PATIENT_REPLACE_VENT_MSG, AlarmResponses::GuiSetOffPatDataOff,
      AlarmResponses::GuiSetOnPatDataOn);

   Alarm* pA35 = new (::rFHA_)             // Define the alarm
      Alarm(A35, *pA35Info, *pG3500,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);

   Alarm::AddAlarm(pA35);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG3500_C60 = new (::rFHOK_)
      OperatingComponent(*pC60, OperatingComponent::OR, FALSE);

   
   pC60->addParent(*pG3500);               // Assign a parent group to the condition

   pG3500->addChild(*pKG3500_C60);         // Link the operating group to the parent


   pG3500->setCausedAlarm(pA35);           // Set the caused state


   //======================================================
   //
   //   A40:   Technical Malfunction (BTAT8)
   //   $[05055]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA40Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 155, VENTILATION_UNAFFECTED_MSG, 
      O2_SENSOR_OOR_MSG);

   Alarm* pA40 = new (::rFHA_)             // Define the alarm
      Alarm(A40, *pA40Info, *pG4000,  OPERAND_NAME_NULL,
            FALSE, O2_SENSOR_MSG, TRUE);

   Alarm::AddAlarm(pA40);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG4000_C65 = new (::rFHOK_)
      OperatingComponent(*pC65, OperatingComponent::OR, FALSE);

   
   pC65->addParent(*pG4000);               // Assign a parent group to the condition

   pG4000->addChild(*pKG4000_C65);         // Link the operating group to the parent


   pG4000->setCausedAlarm(pA40);           // Set the caused state



   //====================================================================
   //
   //   A285:  No O2 Supply.  This is out of order because the group G28500 
   //          needs to be defined before it is referenced.
   //   $[05157] $[05158] $[05159] $[05160]
   //
   //====================================================================



   // !!!!!!!!! NOTE !!!!!!!!!
   // Because of definition ordering constraints, these groups for the
   // No Air Supply alarm are defined here.
   // Define the Temporal constraints and VHM managers
   TemporalConstraint* pG28000Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::EQ, Constraint::EQ, NULL, NULL);

   TemporalConstraint* pG28000ResetConstraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ,
                         CalcFunctions::Return5Sec, NULL);  // >= 5 sec

   // Create Constraint operating groups 
   OperatingGroup* pG28000 = new (::rFHOG_)
      OperatingGroup( G28000, pG28000Constraint, pG28000ResetConstraint);
      AlarmOperand::AddOperand(pG28000);
   // End of definitions for No Air Supply alarm.
   // !!!!!!!!! NOTE !!!!!!!!!



   // Create the alarm information for this alarm
   AlarmInformation* pA285Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 150, MESSAGE_NAME_NULL, 
         MESSAGE_NAME_NULL);

   AlarmInformation* pA285Info1 = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    150, VENTILATION_WITH_21_O2_MSG,   
         NO_O2_SUPPLY_REMEDY_MSG);
   AlarmInformation* pA285Info2 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,   100, VENTILATION_EXCEPT_21_O2_MSG, 
         CHECK_PATIENT_O2_SOURCE_MSG);
   AlarmInformation* pA285Info3 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,    50, NO_VENTILATION_MSG, 
         NO_VENT_CHECK_GAS_MSG, AlarmResponses::PatDataOff,
         AlarmResponses::PatDataOn);



   // Define the Temporal constraints and VHM managers
   TemporalConstraint* pG28500Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::EQ, Constraint::EQ, NULL, NULL);

   TemporalConstraint* pG28500ResetConstraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ,
                         CalcFunctions::Return5Sec, NULL);  // >= 5 sec

   // Create Constraint operating groups 
   OperatingGroup* pG28500 = new (::rFHOG_)
      OperatingGroup( G28500, pG28500Constraint, pG28500ResetConstraint);
      AlarmOperand::AddOperand(pG28500);

   // Create Violation History Managers
   VHMTemporal*  pG28503Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create VHM operating group
   OperatingGroup* pG28503 = new (::rFHOG_)
      OperatingGroup( G28503,NULL,NULL,pG28503Vhm);
      AlarmOperand::AddOperand(pG28503);

   //==========================================================================
   // Define the alarm
   //==========================================================================
   Alarm* pA285 = new (::rFHA_)
      Alarm(A285, *pA285Info, *pG28500,  OPERAND_NAME_NULL,
            FALSE, NO_O2_SUPPLY_MSG, TRUE);


   Alarm::AddAlarm(pA285);                  // Add the alarm



   //==========================================================================
   // Create the operating components for this alarm.  
   // This is sorted by the operating components in groups 28500-28507.
   //==========================================================================
   OperatingComponent* pKG28500_G28503 = new (::rFHOK_)
      OperatingComponent(*pG28503, OperatingComponent::OR, FALSE);    // G28500


   OperatingComponent* pKG28501_C215   = new (::rFHOK_)
      OperatingComponent(*pC215,   OperatingComponent::OR, FALSE);      // G28501

   OperatingComponent* pKG28501_G28000 = new (::rFHOK_)
      OperatingComponent(*pG28000, OperatingComponent::AND, TRUE);    // G28501

   OperatingComponent* pKG28502_C220   = new (::rFHOK_)
      OperatingComponent(*pC220,   OperatingComponent::OR, FALSE);      // G28502

   OperatingComponent* pKG28502_G28000 = new (::rFHOK_)
      OperatingComponent(*pG28000, OperatingComponent::AND, TRUE);    // G28502

   OperatingComponent* pKG28503_C200 = new (::rFHOK_)
      OperatingComponent(*pC200, OperatingComponent::OR, FALSE);    // G28503

   // Connect the operating components to the parent/child relationships.

   pG28500->addChild(*pKG28500_G28503);      // G28500               
   pG28501->addChild(*pKG28501_C215);        // G28501
   pG28501->addChild(*pKG28501_G28000);      // G28501
   pG28502->addChild(*pKG28502_C220);        // G28502
   pG28502->addChild(*pKG28502_G28000);      // G28502
   pG28503->addChild(*pKG28503_C200);        // G28503               

   pG28503->addParent(*pG28500);
   pC215  ->addParent(*pG28501);
   pG28000->addParent(*pG28501);
   pC220  ->addParent(*pG28502);
   pG28000->addParent(*pG28502);
   pC200  ->addParent(*pG28503);


   // Set the caused state for this alarm
   pG28500->setCausedAlarm(pA285);

   // Set the condition augmentations for this alarm
   ConditionAugmentation* pA285_G28501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA285Info1,  *pA285, *pG28501);
   ConditionAugmentation* pA285_G28502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA285Info2,  *pA285, *pG28502);
   ConditionAugmentation* pA285_G28000Aug = new (::rFHCA_)
      ConditionAugmentation(*pA285Info3,  *pA285, *pG28000);

   pA285->addAugmentation(*pA285_G28501Aug); // Add the augmented state
   pG28501->addAugmentation(*pA285_G28501Aug); 

   pA285->addAugmentation(*pA285_G28502Aug);  // Add the augmented state
   pG28502->addAugmentation(*pA285_G28502Aug);  

   pA285->addAugmentation(*pA285_G28000Aug);  // Add the augmented state
   pG28000->addAugmentation(*pA285_G28000Aug);  



   //======================================================
   //
   //   A45:   Technical Malfunction (BTAT9)
   //   $[05056] $[05057] $[05058] $[05059]
   //
   //======================================================
 
   // Create the alarm information for this alarm
   AlarmInformation* pA45Info  = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 140, MESSAGE_NAME_NULL,  
         MESSAGE_NAME_NULL);

   AlarmInformation* pA45Info1 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 30,  NO_VENTILATION_MSG, 
         PROVIDE_OTHER_VENTILATION_MSG, AlarmResponses::VILedOnPatDataOff,
         AlarmResponses::VILedOffPatDataOn);
   AlarmInformation* pA45Info2 = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 140, VENTILATION_WITH_100_O2_MSG,  
         REPLACE_VENTILATOR_SERVICE_MSG);
   AlarmInformation* pA45Info3 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 250, 
         VENTILATION_EXCEPT_100_O2_MSG, CHECK_PATIENT_REPLACE_VENT_MSG);


   Alarm* pA45 = new (::rFHA_)             // Define the alarm
      Alarm(A45, *pA45Info, *pG4500,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);


   Alarm::AddAlarm(pA45);                  // Add the alarm



   // Define the operating components for this alarm 

   OperatingComponent* pKG4500_C70 = new (::rFHOK_)            // G4500
      OperatingComponent(*pC70,     OperatingComponent::OR, FALSE);

   OperatingComponent* pKG4501_G28500 = new (::rFHOK_)         // G4503
      OperatingComponent(*pG28500, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG4501_G5500 = new (::rFHOK_)         // G4503
      OperatingComponent(*pG5500, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG4502_G4501 = new (::rFHOK_)         // G4501
      OperatingComponent(*pG4501,  OperatingComponent::OR,  TRUE);

   OperatingComponent* pKG4502_C210 = new (::rFHOK_)           // G4501
      OperatingComponent(*pC210,   OperatingComponent::AND, FALSE);

   OperatingComponent* pKG4503_G4501 = new (::rFHOK_)         // G4502
      OperatingComponent(*pG4501,  OperatingComponent::OR,  TRUE);

   OperatingComponent* pKG4503_C225 = new (::rFHOK_)           // G4502
      OperatingComponent(*pC225,   OperatingComponent::AND, FALSE);



   // Connect the operating components to the parent/child relationships

   pG4500->addChild(*pKG4500_C70);
   
   pG4501->addChild(*pKG4501_G28500);
   pG4501->addChild(*pKG4501_G5500);

   pG4502->addChild(*pKG4502_G4501);
   pG4502->addChild(*pKG4502_C210);

   pG4503->addChild(*pKG4503_G4501);
   pG4503->addChild(*pKG4503_C225);

   pC70   ->addParent(*pG4500);

   pG28500->addParent(*pG4501);
   pG5500 ->addParent(*pG4501);

   pC210  ->addParent(*pG4502);
   pG4501 ->addParent(*pG4502);

   pC225  ->addParent(*pG4503);
   pG4501 ->addParent(*pG4503);

   // Create the condition augmentations for this alarm

   ConditionAugmentation* pA45_G4501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA45Info1, *pA45, *pG4501);
   ConditionAugmentation* pA45_G4502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA45Info2, *pA45, *pG4502);
   ConditionAugmentation* pA45_G4503Aug = new (::rFHCA_)
      ConditionAugmentation(*pA45Info3, *pA45, *pG4503);


   pG4500->setCausedAlarm(pA45);           // Set the caused state


   pA45->addAugmentation(*pA45_G4501Aug); // Add the augmented state
   pG4501->addAugmentation(*pA45_G4501Aug); 

   pA45->addAugmentation(*pA45_G4502Aug);  // Add the augmented state
   pG4502->addAugmentation(*pA45_G4502Aug);  

   pA45->addAugmentation(*pA45_G4503Aug);  // Add the augmented state
   pG4503->addAugmentation(*pA45_G4503Aug);  




   //======================================================
   //
   //   A50:   Technical Malfunction (BTAT10)
   //   $[05060] $[05061]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA50Info  = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 190, VENTILATION_CONTINUES_MSG, 
         REPLACE_VENTILATOR_SERVICE_MSG);
   AlarmInformation* pA50Info1 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 130, COMPROMISED_AIR_DELIVERY_MSG, 
         CHECK_PATIENT_MSG);


   Alarm* pA50 = new (::rFHA_)             // Define the alarm
      Alarm(A50, *pA50Info, *pG5000,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);

   Alarm::AddAlarm(pA50);                  // Add the alarm


   // Define the operating components for this alarm 

   OperatingComponent* pKG5000_C75 = new (::rFHOK_)            // G5000
      OperatingComponent(*pC75, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG5001_C225 = new (::rFHOK_)           // G5001
      OperatingComponent(*pC225, OperatingComponent::OR, FALSE);



   // Connect the operating components to the parent/child relationships

   pG5000->addChild(*pKG5000_C75);
   pC75  ->addParent(*pG5000);

   pG5001->addChild(*pKG5001_C225);
   pC225 ->addParent(*pG5001);


   // Create the condition augmentations for this alarm

   ConditionAugmentation* pA50_G5001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA50Info1, *pA50, *pG5001);


   pG5000->setCausedAlarm(pA50);           // Set the caused state

   pA50->addAugmentation(*pA50_G5001Aug);  // Add the augmented state
   pG5001->addAugmentation(*pA50_G5001Aug);




   //======================================================
   //
   //   A55:   Technical Malfunction (BTAT11)
   //   $[05062] $[05063] $[05064] $[05065]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA55Info  = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 120, MESSAGE_NAME_NULL,   
            MESSAGE_NAME_NULL);

   Alarm* pA55 = new (::rFHA_)             // Define the alarm
      Alarm(A55, *pA55Info, *pG5500,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);


   Alarm::AddAlarm(pA55);                  // Add the alarm


   AlarmInformation* pA55Info1 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 20,  NO_VENTILATION_MSG,  
         CHECK_PATIENT_REPLACE_VENT_MSG, AlarmResponses::VILedOnPatDataOff,
         AlarmResponses::VILedOffPatDataOn);
   AlarmInformation* pA55Info2 = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 120, VENTILATION_WITH_21_O2_MSG,  
            REPLACE_VENTILATOR_SERVICE_MSG);
   AlarmInformation* pA55Info3 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 90, 
            VENTILATION_EXCEPT_21_O2_MSG, CHECK_PATIENT_REPLACE_VENT_MSG);


   // Define the operating components for this alarm 

   OperatingComponent* pKG5500_C80 = new (::rFHOK_)            // G5500
      OperatingComponent(*pC80, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG5501_G28000 = new (::rFHOK_)         // G5503
      OperatingComponent(*pG28000, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG5501_G4500 = new (::rFHOK_)         // G5503
      OperatingComponent(*pG4500, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG5502_G5501 = new (::rFHOK_)         // G5501
      OperatingComponent(*pG5501, OperatingComponent::OR, TRUE);

   OperatingComponent* pKG5502_C215 = new (::rFHOK_)           // G5501
      OperatingComponent(*pC215, OperatingComponent::AND, FALSE);

   OperatingComponent* pKG5503_G5501 = new (::rFHOK_)         // G5502
      OperatingComponent(*pG5501, OperatingComponent::OR, TRUE);

   OperatingComponent* pKG5503_C220 = new (::rFHOK_)           // G5502
      OperatingComponent(*pC220, OperatingComponent::AND, FALSE);



   // Connect the operating components to the parent/child relationships

   pG5500->addChild(*pKG5500_C80);
   
   pG5501->addChild(*pKG5501_G28000);
   pG5501->addChild(*pKG5501_G4500);

   pG5502->addChild(*pKG5502_G5501);
   pG5502->addChild(*pKG5502_C215);

   pG5503->addChild(*pKG5503_G5501);
   pG5503->addChild(*pKG5503_C220);

   pC80   ->addParent(*pG5500);

   pG28000->addParent(*pG5501);
   pG4500 ->addParent(*pG5501);

   pG5501 ->addParent(*pG5502);
   pC215  ->addParent(*pG5502);

   pG5501 ->addParent(*pG5503);
   pC220  ->addParent(*pG5503);



   // Create the condition augmentations for this alarm

   ConditionAugmentation* pA55_G5501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA55Info1, *pA55, *pG5501);

   ConditionAugmentation* pA55_G5502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA55Info2, *pA55, *pG5502);

   ConditionAugmentation* pA55_G5503Aug = new (::rFHCA_)
      ConditionAugmentation(*pA55Info3, *pA55, *pG5503);



   pG5500->setCausedAlarm(pA55);           // Set the caused state


   pA55->addAugmentation(*pA55_G5501Aug); // Add the augmented state
   pG5501->addAugmentation(*pA55_G5501Aug); 

   pA55->addAugmentation(*pA55_G5502Aug);  // Add the augmented state
   pG5502->addAugmentation(*pA55_G5502Aug); 

   pA55->addAugmentation(*pA55_G5503Aug);  // Add the augmented state
   pG5503->addAugmentation(*pA55_G5503Aug);





   //======================================================
   //
   //   A60:   Technical Malfunction (BTAT12)
   //   $[05066] $[05067]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA60Info  = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 180, VENTILATION_CONTINUES_MSG, 
         REPLACE_VENTILATOR_SERVICE_MSG);
   AlarmInformation* pA60Info1 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 110, COMPROMISED_O2_DELIVERY_MSG, 
         CHECK_PATIENT_MSG);


   Alarm* pA60 = new (::rFHA_)             // Define the alarm
      Alarm(A60, *pA60Info, *pG6000,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);

   Alarm::AddAlarm(pA60);                  // Add the alarm




   // Define the operating components for this alarm 

   OperatingComponent* pKG6000_C85 = new (::rFHOK_)            // G6000
      OperatingComponent(*pC85, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG6001_C220 = new (::rFHOK_)           // G6001
      OperatingComponent(*pC220, OperatingComponent::OR, FALSE);



   // Connect the operating components to the parent/child relationships

   pG6000->addChild(*pKG6000_C85);
   pC85  ->addParent(*pG6000);

   pG6001->addChild(*pKG6001_C220);
   pC220 ->addParent(*pG6001);


   // Create the condition augmentations for this alarm

   ConditionAugmentation* pA60_G6001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA60Info1, *pA60, *pG6001);


   pG6000->setCausedAlarm(pA60);           // Set the caused state

   pA60->addAugmentation(*pA60_G6001Aug);  // Add the augmented state
   pG6001->addAugmentation(*pA60_G6001Aug);  



   //======================================================
   //
   //   A65:   Technical Malfunction (BTAT13)
   //   $[05068] $[05069] $[05070]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA65Info = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 130, MESSAGE_NAME_NULL, 
         CHECK_PATIENT_REPLACE_VENT_MSG);
   AlarmInformation* pA65Info1 = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 130, BD_OK_BAD_SPIRO_MSG, 
         MESSAGE_NAME_NULL);
   AlarmInformation* pA65Info2 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,   280, 
      BD_OK_BAD_SPIRO_PRESS_TRIG_MSG, MESSAGE_NAME_NULL);


   Alarm* pA65 = new (::rFHA_)             // Define the alarm
      Alarm(A65, *pA65Info, *pG6500,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);
   
   Alarm::AddAlarm(pA65);                  // Add the alarm
   
   

   // Define the operating components for this alarm 

   OperatingComponent* pKG6500_C90 = new (::rFHOK_)            // G6500
      OperatingComponent(*pC90, OperatingComponent::OR, FALSE);
   
   OperatingComponent* pKG6501_C170 = new (::rFHOK_)           // G6501
      OperatingComponent(*pC170, OperatingComponent::OR, TRUE);
   
   OperatingComponent* pKG6502_C170 = new (::rFHOK_)           // G6502
      OperatingComponent(*pC170, OperatingComponent::OR, FALSE);

   
   
   // Connect the operating components to the parent/child relationships
   
   pG6500->addChild(*pKG6500_C90);
   pG6501->addChild(*pKG6501_C170);
   pG6502->addChild(*pKG6502_C170);

   pC90  ->addParent(*pG6500);
   pC170 ->addParent(*pG6501);
   pC170 ->addParent(*pG6502);



   // Create the condition augmentations for this alarm

   ConditionAugmentation* pA65_G6501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA65Info1, *pA65, *pG6501);
   ConditionAugmentation* pA65_G6502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA65Info2, *pA65, *pG6502);


   pG6500->setCausedAlarm(pA65);           // Set the caused state


   pA65->addAugmentation(*pA65_G6501Aug);  // Add the augmented state
   pG6501->addAugmentation(*pA65_G6501Aug);

   pA65->addAugmentation(*pA65_G6502Aug);  // Add the augmented state
   pG6502->addAugmentation(*pA65_G6502Aug);





   //======================================================
   //
   //   A70:   Technical Malfunction (BTAT14)
   //   $[05071] $[05072]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA70Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 230,    BD_OK_BAD_SPIRO_MSG, 
         REPLACE_VENTILATOR_SERVICE_MSG);
   AlarmInformation* pA70Info1 = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 160, MESSAGE_NAME_NULL,  
         MESSAGE_NAME_NULL);


   // Define the Temporal constraint.
   TemporalConstraint* pG7001Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GT, Constraint::EQ, 
                         CalcFunctions::Return10Min, NULL);

   // Create a Constraint operating group 
   OperatingGroup* pG7001 = new (::rFHOG_)
      OperatingGroup( G7001,pG7001Constraint);
      AlarmOperand::AddOperand(pG7001);




   // Create a Violation History Manager
   VHMTemporal*  pG7000Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create a VHM operating group 
   OperatingGroup* pG7000 = new (::rFHOG_)
      OperatingGroup( G7000,NULL,NULL,pG7000Vhm);
      AlarmOperand::AddOperand(pG7000);



   // Define the alarm
   Alarm* pA70 = new (::rFHA_)               
      Alarm(A70, *pA70Info, *pG7000,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);

   Alarm::AddAlarm(pA70);                  // Add the alarm



   // Define the operating components for this alarm 

   OperatingComponent* pKG7000_C95 = new (::rFHOK_)            // G7000
      OperatingComponent(*pC95, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG7001_G7000 = new (::rFHOK_)          // G7001
      OperatingComponent(*pG7000, OperatingComponent::OR, FALSE);


   // Connect the operating components to the parent/child relationships
   
   pG7000->addChild(*pKG7000_C95);
   pC95  ->addParent(*pG7000);


   pG7001->addChild(*pKG7001_G7000);
   pG7000->addParent(*pG7001);
   
   


   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA70_G7001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA70Info1, *pA70, *pG7001);


   pG7000->setCausedAlarm(pA70);           // Set the caused state

   pA70->addAugmentation(*pA70_G7001Aug);  // Add the augmented state
   pG7001->addAugmentation(*pA70_G7001Aug);



   //======================================================
   //
   //   A75:   Technical Malfunction (BTAT15)
   //   $[05073]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA75Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 240, VENTILATION_CONTINUES_MSG, 
         REPLACE_VENTILATOR_SERVICE_MSG);

   Alarm* pA75 = new (::rFHA_)             // Define the alarm
      Alarm(A75, *pA75Info, *pG7500,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);


   Alarm::AddAlarm(pA75);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG7500_C100 = new (::rFHOK_)
      OperatingComponent(*pC100, OperatingComponent::OR, FALSE);

   
   pC100->addParent(*pG7500);              // Assign a parent group to the condition

   pG7500->addChild(*pKG7500_C100);        // Link the operating group to the parent


   pG7500->setCausedAlarm(pA75);           // Set the caused state



   //======================================================
   //
   //   A80:   Technical Malfunction (BTAT16)
   //   $[05074]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA80Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 310, VENTILATION_CONTINUES_MSG, 
          REPLACE_VENTILATOR_SERVICE_MSG,
          AlarmResponses::LOILedOnBdAlarmOnGuiSetOffPatDataOff,
          AlarmResponses::LOILedOffBdAlarmOffGuiSetOnPatDataOn);

   Alarm* pA80 = new (::rFHA_)             // Define the alarm
      Alarm(A80, *pA80Info, *pG8000,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);


   Alarm::AddAlarm(pA80);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG8000_C105 = new (::rFHOK_)
      OperatingComponent(*pC105, OperatingComponent::OR, FALSE);

   
   pC105->addParent(*pG8000);              // Assign a parent group to the condition

   pG8000->addChild(*pKG8000_C105);         // Link the operating group to the parent


   pG8000->setCausedAlarm(pA80);            // Set the caused state



   //======================================================
   //
   //   A85:   Technical Malfunction (BTAT17)
   //   $[05075]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA85Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 360, BD_NOT_AFFECTED_MSG, 
          REPLACE_VENTILATOR_SERVICE_MSG,
          AlarmResponses::LOILedOnBdAlarmOnGuiSetOff,
          AlarmResponses::LOILedOffBdAlarmOffGuiSetOn);

   Alarm* pA85 = new (::rFHA_)             // Define the alarm
      Alarm(A85, *pA85Info, *pG8500,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);


   Alarm::AddAlarm(pA85);                  // Add the alarm
   
   
   // Create the operating component for this alarm 
   OperatingComponent* pKG8500_C110 = new (::rFHOK_)
      OperatingComponent(*pC110, OperatingComponent::OR, FALSE);

   
   pC110->addParent(*pG8500);              // Assign a parent group to the condition

   pG8500->addChild(*pKG8500_C110);        // Link the operating group to the parent


   pG8500->setCausedAlarm(pA85);           // Set the caused state



   //======================================================
   //
   //   A110:  Minor Post Fault
   //   $[05076]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA110Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 220, 
      BD_NOT_AFFECTED_OTHER_FUNCTIONS_MSG, SERVICE_REQUIRED_MSG);

   Alarm* pA110 = new (::rFHA_)            // Define the alarm
      Alarm(A110, *pA110Info, *pG11000,  OPERAND_NAME_NULL,
            FALSE, DEVICE_ALERT_MSG, TRUE);


   Alarm::AddAlarm(pA110);                 // Add the alarm
   
   
   // Create the operating component for this alarm 
   OperatingComponent* pKG11000_C190 = new (::rFHOK_)
      OperatingComponent(*pC190, OperatingComponent::OR, FALSE);

   
   pC190->addParent(*pG11000);             // Assign a parent group to the condition

   pG11000->addChild(*pKG11000_C190);      // Link the operating group to the parent


   pG11000->setCausedAlarm(pA110);         // Set the caused state



   //======================================================
   //
   //   A255:  Low Mandatory Tidal Volume.  This alarm is out
   //          of order because other alarms require its
   //          definition before it is referenced.
   //   $[05125] $[05127] $[05128] $[05129] $[05130] $[05131]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA255Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    40, MESSAGE_NAME_NULL, 
         LOW_MAND_TIDAL_VOL_REMEDY_MSG);

   AlarmInformation* pA255LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    40, LOW_MAND_TIDAL_VOL_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA255MediumInfo = new (::rFHAI_)          
      AlarmInformation(Alarm::MEDIUM_URGENCY, 50, LOW_MAND_TIDAL_VOL_MED_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA255HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,  200, LOW_MAND_TIDAL_VOL_HIGH_MSG, 
         MESSAGE_NAME_NULL);

   DependentInformation* pA255DepInfo1  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG,    MESSAGE_NAME_NULL);   
   DependentInformation* pA255DepInfo2  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG,    MESSAGE_NAME_NULL);   


   // Define count constraints.
   // Last 2 or 3 breaths meet detection criteria
   CountConstraint* pG25501Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::LT,
                      CalcFunctions::Return2Events, CalcFunctions::Return4Events); 

   // Last 4 to 9 breaths meet detection criteria
   CountConstraint* pG25502Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::LT, 
                      CalcFunctions::Return4Events, CalcFunctions::Return10Events);

   // Last 10 breaths meet detection criteria
   CountConstraint* pG25503Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ,
                      CalcFunctions::Return10Events, NULL);

   // >= 2 breaths meet detection criteria
   CountConstraint* pG25505Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return2Events, NULL);



   // Create Operating groups for the above constraints
   OperatingGroup* pG25501 = new (::rFHOG_)
      OperatingGroup( G25501,pG25501Constraint);
      AlarmOperand::AddOperand(pG25501);

   OperatingGroup* pG25502 = new (::rFHOG_)
      OperatingGroup( G25502,pG25502Constraint);
      AlarmOperand::AddOperand(pG25502);

   OperatingGroup* pG25503 = new (::rFHOG_)
      OperatingGroup( G25503,pG25503Constraint);
      AlarmOperand::AddOperand(pG25503);

   OperatingGroup* pG25505 = new (::rFHOG_)
      OperatingGroup( G25505,pG25505Constraint);
      AlarmOperand::AddOperand(pG25505);




   // Create a Breath count Violation History Manager, delete breaths > 10
   VHMPrevBreathCountCardinal* pG25504Vhm = new (::rFHVHMPBCC_) 
      VHMPrevBreathCountCardinal(CalcFunctions::Return10Events);

   // Create operating groups for the VHM
   OperatingGroup* pG25504 = new (::rFHOG_)
      OperatingGroup( G25504,NULL, NULL, pG25504Vhm);
      AlarmOperand::AddOperand(pG25504);

   
  

   
   Alarm* pA255 = new (::rFHA_)              // Define the alarm
      Alarm(A255, *pA255Info, *pG25500,  C320, TRUE,
            LOW_MAND_TIDAL_VOL_MSG);
      
   Alarm::AddAlarm(pA255);                   // Add the alarm
   

   OperatingComponent* pKG25500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000,OperatingComponent::AND,  TRUE);

   OperatingComponent* pKG25500_G25505 = new (::rFHOK_)
      OperatingComponent(*pG25505, OperatingComponent::OR, FALSE);
   
   OperatingComponent* pKG25505_G25504 = new (::rFHOK_)
      OperatingComponent(*pG25504, OperatingComponent::OR, FALSE);
   
   OperatingComponent* pKG25501_G25504 = new (::rFHOK_)
      OperatingComponent(*pG25504, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG25502_G25504 = new (::rFHOK_)
      OperatingComponent(*pG25504, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG25503_G25504 = new (::rFHOK_)
      OperatingComponent(*pG25504, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG25504_C320   = new (::rFHOK_)
      OperatingComponent(*pC320,   OperatingComponent::OR, FALSE);   



   // Connect the operating components to the parent/child relationships
   pG25500->addChild(*pKG25500_G25505);
   pG25500->addChild(*pKG25500_G32000);
   pG25505->addChild(*pKG25505_G25504);
   pG25501->addChild(*pKG25501_G25504);
   pG25502->addChild(*pKG25502_G25504);
   pG25503->addChild(*pKG25503_G25504);
   pG25504->addChild(*pKG25504_C320);

   pG32000->addParent(*pG25500);
   pG25505->addParent(*pG25500);
   pG25504->addParent(*pG25505);
   pG25504->addParent(*pG25501);
   pG25504->addParent(*pG25502);
   pG25504->addParent(*pG25503);
   pC320  ->addParent(*pG25504);

   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA255_G25501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA255LowInfo,    *pA255, *pG25501);
   ConditionAugmentation* pA255_G25502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA255MediumInfo, *pA255, *pG25502);
   ConditionAugmentation* pA255_G25503Aug = new (::rFHCA_)
      ConditionAugmentation(*pA255HighInfo,   *pA255, *pG25503);
   

   // Dependent Augmentations for A255_A260 and A255_A235 are defined
   // in A235.


   pG25500->setCausedAlarm(pA255);           // Set the caused state
                                    
   pA255->addAugmentation(*pA255_G25501Aug); // Add the cond augmented state
   pG25501->addAugmentation(*pA255_G25501Aug);

   pA255->addAugmentation(*pA255_G25502Aug); // Add the cond augmented state
   pG25502->addAugmentation(*pA255_G25502Aug);

   pA255->addAugmentation(*pA255_G25503Aug); // Add the cond augmented state
   pG25503->addAugmentation(*pA255_G25503Aug);

   



   //======================================================
   //
   //   A260:  Low Exhaled Minute Volume.  This alarm is out
   //          of order because other alarms require its
   //          definition before it is referenced.
   //   $[05132] $[05134] $[05135] $[05136] $[05137] $[05138] $[05139]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA260Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,     20, MESSAGE_NAME_NULL, 
         CHECK_PATIENT_SETTINGS_MSG);

   AlarmInformation* pA260LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,     20, LOW_MINUTE_VOL_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA260MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY,  20, LOW_MINUTE_VOL_MED_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA260HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,   160, LOW_MINUTE_VOL_HIGH_MSG, 
         MESSAGE_NAME_NULL);


   DependentInformation* pA260DepInfo1  = new (::rFHDI_) 
      DependentInformation(4,5,20, LOW_MAND_TIDAL_VOL_MSG,  MESSAGE_NAME_NULL);   
   DependentInformation* pA260DepInfo2  = new (::rFHDI_) 
      DependentInformation(5,6,21, LOW_SPONT_TIDAL_VOL_MSG, MESSAGE_NAME_NULL);   
   DependentInformation* pA260DepInfo3  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG,      MESSAGE_NAME_NULL);


   // Define Temporal constraint and VHM manager for this alarm
   TemporalConstraint* pG26001Constraint = new (::rFHTC_) 
      TemporalConstraint(Constraint::LE, Constraint::EQ, 
                         CalcFunctions::Return30Sec, NULL);
   TemporalConstraint* pG26002Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GT, Constraint::LE, 
                         CalcFunctions::Return30Sec, 
                         CalcFunctions::Return2Min);
   TemporalConstraint* pG26003Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GT, Constraint::EQ, 
                         CalcFunctions::Return2Min, NULL);

   // Create Constraint operating groups 
   OperatingGroup* pG26001 = new (::rFHOG_)
      OperatingGroup( G26001,pG26001Constraint);
      AlarmOperand::AddOperand(pG26001);

   OperatingGroup* pG26002 = new (::rFHOG_)
      OperatingGroup( G26002,pG26002Constraint);
      AlarmOperand::AddOperand(pG26002);

   OperatingGroup* pG26003 = new (::rFHOG_)
      OperatingGroup( G26003,pG26003Constraint);
      AlarmOperand::AddOperand(pG26003);


   // Create a Violation History Manager
   VHMTemporal* pG26000Vhm = new (::rFHVHMT_) 
      VHMTemporal(NULL);

   // Create a VHM operating group 
   OperatingGroup* pG26000 = new (::rFHOG_)
      OperatingGroup( G26000,NULL,NULL,pG26000Vhm);
      AlarmOperand::AddOperand(pG26000);


   
   
   Alarm* pA260 = new (::rFHA_)              // Define the alarm
      Alarm(A260, *pA260Info, *pG26000,  C305, TRUE,
            LOW_MINUTE_VOL_MSG);

   Alarm::AddAlarm(pA260);                   // Add the alarm
   


   // Create the operating components for this alarm
   OperatingComponent* pKG26000_C305   = new (::rFHOK_)
      OperatingComponent(*pC305,   OperatingComponent::OR, FALSE); 
   OperatingComponent* pKG26000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND,TRUE);
   OperatingComponent* pKG26001_G26000 = new (::rFHOK_)
      OperatingComponent(*pG26000, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG26002_G26000 = new (::rFHOK_)
      OperatingComponent(*pG26000, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG26003_G26000 = new (::rFHOK_)
      OperatingComponent(*pG26000, OperatingComponent::OR, FALSE);

   
   // Connect the operating components to the parent/child relationships
   pG26001->addChild(*pKG26001_G26000);
   pG26002->addChild(*pKG26002_G26000);
   pG26003->addChild(*pKG26003_G26000);
   pG26000->addChild(*pKG26000_C305);
   pG26000->addChild(*pKG26000_G32000);

   pG26000->addParent(*pG26001);
   pG26000->addParent(*pG26002);
   pG26000->addParent(*pG26003);
   pC305  ->addParent(*pG26000);
   pG32000->addParent(*pG26000);


   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA260_G26001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA260LowInfo,    *pA260, *pG26001);
   ConditionAugmentation* pA260_G26002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA260MediumInfo, *pA260, *pG26002);
   ConditionAugmentation* pA260_G26003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA260HighInfo,   *pA260, *pG26003);
                             
   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA260_A255Aug = new (::rFHDA_)
      DependentAugmentation(*pA260DepInfo1, *pA260, *pA255);

   // Dependent augmentations for A260_A265 and A260_A235 are defined in
   // A235.



   pG26000->setCausedAlarm(pA260);           // Set the caused state

   pA260->addAugmentation(*pA260_G26001Aug);   // Add the augmented state
   pG26001->addAugmentation(*pA260_G26001Aug);

   pA260->addAugmentation(*pA260_G26002Aug);   // Add the augmented state
   pG26002->addAugmentation(*pA260_G26002Aug);

   pA260->addAugmentation(*pA260_G26003Aug);   // Add the augmented state
   pG26003->addAugmentation(*pA260_G26003Aug);


   pA260->addAugmentation(*pA260_A255Aug);   // Add the dependent augmented state
   pA255->addAugmentation(*pA260_A255Aug);



   //======================================================
   //
   //   A235:  High Respiratory Rate
   //   $[05114] $[05116] $[05117] $[05118] $[05119] $[05120] $[05121]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA235Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,     60, MESSAGE_NAME_NULL,      
         CHECK_PATIENT_SETTINGS_MSG);

   AlarmInformation* pA235LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,     60, HIGH_RESP_RATE_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA235MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY,  70, HIGH_RESP_RATE_MED_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA235HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,   220, HIGH_RESP_RATE_HIGH_MSG, 
         MESSAGE_NAME_NULL);


   DependentInformation* pA235DepInfo1  = new (::rFHDI_) 
      DependentInformation(4,5,20, LOW_MAND_TIDAL_VOL_MSG,     MESSAGE_NAME_NULL);   
   DependentInformation* pA235DepInfo2  = new (::rFHDI_) 
      DependentInformation(5,6,21, LOW_SPONT_TIDAL_VOL_MSG,    MESSAGE_NAME_NULL);
   DependentInformation* pA235DepInfo3  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG,         MESSAGE_NAME_NULL);   


   // Define temporal constraints for this alarm 
   TemporalConstraint* pG23501Constraint = new (::rFHTC_) 
      TemporalConstraint(Constraint::LE, Constraint::EQ, 
                         CalcFunctions::Return30Sec, NULL);
   TemporalConstraint* pG23502Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GT, Constraint::LE,
                         CalcFunctions::Return30Sec, 
                         CalcFunctions::Return2Min);
   TemporalConstraint* pG23503Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GT, Constraint::EQ, 
                         CalcFunctions::Return2Min, NULL);

   // Create Constraint operating groups 
   OperatingGroup* pG23501 = new (::rFHOG_)
      OperatingGroup( G23501,pG23501Constraint);
      AlarmOperand::AddOperand(pG23501);

   OperatingGroup* pG23502 = new (::rFHOG_)
      OperatingGroup( G23502,pG23502Constraint);
      AlarmOperand::AddOperand(pG23502);

   OperatingGroup* pG23503 = new (::rFHOG_)
      OperatingGroup( G23503,pG23503Constraint);
      AlarmOperand::AddOperand(pG23503);



   // Create a Violation History Manager
   VHMTemporal*  pG23500Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create a VHM operating group 
   OperatingGroup* pG23500 = new (::rFHOG_)
      OperatingGroup( G23500,NULL,NULL,pG23500Vhm);
      AlarmOperand::AddOperand(pG23500);


   

   Alarm* pA235 = new (::rFHA_)              // Define the alarm
      Alarm(A235, *pA235Info, *pG23500,  C300, TRUE,
            HIGH_RESP_RATE_MSG);

   Alarm::AddAlarm(pA235);                   // Add the alarm
   
   

   // Create the operating components for this alarm
   OperatingComponent* pKG23500_C300   = new (::rFHOK_)
      OperatingComponent(*pC300,   OperatingComponent::OR, FALSE); 
   OperatingComponent* pKG23500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND,TRUE);
   OperatingComponent* pKG23501_G23500 = new (::rFHOK_)
      OperatingComponent(*pG23500, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG23502_G23500 = new (::rFHOK_)
      OperatingComponent(*pG23500, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG23503_G23500 = new (::rFHOK_)
      OperatingComponent(*pG23500, OperatingComponent::OR, FALSE);

   
   // Connect the operating components to the parent/child relationships
   pG23500->addChild(*pKG23500_C300);
   pG23500->addChild(*pKG23500_G32000);
   pG23501->addChild(*pKG23501_G23500);
   pG23502->addChild(*pKG23502_G23500);
   pG23503->addChild(*pKG23503_G23500);

   pC300  ->addParent(*pG23500);
   pG32000->addParent(*pG23500);
   pG23500->addParent(*pG23501);
   pG23500->addParent(*pG23502);
   pG23500->addParent(*pG23503);

   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA235_G23501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA235LowInfo,    *pA235, *pG23501);
   ConditionAugmentation* pA235_G23502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA235MediumInfo, *pA235, *pG23502);
   ConditionAugmentation* pA235_G23503Aug = new (::rFHCA_)
      ConditionAugmentation(*pA235HighInfo,   *pA235, *pG23503);
                             
   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA235_A255Aug = new (::rFHDA_)
      DependentAugmentation(*pA235DepInfo1, *pA235, *pA255);
   DependentAugmentation* pA235_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA235DepInfo3, *pA235, *pA260);

   // Dependent augmentation for A235_A265 is defined in A265

   pG23500->setCausedAlarm(pA235);           // Set the caused state


   pA235->addAugmentation(*pA235_G23501Aug);   // Add the condition augmented state
   pG23501->addAugmentation(*pA235_G23501Aug);

   pA235->addAugmentation(*pA235_G23502Aug);   // Add the condition augmented state
   pG23502->addAugmentation(*pA235_G23502Aug);

   pA235->addAugmentation(*pA235_G23503Aug);   // Add the condition augmented state
   pG23503->addAugmentation(*pA235_G23503Aug);



   pA235->addAugmentation(*pA235_A255Aug);   // Add the dependent augmented state
   pA255->addAugmentation(*pA235_A255Aug);

   pA235->addAugmentation(*pA235_A260Aug);   // Add the dependent augmented state
   pA260->addAugmentation(*pA235_A260Aug);



   // Create the dependent augmentations for Alarm A255, defined previously.
   DependentAugmentation* pA255_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA255DepInfo2, *pA255, *pA235);
   DependentAugmentation* pA255_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA255DepInfo1, *pA255, *pA260);

   pA255->addAugmentation(*pA255_A235Aug);   // Add the dependent augmented state
   pA235->addAugmentation(*pA255_A235Aug);

   pA255->addAugmentation(*pA255_A260Aug);   // Add the dependent augmented state
   pA260->addAugmentation(*pA255_A260Aug);


   // Create dependent augmentations for A260_A235, defined previously.
   DependentAugmentation* pA260_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA260DepInfo3, *pA260, *pA235);

   pA260->addAugmentation(*pA260_A235Aug);   // Add the dependent augmented state
   pA235->addAugmentation(*pA260_A235Aug);


   //======================================================
   //
   //   A205:   Apnea
   //   $[05080] $[05083] $[05173] $[05178]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA205Info = new (::rFHAI_) 
      AlarmInformation(Alarm::MEDIUM_URGENCY,30,MESSAGE_NAME_NULL,          
         CHECK_PATIENT_SETTINGS_MSG);
   AlarmInformation* pA205MediumInfo = new (::rFHAI_) 
      AlarmInformation(Alarm::MEDIUM_URGENCY,30,APNEA_SHORT_DUR_ONE_EVENT_MSG, 
      MESSAGE_NAME_NULL);
   AlarmInformation* pA205HighInfo2  = new (::rFHAI_) 
      AlarmInformation(Alarm::HIGH_URGENCY,170, APNEA_LONG_DUR_TWO_EVENTS_MSG,
      MESSAGE_NAME_NULL);

   DependentInformation* pA260DepInfo  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG,                MESSAGE_NAME_NULL);    


   //====================================================================
   //   Define a Temporal constraint, duration > 4 apnea intervals
   //====================================================================
   // Define the Temporal constraint.
   TemporalConstraint* pG20502Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GT, Constraint::EQ, 
                         CalcFunctions::ReturnFourApneaIntervals, NULL);

   // Create a Temporal Constraint operating group 
   OperatingGroup* pG20502 = new (::rFHOG_)
      OperatingGroup( G20502,pG20502Constraint);
      AlarmOperand::AddOperand(pG20502);


   // Create a Temporal Violation History Manager
   VHMTemporal*  pG20501Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create an VHM operating group 
   OperatingGroup* pG20501 = new (::rFHOG_)
      OperatingGroup( G20501,NULL,NULL,pG20501Vhm);
      AlarmOperand::AddOperand(pG20501);



  

   
   //===================================================================
   //   Define an Event count constraint  >= 4 events in 10 apnea intervals
   //===================================================================
   CountConstraint* pG20504Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return4Events, NULL);

   // Create a Count Constraint operating group 
   OperatingGroup* pG20504 = new (::rFHOG_)
      OperatingGroup(G20504,pG20504Constraint);
      AlarmOperand::AddOperand(pG20504);


   // Create a Count Violation History Manager
   VHMEventCountDur*  pG20503Vhm = new (::rFHVHMECD_)
      VHMEventCountDur(CalcFunctions::ReturnTenApneaIntervals);

   // Create a VHM operating group 
   OperatingGroup* pG20503 = new (::rFHOG_)
      OperatingGroup( G20503,NULL,NULL,pG20503Vhm);
      AlarmOperand::AddOperand(pG20503);



   
   // Define the alarm
   Alarm* pA205 = new (::rFHA_)            
      Alarm(A205, *pA205Info, *pG20500,  C10, FALSE,
            APNEA_MSG);

   Alarm::AddAlarm(pA205);                // Add the alarm

   

   //==================================================================
   // Create the operating components for this alarm.  This is sorted
   // as an inverted tree with the highest branches defined first,
   // with lowermost level of leaves at the bottom of the definitions.
   //===================================================================


   OperatingComponent* pKG20500_C10 = new (::rFHOK_)
      OperatingComponent(*pC10, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG20500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);
   OperatingComponent* pKG20501_C10 = new (::rFHOK_)
      OperatingComponent(*pC10, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG20502_G20501 = new (::rFHOK_)
      OperatingComponent(*pG20501, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG20503_C10 = new (::rFHOK_)
      OperatingComponent(*pC10, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG20504_G20503 = new (::rFHOK_)
      OperatingComponent(*pG20503, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG20505_G20502 = new (::rFHOK_)
      OperatingComponent(*pG20502, OperatingComponent::OR, TRUE);
   OperatingComponent* pKG20505_G20504 = new (::rFHOK_)
      OperatingComponent(*pG20504, OperatingComponent::AND, TRUE);
   OperatingComponent* pKG20508_G20502 = new (::rFHOK_)
      OperatingComponent(*pG20502, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG20508_G20504 = new (::rFHOK_)
      OperatingComponent(*pG20504, OperatingComponent::OR, FALSE);





   // Connect the operating components to the parent/child relationships
   pG20500->addChild(*pKG20500_C10);
   pG20500->addChild(*pKG20500_G32000);
   pG20501->addChild(*pKG20501_C10);
   pG20502->addChild(*pKG20502_G20501);
   pG20503->addChild(*pKG20503_C10);
   pG20504->addChild(*pKG20504_G20503);
   pG20505->addChild(*pKG20505_G20502);
   pG20505->addChild(*pKG20505_G20504);
   pG20508->addChild(*pKG20508_G20502);
   pG20508->addChild(*pKG20508_G20504);


   pC10   ->addParent(*pG20500);
   pC10   ->addParent(*pG20501);
   pC10   ->addParent(*pG20503);
   pG32000->addParent(*pG20500);
   pG20501->addParent(*pG20502);
   pG20503->addParent(*pG20504);
   pG20502->addParent(*pG20505);
   pG20504->addParent(*pG20505);
   pG20502->addParent(*pG20508);
   pG20504->addParent(*pG20508);



   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA205_G20505Aug = new (::rFHCA_)
      ConditionAugmentation(*pA205MediumInfo, *pA205, *pG20505);

   ConditionAugmentation* pA205_G20508Aug = new (::rFHCA_)
      ConditionAugmentation(*pA205HighInfo2, *pA205, *pG20508);

   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA205_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA260DepInfo, *pA205, *pA260);



   pG20500->setCausedAlarm(pA205);           // Set the caused state

   pA205->addAugmentation(*pA205_G20505Aug); // Add the augmented state
   pG20505->addAugmentation(*pA205_G20505Aug);

   pA205->addAugmentation(*pA205_G20508Aug); // Add the augmented state
   pG20508->addAugmentation(*pA205_G20508Aug);

   pA205->addAugmentation(*pA205_A260Aug);   // Add the dependent augmented state
   pA260->addAugmentation(*pA205_A260Aug);





   //======================================================
   //
   //   A210:  High Minute Volume Alarm
   //   $[05084] $[05086] $[05087] $[05088] $[05089]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA210Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    70, MESSAGE_NAME_NULL,       
         CHECK_PATIENT_SETTINGS_MSG);
   AlarmInformation* pA210LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    70, HIGH_MINUTE_VOL_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA210MediumInfo = new (::rFHAI_)          
      AlarmInformation(Alarm::MEDIUM_URGENCY, 80, HIGH_MINUTE_VOL_MED_MSG,
         MESSAGE_NAME_NULL);
   AlarmInformation* pA210HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,  230, 
         HIGH_MINUTE_VOL_HIGH_MSG,MESSAGE_NAME_NULL);

   DependentInformation* pA210DepInfo  = new (::rFHDI_) 
      DependentInformation(8,9,24, HIGH_TIDAL_VOL_MSG, 
      MESSAGE_NAME_NULL);


   // Define a Temporal constraint, duration <= 20 sec intervals
   TemporalConstraint* pG21001Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::EQ, Constraint::LE, NULL, 
                         CalcFunctions::Return30Sec);
   
   // Create a Temporal Constraint operating group 
   OperatingGroup* pG21001 = new (::rFHOG_)
      OperatingGroup( G21001,pG21001Constraint);
      AlarmOperand::AddOperand(pG21001);

   
   
   // Define a Temporal constraint, duration > 20, <=40 sec intervals
   TemporalConstraint* pG21002Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GT, Constraint::LE, 
                         CalcFunctions::Return30Sec, 
                         CalcFunctions::Return2Min);
                                                
   // Create a Temporal Constraint operating group 
   OperatingGroup* pG21002 = new (::rFHOG_)
      OperatingGroup( G21002,pG21002Constraint);
      AlarmOperand::AddOperand(pG21002);



   // Define a Temporal constraint, duration > 40 sec intervals
   TemporalConstraint* pG21003Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GT, Constraint::EQ, 
                         CalcFunctions::Return2Min, NULL);
   
   // Create a Temporal Constraint operating group 
   OperatingGroup* pG21003 = new (::rFHOG_)
      OperatingGroup( G21003,pG21003Constraint);
      AlarmOperand::AddOperand(pG21003);

   

   // Create a Temporal Violation History Manager
   VHMTemporal*  pG21000Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);
   
   // Create a VHM operating group 
   OperatingGroup* pG21000 = new (::rFHOG_)
      OperatingGroup( G21000,NULL,NULL,pG21000Vhm);
      AlarmOperand::AddOperand(pG21000);



   


   Alarm* pA210 = new (::rFHA_)              // Define the alarm
      Alarm(A210, *pA210Info, *pG21000,  C295, TRUE,
            HIGH_MINUTE_VOL_MSG);
   
   
   Alarm::AddAlarm(pA210);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG21000_C295 = new (::rFHOK_)
      OperatingComponent(*pC295,   OperatingComponent::OR, FALSE);

   OperatingComponent* pKG21000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG21001_G21000 = new (::rFHOK_)
      OperatingComponent(*pG21000, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG21002_G21000 = new (::rFHOK_)
      OperatingComponent(*pG21000, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG21003_G21000 = new (::rFHOK_)
      OperatingComponent(*pG21000, OperatingComponent::OR, FALSE);



   // Connect the operating components to the parent/child relationships
   pG21000->addChild(*pKG21000_C295);
   pG21000->addChild(*pKG21000_G32000);
   pG21001->addChild(*pKG21001_G21000);
   pG21002->addChild(*pKG21002_G21000);
   pG21003->addChild(*pKG21003_G21000);

   pC295  ->addParent(*pG21000);
   pG32000->addParent(*pG21000);
   pG21000->addParent(*pG21001);
   pG21000->addParent(*pG21002);
   pG21000->addParent(*pG21003);

   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA210_G21001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA210LowInfo, *pA210, *pG21001);

   ConditionAugmentation* pA210_G21002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA210MediumInfo, *pA210, *pG21002);

   ConditionAugmentation* pA210_G21003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA210HighInfo, *pA210, *pG21003);


   // Dependent augmentation for A210_A215 is defined in A215.
   
   


   pG21000->setCausedAlarm(pA210);           // Set the caused state
                                    
   pA210->addAugmentation(*pA210_G21001Aug); // Add the augmented state
   pG21001->addAugmentation(*pA210_G21001Aug);

   pA210->addAugmentation(*pA210_G21002Aug); // Add the augmented state
   pG21002->addAugmentation(*pA210_G21002Aug);

   pA210->addAugmentation(*pA210_G21003Aug); // Add the augmented state
   pG21003->addAugmentation(*pA210_G21003Aug);





   //======================================================
   //
   //   A215:  High Tidal Volume Alarm
   //   $[05090] $[05092] $[05093] $[05094] $[05095]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA215Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    80, MESSAGE_NAME_NULL,      
         HIGH_TIDAL_VOL_REMEDY_MSG);

   AlarmInformation* pA215LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    80, HIGH_TIDAL_VOL_LOW_MSG, 
         MESSAGE_NAME_NULL);
   AlarmInformation* pA215MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 90, HIGH_TIDAL_VOL_MED_MSG, 
         MESSAGE_NAME_NULL);
   AlarmInformation* pA215HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,  240,
         HIGH_TIDAL_VOL_HIGH_MSG,MESSAGE_NAME_NULL);

   DependentInformation* pA215DepInfo  = new (::rFHDI_) 
      DependentInformation(7,8,23, HIGH_MINUTE_VOL_MSG, 
         MESSAGE_NAME_NULL);   



   // Define count constraints.
   // Last 2 or 3 breaths meet detection criteria
   CountConstraint* pG21501Constraint = new (::rFHCC_) 
      CountConstraint(Constraint::GE, Constraint::LT,
                      CalcFunctions::Return2Events, CalcFunctions::Return4Events); 

   // Last 4 to 9 breaths meet detection criteria
   CountConstraint* pG21502Constraint = new (::rFHCC_) 
      CountConstraint(Constraint::GE, Constraint::LT, 
                      CalcFunctions::Return4Events, CalcFunctions::Return10Events);

   // Last 10 breaths meet detection criteria
   CountConstraint* pG21503Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ,
                      CalcFunctions::Return10Events, NULL);

   // >= 2 breaths meet detection criteria
   CountConstraint* pG21505Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return2Events, NULL);


   // Create operating groups for the above constraints 
   OperatingGroup* pG21501 = new (::rFHOG_)
      OperatingGroup( G21501,pG21501Constraint);
      AlarmOperand::AddOperand(pG21501);

   OperatingGroup* pG21502 = new (::rFHOG_)
      OperatingGroup( G21502,pG21502Constraint);
      AlarmOperand::AddOperand(pG21502);

   OperatingGroup* pG21503 = new (::rFHOG_)
      OperatingGroup( G21503,pG21503Constraint);
      AlarmOperand::AddOperand(pG21503);

   OperatingGroup* pG21505 = new (::rFHOG_)
      OperatingGroup( G21505,pG21505Constraint);
      AlarmOperand::AddOperand(pG21505);



   // Create a Breath count Violation History Manager, delete breaths > 10
   VHMPrevBreathCountCardinal* pG21504Vhm = new (::rFHVHMPBCC_) 
      VHMPrevBreathCountCardinal(CalcFunctions::Return10Events);

   // Create a VHM operating group 
   OperatingGroup* pG21504 = new (::rFHOG_)
      OperatingGroup( G21504,NULL,NULL,pG21504Vhm);
      AlarmOperand::AddOperand(pG21504);

   
  

   
   Alarm* pA215 = new (::rFHA_)              // Define the alarm
      Alarm(A215, *pA215Info, *pG21500,  C315, TRUE,
            HIGH_TIDAL_VOL_MSG);

   Alarm::AddAlarm(pA215);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG21500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG21500_G21505 = new (::rFHOK_)
      OperatingComponent(*pG21505, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG21505_G21504 = new (::rFHOK_)
      OperatingComponent(*pG21504, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG21504_C315 = new (::rFHOK_)
      OperatingComponent(*pC315,   OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG21501_G21504 = new (::rFHOK_)
      OperatingComponent(*pG21504, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG21502_G21504 = new (::rFHOK_)
      OperatingComponent(*pG21504, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG21503_G21504 = new (::rFHOK_)
      OperatingComponent(*pG21504, OperatingComponent::OR,  FALSE);
   
   

   // Connect the operating components to the parent/child relationships
   pG21500->addChild(*pKG21500_G21505);
   pG21500->addChild(*pKG21500_G32000);
   pG21505->addChild(*pKG21505_G21504);
   pG21504->addChild(*pKG21504_C315);
   pG21501->addChild(*pKG21501_G21504);
   pG21502->addChild(*pKG21502_G21504);
   pG21503->addChild(*pKG21503_G21504);
   
   pG32000->addParent(*pG21500);
   pG21505->addParent(*pG21500);
   pG21504->addParent(*pG21505);
   pC315  ->addParent(*pG21504);
   pG21504->addParent(*pG21501);
   pG21504->addParent(*pG21502);
   pG21504->addParent(*pG21503);

   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA215_G21501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA215LowInfo,    *pA215, *pG21501);
   ConditionAugmentation* pA215_G21502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA215MediumInfo, *pA215, *pG21502);
   ConditionAugmentation* pA215_G21503Aug = new (::rFHCA_)
      ConditionAugmentation(*pA215HighInfo,   *pA215, *pG21503);
   
   DependentAugmentation* pA215_A210Aug = new (::rFHDA_)
      DependentAugmentation(*pA215DepInfo, *pA215, *pA210);
   
   
   pG21500->setCausedAlarm(pA215);           // Set the caused state

   pA215->addAugmentation(*pA215_G21501Aug); // Add the augmented state
   pG21501->addAugmentation(*pA215_G21501Aug);

   pA215->addAugmentation(*pA215_G21502Aug); // Add the augmented state
   pG21502->addAugmentation(*pA215_G21502Aug);

   pA215->addAugmentation(*pA215_G21503Aug); // Add the augmented state
   pG21503->addAugmentation(*pA215_G21503Aug);

   pA215->addAugmentation(*pA215_A210Aug);   // Add the dependent augmented state
   pA210->addAugmentation(*pA215_A210Aug);


   
   // Create the dependent augmentation for alarm A210, defined previously.
   DependentAugmentation* pA210_A215Aug = new (::rFHDA_)
      DependentAugmentation(*pA210DepInfo, *pA210, *pA215);

   pA210->addAugmentation(*pA210_A215Aug);   // Add the augmented state
   pA215->addAugmentation(*pA210_A215Aug);

   


   //======================================================
   //
   //   A240:  AC Power Loss   
   //   $[05122] $[05174]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA240Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 110, AC_POWER_LOSS_ANALYSIS_MSG, 
         MESSAGE_NAME_NULL);

   AlarmInformation* pA240MediumInfo = new (::rFHAI_)            
      AlarmInformation(Alarm::MEDIUM_URGENCY, 110, LOW_OPERATIONAL_TIME_MSG,  
         PREPARE_FOR_POWER_LOSS_MSG);

   // Define a Temporal constraint, duration >= 1 sec
   TemporalConstraint* pG24000Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ,
                         CalcFunctions::Return3Sec, NULL);

   // Create a Temporal Constraint operating group.
   // Link both constraints to this group.
   OperatingGroup* pG24000 = new (::rFHOG_)
      OperatingGroup(G24000,pG24000Constraint,NULL);
      AlarmOperand::AddOperand(pG24000);

   // Create a Temporal Violation History Manager
   VHMTemporal*  pG24001Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create an VHM operating group 
   OperatingGroup* pG24001 = new (::rFHOG_)
      OperatingGroup( G24001,NULL,NULL,pG24001Vhm);
      AlarmOperand::AddOperand(pG24001);

  
   Alarm* pA240 = new (::rFHA_)              // Define the alarm
      Alarm(A240, *pA240Info, *pG24000,  OPERAND_NAME_NULL,
            FALSE, AC_POWER_LOSS_MSG, TRUE);
   
   Alarm::AddAlarm(pA240);                   // Add the alarm
   
   
   // Define the operating components for this alarm 
   OperatingComponent* pKG24000_G24001 = new (::rFHOK_)            // G24000
      OperatingComponent(*pG24001, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG24001_C260 = new (::rFHOK_)            // G24000
      OperatingComponent(*pC260, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG24002_C261 = new (::rFHOK_)            // G24000
      OperatingComponent(*pC261, OperatingComponent::OR, FALSE);


   // Connect the operating components to the parent/child relationships
   pG24000->addChild (*pKG24000_G24001);
   pG24001->addChild (*pKG24001_C260);
   pG24002->addChild (*pKG24002_C261);

   pG24001->addParent(*pG24000);
   pC260  ->addParent(*pG24001);
   pC261  ->addParent(*pG24002);


   // Set the caused state
   pG24000->setCausedAlarm(pA240);

   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA240_G24002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA240MediumInfo,    *pA240, *pG24002);

   pA240->addAugmentation(*pA240_G24002Aug); // Add condition augmented state
   pG24002->addAugmentation(*pA240_G24002Aug);


    //======================================================
   //
   //   A250:  Low AC Power - Out of order because G25000 is needed by A220
   //   $[05124]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA250Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 210, LOW_AC_POWER_ANALYSIS_MSG, 
         LOW_AC_POWER_REMEDY_MSG);

   // Define a Temporal constraint, duration >= 1 sec
   TemporalConstraint* pG25000Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ,
                         CalcFunctions::Return1Sec, NULL);

   // Define a Temporal Reset constraint, duration >= 1 sec
   TemporalConstraint* pG25000ResetConstraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
                         CalcFunctions::Return1Sec, NULL);

   // Create a Temporal Constraint operating group.
   // Link both constraints to this group.
   OperatingGroup* pG25000 = new (::rFHOG_)
      OperatingGroup(G25000,pG25000Constraint,pG25000ResetConstraint);
      AlarmOperand::AddOperand(pG25000);


   
   // Create a Temporal Violation History Manager
   VHMTemporal*  pG25001Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create an VHM operating group 
   OperatingGroup* pG25001 = new (::rFHOG_)
      OperatingGroup( G25001,NULL,NULL,pG25001Vhm);
      AlarmOperand::AddOperand(pG25001);



   
   Alarm* pA250 = new (::rFHA_)              // Define the alarm
      Alarm(A250, *pA250Info, *pG25000,  OPERAND_NAME_NULL,
            FALSE, LOW_AC_POWER_MSG, TRUE);


   Alarm::AddAlarm(pA250);                   // Add the alarm
   
   

   // Define the operating components for this alarm 
   OperatingComponent* pKG25000_G25001 = new (::rFHOK_)      // G25000
      OperatingComponent(*pG25001, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG25001_C270 = new (::rFHOK_)        // G25001
      OperatingComponent(*pC270, OperatingComponent::OR, FALSE);
   
   OperatingComponent* pKG25001_G24000 = new (::rFHOK_)      // G25001
      OperatingComponent(*pG24000, OperatingComponent::AND, TRUE);


   // Connect the operating components to the parent/child relationships
   pG25000->addChild (*pKG25000_G25001);
   pG25001->addChild (*pKG25001_C270);
   pG25001->addChild (*pKG25001_G24000);
   
   pG25001->addParent(*pG25000);
   pC270  ->addParent(*pG25001);
   pG24000->addParent(*pG25001);
   

   // Set the caused state
   pG25000->setCausedAlarm(pA250);




   //======================================================
   //
   //   A220:  High Delivered O2%
   //   $[05096] $[05098] $[05099]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA220Info = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY,    100, MESSAGE_NAME_NULL,  
         HIGH_O2_REMEDY_MSG);
   AlarmInformation* pA220LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY,    100, HIGH_O2_LOW_ANALYSIS_MSG,     
         MESSAGE_NAME_NULL);
   AlarmInformation* pA220MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 345, HIGH_O2_MEDIUM_ANALYSIS_MSG,  
         MESSAGE_NAME_NULL);


   //========================================================
   // Define the Temporal constraints and VHM managers
   //========================================================
   TemporalConstraint* pG22001Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
                         CalcFunctions::Return30Sec, NULL);  // >= 30 sec

   TemporalConstraint* pG22002Constraint = new (::rFHTC_)  // >= 30 sec, < 2min 
      TemporalConstraint(Constraint::EQ, Constraint::LT, NULL,
                         CalcFunctions::Return90Sec);

   TemporalConstraint* pG22003Constraint = new (::rFHTC_)         // >= 2min
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
                         CalcFunctions::Return90Sec, NULL);

   // Reset Constraint >= 30 sec.
   TemporalConstraint* pG22001ResetConstraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
                         CalcFunctions::Return30Sec, NULL);  

   // Create Constraint operating groups 
   OperatingGroup* pG22002 = new (::rFHOG_)
      OperatingGroup( G22002, pG22002Constraint);
      AlarmOperand::AddOperand(pG22002);


   OperatingGroup* pG22003 = new (::rFHOG_)
      OperatingGroup( G22003, pG22003Constraint);
      AlarmOperand::AddOperand(pG22003);


   // Create Violation History Managers
   VHMTemporal*  pG22001Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   VHMTemporal*  pG22004Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create VHM operating groups 
   OperatingGroup* pG22004 = new (::rFHOG_)
      OperatingGroup( G22004,NULL,NULL,pG22004Vhm);
      AlarmOperand::AddOperand(pG22004);

   // Create VHM, Constraint operating groups
   OperatingGroup* pG22001 = new (::rFHOG_)
      OperatingGroup( G22001, pG22001Constraint, 
                     pG22001ResetConstraint, pG22001Vhm);
      AlarmOperand::AddOperand(pG22001);




   Alarm* pA220 = new (::rFHA_)               // Define the alarm
      Alarm(A220, *pA220Info, *pG22000,  C150, TRUE,
            HIGH_O2_MSG);

   Alarm::AddAlarm(pA220);                  // Add the alarm
   
   
   
   // Create the operating components for this alarm.  

   OperatingComponent* pKG22000_G22001 = new (::rFHOK_)
      OperatingComponent(*pG22001, OperatingComponent::OR,  FALSE);
 
   OperatingComponent* pKG22000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);
   
  
   OperatingComponent* pKG22001_G22004 = new (::rFHOK_)
      OperatingComponent(*pG22004, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG22002_G22001 = new (::rFHOK_)
      OperatingComponent(*pG22001, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG22003_G22001 = new (::rFHOK_)
      OperatingComponent(*pG22001, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG22004_C205   = new (::rFHOK_)
      OperatingComponent(*pC205,   OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG22004_C150   = new (::rFHOK_)
      OperatingComponent(*pC150,   OperatingComponent::AND,  FALSE);
   
   OperatingComponent* pKG22004_G4000 = new (::rFHOK_)
      OperatingComponent(*pG4000,    OperatingComponent::AND, TRUE);

   OperatingComponent* pKG22004_G25000   = new (::rFHOK_)
      OperatingComponent(*pG25000,   OperatingComponent::AND,  TRUE);


   // Connect the operating components to the parent/child relationships

   pG22000->addChild(*pKG22000_G22001);
   pG22000->addChild(*pKG22000_G32000);
   pG22001->addChild(*pKG22001_G22004);
   pG22002->addChild(*pKG22002_G22001);
   pG22003->addChild(*pKG22003_G22001);
   pG22004->addChild(*pKG22004_C205);
   pG22004->addChild(*pKG22004_C150);
   pG22004->addChild(*pKG22004_G4000);
   pG22004->addChild(*pKG22004_G25000);

   pG22001->addParent(*pG22000);
   pG32000->addParent(*pG22000);
   pG22004->addParent(*pG22001);
   pG22001->addParent(*pG22002);
   pG22001->addParent(*pG22003);
   pC205  ->addParent(*pG22004);
   pC150  ->addParent(*pG22004);
   pG4000 ->addParent(*pG22004);
   pG25000->addParent(*pG22004);


   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA220_G22002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA220LowInfo, *pA220, *pG22002);

   ConditionAugmentation* pA220_G22003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA220MediumInfo, *pA220, *pG22003);



   pG22000->setCausedAlarm(pA220);            // Set the caused state


   pA220->addAugmentation(*pA220_G22002Aug);  // Add the augmented state
   pG22002->addAugmentation(*pA220_G22002Aug);

   pA220->addAugmentation(*pA220_G22003Aug);  // Add the augmented state
   pG22003->addAugmentation(*pA220_G22003Aug);




   //======================================================
   //
   //   A225:  High Circuit Pressure
   //   $[05100] $[05101] $[05102] $[05103] $[05104] $[05105] $[05106]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA225Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,     10, MESSAGE_NAME_NULL,  
         HIGH_PRES_REMEDY_MSG);

   AlarmInformation* pA225LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,     10, HIGH_PRES_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA225MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY,  10, HIGH_PRES_MED_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA225HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,   140, HIGH_PRES_HIGH_MSG,
         MESSAGE_NAME_NULL);


   DependentInformation* pA225DepInfo1  = new (::rFHDI_) 
      DependentInformation(4,5,20, LOW_MAND_TIDAL_VOL_MSG,      MESSAGE_NAME_NULL);   
   DependentInformation* pA225DepInfo2  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG,          MESSAGE_NAME_NULL);   
   DependentInformation* pA225DepInfo3  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG,          MESSAGE_NAME_NULL);


   // 1 of last 4 spont breaths meet detection criteria
   CountConstraint* pG22511Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return1Event, NULL);

   // 2 of last 4 spont breaths meet detection criteria
   CountConstraint* pG22512Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return2Events, NULL);

   // 3 of last 4 spont breaths meet detection criteria
   CountConstraint* pG22513Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return3Events, NULL);

   // 4 of last 4 spont breaths meet detection criteria
   CountConstraint* pG22514Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return4Events, NULL);


   
   // Create operating groups for the above constraints
   OperatingGroup* pG22511 = new (::rFHOG_)
      OperatingGroup( G22511,pG22511Constraint);
      AlarmOperand::AddOperand(pG22511);

   OperatingGroup* pG22512 = new (::rFHOG_)
      OperatingGroup( G22512,pG22512Constraint);
      AlarmOperand::AddOperand(pG22512);

   OperatingGroup* pG22513 = new (::rFHOG_)
      OperatingGroup( G22513,pG22513Constraint);
      AlarmOperand::AddOperand(pG22513);

   OperatingGroup* pG22514 = new (::rFHOG_)
      OperatingGroup( G22514,pG22514Constraint);
      AlarmOperand::AddOperand(pG22514);

   
   // Create a Breath count Violation History Manager, delete breaths > 4
   VHMCurrBreathCountCardinal* pG22508Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return4Events);

   OperatingGroup* pG22508 = new (::rFHOG_)
      OperatingGroup( G22508, NULL, NULL, pG22508Vhm);
      AlarmOperand::AddOperand(pG22508);




   // 1 of last 4 mandatory breaths meet detection criteria
   CountConstraint* pG22521Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return1Event, NULL);

   // 2 of last 4 mandatory breaths meet detection criteria
   CountConstraint* pG22522Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return2Events, NULL);

   // 3 of last 4 mandatory breaths meet detection criteria
   CountConstraint* pG22523Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return3Events, NULL);

   // 4 of last 4 mandatory breaths meet detection criteria
   CountConstraint* pG22524Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return4Events, NULL);


      // Create operating groups for the above constraints
   OperatingGroup* pG22521 = new (::rFHOG_)
      OperatingGroup( G22521,pG22521Constraint);
      AlarmOperand::AddOperand(pG22521);

   OperatingGroup* pG22522 = new (::rFHOG_)
      OperatingGroup( G22522,pG22522Constraint);
      AlarmOperand::AddOperand(pG22522);

   OperatingGroup* pG22523 = new (::rFHOG_)
      OperatingGroup( G22523,pG22523Constraint);
      AlarmOperand::AddOperand(pG22523);

   OperatingGroup* pG22524 = new (::rFHOG_)
      OperatingGroup( G22524,pG22524Constraint);
      AlarmOperand::AddOperand(pG22524);

   
   // Create a Breath count Violation History Manager, delete breaths > 4
   VHMCurrBreathCountCardinal* pG22509Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return4Events);

   OperatingGroup* pG22509 = new (::rFHOG_)
      OperatingGroup( G22509, NULL, NULL, pG22509Vhm);
      AlarmOperand::AddOperand(pG22509);




   Alarm* pA225 = new (::rFHA_)                // Define the alarm
      Alarm(A225, *pA225Info, *pG22500,  C135, C136, TRUE,
            HIGH_PRES_CIRC_MSG);

   Alarm::AddAlarm(pA225);                   // Add the alarm
   
   
   // Create the operating components for this alarm
   OperatingComponent* pKG22500_G22560 = new (::rFHOK_)
      OperatingComponent(*pG22560, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);


   OperatingComponent* pKG22560_G22550 = new (::rFHOK_)
      OperatingComponent(*pG22550, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22560_G22551 = new (::rFHOK_)
      OperatingComponent(*pG22551, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22560_G22552 = new (::rFHOK_)
      OperatingComponent(*pG22552, OperatingComponent::OR, FALSE);

   // Low Urgency Alarms
   OperatingComponent* pKG22550_G22530 = new (::rFHOK_)
      OperatingComponent(*pG22530, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22550_G22531 = new (::rFHOK_)
      OperatingComponent(*pG22531, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22550_G22532 = new (::rFHOK_)
      OperatingComponent(*pG22532, OperatingComponent::OR, FALSE);

   // 1m || 2m && 0s
   OperatingComponent* pKG22530_G22521 = new (::rFHOK_)
      OperatingComponent(*pG22521, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22530_G22522 = new (::rFHOK_)
      OperatingComponent(*pG22522, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22530_G22510 = new (::rFHOK_)
      OperatingComponent(*pG22510, OperatingComponent::AND, FALSE);

   // 1s && 1m
   OperatingComponent* pKG22531_G22511 = new (::rFHOK_)
      OperatingComponent(*pG22511, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22531_G22521 = new (::rFHOK_)
      OperatingComponent(*pG22521, OperatingComponent::AND, FALSE);

   // 1s || 2s && 0m
   OperatingComponent* pKG22532_G22511 = new (::rFHOK_)
      OperatingComponent(*pG22511, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22532_G22512 = new (::rFHOK_)
      OperatingComponent(*pG22512, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22532_G22520 = new (::rFHOK_)
      OperatingComponent(*pG22520, OperatingComponent::AND, FALSE);

   // Medium Urgency Alarms
   OperatingComponent* pKG22551_G22533 = new (::rFHOK_)
      OperatingComponent(*pG22533, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22551_G22534 = new (::rFHOK_)
      OperatingComponent(*pG22534, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22551_G22535 = new (::rFHOK_)
      OperatingComponent(*pG22535, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22551_G22536 = new (::rFHOK_)
      OperatingComponent(*pG22536, OperatingComponent::OR, FALSE);

   // 3s && 0m
   OperatingComponent* pKG22533_G22513 = new (::rFHOK_)
      OperatingComponent(*pG22513, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22533_G22520 = new (::rFHOK_)
      OperatingComponent(*pG22520, OperatingComponent::AND, FALSE);

   // 0s && 3m
   OperatingComponent* pKG22534_G22510 = new (::rFHOK_)
      OperatingComponent(*pG22510, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22534_G22523 = new (::rFHOK_)
      OperatingComponent(*pG22523, OperatingComponent::AND, FALSE);

   // 1s && 2m
   OperatingComponent* pKG22535_G22511 = new (::rFHOK_)
      OperatingComponent(*pG22511, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22535_G22522 = new (::rFHOK_)
      OperatingComponent(*pG22522, OperatingComponent::AND, FALSE);

   // 2s && 1m
   OperatingComponent* pKG22536_G22512 = new (::rFHOK_)
      OperatingComponent(*pG22512, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22536_G22521 = new (::rFHOK_)
      OperatingComponent(*pG22521, OperatingComponent::AND, FALSE);

   // High Urgency Alarms
   OperatingComponent* pKG22552_G22537 = new (::rFHOK_)
      OperatingComponent(*pG22537, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22552_G22538 = new (::rFHOK_)
      OperatingComponent(*pG22538, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22552_G22539 = new (::rFHOK_)
      OperatingComponent(*pG22539, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22552_G22540 = new (::rFHOK_)
      OperatingComponent(*pG22540, OperatingComponent::OR, FALSE);


   // 4s || 4m
   OperatingComponent* pKG22537_G22514 = new (::rFHOK_)
      OperatingComponent(*pG22514, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22537_G22524 = new (::rFHOK_)
      OperatingComponent(*pG22524, OperatingComponent::OR, FALSE);

   // 1s || 2s || 3s && 3m
   OperatingComponent* pKG22538_G22511 = new (::rFHOK_)
      OperatingComponent(*pG22511, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22538_G22512 = new (::rFHOK_)
      OperatingComponent(*pG22512, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22538_G22513 = new (::rFHOK_)
      OperatingComponent(*pG22513, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22538_G22523 = new (::rFHOK_)
      OperatingComponent(*pG22523, OperatingComponent::AND, FALSE);

   // 2s || 3s && 2m
   OperatingComponent* pKG22539_G22512 = new (::rFHOK_)
      OperatingComponent(*pG22512, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22539_G22513 = new (::rFHOK_)
      OperatingComponent(*pG22513, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22539_G22522 = new (::rFHOK_)
      OperatingComponent(*pG22522, OperatingComponent::AND, FALSE);

   // 3s && 1m
   OperatingComponent* pKG22540_G22513 = new (::rFHOK_)
      OperatingComponent(*pG22513, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22540_G22521 = new (::rFHOK_)
      OperatingComponent(*pG22521, OperatingComponent::AND, FALSE);

   // Spontaneous setup
   OperatingComponent* pKG22510_G22511 = new (::rFHOK_)
      OperatingComponent(*pG22511, OperatingComponent::OR, TRUE);
   OperatingComponent* pKG22510_G22512 = new (::rFHOK_)
      OperatingComponent(*pG22512, OperatingComponent::AND, TRUE);
   OperatingComponent* pKG22510_G22513 = new (::rFHOK_)
      OperatingComponent(*pG22513, OperatingComponent::AND, TRUE);
   OperatingComponent* pKG22510_G22514 = new (::rFHOK_)
      OperatingComponent(*pG22514, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG22511_G22508 = new (::rFHOK_)
      OperatingComponent(*pG22508, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22512_G22508 = new (::rFHOK_)
      OperatingComponent(*pG22508, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22513_G22508 = new (::rFHOK_)
      OperatingComponent(*pG22508, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22514_G22508 = new (::rFHOK_)
      OperatingComponent(*pG22508, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG22508_C135 = new (::rFHOK_)
      OperatingComponent(*pC135, OperatingComponent::OR, FALSE);

   // Mandatory setup
   OperatingComponent* pKG22520_G22521 = new (::rFHOK_)
      OperatingComponent(*pG22521, OperatingComponent::OR, TRUE);
   OperatingComponent* pKG22520_G22522 = new (::rFHOK_)
      OperatingComponent(*pG22522, OperatingComponent::AND, TRUE);
   OperatingComponent* pKG22520_G22523 = new (::rFHOK_)
      OperatingComponent(*pG22523, OperatingComponent::AND, TRUE);
   OperatingComponent* pKG22520_G22524 = new (::rFHOK_)
      OperatingComponent(*pG22524, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG22521_G22509 = new (::rFHOK_)
      OperatingComponent(*pG22509, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22522_G22509 = new (::rFHOK_)
      OperatingComponent(*pG22509, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22523_G22509 = new (::rFHOK_)
      OperatingComponent(*pG22509, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG22524_G22509 = new (::rFHOK_)
      OperatingComponent(*pG22509, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG22509_C136 = new (::rFHOK_)
      OperatingComponent(*pC136, OperatingComponent::OR, FALSE);
   
   // Connect the operating components to the parent/child relationships
   pG22500->addChild(*pKG22500_G22560);
   pG22500->addChild(*pKG22500_G32000);

   pG22560->addChild(*pKG22560_G22550);
   pG22560->addChild(*pKG22560_G22551);
   pG22560->addChild(*pKG22560_G22552);

   pG22550->addChild(*pKG22550_G22530);
   pG22550->addChild(*pKG22550_G22531);
   pG22550->addChild(*pKG22550_G22532);

   pG22551->addChild(*pKG22551_G22533);
   pG22551->addChild(*pKG22551_G22534);
   pG22551->addChild(*pKG22551_G22535);
   pG22551->addChild(*pKG22551_G22536);

   pG22552->addChild(*pKG22552_G22537);
   pG22552->addChild(*pKG22552_G22538);
   pG22552->addChild(*pKG22552_G22539);
   pG22552->addChild(*pKG22552_G22540);

   // Low Urgency Alarms
   pG22530->addChild(*pKG22530_G22521);
   pG22530->addChild(*pKG22530_G22522);
   pG22530->addChild(*pKG22530_G22510);
   pG22531->addChild(*pKG22531_G22511);
   pG22531->addChild(*pKG22531_G22521);
   pG22532->addChild(*pKG22532_G22511);
   pG22532->addChild(*pKG22532_G22512);
   pG22532->addChild(*pKG22532_G22520);

   // Medium Urgency Alarms
   pG22533->addChild(*pKG22533_G22513);
   pG22533->addChild(*pKG22533_G22520);
   pG22534->addChild(*pKG22534_G22510);
   pG22534->addChild(*pKG22534_G22523);
   pG22535->addChild(*pKG22535_G22511);
   pG22535->addChild(*pKG22535_G22522);
   pG22536->addChild(*pKG22536_G22512);
   pG22536->addChild(*pKG22536_G22521);

   // High Urgency Alarms
   pG22537->addChild(*pKG22537_G22514);
   pG22537->addChild(*pKG22537_G22524);
   pG22538->addChild(*pKG22538_G22511);
   pG22538->addChild(*pKG22538_G22512);
   pG22538->addChild(*pKG22538_G22513);
   pG22538->addChild(*pKG22538_G22523);
   pG22539->addChild(*pKG22539_G22512);
   pG22539->addChild(*pKG22539_G22513);
   pG22539->addChild(*pKG22539_G22522);
   pG22540->addChild(*pKG22540_G22513);
   pG22540->addChild(*pKG22540_G22521);


   // Count Constraint / VHM connectivity
   pG22510->addChild(*pKG22510_G22511);
   pG22510->addChild(*pKG22510_G22512);
   pG22510->addChild(*pKG22510_G22513);
   pG22510->addChild(*pKG22510_G22514);
   pG22511->addChild(*pKG22511_G22508);
   pG22512->addChild(*pKG22512_G22508);
   pG22513->addChild(*pKG22513_G22508);
   pG22514->addChild(*pKG22514_G22508);

   pG22520->addChild(*pKG22520_G22521);
   pG22520->addChild(*pKG22520_G22522);
   pG22520->addChild(*pKG22520_G22523);
   pG22520->addChild(*pKG22520_G22524);
   pG22521->addChild(*pKG22521_G22509);
   pG22522->addChild(*pKG22522_G22509);
   pG22523->addChild(*pKG22523_G22509);
   pG22524->addChild(*pKG22524_G22509);


   // Group / Condition connectivity
   pG22508->addChild(*pKG22508_C135);
   pG22509->addChild(*pKG22509_C136);


   // Connect the operating components to the child/parent relationships
   pG22560->addParent(*pG22500);
   pG32000->addParent(*pG22500);

   pG22550->addParent(*pG22560);
   pG22551->addParent(*pG22560);
   pG22552->addParent(*pG22560);

   pG22530->addParent(*pG22550);
   pG22531->addParent(*pG22550);
   pG22532->addParent(*pG22550);

   pG22533->addParent(*pG22551);
   pG22534->addParent(*pG22551);
   pG22535->addParent(*pG22551);
   pG22536->addParent(*pG22551);

   pG22537->addParent(*pG22552);
   pG22538->addParent(*pG22552);
   pG22539->addParent(*pG22552);
   pG22540->addParent(*pG22552);

   // Low Urgency Alarms
   pG22521->addParent(*pG22530);
   pG22522->addParent(*pG22530);
   pG22510->addParent(*pG22530);
   pG22511->addParent(*pG22531);
   pG22521->addParent(*pG22531);
   pG22511->addParent(*pG22532);
   pG22512->addParent(*pG22532);
   pG22520->addParent(*pG22532);


   // Medium Urgency Alarms
   pG22513->addParent(*pG22533);
   pG22520->addParent(*pG22533);
   pG22510->addParent(*pG22534);
   pG22523->addParent(*pG22534);
   pG22511->addParent(*pG22535);
   pG22522->addParent(*pG22535);
   pG22512->addParent(*pG22536);
   pG22521->addParent(*pG22536);

   // High Urgency Alarms
   pG22514->addParent(*pG22537);
   pG22524->addParent(*pG22537);
   pG22511->addParent(*pG22538);
   pG22512->addParent(*pG22538);
   pG22513->addParent(*pG22538);
   pG22523->addParent(*pG22538);
   pG22512->addParent(*pG22539);
   pG22513->addParent(*pG22539);
   pG22522->addParent(*pG22539);
   pG22513->addParent(*pG22540);
   pG22521->addParent(*pG22540);

   // Count Constraint / VHM connectivity
   pG22511->addParent(*pG22510);
   pG22512->addParent(*pG22510);
   pG22513->addParent(*pG22510);
   pG22514->addParent(*pG22510);
   pG22508->addParent(*pG22511);
   pG22508->addParent(*pG22512);
   pG22508->addParent(*pG22513);
   pG22508->addParent(*pG22514);

   pG22521->addParent(*pG22520);
   pG22522->addParent(*pG22520);
   pG22523->addParent(*pG22520);
   pG22524->addParent(*pG22520);
   pG22509->addParent(*pG22521);
   pG22509->addParent(*pG22522);
   pG22509->addParent(*pG22523);
   pG22509->addParent(*pG22524);

   pC135->addParent(*pG22508);
   pC135->addParent(*pG22520);
   pC136->addParent(*pG22509);
   pC136->addParent(*pG22510);



   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA225_G22550Aug = new (::rFHCA_)
      ConditionAugmentation(*pA225LowInfo,    *pA225, *pG22550);
   ConditionAugmentation* pA225_G22551Aug = new (::rFHCA_)
      ConditionAugmentation(*pA225MediumInfo, *pA225, *pG22551);
   ConditionAugmentation* pA225_G22552Aug = new (::rFHCA_)
      ConditionAugmentation(*pA225HighInfo,   *pA225, *pG22552);
                             
   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA225_A255Aug = new (::rFHDA_)
      DependentAugmentation(*pA225DepInfo1, *pA225, *pA255);
   DependentAugmentation* pA225_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA225DepInfo2, *pA225, *pA260);
   DependentAugmentation* pA225_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA225DepInfo3, *pA225, *pA235);

   pG22500->setCausedAlarm(pA225);           // Set the caused state

   pA225->addAugmentation(*pA225_G22550Aug);   // Add the condition augmented state
   pG22550->addAugmentation(*pA225_G22550Aug);

   pA225->addAugmentation(*pA225_G22551Aug);   // Add the condition augmented state
   pG22551->addAugmentation(*pA225_G22551Aug);

   pA225->addAugmentation(*pA225_G22552Aug);   // Add the condition augmented state
   pG22552->addAugmentation(*pA225_G22552Aug);


   pA225->addAugmentation(*pA225_A255Aug);   // Add the dependent augmented state
   pA255->addAugmentation(*pA225_A255Aug);

   pA225->addAugmentation(*pA225_A260Aug);   // Add the dependent augmented state
   pA260->addAugmentation(*pA225_A260Aug);

   pA225->addAugmentation(*pA225_A235Aug);   // Add the dependent augmented state
   pA235->addAugmentation(*pA225_A235Aug);


   //======================================================
   //
   //   A230:  High Ventilator Pressure
   //   $[05107] $[05108] $[05109] $[05110] $[05111] $[05112] $[05113]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA230Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    90, MESSAGE_NAME_NULL,  
         HIGH_PRES_REMEDY_MSG);

   AlarmInformation* pA230LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    90, HIGH_PRES_VENT_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA230MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 170, HIGH_PRES_VENT_MED_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA230HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,   340, HIGH_PRES_VENT_HIGH_MSG, 
         MESSAGE_NAME_NULL);


   DependentInformation* pA230DepInfo1  = new (::rFHDI_) 
      DependentInformation(4,5,20, LOW_MAND_TIDAL_VOL_MSG, MESSAGE_NAME_NULL);   
   DependentInformation* pA230DepInfo2  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG,     MESSAGE_NAME_NULL);   
   DependentInformation* pA230DepInfo3  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG,     MESSAGE_NAME_NULL);

   // Define current breath count constraints
   CountConstraint* pG23001Constraint = new (::rFHCC_) 
      CountConstraint(Constraint::EQ, Constraint::EQ, 
                      CalcFunctions::Return1Event, NULL);
   CountConstraint* pG23002Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ, 
                      CalcFunctions::Return2Events, NULL);
   CountConstraint* pG23003Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ, 
                      CalcFunctions::Return3Events, NULL);
   CountConstraint* pG23005Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return1Event, NULL);

   // Create Constraint operating groups 
   OperatingGroup* pG23001 = new (::rFHOG_)
      OperatingGroup( G23001,pG23001Constraint);
      AlarmOperand::AddOperand(pG23001);

   OperatingGroup* pG23002 = new (::rFHOG_)
      OperatingGroup( G23002,pG23002Constraint);
      AlarmOperand::AddOperand(pG23002);

   OperatingGroup* pG23003 = new (::rFHOG_)
      OperatingGroup( G23003,pG23003Constraint);
      AlarmOperand::AddOperand(pG23003);

   OperatingGroup* pG23005 = new (::rFHOG_)
      OperatingGroup( G23005,pG23005Constraint);
      AlarmOperand::AddOperand(pG23005);


   // Create a Violation History Manager
   VHMCurrBreathCountDur* pG23004Vhm = new (::rFHVHMCBCD_) 
      VHMCurrBreathCountDur(CalcFunctions::ReturnMax60or3ApneaInts);

   // Create a VHM operating group 
   OperatingGroup* pG23004 = new (::rFHOG_)
      OperatingGroup( G23004,NULL,NULL,pG23004Vhm);
      AlarmOperand::AddOperand(pG23004);


   
   
   Alarm* pA230 = new (::rFHA_)                // Define the alarm
      Alarm(A230, *pA230Info, *pG23000,  C325, TRUE,
            HIGH_PRES_VENT_MSG);

   Alarm::AddAlarm(pA230);                   // Add the alarm
   
   

   // Create the operating components for this alarm
   OperatingComponent* pKG23000_G23005 = new (::rFHOK_)
      OperatingComponent(*pG23005, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG23000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);
   OperatingComponent* pKG23001_G23004 = new (::rFHOK_)
      OperatingComponent(*pG23004, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG23002_G23004 = new (::rFHOK_)
      OperatingComponent(*pG23004, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG23003_G23004 = new (::rFHOK_)
      OperatingComponent(*pG23004, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG23004_C325   = new (::rFHOK_)
      OperatingComponent(*pC325,   OperatingComponent::OR, FALSE); 
   OperatingComponent* pKG23005_G23004 = new (::rFHOK_)
      OperatingComponent(*pG23004, OperatingComponent::OR, FALSE);

   
   // Connect the operating components to the parent/child relationships
   pG23000->addChild(*pKG23000_G23005);
   pG23000->addChild(*pKG23000_G32000);
   pG23001->addChild(*pKG23001_G23004);
   pG23002->addChild(*pKG23002_G23004);
   pG23003->addChild(*pKG23003_G23004);
   pG23004->addChild(*pKG23004_C325);
   pG23005->addChild(*pKG23005_G23004);

   pG23005->addParent(*pG23000);
   pG32000->addParent(*pG23000);
   pG23004->addParent(*pG23001);
   pG23004->addParent(*pG23002);
   pG23004->addParent(*pG23003);
   pC325  ->addParent(*pG23004);
   pG23004->addParent(*pG23005);


   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA230_G23001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA230LowInfo,    *pA230, *pG23001);
   ConditionAugmentation* pA230_G23002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA230MediumInfo, *pA230, *pG23002);
   ConditionAugmentation* pA230_G23003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA230HighInfo,   *pA230, *pG23003);
                             
   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA230_A255Aug = new (::rFHDA_)
      DependentAugmentation(*pA230DepInfo1, *pA230, *pA255);
   DependentAugmentation* pA230_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA230DepInfo2, *pA230, *pA260);
   DependentAugmentation* pA230_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA230DepInfo3, *pA230, *pA235);


   pG23000->setCausedAlarm(pA230);           // Set the caused state

   pA230->addAugmentation(*pA230_G23001Aug);   // Add the condition augmented state
   pG23001->addAugmentation(*pA230_G23001Aug);

   pA230->addAugmentation(*pA230_G23002Aug);   // Add the condition augmented state
   pG23002->addAugmentation(*pA230_G23002Aug);

   pA230->addAugmentation(*pA230_G23003Aug);   // Add the condition augmented state
   pG23003->addAugmentation(*pA230_G23003Aug);


   pA230->addAugmentation(*pA230_A255Aug);   // Add the dependent augmented state
   pA255->addAugmentation(*pA230_A255Aug);

   pA230->addAugmentation(*pA230_A260Aug);   // Add the dependent augmented state
   pA260->addAugmentation(*pA230_A260Aug);

   pA230->addAugmentation(*pA230_A235Aug);   // Add the dependent augmented state
   pA235->addAugmentation(*pA230_A235Aug);

   //======================================================
   //
   //   A241:  Inoperative Battery   
   //   $[05175]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA241Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 120, NON_FUNC_BATTERY_ANALYSIS_MSG, 
            NON_FUNC_BATTERY_REMEDY_MSG);

   Alarm* pA241 = new (::rFHA_)             // Define the alarm
      Alarm(A241, *pA241Info, *pG24100,  OPERAND_NAME_NULL,
            FALSE, INOPERATIVE_BATTERY_MSG, TRUE);

   Alarm::AddAlarm(pA241);                  // Add the alarm
   
   

   // Create the operating component for this alarm 
   OperatingComponent* pKG24100_C262 = new (::rFHOK_)
      OperatingComponent(*pC262, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG24100_C263 = new (::rFHOK_)
      OperatingComponent(*pC262, OperatingComponent::OR, FALSE);

   
   pC262->addParent(*pG24100);        // Assign a parent group to the condition
   pC263->addParent(*pG24100);        // Assign a parent group to the condition

   pG24100->addChild(*pKG24100_C262); // Link the operating group to the parent
   pG24100->addChild(*pKG24100_C263); // Link the operating group to the parent

   pG24100->setCausedAlarm(pA241);    // Set the caused state


   //======================================================
   //
   //   A243:  Low Battery   
   //   $[05177]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA243Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 130, LOW_BATTERY_ANALYSIS_MSG, 
            LOW_BATTERY_REMEDY_MSG);

   Alarm* pA243 = new (::rFHA_)             // Define the alarm
      Alarm(A243, *pA243Info, *pG24300,  OPERAND_NAME_NULL,
            FALSE, LOW_BATTERY_MSG, TRUE);

   Alarm::AddAlarm(pA243);                  // Add the alarm
   
      // Create the operating component for this alarm 
   OperatingComponent* pKG24300_G24000 = new (::rFHOK_)
      OperatingComponent(*pG24000, OperatingComponent::OR, TRUE);
   OperatingComponent* pKG24300_G24100 = new (::rFHOK_)
      OperatingComponent(*pG24100, OperatingComponent::AND, TRUE);
   OperatingComponent* pKG24300_C261 = new (::rFHOK_)
      OperatingComponent(*pC261, OperatingComponent::AND, FALSE);

   // Link parents to children
   pG24000->addParent(*pG24300);
   pG24100->addParent(*pG24300);
   pC261  ->addParent(*pG24300);

   // Link children to parents
   pG24300->addChild(*pKG24300_G24000);
   pG24300->addChild(*pKG24300_G24100);
   pG24300->addChild(*pKG24300_C261);

   pG24300->setCausedAlarm(pA243);    // Set the caused state


   //======================================================
   //
   //   A265:  Low Spontaneous Tidal Volume
   //   $[05140] $[05142] $[05143] $[05144] $[05145] $[05146]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA265Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    50, MESSAGE_NAME_NULL,  
         CHECK_PATIENT_SETTINGS_MSG);

   AlarmInformation* pA265LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    50, LOW_SPONT_TIDAL_VOL_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA265MediumInfo = new (::rFHAI_)            
      AlarmInformation(Alarm::MEDIUM_URGENCY, 60, LOW_SPONT_TIDAL_VOL_MED_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA265HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,  210, LOW_SPONT_TIDAL_VOL_HIGH_MSG,
         MESSAGE_NAME_NULL);

   DependentInformation* pA265DepInfo1  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG, MESSAGE_NAME_NULL);   
   DependentInformation* pA265DepInfo2  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG, MESSAGE_NAME_NULL);   


   // Define count constraints.
   // Last 2 or 3 breaths meet detection criteria
   CountConstraint* pG26501Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::LT,
                      CalcFunctions::Return2Events, CalcFunctions::Return4Events); 

   // Last 4 to 9 breaths meet detection criteria
   CountConstraint* pG26502Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::LT, 
                      CalcFunctions::Return4Events, CalcFunctions::Return10Events);

   // Last 10 breaths meet detection criteria
   CountConstraint* pG26503Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ,
                      CalcFunctions::Return10Events, NULL);

   // >= 2 breaths meet detection criteria
   CountConstraint* pG26505Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ,
                      CalcFunctions::Return2Events, NULL);


   
   // Create operating groups for the above constraints
   OperatingGroup* pG26501 = new (::rFHOG_)
      OperatingGroup( G26501,pG26501Constraint);
      AlarmOperand::AddOperand(pG26501);

   OperatingGroup* pG26502 = new (::rFHOG_)
      OperatingGroup( G26502,pG26502Constraint);
      AlarmOperand::AddOperand(pG26502);

   OperatingGroup* pG26503 = new (::rFHOG_)
      OperatingGroup( G26503,pG26503Constraint);
      AlarmOperand::AddOperand(pG26503);

   OperatingGroup* pG26505 = new (::rFHOG_)
      OperatingGroup( G26505,pG26505Constraint);
      AlarmOperand::AddOperand(pG26505);
   

   // Create a Breath count Violation History Manager, delete breaths > 10
   VHMPrevBreathCountCardinal* pG26504Vhm = new (::rFHVHMPBCC_) 
      VHMPrevBreathCountCardinal(CalcFunctions::Return10Events);

   OperatingGroup* pG26504 = new (::rFHOG_)
      OperatingGroup( G26504, NULL, NULL, pG26504Vhm);
      AlarmOperand::AddOperand(pG26504);


   


   Alarm* pA265 = new (::rFHA_)              // Define the alarm
      Alarm(A265, *pA265Info, *pG26500,  C321, TRUE,
            LOW_SPONT_TIDAL_VOL_MSG);
   
   Alarm::AddAlarm(pA265);                   // Add the alarm
   
   


   // Create the operating components for this alarm.  

   OperatingComponent* pKG26500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000,OperatingComponent::AND,  TRUE);

   OperatingComponent* pKG26500_G26505 = new (::rFHOK_)
      OperatingComponent(*pG26505, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG26501_G26504 = new (::rFHOK_)
      OperatingComponent(*pG26504, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG26502_G26504 = new (::rFHOK_)
      OperatingComponent(*pG26504, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG26503_G26504 = new (::rFHOK_)
      OperatingComponent(*pG26504, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG26505_G26504 = new (::rFHOK_)
      OperatingComponent(*pG26504, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG26504_C321   = new (::rFHOK_)
      OperatingComponent(*pC321,   OperatingComponent::OR, FALSE);   



   // Connect the operating components to the parent/child relationships
   pG26500->addChild(*pKG26500_G26505);
   pG26500->addChild(*pKG26500_G32000);
   pG26505->addChild(*pKG26505_G26504);
   pG26501->addChild(*pKG26501_G26504);
   pG26502->addChild(*pKG26502_G26504);
   pG26503->addChild(*pKG26503_G26504);
   pG26504->addChild(*pKG26504_C321);

   pG32000->addParent(*pG26500);
   pG26505->addParent(*pG26500);
   pG26504->addParent(*pG26501);
   pG26504->addParent(*pG26502);
   pG26504->addParent(*pG26503);
   pG26504->addParent(*pG26505);
   pC321  ->addParent(*pG26504);


   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA265_G26501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA265LowInfo,    *pA265, *pG26501);
   ConditionAugmentation* pA265_G26502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA265MediumInfo, *pA265, *pG26502);
   ConditionAugmentation* pA265_G26503Aug = new (::rFHCA_)
      ConditionAugmentation(*pA265HighInfo,   *pA265, *pG26503);
   
   // Create the dependent augmentations for this alarm
   DependentAugmentation* pA265_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA265DepInfo1, *pA265, *pA260);
   DependentAugmentation* pA265_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA265DepInfo2, *pA265, *pA235);



   pG26500->setCausedAlarm(pA265);           // Set the caused state
                                    
   pA265->addAugmentation(*pA265_G26501Aug); // Add the cond augmented state
   pG26501->addAugmentation(*pA265_G26501Aug);

   pA265->addAugmentation(*pA265_G26502Aug); // Add the cond augmented state
   pG26502->addAugmentation(*pA265_G26502Aug);

   pA265->addAugmentation(*pA265_G26503Aug); // Add the cond augmented state
   pG26503->addAugmentation(*pA265_G26503Aug);

   pA265->addAugmentation(*pA265_A260Aug);   // Add the dep  augmented state
   pA260->addAugmentation(*pA265_A260Aug);

   pA265->addAugmentation(*pA265_A235Aug);   // Add the dep  augmented state
   pA235->addAugmentation(*pA265_A235Aug);


   
   // Create dependent augmentation for A235_A265, defined previously.
   DependentAugmentation* pA235_A265Aug = new (::rFHDA_)
      DependentAugmentation(*pA235DepInfo2, *pA235, *pA265);

   pA235->addAugmentation(*pA235_A265Aug);   // Add the dependent augmented state
   pA265->addAugmentation(*pA235_A265Aug);

   // Create dependent augmentations for A260_A265, defined previously.
   DependentAugmentation* pA260_A265Aug = new (::rFHDA_)
      DependentAugmentation(*pA260DepInfo2, *pA260, *pA265);

   pA260->addAugmentation(*pA260_A265Aug);   // Add the dependent augmented state
   pA265->addAugmentation(*pA260_A265Aug);





   //======================================================
   //
   //   A270:  Low Delivered O2%
   //   $[05148]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA270Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,   120, LOW_O2_ANALYSIS_MSG,   LOW_O2_REMEDY_MSG);


   // Define the Temporal constraints and VHM managers
   TemporalConstraint* pG27002Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
                         CalcFunctions::Return30Sec,NULL);  // >= 30 sec

   TemporalConstraint* pG27002ResetConstraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ,
                         CalcFunctions::Return30Sec,NULL);  // >= 30 sec


   // Create Constraint operating groups 
   OperatingGroup* pG27002 = new (::rFHOG_)
      OperatingGroup( G27002, pG27002Constraint, 
                    pG27002ResetConstraint);
      AlarmOperand::AddOperand(pG27002);



   // Create Violation History Managers
   VHMTemporal*  pG27001Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create VHM operating group
   OperatingGroup* pG27001 = new (::rFHOG_)
      OperatingGroup( G27001,NULL,NULL,pG27001Vhm);
      AlarmOperand::AddOperand(pG27001);


   // Define the alarm.  C155 is the actual parameter being sent to the
   // constructor to set the Alarm::resetConditionName_ attribute.  When this
   // attribute is non-null, the Alarm will become inactive when one of the
   // Alarm objects causing Loss of Patient Data Monitoring becomes active.
   // When the Alarm causing Loss of Patient Data Monitoring autoresets, all
   // Patient Data Alarm detection algorithms will be reset.
   Alarm* pA270 = new (::rFHA_)              // Define the alarm
      Alarm(A270, *pA270Info, *pG27000,  C155, TRUE,
            LOW_O2_MSG);

   Alarm::AddAlarm(pA270);                    // Add the alarm
   
   
   
   // Create the operating components for this alarm.  

  
   OperatingComponent* pKG27000_G27002 = new (::rFHOK_)
      OperatingComponent(*pG27002, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG27000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG27001_C205   = new (::rFHOK_)
      OperatingComponent(*pC205,   OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG27001_C155   = new (::rFHOK_)
      OperatingComponent(*pC155,   OperatingComponent::AND,  FALSE);

   OperatingComponent* pKG27001_G4000 = new (::rFHOK_)
      OperatingComponent(*pG4000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG27002_G27001 = new (::rFHOK_)    //  Temporal
      OperatingComponent(*pG27001, OperatingComponent::OR,  FALSE);



   // Connect the operating components to the parent/child relationships

   pG27000->addChild(*pKG27000_G27002);
   pG27000->addChild(*pKG27000_G32000);

   pG27001->addChild(*pKG27001_C205);
   pG27001->addChild(*pKG27001_C155);
   pG27001->addChild(*pKG27001_G4000);

   pG27002->addChild(*pKG27002_G27001);
   
   pG27002->addParent(*pG27000);
   pG32000->addParent(*pG27000);
   pC205  ->addParent(*pG27001);
   pC155  ->addParent(*pG27001);
   pG4000 ->addParent(*pG27001);
   pG27001->addParent(*pG27002);


   pG27000->setCausedAlarm(pA270);           // Set the caused state


   //======================================================
   //
   //   A280:  No Air Supply   
   //   $[05151] $[05152] $[05153] $[05154] $[05155] $[05156]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA280Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,  200, MESSAGE_NAME_NULL, 
         MESSAGE_NAME_NULL);

   AlarmInformation* pA280Info1 = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,   190, 
         COMPRESSOR_INOP_WITH_100_O2_MSG,   NO_AIR_SUPPLY_REMEDY_MSG);
   AlarmInformation* pA280Info2 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,  260,
         COMPRESSOR_INOP_EXCEPT_100_O2_MSG,   CHECK_PATIENT_AIR_SOURCE_MSG);
   AlarmInformation* pA280Info3 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,  270, VENTILATION_EXCEPT_100_O2_MSG,
         CHECK_PATIENT_AIR_SOURCE_MSG);
   AlarmInformation* pA280Info4 = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,  200, VENTILATION_WITH_100_O2_MSG,
         NO_AIR_SUPPLY_REMEDY_MSG);
   AlarmInformation* pA280Info5 = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,   40, NO_VENTILATION_MSG, 
         NO_VENT_CHECK_GAS_MSG, AlarmResponses::PatDataOff,
         AlarmResponses::PatDataOn);



   // !!!!!!!!! NOTE !!!!!!!!!
   // Because of definition ordering constraints, G28000 and its constraints
   // are defined in the No O2 Supply alarm.
   // !!!!!!!!! NOTE !!!!!!!!!



   // Create Violation History Managers
   VHMTemporal*  pG28006Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create VHM operating group
   OperatingGroup* pG28006 = new (::rFHOG_)
      OperatingGroup( G28006,NULL,NULL,pG28006Vhm);
      AlarmOperand::AddOperand(pG28006);



   //==========================================================================
   // Define the alarm
   //==========================================================================
   Alarm* pA280 = new (::rFHA_)
      Alarm(A280, *pA280Info, *pG28000,  OPERAND_NAME_NULL,
            FALSE, NO_AIR_SUPPLY_MSG, TRUE);


   Alarm::AddAlarm(pA280);                      // Add the alarm
   
   

   //==========================================================================
   // Create the operating components for this alarm.  
   // This is sorted by the operating components in groups 28000-28005.
   //==========================================================================
   OperatingComponent* pKG28000_G28006 = new (::rFHOK_)
      OperatingComponent(*pG28006, OperatingComponent::OR, FALSE);    // G28000


   OperatingComponent* pKG28001_G29000 = new (::rFHOK_)
      OperatingComponent(*pG29000, OperatingComponent::OR, FALSE);    // G28001

   OperatingComponent* pKG28001_G28500 = new (::rFHOK_)
      OperatingComponent(*pG28500, OperatingComponent::AND, TRUE);    // G28001

   OperatingComponent* pKG28001_C210   = new (::rFHOK_)
      OperatingComponent(*pC210,   OperatingComponent::AND,FALSE);      // G28001



   OperatingComponent* pKG28002_G29000 = new (::rFHOK_)
      OperatingComponent(*pG29000, OperatingComponent::OR, FALSE);    // G28002

   OperatingComponent* pKG28002_G28500 = new (::rFHOK_)
      OperatingComponent(*pG28500, OperatingComponent::AND, TRUE);    // G28002

   OperatingComponent* pKG28002_C225   = new (::rFHOK_)
      OperatingComponent(*pC225,   OperatingComponent::AND,FALSE);      // G28002



   OperatingComponent* pKG28003_C145   = new (::rFHOK_)
      OperatingComponent(*pC145,   OperatingComponent::OR, TRUE);       // G28003

   OperatingComponent* pKG28003_G28500 = new (::rFHOK_)
      OperatingComponent(*pG28500, OperatingComponent::AND, TRUE);    // G28003

   OperatingComponent* pKG28003_C225   = new (::rFHOK_)
      OperatingComponent(*pC225,   OperatingComponent::AND,FALSE);      // G28003



   OperatingComponent* pKG28004_C145   = new (::rFHOK_)
      OperatingComponent(*pC145,   OperatingComponent::OR,  TRUE);      // G28004

   OperatingComponent* pKG28004_G28500 = new (::rFHOK_)
      OperatingComponent(*pG28500, OperatingComponent::AND, TRUE);    // G28004

   OperatingComponent* pKG28004_C210   = new (::rFHOK_)
      OperatingComponent(*pC210,   OperatingComponent::AND,FALSE);      // G28004

   OperatingComponent* pKG28005_C145   = new (::rFHOK_)       
      OperatingComponent(*pC145,     OperatingComponent::OR,  TRUE);    // G28007

   OperatingComponent* pKG28005_G29000   = new (::rFHOK_)       
      OperatingComponent(*pG29000,   OperatingComponent::OR, FALSE);  // G28007
                                                                          
   OperatingComponent* pKG28006_C5 = new (::rFHOK_)
      OperatingComponent(*pC5, OperatingComponent::OR, FALSE);         // G28006

   OperatingComponent* pKG28006_G28005 = new (::rFHOK_)
      OperatingComponent(*pG28005, OperatingComponent::AND, FALSE);    // G28006
      


   //==========================================================================
   // Connect the operating components to the parent/child relationships.
   //==========================================================================

   pG28000->addChild(*pKG28000_G28006);      // G28000               
   pG28006->addParent(*pG28000);

                                             
   pG28001->addChild(*pKG28001_G29000);      // G28001
   pG29000->addParent(*pG28001);

   pG28001->addChild(*pKG28001_G28500);      // G28001
   pG28500->addParent(*pG28001);

   pG28001->addChild(*pKG28001_C210);        // G28001
   pC210  ->addParent(*pG28001);


   pG28002->addChild(*pKG28002_G29000);      // G28002
   pG29000->addParent(*pG28002);

   pG28002->addChild(*pKG28002_G28500);      // G28002
   pG28500->addParent(*pG28002);

   pG28002->addChild(*pKG28002_C225);        // G28002
   pC225  ->addParent(*pG28002);


   pG28003->addChild(*pKG28003_C145);        // G28003
   pC145  ->addParent(*pG28003);

   pG28003->addChild(*pKG28003_G28500);      // G28003
   pG28500->addParent(*pG28003);

   pG28003->addChild(*pKG28003_C225);        // G28003
   pC225  ->addParent(*pG28003);


   pG28004->addChild(*pKG28004_C145);        // G28004
   pC145  ->addParent(*pG28004);

   pG28004->addChild(*pKG28004_G28500);      // G28004
   pG28500->addParent(*pG28004);                      

   pG28004->addChild(*pKG28004_C210);        // G28004
   pC210  ->addParent(*pG28004);


   pG28005->addChild(*pKG28005_C145);        // G28005
   pC145  ->addParent(*pG28005);

   pG28005->addChild(*pKG28005_G29000);      // G28005
   pG29000->addParent(*pG28005);


   pG28006->addChild(*pKG28006_C5);          // G28006               
   pC5    ->addParent(*pG28006);

   pG28006->addChild(*pKG28006_G28005);      // G28006
   pG28005->addParent(*pG28006);


   //==========================================================================
   // Set the caused state for this alarm
   //==========================================================================
   pG28000->setCausedAlarm(pA280);

   // Set the condition augmentations for this alarm
   ConditionAugmentation* pA280_G28001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA280Info1,  *pA280, *pG28001);
   ConditionAugmentation* pA280_G28002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA280Info2,  *pA280, *pG28002);
   ConditionAugmentation* pA280_G28003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA280Info3,  *pA280, *pG28003);
   ConditionAugmentation* pA280_G28004Aug = new (::rFHCA_)
      ConditionAugmentation(*pA280Info4,  *pA280, *pG28004);
   ConditionAugmentation* pA280_G28500Aug = new (::rFHCA_)
      ConditionAugmentation(*pA280Info5,  *pA280, *pG28500);

   pA280->addAugmentation(*pA280_G28001Aug); // Add the augmented state
   pG28001->addAugmentation(*pA280_G28001Aug);

   pA280->addAugmentation(*pA280_G28002Aug); // Add the augmented state
   pG28002->addAugmentation(*pA280_G28002Aug);

   pA280->addAugmentation(*pA280_G28003Aug); // Add the augmented state
   pG28003->addAugmentation(*pA280_G28003Aug);

   pA280->addAugmentation(*pA280_G28004Aug); // Add the augmented state
   pG28004->addAugmentation(*pA280_G28004Aug);

   pA280->addAugmentation(*pA280_G28500Aug); // Add the augmented state
   pG28500->addAugmentation(*pA280_G28500Aug);


   //======================================================
   //
   //   A290:  Compressor Inoperative
   //   $[05161] $[05162] $[05163] $[05164]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA290Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 180, 
         COMPRESSOR_INOPERATIVE_ANALYSIS_MSG, MESSAGE_NAME_NULL);

   AlarmInformation* pA290Info1 = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 160, MESSAGE_NAME_NULL, 
         REPLACE_COMPRESSOR_MSG);
   AlarmInformation* pA290Info2 = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 180, 
      COMPRESSOR_NOT_DURING_LOW_AC_POWER_MSG, MESSAGE_NAME_NULL);
   AlarmInformation* pA290Info3 = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 170, 
      COMPRESSOR_NOT_DURING_AC_POWER_LOSS_MSG, MESSAGE_NAME_NULL);


   // Define Temporal constraints for this alarm.
   TemporalConstraint* pG29001Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
                         CalcFunctions::Return10Sec, NULL);


   // Create a Temporal Constraint operating group.
   OperatingGroup* pG29001 = new (::rFHOG_)
      OperatingGroup( G29001,pG29001Constraint);
      AlarmOperand::AddOperand(pG29001);


   
   // Create a Temporal Violation History Manager
   VHMTemporal*  pG29004Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create an VHM operating group 
   OperatingGroup* pG29004 = new (::rFHOG_)
      OperatingGroup( G29004,NULL,NULL,pG29004Vhm);
      AlarmOperand::AddOperand(pG29004);



  
   

   // Define the alarm
   Alarm* pA290 = new (::rFHA_)
      Alarm(A290, *pA290Info, *pG29000,  OPERAND_NAME_NULL,
            FALSE, COMPRESSOR_INOPERATIVE_MSG, TRUE);


   Alarm::AddAlarm(pA290);                  // Add the alarm
   
   

   // Create the operating components for this alarm.  
   OperatingComponent* pKG29000_C145   = new (::rFHOK_)
      OperatingComponent(*pC145,   OperatingComponent::OR, FALSE);
   
   OperatingComponent* pKG29000_C140   = new (::rFHOK_)
      OperatingComponent(*pC140,  OperatingComponent::AND,  FALSE);
   
   OperatingComponent* pKG29001_G29004 = new (::rFHOK_)
      OperatingComponent(*pG29004, OperatingComponent::OR, FALSE);
   
   OperatingComponent* pKG29002_G25000 = new (::rFHOK_)
      OperatingComponent(*pG25000, OperatingComponent::OR, FALSE);
   
   OperatingComponent* pKG29003_G24000 = new (::rFHOK_)
      OperatingComponent(*pG24000, OperatingComponent::OR, FALSE);
   
   OperatingComponent* pKG29004_G25000 = new (::rFHOK_)
      OperatingComponent(*pG25000, OperatingComponent::OR,  TRUE);
   
   OperatingComponent* pKG29004_G24000 = new (::rFHOK_)
      OperatingComponent(*pG24000, OperatingComponent::AND, TRUE);
   
   OperatingComponent* pKG29004_C140 = new (::rFHOK_)
      OperatingComponent(*pC140, OperatingComponent::AND,  FALSE);
   
   

   // Connect the operating components to the parent/child relationships.
   pG29000->addChild(*pKG29000_C145);
   pC145  ->addParent(*pG29000);
   
   pG29000->addChild(*pKG29000_C140);
   pC140  ->addParent(*pG29000);
   
   pG29001->addChild(*pKG29001_G29004);
   pG29004->addParent(*pG29001);
   
   pG29002->addChild(*pKG29002_G25000);
   pG25000->addParent(*pG29002);
   
   pG29003->addChild(*pKG29003_G24000);
   pG24000->addParent(*pG29003);
   
   pG29004->addChild(*pKG29004_G25000);
   pG25000->addParent(*pG29004);
   
   pG29004->addChild(*pKG29004_G24000);
   pG24000->addParent(*pG29004);

   pG29004->addChild(*pKG29004_C140);
   pC140->addParent(*pG29004);

   
   // Set the caused state for this alarm
   pG29000->setCausedAlarm(pA290);

   // Set the condition augmentations for this alarm
   ConditionAugmentation* pA290_G29001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA290Info1,  *pA290, *pG29001);
   ConditionAugmentation* pA290_G29002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA290Info2,  *pA290, *pG29002);
   ConditionAugmentation* pA290_G29003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA290Info3,  *pA290, *pG29003);

   pA290->addAugmentation(*pA290_G29001Aug); // Add the augmented state
   pG29001->addAugmentation(*pA290_G29001Aug);

   pA290->addAugmentation(*pA290_G29002Aug); // Add the augmented state
   pG29002->addAugmentation(*pA290_G29002Aug);

   pA290->addAugmentation(*pA290_G29003Aug); // Add the augmented state
   pG29003->addAugmentation(*pA290_G29003Aug);

   


   //======================================================
   //
   //   A295:  Circuit Disconnect #1
   //   $[05165]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA295Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 60, 
         NO_VENTILATION_SAFETY_VALVE_CLOSED_MSG, 
         CIRCUIT_DISCONNECT_REMEDY_MSG, AlarmResponses::PatDataOff,
         AlarmResponses::PatDataOn);

  
   // Define the alarm
   Alarm* pA295 = new (::rFHA_)
      Alarm(A295, *pA295Info, *pG29500,  C193, FALSE,
            CIRCUIT_DISCONNECT_MSG);

   Alarm::AddAlarm(pA295);                  // Add the alarm
   
   
   // Create the operating components for this alarm.  
   OperatingComponent* pKG29500_C193   = new (::rFHOK_)
      OperatingComponent(*pC193,  OperatingComponent::OR,  FALSE);
    
   
   // Connect the operating components to the parent/child relationships.
   pG29500->addChild(*pKG29500_C193);


   pC193  ->addParent(*pG29500);   

   
   // Set the caused state for this alarm
   pG29500->setCausedAlarm(pA295);
   
   

   //======================================================
   //
   //   A300:  Standby Disconnect
   //   $[05166]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA300Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 70, 
         NO_VENTILATION_SAFETY_VALVE_CLOSED_MSG,
         CHECK_PATIENT_VENTILATOR_MSG, AlarmResponses::PatDataOff,
         AlarmResponses::PatDataOn);


   // Define Temporal Constraints for this alarm.
   TemporalConstraint* pG30001Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GT, Constraint::EQ,
                         CalcFunctions::Return10Sec, NULL);

   // Create a Temporal Constraint operating group.
   OperatingGroup* pG30001 = new (::rFHOG_)
      OperatingGroup( G30001,pG30001Constraint);
      AlarmOperand::AddOperand(pG30001);

   
   // Create a Violation History Manager
   VHMTemporal*  pG30000Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   // Create a VHM operating group 
   OperatingGroup* pG30000 = new (::rFHOG_)
      OperatingGroup( G30000,NULL,NULL,pG30000Vhm);
      AlarmOperand::AddOperand(pG30000);


  
   
   
   // Define the alarm
   Alarm* pA300 = new (::rFHA_)
      Alarm(A300, *pA300Info, *pG30000,  C195, FALSE,
            CIRCUIT_DISCONNECT_MSG, TRUE);

   Alarm::AddAlarm(pA300);                  // Add the alarm
   
   
   // Create the operating components for this alarm.  
   OperatingComponent* pKG30000_C195   = new (::rFHOK_)
      OperatingComponent(*pC195,   OperatingComponent::OR, FALSE);
  
   OperatingComponent* pKG30001_G30000 = new (::rFHOK_)
      OperatingComponent(*pG30000, OperatingComponent::OR, FALSE);
   

   // Connect the operating components to the parent/child relationships.
   pG30000->addChild(*pKG30000_C195);
   pG30001->addChild(*pKG30001_G30000);

   pC195  ->addParent(*pG30000);
   pG30000->addParent(*pG30001);
   
   // Set the caused state for this alarm
   pG30000->setCausedAlarm(pA300);
   
   

   //======================================================
   //
   //   A305:  Severe Occlusion   
   //   $[05167]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA305Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,  80, LITTLE_NO_VENTILATION_MSG, 
         SEVERE_OCCLUSION_REMEDY_MSG, AlarmResponses::PatDataOff,
         AlarmResponses::PatDataOn);


   Alarm* pA305 = new (::rFHA_)              // Define the alarm
      Alarm(A305, *pA305Info, *pG30500,  C290, FALSE,
            SEVERE_OCCLUSION_MSG, TRUE);

   Alarm::AddAlarm(pA305);                   // Add the alarm


   // Create the operating component for this alarm 
   OperatingComponent* pKG30500_C290 = new (::rFHOK_)
      OperatingComponent(*pC290, OperatingComponent::OR, FALSE);

   
   pC290  ->addParent(*pG30500);             // Assign a parent group to the condition

   pG30500->addChild(*pKG30500_C290);        // Link the operating group to the parent


   pG30500->setCausedAlarm(pA305);           // Set the caused state



   //======================================================
   //
   //   A310:  Inspiration Too Long
   //   $[05168] $[05169] $[05170] $[05171]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA310Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    30, MESSAGE_NAME_NULL, 
         INSP_TOO_LONG_REMEDY_MSG);

   AlarmInformation* pA310LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    30, INSP_TOO_LONG_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA310MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 40, INSP_TOO_LONG_MED_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA310HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,  190, INSP_TOO_LONG_HIGH_MSG, 
         MESSAGE_NAME_NULL);

   // Define count constraints.
   // Last 2 or 3 breaths meet detection criteria
   CountConstraint* pG31001Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::LT,
                      CalcFunctions::Return2Events, CalcFunctions::Return4Events); 

   // Last 4 to 9 breaths meet detection criteria
   CountConstraint* pG31002Constraint = new (::rFHCC_) 
      CountConstraint(Constraint::GE, Constraint::LT, 
                      CalcFunctions::Return4Events, CalcFunctions::Return10Events);

   // Last 10 breaths meet detection criteria
   CountConstraint* pG31003Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ,
                      CalcFunctions::Return10Events, NULL);

   // >= 2 breaths meet detection criteria
   CountConstraint* pG31005Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return2Events, NULL);

   // Create operating groups for the above constraints 
   OperatingGroup* pG31001 = new (::rFHOG_)
      OperatingGroup( G31001,pG31001Constraint);
      AlarmOperand::AddOperand(pG31001);

   OperatingGroup* pG31002 = new (::rFHOG_)
      OperatingGroup( G31002,pG31002Constraint);
      AlarmOperand::AddOperand(pG31002);

   OperatingGroup* pG31003 = new (::rFHOG_)
      OperatingGroup( G31003,pG31003Constraint);
      AlarmOperand::AddOperand(pG31003);

   OperatingGroup* pG31005 = new (::rFHOG_)
      OperatingGroup( G31005,pG31005Constraint);
      AlarmOperand::AddOperand(pG31005);

   // Create a Breath count Violation History Manager, delete breaths > 10
   VHMCurrBreathCountCardinal* pG31004Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return10Events);

   // Create operating groups for the VHM
   OperatingGroup* pG31004 = new (::rFHOG_)
      OperatingGroup( G31004,NULL, NULL, pG31004Vhm);
      AlarmOperand::AddOperand(pG31004);

   
  

   
   Alarm* pA310 = new (::rFHA_)              // Define the alarm
      Alarm(A310, *pA310Info, *pG31000,  C15, TRUE,
            INSP_TOO_LONG_MSG);

   Alarm::AddAlarm(pA310);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG31000_G31005 = new (::rFHOK_)
      OperatingComponent(*pG31005, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG31000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG31001_G31004 = new (::rFHOK_)
      OperatingComponent(*pG31004, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG31002_G31004 = new (::rFHOK_)
      OperatingComponent(*pG31004, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG31003_G31004 = new (::rFHOK_)
      OperatingComponent(*pG31004, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG31004_C15 = new (::rFHOK_)
      OperatingComponent(*pC15,   OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG31005_G31004 = new (::rFHOK_)
      OperatingComponent(*pG31004, OperatingComponent::OR,  FALSE); 
   

   // Connect the operating components to the parent/child relationships
   pG31000->addChild(*pKG31000_G31005);
   pG31000->addChild(*pKG31000_G32000);
   pG31001->addChild(*pKG31001_G31004);
   pG31002->addChild(*pKG31002_G31004);
   pG31003->addChild(*pKG31003_G31004);
   pG31004->addChild(*pKG31004_C15);
   pG31005->addChild(*pKG31005_G31004);

   pG31005->addParent(*pG31000);
   pG32000->addParent(*pG31000);
   pG31004->addParent(*pG31001);
   pG31004->addParent(*pG31002);
   pG31004->addParent(*pG31003);
   pG31004->addParent(*pG31005);
   pC15   ->addParent(*pG31004);
   pG31004->addParent(*pG31005);

   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA310_G31001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA310LowInfo,    *pA310, *pG31001);
   ConditionAugmentation* pA310_G31002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA310MediumInfo, *pA310, *pG31002);
   ConditionAugmentation* pA310_G31003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA310HighInfo,   *pA310, *pG31003);
   
   
   pG31000->setCausedAlarm(pA310);           // Set the caused state

   pA310->addAugmentation(*pA310_G31001Aug); // Add the augmented state
   pG31001->addAugmentation(*pA310_G31001Aug);

   pA310->addAugmentation(*pA310_G31002Aug); // Add the augmented state
   pG31002->addAugmentation(*pA310_G31002Aug);

   pA310->addAugmentation(*pA310_G31003Aug); // Add the augmented state
   pG31003->addAugmentation(*pA310_G31003Aug);
   



   //======================================================
   //
   //   A315:  Procedure Error  
   //   $[05172]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA315Info = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 150, PROCEDURE_ERROR_ANALYSIS_MSG, 
         PROCEDURE_ERROR_REMEDY_MSG);

   Alarm* pA315 = new (::rFHA_)              // Define the alarm
      Alarm(A315, *pA315Info, *pG31500,  OPERAND_NAME_NULL,
            FALSE, PROCEDURE_ERROR_MSG, TRUE);
   
   
   Alarm::AddAlarm(pA315);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  
   OperatingComponent* pKG31500_C265 = new (::rFHOK_)
      OperatingComponent(*pC265,   OperatingComponent::OR,  FALSE);

   
   // Connect the operating components to the parent/child relationships
   pG31500->addChild(*pKG31500_C265);  
   
   pC265  ->addParent(*pG31500);
   
   
   pG31500->setCausedAlarm(pA315);           // Set the caused state
   


   //======================================================
   //
   //   A335:  Volume Limit
   //   $[05181]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA335Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    15, VOLUME_LIMIT_ANALYSIS_MSG,
         VOLUME_LIMIT_REMEDY_MSG);

   // Define count constraints
   CountConstraint* pG33501Constraint = new (::rFHCC_) 
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return3Events, NULL);// 3 of last 4 breaths

   // Create operating groups for the above constraints 
   OperatingGroup* pG33501 = new (::rFHOG_)
      OperatingGroup( G33501,pG33501Constraint);
      AlarmOperand::AddOperand(pG33501);


   // Create a Breath count Violation History Manager, delete breaths > 4
   VHMCurrBreathCountCardinal* pG33502Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return4Events);

   // Create a VHM operating group 
   OperatingGroup* pG33502 = new (::rFHOG_)
      OperatingGroup( G33502,NULL,NULL,pG33502Vhm);
      AlarmOperand::AddOperand(pG33502);

   
  

   Alarm* pA335 = new (::rFHA_)              // Define the alarm
      Alarm(A335, *pA335Info, *pG33500, C335, TRUE, VOLUME_LIMIT_MSG);

   Alarm::AddAlarm(pA335);                   // Add the alarm
   
   
   // Create the operating components for this alarm.  

   OperatingComponent* pKG33500_G33501 = new (::rFHOK_)
      OperatingComponent(*pG33501, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG33500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG33501_G33502 = new (::rFHOK_)
      OperatingComponent(*pG33502, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG33502_C335 = new (::rFHOK_)
      OperatingComponent(*pC335,   OperatingComponent::OR,  FALSE);


   // Connect the operating components to the parent/child relationships
   pG33500->addChild(*pKG33500_G33501);
   pG33500->addChild(*pKG33500_G32000);
   pG33501->addChild(*pKG33501_G33502);
   pG33502->addChild(*pKG33502_C335);
   
   pG32000->addParent(*pG33500);
   pG33501->addParent(*pG33500);
   pG33502->addParent(*pG33501);
   pC335  ->addParent(*pG33502);

   
   pG33500->setCausedAlarm(pA335);           // Set the caused state

   // This alarm has to be placed here in the code because of
   // other alarms using it (A380) as a dependancy.
   //======================================================
   //
   //   A380:  Inspired Spont Pressure Limit
   //   $[TC05000] $[TC05001] $[TC05002] $[TC05003] $[TC05004]
   //   $[TC05005] $[TC05006]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA380Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    12, MESSAGE_NAME_NULL, 
         MESSAGE_NAME_NULL);
         
   AlarmInformation* pA380InfoTC = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    12, MESSAGE_NAME_NULL, 
         INSPIRED_SPONT_TC_PRESSURE_LIMIT_REMEDY_MSG);
         
   AlarmInformation* pA380InfoPAV = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    12, MESSAGE_NAME_NULL, 
         INSPIRED_SPONT_PAV_PRESSURE_LIMIT_REMEDY_MSG);

   AlarmInformation* pA380LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    12, INSPIRED_SPONT_PRESSURE_LIMIT_LOW_MSG,  
         MESSAGE_NAME_NULL);
         
   AlarmInformation* pA380MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 15, INSPIRED_SPONT_PRESSURE_LIMIT_MED_MSG,  
         MESSAGE_NAME_NULL);

   AlarmInformation* pA380HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 145, INSPIRED_SPONT_PRESSURE_LIMIT_HIGH_MSG, 
         MESSAGE_NAME_NULL);


   DependentInformation* pA380DepInfo1  = new (::rFHDI_) 
      DependentInformation(5,6,21, LOW_SPONT_TIDAL_VOL_MSG,     MESSAGE_NAME_NULL);   
   DependentInformation* pA380DepInfo2  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG,          MESSAGE_NAME_NULL);   
   DependentInformation* pA380DepInfo3  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG,          MESSAGE_NAME_NULL);


   // Define count constraints
   // 1 or 2 of last 4 breaths
   CountConstraint* pG38001Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::LE, Constraint::EQ,
                      CalcFunctions::Return2Events, NULL); 
   // 3 of last 4 breaths
   CountConstraint* pG38002Constraint = new (::rFHCC_) 
      CountConstraint(Constraint::EQ, Constraint::EQ, 
                      CalcFunctions::Return3Events, NULL);
   // 4 of last 4 breaths
   CountConstraint* pG38003Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return4Events, NULL);
    // >= 1 of 4 breaths
   CountConstraint* pG38005Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return1Event, NULL);

   // Create operating groups for the above constraints 
   OperatingGroup* pG38001 = new (::rFHOG_)
      OperatingGroup( G38001,pG38001Constraint);
      AlarmOperand::AddOperand(pG38001);

   OperatingGroup* pG38002 = new (::rFHOG_)
      OperatingGroup( G38002,pG38002Constraint);
      AlarmOperand::AddOperand(pG38002);

   OperatingGroup* pG38003 = new (::rFHOG_)
      OperatingGroup( G38003,pG38003Constraint);
      AlarmOperand::AddOperand(pG38003);

   OperatingGroup* pG38005 = new (::rFHOG_)
      OperatingGroup( G38005,pG38005Constraint);
      AlarmOperand::AddOperand(pG38005);

   OperatingGroup* pG38006 = new (::rFHOG_)
      OperatingGroup( G38006);
   AlarmOperand::AddOperand(pG38006);
   
   OperatingGroup* pG38007 = new (::rFHOG_)
      OperatingGroup( G38007);
   AlarmOperand::AddOperand(pG38007);


   // Create a Breath count Violation History Manager, delete breaths > 4
   VHMCurrBreathCountCardinal* pG38004Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return4Events);

   // Create operating groups for the VHM
   OperatingGroup* pG38004 = new (::rFHOG_)
      OperatingGroup( G38004,NULL, NULL, pG38004Vhm);
      AlarmOperand::AddOperand(pG38004);

   
  

   
   Alarm* pA380 = new (::rFHA_)              // Define the alarm
      Alarm(A380, *pA380Info, *pG38000,  C380, TRUE,
            INSPIRED_SPONT_PRESSURE_LIMIT_MSG);

   Alarm::AddAlarm(pA380);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG38000_G38005 = new (::rFHOK_)
      OperatingComponent(*pG38005, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG38000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG38001_G38004 = new (::rFHOK_)
      OperatingComponent(*pG38004, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG38002_G38004 = new (::rFHOK_)
      OperatingComponent(*pG38004, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG38003_G38004 = new (::rFHOK_)
      OperatingComponent(*pG38004, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG38004_C380 = new (::rFHOK_)
      OperatingComponent(*pC380,   OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG38005_G38004 = new (::rFHOK_)
      OperatingComponent(*pG38004, OperatingComponent::OR,  FALSE); 
   
   OperatingComponent* pKG38006_C901 = new (::rFHOK_)
      OperatingComponent(*pC901,   OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG38007_C902 = new (::rFHOK_)
      OperatingComponent(*pC902,   OperatingComponent::OR,  FALSE);


   // Connect the operating components to the parent/child relationships
   pG38000->addChild(*pKG38000_G38005);
   pG38000->addChild(*pKG38000_G32000);
   pG38001->addChild(*pKG38001_G38004);
   pG38002->addChild(*pKG38002_G38004);
   pG38003->addChild(*pKG38003_G38004);
   pG38004->addChild(*pKG38004_C380);
   pG38005->addChild(*pKG38005_G38004);
   pG38006->addChild(*pKG38006_C901);
   pG38007->addChild(*pKG38007_C902);
   
   pG38005->addParent(*pG38000);
   pG32000->addParent(*pG38000);
   pG38004->addParent(*pG38001);
   pG38004->addParent(*pG38002);
   pG38004->addParent(*pG38003);
   pC380  ->addParent(*pG38004);
   pG38004->addParent(*pG38005);
   pC901  ->addParent(*pG38006);
   pC902  ->addParent(*pG38007);
   
   
   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA380_A265Aug = new (::rFHDA_)
      DependentAugmentation(*pA380DepInfo1, *pA380, *pA265);
   DependentAugmentation* pA380_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA380DepInfo2, *pA380, *pA260);
   DependentAugmentation* pA380_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA380DepInfo3, *pA380, *pA235);

   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA380_G38001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA380LowInfo,    *pA380, *pG38001);
   ConditionAugmentation* pA380_G38002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA380MediumInfo, *pA380, *pG38002);
   ConditionAugmentation* pA380_G38003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA380HighInfo,   *pA380, *pG38003);
   ConditionAugmentation* pA380_G38006Aug = new (::rFHCA_)
      ConditionAugmentation(*pA380InfoTC,     *pA380, *pG38006);
   ConditionAugmentation* pA380_G38007Aug = new (::rFHCA_)
      ConditionAugmentation(*pA380InfoPAV,    *pA380, *pG38007);
   
   
   pG38000->setCausedAlarm(pA380);           // Set the caused state

  
   pA380->addAugmentation(*pA380_G38006Aug); // Add condition augmentation. // Must be 1st/2nd CondAug.
   pG38006->addAugmentation(*pA380_G38006Aug);
   
   pA380->addAugmentation(*pA380_G38007Aug); // Add condition augmentation. // Must be 1st/2nd CondAug.
   pG38007->addAugmentation(*pA380_G38007Aug);

   pA380->addAugmentation(*pA380_G38001Aug); // Add the augmented state
   pG38001->addAugmentation(*pA380_G38001Aug);

   pA380->addAugmentation(*pA380_G38002Aug); // Add the augmented state
   pG38002->addAugmentation(*pA380_G38002Aug);

   pA380->addAugmentation(*pA380_G38003Aug); // Add the augmented state
   pG38003->addAugmentation(*pA380_G38003Aug);


   pA380->addAugmentation(*pA380_A265Aug);   // Add the dependent augmented state
   pA265->addAugmentation(*pA380_A265Aug);

   pA380->addAugmentation(*pA380_A260Aug);   // Add the dependent augmented state
   pA260->addAugmentation(*pA380_A260Aug);

   pA380->addAugmentation(*pA380_A235Aug);   // Add the dependent augmented state
   pA235->addAugmentation(*pA380_A235Aug);

   
   //======================================================
   //
   //   A345:  Compensation Limit
   //   $[TC05007] $[TC05008] $[TC05009] $[TC05010] $[TC05011]
   //   $[TC05012] $[TC05013] 
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA345Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    14, MESSAGE_NAME_NULL, 
         MESSAGE_NAME_NULL);
   
   AlarmInformation* pA345InfoTC = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    14, MESSAGE_NAME_NULL, 
         COMPENSATION_TC_LIMIT_REMEDY_MSG);
   
   AlarmInformation* pA345InfoPAV = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    14, MESSAGE_NAME_NULL, 
         COMPENSATION_PAV_LIMIT_REMEDY_MSG);

   AlarmInformation* pA345LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    14, COMPENSATION_LIMIT_LOW_MSG,  
         MESSAGE_NAME_NULL);
   
   AlarmInformation* pA345MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 18, COMPENSATION_LIMIT_MED_MSG,  
         MESSAGE_NAME_NULL);
   
   AlarmInformation* pA345HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 148, COMPENSATION_LIMIT_HIGH_MSG, 
         MESSAGE_NAME_NULL);


   DependentInformation* pA345DepInfo1  = new (::rFHDI_) 
      DependentInformation(5,6,21, LOW_SPONT_TIDAL_VOL_MSG,     MESSAGE_NAME_NULL);   
   DependentInformation* pA345DepInfo2  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG,          MESSAGE_NAME_NULL);   
   DependentInformation* pA345DepInfo3  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG,          MESSAGE_NAME_NULL);
   DependentInformation* pA345DepInfo4  = new (::rFHDI_) 
      DependentInformation(12,15,145, INSPIRED_SPONT_PRESSURE_LIMIT_MSG,          MESSAGE_NAME_NULL);


   // Define count constraints
   // 1 or 2 of last 4 breaths
   CountConstraint* pG34501Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::LE, Constraint::EQ,
                      CalcFunctions::Return2Events, NULL); 
   // 3 of last 4 breaths
   CountConstraint* pG34502Constraint = new (::rFHCC_) 
      CountConstraint(Constraint::EQ, Constraint::EQ, 
                      CalcFunctions::Return3Events, NULL);
   // 4 of last 4 breaths
   CountConstraint* pG34503Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return4Events, NULL);
    // >= 1 of 4 breaths
   CountConstraint* pG34505Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return1Event, NULL);

   // Create operating groups for the above constraints 
   OperatingGroup* pG34501 = new (::rFHOG_)
      OperatingGroup( G34501,pG34501Constraint);
      AlarmOperand::AddOperand(pG34501);

   OperatingGroup* pG34502 = new (::rFHOG_)
      OperatingGroup( G34502,pG34502Constraint);
      AlarmOperand::AddOperand(pG34502);

   OperatingGroup* pG34503 = new (::rFHOG_)
      OperatingGroup( G34503,pG34503Constraint);
      AlarmOperand::AddOperand(pG34503);

   OperatingGroup* pG34505 = new (::rFHOG_)
      OperatingGroup( G34505,pG34505Constraint);
      AlarmOperand::AddOperand(pG34505);

   OperatingGroup* pG34506 = new (::rFHOG_)
      OperatingGroup( G34506);
   AlarmOperand::AddOperand(pG34506);
   
   OperatingGroup* pG34507 = new (::rFHOG_)
      OperatingGroup( G34507);
   AlarmOperand::AddOperand(pG34507);


   // Create a Breath count Violation History Manager, delete breaths > 4
   VHMCurrBreathCountCardinal* pG34504Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return4Events);

   // Create operating groups for the VHM
   OperatingGroup* pG34504 = new (::rFHOG_)
      OperatingGroup( G34504,NULL, NULL, pG34504Vhm);
      AlarmOperand::AddOperand(pG34504);

   
  

   
   Alarm* pA345 = new (::rFHA_)              // Define the alarm
      Alarm(A345, *pA345Info, *pG34500,  C345, TRUE,
            COMPENSATION_LIMIT_MSG);

   Alarm::AddAlarm(pA345);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG34500_G34505 = new (::rFHOK_)
      OperatingComponent(*pG34505, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG34500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG34501_G34504 = new (::rFHOK_)
      OperatingComponent(*pG34504, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG34502_G34504 = new (::rFHOK_)
      OperatingComponent(*pG34504, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG34503_G34504 = new (::rFHOK_)
      OperatingComponent(*pG34504, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG34504_C345 = new (::rFHOK_)
      OperatingComponent(*pC345,   OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG34505_G34504 = new (::rFHOK_)
      OperatingComponent(*pG34504, OperatingComponent::OR,  FALSE); 

   OperatingComponent* pKG34506_C901 = new (::rFHOK_)
      OperatingComponent(*pC901,   OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG34507_C902 = new (::rFHOK_)
      OperatingComponent(*pC902,   OperatingComponent::OR,  FALSE);


   // Connect the operating components to the parent/child relationships
   pG34500->addChild(*pKG34500_G34505);
   pG34500->addChild(*pKG34500_G32000);
   pG34501->addChild(*pKG34501_G34504);
   pG34502->addChild(*pKG34502_G34504);
   pG34503->addChild(*pKG34503_G34504);
   pG34504->addChild(*pKG34504_C345);
   pG34505->addChild(*pKG34505_G34504);
   pG34506->addChild(*pKG34506_C901);
   pG34507->addChild(*pKG34507_C902);



   pG34505->addParent(*pG34500);
   pG32000->addParent(*pG34500);
   pG34504->addParent(*pG34501);
   pG34504->addParent(*pG34502);
   pG34504->addParent(*pG34503);
   pC345  ->addParent(*pG34504);
   pG34504->addParent(*pG34505);
   pC901  ->addParent(*pG34506);
   pC902  ->addParent(*pG34507);

   
   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA345_A265Aug = new (::rFHDA_)
      DependentAugmentation(*pA345DepInfo1, *pA345, *pA265);
   DependentAugmentation* pA345_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA345DepInfo2, *pA345, *pA260);
   DependentAugmentation* pA345_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA345DepInfo3, *pA345, *pA235);

   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA345_G34501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA345LowInfo,    *pA345, *pG34501);
   ConditionAugmentation* pA345_G34502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA345MediumInfo, *pA345, *pG34502);
   ConditionAugmentation* pA345_G34503Aug = new (::rFHCA_)
      ConditionAugmentation(*pA345HighInfo,   *pA345, *pG34503);
   ConditionAugmentation* pA345_G34506Aug = new (::rFHCA_)
      ConditionAugmentation(*pA345InfoTC,     *pA345, *pG34506);
   ConditionAugmentation* pA345_G34507Aug = new (::rFHCA_)
      ConditionAugmentation(*pA345InfoPAV,    *pA345, *pG34507);

   
   
   pG34500->setCausedAlarm(pA345);           // Set the caused state


   pA345->addAugmentation(*pA345_G34506Aug); // Add condition augmentation. // Must be 1st/2nd CondAug.
   pG34506->addAugmentation(*pA345_G34506Aug);
   
   pA345->addAugmentation(*pA345_G34507Aug); // Add condition augmentation. // Must be 1st/2nd CondAug.
   pG34507->addAugmentation(*pA345_G34507Aug);

   pA345->addAugmentation(*pA345_G34501Aug); // Add the augmented state
   pG34501->addAugmentation(*pA345_G34501Aug);

   pA345->addAugmentation(*pA345_G34502Aug); // Add the augmented state
   pG34502->addAugmentation(*pA345_G34502Aug);

   pA345->addAugmentation(*pA345_G34503Aug); // Add the augmented state
   pG34503->addAugmentation(*pA345_G34503Aug);


   pA345->addAugmentation(*pA345_A265Aug);   // Add the dependent augmented state
   pA265->addAugmentation(*pA345_A265Aug);

   pA345->addAugmentation(*pA345_A260Aug);   // Add the dependent augmented state
   pA260->addAugmentation(*pA345_A260Aug);

   pA345->addAugmentation(*pA345_A235Aug);   // Add the dependent augmented state
   pA235->addAugmentation(*pA345_A235Aug);

   


   //======================================================
   //
   //   A355:  Unable To Start PAV
   //   $[PA05000] $[PA05001] $[PA05002] $[PA05003] $[PA05004]
   //   $[PA05005] $[PA05006]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA355Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    35, MESSAGE_NAME_NULL,       
         LONG_PAV_STARTUP_REMEDY_MSG);
   AlarmInformation* pA355LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    35, LONG_PAV_STARTUP_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA355MediumInfo = new (::rFHAI_)          
      AlarmInformation(Alarm::MEDIUM_URGENCY, 45, LONG_PAV_STARTUP_MED_MSG,
         MESSAGE_NAME_NULL);
   AlarmInformation* pA355HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY,  195, LONG_PAV_STARTUP_HIGH_MSG, 
         MESSAGE_NAME_NULL);


   DependentInformation* pA355DepInfo1  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG, MESSAGE_NAME_NULL);
   DependentInformation* pA355DepInfo2  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG, MESSAGE_NAME_NULL);   
   DependentInformation* pA355DepInfo3  = new (::rFHDI_) 
      DependentInformation(5,6,21, LOW_SPONT_TIDAL_VOL_MSG, MESSAGE_NAME_NULL);  
   
   // Define the Temporal constraints and VHM managers
   // Define a Temporal constraint, duration >= 45
   TemporalConstraint* pG35501Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
      CalcFunctions::Return45Sec, NULL);
   
   // Define a Temporal constraint, duration >= 45, < 120
   TemporalConstraint* pG35502Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::EQ, Constraint::LT, 
      NULL, CalcFunctions::Return45Sec);
                                                
   // Define a Temporal constraint, duration >= 90 and < 120 sec intervals
   TemporalConstraint* pG35503Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::LT, 
      CalcFunctions::Return45Sec, CalcFunctions::Return75Sec);   

   // Define a Temporal constraint, duration >= 120 sec intervals
   TemporalConstraint* pG35504Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
      CalcFunctions::Return75Sec, NULL);   


   // Create Violation History Managers
   VHMTemporal*  pG35501Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   VHMTemporal*  pG35505Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);
   

   // Create VHM, Constraint operating groups
   OperatingGroup* pG35501 = new (::rFHOG_)
      OperatingGroup( G35501, pG35501Constraint, NULL, pG35501Vhm);
      AlarmOperand::AddOperand(pG35501);

   // Create a Temporal Constraint operating group 
   OperatingGroup* pG35502 = new (::rFHOG_)
      OperatingGroup( G35502,pG35502Constraint);
      AlarmOperand::AddOperand(pG35502);

   // Create a Temporal Constraint operating group 
   OperatingGroup* pG35503 = new (::rFHOG_)
      OperatingGroup( G35503,pG35503Constraint);
      AlarmOperand::AddOperand(pG35503);

   // Create a Temporal Constraint operating group 
   OperatingGroup* pG35504 = new (::rFHOG_)
      OperatingGroup( G35504,pG35504Constraint);
      AlarmOperand::AddOperand(pG35504);

   // Create VHM operating groups 
   OperatingGroup* pG35505 = new (::rFHOG_)
      OperatingGroup( G35505, NULL, NULL, pG35505Vhm);
      AlarmOperand::AddOperand(pG35505);


   Alarm* pA355 = new (::rFHA_)              // Define the alarm
      Alarm(A355, *pA355Info, *pG35500,  C355, FALSE,
            LONG_PAV_STARTUP_MSG);
   Alarm::AddAlarm(pA355);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG35500_G35501 = new (::rFHOK_)
      OperatingComponent(*pG35501, OperatingComponent::OR,  FALSE);
 
   OperatingComponent* pKG35500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);
   
   OperatingComponent* pKG35501_G35505 = new (::rFHOK_)
      OperatingComponent(*pG35505, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG35502_G35501 = new (::rFHOK_)
      OperatingComponent(*pG35501, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG35503_G35501 = new (::rFHOK_)
      OperatingComponent(*pG35501, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG35504_G35501   = new (::rFHOK_)
      OperatingComponent(*pG35501,   OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG35505_C355   = new (::rFHOK_)
      OperatingComponent(*pC355,   OperatingComponent::OR,  FALSE);
   

   // Connect the operating components to the parent/child relationships
   pG35500->addChild(*pKG35500_G35501);
   pG35500->addChild(*pKG35500_G32000);
   pG35501->addChild(*pKG35501_G35505);
   pG35502->addChild(*pKG35502_G35501);
   pG35503->addChild(*pKG35503_G35501);
   pG35504->addChild(*pKG35504_G35501);
   pG35505->addChild(*pKG35505_C355);

   pG35501->addParent(*pG35500);
   pG32000->addParent(*pG35500);
   pG35505->addParent(*pG35501);
   pG35501->addParent(*pG35502);
   pG35501->addParent(*pG35503);
   pG35501->addParent(*pG35504);
   pC355  ->addParent(*pG35505);

   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA355_G35502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA355LowInfo, *pA355, *pG35502);

   ConditionAugmentation* pA355_G35503Aug = new (::rFHCA_)
      ConditionAugmentation(*pA355MediumInfo, *pA355, *pG35503);

   ConditionAugmentation* pA355_G35504Aug = new (::rFHCA_)
      ConditionAugmentation(*pA355HighInfo, *pA355, *pG35504);


   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA355_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA355DepInfo1, *pA355, *pA235);
   DependentAugmentation* pA355_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA355DepInfo2, *pA355, *pA260);
   DependentAugmentation* pA355_A265Aug = new (::rFHDA_)
      DependentAugmentation(*pA355DepInfo3, *pA355, *pA265);


   pG35500->setCausedAlarm(pA355);           // Set the caused state
                                    
   pA355->addAugmentation(*pA355_G35502Aug); // Add the augmented state
   pG35502->addAugmentation(*pA355_G35502Aug);

   pA355->addAugmentation(*pA355_G35503Aug); // Add the augmented state
   pG35503->addAugmentation(*pA355_G35503Aug);

   pA355->addAugmentation(*pA355_G35504Aug); // Add the augmented state
   pG35504->addAugmentation(*pA355_G35504Aug);


   pA355->addAugmentation(*pA355_A235Aug);   // Add the dependent augmentation
   pA235->addAugmentation(*pA355_A235Aug);

   pA355->addAugmentation(*pA355_A260Aug);   // Add the dependent augmentation
   pA260->addAugmentation(*pA355_A260Aug);

   pA355->addAugmentation(*pA355_A265Aug);   // Add the dependent augmentation
   pA265->addAugmentation(*pA355_A265Aug);



   //======================================================
   //
   //   A360:  Unable To Assess PAV R&C
   //   $[PA05007] $[PA05008] $[PA05009]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA360Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    36, MESSAGE_NAME_NULL,       
         PAV_RC_NOT_ASSESSED_REMEDY_MSG);
   AlarmInformation* pA360LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    36, PAV_RC_NOT_ASSESSED_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA360MediumInfo = new (::rFHAI_)          
      AlarmInformation(Alarm::MEDIUM_URGENCY, 46, PAV_RC_NOT_ASSESSED_MED_MSG,
         MESSAGE_NAME_NULL);


   // Define the Temporal constraints and VHM managers
   // Define a Temporal constraint, duration >= 15 min
   TemporalConstraint* pG36001Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
      CalcFunctions::Return15Min, NULL);
   
   // Define a Temporal constraint, duration < 30 min
   TemporalConstraint* pG36002Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::LT, 
      NULL, CalcFunctions::Return15Min);   

   // Define a Temporal constraint, duration >= 30 min
   TemporalConstraint* pG36003Constraint = new (::rFHTC_)
      TemporalConstraint(Constraint::GE, Constraint::EQ, 
      CalcFunctions::Return15Min, NULL);   


   // Create Violation History Managers
   VHMTemporal*  pG36001Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);

   VHMTemporal*  pG36004Vhm = new (::rFHVHMT_)
      VHMTemporal(NULL);
   

   // Create VHM, Constraint operating groups
   OperatingGroup* pG36001 = new (::rFHOG_)
      OperatingGroup( G36001, pG36001Constraint, NULL, pG36001Vhm);
      AlarmOperand::AddOperand(pG36001);

   // Create a Temporal Constraint operating group 
   OperatingGroup* pG36002 = new (::rFHOG_)
      OperatingGroup( G36002,pG36002Constraint);
      AlarmOperand::AddOperand(pG36002);

   // Create a Temporal Constraint operating group 
   OperatingGroup* pG36003 = new (::rFHOG_)
      OperatingGroup( G36003,pG36003Constraint);
      AlarmOperand::AddOperand(pG36003);

   // Create a Temporal Constraint operating group 
   OperatingGroup* pG36004 = new (::rFHOG_)
      OperatingGroup( G36004, NULL, NULL, pG36004Vhm);
      AlarmOperand::AddOperand(pG36004);


   Alarm* pA360 = new (::rFHA_)              // Define the alarm
      Alarm(A360, *pA360Info, *pG36000,  C360, FALSE,
            PAV_RC_NOT_ASSESSED_MSG);
   Alarm::AddAlarm(pA360);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG36000_G36001 = new (::rFHOK_)
      OperatingComponent(*pG36001, OperatingComponent::OR,  FALSE);
 
   OperatingComponent* pKG36000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);
   
   OperatingComponent* pKG36001_G36004 = new (::rFHOK_)
      OperatingComponent(*pG36004, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG36002_G36001 = new (::rFHOK_)
      OperatingComponent(*pG36001, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG36003_G36001 = new (::rFHOK_)
      OperatingComponent(*pG36001, OperatingComponent::OR,  FALSE);
     
   OperatingComponent* pKG36004_C360   = new (::rFHOK_)
      OperatingComponent(*pC360,   OperatingComponent::OR,  FALSE);
   

   // Connect the operating components to the parent/child relationships
   pG36000->addChild(*pKG36000_G36001);
   pG36000->addChild(*pKG36000_G32000);
   pG36001->addChild(*pKG36001_G36004);
   pG36002->addChild(*pKG36002_G36001);
   pG36003->addChild(*pKG36003_G36001);
   pG36004->addChild(*pKG36004_C360);

   pG36001->addParent(*pG36000);
   pG32000->addParent(*pG36000);
   pG36004->addParent(*pG36001);
   pG36001->addParent(*pG36002);
   pG36001->addParent(*pG36003);
   pC360  ->addParent(*pG36004);

   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA360_G36002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA360LowInfo, *pA360, *pG36002);

   ConditionAugmentation* pA360_G36003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA360MediumInfo, *pA360, *pG36003);


   pG36000->setCausedAlarm(pA360);           // Set the caused state
                                    
   pA360->addAugmentation(*pA360_G36002Aug); // Add the augmented state
   pG36002->addAugmentation(*pA360_G36002Aug);

   pA360->addAugmentation(*pA360_G36003Aug); // Add the augmented state
   pG36003->addAugmentation(*pA360_G36003Aug);



   //======================================================
   //
   //   A365:  Volume Not Delivered (VS based spont breaths)
   //   $[VC05000] $[VC05001] $[VC05002] $[VC05004]
   //   $[VC05005] $[VC05006]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA365Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 13, MESSAGE_NAME_NULL, 
         VOLUME_NOT_DELIVERED_REMEDY_MSG);

   AlarmInformation* pA365LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 13, 
      VOLUME_NOT_DELIVERED_SPONT_LOW_MSG, MESSAGE_NAME_NULL);
   AlarmInformation* pA365MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 17,
      VOLUME_NOT_DELIVERED_SPONT_MED_MSG, MESSAGE_NAME_NULL);

   DependentInformation* pA365DepInfo1  = new (::rFHDI_) 
      DependentInformation(5,6,21, LOW_SPONT_TIDAL_VOL_MSG, MESSAGE_NAME_NULL);
   DependentInformation* pA365DepInfo2  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG, MESSAGE_NAME_NULL);   
   DependentInformation* pA365DepInfo3  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG, MESSAGE_NAME_NULL);

   
   // Define count constraints.
   // Last 2 to 9 breaths meet detection criteria
   CountConstraint* pG36501Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::LT,
                      CalcFunctions::Return2Events, CalcFunctions::Return10Events); 

   // Last 10 breaths meet detection criteria
   CountConstraint* pG36502Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ,
                      CalcFunctions::Return10Events, NULL);

   // >= 2 breaths meet detection criteria
   CountConstraint* pG36504Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return2Events, NULL);

   // Create operating groups for the above constraints 
   OperatingGroup* pG36501 = new (::rFHOG_)
      OperatingGroup( G36501,pG36501Constraint);
      AlarmOperand::AddOperand(pG36501);

   OperatingGroup* pG36502 = new (::rFHOG_)
      OperatingGroup( G36502,pG36502Constraint);
      AlarmOperand::AddOperand(pG36502);

   OperatingGroup* pG36504 = new (::rFHOG_)
      OperatingGroup( G36504,pG36504Constraint);
      AlarmOperand::AddOperand(pG36504);


   // Create a Breath count Violation History Manager, delete breaths > 10
   VHMCurrBreathCountCardinal* pG36503Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return10Events);

   // Create operating groups for the VHM
   OperatingGroup* pG36503 = new (::rFHOG_)
      OperatingGroup( G36503,NULL, NULL, pG36503Vhm);
      AlarmOperand::AddOperand(pG36503);

   
  
   Alarm* pA365 = new (::rFHA_)              // Define the alarm
      Alarm(A365, *pA365Info, *pG36500,  C365, TRUE,
            VOLUME_NOT_DELIVERED_MSG);
   Alarm::AddAlarm(pA365);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG36500_G36504 = new (::rFHOK_)
      OperatingComponent(*pG36504, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG36500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG36501_G36503 = new (::rFHOK_)
      OperatingComponent(*pG36503, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG36502_G36503 = new (::rFHOK_)
      OperatingComponent(*pG36503, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG36503_C365 = new (::rFHOK_)
      OperatingComponent(*pC365, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG36504_G36503 = new (::rFHOK_)
      OperatingComponent(*pG36503, OperatingComponent::OR,  FALSE); 
   

   // Connect the operating components to the parent/child relationships
   pG36500->addChild(*pKG36500_G36504);
   pG36500->addChild(*pKG36500_G32000);
   pG36501->addChild(*pKG36501_G36503);
   pG36502->addChild(*pKG36502_G36503);
   pG36503->addChild(*pKG36503_C365);
   pG36504->addChild(*pKG36504_G36503);

   pG36504->addParent(*pG36500);
   pG32000->addParent(*pG36500);
   pG36503->addParent(*pG36501);
   pG36503->addParent(*pG36502);
   pC365  ->addParent(*pG36503);
   pG36503->addParent(*pG36504);


   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA365_G36501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA365LowInfo,    *pA365, *pG36501);
   ConditionAugmentation* pA365_G36502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA365MediumInfo, *pA365, *pG36502);
   
   
   pG36500->setCausedAlarm(pA365);           // Set the caused state

   pA365->addAugmentation(*pA365_G36501Aug); // Add the augmented state
   pG36501->addAugmentation(*pA365_G36501Aug);

   pA365->addAugmentation(*pA365_G36502Aug); // Add the augmented state
   pG36502->addAugmentation(*pA365_G36502Aug);

   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA365_A265Aug = new (::rFHDA_)
      DependentAugmentation(*pA365DepInfo1, *pA365, *pA265);
   DependentAugmentation* pA365_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA365DepInfo2, *pA365, *pA260);
   DependentAugmentation* pA365_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA365DepInfo3, *pA365, *pA235);


   pA365->addAugmentation(*pA365_A265Aug);   // Add the dependent augmentation
   pA265->addAugmentation(*pA365_A265Aug);

   pA365->addAugmentation(*pA365_A260Aug);   // Add the dependent augmentation
   pA260->addAugmentation(*pA365_A260Aug);

   pA365->addAugmentation(*pA365_A235Aug);   // Add the dependent augmentation
   pA235->addAugmentation(*pA365_A235Aug);




   
   //======================================================
   //
   //   A366:  Volume Not Delivered (VC+ based mand breaths) 
   //   $[VC05011] $[VC05012] $[VC05013] $[VC05014] $[VC05015]
   //   $[VC05016]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA366Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 17, MESSAGE_NAME_NULL, 
         VOLUME_NOT_DELIVERED_REMEDY_MSG);

   AlarmInformation* pA366LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 17, 
      VOLUME_NOT_DELIVERED_MAND_LOW_MSG, MESSAGE_NAME_NULL);
   AlarmInformation* pA366MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 16,
      VOLUME_NOT_DELIVERED_MAND_MED_MSG, MESSAGE_NAME_NULL);

   DependentInformation* pA366DepInfo1  = new (::rFHDI_) 
      DependentInformation(4,5,20, LOW_MAND_TIDAL_VOL_MSG,  MESSAGE_NAME_NULL);
   DependentInformation* pA366DepInfo2  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG, MESSAGE_NAME_NULL);   
   DependentInformation* pA366DepInfo3  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG, MESSAGE_NAME_NULL);

   
   // Define count constraints.
   // Last 2 to 9 breaths meet detection criteria
   CountConstraint* pG36601Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::LT,
                      CalcFunctions::Return2Events, CalcFunctions::Return10Events); 

   // Last 10 breaths meet detection criteria
   CountConstraint* pG36602Constraint = new (::rFHCC_)
      CountConstraint(Constraint::GE, Constraint::EQ,
                      CalcFunctions::Return10Events, NULL);

   // >= 2 breaths meet detection criteria
   CountConstraint* pG36604Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return2Events, NULL);

   // Create operating groups for the above constraints 
   OperatingGroup* pG36601 = new (::rFHOG_)
      OperatingGroup( G36601,pG36601Constraint);
      AlarmOperand::AddOperand(pG36601);

   OperatingGroup* pG36602 = new (::rFHOG_)
      OperatingGroup( G36602,pG36602Constraint);
      AlarmOperand::AddOperand(pG36602);

   OperatingGroup* pG36604 = new (::rFHOG_)
      OperatingGroup( G36604,pG36604Constraint);
      AlarmOperand::AddOperand(pG36604);


   // Create a Breath count Violation History Manager, delete breaths > 10
   VHMCurrBreathCountCardinal* pG36603Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return10Events);

   // Create operating groups for the VHM
   OperatingGroup* pG36603 = new (::rFHOG_)
      OperatingGroup( G36603,NULL, NULL, pG36603Vhm);
      AlarmOperand::AddOperand(pG36603);

   
  
   Alarm* pA366 = new (::rFHA_)              // Define the alarm
      Alarm(A366, *pA366Info, *pG36600,  C366, TRUE,
            VOLUME_NOT_DELIVERED_MSG);
   Alarm::AddAlarm(pA366);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG36600_G36604 = new (::rFHOK_)
      OperatingComponent(*pG36604, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG36600_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG36601_G36603 = new (::rFHOK_)
      OperatingComponent(*pG36603, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG36602_G36603 = new (::rFHOK_)
      OperatingComponent(*pG36603, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG36603_C366 = new (::rFHOK_)
      OperatingComponent(*pC366, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG36604_G36603 = new (::rFHOK_)
      OperatingComponent(*pG36603, OperatingComponent::OR,  FALSE); 
   

   // Connect the operating components to the parent/child relationships
   pG36600->addChild(*pKG36600_G36604);
   pG36600->addChild(*pKG36600_G32000);
   pG36601->addChild(*pKG36601_G36603);
   pG36602->addChild(*pKG36602_G36603);
   pG36603->addChild(*pKG36603_C366);
   pG36604->addChild(*pKG36604_G36603);

   pG36604->addParent(*pG36600);
   pG32000->addParent(*pG36600);
   pG36603->addParent(*pG36601);
   pG36603->addParent(*pG36602);
   pC366  ->addParent(*pG36603);
   pG36603->addParent(*pG36604);


   
   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA366_G36601Aug = new (::rFHCA_)
      ConditionAugmentation(*pA366LowInfo,    *pA366, *pG36601);
   ConditionAugmentation* pA366_G36602Aug = new (::rFHCA_)
      ConditionAugmentation(*pA366MediumInfo, *pA366, *pG36602);
   
   
   pG36600->setCausedAlarm(pA366);           // Set the caused state

   pA366->addAugmentation(*pA366_G36601Aug); // Add the augmented state
   pG36601->addAugmentation(*pA366_G36601Aug);

   pA366->addAugmentation(*pA366_G36602Aug); // Add the augmented state
   pG36602->addAugmentation(*pA366_G36602Aug);

   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA366_A255Aug = new (::rFHDA_)
      DependentAugmentation(*pA366DepInfo1, *pA366, *pA255);
   DependentAugmentation* pA366_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA366DepInfo2, *pA366, *pA260);
   DependentAugmentation* pA366_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA366DepInfo3, *pA366, *pA235);


   pA366->addAugmentation(*pA366_A255Aug);   // Add the dependent augmentation
   pA255->addAugmentation(*pA366_A255Aug);

   pA366->addAugmentation(*pA366_A260Aug);   // Add the dependent augmentation
   pA260->addAugmentation(*pA366_A260Aug);

   pA366->addAugmentation(*pA366_A235Aug);   // Add the dependent augmentation
   pA235->addAugmentation(*pA366_A235Aug);




   //======================================================
   //
   //   A385:  Inspired Spont Volume Limit
   //   $[TC05000] $[TC05001] $[TC05002] $[TC05003] $[TC05004]
   //   $[TC05005] $[TC05006]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA385Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    12, MESSAGE_NAME_NULL, 
         INSPIRED_SPONT_VOLUME_LIMIT_REMEDY_MSG);

   AlarmInformation* pA385LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    12, INSPIRED_SPONT_VOLUME_LIMIT_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA385MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 15, INSPIRED_SPONT_VOLUME_LIMIT_MED_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA385HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 145, INSPIRED_SPONT_VOLUME_LIMIT_HIGH_MSG, 
         MESSAGE_NAME_NULL);


   DependentInformation* pA385DepInfo1  = new (::rFHDI_) 
      DependentInformation(5,6,21, LOW_SPONT_TIDAL_VOL_MSG,      MESSAGE_NAME_NULL);   
   DependentInformation* pA385DepInfo2  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG,          MESSAGE_NAME_NULL);   
   DependentInformation* pA385DepInfo3  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG,          MESSAGE_NAME_NULL);


   // Define count constraints
   // 1 or 2 of last 4 breaths
   CountConstraint* pG38501Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::LE, Constraint::EQ,
                      CalcFunctions::Return2Events, NULL); 
   // 3 of last 4 breaths
   CountConstraint* pG38502Constraint = new (::rFHCC_) 
      CountConstraint(Constraint::EQ, Constraint::EQ, 
                      CalcFunctions::Return3Events, NULL);
   // 4 of last 4 breaths
   CountConstraint* pG38503Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return4Events, NULL);
    // >= 1 of 4 breaths
   CountConstraint* pG38505Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return1Event, NULL);

   // Create operating groups for the above constraints 
   OperatingGroup* pG38501 = new (::rFHOG_)
      OperatingGroup( G38501,pG38501Constraint);
      AlarmOperand::AddOperand(pG38501);

   OperatingGroup* pG38502 = new (::rFHOG_)
      OperatingGroup( G38502,pG38502Constraint);
      AlarmOperand::AddOperand(pG38502);

   OperatingGroup* pG38503 = new (::rFHOG_)
      OperatingGroup( G38503,pG38503Constraint);
      AlarmOperand::AddOperand(pG38503);

   OperatingGroup* pG38505 = new (::rFHOG_)
      OperatingGroup( G38505,pG38505Constraint);
      AlarmOperand::AddOperand(pG38505);


   // Create a Breath count Violation History Manager, delete breaths > 4
   VHMCurrBreathCountCardinal* pG38504Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return4Events);

   // Create operating groups for the VHM
   OperatingGroup* pG38504 = new (::rFHOG_)
      OperatingGroup( G38504,NULL, NULL, pG38504Vhm);
      AlarmOperand::AddOperand(pG38504);

   
  

   
   Alarm* pA385 = new (::rFHA_)              // Define the alarm
      Alarm(A385, *pA385Info, *pG38500,  C385, TRUE,
            INSPIRED_SPONT_VOLUME_LIMIT_MSG);

   Alarm::AddAlarm(pA385);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG38500_G38505 = new (::rFHOK_)
      OperatingComponent(*pG38505, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG38500_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG38501_G38504 = new (::rFHOK_)
      OperatingComponent(*pG38504, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG38502_G38504 = new (::rFHOK_)
      OperatingComponent(*pG38504, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG38503_G38504 = new (::rFHOK_)
      OperatingComponent(*pG38504, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG38504_C385 = new (::rFHOK_)
      OperatingComponent(*pC385,   OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG38505_G38504 = new (::rFHOK_)
      OperatingComponent(*pG38504, OperatingComponent::OR,  FALSE); 
   

   // Connect the operating components to the parent/child relationships
   pG38500->addChild(*pKG38500_G38505);
   pG38500->addChild(*pKG38500_G32000);
   pG38501->addChild(*pKG38501_G38504);
   pG38502->addChild(*pKG38502_G38504);
   pG38503->addChild(*pKG38503_G38504);
   pG38504->addChild(*pKG38504_C385);
   pG38505->addChild(*pKG38505_G38504);

   pG38505->addParent(*pG38500);
   pG32000->addParent(*pG38500);
   pG38504->addParent(*pG38501);
   pG38504->addParent(*pG38502);
   pG38504->addParent(*pG38503);
   pC385  ->addParent(*pG38504);
   pG38504->addParent(*pG38505);


   
   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA385_A265Aug = new (::rFHDA_)
      DependentAugmentation(*pA385DepInfo1, *pA385, *pA265);
   DependentAugmentation* pA385_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA385DepInfo2, *pA385, *pA260);
   DependentAugmentation* pA385_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA385DepInfo3, *pA385, *pA235);

   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA385_G38501Aug = new (::rFHCA_)
      ConditionAugmentation(*pA385LowInfo,    *pA385, *pG38501);
   ConditionAugmentation* pA385_G38502Aug = new (::rFHCA_)
      ConditionAugmentation(*pA385MediumInfo, *pA385, *pG38502);
   ConditionAugmentation* pA385_G38503Aug = new (::rFHCA_)
      ConditionAugmentation(*pA385HighInfo,   *pA385, *pG38503);
   
   
   pG38500->setCausedAlarm(pA385);           // Set the caused state

   pA385->addAugmentation(*pA385_G38501Aug); // Add the augmented state
   pG38501->addAugmentation(*pA385_G38501Aug);

   pA385->addAugmentation(*pA385_G38502Aug); // Add the augmented state
   pG38502->addAugmentation(*pA385_G38502Aug);

   pA385->addAugmentation(*pA385_G38503Aug); // Add the augmented state
   pG38503->addAugmentation(*pA385_G38503Aug);


   pA385->addAugmentation(*pA385_A265Aug);   // Add the dependent augmented state
   pA265->addAugmentation(*pA385_A265Aug);

   pA385->addAugmentation(*pA385_A260Aug);   // Add the dependent augmented state
   pA260->addAugmentation(*pA385_A260Aug);

   pA385->addAugmentation(*pA385_A235Aug);   // Add the dependent augmented state
   pA235->addAugmentation(*pA385_A235Aug);

   //======================================================
   //
   //   A390:  Inspired Mand Volume Limit
   //   $[VC05017] $[VC05018] $[VC05019] $[VC05020] $[VC05021]
   //   $[VC05022] $[VC05023]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA390Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    12, MESSAGE_NAME_NULL, 
         INSPIRED_MAND_VOLUME_LIMIT_REMEDY_MSG);

   AlarmInformation* pA390LowInfo = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY,    11, INSPIRED_MAND_VOLUME_LIMIT_LOW_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA390MediumInfo = new (::rFHAI_)
      AlarmInformation(Alarm::MEDIUM_URGENCY, 14, INSPIRED_MAND_VOLUME_LIMIT_MED_MSG,  
         MESSAGE_NAME_NULL);
   AlarmInformation* pA390HighInfo = new (::rFHAI_)
      AlarmInformation(Alarm::HIGH_URGENCY, 144,INSPIRED_MAND_VOLUME_LIMIT_HIGH_MSG, 
         MESSAGE_NAME_NULL);


   DependentInformation* pA390DepInfo1  = new (::rFHDI_) 
      DependentInformation(4,5,20, LOW_MAND_TIDAL_VOL_MSG, MESSAGE_NAME_NULL);     
   DependentInformation* pA390DepInfo2  = new (::rFHDI_) 
      DependentInformation(2,2,16, LOW_MINUTE_VOL_MSG,          MESSAGE_NAME_NULL);   
   DependentInformation* pA390DepInfo3  = new (::rFHDI_) 
      DependentInformation(6,7,22, HIGH_RESP_RATE_MSG,          MESSAGE_NAME_NULL);


   // Define count constraints
   // 1 or 2 of last 4 breaths
   CountConstraint* pG39001Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::LE, Constraint::EQ,
                      CalcFunctions::Return2Events, NULL); 
   // 3 of last 4 breaths
   CountConstraint* pG39002Constraint = new (::rFHCC_) 
      CountConstraint(Constraint::EQ, Constraint::EQ, 
                      CalcFunctions::Return3Events, NULL);
   // 4 of last 4 breaths
   CountConstraint* pG39003Constraint = new (::rFHCC_)
      CountConstraint(Constraint::EQ, Constraint::EQ,
                      CalcFunctions::Return4Events, NULL);
    // >= 1 of 4 breaths
   CountConstraint* pG39005Constraint = new (::rFHCC_)          
      CountConstraint(Constraint::GE, Constraint::EQ, 
                      CalcFunctions::Return1Event, NULL);

   // Create operating groups for the above constraints 
   OperatingGroup* pG39001 = new (::rFHOG_)
      OperatingGroup( G39001,pG39001Constraint);
      AlarmOperand::AddOperand(pG39001);

   OperatingGroup* pG39002 = new (::rFHOG_)
      OperatingGroup( G39002,pG39002Constraint);
      AlarmOperand::AddOperand(pG39002);

   OperatingGroup* pG39003 = new (::rFHOG_)
      OperatingGroup( G39003,pG39003Constraint);
      AlarmOperand::AddOperand(pG39003);

   OperatingGroup* pG39005 = new (::rFHOG_)
      OperatingGroup( G39005,pG39005Constraint);
      AlarmOperand::AddOperand(pG39005);


   // Create a Breath count Violation History Manager, delete breaths > 4
   VHMCurrBreathCountCardinal* pG39004Vhm = new (::rFHVHMCBCC_) 
      VHMCurrBreathCountCardinal(CalcFunctions::Return4Events);

   // Create operating groups for the VHM
   OperatingGroup* pG39004 = new (::rFHOG_)
      OperatingGroup( G39004,NULL, NULL, pG39004Vhm);
      AlarmOperand::AddOperand(pG39004);

   
  

   
   Alarm* pA390 = new (::rFHA_)              // Define the alarm
      Alarm(A390, *pA390Info, *pG39000,  C390, TRUE,
             INSPIRED_MAND_VOLUME_LIMIT_MSG);

   Alarm::AddAlarm(pA390);                   // Add the alarm
   
   

   // Create the operating components for this alarm.  

   OperatingComponent* pKG39000_G39005 = new (::rFHOK_)
      OperatingComponent(*pG39005, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG39000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG39001_G39004 = new (::rFHOK_)
      OperatingComponent(*pG39004, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG39002_G39004 = new (::rFHOK_)
      OperatingComponent(*pG39004, OperatingComponent::OR,  FALSE);
   
   OperatingComponent* pKG39003_G39004 = new (::rFHOK_)
      OperatingComponent(*pG39004, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG39004_C390 = new (::rFHOK_)
      OperatingComponent(*pC390,   OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG39005_G39004 = new (::rFHOK_)
      OperatingComponent(*pG39004, OperatingComponent::OR,  FALSE); 
   

   // Connect the operating components to the parent/child relationships
   pG39000->addChild(*pKG39000_G39005);
   pG39000->addChild(*pKG39000_G32000);
   pG39001->addChild(*pKG39001_G39004);
   pG39002->addChild(*pKG39002_G39004);
   pG39003->addChild(*pKG39003_G39004);
   pG39004->addChild(*pKG39004_C390);
   pG39005->addChild(*pKG39005_G39004);

   pG39005->addParent(*pG39000);
   pG32000->addParent(*pG39000);
   pG39004->addParent(*pG39001);
   pG39004->addParent(*pG39002);
   pG39004->addParent(*pG39003);
   pC390   ->addParent(*pG39004);
   pG39004->addParent(*pG39005);


   
   // Create the dependent augmentation for this alarm, no enabling group
   DependentAugmentation* pA390_A255Aug = new (::rFHDA_)
      DependentAugmentation(*pA390DepInfo1, *pA390, *pA255);
      
   DependentAugmentation* pA390_A260Aug = new (::rFHDA_)
      DependentAugmentation(*pA390DepInfo2, *pA390, *pA260);
      
   DependentAugmentation* pA390_A235Aug = new (::rFHDA_)
      DependentAugmentation(*pA390DepInfo3, *pA390, *pA235);

   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA390_G39001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA390LowInfo,    *pA390, *pG39001);
   ConditionAugmentation* pA390_G39002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA390MediumInfo, *pA390, *pG39002);
   ConditionAugmentation* pA390_G39003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA390HighInfo,   *pA390, *pG39003);
   
   
   pG39000->setCausedAlarm(pA390);           // Set the caused state

   pA390->addAugmentation(*pA390_G39001Aug); // Add the augmented state
   pG39001->addAugmentation(*pA390_G39001Aug);

   pA390->addAugmentation(*pA390_G39002Aug); // Add the augmented state
   pG39002->addAugmentation(*pA390_G39002Aug);

   pA390->addAugmentation(*pA390_G39003Aug); // Add the augmented state
   pG39003->addAugmentation(*pA390_G39003Aug);


   pA390->addAugmentation(*pA390_A255Aug);   // Add the dependent augmented state
   pA255->addAugmentation(*pA390_A255Aug);

   pA390->addAugmentation(*pA390_A260Aug);   // Add the dependent augmented state
   pA260->addAugmentation(*pA390_A260Aug);

   pA390->addAugmentation(*pA390_A235Aug);   // Add the dependent augmented state
   pA235->addAugmentation(*pA390_A235Aug);
  


   //======================================================
   //
   //   A400:  Low Circ (Ppeak) Pressure Alarm
   //   $[NI05001] $[NI05002] $[NI05003] $[NI05004] 
   //
   //======================================================


   // Create the alarm information for this alarm
   AlarmInformation* pA400Info = new (::rFHAI_)
	  AlarmInformation(
		  Alarm::LOW_URGENCY,     10, MESSAGE_NAME_NULL, LOW_PRES_REMEDY_MSG);

   AlarmInformation* pA400LowInfo = new (::rFHAI_)
	  AlarmInformation(
		  Alarm::LOW_URGENCY,     10, LOW_PRES_LOW_MSG, MESSAGE_NAME_NULL);

   AlarmInformation* pA400MediumInfo = new (::rFHAI_)
	  AlarmInformation(
		  Alarm::MEDIUM_URGENCY,  10, LOW_PRES_MED_MSG, MESSAGE_NAME_NULL);

   AlarmInformation* pA400HighInfo = new (::rFHAI_)
	  AlarmInformation(
		  Alarm::HIGH_URGENCY,   140, LOW_PRES_HIGH_MSG, MESSAGE_NAME_NULL);


   // Define count constraints.

   // Last 2 to 3 breaths meet detection criteria
   CountConstraint* pG40001Constraint = new (::rFHCC_) 
	  CountConstraint(Constraint::GE, Constraint::LT, 
			  CalcFunctions::Return2Events, CalcFunctions::Return4Events);

   // Last 4 to 9 breaths meet detection criteria
   CountConstraint* pG40002Constraint = new (::rFHCC_) 
	  CountConstraint(Constraint::GE, Constraint::LT, 
			  CalcFunctions::Return4Events, CalcFunctions::Return10Events);

   // Last 10 or more breaths meet detection criteria
   CountConstraint* pG40003Constraint = new (::rFHCC_)
	  CountConstraint(Constraint::GE, Constraint::EQ,
			  CalcFunctions::Return10Events, NULL);

   // >= 2 breaths meet detection criteria
   CountConstraint* pG40005Constraint = new (::rFHCC_)
	  CountConstraint(Constraint::GE, Constraint::EQ, 
			  CalcFunctions::Return2Events, NULL);


   // Create operating groups for the above constraints 
   OperatingGroup* pG40001 = new (::rFHOG_) 
	 OperatingGroup( G40001,pG40001Constraint);
   AlarmOperand::AddOperand(pG40001);

   OperatingGroup* pG40002 = new (::rFHOG_) 
	 OperatingGroup( G40002,pG40002Constraint);
   AlarmOperand::AddOperand(pG40002);

   OperatingGroup* pG40003 = new (::rFHOG_) 
	 OperatingGroup( G40003,pG40003Constraint);
   AlarmOperand::AddOperand(pG40003);

   OperatingGroup* pG40005 = new (::rFHOG_) 
	 OperatingGroup( G40005,pG40005Constraint);
   AlarmOperand::AddOperand(pG40005);


   // Create a Breath count Violation History Manager, delete breaths > 10
   VHMPrevBreathCountCardinal* pG40004Vhm = new (::rFHVHMPBCC_) 
	  VHMPrevBreathCountCardinal(CalcFunctions::Return10Events);

   // Create a VHM operating group 
   OperatingGroup* pG40004 = new (::rFHOG_) 
	 OperatingGroup( G40004,NULL,NULL,pG40004Vhm);
   AlarmOperand::AddOperand(pG40004);



   // define the alarm
   Alarm* pA400 = new (::rFHA_) 
	 Alarm(A400, *pA400Info, *pG40000, C400, TRUE, LOW_PRES_CIRC_MSG);

   Alarm::AddAlarm(pA400);                   // Add the alarm


   // Create the operating components for this alarm.  

   OperatingComponent* pKG40000_G32000 = new (::rFHOK_) // $[05039]
      OperatingComponent(*pG32000, OperatingComponent::AND, TRUE);

   OperatingComponent* pKG40000_G40005 = new (::rFHOK_)
      OperatingComponent(*pG40005, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG40005_G40004 = new (::rFHOK_)
      OperatingComponent(*pG40004, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG40004_C400 = new (::rFHOK_)
      OperatingComponent(*pC400,   OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG40001_G40004 = new (::rFHOK_)
      OperatingComponent(*pG40004, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG40002_G40004 = new (::rFHOK_)
      OperatingComponent(*pG40004, OperatingComponent::OR,  FALSE);

   OperatingComponent* pKG40003_G40004 = new (::rFHOK_)
      OperatingComponent(*pG40004, OperatingComponent::OR,  FALSE);




   // Connect the operating components to the parent/child relationships
   pG40000->addChild(*pKG40000_G40005);
   pG40000->addChild(*pKG40000_G32000);
   pG40005->addChild(*pKG40005_G40004);
   pG40004->addChild(*pKG40004_C400);
   pG40001->addChild(*pKG40001_G40004);
   pG40002->addChild(*pKG40002_G40004);
   pG40003->addChild(*pKG40003_G40004);

   pG32000->addParent(*pG40000);
   pG40005->addParent(*pG40000);
   pG40004->addParent(*pG40005);
   pC400  ->addParent(*pG40004);
   pG40004->addParent(*pG40001);
   pG40004->addParent(*pG40002);
   pG40004->addParent(*pG40003);


   // Create the condition augmentations for this alarm
   ConditionAugmentation* pA400_G40001Aug = new (::rFHCA_)
      ConditionAugmentation(*pA400LowInfo,    *pA400, *pG40001);

   ConditionAugmentation* pA400_G40002Aug = new (::rFHCA_)
      ConditionAugmentation(*pA400MediumInfo, *pA400, *pG40002);

   ConditionAugmentation* pA400_G40003Aug = new (::rFHCA_)
      ConditionAugmentation(*pA400HighInfo, *pA400, *pG40003);

   pG40000->setCausedAlarm(pA400);           // Set the caused state

   pA400->addAugmentation(*pA400_G40001Aug); // Add the augmented state
   pG40001->addAugmentation(*pA400_G40001Aug);

   pA400->addAugmentation(*pA400_G40002Aug); // Add the augmented state
   pG40002->addAugmentation(*pA400_G40002Aug);

   pA400->addAugmentation(*pA400_G40003Aug); // Add the augmented state
   pG40003->addAugmentation(*pA400_G40003Aug);


   //======================================================
   //
   //   G32000:  Loss of Patient Data Monitoring
   //
   //   This is not an alarm, but is a common group included by many alarms.
   //   All alarms identified as Patient Data Alarms in the Alarm Matrices will
   //   AND the negation of the state of this group in their detection criteria.
   //   This group includes all conditions which cause Loss of Patient Data 
   //   Monitoring.  If any of the conditions which cause Loss of Patient Data
   //   Monitoring become true, all of the Patient Data Alarms would become 
   //   inactive because of this OperatingGroup object's state being included
   //   in their detection criteria $[05039].
   //
   //======================================================

   // Create the operating components. 
   OperatingComponent* pKG32000_G32001 = new (::rFHOK_)
      OperatingComponent(*pG32001, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32000_G32002 = new (::rFHOK_)
      OperatingComponent(*pG32002, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32000_G32003 = new (::rFHOK_)
      OperatingComponent(*pG32003, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32000_G32004 = new (::rFHOK_)
      OperatingComponent(*pG32004, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32000_G32005 = new (::rFHOK_)
      OperatingComponent(*pG32005, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG32001_G28500 = new (::rFHOK_)
      OperatingComponent(*pG28500, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32001_G4500 = new (::rFHOK_)
      OperatingComponent(*pG4500, OperatingComponent::AND, FALSE);
   
   OperatingComponent* pKG32002_G28000 = new (::rFHOK_)
      OperatingComponent(*pG28000, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32002_G5500 = new (::rFHOK_)
      OperatingComponent(*pG5500, OperatingComponent::AND, FALSE);
   
   OperatingComponent* pKG32003_G28000 = new (::rFHOK_)
      OperatingComponent(*pG28000, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32003_G28500 = new (::rFHOK_)
      OperatingComponent(*pG28500, OperatingComponent::AND, FALSE);
   
   OperatingComponent* pKG32004_G500 = new (::rFHOK_)
      OperatingComponent(*pG500, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32004_G3500 = new (::rFHOK_)
      OperatingComponent(*pG3500, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32004_G8000 = new (::rFHOK_)
      OperatingComponent(*pG8000, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32004_G29500 = new (::rFHOK_)
      OperatingComponent(*pG29500, OperatingComponent::OR, FALSE);

   OperatingComponent* pKG32005_G30000 = new (::rFHOK_)
      OperatingComponent(*pG30000, OperatingComponent::OR, FALSE);
   OperatingComponent* pKG32005_G30500 = new (::rFHOK_)
      OperatingComponent(*pG30500, OperatingComponent::OR, FALSE);

  // Connect the operating components to the parent/child relationships
   pG32000->addChild(*pKG32000_G32001);  
   pG32000->addChild(*pKG32000_G32002);  
   pG32000->addChild(*pKG32000_G32003);  
   pG32000->addChild(*pKG32000_G32004);  
   pG32000->addChild(*pKG32000_G32005);  
 
   pG32001->addChild(*pKG32001_G28500);  
   pG32001->addChild(*pKG32001_G4500);  
   
   pG32002->addChild(*pKG32002_G28000);  
   pG32002->addChild(*pKG32002_G5500);  
   
   pG32003->addChild(*pKG32003_G28000);  
   pG32003->addChild(*pKG32003_G28500);  
   
   pG32004->addChild(*pKG32004_G500);  
   pG32004->addChild(*pKG32004_G3500);  
   pG32004->addChild(*pKG32004_G8000);  
   pG32004->addChild(*pKG32004_G29500);
  
   pG32005->addChild(*pKG32005_G30000);  
   pG32005->addChild(*pKG32005_G30500); 

   pG32001->addParent(*pG32000);
   pG32002->addParent(*pG32000);
   pG32003->addParent(*pG32000);
   pG32004->addParent(*pG32000);
   pG32005->addParent(*pG32000);

   pG28500->addParent(*pG32001);
   pG4500->addParent(*pG32001);
 
   pG28000->addParent(*pG32002);
   pG5500->addParent(*pG32002);
 
   pG28000->addParent(*pG32003);
   pG28500->addParent(*pG32003);

   pG500->addParent(*pG32004);
   pG3500->addParent(*pG32004);
   pG8000->addParent(*pG32004);
   pG29500->addParent(*pG32004);

   pG30000->addParent(*pG32005);
   pG30500->addParent(*pG32005);
   // $[TI1]

   //======================================================
   //
   //   A1000:  Inoperative PROX Board
   //   $[PX06001]
   //
   //======================================================

   // Create the alarm information for this alarm
   AlarmInformation* pA1000Info = new (::rFHAI_)
      AlarmInformation(Alarm::LOW_URGENCY, 120, PROX_INOPERATIVE_ANALYSIS_MSG, 
         PROX_INOPERATIVE_REMEDY_MSG );

   Alarm* pA1000 = new (::rFHA_)              // Define the alarm
      Alarm(A1000, *pA1000Info, *pG100000,  OPERAND_NAME_NULL,
            FALSE, PROX_INOPERATIVE_MSG, TRUE);
   
   Alarm::AddAlarm(pA1000);                   // Add the alarm
   
   // Create the operating components for this alarm.  
   OperatingComponent* pKG100000_C1000 = new (::rFHOK_)
      OperatingComponent(*pC1000,   OperatingComponent::OR,  FALSE);
   
   // Connect the operating components to the parent/child relationships
   pG100000->addChild(*pKG100000_C1000);  
   
   pC1000  ->addParent(*pG100000);
   
   
   pG100000->setCausedAlarm(pA1000);           // Set the caused state

} // end InitHierarchy.


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SendEndOfBreathEvent
//
//@ Interface-Description
//      This method is messaged by the PatientDataMgr class when new
//      end-of-breath data is available.  The END_OF_BREATH_EN event is sent
//      to the Alarm task OperatingParameterEventQueue.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::SendEndOfBreathEvent(void)
{
  CALL_TRACE("SendEndOfBreathEvent(void)");

  AlarmManager::GetBreathType() = (::BreathType)
    PatientDataMgr::GetDiscreteValue(PatientDataId::PREVIOUS_BREATH_TYPE_ITEM);  
  Alarm::AppendTaskQueueEvent(END_OF_BREATH_EN);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SendAveragedDataEvent
//
//@ Interface-Description
//      This method is messaged by the PatientDataMgr class when new
//      averaged patient data is available.  The AVERAGED_PATIENT_DATA_EN
//      event is sent to the Alarm task OperatingParameterEventQueue.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::SendAveragedDataEvent(void)
{
  CALL_TRACE("SendAveragedDataEvent(void)");

  Alarm::AppendTaskQueueEvent(AVERAGED_PATIENT_DATA_EN);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBreathType
//
//@ Interface-Description
//      This method returns a reference to its static data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
::BreathType&
AlarmManager::GetBreathType(void)
{
  CALL_TRACE("GetBreathType(void)");

  static ::BreathType BreathType = ::CONTROL;

  return(BreathType);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ManualResetAlarms
//
//@ Interface-Description
//      This method is messaged when the event indicating the operator
//      pressed the Alarm Reset key is received.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::ManualResetAlarms(void)
{
  CALL_TRACE("ManualResetAlarms(void)");

  Alarm::ListManResetAlarmConditions(AlarmManager::ROperandNameList_);
  Alarm::ListActivePDResetAlarmConditions(AlarmManager::ROperandNameList_);
  AlarmManager::ProcessOperands_( AlarmManager::RESET_O_UT);
  
  AlarmManager::ReevaluateAlarms_(Alarm::MANUAL_RESET_UT);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//      This method is called as part of Settings' observer/subject
//      mechanism, when a setting that this class is "observing" changes.
//      The type of change (accepted vs. adjusted) and a pointer to the
//      changed subject are passed as parameters.
//
//      If any Alarm objects must be reevaluated due to a particular setting
//      change, an event will be appended to the OperatingParameterEventQueue
//      accordingly.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
AlarmManager::valueUpdate(const Notification::ChangeQualifier qualifierId,
			  const SettingSubject*               pSubject)
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");

  if (qualifierId == Notification::ACCEPTED) // $[TI1]
  {
    switch (pSubject->getId())
    {
      case SettingId::TRIGGER_TYPE: // $[TI1.1]
	Alarm::AppendTaskQueueEvent(TRIGGER_SETTING_CHANGE_EN);
	break;
      case SettingId::OXYGEN_PERCENT: // $[TI1.2]
	Alarm::AppendTaskQueueEvent(O2_PERCENT_SETTING_CHANGE_EN);
	break;
      case SettingId::APNEA_O2_PERCENT: // $[TI1.3]
	Alarm::AppendTaskQueueEvent(O2_PERCENT_SETTING_CHANGE_EN);
	break;
      case SettingId::VENT_TYPE: // $[TI1.4]
	Alarm::AppendTaskQueueEvent(VENT_TYPE_SETTING_CHANGE_EN);
	break;
      default:
	// unexpected setting subject...
	AUX_CLASS_ASSERTION_FAILURE(pSubject->getId());
	break;
    };
  } // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment
//
//@ Interface-Description
//      When task-level, state changes occur, all subjects are notified
//	to detach from all observers.  This is done because the GUI-Apps
//	observers may be overwritten by new class instances, without any
//	explicit destructor/detachment calls.
//
//	This method is defined here to indicate to any of this instance's
//	subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
AlarmManager::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]

#endif // SIGMA_GUI_CPU


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SendEvent
//
//@ Interface-Description
//  This method is the client/server portion of class AlarmManager.
//
//  This method sends Operating Parameter Events sent to the Alarm-Analysis
//  subsystem across the Ethernet link.
// >Von
//  eventName     (Int32)OperatingParamEventName.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmManager::SendEvent(Int32 eventName)
{
  CALL_TRACE("SendEvent(eventName)");

  SocketWrap sock;
  Int32 tempInt = (Int32)sock.HTONL((Uint32)eventName);
  //Send the Operating Parameter Events across the Ethernet link.
  ::XmitData(SEND_EVENT_MSG_ID, &tempInt, sizeof(tempInt));
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReceiveEvent
//
//@ Interface-Description
//  This method is the server for AlarmManager::SendEvent().  ReceiveEvent 
//  posts the event pointed to by pDataBlock to the Operating Parameter Event
//  Queue.
// >Von
//  msgId           Enumerator indicating which message has been sent.  This
//                  parameter is unused because there is only one message sent
//                  to this server.
//  pDataBlock      Pointer to an Int32.
//  size            Size (in bytes) of data pointed to by pDataBlock.   This
//                  parameter is unused because there is only one message sent
//                  to this server.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   (pDataBlock != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmManager::ReceiveEvent(XmitDataMsgId msgId, void* pDataBlock,Uint size)
{
  CALL_TRACE("ReceiveEvent(msgId, pDataBlock, size)");
  UNUSED_SYMBOL(msgId);
  UNUSED_SYMBOL(size);


  CLASS_PRE_CONDITION(pDataBlock != NULL);

  SocketWrap sock;
  //This function is specific to receiving 32-bit alarm event so it
  //knows that it receives 32-bit in pData but it's declared void* to match the
  //prototype of the callback.. the callback prototype could have been done a template
  *((Uint32*)pDataBlock) = sock.NTOHL(*((Uint32*)pDataBlock));
  // Send the alarm event pointed to by pDataBlock to the Operating Parameter
  // Event Queue.
  Alarm::AppendTaskQueueEvent(*((Int32*)pDataBlock));
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessTaskQueue
//
//@ Interface-Description
//      This method is messaged when an event is pending on the
//      OperatingParameterEventQueue.
//---------------------------------------------------------------------
//@ Implementation-Description
//      There are two basic types of events
//      which are sent to the OperatingParameterEventQueue: events which 
//      must be processed by themselves to completion in one cycle of alarm
//      subsystem processing (MANUAL_RESET_EN) and events which may be 
//      processed together with other
//      events of the same type.  The latter is comprised of two subtypes:
//      OperatingParameterEventName enumerators which force evaluation of the
//      associated OperatingCondition objects and pointers to ViolationHistory 
//      objects (when a timer associated with a ViolationHistory object counts 
//      down to zero, the timer sends the pointer to the ViolationHistory 
//      object as a message to the Alarm task which forces evaluation of the 
//      parent OperatingGroup objects of the OperatingGroup pointed to by the 
//      ViolationHistory object).
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::ProcessTaskQueue(void)
{
	CALL_TRACE("ProcessTaskQueue(void)");

  Int32 eventMessage = 0;    // Event message

  // Handle to OPERATING_PARAMETER_EVENT_Q
  MsgQueue operatingParameterEventQ(OPERATING_PARAMETER_EVENT_Q);

  Int32 queueStatus;  // Queue return status value

#if !defined(SIGMA_UNIT_TEST)
  while(TRUE) // This will always be executed.
  {
#endif
    queueStatus = operatingParameterEventQ.pendForMsg(eventMessage);

    // Check to see if an event was read.
    CLASS_ASSERTION(queueStatus == Ipc::OK); 

    AlarmQueueMsg msg;
    msg.qWord = eventMessage;

    switch ( msg.event.eventType )
    {
      case AlarmQueueMsg::TASK_MONITOR_EVENT:            // $[TI1]
	TaskMonitor::Report();
        break;

      case AlarmQueueMsg::OPERATING_PARAMETER_EVENT:     // $[TI3]
	// The event should not be EVENT_NAME_NULL.
	CLASS_ASSERTION(msg.event.eventData != EVENT_NAME_NULL);

        AlarmManager::ProcessEvents_(msg.qWord);
#ifdef SIGMA_GUI_CPU
	AlarmManager::ReevaluateAlarmOperands_();
#endif // SIGMA_GUI_CPU
	break;

#ifdef SIGMA_GUI_CPU
      case AlarmQueueMsg::ALARM_BROADCAST_EVENT:     // $[TI4]
        AlarmBroadcastArray::BroadcastAlarms();
        break;

      case AlarmQueueMsg::VIOLATION_HISTORY_EVENT:       // $[TI2]
	AlarmManager::ProcessViolationHistoryObj_(msg.qWord);
	AlarmManager::ReevaluateAlarmOperands_();
	break;
#endif // SIGMA_GUI_CPU

      default:
	CLASS_ASSERTION_FAILURE();
	break;
    };

#if !defined(SIGMA_UNIT_TEST)
  } // while (TRUE)
#endif

}





//=====================================================================
//
//      Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessEvents_
//
//@ Interface-Description
//      This method is messaged when an event is retrieved from the
//      OperatingParameterEventQueue.
//
// >Von
//      eventMessage   Events which map to OperatingConditions which
//                     reside on both CPUs.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method handles events.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::ProcessEvents_(Int32 eventMessage)
{
  CALL_TRACE("ProcessEvents_(eventMessage)");

  AlarmQueueMsg msg;
  msg.qWord = eventMessage;

#ifdef SIGMA_BD_CPU
  switch (msg.event.eventData)
  {
    case LOI_LED_ON_EN: // $[TI1]
      VentStatus::DirectVentStatusService(VentStatus::LOSS_OF_GUI_LED,
          VentStatus::ACTIVATE);
      break;
    case LOI_LED_OFF_EN: // $[TI2]
      VentStatus::DirectVentStatusService(VentStatus::LOSS_OF_GUI_LED, 
          VentStatus::DEACTIVATE);
      break;
    case BD_ALARM_ON_EN: // $[TI3]
      VentStatus::DirectVentStatusService(VentStatus::BD_AUDIO_ALARM,   
          VentStatus::ACTIVATE);
      break;
    case BD_ALARM_OFF_EN: // $[TI4]
      VentStatus::DirectVentStatusService(VentStatus::BD_AUDIO_ALARM, 
          VentStatus::DEACTIVATE);
      break;
    default: // $[TI5]
      AlarmManager::SendEvent(eventMessage);
      break;
  };
#endif // SIGMA_BD_CPU

#ifdef SIGMA_GUI_CPU

  // Retrieve all alarms.
  const AlarmList& alarms = Alarm::GetAlarms();

  OperandName tempName;

    // Events which map to OperatingConditions which reside on the
    // GUI_CPU.

  switch (msg.event.eventData)
  {
    case BTAT_1_EN: // $[TI1]
      ConditionEvaluations::SetCondition(C30);
      AlarmManager::ROperandNameList_.append(tempName = C30); 
      break;
    case NOT_BTAT_1_EN: // $[TI2]
      ConditionEvaluations::ClearCondition(C30);
      AlarmManager::ROperandNameList_.append(tempName = C30); 
      break;
    case BTAT_2_EN: // $[TI3] 
      ConditionEvaluations::SetCondition(C35);
      AlarmManager::ROperandNameList_.append(tempName = C35); 
      break;
    case NOT_BTAT_2_EN: // $[TI4] 
      ConditionEvaluations::ClearCondition(C35);
      AlarmManager::ROperandNameList_.append(tempName = C35); 
      break;
    case BTAT_3_EN:  // $[TI5]
      ConditionEvaluations::SetCondition(C40);
      AlarmManager::ROperandNameList_.append(tempName = C40); 
      break;
    case NOT_BTAT_3_EN:  // $[TI6]
      ConditionEvaluations::ClearCondition(C40);
      AlarmManager::ROperandNameList_.append(tempName = C40); 
      break;
    case BTAT_4_EN: // $[TI7]
      ConditionEvaluations::SetCondition(C45);
      AlarmManager::ROperandNameList_.append(tempName = C45); 
      break;
    case NOT_BTAT_4_EN: // $[TI8]
      ConditionEvaluations::ClearCondition(C45);
      AlarmManager::ROperandNameList_.append(tempName = C45); 
      break;
    case BTAT_5_EN: // $[TI9]
      ConditionEvaluations::SetCondition(C50);
      AlarmManager::ROperandNameList_.append(tempName = C50); 
      break;
    case NOT_BTAT_5_EN: // $[TI10]
      ConditionEvaluations::ClearCondition(C50);
      AlarmManager::ROperandNameList_.append(tempName = C50); 
      break;
    case BTAT_6_EN: // $[TI11]
      ConditionEvaluations::SetCondition(C55);
      AlarmManager::ROperandNameList_.append(tempName = C55); 
      break;
    case NOT_BTAT_6_EN: // $[TI12]
      ConditionEvaluations::ClearCondition(C55);
      AlarmManager::ROperandNameList_.append(tempName = C55); 
      break;
    case BTAT_7_EN: // $[TI13]
      ConditionEvaluations::SetCondition(C60);
      AlarmManager::ROperandNameList_.append(tempName = C60); 
      break;
    case NOT_BTAT_7_EN: // $[TI14]
      ConditionEvaluations::ClearCondition(C60);
      AlarmManager::ROperandNameList_.append(tempName = C60); 
      break;
    case BTAT_8_EN: // $[TI15]
      ConditionEvaluations::SetCondition(C65);
      AlarmManager::ROperandNameList_.append(tempName = C65); 
      break;
    case NOT_BTAT_8_EN: // $[TI16]
      ConditionEvaluations::ClearCondition(C65);
      AlarmManager::ROperandNameList_.append(tempName = C65); 
      break;
    case BTAT_9_EN: // $[TI17]
      ConditionEvaluations::SetCondition(C70);
      AlarmManager::ROperandNameList_.append(tempName = C70); 
      break;
    case NOT_BTAT_9_EN: // $[TI18]
      ConditionEvaluations::ClearCondition(C70);
      AlarmManager::ROperandNameList_.append(tempName = C70); 
      break;
    case BTAT_10_EN: // $[TI19] 
      ConditionEvaluations::SetCondition(C75);
      AlarmManager::ROperandNameList_.append(tempName = C75); 
      break;
    case NOT_BTAT_10_EN: // $[TI20]
      ConditionEvaluations::ClearCondition(C75);
      AlarmManager::ROperandNameList_.append(tempName = C75); 
      break; 
    case BTAT_11_EN: // $[TI21]
      ConditionEvaluations::SetCondition(C80);
      AlarmManager::ROperandNameList_.append(tempName = C80); 
      break;
    case NOT_BTAT_11_EN: // $[TI22]
      ConditionEvaluations::ClearCondition(C80);
      AlarmManager::ROperandNameList_.append(tempName = C80); 
      break;
    case BTAT_12_EN: // $[TI23]
      ConditionEvaluations::SetCondition(C85);
      AlarmManager::ROperandNameList_.append(tempName = C85); 
      break;
    case NOT_BTAT_12_EN: // $[TI24]
      ConditionEvaluations::ClearCondition(C85);
      AlarmManager::ROperandNameList_.append(tempName = C85); 
      break;
    case BTAT_13_EN: // $[TI25]
      ConditionEvaluations::SetCondition(C90);
      AlarmManager::ROperandNameList_.append(tempName = C90); 
      break;
    case NOT_BTAT_13_EN: // $[TI26]
      ConditionEvaluations::ClearCondition(C90);
      AlarmManager::ROperandNameList_.append(tempName = C90); 
      break;
    case BTAT_14_EN: // $[TI27]
      ConditionEvaluations::SetCondition(C95);
      AlarmManager::ROperandNameList_.append(tempName = C95); 
      break;
    case NOT_BTAT_14_EN: // $[TI28]
      ConditionEvaluations::ClearCondition(C95);
      AlarmManager::ROperandNameList_.append(tempName = C95); 
      break;
    case BTAT_15_EN: // $[TI29]
      ConditionEvaluations::SetCondition(C100);
      AlarmManager::ROperandNameList_.append(tempName = C100); 
      break;
    case NOT_BTAT_15_EN: // $[TI30]
      ConditionEvaluations::ClearCondition(C100);
      AlarmManager::ROperandNameList_.append(tempName = C100); 
      break;
    case BTAT_16_EN: // $[TI31]
      ConditionEvaluations::SetCondition(C105);
      AlarmManager::ROperandNameList_.append(tempName = C105); 
      break;
    case NOT_BTAT_16_EN: // $[TI32]
      ConditionEvaluations::ClearCondition(C105);
      AlarmManager::ROperandNameList_.append(tempName = C105); 
      break;
    case BTAT_17_EN: // $[TI33]
      ConditionEvaluations::SetCondition(C110);
      AlarmManager::ROperandNameList_.append(tempName = C110); 
      break;
    case NOT_BTAT_17_EN: // $[TI34]
      ConditionEvaluations::ClearCondition(C110);
      AlarmManager::ROperandNameList_.append(tempName = C110); 
      break;
    case MINOR_POST_FAULT_EN: // $[TI35]
      ConditionEvaluations::SetCondition(C190);            
      AlarmManager::ROperandNameList_.append(tempName = C190); 
      break;
    case NOT_MINOR_POST_FAULT_EN: // $[TI36]
      ConditionEvaluations::ClearCondition(C190);            
      AlarmManager::ROperandNameList_.append(tempName = C190); 
      break;
    case WALL_AIR_PRESENT_EN: // $[TI37] 
      ConditionEvaluations::ClearCondition(C5);
      AlarmManager::ROperandNameList_.append(tempName = C5); 
      break;
    case NOT_WALL_AIR_PRESENT_EN: // $[TI38] 
      ConditionEvaluations::SetCondition(C5);
      AlarmManager::ROperandNameList_.append(tempName = C5); 
      break;
    case COMP_AIR_PRESENT_EN: // $[TI41]
      ConditionEvaluations::ClearCondition(C140);
      AlarmManager::ROperandNameList_.append(tempName = C140); 
      break;
    case NOT_COMP_AIR_PRESENT_EN: // $[TI42]
      ConditionEvaluations::SetCondition(C140);
      AlarmManager::ROperandNameList_.append(tempName = C140); 
      break;
    case COMP_OPTION_PRESENT_EN: // $[TI43] 
      ConditionEvaluations::SetCondition(C145);
      AlarmManager::ROperandNameList_.append(tempName = C145); 
      break;
    case NOT_COMP_OPTION_PRESENT_EN: // $[TI44] 
      ConditionEvaluations::ClearCondition(C145);
      AlarmManager::ROperandNameList_.append(tempName = C145); 
      break;
    case TRIGGER_SETTING_CHANGE_EN: // $[TI48]         
      AlarmManager::ROperandNameList_.append(tempName = C170); 
      break;
    case WALL_O2_PRESENT_EN: // $[TI53]
      ConditionEvaluations::ClearCondition(C200);            
      AlarmManager::ROperandNameList_.append(tempName = C200); 
      break;
    case NOT_WALL_O2_PRESENT_EN: // $[TI54]
      ConditionEvaluations::SetCondition(C200);            
      AlarmManager::ROperandNameList_.append(tempName = C200); 
      break;
    case O2_PERCENT_SETTING_CHANGE_EN: // $[TI57]
      AlarmManager::ROperandNameList_.append(tempName = C210); 
      AlarmManager::ROperandNameList_.append(tempName = C215); 
      AlarmManager::ROperandNameList_.append(tempName = C220); 
      AlarmManager::ROperandNameList_.append(tempName = C225); 
      break;
    case INTERNAL_BATTERY_POWER_EN: // $[TI58]
      ConditionEvaluations::SetCondition(C260);
      AlarmManager::ROperandNameList_.append(tempName = C260); 
      break;
    case NOT_INTERNAL_BATTERY_POWER_EN: // $[TI59]
      ConditionEvaluations::ClearCondition(C260);
      AlarmManager::ROperandNameList_.append(tempName = C260); 
      break;
    case BATTERY_TIME_LEFT_LT_2_MIN_EN: // $[TI83]
      ConditionEvaluations::SetCondition(C261);
      AlarmManager::ROperandNameList_.append(tempName = C261); 
      break;
    case NOT_BATTERY_TIME_LEFT_LT_2_MIN_EN: // $[TI78]
      ConditionEvaluations::ClearCondition(C261);
      AlarmManager::ROperandNameList_.append(tempName = C261); 
      break;
    case NON_FUNCTIONAL_BATTERY_EN: // $[TI79]
      ConditionEvaluations::SetCondition(C262);
      AlarmManager::ROperandNameList_.append(tempName = C262); 
      break;
    case NOT_NON_FUNCTIONAL_BATTERY_EN: // $[TI80]
      ConditionEvaluations::ClearCondition(C262);
      AlarmManager::ROperandNameList_.append(tempName = C262); 
      break;
    case CHARGE_TIME_GT_8_HRS_EN: // $[TI81]
      ConditionEvaluations::SetCondition(C263);
      AlarmManager::ROperandNameList_.append(tempName = C263); 
      break;
    case NOT_CHARGE_TIME_GT_8_HRS_EN: // $[TI82]
      ConditionEvaluations::ClearCondition(C263);
      AlarmManager::ROperandNameList_.append(tempName = C263); 
      break;
    case PROCEDURE_ERROR_EN:  // $[TI60]
      ConditionEvaluations::SetCondition(C265);
      AlarmManager::ROperandNameList_.append(tempName = C265); 
      break;
    case NOT_PROCEDURE_ERROR_EN:  // $[TI61]
      ConditionEvaluations::ClearCondition(C265);
      AlarmManager::ROperandNameList_.append(tempName = C265); 
      break;
    case LOW_AC_POWER_EN:  // $[TI62]
      ConditionEvaluations::SetCondition(C270);
      AlarmManager::ROperandNameList_.append(tempName = C270); 
     break;
    case NOT_LOW_AC_POWER_EN:  // $[TI63]
      ConditionEvaluations::ClearCondition(C270);
      AlarmManager::ROperandNameList_.append(tempName = C270); 
      break;
    case SEVERE_OCCLUSION_EN: // $[TI84] 
      ConditionEvaluations::SetCondition(C290);
      AlarmManager::ROperandNameList_.append(tempName = C290); 
      break;
    case NOT_SEVERE_OCCLUSION_EN: // $[TI85] 
      ConditionEvaluations::ClearCondition(C290);
      AlarmManager::ROperandNameList_.append(tempName = C290); 
      break;
    case DISCONNECT_WITH_COMPRESSOR_EN:
      alarms[A295]->setRemedyMessage(CIRCUIT_DISCONNECT_COMPRESSOR_REMEDY_MSG);
	  ConditionEvaluations::SetCondition(C193);            
	  AlarmManager::ROperandNameList_.append(tempName = C193); 
	  break;
    case DISCONNECT_EN:  // $[TI86]
      alarms[A295]->setRemedyMessage(CIRCUIT_DISCONNECT_REMEDY_MSG);
      ConditionEvaluations::SetCondition(C193);            
      AlarmManager::ROperandNameList_.append(tempName = C193); 
      break;
    case NOT_DISCONNECT_EN:  // $[TI87]
      ConditionEvaluations::ClearCondition(C193);            
      AlarmManager::ROperandNameList_.append(tempName = C193); 
      break;
    case STARTUP_DISCONNECT_EN: // $[TI88]
      ConditionEvaluations::SetCondition(C195);            
      AlarmManager::ROperandNameList_.append(tempName = C195); 
      break;
    case NOT_STARTUP_DISCONNECT_EN: // $[TI89]
      ConditionEvaluations::ClearCondition(C195);            
      AlarmManager::ROperandNameList_.append(tempName = C195); 
      break;
    case IN_APNEA_VENTILATION_EN: // $[TI69]
      ConditionEvaluations::SetCondition(C10);
      AlarmManager::ROperandNameList_.append(tempName = C210); 
      AlarmManager::ROperandNameList_.append(tempName = C215); 
      AlarmManager::ROperandNameList_.append(tempName = C220); 
      AlarmManager::ROperandNameList_.append(tempName = C225); 
      AlarmManager::ROperandNameList_.append(tempName = C10);
      break;
    case NOT_IN_APNEA_VENTILATION_EN: // $[TI70] 
      ConditionEvaluations::ClearCondition(C10);
      AlarmManager::ROperandNameList_.append(tempName = C210); 
      AlarmManager::ROperandNameList_.append(tempName = C215); 
      AlarmManager::ROperandNameList_.append(tempName = C220); 
      AlarmManager::ROperandNameList_.append(tempName = C225); 
      AlarmManager::ROperandNameList_.append(tempName = C10); 
      break;
    case INSP_TIME_TOO_LONG_EN: // $[TI71]
      ConditionEvaluations::SetCondition(C15);
      AlarmManager::ROperandNameList_.append(tempName = C15); 
      break;
    case HIGH_CIRCUIT_PRESSURE_EN: // $[TI72]
      switch ((::BreathType)
          PatientDataMgr::GetDiscreteValue(PatientDataId::BREATH_TYPE_ITEM))
      {
        case CONTROL: // $[TI72.1]
        case ASSIST:  // $[TI72.2]
          // Examine the HIGH CIRCUIT PRESSURE alarm for a mandatory breath.
          ConditionEvaluations::SetCondition(C136);
          AlarmManager::ROperandNameList_.append(tempName = C136);
          break;
        case SPONT: // $[TI72.3]
          // Examine the HIGH CIRCUIT PRESSURE alarm for a spontaneous breath.
          ConditionEvaluations::SetCondition(C135);
          AlarmManager::ROperandNameList_.append(tempName = C135);
          break;
        case NON_MEASURED: // $[TI72.4]
          break;
        default:
          CLASS_ASSERTION(FALSE);
          break;
      };
      break;
    case VENT_PRESSURE_EN: // $[TI73]
      ConditionEvaluations::SetCondition(C325);
      AlarmManager::ROperandNameList_.append(tempName = C325); 
      break;
    case VOLUME_NOT_DELIVERED_VS_EN: // $[TI105]
      ConditionEvaluations::SetCondition(C365);            
      AlarmManager::ROperandNameList_.append(tempName = C365); 
      break;
    case VOLUME_NOT_DELIVERED_VC_EN: // $[TI108]
      ConditionEvaluations::SetCondition(C366);            
      AlarmManager::ROperandNameList_.append(tempName = C366); 
      break;
    case END_OF_BREATH_EN: // $[TI74]
      ConditionEvaluations::ClearCondition(C15);     
      ConditionEvaluations::ClearCondition(C135);     
      ConditionEvaluations::ClearCondition(C136);     
      ConditionEvaluations::ClearCondition(C325);     
      ConditionEvaluations::ClearCondition(C335);     
      ConditionEvaluations::ClearCondition(C345);     
      ConditionEvaluations::ClearCondition(C365);     
      ConditionEvaluations::ClearCondition(C366);     
      ConditionEvaluations::ClearCondition(C380);
      ConditionEvaluations::ClearCondition(C385); 
      ConditionEvaluations::ClearCondition(C390);       

      AlarmManager::ROperandNameList_.append(tempName = C325);
      AlarmManager::ROperandNameList_.append(tempName = C335);

      switch (AlarmManager::GetBreathType())
      {
        case CONTROL: // $[TI74.1]
        case ASSIST:  // $[TI74.2]
          // Examine the LOW EXHALED MANDATORY TIDAL VOLUME and
          // INSPIRATION MAND VOLUME LIMIT alarm for a
          // mandatory breath.
          AlarmManager::ROperandNameList_.append(tempName = C136); 
          AlarmManager::ROperandNameList_.append(tempName = C315); // $[05091]
          AlarmManager::ROperandNameList_.append(tempName = C320); // $[05126]
          AlarmManager::ROperandNameList_.append(tempName = C366);
          AlarmManager::ROperandNameList_.append(tempName = C390);
          AlarmManager::ROperandNameList_.append(tempName = C400);
          break;
        case SPONT: // $[TI74.3]
          // Examine the LOW EXHALED SPONTANEOUS TIDAL VOLUME alarm,
          // the INSPIRATION TOO LONG alarm, the COMPENSATION LIMIT alarm
          // and the INSPIRATION SPONT VOLUME LIMIT and 
          // INSPIRATION SPONT PRESSURE LIMIT alarms for a 
          // spontaneous breath.
          AlarmManager::ROperandNameList_.append(tempName = C135); 
          AlarmManager::ROperandNameList_.append(tempName = C315); // $[05091]
          AlarmManager::ROperandNameList_.append(tempName = C321); // $[05141]
          AlarmManager::ROperandNameList_.append(tempName = C15); 
          AlarmManager::ROperandNameList_.append(tempName = C345);
          AlarmManager::ROperandNameList_.append(tempName = C365);
          AlarmManager::ROperandNameList_.append(tempName = C380);
          AlarmManager::ROperandNameList_.append(tempName = C385);
          AlarmManager::ROperandNameList_.append(tempName = C400);
          AlarmManager::ROperandNameList_.append(tempName = C901);
          AlarmManager::ROperandNameList_.append(tempName = C902);
          break;
        case NON_MEASURED: // $[TI74.4]
          // Do END_OF_BREATH processing for INSPIRATION TOO LONG.
          AlarmManager::ROperandNameList_.append(tempName = C15); 
          break;
        default:
          CLASS_ASSERTION(FALSE);
          break;
      };
      break;
    case AVERAGED_PATIENT_DATA_EN: // $[TI75]
      AlarmManager::ROperandNameList_.append(tempName = C295); // $[05085]
      AlarmManager::ROperandNameList_.append(tempName = C300); // $[05115]
      AlarmManager::ROperandNameList_.append(tempName = C305); // $[05133]
      break;
    case FIO2_HIGH_EN: // $[TI91]
      ConditionEvaluations::SetCondition(C150);            
      ConditionEvaluations::ClearCondition(C155);            
      AlarmManager::ROperandNameList_.
          append(tempName = C150); // $[05097]
      AlarmManager::ROperandNameList_.
          append(tempName = C155); // $[05149]
      break;
    case FIO2_NORMAL_EN: // $[TI92]
      ConditionEvaluations::ClearCondition(C150);            
      ConditionEvaluations::ClearCondition(C155);            
      AlarmManager::ROperandNameList_.
          append(tempName = C150); // $[05097]
      AlarmManager::ROperandNameList_.
          append(tempName = C155); // $[05149]
      break;
    case FIO2_LOW_EN: // $[TI93] 
      ConditionEvaluations::ClearCondition(C150);            
      ConditionEvaluations::SetCondition(C155);            
      AlarmManager::ROperandNameList_.
          append(tempName = C150); // $[05097]
      AlarmManager::ROperandNameList_.
          append(tempName = C155); // $[05149]
      break;
    case O2_MONITOR_INSTALLED_EN: // $[TI94]
      ConditionEvaluations::SetCondition(C205);            
      AlarmManager::ROperandNameList_.append(tempName = C205); 
      break;
    case NOT_O2_MONITOR_INSTALLED_EN: // $[TI95]
      ConditionEvaluations::ClearCondition(C205);            
      AlarmManager::ROperandNameList_.append(tempName = C205); 
      break;
    case HUNDRED_PERCENT_O2_EN: // $[TI96]
      ConditionEvaluations::SetIs100PercentO2Requested();
      AlarmManager::ROperandNameList_.append(tempName = C210); 
      AlarmManager::ROperandNameList_.append(tempName = C215); 
      AlarmManager::ROperandNameList_.append(tempName = C220); 
      AlarmManager::ROperandNameList_.append(tempName = C225); 
      break;
    case NOT_HUNDRED_PERCENT_O2_EN: // $[TI97]
      ConditionEvaluations::ClearIs100PercentO2Requested();
      AlarmManager::ROperandNameList_.append(tempName = C210); 
      AlarmManager::ROperandNameList_.append(tempName = C215); 
      AlarmManager::ROperandNameList_.append(tempName = C220); 
      AlarmManager::ROperandNameList_.append(tempName = C225); 
      break;
    case VOLUME_LIMIT_EN: // $[TI98]
      ConditionEvaluations::SetCondition(C335);
      AlarmManager::ROperandNameList_.append(tempName = C335); 
      break;
    case COMPENSATION_LIMIT_EN: // $[TI100]
      ConditionEvaluations::SetCondition(C345);
      AlarmManager::ROperandNameList_.append(tempName = C345); 
      AlarmManager::ROperandNameList_.append(tempName = C901);
      AlarmManager::ROperandNameList_.append(tempName = C902);
      break;
    case MANUAL_RESET_EN: // $[TI90]
      AlarmManager::ManualResetAlarms();
      break;
    case PAV_STARTUP_EN: // $[TI101]
      ConditionEvaluations::SetCondition(C355);            
      AlarmManager::ROperandNameList_.append(tempName = C355); 
      break;
    case NOT_PAV_STARTUP_EN: // $[TI102]
      ConditionEvaluations::ClearCondition(C355);            
      AlarmManager::ROperandNameList_.append(tempName = C355); 
      break;
    case PAV_ASSESSMENT_EN: // $[TI103]
      ConditionEvaluations::SetCondition(C360);            
      AlarmManager::ROperandNameList_.append(tempName = C360); 
      break;
    case NOT_PAV_ASSESSMENT_EN: // $[TI104]
      ConditionEvaluations::ClearCondition(C360);            
      AlarmManager::ROperandNameList_.append(tempName = C360); 
      break;
    case INSPIRED_SPONT_PRESSURE_LIMIT_EN: // $[TI105]
      ConditionEvaluations::SetCondition(C380);
      AlarmManager::ROperandNameList_.append(tempName = C380); 
      AlarmManager::ROperandNameList_.append(tempName = C901);
      AlarmManager::ROperandNameList_.append(tempName = C902);
      break;
    case INSPIRED_SPONT_VOLUME_LIMIT_EN: // $[TI106]
      ConditionEvaluations::SetCondition(C385);
      AlarmManager::ROperandNameList_.append(tempName = C385); 
      break;
    case  INSPIRED_MAND_VOLUME_LIMIT_EN: // $[TI107]
       ConditionEvaluations::SetCondition(C390);            
       AlarmManager::ROperandNameList_.append(tempName = C390); 
       break;
    case VENT_TYPE_SETTING_CHANGE_EN: // $[TI109]
      ConditionEvaluations::ClearCondition(C15);            
      AlarmManager::ROperandNameList_.append(tempName = C15); 
      break;
	case PROX_ALARM_OFF_EN: 
	   ConditionEvaluations::ClearCondition(C1000);            
	   AlarmManager::ROperandNameList_.append(tempName = C1000); 
	   break;
	case PROX_ALARM_ON_EN: 
	   ConditionEvaluations::SetCondition(C1000);            
	   AlarmManager::ROperandNameList_.append(tempName = C1000); 
	   break;
    default:
      CLASS_ASSERTION(FALSE);
      break;
    };
#endif // SIGMA_GUI_CPU
}

#ifdef SIGMA_GUI_CPU
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessViolationHistoryObj_
//
//@ Interface-Description
//      This method is messaged when a ViolationHistory Object
//      is retrieved from the OperatingParameterEventQueue.
//
// >Von
//      eventMessage   Events which map to OperatingConditions which
//                     reside on both CPUs.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      A pointer to a ViolationHistory Object is used to evaluate
//      the parent OperatingGroup objects of the OperatingGroup pointed
//      to by the ViolationHistory object.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::ProcessViolationHistoryObj_(Int32 eventMessage)
{
  CALL_TRACE("ProcessViolationHistoryObj_(eventMessage)");

  AlarmQueueMsg msg;
  msg.qWord = eventMessage;
  MemPtr mPtr = MemoryHandle::GetMemoryPtr( msg.event.eventData );
  CLASS_ASSERTION( mPtr != NULL );
  ViolationHistory* pViolation = static_cast<ViolationHistory*>(mPtr);
  CLASS_PRE_CONDITION(pViolation != NULL);

  // Make sure the pointer is valid.
  ViolationHistory::CheckIsValid(pViolation);

  OperatingGroup* pGroup = pViolation->getGroup();
  CLASS_PRE_CONDITION(pGroup != NULL);

  ViolationHistoryManager* pViolationManager = 
      pGroup->getViolationManager();
  CLASS_PRE_CONDITION(pViolationManager != NULL);

  pViolationManager->violationTimerFired(pViolation);
  // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpdateAlarms_
//
//@ Interface-Description
//      Method UpdateAlarms_ directs the evaluation of the Alarm objects after
//      the Operating Parameter Hierarchy has been evaluated.  
//---------------------------------------------------------------------
//@ Implementation-Description
//      UpdateAlarms_ examines each Alarm object and places those that need to
//      be evaluated on the RCheckAlarmList_.  RCheckAlarmList_ is iterated
//      through four times.  The first time, all ConditionAugmentations are
//      evaluated and their annunciations added to the Alarm object's
//      current analysis and remedy message lists, their system responses
//      and reset system responses run as needed and urgency/priority merged
//      with the Alarm object's.  The second iteration identifies those Alarm
//      objects which are going to be dependent to other superior Alarm
//      objects.  The third iteration adds the dependent annunciations to the
//      superior Alarm objects.  The final iteration clears the Alarm object's
//      update flags.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::UpdateAlarms_(void)
{
  CALL_TRACE("UpdateAlarms_(void)");

  int i;

  // Reset the Alarm::CancelScreenLockStatus_ attribute.
  Alarm::ClearCancelScreenLockStatus();

  const AlarmList& alarms = Alarm::GetAlarms();

  // Initialize the RCheckAlarmList_.
  AlarmManager::RCheckAlarmList_.clearList();

  // Update attribute isActive for all alarms with causeUpdate set to TRUE and
  // if an Alarm object needs to be evaluated (Alarm::causeUpdate_ and/or
  // Alarm::augmentUpdate_ equals TRUE) place it on RCheckAlarmList_.
  for (i = 0; i < MAX_NUM_ALARMS; i++)
  {
    if (alarms[i]->evaluateUpdates()) // $[TI1]
    {
      AlarmManager::RCheckAlarmList_.addItem(*alarms[i]);
    } // else $[TI2]
  }

  // Evaluate all of the ConditionAugmentation objects for each Alarm object
  // on RCheckAlarmList_.
  SigmaStatus status = AlarmManager::RCheckAlarmList_.goFirst();
  while (status == SUCCESS) // $[TI3]
  {
    ((Alarm&)AlarmManager::RCheckAlarmList_.currentItem()).
        evaluateConditionAugs();
    status = AlarmManager::RCheckAlarmList_.goNext();
  } //  No while statement executed. $[TI4]

  // Examine each Alarm on the RCheckAlarmList_ to determine if any superior
  // Alarm objects (in the superior Alarm / DependentAugmentation / dependent
  // Alarm relationship for which the Alarm being processed is the dependent
  // Alarm) are active, not dependent and older than this Alarm being
  // processed.  If so, set isDependent_ for this Alarm.
  status = AlarmManager::RCheckAlarmList_.goFirst();
  while (status == SUCCESS) // $[TI5]
  {
    ((Alarm&)AlarmManager::RCheckAlarmList_.currentItem()).
        evaluateDependencies();
    status = AlarmManager::RCheckAlarmList_.goNext();
  } // No while statement executed. $[TI6]

  // Evaluate all of the DependentAugmentation objects for each Alarm object
  // on RCheckAlarmList_.
  status = AlarmManager::RCheckAlarmList_.goFirst();
  while (status == SUCCESS) // $[TI7]
  {
    ((Alarm&)AlarmManager::RCheckAlarmList_.currentItem()).
        evaluateDependentAugs();
    status = AlarmManager::RCheckAlarmList_.goNext();
  } // No while statement executed. $[TI8]

  // Clear the update flags for each Alarm object on RCheckAlarmList_.
  status = AlarmManager::RCheckAlarmList_.goFirst();
  while (status == SUCCESS) // $[TI9]
  {
    ((Alarm&)AlarmManager::RCheckAlarmList_.currentItem()).clearUpdates();
    status = AlarmManager::RCheckAlarmList_.goNext();
  } // No while statement executed. $[TI10]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReevaluateAlarmOperands_
//
//@ Interface-Description
//      Once all events on the OperatingParameterEvent Queue which translate
//      into OperandName enumerators have been processed and the
//      OperandName enumerators have been placed on ROperandNameList_,
//      evaluate the Operating Parameter Hierarchy and then evaluate marked
//      Alarm objects.  If any Alarm objects causing Loss of Patient Data
//      Monitoring have autoreset, reset all Patient Data Alarms.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::ReevaluateAlarmOperands_(void) 
{
  CALL_TRACE("ReevaluateAlarmOperands_(void)");

  // Mark AlarmOperand objects identified by OperandName enumerators
  // on ROperandNameList_ and their ancestors and evaluate the Operating
  // Parameter Hierarchy, marking those Alarm objects which need to be
  // evaluated.
  AlarmManager::ProcessOperands_( AlarmManager::CONDITION_CHANGE_O_UT);

  // Evaluate marked Alarm objects.
  AlarmManager::ReevaluateAlarms_(Alarm::UPDATE_TYPE_NULL);
  
  // If any Alarm objects causing Loss of Patient Data Monitoring have
  // autoreset, reset all Patient Data Alarms.
  if (Alarm::GetPatientDataReset()) // $[TI1]
  {
     ResetPatientAlarms_();
  } // else $[TI2]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ResetPatientAlarms_
//
//@ Interface-Description
//      Reset all Patient Data Alarms.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Certain alarms depend on patient data for their detection.  When
//      an Alarm causing Loss of Patient Data Monitoring autoresets, the
//      alarms depending on patient data must be reset because the old data 
//      their detection algorithms are based on is invalid and now that the 
//      alarm causing Loss of Patient Data has reset, the new data will be 
//      valid.  This method directs that processing.  For each
//      Alarm, there is an attribute, patientDataAlarm_, which, if TRUE,
//      identifies the Alarm as one of the alarms depending on patient data.
//      Each Alarm is examined and if it is a Patient Data Alarm, it's reset
//      condition enumerators (Alarm::resetConditionName_) are placed on 
//      ROperandNameList_.  Then, the Operating Parameter Hierarchy is marked 
//      according to the OperandNames appearing on ROperandNameList_ and 
//      evaluated in reset mode.  Next, Alarm objects marked during the 
//      evaluation of the Operating Parameter Hierarchy are evaluated and 
//      annunciations sent to the Alarm Annunciation cluster.  Finally, the 
//      Alarm::PatientDataReset_ class attribute is reset.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::ResetPatientAlarms_(void)
{
  CALL_TRACE("ResetPatientAlarms_(void)");
  Alarm::ListPDResetAlarmConditions(AlarmManager::ROperandNameList_);
  AlarmManager::ProcessOperands_( AlarmManager::RESET_O_UT);
  
  ReevaluateAlarms_(Alarm::PAT_DATA_RESET_UT);
  Alarm::ClearPatientDataReset();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReevaluateAlarms_
//
//@ Interface-Description
//      Once the Operating Parameter Hierarchy has been evaluated, the marked
//      Alarm objects are evaluated and an AlarmUpdateBuffer object
//      containing the base message, analysis message and remedy message
//      enumerators, as well as urgency/priority information for all active
//      Alarm objects is sent to the Alarm Annunciation cluster.
//
// >Von
//      updateType  Used to set the Alarm class variable, UpdateType_.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      ((UPDATE_TYPE_NULL <= updateType) &&
//       (updateType < MAX_NUM_UPDATE_TYPE))
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::ReevaluateAlarms_(Alarm::UpdateType updateType) 
{
  CALL_TRACE("ReevaluateAlarms_(updateType)");

  CLASS_PRE_CONDITION((Alarm::UPDATE_TYPE_NULL <= updateType) &&
                      (updateType < Alarm::MAX_NUM_UPDATE_TYPE));

  // Message the Alarm class method SetUpdateType to set the class variable
  // Alarm::UpdateType_ to the passed parameter, updateType.
  Alarm::SetUpdateType(updateType);

  // Process all Alarm objects and build the analysis/remedy messages and
  // determine urgency/priority.
  AlarmManager::UpdateAlarms_();

  // Place AlarmUpdateRecords on RAlarmUpdateList to be sent to the
  // Alarm Annunciation cluster for each active alarm which is not to be
  // annunciated as a dependent alarm.
  AlarmManager::CreateAlarmUpdateList_();

  // Build buffer of alarm update information to be sent to the 
  // AlarmAnnunciator class running in the GUI Task.
  AlarmAnnunciator::BuildBuffer(AlarmManager::RAlarmUpdateList_, 
      Alarm::GetUpdateType(), Alarm::GetCancelScreenLockStatus(),
      Alarm::GetNoPatientDataDisplayStatus(),
      Alarm::GetNoGuiSettingChangesStatus(), Alarm::GetVentInopLedOnStatus());

  // Clear the Alarm class update type variable.
  Alarm::ClearUpdateType();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessOperands_
//
//@ Interface-Description
//      When Alarm events are pulled off of the OperatingParameterEvent
//      Queue they are translated into OperandName enumerators which are
//      placed on the ROperandNameList_.  The OperandName enumerators
//      are used to identify which OperatingCondition and OperatingGroup
//      objects need to be evaluated by the Operating Parameter Analysis
//      cluster.  This method directs that processing.  
// >Von
//      operandUpdateType  Identifies the mode for evaluating the Operating
//                         Parameter Hierarchy.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//      There are two modes for evaluating the Operating Parameter Hierarchy: 
//      reset mode and operand change mode.  The mode is identified by the 
//      passed parameter, operandUpdateType.  In reset mode, all AlarmOperands
//      identified on the ROperandNameList_ and their ancestors are reset
//      which causes their associated Alarm objects and augmentations to be 
//      reset.   In operand change mode, one or more AlarmOperand objects
//      has/have changed state or a timer has fired indicating time to check
//      an AlarmOperand; the AlarmOperand and all its ancestors are evaluated
//      and any associated Alarm objects are evaluated.
//---------------------------------------------------------------------
//@ PreCondition
//      (( AlarmManager::OPERAND_UPDATE_TYPE_NULL < operandUpdateType) &&
//       (operandUpdateType <  AlarmManager::MAX_NUM_OPERAND_UPDATE_TYPE))
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::ProcessOperands_(
                AlarmManager::OperandUpdateType operandUpdateType)
{
  CALL_TRACE("ProcessOperands_(operandUpdateType)");

  CLASS_PRE_CONDITION(
              (AlarmManager::OPERAND_UPDATE_TYPE_NULL < operandUpdateType) &&
              (operandUpdateType <  AlarmManager::MAX_NUM_OPERAND_UPDATE_TYPE));

  // Initialize the list of top level groups to empty.
  OperatingGroup::ClearTopLevelGroups();

  SigmaStatus status = AlarmManager::ROperandNameList_.goFirst();

  // Process each OperandName enumerator on ROperandNameList_.
  while (status == SUCCESS) // $[TI1]
  {
    // Get the AlarmOperand named by the OperandName enumerator.
    AlarmOperand* operand = AlarmOperand::GetOperand(
        (OperandName)AlarmManager::ROperandNameList_.currentItem());

    if (operandUpdateType ==  AlarmManager::CONDITION_CHANGE_O_UT) // $[TI1.1]
    {
      // Message the AlarmOperand and all of its ancestors to mark themselves
      // so they will be evaluated in update mode. 
      operand->setUpdate();
    }
    else if (operandUpdateType ==  AlarmManager::RESET_O_UT) // $[TI1.2]
    {
      // Message the AlarmOperand and all of its ancestors to mark themselves
      // so they will be evaluated in reset mode. 
      operand->setReset();
    } 

    status = AlarmManager::ROperandNameList_.goNext();
  } // No while statement executed. $[TI2]

  // Clear ROperandNameList.
  AlarmManager::ROperandNameList_.clearList();

  // Evaluate all of the AlarmOperand objects which were marked to be 
  // evaluated.
  AlarmManager::ReevaluateMarkedOperands_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReevaluateMarkedOperands_ 
//
//@ Interface-Description
//      The initial processing done by the Operating Parameter Analysis
//      cluster involves marking AlarmOperand objects identified in the
//      AlarmManager::ROperandNameList_ as needing to be updated or reset.
//      When the AlarmOperand objects and all of their ancestors are marked,
//      this method is called to evaluate them.
//---------------------------------------------------------------------
//@ Implementation-Description
//      As the Operating Parameter Hierarchy is being marked (from the bottom
//      up starting with an OperatingCondition or OperatingGroup and then 
//      moving up the hierarchy to their ancestor OperatingGroup objects) when 
//      an OperatingGroup with no parents is reached, a reference to it is put 
//      on OperatingGroup::RActiveTopLevelGroups_.  When all AlarmOperand
//      objects have been marked, they are evaluated from the top down, by
//      evaluating each of the OperatingGroup objects on
//      OperatingGroup::RActiveTopLevelGroups_ and their descendents.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::ReevaluateMarkedOperands_(void)
{
  CALL_TRACE("ReevaluateMarkedOperands_(void)");

  OperatingGroupList& groups = OperatingGroup::GetTopLevelGroups();
  SigmaStatus status = groups.goFirst();

  // Evaluate each of the OperatingGroup objects on the list of active top
  // level groups.  This includes evaluating their descendents.  
  while (status == SUCCESS) // $[TI1]
  {
    groups.currentItem().evaluate();

    status = groups.goNext();
  } // No while statement executed. $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CreateAlarmUpdateList_
//
//@ Interface-Description
//      After the Alarm Analysis cluster has evaluated the Alarm objects
//      marked during the evaluation of the Operating Parameter Hierarchy,
//      it creates a list of AlarmUpdate objects to send to the Alarm
//      Annunciation cluster in the AlarmUpdateBuffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
AlarmManager::CreateAlarmUpdateList_(void) 
{
  CALL_TRACE("CreateAlarmUpdateList_(void)");

  // Initialize the list.
  AlarmManager::RAlarmUpdateList_.clearList();

  SigmaStatus status = AlarmManager::RCheckAlarmList_.goFirst();

  // Process each Alarm on RCheckAlarmList_.
  while (status == SUCCESS) // $[TI1]
  {
    Alarm& rAlarm = (Alarm&)AlarmManager::RCheckAlarmList_.currentItem();

    // Create an AlarmUpdate record and append it to the update list
    AlarmManager::RAlarmUpdateList_.append(AlarmUpdate(rAlarm));

    // Clear the instance attribute updateType_ of the Alarm
    rAlarm.clearUpdateType();

    status = AlarmManager::RCheckAlarmList_.goNext();
  } // No while statement executed. $[TI2]
}




#endif // SIGMA_GUI_CPU


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
AlarmManager::SoftFault(const SoftFaultID  softFaultID,
					    const Uint32       lineNumber,
					    const char*        pFileName,
					    const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::ALARMMANAGER, lineNumber,
                          pFileName, pPredicate);
}


