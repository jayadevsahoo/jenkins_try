
#ifndef DciParityModeValue_HH
#define DciParityModeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  DciParityModeValue - Values of DCI's Parity Mode Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DciParityModeValue.hhv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct DciParityModeValue
{
  //@ Type:  DciParityModeValueId
  // All of the possible values of the DCI Parity Mode Setting.
  enum DciParityModeValueId
  {
    // $[01191] -- values of the range of DCI Parity Mode Setting...
    EVEN_PARITY_VALUE,
    ODD_PARITY_VALUE,
    NO_PARITY_VALUE,

    TOTAL_PARITY_MODES
  };
};


#endif // DciParityModeValue_HH 
