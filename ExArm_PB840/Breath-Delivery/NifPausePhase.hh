#ifndef NifPausePhase_HH
#define NifPausePhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: NifPausePhase -  NIF Pause Phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/NifPausePhase.hhv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathPhase.hh"

//@ End-Usage


class NifPausePhase : public BreathPhase {
  public:
    NifPausePhase( void) ;
	virtual ~NifPausePhase( void) ;

    virtual void newBreath( void) ;
    virtual void relinquishControl( const BreathTrigger& trigger) ;
    virtual void newCycle( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

  protected:

	virtual void updateFlowControllerFlags_( void) ;

  private:
    NifPausePhase( const NifPausePhase&) ;	// not implemented...
    void   operator=( const NifPausePhase&) ;		// not implemented...

	//@ Data-Member: highCircuitPressure_
	// high circuit pressure setting value
	Real32 highCircuitPressure_ ;

};


#endif // NifPausePhase_HH 
