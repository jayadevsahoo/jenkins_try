
#ifndef DisconnectPhase_HH
#define DisconnectPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: DisconnectPhase - active phase during circuit disconnect phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DisconnectPhase.hhv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004 By: syw     Date: 25-Aug-1997   DR Number: DCS 2419
//  	Project:  Sigma (R8027)
//		Description:
//			Added relinquishControl().
//
//  Revision: 003 By: iv     Date: 18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//			Added a varialble IDLE_FLOW = 10.0 lpm.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

#include "BreathPhase.hh"

//@ Usage-Classes
//@ End-Usage

//@ Constant: IDLE_FLOW
// constant base flow during disconnect in lpm
static const Real32 IDLE_FLOW = 10.0F ;	// $[04214]

class DisconnectPhase : public BreathPhase
{
  public:
    DisconnectPhase( void) ;
    virtual ~DisconnectPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

    virtual void newCycle(void) ;
    virtual void newBreath(void) ;
    virtual void relinquishControl( const BreathTrigger& trigger) ;
  
  protected:

  private:
    DisconnectPhase( const DisconnectPhase&) ;		// not implemented...
    void   operator=( const DisconnectPhase&) ;		// not implemented...

} ;


#endif // DisconnectPhase_HH 
