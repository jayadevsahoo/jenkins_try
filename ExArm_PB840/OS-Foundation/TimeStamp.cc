#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimeStamp - A time marker object which represents wall
//  clock time and related functions.
//---------------------------------------------------------------------
//@ Interface-Description
//  The TimeStamp object can be utilized by providing a marker of time 
//  for tracking event occurrences.
//---------------------------------------------------------------------
//@ Rationale
//  The TimeStamp object provides an easy to use interface of keeping  wall 
//  clock time stamped events. This object hides the implementation specific
//  details of the Time Of Day clock.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Stores the Time of Day in a TimeStamp object. The ToD is retrieved
//  from the board ToD clock. This time is representative of wall clock
//  time. The user can also specify the time parameters.
//  The TimeStamp object utilizes Julian Days and milliseconds for
//  arithmetic and comparison operators. Julian Days represent the date
//  and milliseconds represent time. These values are computed when needed
//  they are not stored in the object.
//
//  Note: A four digit year was used to solve the problem in using 
//  comparison operators after the turn of the century. A two digit year
//  did not provide enough information.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre and
//  post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//  This class should not be used for interval timing since the time 
//  can be changed resulting in incorrect interval computation.
//  The turn of the century ( 2000 ) is handled by comparing the current
//  year with a base year of 1995. Resulting in assigning the correct year
//  to the object. Because of this assumption of the 
//  base year being 1995, the comparison will be invalid in 2095.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/TimeStamp.ccv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 004   By: gdc   Date: 04-Jan-2000  DR Number: 5588
//  Project:  NeoMode
//  Description:
//      Modified so POST can use this class in its entirety. Defined
//      zerovars in the POST zerovars section. Defined GetRNullTimeStamp()
//      so local static memory (zerovars) can be used instead of 
//      global reference.
//
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number:1943/1944
//    Project: Sigma (R8027)
//    Description:
//       Added error reporting interface to Safety-Net when clock fails
//       or when it returns a time/date out of range.  Constructed 
//       TimeStamps are now bounded to a range appropriate to the 840
//       application.  Bounding the date/time allowed removal of overflow 
//       logic in subtraction operator.  Modified julian day algorithm 
//       for better performance and removed superfluous operators.
//       
//       Added revision headers.
//
//       Per rework instructions, added logic in subtraction operator
//       to return MAX_INT or MIN_INT when difference exceeds 23 days
//       instead of allowing arithmetic overflow or underflow.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

// relocate the zerovars into postzerovars so POST can use class
#pragma option -NZpostzerovars

#include "Sigma.hh"
#include "TimeStamp.hh"
#include "Background.hh"
#include "Post.hh"
#include <limits.h>
#include "Sys_Init.hh"

#ifdef SIGMA_DEVELOPMENT
#  include <stdio.h>
//#  include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

//@ Usage-Classes
//@ End-Usage

//@ Code...

static Uint EDAYS_[12];
static Uint ELEAPDAYS_[12];
static Int32 MAX_DAYS_;
static Boolean FailureReported_;
static Boolean TodFailed_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimeStamp  [Default Constructor]
//
//@ Interface-Description
//  Creates a TimeStamp object from the Time of Day clock.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the internal timestamp_ to the current wall clock time.
//  Calls the now() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimeStamp::TimeStamp(void)
{
  CALL_TRACE("TimeStamp::TimeStamp(void)");

  now();
		// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimeStamp  [Copy Constructor]
//
//@ Interface-Description
//  Copies a TimeStamp object to another created TimeStamp object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Copies the referenced TimeStamp data to this object's data using
//  the assignment operator.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimeStamp::TimeStamp(const TimeStamp& rTime)
{
  CALL_TRACE("TimeStamp::TimeStamp(rTime)");

  *this = rTime;
			// $[TI7]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimeStamp  [Constructor]
//
//@ Interface-Description
//  Create a TimeStamp object from a Time of day structure.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Copy the referenced ToD structure in to the TimeStamp object.
//  Check the year and add the appropriate year bias. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimeStamp::TimeStamp(const struct TimeOfDay& rTOD)
{
  CALL_TRACE("TimeStamp(const struct TimeOfDay&)");

    year_	  = rTOD.year;
	month_    = rTOD.month;
	dayOfMon_ = rTOD.date;
	hour_     = rTOD.hour;
	min_      = rTOD.minute;
	sec_      = rTOD.second;
    msec_     = rTOD.milliseconds;

    if ( !isBounded() )
    {   // $[TI4]
        invalidate();
    }
    // $[TI5]
    
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimeStamp  [Constructor]
//
//@ Interface-Description
//  Create a TimeStamp object from a user input.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the corresponding private data fields with the passed in values.
//---------------------------------------------------------------------
//@ PreCondition
//  (year >= 1994 && year <= 2093)
//  (month >= 1 && month <= 12)
//  (dayOfMonth >= 1 && dayOfMonth <= 31)
//  (hour >= 0 && hour <= 23)
//  (minute >= 0 && minute <= 59)
//  (second >= 0 && second <= 59)
//  (hsec >= 0 && hsec <= 99)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
TimeStamp::TimeStamp(  const Int32 year
                     , const Int32 month
                     , const Int32 dayOfMonth
                     , const Int32 hour
                     , const Int32 minute
                     , const Int32 second
                     , const Int32 msec )
{
    AUX_CLASS_PRE_CONDITION(year >= START_EPOCH_YEAR && year < MAX_EPOCH_YEAR, year);
    AUX_CLASS_PRE_CONDITION(month >= 1 && month <= 12, month);
    AUX_CLASS_PRE_CONDITION(dayOfMonth >= 1 && dayOfMonth <= 31, dayOfMonth);
    AUX_CLASS_PRE_CONDITION(hour >= 0 && hour <= 23, hour);
    AUX_CLASS_PRE_CONDITION(minute >= 0 && minute <= 59, minute);
    AUX_CLASS_PRE_CONDITION(second >= 0 && second <= 59, second);
    AUX_CLASS_PRE_CONDITION(msec >= 0 && msec <= 999, msec);

    year_     = year;
    month_    = month;
    dayOfMon_ = dayOfMonth;
    hour_     = hour;
    min_      = minute;
    sec_      = second;
    msec_     = msec;
                                       // $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimeStamp  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimeStamp::~TimeStamp(void)
{
	CALL_TRACE("TimeStamp::~TimeStamp(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [public, static]
//
//@ Interface-Description
//  One-time initialization routine to run placement new on
//  static data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Create a static NullTimeStamp object. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimeStamp::Initialize(void)
{
	CALL_TRACE("TimeStamp::Initialize(void)");

    Uint edays[]      
        = { 0, 31, 59, 90,120,151,181,212,243,273,304,334 };
    Uint eleapdays[]  
        = { 0, 31, 60, 91,121,152,182,213,244,274,305,335 };

	// initialize local static
	for (int i=0; i<countof(edays); i++)
	{
	  EDAYS_[i] = edays[i];
	  ELEAPDAYS_[i] = eleapdays[i];
	}

	MAX_DAYS_ = INT_MAX / MSEC_PER_DAY - 1;

	FailureReported_ = FALSE;
	TodFailed_ = FALSE;

	GetRNullTimeStamp();

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Now [public, static]
//
//@ Interface-Description
//  Returns TimeStamp object.  Used to construct and pass TimeStamp
//  objects on the stack.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Constructs a TimeStamp and returns a copy of it.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
TimeStamp
TimeStamp::Now(void)
{
    CALL_TRACE("TimeStamp::Now(void)");

    TimeStamp  now;
                        // $[TI1]
    return  now;   
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetRNullTImeStamp
//
//@ Interface-Description
//  Returns a const TimeStamp reference for the NULL TimeStamp object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls private GetRNullTimeStamp_() that returns a non-const 
//  null TimeStamp and returns a const reference to the caller.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
const TimeStamp& 
TimeStamp::GetRNullTimeStamp(void)
{
  return GetRNullTimeStamp_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: now
//
//@ Interface-Description
//  Gets and stores the time of day from the ToD clock.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the the ToD clock is enabled copy the time and date to corresponding
//  private data members.
//  If the ToD is disabled invalidate the TimeStamp object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void 
TimeStamp::now(void)
{
    struct	TimeOfDay	boardTime;
	::ReadTimeOfDay(&boardTime);

    year_	  = boardTime.year;
    month_    = boardTime.month;
    dayOfMon_ = boardTime.date;
    hour_     = boardTime.hour;
    min_      = boardTime.minute;
    sec_      = boardTime.second;
    msec_     = boardTime.milliseconds;
    
    if ( !isBounded() )
    {   // $[TI3.1]
        invalidate();
		TodFailed_ = TRUE;
    } 	// $[TI3.2]

		// In the following conditional, Post::IsPostActive must be checked
	// before Sys_Init::IsSystemInitialized since the Sys-Init vars
	// are contained in uninitialized memory during POST. Checking the
	// the uninitialized Sys-Init variable will most likely cause a DRAM 
	// parity error and reset the processor.

	if ( TodFailed_ && !FailureReported_ && !Post::IsPostActive() 
		&& Sys_Init::IsSystemInitialized() )
	{   // $[TI4.1]
	  FailureReported_ = TRUE;
#ifdef SIGMA_BD_CPU
	  Background::ReportBkEvent( BK_BD_TOD_FAIL );
#endif
#ifdef SIGMA_GUI_CPU
	  Background::ReportBkEvent( BK_GUI_TOD_FAIL );
#endif
	}   // $[TI4.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetBeginEpoch
//
//@ Interface-Description
//  Sets the TimeStamp object to the earliest valid time which is 
//  time 00:00:00.0 on January 1, 1994.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the private data members according to the interface spec.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

const TimeStamp & 
TimeStamp::GetBeginEpoch(void)
{
    static TimeStamp beginEpoch(START_EPOCH_YEAR,1,1,0,0,0,0);

    // $[TI1]
    return beginEpoch;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//  Overloaded operator "=".
//  Assigns passed in TimeStamp reference to a TimeStamp object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assign the referenced TimeStamp to this TimeStamp.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
TimeStamp::operator=(const TimeStamp& rTime)
{
  CALL_TRACE("TimeStamp::operator=(rTime)");

  if ( this != &rTime )
  {                          //$[TI1]
    year_     = rTime.year_;
    month_    = rTime.month_;
    dayOfMon_ = rTime.dayOfMon_;
    hour_     = rTime.hour_;
    min_      = rTime.min_;
    sec_      = rTime.sec_;
    msec_     = rTime.msec_;
  }                          //$[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTimeOfDay
//
//@ Interface-Description
//  Conversion operator returning TimeOfDay struct.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method fills a local TimeOfDay struct from the private 
//  data member timestamp_ and returns a copy of the local struct.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
TimeOfDay
TimeStamp::getTimeOfDay() const
{
    CALL_TRACE("getTimeOfDay()");
    TimeOfDay   tod;

    if ( !isInvalid() )
    {
      tod.year = year_;
      tod.month            = month_;
      tod.date             = dayOfMon_;
      tod.hour             = hour_;
      tod.minute           = min_;
      tod.second           = sec_;
      tod.milliseconds	   = msec_;
    }
    else
    {                               // $[TI2]
      tod.year             = 0;
      tod.month            = 0;
      tod.date             = 0;
      tod.hour             = 0;
      tod.minute           = 0;
      tod.second           = 0;
      tod.milliseconds = 0;
    }

    return tod;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator-
//
//@ Interface-Description
//  Overloaded subtraction operator for a TimeStamp object.
//  Returns an Int32 difference in milliseconds.  When the difference
//  exceeds 23 days, this operator returns INT_MAX or INT_MIN.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The difference in milliseconds between the julian days is added to
//  the difference in time of day.  
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
Int32
TimeStamp::operator-(const TimeStamp& rTime) const
{
   Uint32   julian1    = julianDay_();
   Uint32   julian2    = rTime.julianDay_();
   Int32    julianDiff = julian1 - julian2;

   //  Since we can only fit a difference of 24.86 days (in milliseconds)
   //  into a 32-bit integer, we'll return the maximum or minimum integer
   //  values when the difference in the julian days exceeds 23 days.  
   //  This is done instead of returning an unpredictable result of 
   //  an overflow or undeflow operation.

   if ( julianDiff > MAX_DAYS_ )
   {
       // $[TI1]
       return INT_MAX;
   }
   // $[TI2]

   if ( julianDiff < -MAX_DAYS_ )
   {
       // $[TI3]
       return INT_MIN;
   }
   // $[TI4]

   return ( julianDiff * MSEC_PER_DAY + (getMilSec_() - rTime.getMilSec_()) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: julianDay_
//
//@ Interface-Description
//  Returns a Uint32 julian day.
//  Converts Gregorian calendar date to a julian day number.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  The "julian day" is computed as the number of days since START_YEAR.
//  The "julian day" is computed using the algorithm:
//
//     julian_day = "day of current month"
//                  + "days elapsed in months previous to current month"
//                  + 365 * "number of years since 1993"
//                  + "one day for each leap year since 1993"
//
//  If the TimeStamp is invalid, the julian day is returned as 0 which
//  provides that all valid dates are greater than the "invalid" date.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Uint32
TimeStamp::julianDay_() const
{
    Uint32  julian = 0;

    if ( !isInvalid() )
    {
        // $[TI1]
        Uint * pDays = ( year_ % 4 ) ? EDAYS_ : ELEAPDAYS_; // $[TI3] $[TI4]
 
        julian =   dayOfMon_
                 + pDays[month_-1]
                 + (year_ * 365)
                 + (year_ / 4);
    }

    // $[TI2]
    return julian;
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMilSec_
//
//@ Interface-Description
//  Converts hour, minutes, seconds and hundredths of seconds to milliseconds.
//  Takes no inputs and returns Uint32 msec.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Takes the private data used to store the time and converts it to
//  milliseconds. Each unit of time is multiplied by a millisecond conversion
//  factor ( enumerated type TimeStamp::TimeConvType ). Then each step 
//  is added to the last to get the final millisecond count. The final
//  count is returned as the total millisecond count for this TimeStamp's
//  time.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Uint32
TimeStamp::getMilSec_() const
{
  // $[TI1]
  return (  hour_ * MSEC_PER_HOUR
          + min_ * MSEC_PER_MIN
          + sec_ * MSEC_PER_SEC
          + msec_ );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator<
//
//@ Interface-Description
//  Overloaded operator "<".
//  Compares two TimeStamp objects and returns a boolean value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Is this timestamp less than the referenced timestamp.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
TimeStamp::operator<(const TimeStamp& rTime) const
{
                                    // $[TI1] (T)
                                    // $[TI2] (F)
  return !operator>=(rTime);
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator<=
//
//@ Interface-Description
//  Overloaded operator "<=".
//  Compares two TimeStamp objects and returns a boolean value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Is this timestamp less than or equal to the referenced timestamp.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
TimeStamp::operator<=(const TimeStamp& rTime) const
{
  Uint32 julNum = julianDay_();
  Uint32 msec = getMilSec_();
  Uint32 rJulNum = rTime.julianDay_();
  Uint32 rMsec = rTime.getMilSec_();
 
                                     // $[TI1] (T)
                                     // $[TI2] (F)
  return (   ( julNum < rJulNum ) 
          || ( julNum == rJulNum && msec <= rMsec ) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator>
//
//@ Interface-Description
//  Overloaded operator ">".
//  Compares two TimeStamp objects and returns a boolean value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Is this timestamp greater than the referenced timestamp.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
TimeStamp::operator>(const TimeStamp& rTime) const
{
                                    // $[TI1] (T)
                                    // $[TI2] (F)
  return !operator<=(rTime);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator>=
//
//@ Interface-Description
//  Overloaded operator ">=".
//  Compares two TimeStamp objects and returns a boolean value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Is this timestamp greater than or equal to the referenced timestamp.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
TimeStamp::operator>=(const TimeStamp& rTime) const
{
  Uint32 julNum = julianDay_();
  Uint32 msec = getMilSec_();
  Uint32 rJulNum = rTime.julianDay_();
  Uint32 rMsec = rTime.getMilSec_();
 
                                     // $[TI1] (T)
                                     // $[TI2] (F)
  return (   ( julNum > rJulNum ) 
          || ( julNum == rJulNum && msec >= rMsec ) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
//  Overloaded operator "==".
//  Compares two TimeStamp objects and returns a boolean value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Is this timestamp equal to the referenced timestamp.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
TimeStamp::operator==(const TimeStamp& rTime) const
{
               // $[TI1] (T)
               // $[TI2] (F)
  return(   year_     == rTime.year_
         && month_    == rTime.month_
         && dayOfMon_ == rTime.dayOfMon_
         && hour_     == rTime.hour_
         && min_      == rTime.min_
         && sec_      == rTime.sec_
         && msec_     == rTime.msec_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
//  Overloaded operator "!=".
//  Compares two TimeStamp objects and returns a boolean value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Is this timestamp not equal to the referenced timestamp.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
TimeStamp::operator!=(const TimeStamp& rTime) const
{
               // $[TI1] (T)
               // $[TI2] (F)
  return !operator==(rTime);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
TimeStamp::SoftFault(const SoftFaultID  softFaultID,
                                     const Uint32       lineNumber,
                                     const char*        pFileName,
                                     const char*        pPredicate)
{
        CALL_TRACE("TimeStamp::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
        FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::TIMESTAMP, lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetRNullTImeStamp_ [private]
//
//@ Interface-Description
//  Returns a TimeStamp reference for the NULL TimeStamp object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An invalidated TimeStamp object is defined as a local static object
//  and a reference to it is returned to the caller.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TimeStamp&
TimeStamp::GetRNullTimeStamp_(void)
{
  // return an invalidated TimeStamp
  static TimeStamp NullTimeStamp_( GetBeginEpoch() );
  NullTimeStamp_.invalidate();
  return NullTimeStamp_;
}

