#ifndef AlarmManager_HH
#define AlarmManager_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AlarmManager - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmManager.hhv   25.0.4.0   19 Nov 2013 13:51:16   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 05/31/96         DR Number:  1032
//      Project:   Sigma   (R8027)
//      Description:
//         Added callback routine for averaged patient data.
//  
//   Revision 03    By:   hct    Date: 08/13/96         DR Number:  1206
//      Project:   Sigma   (R8027)
//      Description:
//         Removed functionality that allow processing of multiple alarm
//         events in one cycle of Alarm-Analysis subsystem processing.
//         If two events which canceled each other out were received and
//         processed in the same round of processing, it is possible an alarm
//         condition would have gone unreported.
//  
//   Revision 04    By:   hct    Date: 11/11/96         DR Number:  1530
//      Project:   Sigma   (R8027)
//      Description:
//         The end-of-breath data packet contains the breath type for the new
//         breath instead of the previous breath.  Added callback 
//         ReceiveRealtimeData to store the breath type in the static
//         data of method GetBreathType.
//  
//   Revision 05    By:   hct     Date: 11/06/96        DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.
//  
//   Revision 06    By:   hct    Date: 12/18/96         DR Number:  1611
//      Project:   Sigma   (R8027)
//      Description:
//         Use new previous breath type from End-of-Breath data packet.  No
//         need to register for Real Time Data.
//
//   Revision 007   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 008   By:   sah      Date: 03/15/99       DR Number: 5314
//      Project:   ATC
//      Description:
//         Settings' callback/monitoring mechanism changed to an observer/
//         subject framework.  Changed this class to use the new mechanism.
//  
//====================================================================

//@ Usage-Classes
#include "Sigma.hh"
#include "Alarm.hh"
#include "Alarm_Analysis.hh"
#include "AlarmUpdateList.hh"
#include "BreathType.hh"
#include "CheckAlarmList.hh"
#include "OperandNameList.hh"
#include "OperatingGroup.hh"
#include "OsFoundation.hh"
#include "OperatingCondition.hh"
#include "SettingObserver.hh"

//@ End-Usage

class AlarmManager : public SettingObserver
{
  public:
    static void Initialize(void);
    static void ProcessTaskQueue(void);
    static void SendEvent(Int32 eventName);
    static void ReceiveEvent(XmitDataMsgId msgId, void* pDataBlock, Uint size);

#ifdef SIGMA_GUI_CPU
    AlarmManager(void);
    ~AlarmManager(void);

    static void InitMemory(void);
    static void InitHierarchy(void);
    
    static void ManualResetAlarms(void);
    static void SendEndOfBreathEvent(void);
    static void SendAveragedDataEvent(void);
    static ::BreathType& GetBreathType(void);
    inline static void   AppendOperandName(OperandName name);

#  ifdef SIGMA_UNIT_TEST
    inline static OperandNameList& GetOperandNameList(void);
    inline static CheckAlarmList&  GetCheckAlarmList(void);
    inline static void             ClearList(void);

#  endif // SIGMA_UNIT_TEST

	// SettingObserver virtual methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;
#endif // SIGMA_GUI_CPU
	
    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


  protected:

  private:

    AlarmManager(const AlarmManager&);     // Declared but not implemented
    void operator=(const AlarmManager&);   // Declared but not implemented

    enum OperandUpdateType {OPERAND_UPDATE_TYPE_NULL = -1, RESET_O_UT,
        CONDITION_CHANGE_O_UT, MAX_NUM_OPERAND_UPDATE_TYPE};

    static void ProcessEvents_      (Int32 tempInt32);

#ifdef SIGMA_GUI_CPU
    static void UpdateAlarms_            (void);
    static void ReevaluateAlarmOperands_ (void);
    static void ResetPatientAlarms_      (void);
    static void ReevaluateAlarms_        (Alarm::UpdateType updateType);
    static void ProcessOperands_         (AlarmManager::OperandUpdateType conditionUpdateType);
    static void ReevaluateMarkedOperands_(void);
    static void CreateAlarmUpdateList_   (void);
    static void ProcessViolationHistoryObj_ (Int32 tempInt32);


    //@ Data-Member:     ROperandNameList_
    // As events are taken from the OperatingParameterEvent Queue they are
    // translated into AlarmOperandName enumerators (indicating which
    // AlarmOperand objects need to be evaluated in response to the
    // alarm events) which are placed on the ROperandNameList_.
    static OperandNameList& ROperandNameList_;

    //@ Data-Member:     RAlarmUpdateList_
    // While the Alarm Analysis cluster is evaluating Alarm objects it
    // makes AlarmUpdate records to indicate which Alarm objects have
    // undergone change.  This list is used to create the AlarmUpdateBuffer
    // which is sent to the Alarm Annunciation cluster.
    static AlarmUpdateList& RAlarmUpdateList_;

    //@ Data-Member:     RCheckAlarmList_
    // After the Operating Parameter Analysis cluster has finished evaluating
    // the Operating Parameter Hierarchy and control is transferred to the
    // Alarm Analysis cluster, all Alarm objects are examined to see if they
    // need to be evaluated (as indicated by Alarm::causeUpdate_ and/or
    // Alarm::augmentUpdate_ being set to TRUE during Operating Parameter
    // Hierarchy evaluation.)  Any Alarm objects that need to be evaluated
    // are placed on the RCheckAlarmList_.
    static CheckAlarmList&  RCheckAlarmList_;

    //@ Data-Member:     RInstance_
    // Static member reference to the one, and only, instance of this class.
    // An instance of this class is needed for monitoring changes to setting
    // values, via the 'SettingObserver' base class.
    static AlarmManager&  RInstance_;
#endif // SIGMA_GUI_CPU

};

// Inlined methods
#include "AlarmManager.in"

#endif // AlarmManager_HH 
