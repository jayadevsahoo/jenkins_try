#ifndef  Stdio_HH
#define  Stdio_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
//====================================================================
// Filename:  Sprintf - Sigma Universal Sprintf 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/Stdio.hhv   25.0.4.0   19 Nov 2013 14:35:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: gdc   Date: 15-APR-1997   DCS Number: 5055
//    Project: Color
//    Description:
//       Initial version
//
//====================================================================
 
#include <stdio.h>
#include <stdarg.h>

//@ Free-Function Declarations
int Sprintf(char *s, const char* pFormat, ...);
int Printf(const char* pFormat, ...);
//@ End-Free-Function Declarations

#endif   // Stdio_HH
