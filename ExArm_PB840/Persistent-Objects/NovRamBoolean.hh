
#ifndef NovRamBoolean_HH
#define NovRamBoolean_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  NovRamBoolean - NOVRAM Boolean Value.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamBoolean.hhv   25.0.4.0   19 Nov 2013 14:18:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' and 'verifySelf()' to non-inlined methods.
//	Also, added new private method, 'isValid_()'.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"
#include "NovRamStatus.hh"

//@ Usage-Classes
//@ End-Usage


class NovRamBoolean
{
  public:
    NovRamBoolean (void);
    ~NovRamBoolean(void);

    inline Boolean  isTrue (void) const;
    inline Boolean  isFalse(void) const;

    NovRamStatus::InitializationId  initialize(void);
    NovRamStatus::VerificationId    verifySelf(void) const;

    inline void  updateValue(const Boolean newValue);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  private:
    NovRamBoolean(const NovRamBoolean&);	// not implemented...
    void  operator=(const NovRamBoolean&);	// not implemented...

    inline Boolean  isValid_(void) const;

    //@ Type:  BooleanPattern
    // NOVRAM boolean values.
    enum BooleanPattern
    {
      FALSE_PATTERN = 0x79969969,
      TRUE_PATTERN  = 0x76696696
    };

    //@ Data-Member:  flagValue_
    // The NOVRAM boolean value.
    BooleanPattern  flagValue_;
};


// Inlined methods...
#include "NovRamBoolean.in"


#endif // NovRamBoolean_HH 
