
#ifndef PhasedInContext_HH
#define PhasedInContext_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: PhasedInContext - Context of Phased-In Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PhasedInContext.hhv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "TemplateMacros.hh"

//@ Usage-Classes
#include "BdSettingValues.hh"
#include "PhaseInEvent.hh"
class  FixedBitArray(NUM_BD_SETTING_IDS);  // forward declaration...
//@ End-Usage


class PhasedInContext
{
  public:
    PhasedInContext(void);
    ~PhasedInContext(void);

    void  phaseInSafetyPcvSettings(void);
    void  phaseInPendingSettings  (
    			const PhaseInEvent::PhaseInState    phaseInState
				  );

    Boolean  arePatientSettingsAvailable(void) const;
    Boolean  areBatchSettingsInitialized(void) const;

    inline const BoundedValue&  getBoundedValue (
				    const SettingId::SettingIdType boundedId
				    		)  const;
    inline DiscreteValue        getDiscreteValue(
				    const SettingId::SettingIdType discreteId
				    		)  const;

#if defined(SIGMA_DEVELOPMENT)
    void  setBoundedValue (const SettingId::SettingIdType boundedId,
			   const BoundedValue&            boundedValue);
    void  setDiscreteValue(const SettingId::SettingIdType discreteId,
			   DiscreteValue                  discreteValue);

    inline const BdSettingValues&  getPhasedInValues(void) const;
#endif // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    PhasedInContext(const PhasedInContext&);	// not implemented...
    void  operator=(const PhasedInContext&);	// not implemented...

#if defined(SIGMA_DEVELOPMENT)
    static void  RegisterSettingPhaseIn_(
				  const SettingId::SettingIdType settingId
				        );
#endif  // defined(SIGMA_DEVELOPMENT)

    void  performPhaseIn_(FixedBitArray(NUM_BD_SETTING_IDS)& rPhaseInFlags);

    // Data-Member:  phasedInBdValues_
    // The phased-in breath-delivery setting values.
    BdSettingValues  phasedInBdValues_;
};


// Inlined methods
#include "PhasedInContext.in"


#endif // PhasedInContext_HH 
