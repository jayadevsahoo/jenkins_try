#ifndef ExternalControlSubScreen_HH
#define ExternalControlSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ExternalControlSubScreen - The sub-screen in the Lower screen
// selected by pressing the External Remote Test button on the Other Screens
// subscreen.  It allows the operator to establish external control
// communication and execute remote command.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ExternalControlSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $ 
//@ Modification-Log:
//
//  Revision: 005  By: gdc    Date: 26-May-2007   DCS Number:  6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 004   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 003  By: dosman 	   Date:  26-Feb-1999    DCS Number: 5322
//  Project:  ATC
//  Description:
//	Implemented changes for ATC.
//	Added new method to send available software options string to remote PC.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By: gdc      Date: 23-Sep-1997  DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//     Cleaned up Service-Mode interfaces to support remote test.
//     Removed redundant setup screen for entry into external control.
//
//====================================================================

#include "SubScreen.hh"
#include "AdjustPanelTarget.hh"
#include "LaptopEventTarget.hh"
#include "ServicePromptMgr.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "LaptopEventId.hh"
#include "SubScreenTitleArea.hh"
#include "TextField.hh"
#include "GuiTestManager.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ExternalControlSubScreen : public SubScreen,
								public AdjustPanelTarget,
								public LaptopEventTarget,
								public ServicePromptMgr,
								public GuiTestManagerTarget 
{
public:
	ExternalControlSubScreen(SubScreenArea *pSubScreenArea);
	~ExternalControlSubScreen(void);

	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelRestoreFocusHappened(void);
				
	// LaptopEventTarget virtual methods
	virtual void laptopRequestHappened(LaptopEventId::LTEventId eventId, Uint8 *pMsgData);
	virtual void laptopFailureHappened(SerialInterface::SerialFailureCodes
														serialFailureCodes,
														Uint8 eventId);

	// GuiTestMangerTarget virtual methods
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);
protected:

private:
	// these methods are purposely declared, but not implemented...
	ExternalControlSubScreen(void);						// not implemented...
	ExternalControlSubScreen(const ExternalControlSubScreen&);		// not implemented...
	void   operator=(const ExternalControlSubScreen&);	// not implemented...

	static const wchar_t*  GetVentilatorOptionsString_(void);  

	inline void testStart_(void);
	inline void testEnd_(void);
	void performTest_(Uint8 *);
	void performFunction_(Uint8 *);

	void sendCommand_(Uint8 command) const;
	void sendSwRevisionCommand_(const char *guiRevision, const char *bdRevision) const;  
	void sendSwOptionsCommand_(void) const;
	void sendSerialNumberCommand_(void) const;
	void sendRunTimeCommand_(Real32 compressorValue, Real32 systemValue) const;
	
	enum SystemModeState_
	{
		NULL_SYSTEM_MODE_ID = -1,

		SM_LAPTOP_CONNECTED,
		SM_COMMUNICATION_GRANTED,
		SM_COMMUNICATION_IN_PROGRESS,

		NUMBER_OF_SYSTEM_MODE
	};

	//@ Data-Member: readyToProcess_
	// Boolean to indicate the readiness to proceed test.
	Boolean readyToProcess_;

	//@ Data-Member: statusText_ 
	// The text field, used for displaying status while test in progress
	TextField statusText_;

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member: guiTestMgr_ 
	// Object instance of GuiTestManager
	GuiTestManager guiTestMgr_;

	enum
	{
		//@ Constant: MAX_MSG_BUFFER_BYTE_SIZE_
		// The maximum number of target pointers that can be stored by this
		// cascade
		MAX_MSG_BUFFER_BYTE_SIZE_ = 500
	};

	//@ Data-Member: currentState_
	// The flag indicats the current communication mode.
	SystemModeState_ currentState_;

	//@ Data-Member: currentTestId_
	// Identifier to hold the test Id to be tested.
	SmTestId::ServiceModeTestId currentTestId_;

};

// Inlined methods
#include "ExternalControlSubScreen.in"

#endif // ExternalControlSubScreen_HH

