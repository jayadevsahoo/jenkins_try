
#ifndef VolumePlotScaleValue_HH
#define VolumePlotScaleValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  VolumePlotScaleValue - Y-Axis Scale Values for the Volume Plot.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/VolumePlotScaleValue.hhv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: mnr    Date: 14-Jan-2009    SCR Number: 5987
//  Project:  NEO
//  Description:
//      Added new volume plot scale ranges.
//
//  Revision: 004  By: mnr    Date: 23-Nov-2009    SCR Number: 5987
//  Project:  NEO
//  Description:
//      Added a new volume plot scale range from -5 to 10.
//
//  Revision: 003  By: rhj    Date: 15-Sep-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added a new volume plot scale range from -2000 to 6000
//
//  Revision: 002   By: sah    Date: 19-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added additional scale for neonate's lower volumes
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//      Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct VolumePlotScaleValue
{
  //@ Type:  VolumePlotScaleValueId
  // All of the possible values for VolumePlotScaleSetting.
  enum VolumePlotScaleValueId
  {
    // $[01161] -- values of the range of Volume Plot Axis Setting...
	MINUS_1_TO_2_ML,
	MINUS_1_TO_4_ML,
	MINUS_2_TO_6_ML,
	MINUS_2_TO_10_ML,
	MINUS_5_TO_10_ML,
    MINUS_5_TO_25_ML,	  	
    MINUS_5_TO_50_ML,	  	
    MINUS_10_TO_100_ML,	  	
    MINUS_20_TO_200_ML,
    MINUS_50_TO_500_ML,
    MINUS_100_TO_1000_ML,
    MINUS_200_TO_2000_ML,
    MINUS_300_TO_3000_ML,
    MINUS_2000_TO_6000_ML,  
    TOTAL_VOLUME_SCALE_VALUES
  };
};


#endif // VolumePlotScaleValue_HH 
