#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: BD_IO_Devices - Subsystem definitions, construction, and
//         initialization.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class contains the BD I/O Devices subsystem initialization
//      method which constructs and initializes all the BD I/O Devices
//      objects.  Subsystem-wide constants are also defined.
//
//		This class provides support for the BD_CPU and the GUI_CPU.
//---------------------------------------------------------------------
//@ Rationale
//      Each subsystem requires class construction and initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//      In the initialization method, each object is constructed and
//      placed in memory.  Any object that needs initialization will execute
//      its own initialize() method.
//
// $[00401] $[00478]
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      This is a static class and is not to be constructed.
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BD_IO_Devices.ccv   26.0.1.2   25 Jul 2013 14:53:34   rrush  $
//
//@ Modification-Log
//
//  Revision: 051   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 050  By: erm   Date: 15-MAY-2003    DR Number: 6055
//  Project:  AVER
//	Description:
//		Modified Construction on 5 and 12 Gui Sentry
//
//  Revision: 049  By: syw   Date:  21-Mar-2000    DR Number: 5611
//  Project:  NeoMode
//	Description:
//		Eliminate SafetyValveSwitchedSideSNData.
//
//  Revision: 038  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//			Added REventFilter object.
//
//  Revision: 037 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 036  By:  syw    Date: 01-Oct-1997    DR Number: DCS 2410
//       Project:  Sigma (R8027)
//       Description:
//			Read in atmospheric pressure offset from novram and set the
//			offset to the RAtmosphericPressureSensor.
//
//  Revision: 035  By: syw     Date:  16-Sep-1997    DR Number: 2427
//  	Project:  Sigma (840)
//		Description:
//			Added requirement traceability.
//
//  Revision: 034  By:  syw    Date:  21-Jul-97    DR Number: DCS 2283
//       Project:  Sigma (R8027)
//       Description:
//             	Added calls to TemperatureSensor.setTempOffset() for air, o2
//				and exhGas instances.
//
//	Revision: 033  By:  syw    Date:  03-Jul-1997    DR Number: DCS 2282
//      Project:  Sigma (R8027)
//      Description:
//			Removed all spiro references from ExhFlowSensor.
//
//  Revision: 032  By:  syw    Date:  03-Jul-97    DR Number: DCS 2279
//       Project:  Sigma (R8027)
//       Description:
//             Created air and o2 instances of FlowSensorOffset amd initialize
//				with NovRam contents.
//
//  Revision: 031  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 030  By: syw   Date:  07-May-1997    DR Number: 2028
//  	Project:  Sigma (840)
//		Description:
//			Changed factor scaling for battery voltage to 10v = 39.2 v.
//
//  Revision: 029  By: syw   Date:  11-Apr-1997    DR Number: 589
//  	Project:  Sigma (840)
//		Description:
//			Fix was made in a revision before this.  This is to document
//			this DCS for traceability.
//
//  Revision: 028  By: by   Date:  15-Mar-1997    DR Number: None
//  	Project:  Sigma (840)
//		Description:
//          Updated SafetyNetSensorData constructor as specified
//          by code review for Safety-Net Subsystem.
//
//  Revision: 027  By: syw   Date:  13-Mar-1997    DR Number: 1780, 1827
//  	Project:  Sigma (840)
//		Description:
//          Changed FlowSensor constructor arguments.  Eliminate
//			RExhDryFlowSensor instance.  RExhO2FlowSensor is now an
//			ExhFlowSensor with both tables pointing to o2 cal data.
//
//  Revision: 026  By: by    Date:  19-Feb-1997    DR Number: DCS 1780
//  	Project:  Sigma (840)
//		Description:
//          Changed FlowSensor constructor arguments.  Use exhO2CalInfo table
//			as second table for RExhFlowSensor and RExhDryFlowSensor.
//
//  Revision: 025  By: by    Date:  12-Feb-1997    DR Number: DCS 1620
//  	Project:  Sigma (840)
//		Description:
//          Modified the constructor of RAtmPressureSensorSNData to use
//          default value when a sensor fault is detected.
//
//  Revision: 024  By: by    Date:  11-Feb-1997    DR Number: DCS 1154
//  	Project:  Sigma (840)
//		Description:
//          Modified constructor of RExhHeaterTemperatureSensorSNData
//          to be tested in the sensor new cycle and the failure time
//          criteria to 20 seconds.  Also modified the contructor of
//          RAtmPressureSensorSNData to be tested in the sensor new cycle.
//
//  Revision: 023  By: syw    Date:  02-Jan-1997    DR Number: DCS 1651
//  	Project:  Sigma (840)
//		Description:
//			Eliminate RSoftResetPort.
//
//  Revision: 022  By: syw    Date:  19-Dec-1996    DR Number: DCS 1571
//  	Project:  Sigma (840)
//		Description:
//			changed argument to 2nd to last argument to FALSE in construction
//			of RAtmPressureSensorSNData.
//
//  Revision: 021  By: syw    Date:  19-Dec-1996    DR Number: DCS 1641
//  	Project:  Sigma (840)
//		Description:
//			Renamed some of the registers to BitAccessGpios.  Added
//			BitAccessGpioMediator object.  Change arguments for BinaryCommand.
//			Changed masks to bit location.  Declare extern PAIIOMap for
//			Service-Mode usage.  Added RSerialReadPort and RSerialWritePort.
//			Deleted RSerialReadWritePort.  Change arguments for DataKey.
//
//  Revision: 020 By: syw   Date: 19-Dec-1996   DR Number: DCS 1603
//      Project:  Sigma (840)
//        Description:
//          Added comment regarding that RAirFlowSerialEeprom,
//			RO2FlowSerialEeprom, and RExhFlowSerialEeprom must be constructed
//			before RCalInfoFlashBlock is constructed
//
//  Revision: 019 By: by   Date: 09-Oct-1996   DR Number: DCS 1324
//      Project:  Sigma (840)
//        Description:
//            Activated the atmospheric pressure sensor range test.
//
//  Revision: 018 By: syw   Date: 19-Aug-1996   DR Number: DCS 1154
//      Project:  Sigma (840)
//        Description:
//            Modified SafetyNetSensorData for the heater to use default value
//            of EXH_MAN_TEMP_MAX_COUNTS to ensure heater remains on when a
//            fault occurs.
//
//  Revision: 017 By: by   Date: 29-Jul-1996   DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Modified SafetyNetSensorData constructors to reflect the
//            20MS BD Secondary Cycle call to SafetyNetTestMediator::newCycle.
//
//  Revision: 016 By: syw   Date: 17-Jul-1996   DR Number: 562
//      Project:  Sigma (840)
//        Description:
//            Added air and o2 flow and temperature sensors for GUI_CPU.
//
//  Revision: 015 By: syw   Date: 26-Jun-1996   DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Changed RPowerSupplyReadWritePort to RPowerSupplyReadPort and
//            RPowerSupplyWritePort exclusively.
//
//  Revision: 014 By: by   Date: 06-Jun-1996   DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Added RSensorMediator.newSecondaryCycle() call before
//            RSensorMediator.newCycle()
//
//  Revision: 013 By: syw   Date: 02-May-1996   DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Construct RExhFlowSensor and RExhGasFlowTemperature for GUI CPU.
//
//  Revision: 012 By: syw   Date: 08-Apr-1996   DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Changed ALPHA to 0.3
//
//  Revision: 011 By: syw   Date: 25-Mar-1996   DR Number DCS 562
//      Project:  Sigma (840)
//        Description:
//            Added support for flash serial number determination.  Instanciate
//            RDacWrapDacPort.
//
//  Revision: 010  By:  syw  Date:  25-Mar-1996    DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Update intialization of all write registers.
//
//  Revision: 009  By:  syw  Date:  15-Mar-1996    DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Removed RamFlashMap, instead use RCalInfoFlashBlock.FlashImage.
//            Default is to point to Flash memory unless we detect a bad EV or
//            flow table, then we point to RAM.
//
//  Revision: 008  By:  syw  Date:  06-Mar-1996    DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Added pre-processor to generate CPU specific code.  Added
//            ExhO2FlowSensor instance.  Added code to point to flash memory if
//            isExhValveCalValid && isFlowSensorInfoValid are true.
//          16-Apr-1996 - a typo was fixed for the dry exh flow sensor
//          to point to the air cal info table.
//
//  Revision: 007  By:  syw  Date:  01-Mar-1996    DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Added initial images for WRITE registers.  Remove RDelayPort since
//            no longer supported by hardware.  Use SIGMA_DEBUG for printf()
//            calls.
//
//  Revision: 006  By:  iv   Date:  20-Feb-1996    DR Number: DCS 678, 674
//      Project:  Sigma (840)
//        Description:
//          Eliminated boundary checks for Ac line voltage.
//          Deleted the initialization for system battery.
//          Changed the INITIAL_SERIAL_PORT_IMAGE value.
//          Removed #ifndef around #include.
//
//  Revision: 005  By:  syw   Date:  25-Jan-1996    DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Defined flow sensor serial eeprom masks.  Constructed air, o2,
//            and exh flow serial eeprom and a CalInfoFlashBlock.  Initialize
//            RSerialReadWriteEeproms to 0x4780.  Removed RPowerSource and
//            RFiO2Monitor calls to initialize() in Initialize().  Obtain
//            psolLiftoff from novram and pass to Psol Constructor.
//
//  Revision: 004  By:  syw   Date:  12-Jan-1996    DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Created PFlashMemory.  Constructed CalInfoFlashBlock and
//            FlowSerialEeprom.  Moved FlowSensor and Exhalation Valve
//            construction after CalInfoFlashBlock is defined.  Change
//            PBDIOFlashMap to PFlashMemory if flash data is OK.
//
//    Revision: 003  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//      Project:  Sigma (840)
//      Description:
//            Removed unnecessary header files.  Added CYCLE_TIME_MS,
//            EXH_HEATER_MASK, SERVICE_MODE_SWITCH_MASK.  Made Rela address
//            specific changes.  Added flash memory definition and eliminated
//            ExhValveTable and XducerCalTable since these are located in flash.
//            Define Global pointer to each instanciated object instead of
//            referencing it from ioAppsMemMapBuffer structure.  Renamed
//            RO2Sensor to RFio2Monitor.  Added RLowVoltageReference,
//            RAiPcbaRevision, RSerialReadWritePort, RPowerSupplyReadWritePort,
//            RCompressorReadWritePort, RSafetyNetControlAPort,
//            RSafetyNetControlBPort, RSafetyNetStatusPort,
//            RBatteryCompressorWritePort, RVentControlWritePort,
//            RServiceSwitch, RCompressorTimer, RCompressorEeprom,
//            RCompressor, RPowerSource, RSystemBattery, RVentStatus,
//            RExhHeater, and <object>SNData.  Made SIGMA_PROD_HW code
//            dependencies.  Changed constructors to handle SNData.  Changed
//            RDacwrap and RSystemBatteryModel id from DACWRAP to SUBMUX_SENSORS.
//            Have pFlowSensorCalInfo and pExhValveTable point to area in flash
//            where data is stored.  Added RegisterType to Register's constructor.
//            Update RGasSupply constructor's arguments.
//
//            Added SubMux::Initialize() method to BD_IO_Devices::Initialize() method.
//            Call RSensorMediator NUM_SUB_MUX_CHANNELS times to initialize the sub mux
//            sensors.  Call RPowerSource.initialize() and RFio2Monitor.initialize().
//
//  Revision: 002  By:  kam   Date:  03-Oct-1995    DR Number: DCS 562
//      Project:  Sigma (840)
//        Description:
//            Updated for Safety Net Sensor Data objects
//
//    Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (840)
//      Description:
//          Initial version
//
//=====================================================================

#include "BD_IO_Devices.hh"

//@ Usage-Classes

#if defined (SIGMA_BD_CPU)

#include "BDIOFlashMap.hh"

#include "BitAccessGpio.hh"
#include "BitAccessGpioMediator.hh"
#include "PressureSensor.hh"
#include "GasInletPressureSensor.hh"
#include "ServosGlobal.h"
#include "SensorMediator.hh"
#include "Psol.hh"
#include "PsolLiftoff.hh"
#include "Solenoid.hh"
#include "SystemBattery.hh"
#include "Fio2Monitor.hh"
#include "VentStatus.hh"
#include "Compressor.hh"
#include "GasSupply.hh"
#include "PowerSource.hh"
#include "BinaryCommand.hh"
#include "DataKey.hh"
#include "EventFilter.hh"
#include "NovRamManager.hh"
#include "FlowSerialEeprom.hh"
#include "CalInfoRefs.hh"
#include "FlowSensorOffset.hh"
#include "AtmPressOffset.hh"
//TODO E600_LL: to be reviewed later
//#include "LinearSensor.hh"

#endif  // defined(SIGMA_BD_CPU)

#include "CalInfoFlashBlock.hh"
//TODO E600_LL: to be reviewed later 
//#include "SerialNumber.hh"
#include "Configuration.hh"
#include "ExhFlowSensor.h"
#include "FlowSensor.hh"
#include "TemperatureSensor.hh"
#include "ExhalationValve.hh"
#include "SmRefs.hh"

#include "BDIORefs.hh"
#include "BDIOTest.h"
#include "SpiFlash.h"
#include "ServosGlobal.h"
#include "Barometer.hh"
#include "ExhCommunicationInterface.h"
#include "ExternalBattery.hh"
#include "Post.hh"
#include "BDIOFlashMap.hh"

#ifdef SIGMA_DEBUG
#include <stdio.h>
#endif // SIGMA_DEBUG

//@ End-Usage

//@ Code...
//TODO E600_LL: In general for this file, current sensor, power supply, and safety-net variables need to be checked

Boolean BD_IO_Devices::SerialNumInitialSignature_ = FALSE ;
Boolean BD_IO_Devices::SerialNumOkay_ = FALSE ;

//=====================================================================
//
//  Breath-Delivery Main Task Cycle Time
//
//====================================================================

const Int32 CYCLE_TIME_MS = 5;
const Int32 SECONDARY_CYCLE_TIME_MS = 20 ;

//=====================================================================
//
//  ADC conversion constants
//
//=====================================================================

#if defined (SIGMA_BD_CPU)

const Real32 ALPHA = 0.30 ;

#endif  // defined(SIGMA_BD_CPU)

const Int16  MAX_COUNT_VALUE  =  4095 ;
const Real32 MAX_VOLT_VALUE  =  5.0f ;
const Real32 COUNT_TO_VOLT_FACTOR = MAX_VOLT_VALUE / MAX_COUNT_VALUE ;

//=====================================================================
//
//  Global Memory Allocation
//
//=====================================================================

#if defined (SIGMA_BD_CPU)

BDIOTest_t					BdioTest;
SpiFlash_t					SpiFlash;


extern struct AIIOMap *const PAIIOMap ;

struct AIIOMap *const PAIIOMap = (struct AIIOMap *)0xFFBEA300 ;
struct CpuIOMap *const PCpuIOMap = (struct CpuIOMap *)0xFFBEB000 ;

#endif  // defined(SIGMA_BD_CPU)

// This global pointer is initialized by NV_MemoryStorageThread.
void *FLASH_SERIAL_NO_ADDR;

// TODO E600_LL: Define BDIO Flash Map here for now but probably should be some where else
struct BDIOFlashMap bdioFlashMap;
struct BDIOFlashMap *pBdioFlashMap = &bdioFlashMap;

//====================================================================
//
//  Memory allocation for the BD I/O Devices subsystem objects
//
//  The memory for each object is allocated in RAM.  The pointer to the allocated
//  memory is cast to the proper class and a reference is assigned.
//
//====================================================================

#if defined (SIGMA_BD_CPU)

//--------------------------------------------------------------------
// inspiration pressure sensor
Uint
    PInspPressureSensor[(sizeof(PressureSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    PressureSensor& RInspPressureSensor =
    *((PressureSensor*) PInspPressureSensor) ;
//--------------------------------------------------------------------
// exhalation pressure sensor
Uint
    PExhPressureSensor[(sizeof(PressureSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    PressureSensor& RExhPressureSensor =
    *((PressureSensor*) PExhPressureSensor) ;
//--------------------------------------------------------------------
// exhalation drive pressure sensor
Uint
    PExhDrvPressureSensor[(sizeof(PressureSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    PressureSensor& RExhDrvPressureSensor =
    *((PressureSensor*) PExhDrvPressureSensor) ;
//--------------------------------------------------------------------
	// Air inlet pressure sensor
Uint
	PAirInletPressureSensor[(sizeof(GasInletPressureSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
	GasInletPressureSensor& RAirInletPressureSensor =
	*((GasInletPressureSensor*) PAirInletPressureSensor) ;
//--------------------------------------------------------------------
// O2 inlet pressure sensor
Uint
	PO2InletPressureSensor[(sizeof(GasInletPressureSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
	GasInletPressureSensor& RO2InletPressureSensor =
	*((GasInletPressureSensor*) PO2InletPressureSensor) ;
//--------------------------------------------------------------------
// o2 side flow sensor offset
Uint
    PO2SideFlowSensorOffset[(sizeof(FlowSensorOffset) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    FlowSensorOffset& RO2SideFlowSensorOffset = 
    *((FlowSensorOffset*) PO2SideFlowSensorOffset) ;
//--------------------------------------------------------------------
// air side flow sensor offset
Uint
    PAirSideFlowSensorOffset[(sizeof(FlowSensorOffset) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    FlowSensorOffset& RAirSideFlowSensorOffset = 
    *((FlowSensorOffset*) PAirSideFlowSensorOffset) ;
//--------------------------------------------------------------------
//--------------------------------------------------------------------
// o2 temperature sensor
Uint
    PO2TemperatureSensor[(sizeof(TemperatureSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    TemperatureSensor& RO2TemperatureSensor =
    *((TemperatureSensor*) PO2TemperatureSensor) ;
//--------------------------------------------------------------------

// air temperature sensor
Uint
    PAirTemperatureSensor[(sizeof(TemperatureSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    TemperatureSensor& RAirTemperatureSensor =
    *((TemperatureSensor*) PAirTemperatureSensor) ;
//--------------------------------------------------------------------
// Exhalation gas temperature sensor
Uint
    PExhGasTemperatureSensor[(sizeof(TemperatureSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    TemperatureSensor& RExhGasTemperatureSensor =
    *((TemperatureSensor*) PExhGasTemperatureSensor) ;
//--------------------------------------------------------------------

#endif // defined (SIGMA_BD_CPU)

// o2 flow sensor
Uint
    PO2FlowSensor[(sizeof(FlowSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    FlowSensor& RO2FlowSensor =
    *((FlowSensor*) PO2FlowSensor) ;
//--------------------------------------------------------------------
// air flow sensor
Uint
    PAirFlowSensor[(sizeof(FlowSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    FlowSensor& RAirFlowSensor =
    *((FlowSensor*) PAirFlowSensor) ;
//--------------------------------------------------------------------
// exh flow sensor
Uint
    PExhFlowSensor[(sizeof(ExhFlowSensor_t) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    ExhFlowSensor_t& RExhFlowSensor =
    *((ExhFlowSensor_t*) PExhFlowSensor) ;
//--------------------------------------------------------------------

#if defined (SIGMA_BD_CPU)

// Sensor Mediator
Uint
    PSensorMediator[(sizeof(SensorMediator) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    SensorMediator& RSensorMediator =
    *((SensorMediator*) PSensorMediator) ;
//--------------------------------------------------------------------
// TODO E600_LL: keep air and o2 psol current sensor code for now for compilation purpose;
// they will need to be removed later since hardware does not support this

// O2 Psol Current Sensor
Uint
    PO2PsolCurrent[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RO2PsolCurrent =
    *((LinearSensor*) PO2PsolCurrent) ;
//--------------------------------------------------------------------
// Air Psol Current Sensor
Uint
    PAirPsolCurrent[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RAirPsolCurrent =
    *((LinearSensor*) PAirPsolCurrent) ;
//--------------------------------------------------------------------
// o2 psol
#if 0    //TODO E600_LL: enable this after psolLiftOff calibration is done
Uint
    PO2Psol[(sizeof(Psol) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Psol& RO2Psol =
    *((Psol*) PO2Psol) ;
#else
    Psol o2Psol(Psol::O2_PSOL, 0);
    Psol &RO2Psol = o2Psol;
    Psol *PO2Psol = &o2Psol;
#endif
//--------------------------------------------------------------------
// air Psol
#if 0    //TODO E600_LL: enable this after psolLiftOff calibration is done
Uint
    PAirPsol[(sizeof(Psol) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Psol& RAirPsol =
    *((Psol*) PAirPsol) ;
#else
	Psol airPsol(Psol::AIR_PSOL, 0);
	Psol &RAirPsol = airPsol;
	Psol *PAirPsol = &airPsol;
#endif
//--------------------------------------------------------------------
// exh psol
	Psol exhPsol(Psol::EXH_PSOL, 0);
	Psol &RExhPsol = exhPsol;
	Psol *PExhPsol = &exhPsol;
//--------------------------------------------------------------------
// E600 - desired flow  
	Psol desiredFlowPsol(Psol::DESIRED_FLOW, 0);
	Psol &RDesiredFlowPsol = desiredFlowPsol;
	Psol *PDesiredFlowPsol = &desiredFlowPsol;

//--------------------------------------------------------------------
// Exhalation Motor Current Sensor
Uint
    PExhMotorCurrent[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RExhMotorCurrent =
    *((LinearSensor*) PExhMotorCurrent) ;
//--------------------------------------------------------------------
// Atmospheric Pressure Sensor
	Barometer& RAtmosphericPressureSensor = *(Barometer::getInstance());
//--------------------------------------------------------------------
// External Battery Module
    ExternalBattery& RExternalBattery = *(ExternalBattery::getInstance());
//--------------------------------------------------------------------
// dc 10V sentry
Uint
    PDc10vSentry[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RDc10vSentry =
    *((LinearSensor*) PDc10vSentry) ;
//--------------------------------------------------------------------
// dc 15V sentry
Uint
    PDc15vSentry[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RDc15vSentry =
    *((LinearSensor*) PDc15vSentry) ;
//--------------------------------------------------------------------
// dc neg 15V sentry
Uint
    PDcNeg15vSentry[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RDcNeg15vSentry =
    *((LinearSensor*) PDcNeg15vSentry) ;
//--------------------------------------------------------------------
//Fio2Monitor
Uint
    PFio2Monitor[(sizeof(Fio2Monitor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Fio2Monitor& RFio2Monitor =
    *((Fio2Monitor*) PFio2Monitor) ;
//--------------------------------------------------------------------
// dc 5V gui
Uint
    PDc5vGui[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RDc5vGui =
    *((LinearSensor*) PDc5vGui) ;
//--------------------------------------------------------------------
// dc 12V gui
Uint
    PDc12vGui[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RDc12vGui =
    *((LinearSensor*) PDc12vGui) ;
//--------------------------------------------------------------------
// dc 5V vent head
Uint
    PDc5vVentHead[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RDc5vVentHead =
    *((LinearSensor*) PDc5vVentHead) ;
//--------------------------------------------------------------------
// dc 12V sentry
Uint
    PDc12vSentry[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RDc12vSentry =
    *((LinearSensor*) PDc12vSentry) ;
//--------------------------------------------------------------------
// alarm cable voltage
Uint
    PAlarmCableVoltage[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RAlarmCableVoltage =
    *((LinearSensor*) PAlarmCableVoltage) ;
//--------------------------------------------------------------------
// safety valve switched side
    Uint PSafetyValveSwitchedSide[(sizeof(LinearSensor) + sizeof(Uint) - 1) / sizeof(Uint)];

    LinearSensor& RSafetyValveSwitchedSide =
    *((LinearSensor*) PSafetyValveSwitchedSide) ;
//--------------------------------------------------------------------
// system battery voltage
Uint
    PSystemBatteryVoltage[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RSystemBatteryVoltage =
    *((LinearSensor*) PSystemBatteryVoltage) ;

//--------------------------------------------------------------------
// power source voltage sensor
Uint
    PPowerSourceVoltage[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RPowerSourceVoltage =
    *((LinearSensor*) PPowerSourceVoltage) ;

//--------------------------------------------------------------------
// system battery current
Uint
    PSystemBatteryCurrent[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RSystemBatteryCurrent =
    *((LinearSensor*) PSystemBatteryCurrent) ;
//--------------------------------------------------------------------
// system battery Model
Uint
    PSystemBatteryModel[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RSystemBatteryModel =
    *((LinearSensor*) PSystemBatteryModel) ;
//--------------------------------------------------------------------
// power fail cap voltage
Uint
    PPowerFailCapVoltage[(sizeof(LinearSensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    LinearSensor& RPowerFailCapVoltage =
    *((LinearSensor*) PPowerFailCapVoltage) ;
//--------------------------------------------------------------------
// DAC wrap sensor
Uint
    PDacWrap[(sizeof(Sensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Sensor& RDacWrap =
    *((Sensor*) PDacWrap) ;
//--------------------------------------------------------------------
// low voltage reference sensor
Uint
    PLowVoltageReference[(sizeof(Sensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Sensor& RLowVoltageReference =
    *((Sensor*) PLowVoltageReference) ;
//--------------------------------------------------------------------
// AI PCBA Revision sensor
Uint
    PAiPcbaRevision[(sizeof(Sensor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Sensor& RAiPcbaRevision =
    *((Sensor*) PAiPcbaRevision) ;
//--------------------------------------------------------------------
// inspiratory autozero solenoid

Uint
    PInspAutozeroSolenoid[(sizeof(Solenoid) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Solenoid& RInspAutozeroSolenoid =
    *((Solenoid*) PInspAutozeroSolenoid) ;
//--------------------------------------------------------------------
// exhalation autozero solenoid
Uint
    PExhAutozeroSolenoid[(sizeof(Solenoid) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Solenoid& RExhAutozeroSolenoid =
    *((Solenoid*) PExhAutozeroSolenoid) ;
//--------------------------------------------------------------------
// crossover solenoid
Uint
    PCrossoverSolenoid[(sizeof(Solenoid) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Solenoid& RCrossoverSolenoid =
    *((Solenoid*) PCrossoverSolenoid) ;
//--------------------------------------------------------------------
// Safety valve

Uint
    PSafetyValve[(sizeof(Solenoid) + sizeof(Uint) - 1)
    / sizeof(Uint)];

    Solenoid& RSafetyValve = *((Solenoid*) PSafetyValve) ;
//--------------------------------------------------------------------

//--------------------------------------------------------------------
// Exhalation valve
Uint
    PExhalationValve[(sizeof(ExhalationValve) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    ExhalationValve& RExhalationValve =
    *((ExhalationValve*) PExhalationValve) ;
//--------------------------------------------------------------------

//--------------------------------------------------------------------
// bit access GPIOs

Uint
    PBitAccessGpio[(sizeof(BitAccessGpio) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    BitAccessGpio& RBitAccessGpio =
    *((BitAccessGpio*) PBitAccessGpio) ;
//--------------------------------------------------------------------
// Service Mode Switch
Uint
    PServiceSwitch[(sizeof(BinaryIndicator) + sizeof(Uint) - 1)
    / sizeof(Uint)];
BinaryIndicator& RServiceSwitch =
    *((BinaryIndicator*) PServiceSwitch) ;
//--------------------------------------------------------------------
//  Compressor
Uint
    PCompressor[(sizeof(Compressor) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    Compressor& RCompressor =
    *((Compressor*) PCompressor) ;
//--------------------------------------------------------------------
// Gas Supply
Uint
    PGasSupply[(sizeof(GasSupply) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    GasSupply& RGasSupply =
    *((GasSupply*) PGasSupply) ;
//--------------------------------------------------------------------
//Power Source
Uint
    PPowerSource[(sizeof(PowerSource) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    PowerSource& RPowerSource =
    *((PowerSource*) PPowerSource) ;
//--------------------------------------------------------------------
// System Battery
Uint
    PSystemBattery[(sizeof(SystemBattery) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    SystemBattery& RSystemBattery =
    *((SystemBattery*) PSystemBattery) ;
//--------------------------------------------------------------------
// Ventilator Status
Uint
    PVentStatus[(sizeof(VentStatus) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    VentStatus& RVentStatus =
    *((VentStatus*) PVentStatus) ;
//--------------------------------------------------------------------
// Data Key
Uint
    PDataKey[(sizeof(DataKey) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    DataKey& RDataKey =
    *((DataKey*) PDataKey) ;
//--------------------------------------------------------------------
// Event Filter
Uint
    PEventFilter[(sizeof(EventFilter) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    EventFilter& REventFilter =
    *((EventFilter*) PEventFilter) ;
//--------------------------------------------------------------------
// Safety Net Sensor Data for the O2 Flow Sensor Temperature Signal
Uint
    PO2TemperatureSensorSNData[(sizeof(SafetyNetSensorData) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    SafetyNetSensorData& RO2TemperatureSensorSNData =
    *((SafetyNetSensorData*) PO2TemperatureSensorSNData) ;
//--------------------------------------------------------------------
// Safety Net Sensor Data for the Air Flow Sensor Temperature Signal
Uint
    PAirTemperatureSensorSNData[(sizeof(SafetyNetSensorData) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    SafetyNetSensorData& RAirTemperatureSensorSNData =
    *((SafetyNetSensorData*) PAirTemperatureSensorSNData) ;
//--------------------------------------------------------------------
// Safety Net Sensor Data for the Expiratory Flow Sensor Temperature Signal
Uint
    PExhGasTemperatureSensorSNData[(sizeof(SafetyNetSensorData) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    SafetyNetSensorData& RExhGasTemperatureSensorSNData =
    *((SafetyNetSensorData*) PExhGasTemperatureSensorSNData) ;
//--------------------------------------------------------------------
// Safety Net Sensor Data for the Atmospheric Pressure Sensor
Uint
    PAtmPressureSensorSNData[(sizeof(SafetyNetSensorData) + sizeof(Uint) - 1)
    / sizeof(Uint)];
    SafetyNetSensorData& RAtmPressureSensorSNData =
    *((SafetyNetSensorData*) PAtmPressureSensorSNData) ;
//--------------------------------------------------------------------
// Safety Net Sensor Data for the FIO2 Monitor Signal
Uint
    PFio2MonitorSNData[(sizeof(SafetyNetSensorData) + sizeof(Uint) - 1)
    / sizeof(Uint)];
SafetyNetSensorData& RFio2MonitorSNData =
    *((SafetyNetSensorData*) PFio2MonitorSNData) ;
//--------------------------------------------------------------------
// air flow serial eeprom
Uint
    PAirFlowSerialEeprom[(sizeof(FlowSerialEeprom) + sizeof(Uint) - 1)
    / sizeof(Uint)];
FlowSerialEeprom& RAirFlowSerialEeprom =
    *((FlowSerialEeprom*) PAirFlowSerialEeprom) ;
//--------------------------------------------------------------------
// o2 flow serial eeprom
Uint
    PO2FlowSerialEeprom[(sizeof(FlowSerialEeprom) + sizeof(Uint) - 1)
    / sizeof(Uint)];
FlowSerialEeprom& RO2FlowSerialEeprom =
    *((FlowSerialEeprom*) PO2FlowSerialEeprom) ;
//--------------------------------------------------------------------

#endif  // defined(SIGMA_BD_CPU)

//--------------------------------------------------------------------
// calibration information flash block
Uint
    PCalInfoFlashBlock[(sizeof(CalInfoFlashBlock) + sizeof(Uint) - 1)
    / sizeof(Uint)];
CalInfoFlashBlock& RCalInfoFlashBlock =
    *((CalInfoFlashBlock*) PCalInfoFlashBlock) ;

//--------------------------------------------------------------------
// system configuration
Uint
    PConfiguration[(sizeof(Configuration) + sizeof(Uint) -1)
    / sizeof(Uint)];
Configuration& RSystemConfiguration =
    *((Configuration*) PConfiguration) ;

//--------------------------------------------------------------------

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
//      This method constructs and initializes the BD I/O Devices objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Each BD I/O Devices object is constructed here.  The construction
//      uses the placement new operator.  This forces the construction to
//      be performed at the specified location.  The actual constructed
//      location is then compared to the specified location.  If the
//      locations do not match, a soft fault is generated.  The class
//      specific initialize methods are then executed.
//		$[04331]
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

void
BD_IO_Devices::Initialize( void)
{
    CALL_TRACE("BD_IO_Devices::Initialize( void)") ;


#if defined (SIGMA_BD_CPU)
	// TODO E600_LL: Might need to be removed in the future
	BdioTest.Initialize();

#if 0 // TODO E600_LL: All SafetyNet related code must be reviewed
    // $[TI1]
    //--------------------------------------------------------------------
    // Construct the Safety Net Sensor Data for the O2 Flow Sensor Temperature Signal
    SafetyNetSensorData* pO2TemperatureSensorSNData = new (&RO2TemperatureSensorSNData)

        SafetyNetSensorData ( Q_TEMP_MIN_COUNTS, Q_TEMP_MAX_COUNTS,
                              Q_AIR_O2_TEMP_DEFAULT, 2,
                              BK_O2_FLOW_TEMP_OOR, TRUE, TRUE );

    FREE_POST_CONDITION(pO2TemperatureSensorSNData ==
        &RO2TemperatureSensorSNData, BD_IO_DEVICES, BDIODEVICES) ;

    //--------------------------------------------------------------------
    // Construct the Safety Net Sensor Data for the Air Flow Sensor Temperature Signal
    SafetyNetSensorData* pAirTemperatureSensorSNData = new (&RAirTemperatureSensorSNData)

        SafetyNetSensorData ( Q_TEMP_MIN_COUNTS, Q_TEMP_MAX_COUNTS,
                              Q_AIR_O2_TEMP_DEFAULT, 2,
                              BK_AIR_FLOW_TEMP_OOR, TRUE, TRUE );

    FREE_POST_CONDITION(pAirTemperatureSensorSNData ==
        &RAirTemperatureSensorSNData, BD_IO_DEVICES, BDIODEVICES) ;

    //--------------------------------------------------------------------
    // Construct the Safety Net Sensor Data for the Expiratory Flow Sensor Temperature Signal
    SafetyNetSensorData* pExhGasTemperatureSensorSNData = new (&RExhGasTemperatureSensorSNData)

        SafetyNetSensorData ( Q_TEMP_MIN_COUNTS, Q_TEMP_MAX_COUNTS,
                              Q_EXH_TEMP_DEFAULT, 2,
                              BK_EXH_FLOW_TEMP_OOR, TRUE, TRUE );

    FREE_POST_CONDITION(pExhGasTemperatureSensorSNData ==
        &RExhGasTemperatureSensorSNData, BD_IO_DEVICES, BDIODEVICES) ;
    //--------------------------------------------------------------------
    // Construct the Safety Net Sensor Data for the Atmospheric Pressure Sensor
    SafetyNetSensorData* pAtmPressureSensorSNData = new (&RAtmPressureSensorSNData)

        // use minimum value for default to under ventilate after sensor fails
        SafetyNetSensorData ( ATM_PRESS_MIN_COUNTS, ATM_PRESS_MAX_COUNTS,
                              ATM_PRESS_MIN_COUNTS, 2,
                              BK_ATM_PRESS_OOR, TRUE, TRUE );

    FREE_POST_CONDITION(pAtmPressureSensorSNData ==
        &RAtmPressureSensorSNData, BD_IO_DEVICES, BDIODEVICES) ;
    //--------------------------------------------------------------------
    // Construct the Safety Net Sensor Data for the FIO2 Monitor Signal
    SafetyNetSensorData* pFio2MonitorSNData = new (&RFio2MonitorSNData)

        SafetyNetSensorData ( O2_SENSOR_MIN_COUNTS, O2_SENSOR_MAX_COUNTS,
                              NO_DEFAULT_VALUE_USED, 2,
                              BK_O2_SENSOR_OOR, TRUE, FALSE );

    FREE_POST_CONDITION(pFio2MonitorSNData ==
        &RFio2MonitorSNData, BD_IO_DEVICES, BDIODEVICES) ;
#endif
    //-----------------------------------------------------------------
    // Construct the inspiration pressure sensor
    PressureSensor* pInspPressureSensor = new (&RInspPressureSensor)
        PressureSensor(NULL, AdcChannels::ADC_INSP_PRESSURE_TRANSDUCER, ALPHA) ;

    FREE_POST_CONDITION(pInspPressureSensor ==
                 &RInspPressureSensor, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the Exhalation pressure sensor
    PressureSensor *pExhPressureSensor = new (&RExhPressureSensor)
       PressureSensor(NULL, AdcChannels::ADC_EXP_PRESSURE_TRANSDUCER, ALPHA) ;

    FREE_POST_CONDITION(pExhPressureSensor ==
                 &RExhPressureSensor, BD_IO_DEVICES, BDIODEVICES) ;
	//-----------------------------------------------------------------
    // Construct the Exhalation pressure sensor
    PressureSensor *pExhDrvPressureSensor = new (&RExhDrvPressureSensor)
       PressureSensor(NULL, AdcChannels::ADC_EXH_DRIVE_PRESSURE_TRANSDUCER, ALPHA) ;

    FREE_POST_CONDITION(pExhDrvPressureSensor ==
                 &RExhDrvPressureSensor, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the Air inlet pressure sensor
	GasInletPressureSensor *pAirInletPressureSensor = new (&RAirInletPressureSensor)
       GasInletPressureSensor(NULL, AdcChannels::ADC_AIR_PRESSURE_TRANSDUCER, ALPHA, 
	   pAirInletPressureTransducer) ;

    FREE_POST_CONDITION(pAirInletPressureSensor ==
                 &RAirInletPressureSensor, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
	    // Construct the O2 inlet pressure sensor
    GasInletPressureSensor *pO2InletPressureSensor = new (&RO2InletPressureSensor)
       GasInletPressureSensor(NULL, AdcChannels::ADC_O2_PRESSURE_TRANSDUCER, ALPHA,
	   pO2InletPressureTransducer) ;

    FREE_POST_CONDITION(pO2InletPressureSensor ==
                 &RO2InletPressureSensor, BD_IO_DEVICES, BDIODEVICES) ;

    //-----------------------------------------------------------------
    // Construct the SystemBattery Voltage sensor
    LinearSensor* pSystemBatteryVoltage = new (&RSystemBatteryVoltage)
        LinearSensor( NULL, AdcChannels::ADC_INT_BATTERY, SYSBATT_VOLTSENSOR_VOLT_MIN,
        SYSBATT_VOLTSENSOR_VOLT_MAX, SYSBATT_VOLTSENSOR_ENG_MIN, SYSBATT_VOLTSENSOR_ENG_MAX );

    //-----------------------------------------------------------------
    // Construct the Power Source Voltage sensor
    LinearSensor* pPowerSourceVoltage = new (&RPowerSourceVoltage)
        LinearSensor( NULL, AdcChannels::ADC_EXT_BATTERY, PowerSource::VOLTSENSOR_VOLT_MIN,
        PowerSource::VOLTSENSOR_VOLT_MAX, PowerSource::VOLTSENSOR_VOLT_MIN, 
        PowerSource::VOLTSENSOR_VOLT_MAX );

    //-----------------------------------------------------------------
    // Construct the Fio2 Monitor
    // Note:  RFio2MonitorSNData must be constructed before RFio2Monitor
	//TODO E600_LL: FiO2 SafetyNet is to be revisited
    Fio2Monitor* pFio2Monitor = new (&RFio2Monitor)
		 Fio2Monitor( AdcChannels::ADC_FIO2_SENSOR/*, &RFio2MonitorSNData*/ );

    FREE_POST_CONDITION(pFio2Monitor ==
                 &RFio2Monitor, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
	// BitAccessGpio construction
	BitAccessGpio* pBitAccessGpio= new (&RBitAccessGpio)
		BitAccessGpio(BitAccessGpio::OFF) ;

    FREE_POST_CONDITION(pBitAccessGpio ==
        &RBitAccessGpio, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the insp auto zero solenoid
    Solenoid* pSolenoid = new (&RInspAutozeroSolenoid)
        Solenoid(Solenoid::CLOSED,
                DOUT_P1_SOLENOID, RBitAccessGpio ) ;

    FREE_POST_CONDITION(pSolenoid ==
        &RInspAutozeroSolenoid, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the exh auto zero solenoid
    pSolenoid = new (&RExhAutozeroSolenoid)
        Solenoid(Solenoid::CLOSED,
                DOUT_P2_SOLENOID, RBitAccessGpio ) ;

    FREE_POST_CONDITION(pSolenoid ==
        &RExhAutozeroSolenoid, BD_IO_DEVICES, BDIODEVICES) ;
	//-----------------------------------------------------------------
    // Construct the exh auto zero solenoid
    // Note: RBitAccessGpio must be constructed before RExhAutozeroSolenoid
    pSolenoid = new (&RCrossoverSolenoid) Solenoid(Solenoid::CLOSED, DOUT_CROSSOVER_SOLENOID, RBitAccessGpio) ;

    FREE_POST_CONDITION(pSolenoid ==
        &RCrossoverSolenoid, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the safety valve solenoid
    // Note: RBitAccessGpio must be constructed before RSafetyValve
    pSolenoid = new (&RSafetyValve) Solenoid(Solenoid::OPEN, DOUT_SAFETY_SOLENOID, RBitAccessGpio) ;

    FREE_POST_CONDITION(pSolenoid == &RSafetyValve, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the o2 psol.
    // PsolLiftoff is constructed to access the liftoff calibration valve.
    PsolLiftoff psolLiftoff;
	NovRamManager::GetPsolLiftoffCurrents (psolLiftoff);

    Psol* pPsol = new (&RO2Psol)
        Psol( Psol::O2_PSOL,
             psolLiftoff.getO2PsolLiftoff()) ;

    FREE_POST_CONDITION(pPsol ==
                 &RO2Psol, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the air psol.
    pPsol = new (&RAirPsol)
        Psol( Psol::AIR_PSOL,
            psolLiftoff.getAirPsolLiftoff()) ;

    FREE_POST_CONDITION(pPsol ==
                 &RAirPsol, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
	// TODO E600_LL: Remove Compressor code when appropriate
    //-----------------------------------------------------------------
    // Construct the compressor
    // Note: RCompressorReadPort and RCompressorWritePort must be constructed before
    //		 RCompressor
    Compressor* pCompressor = new (&RCompressor) Compressor() ;

    FREE_POST_CONDITION(pCompressor ==
        &RCompressor, BD_IO_DEVICES, BDIODEVICES) ;

    // Construct the power source
    PowerSource* pPowerSource = new (&RPowerSource) PowerSource() ;

    FREE_POST_CONDITION(pPowerSource ==
        &RPowerSource, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the vent status
    VentStatus* pVentStatus = new (&RVentStatus) VentStatus() ;

    FREE_POST_CONDITION(pVentStatus ==
        &RVentStatus, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct Event Filter
    EventFilter* pEventFilter = new (&REventFilter) EventFilter() ;

    FREE_POST_CONDITION(pEventFilter ==
        &REventFilter, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the sensor mediator
    SensorMediator* pSensorMediator = new (&RSensorMediator) SensorMediator() ;

    FREE_POST_CONDITION(pSensorMediator ==
                 &RSensorMediator, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // air flow serial eeprom
       FlowSerialEeprom *pFlowSerialEeprom =
		   new (&RAirFlowSerialEeprom) FlowSerialEeprom( FlowSerialEeprom::AIR_SET) ;

    FREE_POST_CONDITION( pFlowSerialEeprom ==
        &RAirFlowSerialEeprom,  BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // o2 flow serial eeprom
       pFlowSerialEeprom =
		   new (&RO2FlowSerialEeprom) FlowSerialEeprom( FlowSerialEeprom::O2_SET) ;

    FREE_POST_CONDITION( pFlowSerialEeprom ==
        &RO2FlowSerialEeprom,  BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------

#endif  // defined(SIGMA_BD_CPU)

    //-----------------------------------------------------------------
    // Construct the calibration information flash block
    // Note: RAirFlowSerialEeprom, RO2FlowSerialEeprom, and RExhFlowSerialEeprom
    //		 must be constructed before RCalInfoFlashBlock
    CalInfoFlashBlock *pCalInfoFlashBlock =
        new (&RCalInfoFlashBlock) CalInfoFlashBlock( (struct BDIOFlashMap *)FLASH_SERIAL_NO_ADDR) ;

    FREE_POST_CONDITION( pCalInfoFlashBlock ==
        &RCalInfoFlashBlock,  BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
	if(!Post::IsNovramInitialized())//check SERIAL_NO_INIT_PATTERN (0x55555555 U's)
	{
		//replace U's with default just before the NV serialNumber data is first used, 
		//to prevent side-effect to others
		struct BDIOFlashMap *pBDIOFlashMap = (struct BDIOFlashMap *)FLASH_SERIAL_NO_ADDR;
		SerialNumber snDefault;
		strncpy(pBDIOFlashMap->serialNumber, snDefault.getString(), SERIAL_NO_SIZE);
	}

    // construct system configuration
    Configuration *pConfiguration =
        new (&RSystemConfiguration) Configuration( (struct BDIOFlashMap *)FLASH_SERIAL_NO_ADDR) ;

    FREE_POST_CONDITION( pConfiguration ==
        &RSystemConfiguration, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------

#if defined (SIGMA_BD_CPU)

	//-----------------------------------------------------------------
    // Construct the exhalation flow sensor

    ExhFlowSensor_t* pExhFlowSensor = new (&RExhFlowSensor)
        ExhFlowSensor_t( AdcChannels::ADC_SIGNAL_TEST );

    FREE_POST_CONDITION(pExhFlowSensor ==
                 &RExhFlowSensor, BD_IO_DEVICES, BDIODEVICES) ;

	// Note: RCalInfoFlashBlock must be constructed before executing further

    // TODO E600_LL: Need to review the purpose of the following statement
	// If the cal data aren't valid, init the persistent memory with a valid constructed BDIOFlashMap
	Boolean bFlowSensorInfoValid = RCalInfoFlashBlock.getFlowSensorInfoValid();
	Boolean bExhValveCalInfoValid = RCalInfoFlashBlock.getExhValveCalValid();
	if(!bFlowSensorInfoValid)
	{
		RETAILMSG(1, (L"Flow Sensor Info is NOT valid!\r\n"));
	}
	if(!bExhValveCalInfoValid)
	{
		RETAILMSG(1, (L"Exh. Valve Cal Info is NOT valid!\r\n"));
	}
    
#if 0	// TODO E600_LL: this block of code seems to be unnecessary now; need to review.
	if (!bFlowSensorInfoValid || !bExhValveCalInfoValid)
    {
        struct BDIOFlashMap *PersistentFlashMap = (struct BDIOFlashMap *)FLASH_SERIAL_NO_ADDR;

        // Get a copy of the exhalation flow cal data, in case we need to get it back after setting defaults
        ExhSensorTable_t PersistentSensorTable = PersistentFlashMap->exhCalInfo.ExhSensorTable;

        // Do the same for the exhalation valve cal data
        CalInfoFlashSignature::Signature    PersistentExhValveCalInfoSignature  = PersistentFlashMap->exhValveCalInfoSignature;
        ExhValveTable                       PersistentExhValveCalInfo           = PersistentFlashMap->exhValveCalInfo;
        CrcValue                            PersistentExhValveCalInfoCrc        = PersistentFlashMap->exhValveCalInfoCrc;

        // Copy the default version into the persistent memory
        memcpy(FLASH_SERIAL_NO_ADDR, pBdioFlashMap, sizeof(BDIOFlashMap));

        // Once the default has been set,
        // if a cal had already been performed, give the flow sensor the calibrated data
        if (RCalInfoFlashBlock.getExhFlowSensorInfoValid())
        {
            RExhFlowSensor.SetCalData(PersistentSensorTable);
        }

        // If the cal was already done, get the good cal data back
        if (RCalInfoFlashBlock.getExhValveCalValid())
        {
            PersistentFlashMap->exhValveCalInfoSignature    =   PersistentExhValveCalInfoSignature;
            PersistentFlashMap->exhValveCalInfo             =   PersistentExhValveCalInfo;
            PersistentFlashMap->exhValveCalInfoCrc          =   PersistentExhValveCalInfoCrc;
        }

        // Need to do the same for the exhalation valve cal data
    }
#endif
	
	pBdioFlashMap = (struct BDIOFlashMap *)FLASH_SERIAL_NO_ADDR;

	if(ExhCommunicationInterface::getInstance()->GetSensorBoardType() == EXH_SENSOR_BOARD_OLD)
	{
		if (RCalInfoFlashBlock.getExhFlowSensorCalValid())
		{
			RExhFlowSensor.SetCalData(pBdioFlashMap->exhCalInfo.ExhSensorTable);
		}
		else
		{
			RETAILMSG(1, (L"ExhFlowSensor Cal is NOT valid!\r\n"));
		}
	}

	if(!bFlowSensorInfoValid)
	{
		FlowSensorCalInfo       EepromO2CalInfo;
		FlowSensorCalInfo       EepromAirCalInfo;
		ExhSensorTableStorage_t		EepromExhCalInfo;

		// Read from the EEPROMs and store locally
		// These return 0 if read was good.
		Uint32 O2ReadResult = RO2FlowSerialEeprom.readFlowSensorEeprom( &EepromO2CalInfo, FlowSerialEeprom::O2_SET);
		Uint32 AirReadResult = RAirFlowSerialEeprom.readFlowSensorEeprom( &EepromAirCalInfo, FlowSerialEeprom::AIR_SET);
		Uint32 ExhIsReadError = ExhCommunicationInterface::getInstance()->readSerialNumber(EepromExhCalInfo.serialNumber_);

		// Read was good? Store in the persistent memory area
		if (!O2ReadResult && !RO2FlowSerialEeprom.IsReadError())
		{
			pBdioFlashMap->o2CalInfo = EepromO2CalInfo;
			pBdioFlashMap->o2CalInfoCrc = CalculateCrc((Byte *) &(pBdioFlashMap->o2CalInfo), sizeof (FlowSensorCalInfo));
		}

		// Read was good? Store in the persistent memory area
		if (!AirReadResult && !RAirFlowSerialEeprom.IsReadError())
		{
			pBdioFlashMap->airCalInfo = EepromAirCalInfo;
			pBdioFlashMap->airCalInfoCrc = CalculateCrc((Byte *) &(pBdioFlashMap->airCalInfo), sizeof (FlowSensorCalInfo));
		}

		// Read was good? Store in the persistent memory area
		if (!ExhIsReadError )
		{
			pBdioFlashMap->exhCalInfo = EepromExhCalInfo;
			pBdioFlashMap->exhCalInfoCrc = CalculateCrc((Byte *) &(pBdioFlashMap->exhCalInfo), sizeof (ExhSensorTableStorage_t));
		}
		// Set the flow data in the data area the rest of the system accesses
		RCalInfoFlashBlock.InitFlowSensorData(EepromAirCalInfo, EepromO2CalInfo, EepromExhCalInfo);
	}	

    //=====================================================================
    //
    //  The gas temperature sensors are part of the flow sensors and
    //  therefore must be constructed before the flow sensors.
    //
    //=====================================================================

    //-----------------------------------------------------------------
    // Construct the O2 gas temperature sensor
    // Note: RO2TemperatureSensorSNData must be constructed before RO2TemperatureSensor

    TemperatureSensor* pO2TemperatureSensor = new (&RO2TemperatureSensor)
        TemperatureSensor(&RO2TemperatureSensorSNData,
                         AdcChannels::ADC_O2_TEMP,
                         ::FlowSensorThermistorTable) ;

    FREE_POST_CONDITION(pO2TemperatureSensor ==
        &RO2TemperatureSensor, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the air gas temperature sensor
    // Note:  RAirTemperatureSensorSNData must be constructed before RAirTemperatureSensor

    TemperatureSensor* pAirTemperatureSensor = new (&RAirTemperatureSensor)
        TemperatureSensor(&RAirTemperatureSensorSNData,
                          AdcChannels::ADC_AIR_TEMP,
                          ::FlowSensorThermistorTable) ;

    FREE_POST_CONDITION(pAirTemperatureSensor ==
        &RAirTemperatureSensor, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the exhalation Gas temperature sensor
    // Note:  RExhGasTemperatureSensorSNData must be constructed before RExhGasTemperatureSensor
#if 0 // TODO E600_LL: current hardware does not support exh. gas temp but will in the future
    TemperatureSensor* pExhGasTemperatureSensor = new (&RExhGasTemperatureSensor)
        TemperatureSensor(&RExhGasTemperatureSensorSNData,
                          AdcChannels::EXHALATION_GAS_TEMPERATURE_SENSOR,
                          ::FlowSensorThermistorTable) ;

    FREE_POST_CONDITION(pExhGasTemperatureSensor ==
        &RExhGasTemperatureSensor, BD_IO_DEVICES, BDIODEVICES) ;
#endif
    //-----------------------------------------------------------------
    // Construct the o2 side flow sensor offset
    FlowSensorOffset* pFlowSensorOffset = new (&RO2SideFlowSensorOffset)
        FlowSensorOffset() ;

    FREE_POST_CONDITION(pFlowSensorOffset ==
                 &RO2SideFlowSensorOffset, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the air side flow sensor offset

    pFlowSensorOffset = new (&RAirSideFlowSensorOffset)
        FlowSensorOffset() ;

    FREE_POST_CONDITION(pFlowSensorOffset ==
                 &RAirSideFlowSensorOffset, BD_IO_DEVICES, BDIODEVICES) ;
    //-----------------------------------------------------------------
    // Construct the o2 flow sensor
    // Note: RO2TemperatureSensor must be constructed before RO2FlowSensor

    FlowSensor* pO2FlowSensor = new (&RO2FlowSensor)
        FlowSensor(NULL,
           AdcChannels::ADC_O2_FLOW,
           RO2TemperatureSensor,
           ALPHA,
           &pBdioFlashMap->o2CalInfo) ;

    FREE_POST_CONDITION(pO2FlowSensor ==
                 &RO2FlowSensor, BD_IO_DEVICES, BDIODEVICES) ;

    //-----------------------------------------------------------------
    // Construct the air flow sensor
    // Note: RAirTemperatureSensor must be constructed before RAirFlowSensor

    FlowSensor* pAirFlowSensor = new (&RAirFlowSensor)
        FlowSensor(NULL,
           AdcChannels::ADC_AIR_FLOW,
           RAirTemperatureSensor,
           ALPHA,
           &pBdioFlashMap->airCalInfo) ;

    FREE_POST_CONDITION(pAirFlowSensor ==
                 &RAirFlowSensor, BD_IO_DEVICES, BDIODEVICES) ;

    //-----------------------------------------------------------------
    // Construct the exhalation flow sensor
    // Note: RExhGasTemperatureSensor must be constructed before RExhFlowSensor

// E600 BDIO	FlowSensorCalInfo* pSecondaryFlowSensorCalInfo = &pBdioFlashMap->exhO2CalInfo ;

    //-----------------------------------------------------------------

    //-----------------------------------------------------------------
    // Construct the exhalation valve

    ExhValveTable *pExhValveTable = &pBdioFlashMap->exhValveCalInfo ;

    ExhalationValve* pExhalationValve = new (&RExhalationValve) ExhalationValve( pExhValveTable) ;

    FREE_POST_CONDITION(pExhalationValve == &RExhalationValve, BD_IO_DEVICES, BDIODEVICES) ;

    //-----------------------------------------------------------------
    //
    //  Initialize the BD I/O subsystem
    //
    //-----------------------------------------------------------------

    FlowSensorCalInfo* pFlowSensorCalInfo;

    pFlowSensorCalInfo = &pBdioFlashMap->o2CalInfo ;
    RO2TemperatureSensor.setTempOffset( pFlowSensorCalInfo->getTemperatureCorrection()) ;

    pFlowSensorCalInfo = &pBdioFlashMap->airCalInfo ;
    RAirTemperatureSensor.setTempOffset( pFlowSensorCalInfo->getTemperatureCorrection()) ;

	RAirTemperatureSensor.setTauCoeffs( 2.19211, 0.09391, 0.10783, 0.00069) ;
	RO2TemperatureSensor.setTauCoeffs( 2.19211, 0.09391, 0.10783, 0.00069) ;

	// set the atmospheric pressure offset with value stored in NovRam.
	AtmPressOffset temp ;
    NovRamManager::GetAtmPressOffset( temp) ;
	Real32 offset = temp.getAtmPressOffset() ;
	RAtmosphericPressureSensor.setOffset( offset) ;

    NovRamManager::GetAirFlowSensorOffset( RAirSideFlowSensorOffset) ;
	NovRamManager::GetO2FlowSensorOffset( RO2SideFlowSensorOffset) ;

	RSensorMediator.newSecondaryCycle();
    RSensorMediator.newCycle();

    // Initialize the sensor filters.  These cannot be initialize
    // during construction because no sensor data exists until
    // the sensor mediator new cycle is executed.
    RO2FlowSensor.initValues( RO2FlowSensor.getValue(), 0.0, 0.0,
                              RO2FlowSensor.getValue()) ;
    RAirFlowSensor.initValues( RAirFlowSensor.getValue(), 0.0, 0.0,
                               RAirFlowSensor.getValue()) ;
    RExhFlowSensor.initValues( RExhFlowSensor.getValue(), 0.0, 0.0,
                               RExhFlowSensor.getValue()) ;

    RInspPressureSensor.initValues( RInspPressureSensor.getValue(), 0.0, 0.0,
                                    RInspPressureSensor.getValue()) ;
    RExhPressureSensor.initValues( RExhPressureSensor.getValue(), 0.0, 0.0,
                                   RExhPressureSensor.getValue());

#endif  // defined(SIGMA_BD_CPU)

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BD_IO_Devices::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;

    FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, BD_IO_DEVICES,
                            lineNumber, pFileName, pPredicate) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	GetSerialNumInitialSignature
//
//@ Interface-Description
//		This method has no arguments and returns the status whether the
//		serial number in flash is the initial signature.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns data member SerialNumInitialSignature_.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
BD_IO_Devices::GetSerialNumInitialSignature( void)
{
	CALL_TRACE("BD_IO_Devices::GetSerialNumInitialSignature( void)") ;

    // $[TI1]

	return( SerialNumInitialSignature_) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================



