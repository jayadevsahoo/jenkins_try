
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//	      Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AbstractSortC<TYPE> - Abstract Sorted List of 'TYPE' Copies.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSortC.I_v   25.0.4.0   19 Nov 2013 14:03:44   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//=====================================================================
//
//  Friend Functions...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
 
//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  operator<<(ostr, sortList)  [Friend Function]
//
// Interface-Description
//	Dump out an ASCII representation of the current state of this
//	list to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//	none
//---------------------------------------------------------------------
// PostConditions
//	none
// End-Method
//=====================================================================
 
inline Ostream&
operator<<(Ostream& ostr, const AbstractSortC<TYPE>& sortList)
{
  CALL_TRACE("::operator<<(ostr, sortList)");
  return(sortList.print_(ostr));
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEmpty()  [const]
//
//@ Interface-Description
//	Is this list empty?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
AbstractSortC<TYPE>::isEmpty(void) const
{
  CALL_TRACE("isEmpty()");
  return(rElemList_.isEmpty());  // $[TI1]
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AbstractSortC<TYPE>(pList)  [Constructor]
//
//@ Interface-Description
//	Create an empty sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The 'rElemList_' alias can NOT be used from within this
//	constructor, because the element list will NOT be constructed
//	until the derived class's constructor is run.
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	(isEmpty())
//@ End-Method
//=====================================================================

inline
AbstractSortC<TYPE>::AbstractSortC<TYPE>(AbstractDListC<TYPE>* pList)
					 : rElemList_(*pList)
{
  CALL_TRACE("AbstractSortC<TYPE>()");
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isLegalNode_(pNode)  [const]
//
//@ Interface-Description
//	Is 'pNode' a pointer to a legal, allocated doubly-linked node?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
AbstractSortC<TYPE>::isLegalNode_(const DLinkedNodeC<TYPE>* pNode) const
{
  CALL_TRACE("isLegalNode_(pNode)");
  return(rElemList_.isLegalNode_(pNode));  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getFirst_()  [const]
//
//@ Interface-Description
//	Return a constant pointer to the first node in this list.  If the
//	list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const DLinkedNodeC<TYPE>*
AbstractSortC<TYPE>::getFirst_(void) const
{
  CALL_TRACE("getFirst_() const");
  return(rElemList_.getFirst_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrent_()  [const]
//
//@ Interface-Description
//	Return a constant pointer to the current node in this list.  If the
//	list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const DLinkedNodeC<TYPE>*
AbstractSortC<TYPE>::getCurrent_(void) const
{
  CALL_TRACE("getCurrent_() const");
  return(rElemList_.getCurrent_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getLast_()  [const]
//
//@ Interface-Description
//	Return a constant pointer to the last node in this list.  If the
//	list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const DLinkedNodeC<TYPE>*
AbstractSortC<TYPE>::getLast_(void) const
{
  CALL_TRACE("getLast_() const");
  return(rElemList_.getLast_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getFirst_()
//
//@ Interface-Description
//	Return a non-constant pointer to the first node in this list.  If
//	the list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline DLinkedNodeC<TYPE>*
AbstractSortC<TYPE>::getFirst_(void)
{
  CALL_TRACE("getFirst_()");
  return(rElemList_.getFirst_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrent_()
//
//@ Interface-Description
//	Return a non-constant pointer to the current node in this list.
//	If the list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline DLinkedNodeC<TYPE>*
AbstractSortC<TYPE>::getCurrent_(void)
{
  CALL_TRACE("getCurrent_()");
  return(rElemList_.getCurrent_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getLast_()
//
//@ Interface-Description
//	Return a non-constant pointer to the last node in this list.  If
//	the list is empty, then 'NULL' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline DLinkedNodeC<TYPE>*
AbstractSortC<TYPE>::getLast_(void)
{
  CALL_TRACE("getLast_()");
  return(rElemList_.getLast_());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setCurrent_(pNewCurr)
//
//@ Interface-Description
//	Set the current node of this list to 'pNewCurr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	(isLegalNode_(pNewCurr))
//---------------------------------------------------------------------
//@ PostConditions
//	(pNewCurr == getCurrent_())
//@ End-Method
//=====================================================================

inline void
AbstractSortC<TYPE>::setCurrent_(DLinkedNodeC<TYPE>* pNewCurr)
{
  CALL_TRACE("setCurrent_(pNewCurr)");
  SAFE_CLASS_PRE_CONDITION((isLegalNode_(pNewCurr)));
  rElemList_.setCurrent_(pNewCurr);
}  // $[TI1]


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isFull()  [const]
//
//@ Interface-Description
//	Is this list full?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
AbstractSortC<TYPE>::isFull(void) const
{
  CALL_TRACE("isFull()");
  return(rElemList_.isFull());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isAtFirst()  [const]
//
//@ Interface-Description
//	Is this list NOT empty, and is the current node also the first
//	node in this list?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
AbstractSortC<TYPE>::isAtFirst(void) const
{
  CALL_TRACE("isAtFirst()");
  return(rElemList_.isAtFirst());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isAtLast()  [const]
//
//@ Interface-Description
//	Is this list NOT empty, and is the current node also the last
//	node in this list?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
AbstractSortC<TYPE>::isAtLast(void) const
{
  CALL_TRACE("isAtLast()");
  return(rElemList_.isAtLast());  // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getNumItems()  [const]
//
//@ Interface-Description
//	Return the number of items that are currently in this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Uint32
AbstractSortC<TYPE>::getNumItems(void) const
{
  CALL_TRACE("getNumItems()");
  return(rElemList_.getNumItems());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getMaxItems()  [const]
//
//@ Interface-Description
//	Return the maximum number of items allowed in this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Uint32
AbstractSortC<TYPE>::getMaxItems(void) const
{
  CALL_TRACE("getMaxItems()");
  return(rElemList_.getMaxItems());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeCurrent()
//
//@ Interface-Description
//	Remove the current node from the list, and return 'SUCCESS'.  If
//	the list is empty, then return 'FAILURE', and the list is left
//	unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the list is not empty, then remove the current node and return
//	'SUCCESS', otherwise return 'FAILURE'.
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline SigmaStatus
AbstractSortC<TYPE>::removeCurrent(void)
{
  CALL_TRACE("removeCurrent()");
  return((!isEmpty()) ? rElemList_.removeCurrent()	// $[TI1]
		      : FAILURE);			// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  clearList()
//
//@ Interface-Description
//	Clear out this list, and leave this list as an empty list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	(isEmpty())
//@ End-Method
//=====================================================================

inline void
AbstractSortC<TYPE>::clearList(void)
{
  CALL_TRACE("clearList()");
  rElemList_.clearList();
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  goFirst()
//
//@ Interface-Description
//	Move the current pointer to the first node in this list, and
//	return 'SUCCESS'.  If the list is empty, then 'FAILURE' is
//	returned, and the list is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	((goFirst() == SUCCESS) ? isAtFirst() : isEmpty())
//@ End-Method
//=====================================================================

inline SigmaStatus
AbstractSortC<TYPE>::goFirst(void)
{
  CALL_TRACE("goFirst()");
  return(rElemList_.goFirst());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  goNext()
//
//@ Interface-Description
//	Move the current pointer to the next node in this list, and
//	return 'SUCCESS'.  If the list is empty or the current node is
//	at the end of the list, then 'FAILURE' is returned, and the list
//	is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	((goNext() == SUCCESS) ? !isAtFirst() : (isEmpty()  ||  isAtLast()))
//@ End-Method
//=====================================================================

inline SigmaStatus
AbstractSortC<TYPE>::goNext(void)
{
  CALL_TRACE("goNext()");
  return(rElemList_.goNext());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  goPrev()
//
//@ Interface-Description
//	Move the current pointer to the previous node in this list, and
//	return 'SUCCESS'.  If the list is empty or the current node is
//	at the front of the list, then 'FAILURE' is returned, and the list
//	is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	((goPrev() == SUCCESS) ? !isAtLast() : (isEmpty()  ||  isAtFirst()))
//@ End-Method
//=====================================================================

inline SigmaStatus
AbstractSortC<TYPE>::goPrev(void)
{
  CALL_TRACE("goPrev()");
  return(rElemList_.goPrev());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  goLast()
//
//@ Interface-Description
//	Move the current pointer to the last node in this list, and
//	return 'SUCCESS'.  If the list is empty, then 'FAILURE' is
//	returned, and the list is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	((goLast() == SUCCESS) ? isAtLast() : isEmpty())
//@ End-Method
//=====================================================================

inline SigmaStatus
AbstractSortC<TYPE>::goLast(void)
{
  CALL_TRACE("goLast()");
  return(rElemList_.goLast());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  currentItem()  [const]
//
//@ Interface-Description
//	Return a constant reference to the current item in this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	(!isEmpty())
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline const TYPE&
AbstractSortC<TYPE>::currentItem(void) const
{
  CALL_TRACE("currentItem() const");
  SAFE_CLASS_PRE_CONDITION((!isEmpty()));  // checked by 'rElemList_'...
  return(rElemList_.currentItem());  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(sortList)  [Assignment Operator]
//
//@ Interface-Description
//	Copy the items from 'sortList' into this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	(*this == sortList  &&  (isEmpty()  ||
//	                         currentItem() == sortList.currentItem()))
//@ End-Method
//=====================================================================

inline void
AbstractSortC<TYPE>::operator=(const AbstractSortC<TYPE>& sortList)
{
  CALL_TRACE("operator=(sortList)");
  rElemList_ = sortList.rElemList_;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator==(sortList)  [const]
//
//@ Interface-Description
//	Is this sorted list equivalent to 'sortList'?  Two sorted lists
//	are equivalent if they have the same number of nodes, and each of
//	the items in one list is equivalent to the corresponding item of
//	the other list.  (NOTE:  Equivalence of two lists does NOT depend
//	on the current nodes of the two lists.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
AbstractSortC<TYPE>::operator==(const AbstractSortC<TYPE>& sortList) const
{
  CALL_TRACE("operator==(sortList)");
  return(rElemList_ == sortList.rElemList_);  // $[TI1] (T) $[TI2] (F)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator!=(sortList)  [const]
//
//@ Interface-Description
//	Is this sorted list NOT equivalent to 'sortList'?  Two sorted lists
//	are equivalent if they have the same number of nodes, and each of
//	the items in one list is equivalent to the corresponding item of
//	the other list.  (NOTE:  Equivalence of two lists does NOT depend
//	on the current nodes of the two lists.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

inline Boolean
AbstractSortC<TYPE>::operator!=(const AbstractSortC<TYPE>& sortList) const
{
  CALL_TRACE("operator!=(sortList)");
  return(rElemList_ != sortList.rElemList_);  // $[TI1] (T) $[TI2] (F)
}
