#ifndef SmDatakey_HH
#define SmDatakey_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SmDatakey - Service Mode Datakey functions
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmDatakey.hhv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  18-Feb-2009    SCR Number: 6480
//  Project:  840S
//  Description:
//		Implemented "set vent head operational hours" test.
//
//  Revision: 003  By: erm    Date: 19-Dec-2002     DR Number: 6028
//  Project:  EMI
//  Description:
//         Added method setMfgDatakeyToProdDatakey() to convert
//         Mfg datakey to Production during service-mode
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 001  By:  gdc    Date: 23-Sep-1997    DR Number: 2513
//       Project:  Sigma (R8027)
//       Description:
//           Initial version to support service/manufacturing test.
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

class SmDatakey
{
  public:
	SmDatakey(void);
	~SmDatakey(void);

	static void SoftFault(const SoftFaultID softFaultID,
	  const Uint32      lineNumber,
	  const char*       pFileName  = NULL, 
	  const char*       pPredicate = NULL);
	
    static const SmDatakey & GetSmDatakey(void);

	void setDatakeyData(void) const;
	void setMfgDatakeyToProdDatakey(void) const;
	void setVentHeadOperationalHours(const char* pParameters) const;

  protected:

  private:
	SmDatakey(const SmDatakey&);	// not implemented...
	void operator=(const SmDatakey&);	// not implemented...

};


#endif // SmDatakey_HH 
