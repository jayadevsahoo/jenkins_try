#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaTidalVolSetting - Apnea Tidal Volume Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the volume (in
//  milliliters) of gas that is to be delivered to the patient during a
//  mandatory, volume-based apnea breath.  This setting only applies to
//  mandatory apnea breaths.  This class inherits from 'BatchBoundedSetting'
//  and provides the specific information needed for representation of apnea
//  tidal volume.  This information includes the interval and range of this
//  setting's values, this setting's response to a Main Control Setting's
//  transition (see 'acceptTransition()'), this batch setting's new-patient
//  value (see 'getNewPatientValue()'), and the contraints and dependent
//  settings of this setting.
//
//  This class provides a static method for calculating apnea tidal volume
//  based on the values of other settings.  This calculation method provides
//  a standard way for all settings (as needed) to calculate an apnea tidal
//  volume value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateConstraints_()' method for updating
//  the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaTidalVolSetting.ccv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By: mnr    Date: 28-Dec-2009     SCR Number: 6437
//  Project:  NEO
//  Description:
//		Resolution transition fixed, lower limit based on Advanced now. 
//	
//  Revision: 014   By: mnr    Date: 23-Nov-2009     SCR Number: 6522
//  Project:  NEO
//  Description:
//		Separate Apnea Tidal vol min for SQUARE FLOW PATTERN, resolution 
//		to tenths from 2mL to 5mL.
//
//  Revision: 013   By: mnr    Date: 27-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Enforce lower bound in calcBasedOnSetting_().
//
//  Revision: 012   By: mnr    Date: 21-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Lower min values based on NeoMode Option status.
//
//  Revision: 011   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//	Various updates to allow lower tidal volume min value based on MAND TYPE.
//
//  Revision: 010   By: gdc    Date: 26-Jan-2009    SCR Number: 6461
//  Project:  840S
//  Description:
//      Implemented #define workaround for compiler bug.
//
//  Revision: 009   By: sah   Date:  16-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  removed apnea minute volume as a dependent setting, now it
//         automatically tracks changes to this setting
//
//  Revision: 008   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//	*  re-wrote 'acceptTransition()' to handle NEONATAL circuit
//	*  modified 'updateConstraints_()' to handle new soft limits
//	*  added 'findSoftMinMaxValue_()' for handling new soft limits
//	*  added new 'getAbsoluteMinValue_()' method to accomodate
//         new, dynamic lower limits
//
//  Revision: 007   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new apnea minute volume setting as a dependent setting
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 006   By: dosman Date: 02 Jun 1998    DR Number: 66
//  Project:  BILEVEL
//  Description:
//	this dcs was composed of two problems:
//	When apnea tidal volume is changing as the primary setting,
//	and jumps beyond it's maximum, based on dependent setting I:E ratio,
//	in computing it's new value it must compute insp time,
//	and calcBasedOnSetting() was rounding insp time in mid-computation
//	in order to put insp time at a legal click boundary for insp time,
//	but since insp time's resolution in VCV is 0.2 instead of 0.1
//	the rounding was significant enough to change apnea tidal volume's
//	value to be less than the maximum that would occur if you stepped up to 
//	the maximum value for apnea tidal volume.  In the latter case, 
//	the maximum value is determined by the turning of the knob, and then
//	that value is used to compute an inspiratory time value, which is then
//	rounded, but does not then have repurcussions back to apnea tidal volume.
//	so I removed the rounding of insp time that goes on in mid-computation of
//	apnea tidal volume.
//
//  Revision: 005   By: dosman Date: 21 May 1998    DR Number: 66
//  Project:  BILEVEL
//  Description:
//	There was a mistake in the formula for apneaTidalVolume that was causing
//	this problem.
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.31.1.0) into Rev "BiLevel" (1.31.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 002   By: sah    Date: 23 Dec 1996    DR Number: 1624
//  Project: Sigma (R8027)
//  Description:
//	Incorporated new PC-to-VC formulas.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaTidalVolSetting.hh"
#include "SettingConstants.hh"
#include "MandTypeValue.hh"
#include "FlowPatternValue.hh"
#include "PatientCctTypeValue.hh"
#include "ApneaInspTimeSetting.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
#include "ApneaRespRateSetting.hh"
#include "SettingContextHandle.hh"
#include "SoftwareOptions.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02099] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
{
	SettingId::APNEA_IE_RATIO,
	SettingId::APNEA_INSP_TIME,
	SettingId::APNEA_EXP_TIME,

	SettingId::NULL_SETTING_ID
};


// $[02095] The setting's range ...
// $[02097] The setting's resolution ...


// use #define values in static const struct to workaround compiler bug
static const BoundedInterval  LAST_NODE_ =
{
	DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS, 
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  NODE3_ =
{
	DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC, // change-point...5.0f
	0.10f,		// resolution...
	TENTHS,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE2_ =
{
	100.0f,		// change-point...
	1.0f,		// resolution...
	ONES,		// precision...
	&::NODE3_
};
static const BoundedInterval  NODE1_ =
{
	400.0f,		// change-point...
	5.0f,		// resolution...
	ONES,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	2500.0f,		// absolute maximum...
	10.0f,		// resolution...
	TENS,		// precision...
	&::NODE1_
};



//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaTidalVolSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the value interval
//  get initialized.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaTidalVolSetting::ApneaTidalVolSetting(void)
: BatchBoundedSetting(SettingId::APNEA_TIDAL_VOLUME,
					  ::ARR_DEPENDENT_SETTING_IDS_,
					  &::INTERVAL_LIST_,
					  APNEA_TIDAL_VOL_MAX_ID,  // maxConstraintId...
					  APNEA_TIDAL_VOL_MIN_ID)  // minConstraintId...
{
	CALL_TRACE("ApneaTidalVolSetting()");

	// register this setting's soft bounds...
	registerSoftBound_(::APNEA_TIDAL_VOL_SOFT_MIN_BASED_IBW_ID, Setting::LOWER);
	registerSoftBound_(::APNEA_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID, Setting::UPPER);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaTidalVolSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaTidalVolSetting::~ApneaTidalVolSetting(void)
{
	CALL_TRACE("~ApneaTidalVolSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01096] -- apnea breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	ApneaTidalVolSetting::getApplicability(
										  const Notification::ChangeQualifier qualifierId
										  ) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pApneaMandType =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

	DiscreteValue  apneaMandTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			apneaMandTypeValue = pApneaMandType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			apneaMandTypeValue = pApneaMandType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	return((apneaMandTypeValue == MandTypeValue::VCV_MAND_TYPE)
		   ? Applicability::CHANGEABLE		// $[TI3]
		   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02096] -- apnea tidal volume's new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	ApneaTidalVolSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	// get the ADJUSTED value of IBW...
	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

	// calculate the new patient value ...
	BoundedValue  newPatient;

	// calculate the IBW-based new-patient value...
	newPatient.value = MAX_VALUE(ABSOLUTE_MIN,					// $[TI1]
								 (IBW_VALUE * SettingConstants::NP_TIDAL_VOL_IBW_SCALE)); // $[TI2]

	// warp the calculated value to a "click" boundary...
	getBoundedRange().warpValue(newPatient);

	return(newPatient);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  This setting is only interested in changes of apnea mandatory type
//  from "PCV" to "VCV".
//---------------------------------------------------------------------
//@ Implementation-Description
//  The analysis that documents the definition of the "regions", and
//  their corresponding formulas, can be found in the Design History
//  File.  The document titled "Summary Analysis of Mandatory Type's
//  PC-to-VC Transition Rules" was submitted to the DHF in December 1996.
//
//  All of the testing of region-occupancy is done in such a way as to
//  take into account the inherent inaccuracies of floating-point numbers.
//  For example, when testing to see if respiratory rate is greater-than
//  or equal-to '(4.64 * IBW)', a small "epsilon" (approximately one-tenth
//  of the resolution) is subtracted to ensure the "equal-to" part of
//  the region is included.
//
//  $[02024]\b\   - transitioning based on circuit type...
//  $[NE02007]\a\ - transitioning from PC to VC (NEONATAL circuit)...
//  $[NE02008]\a\ - transitioning from PC to VC (PEDIATRIC circuit)...
//  $[NE02009]\a\ - transitioning from PC to VC (ADULT circuit)...
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId == SettingId::APNEA_MAND_TYPE  &&
//   newValue  == MandTypeValue::VCV_MAND_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//   None
//@ End-Method
//=====================================================================

void
	ApneaTidalVolSetting::acceptTransition(
										  const SettingId::SettingIdType settingId,
										  const DiscreteValue            newValue,
										  const DiscreteValue            currValue
										  )
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	// apnea tidal volume's only transition is due to an apnea mandatory
	// type change from 'PCV' to 'VCV'...
	AUX_CLASS_PRE_CONDITION((settingId == SettingId::APNEA_MAND_TYPE),
							settingId);
	AUX_CLASS_PRE_CONDITION((newValue  == MandTypeValue::VCV_MAND_TYPE),
							newValue);

	//===================================================================
	// apnea mandatory Type transition from PCV to VCV...
	//===================================================================

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
	const Setting*  pApneaRespRate =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_RESP_RATE);
	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;
	const Real32  APNEA_RESP_RATE_VALUE =
		BoundedValue(pApneaRespRate->getAdjustedValue()).value;

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	const Real32  IBW_BASED_APNEA_TIDAL_VOL_VALUE =
		(SettingConstants::NP_TIDAL_VOL_IBW_SCALE * IBW_VALUE);

	BoundedValue  apneaTidalVolume;

	// set to new-patient based value...
	apneaTidalVolume.value = IBW_BASED_APNEA_TIDAL_VOL_VALUE;

	Real32  apneaMinuteVolumeFactorLimit;

	// the minute volume factor limit is the respiratory rate value, above
	// which a scaled factor is used against tidal volume's new-patient value;
	// the purpose of the minute volume factor is to keep a consistent minute
	// volume with respect to the new-patient values of tidal volume and
	// respiratory rate...
	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			apneaMinuteVolumeFactorLimit = SettingConstants::NP_RESP_RATE_NEO_VALUE;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			apneaMinuteVolumeFactorLimit = SettingConstants::NP_RESP_RATE_PED_VALUE;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			apneaMinuteVolumeFactorLimit = SettingConstants::NP_RESP_RATE_ADULT_VALUE;
			break;
		default :
			// unexpected 'PATIENT_CCT_TYPE_VALUE' value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	};

	if ( APNEA_RESP_RATE_VALUE > apneaMinuteVolumeFactorLimit )
	{	// $[TI4]
		// multiply new-patient based value by an apnea respiratory rate factor...
		apneaTidalVolume.value *=
			(apneaMinuteVolumeFactorLimit / APNEA_RESP_RATE_VALUE);
	}	// $[TI5]

	// temporarily disable all soft bounds so they don't limit the calculated
	// value (this also updates the constraints)...
	overrideAllSoftBoundStates_();

	//-------------------------------------------------------------------
	// NOTE:  up to now only the respiratory rate and IBW formulas have been
	//        explicitly applied, the 'warpValue()' below clips based on this
	//        setting's bounds...
	//-------------------------------------------------------------------

	// check the value to not fall below the lower bound
	const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
	apneaTidalVolume.value = MAX_VALUE( ABSOLUTE_MIN, apneaTidalVolume.value );

	// using this setting's constraints (sans the soft bounds), round/clip
	// calculated value..
	getBoundedRange_().warpValue(apneaTidalVolume);

    if( apneaTidalVolume.value == DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC )
	{
		apneaTidalVolume.precision = ONES;
	}

	// store the transition value into the adjusted context...
	setAdjustedValue(apneaTidalVolume);

	// now that the calculated value has been stored, re-enable all applicable
	// soft bounds...
	updateAllSoftBoundStates_(Setting::NON_CHANGING);

	// activation of Transition Rule MUST be italicized...
	setForcedChangeFlag();
}

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  correctForMinimumTi()
//
//@ Interface-Description
//  The method is called during certain setting transitions to correct the
//	adjusted apnea tidal volume value to ensure its within range based upon the
// 	inspiratory time setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the tidal volume is outside of its allowed range based upon the
//  inspriatory time dependent setting then bring the value to within
//  range.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void ApneaTidalVolSetting::correctForMinimumTi()
{
    ApneaInspTimeSetting* inspTimeSetting = (ApneaInspTimeSetting*)SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

	// If the apnea inspiratory time setting is at its lowest limit, then correct the apnea tidal volume
	// based upon apnea inspiratory time. Under certain transition rule situations, the formula computing
	// the apnea inspiratory time would compute below the lower limit of 0.2 seconds, then brought back
	// to its minimum value of 0.2. When this occurs, the apnea tidal volume must be re-adjusted for
	// the newly adjusted apnea inspiratory time.
	if (IsEquivalent( BoundedValue(inspTimeSetting->getAdjustedValue()).value, inspTimeSetting ->getMinLimit(), HUNDREDTHS))
	{
		// Get the adjusted apnea tidal volume taking into account the apnea inspiratory time setting
		BoundedValue correctedTidalVolume = calcBasedOnSetting_(SettingId::APNEA_INSP_TIME,
				BoundedRange::WARP_UP, BASED_ON_ADJUSTED_VALUES);

		// Set a new adjusted apnea tidal volume that is within range
		setAdjustedValue(correctedTidalVolume);
	}
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	ApneaTidalVolSetting::isAcceptedValid(void) const
{
	CALL_TRACE("isAcceptedValid()");

	Boolean  isValid = BoundedSetting::isAcceptedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ACCEPTED) !=
			 Applicability::INAPPLICABLE )
		{
			// is the the accepted apnea tidal volume value consistent with the
			// accepted apnea inspiratory time?...
			const Setting*  pApneaInspTime =
				SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

			isValid = pApneaInspTime->isAcceptedValid();
		}
	}

	return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	ApneaTidalVolSetting::isAdjustedValid(void)
{
	CALL_TRACE("isAdjustedValid()");

	Boolean  isValid = BoundedSetting::isAdjustedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ADJUSTED) !=
			 Applicability::INAPPLICABLE )
		{
			// is the the adjusted apnea tidal volume value consistent with the
			// adjusted apnea inspiratory time?...
			Setting*  pApneaInspTime =
				SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

			isValid = pApneaInspTime->isAdjustedValid();
		}
	}

	return(isValid);
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ApneaTidalVolSetting::SoftFault(const SoftFaultID  softFaultID,
									const Uint32       lineNumber,
									const char*        pFileName,
									const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							APNEA_TIDAL_VOLUME_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02095]   -- apnea tidal volume's range...
//  $[NE02003] -- apnea tidal volume's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ApneaTidalVolSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	Real32  softMinValue, softMaxValue;

	findSoftMinMaxValues_(softMinValue, softMaxValue);

	//===================================================================
	// calculate apnea tidal volume's most restrictive MAXIMUM bound...
	//===================================================================
	{
		BoundedRange::ConstraintInfo  maxConstraintInfo;

		//-----------------------------------------------------------------
		// determine which IBW-based upper limit to apply...
		//-----------------------------------------------------------------

		BoundedValue  ibwBasedMax;

		if ( isSoftBoundActive_(APNEA_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID) )
		{	// $[TI1] -- this setting's soft maximum bound is currently active...
			// initialize to soft bound maximum bound id...
			ibwBasedMax.value = softMaxValue;

			maxConstraintInfo.id = APNEA_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID;
		}
		else
		{	// $[TI2] -- this setting's soft maximum bound is NOT active...
			// initialize to absolute maximum bound id...
			ibwBasedMax.value = (IBW_VALUE *
								 SettingConstants::MAX_TIDAL_VOL_IBW_SCALE);

			maxConstraintInfo.id = APNEA_TIDAL_VOL_MAX_BASED_IBW_ID;
		}

		//-----------------------------------------------------------------
		// determine which upper limit to apply (absolute or IBW-based?)...
		//-----------------------------------------------------------------

		const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

		if ( ibwBasedMax.value < ABSOLUTE_MAX )
		{	// $[TI3]
			// reset this range's maximum constraint to allow for "warping" of a
			// new maximum value...
			getBoundedRange_().resetMaxConstraint();

			// warp the maximum value to a lower "click" boundary...
			getBoundedRange_().warpValue(ibwBasedMax, BoundedRange::WARP_DOWN);

			// the IBW-based maximum is more restrictive, therefore make it the
			// upper bound value...
			maxConstraintInfo.value = ibwBasedMax.value;
		}
		else
		{	// $[TI4]
			// the absolute maximum is more restrictive, therefore make it the
			// upper bound value...
			maxConstraintInfo.id    = APNEA_TIDAL_VOL_MAX_ID;
			maxConstraintInfo.value = ABSOLUTE_MAX;
		}

		// is set to soft bound?...
		maxConstraintInfo.isSoft =
		// $[TI5] (TRUE)  $[TI6] (FALSE)...
			(maxConstraintInfo.id == APNEA_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID);

		getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
	}


	//===================================================================
	// calculate apnea tidal volume's most restrictive MINIMUM bound...
	//===================================================================
	{
		BoundedRange::ConstraintInfo  minConstraintInfo;

		//-----------------------------------------------------------------
		// determine which IBW-based lower limit to apply...
		//-----------------------------------------------------------------

		BoundedValue  ibwBasedMin;

		if ( isSoftBoundActive_(APNEA_TIDAL_VOL_SOFT_MIN_BASED_IBW_ID) )
		{	// $[TI7] -- this setting's soft minimum bound is currently active...
			// initialize to soft bound minimum bound id...
			ibwBasedMin.value = softMinValue;

			minConstraintInfo.id = APNEA_TIDAL_VOL_SOFT_MIN_BASED_IBW_ID;
		}
		else
		{	// $[TI8] -- this setting's soft bound is NOT active...
			// initialize to absolute minimum bound id...
			ibwBasedMin.value = (IBW_VALUE *
								 SettingConstants::MIN_TIDAL_VOL_IBW_SCALE);

			minConstraintInfo.id = APNEA_TIDAL_VOL_MIN_BASED_IBW_ID;
		}

		//-----------------------------------------------------------------
		// determine which lower limit to apply (absolute or IBW-based?)...
		//-----------------------------------------------------------------

		const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

		if ( ibwBasedMin.value > ABSOLUTE_MIN )
		{	// $[TI9]
			// reset this range's minimum constraint to allow for "warping" of a
			// new minimum value...
			getBoundedRange_().resetMinConstraint();

			// warp the minimum value to a upper "click" boundary...
			getBoundedRange_().warpValue(ibwBasedMin, BoundedRange::WARP_UP);

			// the IBW-based minimum is more restrictive, therefore make it the
			// upper bound value...
			minConstraintInfo.value = ibwBasedMin.value;
		}
		else
		{	// $[TI10]
			// the absolute minimum is more restrictive, therefore make it the
			// upper bound value...
			minConstraintInfo.id    = APNEA_TIDAL_VOL_MIN_ID;
			minConstraintInfo.value = ABSOLUTE_MIN;
		}

		// is set to soft bound?...
		minConstraintInfo.isSoft =
		// $[TI11] (TRUE)  $[TI12] (FALSE)...
			(minConstraintInfo.id == APNEA_TIDAL_VOL_SOFT_MIN_BASED_IBW_ID);

		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NE02003] -- apnea tidal volume's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	ApneaTidalVolSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
												Real32& rSoftMaxValue) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	BoundedValue  boundedValue;

	//-------------------------------------------------------------------
	// determine lower soft bound limit value...
	//-------------------------------------------------------------------

	// calculate lower soft bound limit...
	boundedValue.value =
		(IBW_VALUE * SettingConstants::MIN_TIDAL_VOL_SOFT_IBW_SCALE);

	// warp the calculated value to the upper "click", with no clipping...
	getBoundedRange().warpValue(boundedValue, BoundedRange::WARP_UP, FALSE);

	rSoftMinValue = boundedValue.value;

	//-------------------------------------------------------------------
	// determine upper soft bound limit value...
	//-------------------------------------------------------------------

	// calculate upper soft bound limit...
	boundedValue.value =
		(IBW_VALUE * SettingConstants::MAX_TIDAL_VOL_SOFT_IBW_SCALE);

	// warp the calculated value to the nearest "click", with no clipping...
	getBoundedRange().warpValue(boundedValue, BoundedRange::WARP_DOWN, FALSE);

	rSoftMaxValue = boundedValue.value;
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All new-patient calculations are to be based on the VCV setting values.
//
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//  (basedOnSettingId == SettingId::APNEA_EXP_TIME  ||
//   basedOnSettingId == SettingId::APNEA_IE_RATIO  ||
//   basedOnSettingId == SettingId::APNEA_INSP_TIME)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
	ApneaTidalVolSetting::calcBasedOnSetting_(
											 const SettingId::SettingIdType basedOnSettingId,
											 const BoundedRange::WarpDir    warpDirection,
											 const BasedOnCategory          basedOnCategory
											 ) const
{
	CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

	AUX_CLASS_PRE_CONDITION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
							basedOnCategory);

	BoundedValue  apneaInspTime;

	// determine a value for 'apneaInspTime', based on which timing
	// parameter is forcing this call...
	switch ( basedOnSettingId )
	{
		case SettingId::APNEA_EXP_TIME :
			{	// $[TI1] -- calculate an apnea insp time from apnea exp time...
				const Setting*  pApneaExpTime =
					SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME);

				const Real32  APNEA_EXP_TIME_VALUE =
					BoundedValue(pApneaExpTime->getAdjustedValue()).value;
				const Real32  APNEA_BREATH_PERIOD_VALUE =
					ApneaRespRateSetting::GetApneaBreathPeriod(basedOnCategory);

				apneaInspTime.value = (APNEA_BREATH_PERIOD_VALUE - APNEA_EXP_TIME_VALUE);
			}
			break;
		case SettingId::APNEA_IE_RATIO :
			{	// $[TI2] -- calculate an apnea insp time from apnea I:E ratio...
				const Setting*  pApneaIeRatio =
					SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO);

				const Real32  APNEA_IE_RATIO_VALUE =
					BoundedValue(pApneaIeRatio->getAdjustedValue()).value;
				const Real32  APNEA_BREATH_PERIOD_VALUE =
					ApneaRespRateSetting::GetApneaBreathPeriod(basedOnCategory);

				Real32  eRatioValue;
				Real32  iRatioValue;

				if ( APNEA_IE_RATIO_VALUE < 0.0f )
				{	// $[TI2.1]
					// The apnea I:E ratio value is negative, therefore we have 1:xx with
					// "xx" the expiratory portion of the ratio...
					iRatioValue = 1.0f;
					eRatioValue = -(APNEA_IE_RATIO_VALUE);
				}
				else
				{	// $[TI2.2]
					// The apnea I:E ratio value is positive, therefore we have xx:1 with
					// "xx" the inspiratory portion of the ratio...
					iRatioValue = APNEA_IE_RATIO_VALUE;
					eRatioValue = 1.0f;
				}

				// calculate the apnea insp time from the apnea I:E ratio...
				apneaInspTime.value = (iRatioValue * APNEA_BREATH_PERIOD_VALUE) /
									  (iRatioValue + eRatioValue);

				BoundedSetting*  pApneaInspTime =
					SettingsMgr::GetBoundedSettingPtr(SettingId::APNEA_INSP_TIME);

				// because apnea inspiratory time has a "coarse" resolution in VCV,
				// the "raw" apnea insp time value must be rounded to a resolution
				// boundary, before using the value in the calculations, below...
				pApneaInspTime->warpToRange(apneaInspTime, warpDirection, FALSE);
			}
			break;
		case SettingId::APNEA_INSP_TIME :
			{	// $[TI3] -- get the "adjusted" apnea insp time value...
				const Setting*  pApneaInspTime =
					SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

				apneaInspTime.value =
					BoundedValue(pApneaInspTime->getAdjustedValue()).value;
			}
			break;
		default :
			// unexpected 'basedOnSettingId' value...
			AUX_CLASS_ASSERTION_FAILURE(basedOnSettingId);
			break;
	};

	// get pointers to each of the apnea VCV parameters...
	const Setting*  pApneaFlowPattern =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);
	const Setting*  pApneaPeakInspFlow =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_PEAK_INSP_FLOW);
	const Setting*  pApneaPlateauTime =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_PLATEAU_TIME);

	const DiscreteValue  APNEA_FLOW_PATTERN_VALUE =
		pApneaFlowPattern->getAdjustedValue();

	const Real32  APNEA_PEAK_INSP_FLOW_VALUE =
		BoundedValue(pApneaPeakInspFlow->getAdjustedValue()).value;
	const Real32  APNEA_PLATEAU_TIME_VALUE   =
		BoundedValue(pApneaPlateauTime->getAdjustedValue()).value;

	BoundedValue  apneaTidalVolume;

	switch ( APNEA_FLOW_PATTERN_VALUE )
	{
		case FlowPatternValue::SQUARE_FLOW_PATTERN :  // $[TI4] 
			{
				static const Real32  FACTOR_ = 1.0f / (60000.0f * 0.001f);

				// calculate the apnea tidal volume value...
				apneaTidalVolume.value = (APNEA_PEAK_INSP_FLOW_VALUE *
										  (apneaInspTime.value - APNEA_PLATEAU_TIME_VALUE)) * FACTOR_;
			}
			break;

		case FlowPatternValue::RAMP_FLOW_PATTERN :	  // $[TI5]
			{
				static const Real32  FACTOR_ = 1.0f / (60000.0f * 0.001f * 2.0f);

				const Setting*  pApneaMinInspFlow =
					SettingsMgr::GetSettingPtr(SettingId::APNEA_MIN_INSP_FLOW);

				const Real32  APNEA_MIN_INSP_FLOW_VALUE =
					BoundedValue(pApneaMinInspFlow->getAdjustedValue()).value;

				// calculate the apnea tidal volume value...
				apneaTidalVolume.value =
					((APNEA_PEAK_INSP_FLOW_VALUE + APNEA_MIN_INSP_FLOW_VALUE) *
					 (apneaInspTime.value - APNEA_PLATEAU_TIME_VALUE)) * FACTOR_;
			}
			break;

		default :
			AUX_CLASS_ASSERTION_FAILURE(APNEA_FLOW_PATTERN_VALUE);
			break;
	};

	const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
	// $[02095] Enforce lower bound
	apneaTidalVolume.value = MAX_VALUE(ABSOLUTE_MIN, apneaTidalVolume.value);	

	// place apnea tidal volume on a "click" boundary...
	getBoundedRange().warpValue(apneaTidalVolume, warpDirection, FALSE);

	if( apneaTidalVolume.value == DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC )
	{
		apneaTidalVolume.precision = ONES;
	}

	return(apneaTidalVolume);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Return apnea tidal volume's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic minimum constraint.
//
//  $[02095] -- apnea tidal volume's range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	ApneaTidalVolSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	const DiscreteValue  MAND_TYPE_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
											  SettingId::APNEA_MAND_TYPE);

    const Setting*  P_FLOW_PATTERN =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);

	const DiscreteValue  FLOW_PATTERN_VALUE = P_FLOW_PATTERN->getAdjustedValue();

	AdjustedContext&  rAdjustedContext = ContextMgr::GetAdjustedContext();

	Boolean isNeoModeAdvancedEnabled = 
		SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_ADVANCED);

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			if( isNeoModeAdvancedEnabled )
			{
				// return the lower bound based on MAND TYPE
				if( MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE )
				{
					// SCR 6522 : Use different min for Square Flow Pattern
					if(	!rAdjustedContext.isInNewPatientSetupMode() 
						&& ( FLOW_PATTERN_VALUE == FlowPatternValue::SQUARE_FLOW_PATTERN )
				      )
					{
						absoluteMinValue = 
							SettingConstants::TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE;
					}
					else { // [NE02000]
						absoluteMinValue = SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS;
					}
				}
				else // [NE02000]
				{
					absoluteMinValue = SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_NON_VC_PLUS;	
				}
			}
			else
			{
				// [NV02000] Apnea Tidal vol shall be 5mL if Advanced NeoMode is NOT present
				absoluteMinValue = SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_MISC;
			}
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2]
			absoluteMinValue = SettingConstants::TIDAL_VOL_STANDARD_MIN_VALUE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMinValue);
}
