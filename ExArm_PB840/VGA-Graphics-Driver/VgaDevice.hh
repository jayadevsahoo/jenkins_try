
#ifndef VgaDevice_HH
#define VgaDevice_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VgaDevice - Interface class for VGA device functions.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaDevice.hhv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 003   By: gdc    Date:  17-Jan-2001    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001   By: sah    Date:  01-Aug-1997    DCS Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "VgaGraphicsDriver.hh"
#include "MemoryMap.hh"
#include "VgaDisplayId.hh"

//@ Usage-Classes
//@ End-Usage


class VgaDevice 
{
  public:
    static void  Initialize(void);

    static void  SetContrast  (const Uint8 lowerScreenContrast,
    			               const Uint8 upperScreenContrast);

    static void  SetBrightness(const Int32 percentLowerBrightness,
                               const Int32 percentUpperBrightness);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

};


#endif // VgaDevice_HH 
