//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file psstream.h
///
/// This file contains a string stream typedefs that do not use any dynamic
/// storage allocation. Fixed block allocators are used to eliminate dynamic
/// storage allocation heap fragmentation issues.
///
/// Unfortunately, the Dinkumare std::stringstream implementation uses the
/// heap for its locale support, even when using our fixed block allocator
/// template argument. Therefore, the cov_stringstream class was created
/// to have the exact same interface as std::stringstream yet it wraps
/// the sprintf/wsprintf CRT API to eliminate dynamic storage allocation.
///
/// The user of this class is responsible to ensure the STL container never
///	needs more than the fixed size provided by the allocator. Otherwise a
///	SoftFault is triggered.
///
/// Example usage:
///
///	postringstream os;
///	os << 1.23 << " My Data";
//------------------------------------------------------------------------------
#ifndef PSSTREAM_H
#define PSSTREAM_H

//------------------------------------------------------------------------------
// I N C L U D E S
//------------------------------------------------------------------------------
#include <sstream>
#include "multipool_allocator.hh"

////////////////////////////////////////////////////////////////////////////////////////
// Viking's comments
// The Dinkumware basic_stringstream uses the heap, even if we pass in our own allocator.
// Therefore, we use our cov_stringstream that wraps the sprintf/swprintf.
// 
// PB880 comments
// Now we are using std library from Microsoft, the basic stringstream with allocator
// should work.
////////////////////////////////////////////////////////////////////////////////////////

typedef std::basic_stringstream<char, std::char_traits<char>, multipool_allocator<char> > pstringstream;
typedef std::basic_ostringstream<char, std::char_traits<char>, multipool_allocator<char> > postringstream;

typedef std::basic_stringstream<wchar_t, std::char_traits<wchar_t>, multipool_allocator<wchar_t> > pwstringstream;
typedef std::basic_ostringstream<wchar_t, std::char_traits<wchar_t>, multipool_allocator<wchar_t> > pwostringstream;

#endif // PSSTREAM_H

