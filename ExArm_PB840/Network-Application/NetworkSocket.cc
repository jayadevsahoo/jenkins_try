#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NetworkSocket -  Provides interface to the standard Network
//    Socket functionality, a parent class of all the socket classes.
//---------------------------------------------------------------------

//@ Interface-Description
//    The NetworkSocket class encapsulates a socket oriented network
//    communcation. This class provides most common functionalities
//    of sending and receiving network data on a socket. Most of the
//    methods are virtual so child classes can inherit or override
//    these methods to handle different types of socket communcation.
//
//    The class provides a sequence number for each network message
//      sent. A packet sequence number and the network message
//    id on a socket uniquely identifies a network message. These two
//    information can be used to track down a message on a specific
//    socket connection.
//
//    The NetworkSocket class provides the sendMsg and recvMsg virtual
//    methods to send and receive a complete network message. Each 
//    message is prepended with a network message header.
//    The class utilizes a send and receive mutex locking mechanism
//    to guarantee a complete network message is transmitted on a
//    given socket before a new network message transmission request.
//
//    A network message header is a fixed structure and contains
//    a network message id, data size and a packet sequence number. 
//
//    The class also provides the sendData and readData virtual methods
//    which send and receives a given number of bytes of data on the
//    current socket. An application code can interface with these
//    methods directly if it wants to process a network message header
//    information and its data. The lowest level data transmission
//    methods are the 'receive' and 'transmit' virtual methods which
//    directly moves data back and forth into the TCP/IP Stack-Ware
//    buffer.  All the repeated socket library errors are logged to
//    the Non-Volatile diagnostic memory area.
//    The reInit method reinitializes socket attributes after
//    a socket is opened.
//
//---------------------------------------------------------------------
//@ Rationale
//    
//    This class captures most of the common behaviour of all the
//    socket communication so to maximize the code sharing amongst
//    all different types of socket communication necessary for the
//    Sigma operation. All the communcation methods are modularized
//    finely so subclasses can override only the portion of communication
//    that it differs from.
//---------------------------------------------------------------------
//@ Implementation-Description
//    A network socket is created as a non-blocking IO mode as default.
//    An open socket id is passed to reInit method to be initialized
//    into the NetworkSocket object. When a socket call returns an error
//    status within the sendData and recvData methods, the 'send' and
//    'recv' operations are repeated up to four times just in the case
//    that the error condition is gone.
//    
//@ Fault-Handling
//  Some of the repeated socket library errors are recorded to
//  the non-volatile diagnostic memory. All the failed requests
//  return an error status to calling methods. Each method follows
//  the contract programming practice to generate a soft fault in
//  the case of software logic error. 
//---------------------------------------------------------------------
//@ Restrictions
//   One static MutEx object that is used for synchronizing a packet
//   serial number should have been instance variable,
//   so each socket instance has its own  MutEx object instead 
//   of sharing with all other NetworkSocket instances.
//   But mutex ids are predefined in Sigma and it is
//   hard to dynamically allocate a unique mutex for each instance.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/NetworkSocket.ccv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: gdc     Date: 30-Dec-2010  SCR Number: 6714
//  Project:  XENA2
//  Description:
//      Changed so an out of mbufs condition brings down communications
//      allowing stack to free all mbufs and restart communications.
//
//  Revision: 007  By: gdc     Date: 30-Dec-2010  SCR Number: 6722
//  Project:  XENA2
//  Description:
//      Generate message sequence numbers only for debug builds.
//
//  Revision: 006  By: gdc     Date: 30-Dec-2010  SCR Number: 6697
//  Project:  XENA2
//  Description:
//      Removed code to broadcast BD IP address as this logic is no 
//      longer used and consumes all mbufs causing a BD reset
//      during communication loss with the GUI.
//
//  Revision: 005  By: gdc     Date: 30-Dec-2010  SCR Number: 6717
//  Project:  XENA2
//  Description:
//      Corrected creation and initialization of all socket types.
//
//  Revision: 004  By: gdc     Date: 30-Dec-2010  SCR Number: 6716
//  Project:  XENA2
//  Description:
//      Changed to use thread-safe socket library.
//
//  Revision: 003  By: gdc     Date: 30-Dec-2010  SCR Number: 6712
//  Project:  XENA2
//  Description:
//      Use socket sendmsg() instead of send() to eliminate copy 
//      operation.
//
//  Revision: 002  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  13-Sep-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "TaskMonitor.hh"
#include "CommFaultId.hh"
#include "CriticalSection.hh"
#include "Ipc.hh"
#include "IpcIds.hh"

#include "Task.hh"
#include "NetworkSocket.hh"
//TODO E600 VM: NetworkSocket should not #include NetworkApp. This should be removed
//after all the circular dependencies are removed (remaining is ReportCommError() and
// static vars ThisBoardAliveMsg, OtherBoardAliveMsg)
#include "NetworkApp.hh"
#include "NetworkMsgHeader.hh"
#include "OsTimeStamp.hh"
#include "EventData.hh"


extern "C" Uint32 get_my_ip_addr();

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NetworkSocket(void)  [Default Constructor]
//
//@ Interface-Description
//    The socket is initially set to a closed state.
//---------------------------------------------------------------------
//@ Implementation-Description
//    SocketId_ instance variable is set to -1 to indicate the
//    socket is closed.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

NetworkSocket::NetworkSocket(void)
{
    socketId_ = -1;    
    setLostMsgFlag_(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reInit(Int sockId)
//
//@ Interface-Description
//    Initializes the socket id with a passed in socket id. The passed
//    socket id should a non-zero which makes the socket open.
//    Reinitialize the packet sequence number to zero. 
//---------------------------------------------------------------------
//@ Implementation-Description
//   The socket option is set to maintain the TCP/IP level keep
//   alive protocol on the socket link.
//---------------------------------------------------------------------
//@ PreCondition
//	(sockId > 0)
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
NetworkSocket::reInit(Int sockId)
{
    CLASS_PRE_CONDITION(sockId > 0);
    socketId_ = sockId;
    sequenceNum_ = 0;
	maxMsgRecvTime_ = MAX_INPUT_WAIT_DURATION;
    setLostMsgFlag_(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NetworkSocket()  [Destructor]
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Close the socket if it is opened()
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

NetworkSocket::~NetworkSocket(void)
{
    CALL_TRACE("~NetworkSocket()");
    if ( opened() )
    {
        down(); 
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:: sendMsg( XmitDataMsgId msgId, char *buf, Uint nbytes)
//
//@ Interface-Description
//    This method sends a requested network message on this socket.
//    The message data is in the 'buf' memory area. The 'nbytes' is 
//    a message data size not including a message header.
//    If a given 'nbytes' value is zero, only the message header
//    portion is sent out to the network without any accompanying data.
//    The method returns a number of data bytes that are
//    successfully transmitted. The method returns -1 value when there
//    is an error. When the socket is not open, the method doesn't
//    transmit any data and returns -1.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    The method copies a network message header and copies user
//    data into a local XmitBuf structure on a calling task's stack.
//    And it calls one socket library call to pass the data to the TCP/IP
//    code. The TCP/IP code ensures that the data given to
//    one call is transmitted as a contiguous data stream.
//--------------------------------------------------------------------- 
//@ PreCondition
//   (nbytes <= MAX_NETWORK_BUF_SIZE)
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

Int
NetworkSocket::sendMsg( XmitDataMsgId msgId, char *buf, Uint nbytes)
{
	//TODO E600 VM: create wrappers for socket calls and port the wrappers
	//instead of the #ifdef here
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)
    CLASS_PRE_CONDITION( nbytes <= MAX_NETWORK_BUF_SIZE);
	const int MAX_SEND_RETRIES = 6;
    Int count = 0;
	Int bytesToSend = 0;
	int retry = MAX_SEND_RETRIES;
    if ( !opened() )
    {
        return -1; 
    }

	//construct a network message in a buffer XmitBuf that belongs to the thread stack:
	// - Create a header and prefix the data with it. The header is an app layer thing to
	// identify the nature of the data (the message). The total is the data stream to the
	// the TCP layer which takes care of segmenting the data (data being everything in
	// the buffer) if its size is bigger than the Max Transmission Unit (MTU) and copy all
	// The buffer has to be into the thread stack to insure not loosing the data or mixing
	// it with packets from other thread send..
	XmitBuf sendBuf;
	//this sequence number is not used (dead code).. kept as-is from 840 for compatibility
    Uint32 seqNumber = getNextSequenceNum();
	sendBuf.setHeader((Uint16)msgId, (Uint16)nbytes, seqNumber);
	//copy data portion. The endianness of data was already converted hton 
	//by the caller app
    if ( nbytes && buf)
    {
		memcpy((void*)sendBuf.data, (const void *)buf,  (size_t) nbytes);
    }
	//total send bytes: msg header + msg data
	bytesToSend = nbytes + sizeof(sendBuf.header);

	//change the header's endianness to network. Change only the header because
	//data was done by the caller which knows what types it's sending..
	sendBuf.headerHtoN();

	//send() returns: 
	// - number of sent bytes if it sent any
	// - SOCKET_ERROR if failed
	while( (count <= 0) && (retry-- > 0) )
	{
		count = send(socketId_, (const char*)&sendBuf, bytesToSend, 0);   // no flags used
		if (count == SOCKET_ERROR)
		{
			int error = WSAGetLastError();
			if( (error == WSAEWOULDBLOCK) || (error == WSAEINPROGRESS) )
			{
				//make sure total send retries + sleep is less than task monitor period;
				//otherwise, report to task monitor
				Task::Delay(0, 50);
			}
			else
			{
				setLostMsgFlag_(TRUE);
				NetworkApp::ReportCommError("NetworkSocket:sendMsg, send()", error);
				return -1;	//fatal error, end this loop
			}
		}
		//else count > 0 meaning send() sent some bytes, loop ends
	}
    
	if ( count !=  bytesToSend )
    {
        setLostMsgFlag_(TRUE);
		NetworkApp::ReportCommError("NetworkSocket::sendMsg, send() bytes:", count);
        return -1;
    }

	//reaching here means everything went well and total of requested nbytes was sent
	//(only data bytes send requested by the caller, header size does not count)
    return nbytes; 
#else
	return -1;
#endif//WinCE || WIN32
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:: recvMsg( XmitDataMsgId msgId, char *buf, Uint maxBytes)
//
//@ Interface-Description
//    The method receives a given network message to a user provided
//    data area. The message header information is not put into the
//    user data area. If the method doesn't receive a specified
//    network message, it generates an assertion. If a received
//    network message size is larger than a specified maximum
//    bytes, the method returns -1. All other type of communication
//    errors are detected by the 'readData' method and an error message
//    will be logged to the non-volatile diagnostic memory. The method
//    returns -1 when there is an communication error or the socket
//    is not open.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//   (maxBytes <= MAX_NETWORK_BUF_SIZE)
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int 
NetworkSocket::recvMsg( XmitDataMsgId msgId, char *pData, 
                        Uint32 maxBytes ) 
{
    CLASS_PRE_CONDITION(maxBytes <= MAX_NETWORK_BUF_SIZE);

    NetworkMsgHeader header;

    if (!opened() )
    {
        return -1; 
    }

    if ( readData((char *) &header, sizeof(NetworkMsgHeader)) 
         == sizeof( NetworkMsgHeader))
    {
		header.convNtoH();	//convert the header endiannes
		//if data size indicated inheader is more than what we expect,
		//then the message is worng, go to return -1
        if ( header.pktSize <=  maxBytes)
        {
			//do this check ONLY when there is data bytes in the message! because it's OK
			//to call recevMsg with pData = NULL if the received message is a header only
			CLASS_ASSERTION(pData != NULL)
			//Note: data endianness is fixed by the caller because it knows its data types
            if (header.pktSize && ( readData(pData, header.pktSize) 
                                    != header.pktSize ))
            {
                return -1;   
            }
			AUX_CLASS_ASSERTION(header.msgId == msgId, header.msgId);
            return header.pktSize; 
        }
    }

    return -1;   
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  readData( char *buf, int nbytes)
//
//@ Interface-Description
// The readData method receives 'nbytes' of network data
// from this socket into the 'buf' memory area. The method returns
// a byte count of the received data. If any communication error,
// the method returns -1. Otherwise, the method returns nbytes.
// All the socket library call errors
// are examined and logged internally.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
// All the sockets are implemented as a non-blocking IO socket
// to avoid a Task getting permanently suspended at the lower
// level TCP/IP code when the physical ethernet cable gets 
// disconnected.
// For a non-block  mode socket, the receive method reads only
// those data bytes that are already accumulated in the socket
// buffer. 
//
// If there is more data to read than what is on the socket during
// the recv() call, this method polls the socket for more data using
// ioctl() and delays 100 milliseconds between polling attempts to 
// allow the Stackware task time to read more network data into the 
// socket buffer.
//
// The polling ioctl() method is preferred to using "select for
// read" as ioctl can run outside the network critical section to read
// the socket. ioctl also uses fewer CPU cycles than select(). During
// development, we determined that select for read used so much of
// the CPU resources when the network was jammed that lower priority 
// tasks did not have a chance to run would cause more task monitor resets.
// This method is also preferred to calling recv() in a loop since recv()
// requires the same network critical section lock (elevating the calling
// task priority to network high) and consumes even more processing time 
// than the select() function.
//--------------------------------------------------------------------- 
//@ PreCondition
//         ( buf && nbytes )
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
NetworkSocket::readData(char *buf, Uint nbytes)
{
	//TODO E600 VM: create wrappers for socket calls and port th ewrappers instead
	//of the #ifdef here
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)

    CLASS_PRE_CONDITION( buf && nbytes );
    if (!opened())
    {
        return -1; 
    }

    OsTimeStamp waittime;
	Uint networkapp_recv_timeout;
    int nextbytes = nbytes;
    int error;

    while ( nextbytes > 0 )
    {
		//This is a time check for the while(nextbytes>0) to avoid stucking
		networkapp_recv_timeout = waittime.getPassedTime();
        if ( networkapp_recv_timeout >= maxMsgRecvTime_ )
        {
            NetworkApp::ReportCommError("networksocket:readdata ioctl() timeout", 
                                        networkapp_recv_timeout);
            return -1; 
        }

        int count;
        count = recv(socketId_, buf, nextbytes, 0);
		//received some bytes.. copy them and continue to poll id socket has more using ioctl
		//This is an 840 method that was ported as-is since it worked fine for it..
        if ( count > 0 )
        {
            nextbytes -= count;
            buf = (char *)buf + count;
        }
		//failed to receive: either a gracefully closed socket (count = 0) or count = SOCKET_ERROR
		//If socket error because of blocking or in progress, this is not a fatal error, give it time..
        else
        {
			error = WSAGetLastError();
			if( (error != WSAEWOULDBLOCK) && (error != WSAEINPROGRESS) )
			{
				NetworkApp::ReportCommError( "networksocket::readdata recv()", error);
				return -1;
			}
			//else WSAEWOULDBLOCK is nonfatal error (recv is busy or data not ready, proceed/retry normally)
        }

		//TODO E600, VM: this is an 840 design kept as-is since it worked fine for it, redesign if needed
        if ( nextbytes > 0 )
        {
            // we still have more data to read. poll for more data on the socket 
            // using ioctl(fionread). see implementation description above.
            ULONG bytesready = 0;
            int taskreport = 0;
            do
            {
                if ( ioctlsocket(socketId_, FIONREAD, &bytesready) == SOCKET_ERROR )
                {
					error = WSAGetLastError();
					if(error != WSAEINPROGRESS)
					{
						NetworkApp::ReportCommError("networksocket::readdata ioctl()", error);
						return -1;
					}
                }
                if ( bytesready == 0 )
                {
					//This is a time check for the do/while(bytesready==0) to avoid stucking
					networkapp_recv_timeout = waittime.getPassedTime();
                    if ( networkapp_recv_timeout >= maxMsgRecvTime_ )
                    {
                        NetworkApp::ReportCommError("networksocket:readdata ioctl() timeout", 
                                                    networkapp_recv_timeout);
                        return -1; 
                    }
                    
                    if ( taskreport++ % 4 == 0 )
                    {
                        TaskMonitor::Report();
                    }

					Task::Delay(0,100);
                }
            } while(bytesready == 0);
        }
    }
	//Reaching this point means everything went well and requested nbytes were received
    return nbytes;
#else
	return 0;
#endif//WinCE || WIN32
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  down()
//
//@ Interface-Description
//      close the socket.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Call the 'close' socket library call to close the socket.
//      While a socket is closed, socketId_ is set to -1.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
NetworkSocket::down( )
{
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)

    if (!opened())
    {
        return; 
    }
    closesocket(socketId_);
    socketId_ = -1;
#endif//WinCE || WIN32
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifyConnection(XmitDataMsgId, XmitDataMsgId)
//
//@ Interface-Description
//    As long as this socket is opened, return a true value.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method is overriden by child classes if more verification
//    is necessary.
//---------------------------------------------------------------------
//@ PreCondition
//      ( opened() )
//   This socket should have been opened.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//---------------------------------------------------------------------
SigmaStatus
NetworkSocket::verifyConnection(XmitDataMsgId, XmitDataMsgId)
{
    CLASS_PRE_CONDITION(opened());
    return SUCCESS; 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getNextSequenceNum()
//
//@ Interface-Description
//  Increment the packet sequence number and return the sequence
//  number for this network socket.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Increment sequence number under mutex to allow for multiple threads.
//  Only generate sequence numbers for debugging. Otherwise, return
//  zero.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

Uint32
NetworkSocket::getNextSequenceNum(void)
{
#ifdef SIGMA_DEBUG
    CriticalSection mutex(NETAPP_SENDMSG_ACCESS_MT);
    return ++sequenceNum_;
#else
    return 0;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: openTcpServer
//
//@ Interface-Description
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Int 
NetworkSocket::openTcpServer(PortId portId, Int numClients)
{
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)

    PRINTF(("NetworkSocket::openTcpServer: portid = %d, numClients = %d, ", portId, numClients));

	//TODO E600, VM: the third ARG (int protocol ) was kept (0) as-is from 840. Since we know
	//               it is a TCP socket, why not use IPPROTO_TCP ?
	SOCKET newSocket = socket(AF_INET, SOCK_STREAM, 0);
    if ( newSocket == INVALID_SOCKET )
    {
		//TODO E600 VM: another cause of circular dependency. Sockets should not be reporting
		//     communication errors.. this should be on the app level. remove all calls to
		//     ReportCommError from all sockets.
        NetworkApp::ReportCommError("NetSocket::openTcpServer() socket()", WSAGetLastError());
        return -1;
    }
    PRINTF(("socketId = %d\n", newSocket));

    // Allow other sockets to bind() to this port, unless here is an active listening
    // socket bound to this port already. Avoids "address already in use" when restarting
    // the socket.
    int reuseon=1;
    if ( setsockopt(newSocket, SOL_SOCKET, SO_REUSEADDR,(char *)&reuseon, sizeof(reuseon)) != 0 )
    {
        NetworkApp::ReportCommError("NetSocket::openTcpServer setsockopt(REUSEADDR)", WSAGetLastError());
		closesocket(newSocket);
        return -1;
    }

    sockaddr_in  servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(portId);
    servaddr.sin_family      = AF_INET;
    if ( bind(newSocket, (sockaddr *)&servaddr, sizeof(servaddr)) != 0 )
    {
        NetworkApp::ReportCommError("NetworkSocket::openTcpServer bind()", WSAGetLastError());
		closesocket(newSocket);
        return -1;
    }

    if ( listen(newSocket, numClients ) != 0 )
    {
        NetworkApp::ReportCommError( "NetworkSocket::openTcpServer listen()", WSAGetLastError());
		closesocket(newSocket);
        return -1;
    }

    reInit(newSocket);

    return newSocket;
#else
	return -1;
#endif//WinCE || WIN32
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptConnection( NetworkSocket &rSocket, Int32  waitTime)
//
//@ Interface-Description
// Accepts a socket connection from the GUI board. 
// After verifying that the new socket connection can send
// and receive data, the new socket information is initialized 
// into the "rSocket" object. 
//--------------------------------------------------------------------- 
//@ Implementation-Description 
// This method is called only on the BD board to make a TCP/IP
// socket connection between the BD and GUI.
// The contactSocket in NetworkApp::OpenConnections is the socket 
// used by the GUI board to make a connection to the BD. When the 
// connection is accepted, a new socket is created for the connection 
// itself.
// This method uses the select() function to wait for readability on
// the contact socket. If the select succeeds, this method calls the
// accept() function to create a new socket that is set to non-blocking
// and assigned to the referenced NetworkSocket parameter.
//--------------------------------------------------------------------- 
//@ PreCondition
//  ( rSocket.opened() == FALSE )
//---------------------------------------------------------------------
//@ PostCondition 
//        none
//@ End-Method 
//===================================================================== 

Int 
NetworkSocket::acceptConnection( NetworkSocket &rSocket, Uint32 waitTime )
{
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)
	CLASS_PRE_CONDITION( rSocket.opened() == FALSE );
    Int newSocket;

    PRINTF(("NetworkSocket::acceptConnection socketId_ = %d\n", socketId_));

    fd_set readMask;
    FD_ZERO(&readMask);
    FD_SET(socketId_, &readMask);
	//TODO E600 VM: called waitForInput() instead of repeating the same code here.
	//  and for now keeping the nfds arg param but it is not really used.. 
	if(WaitForInput(NULL, &readMask, waitTime) <= 0)
	{
		return -1;
	}
	// else we got some active socket handles..
	//accept connection on the new socket (we do not need the returned buffer adress)
    if ( (newSocket = accept(socketId_, (struct sockaddr *)0, 0)) == INVALID_SOCKET )
    {
        NetworkApp::ReportCommError("NetworkSocket::acceptConnection:accept()", WSAGetLastError());
        return -1;
    }

    ULONG nonblockingon = 1;
    if ( ioctlsocket(newSocket, FIONBIO, &nonblockingon) != 0 )
    {
        NetworkApp::ReportCommError("NetworkSocket::acceptConnection: ioctlsocket(FIONBIO)", WSAGetLastError());
        closesocket(newSocket);
        return -1; 
    }

    // assign the new socket to the reference socket and (re)initialize
    rSocket.reInit(newSocket);

    TaskMonitor::Report();
    if ( rSocket.verifyConnection(NetworkApp::ThisBoardAliveMsg,
                                  NetworkApp::OtherBoardAliveMsg ) == FAILURE )
    {
        PRINTF(("NetworkSocket::acceptConnection verifyConnection failed.\n"));
        rSocket.down();
        return -1;
    }

    return newSocket;
#else
	return -1;
#endif//WinCE || WIN32
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: openTcpClient
//
//@ Interface-Description
//   This method is called from the GUI board to establish a socket
//   connection to the BD board as a client. The calling routine 
//   provides a BD board's internet address and port number.
//   After a socket connection is made, tests whether the socket can be
//   used to transmit and receive data.
//--------------------------------------------------------------------
//@ Implementation-Description
//  Need to REVISE - TBD
//    The call to the connect socket library routine is repeated
//    up to 12 times after pausing a given wait duration.
//    This is to provide enough time for the other board to come to
//    the socket connection phase. The socket mode is modified to be
//    a non-blocking while connecting to the other board.
//    The occassional long delay in the blocked mode socket connect
//    was noticed during the development.  In this way, the connect
//    call returns before a complete TCP/IP socket connection is
//    finished and the higher level code and check the statusi
//    periodically.  In this way, the code has more control over
//    when to stop the connection process if there is too long of 
//    delay in connecting to the other board.
//
//    After a socket connection is made, the socket mode is changed
//    back to the blocking mode.
//
//   
//---------------------------------------------------------------------
//@ PreCondition
//         (opened() == FALSE )
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Int
NetworkSocket::openTcpClient(Uint32 ipAddr, PortId portId)
{
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)

    CLASS_PRE_CONDITION(opened() == FALSE);
    int error;

    PRINTF(("openTcpClient: ipAddr = %08x, port = %d, ", ipAddr, portId));

    sockaddr_in  servaddr;  // server address
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;   
    servaddr.sin_addr.s_addr = htonl(ipAddr);
    servaddr.sin_port        = htons(portId);

	//TODO E600, VM: the third ARG (int protocol ) was kept (0) as-is from 840. Since we know
	//               it is a TCP socket, why not use IPPROTO_TCP ?
	SOCKET newSocket = socket(AF_INET, SOCK_STREAM, 0);
    if ( newSocket == INVALID_SOCKET )
    {
		error = WSAGetLastError();
        NetworkApp::ReportCommError("NetSocket::openTcpClient_() socket()", error);
        return -1;
    }
    PRINTF(("socketId = %d\n", newSocket));

	// Set socket IO mode to non-blocking
    ULONG nonblockingon = 1;
    if ( ioctlsocket(newSocket, FIONBIO, &nonblockingon) != 0 )
    {
        NetworkApp::ReportCommError( "NetworkSocket::openTcpClient_(): IOCTL(FIONBIO)", WSAGetLastError());
		closesocket(newSocket);
        return -1;
    }

    // perform non-blocking socket connect using connect and select.
	// Only with blocking socket do we expect "connect" to return (0) for sucessful conncetion; otherwise,
	// failed connection. But for non-blocking socket, "connect" is expected to return "SOCKET_ERROR" with
	// actual error code is WSAEWOULDBLOCK, proceed to "select()"; if not, fail.
    if ( connect(newSocket , (struct sockaddr *)&servaddr, sizeof(servaddr)) == SOCKET_ERROR )
    {
		error = WSAGetLastError();
        if ( (error != WSAEWOULDBLOCK) && (error != WSAEISCONN) && (error != WSAEINPROGRESS) )
        {
            closesocket(newSocket);
            PRINTF(("\nconnect() error = %d\n", error));
            return -1;
        }
	}
     
    // call select() to wait for writeability or timeout or error
    fd_set fdset;
    FD_ZERO(&fdset);
    FD_SET(newSocket, &fdset);
	//Make sure the timeout value is less than TaskMonitor period; otherwise
	//split it in a loop of cycles and report to TaskMonitor every cycle
    timeval timeout;
	timeout.tv_sec = 1;  // 1 second
	timeout.tv_usec = 0;

    int count;
	TaskMonitor::Report();	//because "select" may suspend for timeout value
	//TODO E600, VM: kept this 840 concept as-is for now
    count = select(NULL, NULL, &fdset, NULL, &timeout);

	//if timed out..
    if ( count == 0 )
    {
        closesocket(newSocket);
        PRINTF(("select() timeout\n"));
        return -1;
    }
    else if ( count == SOCKET_ERROR)
    {
        PRINTF( ("select() failed, error = %d\n", WSAGetLastError()) );
		closesocket(newSocket);
        return -1;
    }
    // else successful select

    // get socket options to see if select() resulted in errors. Get the socket-level
	// option for ERROR option
    int sockOpt;//this will contain the returned value of the requested option
    int len = sizeof(sockOpt);
    if ( getsockopt(newSocket, SOL_SOCKET, SO_ERROR, (char*)&sockOpt, &len) != 0 )
    {
		error = WSAGetLastError();	//this error is failure of getsockopt() itself
        closesocket(newSocket);
        PRINTF(("getsockopt() error = %d",error));
        return -1;
    }
	//if there was a socket error indicated by getopt
    if ( sockOpt != 0 )
    {
        closesocket(newSocket);
        PRINTF(("connection failed, sockError = %d\n", sockOpt));
        return -1;
    }

    // set up the timeout periods etc...
    reInit(newSocket);

    if ( verifyConnection(NetworkApp::ThisBoardAliveMsg,
                          NetworkApp::OtherBoardAliveMsg ) == FAILURE )
    {
        PRINTF(("verifyConnection failed.\n"));
        return -1;
    }

    return newSocket;
#else
	return -1;
#endif//WINCE
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setLostMsgFlag_(Boolean flag)
//
//@ Interface-Description
//        Sets a lost message indicator. If a TRUE value is passed in
//      as the flag indicator value,  the lost message indicator is
//      set to indicate that a network message is lost due to either
//      receive or tranmit communication errors. If a FALSE value is
//      passed in, the lost message indicator is cleared.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      Sets the lostMsgFlag_ instance variable.
//      If the lost message indicator is set a true value, the method
//      sets the NetworkApp communication down state.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
NetworkSocket::setLostMsgFlag_(Boolean value)
{
    if ( value == TRUE )
    {
		//TODO E600 VM: another cause of circular dependency. NetworkSocket
		//  should not be calling NetworkApp; particularly, should not be setting
		//  something for NetworkApp on its behalf. Instead, use a callback
		//  or set a certain status here and let NetworkApp::IsCommunicationUp()
		//  poll it and combine it in its logic to determine if network is down.
        NetworkApp::SetCommDown_();    
    }
    lostMsgFlag_  = value; 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  anyLostMsg()
//
//@ Interface-Description
//      Determines if this socket has lost a network message. A network
//      message is considered lost when a network message cannot be
//      received or transmitted to the repeated communication errors.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Boolean
NetworkSocket::anyLostMsg() const
{
    return lostMsgFlag_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  WaitForInput( const fd_set *readMask, const Uint waitTime)
//						[static]
//
//@ Interface-Description
//    This method is called to wait for either incoming network 
//    data  or a connection attempt on a given socket. nfds specifies
//    the maximum socket descriptor number plus one.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    A passed in socket is an blocked I/O socket, therefore the
//    'select'  network call will not return until either some input
//    comes in on the specified sockets or a given time duration passes. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      pReadMask
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
NetworkSocket::WaitForInput(int nfds, fd_set *pReadMask, Uint32 waitTime)
{
	UNUSED_SYMBOL(nfds);		
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)
    SAFE_CLASS_PRE_CONDITION(pReadMask);

    // select return codes
    // return == 0, timeout
    // return == SOCKET_ERROR, error
    // return > 0, success (number of returned handles)

    int error;
    int nready;
    timeval timeout;

	long secondsInSelectTimeout = waitTime / 1000;
	long millisecondsInSelectTimeout = waitTime % 1000;
	timeout.tv_sec = secondsInSelectTimeout;
	timeout.tv_usec = millisecondsInSelectTimeout * 1000;	//millisecond x 1000 --> microseconds

	//TODO E600 VM: timeout value of select() should be smaller than TaskMonitor period
	//because the task suspends while select() is running. Instead of a big timeout
	//period, repeate the attempts several times and report to TaskMonitor in between.
	TaskMonitor::Report(); //because select() may take time up to "timeout"
	nready = select(NULL, pReadMask, NULL, NULL, &timeout);
	if (nready == SOCKET_ERROR)
	{
		error = WSAGetLastError();
		if(error != WSAEINPROGRESS)
		{
			NetworkApp::ReportCommError("NetworkSocket::WaitForInput select()", error);
			return -1;
		}

		Task::Delay(0, 100);
	}
	else if(nready == 0)	//select() timed out
	{
		// Don't report a communications error on a select timeout. Otherwise,
        // log would fill with connect timeouts if GUI isn't communicating.
        PRINTF(("NetworkSocket::WaitForInput select() timeout"));
		return -1;
	}
	//else nready > 0 do not delay, loop will end because select() found valid socket handles
	return nready;
#else
	return -1; //same error code that we return when timed out
#endif
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
NetworkSocket::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, NETWORK_APPLICATION, NETWORK_SOCKET_CLASS,
                            lineNumber, pFileName, pPredicate);
}
