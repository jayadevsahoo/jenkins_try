
#ifndef CrcUtilities_HH
#define CrcUtilities_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename:  CrcUtilities - Cyclic-Redundancy-Code Utilities.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/CrcUtilities.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

//@ Begin-Free-Declarations
//@ Type:  CrcValue
// The type of the 16-bit CRC values.
typedef Uint16  CrcValue;

extern CrcValue     CalculateCrc(const Byte     arrBytes[],
			         const size_t   sizeOfBlock);

extern SigmaStatus  VerifyCrc   (const Byte     arrBytes[],
			         const size_t   sizeOfBlock,
			         const CrcValue referenceCrc);
//@ End-Free-Declarations


#endif // CrcUtilities_HH 
