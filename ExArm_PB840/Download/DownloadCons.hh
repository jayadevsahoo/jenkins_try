
#ifndef DownloadCons_HH
#define DownloadCons_HH

#include "DownloadTypes.hh"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: DownloadCons - Defines various Down load constant values.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/DownloadCons.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 001  By:  jhv    Date:  01-05-96    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

	enum DownloadConsConstants
	{
#if	defined(SIGMA_SERVER)
		MAX_CONNECTIONS = 2,
#elif defined(SIGMA_CLIENT)
		MAX_CONNECTIONS = 1,
#endif //defined(SIGMA_CLIENT)

#if	defined(SIGMA_SUN)
#if defined(SIGMA_DEBUG)
		CLIENT_START_IPADDR = 0x92d69155,  // 146.214.145.85
#else
		CLIENT_START_IPADDR = 0x92d69150,  // 146.214.145.80
#endif //defined(SIGMA_DEBUG)
#endif //defined(SIGMA_SUN)
		MAX_BUF_SIZE = 4096,
		DATA_BLOCK_SIZE = MAX_BUF_SIZE,
		MAX_BROADCAST_MSG_SIZE = 1000,
		ETHERNET_ADDR_SIZE = 6,

#ifdef SIGMA_UNIT_TEST
		MAX_WAIT_LOOP = 1,
		WAIT_DATA_INTERVAL = 5,			// 5 millisecond( .005 second )
		DOWNLOAD_COMM_RETRY = 2,
		MAX_DATA_RETRY = 2,
		MAX_DATA_WAIT_INTERVAL = 1000,	// 1 second in  millisecond
		MAX_FLASH_BURN_TIME  = 10,		// 10 seconds.
		ONE_MINUTE = 6000,				// 6000 ms
#else
		MAX_WAIT_LOOP = 120,
		WAIT_DATA_INTERVAL = 5000,		// 5000 millisecond( 5 second )
		DOWNLOAD_COMM_RETRY = 3,
		MAX_DATA_RETRY = 5,
		MAX_DATA_WAIT_INTERVAL = 20000, // 20 seconds in  millisecond
		MAX_FLASH_BURN_TIME  = 180,		// 3 minutes, 180 seconds.
		ONE_MINUTE = 60000,				// 60000 ms
#endif // SIGMA_UNIT_TEST

		FLASH_MEM_WAIT_INTERVAL = 200,  // 200 millisecond
		CONNECT_WAIT_INTERVAL = 1000, 	// 1000 millisecond
		MAX_FILE_NAME_LENGTH = 200,
		BUFFER_WAIT_INTERVAL = 500,		// 500 ms
		MILLI_SEC_PER_TICK = 10,		// 10 ms per tick
		DATA_BLOCK_WAIT_INTERVAL = 100, // 100 ms.
		ONE_SECOND = 1000,				//  a second, 1000 ms 
		HALF_SECOND = 500,				// a half second
		NETWORK_BUFFER_ENTRIES = 200,
		VENDOR_ETHER_PREFIX = 0x00A06100,
		CONNECT_DELAY_INTERVAL = 1000,
		MAX_INACTIVE_INTERVAL = 12000 		// 2 minutes in ticks.
	};

	enum TargetTypes
	{
		BD_BOARD,
		GUI_BOARD
	};
	
#define BD_ETHER_NUMBER  0xFFFFFF00
#define GUI_ETHER_NUMBER  0xFFFFFE00
#define NETWORK_ID_MASK 0xFFFFFF00

#endif // DownloadCons_HH 
