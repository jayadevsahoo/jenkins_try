#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BdReadyMessage - BD Node Ready Message.
//---------------------------------------------------------------------
//@ Interface-Description
//  The BdReadyMessage is a TaskControlMessage created by BD Task-
//  Control and sent to GUI Task-Control indicating the BD is ready in
//  a specified SigmaState.  The message contains the BD's current
//  SigmaState and other state information.  BD Task-Control sends its
//  "service required" and "service mode requested" states in the
//  message.  It also contains instances of the PostAgent, DataKeyAgent,
//  and ServiceModeAgent classes used to transport
//  subsystem specific information to the GUI.  This message class uses
//  the AppContext callback mechanism to dispatch the message to the
//  receiving task's callback function.
//---------------------------------------------------------------------
//@ Rationale
//  The BdReadyMessage encapsulates state information passed between
//  the BD and GUI processors.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is a derived TaskControlMessage.  The base class new and
//  delete operators override the library operators to allocate memory
//  for the message in the global BufferPool.  The copy constructor is
//  defined so the receiving task can reconstruct the message in its
//  own address space with valid virtual pointers.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdReadyMessage.ccv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "BdReadyMessage.hh"
#include <string.h>

//@ Usage-Classes
#include "AppContext.hh"
#include "NovRamManager.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


#if defined SIGMA_BD_CPU || defined SIGMA_UNIT_TEST || LOCAL_SIM
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdReadyMessage [constructor]
//
//@ Interface-Description
//  The BdReadyMessage constructor uses the state, service-required,
//  and service-mode-requested arguments to intialize the contents of
//  the message.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class TaskControlMessage constructor with the
//  classId for this message.  Initializes its private data members.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdReadyMessage::BdReadyMessage(
    enum SigmaState state,
    Boolean service_required)

    :  TaskControlMessage(Sys_Init::ID_BDREADYMESSAGE)
      ,state_(state)
      ,serviceRequired_(service_required)
      ,postAgent_()
      ,dataKeyAgent_() 
      ,serviceModeAgent_()
      ,configurationAgent_() 
      ,bdSettingsInitialized_(NovRamManager::ArePatientSettingsAvailable())

{
    CALL_TRACE("BdReadyMessage(SigmaState, Boolean)");
    // $[TI1]
}
#endif


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdReadyMessage [copy constructor]
//
//@ Interface-Description
//  Copy constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class TaskControlMessage constructor from the 
//  referenced BdReadyMessage.  Initializes internal
//  data members directly from contents of referenced message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

 
BdReadyMessage::BdReadyMessage(const BdReadyMessage& rMessage) :
     TaskControlMessage(rMessage)
    ,state_(rMessage.state_)
    ,serviceRequired_(rMessage.serviceRequired_)
    ,postAgent_(rMessage.postAgent_)
    ,dataKeyAgent_(rMessage.dataKeyAgent_)
    ,serviceModeAgent_(rMessage.serviceModeAgent_)
    ,configurationAgent_(rMessage.configurationAgent_)
    ,bdSettingsInitialized_(rMessage.bdSettingsInitialized_)
{
    CALL_TRACE("BdReadyMessage(const BdReadyMessage&)");
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdReadyMessage [destructor]
//
//@ Interface-Description
//  BdReadyMessage destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdReadyMessage::~BdReadyMessage()
{
    CALL_TRACE("~BdReadyMessage()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class with its state qualifier.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name
//  and state qualifier.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
BdReadyMessage::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    switch ( state_ )
    {
        case (STATE_INOP):         // $[TI1]
            return "BdReady-Inop";
        case (STATE_ONLINE):       // $[TI2]
            return "BdReady-Online";
        case (STATE_SERVICE):      // $[TI3]
            return "BdReady-Service";
        case (STATE_SST):          // $[TI4]
            return "BdReady-SST";
        case (STATE_START):        // $[TI5]
            return "BdReady-Start";
        case (STATE_TIMEOUT):      // $[TI6]
            return "BdReady-Timeout";
        case (STATE_INIT):         // $[TI7]
            return "BdReady-Init";
        default:                   // $[TI8]
            break;
    };
    return "BdReady-Invalid";
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch
//
//@ Interface-Description
//  The method dispatches this BdReadyMessage to the callback
//  function registered with the specified AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the AppContext dispatch method with a reference to this
//  BdReadyMessage.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BdReadyMessage::dispatch(const AppContext& context) const
{
    CALL_TRACE("dispatch(void)");
    context.dispatch(*this);
    // $[TI1]
}


 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BdReadyMessage::SoftFault(const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_BDREADYMESSAGE, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================


 

