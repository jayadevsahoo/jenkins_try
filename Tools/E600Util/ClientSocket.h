#if !defined(AFX_CLIENTSOCKET_H__7CFF06D0_14C3_427B_92A4_2DD2AF08D14E__INCLUDED_)
#define AFX_CLIENTSOCKET_H__7CFF06D0_14C3_427B_92A4_2DD2AF08D14E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ClientSocket.h : header file
//

#include "Afxsock.h"

//#define TOOL_ETHERNET_PORT_UDP	49151
//#define UI_ETHERNET_PORT_UDP		49152
//#define BD_ETHERNET_PORT_UDP		49153
#define TOOL_ETHERNET_PORT_UDP		50000
#define UI_ETHERNET_PORT_UDP		50002
#define BD_ETHERNET_PORT_UDP		50001

#define DEVICE_ETHERNET_PORT_TCP	50004
#define TOOL_ETHERNET_PORT_TCP		50003

#define UI_IP_ADDRESS				"192.168.0.2"
#define BD_IP_ADDRESS				"192.168.0.3"

#define NETWORK_BYTE_ORDER			1

/////////////////////////////////////////////////////////////////////////////
// CClientSocket command target

class CClientSocket : public CSocket
{
// Attributes
public:

// Operations
public:
	CClientSocket();
	virtual ~CClientSocket();

// Overrides
public:

	CDialog * pDlg;
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClientSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	bool GetRXData(char *pBuff, int *pRXSize);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CClientSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	char RXBuff[1000];
	int nRead;
	char RXBuff2[1000];
	int nRead2;
	int DataRX;
	int NextRX;
	struct sockaddr_in si_other;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIENTSOCKET_H__7CFF06D0_14C3_427B_92A4_2DD2AF08D14E__INCLUDED_)
