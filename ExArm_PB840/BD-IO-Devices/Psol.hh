#ifndef Psol_HH
#define Psol_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Psol - Proportional solenoid class.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Psol.hhv   25.0.4.0   19 Nov 2013 13:54:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 003  By: syw    Date:  25-Jan-1996    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added psolLiftoff to constructor.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Declared updatePsol() method non-inlined.  Removed getCloseLevel()
//			method.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "LinearSensor.hh"
#include "DacPort.h"

//@ End-Usage

class Psol
{
  public:
    enum PsolId {AIR_PSOL=0, O2_PSOL, EXH_PSOL, DESIRED_FLOW, NUM_PSOLS} ;
    Psol( const PsolId id, const Uint16 psolLiftoff) ;
    ~Psol( void) ;
    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL,
						   const char*       pPredicate = NULL) ;

    void updatePsol( DacCounts maCounts) ;
    // TODO E600_LL: to be removed when appropriate
	inline Real32 getCurrent( void) const {return 0;}
    inline DacCounts getPsolCommand( void) const ;
    inline DacCounts getLiftoff( void) const ;
    inline void setLiftoff( const DacCounts liftoff) ;
    inline PsolId getPsolId( void) const ;

  protected:

  private:
    Psol( void) ;                  // Declared but not implemented     
    Psol( const Psol&) ;           // Declared but not implemented     
    void operator=( const Psol&) ; // Declared but not implemented 

    //@ Data-Member: psolId_
    // id of PSOL
    PsolId psolId_ ;

#if defined(SIGMA_BD_CPU)
    //@ Data-Member: rPsolDacPort_
    // PSOL DAC port register.
    DacPort psolDacPort_ ;
#endif

    //@ Data-Member: currPsolCommand_
    // The current PSOL command.
    DacCounts currPsolCommand_ ;

    //@ Data-Member: liftOff_
    // value to acheive liftoff flow
    DacCounts liftoff_ ;
} ;

// Inlined methods
#include "Psol.in"

#endif // Psol_HH
