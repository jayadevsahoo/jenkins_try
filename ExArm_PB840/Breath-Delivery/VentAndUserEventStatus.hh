
#ifndef VentAndUserEventStatus_HH
#define VentAndUserEventStatus_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: VentAndUserEventStatus - sends ventilator and user event status to GUI
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VentAndUserEventStatus.hhv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002 By: sp   Date:   06-Mar-1996    DR Number: DCS NONE 
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  kam    Date:  30-Oct-1995    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial release.
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

#include "EventData.hh"
#include "BdGuiCommSync.hh"

//@ Usage-Classes

//@ End-Usage


class VentAndUserEventStatus {
  public:
    static void SoftFault(const SoftFaultID softFaultID,
				  const Uint32      lineNumber,
				  const char*       pFileName  = NULL, 
				  const char*       pPredicate = NULL);

    static void Initialize (void);
    static void SendStatusToGui(const EventData::EventId id,
                                const EventData::EventStatus status,
                                const EventData::EventPrompt prompt=EventData::NULL_EVENT_PROMPT);
    static void PostEventStatus (const EventData::EventId id,
                                 const EventData::EventStatus status,
                                 const EventData::EventPrompt prompt=EventData::NULL_EVENT_PROMPT);
    static void ResyncEventStatus (void);
    static inline void SetEventStatusCommState (const BdGuiCommSync::CommState state);
  
  protected:

  private:
    // Static class; never constructed or destructed
    VentAndUserEventStatus (void);    // not implemented
    ~VentAndUserEventStatus (void);   // not implemented
    VentAndUserEventStatus(const VentAndUserEventStatus&);  // not implemented...
    void operator=(const VentAndUserEventStatus&);          // not implemented...

    //@ Data-Member: EventStatus_
    // current status of each of the events
    static EventData::EventStatus EventStatus_[NUM_EVENT_IDS];
  
    // Data-Member: EventStatusCommState_
    // Indicates the status of communication of ventilator and
    // user event status with the GUI
    static BdGuiCommSync::CommState EventStatusCommState_;
    
};



// Inlined methods...
#include "VentAndUserEventStatus.in"


#endif // EventStatus_HH 
