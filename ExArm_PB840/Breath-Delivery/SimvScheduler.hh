#ifndef SimvScheduler_HH
#define SimvScheduler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SimvScheduler - the active BreathPhaseScheduler when the 
//  current mode is SIMV.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SimvScheduler.hhv   25.0.4.0   19 Nov 2013 14:00:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 009  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  BiLevel (R8027)
//       Description:
//             Bilevel initial version.
//             Added method startExhalation_().
//
//  Revision: 008  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 007  By:  iv    Date:  04-Dec-1996    DR Number: DCS 1597
//       Project:  Sigma (R8027)
//       Description:
//             Added isRateAdjustmentRequired_ flag to handle rate changes
//             during the simv mandatory intervals.
//
//  Revision: 006  By:  iv     Date:  05-June-1996    DR Number: DCS 713 
//       Project:  Sigma (R8027)
//       Description:
//             Added a member pendingMandatoryInterval_.
//
//  Revision: 005  By:  iv     Date:  07-May-1996    DR Number: DCS 967 
//       Project:  Sigma (R8027)
//       Description:
//             Added NULL_INTERVAL as an additional enum value to SimvInterval enum
//             type.
//
//  Revision: 004  By:  iv     Date:  15-Mar-1996    DR Number: DCS 803 
//       Project:  Sigma (R8027)
//       Description:
//             Add access methods for unit test.
//
//  Revision: 003 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//
//====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "BreathType.hh"
#  include "BreathPhaseScheduler.hh"
#  include "TimerTarget.hh"

//@ Usage-Classes
class Trigger;
class BreathPhase;
class IntervalTimer;
//@ End-Usage

class SimvScheduler : public BreathPhaseScheduler, public TimerTarget
{
  public:
	SimvScheduler(IntervalTimer& rTimer);
	virtual ~SimvScheduler(void);

	enum SimvInterval {NULL_INTERVAL, MANDATORY, SPONTANEOUS};

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	virtual void determineBreathPhase(const BreathTrigger& breathTrigger);
	virtual void relinquishControl(const ModeTrigger& modeTrigger);
	virtual void timeUpHappened(const IntervalTimer& timer);
	virtual void takeControl(const BreathPhaseScheduler& scheduler);
  
  protected:
	virtual EventData::EventStatus reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus);
	virtual void settingChangeHappened_(const SettingId::SettingIdType id);
	virtual void enableTriggers_(void);

  private:

    SimvScheduler(const SimvScheduler&);  // Declared but not implemented
    void operator=(const SimvScheduler&); // Declared but not implemented
    SimvScheduler(void); // Declared but not implemented

	void startInspiration_(const BreathTrigger& breathTrigger,
							BreathPhase* pBreathPhase,
							BreathType breathType);
	void startExhalation_(const BreathTrigger& breathTrigger,
	                      BreathPhase* pBreathPhase,
	                      const Boolean noInspiration = FALSE);

	void adjustIntervalForRateChange_(void);
    //@ Data-Member: simvInterval_;
    //mand or spont interval of SIMV cycle
    SimvInterval simvInterval_;

	//@ Data-Member: simvCycleTimeMs_
	//calculated duration for this cycle, based on respiratory
	//rate setting
	Int32 simvCycleTimeMs_;

	//@ Data-Member: actualSimvInterval_
	//calculated duration for this cycle, corrected for maneuvers,
	// i.e. expiratory pause, in msec
	Int32 actualSimvInterval_;

    //@ Data-Member: targetMandatoryInterval_;
    //length of current mandatory interval
    Int32 targetMandatoryInterval_;

    //@ Data-Member: pendingMandatoryInterval_;
    //length of mandatory interval pending a rate change
    Int32 pendingMandatoryInterval_;

    //@ Data-Member: maxMandatoryInterval_;
    //max length for mandatory interval
    Int32 maxMandatoryInterval_;

    //@ Data-Member: simvIntervalExtension_
    // the extension of the simv interval, in milliseconds,
    //cause by expiratory or inspiratory pause 
    Int32 simvIntervalExtension_;

	//@ Data-Member: isFirstBreath_
	// a boolean flag used for flagging the first breath in Simv 
	Boolean isFirstBreath_;

	//@ Data-Member:& rCycleTimer_
	// a reference to the server IntervalTimer
	IntervalTimer& rCycleTimer_;

	//@ Data-Member: pendingBreathPeriod_
	// the simv breath period proposed due to a rate change
	Int32 pendingBreathPeriod_;

	//@ Data-Member: isRateAdjustmentRequired_ 
	// set to true when rate change request is detected during the mandatory interval
	Boolean isRateAdjustmentRequired_;

};


#endif // SimvScheduler_HH 
