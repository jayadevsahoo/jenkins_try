#ifndef PostNovram_HH
#define PostNovram_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: PostNovram.hh - Phase 2 POST NOVRAM Definitions
//
//---------------------------------------------------------------------
//@ Interface-Description
//    This file contains the persistent data variables used by POST
//    Phase 2 and 3.  Data defined by this PostNovram struct is located in
//    Non-Volatile RAM.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/PostNovram.hhv   25.0.4.0   19 Nov 2013 14:17:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 002    By: Gary Cederquist  Date: 08-APR-1997  DR Number: 1901
//    Project: Sigma (R8027)
//    Description:
//       Changed to use symbolic reference to NOVRAM base address.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Post.hh"
#include "MemoryMap.hh"
#include "TimeStamp.hh"
#include "postnv.hh"

//@ End-Usage

enum
{
    UMPIRE_QUEUE_DEPTH = 3
};

#define POST_NOVRAM_VERSION_CODE  (0xFE05)
#define NLV_COOKIE (0x5ecaac10)

struct PostNovram
{
   Uint16                versionCode;
   TimeStamp             powerDownTime;
   Criticality           backgroundFailed;
   Uint32		         operationalTime;
   Uint32		         acLineVoltage;
   Uint32                strikeIndex;
   Uint32                strikeHour[UMPIRE_QUEUE_DEPTH];
   Uint32		         failedAdcChan;
   Boolean               bdFailed;
};


extern PostNovram * PPostNovram;

static const int POST_NOVRAM_LENGTH = 512;

#endif // PostNovram_HH
