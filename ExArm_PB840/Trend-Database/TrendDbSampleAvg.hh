#ifndef TrendDbSampleAvg_HH
#define TrendDbSampleAvg_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbSampleAvg
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleAvg.hhv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
//  Revision: 001  By:  ksg    Date:  17-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
// 
//=====================================================================


#include "TrendDbSampleData.hh"




class TrendDbSampleAvg : public TrendDbSampleData
{
public:    
	TrendDbSampleAvg( PatientDataId::PatientItemId id,
					  TrendSelectValue::TrendSelectValueId trendId );
	TrendDbSampleAvg( SettingId::SettingIdType id,
					  TrendSelectValue::TrendSelectValueId trendId );
	virtual ~ TrendDbSampleAvg();


	virtual void compressData( TrendTimeScaleValue::TrendTimeScaleValueId timescale );


	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
protected:
	virtual void clear_();

private:
	TrendDbSampleAvg( const TrendDbSampleAvg& );	// not implemented
	void operator=( const TrendDbSampleAvg& );	// not implemented

	TrendValue  computeAvgAndClear_( TrendTimeScaleValue::TrendTimeScaleValueId timescale );
	void        addToAcumm_( const TrendValue & value, 
							 TrendTimeScaleValue::TrendTimeScaleValueId timescale );

	//@ Data-Member: acummArray_
	// Array of bounded values for timescales above 1Hr
	// value = accumulated sampled values
	// num = number of samples accumulated
	struct acummType
	{
		Real32 value;
		Uint8  num;
	} acummArray_[ TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES - 1 ];
};

#endif // TrendDbSampleAvg_HH
