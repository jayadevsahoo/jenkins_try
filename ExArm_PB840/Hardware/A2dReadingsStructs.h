
//*****************************************************************************
//
//  A2dReadingsStructs.h - Header file for the A/D readings structures.
//
//*****************************************************************************

#ifndef A2D_READINGS_STRUCTS_H

#define A2D_READINGS_STRUCTS_H

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

struct A2dWithStatus_t
{
    uint16_t            Value;                                      // 12 bit value from the A/D
    bool                Valid;                                      // false if there was a problem reading the A/D
};

// This structure contains the raw A/D data.
struct RawA2dReadings_t
{
	A2dWithStatus_t     ExpPressure;
    A2dWithStatus_t     InspPressure;
    A2dWithStatus_t     VoltageCheck;
	A2dWithStatus_t     AirTemp;
	A2dWithStatus_t     AirFlow;
	A2dWithStatus_t     O2Temp;
	A2dWithStatus_t     O2Flow;
	A2dWithStatus_t     ExhDrivePressure;
	A2dWithStatus_t		O2Sensor;
	A2dWithStatus_t		AirPressure;
	A2dWithStatus_t		O2Pressure;
	A2dWithStatus_t		VbattInt;
	A2dWithStatus_t		ExhSupplyPressure;
	A2dWithStatus_t		Vref2p5;
	A2dWithStatus_t		Vref3p16;
	A2dWithStatus_t		ExhaleFlow;

	void InitValidState(bool State)
	{
		ExpPressure.Valid = State;
        InspPressure.Valid = State;
		VoltageCheck.Valid = State;
	    AirTemp.Valid = State;
	    AirFlow.Valid = State;
	    O2Temp.Valid = State;
	    O2Flow.Valid = State;
	    ExhDrivePressure.Valid = State;
		O2Sensor.Valid = State;
		AirPressure.Valid = State;
		O2Pressure.Valid = State;
		VbattInt.Valid = State;
		ExhSupplyPressure.Valid = State;
		Vref2p5.Valid = State;
		Vref3p16.Valid = State;
		ExhaleFlow.Valid = State;
	}
};


struct FilteredA2dReadings_t
{
	AdcCounts				InspPressureFiltered;
	AdcCounts				ExpPressureFiltered;
	AdcCounts				AirFlowFiltered;
	AdcCounts				O2FlowFiltered;
	AdcCounts				ExhaleFlowFiltered;
};

struct RawAndFilteredA2dReadings_t
{
	RawA2dReadings_t		RawReadings;
	FilteredA2dReadings_t	FilteredReadings;
};

// This structure contains the A/D data after conversion to engineering units.
struct ConvertedA2dReadings_t
{
    Pressure_t              InspPressure;
    Pressure_t              FilteredInspPressure;
    Pressure_t              ExpPressure;
    Pressure_t              FilteredExpPressure;
	Pressure_t				ExhDrivePressure;
	Pressure_t				FilteredExhDrivePressure;
	Pressure_t				AirwayPressure;
	Pressure_t				FilteredAirwayPressure;
	Pressure_t				AirPressure;
	Pressure_t				O2Pressure;
	Pressure_t				SupplyPressure;
	Flow_t					AirFlow;
	Flow_t					FilteredAirFlow;
	Flow_t					O2Flow;
	Flow_t					FilteredO2Flow;
	Flow_t					ExhalationFlow;
	Flow_t					FilteredExhFlow;
	float					FiO2;
};

struct CommandValues_t
{
    DacCounts                  AirDac;              // DAC_AIR_VALVE,
    DacCounts                  O2Dac;               // DAC_O2_VALVE,
    DacCounts                  ExhalationDAC;       // DAC_EXHALATION_VALVE,
	float						DesiredAirFlow;
	float						DesiredO2Flow;
	float						DesiredPressure;
	float						Phase;
};

#endif

