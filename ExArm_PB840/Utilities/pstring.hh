//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file bstring.h
///
/// The pstring class is just a std::basic_string typedef with the allocator
/// used being block_allocator. block_allocator obtains memory from a fixed block
/// memory pool not the global heap.
///
/// Use pstring exactly the same as std::string except that the maximum string
/// size is limited to the maximum size supported by block_allocator.
/// The user of this class is responsible to ensure the STL container never
///	needs more than the fixed size provided by the allocator. Otherwise a
///	SoftFault is triggered.
///
/// Example usage:
///
/// pstring s = "This is ";
///	s += "my string.";
//------------------------------------------------------------------------------
#ifndef PSTRING_H
#define PSTRING_H

//------------------------------------------------------------------------------
// I N C L U D E S
//------------------------------------------------------------------------------
#include "multipool_allocator.hh"
#include <string>

typedef std::basic_string<char, std::char_traits<char>, multipool_allocator<char> > pstring;
typedef std::basic_string<wchar_t, std::char_traits<wchar_t>, multipool_allocator<wchar_t> > pwstring;

#endif // PSTRING_H

