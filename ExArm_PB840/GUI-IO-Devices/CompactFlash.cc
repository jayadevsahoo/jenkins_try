#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CompactFlash - Contains the interface to the compact flash
//---------------------------------------------------------------------
//@ Interface-Description
// Clients of the Compact Flash get a static reference to the interface
// via the CompactFlash::GetCompactFlash() method.  Each client can call
// the open() to configure the interface, at least one must call open() all
// other calls to open() are no-op's.
// To read data from the Compact Flash the client must provide RAM to hold
// the data, and then calls the read() method passing in the starting block
// LBA number, address to the RAM and number of 512 byte blocks to read.
// To write data to the compact flash the client uses the write() method 
// passing in the starting LBA number, address of the data to be written and
// the number of 512 byte blocks to write.
// The selfTest() is used to verify the operation and interface to the 
// Compact Flash, selfTest() takes a Boolean TRUE for the extended test.
// 
// The public interfaces are guard with a CriticalSection and care must be
// taken between different clients to avoid priority inversions.
//
//---------------------------------------------------------------------
//@ Rationale
// To provide a software encapsulation of the CompactFlash.
//---------------------------------------------------------------------
//@ Implementation-Description
// Implementation is based on the SanDisk compact flash Data sheet
// Version 10.6 Doc ID 20-10-00038 Date April 2004
// The interaction with the CompactFlash is via the Xena FPGA registers
// for detail see the Xena Hardware specification.  The registers are defined
// in a union so that they are sized aligned per the hardware spec.
// Each of the public interface methods locks a critical section and then
// calls a private implementation method, see the class descriptions for 
// more detail.  To prevent dead lock a public method can't all another
// public method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Returns SigmaStatus for success of the operations
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-IO-Devices/vcssrc/CompactFlash.ccv   25.0.4.0   19 Nov 2013 14:12:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjf    Date:  22-Aug-2006    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version.
// 
//=====================================================================

//@ Usage-Classes
//@ End-Usage
//#define GUI_IO_DEBUG // uncomment to get verbose output
#include "GuiIo.hh"

#include "CompactFlash.hh"

#include "CriticalSection.hh"
#include "IpcIds.hh"
#include "OsUtil.hh"
#include "OsTimeStamp.hh"
#include "SoftwareOptions.hh"
#include "Task.hh"


//=====================================================================
// Compact flash register types
//=====================================================================

//@ Type: CompactFlashOption Register
// Register used to setup the configuration of the card
//   first 16 bits are mapped to the CF Address 200
//   register size is 64 bits
struct CompactFlashOptionReg
{
	union
	{
		Uint16  configuration;  
		Uint32  CF_CFG_CONFIGURATION_REG[2];         
	};
};

//@ Type: CompactFlashAlternateReg
// Register used to get the alternate status and set the device control
//  (Read Only) get the alternate status
//  (Write Only) Used for device control
//  register size is 32 bits
struct CompactFlashAlternateReg
{
	union
	{
		const Uint8 status;     
		Uint8       control;    
		Uint32      CFI_TF_CONTROL_REG;     
	};
};

//@ Type: CompactFlashTaskFile Registers
// Defines the structure of the FPGA registers to the compact flash
// task file.  Each register is on a 32 bit boundary and some registers
// have more than one purpose depending on if it is read or written
struct CompactFlashTaskFileRegs
{
	union
	{
		// Read & Write data to the compact flash
		Uint16  data;   
		Uint32  CFI_TF_DATA_REG;
	};

	union
	{
		// (Read Only) Error from the compact flash
		const Uint8 error;
		// (Write Only) Set compact flash features
		Uint8 feature;      
		Uint32 CFI_TF_ERROR_REG;
	};

	union
	{
		// Read/write number of sectors to transfer
		Uint8 sectorCount;  
		Uint32 CFI_TF_SECTOR_COUNT_REG;
	};

	union
	{
		// Read/write bits 0 - 7 of the LBA
		Uint8 sectorNumber; 
		Uint32 CFI_TF_SECTOR_NUMBER_REG;
	};

	union
	{
		// Read/write bits 8 - 15 of the LBA
		Uint8 cylinderLow;  
		Uint32 CFI_TF_CYLINDER_LOW_REG;
	};

	union
	{
		// Read/write bits 16 - 23 of the LBA
		Uint8 cylinderHigh; 
		Uint32 CFI_TF_CYLINDER_HIGH_REG;
	};

	union
	{
		// Read/write bits 24 - 27 of the LBA
		Uint8 driveHead;
		Uint32 CFI_TF_DRIVE_HEAD_REG;
	};

	union
	{
		// (Read Only) get the compact flash status
		const Uint8 status;
		// (Write Only) send ATA command
		Uint8 command;      
		Uint32 CFI_TF_COMMAND_REG;
	};
};


//=====================================================================
// static variables and types
//=====================================================================
// Compact flash FPGA registers
static const Uint32 COMPACT_FLASH_OPTION    = 0x40000800;
static const Uint32 COMPACT_FLASH_TASK_FILE = 0x400007C0;
static const Uint32 COMPACT_FLASH_ALTERNATE = 0x40000FD8;

// Bit definitions (count from 0 - 7)
static const Uint8  CF_PRIMARY_IO_MODE  = 0x02;	// Bit 1 (Configuration Option)
static const Uint8  CF_LEVEL_INTERRUPTS = 0x40;	// Bit 6 (Configuration Option)
static const Uint8  CF_DCR_SWRST        = 0x04;	// Bit 2 (Device Control)
static const Uint8  CF_DCR_IEN          = 0x02;	// Bit 1 (Device Control)
static const Uint8  CF_ALTSTATUS_BUSY   = 0x80;	// Bit 7 (Alternate Status)
static const Uint8  CF_ALTSTATUS_DRQ    = 0x08;	// Bit 3 (Alternate Status)
static const Uint8  CF_DIAG_NO_ERROR    = 0x01;	// Bit 0 (Diagnostic Code)
static const Uint8  CF_LBA_MODE         = 0x40;	// Bit 6 (Drive/Head)
static const Uint8  CF_STATUS_ERR       = 0x01;	// Bit 1 (Status)


// ATA commands
static const Uint8  ATA_READ_SECTOR         = 0x20;
static const Uint8  ATA_WRITE_SECTOR        = 0x30;
static const Uint8  ATA_DRIVE_DIAGNOSTIC    = 0x90;
static const Uint8  ATA_IDENTIFY_DRIVE      = 0xEC;

// Drive Identify offsets into the array of words
// returned from the ATA_IDENTIFY_DRIVE command for the
// total sector capacity
static const Uint8  ATA_DRIVE_LBA_LSW       = 0x3C; 
static const Uint8  ATA_DRIVE_LBA_MSW       = 0x3D;

// Wait time for a command to complete in ms
static const Uint32 CF_CMD_WAIT_TIME        = 1000;	// 1 second in ms

// Performance goal used in the compact flash self test
// Set to 512K/Sec or reading 128k in 250ms
static const Uint32 PERFORMANCE_GOAL        = 250;	// ms
static const Uint8  PERFORMANCE_LOOPS       = 0xFF;


//@ Code...

//=====================================================================
//
// Public methods - CompactFlash
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CompactFlash [constructor]
//
//@ Interface-Description
// Can only be constructed by the GetCompactFlash() method 
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  Setup the CompactFlash interface with initial status "NOT_INITIALIZED"
//  Passes in the FPGA register locations
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
CompactFlash::CompactFlash(   Uint32 cfOptionRegAddr,
							  Uint32 cfTaskFileRegsAddr,
							  Uint32 cfAltRegAddr )
	:   status_( NOT_INITIALIZED ),
		capacity_( 0 ),
		pOptionReg_( (CompactFlashOptionReg *) cfOptionRegAddr ),
		pTaskFileRegs_( (CompactFlashTaskFileRegs *) cfTaskFileRegsAddr ),
		pAlternateReg_( (CompactFlashAlternateReg *) cfAltRegAddr )
{            
	DBG1( "CompactFlashInterface constructor status %d\n", status_ );
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CompactFlash [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
CompactFlash::~CompactFlash( void )
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [static]
//
//@ Interface-Description
//  Used by GUI_IO Initialize to construct the static interface
//  object at system startup
//---------------------------------------------------------------------
//@ Implementation-Description
//  Force construction
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void CompactFlash::Initialize( void )
{
	GetCompactFlash();  
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetCompactFlash [static]
//
//@ Interface-Description
//  Returns a reference to a static instance of the CompactFlash object
//---------------------------------------------------------------------
//@ Implementation-Description
//  On first entry, it instantiates the interface and returns its reference. 
//  Subsequent calls only return the reference to the static instantiation.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
CompactFlash & CompactFlash::GetCompactFlash( void )
{
	static CompactFlash CompactFlash1_( COMPACT_FLASH_OPTION,
										COMPACT_FLASH_TASK_FILE,
										COMPACT_FLASH_ALTERNATE );
	return CompactFlash1_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: open
//
//@ Interface-Description
//  The public "open()" locks the interface and calls the
//  implementation of private open.  Before the interface can be used
//  a call to open() must be made, other calls to open are no-op's
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::open( void )
{
	CriticalSection mutex( COMPACT_FLASH_MT );

	return( open_() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: read
//
//@ Interface-Description
//  The public "read()" locks the interface and calls the private 
//  implementation of read.  The user must supply enough memory
//  space for the data.
//   blockLbaNum - starting block on the compact flash
//   pBlockData - pointer to RAM to hold the data from the Compact Flash
//   numBlocks - number of 512 byte blocks to read from the compact flash
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	Returns the success of the operation
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::read(Uint32 blockLbaNum, BlockPtr pBlockData, Uint8 numBlocks )
{
	CriticalSection mutex( COMPACT_FLASH_MT );

	return( read_( blockLbaNum, pBlockData, numBlocks ) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: write
//
//@ Interface-Description
//  The public "write()" locks the interface and calls the private
//  implementation of the write.  The caller supplies the data to
//  be transferred to the Compact Flash
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	Returns the succes of the operation
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::write( Uint32 blockLbaNum, const BlockPtr pBlockData, Uint8 numBlocks )
{
	CriticalSection mutex( COMPACT_FLASH_MT );

	return( write_( blockLbaNum, pBlockData, numBlocks ) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: selfTest
//
//@ Interface-Description
//  The public "selfTest()" locks the interface and calls the implementation
//  of the seflfTest.  The short selftest() just relies on the card's
//  diagnostic, while the long test verifies read/write and performance
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the private selfTest_() fails then go ahead and update the status
//  this is needed to support serive mode testing
// 
//  Satisfies requirement: 
// 	[TR01129] ... system shall provide a method to test the compact flash ...
//  [TR01130] ... test shall verify card communication, card internal diagnostics,
//  read & write operations
//  [TR01131] ... test shall ... complete within 5 minutes
// 
// 
// 							
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	Returns the success of the operation
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::selfTest( Boolean isExtended )
{
	CriticalSection mutex( COMPACT_FLASH_MT );

	SigmaStatus cfSelfTest = selfTest_( isExtended );
	if( cfSelfTest != SUCCESS )
	{
		DBG1( "SelfTest: FAILED, isextended %d\n", isExtended );
		status_ = CARD_ERROR;
	}

	return( cfSelfTest );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: open_ [private]
//
//@ Interface-Description
//  Configures the Compact Flash Interface.  Returns Sigma Status
//  on successful configuration "open", and set's the status to
//  "STATUS_OK".  If the FPGA interface is not found or invalid
//  (ie not Xena hardware for example) then set status to "FPGA_ERROR".
//  If no card is installed, or is not found then set the status to
//  "NO_CARD_ERROR".  If the card fails to response then set the status
//  to "TIMEOUT_ERROR".  If the card fails configuration then set status
//  to "CARD_ERROR".
//---------------------------------------------------------------------
//@ Implementation-Description
//  FPGA_ERROR
//    Not Xena hardware or no access to the configuration register
//  NO_CARD_ERROR
//    The Task File register doesn't store values
//  TIMEOUT_ERROR
//   Card fails to respond to the soft reset
//  CARD_ERROR
//   Card's diagnostic returns an error
//---------------------------------------------------------------------
//@ PreCondition
//	If the card status is already OK then just return Success
//  If this is not Xena hardware then just return FAILURE
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::open_( void )
{
	DBG1( "CF Open %d\n", status_ );

	// If interface is already open and not in error then 
	// just return
	if (status_ == CompactFlash::STATUS_OK)
	{
		DBG( "Open: Already Initialized\n" );
		return SUCCESS;
	}

	// Check for the Xena board, if not set the "FPGA" error
	if (!IsXenaConfig())
	{
		DBG( "Open: Not Xena board\n" );
		status_= FPGA_ERROR;
		return FAILURE;
	}

	// Step 1 - Initialize the card and run a short self test
	CfStatus newStatus = STATUS_OK;

	// Set the compact flash into I/O Primary mode and Level Interrupts
	pOptionReg_->configuration = (CF_PRIMARY_IO_MODE | CF_LEVEL_INTERRUPTS);

	// Verify that the FPGA interface has taken the value
	// and move onto the next step
	if (pOptionReg_->configuration == (CF_PRIMARY_IO_MODE | CF_LEVEL_INTERRUPTS))
	{
		// Check the task file interface by modifying the sector count
		// This will fail if the card is not installed
		Uint8 rd = pTaskFileRegs_->sectorCount + 1;
		pTaskFileRegs_->sectorCount = rd;

		// If the task file took then move on to the next step
		if (pTaskFileRegs_->sectorCount == rd)
		{
			// Initialize the compact flash card. Set the soft reset and
			// interrupts, hold for at least 1ms and then release
			pAlternateReg_->control = (CF_DCR_SWRST | CF_DCR_IEN );
			Task::Delay(0, 10);
			pAlternateReg_->control = CF_DCR_IEN;

			// Wait for the drive and the run a short self test
			if (waitDriveReady_() == SUCCESS)
			{
				// Do the quick self test, if error then set status and stop
				if (selfTest_( FALSE ) != SUCCESS)
				{
					newStatus = CARD_ERROR;
				}
			}
			else
			{
				// Drive failed to respone
				DBG( "Open: Reset timeout\n" );
				newStatus = CARD_ERROR;
			}
		}
		else
		{
			// Taks File failed to take value
			DBG( "Open: FPGA Task file error\n" );
			newStatus = NO_CARD_ERROR;
		}
	}
	else
	{
		// FPGA failed to take opention values
		DBG( "Open: FPGA Interface error\n" );
		newStatus = FPGA_ERROR;
	}

	// Step 2
	// If the first step had no errors then move to the second step
	// which will load the drive identification information and
	// pull out the capacity
	if (newStatus == STATUS_OK)
	{
		if (sendCommand_( ATA_IDENTIFY_DRIVE ) == SUCCESS)
		{
			// Load the information block to get the capacity
			// and set the status to OK
			Block       infoBlock;
			if (loadBlock_(infoBlock) == SUCCESS)
			{
				capacity_ = (Uint32) (( infoBlock[ATA_DRIVE_LBA_MSW] << 16) | 
									  infoBlock[ATA_DRIVE_LBA_LSW] );
			}
			else
			{
				// Load block failed
				newStatus = CARD_ERROR;
			}
		}
		else
		{
			// Send command failed
			newStatus = CARD_ERROR;
		}
	}

	// Wait until complete before setting the card status
	status_ = newStatus;
	DBG2( "Open: Capacity %d, Status %d\n", capacity_, status_ );
	return( (status_ == STATUS_OK) ? SUCCESS : FAILURE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: read_ [private]
//
//@ Interface-Description
//  Read signal or multi blocks from the compact flash
//  Parameters
//    blockLbaNum - Starting LBA block on the compact flash
//    pBlockData - pointer to RAM used to hold the data from the compact flash
//    numBlock - Number of 512 byte blocks to load
//  Returns the status of the operation 
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the Interface is initialized and not in error then make a 
//  ATA data read request to load the given block(s) from flash.  
//  Read in each 512 byte block at a time.
//  Sets the card status on error
//---------------------------------------------------------------------
//@ PreCondition
//	Status must be OK and block in range, return FAILURE if precondition 
//  are not meet
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::read_( Uint32 blockLbaNum,
								 BlockPtr pBlocksData,
								 Uint8 numBlocks )
{
	DBG2( "CF Read blocks %d - %d\n", blockLbaNum, (blockLbaNum+numBlocks-1) );

	if ((status_ != STATUS_OK) || (blockLbaNum+numBlocks) > capacity_)
	{
		DBG1( "Read: Error, status %d", status_ );
		return FAILURE;
	}

	// Send the Read sector command
	Int16 loopCnt = 0;
	BlockPtr pData = pBlocksData;
	if (sendCommand_( ATA_READ_SECTOR, blockLbaNum, numBlocks ) == SUCCESS)
	{
		while ((loopCnt < numBlocks) && status_ == STATUS_OK)
		{
			// Load the data into the block to return
			if (loadBlock_( pData ) == SUCCESS)
			{
				// Update the values for the next load
				loopCnt++;
				pData += BLOCK_SIZE;
			}
			else
			{
				status_ = CARD_ERROR;
			}
		}
	}
	else
	{
		status_ = CARD_ERROR;
	}

	DBG3( "Read: CF Status %x, CF Alt Status %x, CF Error %x\n",
		  pTaskFileRegs_->status, pAlternateReg_->status, pTaskFileRegs_->error );
	return( (status_ == STATUS_OK) ? SUCCESS : FAILURE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: write_ [private]
//
//@ Interface-Description
//  Send a block(s) of data to the compact flash
//  Parameters
//    blockLbaNum - Starting LBA block on the compact flash
//    pBlockData - pointer to the data to transfer to the compact flash
//    numBlocks - number of 512 byte blocks to transfer
//  Returns the sucess of the operation
//---------------------------------------------------------------------
//@ Implementation-Description
//  Compact flash must be Initialized and not in error
//  Sends the ATA write command, then transfers one block at a time.
//  Sets the card status on error
//---------------------------------------------------------------------
//@ PreCondition
//	Status is OK and the block is in range, will return FAILURE if 
//  precondition is not meet
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::write_( Uint32 blockLbaNum,
								  const BlockPtr pBlockData,
								  Uint8 numBlocks )
{
	DBG2( "CF Write block %d - %d\n", blockLbaNum, (blockLbaNum+numBlocks-1) );

	if ((status_ != STATUS_OK) || (blockLbaNum+numBlocks) > capacity_)
	{
		DBG2( "Write: Error, status %d, block LBA %d\n",
			  status_, (blockLbaNum+numBlocks-1) );
		return FAILURE;
	}

	// Send the Write sector command
	Int16 loopCnt = 0;
	BlockPtr pData = pBlockData;
	if (sendCommand_( ATA_WRITE_SECTOR, blockLbaNum, numBlocks ) == SUCCESS)
	{
		while ((loopCnt < numBlocks) && status_ == STATUS_OK)
		{
			// Transfer the block to the compact flash
			if (storeBlock_( pData ) == SUCCESS)
			{
				loopCnt++;
				pData += BLOCK_SIZE;
			}
			else
			{
				status_ = CARD_ERROR;
			}
		}
	}
	else
	{
		status_ = CARD_ERROR;
	}

	DBG3( "Write: CF Status %x, CF Alt Status %x, CF Error %x\n",
		  pTaskFileRegs_->status, pAlternateReg_->status, pTaskFileRegs_->status );
	return( (status_ == STATUS_OK) ? SUCCESS : FAILURE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: selfTest_ [private] 
//
//@ Interface-Description
//  Performs a short or extended self test on the compact flash
//  The short test isExtended if FALSE
//   Sends the ATA diagnostic command, checks the returned status
//  The extended self test isExtended is TRUE
//   Does the short test pulse
//   Read/Writes one block to the compact flash
//   checks the transfer performance of the compact flash
//  Returns FAILURE if any step fails
//---------------------------------------------------------------------
//@ Implementation-Description
//  See comments in the code, any failure will abort the rest of the test
//  selfTest doesn't set the status of the card, this is done in the calls
//  to selftest.
//---------------------------------------------------------------------
//@ PreCondition
//	The FPGA interface can't be in error for the test to begin
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::selfTest_( Boolean isExtended )
{
	DBG1( "CF Self Test, extended %d\n", isExtended );

	// Set inital state of the error to FALSE
	Boolean error = FALSE;

	// TODO E600 port
	/*
	// Send the ATA drive diagnostic command
	if (sendCommand_( ATA_DRIVE_DIAGNOSTIC ) != SUCCESS)
	{
		DBG( "SelfTest: Send Diag cmd error\n" );
		error = TRUE;
	}

	// Check the response on the status register
	// Only the No Error bit should be set, no others
	if (!error && pTaskFileRegs_->error != CF_DIAG_NO_ERROR)
	{
		error = TRUE;
	}

	DBG2( "SelfTest: Diag Register %x, error %d\n", 
		  pTaskFileRegs_->error, error );

	// If the extended option is set then do more testing
	// of read/write and trnasfer speed
	if( !error && isExtended )
	{
		// Setup to test read/write operations
		static const Int16 BUFFER_SIZE = 1;
		static CompactFlash::Block  buffer_write[ BUFFER_SIZE ];
		static CompactFlash::Block  buffer_read[ BUFFER_SIZE ];

		// Setup the buffers test buffers
		// Use a fixed pattern of all alternating 1 and 0's
		memset( buffer_write, 0xA5, sizeof(buffer_write) );
		memset( buffer_read, 0x0, sizeof(buffer_read) );

		// Write out the block and then turn around and read it back into a different buffer
		Boolean return_write = write_( TEST_BLOCK, (BlockPtr) buffer_write, BUFFER_SIZE );
		Boolean return_read = read_( TEST_BLOCK, (BlockPtr) buffer_read, BUFFER_SIZE );

		// Compare and report
		Int16 compareBlock = memcmp( buffer_read, buffer_write, sizeof(buffer_read) );
		if (!return_write || !return_read || compareBlock != 0)
		{
			// Set error if they don't compare
			DBG3( "SelfTest: Write/Read failed status %d, Read Status %d, Compare %d\n",
				  return_write, return_read, compareBlock );
			error = TRUE;
		}

		DBG1( "SelfTest: Read/Write errors %d\n", error );

		// Check the transfer performance
		if (!error )
		{
			// Performance bench mark the read transfer from the compact flash
			// with 255 contiguous blocks
			Int16 loopCount = 0;
			Uint32 performanceTime = 0;
			OsTimeStamp fastTime;
			if (sendCommand_( ATA_READ_SECTOR, TEST_BLOCK, PERFORMANCE_LOOPS ) == SUCCESS)
			{
				// Star the time, to capture the transfer timing
				fastTime.now();
				while (!error && (loopCount < PERFORMANCE_LOOPS))
				{
					// Load the data into the block to return
					if (loadBlock_( (BlockPtr) buffer_read ) != SUCCESS)
					{
						// Load block from compact flash failed
						DBG1( "SelfTest performance failed loop %d\n", loopCount );
						error = TRUE;
					}
					loopCount++;
				}
				performanceTime = fastTime.getPassedTime();
				DBG3( "SelfTest: Performance testing errors %d, time %d, goal %d ms\n",
					  error, performanceTime, PERFORMANCE_GOAL );

				// If no errors then verify the performance
				if (!error && performanceTime > PERFORMANCE_GOAL)
				{
					error = TRUE;
				}
			}
			else
			{
				// ATA read command failed
				error = TRUE;
			}
		}
	// End of the extended testing
	} 

	DBG1( "SelfTest: Successful %d, Card status %d\n", !error );
	*/
	return( (!error) ? SUCCESS : FAILURE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendCommand_		[private]
//
//@ Interface-Description
// Send the ATA command and optional LBA block number
// Valid LBA numbers go from 0 to Capacity (-1 means no LBA number)
// Parameters
//   ataCmd - the command to send
//   blockLbaNum - the option LBA block number
//   blockCnt - optional block count
// Returns the success of the send command
//---------------------------------------------------------------------
//@ Implementation-Description
//  Formats the ATA command and sends it on the command register
//  then waits for the busy to clear.
//  Returns FAILURE if there is an error or timeout wating for the
//  busy to clear.
//  Doesn't change the card status, this is done by the parent calls
//---------------------------------------------------------------------
//@ PreCondition
//	blockLbaNum must be -1 or valid, any other value will result in FAILURE
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::sendCommand_( Uint8 ataCmd, Int32 blockLbaNum, Uint8 blockCnt )
{
	// If there is a block then set the transfer count to one
	if (blockLbaNum >= 0 && (blockLbaNum+blockCnt) < capacity_)
	{
		pTaskFileRegs_->sectorCount = blockCnt;

		// Convert the blk into a LBA
		// Bits 0 - 7 go into Sector Number
		//     8 - 15 go into Cylinder Low
		//    16 - 23 go into Cylinder High
		//    24 - 27 fo into the lower 4 bits of Head
		// Plus bit 6 of Head is set for LBA mode
		pTaskFileRegs_->sectorNumber = (Uint8)( blockLbaNum & 0xFF );
		pTaskFileRegs_->cylinderLow = (Uint8)( (blockLbaNum & 0xFF00) >> 8 );
		pTaskFileRegs_->cylinderHigh = (Uint8)( (blockLbaNum & 0xFF0000) >> 16 );
		pTaskFileRegs_->driveHead = (Uint8)( ((blockLbaNum & 0xF000000) >> 24) | CF_LBA_MODE );
	}
	else if (blockLbaNum == -1)	// No LBA number
	{
		// Set values to zero
		pTaskFileRegs_->sectorCount = 0;
		pTaskFileRegs_->sectorNumber = 0;
		pTaskFileRegs_->cylinderLow = 0;
		pTaskFileRegs_->cylinderHigh = 0;
		pTaskFileRegs_->driveHead = 0;
	}
	else
	{
		DBG1(   "SendCmd: Invliad blk %d\n", blockLbaNum );
		return FAILURE;
	}

	// Write the command into the command register, this will cause
	// the command to be transferred to the compact flash
	pTaskFileRegs_->command = ataCmd;

	// Write for the command to complete
	Boolean error = FALSE;
	if (waitDriveReady_() == SUCCESS)
	{
		if (( pTaskFileRegs_->status & CF_STATUS_ERR ))
		{
			DBG1( "SendCmd: Error %x\n", pTaskFileRegs_->error );
			error = TRUE;
		}
	}
	else
	{
		DBG( "SendCmd: Wait timeout\n" );
		error = TRUE;
	}

	return( (!error) ? SUCCESS : FAILURE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: waitDriveReady_		[private]
//
//@ Interface-Description
//  Waits for the busy bit to be cleared, will timeout after 1 sec
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return FAILURE if timeout
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::waitDriveReady_( void )
{
	const Int16 waitDelay = 10;	 // in ms

	// Each loop wait for the drive to go not busy and wait at
	// least the command wait time then give up and return false
	Int16 cnt = (CF_CMD_WAIT_TIME / waitDelay) + 1;
	while ((pAlternateReg_->status & CF_ALTSTATUS_BUSY) && cnt)
	{
		cnt--;
		Task::Delay( 0, waitDelay );
	}

	// If there is a count left then the busy bit cleared before timeout
	return( (cnt) ? SUCCESS : FAILURE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadBlock_		[private]
//
//@ Interface-Description
//  Transfer a block from the FPGA interface to ram, pointed to by pBlockData
//  The FPGA interface must already be ready for the transfer before this call
//  Return FAILURE if the transfer is not ready or the transfer times out
//---------------------------------------------------------------------
//@ Implementation-Description
//  Will verify that the data ready status is set and ready
//  before returning Success
//---------------------------------------------------------------------
//@ PreCondition
//	Data is ready to be transferred
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::loadBlock_( CompactFlash::BlockPtr pBlockData )
{
	// Verify data ready
	Boolean error = FALSE;
	if ((pAlternateReg_->status & CF_ALTSTATUS_DRQ))
	{
		// Transfer each pair of bytes by reading from the data register 
		// each read will cause the FPGA to latch the next value
		for (Int16 idx = 0; idx < BLOCK_SIZE; idx++)
		{
			pBlockData[ idx ] = pTaskFileRegs_->data;
		}

		// Wait for the transfer to complete then check the status
		if (waitDriveReady_() == SUCCESS)
		{
			// Check the status register
			if ((pTaskFileRegs_->status & CF_STATUS_ERR ))
			{
				DBG1( "loadBlock: Error bit set, status %x\n",
					  pTaskFileRegs_->status );
				error = TRUE;
			}
		}
		else
		{
			// Transfer timed out
			DBG( "LoadBlock: Transfer timedout\n" );
			error = TRUE;
		}
	}
	else
	{
		// Data ready bit is not set
		DBG1( "loadBlock: Data is not ready for transfer, status %x\n",
			  pTaskFileRegs_->status );
		error = TRUE;
	}

	return( (!error) ? SUCCESS : FAILURE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: storeBlock_		[private]
//
//@ Interface-Description
//  Transfer a block of memory pointed to by pBlockData to the FPGA interface
//  The FPGA interface must already be ready for the transfer before this call
//  Returns FAILURE if the transfer is not ready, has an error, or timeout
//---------------------------------------------------------------------
//@ Implementation-Description
//  Make sure compact flash is ready for data and that there are no
//  errors after the operation
//---------------------------------------------------------------------
//@ PreCondition
//	Compact flash is ready for data
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
SigmaStatus CompactFlash::storeBlock_( const CompactFlash::BlockPtr blockData )
{
	// Verify that compact card is ready for data
	Boolean error  = FALSE;
	if ((pAlternateReg_->status & CF_ALTSTATUS_DRQ ))
	{
		// Transfer each pair of bytes by reading from the compact flash
		// into the FPGA data register, each write will cause the FPGA
		// to send it to the compact flash before returning to code
		for (Int16 idx = 0; idx < BLOCK_SIZE; idx++)
		{
			pTaskFileRegs_->data = blockData[ idx ];
		}

		// Wait for the transfer to complete then check status
		if (waitDriveReady_() == SUCCESS)
		{
			// Check the status register
			if ((pTaskFileRegs_->status & CF_STATUS_ERR))
			{
				DBG1( "storeBlock: Error bit set, status %x\n",
					  pTaskFileRegs_->status );
				error = TRUE;
			}
		}
		else
		{
			DBG( "storeBlock: Transfer timed out\n" );
			error = TRUE;
		}
	}
	else
	{
		DBG1( "StoreBlock:: Data is not ready for transfer, status %x\n",
			  pTaskFileRegs_->status );
		error = TRUE;
	}

	return( (!error) ? SUCCESS : FAILURE );
}

