#ifndef VentStatus_HH
#define VentStatus_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//  Class: VentStatus - implements the functionality required for
//                      interfacing with the vent head status LEDs
//                      and SVO, ten second timer, vent inop, and
//                      BD audio alarm registers
//-------------------------------------------------------------
// @ Version-Information
// @ (#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/VentStatus.hhv   10.7   08/17/07 09:32:32   pvcs  
//
// @ Modification-Log
//
//  Revision: 007  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 006  By:  syw    Date: 23-Jun-1997    DR Number: DCS 2244
//       Project:  Sigma (R8027)
//       Description:
//			Added newCycle() method to update the status of the bdAudioAlarmOn_
//			based on the register contents.
//
//  Revision: 005 By: iv   Date: 06-May-1997   DR Number: DCS 2027
//      Project:  Sigma (840)
//      Description:
//			Removed registerForAlarmActivationFlag(),  
//          getAlarmActivationFlagStatus() and the data members:
//          alarmActivatedFlag_ and alarmActivationFlagUserId_ .
//
//  Revision: 004 By: syw   Date: 14-Apr-1997   DR Number: DCS 1921
//      Project:  Sigma (840)
//      Description:
//			Added alarm off timer to determine amout of time that has
//			elapsed since the alarm is turned off.
//
//  Revision: 003 By: by   Date: 13-Feb-1997   DR Number: DCS 1718
//      Project:  Sigma (840)
//      Description:
//          Added registerForAlarmActivationFlag() for a client to
//          register for an alarm activation call back.  Also added
//          getAlarmActivationFlagStatus() the registered client to
//          retrieve the alarm activation status.  Added logic
//          in turnOnBdAudioAlarm_() and latchOnBdAudioAlarm_() to
//          set alarm activity flag to TRUE when executed.
//
//  Revision: 002  By:  syw   Date:  13-Jan-1997    DR Number: DCS 1603
//      Project:  Sigma (840)
//      Description:
//            Added LatchVentStatusService(), latchOnGuiLed_(), latchOffGuiLed_(),
//            latchOnBdAudioAlarm_(), latchOffBdAudioAlarm_() methods.
//            Eliminated VentStatusService() and newCycle() methods since
//            BitAccessRegisterMediator will service the request.  Eliminate
//            ExternalServiceTable_.
//
//  Revision: 001  By:  by   Date:  11-Nov-1996    DR Number: 
//       Project:  Sigma (R840)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
#include "BinaryCommand.hh"
#include "OsTimeStamp.hh"
//@ End-Usage

extern const Uint8 LOSS_GUI_COMM_MASK1 ;
extern const Uint8 LOSS_GUI_COMM_MASK2 ;
extern const Uint16 BD_AUDIO_ALARM_MASK ;

class VentStatus 
{
    //@ Friend: SafetyValve
    friend class SafetyValve;

    //@ Friend: BdLampTest
    friend class BdLampTest;

    //@ Friend: BdAudioTest
    friend class BdAudioTest;

    //@ Friend: SmTasks
    friend class SmTasks;

  public:

    enum CommandType
    {
        NONE,
        DEACTIVATE,
        ACTIVATE
    };

    enum ObjectId
    {
        FIRST_OBJECT,
        SVO_STATE = FIRST_OBJECT,
        LOSS_OF_GUI_LED,
        VENT_INOP_STATE,
        BD_AUDIO_ALARM,
        TEN_SECOND_TIMER,
        VENT_INOP_REG_A,
        VENT_INOP_REG_B,
        TEN_SECOND_TIMER_REG_A,
        TEN_SECOND_TIMER_REG_B,

        TOTAL_VENT_STATUS_OBJECTS
    };

    VentStatus( void );
    ~VentStatus( void );

    static void
    DirectVentStatusService( const ObjectId object, const CommandType command );

    static void
    LatchVentStatusService( const ObjectId object, const CommandType command );

    inline void
    registerVentStatusCallBack( PVentStatusCallBack pVentStatusCallBack );

    inline Boolean getBdAudioAlarmStatus( void ) const;

	inline Uint32 getAlarmOffTimeMs( void) ;
	
    static void SoftFault( const SoftFaultID softfaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName = NULL,
						   const char*       pPredicate = NULL );

	void newCycle( void) ;
	
  protected:
    
  private:

     VentStatus( const VentStatus& );          //not implemented
     void operator=( const VentStatus& );      //not implemented

     inline void turnOnSvoLed_( void );
     inline void turnOffSvoLed_( void );
     inline void turnOnVentInOpLed_( void );
     inline void turnOffVentInOpLed_( void );
     inline void turnOnGuiLed_( void );
     inline void turnOffGuiLed_( void );
     inline void turnOnBdAudioAlarm_( void );
     inline void turnOffBdAudioAlarm_( void );
     inline void latchOnGuiLed_( void );
     inline void latchOffGuiLed_( void );
     inline void latchOnBdAudioAlarm_( void );
     inline void latchOffBdAudioAlarm_( void );

     void activateSvoState_( void );
     void deactivateSvoState_( void );
     void activateVentInopState_( const ObjectId object );
     void deactivateVentInopState_( const ObjectId object );
     void activateTenSecondTimer_( const ObjectId object );
     void deactivateTenSecondTimer_( const ObjectId object );

     //@ Data-Member: ventInopStateActivated_ 
     // indicates whether VENT INOP is currently active
     Boolean ventInopStateActivated_;

     //@ Data-Member: svoLed_
     // provides the io interface to turn on/off the SVO LED
     BinaryCommand  svoLed_;

     //@ Data-Member: ventInOpLed_
     // provides the io interface to turn on/off the Vent InOp LED
     BinaryCommand  ventInOpLed_;

     //@ Data-Member: guiLed1_
     // provides the io interface to turn on/off the GUI LED
     BinaryCommand  guiLed1_;

     //@ Data-Member: guiLed2_
     // provides the io interface to turn on/off the GUI LED
     BinaryCommand  guiLed2_;

     //@ Data-Member: bdAudioAlarmOn_
     // indicates whether the BD audio alarm is on
     Boolean bdAudioAlarmOn_;

     //@ Data-Member: bdAudioAlarm_
     // provides the io interface to turn on/off the BD audio alarm 
     BinaryCommand  bdAudioAlarm_;

     //@ Data-Member: safetStateA_ 
     // provides the io interface to activate/de-activate safe state A 
     BinaryCommand  safetStateA_;

     //@ Data-Member: safetStateB_ 
     // provides the io interface to activate/de-activate safe state B 
     BinaryCommand  safetStateB_;

     //@ Data-Member: forcedVentInopA_ 
     // provides the io interface to activate/deactivate
     // forced VENT INOP state A 
     BinaryCommand  forcedVentInopA_;

     //@ Data-Member: forcedVentInopB_ 
     // provides the io interface to activate/deactivate
     // forced VENT INOP state B 
     BinaryCommand  forcedVentInopB_;

     //@ Data-Member: tenSecondTimer_ 
     // provides the io interface to deactivate 10 second
     // timer on Safety Net Register A 
     BinaryCommand  tenSecondTimerA_;

     //@ Data-Member: tenSecondTimer_ 
     // provides the io interface to deactivate 10 second
     // timer on Safety Net Register B 
     BinaryCommand  tenSecondTimerB_;

     //@ Data-Member: pVentStatusCallBack_
     // pointer to the function called when ventilator status conditions that 
     // VentAndUserEventStatus needs to know about are detected so that
     // the GUI can be informed
     PVentStatusCallBack pVentStatusCallBack_;

	//@ Data-Member: alarmOffTimer_
	// indicates how long the alarm has been off
	OsTimeStamp alarmOffTimer_ ;
};

// Inlined methods...
#include "VentStatus.in"

#endif  //VentStatus_HH

