/*****************************************************************************/
/*   Module Name:    840.c                                           
/*   Purpose:      840 Ventilator Serial Communications Module                               
/*                                                                          
/*                                                                           
/*   Requirements:    
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 Critical Observations during 			Kathirvel(CGS)    27 Sep,2000
/*	 Phase1 is incorportated. 
/*
/*   ComWrtByte calls are replaced with     Dave Richardson   15 Nov, 2002
/*   ComSend so as to not continue until
/*   serail transmission has completed
/*   or timed out.
/*                                                                           
/*   $Revision::   1.2      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETRIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.

Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <rs232.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#include "840.h"
#include "840cmds.h"
#include "840Msgs.h"
#include "com.h"
#include "commreg.h"
#include "crc.h"
#include "version.h"

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#define TEST_STR                   "\x00\x04\x00\x00\x00\x00"
#define TEST_STR_LEN               6
#define TEST_STR_RESPONSE_STR      "\x06\x00\x04\x01\x01\x6b\x30"
#define TEST_STR_RESPONSE_STR2     "\x06\x00\x04\x00\x0f\x00\x00"
#define TEST_STR_RESPONSE_STR_LEN  7

/***********************************************/
/*************** LOCAL VARIABLES ***************/
/***********************************************/

static COM_STRUCT com; // structure to hold all com variables
static int  _840connected   = 0;
static char seq_number      = 0;
static int  lastConnectType = 0;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     Is840Connected                                                      
/*                                                                           
/* Purpose:  Determine if the 840 was detected.
/*                                          
/*                                                                           
/* Example Call:   int Is840Connected(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0 - not connected
/*			 1 - connected
/*                                                                           
/*****************************************************************************/
int Is840Connected(void)
{
	return _840connected;
}

/*****************************************************************************/
/* Name:     Establish840                                              
/*                                                                           
/* Purpose:  Establish initial communications with the 840.
/*                                          
/*                                                                           
/* Example Call:   int Establish840(int noAutoDetect);                                    
/*                                                                           
/* Input:    noAutoDetect - if true, then don't do an auto-detect of the 840
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   SUCCESS or FAILURE
/*
/*****************************************************************************/
int Establish840(int noAutoDetect)
{
	unsigned short crc;
	char ACKmsg[] = {ACK, '\0'};
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors

	seq_number = 0;

	// calculate the CRC
	crc = CalculateCrc((unsigned char*)com.testStr, com.testStr[MESSAGE_LENGTH]);

	// add CRC to end of msg
	com.testStr[com.testStrLen-2] = (unsigned char)(crc>>8);
	com.testStr[com.testStrLen-1] = (unsigned char)crc;

	// try to establish communications with the 840
	if(ComEstablish(&com, noAutoDetect) == SUCCESS){
		// let vent know we established com
		//ComWrtByte(com.port, ACK);  // ACK back
		ComSend(ACKmsg, strlen(ACKmsg), &com);  // ACK back
		_840connected = 1;
		com.connected = 1;
		com.timeout   = 1.0;      
		nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if testing for a non-existent port
		SetComTime(com.port, com.timeout);
		SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
		FlushInQ840();
		FlushOutQ840();

		GetVentInfo();
		return SUCCESS;   // success
	}
	else{
		_840connected = 0;
		return FAILURE;   
	}
}

/*****************************************************************************/
/* Name:     Send840                                                  
/*                                                                           
/* Purpose:   Send frame data to the 840. The caller does not calculate
/*  the CRC, the CRC is appended in this routine. Also, the length is determined
/* 	by the first 2 bytes of the frame. Only correctly framed data, minus the CRC
/*  which will be appended here, can be sent via this routine.                       
/*                                                                           
/* Example Call:  int Send840(char* msg);              
/*                                                                           
/* Input:    msg - a pointer to the message being sent      
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int Send840(char* msg)
{
	int tries;
	unsigned short crc;
	/*char*/ int ret;
	char sendStr[MAX_COM_STR];
	unsigned short len;
	char NAKmsg[] = {NAK, '\0'};

	if(!com.connected){
		return FAILURE;   // failure, not connected
	}

	len = (unsigned short)((unsigned char)msg[0] << 8) + (unsigned char)msg[1];	

	// make working copy of send string
	memcpy(sendStr, msg, len);

	msg[SEQ_NUMBER]= seq_number++;

	// calculate the CRC
	crc = CalculateCrc((unsigned char*)sendStr, len);

	// append CRC to end of sendStr
	sendStr[len]   = (unsigned char)(crc>>8);
	sendStr[len+1] = (unsigned char)crc;

	// keep trying message until we get an ACK back or run out of tries
	for(tries = 0; tries < MAX_COM_TRIES; tries++){
		// send the message plus 2 byte CRC
		if(ComSend(sendStr, len+2, &com) == SUCCESS){
			// now check for the ACK or NAK
			#ifndef DEMOMODE
			ret = ComRdByte(com.port);
			#else
			ret = ACK;
			#endif
			if(ret == ACK){
				return SUCCESS;   // success
			}
			else if(ret == NAK){
				//ComWrtByte(com.port, NAK);  // NAK Back 
				ComSend(NAKmsg, strlen(NAKmsg), &com); // NAK back
			}

			FlushInQ840();
			FlushOutQ840();

			// wait a while between retries
			Delay(0.1);
		}
	}

	return FAILURE;   
}

/*****************************************************************************/
/* Name:     Recv840                                                     
/*                                                                           
/* Purpose:  Receive data from the 840. Verifies the packet is
/*  good by checking the CRC.
/*                                          
/*                                                                           
/* Example Call:   int Recv840(char* msg, int len, int* actual, double maxWait);                                    
/*                                                                           
/* Input:   msg - a pointer to the message being sent
/*          len - the length of the string pointed to by msg
/*          actual - the number of bytes actually read
/*          maxWait - max time to wait for the characters to arrive        
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int Recv840(char* msg, int len, int* actual, double maxWait)
{
	int tries;
	unsigned short crc;
	unsigned short recv_crc;
	int remainingLen;
	double stopWaitingTime;
	char ACKmsg[] = {ACK, '\0'};
	char NAKmsg[] = {NAK, '\0'};

	if(!com.connected){
		return FAILURE;   // failure, not connected
	}

	// keep trying message until we get an ACK back or timeout
	stopWaitingTime = Timer() + maxWait;
	while(Timer() <= stopWaitingTime){
		// get the frame size (first 2 bytes)
		if(ComRecv(msg, 2, actual, &com) == SUCCESS){
			// calc remaining bytes to be received
			remainingLen = (msg[0] << 8) + (unsigned char)msg[1];

			// if failed to get frame len, or len is too long for buffer or less than 0
			if(*actual != 2 || remainingLen + 2 > len || remainingLen <= 0){
				FlushInQ840();   // flush for another try
				continue;
			}

			// now get the rest of the message
			if(ComRecv(msg+2, remainingLen, actual, &com) == SUCCESS){
				*actual += 2;   // add 2 for the length we already got

				// see if frame was received correctly
				if(Parse840Frame(msg, *actual) == SUCCESS){
					// good packet
					//ComWrtByte(com.port, ACK);  // ACK back
					ComSend(ACKmsg, strlen(ACKmsg), &com);  // ACK back
					return SUCCESS;
				}
			}

			// bad packet, so let vent know
			//ComWrtByte(com.port, NAK);  // NAK back 
			ComSend(NAKmsg, strlen(NAKmsg), &com); // NAK back

			// get the vent NAK back
			ComRecv(msg, 1, actual, &com);
		}
		else{
			// else timed out because got no bytes in
			ProcessSystemEvents();  
			continue;
		}
	}

	return FAILURE;
}

/*****************************************************************************/
/* Name:     Terminate840                                                      
/*                                                                           
/* Purpose:  Close the connection to the 840.
/*                                          
/*                                                                           
/* Example Call:   int Terminate840(void);                                    
/*                                                                           
/* Input:    None          
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int Terminate840(void)
{
	if(com.connected){
		com.connected = 0;
		return ComTerminate(&com);
	}
	else{
		return SUCCESS;   // success, not connected so don't try
	}
}

/*****************************************************************************/
/* Name:     Init840ComStruct                                                     
/*                                                                           
/* Purpose:  Set the com struct to its initial value.
/*                                          
/*                                                                           
/* Example Call:   void Init840ComStruct(void);                                    
/*                                                                           
/* Input:    None        
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void Init840ComStruct(void)
{
	strcpy(com.name, "840");
	com.uniqueID = UNIQUE_ID_840;

	com.timeout = 0.50;      // in seconds

	com.delayBetweenComTries = 1.0;

	com.inQueueSize  = MAX_COM_STR;
	com.outQueueSize = MAX_COM_STR;
	com.connected = 0;

	// port parameters min and max values
	com.portMax     = MAX_SUPPORTED_COM_PORT;
	com.portMin     = 1;
	com.baudMax     = 19200;
	com.baudMin     = 1200;
	com.parityMax   = 0;
	com.parityMin   = 0;
	com.databitsMax = 8;
	com.databitsMin = 8;
	com.stopbitsMax = 1;
	com.stopbitsMin = 1;

	// test string for com
	com.testStrLen = TEST_STR_LEN;             
	com.testStrResponseLen = TEST_STR_RESPONSE_STR_LEN;
	memcpy(com.testStr, TEST_STR, com.testStrLen);

	com.pResponseValidFunc = TestStrResponseValidFunc;
	com.pComDetectStatusFunc = NULL;

	com.registryKey = COM_840KEY;
	com.autoDetectDisabled = 0;

	if(ReadComConfig(&com) != SUCCESS){
		com.port     = 0;
		com.baud     = com.baudMin;
		com.parity   = com.parityMin;
		com.databits = com.databitsMin;
		com.stopbits = com.stopbitsMin;

		WriteComConfig(&com);
	}

	return;
}

/*****************************************************************************/
/* Name:     TestStrResponseValidFunc                                                      
/*                                                                           
/* Purpose:  Checks if the response to sending the test string is what 
/*  was expected. To autodetect the COM port settings, we send the same test 
/*  string message to the device, each time changing the port settings. Once 
/*  we get a correct response, then we have found the correct 
/*  baud/parity/databits/...
/*                                          
/*                                                                           
/* Example Call:   static int TestStrResponseValidFunc(char* str, int len);                                    
/*                                                                           
/* Input:    char* - pointer to response message string
/*  len - the length of the message string          
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static int TestStrResponseValidFunc(char* str, int len)
{
	// ensure first byte is the ACK from the message we sent
	if(str[0] != ACK){
		return FAILURE;
	}

	// inc past the ACK byte and shorten len too
	str++;
	len--;

	// message must be 6 chars in length
	if(len != 6){
		return FAILURE;
	}

	// ensure the frame sent is correct
	if(Parse840Frame(str, len) != SUCCESS){
		return FAILURE;
	}

	if(str[MESSAGE_ID] == COMMUNICATION_GRANTED_COMMAND){
		lastConnectType = CONNECTED_COMMUNICATION_GRANTED;
	}
	else if(str[MESSAGE_ID] == INVALID_MSG_COMMAND){
		lastConnectType = CONNECTED_INVALID_MSG;
	}
	else{
		lastConnectType = CONNECTED_FAILED;
	}

	// message has to be a COMMUNICATION_GRANTED_COMMAND or a INVALID_MSG_COMMAND.
	// If it is the latter, then the vent is already in service mode
	if(str[MESSAGE_ID] == COMMUNICATION_GRANTED_COMMAND
	|| str[MESSAGE_ID] == INVALID_MSG_COMMAND){
		return SUCCESS;
	}
	else{
		return FAILURE;
	}
}

/*****************************************************************************/
/* Name:     Parse840Frame                                                   
/*                                                                           
/* Purpose:  Parse a 840 frame to ensure the frame format and 
/*  CRC are correct.
/*                                          
/*                                                                           
/* Example Call:  static int Parse840Frame(char* str, int len);                                    
/*                                                                           
/* Input:    char* - pointer to response message string
/*  len - the length of the message string          
/*                                                                           
/* Output:   int - SUCCESS or FAILURE                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static int Parse840Frame(char* str, int len)
{
	unsigned short crc, recv_crc;

	// ensure the length of packet is correct
	if(len - 2 != (str[0] << 8) + (unsigned char)str[1]){
		return FAILURE;
	}

	// ensure the CRC for frame is correct
	crc = CalculateCrc((unsigned char*)str, len - 2);
	recv_crc = (str[len - 2] << 8) + (unsigned char)str[len - 1];

	if(crc != recv_crc){
		return FAILURE;
	}

	return SUCCESS;
}

/*****************************************************************************/
/* Name:     CharsIn840Q                                                     
/*                                                                           
/* Purpose:  Are chars in the serial port in Q waiting to be 
/*  processed?
/*                                          
/*                                                                           
/* Example Call:   int CharsIn840Q(void);                                    
/*                                                                           
/* Input:    None          
/*                                                                           
/* Output:   int - TRUE if chars are in serial Q, FALSE otherwise.                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CharsIn840Q(void)
{
	if(CharsInQ(&com) == TRUE){
		return TRUE;
	}
	else{
		return FALSE;
	}
}

/*****************************************************************************/
/* Name:     ComBreak840                                                  
/*                                                                           
/* Purpose:  Send com break signal to 840.
/*                                          
/*                                                                           
/* Example Call:  int ComBreak840(int breakTime);                                    
/*                                                                           
/* Input:    None       
/*                                                                           
/* Output:   int - LabWindows CVI ComBreak() function return value.                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int ComBreak840(int breakTime)
{
	return ComBreak(com.port, breakTime);
}

/*****************************************************************************/
/* Name:     FlushInQ840                                                     
/*                                                                           
/* Purpose:  Flush all characters from the in queue.
/*                                          
/*                                                                           
/* Example Call:   int FlushInQ840(void);                                    
/*                                                                           
/* Input:    None         
/*                                                                           
/* Output:   int - the byte from the selected port, unless and error occurs. If an
/*      error occurs, the result is a negative CVI error code.                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int FlushInQ840(void)
{
	return ComFlushInQ(&com);
}

/*****************************************************************************/
/* Name:     FlushOutQ840                                                    
/*                                                                           
/* Purpose:  Flush all characters from the out queue.
/*                                          
/*                                                                           
/* Example Call:  int FlushOutQ840(void);                                    
/*                                                                           
/* Input:    None        
/*                                                                           
/* Output:   int - the byte from the selected port, unless and error occurs. If an
/*      error occurs, the result is a negative CVI error code.                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int FlushOutQ840(void)
{
	return ComFlushOutQ(&com);
}

/*****************************************************************************/
/* Name:     RegisterFor840DetectCB                                                    
/*                                                                           
/* Purpose:  Register for a callback during the com detection 
/*  sequence. When the 840 is being detected, it tries numerous different
/*  com ports and speed settings. During this process, the UI needs to 
/*  know what's happening so the user can be informed. Also, if the user
/*  decides to cancel the com code needs to abort the detection process.
/*                                          
/*   The caller can register a function pointer that conforms to the 
/*  ComDetectStatusFuncPtr typedef. The com routines will call this 
/*  function upon changing to each different com port. It will also
/*  send indication on whether the detection was successful. The implementer
/*  of this routine returns 0 if the detection process is to continue, otherwise
/*  a 1 will cause the com detection algorithm to abort early.   
/*                                                                     
/* Example Call:  void RegisterFor840DetectCB(ComDetectStatusFuncPtr func);                                    
/*                                                                           
/* Input:    func - pointer to the callback function.         
/*                                                                           
/* Output:   None                                                        
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void RegisterFor840DetectCB(ComDetectStatusFuncPtr func)
{
	com.pComDetectStatusFunc = func;
	return; // Modified the statement due to Critical Observation.
}

/*****************************************************************************/
/* Name:     GetLastConnectType840                                                   
/*                                                                           
/* Purpose:  Get the type of auto-detect connection that the 840 
/*  connected with. The three types are:
/*                                          
/*  CONNECTED_FAILED - failed to auto-detect 840.
/*  CONNECTED_INVALID_MSG - connected to 840, but it was already in remote mode.
/*  CONNECTED_COMMUNICATION_GRANTED - connected to 840, and we transitioned into
/*      remote mode.
/*                                                                                                                                         
/* Example Call:  int GetLastConnectType840(void);                                    
/*                                                                           
/* Input:             
/*                                                                           
/* Output:   int - one of the three connection types.                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int GetLastConnectType840(void)
{
	return lastConnectType;
}

