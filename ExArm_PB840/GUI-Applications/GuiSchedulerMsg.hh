#ifndef GuiSchedulerMsg_HH
#define GuiSchedulerMsg_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

#include "GuiTimerId.hh"
#include "Keys.hh"
#include "InterTaskMessage.hh"

//=====================================================================
// File: GuiSchedulerMsg - Data structure definition for task message
// VRTX queue messages.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiSchedulerMsg.hhv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 001  By:  yyy    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

union GuiSchedulerMsg
{
	//@ Type: MessageEventType
	// This enum identifies the event type
	enum MessageEventType
	{
    	BREATH_BAR_UPDATE_EVENT = FIRST_APPLICATION_MSG
	};

	struct 
	{
    	Uint   msgType  : 8;
    	Uint   msgId    : 8;
	    Uint   msgData  : 16;
	} GuiSchedulerMessage;

	// Use the following member to simply pass the 32 bits of
	// GuiSchedulerMessage data to message queues.
	Uint32 qWord;
};

#endif // GuiSchedulerMsg_HH
