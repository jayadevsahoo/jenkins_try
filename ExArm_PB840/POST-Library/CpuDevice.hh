#ifndef CpuDevice_HH
#define CpuDevice_HH
 
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  CpuDevice - POST accessor methods to CPU registers
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/CpuDevice.hhv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc    Date:  27-Jan-2011    SCR Number: 6706
//  Project:  XENA2
//  Description:
//		Added IsXena2Config function.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================
 

// Type Definitions 
#include "SigmaTypes.hh"
 
// CPU Types
enum  BoardType
{
    BD_CPU_TYPE  = 0,
    GUI_CPU_TYPE = 1
};

class CpuDevice
{
  public:
           static Uint8     GetBoardRevision(void);
           static void      SetBitsIoReg1(Uint8 mask);
           static void      ClearBitsIoReg1(Uint8 mask);
           static Boolean   IsXena2Config(void);
    inline static Boolean   IsBdCpu(void);
    inline static Boolean   IsGuiCpu(void);
    inline static void      LightDiagnosticLED(Uint8 pattern);
    inline static void      InvalidateBothCaches(void);
    inline static void      ClearWatchdogBite(void);
    static inline void      StrobeWatchdog(void);
    inline static void      EnableAutovector(Uint8 mask);
    inline static Boolean   IsWatchdogBiteOn(void);
    inline static void      ResetAutovector3(void);
    inline static void      ResetAutovector4(void);
    inline static void      ResetAutovector5(void);
    inline static void      ResetAutovector6(void);
    inline static void      ResetAutovectors(void);
    inline static Boolean   IsAudioAckAsserted(void);
    inline static Boolean   IsAudioAttnAsserted(void);
    inline static Boolean   IsAudioBusyAsserted(void);

  private:
    CpuDevice(const CpuDevice&);      // not implemented...
    CpuDevice(void);                       // not implemented...
    ~CpuDevice(void);                      // not implemented...
    void  operator=(const CpuDevice&);     // not implemented...
};
 
#include "CpuDevice.in"

#endif // CpuDevice_HH
