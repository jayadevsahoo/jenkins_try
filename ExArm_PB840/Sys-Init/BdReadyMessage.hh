#ifndef BdReadyMessage_HH
#define BdReadyMessage_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  BdReadyMessage - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdReadyMessage.hhv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
#if 0  
// for bug in genDD
#endif

//@ Usage-Classes
#include "ConfigurationAgent.hh"
#include "Sys_Init.hh"
#include "TaskControlMessage.hh"
#include "SigmaState.hh"
#include "PostAgent.hh"
#include "DataKeyAgent.hh"
#include "ServiceModeAgent.hh"
//@ End-Usage

 
class BdReadyMessage : public TaskControlMessage {
  public:
 
    BdReadyMessage(SigmaState state, Boolean service_required);
    BdReadyMessage(const BdReadyMessage& rBdReady);

    ~BdReadyMessage();

    const char* nameOf(void) const;
    void dispatch(const AppContext& context) const;

    inline SigmaState     getState(void) const;
    inline Boolean        isServiceModeRequested(void) const;
    inline Boolean        isServiceRequired(void) const;
    inline Boolean        areBdSettingsInitialized(void) const;

    inline const PostAgent&          getPostAgent(void) const;
    inline const DataKeyAgent&       getDataKeyAgent(void) const;
    inline const ServiceModeAgent&   getServiceModeAgent(void) const;
    inline const ConfigurationAgent& getConfigurationAgent(void) const;

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);
 
  private:
    BdReadyMessage();                        // declared only
    void operator=(const BdReadyMessage&);   // declared only

    //@ Data-Member:    state_
    //  The node is ready in this state.
    SigmaState             state_;

    //@ Data-Member:    serviceRequired_
    //  Indicates if the node requires service.
    Boolean                serviceRequired_;

    //@ Data-Member:    postAgent_
    //  Contains the PostAgent object from the BD
    //  providing access to various states
    const PostAgent        postAgent_;

    //@ Data-Member:    dataKeyAgent_
    //  Contains the DatakeyAgent object from the BD
    //  providing accessors to serial number and other information
    const DataKeyAgent     dataKeyAgent_;

    //@ Data-Member:    serviceModeAgent_
    //  Contains the ServiceModeAgent object from the BD
    //  providing accessors to Service-Mode subsystem data.
    const ServiceModeAgent serviceModeAgent_;

    //@ Data-Member:    configurationAgent_
    //  Contains the ConfigurationAgent object from the BD
    //  providing accessors to Configuration information from the BD
    const ConfigurationAgent configurationAgent_;

    //@ Data-Member:    bdSettingsInitialized_
    //  Contains the state of BD settings from NovramManager
    const Boolean bdSettingsInitialized_;

};
 
#include "BdReadyMessage.in"

#endif // BdReadyMessage_HH
