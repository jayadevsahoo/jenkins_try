#ifndef SmStatusId_HH
#define SmStatusId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SmStatusId - enum ID's for all test status items
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Service-Mode/vcssrc/SmStatusId.hhv   10.7   08/17/07 10:38:56   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date: 30-Sep-1997    DR Number: DCS 1985
//       Project:  Sigma (840)
//       Description:
//            Assigned specific values to enumerators.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  08-Nov-95    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//====================================================================


//@ Usage-Classes

//@ End-Usage


class SmStatusId
{
public:
	
	//@ Type: TestStatusId
	// This enum lists the possible returned status types
	enum TestStatusId
	{
		  NULL_STATUS_ID = -1

		, TEST_END = 0
		, TEST_START = 1
		, TEST_ALERT = 2
		, TEST_FAILURE = 3
		, TEST_OPERATOR_EXIT = 4
		, NOT_INSTALLED = 5

		, NUM_TEST_DATA_STATUS
	};

	//@ Type: TestConditionId
	// This enum lists the possible returned conditions
	enum TestConditionId
	{
		  NULL_CONDITION_ITEM = 0

		, CONDITION_1  = 1
		, CONDITION_2  = 2
		, CONDITION_3  = 3
		, CONDITION_4  = 4
		, CONDITION_5  = 5
		, CONDITION_6  = 6
		, CONDITION_7  = 7
		, CONDITION_8  = 8
		, CONDITION_9  = 9
		, CONDITION_10 = 10
		, CONDITION_11 = 11
		, CONDITION_12 = 12
		, CONDITION_13 = 13
		, CONDITION_14 = 14
		, CONDITION_15 = 15
		, CONDITION_16 = 16
		, CONDITION_17 = 17
		, CONDITION_18 = 18
		, CONDITION_19 = 19
		, CONDITION_20 = 20

		, NUM_CONDITION_ITEM
	};

};

#endif // SmStatusId_HH 
