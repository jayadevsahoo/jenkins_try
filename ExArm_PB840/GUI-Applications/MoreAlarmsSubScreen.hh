#ifndef MoreAlarmsSubScreen_HH
#define MoreAlarmsSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MoreAlarmsSubScreen - Displays, on the Upper Screen, alarm messages
// supplemental to the Alarm and Status Area, specifically the 3rd through 8th
// highest urgency alarms.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MoreAlarmsSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//====================================================================

#include "SubScreen.hh"
#include "GuiTimerTarget.hh"

//@ Usage-Classes
#include "AlarmMsg.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class SubScreenArea;

class MoreAlarmsSubScreen : public SubScreen, public GuiTimerTarget
{
public:
	MoreAlarmsSubScreen(SubScreenArea* pSubScreenArea);
	~MoreAlarmsSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	void updateMoreAlarmsDisplay(void);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	MoreAlarmsSubScreen(void);							// not implemented...
	MoreAlarmsSubScreen(const MoreAlarmsSubScreen&);	// not implemented...
	void operator=(const MoreAlarmsSubScreen&);			// not implemented...

    //@ Data-Member: alarmMsg3_;
    // Container for the third active alarm slot
    AlarmMsg alarmMsg3_;

    //@ Data-Member: alarmMsg4_;
    // Container for the fourth active alarm slot
    AlarmMsg alarmMsg4_;

    //@ Data-Member: alarmMsg5_;
    // Container for the fifth active alarm slot
    AlarmMsg alarmMsg5_;

    //@ Data-Member: alarmMsg6_;
    // Container for the sixth active alarm slot
    AlarmMsg alarmMsg6_;

    //@ Data-Member: alarmMsg7_;
    // Container for the seventh active alarm slot
    AlarmMsg alarmMsg7_;

    //@ Data-Member: alarmMsg8_;
    // Container for the eighth active alarm slot
    AlarmMsg alarmMsg8_;

};

#endif // MoreAlarmsSubScreen_HH 
