#ifndef ServiceLowerOtherScreensSubScreen_HH
#define ServiceLowerOtherScreensSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceLowerOtherScreensSubScreen - The subscreen selected by the
// Other Screens button on the Lower screen.  Allows access to the
// ServiceMode Setup, External test control, Exh. valve calibration, and
// VentInop test.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceLowerOtherScreensSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By:  rhj	   Date:  24-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:     
//      Added Compact Flash Test Subscreen.
//
//  Revision: 006   By: quf    Date: 23-Jan-2002    DR Number: 5984
//  Project:  GUIComms
//  Description:
//	Added button for Serial Loopback Test.
//
//  Revision: 005  By:  yyy    Date:  01-Sep-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//             Initial release.
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003 By:  yyy    Date:  26-SEP-97    DR Number: 2410
//    Project:  Sigma (R8027)
//    Description:
//      Updated access method for pressure transducer calibrationtest due to changes
//		in service mode.
//
//  Revision: 002  By:  yyy    Date:  30-JUL-97    DR Number: 2313,2279
//    Project:  Sigma (R8027)
//    Description:
//      Added SM_FLOW_SENSOR_CAL_, and SM_TAU_CALIBRATION_ buttons.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"
#include "AdjustPanelTarget.hh"
#include "ServicePromptMgr.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ServiceLowerOtherScreensSubScreen : public SubScreen,
								public AdjustPanelTarget,
								public ServicePromptMgr
{
public:
	ServiceLowerOtherScreensSubScreen(SubScreenArea *pSubScreenArea);
	~ServiceLowerOtherScreensSubScreen(void);

	// Overload SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	void buttonUpHappened(Button *, Boolean);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelRestoreFocusHappened(void);
	virtual void adjustPanelAcceptPressHappened(void);
				
	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	ServiceLowerOtherScreensSubScreen(void);								// not implemented...
	ServiceLowerOtherScreensSubScreen(const ServiceLowerOtherScreensSubScreen&);	// not implemented...
	void operator=(const ServiceLowerOtherScreensSubScreen&);				// not implemented...

	void clearVentInopButtonRequest_(TextButton *pButton);
	
	//@ Data-Member: smSetupButton_;
	// A text button which activates the Service Mode Setup subscreen.
	TextButton smSetupButton_;

	//@ Data-Member: smSystemTestButton_;
	// A text button which activates the Service Mode System Test subscreen.
	TextButton smSystemTestButton_;

	//@ Data-Member: smCalibrationButton_;
	// A text button which activates the Service Mode Calibration subscreen.
	TextButton smCalibrationButton_;

	//@ Data-Member: ventInopButton_
	// A text button which activates the Vent INOP test.
	TextButton ventInopButton_;

	//@ Data-Member: smPressureXducerCalibrationButton_
	// A text button which activates the PressureXducer calibration test.
	TextButton smPressureXducerCalibrationButton_;

	//@ Data-Member: smFlowerSensorCalibrationButton_
	// A text button which activates the Flow Sensor calibration test.
	TextButton smFlowerSensorCalibrationButton_;

	//@ Data-Member: operationHourCopyButton_
	// A text button which activates the dataKey copy test.
	TextButton operationHourCopyButton_;

	//@ Data-Member: serialLoopbackTestButton_
	// A text button which activates the serial loopback test.
	TextButton serialLoopbackTestButton_;

	//@ Data-Member: compactFlashTestButton_
	// A text button that activates the compact flash test
	TextButton compactFlashTestButton_;

	//@ Data-Member: pCurrentButton_
	// The pCurrentButton_ is used for remembering the previously button
	// which was pressed by the user.
	Button *pCurrentButton_;

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

};

#endif // ServiceLowerOtherScreensSubScreen_HH 
