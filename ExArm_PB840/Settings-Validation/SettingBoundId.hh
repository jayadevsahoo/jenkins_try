
#ifndef SettingBoundId_HH
#define SettingBoundId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993_ID, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  SettingBoundId - Enumeration of all of the bounds.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingBoundId.hhv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 022   By: rhj    Date: 13-July-2010   SCR Number: 6581 
//  Project:  XENA2
//  Description:
//       Added DISCO_SENS_NEONATAL_SOFT_MAX_ID.
//   
//  Revision: 021   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//   
//  Revision: 020  By: rhjimen     Date:  01-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Added Trend cursor bounded ids
//
//  Revision: 019  By: gdc     Date:  27-May-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      Added High Ti spont limit setting for NIV SPONT/SIMV.
//
//  Revision: 018  By: gdc     Date:  18-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//
//  Revision: 017  By: gfu     Date:  11-Aug-2004    DR Number: 6132
//  Project:  PAV3
//  Description:
//      Modified code per SDCR #6132.  Also added keyword "Log" to file header
//      as requested during code review.
//
//  Revision: 016  By: scottp     Date:  28-Mar-2002    DR Number: 5790
//  Project:  VCP
//  Description:
//      Added bound id for high insp tidal vol's limit for different modes.
//      Added vt and vs messages limited by Vti.
//
//  Revision: 015   By: hlg   Date:  04-SEP-2001    DR Number: 5931
//  Project:  GUIComms
//  Description:
//      Removed unused functionality for setting bounds on comm
//      port configuation.
//
//  Revision: 014   By: hct   Date:  09-OCT-2000    DR Number: 5493
//  Project:  GUIComms
//  Description:
//      Added single screen compatibility.  Added bound value for
//	ComPortConfigValue.
//
//  Revision: 013  By: sah     Date:  11-Sep-2000    DR Number: 5766
//  Project:  VTPC
//  Description:
//      Added bound id for high insp tidal vol's new soft limit.
//
//  Revision: 012  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added bound ids for new volume support level setting
//      *  added bound ids for peak insp flow's new VC+ bounds
//
//  Revision: 011   By: sah   Date:  27-Apr-2000    DR Number: 5713
//  Project:  NeoMode
//  Description:
//      Added NEONATAL-only, soft bounds, based on the currently-accepted
//      value.
//
//  Revision: 010   By: sah   Date:  18-Apr-2000    DR Number: 5704
//  Project:  NeoMode
//  Description:
//      Fixed comment regarding P-V Loop's constraint definitions.
//
//  Revision: 009   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//  NeoMode project-related changes:
//  *  added new soft bounds, and circuit-specific bounds
//
//  Revision: 008   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//  Added support for new applicability functionality:
//  *  standardized the names of the "BASED" bound ids, for more
//     readability
//  *  added new BiLevel-specific bound ids
//
//  Revision: 007   By: sah   Date:  04-Feb-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//  Project-related changes:
//  *  added new ATC-specific bound ids
//
//  Revision: 006   By: dosman    Date: 30-Sep-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//  removed extraneous prompt bound
//
//  Revision: 005   By: dosman    Date: 10-Aug-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//  --- Merged with Color HardBoundId.hh
//
//  Revision: 004   By: dosman    Date: 28-Apr-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//     added constants for when hit the following in bilevel:
//  inspiratory time max/min,
//  expiratory time max/min, 
//  I:E ratio high soft bound
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//  Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: dosman    Date: 19-Feb-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//     added constants for when hit min HighCctPress based on PeepHigh
//
//  Revision: 001   By: dosman    Date: 06-Jan-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//  Initial BiLevel version.
//  Based on 'HardBoundId.hh', revision '1.28'.
//  replaced magic numbers with symbolic constants
//  added constant ids for PeepHigh
//
//====================================================================

//@ Usage-Classes
//@ End-Usage


//@ Type:  SettingBoundId
// Enumeration of all the bounds.
enum SettingBoundId
{
	NULL_SETTING_BOUND_ID = -1,		  // undefined bound id

	// ====================================================================
	//  bound ids...
	// ===================================================================

	ALARM_VOL_MIN_ID,				 // alarm volume min.
	ALARM_VOL_MAX_ID,				 // alarm volume max.

	APNEA_FLOW_PATT_BASED_IE_MAX_ID,// flow pattern based on max. I:E ratio
	APNEA_FLOW_PATT_BASED_TI_MAX_ID,// flow pattern based on max. insp. time
	APNEA_FLOW_PATT_BASED_TI_MIN_ID,// flow pattern based on min. insp. time

	APNEA_IE_RATIO_MAX_ID,			 // apnea I:E ratio absolute max.

	APNEA_INSP_PRESS_MIN_ID,		 // apnea Pi absolute min.
	APNEA_INSP_PRESS_MAX_BASED_PEEP_ID,// apnea Pi <= (90 - PEEP)
	APNEA_INSP_PRESS_MAX_BASED_PEEPL_ID,// apnea Pi <= (90 - PEEPl)
	APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEP_ID,// apnea Pi <= (Pcirc - PEEP - 2)
	APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEPL_ID,// apnea Pi <= (Pcirc - PEEPl - 2)
	APNEA_INSP_PRESS_MAX_ID,		 // apnea Pi absolute max.

	APNEA_INSP_TIME_MIN_ID,			 // apnea insipiratory time absolute min.
	APNEA_INSP_TIME_MAX_ID,			 // apnea inspiratory time absolute max.

	APNEA_INTERVAL_MIN_ID,			 // apnea interval absolute min.
	APNEA_INTERVAL_MIN_BASED_APNEA_RR_ID,// apnea interval >= (60/apnea RR)
	APNEA_INTERVAL_MAX_ID,			 // apnea interval absolute max.

	APNEA_O2_MIN_ID,				 // apnea oxygen absolute min.
	APNEA_O2_MIN_BASED_O2_ID,		 // apnea oxygen >= O2%
	APNEA_O2_MAX_ID,				 // apnea oxygen absolute max.

	APNEA_PEAK_FLOW_MIN_BASED_CCT_TYPE_ID,// apnea peak flow circuit type min.
	APNEA_PEAK_FLOW_MAX_BASED_CCT_TYPE_ID,// apnea peak flow circuit type max.

	APNEA_RESP_RATE_MIN_ID,			 // apnea respiratory rate absolute min.
	APNEA_RESP_RATE_MIN_BASED_INTERVAL_ID,// apnea respiratory rate >= (60/Tai) bpm
	APNEA_RESP_RATE_MAX_ID,			 // apnea respiratory rate absolute max.

	APNEA_TIDAL_VOL_MIN_ID,			 // apnea tidal volume absolute min.
	APNEA_TIDAL_VOL_MIN_BASED_IBW_ID,// apnea tidal volume's IBW-based min.
	APNEA_TIDAL_VOL_SOFT_MIN_BASED_IBW_ID,// apnea tidal vol's IBW-based soft min.
	APNEA_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID,// apnea tidal vol's IBW-based soft max.
	APNEA_TIDAL_VOL_MAX_BASED_IBW_ID,// apnea tidal volume's IBW-based max.
	APNEA_TIDAL_VOL_MAX_ID,			 // apnea tidal volume absolute max.

	ATM_PRESSURE_MIN_ID,			 // atmospheric pressure absolute min.
	ATM_PRESSURE_MAX_ID,			 // atmospheric pressure absolute max.

	DCI_DATA_BITS_BASED_PARITY_ID,	 // DCI data bits can't be 8-bits with parity

	DCI_PARITY_MODE_BASED_BITS_ID,	 // DCI parity mode can't be active with 8-bit

	DISPLAY_BRIGHTNESS_MIN_ID,		 // display brightness absolute min.
	DISPLAY_BRIGHTNESS_MAX_ID,		 // display brightness absolute max.

	DISPLAY_CONTRAST_SCALE_MIN_ID,	 // display contrast scale absolute min.
	DISPLAY_CONTRAST_SCALE_MAX_ID,	 // display contrast scale absolute max.

	DISPLAY_CONTRAST_DELTA_MIN_ID,	 // display contrast delta absolute min.
	DISPLAY_CONTRAST_DELTA_MAX_ID,	 // display contrast delta absolute max.

	DISCO_SENS_MIN_ID,				 // disconnection sensitivity absolute min.
	DISCO_SENS_VENT_BASED_MAX_ID,	 // disconnection sensitivity max based on vent type.
	DISCO_SENS_MAX_ID,				 // disconnection sensitivity absolute max.
	DISCO_SENS_NEONATAL_SOFT_MAX_ID, // disconnection sensitivity NeoNatal based, "soft" max.

	EXP_SENS_MIN_ID,				 // expiratory sensitivity absolute min.
	EXP_SENS_MAX_ID,				 // expiratory sensitivity absolute max.

	EXP_TIME_MIN_ID,		 // expiratory time minimum

	FLOW_ACCEL_PERCENT_MIN_ID,		 // flow acceleration percent absolute min.
	FLOW_ACCEL_PERCENT_MAX_ID,		 // flow acceleration percent absolute max.

	FLOW_PATTERN_BASED_IE_MAX_ID,	 // flow pattern based on max. I:E ratio
	FLOW_PATTERN_BASED_TI_MAX_ID,	 // flow pattern based on max. insp. time
	FLOW_PATTERN_BASED_TI_MIN_ID,	 // flow pattern based on min. insp. time
	FLOW_PATTERN_BASED_TE_MIN_ID,	 // flow pattern based on min. exp. time

	FLOW_PLOT_SCALE_MIN_ID,			 // flow plot scale min.
	FLOW_PLOT_SCALE_MAX_ID,			 // flow plot scale max.

	FLOW_SENS_MIN_ID,				 // flow sensitivity absolute min.
	FLOW_SENS_MAX_ID,				 // flow sensitivity absolute max.

	HIGH_CIRC_MIN_BASED_LOW_ID,		 // high Ppeak > low Ppeak
	HIGH_CIRC_MIN_BASED_PEEP_ID,	 // high Pcirc >= (PEEP + 2)
	HIGH_CIRC_MIN_BASED_PEEPL_ID,	 // high Pcirc >= (PEEPl + 2)
	HIGH_CIRC_MIN_BASED_PSUPP_PEEP_ID,// high Pcirc >= (PEEP + Psupp + 2)
	HIGH_CIRC_MIN_BASED_PSUPP_PEEPL_ID,// high Pcirc >= (PEEPl + Psupp + 2)
	HIGH_CIRC_MIN_BASED_INSP_PRESS_PEEP_ID,// high Pcirc >= (PEEP + Pi + 2)
	HIGH_CIRC_MIN_BASED_PEEPH_ID,	 // high Pcirc >= (PEEPh + 2) 
	HIGH_CIRC_MAX_ID,				 // high circuit pressure absolute max.

	HIGH_EXH_MIN_VOL_MIN_ID,		 // high exhaled minute volume absolute min.
	HIGH_EXH_MIN_VOL_MIN_BASED_LOW_ID,// high exh. minute volume below low exh. minute volume
	HIGH_EXH_MIN_VOL_MAX_ID,		 // high exhaled minute volume absolute max.

	HIGH_EXH_TIDAL_VOL_MIN_ID,				  // high-Vte absolute min.
	HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_MAND_ID, // high-Vte > low-Vte-mand
	HIGH_EXH_TIDAL_VOL_MIN_BASED_LOW_SPONT_ID,// high-Vte > low-Vte-spont
	HIGH_EXH_TIDAL_VOL_MAX_ID,				  // high-Vte absolute max.
	// Mand softbound uses generic HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID due 
	// to restriction in Settings-Validation    
	HIGH_INSP_TIDAL_VOL_MIN_MAND_ID,		   // high-Vti-mand absolute min.       
	HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_MAND_ID, // high-Vti-mand >= (5.0 * IBW)      

	HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_MAND_ID, // high-Vti-mand <= (45.7 * IBW)     
	HIGH_INSP_TIDAL_VOL_MAX_MAND_ID,			// high-Vti-mand absolute max.       


	HIGH_INSP_TIDAL_VOL_MIN_ID,			  // high-Vti absolute min.
	HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_ID, // high-Vti >= (5.0 * IBW)
	HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID,// high-Vti <= (20.0 * IBW)
	HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_ID, // high-Vti <= (45.7 * IBW)
	HIGH_INSP_TIDAL_VOL_MAX_ID,			  // high-Vti absolute max.

	HIGH_INSP_TIDAL_VOL_MIN_SPONT_ID,					// absolut min in spont mode.
	HIGH_INSP_TIDAL_VOL_MIN_BASED_IBW_SPONT_ID,			// min based wieghtin spont mode. 
	// Spont softbound uses generic HIGH_INSP_TIDAL_VOL_SOFT_MAX_BASED_IBW_ID due 
	// to restriction in Settings-Validation    
	HIGH_INSP_TIDAL_VOL_MAX_BASED_IBW_SPONT_ID,			// max based on wieght in spont mode.
	HIGH_INSP_TIDAL_VOL_MAX_SPONT_ID,					// Absolut max in spont mode.

	HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL_ID,		  // high-Vti min based on current tidal volume.
	HIGH_INSP_TIDAL_VOL_MIN_BASED_TIDAL_VOL_MAND_ID,  //  High-Vti min based on tidal volume.
	HIGH_INSP_TIDAL_VOL_MIN_BASED_VOL_SUP_SPONT_ID,	  //  Hight-Vti min based on volumen support in spont mode


	HIGH_RESP_RATE_MIN_ID,			 // high respiratory rate absolute min.
	HIGH_RESP_RATE_MAX_ID,			 // high respiratory rate absolute max.

	HL_RATIO_SOFT_MAX_ID,			 // TH:TL time ratio "soft" maximum...
	HL_RATIO_MAX_ID,				 // TH:TL time ratio maximum...

	HUMID_VOLUME_MIN_ID,			 // humidifier volume absolute min.
	HUMID_VOLUME_MAX_ID,			 // humidifier volume absolute max.

	IBW_MIN_BASED_CCT_TYPE_ID,		 // IBW minimum based on circuit type
	IBW_SOFT_MIN_BASED_CCT_TYPE_ID,	 // IBW "soft" minimum based on circuit type
	IBW_SOFT_MAX_BASED_CCT_TYPE_ID,	 // IBW "soft" maximum based on circuit type
	IBW_MAX_BASED_CCT_TYPE_ID,		 // IBW maximum based on circuit type

	IE_RATIO_SOFT_MAX_ID,			 // I:E time ratio "soft" maximum...
	IE_RATIO_MAX_ID,				 // I:E time ratio maximum...

	INSP_PRESS_MIN_ID,				 // inspiratory pressure absolute min.
	INSP_PRESS_MAX_BASED_PEEP_ID,	 // inspiratory pressure <= (90 - PEEP)
	INSP_PRESS_MAX_BASED_PCIRC_PEEP_ID,// insp. pressure <= (Pcirc - PEEP - 2)
	INSP_PRESS_MAX_ID,				 // inspiratory pressure absolute max.

	INSP_TIME_MIN_ID,				 // inspiratory time absolute min.
	INSP_TIME_MIN_BASED_PLAT_TIME_ID,// inspiratory time min based on Tpl
	INSP_TIME_MAX_ID,				 // inspiratory time absolute max.

	LOW_CIRC_MIN_ID,				 // low circuit pressure abs. min.
	LOW_CIRC_MIN_BASED_PEEP_ID,		 // low circuit pressure min based on PEEP
	LOW_CIRC_MAX_BASED_HIGH_ID,		 // low circuit pressure < high
	LOW_CIRC_MAX_ID,				 // low circuit pressure abs. max.

	LOW_EXH_MAND_TIDAL_MIN_ID,	 // low exhaled Mandatory tidal volume abs. min.
	LOW_EXH_MAND_TIDAL_MAX_BASED_HIGH_ID,// low exhaled mandatory tidal volume < high
	LOW_EXH_MAND_TIDAL_MAX_ID,	 // low exhaled Mandatory tidal volume abs. max.

	LOW_EXH_MIN_VOL_MIN_ID,			 // low exhaled minute volume abs. min.
	LOW_EXH_MIN_VOL_SOFT_MIN_ID,	 // low exhaled minute volume soft min.
	LOW_EXH_MIN_VOL_MAX_BASED_HIGH_ID,// low exhaled minute volume < high
	LOW_EXH_MIN_VOL_MAX_ID,			 // low exhaled minute volume abs. max.

	LOW_EXH_SPONT_TIDAL_MIN_ID,	 // low exhaled spontaneous tidal volume abs. min.
	LOW_EXH_SPONT_TIDAL_MAX_BASED_HIGH_ID,// low exhaled spontaneous tidal volume < high
	LOW_EXH_SPONT_TIDAL_MAX_ID,	 // low exhaled spontaneous tidal volume abs. max.

	O2_PERCENT_MIN_ID,				 // oxygen percentage absolute min.
	O2_PERCENT_MAX_ID,				 // oxygen percentage absolute max.

	PEAK_INSP_FLOW_MIN_BASED_CCT_TYPE_ID,// peak insp flow circuit type min.
	PEAK_INSP_FLOW_MAX_BASED_CCT_TYPE_ID,// peak insp flow circuit type max.
	PEAK_INSP_FLOW_MIN_BASED_VCP_ID, // peak insp flow VC+ min.
	PEAK_INSP_FLOW_MAX_BASED_VCP_ID, // peak insp flow VC+ max.

	PERCENT_SUPPORT_MIN_ID,			 // percent support absolute min.
	PERCENT_SUPPORT_MAX_ID,			 // percent support absolute max.
	PERCENT_SUPPORT_SOFT_MAX_ID,	 // percent support max soft bound

	PLATEAU_MIN_ID,					 // plateau time absolute min.
	PLATEAU_MAX_ID,					 // plateau time absolute max.

	PEEP_MIN_ID,					 // PEEP absolute min.
	PEEP_SOFT_MIN_BASED_ACCEPTED_ID, // PEEP >= (PEEP-accepted - 2)
	PEEP_MAX_BASED_PCIRC_ID,		 // PEEP <= (Pcirc - 2)
	PEEP_MAX_BASED_PCIRC_PI_ID,		 // PEEP <= (Pcirc - Pi - 2)
	PEEP_MAX_BASED_PI_ID,			 // PEEP <= (90 - Pi)
	PEEP_MAX_BASED_PCIRC_PSUPP_ID,	 // PEEP <= (Pcirc - Psupp - 2)
	PEEP_MAX_BASED_PSUPP_ID,		 // PEEP <= (90 - Psupp)
	PEEP_SOFT_MAX_BASED_ACCEPTED_ID, // PEEP <= (PEEP-accepted + 2)
	PEEP_MAX_ID,					 // PEEP absolute max.

	PEEP_HIGH_MIN_ID,				 // PEEPh absolute min.
	PEEP_HIGH_MIN_BASED_PEEPL_ID,	 // PEEPh >= (PEEPl + 5)
	PEEP_HIGH_MAX_BASED_PCIRC_ID,	 // PEEPh <= (Pcirc - 2)
	PEEP_HIGH_MAX_ID,				 // PEEPh absolute max.

	PEEP_HIGH_TIME_MIN_ID,			 // PEEP high time absolute min.
	PEEP_HIGH_TIME_MAX_ID,			 // PEEP high time absolute max.

	PEEP_LOW_MIN_ID,				 // PEEPl absolute min. 
	PEEP_LOW_MAX_BASED_PCIRC_ID,	 // PEEPl <= (Pcirc - 2)
	PEEP_LOW_MAX_BASED_PCIRC_PSUPP_ID,// PEEPl <= (Pcirc - Psupp - 2)
	PEEP_LOW_MAX_BASED_PSUPP_ID,	 // PEEPl <= (90 - Psupp)
	PEEP_LOW_MAX_BASED_PEEPH_ID,	 // PEEPl <= (PEEPh - 5)
	PEEP_LOW_MAX_ID,				 // PEEPl absolute max. 

	PEEP_LOW_TIME_MIN_ID,			 // PEEP low time absolute min.

	PRESS_SENS_MIN_ID,				 // pressure sensitivity absolute min.
	PRESS_SENS_MAX_ID,				 // pressure sensitivity absolute max.

	PRESS_PLOT_SCALE_MIN_ID,		 // pressure plot scale min.
	PRESS_PLOT_SCALE_MAX_ID,		 // pressure plot scale max.

	PRESS_SUPP_MIN_ID,				 // press. support absolute min.
	PRESS_SUPP_MAX_BASED_PEEP_ID,	 // Psupp <= (90 - PEEP)
	PRESS_SUPP_MAX_BASED_PEEPL_ID,	 // Psupp <= (90 - PEEPl)
	PRESS_SUPP_MAX_BASED_PCIRC_PEEP_ID,// Psupp <= (Pcirc - PEEP - 2)
	PRESS_SUPP_MAX_BASED_PCIRC_PEEPL_ID,// Psupp <= (Pcirc - PEEPl - 2)
	PRESS_SUPP_MAX_ID,				 // press. support absolute max.

	PRESS_VOL_LOOP_BASE_MIN_ID,		 // minimum P-V loop baseline
	PRESS_VOL_LOOP_BASE_MAX_ID,		 // maximum P-V loop baseline

	RESP_RATE_MIN_ID,				 // respiratory rate absolute min.
	RESP_RATE_MAX_ID,				 // respiratory rate absolute max.

	TIDAL_VOL_MIN_ID,				 // tidal volume absolute min.
	TIDAL_VOL_MIN_BASED_IBW_ID,		 // tidal volume's IBW-based min.
	TIDAL_VOL_SOFT_MIN_BASED_IBW_ID, // tidal volume's IBW-based soft min.
	TIDAL_VOL_SOFT_MAX_BASED_IBW_ID, // tidal volume's IBW-based soft max.
	TIDAL_VOL_MAX_BASED_IBW_ID,		 // tidal volume's IBW-based max.
	TIDAL_VOL_MAX_ID,				 // tidal volume absolute max.

	TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL_ID,		   // max tidal vol based on vti  
	TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL_MAND_ID,   // max tidal vol based on vti under mand (VCP);   


	TIME_PLOT_SCALE_MIN_ID,			 // time plot scale min.
	TIME_PLOT_SCALE_MAX_ID,			 // time plot scale max.

	TUBE_ID_MIN_ID,					 // tube size absolute min.
	TUBE_ID_SOFT_MIN_ID,			 // tube size IBW-based, "soft" min.
	TUBE_ID_SOFT_MAX_ID,			 // tube size IBW-based, "soft" max.
	TUBE_ID_MAX_ID,					 // tube size absolute max.

	VOLUME_PLOT_SCALE_MIN_ID,		 // volume plot scale min.
	VOLUME_PLOT_SCALE_MAX_ID,		 // volume plot scale max.

	VOLUME_SUPP_MIN_ID,				   // volume support absolute min.
	VOLUME_SUPP_MIN_BASED_IBW_ID,	   // volume support's IBW-based min.
	VOLUME_SUPP_SOFT_MIN_BASED_IBW_ID, // volume support's IBW-based soft min.
	VOLUME_SUPP_SOFT_MAX_BASED_IBW_ID, // volume support's IBW-based soft max.
	VOLUME_SUPP_MAX_BASED_IBW_ID,	   // volume support's IBW-based max.
	VOLUME_SUPP_MAX_ID,				   // volume support absolute max.

	VOL_SUP_MAX_BASED_HIGH_INSP_TIDAL_VOL_SPONT_ID,	// max Vtsupp based on vti under spont (VS);    

	YEAR_MAX_ID,			 // Year absolute max.
	YEAR_MIN_ID,			 // Year absolute min.

	HIGH_SPONT_INSP_TIME_MIN_ID,	   // high spont insp time absolute min
	HIGH_SPONT_INSP_TIME_MAX_BASED_IBW_ID, // high spont insp time based on IBW

	TREND_CURSOR_POSITION_MIN_ID,	   // Trend cursor minimum position
	TREND_CURSOR_POSITION_MAX_ID,	   // Trend cursor maximum position

	IBW_MAX_BASED_ON_APNEA_TIDAL_VOL_ID,// IBW max based on apnea tidal volume
	IBW_MIN_BASED_ON_APNEA_TIDAL_VOL_ID,// IBW min based on apnea tidal volume
	IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID, // IBW <= high-Vti-mand / 5.0
	IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID, // IBW >= high-Vti-mand / 45.7
	IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_ID, // IBW <= high-Vti / 5.0
	IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_ID, // IBW >= high-Vti / 45.7
	IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID,		// IBW max based on high Vti spont
	IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID,		// IBW min based on high Vti spont 
	IBW_MAX_BASED_ON_TIDAL_VOL_ID,		// IBW max based on Vt
	IBW_MIN_BASED_ON_TIDAL_VOL_ID,		 // IBW min based on Vt
	IBW_MAX_BASED_ON_VOLUME_SUPP_ID,   // IBW max based on volume support 
	IBW_MIN_BASED_ON_VOLUME_SUPP_ID,   // IBW min based on volume support 
	IBW_MIN_BASED_ON_ATC_ID, // IBW min based on support type = ATC
	IBW_MIN_BASED_ON_PAV_ID, // IBW min based on support type = PAV

	NUM_SETTING_BOUND_IDS
};


#endif // SettingBoundId_HH 
