#ifndef BackgroundFaultHandler_IN
#define BackgroundFaultHandler_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//  Class: BackgroundFaultHandler - independent task for reporting and
//                                  processing of system failures.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundFaultHandler.inv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeStateCallback
//
//@ Interface-Description
//  This method accepts a ChangeStateMessage reference and returns
//  no value.
//  This method replies to Sys_Init Subsystem after a changed state
//  has occurred.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method response to Sys-Init Subsystem as to being ready for
//  to change into the new system state.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

inline void
BackgroundFaultHandler::ChangeStateCallback( const ChangeStateMessage& rMessage )
{
    // $[TI1]
    TaskControlAgent::ReportTaskReady( rMessage );
}

//============================ M E T H O D   D E S C R I P T I O N ============
//@ Method:  GetAssociatedBtat
//
//@ Interface-Description
//  This method accepts an enum BkEventName and returns its associated
//  OperatingParameterEventName.
//
//  This method returns the associated BTAT for the input BkEventName.
//----------------------------------------------------------------------------
//@ Implementation-Description
//  This method returns the alarm type associated with the input BkEventName.
//----------------------------------------------------------------------------
//@ PreCondition
//  eventName < BK_MAX_EVENT
//----------------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=============================================================================
 
inline OperatingParameterEventName
BackgroundFaultHandler::GetAssociatedBtat( const enum BkEventName eventName )
{
     CLASS_PRE_CONDITION( eventName < BK_MAX_EVENT );
 
    // $[TI1]
    return( BK_EVENT_DEFINITON_TABLE_[ eventName ].alarmType );
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

#endif // BackgroundFaultHandler_IN 

