//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIUserFloat.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "PatientDataUIUserFloat.h"

//-------------------------------------
// C O D E
//-------------------------------------


UserFloat::UserFloat(char *pName,float f):Value(pName),m_fValue(f)
{
}
    

void UserFloat::updatePacket(void *address)
{
    if(m_bIsDesired)
    {
        ((unsigned int *)address)[0]=htonl(*((unsigned int *)(&m_fDesiredValue)));
    }
}

bool UserFloat::updateValue(float fIn)
{
    m_bIsSet=true;
    m_fValue=fIn;
    return true;
}

float UserFloat::getFloatValue()
{
    return m_fValue;
}

bool UserFloat::isFloat()
{
    return true;
}

bool UserFloat::setFloat(float fValue)
{
    m_fDesiredValue  = fValue;
    m_bIsDesired     = true;
    return true;    
}
