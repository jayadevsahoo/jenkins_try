#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Btps - BTPS compensation
//---------------------------------------------------------------------
//@ Interface-Description
//		This is a static class. This class has methods that compute the
//  	exhalation BTPS factor used to compensate exhaled volume and the
//  	inspiration BTPS factor used to compensate delivered volume.  This
//  	class also implements access methods to obtain these factors.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements BTPS factor compensation algorithm.
//---------------------------------------------------------------------
//@ Implementation-Description
//		This class has two static data members to store results from the
//  	Btps calculations.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		no instances.  Static class.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Btps.ccv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  syw    Date:  15-May-1997    DR Number: DCS 1620
//       Project:  Sigma (R8027)
//       Description:
//       	Moved CalculateBtps to .cc since code added is no longer an inline
//			method.  Limit InspBtpsCf_ and ExpBtpsCf_ to be > 0.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Btps.hh"
#include "Barometer.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

// $[04055]
const Real32 STD_ATM_PRESSURE = 1033.66F ; 	// cmH2O
const Real32 VAPOR_PRESS_BTPS = 64.037F ; 	// cmH2O
const Real32 VAPOR_PRESS_GAS = 5.866F ; 	// cmH2O
const Real32 TEMPERATURE_SLPM = 294.25F ; 	// K
const Real32 TEMPERATURE_BTPS = 310.15F ; 	// K
const Real32 BODY_TEMPERATURE = 310.15F ; 	// K
const Real32 STD_TEMPERATURE = 294.25F ; 	// K

// initialization of static variables
Real32 Btps::ExpBtpsCf_ = 1.0F ;
Real32 Btps::InspBtpsCf_ = 1.0F ; 

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CalculateBtps
//
//@ Interface-Description
//		This method takes no argument and has no return value.  This method
//      updates the inspiration and exhalation dry to BTPS condition factor
//		used in tidal volume computation.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The BTPS factor is calculated for inspiration and exhalation.
//		The results of the calculation are stored in InspBtpsCf_ and
//		ExpBtpsCf_ and both must be >= 0.  
// 		$[04055] $[04058]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void 
Btps::CalculateBtps( void)
{
	CALL_TRACE("Btps::CalculateBtps( void)") ;
	
	Real32 atmPressure = RAtmosphericPressureSensor.getValue() ;

    InspBtpsCf_ = (atmPressure / STD_ATM_PRESSURE)
    				 * (atmPressure - VAPOR_PRESS_BTPS)
    				 / (atmPressure - VAPOR_PRESS_GAS)
    				 * (TEMPERATURE_SLPM / TEMPERATURE_BTPS) ;
                  
    ExpBtpsCf_ = (BODY_TEMPERATURE * STD_ATM_PRESSURE)
    				 / (STD_TEMPERATURE * (atmPressure - VAPOR_PRESS_BTPS)) ;

	if (InspBtpsCf_ < 0.0)
	{
	   	// $[TI1]
		InspBtpsCf_ = 0.0 ;
	}			 
   	// $[TI2]

	if (ExpBtpsCf_ < 0.0)
	{
	   	// $[TI3]
		ExpBtpsCf_ = 0.0 ;
	}			 
   	// $[TI4]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Btps::SoftFault( const SoftFaultID  softFaultID,
                 const Uint32       lineNumber,
				 const char*        pFileName,
				 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, BTPS,
                             lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================





