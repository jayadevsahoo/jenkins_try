//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUISinSignal.h
//----------------------------------------------------------------------------

#ifndef SINSIGNAL_H
#define SINSIGNAL_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "PatientDataUIUserFloat.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class SinSignal:public UserFloat
{
    public:
    SinSignal(char *pName, unsigned int &uiTime, 
        double dFrequency= 1.0,
        double dPhase    = 0.0,
        double dMin      =40.0,
        double dMax      =70.0
        );



    void updatePacket(void *address);

    bool isWaveform();
    bool isSinSignal();


    bool setSinSignal(double dFrequency,double dPhase,double dMin,double dMax);
    bool setFloat(float fValue);

    double getFrequency();
    double getPhase();
    double getMax();
    double getMin();


    bool unsetValue();

    private:
    bool   m_bIsDesired;
    double m_dMax     ;
    double m_dMin     ;
    double m_dFreqency;
    double m_dPhase   ;
    double m_dRange   ;
    double m_dTime    ;
    unsigned int &m_uiTime;
};

#endif

