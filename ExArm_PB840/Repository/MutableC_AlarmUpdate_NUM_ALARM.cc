#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  MutableC_AlarmUpdate_NUM_ALARM - Mutable Fixed Sorted List of 'AlarmUpdate' Items.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides an abstraction for a sorted list of copies of
//  data items each of type 'AlarmUpdate'.  The data items of the list are
//  copies of the data items that are added to the list.  This is a
//  fixed list where, at most, 'NUM_ALARM' items can be on the list at
//  one time.  Unlike 'SortC_AlarmUpdate_NUM_ALARM' -- this class's base
//  class -- this list allows the alteration of the items within the
//  list -- hence, the "mutable" in its name.  Altering any of this
//  list's items may result in a list where the items are not sorted,
//  therefore a 'reSort()' method is provided.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a sorted collection of sortable objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Items are sorted as they're inserted into the list.  A
//  doubly-linked list of copies is used internally to keep the items.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions namely have to do with the data type that is
//  chosen for this list.  The data type must provide the following
//  mechanisms, to be allowed to be used in this list:
//>Von
//      AlarmUpdate::AlarmUpdate(const AlarmUpdate& aType);            // copy constructor...
//      AlarmUpdate::operator=(const AlarmUpdate& aType);       // assignment operator...
//      AlarmUpdate::operator==(const AlarmUpdate& aType) const;// equivalence operator...
//      AlarmUpdate::operator!=(const AlarmUpdate& aType) const;// inequivalence operator...
//      AlarmUpdate::operator>(const AlarmUpdate& aType) const; // greater-than operator...
//      AlarmUpdate::operator<(const AlarmUpdate& aType) const; // less-than operator...
//>Voff
//  For primitive (built-in) types these are already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      The assignment operator of any class used as a type of this list
//      must be implemented such that upon completion of an assignment
//      between two instances, the instances are equivalent (i.e.
//      'a = b; POST_CONDITION(a == b);').
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedMutableSortC.C_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "MutableC_AlarmUpdate_NUM_ALARM.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  MutableC_AlarmUpdate_NUM_ALARM(sortList)  [Copy Constructor]
//
//@ Interface-Description
//  Construct this sorted list using 'sortList'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The base class will handle all of the initialization.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == sortList  &&  (isEmpty()  ||
//                           currentItem() == sortList.currentItem()))
//@ End-Method
//=====================================================================

MutableC_AlarmUpdate_NUM_ALARM::MutableC_AlarmUpdate_NUM_ALARM(
			       const MutableC_AlarmUpdate_NUM_ALARM& sortList
							  )
				 : SortC_AlarmUpdate_NUM_ALARM(sortList)
{
  CALL_TRACE("MutableC_AlarmUpdate_NUM_ALARM(sortList)");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  MutableC_AlarmUpdate_NUM_ALARM(sortList)  [Conversion Constructor]
//
//@ Interface-Description
//  Construct this sorted list using 'sortList'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The base class will handle all of the initialization.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == sortList  &&  (isEmpty()  ||
//                           currentItem() == sortList.currentItem()))
//@ End-Method
//=====================================================================

MutableC_AlarmUpdate_NUM_ALARM::MutableC_AlarmUpdate_NUM_ALARM(
				       const SortC_AlarmUpdate_NUM_ALARM& sortList
							  )
				 : SortC_AlarmUpdate_NUM_ALARM(sortList)
{
  CALL_TRACE("MutableC_AlarmUpdate_NUM_ALARM(sortList)");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  MutableC_AlarmUpdate_NUM_ALARM()  [Default Constructor]
//
//@ Interface-Description
//  Create an empty sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The base class will handle all of the initialization.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

MutableC_AlarmUpdate_NUM_ALARM::MutableC_AlarmUpdate_NUM_ALARM(void)
				: SortC_AlarmUpdate_NUM_ALARM()
{
  CALL_TRACE("MutableC_AlarmUpdate_NUM_ALARM()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~MutableC_AlarmUpdate_NUM_ALARM()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

MutableC_AlarmUpdate_NUM_ALARM::~MutableC_AlarmUpdate_NUM_ALARM(void)
{
  CALL_TRACE("~MutableC_AlarmUpdate_NUM_ALARM()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSorted()  [const, virtual]
//
//@ Interface-Description
//  This method overrides the base class's pure virtual method, and
//  returns a boolean indicator as to whether this list is in a
//  currently sorted state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a mutable sorted list, therefore there is a chance that
//  the items are out-of-order.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
MutableC_AlarmUpdate_NUM_ALARM::isSorted(void) const
{
  CALL_TRACE("isSorted()");

  Boolean  isSortedFlag = TRUE;

  if (!isEmpty())
  {   // $[TI1]
    // this sorted list is not empty, therefore check to see if there is
    // any nodes that are out-of-order...
    const DNodeC_AlarmUpdate* const  P_FIRST   = getFirst_();
    const DNodeC_AlarmUpdate*        pCurrNode = P_FIRST;
    const DNodeC_AlarmUpdate*        pNextNode;

    // while the nodes are sorted and we haven't looped around to the
    // first node, see if 'pCurrNode' is sorted in relation to the next
    // node ('pNextNode')...
    while (isSortedFlag  &&  (pNextNode = pCurrNode->getNextPtr()) != P_FIRST)
    {   // $[TI1.1] -- at least one iteration...
      // $[TI1.1.1] (TRUE) $[TI1.1.2] (FALSE)
      isSortedFlag = (pCurrNode->getItem() <  pNextNode->getItem()  ||
		      pCurrNode->getItem() == pNextNode->getItem());
      pCurrNode = pNextNode;    // move 'pCurrNode' to the next node...
    }   // $[TI1.2] -- no iterations (there is only one item in this list)...
  }   // $[TI2] -- this list is currently empty...

  return(isSortedFlag);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  reSort()
//
//@ Interface-Description
//  Re-sort this list of sortable items.  The current item is not
//  necessarily the same after re-sorting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use a local sorted list to sort all of the items from this list.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isSorted())
//@ End-Method
//=====================================================================

void
MutableC_AlarmUpdate_NUM_ALARM::reSort(void)
{
  CALL_TRACE("reSort()");

  if (!isEmpty())
  {   // $[TI1]
    // this list is NOT empty, therefore, using a local sorted list, sort
    // this list...
    DListC_AlarmUpdate_NUM_ALARM&       rElemList = getElemList_();
    MutableC_AlarmUpdate_NUM_ALARM  tmpList;

    // starting from the first item in this sorted list...
    rElemList.goFirst();

    // add each item from this list to 'tmpList' -- where they are sorted,
    // then go to the next item in this list...
    do
    {
      tmpList.addItem(rElemList.currentItem());
    } while (rElemList.goNext() == SUCCESS);

    // copy the newly sorted list into this list...
    *this = tmpList;
  }   // $[TI2] -- this list is currently empty...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                      [static]
//
//@ Interface-Description
//      Report the software fault that occured within the source code
//      of this class.  The fault, indicated by 'softFaultID', occured
//      at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//      none
//---------------------------------------------------------------------
//@ PostConditions
//      none
//@ End-Method
//=====================================================================

void
MutableC_AlarmUpdate_NUM_ALARM::SoftFault(const SoftFaultID softFaultID,
				       const Uint32      lineNumber,
				       const char*       pFileName,
				       const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, FIXED_MUTABLE_SORTC,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr)  [const, virtual]
//
// Interface-Description
//  This method overrides the base class's pure virtual method, and
//  dumps out an ASCII representation of the current state of this
//  list to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
MutableC_AlarmUpdate_NUM_ALARM::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "MutableC_AlarmUpdate_NUM_ALARM..." << getElemList_();

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
