#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: P100PausePhase - P100 Pause Phase
//---------------------------------------------------------------------
//@ Interface-Description
// This class is derived from the BreathPhase class. Virtual methods
// defined in this class provide the implementation of the P100 Pause
// (Occlusion) phase. The P100PausePhase provides the control necessary
// to occlude the circuit during the P100 maneuver so the P100Maneuver
// class can measure the pressure deflection created by the patient
// drawing against a closed circuit for 100 ms. Virtual methods are
// implemented to initialize the P100 occlusion phase, to control the 
// P100 occlusion every BD cycle and to wrap up the phase and 
// relinquish control at the end of the phase.
//---------------------------------------------------------------------
//@ Rationale
// This class implements P100 pause phase.	
//---------------------------------------------------------------------
//@ Implementation-Description
// In order to create a closed circuit, the exhalation valve is closed 
// during this phase and the PSOLs are commanded closed. This class 
// uses the same type of control algorithm as the ExpPausePhase class
// to create a closed circuit.
//---------------------------------------------------------------------
//@ Fault-Handling
// N/A
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/P100PausePhase.ccv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//=====================================================================

#include "P100PausePhase.hh"

#include "MainSensorRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "ValveRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "PhasedInContextHandle.hh"
#include "ApneaInterval.hh"
#include "PressureSensor.hh"
#include "Trigger.hh"
#include "TimerBreathTrigger.hh"
#include "Psol.hh"
#include "ExhValveIController.hh"
#include "ControllersRefs.hh"
#include "BD_IO_Devices.hh"
#include "FlowController.hh"
#include "PausePressTrigger.hh"
#include "HighCircuitPressureInspTrigger.hh"
#include "P100Maneuver.hh"
#include "ManeuverRefs.hh"

//@ End-Usage

//@ Constant: PRESSURE_TO_SEAL
// pressure above pat press for exhalation valve to seal
extern const Real32 PRESSURE_TO_SEAL ;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: P100PausePhase()
//
//@ Interface-Description
// Constructor. This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

P100PausePhase::P100PausePhase( void)
 : BreathPhase(BreathPhaseType::P100_PAUSE) 	   	// $[TI1]
{
	CALL_TRACE("P100PausePhase::P100PausePhase( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~P100PausePhase()
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

P100PausePhase::~P100PausePhase(void)
{
	CALL_TRACE("P100PausePhase::~P100PausePhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
// This method has a BreathTrigger& as an argument and has no return 
// value.  This method is called at the end of the P100 pause
// phase to wrap up the phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls Maneuver::Complete() to inform the P100Maneuver of the end
// of the P100 pause phase.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
P100PausePhase::relinquishControl( const BreathTrigger& )
{
   	CALL_TRACE("relinquishControl(BreathTrigger& )");

    // $[RM12096] When an active P100 maneuver terminates, breath delivery shall return to the previous scheduler.

	// inform the maneuver that the P100 phase is complete
	// the maneuver determines whether a COMPLETE or CANCEL status is
	// sent to the GUI based on the state of the maneuver itself
	Maneuver::Complete();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
// This method has no arguments and no return value.  This method is
// called every BD cycle during an NIF pause phase to control the
// exhalation valve to maintain a closed circuit.
//---------------------------------------------------------------------
//@ Implementation-Description
// The Psols are closed.  The exhalation valve is commanded to patient
// pressure plus PRESSURE_TO_SEAL with a maximum of HCP.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
P100PausePhase::newCycle( void)
{
    CALL_TRACE("newCycle(void)");

	// $[RM12091] When the P100 maneuver becomes Active, the inspiratory and exhalation valves shall be closed.

	updateFlowControllerFlags_() ;
	
	RAirFlowController.updatePsol( 0.0F) ;
   	RO2FlowController.updatePsol( 0.0F) ;

	Real32  targetPressure = 
		MIN_VALUE( PRESSURE_TO_SEAL + RExhPressureSensor.getFilteredValue(), 
				   highCircuitPressure_) ;

	RExhValveIController.updateExhalationValve( targetPressure, 0.0F) ;

	elapsedTimeMs_ += CYCLE_TIME_MS ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
// This method has no arguments and has no return value.  This method is 
// called at the beginning of the P100 pause phase to initialize the
// phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The ExhValveIController and Psols are initalized via newBreath() method
// calls. Patient triggers are not enabled to force the patient to inspire
// against a closed circuit so that negative pressure can be measured and
// reported to the GUI. The P100Maneuver maintains an interval timer 
// that terminates the phase if the phase fails to terminate normally
// via the patient inspiring more than 0.5 cmH2O required to start the 
// 100 ms timer. The high circuit pressure inspiratory trigger is also
// enabled during this phase.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
P100PausePhase::newBreath(void)
{
    CALL_TRACE("newBreath(void)");
   	CLASS_ASSERTION(RP100Maneuver.getManeuverState() == PauseTypes::PENDING);

	// $[RM12091] When the P100 maneuver becomes Active, the inspiratory and exhalation valves shall be closed.

	const BoundedValue& rHighCircuitPressure =
		PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS) ;

	highCircuitPressure_ = rHighCircuitPressure.value ;
	
    RExhValveIController.newBreath() ;
    RAirFlowController.newBreath() ;
   	RO2FlowController.newBreath() ;
    
    RHighCircuitPressureInspTrigger.enable();

	elapsedTimeMs_ = 0 ;

    // $[RM12306] The maneuver shall transition to active when the P100 inspiratory trigger conditions (see Control Specification) have been met.
	RP100Maneuver.activate();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
P100PausePhase::SoftFault( const SoftFaultID  softFaultID,
                   				 const Uint32       lineNumber,
		   						 const char*        pFileName,
		  						 const char*        pPredicate)
{
  	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, P100PAUSEPHASE,
    	                     lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFlowControllerFlags_
//
//@ Interface-Description
// This method has no arguments and has no return value.  This method is
// called to set the controller shutdown flag of the flow controllers to
// TRUE.  This method overloads the base class method.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls setControllerShutdown for both flow controllers.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
P100PausePhase::updateFlowControllerFlags_( void)
{
	CALL_TRACE("P100PausePhase::updateFlowControllerFlags_( void)") ;

	// $[RM12091] When the P100 maneuver becomes Active, the inspiratory and exhalation valves shall be closed.

	RO2FlowController.setControllerShutdown( TRUE) ;
	RAirFlowController.setControllerShutdown( TRUE) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================






