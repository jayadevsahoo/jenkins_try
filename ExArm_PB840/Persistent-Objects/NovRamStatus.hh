
#ifndef NovRamStatus_HH
#define NovRamStatus_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename:  NovRamStatus - Non-Volatile RAM Status.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamStatus.hhv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
//@ End-Usage

struct NovRamStatus
{
  //@ Type:  VerificationId
  // Status of the NOVRAM verifications.
  enum VerificationId
  {
    // these enums are intentionally ordered from no error ("ALL VALID"), to
    // an error ("INVALID"), this ordering must be maintained...
    ALL_ITEMS_VALID,
    ITEM_VALID = ALL_ITEMS_VALID,
    ITEM_INVALID
  };

  //@ Type:  InitializationId
  // Status of the NOVRAM initialization.
  enum InitializationId
  {
    // these enums are intentionally ordered from no error ("VERIFIED"), to
    // drastic error ("INITIALIZED"), this ordering must be maintained...
    ITEM_VERIFIED,
    ITEM_RECOVERED,
    ITEM_INITIALIZED
  };
};


#endif // NovRamStatus_HH 
