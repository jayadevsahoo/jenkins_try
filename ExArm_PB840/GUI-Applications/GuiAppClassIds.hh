
#ifndef GuiAppClassIds_HH
#define GuiAppClassIds_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  GuiAppClassIds - Ids of all of the Patient-Data Classes.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiAppClassIds.hhv   25.0.4.0   19 Nov 2013 14:07:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 017   By: mnr    Date: 13-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//	Added SSTPROXSUBSCREEN at 110. Obsoleted item removed.
//
//  Revision: 016   By: rhj    Date: 17-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//	Added PROXSETUPSUBSCREEN enum.
//
//  Revision: 015   By: rhj    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  Trend
//  Description:
//	Added SOFTWARE_OPTIONS_SUBSCREEN enum.
// 
//  Revision: 014   By: gdc    Date: 20-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//	Added class IDs for Trending project.
//
//  Revision: 013   By: gdc    Date: 15-Feb-2005    SCR Number: 6144
//  Project:  Trend
//  Description:
//	Added NumericLimitSettingButton class to support setting Dsens=OFF.
//
//  Revision: 012   By: quf    Date: 23-Jan-2002    DR Number: 5984
//  Project:  GUIComms
//  Description:
//	Added SERIALLOOPBACKTESTSUBSCREEN.
//
//  Revision: 011  By: sah     Date: 09-Jun-00    DCS Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added dropdown buttons
//
//  Revision: 010  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 009  By:  sah   Date:  09-Mar-1999   DCS Number:  5314
//	Project:  ATC
//	Description:
//		Obsoleted GuiSetting{Target,Registrar}, SoftBoundSettingButton,
//      and MandatoryTypeButton classes.  Also, commented out other
//      previously-obsoleted ids.
//
//  Revision: 008  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/GuiAppClassIds.hhv   1.9.1.0   07/30/98 10:13:26   gdc
//
//  Revision: 007  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//   Revision 006   By:  sah    Date: 15-Jan-1998        DR Number:  5004
//   Project:   Sigma   (R8027)
//   Description:
//       Added class ID for new 'AlarmStrs' class.  Also, obsoleted ID
//       for 'Symbols'.
//
//  Revision: 005 By:  yyy    Date:  26-SEP-97    DR Number: 2410
//    Project:  Sigma (R8027)
//    Description:
//      Updated class Id for pressure transducer calibrationtest due to changes
//		in service mode.
//
//  Revision: 004  By:  yyy   Date:  06-Aug-1997   DCS Number:  1803
//	Project:  Sigma (R8027)
//	Description:
//		Added new class 'NovRamEventCascade'.
//
//  Revision: 003  By:  yyy   Date:  30-Jul-1997   DCS Number:  2013,2205
//	Project:  Sigma (R8027)
//	Description:
//		Added new class 'FlowSensorCalibrationSubScreen' and
//		'TauCharacterizationSubScreen'.
//
//  Revision: 002  By:  sah   Date:  28-Jul-1997   DCS Number:  2320
//	Project:  Sigma (R8027)
//	Description:
//		Added new class 'AlarmStateManager'.
//
//  Revision: 001  By:  sah    Date:  06-16-94    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version 
//  
//====================================================================

#include "Sigma.hh"

//@ Type: StringId
// Definitions to fake out Language interface
typedef const wchar_t* StringId;
extern const StringId NULL_STRING_ID;

//@ Type: GuiAppClassId
// Ids of all of the classes/files of this subsystem
enum GuiAppClassId
{
	ALARMANDSTATUSAREA = 0,
	ALARMLIMITSETTINGBUTTON = 1,
	ALARMLOGSUBSCREEN = 2,
	ALARMLOGTABBUTTON = 3,
	ALARMMSG = 4,
	ALARMRENDER = 5,
	ALARMSETUPSUBSCREEN = 6,
	ALARMSILENCERESETHANDLER = 7,
	ALARMSLIDER = 8,
	ALARMVOLUMEHANDLER = 9,
	APNEASETUPSUBSCREEN = 10,
	APNEAVENTILATIONSUBSCREEN = 11,
	BDEVENTCASCADE = 12,
	BDEVENTREGISTRAR = 13,
	BDEVENTTARGET = 14,
	BOUNDSTRS = 15,
	BREATHTIMINGDIAGRAM = 16,
	BREATHTIMINGSUBSCREEN = 17,
	CALIBRATIONSETUPSUBSCREEN = 18,
	COMMSETUPSUBSCREEN = 19,
	DATETIMESETTINGSSUBSCREEN = 20,
	DEVELOPMENTOPTIONSSUBSCREEN = 21,
	DIAGCODELOGSUBSCREEN = 22,
	DIAGLOGMENUSUBSCREEN = 23,
	DIAGNOSTICFAULT = 24,
	/** DIAGRESULTSUBSCREEN = 25,  OBSOLETED **/
	DISCRETESETTINGBUTTON = 26,
	/** ENTERSSTSUBSCREEN = 27,  OBSOLETED **/
	ESTRESULTSUBSCREEN = 28,
	ESTTESTSUBSCREEN = 29,
	EXITSERVICEMODESUBSCREEN = 30,
	EXPIRATORYPAUSEHANDLER = 31,
	/** FLOWPATTERNSETTINGBUTTON = 32,  OBSOLETED **/
	GUIAPP = 33,
	/** GUITRENDDATAVALUES = 34, OBSOLETED **/
	/** GUISETTINGREGISTRAR = 35,  OBSOLETED **/
	/** GUITRENDDATASET = 36, OBSOLETED **/
	GUIEVENTREGISTRAR = 37,
	GUIEVENTTARGET = 38,
	GUITESTMANAGER = 39,
	GUITESTMANAGERTARGET = 40,
	GUITIMERREGISTRAR = 41,
	GUITIMERTARGET = 42,
	HELPSUBSCREEN = 43,
	HUNDREDPERCENTO2HANDLER = 44,
	IBWSETTINGBUTTON = 45,
	IERATIOSETTINGBUTTON = 46,
	INITSCREEN = 47,
	INSPIRATORYPAUSEHANDLER = 48,
	KEYHANDLERS = 49,
	LAPTOPEVENTCASCADE = 50,
	LAPTOPEVENTREGISTRAR = 51,
	LAPTOPEVENTTARGET = 52,
	LAPTOPMESSAGEHANDLER = 53,
	LEAKGAUGE = 54,
	LOGCELL = 55,
	LOGTARGET = 56,
	LOOPPLOT = 57,
	LOWEROTHERSCREENSSUBSCREEN = 58,
	LOWERSCREEN = 59,
	LOWERSCREENSELECTAREA = 60,
	LOWERSUBSCREENAREA = 61,
	MAINSETTINGSAREA = 62,
	MAINSETTINGSUBSCREEN = 63,
	MANUALINSPIRATIONHANDLER = 64,
	MESSAGEAREA = 65,
	MISCBDEVENTHANDLER = 66,
	MOREALARMSSUBSCREEN = 67,
	MOREALARMSTABBUTTON = 68,
	MOREDATASUBSCREEN = 69,
	MORESETTINGSSUBSCREEN = 70,
	NEWPATIENTSUBSCREEN = 71,
	NOVRAMEVENTREGISTRAR = 72,
	NOVRAMEVENTTARGET = 73,
	NUMERICSETTINGBUTTON = 74,
	OPERATIONALTIMESUBSCREEN = 75,
	PADLOCK = 76,
	PATIENTDATACASCADE = 77,
	PATIENTDATAREGISTRAR = 78,
	PATIENTDATATARGET = 79,
	PRESSUREVOLUMEPLOT = 80,
	/** PROCEEDBUTTON = 81,  OBSOLETED **/
	PROMPTAREA = 82,
	SAFETYVENTILATIONSUBSCREEN = 83,
	SCREENBRIGHTNESSHANDLER = 84,
	SCREENCONTRASTHANDLER = 85,
	SCREENLOCKHANDLER = 86,
	SCROLLABLELOG = 87,
	SCROLLBAR = 88,
	SCROLLBARTARGET = 89,
	SEQUENTIALSETTINGBUTTON = 90,
	SERIALNUMSETUPSUBSCREEN = 91,
	SERVICEDATAREGISTRAR = 92,
	SERVICEDATATARGET = 93,
	SERVICELOWEROTHERSCREENSSUBSCREEN = 94,
	SERVICELOWERSCREEN = 95,
	SERVICELOWERSCREENSELECTAREA = 96,
	SERVICELOWERSUBSCREENAREA = 97,
	SERVICEINITIALIZATIONSUBSCREEN = 98,
	SERVICEMODESETUPSUBSCREEN = 99,
	SERVICEPROMPTMGR = 100,
	SERVICESTATUSAREA = 101,
	SERVICETITLEAREA = 102,
	SERVICEUPPERSCREEN = 103,
	SERVICEUPPERSCREENSELECTAREA = 104,
	SERVICEUPPERSUBSCREENAREA = 105,
	SETTINGBUTTON = 106,
	/** SMSTARTUPSUBSCREEN = 107,  OBSOLETED **/
	/** SMTRANSLATIONTABLE = 108,  OBSOLETED **/
	/** SSTCONFIRMATIONREGISTRAR = 109,  OBSOLETED **/
	SSTPROXSUBSCREEN = 110,
	SSTLEAKSUBSCREEN = 111,
	SSTRESULTSSUBSCREEN = 112,
	SSTSETUPSUBSCREEN = 113,
	SSTTESTSUBSCREEN = 114,
	SUBSCREEN = 115,
	SUBSCREENAREA = 116,
	SUBSCREENBUTTON = 117,
	SUBSCREENTITLEAREA = 118,
	/** SYMBOLS = 119,  OBSOLETED **/
	EXTERNALCONTROLSUBSCREEN = 120,
	TABBUTTON = 121,
	TESTRESULT = 122,
	TESTRESULTTARGET = 123,
	TEXTAREA = 124,
	TEXTBUTTON = 125,
	TEXTUTIL = 126,
	TIMINGSETTINGBUTTON = 127,
	TIMEPLOT = 128,
	/** TOGGLEBUTTON = 129,  OBSOLETED **/
	TOUCHABLETEXT = 130,
	UPPEROTHERSCREENSSUBSCREEN = 131,
	UPPERSCREEN = 132,
	UPPERSCREENSELECTAREA = 133,
	UPPERSUBSCREENAREA = 134,
	VENTCONFIGSUBSCREEN = 135,
	VENTSETUPSUBSCREEN = 136,
	/** VENTSETUPTABBUTTON = 137,  OBSOLETED **/
	VENTSTARTSUBSCREEN = 138,
	VENTTESTSUMMARYSUBSCREEN = 139,
	VITALPATIENTDATAAREA = 140,
	WAVEFORMDATABUFFER = 141,
	WAVEFORMINTERVALITER = 142,
	WAVEFORMPLOT = 143,
	WAVEFORMSSUBSCREEN = 144,
	WAVEFORMSTABBUTTON = 145,
	/** WYEBLOCKBUTTON = 146,  OBSOLETED **/

	ALARMSTATEMANAGER = 147,
	FLOWSENSORCALIBRATIONSUBSCREEN = 148,
	/** TAUCHARACTERIZATIONSUBSCREEN = 149,  OBSOLETED **/
	NOVRAMEVENTCASCADE = 150,
	PRESSUREXDUCERCALIBRATIONSUBSCREEN = 151,
	ALARMSTRS = 152,
    BATCHSETTINGSSUBSCREEN = 153,
    CLOCKSETTINGBUTTON = 154,
	/** MANDATORYTYPEBUTTON = 155,  OBSOLETED **/
	MANEUVERSUBSCREEN = 156,
	/** MANEUVERTABBUTTON = 157,  OBSOLETED **/
	OPERATIONHOURCOPYSUBSCREEN = 158,
	ALARMOFFKEYPANEL = 159,
	OFFKEYPANEL = 160,
	OFFKEYSSUBSCREEN = 161,
	ALARMSILENCEOFFKEYPANEL = 162,
	STATICSTDDEVIATIONBUFFER = 163,
	STDDEVIATIONBUFFER = 164,
	HUMIDVOLUMESETTINGBUTTON = 165,
	SERIALLOOPBACKTESTSUBSCREEN = 166,
	DROPDOWNSETTINGBUTTON = 167,
	NUMERICLIMITSETTINGBUTTON = 168,
	NIFSUBSCREEN = 169,
	P100SUBSCREEN = 170,
	VITALCAPACITYSUBSCREEN = 171,
    RMDATASUBSCREEN = 172,
    FLOWVOLUMEPLOT = 173,
    TRENDGRAPHSSUBSCREEN        = 174,
    TRENDPLOT                   = 175,
    TRENDPLOTBASIS              = 176,
    TRENDTABBUTTON              = 177,
    EVENTSUBSCREEN              = 178,
    EVENTTABBUTTON              = 179,
    SCROLLABLEMENU              = 180,
    TRENDTABLESUBSCREEN         = 181,
    CURSORSETTINGBUTTON         = 182,
    TRENDDBTABLEBUFFER          = 183,
    TRENDCURSORSLIDER           = 184,
    TRENDINGLOG           		= 185,
    TRENDDATASETAGENT 			= 186,
    TRENDINGSCROLLBAR      		= 187,
    COMPACTFLASHTESTSUBSCREEN   = 188,
    TRENDFORMATDATA				= 189,
    SCROLLABLEEVENTLOG          = 190,
    EVENTDATAITEMS              = 191,
    FOCUSBUTTON                 = 192,
    FOCUSBUTTONTARGET           = 193,
	TIMEOUTSUBSCREEN			= 194,
	GUITABLE					= 195,
	SETTINGSCROLLBAR			= 196,
	SETTINGMENUITEMS			= 197,
	TRENDSELECTMENUITEMS		= 198,
	TRENDTIMESCALEMENUITEMS		= 199,
	SCROLLABLEMENUSETTINGBUTTON = 200,
	TRENDEVENTREGISTRAR			= 201,
	TRENDSUBSCREEN              = 202,
	SOFTWARE_OPTIONS_SUBSCREEN  = 203,
	PROXSETUPSUBSCREEN          = 204,
    NUM_GUIAPP_CLASSES
};

// Some global sizes and positions of graphical objects
enum SizeAndPositions
{
	FRAME_SIZE = 3,
	SUB_SCREEN_AREA_WIDTH  = 634,
	UPPER_SUB_SCREEN_AREA_WIDTH  = SUB_SCREEN_AREA_WIDTH,
	UPPER_SUB_SCREEN_AREA_HEIGHT = 276,
	LOWER_SUB_SCREEN_AREA_WIDTH  = SUB_SCREEN_AREA_WIDTH,
	LOWER_SUB_SCREEN_AREA_HEIGHT = 245,

	SERVICE_UPPER_SUB_SCREEN_AREA_WIDTH  = SUB_SCREEN_AREA_WIDTH,
	SERVICE_UPPER_SUB_SCREEN_AREA_HEIGHT = UPPER_SUB_SCREEN_AREA_HEIGHT,
	SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH  = SUB_SCREEN_AREA_WIDTH,
	SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT = 404
};


#endif // GuiAppClassIds_HH 
