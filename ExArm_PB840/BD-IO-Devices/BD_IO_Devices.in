
#ifndef BD_IO_Devices_IN
#define BD_IO_Devices_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//Class: BD_IO_Devices - Subsystem definitions, construction, and
//       initialization.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BD_IO_Devices.inv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002 By: syw   Date: 25-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added support for flash serial number determination.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	GetSerialNumOkay
//
//@ Interface-Description
//		This method has no arguments and returns the status whether the
//		serial number in flash matches the data key's.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns data member SerialNumOkay_.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

inline Boolean
BD_IO_Devices::GetSerialNumOkay( void)
{
	CALL_TRACE("BD_IO_Devices::GetSerialNumOkay( void)") ;

    // $[TI1]

	return( SerialNumOkay_) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#endif // BD_IO_Devices_IN
