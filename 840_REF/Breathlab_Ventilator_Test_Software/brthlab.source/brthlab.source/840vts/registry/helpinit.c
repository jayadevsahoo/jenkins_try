/*****************************************************************************/
/*   Module Name:    helpinit.c                                           
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*
/*   Modification History:
/*
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Removed Ground DC & AC volts from      Rhomere Jimenez   03 Nov, 2005
/*   Help Screens.
/*
/*	 Critical Observation during Phase1
/* 	 is incorportated.						Kathirvel(CGS)    27 Sep,2000
/*
/*   $Revision::   1.2      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <windows.h>
#include <utility.h>
#include "helpinit.h"
#include "ldb.h"
#include "mainpnls.h"
#include "toolbox.h"
#include "cpvtmain.h"
#include "840vts.hh"

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

#if 0
/*****************************************************************************/
/* Name:     GetCHMExecutableName                                                      
/*                                                                           
/* Purpose:  Gets the executable path for the compiled HTML viewer
/*                                          
/*                                                                           
/* Example Call:   int GetCHMExecutableName(char** ComBuffer);                                    
/*                                                                           
/* Input:    char** ComBuffer   buffer to return path in      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int GetCHMExecutableName(char** ComBuffer)
{
	BYTE ExecKeyBuff[50];

	HKEY HelpExtKey;
	DWORD retlen, result;
	char* last_quote;
	retlen = 50;
	result = RegOpenKeyEx(HKEY_CLASSES_ROOT, ".chm", 0, KEY_QUERY_VALUE, &HelpExtKey);
	if(result != ERROR_SUCCESS){
		return HLP_NO_EXT;
	}
	result = RegQueryValueEx(HelpExtKey, NULL, NULL, NULL, ExecKeyBuff, &retlen);
	if(result != ERROR_SUCCESS){
		return HLP_ERROR;
	}
	if(retlen<=0){
		return HLP_ERROR;
	}
	RegCloseKey(HelpExtKey);

	//Get the app to run.
	retlen = 50;
	strcat((char*)ExecKeyBuff, "\\shell\\open\\command");
	result = RegOpenKeyEx(HKEY_CLASSES_ROOT, (LPCSTR)ExecKeyBuff, 0, KEY_QUERY_VALUE, &HelpExtKey);
	if(result ! =ERROR_SUCCESS){
		return HLP_NO_APP;
	}
	result = RegQueryValueEx(HelpExtKey, NULL, NULL, NULL, ExecKeyBuff, &retlen);
	if(result != ERROR_SUCCESS){
		return HLP_ERROR;
	}
	if(retlen <= 0){
		return HLP_ERROR;
	}
	RegCloseKey(HelpExtKey);
	*ComBuffer = StrDup((char*)ExecKeyBuff+1);
	if(last_quote = strchr(*ComBuffer, '"')){
		*last_quote = '\0';
	}
	
	return HLP_NOERROR;
}
#endif

/*****************************************************************************/
/* Name:    ShowHelp                                                      
/*                                                                           
/* Purpose:  Launchs help viewer
/*                                          
/*                                                                           
/* Example Call:   int ShowHelp(int nTopic);                                    
/*                                                                           
/* Input:    input1 -	description          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int ShowHelp(int panel, int control, int nTypeOfHelp)
{
	#if 0
	char *chmexe, *chmcommand;
	#endif
	char ProjectDir[MAX_PATHNAME_LEN+MAX_FILENAME_LEN];

	unsigned int  nCommand   = 0;
	unsigned long lContextID = 0;
	char          szKeyText[256];

#if 0
	if(GetCHMExecutableName(&chmexe)==HLP_NOERROR){

		//chmcommand=malloc(sizeof(chmexe)+64);
		chmcommand=(char *)malloc(sizeof(chmexe)+64); // Modified the statement due to Critical Observation.
		strcpy(chmcommand, chmexe);
		strcat(chmcommand, LDBf(Ldb_HelpCommand));
		LaunchExecutableEx(chmcommand, LE_SHOWNORMAL, 0);
		free(chmcommand); // Modified the statement due to Critical Observation.
		return 1;
	}
	return 0;
#else
	if(GetProjectDir(ProjectDir)!=0)
		return -1;
	strcat(ProjectDir, LDBf(Ldb_Helpfile_Name));

	nCommand = HELP_FINDER;
	lContextID = 0;
	*szKeyText = '\0';
	
	switch(nTypeOfHelp){
		case GENERAL_HELP:
			break;
		case CONTEXT_HELP:
			if(panel == gPanelHandle[ADDUSERPNL]){
				switch(control){ // panel ADDUSERPNL
					case ADDUSERPNL_USERNAME:   // callback function: Adduser_Callback
					case ADDUSERPNL_PASSWORD:   // callback function: Adduser_Callback
					case ADDUSERPNL_PASSWORD_2: // callback function: Adduser_Callback
					case ADDUSERPNL_OK_BUTTON:
					case ADDUSERPNL_CANCEL_BUTTON:
						break;
					default:
						break;
				} // end switch(control){ // panel ADDUSERPNL
			} // end if(panel == gPanelHandle[ADDUSERPNL])
			else if(panel == gPanelHandle[ADMINPANEL]){
				nCommand = HELP_CONTEXT;
				lContextID = Setup_Screen;
				// there are no controls on the ADMINPANEL that can obtain focus
			} // end if(panel == gPanelHandle[ADMINPANEL])
			else if(panel == gPanelHandle[ALARMPANEL]){
				switch(control){ // panel ALARMPANEL
					case ALARMPANEL_DONE:
					case ALARMPANEL_YES:
					case ALARMPANEL_NO:
						break;
					default:
						break;
				} // end switch(control){ // panel ALARMPANEL
			} // end if(panel == gPanelHandle[ALARMPANEL])
			else if(panel == gPanelHandle[AUTODETECT]){
				switch(control){ // panel AUTODETECT
					case AUTODETECT_ACCEPT:
						break;
					default:
						break;
				} // end switch(control){ // panel AUTODETECT
			} // end if(panel == gPanelHandle[AUTODETECT])

//                    case COMP_PANEL_COMP_SERIAL://          2
//                    case COMP_PANEL_COMP_DONE://            3
//                    case COMP_PANEL_COMP_TIME://            4
//                    case COMP_PANEL_COMP_CANCEL://          5
//                    case COMP_PANEL_OLDINFO://              6
//                    case COMP_PANEL_DECORATION://           7
//
			else if(panel == gOPanelHandle[COMSETTING]){
				nCommand = HELP_CONTEXT;
				lContextID = Com_Settings_Tab;
				switch(control){ // panel COMSETTING
                    case COMSETTING_AUTODET_ENABLE_PTS://   2       /* callback function: AutoDetectEnablePTS */
                    case COMSETTING_COM_PORT_PTS://         3       /* callback function: ComPortPTS */
                    case COMSETTING_BAUD_PTS://             4       /* callback function: BaudRatePTS */
                    case COMSETTING_AUTO_DETECT_PTS://      5       /* callback function: AutoDetectPTS */
                    case COMSETTING_AUTODET_ENABLE_840://   6       /* callback function: AutoDetectEnabled840 */
                    case COMSETTING_COM_PORT_840://         7       /* callback function: ComPort840 */
                    case COMSETTING_BAUD_840://             8       /* callback function: BaudRate840 */
                    case COMSETTING_AUTO_DETECT_840://      9       /* callback function: AutoDetect840 */
						break;
					default:
						break;
				} // end switch(control){ // panel COMSETTING
			} // end if(panel == gOPanelHandle[COMSETTING])
//
//                    case DB_CONF_MAXUSERS_MSG://            2
//                    case DB_CONF_PACKDB://                  3       /* callback function: DB_Conf_CB */
//                    case DB_CONF_TEXTMSG_2://               4
//                    case DB_CONF_PICTURE://                 5
//
//                    case LANGCONF_TEXTBOX://                2
//                    case LANGCONF_LANG_SEL://               3       /* callback function: LanguageSelect */
//                    case LANGCONF_LANG_MSG://               4
//
//                    case LOGGINOPTS_CHECKBOX_1://           2       /* callback function: DataLogCB */
//                    case LOGGINOPTS_CHECKBOX_2://           3       /* callback function: DataLogCB */
//                    case LOGGINOPTS_CHECKBOX_3://           4       /* callback function: DataLogCB */
//                    case LOGGINOPTS_CHECKBOX_4://           5       /* callback function: DataLogCB */
//
			else if(panel == gPanelHandle[LOGONPANEL]){
				nCommand = HELP_CONTEXT;
				lContextID = Starting_the_Software;
				switch(control){ // panel LOGONPANEL
                    case LOGONPANEL_OPERATORS://            2       /* callback function: Logon_Callback */
                    case LOGONPANEL_EXIT_BUTTON://          3       /* callback function: secondquit */
                    case LOGONPANEL_PTS_CAL_BUTTON://       4       /* callback function: Logon_Callback */
                    case LOGONPANEL_VENT_CAL_BUTTON://      5       /* callback function: Logon_Callback */
                    case LOGONPANEL_LOGIN_BUTTON://         6       /* callback function: Logon_Callback */
                    case LOGONPANEL_PTS2000_BOX://          7
                    case LOGONPANEL_PTS2000_BOX_2://        8
                    case LOGONPANEL_PTS2000_LBL://          9
                    case LOGONPANEL_PTS2000_LBL_2://        10
                    case LOGONPANEL_PICTURE://              11
                    case LOGONPANEL_VERSION_LBL://          12
                    case LOGONPANEL_840_GUISERIAL://        13
                    case LOGONPANEL_840_BDSERIAL://         14
                    case LOGONPANEL_PTS_SERIAL_LBL_2://     15
                    case LOGONPANEL_CAL_LAST_LBL_2://       16
                    case LOGONPANEL_SW_VERSION_LBL_2://     17
                    case LOGONPANEL_CAL_LAST_LBL_3://       18
                    case LOGONPANEL_PICTURE_2://            19
                    case LOGONPANEL_PTS2000_LBL_3://        20
                    case LOGONPANEL_840_LBL_4://            21
                    case LOGONPANEL_PTS_SERIAL_LBL://       22
                    case LOGONPANEL_SW_VERSION_LBL://       23
                    case LOGONPANEL_840_GUISERIAL_LBL://    24
                    case LOGONPANEL_840_BDSERIAL_LBL://     25
                    case LOGONPANEL_PTS2000_LBL_5://        26
                    case LOGONPANEL_CAL_LAST_LBL://         27
                    case LOGONPANEL_CAL_NEXT_LBL://         28
						break;
					default:
						break;
				} // end switch(control){ // panel LOGONPANEL
			} // end if(panel == gPanelHandle[LOGONPANEL])
//
// *                  case LOOPPANEL_MAXNUMBER://             2
//                    case LOOPPANEL_UNTILFAIL://             3
//                    case LOOPPANEL_OK://                    4       /* callback function: LoopCallback */
//                    case LOOPPANEL_CANCEL://                5       /* callback function: LoopCallback */
//                    case LOOPPANEL_DECORATION://            6
//
			else if(panel == gPanelHandle[MANUALPNL]){
				nCommand = HELP_CONTEXT;
				lContextID = CS_Manual_Test;
				switch(control){ // panel MANUALPNL
                    case MANUALPNL_WORKORDER://             2
                    case MANUALPNL_VENTASSET://             3
                    case MANUALPNL_GROUNDRES://             4
                    case MANUALPNL_VOLTAGE://               5
                    case MANUALPNL_FRWDLEAK://              6
                    case MANUALPNL_REVLEAK://               7
                    case MANUALPNL_GRNDISOL://              8
                    case MANUALPNL_AIRREG://                11
                    case MANUALPNL_O2REG://                 12
                    case MANUALPNL_EST_PF://                13
                    case MANUALPNL_SST_PF://                14
                    case MANUALPNL_OK://                    15
						break;
					default:
						break;
				} // end switch(control){ // panel MANUALPNL
			} // end if(panel == gPanelHandle[MANUALPNL])
//
//                    case PASSWDPNL_PASSWORD://              2       /* callback function: Passwd_Callback */
//                    case PASSWDPNL_OK_BUTTON://             3       /* callback function: Passwd_Callback */
//                    case PASSWDPNL_CANCEL_BUTTON://         4       /* callback function: Passwd_Callback */
//                    case PASSWDPNL_USERNAME://              5
//
//                    case PERF_PANEL_DONE://                 2
//                    case PERF_PANEL_ABORT://                3
//                    case PERF_PANEL_BODY_WEIGHT://          4
//                    case PERF_PANEL_TRIGGER://              5
//                    case PERF_PANEL_PEEP://                 6
//                    case PERF_PANEL_AC_TITLE://             7
//                    case PERF_PANEL_MANTYPE://              8
//                    case PERF_PANEL_O2://                   9
//                    case PERF_PANEL_P://                    10
//                    case PERF_PANEL_PSENS://                11
//                    case PERF_PANEL_TPL://                  12
//                    case PERF_PANEL_VMAX://                 13
//                    case PERF_PANEL_FSET://                 14
//                    case PERF_PANEL_PI://                   15
//                    case PERF_PANEL_VT://                   16
//                    case PERF_PANEL_TI://                   17
//                    case PERF_PANEL_WAVEFORM://             18
//                    case PERF_PANEL_INST_BOX://             19
//                    case PERF_PANEL_TESTTITLE://            20
//                    case PERF_PANEL_O2READING://            21
//                    case PERF_PANEL_PANEL_TIMER://          22      /* callback function: Flasher */
//                    case PERF_PANEL_AC_MIN://               23
//                    case PERF_PANEL_AC://                   24
//                    case PERF_PANEL_VT_TITLE://             25
//                    case PERF_PANEL_TI_S://                 26
//                    case PERF_PANEL_VMAX_TITLE://           27
//                    case PERF_PANEL_TPL_TITLE://            28
//                    case PERF_PANEL_TPL_S://                29
//                    case PERF_PANEL_P_PER://                30
//                    case PERF_PANEL_P_TITLE://              31
//                    case PERF_PANEL_PSENS_CM://             32
//                    case PERF_PANEL_VTRIG_MIN://            33
//                    case PERF_PANEL_PSENS_H20://            34
//                    case PERF_PANEL_O2_TITLE://             35
//                    case PERF_PANEL_O2_PER://               36
//                    case PERF_PANEL_PEEP_TITLE://           37
//                    case PERF_PANEL_PEEP_CM://              38
//                    case PERF_PANEL_PEEP_H20://             39
//                    case PERF_PANEL_PI_TITLE://             40
//                    case PERF_PANEL_FREQ_F://               41
//                    case PERF_PANEL_PI_CM://                42
//                    case PERF_PANEL_PI_H20://               43
//                    case PERF_PANEL_TI_TITLE://             44
//                    case PERF_PANEL_VMAX_MIN://             45
//                    case PERF_PANEL_VTRIG_L://              46
//                    case PERF_PANEL_PSENS_TITLE://          47
//                    case PERF_PANEL_VT_ML://                48
//                    case PERF_PANEL_VMAX_L://               49
//                    case PERF_PANEL_TESTCOUNT://            50
//                    case PERF_PANEL_PICTURE://              51
//                    case PERF_PANEL_DECORATION://           52
//                    case PERF_PANEL_O2TIMER://              53      /* callback function: O2TimerCB */
//                    case PERF_PANEL_DECORATION_2://         54
//                    case PERF_PANEL_O2PERCENT://            55
//
// *                  case PTSPANEL_CALIBRATE://              2       /* callback function: CmdAutoZero */
//                    case PTSPANEL_RESET://                  3       /* callback function: CmdPTSReset */
//
//                    case REPORTOPTS_STDCSE://               2
//                    case REPORTOPTS_TESTRESULTS://          3
//                    case REPORTOPTS_DIAGLOG://              4
//                    case REPORTOPTS_INFOLOG://              5
//                    case REPORTOPTS_TESTLOG://              6
//                    case REPORTOPTS_OK://                   7
//                    case REPORTOPTS_EXIT://                 8
//
// *                  case SPLASH_SEARCHCANCEL://             2       /* callback function: SplashCallback */
//
// *                  case SYSPANEL_CMD_HELP://               2       /* callback function: SysPanelCallback */
//                    case SYSPANEL_CMD_ADMIN://              3       /* callback function: SysPanelCallback */
//                    case SYSPANEL_CMD_REPORT://             4       /* callback function: SysPanelCallback */
//                    case SYSPANEL_CMD_EXIT://               5       /* callback function: SysPanelCallback */
//                    case SYSPANEL_CMD_TESTS://              6       /* callback function: SysPanelCallback */
//                    case SYSPANEL_CMD_840://                7       /* callback function: SysPanelCallback */
//                    case SYSPANEL_CMD_PTS://                8       /* callback function: SysPanelCallback */
//                    case SYSPANEL_STATUS://                 9
//                    case SYSPANEL_USERNAME://               10
//                    case SYSPANEL_CLOCKTIMER://             11      /* callback function: MainClockTimer */
//                    case SYSPANEL_SYSTEM_PANEL://           12
//                    case SYSPANEL_SYS_TT_TIMER://           13      /* callback function: ToolTipTimer */
//                    case SYSPANEL_TIME://                   14
//                    case SYSPANEL_DATE://                   15
//                    case SYSPANEL_DECORATION://             16
//
//                    case SYSSTATPNL_DONE://                 2
//                    case SYSSTATPNL_TEXT_BOX1://            3
//                    case SYSSTATPNL_DECORATION://           4
//                    case SYSSTATPNL_DECORATION_2://         5
//                    case SYSSTATPNL_TEXTMSG://              6
//
//                    case TESTOPTS_FIRMWARETEST://           2       /* callback function: TOptionsCallback */
//                    case TESTOPTS_APTEST://                 3       /* callback function: TOptionsCallback */
//                    case TESTOPTS_BDATEST://                4       /* callback function: TOptionsCallback */
//                    case TESTOPTS_GUIATEST://               5       /* callback function: TOptionsCallback */
//                    case TESTOPTS_CPRESTEST://              6       /* callback function: TOptionsCallback */
//                    case TESTOPTS_METAPORTTEST://           7       /* callback function: TOptionsCallback */
//                    case TESTOPTS_AIRFLOWTEST://            8       /* callback function: TOptionsCallback */
//                    case TESTOPTS_OXYFLOWTEST://            9       /* callback function: TOptionsCallback */
//                    case TESTOPTS_VOLPERF://                10      /* callback function: TOptionsCallback */
//                    case TESTOPTS_PRESPERF://               11      /* callback function: TOptionsCallback */
//                    case TESTOPTS_PEEPPERF://               12      /* callback function: TOptionsCallback */
//                    case TESTOPTS_FIO2PERF://               13      /* callback function: TOptionsCallback */
//                    case TESTOPTS_MANUALTEST://             14      /* callback function: TOptionsCallback */
//                    case TESTOPTS_STOPONFAILS://            15      /* callback function: TOptionsCallback */
//                    case TESTOPTS_ENABLEALL://              16      /* callback function: TOptionsAllCB */
//                    case TESTOPTS_DECORATION://             17
//                    case TESTOPTS_TEXTMSG://                18
//                    case TESTOPTS_TEXTMSG_2://              19
//
// *                  case TESTPANEL_TESTUUT_DEV://           2       /* callback function: MainPanelCallback */
//                    case TESTPANEL_PAUSE_BTN://             3       /* callback function: MainPanelCallback */
//                    case TESTPANEL_STOP_BTN://              4       /* callback function: MainPanelCallback */
//                    case TESTPANEL_RUNTEST_DEV://           5       /* callback function: MainPanelCallback */
//                    case TESTPANEL_LOOPTEST_DEV://          6       /* callback function: MainPanelCallback */
//                    case TESTPANEL_SEQUENCEDISPLAY://       7       /* callback function: MainPanelCallback */
//                    case TESTPANEL_TESTDISPLAY://           8       /* callback function: MainPanelCallback */
//
//                    case USERCONFIG_USERLIST://             2       /* callback function: UserConfig */
//                    case USERCONFIG_ADDUSER://              3       /* callback function: UserConfig */
//                    case USERCONFIG_DELETEUSER://           4       /* callback function: UserConfig */
//                    case USERCONFIG_USERPWD://              5       /* callback function: UserConfig */
//                    case USERCONFIG_MAXUSERS_MSG://         6
//                    case USERCONFIG_TEXTMSG_3://            7
//                    case USERCONFIG_TEXTMSG_4://            8
//                    case USERCONFIG_TEXTMSG_5://            9
//
// *                  case _840PANEL_CAPDIAGLOG://            2       /* callback function: Set840Callback */
//                    case _840PANEL_CLRDIAGLOG://            3       /* callback function: Set840Callback */
//                    case _840PANEL_SETCOMPHOURS://          4       /* callback function: Set840Callback */
//                    case _840PANEL_SETCOMPSERIAL://         5       /* callback function: Set840Callback */
			break; // case CONTEXT_HELP:
		default:
			break;
	} // end switch(nTypeOfHelp)
	SystemHelp(ProjectDir, nCommand, lContextID, szKeyText);
	return 1;
#endif
}


/*****************************************************************************/
/* Name:     ExitHelpAboutCMD                                                          
/*                                                                           
/* Purpose:  Discard the HelpAbout (splash) Screen
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK ExitHelpAboutCMD(panel, control, event,                
/*                                               callbackData, eventData1, eventData2)                                 
/*                                                                           
/* Input:    int panel = ID of the panel where an event occurred                           
/*           int control = ID of the control where an event occurred                       
/*           int event = ID of the event that occurred                                     
/*           void *callbackData = NULL (not used)                                          
/*           int eventData1 = 0 (not used)                                                 
/*           int eventData2 = 0 (not used)                                                         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used: NONE                                                          
/*                                                  
/*                                                                           
/* Return:   0                                                       
/*                                                                           
/*****************************************************************************/
int CVICALLBACK ExitHelpAboutCMD(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event){
		case EVENT_COMMIT:
			DiscardPanel(panel);
			gPanelHandle[SPLASH] = 0;
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/******************************************************************************/
/* Name:     ShowHelpAbout                                                    */
/*                                                                            */
/* Purpose:  This function displays the splash screen as a 'Help About ...'   */
/*           panel so the user may see the program title, revision            */
/*                                                                            */
/* Example Call:   ShowHelpAbout(hParentPanel)                                */
/*                                                                            */
/* Input:    hParentPanel   handle of panel caling this function              */
/* Output:   Displays splash screen                                           */
/*                                                                            */
/* Globals Used:                                                              */
/*           <None>                                                           */
/*                                                                            */
/* Globals Changed:                                                           */
/*           <None>                                                           */
/*                                                                            */
/* Return:   <None>                                                           */
/******************************************************************************/
void ShowHelpAbout(int hParentPanel)
{
	gPanelHandle[SPLASH]=LoadPanel(0, LDBf(Ldb_MainpanelFile), SPLASH);
	SetSplashVersionNum();
	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_SEARCHCANCEL, ATTR_CALLBACK_FUNCTION_POINTER, ExitHelpAboutCMD);
	DisplayPanel(gPanelHandle[SPLASH]);
	return; // Modified the statement due to Critical Observation.
}

