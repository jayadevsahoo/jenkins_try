#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================
#include "EBM_bq34z110.h"

// According to COMMUNICATIONS->I2C INTERFACE section of Chip Specification
// the fixed 7 bits address is 1010101 in binary.
static const Uint8 BQ34Z110_7BIT_ADDR			=	0x55;
static const Uint8 BQ34Z110_WRITE_ADDR			=	((BQ34Z110_7BIT_ADDR<<1) | 0x00);
static const Uint8 BQ34Z110_READ_ADDR			=	((BQ34Z110_7BIT_ADDR<<1) | 0x01);

// Standard Data Commands definition
static const Uint8 SDC_CONTROL[]                =   { 0x00, 0x01 };
static const Uint8 SDC_STATE_OF_CHARGE[]        =   { 0x02, 0x03 };
static const Uint8 SDC_REMAINING_CAPACITY[]     =   { 0x04, 0x05 };
static const Uint8 SDC_FULL_CHARGE_CAPACITY[]   =   { 0x06, 0x07 };
static const Uint8 SDC_VOLTAGE[]                =   { 0x08, 0x09 };
static const Uint8 SDC_AVERAGE_CURRENT[]        =   { 0x0a, 0x0b };
static const Uint8 SDC_TEMPERATURE[]            =   { 0x0c, 0x0d };
static const Uint8 SDC_FLAGS[]                  =   { 0x0e, 0x0f };
static const Uint8 SDC_MFR_DATE[]               =   { 0x6b, 0x6c };
static const Uint8 SDC_MFR_NAME_LENGTH[]        =   { 0x6d };
static const Uint8 SDC_MFR_NAME[]               =   { 0x6e, 0x6f, 0x70, 0x71, 
                                               0x72, 0x73, 0x74, 0x75,
                                               0x76, 0x77, 0x78 };
static const Uint8 SDC_DEVICE_CHEMISTRY_LENGTH[]=   { 0x79 };
static const Uint8 SDC_DEVICE_CHEMISTRY[]       =   { 0x7a, 0x7b, 0x7c, 0x7d };
static const Uint8 SDC_SERIAL_NUMBER[]          =   { 0x7e, 0x7f };

// Extended Data Commands definition
static const Uint8 EDC_PACK_CONFIGURATION[]	 =   { 0x3a, 0x3b };


// Sub commands for SDC_CONTROL
static const Uint16 CNTL_CONTROL_STATUS             =   0x0000;
static const Uint16 CNTL_DEVICE_TYPE                =   0x0001;
static const Uint16 CNTL_FW_VERSION                 =   0x0002;
static const Uint16 CNTL_HW_VERSION                 =   0x0003;
static const Uint16 CNTL_RESET_DATA                 =   0x0005;
static const Uint16 CNTL_PREV_MACWRITE              =   0x0007;
static const Uint16 CNTL_CHEM_ID                    =   0x0008;
static const Uint16 CNTL_BOARD_OFFSET               =   0x0009;
static const Uint16 CNTL_CC_OFFSET                  =   0x000a;
static const Uint16 CNTL_CC_OFFSET_SAVE             =   0x000b;
static const Uint16 CNTL_DF_VERSION                 =   0x000c;
static const Uint16 CNTL_SET_FULLSLEEP              =   0x0010;
static const Uint16 CNTL_STATIC_CHEM_CHKSUM         =   0x0017;
static const Uint16 CNTL_CURRENT                    =   0x0018;
static const Uint16 CNTL_SEALED                     =   0x0020;
static const Uint16 CNTL_IT_ENABLE                  =   0x0021;
static const Uint16 CNTL_CAL_ENABLE                 =   0x002d;
static const Uint16 CNTL_RESET                      =   0x0041;
static const Uint16 CNTL_EXIT_CAL                   =   0x0080;
static const Uint16 CNTL_ENTER_CAL                  =   0x0081;
static const Uint16 CNTL_OFFSET_CAL                 =   0x0082;

EBM_bq34z110* EBM_bq34z110::instance_ = NULL;

EBM_bq34z110* EBM_bq34z110::getInstance()
{
	if ( NULL == instance_ )
	{
		HANDLE deviceHandle = CreateFile(_T("I2C2:"), GENERIC_READ|GENERIC_WRITE, 
			0, NULL, OPEN_EXISTING, 0, NULL);

		if ( deviceHandle != INVALID_HANDLE_VALUE )
		{
			instance_ = new EBM_bq34z110(deviceHandle, BQ34Z110_WRITE_ADDR,
				I2C_SUBADDRESS_MODE_1, I2C_SLOWSPEED_MODE, 50);

			// If create instance failed, close the I2C file handle
			if( instance_ == NULL )
			{
				CloseHandle(deviceHandle);
			}
		}
	}

	return instance_;
}

EBM_bq34z110::EBM_bq34z110(HANDLE FileHandle, uint8_t BusAddress, 
		I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout)
		: I2C( FileHandle, BusAddress, SubAddrMode, BaudIndex, Timeout )
		, isX10Mode_( false )
{
}

EBM_bq34z110::~EBM_bq34z110(void)
{
}


bool EBM_bq34z110::readStateOfCharge( Uint16& socPercent )
{
	// Read two bytes StateOfCharge from I2C
	I2cReturnCode_t retCode
        = I2C::ReadDevice( SDC_STATE_OF_CHARGE[0], &socPercent );

	return (retCode == I2C_SUCCESSFUL);
}

bool EBM_bq34z110::readRemainingCapacity(Uint16 & remainingCapacity)
{
	I2cReturnCode_t retCode
        = I2C::ReadDevice( SDC_REMAINING_CAPACITY[0], &remainingCapacity );

	return (retCode == I2C_SUCCESSFUL);
}

bool EBM_bq34z110::readFullChargeCapacity(Uint32 & fullChargeCapacity)
{
	Uint16 fullChargeCapacity16 = 0;

	I2cReturnCode_t retCode 
        = I2C::ReadDevice( SDC_FULL_CHARGE_CAPACITY[0], &fullChargeCapacity16 );

	if ( retCode == I2C_SUCCESSFUL )
	{
		fullChargeCapacity = fullChargeCapacity16;
		
		// By 10 when X10 mode is selected in Pack Configuration
		if ( isX10Mode_ )
		{
			fullChargeCapacity *= 10;
		}
		
		return true;
	}

	return false;
}

bool EBM_bq34z110::readVoltage(Uint16 & voltage)
{
	I2cReturnCode_t retCode = I2C::ReadDevice( SDC_VOLTAGE[0], &voltage );

	return (retCode == I2C_SUCCESSFUL);
}

bool EBM_bq34z110::readAverageCurrent(Int32 & averageCurrent)
{
	Int16 averageCurrent16 = 0;

	I2cReturnCode_t retCode 
        = I2C::ReadDevice( SDC_AVERAGE_CURRENT[0], &averageCurrent16 );

	if ( retCode == I2C_SUCCESSFUL )
	{
		averageCurrent = averageCurrent16;

		// By 10 when X10 mode is selected in Pack Configuration
		if ( isX10Mode_ )
		{
			averageCurrent *= 10;
		}

		return true;
	}

	return false;
}

bool EBM_bq34z110::readInstantaneousCurrent( Int32& instCurrent )
{
    Uint16 instCurrent16 = 0;

    if ( control_( CNTL_CURRENT, instCurrent16 ) )
    {
        instCurrent = static_cast<Int16>(instCurrent16);
        if ( isX10Mode_ )
        {
            instCurrent *= 10;
        }

        return true;
    }

    return false;
}

bool EBM_bq34z110::readTemperature(Uint16 & temperature)
{
	I2cReturnCode_t retCode = I2C::ReadDevice( SDC_TEMPERATURE[0], &temperature );

	return (retCode == I2C_SUCCESSFUL);
}

bool EBM_bq34z110::readPackConfiguration_(Uint16 & packConfiguration)
{
	I2cReturnCode_t retCode 
        = I2C::ReadDevice( EDC_PACK_CONFIGURATION[0], &packConfiguration );

	return (retCode == I2C_SUCCESSFUL);
}

// Send Control Sub Command to EBM and expect 2 bytes output
bool EBM_bq34z110::control_(Uint16 cntlSubCommand, Uint16 & data)
{
    bool sendCommandOk = control_( cntlSubCommand );

    if ( sendCommandOk )
    {
        I2cReturnCode_t retCode = I2C::ReadDevice( SDC_CONTROL[0], &data );
        return (retCode == I2C_SUCCESSFUL);
    }

    return false;
}

// Send Control Sub Command to EBM
bool EBM_bq34z110::control_(Uint16 cntlSubCommand)
{
    I2cReturnCode_t retCode = I2C::WriteDevice(SDC_CONTROL[0], cntlSubCommand);
    return (retCode == I2C_SUCCESSFUL);
}
