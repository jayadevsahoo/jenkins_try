#ifndef BreathTimingSubScreen_HH
#define BreathTimingSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BreathTimingSubScreen - Displays the Breath Timing Diagram
// in the Lower Subscreen Area when the following Main Settings are selected in
// the Main Settings Area: Expiratory Time, Flow Pattern, I:E Ratio,
// Inspiratory Time, Peak Flow, Plateau Time, Respiratory Rate and Tidal
// Volume.  No adjustable settings are contained in this subscreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BreathTimingSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  22-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added general framework for tracking setting displays, with
//         automatic placement and display
//      *  added three new tracking settings:  minute volume, Vt-to-IBW
//         ratio, and Vt-supp-to-IBW ratio
//      *  removed unused 'ArrMainSettingChangeStates_[]'
//
//  Revision: 004  By:  hhd	   Date:  07-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//      Added batch change capability to the MainSettingsArea, whereby
//      multiple settings can be changed and accepted as a group.
//
//  Revision: 003  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"
#include "GuiEventTarget.hh"
#include "AdjustPanelTarget.hh"

//@ Usage-Classes
#include "BreathTimingDiagram.hh"
#include "SubScreenTitleArea.hh"
#include "GuiAppClassIds.hh"
#include "SettingId.hh"
#include "TextButton.hh"
#include "Line.hh"
#include "SettingButton.hh"
//@ End-Usage

class BreathTimingSubScreen : public SubScreen, public GuiEventTarget,
							public AdjustPanelTarget, public SettingObserver
{
public:
	enum MainSettingAreaState
	{
		PASSIVE,
		UNMODIFIED,
		MODIFIED,
		DESELECTED,
		NUM_STATE
	};

	enum MainSettingAreaCondition_
	{
		NOT_BY_OPERATOR,
		NOT_CHANGED = NOT_BY_OPERATOR,
		BY_OPERATOR,
		CHANGED = BY_OPERATOR,
		NUM_CONDITION
	};

	enum NumChanges
	{
		ZERO,
		GT_ZERO,
		NUM_CHANGES
	};


	BreathTimingSubScreen(SubScreenArea *pSubScreenArea);
	~BreathTimingSubScreen(void);

	// Redefine SubScreen methods
	virtual void activate(void);
	virtual void deactivate(void);
	virtual void acceptHappened(void);
	virtual void initializeMainSetting(void);

	// GuiEventTarget virtual method
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
									Boolean byOperatorAction);


	// AdjustPanelTarget virtual methods
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);

	// SettingObserver virtual method
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject);

	// static methods used by MainSettingsArea to forward information to this
	// class' instance...
	static void ValueUpdateHappened (const Notification::ChangeQualifier qualifierId,
									 const SettingSubject*               pSubject);
	static Boolean UserEventHappened(const SettingId::SettingIdType   settingId,
									 const SettingButton::UserEventId userEventId,
									 const Boolean                    byOperatorAction);

	static void SoftFault(const SoftFaultID softFaultID,
			  			  const Uint32      lineNumber,
			  			  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	BreathTimingSubScreen(void);							// not implemented..
	BreathTimingSubScreen(const BreathTimingSubScreen&);	// not implemented..
	void operator=(const BreathTimingSubScreen&);			// not implemented..

	void  updateDisplay_(Boolean displayCancelButton=FALSE);
	void  updateTrackingDisplay_(const Boolean                  isPressedDown,
								 const SettingId::SettingIdType settingId);

	static Int32 getNumChanged(void);
	
	//@ Data-Member: breathTimingDiagram_
	// This is the breath timing diagram itself.
	BreathTimingDiagram breathTimingDiagram_;

	//@ Data-Member: cancelButton_
	// The cancel command button
	TextButton cancelButton_;

	//@ Data-Member: breathTimingLabel_
	// The breath timing label.
	TextField  breathTimingLabel_;

	//@ Data-Member: xtionTable_
	// State transition table for the events.
	Int32 xtionTable_[NUM_STATE][SettingButton::NUM_EVENT][NUM_CONDITION][NUM_CHANGES];

	//@ Data-Member: MainSettingAreaState_;
	// Flag indicates the current state for the MainSettingsArea.
	static MainSettingAreaState MainSettingAreaState_;

	enum { NUM_TRACKING_INFO_    = 3 };
	enum { NUM_TRACKED_SETTINGS_ = 2 };

	struct TrackingInfo_
	{
		SettingId::SettingIdType  settingId;
		TouchableText*            pLabel;
		NumericField*             pValue;
		SettingId::SettingIdType  arrTrackingIds[NUM_TRACKED_SETTINGS_ + 1];
	};

	//@ Data-Member: ArrTrackingValues_
	// Boolean array indicates the changing state for each setting
	// buttons in the MainSettingArea.
	TrackingInfo_  arrTrackingValuesInfo_[NUM_TRACKING_INFO_ + 1];

	TouchableText  minuteVolumeLabel_;
	NumericField   minuteVolumeValue_;

	TouchableText  vtIbwRatioLabel_;
	NumericField   vtIbwRatioValue_;

	TouchableText  vsuppIbwRatioLabel_;
	NumericField   vsuppIbwRatioValue_;

	Container  trackingSettingArea_;
};


#endif // BreathTimingSubScreen_HH 
