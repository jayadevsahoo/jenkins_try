#ifndef OperatingComponent_HH
#define OperatingComponent_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: OperatingComponent - Alarm-Analysis class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/OperatingComponent.hhv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

//@ Usage-Classes
#include "Alarm_Analysis.hh"
#include "AlarmOperand.hh"

//@ End-Usage


class OperatingComponent 
{
  public:
 
    enum LogicalOperator {OR, AND};

    OperatingComponent(AlarmOperand&                             rChild,
                       const OperatingComponent::LogicalOperator lo,
                       const Boolean                             neg);
    OperatingComponent(const OperatingComponent& rOpComp);
    virtual ~OperatingComponent();

    Boolean evaluate(Boolean state);

    inline  AlarmOperand&   getChild(void);

    inline  Boolean operator==     (const  OperatingComponent& rOpComp) const;
    inline  Boolean operator!=     (const  OperatingComponent& rOpComp) const;

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

 
#ifdef SIGMA_UNIT_TEST
    inline OperatingComponent::LogicalOperator getLogOp(void) const;
    inline Boolean getNegate(void) const;
#endif
 


  protected:

  private:

    OperatingComponent();                      // Declared only
    void operator=(const OperatingComponent&); // Declared only

    //@ Data-Member:    rChild_
    // Reference to the child AlarmOperand.
    AlarmOperand&         rChild_;

    //@ Data-Member:    logOp_
    // Enumerated value representing the logical operator used to combine the
    // state of the child AlarmOperand (referred to by attribute rChild_) with 
    // the states of the other child AlarmOperand objects.
    const OperatingComponent::LogicalOperator logOp_;

    //@ Data-Member:    negate_
    // Boolean value indicating whether or not the state of the child 
    // AlarmOperand (referred to by attribute rChild_) should be inverted 
    // before being combined with the states of the other child AlarmOperand 
    // objects.
    const Boolean         negate_;

};

// Inlined methods
#include "OperatingComponent.in"

#endif // OperatingComponent_HH 
