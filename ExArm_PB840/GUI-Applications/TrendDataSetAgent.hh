#ifndef TrendDataSetAgent_HH
#define TrendDataSetAgent_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendDataSetAgent - Trending dataset agent
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendDataSetAgent.hhv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc    Date:  05-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  Initial Version
//=====================================================================
#include "GuiApp.hh"
#include "GuiTimerTarget.hh"
#include "SettingObserver.hh" 
#include "TrendEventTarget.hh"

//@ Usage-Classes
#include "TrendDataMgr.hh"
#include "TrendDataSet.hh"
#include "TrendRequestInfo.hh"
#include "SettingId.hh"

class SubScreenArea;
//@ End-Usage

class TrendDataSetAgent :
	public GuiTimerTarget, 
	public SettingObserver,
	public TrendEventTarget
{
public:

	// public interface to the singleton object
	static TrendDataSetAgent& GetTrendDataSetAgent(void);

	// Register a TrendEventTarget for trendDataReady callbacks
	static void RegisterTarget(TrendEventTarget* pTarget);

	void requestTrendData(void);

	inline Int32 getCursorFrame(void) const;
	inline Int32 getCursorPosition(void) const;
	inline const TrendDataSet& getTrendDataSet(void);
	inline Boolean isDataSetMostCurrent(void) const;

	// GuiTimerTarget virtual
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// SettingObserver virtual
	virtual void valueUpdate(const Notification::ChangeQualifier qualifierId,
							 const SettingSubject*               pSubject);

	// TrendEventTarget virtual
	virtual void trendDataReady(TrendDataSet& rTrendDataSet);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


private:
	TrendDataSetAgent(void);
	~TrendDataSetAgent(void);

	// these methods are purposely declared, but not implemented...
	TrendDataSetAgent(const TrendDataSetAgent&);	// not implemented...
	void operator=(const TrendDataSetAgent&);		// not implemented...

	void setTrendSelectValue_(Int32 column, SettingId::SettingIdType settingId);

	void updateCursorFrame_(void);

	void registerTarget_(TrendEventTarget* pTarget);

	//@ Data-Member: trendDataRequest_
	// Contains the current trend data request information
	TrendRequestInfo trendDataRequest_;

	//@ Data-Member: trendDataSet_
	// Contains the current trend data set.
	TrendDataSet trendDataSet_;

	//@ Data-Member: cursorPosition_
	// The cursor position displacement from the right side of the trend
	// plots. Range is [0..359]
	Int32 cursorPosition_;

	//@ Data-Member: currentFrame_
	// The starting data position (frame number) of the trend data set.
	// Note: The frame number is a "one-up" number maintained by the
	// Trend-Database for each 10 second update.
	Int32 currentFrame_;

	//@ Data-Member: cursorFrame_
	// The data position (frame number) where the cursor is positioned in
	// the trending database.
	Int32 cursorFrame_;

	//@ Data-Member: previousEndFrame_
	// The data position (frame number) of last frame (most current time)
	// during the previous database update request. Used to calculate the
	// number of frames collected since the last database request to
	// formulate a new database request when the cursor is positioned in an
	// older window than the most current data window so the cursor remains
	// at the selected row.
	Int32 previousEndFrame_;

	//@ Data-Member: trendSelectValue_
	// The current set of trend parameter selection settings (one for each
	// trend select button)
	TrendSelectValue::TrendSelectValueId trendSelectValue_[TrendDataMgr::MAX_COLUMNS];

	//@ Data-Member: numColumnsRequested_
	// The number of columns (trend parameters) requested from the database
	Int32 numColumnsRequested_;

	//@ Data-Member: previousTimeScale_
	// The time scale requested during the previous database retrieval. Used
	// to determine when the cursor should be recentered (when changing time
	// scales).
	TrendTimeScaleValue::TrendTimeScaleValueId previousTimeScale_;

	//@ Data-Member: requestedTimeScale_
	// The time scale requested during the current database retrieval. 
	TrendTimeScaleValue::TrendTimeScaleValueId requestedTimeScale_;

	//@ Data-Member: isFirstRequest_
	// TRUE if this is the first database retrieval request. Required to
	// initialize "previous" values appropriately.
	Boolean isFirstRequest_;

	//@ Data-Member: isDataRequestPending_
	// TRUE while a database request is outstanding.
	Boolean isDataRequestPending_;

	//@ Data-Member: isDataSetMostCurrent_
	// TRUE when the dataset received from TrendDataMgr is for most current
	// time frame window.
	Boolean isDataSetMostCurrent_;

	//@ Constant: MAX_TARGETS_
	// The maximum number of target pointers that can be stored
	enum
	{
		MAX_TARGETS = 5
	};

	//@ Data-Member: targetArray_
	// A static array of TrendEventTarget classes
	TrendEventTarget* targetArray_[MAX_TARGETS];

	//@ Data-Member: numTargets_
	// The number of targets currently registered with this class.
	Int32 numTargets_;

};

#include "TrendDataSetAgent.in"

#endif // TrendDataSetAgent_HH 
