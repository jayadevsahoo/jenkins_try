
#ifndef DListC_AlarmUpdate_NUM_ALARM_HH
#define DListC_AlarmUpdate_NUM_ALARM_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: DListC_AlarmUpdate_NUM_ALARM - Fixed Doubly-Linked List of 'AlarmUpdate' Items.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedDListC.H_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "AlarmConstants.hh"
#include "AlarmConstants.hh"

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "DListC_AlarmUpdate.hh"
#include "Heap_DNodeC_AlarmUpdate_NUM_ALARM.hh"
//@ End-Usage


class DListC_AlarmUpdate_NUM_ALARM : public DListC_AlarmUpdate
{
  public:
    DListC_AlarmUpdate_NUM_ALARM(const DListC_AlarmUpdate_NUM_ALARM& dList);
    DListC_AlarmUpdate_NUM_ALARM(void);
    virtual ~DListC_AlarmUpdate_NUM_ALARM(void);

    inline void  operator=(const DListC_AlarmUpdate_NUM_ALARM& list);

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    //@ Data-Member:  heapOfNodes_
    // A heap where each block contains memory for each list node.
    Heap_DNodeC_AlarmUpdate_NUM_ALARM  heapOfNodes_;
};
 

// Inlined methods...
#include "DListC_AlarmUpdate_NUM_ALARM.in"


#endif  // DListC_AlarmUpdate_NUM_ALARM_HH
