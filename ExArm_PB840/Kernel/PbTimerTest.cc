#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1998, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class:  PbTimerTest - test code for the Puritan-Bennett FPGA timers
//
//---------------------------------------------------------------------
//@ Interface-Description
//
//  >Von
//    This module contains routines to test the timers implemented in
//    FPGA. These timers shall be refered to as the Puritan-Bennett (PB)
//    timers to distinguish them from the Intel 82C54 timer devices. The
//    FPGA implements three timers that are tested by this module.
//
//  Methods:
//    Test             -  Performs the tests
//    TestRegisters_   -  verifies a count can be set and read
//    TestInterrupts_  -  verifies the interrupts tied to the timers
//    TestAccuracy_    -  checks 5ms timer accuracy against Time of Day Clock
//
//@ Implementation-Description
//
//@ Restrictions
//
// The Time of Day Clock must be working and running.  This can be ensured
// by running this test after the Time of Day Clock test.
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//
//@(#) $Header:   /840/Baseline/Kernel/vcssrc/PbTimerTest.ccv   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003    By: Mitesh Raval   Date: 14-Oct-2010  SCR Number: 6671
//  Project: XENA2
//  Description:
//       Occurrences of DS1286 renamed to Delay.
//
//  Revision: 002    By: Gary Cederquist  Date: 26-Apr-1999  DCS Number: 5370
//    Project: 840 Cost Reduction
//    Description:
//       Corrected revision code conflict.
//
//  Revision: 001    By: Gary Cederquist  Date: 21-Oct-1998  DCS Number: 5311
//    Project: 840 Cost Reduction
//    Description:
//       Initial version
//
//=====================================================================

#include "PbTimerTest.hh"
#include "PbTimer.hh"
#include "kernel.hh"
#include "postnv.hh"
#include "Delay.hh"
#include "CpuDevice.hh"
#include "MemoryMap.hh"
//TODO E600 #include "NmiSource.hh"
//TODO E600 this whole file and possibly the whole Kernel folder should be removed
#include "Post_Library.hh"

#if defined(SIGMA_DEBUG)
#include <stdio.h>
#include "misc.hh"
#endif


static volatile Boolean Autovec3Seen_;
static volatile Boolean Autovec4Seen_;
static volatile Boolean Autovec6Seen_;

static volatile Boolean TimerInterruptSeen_;

//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method:   PbTimerCountTest - test the timers count registers
//
//@ Interface-Description
//
// This function tests the ability of the CPU to write to and read from
// the PB timer registers.
//
//    Inputs:  NONE
//
//    Outputs: NONE
//
//    Returns: TRUE - failed
//             FALSE - passed
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This function uses an array of count values to test that each timer
//  can have it's preload register written and read back, and have its
//  counter register read back.
//
//  For each test count:
//
//    1. The counter is stopped.
//
//    2. The preload register is loaded.
//
//    3. The preload register is read back and compared to the
//       value written to the preload register. If the compare
//       fails, the function returns TRUE to indicate a failure.
//
//    4. The counter register is read back and compared to the
//       value written to the preload register. If the compare
//       fails, the function returns TRUE to indicate a failure.
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================

Boolean
PbTimerTest::TestRegisters_(void)
{
  const Uint32  TIMER_TEST_COUNT[] =
  {
	0x00000000 & PBTIMER_MAX,
	0xAAAAAAAA & PBTIMER_MAX,
	0x55555555 & PBTIMER_MAX,
	0xffffffff & PBTIMER_MAX,
  };

  Boolean failed = FALSE;

  for (Uint index = 0 ; index < countof(TIMER_TEST_COUNT) && !failed; index++)
  {
	Uint32 count = TIMER_TEST_COUNT[index];

	//  for each counter, set the preload reg and read back the counter

	failed = TestRegister_( P_PBTIMER_1_CONTROL,
							P_PBTIMER_1_PRELOAD,
							P_PBTIMER_1_COUNTER,
							count )
		  || TestRegister_( P_PBTIMER_2_CONTROL,
							P_PBTIMER_2_PRELOAD,
							P_PBTIMER_2_COUNTER,
							count )
		  || TestRegister_( P_PBTIMER_3_CONTROL,
							P_PBTIMER_3_PRELOAD,
							P_PBTIMER_3_COUNTER,
							count );            // $[TI1.1] $[TI1.2]
  }

  return failed;
}

//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method: TestRegister_ - utility for timer register test
//
//@ Interface-Description
//
//  This routine tests the ability of an PB timer to be programmed with
//  a preload value that can then be read back from the preload register
//  and from the counter register.
//
//    Inputs:
//        pControlReg   - pointer to the control register
//        pPreloadReg   - pointer to the preload register
//        pCounterReg   - pointer to the counter register
//		  initCount     - initial counter preload value
//
//    Outputs:
//
//	  Returns: FALSE - passed
//             TRUE  - failed
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================

Boolean
PbTimerTest::TestRegister_(volatile Uint8 * pControlReg,
				 		   volatile Uint32 * pPreloadReg,
				 		   volatile Uint32 * pCounterReg,
				 		   Uint32 initCount)
{
	return 0;
	// TODO E600 port
	/*

  Uint32 counter;
  Uint32 preload;

  // stop the counter
  *pControlReg = 0;

  // load preload register
  *pPreloadReg = initCount;

  //  force CPU to synchronize bus access so count register read
  //  occurs after writing to the preload register
  asm(" nop ");

  // read back the preload
  preload = *pPreloadReg;

  // read back the counter
  counter = *pCounterReg;

UNIT_TEST_POINT(POSTUT_12B)

  return ( preload != initCount || counter != initCount );
  // $[TI1.1] $[TI1.2]
  */
}

//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method:   TestInterrupts_ - Test PbTimer Interrupt Capability
//
//@ Interface-Description
//  This test assures all counters can generate an interrupt
//  within a specified time.  This ensures that the interrupts used to
//  time the watchdog reset circuit will occur.  This also ensures that
//  if one interrupt line is shorted to another, POST will not continue.
//  Disabling the watchdog during POST depends on the interrupt working
//  correctly.  Without the strobing interrupt, POST would enter a
//  continuous reset condition.  $[11044]
//
//    Inputs:  NONE
//
//    Outputs: NONE
//
//    Returns: 	FALSE - passed
//				TRUE  - failed
//
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================

Boolean
PbTimerTest::TestInterrupts_(void)
{
  // 15ms == 50000 with instruction cache on for delay count
  const Int32 TIMER_INTERRUPT_DELAY_COUNT = 50000;
  const Uint32 AUTOVEC3_VECTOR = 27;
  const Uint32 AUTOVEC4_VECTOR = 28;
  const Uint32 AUTOVEC6_VECTOR = 30;

  Boolean     failed = FALSE;

UNIT_TEST_POINT(POSTUT_24B)

  //  Disable interrupts during setup
  DISABLE_INTERRUPTS();

  Autovec3Seen_ = FALSE;
  Autovec4Seen_ = FALSE;
  Autovec6Seen_ = FALSE;

  //TODO E600: This is from Tony F
#ifdef E600_840_TEMP_REMOVED
  Handler old_isr3 = InstallHandler( AUTOVEC3_VECTOR, PbTimerTest::Av3Handler_);
  Handler old_isr4 = InstallHandler( AUTOVEC4_VECTOR, PbTimerTest::Av4Handler_);
  Handler old_isr6 = InstallHandler( AUTOVEC6_VECTOR, PbTimerTest::Av6Handler_);
#endif

  // **** clear any AutoVector interrupts that may be set ****
  CpuDevice::ResetAutovector3();
  CpuDevice::ResetAutovector4();
  CpuDevice::ResetAutovector6();

  // disable the counters
  *P_PBTIMER_1_CONTROL = 0;
  *P_PBTIMER_2_CONTROL = 0;
  *P_PBTIMER_3_CONTROL = 0;

  //  set the counters to expire within 1 ms - 8Mhz
  *P_PBTIMER_1_PRELOAD = 4000;   // 4000 * 125ns = 0.5ms
  *P_PBTIMER_2_PRELOAD = 4000;
  *P_PBTIMER_3_PRELOAD = 4000;

  // enable the counters
  *P_PBTIMER_1_CONTROL = PBTIMER_EN;
  *P_PBTIMER_2_CONTROL = PBTIMER_EN;
  *P_PBTIMER_3_CONTROL = PBTIMER_EN;

  CpuDevice::EnableAutovector(  ENABLE_AUTOVEC_3    // counter 0
							  | ENABLE_AUTOVEC_4    // counter 1
							  | ENABLE_AUTOVEC_6 ); // counter 2

  ENABLE_INTERRUPTS();

UNIT_TEST_POINT(POSTUT_24B_1)

  //  error if interrupts occur immediately
  if ( Autovec3Seen_ || Autovec4Seen_ || Autovec6Seen_ )
  { 													// $[TI1.1]
	failed = TRUE;
  }
  else
  { 													// $[TI1.2]
	for (  Int32 i=TIMER_INTERRUPT_DELAY_COUNT
		  ; i>0 && (!Autovec3Seen_ || !Autovec4Seen_ || !Autovec6Seen_)
		  ; i--)
	{
	  // delay
	}

	DISABLE_INTERRUPTS();

UNIT_TEST_POINT(POSTUT_24B_2)
	if ( !Autovec3Seen_ || !Autovec4Seen_ || !Autovec6Seen_ )
	{ 												// $[TI2.1]
	  failed = TRUE;
	} 												// $[TI2.2]

  }

  CpuDevice::EnableAutovector( ENABLE_AUTOVEC_NONE );

  // **** reset any AutoVector interrupts that might have been set ****
  CpuDevice::ResetAutovector3();
  CpuDevice::ResetAutovector4();
  CpuDevice::ResetAutovector6();

  //TODO E600: this is from Tony F
#ifdef E600_840_TEMP_REMOVED
  InstallHandler( AUTOVEC3_VECTOR, old_isr3 );
  InstallHandler( AUTOVEC4_VECTOR, old_isr4 );
  InstallHandler( AUTOVEC6_VECTOR, old_isr6 );
#endif

  ENABLE_INTERRUPTS();

  return failed;
}


//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method: TestAccuracy_ - test the accuracy of a timer
//
//@ Interface-Description
//
// This function measures the frequency (accuracy) of the PB Timer 3.
// This counter is specifically tested since it is used to drive the
// operating system and breath delivery cycle times (ie. the 5ms timer).
// It measures the timer's accuracy against the Time of Day Clock. $[11043]
//
//    Inputs:  NONE
//
//    Outputs: NONE
//
//    Returns: 	FALSE - passed
//				TRUE  - failed
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
// This test:
//
//    1. The counter is disabled.
//
//    2. The specified count is loaded into the counter.
//
//    3. Wait for a transition on the Time of Day clock 1/100th second tick.
//
//    4. Enable the counter to start counting down.
//
//    5. Wait for a transition of the time of day clock.
//
//    Note: The DS1286 does not provide a true 10 millisec count in
//          the 1/100ths sec register.  The count is an "average" 100 Hz.
//          The time between updates of the 1/100th sec register are
//          usually 10 millisec, but occasionally one is short by
//          .24 milliseconds.
//
//          A possible measurement error of 2.4% is introduced by using
//          only one tick of the time of day clock.  However, since the
//          crystal oscillator driving the PB timers is
//          accurate to within 0.01%, this measurement inaccuracy is
//          acceptable.  A "short" clock will simply force a higher
//          accuracy (2.6%) instead of 5% for the nominal case.
//
//    6. The counter is read back.
//
//    7. The number of counts that have elapsed in the timers is calculated.
//       if the count is high or low by an amount greater than the allowed
//       margins, this function returns with a status of MAJOR to
//       indicate the timers are out of spec.
//
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================

Boolean
PbTimerTest::TestAccuracy_(void)
{
  // define test parameters using #define since compiler does not
  // resolve floating point intermediates until run-time and POST
  // has not enabled the FPU at this point.

#define TIMER_CLK 					(8000000)
#define CLOCK_TICKS_FOR_MEASUREMENT (1)
#define MEASUREMENT_TIME 			(0.01 * CLOCK_TICKS_FOR_MEASUREMENT)
#define NUMBER_OF_CLOCKS 			(TIMER_CLK * MEASUREMENT_TIME)
#define MAX_CLOCKS					Uint32((1.0 + 0.05) * NUMBER_OF_CLOCKS)
#define MIN_CLOCKS 					Uint32((1.0 - 0.05) * NUMBER_OF_CLOCKS)

  const Uint32 STARTING_COUNT = PBTIMER_MAX;
  const Uint32 TIMER_INTERRUPT_VECTOR = 30;

  Boolean    failed = FALSE;

  Uint32 finalCount;
  Uint32 elapsedCount;
  Boolean timerRolledOver;
  Boolean clockOk;

  //  Disable interrupts during setup
  DISABLE_INTERRUPTS();

  TimerInterruptSeen_ = FALSE;

  //TODO E600 this is from Tony F
#ifdef E600_840_TEMP_REMOVED
  Handler old_isr = InstallHandler(  TIMER_INTERRUPT_VECTOR
								   , PbTimerTest::InterruptHandler_);
#endif

  // *****************************************************************
  // ****** Set up PB Timer 3  (5ms timer)                       *****
  // *****************************************************************

  // **** stop the counter
  *P_PBTIMER_3_CONTROL = 0;
  *P_PBTIMER_3_PRELOAD = STARTING_COUNT;

  // **** wait for a tick transition on the Time of Day Clock
  clockOk = Delay::WaitForTicks(1);

UNIT_TEST_POINT(POSTUT_13B_1)

  if ( !clockOk )
  { 											// $[TI1.1]
	failed = TRUE;
  }
  else
  { 											// $[TI1.2]
	// **** clear any previous interrupts
	CpuDevice::ResetAutovector6();

	// **** enable the counter
	*P_PBTIMER_3_CONTROL = PBTIMER_EN;

	CpuDevice::EnableAutovector(ENABLE_AUTOVEC_6);  // counter 2

	//  enable interrupts
	ENABLE_INTERRUPTS();

	// **** wait for tick transitions on the Time of Day Clock
	if (!Delay::WaitForTicks(CLOCK_TICKS_FOR_MEASUREMENT))
	{ 											// $[TI2.1]
	  failed = TRUE;
	}
	else
	{ 											// $[TI2.2]
	  // **** read back the count
	  finalCount  = *P_PBTIMER_3_COUNTER;
	  timerRolledOver = TimerInterruptSeen_;

	  //  Disable interrupts
	  DISABLE_INTERRUPTS();

	  // **** reset any AutoVector interrupts that might have been set ****
	  CpuDevice::ResetAutovector3();
	  CpuDevice::ResetAutovector4();
	  CpuDevice::ResetAutovector6();

	  //  Mask all autovector interrupts
	  CpuDevice::EnableAutovector(ENABLE_AUTOVEC_NONE);

	  //  Disable the timer
	  *P_PBTIMER_3_CONTROL = 0;

	  //TODO E600 this is from Tony F
#ifdef E600_840_TEMP_REMOVED
	  (void) InstallHandler( TIMER_INTERRUPT_VECTOR, old_isr );
#endif

	  elapsedCount = STARTING_COUNT - finalCount;

UNIT_TEST_POINT(POSTUT_13B)

	  failed = (   elapsedCount > MAX_CLOCKS
				|| elapsedCount < MIN_CLOCKS
				|| timerRolledOver );			// $[TI3.1] $[TI3.2]
	}
  }
  ENABLE_INTERRUPTS();

  return failed;
}


//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method:  DisableTimers_
//
//@ Interface-Description
//
//  Utility for disabling all PB timers.
//
//  Inputs:  NONE
//
//  Outputs: NONE
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This function disables the timers by reseting the control register.
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================
void
PbTimerTest::DisableTimers_(void)
{
  DISABLE_INTERRUPTS();

  *P_PBTIMER_1_CONTROL = 0;
  *P_PBTIMER_2_CONTROL = 0;
  *P_PBTIMER_3_CONTROL = 0;

  CpuDevice::ResetAutovector3();
  CpuDevice::ResetAutovector4();
  CpuDevice::ResetAutovector6();

  ENABLE_INTERRUPTS();
  // $[TI1]
}


//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method: InterruptHandler_ -  flag AutoVector 6 interrupts
//
//@ Interface-Description
//
//  INPUTS: NONE
//
//  OUPUTS: NONE
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This handler sets the flag TimerInterruptSeen_ to TRUE, then
//  returns.
//
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

// TODO E600 removed the keyword "interrupt"
void
PbTimerTest::InterruptHandler_(void)
{
  CpuDevice::ResetAutovector6();
  TimerInterruptSeen_ = TRUE;
  // $[TI1]
}

//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method:   Av3Handler_
//
//@ Interface-Description
//  This is the interrupt handler to process autovector level 3 interrupts
//  during the PbTimerInterruptTest.  It sets the Autovec3Seen_ TRUE.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================
// TODO E600 removed the keyword "interrupt"
void
PbTimerTest::Av3Handler_(void)
{
  CpuDevice::ResetAutovector3();
  Autovec3Seen_ = TRUE;
  // $[TI1]
}

//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method:   Av4Handler_
//
//@ Interface-Description
//  This is the interrupt handler to process autovector level 4 interrupts
//  during the PbTimerInterruptTest.  It sets the Autovec4Seen_ TRUE.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================
// TODO E600 removed the keyword "interrupt"
void
PbTimerTest::Av4Handler_(void)
{
  CpuDevice::ResetAutovector4();
  Autovec4Seen_ = TRUE;
  // $[TI1]
}

//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method:   Av6Handler_
//
//@ Interface-Description
//  This is the interrupt handler to process autovector level 6 interrupts
//  during the PbTimerInterruptTest.  It sets the Autovec6Seen_ TRUE.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================

// TODO E600 removed the keyword "interrupt"
void
PbTimerTest::Av6Handler_(void)
{
  CpuDevice::ResetAutovector6();
  Autovec6Seen_ = TRUE;
  // $[TI1]
}


//============== M E T H O D   D E S C R I P T I O N ===================
//@ Method:  Test
//
//@ Interface-Description
//
// This method tests the PB timers by performing a register test, interrupt
// test, and accuracy test.
//
//    Inputs:  NONE
//
//    Outputs: PASSED - passed
//             MAJOR - failed
//
//---------------------------------------------------------------------
//@ Implementation-Description
//
// This function call the three tests that make up the timer test suite.
// If any of them fail, this function returns with the appropriate
// status.
//
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================
Criticality
PbTimerTest::Test(void)
{
  Boolean failed = FALSE;

  //TODO E600 port this
  // only run test for cost reduction (and later) boards
/*
  if ( CpuDevice::GetBoardRevision() == NMI_COST_REDUCTION )
  {												// $[TI1.1]
	CpuDevice::StrobeWatchdog();
	failed =   PbTimerTest::TestRegisters_()
			|| PbTimerTest::TestInterrupts_()
			|| PbTimerTest::TestAccuracy_();	// $[TI2.1] $[TI2.2]

	// try to disable the timers even if test fails
	PbTimerTest::DisableTimers_();
  }												// $[TI1.2]
*/

  return failed ? MAJOR : PASSED;				// $[TI3.1] $[TI3.2]
}
