// FixHTMLDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FixHTML.h"
#include "FixHTMLDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif





// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CFixHTMLDlg dialog




CFixHTMLDlg::CFixHTMLDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFixHTMLDlg::IDD, pParent)
	, m_strEdit(_T(""))
	, m_strLog(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	pApp = (CFixHTMLApp*)AfxGetApp();
}

void CFixHTMLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDITBOX, m_strEdit);

	DDX_Control(pDX, IDC_LIST2, m_List);
	DDX_Text(pDX, IDC_EDIT_LOG, m_strLog);
}

BEGIN_MESSAGE_MAP(CFixHTMLDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_START, &CFixHTMLDlg::OnBnClickedButtonStart)
	ON_WM_TIMER()
	ON_MESSAGE(WM_FIXING, &CFixHTMLDlg::OnMessage_FixIt)
	ON_MESSAGE(WM_FILEDIRTY, &CFixHTMLDlg::OnMessage_FileDirty)
	ON_MESSAGE(WM_LOG, &CFixHTMLDlg::OnMessage_Log)

	ON_BN_CLICKED(IDC_BUTTON_BROWSE_ROOT_PATH, &CFixHTMLDlg::OnBnClickedButtonBrowseRootPath)
	ON_BN_CLICKED(IDC_BUTTON_STOP, &CFixHTMLDlg::OnBnClickedButtonStop)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST2, &CFixHTMLDlg::OnNMDblclkList2)
END_MESSAGE_MAP()


// CFixHTMLDlg message handlers

BOOL CFixHTMLDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
//InsertColumns()

	m_List.SetExtendedStyle( LVS_EX_FULLROWSELECT | LVS_EX_ONECLICKACTIVATE  | LVS_EX_GRIDLINES | LVS_EDITLABELS );

	LV_COLUMN Column;
	int nCol;

	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	CString szText;
	
	szText = "Status";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	Column.cx = 400;
	nCol = m_List.InsertColumn( 0, &Column );

	szText = "Processing file name";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	
	Column.cx = 500;
	nCol = m_List.InsertColumn( 0, &Column );



	CString str = ReadRegistrySetting(SWAT_REG_PARAM_TREE_PATH);
	GetDlgItem(IDC_EDIT_ROOT_PATH)->SetWindowText(str);
	UpdateData(0);


  

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CFixHTMLDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFixHTMLDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFixHTMLDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CFixHTMLDlg::OnBnClickedButtonStart()
{
	UpdateData(0);

	CString str;
	GetDlgItem(IDC_EDIT_ROOT_PATH)->GetWindowText(str);
	pApp->m_strRootPath = str;

	//do nothing if folder field is empty
	if(str.IsEmpty()) {
		AfxMessageBox(_T("Please select root folder, including language name."));
		return;
	}


	pApp->m_bRun = 1;

	m_List.DeleteAllItems();
	m_strLog.Empty();
	UpdateData(0);

	

	m_pWorkThread = AfxBeginThread((AFX_THREADPROC)Thread_Work, (LPVOID)this);
	SetTimer(111,100, NULL);
	pApp->m_bRunning = true;
	GetDlgItem(IDC_BUTTON_START)->EnableWindow(false);


}


UINT CFixHTMLDlg::Thread_Work(LPVOID lp)
{
	CFixHTMLApp* pApp = (CFixHTMLApp*)AfxGetApp();
	Recurse(pApp->m_strRootPath);
	pApp->m_bRunning = false;

	return 0;
}



void CFixHTMLDlg::Recurse(CString pstr)
{
	//Sleep(50);
	CWaitCursor wait;
	CFixHTMLApp* pApp = (CFixHTMLApp*)AfxGetApp();

   CFileFind finder;
   // build a string with wildcards
   CString strWildcard(pstr);
   strWildcard += _T("\\*.*");

   // start working for files
   BOOL bWorking = finder.FindFile(strWildcard);

   while (bWorking)
   {
	   if(pApp->m_bRun == 0) 
	   {
		   return;
	   }
      bWorking = finder.FindNextFile();

      // skip . and .. files; otherwise, we'd
      // recur infinitely!
      if (finder.IsDots())
	  {
		 CString str = finder.GetFilePath();
         continue;
	  }
	  if (finder.IsNormal())
	  {
 		 CString str = finder.GetFilePath();
		 ShowWorking(str);
		 if(str.Right(4).CompareNoCase(_T(".htm")) == 0)
		 {
			 FixHTMLNow(str);
		 }
		 
		 continue;
	  }

	  // if it's a directory, recursively search it
      if (finder.IsDirectory())
      {
         CString str = finder.GetFileName();
		 Recurse(finder.GetFilePath());
      }
	  else
	  {
		 CString str = finder.GetFilePath();
		 ShowWorking(str);
		 if(str.Right(4).CompareNoCase(_T(".htm")) == 0)
		 {
			 FixHTMLNow(str);
		 }
		 
		 continue;
	  }
   }
   finder.Close();
}

void CFixHTMLDlg::ShowWorking(CString strHTMLFileName)
{
	CFixHTMLApp* pApp = (CFixHTMLApp*)AfxGetApp();
	pApp->m_strFileName = strHTMLFileName;
}
void CFixHTMLDlg::OnTimer(UINT_PTR nIDEvent)
{
	m_strEdit = pApp->m_strFileName;
	UpdateData(0);

	if(!pApp->m_bRunning)
	{
		KillTimer(111);
		GetDlgItem(IDC_BUTTON_START)->EnableWindow(true);
	}
	CDialog::OnTimer(nIDEvent);
}

LRESULT CFixHTMLDlg::OnMessage_FixIt(WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);
	CString* pstr = reinterpret_cast<CString*>(lParam);
	CString str(*pstr);
	str.TrimRight();
	
	int nIndex = m_List.GetItemCount();
	m_List.InsertItem( nIndex, str);
	
	m_List.SetItemText(nIndex, 1, _T(""));
	m_List.EnsureVisible(nIndex, TRUE);

	m_List.SetItemState(0, 0, LVIS_SELECTED|LVIS_FOCUSED);
	delete pstr;
	return 0;

}


LRESULT CFixHTMLDlg::OnMessage_FileDirty(WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);
	DirtyMsg *pStruct = reinterpret_cast<DirtyMsg*>(lParam);

	//select the currently running script in the list box
	LVFINDINFO FindInfo;
	FindInfo.flags = LVFI_PARTIAL|LVFI_STRING;
	FindInfo.psz = pStruct->strHTMLFileName;
	int nIndex = m_List.FindItem(&FindInfo);
	

	m_List.SetItemText(nIndex, 1, m_List.GetItemText(nIndex, 1) + _T(" ") + pStruct->strStringFound);

	
	delete pStruct;
	return 0;

}
/*
void CFixHTMLDlg::OpenInFile()
{

}
void CFixHTMLDlg::CreateOutFile()
{

}*/
void CFixHTMLDlg::FixHTMLNow(CString strHTMLFileName)
{
	CFixHTMLApp* pApp = (CFixHTMLApp*)AfxGetApp();
	CString strNameOnly = strHTMLFileName.Right(strHTMLFileName.GetLength() - strHTMLFileName.ReverseFind('\\') - 1);

	//if file name starts with "Org_" then just return
	if(strNameOnly.Left(5)==FILE_TAG_ORIGINAL)
	{
		return;
	}

	//
	if(! pApp->LoadConfig(strHTMLFileName))
	{

		return;
	}

	//otherwise create a new html file	
	CString strHTMLFileNameNew(strHTMLFileName+_T(".html"));

	CString* p = new CString(strHTMLFileName);
	::PostMessage(pApp->m_pMainWnd->m_hWnd,WM_FIXING, 0, reinterpret_cast<LPARAM>(p));
	Sleep(500);
	

	try{
		pApp->m_fileIn.Open( strHTMLFileName, CFile::modeRead | CFile::typeText );
	}
	catch( CFileException* e )
	{
		TCHAR   szCause[300];
		e->GetErrorMessage(szCause, 255);
		CString str;
		str.Format(_T("Exception for %s : cause=%s"), strHTMLFileName, szCause);
    
		Log(str);
	    e->Delete();
		return;
	}

	try{

		pApp->m_fileOut.Open( strHTMLFileNameNew, CFile::modeCreate | CFile::modeWrite | CFile::typeText | CStdioFileEx::modeWriteUnicode );


	}
	catch( CFileException* e )
	{
		TCHAR   szCause[300];
		e->GetErrorMessage(szCause, 255);
		CString str;
		str.Format(_T("Exception for %s : cause=%s"), strHTMLFileName+_T(".html"), szCause);

	    Log(str);
	    
		e->Delete();
		return;
	}

	CString str;
	bool bDirty(false);

	while( 	pApp->m_fileIn.ReadString(str) )
	{
		//replace this string with Viking font
		if(str.Find(_T("table border=")) != -1)
		{
			pApp->m_fileOut.WriteString(_T("\r\n<p style=\"font-family: Viking\">"));
		}

		////Remove extra data not required for image comparison
		if(str.Find(_T("Testcase ID ")) != -1)
		{
			continue;
		}
		else if(str.Find(_T("Requirement ")) != -1)
		{
			continue;
		}
		else if(str.Find(_T("Objective ")) != -1)
		{
			continue;
		}
		else if(str.Find(_T("Computed ")) != -1)
		{
			continue;
		}
		else if(str.Find(_T("==== TOTAL TEST CASES")) != -1)
		{
			continue;
		}
		else if(str.Find(_T("Warning")) != -1)
		{
			continue;
		}
		else if(str.Find(_T("WARNINGS ")) != -1)
		{
			continue;
		}
		/////////////////////////////////////////
		bool bFoundIt(false);

		//loop for the enums
		POSITION pos = pApp->m_listReplaceString.GetHeadPosition();
		for (int i=0;i < pApp->m_listReplaceString.GetCount();i++)
		{
  
			structData data = pApp->m_listReplaceString.GetNext(pos);

			//if(str.Replace(data.m_strToFind, data.data.m_strToFind)!= 0)
			int nIndexFound = str.Find(data.m_strToFind);
			if(nIndexFound!= -1)
			{
				bDirty = true;
				CString strNewLine;
				strNewLine.Format(_T("<font size=\"2\" color=\"##000000\"> %s - %s</font><br />"), 
					data.m_strToFind, 
					data.m_strReplaceWith); 

				pApp->m_fileOut.WriteString(_T("\r\n")+strNewLine);//new language string
				bFoundIt = true;

				DirtyMsg *pStruct = new DirtyMsg;
				pStruct->strHTMLFileName = strHTMLFileName;
				pStruct->strStringFound = data.m_strToFind;

				//to show status
				::PostMessage(pApp->m_pMainWnd->m_hWnd,WM_FILEDIRTY, 0, reinterpret_cast<LPARAM>(pStruct));
				break;

			}
			
		}
		if(!bFoundIt) pApp->m_fileOut.WriteString(_T("\r\n")+str);//original string if not found
		
	}

	{
		////////Insert signature block/////////////////////////////////////////////////////////////////
		CString strBlankLine(_T("<font size=\"2\" color=\"##000000\"></font><br /> "));
		CString strFormat;
		str = _T("<u>Comments and Test Signatures</u>"); 		strFormat.Format(_T("<font size=\"2\" color=\"##000000\"> %s </font><br />"), str); 		pApp->m_fileOut.WriteString(_T("\r\n")+strFormat);		pApp->m_fileOut.WriteString(_T("\r\n")+strBlankLine);		str = _T("Comments:_____________________________________________________________"); 		strFormat.Format(_T("<font size=\"2\" color=\"##000000\"> %s </font><br />"), str); 		pApp->m_fileOut.WriteString(_T("\r\n")+strFormat);		pApp->m_fileOut.WriteString(_T("\r\n")+strBlankLine);		str = _T("______________________________________________________________________"); 		strFormat.Format(_T("<font size=\"2\" color=\"##000000\"> %s </font><br />"), str); 		pApp->m_fileOut.WriteString(_T("\r\n")+strFormat);		pApp->m_fileOut.WriteString(_T("\r\n")+strBlankLine);		str = _T("______________________________________________________________________"); 		strFormat.Format(_T("<font size=\"2\" color=\"##000000\"> %s </font><br />"), str); 		pApp->m_fileOut.WriteString(_T("\r\n")+strFormat);		pApp->m_fileOut.WriteString(_T("\r\n")+strBlankLine);		str = _T("Test Operator Signature/Date:__________________________________________"); 		strFormat.Format(_T("<font size=\"2\" color=\"##000000\"> %s </font><br />"), str); 		pApp->m_fileOut.WriteString(_T("\r\n")+strFormat);		pApp->m_fileOut.WriteString(_T("\r\n")+strBlankLine);		str = _T("Review Engineer Signature/Date:________________________________________"); 		strFormat.Format(_T("<font size=\"2\" color=\"##000000\"> %s </font><br />"), str); 		pApp->m_fileOut.WriteString(_T("\r\n")+strFormat);	}/*<font size="2" color="##000000"><u>Comments and Test Signatures</u></font><br /> <font size="2" color="##000000"></font><br /> <font size="2" color="##000000">Comments:_____________________________________________________________</font><br /> <font size="2" color="##000000"></font><br /> <font size="2" color="##000000">______________________________________________________________________</font><br /> <font size="2" color="##000000"></font><br /> <font size="2" color="##000000">______________________________________________________________________</font><br /> <font size="2" color="##000000"></font><br /> <font size="2" color="##000000"></font><br /> <font size="2" color="##000000">Test Operator Signature/Date:__________________________________________</font><br /> <font size="2" color="##000000"></font><br /> <font size="2" color="##000000"></font><br /> <font size="2" color="##000000">Review Engineer Signature/Date:_________________________________________</font><br /> 



	*///////////////////////////////////////////////////////////////////////////

	pApp->m_fileIn.Close();
	pApp->m_fileOut.Close();

	if(! bDirty)
	{//delete new out file since nothing changed in it
		DeleteFile(pApp->m_fileOut.GetFileName());

	}


	



}

void CFixHTMLDlg::OnBnClickedButtonLoad()
{
	//load this struct
	
	//CStdioFile file( strHTMLFileName, CFile::modeRead | CFile::typeText );



}
CString CFixHTMLDlg::ReadRegistrySetting(CString strParam)
{
	DWORD dwType;
	TCHAR  szVersion[500];
	DWORD dwDataSize=500;
	memset(szVersion,0,500);
	HKEY hKey;
	long lResult = RegOpenKeyEx(HKEY_CURRENT_USER, SWAT_REG_KEY, 0, KEY_READ, &hKey);
	if(ERROR_SUCCESS == lResult)
	{
	   lResult = RegQueryValueEx(hKey,strParam, NULL, &dwType, (BYTE*)szVersion, &dwDataSize);
	   if(ERROR_SUCCESS == lResult)
	   {
		   RegCloseKey(hKey);
		   CString str(szVersion);
		   return str + (_T("\\SCRSHOTS"));
	   }
	   RegCloseKey(hKey);
	}
	else
	{
		
	}

	return _T(" ");
}

/*
bool CFixHTMLDlg::LoadConfig(CString strHTMLPathName)
{

	//CString str;
	//GetDlgItem(IDC_EDIT_ROOT_PATH)->GetWindowText(str);



	int nIndex = strHTMLPathName.ReverseFind('\\');
	CString strLanguage;
	strLanguage = strHTMLPathName.Right(strHTMLPathName.GetLength()-nIndex-1);

	CString strFileName;
	strFileName.Format(_T("%s\\_%s.enum"),strHTMLPathName, strLanguage);






	structData data;

	try{
	
		CStdioFile file; 
		if(!file.Open( strFileName , CFile::modeRead | CFile::typeText ))
		{
			Log(_T("Unable to open enums file : ")+strFileName);
			return false;
		}

		Log(_T("Loaded enums file : ")+strFileName);
			

		CString strRead;
		while(file.ReadString(strRead))
		{
			int nIndex = strRead.Find(_T("--||--")); 
			if(nIndex == -1)
			{
				strRead.Trim();
				if(!strRead.IsEmpty())
				{
					Log(_T("Following string didn't match guidance :"));
					Log(strRead);
				}
			}
			else
			{
				data.m_strToFind = strRead.Left(nIndex);
				data.m_strReplaceWith= strRead.Right(strRead.GetLength()-nIndex-6);
				pApp->m_listReplaceString.AddTail(data);
			}

		}
	}



	catch( CFileException* e )
	{
		TCHAR   szCause[300];
		e->GetErrorMessage(szCause, 255);
		CString str;
		str.Format(_T("Exception for %s : cause=%s"), strFileName, szCause);

	    Log(str);
	    
		e->Delete();

		return false;
	}



	return true;


	
}*/

void CFixHTMLDlg::Log(CString str)
{
	CFixHTMLApp* pApp = (CFixHTMLApp*)AfxGetApp();
	CString* p = new CString(str);
	::PostMessage(pApp->m_pMainWnd->m_hWnd,WM_LOG, 0, reinterpret_cast<LPARAM>(p));
}


LRESULT CFixHTMLDlg::OnMessage_Log(WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);
	CString* pstr = reinterpret_cast<CString*>(lParam);
	CString str(*pstr);
	str.TrimRight();
	
	if(m_strLog.IsEmpty())
		m_strLog = str;
	else
		m_strLog = m_strLog+_T("\r\n")+str;

	UpdateData(0);

	delete pstr;
	return 0;
}
void CFixHTMLDlg::OnBnClickedButtonBrowseRootPath()
{
	CString str;
	GetDlgItem(IDC_EDIT_ROOT_PATH)->GetWindowText(str);

	CFileDialog dlg(TRUE, NULL,NULL,0, _T("enum file (enum)|*.enum|"), NULL);

	dlg.m_ofn.lpstrInitialDir  = str;



	if(dlg.DoModal()==IDCANCEL)		
		return;

	if(dlg.GetFileName().Find(_T("enum")) == -1)
	{
		AfxMessageBox(_T("Need enum file to continue."));
		GetDlgItem(IDC_EDIT_ROOT_PATH)->SetWindowText(_T(""));
		pApp->m_strRootPath = _T("");
		return;
	}
		
	GetDlgItem(IDC_EDIT_ROOT_PATH)->SetWindowText(dlg.GetPathName());


	/*Folder Browser code.
    BROWSEINFO bi;
    ZeroMemory(&bi, sizeof(BROWSEINFO));

    bi.hwndOwner = m_hWnd;
    bi.ulFlags   = BIF_RETURNONLYFSDIRS;

    LPITEMIDLIST pidl = SHBrowseForFolder(&bi);

    BOOL bRet = FALSE;

    TCHAR szFolder[MAX_PATH*2];
    szFolder[0] = _T('\0');

    if (pidl)
    {
        if (SHGetPathFromIDList(pidl, szFolder))
        {
            bRet = TRUE;
        }
        IMalloc *pMalloc = NULL; 
        if (SUCCEEDED(SHGetMalloc(&pMalloc)) && pMalloc) 
        {  
            pMalloc->Free(pidl);  
            pMalloc->Release(); 
        }
    }
    //Returned value is in szFolder.
	GetDlgItem(IDC_EDIT_ROOT_PATH)->SetWindowText(szFolder);
*/



}

void CFixHTMLDlg::OnBnClickedButtonStop()
{
	pApp->m_bRun = 0;
}

void CFixHTMLDlg::OnNMDblclkList2(NMHDR *pNMHDR, LRESULT *pResult)
{
//	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<NMITEMACTIVATE>(pNMHDR);
	LPNMITEMACTIVATE lpnmitem = (LPNMITEMACTIVATE) pNMHDR;

	//return if no results for this test case yet
//	if(m_List.GetItemText(lpnmitem->iItem,1).IsEmpty())return;


	CString str = m_List.GetItemText(lpnmitem->iItem,0);
	
	ShellExecute(0, _T("open"), _T("iexplore.exe"), str+_T(".html"), 0, SW_SHOW);
	
	*pResult = 0;
}
