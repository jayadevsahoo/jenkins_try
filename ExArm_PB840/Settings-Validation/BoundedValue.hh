
#ifndef BoundedValue_HH
#define BoundedValue_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  BoundedValue - Type Definition of 'BoundedValue'.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BoundedValue.hhv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//   
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version 
//  
//====================================================================

#include "Sigma.hh"
#include "Precision.hh"


//@ Type:  BoundedValue
// This is a structure to contain all bounded values.
struct BoundedValue
{
    typedef enum ClippingState_enum
    {
	    UNCLIPPED,
	    ABSOLUTE_CLIP,
    	VARIABLE_CLIP
    } ClippingState;

  inline  operator Real32(void) const { return(value); };

  Real32     value;
  Precision  precision;
};


//====================================================================
//
//  Global Functions...
//
//====================================================================

//@ Begin-Free-Declarations
inline Boolean  operator==(const BoundedValue& boundedValue1,
			   const BoundedValue& boundedValue2);
inline Boolean  operator!=(const BoundedValue& boundedValue1,
			   const BoundedValue& boundedValue2);

inline Boolean  operator<(const BoundedValue& boundedValue1,
			  const BoundedValue& boundedValue2);
inline Boolean  operator>(const BoundedValue& boundedValue1,
			  const BoundedValue& boundedValue2);
//@ End-Free-Declarations

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
extern Ostream&  operator<<(Ostream& ostr, const BoundedValue& boundedValue);
#endif  // defined(SIGMA_DEVELOPMENT)


// Inlined Functions...
#include "BoundedValue.in"


#endif // BoundedValue_HH
 
