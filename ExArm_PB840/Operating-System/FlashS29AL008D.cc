#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
 
//============================== C L A S S   D E S C R I P T I O N ====
//@ Filename: FlashS29AL008D.cc - FLASH Memory Programmer for (i.e) Spansion
//---------------------------------------------------------------------
//@ Interface-Description
//  The FlashS29AL008D contains free functions to program the Flash memory
//  device.  The FlashS29AL008D_EraseBlock function erases a block of flash memory.  
//  The FlashWriteWord function writes a word to flash memory.  The Service-Mode
//  subsystem uses these functions to program the calibration tables
//  as well as the Serial Number and software configuration item to
//  flash memory.
//---------------------------------------------------------------------
//@ Rationale
//  The FlashS29AL008D class contains free functions required to program flash
//  during normal application execution.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The FlashEraseBlock function erases a flash memory block.  The 
//  FlashWriteWord function writes a single 4-byte word to a flash
//  memory location.
//
//  THIS CODE MUST EXECUTE IN DRAM/NOVRAM AND NOT IN FLASH MEMORY.
//  IN ADDITION, IT MUST NOT REFERENCE ANYTHING IN FLASH INCLUDING 
//  CONSTANTS OR SYSTEM SERVICES.  
//
//  The MRI compiler pragma option -NTvars generates this code in the
//  vars section instead of the default code section so the linker
//  locates the code in the vars section which is located in DRAM.
//
//---------------------------------------------------------------------
//@ Fault-Handling
//  Not applicable
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/FlashS29AL008D.ccv   25.0.4.0   19 Nov 2013 14:16:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision 001  By: Rhomere Jimenez  Date: 07-March-2006  DR Number: 6192
//   Project:   Xena
//   Description:
//      Initial version.
//=====================================================================

#include "FlashS29AL008D.hh"
#include "Watchdog.hh"    //  for STROBE_WATCHDOG
#include <cpu.h>
#include <vrtxil.h>

//  The -NTvars option directs compiled code to the "vars" section
//  instead of the "code" section.  This is necessary since flash memory
//  cannot be programmed (successfully) while executing code in flash.

#pragma option -NTvars 

//  The following must not compile or link into FLASH.  

// List of commands to the Flash memory device
static const Uint32 FLASH_S29AL008D_READ_ARRAY               = 0xf0f0f0f0;
static const Uint32 FLASH_S29AL008D_IDENTIFY_DEVICE          = 0x90909090;

static const Uint32 FLASH_S29AL008D_FIRST_DEVICE_ADDR1       = 0x555;
static const Uint32 FLASH_S29AL008D_FIRST_DEVICE_ADDR2       = 0x2aa;
static const Uint32 FLASH_S29AL008D_FIRST_DEVICE_ADDR3       = 0x555;

static const Uint32 FLASH_S29AL008D_FIRST_DEVICE_DATA1       = 0xaaaaaaaa;
static const Uint32 FLASH_S29AL008D_FIRST_DEVICE_DATA2       = 0x55555555;
static const Uint32 FLASH_S29AL008D_WRITE_ENABLE             = 0xffbe8000;
static const Uint32 FLASH_S29AL008D_ERASE_SETUP              = 0x80808080;
static const Uint32 FLASH_S29AL008D_SECTOR_ERASE             = 0x30303030;
static const Uint32 FLASH_S29AL008D_ERASE_CONFIRM            = 0xffffffff;
static const Uint32 FLASH_S29AL008D_WRITE_SETUP              = 0xa0a0a0a0;
static const Uint32 FLASH_S29AL008D_CHIP_ERASE               = 0x10101010;


static const Uint32 FLASH_S29AL008D_NUM_INTERLEAVES          = 2;
static const Uint32   FLASH_S29AL008D_NUM_SUB_BLOCKS = 19;  
static const Uint32   FLASH_S29AL008D_TOTAL_SUB_BLOCKS = 
                          FLASH_S29AL008D_NUM_INTERLEAVES * FLASH_S29AL008D_NUM_SUB_BLOCKS;

static const Uint32   FLASH_S29AL008D_SUB_BLOCK1_SIZE  = 0x4000;     // 16k word.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK2_SIZE  = 0x2000;     // 8k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK3_SIZE  = 0x2000;     // 8k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK4_SIZE  = 0x8000;     // 32k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK5_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK6_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK7_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK8_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK9_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK10_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK11_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK12_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK13_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK14_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK15_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK16_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK17_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK18_SIZE  = 0x10000;    // 64k words.
static const Uint32   FLASH_S29AL008D_SUB_BLOCK19_SIZE  = 0x10000;    // 64k words.

// Stores all of the block sizes of the flash chip.
static Int FLASH_S29AL008D_ALL_SUB_BLOCK_SIZES[FLASH_S29AL008D_NUM_SUB_BLOCKS] = 
{
    FLASH_S29AL008D_SUB_BLOCK1_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK2_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK3_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK4_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK5_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK6_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK7_SIZE , 
    FLASH_S29AL008D_SUB_BLOCK8_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK9_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK10_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK11_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK12_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK13_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK14_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK15_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK16_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK17_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK18_SIZE ,
    FLASH_S29AL008D_SUB_BLOCK19_SIZE 

};

// Stores all of the block address
static const Uint32 FSTART = 0xff600000 ;
static Uint32 * FLASH_S29AL008D_BLOCK_ADDRESS[FLASH_S29AL008D_TOTAL_SUB_BLOCKS + 1] = {
(Uint32 *)(FSTART),
(Uint32 *)(FSTART+4*1024*(16)),                                                                                                          //  16 kByte
(Uint32 *)(FSTART+4*1024*(16+8)),                                                                                                        //   8 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8)),                                                                                                      //   8 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32)),                                                                                                   //  32 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64)),                                                                                                //  64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64)),                                                                                             //  64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64)),                                                                                          //  64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16)),                                                                                       //  64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64)),                                                                                    //  64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64)),                                                                                 //  64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64)),                                                                              //  64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64)),                                                                           // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64)),                                                                        // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64)),                                                                     // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64)),                                                                  // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64)),                                                               // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64)),                                                            // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64)),                                                         // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64)),                                                      // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16)),                                                   // 16 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8)),                                                 // 8 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8)),                                               // 8 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32)),                                            // 32 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64)),                                         // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64)),                                      // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64)),                                   // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64)),                                // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64)),                             // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64)),                          // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64+64)),                       // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64+64+64)),                    // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64+64+64+64)),                 // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64+64+64+64+64)),              // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64+64+64+64+64+64)),           // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64+64+64+64+64+64+64)),        // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64+64+64+64+64+64+64+64)),     // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64+64+64+64+64+64+64+64+64)),  // 64 kByte
(Uint32 *)(FSTART+4*1024*(16+8+8+32+64+64+64+16+64+64+64+64+64+64+64+64+64+64+64+16+8+8+32+64+64+64+64+64+64+64+64+64+64+64+64+64+64+64))// 64 kByte
} ;

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  FlashS29AL008D_EraseBlock - Erase FLASH memory block
//
//@ Interface-Description
//  Accepts the starting address of the flash memory block.  Initiates
//  a flash memory erase operation at that address.  Uses the
//  manufacturer's code from the flash device to determine the correct
//  erase command sequence.  Returns the completion status of the erase
//  operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Disables interrupts prior to the erase operation and enables them
//  after the operation completes.  This function continues to strobe 
//  the watchdog timer for the duration of the erase operation to 
//  avoid a reset during the operation. 
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
 
Int
FlashS29AL008D_EraseBlock( volatile Uint32 * address )
{
    
    int ret = 0;
    Uint32 status = 0;

    cpu_interrupt_disable();
    STROBE_WATCHDOG;        //retrigger the watchdog

    *address = FLASH_S29AL008D_READ_ARRAY;
    Uint32 * tempAddress = (Uint32*) address;
    for ( Uint interleave=0; 
          interleave<FLASH_S29AL008D_NUM_INTERLEAVES && ret == 0; 
          interleave++, address++ )
    {
        // first cycle
         tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR1);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA1;        // command mode

        // second cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR2);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA2;        // command mode

        // third cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR3);
        *tempAddress = FLASH_S29AL008D_ERASE_SETUP;               // read id command

        // forth cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR1);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA1;        // command mode

        // fifth cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR2);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA2;        // command mode

        // sixth cycle
        *address = FLASH_S29AL008D_SECTOR_ERASE;                  // sector address
        STROBE_WATCHDOG;        //retrigger the watchdog

        for (Uint32 waitLoop = 0; waitLoop < 0xfffffffe ; waitLoop ++ )
        {
            STROBE_WATCHDOG;        //retrigger the watchdog
            if ( *address == FLASH_S29AL008D_ERASE_CONFIRM )
            {
                status = 1;
                break;
            }
        }
        if(!status)
        {
            ret = 1;
            break;
        }
        STROBE_WATCHDOG;        //retrigger the watchdog
        
        *address = FLASH_S29AL008D_READ_ARRAY;
        asm(" nop ");
    }
    STROBE_WATCHDOG;        //retrigger the watchdog

    cpu_interrupt_enable();
    return ret;
    
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  FlashS29AL008D_WriteWord - Writes a 4-byte word to 
//                                             flash memory
//
//@ Interface-Description
//  Accepts the address of the flash memory word and the 4-byte data
//  to be programmed.  Initiates a flash memory write operation at 
//  the specified address.  Uses the manufacturer's code from the 
//  flash device to determine the correct write word command sequence.
//  Returns the completion status of the write operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Disables interrupts prior to the write operation and enables them
//  after the operation completes.  Continues to strobe the watchdog
//  timer for the duration of the operation.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
 
Int
FlashS29AL008D_WriteWord(volatile Uint32 * address, const Uint32 data)
{
    
    int ret = 0;
    Uint32 status = 0;
    Uint32 * actualAddress;
    cpu_interrupt_disable();

    STROBE_WATCHDOG;        //retrigger the watchdog

    Uint32 * tempAddress = (Uint32*) ( (Uint32) address & 0xffe00004);
    actualAddress = (Uint32*) FlashS29AL008D_ConvertFlashAddress(
                                       (Uint32) tempAddress, 0);
    *actualAddress = FLASH_S29AL008D_READ_ARRAY;

    // first cycle
    actualAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                       (Uint32)tempAddress, 
                                       FLASH_S29AL008D_FIRST_DEVICE_ADDR1);
    *actualAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA1;          // command mode

    // second cycle
    actualAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                       (Uint32)tempAddress, 
                                       FLASH_S29AL008D_FIRST_DEVICE_ADDR2);
    *actualAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA2;          // command mode

    // third cycle
    actualAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                       (Uint32)tempAddress, 
                                       FLASH_S29AL008D_FIRST_DEVICE_ADDR3);
    *actualAddress = FLASH_S29AL008D_WRITE_SETUP;                 // read id command

    // forth cycle
    *address = data;                  // sector address

    STROBE_WATCHDOG;        //retrigger the watchdog

    for (Uint32 waitLoop = 0 ; waitLoop < 0xfffffffe ; waitLoop ++ )
    {
        STROBE_WATCHDOG;        //retrigger the watchdog
        if ( *address  == data )
        {
            status = 1;
            break;
        }
    }
    if(!status)
    {
        ret = 1;
    }

    actualAddress = (Uint32*) FlashS29AL008D_ConvertFlashAddress( (Uint32) tempAddress, 0);
    *actualAddress = FLASH_S29AL008D_READ_ARRAY;

    asm(" nop ");


    cpu_interrupt_enable();
    STROBE_WATCHDOG;        //retrigger the watchdog
 
    return ret;
    
    
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  FlashS29AL008D_GetDeviceId - Queries the device id from 
//                                          FLASH
//@ Interface-Description
//  Accepts the address of the flash memory block.  Initiates a flash 
//  memory read array operation at this address.  Then it is set to 
//  a Intelligent Identifier command mode which enables the ability to
//  read the device Id at that address.  After reading the device Id,
//  it is set back to the read array operation mode in order to get out 
//  of Intelligent Identifier command mode.  This function returns
//  the device id in 32 bits.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================
 
Int
FlashS29AL008D_GetDeviceId( volatile Uint32 * address )
{
    
    Uint32 deviceId = 0;
    STROBE_WATCHDOG;        //retrigger the watchdog

    *address = FLASH_S29AL008D_READ_ARRAY;
    Uint32 * tempAddress = (Uint32 *) address;
    tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                         (Uint32) address, 
                                         FLASH_S29AL008D_FIRST_DEVICE_ADDR1);
    *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA1;        // command mode
    tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                         (Uint32)address, 
                                         FLASH_S29AL008D_FIRST_DEVICE_ADDR2);
    *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA2;        // command mode
    tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                         (Uint32)address, 
                                         FLASH_S29AL008D_FIRST_DEVICE_ADDR3);
    *tempAddress = FLASH_S29AL008D_IDENTIFY_DEVICE;           // read id command

    // wait for FPGA to be ready
    for (Uint8 delay = 0 ; delay < 10 ; delay++)
    {
        STROBE_WATCHDOG; //retrigger the watchdog
    }

    deviceId = *address;       
    *tempAddress = FLASH_S29AL008D_READ_ARRAY;        // command mode
    asm(" nop ");
    STROBE_WATCHDOG;        //retrigger the watchdog

    return deviceId;
      
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  FlashS29AL008D_ConvertFlashAddress - Converts the logical
//                              base address with an offset address to 
//                              a command flash address.
//
//@ Interface-Description
//  This method accepts a logical base address and an offset address 
//  and converts it to a command flash address. This function returns
//  the command flash address in 32 bits. 
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  The logical base address must be within the valid range of Flash.  Also,
//  the offset address must be a valid address.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================
Uint32  FlashS29AL008D_ConvertFlashAddress(Uint32 logicalAddress, Uint32 offsetAddress)
{
    Uint32  commandAddress; // Command Flash Address
    Uint32  tempAddress;    // Used for calculations
    STROBE_WATCHDOG;        //retrigger the watchdog

    commandAddress =  (logicalAddress & 0xffe00000);
    tempAddress =  offsetAddress & 0x3fffe;
    tempAddress =  tempAddress << 3;
    commandAddress |=  tempAddress;

    tempAddress =  offsetAddress & 0x1;

    if ( !(logicalAddress & 0x4) )
    {
        tempAddress ^=  ((logicalAddress >> 2) & 0x1);
    }
    tempAddress =  tempAddress << 3;
    commandAddress |=  tempAddress;

    tempAddress =  logicalAddress & 0x4;

    commandAddress |= tempAddress;
    STROBE_WATCHDOG;        //retrigger the watchdog

    return(commandAddress);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  FlashS29AL008D_GetFlashBlockAddress - returns the block   
//                                               address based on the 
//                                               specified index.
//
//@ Interface-Description
//  This method accepts an index to an array of block addresses and returns 
/// the block address based on the given index.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It returns the static variable FLASH_S29AL008D_BLOCK_ADDRESS[index].
//---------------------------------------------------------------------
//@ PreCondition
//  The index argument must be within the range from zero to 
//  FLASH_S29AL008D_TOTAL_SUB_BLOCKS.  
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================

Uint32 * FlashS29AL008D_GetFlashBlockAddress(Uint8 index)
{
    
    return FLASH_S29AL008D_BLOCK_ADDRESS[index];       
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  FlashS29AL008D_GetFlashBlockSize - returns the block size  
//                                              based on the specified
//                                              index.
//
//@ Interface-Description
//  This method accepts an index to the array of block sizes and returns 
/// the block size based on the given index.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It returns the static variable FLASH_S29AL008D_ALL_SUB_BLOCK_SIZES[index].
//---------------------------------------------------------------------
//@ PreCondition
//  The index argument must be within the range from zero to 
//  FLASH_S29AL008D_NUM_SUB_BLOCKS. 
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================

Int FlashS29AL008D_GetFlashBlockSize(Uint8 index)
{
    return FLASH_S29AL008D_ALL_SUB_BLOCK_SIZES[index];       
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  FlashS29AL008D_GetNumSubBlocks - returns the number of 
//                                              sub-blocks per chip
//
//@ Interface-Description
//  This method has no arguments and returns the number of 
//  sub-blocks of the flash chip.
//---------------------------------------------------------------------
//@ Implementation-Description
/// Returns the static variable FLASH_S29AL008D_NUM_SUB_BLOCKS.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================

Uint32  FlashS29AL008D_GetNumSubBlocks(void)
{    
    return FLASH_S29AL008D_NUM_SUB_BLOCKS;       
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  FlashS29AL008D_GetTotalSubBlocks - returns the total 
//                                                number of sub-blocks
//
//@ Interface-Description
//  This method has no arguments and returns the total number of sub-blocks
//  of the flash chip.
//---------------------------------------------------------------------
//@ Implementation-Description
/// Returns the static variable FLASH_S29AL008D_TOTAL_SUB_BLOCKS.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================

Uint32  FlashS29AL008D_GetTotalSubBlocks(void)
{    
    return FLASH_S29AL008D_TOTAL_SUB_BLOCKS;       
}
      


#if defined( SIGMA_COMMON ) || defined( SIGMA_UNIT_TEST )
//=====================================================================
//
//  FlashS29AL008D_EraseBlockNonBlocking(),  FlashCheck() and 
//  FlashS29AL008D_FullChipErase(), are used by the Download subsystem 
//  executing from PROM.  These functions must not be called from FLASH 
//  so they are defined only for the COMMON build.
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  FlashS29AL008D_EraseBlockNonBlocking - Erase FLASH 
//                             memory block without disabling interrupts
//
//@ Interface-Description
//  Accepts the starting address of the flash memory block.  Initiates
//  a flash memory erase operation at that address.  Returns the status
//  of the erase operation.  During the erase operation, the caller can
//  proceed with other non-flash operations.  It can check on the 
//  status of the erase operation by using the FlashS29AL008D_Check() method.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================

Int
FlashS29AL008D_EraseBlockNonBlocking( volatile Uint32 * address )
{
    Int ret = 0;
    Uint32 * tempAddress;
    STROBE_WATCHDOG;        //retrigger the watchdog
    *address = FLASH_S29AL008D_READ_ARRAY;

    for ( Uint interleave=0; 
          interleave<FLASH_S29AL008D_NUM_INTERLEAVES && ret == 0; 
          interleave++, address++ )
    {
        // first cycle
         tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR1);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA1;        // command mode

        // second cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR2);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA2;        // command mode

        // third cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR3);
        *tempAddress = FLASH_S29AL008D_ERASE_SETUP;           // read id command
        STROBE_WATCHDOG;        //retrigger the watchdog

        // forth cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress( 
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR1);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA1;        // command mode

        // fifth cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                      (Uint32) address, 
                                      FLASH_S29AL008D_FIRST_DEVICE_ADDR2);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA2;        // command mode

        // sixth cycle
        *address = FLASH_S29AL008D_SECTOR_ERASE;                  // sector address

    }
    STROBE_WATCHDOG;        //retrigger the watchdog

    return FlashS29AL008D_Check(address);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  FlashS29AL008D_Check - Check Status of active flash 
//                                         operation
//
//@ Interface-Description
//  Accepts the starting address of the flash memory block.  Returns 
//  the status of the current flash operation to the caller.  Returns
//  zero if the operation is complete with no errors.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================

 Int
FlashS29AL008D_Check( volatile Uint32 * address )
{
    Int ret = 0;
    Uint32 status = 0;
    STROBE_WATCHDOG;        //retrigger the watchdog

    Uint32 temp = 0;
    for ( Uint interleave=0; 
          interleave<FLASH_S29AL008D_NUM_INTERLEAVES && ret == 0; 
          interleave++, address++ )
    {
        STROBE_WATCHDOG;        //retrigger the watchdog

        for (Uint32 waitLoop = 0 ; waitLoop < 0xfffffffe ; waitLoop ++ )
        {
            STROBE_WATCHDOG;        //retrigger the watchdog
            temp = *address;
            if ( temp == FLASH_S29AL008D_ERASE_CONFIRM )
            {
                status = 1;
                break;
            }
        }
        if(!status)
        {
            ret = 1;
            break;
        }

        asm(" nop ");

    }

    return ret;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  FlashS29AL008D_FullChipErase - Erase the entire
//                                                 FLASH memory.
//
//@ Interface-Description
//  Uses the manufacturer's code from the flash device to determine 
//  the correct erase command sequence.  Returns the completion 
//  status of the erase operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function continues to strobe the watchdog timer for the 
//  duration of the full erase operation to avoid a reset during 
//  the operation. 
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
 
Int
FlashS29AL008D_FullChipErase()
{
    
    int ret = 0;
    Uint32 status = 0;

    STROBE_WATCHDOG;        //retrigger the watchdog
    Uint32 * address = (Uint32 *) FLASH_BASE;
    Uint32 * tempAddress;
    * address = FLASH_S29AL008D_READ_ARRAY;
    for ( Uint interleave=0; 
          interleave<FLASH_S29AL008D_NUM_INTERLEAVES && ret == 0; 
          interleave++, address++ )
    {
        // first cycle
         tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                        (Uint32) address, 
                                        FLASH_S29AL008D_FIRST_DEVICE_ADDR1);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA1;        // command mode

        // second cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                        (Uint32) address, 
                                        FLASH_S29AL008D_FIRST_DEVICE_ADDR2);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA2;        // command mode

        // third cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                        (Uint32) address, 
                                        FLASH_S29AL008D_FIRST_DEVICE_ADDR3);
        *tempAddress = FLASH_S29AL008D_ERASE_SETUP;               // read id command

        // forth cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                        (Uint32) address, 
                                        FLASH_S29AL008D_FIRST_DEVICE_ADDR1);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA1;        // command mode

        // fifth cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                        (Uint32) address, 
                                        FLASH_S29AL008D_FIRST_DEVICE_ADDR2);
        *tempAddress = FLASH_S29AL008D_FIRST_DEVICE_DATA2;        // command mode

        // sixth cycle
        tempAddress = (Uint32 *) FlashS29AL008D_ConvertFlashAddress(
                                        (Uint32) address, 
                                        FLASH_S29AL008D_FIRST_DEVICE_ADDR1);
        *tempAddress = FLASH_S29AL008D_CHIP_ERASE;               // command mode

        STROBE_WATCHDOG;        //retrigger the watchdog

        for (Uint32 waitLoop = 0; waitLoop < 0xfffffffe ; waitLoop ++ )
        {
            STROBE_WATCHDOG;        //retrigger the watchdog
            if ( *address == FLASH_S29AL008D_ERASE_CONFIRM )
            {
                status = 1;
                break;
            }
        }
        if(!status)
        {
            ret = 1;
            break;
        }
        STROBE_WATCHDOG;        //retrigger the watchdog
        
        *address = FLASH_S29AL008D_READ_ARRAY;
        asm(" nop ");
    }
    STROBE_WATCHDOG;        //retrigger the watchdog

    return ret;
    
}


#endif // SIGMA_COMMON || SIGMA_UNIT_TEST
