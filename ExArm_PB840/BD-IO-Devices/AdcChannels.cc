#include "stdafx.h"
//#ifdef SIGMA_BD_CPU
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AdcChannels - Controls the A/D Conversion system so that
// sensor information can be retrieved.  Also allows the ADC system
// to be put in different operation modes.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class is the interface to the A/D conversion H/W sub-system
//      and supplies all the methods required to sample the ADC channels,
//      to test the ADC, and to control its operational modes.  Methods are
//		implemented to:
//
//      * get a new sample from the specified channel.
//
//		* get the last BD cycle sample for the specified channel (12 bits).
//
//		* get the last BD cycle sample for the specified channel (16 bits).
//
//      * get a new sample for all the channels (12 & 16 bits).
//
//      * stop the ADC system.
//
//      * reset the ADC system.
//
//      * restart the ADC system at channel 0.
//
//      * get the monitor count.
//
//      * initializes the ADC system.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of the Analog to Digital Conversion (ADC)
//      hardware system.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This class is utilizing the low level I/O capabilities
//      supplied by the ADC programmable hardware to implement
//      sampling, controlling and testing of the ADC sub-system.
//
//      The A/D conversion H/W can be in one of the following states:
//
//      1) Freeze - Frozen at channel 0, no A/D operation.
//                  This is the power-up and hard reset state.
//
//      2) Normal - All A/D channels are converted.
//
//      Several commands are available for controlling the A/D sequencer:
//
//      1) Soft reset - issues a command that restarts the A/D sequencer
//				immediately.  It also resets all D/A values to 0.
//
//      2) Resynch - issues a command to restart the A/D sequencer at channel 0.
//
//      3) Freeze - issues a command to stop the A/D conversions.
//
//      4) Monitor - issues a command to obtain the counter that monitors the
//				number of times a complete set of channels (NUM_ADC_CHANNELS)
//				has been converted.
//
//      NewCycle samples each channel and stores the samples in two arrays (12 bit
//		16 bit storage).
//
//		4 distint signals are shared by the same ADC channel.  The selection of one
//		of the 4 signals is controlled by the SubMux class which is called in the
//		NewCycle() method.
//
//      NOTE: H/W arbitration prevents collisions between H/W updating
//              channel data and S/W reading channel data.
//
// $[00401] $[00478] 
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      This is a static class and is not to be constructed.
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/AdcChannels.ccv   26.0.1.0   04 Feb 2013 10:36:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 011  By: pc    Date:  22-Oct-1997    DR Number: 2569
//      Project:  Sigma (840)
//              Description:
//                      Update Compiler Option SIGMA_INTEGRATION to use Sigma Integration Test Program
//                             (aka TestCommand Group)
//                      Provides interface for overriding a/d channel samples by operator supplied data.
//                             (on a channel by channel basis)
//                      Add Modification header for this revision; make test code independent of prod. code.
//
//  Revision: 010  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 009 By: syw    Date: 22-May-1997   DR Number: DCS 2165
//  	Project:  Sigma (R8027)
//		Description:
//			Moved TI label.
//
//  Revision: 008 By: syw    Date: 02-Jan-1997   DR Number: DCS 1651
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate SoftReset() since rSoftResetPort is eliminated.
//
//  Revision: 007 By: syw    Date: 16-Sep-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Fix over/underflow problem in GetNewSample() when offset is added
//			to sample value.
//
//  Revision: 006 By: syw    Date: 22-Aug-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate compiler warnings.
//
//  Revision: 005 By: syw    Date: 04-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added AdcInfo_ to store encoded data along with counts.
//
//  Revision: 004 By: syw    Date: 01-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Removed SetResynchDelay() and GetResynchDelay() since it
//			is no longer supported.  Mask monitor count to lower four bits.
//			Removed StartAdc() method since it only needs to call Resynch().
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By:  syw   Date:  05-Dec-1995    DR Number: DCS 562
//      Project:  Sigma (R8027)
//      Description:
//			Comment out Repeat() method and REPEAT AdcState usage.  Initialize
//			Offset_ data member.  Adjust samples for ground reference offset.
//			Call SubMux::NewCycle in AdcChannels::NewCycle().  In GetNewSample(),
//			prevent the usage of this method if the id is SUB_MUX_SENSORS since
//			there is no direct method to select the specific SUB_MUX_SENSORS.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================
#include "AdcChannels.hh"
#include "DebugAssert.h"
#include "CriticalSection.hh"

//@ End-Usage

static const Uint16 ADC_FILTER_TC	= 8;

AdcCounts AdcChannels::Channel_[AdcChannels::NUM_ADC_CHANNELS_RAW_AND_FILTERED]  = {
0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 
0, 0, 0, 0, 0
};

AdcData_t	AdcChannels::rawDataBuf_[NUM_ADC_READS_PER_CYCLE];
Int8		AdcChannels::rawDataBufIndex_ = 0;
Uint32		AdcChannels::numRawDataSamples_ = 0;
bool		AdcChannels::initialized_ = false;
DataFilter_t<AdcCounts>	AdcChannels::filteredDataBuf_[AdcChannels::NUM_ADC_CHANNELS_RAW];

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AdcData_t()
//
//@ Interface-Description
//      Constructor.  This method has no arguments and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
AdcChannels::AdcChannels()
{

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AdcData_t ()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
AdcChannels::~AdcChannels()
{

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
//      This method takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Initializes class variables and set a flag to indicate the status.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void AdcChannels::Initialize()
{
	rawDataBufIndex_ = 0;
	numRawDataSamples_ = 0;
	memset(rawDataBuf_, 0, sizeof(rawDataBuf_));
	memset(filteredDataBuf_, 0, sizeof(filteredDataBuf_));

	initialized_ = true;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NewCycle()
//
//@ Interface-Description
//      This method takes no arguments and updates the sample data
//      for all NUM_ADC_CHANNELS channels.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The sample data is retrieved from the A/D conversion H/W via
//      an I/O port and stores the data in the Channel_ (12 bit) array.
//		The I/O port value is calculated
//		by using the channel id as an offset to the base A/D I/O address.
//		The offset due to "grounding" is added to the Channel_ array.  The
//		submux channel is also updated so that each submux signal is sampled
//		every fourth NewCycle() call.
//---------------------------------------------------------------------
//@ PreCondition
//      The ADC should be in the normal state.
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
AdcChannels::NewCycle( void)
{
	CALL_TRACE("AdcChannels::NewCycle( void)") ;

	if(!initialized_)
	{
		Initialize();
		return;
	}

	static bool bDebugAssert = false;

	// enter critical section to make sure that numRawDataSamples_ is not changed
	MutEx mutex(ADC_CHANNELS_MT);
	mutex.request();

	// for the first new cycle, numRawDataSamples_ would not be equal to NUM_ADC_READS_PER_CYCLE
	// but after that it should be.
	// Instead of doing an assertion, just do a printf to evaluate this design
	if(AdcChannels::numRawDataSamples_ != NUM_ADC_READS_PER_CYCLE)
	{
		if(bDebugAssert)
		{
			DEBUGMSG(0, (L"AdcChannels: DEBUG_ASSERT, rawDataBufIndex_ = %d, numRawDataSamples_ = %d\r\n", 
			AdcChannels::rawDataBufIndex_, AdcChannels::numRawDataSamples_));
		}
		else
		{
			bDebugAssert = true;
		}
	}
	
	numRawDataSamples_ = 0;

	// exit critical section
	mutex.release();

    for (Uint16 i=0; i < NUM_ADC_CHANNELS_RAW; i++)
    {
    	// use average value for cycle sample
		AdcChannels::Channel_[i] = GetCycleAvg((AdcChannels::AdcChannelId)i);
		// filter all adc channels as a convenience
		AdcChannels::filteredDataBuf_[i].FilterValue(Channel_[i], ADC_FILTER_TC);
    }

	// set up filtered value as hardware simulation for easy access by GetBdCycleSample()
	// TODO E600_LL - the filtered algorithm will need to be reviewed
	Channel_[ADC_INSP_PRESSURE_FILTERED] = filteredDataBuf_[ADC_INSP_PRESSURE_TRANSDUCER].GetFilteredValue();
	Channel_[ADC_EXP_PRESSURE_FILTERED] = filteredDataBuf_[ADC_EXP_PRESSURE_TRANSDUCER].GetFilteredValue();
	Channel_[ADC_AIR_FLOW_FILTERED] = filteredDataBuf_[ADC_AIR_FLOW].GetFilteredValue();
	Channel_[ADC_O2_FLOW_FILTERED] = filteredDataBuf_[ADC_O2_FLOW].GetFilteredValue();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetNewSample()
//
//@ Interface-Description
//      This method takes one argument, id, and returns a newly sampled
//		value for the ADC id specified (12 bit).
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method returns the sampled value retrieved from the A/D
//      conversion H/W via an I/O port.  The I/O port value is calculated
//      by using the id argument as an offset to the base A/D I/O address.
//		The subMux channel should not be used for a new sample since the
//		actual submux channel can not be controlled directly.  It is only
//		controlled by calling the AdcChannels::NewCycle() method.
//---------------------------------------------------------------------
//@ PreCondition
//      The channel id has a range of 0 to NUM_ADC_CHANNELS and the id
//		not equal to SUBMUX_SENSORS.
//      The ADC sequencer should be in the NORMAL state.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

AdcCounts
AdcChannels::GetNewSample( const AdcChannels::AdcChannelId id)
{
	CALL_TRACE("AdcChannels::GetNewSample( const AdcChannels::AdcChannelId id)") ;

    CLASS_PRE_CONDITION( id >= 0 && id < NUM_ADC_CHANNELS_RAW_AND_FILTERED) ;
  
    
	return Channel_[(Uint16)id] ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetRawAdcData()
//
//@ Implementation-Description
//	This method is called by the Analog Input Thread to collect ADC data and increment
//	the samples per cycle count.  Analog Input Thread has the highest priority so no
//	critical section is needed here.
//---------------------------------------------------------------------
void AdcChannels::SetRawAdcData(AdcData_t &rawData)
{
	CLASS_PRE_CONDITION(rawDataBufIndex_ >= 0 && rawDataBufIndex_ <= NUM_ADC_READS_PER_CYCLE);
	
	// save data and keep track of number of samples per cycle
	rawDataBuf_[rawDataBufIndex_] = rawData;
	
	// increment to point to location to store new data
	// wrap around if nec.
	if(++rawDataBufIndex_ >= NUM_ADC_READS_PER_CYCLE)
	{
		rawDataBufIndex_ = 0;
	}	
	numRawDataSamples_++;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetCycleAvg()
//
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method computes the average of raw ADC values that have been
//		sampled for the last cycle for the input channel.
//---------------------------------------------------------------------
AdcCounts AdcChannels::GetCycleAvg(const AdcChannels::AdcChannelId id)
{
	CLASS_PRE_CONDITION(id >= ADC_FIRST_CHANNEL && id < NUM_ADC_CHANNELS_RAW);

	AdcCounts sum = 0;
	for(int i=0; i<NUM_ADC_READS_PER_CYCLE; i++)
	{
		sum+= (rawDataBuf_[i].GetValue((Uint16)id));
	}
	return (sum / NUM_ADC_READS_PER_CYCLE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
AdcChannels::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
                   		const char*        pFileName,
                   		const char*        pPredicate)
{
    CALL_TRACE("AdcChannels::SoftFault( softFaultID, lineNumber, pFileName, \
                pPredicate)") ;
                
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, ADCCHANNELS,
                             lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//#endif // SIGMA_BD_CPU
