#ifndef PostAdcChannels_HH
#define PostAdcChannels_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PostAdcChannels - Extract from enum AdcChannelId in 
//                           BD-IO-Devices subsystem.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST/vcssrc/PostAdcChannels.hhv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

class PostAdcChannels {
  public:

    //@ Type: AdcChannelId
    // ID of the analog to digital converter channels
    // The order corresponds to actual channel numbers from 0 to 31
    enum AdcChannelId {
        INSP_SIDE_PRESSURE_SENSOR,
        EXH_SIDE_PRESSURE_SENSOR,
        O2_FLOW_SENSOR,
        O2_TEMPERATURE_SENSOR,
        AIR_FLOW_SENSOR,
        AIR_TEMPERATURE_SENSOR,
        EXHALATION_FLOW_SENSOR, 
        EXHALATION_GAS_TEMPERATURE_SENSOR,
        SUBMUX_SENSORS,
        SAFETY_VALVE_SWITCHED_SIDE,
        DC_12V_GUI_SENTRY,
        ALARM_CABLE_VOLTAGE,
        O2_PSOL_CURRENT,
        AIR_PSOL_CURRENT,
        LOW_VOLTAGE_REFERENCE,
        ATMOSPHERIC_PRESSURE_SENSOR,
        EXHALATION_COIL_TEMPERATURE_SENSOR,
        EXH_PRESSURE_FILTERED,
        O2_SENSOR,
        DC_5V_GUI_SENTRY,
        DC_12V_SENTRY,
        SAFETY_VALVE_STATE,
        DC_15V_SENTRY,
        DC_NEG_15V_SENTRY,
        POWER_FAIL_CAP_VOLTAGE,
        EXHALATION_HEATER_TEMPERATURE,
        SYSTEM_BATTERY_VOLTAGE,
        DC_5V_VENTHEAD,
        SYSTEM_BATTERY_CURRENT,
        AC_LINE_VOLTAGE,
        EXHALATION_MOTOR_CURRENT,
        DC_10V_SENTRY,
        NUM_ADC_CHANNELS
    } ;
} ;

#endif // PostAdcChannels_HH
