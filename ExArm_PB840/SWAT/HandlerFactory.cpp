#include "stdafx.h"
////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		HandlerFactory.cpp

Purpose:
	Implementation file for CommandHandlerFactory class.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 20-AUG-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/

#include "HandlerFactory.h"
#include "Handlers.h"

// define Mem for all handlers
static Uint SimulateTouchHandlerMem[ (sizeof(SimulateTouchHandler)+sizeof(Uint))/sizeof(Uint) ];
static Uint SimulateBezelKeyHandlerMem[ (sizeof(SimulateBezelKeyHandler)+sizeof(Uint))/sizeof(Uint) ];
static Uint SimulateKnobHandlerMem[ (sizeof(SimulateKnobHandler)+sizeof(Uint))/sizeof(Uint) ];
static Uint ScreenCaptureHandlerMem[ (sizeof(ScreenCaptureHandler)+sizeof(Uint))/sizeof(Uint) ];
static Uint ScreenCaptureBufferInfoHandlerMem[ (sizeof(ScreenCaptureHandler)+sizeof(Uint))/sizeof(Uint) ];
static Uint ClearScreensHandlerMem[ (sizeof(ClearScreensHandler)+sizeof(Uint))/sizeof(Uint) ];
static Uint QueryScreenshotFileNameHandlerMem[ (sizeof(QueryScreenshotFileNameHandler)+sizeof(Uint))/sizeof(Uint) ];

CommandHandlerBase * CommandHandlerFactory::QueryHandler(const char * type)
{
	if (strcmp( type, "push_xy" ) == 0)
	{
		return new (SimulateTouchHandlerMem) SimulateTouchHandler();
	}
	else if (strcmp( type, "bezel_key_press" ) == 0)
	{
		return new (SimulateBezelKeyHandlerMem) SimulateBezelKeyHandler();
	}
	else if (strcmp( type, "knob" ) == 0)
	{
		return new (SimulateKnobHandlerMem) SimulateKnobHandler();
	}
	else if (strcmp( type, "screen_capture" ) == 0)
	{
		return new (ScreenCaptureHandlerMem) ScreenCaptureHandler();
	}
	else if (strcmp( type, "screen_capture_buffer_info" ) == 0)
	{
		return new (ScreenCaptureBufferInfoHandlerMem) ScreenCaptureBufferInfoHandler();
	}
	else if (strcmp( type, "clear_screens" ) == 0)
	{
		return new (ClearScreensHandlerMem) ClearScreensHandler();
	}
	else if (strcmp( type, "query_sc_ftp_addr" ) == 0)
	{
		return new (QueryScreenshotFileNameHandlerMem) QueryScreenshotFileNameHandler();
	}

	return NULL;
}
