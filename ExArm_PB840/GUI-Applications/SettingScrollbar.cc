#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SettingScrollbar - A vertical scrollbar for scrollable menus
//---------------------------------------------------------------------
//@ Interface-Description 
//  A SettingScrollbar is a Scrollbar with a modified appearance. A
//  SettingScrollbar is normally instantiated within a GuiTable, but can
//  be instantiated within any class. The SettingScrollbar is a narrower
//  scrollbar than that provided by the Scrollbar base class. It also
//  does not have an "active" elevator button. Its elevator button is
//  always set flat, but using the base class methods for positioning
//  the elevator, reflects the relative position of the first displayed
//  entry within the number of the entries in the menu.
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the scrollbar characteristics of a scrollable menu
//  scrollbar.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The Scrollbar base class provides all the functionality of
//  the SettingScrollbar. This class only sets up the standard
//  appearance of a scrollbar appearing to the right of a ScrollableMenu
//  or the ScrollableEventLog.
//---------------------------------------------------------------------
//@ Fault-Handling
//  None
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SettingScrollbar.ccv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  15-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Initial version.
//
//====================================================================


#include "SettingScrollbar.hh"

//@ Usage-Classes
#include "ScrollbarTarget.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 WIDTH_ = 10;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SettingScrollbar()  [Constructor]
//
//@ Interface-Description
// Constructs a Scrollbar object.  Passed the following parameters:
// >Von
//	height			Height of scrollbar in pixels (width is fixed).
// 
//	numDisplayRows	The number of displayed rows in the scrolling area.
// 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//	Calls the base class constructor with the specified parameters. Sets
//	the elevator button flat and changes its colors to a set of standard
//  scrollable menu colors. There is no ScrollbarTarget required for a
//  SettingScrollbar since the scrollbar button is never activated so
//  it will never receive any AdjustPanel (scrolling) events.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SettingScrollbar::SettingScrollbar(Uint16 height, 
								   Uint16 numDisplayRows)
:   Scrollbar(height, numDisplayRows, NULL)
{
	CALL_TRACE("Scrollbar::Scrollbar(void)");

	elevatorButton_.setToFlat();
	elevatorButton_.setColor(Colors::LIGHT_BLUE);
	elevatorButton_.setBorderColor(Colors::WHITE);
	elevatorButton_.setFillColor(Colors::WHITE);

	setWidth(WIDTH_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SettingScrollbar()  [Destructor]
//
//@ Interface-Description
// Scrollbar destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SettingScrollbar::~SettingScrollbar(void)
{
	CALL_TRACE("SettingScrollbar::~Scrollbar(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void SettingScrollbar::SoftFault(const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
								const char*        pFileName,
								const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SETTINGSCROLLBAR,
							lineNumber, pFileName, pPredicate);
}
