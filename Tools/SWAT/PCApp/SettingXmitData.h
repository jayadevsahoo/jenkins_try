//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file SettingXmitData.h
//----------------------------------------------------------------------------



#ifndef SETTINGXMITDATA_H
#define SETTINGXMITDATA_H

//#include "SettingsInterfacePkg.h"

//@ Type:  SettingsXactionId
// Transaction ID for the communication of settings.
enum SettingsXactionId {
    // Transaction initiation...
    INITIATE_TRANSACTION = 4,   // GUI begins the transaction...

    // Transaction #1...
    CONNECTION_INITIATED = 5,   // connection opened by GUI, to BD...

    // Transaction #2...
    CONNECTION_COMPLETED = 6,   // transmitted from BD to GUI...

    // Transaction #3...
    VENT_STARTUP_UPDATE_XMITTED = 10,   // transmitted from GUI to BD...
    USER_UPDATE_XMITTED = 11,   // transmitted from GUI to BD...
    EXTERNAL_UPDATE_XMITTED = 12,   // (reserved for EPT combatability)...
    ONLINE_SYNC_XMITTED = 13,   // transmitted from GUI to BD...

    // Transaction #4...
    VALIDATE_XMITTED_SETTINGS = 20, // transmitted from BD to GUI...

    // Transaction #5...
    ACCEPT_XMITTED_SETTINGS = 30,   // transmitted from GUI to BD...

    // Transaction #6...
    SETTINGS_XACTION_COMPLETE = 40, // transmitted from BD to GUI...

    NACK_OR_ERROR = 99
};

// ONLY FOR TIME BEING!
typedef short Int16;
typedef char Int8;
typedef float Real32;
typedef long Int32;
typedef unsigned short Uint16;
typedef unsigned int Uint;
typedef unsigned int Uint32;

// 8 bytes!!
struct NetworkMsgHeader {

    //@ Data-Member: msgId;
    // msgId identifies the type of the network message of the following
    // packet.
    Uint16 msgId;

    //@ Data-Member: pktSize;
    // pktSize indicates size of the following network packet in bytes.
    Uint16 pktSize;

    //@ Data-Member seqNumber
    //  Sequence number for the following network packet.
    Uint32 seqNumber;
};

struct SettingXmitData {
    Int16 settingXmitId;
    Int8 reserved;
    Int8 boundedPrec;

    union                       // anonymous union...
    {
        Real32 boundedValue;
        Int32 discreteValue;
    };
};


#pragma warning(disable:4200)

struct SettingXactionBlock {
    Uint16 xactionId;
    Uint16 numXmitElems;
    SettingXmitData arrXmitElems[];
};
#endif // SETTINGXMITDATA_H
