#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: Watchdog.cc - Strobe the watchdog.
//
//---------------------------------------------------------------------
//@ Interface-Description
//  Strobe the watchdog and monitor the time period between strobes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Any write to the watchdog address will reset the timer.
//---------------------------------------------------------------------
//@ Fault-Handling
//    None
//---------------------------------------------------------------------
//@ Restrictions
//    None
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/Watchdog.ccv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
// 
// Revision 004   By: gdc     Date: 27-Jan-2011       SCR Number: 6706
// Project: XENA2
// Description:
//      Changed to use CpuDevice::IsXena2Config and changed interval
//      calculation to account for counter rollover.
//
// Revision 003   By: mnr     Date: 12-Nov-2010       SCR Number: 6671
// Project: XENA2
// Description:
//      IntervalVerify_ default changed to FALSE and PendingVerify_
//      default changed to TRUE, to avoid false error on power up.
//      If Xena II config, use FPGA counter instead of RTC to avoid 
//      getting false Task Monitor hits.
//
// Revision 002   By: gdc     Date: 06-May-2009       SCR Number: 6509
// Project: 840S
// Description:
//      Changed to log tardy watchdog strobe only once per power cycle.
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================

#include "Watchdog.hh"
#include "TimeStamp.hh"
#include "Background.hh"
#include "DiagnosticCode.hh"
#include "CpuDevice.hh"

#if defined(SIGMA_GUI_CPU) || defined(SIGMA_BD_CPU)
static Boolean IntervalVerify_ = FALSE;           
static Boolean PendingVerify_ = TRUE; 
#endif

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Strobe [Static]
//
//@ Interface-Description
//  Strobe the watchdog.  Takes no arguments and returns void.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Strobes the watchdog by writing to the watchdog reset register. 
//  This method verifies the interval between calls to this routine
//  using the external time of day clock.  If this interval exceeds
//  40ms then it logs a task monitor failure to the system diagnostic
//  log. For Xena2 boards, the FPGA counter driven by the Ethernet
//  clock source is used to calculate the interval between strobes.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
Watchdog::Strobe(void)
{
#if defined(SIGMA_GUI_CPU) || defined(SIGMA_BD_CPU)
#ifdef SIGMA_PRODUCTION

    static const Uint32 MIN_WATCHDOG_STROBE_PERIOD = 40;
    static const Uint32 FPGA_CTR_CYCLE_TIME_MS = 5;
    static const Uint32 FPGA_CTR_MAX_VALUE = 0xFFFFFFFF;

    static TimeStamp    time;
    static Boolean      IsFaultLogged_ = FALSE;
    static Boolean      IsXena2Config_ = CpuDevice::IsXena2Config();
    static Uint32       CtrValPrev_ = IsXena2Config_ ? (*FPGA_CTR) : 0;

    Int32          interval;

    STROBE_WATCHDOG;

    if (IsXena2Config_)
    {
        Uint32 ctrValNow = *FPGA_CTR;
        Uint32 diff;

        if (ctrValNow < CtrValPrev_)
        {
            // account for rollover (every 248.55 days) someone will test this!
            diff = (FPGA_CTR_MAX_VALUE - CtrValPrev_ + 1) + ctrValNow;
        }
        else
        {
            diff = ctrValNow - CtrValPrev_;
        }
        //convert the diff to time in milliseconds
        interval = diff * FPGA_CTR_CYCLE_TIME_MS;
        CtrValPrev_ = ctrValNow;
    }
    else
    {
        TimeStamp      timeNow;
        interval = timeNow - time;
        time = timeNow;
    }

    if ( IntervalVerify_ && (interval > MIN_WATCHDOG_STROBE_PERIOD) && !IsFaultLogged_ )
    {
        IsFaultLogged_ = TRUE;

        DiagnosticCode intervalCode;

        intervalCode.setBackgroundTestCode( ::MINOR_TEST_FAILURE_ID
                                            ,(Uint16) ::BK_TASK_MONITOR_FAIL
                                            ,(Uint32) interval );
        FaultHandler::LogDiagnosticCode( intervalCode );
    }
    // $[TI1.2] implied else

    if (PendingVerify_)
    {
        // $[TI2.1]
        IntervalVerify_ = TRUE;
        PendingVerify_ = FALSE;
    }
    // $[TI2.2] implied else

#endif // SIGMA_PRODUCTION
#endif // defined(SIGMA_GUI_CPU) || defined(SIGMA_BD_CPU)

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetIntervalVerify [Static]
//
//@ Interface-Description
//  Sets watchdog strobe interval verification on or off.  Interval
//  verification results in a diagnostic log entry if the watchdog
//  strobe occurs outside a 40 millisecond window.  Interval
//  verification is disabled immediately whereas interval verification
//  is enabled only after the next strobe cycle completes.  This allows
//  the system to change the time of day clock with triggering a
//  interval verification entry.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the private static IntervalVerify_ and PendingVerify_ used by 
//  the Strobe method.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

#if defined(SIGMA_GUI_CPU) || defined(SIGMA_BD_CPU)
void
Watchdog::SetIntervalVerify(Boolean verify)
{
    if ( verify )
    {
        // $[TI1.1]
        PendingVerify_ = TRUE;
    }
    else
    {
        // $[TI1.2]
        IntervalVerify_ = FALSE;
    }
}
#endif // SIGMA_GUI_CPU || SIGMA_BD_CPU
