#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DateTimeSettingsSubScreen - The subscreen selected by the
// Date/Time Change Settings button in the Lower Other Screens subscreen.
// Allows adjusting of date and time.
//---------------------------------------------------------------------
//@ Interface-Description
// The subscreen contains a setting button for each of the Date/Time
// settings.  The Accept key pressed in order to accept and apply the
// settings changes.
//
// The activateHappened()/deactivateHappened() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// acceptHappened() method is called by BatchSettingsSubScreen when the
// settings are being accepted.  Timer events (specifically subscreen
// setting change timeout event) are passed into timerEventHappened()
// and Adjust Panel events are communicated via the various Adjust
// Panel "happened" methods.
//
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the DateTimeSettingsSubscreen
// in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The implementation is quite simple.  Five setting buttons are displayed
// Each of the five settings, by the virtue of being a SettingButton, is
// registered with the ContextObserver for changes to that setting.
//
// If the Accept key is pressed then the acceptHappened() method is called
// to accept the setting changes and deactivate the subscreen.
//
// The subscreen is also registered as a target of the Subscreen's Setting 
// Change Timeout timer. 
// If the timer runs out this is detected in timerEventHappened() and
// the subscreen is reset.
//
// $[01057]
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DateTimeSettingsSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: rhj   Date:  23-May-2007    SCR Number: 6237 
//  Project:  Trend
//  Description:
//      Added Date International feature.
//
//  Revision: 008   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//      Delete restrictions that no longer applies.
//
//  Revision: 007   By: sah   Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By: yyy      Date: 04-Aug-1997  DR Number: 2314
//    Project:  Sigma (R8027)
//    Description:
//      Added DATE_TIME_CANCEL_S prompt for service mode.
//
//  Revision: 002  By:  yyy   Date:  04-Aug-1997    DCS Number:  1846
//  Project:  Sigma (R8027)
//  Description:
//      Disable the setting buttons if the VitalPatientDataArea is displaying
//      the no ventilation for xxx seconds.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "DateTimeSettingsSubScreen.hh"

//@ Usage-Classes
#include "PromptStrs.hh"
#include "MiscStrs.hh"
#include "MonthValue.hh"
#include "AdjustPanel.hh"
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "PromptArea.hh"
#include "SettingContextHandle.hh"
#include "Sound.hh"
#include "SubScreenArea.hh"
#include "VitalPatientDataArea.hh"
#include "UpperScreen.hh"
#include "DateFormatValue.hh"
#include "AdjustedContext.hh"
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SettingsMgr.hh"
#include "SettingSubject.hh"
#include "Setting.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int16 BUTTON_HEIGHT_ = 46;
static const Int16 BUTTON_WIDTH_ = 80;
static const Int16 BUTTON_BORDER_ = 3;
static const Int16 BUTTON_GAP_ = 20;
static const Int16 BUTTON_Y_ = 76;
static const Int16 BUTTON_X_ 
		= ( ::SUB_SCREEN_AREA_WIDTH - 5*BUTTON_WIDTH_ - BUTTON_GAP_ ) / 2;
static const Int16 BUTTON_Y_SM_ = 116;
static const Int16 SEQUENTIAL_VALUE_CENTER_X_ = 34;
static const Int16 SEQUENTIAL_VALUE_CENTER_Y_ = 26;

static const Int32 DATE_FORMAT_BUTTON_WIDTH_        = 180;

static Uint  MonthInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		    (sizeof(StringId) * (MonthValue::NUM_MONTH_VALUES - 1)))];

static DiscreteSettingButton::ValueInfo*  PMonthInfo_ =
					(DiscreteSettingButton::ValueInfo*)::MonthInfoMemory_;

static Uint  DateFormatMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
		    (sizeof(StringId) * (DateFormatValue::NUM_DATE_FORMAT_VALUES - 1)))];

static DiscreteSettingButton::ValueInfo*  PDateFormatMemory_ =
					(DiscreteSettingButton::ValueInfo*)::DateFormatMemory_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DateTimeSettingsSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the DateTimeSettingsSubscreen.  Passed a pointer to the subscreen
// area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various buttons displayed in the subscreen and adds them
// to the container.
//
// $[01320] The date/time change subscreen shall allow the user to ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DateTimeSettingsSubScreen::DateTimeSettingsSubScreen(SubScreenArea *pSubScreenArea) :
	BatchSettingsSubScreen(pSubScreenArea),
	hourButton_(BUTTON_X_ + BUTTON_GAP_ + 3*(BUTTON_WIDTH_), 
			BUTTON_Y_ + BUTTON_HEIGHT_ + BUTTON_GAP_,
			BUTTON_WIDTH_,				BUTTON_HEIGHT_,
			Button::LIGHT, 				BUTTON_BORDER_,
			Button::ROUND, 				Button::NO_BORDER,
			TextFont::EIGHTEEN,
			SEQUENTIAL_VALUE_CENTER_X_, SEQUENTIAL_VALUE_CENTER_Y_,
			MiscStrs::HOUR_BUTTON_TITLE,
			SettingId::HOUR, 			this,
			MiscStrs::HOUR_HELP_MESSAGE),
	minuteButton_(BUTTON_X_ + BUTTON_GAP_ + 4*(BUTTON_WIDTH_), 
			BUTTON_Y_ + BUTTON_HEIGHT_ + BUTTON_GAP_,
			BUTTON_WIDTH_, 				BUTTON_HEIGHT_,
			Button::LIGHT, 				BUTTON_BORDER_,
			Button::ROUND, 				Button::NO_BORDER,
			TextFont::EIGHTEEN,
			SEQUENTIAL_VALUE_CENTER_X_, SEQUENTIAL_VALUE_CENTER_Y_,
			MiscStrs::MINUTE_BUTTON_TITLE,
			SettingId::MINUTE, 			this,
			MiscStrs::MINUTE_HELP_MESSAGE),
	dayButton_(BUTTON_X_ + 0*(BUTTON_WIDTH_), 
			BUTTON_Y_ + BUTTON_HEIGHT_ + BUTTON_GAP_,
			BUTTON_WIDTH_,				BUTTON_HEIGHT_,
			Button::LIGHT, 				BUTTON_BORDER_,
			Button::ROUND, 				Button::NO_BORDER,
			TextFont::EIGHTEEN,
			SEQUENTIAL_VALUE_CENTER_X_, SEQUENTIAL_VALUE_CENTER_Y_,
			MiscStrs::DAY_BUTTON_TITLE,
			SettingId::DAY, 			this,
			MiscStrs::DAY_HELP_MESSAGE),
	monthButton_(BUTTON_X_ + 1*(BUTTON_WIDTH_), 
                 BUTTON_Y_ + BUTTON_HEIGHT_ + BUTTON_GAP_,
			BUTTON_WIDTH_,				BUTTON_HEIGHT_,
			Button::LIGHT, 				BUTTON_BORDER_,
			Button::ROUND, 				Button::NO_BORDER,
			MiscStrs::MONTH_BUTTON_TITLE,
			SettingId::MONTH, this,
			MiscStrs::MONTH_HELP_MESSAGE, 18),
	yearButton_(BUTTON_X_ + 2*(BUTTON_WIDTH_), 
                BUTTON_Y_ + BUTTON_HEIGHT_ + BUTTON_GAP_,
			BUTTON_WIDTH_,				BUTTON_HEIGHT_,
			Button::LIGHT, 				BUTTON_BORDER_,
			Button::ROUND, 				Button::NO_BORDER,
			TextFont::EIGHTEEN,
			SEQUENTIAL_VALUE_CENTER_X_, SEQUENTIAL_VALUE_CENTER_Y_,
			MiscStrs::YEAR_BUTTON_TITLE,
			SettingId::YEAR, 			this,
			MiscStrs::YEAR_HELP_MESSAGE),
	dateFormatButton_(BUTTON_X_ + 0*(BUTTON_WIDTH_),
				(BUTTON_Y_ ), 
                DATE_FORMAT_BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::ROUND, Button::NO_BORDER,
				MiscStrs::DATE_FORMAT_BUTTON_TITLE,
				SettingId::DATE_FORMAT, this,
				MiscStrs::DATE_FORMAT_HELP_MESSAGE, 12),
	titleArea_(MiscStrs::PROPOSED_DATE_TIME_SETTINGS_TITLE, 
			SubScreenTitleArea::SSTA_CENTER)
{
	CALL_TRACE("DateTimeSettingsSubScreen::DateTimeSettingsSubScreen(SubScreenArea "
													"*pSubScreenArea)");
	// Size and position the subscreen
	setX(0);
	setY(0);

	setFillColor(Colors::MEDIUM_BLUE);

	// Add the title area
	addDrawable(&titleArea_);


	// set up values for month....
	::PMonthInfo_->numValues = MonthValue::NUM_MONTH_VALUES;
	::PMonthInfo_->arrValues[MonthValue::JAN] = MiscStrs::JANUARY_VALUE;
	::PMonthInfo_->arrValues[MonthValue::FEB] = MiscStrs::FEBRUARY_VALUE;
	::PMonthInfo_->arrValues[MonthValue::MAR] = MiscStrs::MARCH_VALUE;
	::PMonthInfo_->arrValues[MonthValue::APR] = MiscStrs::APRIL_VALUE;
	::PMonthInfo_->arrValues[MonthValue::MAY] = MiscStrs::MAY_VALUE;
	::PMonthInfo_->arrValues[MonthValue::JUN] = MiscStrs::JUNE_VALUE;
	::PMonthInfo_->arrValues[MonthValue::JUL] = MiscStrs::JULY_VALUE;
	::PMonthInfo_->arrValues[MonthValue::AUG] = MiscStrs::AUGUST_VALUE;
	::PMonthInfo_->arrValues[MonthValue::SEP] = MiscStrs::SEPTEMBER_VALUE;
	::PMonthInfo_->arrValues[MonthValue::OCT] = MiscStrs::OCTOBER_VALUE;
	::PMonthInfo_->arrValues[MonthValue::NOV] = MiscStrs::NOVEMBER_VALUE;
	::PMonthInfo_->arrValues[MonthValue::DEC] = MiscStrs::DECEMBER_VALUE;
	monthButton_.setValueInfo(::PMonthInfo_);


	// set up values for Date Format Values
    ::PDateFormatMemory_->numValues = DateFormatValue::NUM_DATE_FORMAT_VALUES;
	::PDateFormatMemory_->arrValues[DateFormatValue::DATE_FORMAT_DD_MM_YY] =
												MiscStrs::DATE_FORMAT_DD_MM_YY_LABEL;
	::PDateFormatMemory_->arrValues[DateFormatValue::DATE_FORMAT_DD_MMM_YY] =
												MiscStrs::DATE_FORMAT_DD_MMM_YY_LABEL;
	::PDateFormatMemory_->arrValues[DateFormatValue::DATE_FORMAT_MM_DD_YY_WITH_DASH] =
												MiscStrs::DATE_FORMAT_MM_DD_YY_WITH_DASH_LABEL;
	::PDateFormatMemory_->arrValues[DateFormatValue::DATE_FORMAT_MM_DD_YY_WITH_SLASH] =
												MiscStrs::DATE_FORMAT_MM_DD_YY_WITH_SLASH_LABEL;
	::PDateFormatMemory_->arrValues[DateFormatValue::DATE_FORMAT_YY_MM_DD] =
												MiscStrs::DATE_FORMAT_YY_MM_DD_LABEL;
	::PDateFormatMemory_->arrValues[DateFormatValue::DATE_FORMAT_YY_MMM_DD] =
												MiscStrs::DATE_FORMAT_YY_MMM_DD_LABEL;

    dateFormatButton_.setValueInfo(::PDateFormatMemory_ );


	Uint  idx;

	idx = 0u;
	arrSettingButtonPtrs_[idx++] = &monthButton_;
	arrSettingButtonPtrs_[idx++] = &minuteButton_;
	arrSettingButtonPtrs_[idx++] = &hourButton_;
	arrSettingButtonPtrs_[idx++] = &dayButton_;
	arrSettingButtonPtrs_[idx++] = &yearButton_;
    arrSettingButtonPtrs_[idx++] = &dateFormatButton_;
	arrSettingButtonPtrs_[idx]   = NULL;

	AUX_CLASS_ASSERTION((idx <= MAX_SETTING_BUTTONS_), idx);

	// Add the buttons to the subscreen
	for (idx = 0u; arrSettingButtonPtrs_[idx] != NULL; idx++ )
	{
		addDrawable(arrSettingButtonPtrs_[idx]);
	}
								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DateTimeSettingsSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys DateTimeSettingsSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

DateTimeSettingsSubScreen::~DateTimeSettingsSubScreen(void)
{
	CALL_TRACE("DateTimeSettingsSubScreen::~DateTimeSettingsSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// Called by the BatchSettingsSubScreen when the operator presses the 
// Accept key. We accept the settings and dismiss this screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Make the "Accept" sound, clear the Adjust Panel focus, accept the
// adjusted settings, deactivate this subscreen
// Generate fake Wall Clock timer event for object that displays the
// date/time so that the date/time display is updated.
//
// $[01056] When batch settings are being setup ventilator settings
//			will not be affected
// $[01118] User must press Accept key to accept batch settings.
// $[01257] The Accept key shall be the ultimate confirmation step for all ...
// $[01258] The GUI shall accept all adjusted settings.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DateTimeSettingsSubScreen::acceptHappened(void)
{
	CALL_TRACE("DateTimeSettingsSubScreen::acceptHappened(void)");

	// Clear the Adjust Panel focus, make the Accept sound, store the
	// current settings, accept the adjusted settings and deactivate
	// this subscreen.
	Sound::Start(Sound::ACCEPT);

    AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

    // Accept Date Format setting.
    SettingContextHandle::AcceptBatch();

    // Accept date/time settings
	SettingContextHandle::AcceptTimeDateBatch();

    // Deactivate and dismiss this subscreen
    getSubScreenArea()->deactivateSubScreen();

	// Generate fake Wall Clock timer event for object that displays the
	// date/time so that the date/time display is updated.
	UserAnnunciationMsg timerMsg;
	timerMsg.timerParts.eventType = UserAnnunciationMsg::TIMER_EVENT;
	timerMsg.timerParts.timerId = GuiTimerId::WALL_CLOCK;
	GuiTimerRegistrar::TimerChangeHappened(timerMsg);	// $[TI1]


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the Subscreen Setting Change timer runs out. 
// Passed the id of the timer.
//---------------------------------------------------------------------
//@ Implementation-Description
// There is a possibility that the Subscreen Setting Change timeout occurs
// after this subscreen has been removed from the display.  In this case we
// ignore the event.
//
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the Subscreen Setting Change Timer Id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DateTimeSettingsSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("DateTimeSettingsSubScreen::timerEventHappened("
								"GuiTimerId::GuiTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId == GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT);

	if ( isVisible() )
	{													// $[TI1.1]
		// reactivate this screen
		deactivate();
		activate();
	}													// $[TI1.2]			
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateHappened_
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Starts the subscreen setting change timeout timer.  Set the size and
// locations of the setting buttons in this subscreen.  Tells the Settings-
//  Validation subsystem to begin adjusting settings.  Calls the activate() method
// of each setting button in order to sync their displays with the current
// setting values.
// If we're in Settings lockout mode then we need to display special prompts
// and flatten all setting buttons.  Otherwise, prompt the user for what to 
// do next, bounces up the buttons.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DateTimeSettingsSubScreen::activateHappened_(void)
{
	CALL_TRACE("activateHappened_()");

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{	// Service Mode									// $[TI1.1]
		setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
		setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);
		hourButton_.setY((BUTTON_Y_SM_ + BUTTON_HEIGHT_ + BUTTON_GAP_));
		minuteButton_.setY((BUTTON_Y_SM_ + BUTTON_HEIGHT_ + BUTTON_GAP_));
		dayButton_.setY((BUTTON_Y_SM_ + BUTTON_HEIGHT_ + BUTTON_GAP_));
		monthButton_.setY(BUTTON_Y_SM_ + BUTTON_HEIGHT_ + BUTTON_GAP_);
		yearButton_.setY((BUTTON_Y_SM_ + BUTTON_HEIGHT_ + BUTTON_GAP_));
        dateFormatButton_.setY(BUTTON_Y_SM_ );
	}
	else
	{													// $[TI1.2]
		setWidth(SUB_SCREEN_AREA_WIDTH);
		setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);
 		hourButton_.setY((BUTTON_Y_+ BUTTON_HEIGHT_ + BUTTON_GAP_));
		minuteButton_.setY((BUTTON_Y_+ BUTTON_HEIGHT_ + BUTTON_GAP_));
		dayButton_.setY((BUTTON_Y_+ BUTTON_HEIGHT_ + BUTTON_GAP_));
		monthButton_.setY((BUTTON_Y_+ BUTTON_HEIGHT_ + BUTTON_GAP_));
		yearButton_.setY((BUTTON_Y_+ BUTTON_HEIGHT_ + BUTTON_GAP_));
        dateFormatButton_.setY((BUTTON_Y_));
	}

	// Begin adjusting settings
	SettingContextHandle::AdjustTimeDateBatch();

    // Must set a change flag state to DATE_FORMAT setting button because
	// all of the buttons in this subscreen are already in a change state.
    SettingsMgr::GetSettingPtr(SettingId::DATE_FORMAT)->setForcedChangeFlag();

	// Activate setting buttons
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::activate);

	// If we're in NO VENTILATION or VENT INOP then we need to display 
	// special prompts and flatten all setting buttons.
	if ((UpperScreen::RUpperScreen.getVitalPatientDataArea()->
		 getDisplayMode() == VitalPatientDataArea::MODE_NO_VENT) ||
		(UpperScreen::RUpperScreen.getVitalPatientDataArea()->
		 getDisplayMode() == VitalPatientDataArea::MODE_INOP_NO_VENT))
	{													// $[TI2.1]
		// Set Primary prompt to "View settings."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, PromptStrs::VIEW_SETTINGS_P);
		// Set Advisory prompt to "Settings have been locked out."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_LOW, PromptStrs::SETTINGS_LOCKED_OUT_A);
		// Set Secondary prompt to tell user to press OTHER SCREENS button
		// to cancel
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);

		// Flatten all setting buttons
		operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::setToFlat);
	}
	else
	{													// $[TI2.2]
		// Set Primary prompt to "Adjust settings."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_LOW, PromptStrs::ADJUST_SETTINGS_P);

		// Set the Secondary prompt
		if (GuiApp::GetGuiState() == STATE_SERVICE)
		{	// Service Mode								// $[TI3.1]
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, PromptStrs::DATE_TIME_PROCEED_CANCEL_S);
		}
		else
		{												// $[TI3.2]
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_PROCEED_CANCEL_S);
		}

		// Unflatten all setting buttons
		operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::setToUp);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateHappened_
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed and clears the Adjust Panel
// focus in case we had it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DateTimeSettingsSubScreen::deactivateHappened_(void)
{
	CALL_TRACE("deactivateHappened_(void)");

	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::deactivate);
}			// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdateHappened_
//
//@ Interface-Description
//  Called when a setting has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DateTimeSettingsSubScreen::valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_,
					  const Notification::ChangeQualifier,
					  const ContextSubject*
											   )
{
	CALL_TRACE("valueUpdateHappened_(transitionId, qualifierId, pSubject)");

	// do nothing...
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
DateTimeSettingsSubScreen::SoftFault(const SoftFaultID  softFaultID,
								     const Uint32       lineNumber,
								     const char*        pFileName,
								     const char*        pPredicate)  
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
            DATETIMESETTINGSSUBSCREEN, lineNumber, pFileName, pPredicate);
}
