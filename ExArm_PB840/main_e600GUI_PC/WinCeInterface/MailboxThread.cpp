#include "stdafx.h"
//#############################################################################
//
//  MailboxThread.cpp - Implementation of the MailboxThread_t class.
//
//#############################################################################

#include "MailboxThread.h"


//#############################################################################
//
//  MailboxThread_t::MailboxThread_t - MailboxThread_t constructor
//
//#############################################################################

MailboxThread_t::MailboxThread_t(uint16_t MaxMailEntrySize, uint16_t Entries, LPCWSTR Name, bool DoubleQueue)
{
    MSGQUEUEOPTIONS MessageQueueOptions;


    // Create the message queue and get the read handle
    MessageQueueOptions.dwSize = sizeof(MessageQueueOptions);
//    MessageQueueOptions.dwSize = Entries * MaxMailEntrySize;
    MessageQueueOptions.dwFlags = 0;
    MessageQueueOptions.dwMaxMessages = Entries;
    MessageQueueOptions.cbMaxMessage = MaxMailEntrySize;
    MessageQueueOptions.bReadAccess = true;
	UseDoubleQueue = DoubleQueue;

    ReadHandle[0] = CreateMsgQueue(Name, &MessageQueueOptions);
	 if (!ReadHandle[0])
    {
        ErrVal = GetLastError();
        DEBUG_ASSERT(false);
    }
	if(DoubleQueue)
	{
		CString str;
		str.Format(_T("%s_queue2"), Name);
		ReadHandle[1] = CreateMsgQueue(str, &MessageQueueOptions);
		if (!ReadHandle[1])
		{
			ErrVal = GetLastError();
			RETAILMSG(TRUE,(_T("######  MailboxThread ReadHandle 2 Creation Failed: %s , Entries = %d, Size = %d ***\r\n"), Name,Entries, MaxMailEntrySize));
			DEBUG_ASSERT(false);
		}
	}
	else 
		ReadHandle[1] = NULL;

    // Set up to get a write handle
    MessageQueueOptions.bReadAccess = false;
    WriteHandle[0] = OpenMsgQueue(OpenProcess(0, FALSE, GetCurrentProcessId()), ReadHandle[0], &MessageQueueOptions);
    DEBUG_ASSERT(WriteHandle);
    if (!WriteHandle[0])
    {
        ErrVal = GetLastError();
        DEBUG_ASSERT(false);
    }

	if(DoubleQueue)
	{
		WriteHandle[1] = OpenMsgQueue(OpenProcess(0, FALSE, GetCurrentProcessId()), ReadHandle[1], &MessageQueueOptions);
		 if(!WriteHandle[1])
		{
			ErrVal = GetLastError();
			DEBUG_ASSERT(false);
		}
	}
	
	else 
		WriteHandle[1] = NULL;

    MaxEntrySize = MaxMailEntrySize;
}


//#############################################################################
//
//  MailboxThread_t::~MailboxThread_t - MailboxThread_t destructor
//
//#############################################################################

MailboxThread_t::~MailboxThread_t()
{
    CloseMsgQueue(WriteHandle[0]);
    CloseMsgQueue(ReadHandle[0]);
	if(UseDoubleQueue)
	{
		CloseMsgQueue(WriteHandle[1]);
		CloseMsgQueue(ReadHandle[1]);
	}
}


//#############################################################################
//
//  MailboxThread_t::SendMail - Used by threads to send a message to this task.
//
//      Timeout not implemented at this time
//
//#############################################################################

bool MailboxThread_t::SendMail(uint8_t *MessagePtr, uint16_t MessageSize, int32_t Timeout, short queue)
{
    DEBUG_ASSERT(queue<=1);
	DEBUG_ASSERT(WriteHandle[queue]);
	
    bool EntryFree = false;

    if (WriteMsgQueue(WriteHandle[queue], MessagePtr, MessageSize, Timeout, 0))
    {
        return true;
    }
    else
    {
        ErrVal = GetLastError();
        return false;
    }
}


//#############################################################################
//
//  MailboxThread_t::GetMail -  Used by this task to retrieve messages from the
//                           mailbox.
//
//#############################################################################

bool MailboxThread_t::GetMail(uint8_t *MessagePtr, uint32_t Timeout, short queue)
{
    uint32_t BytesRead;
    uint32_t Flags;
	bool	 rtn;
	
	rtn = false;
	try
		{
		if (ReadMsgQueue(ReadHandle[queue], MessagePtr,  MaxEntrySize, &BytesRead, Timeout, &Flags))
			{
			rtn = true;
			}
		else
			{
			ErrVal = GetLastError();
			if (ErrVal != ERROR_TIMEOUT)
				{
				ErrVal = ERROR_TIMEOUT;
				}
			rtn = false;
			}
		}
	catch(...)
		{
		rtn = false;
		}
	return rtn;
}


//#############################################################################
//
//  MailboxThread_t::GetMail -  Used by this task to retrieve messages from the
//                           mailbox.
//
//#############################################################################

bool MailboxThread_t::GetMail(uint8_t *MessagePtr, uint32_t *BytesRead, uint32_t Timeout, short queue)
{
    uint32_t Flags;

    if (ReadMsgQueue(ReadHandle[queue], MessagePtr,  MaxEntrySize, BytesRead, Timeout, &Flags))
    {
        return true;
    }
    else
    {
        ErrVal = GetLastError();
if (ErrVal != ERROR_TIMEOUT)
{
    ErrVal = ERROR_TIMEOUT;
}
        return false;
    }
}
void MailboxThread_t::GetQueueInformation(LPMSGQUEUEINFO lpInfo, short queue)
{
	DEBUG_ASSERT(WriteHandle[queue]);
	
	GetMsgQueueInfo(WriteHandle[queue], lpInfo);
				
}
uint32_t MailboxThread_t::NumberofMailsInTheQueue(short queue)
{
	MSGQUEUEINFO queueInfo;
	queueInfo.dwSize = sizeof(MSGQUEUEINFO);
	LPMSGQUEUEINFO lpInfo = &queueInfo;
	GetQueueInformation(lpInfo, queue);

	return queueInfo.dwCurrentMessages;
}

