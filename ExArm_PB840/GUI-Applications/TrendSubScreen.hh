#ifndef TrendSubScreen_HH
#define TrendSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendSubScreen -  Trend sub-screen base class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: gdc      Date:  11-Aug-2008   SCR Number: 6439
//  Project:  840S
//  Description:
//  Removed touch screen "drill-down" work-around.
//
//  Revision: 001  By: gdc      Date:  15-Mar-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  Initial Version
//=====================================================================
#include "GuiApp.hh"
#include "SubScreen.hh"
#include "SettingObserver.hh"                                                            
#include "PrintTarget.hh"
#include "AdjustPanelTarget.hh"

#include "ScrollableEventLog.hh"
#include "DiscreteSettingButton.hh"
#include "EventDetailItems.hh"
//@ Usage-Classes

class SubScreenArea;
class TextButton;
class TextField;
class TrendDataSetAgent;
class DropDownSettingButton;
class FocusButton;

//@ End-Usage

class TrendSubScreen 
:	public GuiTimerTarget,
	public SubScreen, 
	public SettingObserver, 
	public PrintTarget
{
public:
	enum Status
	{
		REQUEST_ACCEPTED = 0,
		NOT_ACTIVE,
		BUSY
	};

	// public static interface
	static TrendSubScreen::Status RequestTrendData(void);

	TrendSubScreen::Status requestTrendData(void);

	// pure virtuals
	virtual void updateCursorPosition(void) = 0;
	virtual void disableScrolling(void) = 0;
	virtual void update(void) = 0;

	// SettingObserver virtual method
	virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
							  const SettingSubject*               pSubject);

	// SubScreen virtuals
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtuals
	virtual void buttonDownHappened(Button *pButton,
									Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
								  Boolean byOperatorAction);

	// PrintTarget virtuals
	virtual void cancelPrintRequest(void);
	virtual void updatePrintButton(void);
	virtual void transitionCaptureToPrint(void);
	virtual void printDone(void);

	// GuiTimerTarget virtual
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	TrendSubScreen(SubScreenArea* pSubScreenArea, 
				   TextButton& screenNavButton,
				   TextButton& printButton,
				   TextField& printingText,
				   FocusButton& eventDetailButton,
				   DropDownSettingButton& presetsButton);

	virtual ~TrendSubScreen(void);

	// protected pure virtuals
	virtual Boolean isAnyButtonPressed_(void) const = 0;
	virtual void requestTrendData_(void) = 0;

	void displayGraphs_(void);
	void displayTable_(void);

	//@ Data-Member: PresetsTypeInfo_;
	// A pointer to the ValueInfo structure defining the PRESET menu items
	static DiscreteSettingButton::ValueInfo*  PresetsTypeInfo_;

	//@ Data-Member: rTrendDataSetAgent
	// Reference to the TrendDataBaseAgent through which requests to the
	// Trend Data Base are mediated.
	TrendDataSetAgent& rTrendDataSetAgent_;

	//@ Data-Member: eventDetailLog_
	// The trend event log containing the event IDs and descriptions
	ScrollableEventLog eventDetailLog_;

	//@ Data-Member: eventMenuItems_
	// Contains the strings to display in the eventDetailLog
	EventDetailItems eventMenuItems_;

	//@ Data-Member: isPresetChanging_
	// TRUE when the event button is pressed down
	Boolean isEventButtonDown_;

	void updateEventDetailWindow_(void);

	//@ Data-Member: isSettingChanged_
	// TRUE when any trend setting has changed that requires a dataset refresh
	Boolean isSettingChanged_;

private:
	// these methods are purposely declared, but not implemented...
	TrendSubScreen(void);						 // not implemented
	TrendSubScreen(const TrendSubScreen&);		 // not implemented...
	void operator=(const TrendSubScreen&);		 // not implemented...

	//@ Data-Member: PCurrentTrendSubScreen_
	// A pointer to the currently active TrendSubScreen 
	static TrendSubScreen* PCurrentTrendSubScreen_;

	//@ Data-Member: rScreenNavButton_
	// A reference to the navigation button to switch between the graph and
	// table sub-screens
	TextButton& rScreenNavButton_;

	//@ Data-Member: rPrintButton_
	// A reference to the print button that prints the screen
	TextButton& rPrintButton_;

	//@ Data-Member: rPrintingText_;
	// A reference to the text that flashes the message 'PRINTING' 
	TextField& rPrintingText_;

	//@ Data-Member: rEventDetailButton_
	// A reference to the event detail button that pops up the event detail window
	FocusButton& rEventDetailButton_;

	//@ Data-Member: rPresetsButton_
	// A reference to the presets dropdown button
	DropDownSettingButton& rPresetsButton_;

	//@ Data-Member: isPresetChanging_
	// TRUE when the preset setting value is changing. Used to gate trend
	// parameter callbacks when the preset changes them.
	Boolean isPresetChanging_;

	//@ Data-Member: numEvents_
	// The number of trend events in the trend dataset at the current cursor
	// location.
	Uint32 numEvents_;

	//@ Data-Member: savedSetting1_
	// Value of TREND_SELECT_1 setting when preset button is pressed.
	// Setting reverts to this value if Preset "NONE" or "- -" is selected.
	DiscreteValue savedSetting1_;

	//@ Data-Member: savedSetting2_
	// Value of TREND_SELECT_2 setting when preset button is pressed.
	// Setting reverts to this value if Preset "NONE" or "- -" is selected.
	DiscreteValue savedSetting2_;

	//@ Data-Member: savedSetting3_
	// Value of TREND_SELECT_3 setting when preset button is pressed.
	// Setting reverts to this value if Preset "NONE" or "- -" is selected.
	DiscreteValue savedSetting3_;

	//@ Data-Member: savedSetting4_
	// Value of TREND_SELECT_4 setting when preset button is pressed.
	// Setting reverts to this value if Preset "NONE" or "- -" is selected.
	DiscreteValue savedSetting4_;

	//@ Data-Member: savedSetting5_
	// Value of TREND_SELECT_5 setting when preset button is pressed.
	// Setting reverts to this value if Preset "NONE" or "- -" is selected.
	DiscreteValue savedSetting5_;

	//@ Data-Member: savedSetting6_
	// Value of TREND_SELECT_6 setting when preset button is pressed.
	// Setting reverts to this value if Preset "NONE" or "- -" is selected.
	DiscreteValue savedSetting6_;           

};


#endif // TrendSubScreen_HH 
