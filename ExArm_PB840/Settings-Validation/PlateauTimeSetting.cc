#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PlateauTimeSetting - Plateau Time Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the amount of time (in
//  milliseconds) an inspiration is held in the patient's airway after an
//  inspiratory flow has ceased.  This setting is only active during
//  volume-based ventilation.  This class inherits from 'BoundedSetting'
//  and provides the specific information needed for representation of
//  plateau time.  This information includes the interval and
//  range of this setting's values, this setting's response to a Main
//  Control Setting's transition (see 'acceptTransition()'), this batch
//  setting's new-patient value (see 'getNewPatientValue()'), and this
//  setting's dependent settings.
//
//  This class provides a static method for calculating plateau time
//  based on the value of other settings.  This calculation method
//  provides a standard way for all settings (as needed) to calculate an 
//  plateau time value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines a 'calcDependentValues_()' method for updating this
//  setting's dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PlateauTimeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 007   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 006  By: sah     Date:  18-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added support for new 'VC+' mandatory type in 'getApplicability()'
//      *  corrected SRS mappings
//      *  eliminated unneeded assertions
//
//  Revision: 005   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  re-wrote 'calcBasedOnSetting_()' to be more readable
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 003   By: dosman Date:  16-Sep-1998    DR Number: BiLevel 144
//  Project:  BILEVEL
//  Description:
//      The intermediate value of inspiratory time computed in
//      calcBasedOnSetting_ was changed to be warped in the same
//      direction as the direction the "setting being calculated".
//      Also, special processing was added in BoundedSetting::calcNewValue()
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PlateauTimeSetting.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "FlowPatternValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// This array is set up dynamically via this class's 'calcDependentValues_()',
// because all dependents are not applicable in 'SPONT' mode...
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
{
	SettingId::INSP_TIME,
	SettingId::IE_RATIO,
	SettingId::EXP_TIME,

	SettingId::NULL_SETTING_ID
};


//  $[02223] -- The setting's range ...
//  $[02225] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	0.0f,		// absolute minimum...
	0.0f,		// unused...
	HUNDREDS,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	2000.0f,		// absolute maximum...
	100.0f,		// resolution...
	HUNDREDS,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PlateauTimeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PlateauTimeSetting::PlateauTimeSetting(void)
: BatchBoundedSetting(SettingId::PLATEAU_TIME,
					  ::ArrDependentSettingIds_,
					  &::INTERVAL_LIST_,
					  PLATEAU_MAX_ID,  // maxConstraintId...
					  PLATEAU_MIN_ID)  // minConstraintId...
{
	CALL_TRACE("PlateauTimeSetting()");
	isATimingSetting_ = TRUE;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PlateauTimeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PlateauTimeSetting::~PlateauTimeSetting(void)
{
	CALL_TRACE("~PlateauTimeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	PlateauTimeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	DiscreteValue  mandTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			mandTypeValue = pMandType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			mandTypeValue = pMandType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicabilityId;

	switch ( mandTypeValue )
	{
		case MandTypeValue::PCV_MAND_TYPE :	  // $[TI3]
			applicabilityId = Applicability::INAPPLICABLE;
			break;
		case MandTypeValue::VCV_MAND_TYPE :	  // $[TI4]
			applicabilityId = Applicability::CHANGEABLE;
			break;
		case MandTypeValue::VCP_MAND_TYPE :	  // $[TI5]
			applicabilityId = Applicability::VIEWABLE;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(mandTypeValue);
			break;
	}

	return(applicabilityId);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	PlateauTimeSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	// $[02224] The setting's new-Patient value ...
	newPatient.value     = 0.0f;
	newPatient.precision = HUNDREDS;

	return(newPatient);	  // $[TI1]
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02022]\b\   -- from-PC-to-VC transition rules
//  $[VC02006]\c\ -- from-VC-to-VC+ transition rules
//  $[VC02006]\f\ -- from-PC-to-VC+ transition rules
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	PlateauTimeSetting::acceptTransition(const SettingId::SettingIdType,
										 const DiscreteValue,
										 const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	//=====================================================================
	// mandatory type transition to VC, or VC+...
	//=====================================================================

	BoundedValue  plateauTime;

	// when transition from PCV to VCV, plateau time is to be set to '0'...
	plateauTime.value     = 0.0f;
	plateauTime.precision = ::INTERVAL_LIST_.precision;

	// activation of Transition Rule MUST be italicized...
	setForcedChangeFlag();

	// store the transition value into the adjusted context...
	setAdjustedValue(plateauTime);
}	// $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	PlateauTimeSetting::isAcceptedValid(void) const
{
	CALL_TRACE("isAcceptedValid()");

	Boolean  isValid = BoundedSetting::isAcceptedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ACCEPTED) !=
			 Applicability::INAPPLICABLE )
		{
			// Is the the accepted plateau time value consistent with the accepted
			// inspiratory time?...
			Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

			isValid = pInspTime->isAcceptedValid();
		}
	}

	return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	PlateauTimeSetting::isAdjustedValid(void)
{
	CALL_TRACE("isAdjustedValid()");

	Boolean  isValid = BoundedSetting::isAdjustedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ADJUSTED) !=
			 Applicability::INAPPLICABLE )
		{
			// Is the the adjusted plateau time value consistent with the adjusted
			// inspiratory time?...
			Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

			isValid = pInspTime->isAdjustedValid();

			if ( !isValid )
			{
				const BoundedSetting*  pRespRate =
					SettingsMgr::GetBoundedSettingPtr(SettingId::RESP_RATE);

				cout << "INVALID PlateauTime:\n";
				cout << *this << *pInspTime << *pRespRate << endl;
			}
		}
	}

	return(isValid);
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	PlateauTimeSetting::SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							PLATEAU_TIME_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02227] -- this setting's dependent settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	PlateauTimeSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
	CALL_TRACE("calcDependentValues_()");

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

	const DiscreteValue  MODE_VALUE = pMode->getAdjustedValue();

	Uint  idx = 0u;

	switch ( MODE_VALUE )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :
		case ModeValue::BILEVEL_MODE_VALUE :	  // $[TI1]
			// all dependents are active here...
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
			break;
		case ModeValue::SPONT_MODE_VALUE :		  // $[TI2]
		case ModeValue::CPAP_MODE_VALUE : 
			// only inspiratory time in 'SPONT' mode...
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
			break;
	}

	// terminate with the null id...
	ArrDependentSettingIds_[idx] = SettingId::NULL_SETTING_ID;

	// forward on to base class...
	return(Setting::calcDependentValues_(isValueIncreasing));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All new-patient calculations are to be based on the VCV setting values.
//
//  $[02019] -- plateau time's dependent formula relationship...
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//  (basedOnSettingId == SettingId::EXP_TIME  ||
//   basedOnSettingId == SettingId::IE_RATIO  ||
//   basedOnSettingId == SettingId::INSP_TIME)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
	PlateauTimeSetting::calcBasedOnSetting_(
										   const SettingId::SettingIdType basedOnSettingId,
										   const BoundedRange::WarpDir    warpDirection,
										   const BasedOnCategory          basedOnCategory
										   ) const
{
	CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

	AUX_CLASS_PRE_CONDITION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
							basedOnCategory);

	BoundedSetting*  pInspTime =
		SettingsMgr::GetBoundedSettingPtr(SettingId::INSP_TIME);

	BoundedValue  totalInspTime;

	// determine a value for the total inspiratory time (time of inspiration plus
	// plateau time), based on which timing parameter is forcing this call...
	switch ( basedOnSettingId )
	{
		case SettingId::EXP_TIME :
			{	// $[TI1] -- calculate an insp time from exp time...
				const Setting*  pExpTime =
					SettingsMgr::GetSettingPtr(SettingId::EXP_TIME);

				const Real32  EXP_TIME_VALUE =
					BoundedValue(pExpTime->getAdjustedValue()).value;
				const Real32  BREATH_PERIOD_VALUE =
					RespRateSetting::GetBreathPeriod(basedOnCategory);

				totalInspTime.value = (BREATH_PERIOD_VALUE - EXP_TIME_VALUE);
			}
			break;
		case SettingId::IE_RATIO :
			{	// $[TI2] -- calculate an insp time from I:E ratio...
				const Setting*  pIeRatio =
					SettingsMgr::GetSettingPtr(SettingId::IE_RATIO);

				const Real32  IE_RATIO_VALUE =
					BoundedValue(pIeRatio->getAdjustedValue()).value;
				const Real32  BREATH_PERIOD_VALUE =
					RespRateSetting::GetBreathPeriod(basedOnCategory);

				Real32  eRatioValue;
				Real32  iRatioValue;

				if ( IE_RATIO_VALUE < 0.0f )
				{	// $[TI2.1]
					// The I:E ratio value is negative, therefore we have 1:xx with
					// "xx" the expiratory portion of the ratio...
					iRatioValue = 1.0f;
					eRatioValue = -(IE_RATIO_VALUE);
				}
				else
				{	// $[TI2.2]
					// The I:E ratio value is positive, therefore we have xx:1 with
					// "xx" the inspiratory portion of the ratio...
					iRatioValue = IE_RATIO_VALUE;
					eRatioValue = 1.0f;
				}

				// calculate the insp time from the I:E ratio...
				totalInspTime.value = (iRatioValue * BREATH_PERIOD_VALUE) /
									  (iRatioValue + eRatioValue);

				// because inspiratory time has a "coarse" resolution in VCV,
				// the "raw" insp time value must be rounded to a resolution boundary,
				// before using the value in the calculations, below...
				pInspTime->warpToRange(totalInspTime, warpDirection, FALSE);
			}
			break;
		case SettingId::INSP_TIME :
			{	// $[TI3] -- get the "adjusted" insp time value...
				const Setting*  pInspTime =
					SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

				totalInspTime.value = BoundedValue(pInspTime->getAdjustedValue()).value;
			}
			break;
		default :
			// unexpected 'basedOnSettingId' value...
			AUX_CLASS_ASSERTION_FAILURE(basedOnSettingId);
			break;
	};

	// get pointers to each of the VCV parameters...
	const Setting*  pFlowPattern =
		SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
	const Setting*  pPeakInspFlow =
		SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW);
	const Setting*  pTidalVolume =
		SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

	const DiscreteValue  FLOW_PATTERN_VALUE = pFlowPattern->getAdjustedValue();

	const Real32  PEAK_INSP_FLOW_VALUE =
		BoundedValue(pPeakInspFlow->getAdjustedValue()).value;
	const Real32  TIDAL_VOLUME_VALUE   =
		BoundedValue(pTidalVolume->getAdjustedValue()).value;

	BoundedValue  partialInspTime;

	// ...now, calculate the "raw" inspiratory time (inspiratory time without
	// plateau time)...
	switch ( FLOW_PATTERN_VALUE )
	{
		case FlowPatternValue::SQUARE_FLOW_PATTERN :  // $[TI4] 
			{
				static const Real32  FACTOR_ = (60000.0f * 0.001f);

				partialInspTime.value = ((FACTOR_ * TIDAL_VOLUME_VALUE) /
										 PEAK_INSP_FLOW_VALUE);
			}
			break;

		case FlowPatternValue::RAMP_FLOW_PATTERN :	  // $[TI5]
			{
				static const Real32  FACTOR_ = (60000.0f * 0.001f * 2.0f);

				const Setting*  pMinInspFlow =
					SettingsMgr::GetSettingPtr(SettingId::MIN_INSP_FLOW);

				const Real32  MIN_INSP_FLOW_VALUE =
					BoundedValue(pMinInspFlow->getAdjustedValue()).value;

				partialInspTime.value = ((FACTOR_ * TIDAL_VOLUME_VALUE) /
										 (PEAK_INSP_FLOW_VALUE + MIN_INSP_FLOW_VALUE));
			}
			break;

		default :
			AUX_CLASS_ASSERTION_FAILURE(FLOW_PATTERN_VALUE);
			break;
	};

	BoundedValue  plateauTime;

	// ...finally, calculate the final plateau time using these values...
	if ( totalInspTime.value > partialInspTime.value )
	{	// $[TI6] -- return the difference...
		// because inspiratory time has a "coarse" resolution in VCV,
		// the "partial" insp time value must be rounded to a resolution boundary,
		// before using the value in the calculations, below...
		pInspTime->warpToRange(partialInspTime, warpDirection, FALSE);

		plateauTime.value = (totalInspTime.value - partialInspTime.value);
	}
	else
	{	// $[TI7] -- return a value of '0.0' for plateau time...
		plateauTime.value = 0.0f;
	}

	// place plateau time on a "click" boundary...
	getBoundedRange().warpValue(plateauTime, warpDirection, FALSE);

	return(plateauTime);
}
