#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  IdealBodyWeightSetting - Ideal Body Weight Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the ideal body weight
//  (in kilograms) of the patient.  This is used to incur various bounds
//  and values on other settings.  This class inherits from 'BoundedSetting'
//  and provides the specific information needed for representation of
//  ideal body weight.  This information includes the interval and range of
//  this setting's values, and this batch setting's new-patient value (see
//  'getNewPatientValue()').
//
//  This setting is unique in that it is only allowed to be changed in the
//  first phase on New-Patient Setup.  Many settings base their bounds and/or
//  new-patient value on the value of this setting.  To dramatically reduce
//  the complexity with the changing of ideal body weight, it is only allowed
//  to be changed at a point where all other settings will be initialized
//  AFTER the IBW change is accepted -- that is, only at the beginning of
//  New-Patient Setup.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, and NO dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/IdealBodyWeightSetting.ccv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Lower min value based on NeoMode Advanced options now.
//
//  Revision: 009   By: mnr    Date: 21-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Lower min value based on NeoMode Option status.
//
//  Revision: 008  By: mnr     Date: 24-Jun-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//     Lowered the soft-bound on IBW range to 0.3f.
// 
//  Revision: 007   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 006   By: sah    Date: 09-Feb-2000    DR Number: 5635
//  Project:  NeoMode
//  Description:
//      Eliminated IBW NEONATAL soft-bound.  Changed from using
//      defined constants to using 'getAbsolute{Min,Max}Value_()'
//      in 'findSoftMinMaxValue_()' method.
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  incorporated new circuit-based soft-limits, new-patient values
//         and dynamic ranges
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "IdealBodyWeightSetting.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "PatientCctTypeValue.hh"
#include "SupportTypeValue.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "AdjustedContext.hh"
#include "ContextMgr.hh"
#include "SettingsMgr.hh"
#include "SettingId.hh"
#include "DiscreteSetting.hh"
#include "SoftwareOptions.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02157] The setting's range ...
// $[02160] The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	0.3f,		// absolute minimum...
	0.0f,		// unused...
	TENTHS,		// unused...
	NULL
};
static const BoundedInterval  NODE4_ =
{
	3.5f,		// change-point...
	0.1f,		// resolution...
	TENTHS,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE3_ =
{
	10.0f,		// change-point...
	0.5f,		// resolution...
	TENTHS,		// precision...
	&::NODE4_
};
static const BoundedInterval  NODE2_ =
{
	50.0f,		// change-point...
	1.0f,		// resolution...
	ONES,		// precision...
	&::NODE3_
};
static const BoundedInterval  NODE1_ =
{
	100.0f,		// change-point...
	5.0f,		// resolution...
	ONES,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	150.0f,		// absolute maximum...
	10.0f,		// resolution...
	TENS,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: IdealBodyWeightSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

IdealBodyWeightSetting::IdealBodyWeightSetting(void)
: BatchBoundedSetting(SettingId::IBW,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  IBW_MAX_BASED_CCT_TYPE_ID, // maxConstraintId...
					  IBW_MIN_BASED_CCT_TYPE_ID) // minConstraintId...
{
	CALL_TRACE("IdealBodyWeightSetting()");

	// register this setting's soft bounds...
	registerSoftBound_(::IBW_SOFT_MIN_BASED_CCT_TYPE_ID, Setting::LOWER);
	registerSoftBound_(::IBW_SOFT_MAX_BASED_CCT_TYPE_ID, Setting::UPPER);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~IdealBodyWeightSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

IdealBodyWeightSetting::~IdealBodyWeightSetting(void)
{
	CALL_TRACE("~IdealBodyWeightSetting()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isChanged()  [const, virtual]
//
//@ Interface-Description
//  Returns TRUE if the Ideal Body Weight setting value has changed. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is used for IBW instead of the base class method 
//  BatchBoundedSetting::isChanged to avoid checking the IBW's 
//  applicability using getApplicability() that is dependent on the 
//  isChanged() value of the main vent settings which in turn are 
//  dependent on IBW's isChanged() value. This method avoids this 
//  circular dependency.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean IdealBodyWeightSetting::isChanged(void) const
{
	Boolean  isChanged = TRUE;

	if ( !getForcedChangeFlag_() )
	{
		// the forced-change flag is NOT set, therefore the "change" status
		// is based on whether the accepted and adjusted values are different...
		const BoundedValue  ACCEPTED_VALUE = getAcceptedValue();
		const BoundedValue  ADJUSTED_VALUE = getAdjustedValue();

		// compare using the finer resolution...
		const Precision COMPARE_PRECISION =
			MIN_VALUE(ACCEPTED_VALUE.precision, ADJUSTED_VALUE.precision);

		isChanged = !::IsEquivalent(ACCEPTED_VALUE.value,
									ADJUSTED_VALUE.value, COMPARE_PRECISION);

	}

	return(isChanged);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is normally changeable. It is only viewable after new
//  patient setup is complete and one of the vent modes are changing.
//  $[LC01011] ...prevent adjustment of IBW when user modifies main control settings
//  $[LC02008]\b\ changes to main settings shall prevent changes to IBW
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id IdealBodyWeightSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id applicabilityId = Applicability::CHANGEABLE;

	if ( qualifierId == Notification::ADJUSTED && 
		 ContextMgr::GetAdjustedContext().getAllSettingValuesKnown() &&
		 (SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->isChanged() ||
		  SettingsMgr::GetSettingPtr(SettingId::MODE)->isChanged() ||
		  SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE)->isChanged() ||
		  SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE)->isChanged()) )
	{
		applicabilityId = Applicability::VIEWABLE;
	}

	return applicabilityId;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02158] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue IdealBodyWeightSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	BoundedValue  newPatient;

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			newPatient.value     = 3.0f;
			newPatient.precision = TENTHS;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			newPatient.value     = 15.0f;
			newPatient.precision = ONES;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			newPatient.value     = 50.0f;
			newPatient.precision = ONES;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(newPatient);
}

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to vent setup changes by updating IBW applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void IdealBodyWeightSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
										 const SettingSubject*               pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	if ( qualifierId == Notification::ADJUSTED )
	{  // -- process vent mode changes
		const SettingId::SettingIdType SETTING_ID = pSubject->getId();

		switch ( SETTING_ID )
		{
			case SettingId::VENT_TYPE:
			case SettingId::SUPPORT_TYPE:
			case SettingId::MAND_TYPE:
			case SettingId::MODE:
				// updateApplicability required below
				break;
			default:
				AUX_CLASS_ASSERTION_FAILURE(SETTING_ID);
				break;
		}
		// update this instance's applicability...
		updateApplicability(qualifierId);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean IdealBodyWeightSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This method establishes the callback for monitoring changes to vent
//  setup parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void IdealBodyWeightSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	attachToSubject_(SettingId::VENT_TYPE, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::SUPPORT_TYPE, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::MAND_TYPE, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::MODE, Notification::VALUE_CHANGED);
}



//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void IdealBodyWeightSetting::SoftFault(const SoftFaultID  softFaultID,
									   const Uint32       lineNumber,
									   const char*        pFileName,
									   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							IBW_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02157]   -- setting's range...
//  $[NE02004] -- setting's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	IdealBodyWeightSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	// get the current "soft" min/max values...
	findSoftMinMaxValues_(cctTypeBasedSoftMin_, cctTypeBasedSoftMax_);

	updateMaxConstraint_();
	updateMinConstraint_();

}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NE02004] -- setting's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void IdealBodyWeightSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
												   Real32& rSoftMaxValue) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			// no soft bounds for NEONATAL circuit type, return absolute limits...
			rSoftMinValue = getAbsoluteMinValue_();
			rSoftMaxValue = getAbsoluteMaxValue_();
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			rSoftMinValue = SettingConstants::IBW_PED_CCT_SOFT_MIN_VALUE;
			rSoftMaxValue = SettingConstants::IBW_PED_CCT_SOFT_MAX_VALUE;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			// no upper soft bound for ADULT circuit type, return absolute limit...
			rSoftMinValue = SettingConstants::IBW_ADULT_CCT_SOFT_MIN_VALUE;
			rSoftMaxValue = getAbsoluteMaxValue_();
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute maximum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic maximum constraint.
//
//  $[02157] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32 IdealBodyWeightSetting::getAbsoluteMaxValue_(void) const
{
	CALL_TRACE("getAbsoluteMaxValue_()");

	Real32  absoluteMaxValue;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			absoluteMaxValue = SettingConstants::IBW_NEO_CCT_HARD_MAX_VALUE;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			absoluteMaxValue = SettingConstants::IBW_PED_CCT_HARD_MAX_VALUE;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			absoluteMaxValue = SettingConstants::IBW_ABSOLUTE_MAX_VALUE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMaxValue);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic minimum constraint.
//
//  $[02157] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32 IdealBodyWeightSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	Boolean isNeoModeAdvancedEnabled = 
		SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_ADVANCED);

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			if( isNeoModeAdvancedEnabled )
			{ 
				absoluteMinValue = SettingConstants::IBW_ABSOLUTE_MIN_NEO_VALUE;
			}
			else
			{
				absoluteMinValue = SettingConstants::IBW_ABSOLUTE_MIN_VALUE;
			}
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			absoluteMinValue = SettingConstants::IBW_PED_CCT_HARD_MIN_VALUE;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			absoluteMinValue = SettingConstants::IBW_ADULT_CCT_HARD_MIN_VALUE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMinValue);
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateMaxConstraint_() 
//
//@ Interface-Description
//  Update this setting's maximum constraint to the most restrictive 
//  of its upper bounds.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02157]   -- setting's range...
//  $[NE02004] -- setting's soft range...
//  $[LC02005] The following upper bounds shall apply after the user has set the initial IBW value:
//  \a\ <= VT / 1.16 mL/kg; when VT is applicable
//  \b\ <= Apnea VT / 1.16 mL/kg; when Apnea VT is applicable
//  \c\ <= VT SUPP / 1.16 mL/kg; when VT SUPP is applicable
//  \d\ <= high VTI SPONT limit / 5.0 mL/kg; when VTI SPONT is applicable
//  \e\ <= high VTI MAND  limit / 5.0 mL/kg; when VTI MAND is applicable
//  \f\ <= high VTI limit / 5.0 mL/kg; when VTI is applicable.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void IdealBodyWeightSetting::updateMaxConstraint_(void)
{
	AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();
	Real32 absoluteMaxValue = getAbsoluteMaxValue_();

	const Setting*  pSetting;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
	const Real32  TIDAL_VOLUME = BoundedValue(pSetting->getAdjustedValue()).value;
	Real32 vtBasedMax = 
		(pSetting->getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE) ?
		TIDAL_VOLUME / SettingConstants::MIN_TIDAL_VOL_IBW_SCALE : absoluteMaxValue;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);
	const Real32  APNEA_TIDAL_VOLUME = BoundedValue(pSetting->getAdjustedValue()).value;
	Real32 apneaVtBasedMax = 
		(pSetting->getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE) ?
		APNEA_TIDAL_VOLUME / SettingConstants::MIN_TIDAL_VOL_IBW_SCALE : absoluteMaxValue;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::VOLUME_SUPPORT);
	const Real32  VOLUME_SUPPORT = BoundedValue(pSetting->getAdjustedValue()).value;
	Real32 vsuppBasedMax = 
		(pSetting->getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE) ?
		VOLUME_SUPPORT / SettingConstants::MIN_TIDAL_VOL_IBW_SCALE : absoluteMaxValue;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::HIGH_INSP_TIDAL_VOL);
	const Real32  HIGH_INSP_TIDAL_VOL = BoundedValue(pSetting->getAdjustedValue()).value;
	Real32 highVtiBasedMax = 
		(pSetting->getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE) ?
		HIGH_INSP_TIDAL_VOL / SettingConstants::MIN_HIGH_INSP_TIDAL_VOL_IBW_SCALE : absoluteMaxValue;

	//-------------------------------------------------------------------
	// determine maximum constraint...
	//-------------------------------------------------------------------
	{
		BoundedRange::ConstraintInfo  maxConstraintInfo;

		// the default case - initialize to the hard bound absolute max
		{
			maxConstraintInfo.value  = absoluteMaxValue;
			maxConstraintInfo.id     = IBW_MAX_BASED_CCT_TYPE_ID;
			maxConstraintInfo.isSoft = FALSE;
		}

		if ( cctTypeBasedSoftMax_ < maxConstraintInfo.value 
			 && isSoftBoundActive_(IBW_SOFT_MAX_BASED_CCT_TYPE_ID) )
		{
			maxConstraintInfo.value  = cctTypeBasedSoftMax_;
			maxConstraintInfo.id     = IBW_SOFT_MAX_BASED_CCT_TYPE_ID;
			maxConstraintInfo.isSoft = TRUE;
		}

		// changeable IBW logic follows
		if ( rAdjContext.getAllSettingValuesKnown() )
		{
			if ( vsuppBasedMax < maxConstraintInfo.value )
			{
				maxConstraintInfo.value  = vsuppBasedMax;
				maxConstraintInfo.id     = IBW_MAX_BASED_ON_VOLUME_SUPP_ID;
				maxConstraintInfo.isSoft = FALSE;
			}

			if ( apneaVtBasedMax < maxConstraintInfo.value )
			{
				maxConstraintInfo.value  = apneaVtBasedMax;
				maxConstraintInfo.id     = IBW_MAX_BASED_ON_APNEA_TIDAL_VOL_ID;
				maxConstraintInfo.isSoft = FALSE;
			}

			if ( vtBasedMax < maxConstraintInfo.value )
			{
				maxConstraintInfo.value  = vtBasedMax;
				maxConstraintInfo.id     = IBW_MAX_BASED_ON_TIDAL_VOL_ID;
				maxConstraintInfo.isSoft = FALSE;
			}

			if ( highVtiBasedMax < maxConstraintInfo.value )
			{
				maxConstraintInfo.value  = highVtiBasedMax;
				maxConstraintInfo.isSoft = FALSE;

				pSetting = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);
				const DiscreteValue  MAND_TYPE_VALUE = pSetting->getAdjustedValue();

				pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);
				const DiscreteValue  MODE_VALUE = pSetting->getAdjustedValue();

				pSetting = SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);
				const DiscreteValue  SUPPORT_TYPE_VALUE = pSetting->getAdjustedValue();

				Boolean isVtiSpontType   =
					((MODE_VALUE != ModeValue::AC_MODE_VALUE)  && 
					 ((SUPPORT_TYPE_VALUE == SupportTypeValue::VSV_SUPPORT_TYPE) ||
					  (SUPPORT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE) ||
					  (SUPPORT_TYPE_VALUE == SupportTypeValue::ATC_SUPPORT_TYPE)) );

				Boolean isVtiMandType    = ( MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE );

				if ( isVtiMandType )
				{
					maxConstraintInfo.id = 
						isVtiSpontType ? 
						IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_ID :
						IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID;
				}
				else
				{
					maxConstraintInfo.id = IBW_MAX_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID;
				}
			}
		}

		// reset the maximum constraint and warp the new maximum constraint
		BoundedValue warpedValue;
		warpedValue.value = maxConstraintInfo.value;
		getBoundedRange_().resetMaxConstraint();
		getBoundedRange_().warpValue(warpedValue, BoundedRange::WARP_DOWN);            
		maxConstraintInfo.value = warpedValue.value;

		// update IBW's maximum constraint info...
		getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
	}
} 

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateMinConstraint_() 
//
//@ Interface-Description
//  Update this setting's minimum constraint to the most restrictive 
//  of its lower bounds.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02157]   -- setting's range...
//  $[NE02004] -- setting's soft range...
//  $[LC02004] The following lower bounds shall apply after the user has set the initial IBW value:
//  \a\ >= VT / 45.7 mL/kg; when VT is applicable
//  \b\  >= Apnea VT / 45.7 mL/kg; when Apnea VT is applicable
//  \c\ >= VT SUPP / 45.7 mL/kg; when VT SUPP is applicable (see 2 below)
//  \d\ >= high VTI SPONT limit / 45.7 mL/kg; when VTI SPONT is applicable
//  \e\ >= high VTI MAND limit / 45.7 mL/kg; when VTI MAND is applicable
//  \f\ >= high VTI limit / 45.7 mL/kg; when VTI is applicable
//  \h\ >= 25.0 kg; when support type = PA  (this only applies to Circuit Type = Adult).
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void IdealBodyWeightSetting::updateMinConstraint_(void)
{
	AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();
	Real32 absoluteMinValue = getAbsoluteMinValue_();

	const Setting*  pSetting;
	Boolean postIncrement;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
	const Real32  TIDAL_VOLUME = BoundedValue(pSetting->getAdjustedValue()).value;
	Real32 vtBasedMin =
		(pSetting->getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE) ?
		TIDAL_VOLUME / SettingConstants::MAX_TIDAL_VOL_IBW_SCALE : absoluteMinValue;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);
	const Real32  APNEA_TIDAL_VOLUME = BoundedValue(pSetting->getAdjustedValue()).value;
	Real32 apneaVtBasedMin =
		(pSetting->getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE) ?
		APNEA_TIDAL_VOLUME / SettingConstants::MAX_TIDAL_VOL_IBW_SCALE : absoluteMinValue;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::VOLUME_SUPPORT);
	const Real32  VOLUME_SUPPORT = BoundedValue(pSetting->getAdjustedValue()).value;
	Real32 vsuppBasedMin = 
		(pSetting->getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE) ?
		VOLUME_SUPPORT / SettingConstants::MAX_TIDAL_VOL_IBW_SCALE : absoluteMinValue;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::HIGH_INSP_TIDAL_VOL);
	const Real32  HIGH_INSP_TIDAL_VOL = BoundedValue(pSetting->getAdjustedValue()).value;
	Real32 highVtiBasedMin = 
		(pSetting->getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE) ?
		HIGH_INSP_TIDAL_VOL / SettingConstants::MAX_TIDAL_VOL_IBW_SCALE : absoluteMinValue;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);
	const DiscreteValue SUPPORT_TYPE_VALUE = pSetting->getAdjustedValue();
	Real32 atcBasedMin = 
		(SUPPORT_TYPE_VALUE == SupportTypeValue::ATC_SUPPORT_TYPE) ?
		SettingConstants::IBW_PED_CCT_SOFT_MIN_VALUE : absoluteMinValue;

	Real32 pavBasedMin = 
		(SUPPORT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE) ?
		SettingConstants::IBW_ADULT_CCT_SOFT_MIN_VALUE : absoluteMinValue;

	//-------------------------------------------------------------------
	// determine minimum constraint...
	//-------------------------------------------------------------------
	{
		BoundedRange::ConstraintInfo  minConstraintInfo;

		// the default case - initialize to the hard bound absolute min
		{
			minConstraintInfo.value  = absoluteMinValue;
			minConstraintInfo.id     = IBW_MIN_BASED_CCT_TYPE_ID;
			minConstraintInfo.isSoft = FALSE;
			postIncrement            = FALSE;
		}

		if ( cctTypeBasedSoftMin_ > minConstraintInfo.value 
			 && isSoftBoundActive_(IBW_SOFT_MIN_BASED_CCT_TYPE_ID) )
		{
			minConstraintInfo.value  = cctTypeBasedSoftMin_;
			minConstraintInfo.id     = IBW_SOFT_MIN_BASED_CCT_TYPE_ID;
			minConstraintInfo.isSoft = TRUE;
			postIncrement            = FALSE;
		}

		// changeable IBW follows
		if ( rAdjContext.getAllSettingValuesKnown() )
		{
			if ( vsuppBasedMin > minConstraintInfo.value )
			{
				minConstraintInfo.value  = vsuppBasedMin;
				minConstraintInfo.id     = IBW_MIN_BASED_ON_VOLUME_SUPP_ID;
				minConstraintInfo.isSoft = FALSE;
				postIncrement            = TRUE;
			}

			if ( apneaVtBasedMin > minConstraintInfo.value )
			{
				minConstraintInfo.value  = apneaVtBasedMin;
				minConstraintInfo.id     = IBW_MIN_BASED_ON_APNEA_TIDAL_VOL_ID;
				minConstraintInfo.isSoft = FALSE;
				postIncrement            = TRUE;
			}

			if ( vtBasedMin > minConstraintInfo.value )
			{
				minConstraintInfo.value  = vtBasedMin;
				minConstraintInfo.id     = IBW_MIN_BASED_ON_TIDAL_VOL_ID;
				minConstraintInfo.isSoft = FALSE;
				postIncrement            = TRUE;
			}

			if ( highVtiBasedMin > minConstraintInfo.value )
			{
				minConstraintInfo.value  = highVtiBasedMin;
				minConstraintInfo.isSoft = FALSE;

				pSetting = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);
				const DiscreteValue  MAND_TYPE_VALUE = pSetting->getAdjustedValue();

				pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);
				const DiscreteValue  MODE_VALUE = pSetting->getAdjustedValue();

				pSetting = SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);
				const DiscreteValue  SUPPORT_TYPE_VALUE = pSetting->getAdjustedValue();

				Boolean isVtiSpontType   =
					((MODE_VALUE != ModeValue::AC_MODE_VALUE)  && 
					 ((SUPPORT_TYPE_VALUE == SupportTypeValue::VSV_SUPPORT_TYPE) ||
					  (SUPPORT_TYPE_VALUE == SupportTypeValue::PAV_SUPPORT_TYPE) ||
					  (SUPPORT_TYPE_VALUE == SupportTypeValue::ATC_SUPPORT_TYPE)) );

				Boolean isVtiMandType    = ( MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE );

				if ( isVtiMandType )
				{
					minConstraintInfo.id = 
						isVtiSpontType ? 
						IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_ID :
						IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_MAND_ID;
				}
				else
				{
					minConstraintInfo.id = IBW_MIN_BASED_ON_HIGH_INSP_TIDAL_VOL_SPONT_ID;
				}
				postIncrement        = TRUE;
			}

			if ( atcBasedMin > minConstraintInfo.value )
			{
				minConstraintInfo.value  = atcBasedMin;
				minConstraintInfo.id     = IBW_MIN_BASED_ON_ATC_ID;
				minConstraintInfo.isSoft = FALSE;
				postIncrement            = FALSE;
			}

			if ( pavBasedMin > minConstraintInfo.value )
			{
				minConstraintInfo.value  = pavBasedMin;
				minConstraintInfo.id     = IBW_MIN_BASED_ON_PAV_ID;
				minConstraintInfo.isSoft = FALSE;
				postIncrement            = FALSE;
			}
		}

		// reset the minimum constraint and warp the new minimum constraint 
		BoundedValue warpedValue;
		BoundStatus  boundStatus(getId());
		warpedValue.value = minConstraintInfo.value;
		getBoundedRange_().resetMinConstraint();
		getBoundedRange_().warpValue(warpedValue, BoundedRange::WARP_DOWN);            
		if ( postIncrement )
		{
			getBoundedRange_().increment(1, warpedValue, boundStatus);
		}
		minConstraintInfo.value = warpedValue.value;

		// update IBW's minimum constraint info...
		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}
}

