#ifndef TraceMacros_HH
#define TraceMacros_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  TraceMacros - Call-Trace Macros - DEVELOPMENT ONLY
//---------------------------------------------------------------------
// Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TraceMacros.hhv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
// Modifications
//
//  Revision: 001  By:  gdc    Date:  16-May-95    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//=====================================================================

// Usage-Class
#ifdef TRACE_ON
#ifndef TRACE_OFF
#    include "Trace.hh"
#endif 
#endif 
// End-Usage


// Macro:  TRACE(pFuncName)
// This macro processes function entry and exit for call-tracing.  The
// argument, 'pFuncName', is a string identification of the routine
// that is being entered.

#ifdef TRACE_ON
#  ifndef TRACE_OFF
#    define CALL_TRACE(pFuncName)	\
	Trace  callTrace(pFuncName, __FILE__, __LINE__);
#  else 
#    define CALL_TRACE(pFuncName)
#  endif
#endif


#endif  // TraceMacros_HH
