#ifndef DampingGainTable_HH
#define DampingGainTable_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: DampingGainTable - Definition of damping gain table.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/DampingGainTable.hhv   25.0.4.0   19 Nov 2013 13:54:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

//=====================================================================
//
// This table contains the damping gain command to the exhalation valve.
// The index corresponds to the pressure level.
//
//
// $[00480]
//=====================================================================

#include "ExhValveTable.hh"

static const Int32 DAMPING_GAIN_TABLE_SIZE = PRESSURE_RANGE + 1 ;

static const Uint16 DampingGainTable[DAMPING_GAIN_TABLE_SIZE] = {
		  26,   37,   46,   53,   59,   65,   70,   75,   79,   84,
		  88,   92,   95,   99,  103,  106,  109,  112,  116,  119,
		 121,  124,  127,  130,  133,  135,  138,  140,  143,  145,
		 148,  150,  152,  155,  157,  159,  161,  164,  166,  168,
		 170,  172,  174,  176,  178,  180,  182,  184,  186,  188,
		 190,  191,  193,  195,  197,  199,  200,  202,  204,  206,
		 207,  209,  211,  212,  214,  216,  217,  219,  221,  222,
		 224,  225,  227,  228,  230,  232,  233,  235,  236,  238,
		 239,  241,  242,  243,  245,  246,  248,  249,  251,  252,
		 253,  255,  256,  258,  259,  260,  262,  263,  264,  266,
		 267,  268,  270,  271,  272,  274 } ; 


#endif // DampingGainTable_HH






