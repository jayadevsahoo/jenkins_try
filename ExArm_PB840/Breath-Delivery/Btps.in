
#ifndef Btps_IN
#define Btps_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: Btps - BTPS compensation
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Btps.inv   25.0.4.0   19 Nov 2013 13:59:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  syw    Date:  15-May-1997    DR Number: DCS 1620
//       Project:  Sigma (R8027)
//       Description:
//       	Moved CalculateBtps to .cc since code added is no longer an inline
//			method.
//
//  Revision: 001  By:  syw    Date:  26-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetExpBtpsCf
//
//@ Interface-Description
//		This method takes no arguments and returns the exhalation BTPS
//      factor used for exhaled volume compensation.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns data member ExpBtpsCf_.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Real32 
Btps::GetExpBtpsCf( void)
{
    CALL_TRACE("Btps::GetExpBtpsCf( void)") ;
	
   	// $[TI1]
    return( ExpBtpsCf_) ;  
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetInspBtpsCf
//
//@ Interface-Description
//		This method takes no arguments and returns the inspiration BTPS
//      factor used for delivered volume compensation.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns data member InspBtpsCf_.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
inline Real32 
Btps::GetInspBtpsCf( void)
{
	CALL_TRACE("Btps::GetInspBtpsCf( void)") ;
	
   	// $[TI1]
    return( InspBtpsCf_) ; 
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#endif // Btps_IN 






