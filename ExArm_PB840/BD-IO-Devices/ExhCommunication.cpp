#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhCommunication_t - Implementation of method to configure 
//		exhalation flow sensor's serial port.
//---------------------------------------------------------------------
//@ Interface-Description
//      
//---------------------------------------------------------------------
//@ Rationale
//      
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      None
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//*********************************************************************
#include "GlobalObjects.h"
#include "ExhCommunication.h"
#include "sdk_ser.h"
#include "Task.hh"

//*****************************************************************************
//
//  ExhCommunication_t::ExhCommunication_t - default constructor
//
//*****************************************************************************
ExhCommunication_t::ExhCommunication_t()
{
	exhSensorBoard = EXH_SENSOR_BOARD_NEW;
	StartSerialCommunication(/*2,           // default COM port is 2
                                              CBR_19200,
                                              8,
                                              NOPARITY,
                                              ONESTOPBIT,
                                              MAXDWORD,
                                              0,
                                              0,
                                              0,
                                              5000*/);
}

//*****************************************************************************
//
//  ExhCommunication_t::~ExhCommunication_t - destructor. 
//  This should only occur when the program exits
//
//*****************************************************************************

ExhCommunication_t::~ExhCommunication_t()
{ 
}

//*****************************************************************************
//
//  ExhCommunication_t::DetectSensorBoard(DWORD BaudRate) 
//  Determine if communication with the sensor board is successful with the specified baudrate
//
//*****************************************************************************
bool ExhCommunication_t::DetectSensorBoard(DWORD baudRate, uint8_t command)
{
	uint8_t Data;
	DWORD numberOfBytes = 0;
	DWORD numberOfBytesRead;
	DWORD numberOfBytesWritten;
	bool sensorBoardDetected = false;
	bool baudrateChangeStatus;
	uint8_t buf[20];

	numberOfBytes = GetNumberofBytesWaitingInsideSerialPortBufferToBeRead();
	if(numberOfBytes > 0)
		SerialCommunication_t::ReadData(buf, numberOfBytes, &numberOfBytesRead, 0);
	
	memset(buf, 0, sizeof(buf));	
	baudrateChangeStatus = SerialCommunication_t::SetCommunicationBaudRate(baudRate);
	
	SerialCommunication_t::WriteData(&command, 1, &numberOfBytes);

	//TODO E600: Crashing if using Task::Delay(); using Sleep() for now
	Sleep(100);
	for(int i= 0; i<100; i++)
	{
		numberOfBytes = SerialCommunication_t::GetNumberofBytesWaitingInsideSerialPortBufferToBeRead();
		if(numberOfBytes > 0)
		{
			SerialCommunication_t::ReadData(buf, numberOfBytes, &numberOfBytesRead, 0);

			if(numberOfBytes == 3)
			{
				sensorBoardDetected = true;
 				break;
			}
		}

		Sleep(10);
	}
	return sensorBoardDetected;
}

//*****************************************************************************
//
//  ExhCommunication_t::RetrieveSerialNumber( ) 
//  Retrieve and save the serial number from the exhalation flow sensor
//
//*****************************************************************************
void ExhCommunication_t::RetrieveSerialNumber()
{
	uint8_t Data;
	DWORD numberOfBytes = 0;
	DWORD numberOfBytesRead;
	DWORD numberOfBytesWritten;
	bool baudrateChangeStatus;
	uint8_t command = 0x80;
	uint8_t buf[4];
	Uint32 serialNumber;

	numberOfBytes = GetNumberofBytesWaitingInsideSerialPortBufferToBeRead();
	if(numberOfBytes > 0)
		ReadData(buf, numberOfBytes, &numberOfBytesRead, 0);
	
	memset(buf, 0, sizeof(buf));	

	WriteData(&command, 1, &numberOfBytes);

	//TODO E600: Crashing if using Task::Delay(); using Sleep() for now
	Sleep(100);
	for(int i= 0; i<3; i++)
	{
		numberOfBytes = GetNumberofBytesWaitingInsideSerialPortBufferToBeRead();
		if(numberOfBytes == 4)
		{
			SerialCommunication_t::ReadData(buf, numberOfBytes, &numberOfBytesRead, 0);
			//TODO E600 currently new exhalation flowsenor does not have any
			//option to check if the recieved serial number is a valid or not.
			serialNumber = buf[0]<<24 | buf[1]<<16 | buf[2]<<8 | buf[3];
			RETAILMSG(1, (L"Exh Flow Sensor Serial No = %10d\r\n", serialNumber));
			break;
		}
		Sleep(10);
	}
}

//*****************************************************************************
//
//  ExhCommunication_t::StartSerialCommunication -  
//
//*****************************************************************************
bool ExhCommunication_t::StartSerialCommunication(BYTE  ComPortNumber,
                                                        DWORD BaudRate,
                                                        BYTE  ByteSize,
                                                        BYTE  Parity,
                                                        BYTE  StopBits,
                                                        DWORD ReadIntervalTimeout,
                                                        DWORD ReadTotalTimeoutMultiplier,
                                                        DWORD ReadTotalTimeoutConstant,
                                                        DWORD WriteTotalTimeoutMultiplier,
                                                        DWORD WriteTotalTimeoutConstant)
{

    DriverDebugMask =
        //MASK_DEBUG_HWTXINTR |
        //MASK_DEBUG_HWXMITCOMCHAR | 
        //MASK_DEBUG_DOTXDATA |                 
        //MASK_DEBUG_EVALUATEEVENTFLAG | 
        //MASK_DEBUG_SER_WRITE |
        //MASK_DEBUG_SER_READ |
        //MASK_DEBUG_HWRXINTR |
        //MASK_DEBUG_HWRXDMAINTR |
        0;
    // Has the serial communication been already established and initialized successfully? 
    if(SerialCommunicationInitializationCompleted && SerialPortHandle != INVALID_HANDLE_VALUE)
    {
        return true;    
    }

    // call base class
    bool SerialCommuicationStartedResult = SerialCommunication_t::StartSerialCommunication(ComPortNumber,             
                                                                                           BaudRate,
                                                                                           ByteSize,
                                                                                           Parity,
                                                                                           StopBits,
                                                                                           ReadIntervalTimeout,
                                                                                           ReadTotalTimeoutMultiplier,
                                                                                           ReadTotalTimeoutConstant,
                                                                                           WriteTotalTimeoutMultiplier,
                                                                                           WriteTotalTimeoutConstant);
	bool bResult = DetectSensorBoard(CBR_115200, 0x03);

	if(!bResult)
	{
		bResult = DetectSensorBoard(CBR_19200, 0x01);
		if(bResult)
		{
			exhSensorBoard = EXH_SENSOR_BOARD_OLD;
			RETAILMSG(1, (L"Old Sensor Board Detected\r\n"));
		}
		else
		{
			exhSensorBoard = EXH_SENSOR_BOARD_NEW;
			RETAILMSG(1, (L"Sensor Board NOT Detected, defaulting to New Board!!!\r\n"));
		}
	}
	else
	{
		exhSensorBoard = EXH_SENSOR_BOARD_NEW;
		RETAILMSG(1, (L"New Sensor Board Detected\r\n"));
		RetrieveSerialNumber();
	}

    if(SerialCommuicationStartedResult)
    {
        return true;
    }
    else
    {
        return false;
    }
}


