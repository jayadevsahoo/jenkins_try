/*****************************************************************************/
/*   Module Name:    panels.h                                           
/*   Purpose:      Exports function names in panels.c                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/
#include "globals.h"
#include "screens.h"

#ifndef _PANELS
#define _PANELS

HPNL MakeSingleTracePanel(HPNL hParentPanel);
void SetSinglePanelConfig(ONE_SINGLETRACE *Config);
void SetSinglePanelParm(HPNL hPanel, int nParm, int nUnits, int nMode, int nGasTypeListItem, double nThreshold1, double nThreshold2, int  nBTBpeakMode, int nXScaleFactor, int nYScaleFactor);
void SetSinglePanelIcon(HPNL hPanel, char *szIcon);
void SetSinglePanelParmSelect(HPNL hPanel, char *szText);
void SetSinglePanelParmLabel(HPNL hPanel, char *szText);
void SetSinglePanelUnits(HPNL hPanel, char *szText);
void SetSinglePanelModeVisible(HPNL hPanel, int nVisible);
void SetSinglePanelMode(HPNL hPanel, char *szText);
void SetSinglePanelOtherVisible(HPNL hPanel, int nVisible);
void SetSinglePanelOther(HPNL hPanel, char *szText);
void SetSinglePanelFilterVisible(HPNL hPanel, int nVisible);
void SetSinglePanelFilter(HPNL hPanel, char *szText);
void SetSinglePanelThresholdVisible(HPNL hPanel, int nVisible);
//void SetSinglePanelThreshold(HPNL hPanel, int nThreshold);
void SetSinglePanelTemperatureVisible(HPNL hPanel, int nVisible);
void SetSinglePanelTemperature(HPNL hPanel, double dTemperature, int nTemprUnits);
void SetSinglePanelThresholdVisible(HPNL hPanel, int nVisible);
void SetSinglePanelThreshold(HPNL hPanel, int nThresholdType, double dThreshold);
void SetSinglePanelPeakMode(HPNL hPanel, int nBTBpeakMode);
void SetSinglePanelCurrent(HPNL hPanel, char *szText);
void SetSinglePanelMax(HPNL hPanel, char *szText);
void SetSinglePanelAvg(HPNL hPanel, char *szText);
void SetSinglePanelMin(HPNL hPanel, char *szText);
void SetSinglePanelChartLabel(HPNL hPanel, char *szText);
void SetSinglePanelChartXScale(HPNL hPanel, int nMinXScale, int nMaxXScale);
void SetSinglePanelChartXPrecision(HPNL hPanel, int nXPrecision);
void SetSinglePanelChartXDivisions(HPNL hPanel, int nXDivisions);
void SetSinglePanelChartXAxisText(HPNL hPanel, char *szText);
void SetSinglePanelChartYScale(HPNL hPanel, double dMinYScale, double dMaxYScale);
void SetSinglePanelChartYPrecision(HPNL hPanel, int nYPrecision);
void SetSinglePanelChartYDivisions(HPNL hPanel, int nYDivisions);
void SetSinglePanelColors(HPNL hPanel, CVI_COLOR colorRGB);
void SetSinglePanelWaterLevel(HPNL hPanel, double dLevel);
void SetSinglePanelMaxWaterLevel(HPNL hPanel, double dLevel);
void SetSinglePanelMinWaterLevel(HPNL hPanel, double dLevel);
int  SetSinglePanelSize(HPNL hPanel, int QtyPanels, int Position);
HPNL MakeDualTracePanel(HPNL hParentPanel);
void SetDualPanelConfig(ONE_SINGLETRACE *Config, int nParmNum);
void SetDualPanelParm(HPNL hPanel, int nParmNum, int nParm, int nUnits, int nMode, int nGasTypeListItem, double nThreshold1, double nThreshold2, int  nBTBpeakMode, int nXScaleFactor, int nYScaleFactor);
void SetDualPanelIcon(HPNL hPanel, int nParmNum, char *szIcon);
void SetDualPanelParmSelect(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelParmLabel(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelUnits(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelModeVisible(HPNL hPanel, int nParmNum, int nVisible);
void SetDualPanelMode(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelPeakMode(HPNL hPanel, int nParmNum, int nBTBpeakMode);
void SetDualPanelCurrent(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelMax(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelAvg(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelMin(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelChartLabel(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelChartXScale(HPNL hPanel, int nParmNum, int nMinXScale, int nMaxXScale);
void SetDualPanelChartXPrecision(HPNL hPanel, int nParmNum, int nXPrecision);
void SetDualPanelChartXDivisions(HPNL hPanel, int nParmNum, int nXDivisions);
void SetDualPanelChartXAxisText(HPNL hPanel, int nParmNum, char *szText);
void SetDualPanelChartYScale(HPNL hPanel, int nParmNum, double dMinYScale, double dMaxYScale);
void SetDualPanelChartYPrecision(HPNL hPanel, int nParmNum, int nYPrecision);
void SetDualPanelChartYDivisions(HPNL hPanel, int nParmNum, int nYDivisions);
void SetDualPanelColors(HPNL hPanel, int nParmNum, CVI_COLOR colorRGB);
int  SetDualPanelSize(HPNL hPanel, int QtyPanels, ONE_SINGLETRACE *ptrParmAttrib);
HPNL MakeDataPanel(HPNL hParentPanel);
void SetDataPanelConfig(ONE_SINGLETRACE *Config);
void SetDataPanelParm(HPNL hPanel, int nParm, int nUnits, int nMode, int nGasTypeListItem, double nThreshold1, double nThreshold2, int  nBTBpeakMode, int nXScaleFactor, int nYScaleFactor);
void SetDataPanelIcon(HPNL hPanel, char *szIcon);
void SetDataPanelParmSelect(HPNL hPanel, char *szText);
void SetDataPanelParmLabel(HPNL hPanel, char *szText);
void SetDataPanelUnits(HPNL hPanel, char *szText);
void SetDataPanelModeVisible(HPNL hPanel, int nVisible);
void SetDataPanelMode(HPNL hPanel, char *szText);
void SetDataPanelPeakMode(HPNL hPanel, int nBTBpeakMode);
void SetDataPanelCurrent(HPNL hPanel, char *szText);
void SetDataPanelMax(HPNL hPanel, char *szText);
void SetDataPanelAvg(HPNL hPanel, char *szText);
void SetDataPanelMin(HPNL hPanel, char *szText);
void SetDataPanelColors(HPNL hPanel, CVI_COLOR colorRGB);
int SetDataPanelSize(HPNL hPanel, int QtyPanels, int PanelNum);
HPNL MakeAllparmsPanel(HPNL hParentPanel);
void SetAllparmsPanelSize(HPNL hPanel);
HPNL MakeAllparmsDataPanel(HPNL hParentPanel, int QtyPanels, int Position);
HPNL MakeVolAccPanel(HPNL hParentPanel);
void SetVolAccPanelConfig(ONE_SINGLETRACE *Config);
void SetVolAccPanelParm(HPNL hPanel, int nParm, int nUnits, int nMode, int nGasTypeListItem, double nThreshold1, double nThreshold2, int  nBTBpeakMode, int nXScaleFactor, int nYScaleFactor);
void SetVolAccPanelIcon(HPNL hPanel, char *szIcon);
void SetVolAccPanelParmSelect(HPNL hPanel, char *szText);
void SetVolAccPanelUnits(HPNL hPanel, char *szText);
void SetVolAccPanelModeVisible(HPNL hPanel, int nVisible);
void SetVolAccPanelMode(HPNL hPanel, char *szText);
void SetVolAccPanelGasTypeVisible(HPNL hPanel, int nVisible);
void SetVolAccPanelGasType(HPNL hPanel, char *szText);
void SetVolAccPanelTemperatureVisible(HPNL hPanel, int nVisible);
void SetVolAccPanelTemperature(HPNL hPanel, double dTemperature, int nTemprUnits);
void SetVolAccPanelThresholdVisible(HPNL hPanel, int nVisible);
void SetVolAccPanelThreshold(HPNL hPanel, int nThresholdType, double dThreshold);
void SetVolAccPanelCurrent(HPNL hPanel, char *szText);
void SetVolAccPanelMax(HPNL hPanel, char *szText);
void SetVolAccPanelColors(HPNL hPanel, CVI_COLOR colorRGB);
void SetVolAccPanelSize(HPNL hPanel);
HPNL MakeBasePanel(void);
void SizeBasePanel(HPNL hPanel);
HPNL MakeMainBasePanel(HPNL hParentPanel);
void SetMainBasePanelSize(void);
void ErrorMessageLoadPanel(HPNL hPanel);
int  SizeUserCharts(void);

#endif

