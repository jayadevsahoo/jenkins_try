#ifndef PavBreathDataPacket_HH
#define PavBreathDataPacket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct : PavBreathDataPacket - structure definition of patient
//   data packet containing PAV data that are updated every breath.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PavBreathDataPacket.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: sah    Date:  18-Jan-2001    DR Number: 5838
//	Project:  PAV
//	Description:
//		Split up original 'ComputedPavDataPacket' putting those PAV
//      data items that are updated every (non-plateau) breath in
//      this structure.
//
//====================================================================

#include "Sigma.hh"
#include "PavState.hh"

// @Type: PavBreathDataPacket
struct   PavBreathDataPacket
{
	Real32  patientWorkOfBreathing;
	Real32  totalWorkOfBreathing;
	Real32  elastanceWob;
	Real32  resistiveWob;
	Real32  intrinsicPeep;
	PavState::Id  pavState;

	void convHtoN()
	{
		SocketWrap sock;
		patientWorkOfBreathing = sock.HTONF(patientWorkOfBreathing);
		totalWorkOfBreathing = sock.HTONF(totalWorkOfBreathing);
		elastanceWob = sock.HTONF(elastanceWob);
		resistiveWob = sock.HTONF(resistiveWob);
		intrinsicPeep = sock.HTONF(intrinsicPeep);
		pavState = (PavState::Id)sock.HTONL((Uint32)pavState);
	}

	void convNtoH()
	{
		SocketWrap sock;
		patientWorkOfBreathing = sock.NTOHF(patientWorkOfBreathing);
		totalWorkOfBreathing = sock.NTOHF(totalWorkOfBreathing);
		elastanceWob = sock.NTOHF(elastanceWob);
		resistiveWob = sock.NTOHF(resistiveWob);
		intrinsicPeep = sock.NTOHF(intrinsicPeep);
		pavState = (PavState::Id)sock.NTOHL((Uint32)pavState);
	}
};

#endif // PavBreathDataPacket_HH 

