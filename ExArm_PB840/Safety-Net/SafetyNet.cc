#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.             
//                                            
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================ C L A S S   D E S C R I P T I O N ====
//@ Class: SafetyNet - Safety-Net Subsystem module ID class
//---------------------------------------------------------------------
//@ Interface-Description
//  This class executes all initialize method of the Safety-Net
//  Subsystem.
//---------------------------------------------------------------------
//@ Rationale
//  This class initializes the Safety-Net Subsystem at a high level.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Initialize() method invokes all Background class initialize
//  methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/SafetyNet.ccv   25.0.4.0   19 Nov 2013 14:20:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001   By: by   Date: 12-Dec-1995  DR Number: None
//    Project:  Sigma (840)
//    Description:
//      Initial version.
//=====================================================================

#include "SafetyNet.hh"
#include "TaskMonitor.hh"
#include "Background.hh"


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize
//
//@ Interface-Description
//  This method accepts no parameter and returns no value.
//  This method calls all intialize methods in the Safety-Net Subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes TaskMonitor::Initialize() and
//  Background::Initialize().
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
SafetyNet::Initialize( void )
{
   // $[TI1]
   // Initialize the tasks in this subsystem
   TaskMonitor::Initialize();
   Background::Initialize();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================
 
void
SafetyNet::SoftFault( const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const char*        pFileName,
                      const char*        pPredicate )
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
    FaultHandler::SoftFault( softFaultID, SAFETY_NET, SAFETYNET,
                             lineNumber, pFileName, pPredicate );
}
