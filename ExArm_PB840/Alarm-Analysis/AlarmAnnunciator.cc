#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmAnnunciator - Direct processing for Alarm Annunciation cluster.
//---------------------------------------------------------------------
//@ Interface-Description
// The AlarmManager object collects copies of the updated alarms and other 
// alarm status information in an AlarmUpdateBuffer by messaging 
// AlarmAnnunciator::BuildBuffer and then messages method 
// AlarmAnnunciator::Update to notify AlarmAnnunciator the information is
// available.
//
// AlarmAnnunciator processes the information and notifies the GUI-Applications
// cluster that there has been a change in the alarm status.  AlarmAnnunciator
// is part of the Alarm Annunciation cluster which resides in the Main GUI
// Event task.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
// AlarmAnnunciator is responsible for taking the output of the Alarm Analysis
// cluster of AlarmUpdateBuffer objects and updating 
// the Current Alarm Log and the Alarm History Log.  This class also updates 
// the Alarm Silence state and Alarm Summary statuses.  It is responsible for 
// notifying GUI-Applications of any change in alarm status, so the change can 
// be annunciated to the operator.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
// There are no instantiations of this class.
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmAnnunciator.ccv   25.0.4.0   19 Nov 2013 13:51:12   pvcs  $
//
//@ Modification-Log

//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 06/29/95         DR Number:   498
//      Project:   Sigma   (R8027)
//      Description:
//         Integrate with new Persistent-Objects interface.  The new interface
//         permits other subsystems to integrate with Persistent-Objects 
//         without needing access to Alarm-Analysis subsystem header files.
//         The Persistent-Objects subsystem stores AlarmAction structures 
//         instead of AlarmEvent objects.  The NovRamManager::LogAlarmEvent()
//         method was replaced by the NovRamManager::LogAlarmAction() method. 
//  
//   Revision 003   By:   hct      Date: 06/29/95         DR Number:   499
//      Project:   Sigma   (R8027)
//      Description:
//         Class AlarmCommList is designed to hold the same information as
//         an AlarmUpdateBuffer object but in a form which can be sent 
//         across the Ethernet link between CPUs.  Changes were made in 
//         BuildBuffer()/AppendBuffer() to send/receive AlarmCommList objects
//         across the Ethernet link.
//  
//   Revision 004   By:   hct      Date: 08/02/95         DR Number:   502
//      Project:   Sigma   (R8027)
//      Description:
//         Added support for DCI reports via methods UpdateDciEntry_ and
//         UpdateDciArray_.
//  
//   Revision 005   By:   hct      Date: 01/11/96         DR Number:   649
//      Project:   Sigma   (R8027)
//      Description:
//         Changed conditional compilation label from SIGMA_DEVELOPMENT to
//         SIGMA_SIMULATOR.
//  
//   Revision 006   By:   hct      Date: 04/12/96         DR Number:   881
//      Project:   Sigma   (R8027)
//      Description:
//         Calling UpdateAlarmAnnunciation_() from AlarmSilence()
//         was causing an immediate "cancel Alarm Silence" if there was an
//         existing high urgency alarm. AlarmSilence() and CancelAlarmSilence()
//         now message GuiApp::AlarmEventHappened() directly, instead of 
//         going through UpdateAlarmAnnunciation_().  Also, if a new alarm
//         with an urgency less than high became active, 
//         UpdateAlarmAnnunciation_ would cancel alarm silence if there was a
//         pre-existing high urgency alarm.  The logic to check for a high
//         urgency alarm was moved to Update where only the new alarms are
//         checked.         
//  
//   Revision 007   By:   gbs      Date: 04/23/96         DR Number:   942
//      Project:   Sigma   (R8027)
//      Description:
//         Added UpdateSetFromCpu() unit test method.
//  
//   Revision 008   By:   hct      Date: 05/06/96         DR Number:   938
//      Project:   Sigma   (R8027)
//      Description:
//         High Urgency audible and led fails to clear when alarm autoresets.
//         Got rid of separate variables for keeping track of BD and GUI 
//         highest urgency.  UpdateAlarmAnnunciation_() works on the complete
//         list of current alarms so when the BD reported a high urgency alarm
//         and there was subsequently a low urgency alarm reported by the GUI
//         cpu, the highest urgency for the GUI was HIGH because the urgency
//         for the alarm reported by the BD was included in determining the
//         GUI highest urgency.
//  
//   Revision 009   By:   gbs      Date: 05/17/96         DR Number:   995
//      Project:   Sigma   (R8027)
//      Description:
//         Fix CALL_TRACE and add passed parameter comments.
//         These changes do not affect Unit Test.
//  
//   Revision 010   By:   hct      Date: 08/22/96         DR Number: 10041
//      Project:   Sigma   (R8027)
//      Description:
//         Changed processing for Update() so all AlarmUpdateBuffers (up to
//         NUM_BUFF) are processed in one pass before GUI-Apps is called to 
//         update the displays.
//  
//   Revision 011   By:   hct      Date: 12/05/96         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.  Removed AppendBuffer.
//  
//   Revision 012   By:   hct      Date: 24 APR 1997      DR Number:  1994
//      Project:   Sigma   (R8027)
//      Description:
//         Deleted requirement number 00626.
//  
//   Revision 013   By:   hct      Date: 04 NOV 1997      DR Number:  2604
//      Project:   Sigma   (R8027)
//      Description:
//         Deleted while loop from method update to ensure GUI annunciates
//         every active alarm.
//  
//   Revision 014   By:   hct      Date: 13 NOV 1997      DR Number:  2626
//      Project:   Sigma   (R8027)
//      Description:
//         Added processing for patient data reset.
//
//   Revision 015       By:   sah          Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//          Replaced all references to 'AlarmMessages' with references to
//			the new 'AlarmStrs' class.
//  
//   Revision 016	By:   gdc	   Date: 27-FEB-1998    DR Number: 5017
//      Project:   Sigma   (R8O27)
//      Description:
//		   Added interface to AlarmRepository to provide alarm status
//         information to the Alarm Setup Subscreen Slider Buttons.
//  
//   Revision 017   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 018   By:   yyy      Date: 08/18/99         DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified to handle alarm lock and break through.
//  
//   Revision 019   By:   rhj      Date: 07/13/06         DR Number:   5928
//      Project:   RESPM
//      Description:
//      Added support for the SNDF command, which can report
//      low, medium, and high urgency alarms.
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "AlarmAnnunciator.hh"

// TODO E600 remove
//#include "Ostream.hh"

//@ Usage-Classes
#include "OsFoundation.hh"
#include "Alarm.hh"
#include "AlarmAction.hh"
#include "AlarmBroadcastArray.hh"
#include "AlarmConstants.hh"
#include "AlarmDci.hh"
#include "AlarmEvent.hh"
#include "AlarmManager.hh"
#include "AlarmRepository.hh"
#include "AlarmStrs.hh"
#include "AlarmUpdateBufferList.hh"
#include "AlarmUpdateList.hh"
#include "ConditionEvaluations.hh"
#include "GuiApp.hh"
#include "GuiTimerId.hh"
#include "IpcIds.hh"
#include "MsgQueue.hh"
#include "MutEx.hh"
#include "OperatingParamEventName.hh"
#include "UserAnnunciationMsg.hh"

#ifdef SIGMA_SIMULATOR
#  include "AlarmLogEntry.hh"
#  include "Array_AlarmLogEntry_MAX_ALARM_ENTRIES.hh"
#endif // SIGMA_SIMULATOR

//@ End-Usage

#if defined(SIGMA_GUI_CPU)
static Uint 
    pAlarmUpdateBufferListMemory[(sizeof(AlarmUpdateBufferList) + 
    sizeof(Uint) - 1) / sizeof(Uint)];
AlarmUpdateBufferList& AlarmAnnunciator::RAlarmUpdateBufferList_ = 
    *((AlarmUpdateBufferList*)::pAlarmUpdateBufferListMemory);

static Uint 
    pCurrentAlarmLogMemory[(sizeof(AlarmUpdateSortedList) + 
    sizeof(Uint) - 1) / sizeof(Uint)];
AlarmUpdateSortedList& AlarmAnnunciator::RCurrentAlarmLog_ = 
    *((AlarmUpdateSortedList*)::pCurrentAlarmLogMemory);

#ifdef SIGMA_SIMULATOR
  Boolean AlarmAnnunciator::AlarmSemaphore_;
#endif // SIGMA_SIMULATOR

static Uint
    pAlarmSilenceTimer[(sizeof(MsgTimer) + sizeof(Uint) - 1) / sizeof(Uint)];
MsgTimer& AlarmAnnunciator::RAlarmSilenceTimer_ =
    *((MsgTimer*)::pAlarmSilenceTimer);

static Uint
    pBufferListMutEx[(sizeof(MutEx) + sizeof(Uint) - 1) / sizeof(Uint)];
MutEx& AlarmAnnunciator::RBufferListMutEx_ =
    *((MutEx*)::pBufferListMutEx);

Boolean        AlarmAnnunciator::AlarmsAreSilenced_;
Boolean        AlarmAnnunciator::HighUrgencyAutoReset_;
Alarm::Urgency AlarmAnnunciator::HighestUrgency_;
Int32          AlarmAnnunciator::AlarmSilenceInterval_; 
Boolean        AlarmAnnunciator::CancelScreenLockStatus_;
Boolean        AlarmAnnunciator::NoPatientDataDisplayStatus_;
Boolean        AlarmAnnunciator::NoGuiSettingChangesStatus_;
Boolean        AlarmAnnunciator::VentInopLedOnStatus_;
#endif // SIGMA_GUI_CPU
//@ Code...


//=====================================================================
//
//      Public Methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//   Initialization for AlarmAnnunciator class.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Instantiate the Alarm History Log and Current Alarm Log and initialize
//   alarm status information.
//
//   If this class is instantiated on the GUI_CPU, regester with the 
//   Ethernet communication link for callback when AlarmUpdate information
//   is sent over the Ethernet link.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::Initialize(void)
{
  CALL_TRACE("Initialize(void)");

#if defined(SIGMA_GUI_CPU)
  AlarmAnnunciator::InitData();
  AlarmAnnunciator::InitMemory();
#endif // SIGMA_GUI_CPU
  // $[TI1]
}


#if defined(SIGMA_GUI_CPU)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitMemory
//
//@ Interface-Description
//   Initialize the memory for AlarmAnnunciator class.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Instantiate the Alarm History Log and Current Alarm Log and initialize
//   alarm status information.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::InitMemory(void)
{
  CALL_TRACE("InitMemory(void)");

  CLASS_ASSERTION(sizeof(::pAlarmUpdateBufferListMemory) >= 
      sizeof(AlarmUpdateBufferList));
  new (::pAlarmUpdateBufferListMemory) AlarmUpdateBufferList;

  CLASS_ASSERTION(sizeof(::pCurrentAlarmLogMemory) >= 
      sizeof(AlarmUpdateSortedList));
  new (::pCurrentAlarmLogMemory) AlarmUpdateSortedList;

  // Send the alarm update event to the UserAnnunciationQueue.
  UserAnnunciationMsg msg;
  msg.timerParts.eventType = UserAnnunciationMsg::TIMER_EVENT;
  msg.timerParts.timerId = GuiTimerId::ALARM_SILENCE_TIMEOUT;

  CLASS_ASSERTION(sizeof(::pAlarmSilenceTimer) >=
      sizeof(MsgTimer));
  new (::pAlarmSilenceTimer)    MsgTimer(USER_ANNUNCIATION_Q, (Int32)msg.qWord, 
      AlarmAnnunciator::AlarmSilenceInterval_);

  CLASS_ASSERTION(sizeof(::pBufferListMutEx) >=
      sizeof(MutEx));
  new (::pBufferListMutEx)      MutEx(BUFFER_LIST_MT);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitData
//
//@ Interface-Description
//   Initialize static data for AlarmAnnunciator class.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Initialize alarm status information.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::InitData(void)
{
  CALL_TRACE("InitData(void)");

  //  Alarm Silence Interval is two minutes (120000 ms) $[05021].
  AlarmAnnunciator::AlarmSilenceInterval_ = 120000;

  // Initialize data.
  AlarmAnnunciator::AlarmsAreSilenced_    = FALSE;
  AlarmAnnunciator::HighUrgencyAutoReset_ = FALSE;
  AlarmAnnunciator::HighestUrgency_       = Alarm::NORMAL_URGENCY;

  AlarmAnnunciator::NoPatientDataDisplayStatus_ = FALSE;
  AlarmAnnunciator::NoGuiSettingChangesStatus_ = FALSE;
  AlarmAnnunciator::VentInopLedOnStatus_ = FALSE;
  AlarmAnnunciator::CancelScreenLockStatus_ = FALSE;

#  ifdef SIGMA_SIMULATOR
  AlarmAnnunciator::AlarmSemaphore_ = FALSE;
#  endif // SIGMA_SIMULATOR
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Update  
//
//@ Interface-Description
//       While the Alarm Analysis Cluster is evaluating alarms, it builds a
//       list of alarm updates which is sent to the Alarm Annunciation cluster
//       along with other alarm information by appending an AlarmUpdateBuffer 
//       to the AlarmUpdateBufferList and sending event 
//       UserAnnunciationMsg::ALARM_UPDATE_EVENT to the GUI task.  When the GUI 
//       receives this event, it messages this method which builds the
//       Current Alarm Log, Alarm History Log $[05007] $[05010]
//       $[05013] $[05017] $[05045], Alarm Summary information (highest
//       current urgency, high urgency autoreset status) and makes other status 
//       information available to the GUI-Applications subsystem via query
//       methods.
//       
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::Update(void)
{
  CALL_TRACE("Update(void)");

  Int16 ii = 0;
  Boolean updateOccurred = FALSE;
  SigmaStatus status = FAILURE;

  while ((ii < 2) && (!RAlarmUpdateBufferList_.isEmpty())) // $[TI1]
  {

    // Get the MutEx for the AlarmUpdateBufferList.
    AlarmAnnunciator::RBufferListMutEx_.request();

    status = RAlarmUpdateBufferList_.goFirst();
    CLASS_ASSERTION(status == SUCCESS);

    AlarmUpdateBuffer updateBuffer = RAlarmUpdateBufferList_.currentItem();
    Alarm::UpdateType updateType = updateBuffer.getUpdateType();

    // If a Loss of Patient Data Monitoring alarm has autoreset, there will 
    // be two AlarmUpdateBuffer's to process; the first for the autoreset
    // Loss of Monitoring alarm and the second to reset active patient data
    // alarms.
    if ((ii == 1) && (updateType != Alarm::PAT_DATA_RESET_UT)) // $[TI1.1]
    {
      // Release the MutEx for the AlarmUpdateBufferList.
      AlarmAnnunciator::RBufferListMutEx_.release();
      break;
    } // else $[TI1.2]

    status = RAlarmUpdateBufferList_.removeCurrent();
    CLASS_ASSERTION(status == SUCCESS);
  
    // Release the MutEx for the AlarmUpdateBufferList.
    AlarmAnnunciator::RBufferListMutEx_.release();

    AlarmAnnunciator::CancelScreenLockStatus_ = 
	updateBuffer.getCancelScreenLockStatus();
    AlarmAnnunciator::NoPatientDataDisplayStatus_ = 
	updateBuffer.getNoPatientDataDisplayStatus();
    AlarmAnnunciator::NoGuiSettingChangesStatus_ = 
	updateBuffer.getNoGuiSettingChangesStatus();
    AlarmAnnunciator::VentInopLedOnStatus_ = 
	updateBuffer.getVentInopLedOnStatus();

    AlarmUpdateList updateList(updateBuffer.getAlarmUpdateList());
  
    // If the update list is not empty or the update was caused by the operator
    // pressing the Manual Reset key, update the alarm annunciation information.
    if (!updateList.isEmpty() || (updateType == Alarm::MANUAL_RESET_UT)) // $[TI1.3]
    {
      updateOccurred = TRUE;
      status = updateList.goFirst();
  
      // Process each AlarmUpdate object on the updateList.
      while (status == SUCCESS) // $[TI1.3.1]
      {
	const AlarmUpdate& rAlarmUpdate = updateList.currentItem();
  
	// Message the Persistent Object manager to add an Alarm Event to the
	// AlarmHistoryLog.
	NovRamManager::LogAlarmAction(AlarmEvent(rAlarmUpdate).makeAlarmAction());
  
	// If the Alarm specified in this AlarmUpdate record is currently being
	// annunciated, remove the old annunciation from the Current Alarm Log.
	AlarmAnnunciator::RemoveAnnunciatedAlarm_(rAlarmUpdate.getName());
  
	// If this AlarmUpdate object represents an active, independent Alarm, 
	// add it to the Current Alarm Log and if it has transitioned to
	// Alarm::HIGH_URGENCY, cancel the Alarm Silence function if it is
	// active $[05026] $[05045].
	if ((rAlarmUpdate.getIsActive() == TRUE) && 
	   !rAlarmUpdate.getIsDependent()) // $[TI1.3.1.1]
	{
  
	  if (AlarmAnnunciator::AlarmsAreSilenced_) // $[TI1.3.1.1.1]
	  {
	    if ((rAlarmUpdate.getPreviousUrgency() < Alarm::HIGH_URGENCY) &&
		(rAlarmUpdate.getCurrentUrgency() == Alarm::HIGH_URGENCY) &&
		(rAlarmUpdate.getBreakThroughSilence())) // $[TI1.3.1.1.1.1]
	    {
	      AlarmAnnunciator::CancelAlarmSilence_
		  (Alarm::END_ALARM_SILENCE_HIGH_URGENCY_UT); // $[05045]
	    } // else $[TI1.3.1.1.1.2]
	  } // else $[TI1.3.1.1.2]
  
	  AlarmAnnunciator::RCurrentAlarmLog_.addItem(rAlarmUpdate);
	} // else $[TI1.3.1.2]
  
	// Update the AlarmDci array entry associated with this AlarmUpdate.
	AlarmAnnunciator::UpdateDciEntry_(rAlarmUpdate);
  
	// Update the AlarmBroadcast array entry associated with this AlarmUpdate.
	AlarmBroadcastArray::PutStatus(rAlarmUpdate);
  
	status = updateList.goNext();
      } // No while statement executed. $[TI1.3.2]
  
      AlarmAnnunciator::UpdateDciArray_(updateType);
  
      // Update the Alarm status information.
      AlarmAnnunciator::UpdateAlarmAnnunciation_(updateType);
    } // else $[TI1.4]

    ii++;
  } // while

  // Message GUI-Applications to update user displays if update occurred.
  if (updateOccurred) // $[TI3]
  {
    GuiApp::AlarmEventHappened();
  } // $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelAlarmSilence  
//
//@ Interface-Description
//   This method is messaged by the GUI-Applications Main Event task on the
//   GUI_CPU when it receives the Alarm Silence timer timeout event. 
//   Reset the Alarm Silence status and prepare Alarm Status information 
//   so the GUI will audibly annunciate alarms.
//---------------------------------------------------------------------
//@ Implementation-Description
//   When messaged, this method calls the private CancelAlarmSilence_ method
//   to stop Alarm Silence and make an Alarm History Log entry.  It also calls
//   GuiApp::AlarmEventHappened() to notify GUI of the change in
//   alarm status.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::CancelAlarmSilence(void)
{
  CALL_TRACE("CancelAlarmSilence(void)");

  // Kill the Alarm Silence timer and make Alarm Log entry.
  AlarmAnnunciator::CancelAlarmSilence_(Alarm::END_ALARM_SILENCE_UT);

  // Message GUI-Applications to update user displays.
  GuiApp::AlarmEventHappened();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmSilence        
//
//@ Interface-Description
//   If the Alarm Silence function is currently inactive, start the Alarm
//   Silence timer and prepare Alarm Status information so the GUI will stop
//   audibly annunciating alarms.  If the Alarm Silence function is currently
//   active, restart the Alarm Silence timer with the Alarm Silence interval.
//   $[05022] $[05023] Create an Alarm History Log entry to indicate the Alarm 
//   Silence key was pressed $[05024] $[05045].
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::AlarmSilence(void)
{
  CALL_TRACE("AlarmSilence(void)");

  if (AlarmAnnunciator::AlarmsAreSilenced_) // $[TI1]
  {
    // Restart the alarm silence timer
    AlarmAnnunciator::RAlarmSilenceTimer_.set();
  }
  else // $[TI2]
  {
    // Set the AlarmsAreSilenced_ attribute used by GUI to control the 
    // Alarm Silence key LED $[05025] $[05031].
    AlarmAnnunciator::AlarmsAreSilenced_ = TRUE;

    // Start the Alarm Silence timer
    AlarmAnnunciator::RAlarmSilenceTimer_.set();
  }

  // Message the Persistent Object manager to add an Alarm Event to the
  // AlarmHistoryLog.
  NovRamManager::LogAlarmAction(AlarmEvent(ALARM_SILENCE_UT_MSG,TimeStamp(),
    Alarm::ALARM_SILENCE_UT).makeAlarmAction());

  // Message GUI-Applications to update user displays.
  GuiApp::AlarmEventHappened();
}
#endif // SIGMA_GUI_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BuildBuffer
//
//@ Interface-Description
//  This method will create an AlarmUpdateBuffer and append it
//  to the AlarmAnnunciator::AlarmUpdateBufferList_.
// >Von
//  rAlarmUpdateList            Holds the AlarmUpdateList.
//  updateType                  Holds the value of
//                              Alarm::GetUpdateType().
//  cancelScreenLockStatus      Holds the Boolean value of 
//                              Alarm::GetCancelScreenLockStatus().
//  noPatientDataDisplayStatus  Holds the Boolean value of 
//                              Alarm::GetNoPatientDataDisplayStatus().
//  noGuiSettingChangesStatus   Holds the Boolean value of
//                              Alarm::GetNoGuiSettingChangesStatus().
//  ventInopLedOnStatus         Holds the Boolean value of
//                              Alarm::GetVentInopLedOnStatus().
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   ((Alarm::UPDATE_TYPE_NULL <= updateType) &&
//       (updateType < Alarm::MAX_NUM_UPDATE_TYPE))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::BuildBuffer(const AlarmUpdateList& rAlarmUpdateList,
                              Alarm::UpdateType updateType,
                              Boolean           cancelScreenLockStatus,
                              Boolean           noPatientDataDisplayStatus,
                              Boolean           noGuiSettingChangesStatus,
                              Boolean           ventInopLedOnStatus)
{
  CALL_TRACE("BuildBuffer(rAlarmUpdateList, updateType, cancelScreenLockStatus, noPatientDataDisplayStatus, noGuiSettingChangesStatus, ventInopLedOnStatus)");

  CLASS_PRE_CONDITION((Alarm::UPDATE_TYPE_NULL <= updateType) &&
                      (updateType < Alarm::MAX_NUM_UPDATE_TYPE));

#ifdef SIGMA_GUI_CPU
  // Get the AlarmUpdateBufferList MutEx before appending.
  AlarmAnnunciator::RBufferListMutEx_.request();

  RAlarmUpdateBufferList_.append(AlarmUpdateBuffer
      (rAlarmUpdateList, updateType, cancelScreenLockStatus, 
      noPatientDataDisplayStatus, noGuiSettingChangesStatus, 
      ventInopLedOnStatus));

  // Release the MutEx for the AlarmUpdateBufferList.
  AlarmAnnunciator::RBufferListMutEx_.release();

  // Send the alarm update event to the UserAnnunciationQueue.
  UserAnnunciationMsg msg;
  msg.alarmParts.eventType = UserAnnunciationMsg::ALARM_UPDATE_EVENT;

  CLASS_ASSERTION(MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) == 
      Ipc::OK);
#endif // SIGMA_GUI_CPU

  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//                     
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
AlarmAnnunciator::SoftFault(const SoftFaultID  softFaultID,
						    const Uint32       lineNumber,
						    const char*        pFileName,
						    const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS,
                          Alarm_Analysis::ALARMANNUNCIATOR, 
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Private Methods
//
//=====================================================================

#if defined(SIGMA_GUI_CPU)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RemoveAnnunciatedAlarm_   
//
//@ Interface-Description
//   Remove the AlarmUpdate record whose name_ attribute matches the parameter
//   name, from the Current Alarm Log.
//
// >Von
//   name  Enumerated value of the name_ attribute of the AlarmUpdate object
//         to be removed from the CurrentAlarmLog.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   ((ALARM_NAME_NULL < name) && (name < MAX_NUM_ALARMS))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::RemoveAnnunciatedAlarm_(AlarmName name)
{
  CALL_TRACE("RemoveAnnunciatedAlarm_(name)");

  CLASS_PRE_CONDITION(((ALARM_NAME_NULL) < name) && 
                      (name < (MAX_NUM_ALARMS)));

  // Process each record in the Current Alarm Log until a match is found.
  SigmaStatus status =
      AlarmAnnunciator::RCurrentAlarmLog_.goFirst();
  while (status == SUCCESS) // $[TI1]
  {
    if (AlarmAnnunciator::RCurrentAlarmLog_.currentItem().
        getName() == name) // $[TI1.1]
    {
      AlarmAnnunciator::RCurrentAlarmLog_.removeCurrent();
      // Fail while loop test.
      status = FAILURE;
    }
    else // $[TI1.2]
    {
      status = AlarmAnnunciator::RCurrentAlarmLog_.goNext();
    }
  } // No while statement executed. $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpdateAlarmAnnunciation_       
//
//@ Interface-Description
//   Using the current alarm list:
//     Update the Alarm status information:
//       Highest current urgency $[05001] $[05002] $[05003]
//       High urgency auto reset status $[05004]
//       Alarm Silence status $[05015]
//
// >Von
//  updateType  If the parameter updateType equals MANUAL_RESET_UT then clear 
//              the High Urgency Auto Reset $[05018].
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   ((Alarm::UPDATE_TYPE_NULL <= updateType) &&
//    (updateType < Alarm::MAX_NUM_UPDATE_TYPE)) 
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::UpdateAlarmAnnunciation_(Alarm::UpdateType updateType) 
{
  CALL_TRACE("UpdateAlarmAnnunciation_(updateType)");

  CLASS_PRE_CONDITION((Alarm::UPDATE_TYPE_NULL <= updateType) && 
                      (updateType < Alarm::MAX_NUM_UPDATE_TYPE));

  // Temporary variable used in determining highest current urgency.
  Alarm::Urgency tempUrgency = Alarm::NORMAL_URGENCY;

  // Process each AlarmUpdate record in the Current Alarm Log.
  SigmaStatus status =
      AlarmAnnunciator::RCurrentAlarmLog_.goFirst();
  while (status == SUCCESS) // $[TI1]
  {
    AlarmUpdate& rAlarmUpdate = 
        AlarmAnnunciator::RCurrentAlarmLog_.currentItem();

#  ifdef SIGMA_SIMULATOR
    cout << AlarmStrs::GetMessage(rAlarmUpdate.getBaseMessage()) << " ";
    Uint32 tempIndex = rAlarmUpdate.getAnalysisIndex();
    for (Uint32 j = 0; j < tempIndex; j++)
    {
      cout << 
          AlarmStrs::GetMessage(rAlarmUpdate.getAnalysisMessage()[j]);
      cout << " ";
    }

    tempIndex = rAlarmUpdate.getRemedyIndex();
    for (j = 0; j < tempIndex; j++)
    {
      cout << AlarmStrs::GetMessage(rAlarmUpdate.getRemedyMessage()[j]); 
      cout << " ";
    }
    cout << " ";

    AlarmAnnunciator::DisplayUrgency_(rAlarmUpdate.getUrgencyMessage());
#  endif // SIGMA_SIMULATOR

    // Keep track of the highest current urgency
    if (rAlarmUpdate.getCurrentUrgency() > tempUrgency) // $[TI1.1]
    {
      tempUrgency = rAlarmUpdate.getCurrentUrgency();
    } // else $[TI1.2]

#  ifdef SIGMA_SIMULATOR
    AlarmAnnunciator::DisplayUpdateType_(rAlarmUpdate.getUpdateTypeMessage());
    cout << endl;
#  endif // SIGMA_SIMULATOR

    status = AlarmAnnunciator::RCurrentAlarmLog_.goNext();
  } // No while statement executed. $[TI2]

  // All AlarmUpdate objects have been processed so update status information.

  // If Manual Reset caused this update, clear the state of the 
  // HighUrgencyAutoReset_ attribute and cancel the Alarm Silence function
  // $[05018]
  if (updateType == Alarm::MANUAL_RESET_UT) // $[TI3]
  {
    AlarmAnnunciator::HighUrgencyAutoReset_ = FALSE;

    if (AlarmAnnunciator::AlarmsAreSilenced_) // $[TI3.1]
    {
      AlarmAnnunciator::CancelAlarmSilence_(Alarm::MANUAL_RESET_UT); // $[05045]
    } // else $[TI3.2]
  }
  // If Manual Reset did not cause this update, then if the highest urgency
  // from the previous loop of alarm processing was HIGH_URGENCY and the
  // highest urgency from this loop of alarm processing (as reflected in the
  // temporary variable tempUrgency) is less than HIGH_URGENCY then at least
  // one alarm has transitioned from HIGH_URGENCY to a lower urgency or has
  // autoreset to NORMAL so set the High Urgency Auto Reset status attribute
  // to TRUE.
  else if ((HighestUrgency_ == Alarm::HIGH_URGENCY) && 
           (tempUrgency < Alarm::HIGH_URGENCY)) // $[TI4]
  {
    AlarmAnnunciator::HighUrgencyAutoReset_ = TRUE;
  } // else $[TI5]

  // Update the HighestUrgency_ attribute.
  AlarmAnnunciator::HighestUrgency_ = tempUrgency;

 
#  ifdef SIGMA_SIMULATOR
  AlarmAnnunciator::UpdateAlarmAudio_();
  AlarmAnnunciator::UpdateAlarmSummary_();
#  endif // SIGMA_SIMULATOR

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelAlarmSilence_
//
//@ Interface-Description
//   Private method to stop Alarm Silence and make an Alarm History Log entry.
//   $[05019] $[05045]
//
// >Von
//   updateType  Identifies the cause of ending Alarm Silence (New high urgency 
//   alarm $[05030], Manual Reset, Alarm Silence timeout) $[05027] $[05029] 
//   $[05045]. 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   Reset the Alarm Silence status and prepare Alarm Status information so the 
//   GUI will audibly annunciate alarms.  The methods which call this method
//   will notify the GUI of the change in alarm status.
//---------------------------------------------------------------------
//@ PreCondition
//   ((Alarm::UPDATE_TYPE_NULL < updateType) &&
//    (updateType < Alarm::MAX_NUM_UPDATE_TYPE))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::CancelAlarmSilence_(Alarm::UpdateType updateType)
{
  CALL_TRACE("CancelAlarmSilence_(updateType)");

  CLASS_PRE_CONDITION((Alarm::UPDATE_TYPE_NULL < updateType) &&
                      (updateType < Alarm::MAX_NUM_UPDATE_TYPE));
                      
  // Clear the AlarmsAreSilenced_ attribute used by GUI to control the 
  // Alarm Silence key LED $[05025] $[05028].
  AlarmAnnunciator::AlarmsAreSilenced_ = FALSE;

  // Kill the alarm silence timer.
  AlarmAnnunciator::RAlarmSilenceTimer_.cancel();

  NovRamManager::LogAlarmAction(AlarmEvent
    (END_ALARM_SILENCE_UT_MSG,TimeStamp(), updateType).makeAlarmAction());
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpdateDciEntry_
//
//@ Interface-Description
//   Private method to update the Dci status of the alarm specified in the
//   passed parameter.
//
// >Von
//   rAlarmUpdate  Reference to the AlarmUpdate object. 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   If the Alarm reference by the AlarmUpdate object is active, set
//   the corresponding AlarmDci array entry to ALARM_DCI_ALARM, else if
//   the previous urgency is HIGH and the current urgency is < HIGH set
//   the array entry to ALARM_DCI_RESET else set it to ALARM_DCI_NORMAL.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::UpdateDciEntry_(const AlarmUpdate& rAlarmUpdate)
{
  CALL_TRACE("UpdateDciEntry_(rAlarmUpdate)");

  // Update the AlarmRepository associated with this AlarmUpdate.
  AlarmRepository::SetStatus(  rAlarmUpdate.getName()
                             , rAlarmUpdate.getCurrentUrgency() );
 
  // Update the alarm status in the AlarmDci array.
  if (rAlarmUpdate.getIsActive()) // $[TI1]
  {
    AlarmDci::PutStatus(rAlarmUpdate.getName(), AlarmDci::ALARM_DCI_ALARM);
  }
  else if ((rAlarmUpdate.getPreviousUrgency() == Alarm::HIGH_URGENCY) &&
        (rAlarmUpdate.getCurrentUrgency() < Alarm::HIGH_URGENCY)) // $[TI2]
  {
    AlarmDci::PutStatus(rAlarmUpdate.getName(), AlarmDci::ALARM_DCI_RESET);
  }        
  else // $[TI3]
  {
    AlarmDci::PutStatus(rAlarmUpdate.getName(), AlarmDci::ALARM_DCI_NORMAL);
  }


  // Update the alarm status for AlarmDci array for the SNDF command. 
  if (rAlarmUpdate.getIsActive()) 
  {
      
      switch(rAlarmUpdate.getCurrentUrgency())
      {
          case Alarm::LOW_URGENCY:
              AlarmDci::PutStatusF(rAlarmUpdate.getName(), AlarmDci::ALARM_DCI_LOW_URGENCY);
              break;
          case Alarm::MEDIUM_URGENCY:
              AlarmDci::PutStatusF(rAlarmUpdate.getName(), AlarmDci::ALARM_DCI_MEDIUM_URGENCY);
              break;
          case Alarm::HIGH_URGENCY:
              AlarmDci::PutStatusF(rAlarmUpdate.getName(), AlarmDci::ALARM_DCI_HIGH_URGENCY);
              break;
          default:
              AUX_CLASS_ASSERTION_FAILURE( rAlarmUpdate.getCurrentUrgency( ));
              break;
      }
    
  }
  else if ((rAlarmUpdate.getPreviousUrgency() != Alarm::NORMAL_URGENCY) &&
        (rAlarmUpdate.getCurrentUrgency() == Alarm::NORMAL_URGENCY)) // $[TI2]
  {
    AlarmDci::PutStatusF(rAlarmUpdate.getName(), AlarmDci::ALARM_DCI_RESET);
  }        
  else 
  {
    AlarmDci::PutStatusF(rAlarmUpdate.getName(), AlarmDci::ALARM_DCI_NORMAL);
  }

  
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpdateDciArray_
//
//@ Interface-Description
//   Private method to update the Dci array based on the passed parameter,
//   updateType.
//
// >Von
//   updateType  Specifies the cause of the update of alarms. 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   If the update is caused by the operator pressing the Manual Reset key,
//   reset all AlarmDci status entries which equal ALARM_DCI_RESET to 
//   ALARM_DCI_NORMAL.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmAnnunciator::UpdateDciArray_(Alarm::UpdateType updateType)
{
  CALL_TRACE("UpdateDciArray_(updateType)");

    // If this update is due to the operator pressing the Manual Reset key,
    // cycle through the AlarmDci array and set all ALARM_DCI_RESET entries to
    // ALARM_DCI_NORMAL.
    if (updateType == Alarm::MANUAL_RESET_UT) // $[TI1]
    {
      for (Uint32 ii = 0; ii < MAX_NUM_ALARMS; ii++)
      {
        if (AlarmDci::GetStatus((AlarmName)ii) == AlarmDci::ALARM_DCI_RESET) // $[TI1.1]
        {
          AlarmDci::PutStatus(((AlarmName)ii), AlarmDci::ALARM_DCI_NORMAL);
          AlarmRepository::SetStatus(((AlarmName)ii), Alarm::NORMAL_URGENCY);
        } // else $[TI1.2]

        if (AlarmDci::GetStatusF((AlarmName)ii) == AlarmDci::ALARM_DCI_RESET) // $[TI1.1]
        {
          AlarmDci::PutStatusF(((AlarmName)ii), AlarmDci::ALARM_DCI_NORMAL);
        } 

      }
    } // else $[TI2]
}


#  ifdef SIGMA_SIMULATOR
//============================ M E T H O D   D E S C R I P T I O N ====
// Method: GetAlarmSemaphore  
//
// Interface-Description
//   none
//---------------------------------------------------------------------
// Implementation-Description
//   none
//---------------------------------------------------------------------
// PreCondition
//   none
//---------------------------------------------------------------------
// PostCondition
//   none
// End-Method
//=====================================================================
Boolean
AlarmAnnunciator::GetAlarmSemaphore(void)
{
  CALL_TRACE("GetAlarmSemaphore(void)");

  if (AlarmAnnunciator::AlarmSemaphore_)
  {
    return(FALSE);
  }
  else
  {
    AlarmAnnunciator::AlarmSemaphore_ = TRUE;
    return(TRUE);
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: ReleaseAlarmSemaphore  
//
// Interface-Description
//   none
//---------------------------------------------------------------------
// Implementation-Description
//   none
//---------------------------------------------------------------------
// PreCondition
//   none
//---------------------------------------------------------------------
// PostCondition
//   none
// End-Method
//=====================================================================
void
AlarmAnnunciator::ReleaseAlarmSemaphore(void)
{
  CALL_TRACE("ReleaseAlarmSemaphore(void)");

  AlarmAnnunciator::AlarmSemaphore_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: DisplayAlarmLog   
//
// Interface-Description
//   none
//---------------------------------------------------------------------
// Implementation-Description
//   none
//---------------------------------------------------------------------
// PreCondition
//   none
//---------------------------------------------------------------------
// PostCondition
//   none
// End-Method
//=====================================================================
void
AlarmAnnunciator::DisplayAlarmLog(void)
{
  CALL_TRACE("DisplayAlarmLog(void)");

  FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES) alarmLogEntries;
  NovRamManager::GetAlarmLogEntries(alarmLogEntries);

  for (Uint32 ii = 0; ii < MAX_ALARM_ENTRIES; ii++)
  {
    const AlarmEvent alarmEvent( 
        alarmLogEntries[ii].getAlarmAction());

    cout << alarmEvent.getTimeStamp().getHour() << ":";
    Uint32 tempInt = alarmEvent.getTimeStamp().getMinutes();
    if (tempInt < 10) 
    {
      cout << "0";
    } 
    cout << tempInt << ":";
    tempInt = alarmEvent.getTimeStamp().getSeconds();
    if (tempInt < 10) 
    {
      cout << "0";
    } 
    cout << tempInt << " ";

    AlarmAnnunciator::DisplayUpdateType_(alarmEvent.getUpdateTypeMessage());

    cout << AlarmStrs::GetMessage(alarmEvent.getBaseMessage()) << " ";

    AlarmAnnunciator::DisplayUrgency_(alarmEvent.getUrgencyMessage());

    Uint32 tempIndex = alarmEvent.getAnalysisIndex();
    for (Uint32 j = 0; j < tempIndex; j++)
    {
      cout << 
          AlarmStrs::GetMessage(alarmEvent.getAnalysisMessage()[j]) 
          << " ";
    }
    cout << endl;
  } 
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: UpdateAlarmSummary_
//
// Interface-Description
//   none
//---------------------------------------------------------------------
// Implementation-Description
//   none
//---------------------------------------------------------------------
// PreCondition
//   none
//---------------------------------------------------------------------
// PostCondition
//   none
// End-Method
//=====================================================================
void
AlarmAnnunciator::UpdateAlarmSummary_(void)
{
  CALL_TRACE("UpdateAlarmSummary_(void)");

  cout << AlarmStrs::GetMessage(ALARM_SUMMARY_MSG);
  
  if (AlarmAnnunciator::GetHighestUrgency() == Alarm::NORMAL_URGENCY)
  {
    cout << AlarmStrs::GetMessage(NORMAL_URGENCY_MSG);
  }
  else
  {
    switch (AlarmAnnunciator::GetHighestUrgency())
    {
      case (Alarm::LOW_URGENCY):
        cout << AlarmStrs::GetMessage(LOW_URGENCY_MSG) << " ";
        break;
      case (Alarm::MEDIUM_URGENCY):
        cout << AlarmStrs::GetMessage(MEDIUM_URGENCY_MSG) << " ";
        break;
      case (Alarm::HIGH_URGENCY):
        cout << AlarmStrs::GetMessage(HIGH_URGENCY_MSG) << " ";
        break;
      case (Alarm::NORMAL_URGENCY):
        break;
      default:
        cout << AlarmStrs::GetMessage(ERROR_IN_URGENCY_MSG) << " ";
    }
  }

  if (AlarmAnnunciator::HighUrgencyAutoReset_ &&
     (AlarmAnnunciator::GetHighestUrgency() < Alarm::HIGH_URGENCY))
  {
    cout << " " << AlarmStrs::GetMessage(HIGH_URGENCY_AUTO_RESET_MSG);
  }

  cout << endl;
  cout << endl;
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: UpdateAlarmAudio_
//
// Interface-Description
//   none
//---------------------------------------------------------------------
// Implementation-Description
//   none
//---------------------------------------------------------------------
// PreCondition
//   none
//---------------------------------------------------------------------
// PostCondition
//   none
// End-Method
//=====================================================================
void
AlarmAnnunciator::UpdateAlarmAudio_(void)
{
  CALL_TRACE("UpdateAlarmAudio_(void)");

  cout << AlarmStrs::GetMessage(ALARM_AUDIO_MSG);
  if (AlarmAnnunciator::AlarmsAreSilenced_)
  {
  cout << AlarmStrs::GetMessage(ALARM_SILENCE_AUDIO_MSG);
  }
  else
  { 
    switch (AlarmAnnunciator::GetHighestUrgency())
    {
      case (Alarm::NORMAL_URGENCY):
        cout << AlarmStrs::GetMessage(NORMAL_URGENCY_AUDIO_MSG);
   break;
      case (Alarm::LOW_URGENCY):
        cout << AlarmStrs::GetMessage(LOW_URGENCY_AUDIO_MSG);
   break;
      case (Alarm::MEDIUM_URGENCY):
        cout << AlarmStrs::GetMessage(MEDIUM_URGENCY_AUDIO_MSG);
   break;
      case (Alarm::HIGH_URGENCY):
        cout << AlarmStrs::GetMessage(HIGH_URGENCY_AUDIO_MSG);
        break;
      default:
        cout << AlarmStrs::GetMessage(ERROR_IN_URGENCY_MSG);
      }
  }

  cout << endl;
}
#  endif // SIGMA_SIMULATOR

#endif // SIGMA_GUI_CPU
