/*****************************************************************************/
/*   Module Name:    72500.h                                             
/*   Purpose:      This module provides the function prototypes for the PTS2000                                
/*                 system which provides measurement of pressure, flow, Oxygen %                                                          
/*                 Barometric pressure code is in 72500.c  
/*                                                          
/*   Requirements:   This module implements requirements detailed in 72500-93          
/*                                                             
/*                                                                           
/*   $Revision::   1.1 $                                               
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _72500
#define _72500

/************** Defines ********************/
#define PTS_CONFIG_FILE   "c:/instcfg/72500.cfg"
#define COMERROR      1
#define FILEERROR     2
#define CAL_LENGTH   12
#define NAK         0x15
#define ACK         0x06

#define SERIAL_NUM_LENGTH  30 // size includes space for termination bytes
#define VERSION_NUM_LENGTH 30 // size includes space for termination bytes
#define CAL_DATE_LENGTH    30 // size includes space for termination bytes
#define SERIAL_NUM_LENGTH_VALID  12 // size includes space for termination bytes
#define VERSION_NUM_LENGTH_VALID 14 // size includes space for termination bytes
#define CAL_DATE_LENGTH_VALID    12 // size includes space for termination bytes


int GetPTS_CalDate(char *buffer);
int GetPTS_SoftwareRev(char *buffer);
int GetPTS_SerialID(char *buffer);
int IsNextCalOverdue(char *NextCalDate);
char *PTS_Revision(void);
int MeasureHighPressure(double *);
int MeasureHighFlow(double *);
int MeasureHighFlowAvg(double *);
int MeasureLowPressure(double *);
int MeasureLowFlow(double *);
int MeasureLowFlowAvg(double *);
int MeasureOxygen(double *);
int MeasureBarometricPressure(double *);
int MeasureLowFlowO2(double *);
int MeasureLowFlowO2Avg(double *);
int MeasureHighFlowO2(double *);
int MeasureHighFlowO2Avg(double *);
int ZeroHighPressure(void);
int ZeroLowPressure(void);

int SetBarPrsOn(void);
int SetPTSSmplRate(int rate);

extern char   szSerialNumber[SERIAL_NUM_LENGTH];      // default provided for simulator
extern char   szVersionNumber[VERSION_NUM_LENGTH]; // default provided for simulator
extern char   szCalDate[CAL_DATE_LENGTH];            // default provided for simulator
extern char   szNextCalDate[CAL_DATE_LENGTH];

#endif // #define _72500
