/*****************************************************************************/
/*   Module Name:     tt.c                                              
/*   Purpose:      Functions for creating, displaying, and removing tooltips.                                             
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Modified InitTips to not allocate      Dave Richardson   23 Nov, 2002
/*   and abandon memory
/*
/*   $Revision::   1.2      $                                                
/*
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/tooltips/tt.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:37:10   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   23 Nov 2002 12:26:22   Richardson
 * Updated per SCR # 31

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <formatio.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#include "cpvtmain.h" // PANELS, gPanelHandle[PANELS];
#include "ldb.h"
#include "mainpnls.h"
#include "tt.h"

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

ListType TipList = NULL;

static TipListNode LastTip;
static int CurrentPanelConstant = -1;
static int CurrentToolTipPanel;
static int PANEL_ToolTipTimer;
static int ToolTip_Cntrl_ID;
static int MyPanel;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:    ToolTipItemPtrCmpFn	                                                   
/*                                                                           
/* Purpose:  Compare function for use by the CVI list structure
/*                                                                           
/* Example Call:   static int ToolTipItemPtrCmpFn(void *itemA, void *itemB);                                    
/*                                                                           
/* Input:    itemA - First item to compare
/*			 itemB - Second item to compare
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static int ToolTipItemPtrCmpFn(void *itemA, void *itemB)
{
	int ok;

	ok = !((((TipListNode*)itemA)->CtrlID==((TipListNode*)itemB)->CtrlID) && 
	      (((TipListNode*)itemA)->PanelID==((TipListNode*)itemB)->PanelID));

	return ok;
}


/*****************************************************************************/
/* Name:     SwitchPanels	                                                      
/*                                                                           
/* Purpose:  Switchs the tooltip timer to another panel   
/*                                                                           
/* Example Call:   void SwitchPanels(int Newpanel);                                    
/*                                                                           
/* Input:    Newpanel - Handle of the panel to switch to          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SwitchPanels(int Newpanel)
{
	int lastbreakstate; // library errors break-state

	//This may cause some library errors so let's shut them off
	lastbreakstate = SetBreakOnLibraryErrors(FALSE);
	if(MyPanel != -1 && ToolTip_Cntrl_ID != -1){
		DiscardPanel(MyPanel);
	}
	MyPanel = -1;

	//now return the library errors to the previous state
	SetBreakOnLibraryErrors(lastbreakstate);

	ToolTip_Cntrl_ID = 0;
	LastTip.PanelID  = 0;
	LastTip.CtrlID   = 0;
	DiscardCtrl(CurrentToolTipPanel, PANEL_ToolTipTimer);

	CurrentToolTipPanel = Newpanel;
	PANEL_ToolTipTimer = NewCtrl(Newpanel, CTRL_TIMER, "ToolTipTimer", 0, 0);
	SetCtrlAttribute(Newpanel, PANEL_ToolTipTimer, ATTR_INTERVAL, 1.0);
	SetCtrlAttribute(Newpanel, PANEL_ToolTipTimer, ATTR_CALLBACK_FUNCTION_POINTER, ToolTipTimer);
	//we don't know the panel constant so flag it as such
	CurrentPanelConstant = -1;

	return;
}	

/*****************************************************************************/
/* Name:     SwitchPanelsWithConst	                                                      
/*                                                                           
/* Purpose:  Switchs the tooltip timer to another panel and specifies the panel constant   
/*                                                                           
/* Example Call:   void SwitchPanelsWithConst(int Newpanel, int NewPanelConst);                                    
/*                                                                           
/* Input:    Newpanel - Handle of the panel to switch to
/*			 NewPanelConst - Constant of the panel to switch to
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SwitchPanelsWithConst(int Newpanel, int NewPanelConst)
{
	SwitchPanels(Newpanel);
	CurrentPanelConstant = NewPanelConst;

	return;
}	


/*****************************************************************************/
/* Name:     InitTips	                                                     
/*                                                                           
/* Purpose:  Sets up the tool tip list and creates the initial timer 
/*                                                                           
/* Example Call:  void InitTips(int panel);                                    
/*                                                                           
/* Input:    panel - initial panel to start tooltips on          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void InitTips(int panel)
{
	CurrentToolTipPanel = panel;
	PANEL_ToolTipTimer = NewCtrl(panel, CTRL_TIMER, "ToolTipTimer", 0, 0);
	SetCtrlAttribute(panel, PANEL_ToolTipTimer, ATTR_INTERVAL, 1.0);
	SetCtrlAttribute(panel, PANEL_ToolTipTimer, ATTR_CALLBACK_FUNCTION_POINTER, ToolTipTimer);
	TTStringInsert();

	return;
}


/*****************************************************************************/
/* Name:     PauseTips	                                                      
/*                                                                           
/* Purpose:  Disables ToolTip Timers
/*                                                                           
/* Example Call:   PauseTips();                                    
/*                                                                           
/* Input:    <none>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void PauseTips(void)
{
	//Disable standard timer
	SetCtrlAttribute(CurrentToolTipPanel, PANEL_ToolTipTimer, ATTR_ENABLED, FALSE);

	//Disable secondary timer on the syspanel
	if(gPanelHandle[SYSPANEL] != 0){
		SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_SYS_TT_TIMER, ATTR_ENABLED, FALSE);
	}

	//If a tooltip is being displayed remove it!
	if(ToolTip_Cntrl_ID != 0){
		DiscardCtrl(MyPanel, ToolTip_Cntrl_ID);
		ToolTip_Cntrl_ID = 0;
		DiscardPanel(MyPanel);
		MyPanel = -1;
	}

	return;
}


/*****************************************************************************/
/* Name:     UnPauseTips	                                                      
/*                                                                           
/* Purpose:  Reenables ToolTip Timers
/*                                                                           
/* Example Call:   UnPauseTips();                                    
/*                                                                           
/* Input:    <none>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void UnPauseTips(void)
{
	//Enable standard timer
	SetCtrlAttribute(CurrentToolTipPanel, PANEL_ToolTipTimer, ATTR_ENABLED, TRUE);

	//Enable secondary timer on the syspanel
	if(gPanelHandle[SYSPANEL] != 0){
		SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_SYS_TT_TIMER, ATTR_ENABLED, TRUE);
	}

	return;
}


/*****************************************************************************/
/* Name:     MatchTip	                                                      
/*                                                                           
/* Purpose:  Finds the control which belongs to panel and contains the coordinates
/*		x and y and returns it's handle.
/*                                                                           
/* Example Call:   int MatchTip(int panel, int x, int y);                                    
/*                                                                           
/* Input:    panel - The panel handle to match the coordinates on
/*			 x - x coordinate to match
/*			 y - y coordinate to match
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ID of the control which contains the given x&y coordinates
/*			 -1 if no control found
/*                                                                           
/*****************************************************************************/
int MatchTip(int panel, int x, int y)
{
	int i;
	int ctrl_id;
	int num_ctrls;
	int cx, cy, ch, cw;

	GetPanelAttribute(panel, ATTR_PANEL_FIRST_CTRL, &ctrl_id);
	GetPanelAttribute(panel, ATTR_NUM_CTRLS,        &num_ctrls);
	for(i = 0; i < num_ctrls; i++){
		GetCtrlBoundingRect(panel, ctrl_id, &cy, &cx, &ch, &cw);
		if(x >= cx && x <= (cx+cw) && y >= cy && y <= (cy+ch)){
			//printf("%d %d; %d %d; %d\n", x, y, cx, cy, ctrl_id);
			return ctrl_id;
		}
		GetCtrlAttribute(panel, ctrl_id, ATTR_NEXT_CTRL, &ctrl_id);
	}

	return -1;
}


/*****************************************************************************/
/* Name:     GetPanelConst                                                    
/*                                                                           
/* Purpose:  Finds the index to gPanelHandle for the selected panel 
/*                                                                           
/* Example Call:  int GetPanelConst(int panel);                                    
/*                                                                           
/* Input:    panel - panel handle of the panel we want to constant for          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   requested panel constant or -1 if not found                                                         
/*                                                                           
/*****************************************************************************/
int GetPanelConst(int panel)
{
	int i;

	if(CurrentPanelConstant != -1){
		return CurrentPanelConstant;
	}
	for(i = 0; i < PANELS; i++)
	{
		if(gPanelHandle[i] == panel){
			return i;
		}
	}

	return -1; // panel record not found
}																	  


/*****************************************************************************/
/* Name:     ToolTipTimer                                                      
/*                                                                           
/* Purpose:  timer callback which displays and discards tooltips
/*                                                                           
/* Example Call:   int CVICALLBACK ToolTipTimer(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK ToolTipTimer(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int top, left, height, width;
	int TIP;
	int i, j;
	int PanelWidth, PanelHeight, visible;
	int NewWidth, NewHeight;
	TipListNode MatchThisTip;
	TipListNode *GotThisTip;
	int ListPos;

	static int mouse_coord_y     = 0;
	static int mouse_coord_x     = 0;
	static int old_mouse_coord_y = 0;
	static int old_mouse_coord_x = 0;

	GetPanelAttribute(panel, ATTR_VISIBLE, &visible);
	if(!visible){
		return 0;
	}

	switch(event){
		case EVENT_TIMER_TICK:
			GetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_VISIBLE, &visible);

			if(visible && CurrentPanelConstant == -1){
				GetRelativeMouseState(gPanelHandle[BASEPANEL], 0, &mouse_coord_x, &mouse_coord_y, NULL, NULL, NULL);
			}
			else{
				GetRelativeMouseState(panel,                   0, &mouse_coord_x, &mouse_coord_y, NULL, NULL, NULL);
			}

			if(	(abs(mouse_coord_x - old_mouse_coord_x) <= TOOLTIP_BOUND) &&
			    (abs(mouse_coord_y - old_mouse_coord_y) <= TOOLTIP_BOUND) )
				{
				TIP = MatchTip(panel, mouse_coord_x, mouse_coord_y);
				if(TIP >= 0){
					GetCtrlAttribute(panel, TIP, ATTR_VISIBLE, &visible);
				}
				if(TIP + 1 && visible){
					MatchThisTip.CtrlID = TIP;
					MatchThisTip.PanelID = GetPanelConst(panel);
					ListPos = ListFindItem(TipList, &MatchThisTip, FRONT_OF_LIST, ToolTipItemPtrCmpFn);
					if(ListPos != 0){
						GotThisTip = ListGetPtrToItem(TipList, ListPos);
						if( GotThisTip->CtrlID  != LastTip.CtrlID &&
						    GotThisTip->PanelID != LastTip.PanelID)
							{
							LastTip.PanelID = GotThisTip->PanelID;
							LastTip.CtrlID  = GotThisTip->CtrlID;
							MyPanel = NewPanel(panel, "", 0, 0, 0, 0);
						    SetPanelAttribute(MyPanel, ATTR_FRAME_STYLE,      VAL_OUTLINED_FRAME);
						    SetPanelAttribute(MyPanel, ATTR_ACTIVATE_WHEN_CLICKED_ON, FALSE);
							SetPanelAttribute(MyPanel, ATTR_BACKCOLOR,        0x00FFFFAA);
							SetPanelAttribute(MyPanel, ATTR_TITLEBAR_VISIBLE, FALSE);
						   	SetPanelAttribute(MyPanel, ATTR_SIZABLE,          FALSE);
							ToolTip_Cntrl_ID = NewCtrl(MyPanel, CTRL_TEXT_MSG, GotThisTip->CtrlStr, 0, 0);
							SetCtrlAttribute(MyPanel, ToolTip_Cntrl_ID, ATTR_TEXT_BGCOLOR, VAL_TRANSPARENT);
							SetCtrlAttribute(panel, control, ATTR_INTERVAL, 0.1);
							GetCtrlBoundingRect(MyPanel, ToolTip_Cntrl_ID, &top, &left, &height, &width);
							GetPanelAttribute(panel, ATTR_WIDTH, &PanelWidth);
							GetPanelAttribute(panel, ATTR_HEIGHT, &PanelHeight);
							if((mouse_coord_x + (width / 2)) > PanelWidth){
								NewWidth = PanelWidth - width - 5;
							}
							else if((mouse_coord_x - (width / 2)) < 0){
								NewWidth = 5;
							}
							else{
								NewWidth = mouse_coord_x - (width / 2);
							}
							NewHeight = mouse_coord_y + 20;
							if(NewHeight + 20 > PanelHeight){
								NewHeight = mouse_coord_y - 30;
							}
							SetPanelPos(MyPanel, NewHeight, NewWidth);
							SetPanelAttribute(MyPanel, ATTR_ZPLANE_POSITION, 0);
							SetPanelSize(MyPanel, height, width);
							SetPanelAttribute(MyPanel, ATTR_VISIBLE, TRUE);
						} // end if( GotThisTip->CtrlID  != LastTip.CtrlID && ...
					} // end if(ListPos != 0)
				} // end if(TIP + 1 && visible)
			} // end if(	(abs(mouse_coord_x - old_mouse_coord_x) <= TOOLTIP_BOUND) && ...
			else{
				if(ToolTip_Cntrl_ID != 0){
					DiscardCtrl(MyPanel, ToolTip_Cntrl_ID);
					LastTip.PanelID  = -1;
					LastTip.CtrlID   = -1;
					ToolTip_Cntrl_ID =  0;
					DiscardPanel(MyPanel);
					MyPanel = -1;
					SetCtrlAttribute(panel, control, ATTR_INTERVAL, 1.0);
				}
			}
			old_mouse_coord_x = mouse_coord_x;
			old_mouse_coord_y = mouse_coord_y;
		break; // case EVENT_TIMER_TICK:
	} // end switch(event)

	return 0;
}

