//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file XMLUtil.cpp
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"XMLUtil.h"
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOM.hpp>


//-------------------------------------
// C O D E
//-------------------------------------

namespace XMLUtil {

using namespace XERCES_CPP_NAMESPACE;

//---------------------------
// loadXMLDocument 
//---------------------------
DOMDocument *loadXMLDocument (char *filename) {

    DOMDocument *doc = NULL;

    static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
    DOMImplementation *impl =
        DOMImplementationRegistry::getDOMImplementation (gLS);
    DOMLSParser *parser =
        ((DOMImplementationLS *)
         impl)->createLSParser (DOMImplementationLS::MODE_SYNCHRONOUS, 0);
    DOMConfiguration *config = parser->getDomConfig ();

    config->setParameter (XMLUni::fgXercesSchema, true);
    config->setParameter (XMLUni::fgXercesSchemaFullChecking, true);

    config->setParameter (XMLUni::fgXercesUserAdoptsDOMDocument, true);

    if (config->canSetParameter (XMLUni::fgXercesDoXInclude, true)) {
        config->setParameter (XMLUni::fgXercesDoXInclude, true);
    }

    try {
        parser->resetDocumentPool ();
        doc = parser->parseURI (filename);
    }
#if 0
    catch (const XMLException & toCatch) {
        parser->release ();
        throw;
    }
    catch (const DOMException & toCatch) {
        parser->release ();
        throw;
    }
#endif
    catch (...) {
        parser->release ();
        throw;
    }

    parser->release ();

    return doc;
}






//---------------------------
// saveXMLDocument 
//---------------------------
int saveXMLDocument (char *filename, DOMNode * doc) {

    static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
    DOMImplementation *impl =
        DOMImplementationRegistry::getDOMImplementation (gLS);


    DOMLSSerializer *writer =
        ((DOMImplementationLS *) impl)->createLSSerializer ();
    DOMConfiguration *config = writer->getDomConfig ();

    config->setParameter (XMLUni::fgDOMWRTFormatPrettyPrint, true);


    DOMLSOutput *theOutputDesc =
        ((DOMImplementationLS *) impl)->createLSOutput ();


    try {
        XMLFormatTarget *myFormTarget =
            new LocalFileFormatTarget (filename);
        theOutputDesc->setByteStream (myFormTarget);
        writer->write (doc, theOutputDesc);
        delete myFormTarget;
    }
#if 0
    catch (const XMLException & toCatch) {
        writer->release ();
        theOutputDesc->release ();
        throw;
    }
    catch (const DOMException & toCatch) {
        writer->release ();
        theOutputDesc->release ();
        throw;
    }
#endif
    catch (...) {
        writer->release ();
        theOutputDesc->release ();
        throw;
    }
    writer->release ();
    theOutputDesc->release ();

	//TODO[ME]:Should return status.
	return 0;
}

//---------------------------
// forEachXpath 
//---------------------------
int forEachXpath (char *xpath, DOMDocument * doc,
                  int (*action) (DOMNode * ptr, void *t),
                  void *action_object) {
    XMLCh *xpathStr = XMLString::transcode (xpath);

    if (doc == NULL)
        return -1;

    DOMElement *root = doc->getDocumentElement ();

    if (root) {
        try {
            DOMXPathNSResolver *resolver = doc->createNSResolver (root);
            DOMXPathResult *result = doc->evaluate (xpathStr,
			    root,
			    resolver,
			    DOMXPathResult::ORDERED_NODE_SNAPSHOT_TYPE,
			    NULL);

            XMLSize_t nLength = result->getSnapshotLength ();

            for (XMLSize_t i = 0; i < nLength; i++) {
                result->snapshotItem (i);
                action (result->getNodeValue (), action_object);
            }

            result->release ();
            resolver->release ();
        }
#if 0
        catch (const DOMXPathException & e) {
            XMLString::release (&xpathStr);
            throw;
        }
        catch (const DOMException & e) {
            XMLString::release (&xpathStr);
            throw;
        }
#endif
        catch (...) {
            XMLString::release (&xpathStr);
            throw;
        }
    }
    XMLString::release (&xpathStr);
	
	//TODO[ME]:Should return status.
	return 0;

}


}
