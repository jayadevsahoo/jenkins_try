#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class: GuiApp - Main subsystem class, which contains the globals
//  used by the subsystem.
//---------------------------------------------------------------------
//@ Interface-Description
// This is the main GUI-Applications subsystem interface class.  The GUI-Apps
// subsystem must be initialized via the Initialize() method.
//
// Alarm events are communicated by the Alarm-Analysis subsystem via
// AlarmEventHappened().  GUI-Applications can inquire about the settings
// lockout state by calling IsSettingsLockedOut().
//
// Normal ventilation mode is signalled via a call to BeginNormalVentilation().
// Service mode is signalled via a call to BeginServiceMode().
//
// GuiApp also contains the main loop code for running the GUI-Applications
// task on the target system. The method TaskExecutor() looks after reading
// events off the User Annunciation queue, passing the events to the
// appropriate parties and repainting the Upper and Lower screens after
// each event.  The loop must also make an "I'm alive" call to prevent the
// watchdog timer from resetting the system.  The method TaskScheduler() looks
// after reading events off the Gui App Scheduler queue, passing the events
// to the appropriate parties and process the highest priority gui event.
// The loop must also make an "I'm alive" call to prevent the watchdog timer
// from resetting the system.  The method TaskBreathBar() looks
// after reading events off the Gui App Breath Bar queue, passing the events
// to the appropriate parties and update the breath bar area with the
// lattest breath type and breath phase.  The loop must also make an
// "I'm alive" call to prevent the watchdog timer from resetting the system.
//
// Several public data members provide direct access to key objects in
// GUI-Apps.  Among them, the Message and Prompt Areas are shared by both
// normal ventilation and service mode.  The Upper and Lower Screen are only
// accessible by normal ventilation mode.  The ServiceUpper and ServiceLower
// Screen are only accessible by service mode.  These members are accessed
// continually from many different objects so it was deemed very convenient
// to have them public accessible.
//
// The UserActivityHappened() method is called every time user activity occurs
// (screen touches, key presses, knob turns).  It resets the timers for timing
// out setting changes (specifically, the timers are the Subscreen Setting
// Change and Main Setting Change timers).  Objects which need to react to
// these timers simply register and start these timers.  The timers are
// reset every time user activity occurs.  If a timer expires then the
// registered object will be informed.  A registered object should ignore
// this event if that object is not visible on-screen.
//---------------------------------------------------------------------
//@ Rationale
// An entry point for initializing GUI-Applications and passing events
// from other subsystems was needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes the "global" screen areas and displays them.  Creates the
// key handlers which react to some of the offscreen key press/release
// events.  The screen areas include the Upper and Lower screens for normal
// ventilation mode and Service Upper and ServiceLower screens for service
// mode.
//
// After initialization, the primary purpose of this class is to act as
// the main task for GUI-Applications.  The TaskExecutor() method simply reads 32-bit
// queue messages off the User Annunciation queue and, depending on the
// event type (specified as part of the message), determines the appropriate
// action.
//
// This class is important at startup of the ventilator as it is the passed
// the startup condition flag which tells it which mode the GUI should begin
// in, e.g. whether it should switch to Service mode, display the Vent
// Startup subscreen or, in the case of power-fail recovery, jump straight
// to normal ventilation if there is a patient setting available.
// $[BL00400]
//---------------------------------------------------------------------
//@ Fault-Handling
// Usual assertion to verify code correctness.
//---------------------------------------------------------------------
//@ Restrictions
// The Initialize() method should only be called once.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiApp.ccv   25.0.4.0   19 Nov 2013 14:07:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 115  By: mnr   Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      PROX Revision/Serial Num related updates.
//
//  Revision: 114   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API. Upon receiving VSET_COMMAND_EVENT
//  from a serial port task, it calls the VsetServer to process the
//  incoming VentSet command at GuiApp task priority.
//
//  Revision: 113 By:  erm    Date: 5-May-2009    SCR Number: 6498
//  Project:  840S
//  Description:
//  Removed isTreadReady var
//
//  Revision: 112 By:  erm    Date: 15-Apr-2009    SCR Number: 6498
//  Project:  840S
//  Description:
//    Allow PatientDataMgr::ActivateWaveform after TrendMgr is ready
//
//  Revision: 111  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 110  By: gdc     Date:  26-May-2007    SCR Number: 6330
//  Project:  Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 109  By: gdc     Date:  26-May-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//  Trend related changes. Added PrintTarget processing to allow for
//  printing Trend sub-screen as well as Waveforms.
//
//  Revision: 108  By: gdc     Date:  19-Jul-2006    DCS Number: 6115
//  Project:  RESPM
//  Description:
//	Added repaint() after removeAllDrawables() call in BeginServiceMode()
//  to clear changed drawables list in BaseContainers before
//  constructing Service-Mode sub-screens in the same memory used by
//  normal ventilation sub-screens. Added bzero of ScreenMemory before
//  constructing service-mode sub-screens since constructors assume
//  objects are constructed in memory initialized to zero (zerovars).
//  This was not the case when service-mode screens are constructed
//  in the same memory as normal ventilation screens during the transition
//  to SST.
//
//  Revision: 107  By: quf     Date:  10-Oct-2001    DCS Number: 5970
//  Project:  GUIComms
//  Description:
//	Fixed usage of START_DELTA_CODE, END_DELTA_CODE, and DELTA_CODE
//	comments.
//
//  Revision: 106  By: hlg     Date:  01-Oct-2001    DCS Number: 5966
//  Project:  GUIComms
//  Description:
//	Added mapping to SRS print requirements.
//
//  Revision: 105  By: quf     Date:  13-Sep-2001    DCS Number: 5493
//  Project:  GUIComms
//  Description:
//	Print implementation changes:
//	- Cleaned up the print cancel and print done mechanisms.
//
//  Revision: 104   By: gdc    Date:  04-Jan-2001    DCS Number: 5493
//  Project:  GuiComms
//  Description:
//  Modified for new TouchDriver interface.
//
//  Revision: 103   By: hct    Date:  03-MAY-2000    DCS Number: 5493
//  Project:  GUIComms
//  Description:
//	GUIComms initial revision.
//
//  Revision: 102   By: hhd    Date:  30-Mar-2000    DCS Number: 5675
//  Project: Neo-Mode
//  Description:
// 	Changed method IsDataKeyInstall() into public.
//
//  Revision: 101   By: yyy    Date:  21-Oct-1999    DCS Number: 5557
//  Project: ATC
//  Description:
//	Clear GuiFoundation and user annunciation queque when transitioning
//  from normal ventilation to service mode.
//
//  Revision: 100   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle. Decreased waveform plot
//  cycles for smoother plotting as the waveforms plotting no longer
//  "locks" the display to the detriment of breath-bar drawing. Waveforms
//  now release the display whenever a Direct Draw mode is requested.
//
//  	17-Nov-1999 (hhd) Modified to reduce header file dependency in GuiApp subsystem.
//
//  Revision: 099  By: gdc    Date: 22-Jul-1999    DR Number: 5129
//  Project:  CostReduce-1
//  Description:
//      Changed to process all alarm annunciations prior to periodic
//		tasks. Added gating mechanism for SCREEN_TOUCH events as well
//		as SCREEN_RELEASE events.
//
//  Revision: 098  By: clw    Date: 04-Mar-1999    DR Number: 5346
//  Project:  ATC
//  Description:
//      Modfied BeginNormalVentilation() for BTREE_TEST only.
//      This ensures that on exit, the last command to the SAAS is "cancel alarm".
//
//  Revision: 097  By: healey    Date: 08-Feb-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Initial ATC revision.
//
//  Revision: 096  By: sah    Date: 07-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 095  By: gdc    Date: 22-Jul-1999    DR Number: 5129
//  Project:  806 Compressor
//  Description:
//      Changed to process all alarm annunciations prior to periodic
//      tasks. Added gating mechanism for SCREEN_TOUCH events as well
//      as SCREEN_RELEASE events.
//
//  Revision: 094  By:  syw   Date:  19-Nov-1998    DR Number: 5218
//       Project:  BiLevel
//       Description:
//			Added DATAKEY_INVALID state if isOptionsFieldOkay() is FALSE.
//			Eliminate isSetFlashSerialNumber() logic in SetConfigurationAgentFields_().
//
//  Revision: 093  By:  syw   Date:  16-Nov-1998    DR Number: 5252
//       Project:  BiLevel
//       Description:
//			Added requirement tracing.
//
//  Revision: 092  By: syw      Date: 27-Oct-1998  DR Number: 5215
//    Project:  BiLevel
//    Description:
//		Changed ConfigurationCode_ to VentilatorOptions_.  Display option as hex field.
//
//  Revision: 028  By: yyy      Date: 05-Jan-1998  DR Number:
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/GuiApp.ccv   1.167.1.0   07/30/98 10:13:22   gdc
//	   Eliminated use of demo data key type...  Use SoftwareOptions class instead.
//	   Added storage for configurationCode to be displayed under vent config.
//	   Eliminated SetSerialNumbersMatchedFlag and data members since it has no effect.
//	   Modified logic when to verify serial number matching.
//
//  Revision: 027  By:  gdc	   Date:  29-Jun-1998    DCS Number:
//  Project:  Color
//  Description:
//		Initial version.
//
//   Revision 026   By:  gdc    Date: 22-Jan-1998        DR Number:  5002
//   Project:   Sigma   (R8027)
//   Description:
//       Added calls to set the nurse's call to the correct state on
//       entry to normal ventilation and service modes.
//
//   Revision 025   By:  sah    Date: 15-Jan-1998        DR Number:  5004
//   Project:   Sigma   (R8027)
//   Description:
//       Added a call to 'AlarmStrs::Initialize()'.
//
//  Revision: 024 By:  yyy    Date:  21-Nov-97    DR Number: 2634
//    Project:  Sigma (R8027)
//    Description:
//      Fixed the reversed logic error for demokey checking.  Removed all code
//		related to updating the Gui NovRam serial number.
//
//  Revision: 023 By:  yyy    Date:  26-SEP-97    DR Number: 2410
//    Project:  Sigma (R8027)
//    Description:
//      Updated access method for pressure transducer calibrationtest due to changes
//		in service mode.
//
//  Revision: 022 By:  yyy    Date:  25-SEP-97    DR Number: 2522
//    Project:  Sigma (R8027)
//    Description:
//      Pass the transaction successful message to interested subscreen before test starts.
//
//  Revision: 021  By:  yyy    Date:  24-SEP-1997    DR Number:   1861
//    Project:  Sigma (R8027)
//    Description:
//      Updated access method for vent inop test due to changes in service
//		mode.
//
//  Revision: 020  By:  yyy    Date:  24-SEP-1997    DR Number:   2385
//    Project:  Sigma (R8027)
//    Description:
//      Added methods for flow sensor calibration status due to changes in service
//		mode.
//
//  Revision: 019  By:  yyy    Date:  10-Sep-97    DR Number: 2289
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated DEMO datakey type handle.
//
//  Revision: 018  By:  yyy    Date:  10-Sep-97    DR Number: 1923
//    Project:  Sigma (R8027)
//    Description:
//      Changed the name of communications Diagnostic log to the system
//		information log.
//
//  Revision: 017  By:  yyy    Date:  08-Sep-1997    DCS Number: 2269
//  Project:  Sigma (R8027)
//  Description:
//      Used REAL_TIME_CLOCK_UPDATE the newly added callbacks registered for real-time
//		clock changes to update the timer.
//
//  Revision: 016  By:  yyy    Date:  08-Sep-1997    DCS Number: 2401
//  Project:  Sigma (R8027)
//  Description:
//		Added eventStatus as the second parameter for ChangeHappened().
//
//  Revision: 015  By:  yyy    Date:  28-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Recover revision 12.
//
//  Revision: 014  By:  yyy    Date:  28-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      undo revision 12 per "7C" release.
//
//  Revision: 013  By:  sah    Date:  20-AUG-1997    DCS Number: 2379
//  Project:  Sigma (R8027)
//  Description:
//      Increased period for alarm updates.  Add Method to set the
//		'IsAlarmUpdateNeeded_' flag, so that the flag can be set when
//		an Alarm History Log update notification is received on the
//		queue.
//
//  Revision: 012  By:  yyy    Date:  04-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Registered for NovRamEvent  to capture the DATE/TIME setting change
//		events and update the date/time accordingly.
//
//  Revision: 011  By:  yyy    Date:  30-JUL-97    DR Number: 2279
//    Project:  Sigma (R8027)
//    Description:
//      Added IsFlowSensorInfoRequired(), SetFlowSensorInfoRequiredFlag()
//		methods to parse the flow sensor information.
//
//  Revision: 010  By:  yyy    Date:  30-JUL-97    DR Number: 2313
//    Project:  Sigma (R8027)
//    Description:
//      30-JUL-97  Added IsTauCharacterizationRequired(), SetTauCharacterizationRequiredFlag()
//		methods to parse the tau characterization information.
//		13-AUG-97  Removed all references for TauCharacterization
//
//  Revision: 009  By:  sah    Date:  01-Aug-1997    DCS Number: 2017
//  Project:  Sigma (R8027)
//  Description:
//      Added IsExhValveCalSuccessful(), SetExhValveCalSuccessfulFlag(),
//		methods to parse the exh. valve status information.
//
//  Revision: 008  By:  sah    Date:  28-Jul-1997    DCS Number: 2320
//  Project:  Sigma (R8027)
//  Description:
//      Added 'AlarmStateManager' to take over the processing of alarm
//		events.  Also, increased the rate of alarm processing to every
//		200ms (from 400ms).
//
//  Revision: 007  By:  yyy    Date:  09-Jun-97    DR Number: 2202
//		Project:  Sigma (R8027)
//		Description:
//			Removed all referenced to IsPowerFailure...., uses
//			IsVentStartupSequenceCompleted to detect the short power
//			failure.
//
//  Revision: 006  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision:  005  By: yyy   Date:  22-May-1997      DR Number: DCS 2151
//    Project:    Sigma (R8027)
//    Description:
//      Registered the compressor and operational time with NovRam update
//		manager to enable an realtime update of the hours.
//
//  Revision: 004  By: yyy      Date: 21-May-1997  DR Number: 1775,1803,1880,1903,1917
//    Project:  Sigma (R8027)
//    Description:
//      The flag IsAlarmUpdateNeeded_ used to be updated whenever the Alarm
//		post an alarm message to GuiApp's queue.  However, since Alarm subsystem
//		posts an alarm message for each breath no matter there is an alarm or
//		not.  This results an Alarm related subscreens being updated every time a
//		new breath being generated.  Now, we only update the IsAlarmUpdateNeeded_
//		when the AlarmAnnunciator tells the GuiApp to do so.
//		For DCS 1917, this change fixed the first item of that DCS.
//
//  Revision: 003  By: yyy      Date: 15-May-1997  DR Number: 2113
//    Project:  Sigma (R8027)
//    Description:
//      Added SRS 05011, 05014 mapping.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2006
//    Project:  Sigma (R8027)
//    Description:
//      Added calls to Sound::SetAlarmVolume() with the accepted alarm volume
//		settings as default alarm volume level.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

//
// Sigma includes.
//

#include "AlarmRender.hh"
#include "AlarmStatusCapture.hh"
#include "AppContext.hh"
#include "BDIORefs.hh"
#include "BdEventRegistrar.hh"
#include "BdGuiEvent.hh"
#include "BdReadyMessage.hh"
#include "BigSerialNumber.hh"
#include "BoundStrs.hh"
#include "ChangeStateMessage.hh"
#include "CommDownMessage.hh"
#include "Configuration.hh"
#include "ConfigurationAgent.hh"
#include "DataKeyAgent.hh"
#include "GuiApp.hh"
#include "GuiEventRegistrar.hh"
#include "GuiIo.hh"
#include "GuiSchedulerMsg.hh"
#include "GuiTimerRegistrar.hh"
#include "InitScreen.hh"
#include "IpcIds.hh"
#include "KeyHandlers.hh"
#include "LanguageValue.hh"
#include "LaptopEventRegistrar.hh"
#include "LowerScreen.hh"
#include "LowerSubScreenCapture.hh"
#include "MailBox.hh"
#include "MiscStrs.hh"
#include "ModeSettingsCapture.hh"
#include "MsgQueue.hh"
#include "MutEx.hh"
#include "NovRamEventRegistrar.hh"
#include "OsFoundation.hh"
#include "OsUtil.hh"
#include "PDTimerRegistrar.hh"
#include "PatientDataCapture.hh"
#include "PatientDataId.hh"
#include "PatientDataMgr.hh"
#include "PatientDataRegistrar.hh"
#include "Post.hh"
#include "PrintTarget.hh"
#include "SerialNumber.hh"
#include "ServiceDataMgr.hh"
#include "ServiceDataRegistrar.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceMode.hh"
#include "ServiceUpperScreen.hh"
#include "SettingsCapture.hh"
#include "Task.hh"
#include "TaskControl.hh"
#include "TaskControlAgent.hh"
#include "TaskControlQueueMsg.hh"
#include "TaskMonitor.hh"
#include "TaskMonitorQueueMsg.hh"
#include "TextUtil.hh"
#include "TrendEventRegistrar.hh"
#include "UpperScreen.hh"
#include "UserAnnunciationMsg.hh"
#include "VgaGraphicsDriver.hh"
#include "VsetServer.hh"
#include "WaveformsCapture.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "AlarmAnnunciator.hh"
#include "AlarmLogSubScreen.hh"
#include "AlarmStateManager.hh"
#include "AlarmStrs.hh"
#include "BaseContainer.hh"
#include "HundredPercentO2Handler.hh"
#include "KeyPanel.hh"
#include "Led.hh"
#include "MemoryStructs.hh"
#include "MiscBdEventHandler.hh"
#include "MoreAlarmsSubScreen.hh"
#include "SettingContextHandle.hh"
#include "SoftwareOptions.hh"
#include "Sound.hh"
#include "Subject.hh"
#include "Touch.hh"
#include "TouchDriver.hh"
#include "WaveformsSubScreen.hh"
#include "TimeoutSubScreen.hh"
#include <string.h>
#include <windows.h>
#include <tchar.h>
#include "StringConverter.hh"
//@ End-Usage


Boolean IsBdEventUpdateNeeded_;
Boolean IsTimerEventUpdateNeeded_;

//void ClearEvent_(MsgQueue &userAnnunciationQ);

//=====================================================================
//
//		Static Data...
//
//=====================================================================

// Uint boundary-aligned memory pools for static members.

Uint ScreenMemory[(sizeof (ScreenMemoryUnion) + sizeof (Uint) - 1)
												/ sizeof (Uint)];

static Uint KeyHandlersMemory[(sizeof(KeyHandlers) + sizeof(Uint) - 1)
												 / sizeof(Uint)];
static Uint MiscBdEventHandlerMemory[(sizeof(MiscBdEventHandler)
										 + sizeof(Uint) - 1) / sizeof(Uint)];

// Initializations of static member data.
KeyHandlers&	GuiApp::RKeyHandlers = *((KeyHandlers *) KeyHandlersMemory);
MiscBdEventHandler&	GuiApp::RMiscBdEventHandler = *((MiscBdEventHandler *) MiscBdEventHandlerMemory);
PromptArea*		GuiApp::PPromptArea = NULL;
MessageArea*	GuiApp::PMessageArea = NULL;
GuiApp::GuiDisplayMode GuiApp::MajorScreenMode_ = GuiApp::INITIALIZING;
SigmaState GuiApp::BdState_=STATE_START;
SigmaState GuiApp::GuiState_=STATE_START;
Boolean 		GuiApp::IsSettingsLockedOut_ = FALSE;
Boolean 		GuiApp::IsSettingsTransactionSuccess_ = TRUE;
InitScreen&		GuiApp::RInitScreen_ = *((InitScreen *)
		(((ScreenMemoryUnion *)::ScreenMemory)->normalVentMemory.upperScreen));

Boolean GuiApp::IsSerialCommunicationUp_ = FALSE;
DiscreteValue  GuiApp::PressUnitsSetting_ = PressUnitsValue::CMH2O_UNIT_VALUE;
Boolean GuiApp::ExhValveCalRequiredFlag_ = FALSE;
Boolean GuiApp::ExhValveCalStatusFlag_ = FALSE;
Boolean GuiApp::FlowSensorInfoRequiredFlag_ = FALSE;
Boolean GuiApp::FlowSensorCalPassedFlag_ = FALSE;
Boolean GuiApp::DataKeyInstalledFlag_ = FALSE;
Boolean GuiApp::IsVentStartupSequenceComplete_ = FALSE;
Boolean GuiApp::IsBdAndGuiFlashTheSame_ = FALSE;
Boolean GuiApp::IsGuiPostFailed_=FALSE;
Boolean GuiApp::IsBdPostFailed_=FALSE;
Boolean GuiApp::IsGuiBackgroundFailed_=FALSE;
Boolean GuiApp::IsBdBackgroundFailed_=FALSE;
Boolean GuiApp::VentInopMajorFailureFlag_=FALSE;
Boolean GuiApp::AtmPresXducerCalSuccessfulFlag_=FALSE;
Boolean GuiApp::SstRequiredFlag_=FALSE;
Boolean GuiApp::IsServiceRequired_=FALSE;
Boolean GuiApp::BdVentInopTestInProgress_=FALSE;
Boolean GuiApp::GuiVentInopTestInProgress_=FALSE;
ShutdownState GuiApp::StartupState_=UNKNOWN;
SerialNumber GuiApp::DataKeyGuiSerialNumber_;
SerialNumber GuiApp::DataKeyBdSerialNumber_;
SerialNumber GuiApp::GuiSerialNumber_;
SerialNumber GuiApp::BdSerialNumber_;
SerialNumber GuiApp::GuiNovRamSerialNumber_;
SerialNumber GuiApp::BdNovRamSerialNumber_;
SerialNumber GuiApp::CompressorSerialNum_;
BigSerialNumber GuiApp::ProxFirmwareRev_;
Uint32 GuiApp::ProxSerialNum_;
// TODO E600 added to remove linker error
Uint16 GuiApp::Language_ = 0;
Uint16 GuiApp::LanguageIndex_ = 0;
Boolean GuiApp::IsProxInstalled_ = false;

// Initialize static constants.
static const Int32 MAX_STR_LENGTH = 50;

char GuiApp::BdSoftwareRevNum_[MAX_STR_LENGTH];    
char GuiApp::BdKernelPartNumber_[MAX_STR_LENGTH];   

// Memory for the MutEx used to gain exclusive access to VGA drawing
// functions.
static Uint GuiEventAccessMemory[(sizeof(MutEx) + sizeof(Uint) - 1) / sizeof(Uint)];
MutEx& GuiApp::RGuiEventAccessMutEx_ = *((MutEx *)GuiEventAccessMemory);

GuiTaskInfo_ GuiApp::GuiEventInfoArray_[GuiApp::MAX_GUI_EVENT];
Boolean GuiApp::IsAlarmUpdateNeeded_=FALSE;
Uint16 GuiApp::GuiBreathPhase_;
Uint16 GuiApp::GuiBreathType_;

Boolean GuiApp::UpdateCompletedBatchProcessFlag_=FALSE;

PrintTarget* GuiApp::PPrintTarget_;

// Initialize global constant(s)
const StringId NULL_STRING_ID = NULL;

//
// Debug members.
//
Uint  GuiApp::ArrDebugFlags_[];


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
// Initializes all the data of the GUI-Applications subsystem.  Called
// once only.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes any static classes that need it and "placement new"s all
// top-level objects of the subsystem.
//
// $[01221] The Normal LED must be lit to indicate no alarms.
//---------------------------------------------------------------------
//@ PreCondition
// This must be the only call to this method.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================


void
GuiApp::Initialize(void)
{
	CALL_TRACE("GuiApp::Initialize(void)");

	// Initialize other statics within this subsystem.
	AlarmStrs::Initialize();
	AlarmStateManager::Initialize();
	BoundStrs::Initialize();
	BdEventRegistrar::Initialize();
	GuiTimerRegistrar::Initialize();
	NovRamEventRegistrar::Initialize();
	PatientDataRegistrar::Initialize();
	AlarmRender::Initialize();
	TextUtil::Initialize();
	ServiceDataRegistrar::Initialize();
	LaptopEventRegistrar::Initialize();
	TimeoutSubScreen::Initialize();


	// Register with the Persistent-Objects subsystem to make sure that we
	// are informed of update events to this data type.
	// First, build the message that will be placed on the GUI-Apps User
	// Annunciation queue when the update event occurs.
	UserAnnunciationMsg updateMsg;
	updateMsg.novRamUpdateParts.eventType =
									UserAnnunciationMsg::NOVRAM_UPDATE_EVENT;

	// Second, tell the NovRam update manager to send the message when the specified
	// data collection is updated.
	updateMsg.novRamUpdateParts.updateId = NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE;
    NovRamUpdateManager::RegisterForUpdates(NovRamUpdateManager::REAL_TIME_CLOCK_UPDATE,
									USER_ANNUNCIATION_Q, updateMsg.qWord);

	updateMsg.novRamUpdateParts.updateId = NovRamUpdateManager::SYS_INFO_LOG_UPDATE;
    NovRamUpdateManager::RegisterForUpdates(NovRamUpdateManager::SYS_INFO_LOG_UPDATE,
									USER_ANNUNCIATION_Q, updateMsg.qWord);

	updateMsg.novRamUpdateParts.updateId = NovRamUpdateManager::SYSTEM_LOG_UPDATE;
    NovRamUpdateManager::RegisterForUpdates(NovRamUpdateManager::SYSTEM_LOG_UPDATE,
									USER_ANNUNCIATION_Q, updateMsg.qWord);

	updateMsg.novRamUpdateParts.updateId = NovRamUpdateManager::EST_LOG_UPDATE;
    NovRamUpdateManager::RegisterForUpdates(NovRamUpdateManager::EST_LOG_UPDATE,
									USER_ANNUNCIATION_Q, updateMsg.qWord);

	updateMsg.novRamUpdateParts.updateId = NovRamUpdateManager::SST_LOG_UPDATE;
    NovRamUpdateManager::RegisterForUpdates(NovRamUpdateManager::SST_LOG_UPDATE,
									USER_ANNUNCIATION_Q, updateMsg.qWord);

	updateMsg.novRamUpdateParts.updateId = NovRamUpdateManager::SST_RESULT_TABLE_UPDATE;
    NovRamUpdateManager::RegisterForUpdates(NovRamUpdateManager::SST_RESULT_TABLE_UPDATE,
									USER_ANNUNCIATION_Q, updateMsg.qWord);

	updateMsg.novRamUpdateParts.updateId = NovRamUpdateManager::EST_RESULT_TABLE_UPDATE;
    NovRamUpdateManager::RegisterForUpdates(NovRamUpdateManager::EST_RESULT_TABLE_UPDATE,
									USER_ANNUNCIATION_Q, updateMsg.qWord);

	updateMsg.novRamUpdateParts.updateId = NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE;
    NovRamUpdateManager::RegisterForUpdates(NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE,
									USER_ANNUNCIATION_Q, updateMsg.qWord);

	updateMsg.novRamUpdateParts.updateId = NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE;
    NovRamUpdateManager::RegisterForUpdates(NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE,
									USER_ANNUNCIATION_Q, updateMsg.qWord);

	// Store the current value of the pressure units setting.
	PressUnitsSetting_ =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::PRESS_UNITS);

	GuiFoundation::GetUpperBaseContainer().setFillColor(Colors::MEDIUM_BLUE);
	GuiFoundation::GetLowerBaseContainer().setFillColor(Colors::MEDIUM_BLUE);

	CLASS_ASSERTION(sizeof (InitScreen) <= sizeof (UpperScreen));
	new (&RInitScreen_) InitScreen;

	{
	  // Append the Init Screen container to the upper base container.
	  GuiFoundation::GetUpperBaseContainer().addDrawable(&RInitScreen_);
	}

	new (&RGuiEventAccessMutEx_) MutEx(GUIAPP_SCHEDULER_MT);

	GuiEventInfoArray_[GUI_REPAINT_UPPER_EVENT].priorityConst	=
	GuiEventInfoArray_[GUI_REPAINT_UPPER_EVENT].priority 		= 1;

	GuiEventInfoArray_[GUI_REPAINT_LOWER_EVENT].priorityConst	=
	GuiEventInfoArray_[GUI_REPAINT_LOWER_EVENT].priority 		= 1;

	GuiEventInfoArray_[GUI_PATIENT_DATA_EVENT].priorityConst 	=
	GuiEventInfoArray_[GUI_PATIENT_DATA_EVENT].priority			= 2;

	GuiEventInfoArray_[GUI_WAVEFORM_EVENT].priorityConst	 	=
	GuiEventInfoArray_[GUI_WAVEFORM_EVENT].priority 			= 0;

	GuiEventInfoArray_[GUI_ALARM_EVENT].priorityConst  			=
	GuiEventInfoArray_[GUI_ALARM_EVENT].priority 				= 1;

	GuiEventInfoArray_[GUI_PLOT1_EVENT].priorityConst  			=
	GuiEventInfoArray_[GUI_PLOT1_EVENT].priority 				= 0;

	GuiEventInfoArray_[GUI_PLOT2_EVENT].priorityConst  			=
	GuiEventInfoArray_[GUI_PLOT2_EVENT].priority 				= 0;

	GuiEventInfoArray_[GUI_BD_EVENT].priorityConst 				=
	GuiEventInfoArray_[GUI_BD_EVENT].priority					= 0;

	GuiEventInfoArray_[GUI_TIMER_EVENT].priorityConst 			=
	GuiEventInfoArray_[GUI_TIMER_EVENT].priority				= 2;

	GuiEventInfoArray_[GUI_REPAINT_UPPER_EVENT].timeoutPeriodConst	= 12;	//6;
	GuiEventInfoArray_[GUI_REPAINT_LOWER_EVENT].timeoutPeriodConst	= 12;	//8;
	GuiEventInfoArray_[GUI_PATIENT_DATA_EVENT].timeoutPeriodConst 	= 15;	//6;
	GuiEventInfoArray_[GUI_WAVEFORM_EVENT].timeoutPeriodConst 		= 5;	//2;
	GuiEventInfoArray_[GUI_ALARM_EVENT].timeoutPeriodConst 			= 20;	//8;
	GuiEventInfoArray_[GUI_PLOT1_EVENT].timeoutPeriodConst 			= 20;	//8;
	GuiEventInfoArray_[GUI_PLOT2_EVENT].timeoutPeriodConst 			= 24;	//8;
    GuiEventInfoArray_[GUI_BD_EVENT].timeoutPeriodConst 			= 5;	//2;
	GuiEventInfoArray_[GUI_TIMER_EVENT].timeoutPeriodConst			= 12;	//3;

	GuiEventInfoArray_[GUI_TIMER_EVENT].stateMask 	=
	GuiEventInfoArray_[GUI_REPAINT_UPPER_EVENT].stateMask 	=
	GuiEventInfoArray_[GUI_REPAINT_LOWER_EVENT].stateMask 	= STATE_ONLINE |
															  STATE_SERVICE |
															  STATE_SST |
															  STATE_INOP |
															  STATE_TIMEOUT |
															  STATE_INIT |
															  STATE_UNKNOWN |
															  STATE_FAILED |
															  STATE_START;
	GuiEventInfoArray_[GUI_PATIENT_DATA_EVENT].stateMask 	= STATE_ONLINE;
	GuiEventInfoArray_[GUI_WAVEFORM_EVENT].stateMask 		= STATE_ONLINE;
	GuiEventInfoArray_[GUI_ALARM_EVENT].stateMask 			= STATE_ONLINE |
															  STATE_INOP |
															  STATE_TIMEOUT |
															  STATE_UNKNOWN |
															  STATE_FAILED;
	GuiEventInfoArray_[GUI_PLOT1_EVENT].stateMask 			= STATE_ONLINE;
	GuiEventInfoArray_[GUI_PLOT2_EVENT].stateMask 			= STATE_ONLINE;
	GuiEventInfoArray_[GUI_BD_EVENT].stateMask 				= STATE_ONLINE;

	for (Int16 idx = 0; idx < MAX_GUI_EVENT; idx++)
	{
		GuiEventInfoArray_[idx].timeoutCount = 0;
		GuiEventInfoArray_[idx].executionBiasCount = 0;
		GuiEventInfoArray_[idx].executionSequence = 0;
	}

	// NULL terminated any strings.
	BdSoftwareRevNum_[0] = '\0';
	BdKernelPartNumber_[0] = '\0';

	PPrintTarget_ = NULL;
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskBreathBar [public, static]
//
//@ Interface-Description
// Entry point for the GUI-Applications breath bar update task on the
// target system.  This is an infinite loop which pends for messages on
// the User Annunciation queue and forwards the various types of messages
// received to the appropriate object/handler.  The message format is
// specified in a GuiSchedulerMsg structure.
//---------------------------------------------------------------------
//@ Implementation-Description
// In an infinite loop, we then wait on the User Annunciation Queue.
// If a message is received on the queue we decode it and execute the
// appropriate action.
//
// We only wait 20msec for a new message on the queue.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::TaskBreathBar(void)
{

	MsgQueue guiSchedulerQ(GUIAPP_BREATH_BAR_Q);	// Handle to queue
	GuiSchedulerMsg  msg;
	Int32 qWord;								// 32 bits from the queue
	Int32 rval;									// Return value

	while(1)
	{														// $[TI1]
		msg.qWord = 0;	// Clear previous event data

		rval = guiSchedulerQ.pendForMsg(qWord);

		// Check to see if we read something
		if (rval == Ipc::OK)
		{													// $[TI1.1]
			// Plug event data into our UserAnnunciationMsg union so that
			// we can interpret the 32 bits in the message.
			msg.qWord = qWord;

			switch (msg.GuiSchedulerMessage.msgType)
			{												// $[TI1.1.1]
			// Task Control message
			case TaskMonitorQueueMsg::TASK_MONITOR_MSG:		// $[TI1.1.1.1]
				TaskMonitor::Report();
				break;

			// NovRam update message
			case GuiSchedulerMsg::BREATH_BAR_UPDATE_EVENT:	// $[TI1.1.1.2]
				// Inform the patient data registrar which will inform all
				// appropriate objects of the latest patient data changes.
				GuiBreathPhase_ = msg.GuiSchedulerMessage.msgId;
				GuiBreathType_ = msg.GuiSchedulerMessage.msgData;
				PatientDataRegistrar::UpdateHappened(GUI_BREATH_BAR_TASK);
				break;

			default:										// $[TI1.1.1.3]
				AUX_CLASS_ASSERTION_FAILURE(qWord);
				break;
			}
		}													// $[TI1.2]
	} // the end of while
															// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskScheduler [public, static]
//
//@ Interface-Description
// Entry point for the GUI-Applications scheduler task on the target system.
// This is an infinite loop which pends for messages on the User Annunciation
// queue and forwards the various types of messages received to the
// appropriate object/handler.  The message format is specified in a
// GuiSchedulerMsg structure.
//---------------------------------------------------------------------
//@ Implementation-Description
// In an infinite loop, we wait on the User Annunciation Queue.  If a
// message is received on the queue we decode it and execute the appropriate
// action.  Else we set the Gui Event priority based on the event priority
// and its execution bias counts.
//
// We only wait 20msec for a new message on the queue.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::TaskScheduler(void)
{

	MsgQueue guiSchedulerQ(GUIAPP_SCHEDULER_Q);	// Handle to queue
	GuiSchedulerMsg  msg;
	Int32 qWord;								// 32 bits from the queue
	Int32 rval;									// Return value

	while(1)
	{													// $[TI1]
		msg.qWord = 0;	// Clear previous event data

		rval = guiSchedulerQ.pendForMsg(qWord, 10);

		// Check to see if we read something
		if (rval == Ipc::OK)
		{												// $[TI1.1]
			// Plug event data into our UserAnnunciationMsg union so that
			// we can interpret the 32 bits in the message.
			msg.qWord = qWord;

			switch (msg.GuiSchedulerMessage.msgType)
			{											// $[TI1.1.1]
			// Task Control message
			case TaskMonitorQueueMsg::TASK_MONITOR_MSG:	// $[TI1.1.1.1]
				TaskMonitor::Report();
				break;

			default:									// $[TI1.1.1.2]
				AUX_CLASS_ASSERTION_FAILURE(qWord);
				break;
			}
		}
		else
		{												// $[TI1.2]
			SetNextGuiEvent_();
		}

	} // the end of while
													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskExecutor [public, static]
//
//@ Interface-Description
// Entry point for the GUI-Applications task on the target system.  This
// is an infinite loop which pends for messages on the User Annunciation
// queue and forwards the various types of messages received to the
// appropriate object/handler.  The message format is specified in a
// UserAnnunciationMsg structure.  After handling a message (or group of
// messages) the Upper and Lower screens are repainted so that any graphical
// changes which occurred as a result of the message(s) are displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
// In an infinite loop, we wait on the User Annunciation Queue.  If a
// message is received on the queue we decode it and execute the appropriate
// action.
//
// We only wait 50msec for a new message on the queue.  If no message
// is received in that time then we can execute all cyclic periodic
// screen display based on each event's priority.  The periodic events
// are GUI_REPAINT_UPPER_EVENT,	GUI_REPAINT_LOWER_EVENT,
// GUI_PATIENT_DATA_EVENT, GUI_WAVEFORM_EVENT, GUI_ALARM_EVENT,
// GUI_PLOT1_EVENT, and GUI_PLOT2_EVENT.  Before each cyclic event is
// executed we first check if there are more alarm events on the User
// Annunciation Queue.  We will empty as many alarm events as possible
// to avoid of queue overflow during heavy load.  We will execute at least
// one gui event on each cyclic period.
//
// If the User Annunciation Queue is 90% full then we will skip the
// periodic gui event and execute the next queue event.
//
// $[GC01004] When initiated, the print function output shall capture ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::TaskExecutor(void)
{

	MsgQueue userAnnunciationQ(USER_ANNUNCIATION_Q);
									// Handle to User Annunc. queue
	Int32 qWord;					// 32 bits from the queue
	UserAnnunciationMsg eventMsg;	// Structure of 32 bits qWord
	Int32 rval;						// Return value
	AppContext appContext;			// For task control message
									// handling
	Int16 screenTouched;
	Int16 x = 0;
	Int16 y = 0;
    Boolean startWave = FALSE;
	
	// Register for messages from Sys-Init
	appContext.setCallback(GuiApp::BdReadyCallback);
	appContext.setCallback(GuiApp::ChangeStateCallback);
	appContext.setCallback(GuiApp::CommDownCallback);

	// Task loops forever
	while(1)
	{
		eventMsg.qWord = 0;	// Clear previous event data

		// Wait for the next event on the User Annunciation queue, timeout
		// the wait in 50ms so we can update waveform info.
		rval = userAnnunciationQ.pendForMsg(qWord, 50);
		

		// Check to see if we read something
		if (rval == Ipc::OK)
		{													// $[TI1]
			// Plug event data into our UserAnnunciationMsg union so that
			// we can interpret the 32 bits in the message.
			eventMsg.qWord = qWord;

			// OK, check out what kind of event we received
			switch (eventMsg.touchParts.eventType)
			{
			case UserAnnunciationMsg::INPUT_FROM_TOUCH:		// $[TI1.1]
				UserActivityHappened();
				switch (eventMsg.touchParts.action)
				{
				case UserAnnunciationMsg::SCREEN_TOUCH:		// $[TI1.1.1]
					TouchDriver::ReadTouch(x,y);

#ifdef  BROADCAST_TOUCHED_POSITION
                    broadcastTouchedPosition_(x,y);
#endif
					screenTouched = Touch::TouchEvent(x,y);
					TouchDriver::AckTouchInput();
					BoostScreenUpdate_(screenTouched);
					break;

				case UserAnnunciationMsg::SCREEN_RELEASE:	// $[TI1.1.2]
					screenTouched = Touch::ReleaseEvent(0,
						0);

					// Set a flag informing GUI-IO-Devices that touch events
					// can again be put on our input queue.
					TouchDriver::AckTouchInput();
					BoostScreenUpdate_(screenTouched);
					break;

				default:									// $[TI1.1.3]
					CLASS_ASSERTION_FAILURE();			// Illegal event
					break;
				}
				break;

			// Knob turn event
			case UserAnnunciationMsg::INPUT_FROM_KNOB:		// $[TI1.2]
				UserActivityHappened();

				// Get the current knob delta total from GUI-IO-Devices and
				// pass it on to GUI-Foundations.
				screenTouched = AdjustPanel::KnobEvent(::GetKnobDelta());
				BoostKnobUpdate_(screenTouched);
				break;

			// Offscreen key event
			case UserAnnunciationMsg::INPUT_FROM_KEY:		// $[TI1.3]
				UserActivityHappened();
				switch (eventMsg.touchParts.action)
				{
				case UserAnnunciationMsg::KEY_PRESS:		// $[TI1.3.1]
					KeyPanel::KeyPressHappened(
							(GuiKeys)eventMsg.keyParts.keyCode);

					RaiseGuiEventPriority(GuiApp::GUI_REPAINT_LOWER_EVENT,
							GuiApp::GUI_THIRD_HIGHEST_PRIORITY);
					break;

				case UserAnnunciationMsg::KEY_RELEASE:		// $[TI1.3.2]
					KeyPanel::KeyReleaseHappened(
							(GuiKeys)eventMsg.keyParts.keyCode);

					// Set a flag informing GUI-IO-Devices that key events
					// can again be put on our input queue.
					KeyInputAllowed = TRUE;
					break;

				default:									// $[TI1.3.3]
					CLASS_ASSERTION_FAILURE();		// Illegal event
					break;
				}
				break;

			// Timer event
			case UserAnnunciationMsg::TIMER_EVENT:			// $[TI1.4]
				// Inform the timer registrar which will inform the
				// appropriate object that it's timer has gone off.
				IsTimerEventUpdateNeeded_ = TRUE;
				GuiTimerRegistrar::TimerChangeHappened(eventMsg);
				break;

			// Alarm update event
			case UserAnnunciationMsg::ALARM_UPDATE_EVENT:			   // $[TI1.6]
				AlarmAnnunciator::Update();

				// If a new alarm occurs while the screen lock function is
				// active, the screen lock state must be canceled.
				if (AlarmAnnunciator::GetCancelScreenLockStatus() == TRUE)
				{													   // $[TI1.6.1]
					RKeyHandlers.getScreenLockHandler().unlockScreen();
				}													   // $[TI1.6.2]

				break;

			case UserAnnunciationMsg::CAPTURE_PATIENT_DATA_EVENT:	   // $[TI1.17]
				{
					if (SerialInterface::IsPrintRequested())
					{
						// $[TI1.17.1]
						PatientDataCapture::GetPatientDataCapture().captureData();
						UserAnnunciationMsg msg;
						msg.alarmParts.eventType 
							= UserAnnunciationMsg::CAPTURE_ALARM_STATUS_EVENT;
						CLASS_ASSERTION(
									   MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) 
									   == Ipc::OK);
					}
					else if (PPrintTarget_)
					{
						// $[TI1.17.2]
						PPrintTarget_->printDone();
					}
				}
			break;

			case UserAnnunciationMsg::CAPTURE_ALARM_STATUS_EVENT:	   // $[TI1.18]
				{
					if (SerialInterface::IsPrintRequested())
					{
						// $[TI1.18.1]
						AlarmStatusCapture::GetAlarmStatusCapture().captureData(); 
						UserAnnunciationMsg msg;
						msg.alarmParts.eventType 
							= UserAnnunciationMsg::CAPTURE_WAVEFORMS_EVENT;
						CLASS_ASSERTION(
									   MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) 
									   == Ipc::OK);
					}
					else if (PPrintTarget_)
					{
						// $[TI1.18.2]
						PPrintTarget_->printDone();
					}
				}
			break;

			case UserAnnunciationMsg::CAPTURE_WAVEFORMS_EVENT:		   // $[TI1.19]
				{
					if (SerialInterface::IsPrintRequested())
					{
						// $[TI1.19.1]
						WaveformsCapture::GetWaveformsCapture().captureData();
						UserAnnunciationMsg msg;
						msg.alarmParts.eventType 
							= UserAnnunciationMsg::CAPTURE_SETTINGS_EVENT;
						CLASS_ASSERTION(
									   MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) 
									   == Ipc::OK);
					}
					else if (PPrintTarget_)
					{
						PPrintTarget_->printDone();
					}
				}
			break;

			case UserAnnunciationMsg::CAPTURE_SETTINGS_EVENT:		   // $[TI1.20]
				{
					if (SerialInterface::IsPrintRequested())
					{
						// $[TI1.20.1]
						ModeSettingsCapture::GetModeSettingsCapture().captureData();
						SettingsCapture::GetSettingsCapture().captureData();
						UserAnnunciationMsg msg;
						msg.alarmParts.eventType 
							= UserAnnunciationMsg::CAPTURE_LOWER_SUB_SCREEN_EVENT;
						CLASS_ASSERTION(
									   MsgQueue::PutMsg(USER_ANNUNCIATION_Q, (Int32)msg.qWord) 
									   == Ipc::OK);
					}
					else if (PPrintTarget_)
					{
						// $[TI1.20.2]
						PPrintTarget_->printDone();
					}
				}
			break;

			case UserAnnunciationMsg::CAPTURE_LOWER_SUB_SCREEN_EVENT:  // $[TI1.21]
				{
					if (SerialInterface::IsPrintRequested())
					{
						// $[TI1.21.1]
						LowerSubScreenCapture::GetLowerSubScreenCapture().captureData();

						if (PPrintTarget_)
						{
							PPrintTarget_->transitionCaptureToPrint();
						}
					}
					else if (PPrintTarget_)
					{
						// $[TI1.21.2]
						PPrintTarget_->printDone();
					}
				}
			break;

			case UserAnnunciationMsg::CAPTURE_DONE_EVENT:	// $[TI1.22]
				if (PPrintTarget_)
				{                
					PPrintTarget_->printDone();
				}
				break;

			case UserAnnunciationMsg::PRINTER_ASSIGNMENT_EVENT:	// $[TI1.19]
                if (PPrintTarget_)
                {
					PPrintTarget_->updatePrintButton();
                }
				break;

			// Task Control message
			case TaskControlQueueMsg::TASK_CONTROL_MSG:		// $[TI1.7]
			case TaskMonitorQueueMsg::TASK_MONITOR_MSG:		// $[TI1.8]
				appContext.dispatchEvent(eventMsg.qWord);
				break;

			// Settings transaction message
			case UserAnnunciationMsg::SETTINGS_EVENT:		// $[TI1.9]
				SetSettingsTransactionSuccess(
							eventMsg.settingsParts.transactionSuccess);
				break;

			// Patient Data timer message
			case UserAnnunciationMsg::PATIENT_DATA_TIMER_EVENT:
															// $[TI1.10]
				// Special timer event generated by the Patient-Data subsystem.
				// Patient-Data timers put timer events on the GUI-App queue,
				// just forward notification of the event to the Patient-Data
				// subsystem and it will handle it.
				PDTimerRegistrar::TimerEventHappened(eventMsg);
				break;

			// NovRam update message
			case UserAnnunciationMsg::NOVRAM_UPDATE_EVENT:	// $[TI1.11]
				// Inform the NovRam event registrar which will inform the
				// appropriate object(s) that novram data has changed.
				NovRamEventRegistrar::EventHappened(eventMsg);
				break;

			// Service data message
			case UserAnnunciationMsg::SERVICE_DATA_EVENT:	// $[TI1.12]
				// Inform the service data registrar which will inform the
				// appropriate object that service data has changed.
				ServiceDataRegistrar::UpdateHappened(
							   	(TestDataId::TestDataItemId)
								eventMsg.serviceDataParts.datumId);
				break;

			// BD to GUI event message
			case UserAnnunciationMsg::BD_GUI_EVENT:			// $[TI1.13]
				// Inform the BD event registrar which will inform the
				// appropriate object(s) that the particular event has occurred
				IsBdEventUpdateNeeded_ = TRUE;
				BdEventRegistrar::ChangeHappened(
				   (EventData::EventId) eventMsg.bdEventParts.eventId,
				   (EventData::EventStatus) eventMsg.bdEventParts.eventStatus,
				   (EventData::EventPrompt) eventMsg.bdEventParts.eventPrompt);
				break;

			// Serial communication request message
			case UserAnnunciationMsg::SERIAL_REQUEST_EVENT:	// $[TI1.14]
				// Inform the Laptop event registrar which will inform the
				// appropriate object(s) that the particular event has occurred
				LaptopEventRegistrar::LaptopRequestHappened(
								eventMsg.serialRequestParts.portNum);
				break;

			// Serial communication request message
			case UserAnnunciationMsg::SERIAL_FAILURE_EVENT:	// $[TI1.15]
				// Inform the Laptop event registrar which will inform the
				// appropriate object(s) that the particular event has occurred
				LaptopEventRegistrar::LaptopFailureHappened(
								(SerialInterface::SerialFailureCodes)
									eventMsg.serialRequestParts.failureId,
								eventMsg.serialRequestParts.portNum);
				break;


            case UserAnnunciationMsg::TREND_DATA_READY_EVENT :
                // Inform trend registrar that TrendDataSet is avaiable   
                TrendEventRegistrar::TrendDataReady(
                                        eventMsg.trendDataReadyParts.dataSetAddr );
                break;

            case UserAnnunciationMsg::TREND_MANAGER_STATUS_EVENT :
                // Infor trend registrar about the manager's status
                TrendEventRegistrar::TrendManagerStatus(
                                        eventMsg.trendMgrStatusParts.isEnabled,
                                        eventMsg.trendMgrStatusParts.isRunning );
                break;
             
            case UserAnnunciationMsg::VSET_COMMAND_EVENT :
                // process current VSET CMD stored in VsetServer
				VsetServer::ProcessBatch();
                break;

			// Unexpected event
			default:										// $[TI1.16]
				AUX_CLASS_ASSERTION_FAILURE(qWord);
				break;
			}
		}													// $[TI2]

		if (NORMAL_VENTILATION == MajorScreenMode_ && GuiState_ == STATE_ONLINE)
		{	
           if(!startWave )
			{
				startWave = TRUE;
				// Tell Patient-Data subsystem to record waveform data.
				PatientDataMgr::ActivateWaveformData();
				
			}
															// $[TI5.1]
			// Always update the real time waveform plotting to ensure the
			// smoothness of the display.
			UpperSubScreenArea::GetWaveformsSubScreen()->update();
		}													// $[TI5.2]

		// To consume as many as user annunciation queue event as
		// possible to avoid of queue overflow.
		//TODO E600	AccumulateEvent_(userAnnunciationQ);
		ProcessAlarmAnnunciations_();

		// The following code is to execute the periodic events.
		// The call to GetNextGuiEvent_() shall return the next
		// periodic event that should be executed.
		if (userAnnunciationQ.numMsgsAvail() < 90)
		{													// $[TI3]
			Int16 nextEvent;
			Boolean isEventCompleted = FALSE;

			while (!isEventCompleted)
			{												// $[TI3.1]
				TaskMonitor::Report();
				eventMsg.qWord = 0;	// Clear previous event data
				
				switch (nextEvent = GetNextGuiEvent_())
				{											// $[TI3.1.1]
				case GUI_REPAINT_UPPER_EVENT:				// $[TI3.1.1.1]
					// Display all recent screen updates
					if (GuiFoundation::GetUpperBaseContainer().isRepaintNeeded())
					{										// $[TI3.1.1.1.1]
						GuiFoundation::GetUpperBaseContainer().repaint();
						isEventCompleted = TRUE;
					}										// $[TI3.1.1.1.2]
					break;
				case GUI_REPAINT_LOWER_EVENT:				// $[TI3.1.1.2]
					// Display all recent screen updates
					if (GuiFoundation::GetLowerBaseContainer().isRepaintNeeded())
					{										// $[TI3.1.1.2.1]
						GuiFoundation::GetLowerBaseContainer().repaint();
						isEventCompleted = TRUE;
					}										// $[TI3.1.1.2.2]
					break;
				case GUI_PATIENT_DATA_EVENT:				// $[TI3.1.1.3]

					// Inform the patient data registrar which will inform all
					// appropriate objects of the latest patient data changes.
					PatientDataRegistrar::UpdateHappened(GUI_EXECUTOR_TASK);
					break;
				case GUI_WAVEFORM_EVENT:					// $[TI3.1.1.4]
					break;
				case GUI_ALARM_EVENT:						// $[TI3.1.1.5]
					if (IsAlarmUpdateNeeded_)
					{										// $[TI3.1.1.5.1]
						AlarmEventChangeHappened_();
						IsAlarmUpdateNeeded_ = FALSE;
					}										// $[TI3.1.1.5.2]
					AlarmStateManager::ProcessPeriodicAlarmUpdates();
					break;
				case GUI_PLOT1_EVENT:						// $[TI3.1.1.6]
				case GUI_PLOT2_EVENT:						// $[TI3.1.1.7]	
					if (UpperSubScreenArea::GetWaveformsSubScreen()->updatePlot())
					{										// $[TI3.1.1.6.1]	
						// Waveform update is completed so we had done the job.
						isEventCompleted = TRUE;
					}										// $[TI3.1.1.6.2]	
					break;
                case GUI_BD_EVENT:							// $[TI3.1.1.8]
					// Inform the BD event registrar which will inform the
					// appropriate object(s) that the particular event has occurred
					if (IsBdEventUpdateNeeded_)
					{										// $[TI3.1.1.8.1]
						BdEventRegistrar::EventHappened();
						IsBdEventUpdateNeeded_ = FALSE;
					}										// $[TI3.1.1.8.2]
					break;
				case GUI_TIMER_EVENT:						// $[TI3.1.1.9]
					if (IsTimerEventUpdateNeeded_)
					{										// $[TI3.1.1.9.1]
						GuiTimerRegistrar::TimerEventHappened();
						IsTimerEventUpdateNeeded_ = FALSE;
					}										// $[TI3.1.1.9.2]
					break;
				case GUI_NO_EVENT:							// $[TI3.1.1.10]
					isEventCompleted = TRUE;
					break;
				case MAX_GUI_EVENT:							// $[TI3.1.1.11]
				default:
					AUX_CLASS_ASSERTION_FAILURE(nextEvent);
					break;
				}

					if (IsBdEventUpdateNeeded_)
					{															// $[TI2]
						RaiseGuiEventPriority(GUI_BD_EVENT, GUI_SECOND_HIGHEST_PRIORITY);
					}					
										
				// To consume as many as user annunciation queue event as
				// possible to avoid of queue overflow.
				//TODO E600	AccumulateEvent_(userAnnunciationQ);

				ProcessAlarmAnnunciations_();
			}												// $[TI3.2]
		}													// $[TI4]
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdReadyCallback
//
//@ Interface-Description
// Entry point for the GUI-Applications to process the current Bd messages.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method maintains a copy of the system status flags passed into
// Gui-App by Sys-Init eg: required service flags, serial number
// data, and Flash table flag etc.
//---------------------------------------------------------------------
//@ PreCondition
// None
//---------------------------------------------------------------------
//@ PostCondition
// None
//@ End-Method
//=====================================================================

void 
GuiApp::BdReadyCallback(const BdReadyMessage& rMessage)
{
	CALL_TRACE("GuiApp::BdReadyCallback(const BdReadyMessage&)");

	const PostAgent &postAgent = rMessage.getPostAgent();
	
	// Set Vent startup state
	IsVentStartupSequenceComplete_ = rMessage.areBdSettingsInitialized();
	const ServiceModeAgent& rServiceModeAgent = rMessage.getServiceModeAgent();

	SetExhValveCalRequiredFlag(ServiceMode::NEVER_RUN == rServiceModeAgent.getExhValveCalStatus());
	SetExhValveCalSuccessfulFlag(ServiceMode::PASSED == rServiceModeAgent.getExhValveCalStatus());

	SetFlowSensorInfoRequiredFlag(rServiceModeAgent.isFlowSensorInfoRequired());
	SetFlowSensorCalPassedFlag(rServiceModeAgent.getFlowSensorCalStatus() == ServiceMode::PASSED);
	
	SetBdVentInopTestInProgressFlag(rServiceModeAgent.getVentInopTestStatus() == ServiceMode::IN_PROGRESS);
	SetVentInopMajorFailureFlag(rServiceModeAgent.getVentInopTestStatus() == ServiceMode::FAILED);

	SetAtmPresXducerCalStatusFlag(rServiceModeAgent.getAtmPresXducerCalStatus() == ServiceMode::PASSED);

	SetSstRequiredFlag(rServiceModeAgent.isSstRequired());
	SetConfigurationAgentFields_(rMessage);
	SetBdAndGuiFlash(ServiceMode::AreBdAndGuiFlashTheSame());
	SetBdKernelPartNumber(postAgent.getKernelPartNumber());  
	IsBdPostFailed_ = postAgent.isPostFailed();
	IsBdBackgroundFailed_ = postAgent.getBackgroundFailed() > PASSED;
	IsServiceRequired_ = rMessage.isServiceRequired();
	StartupState_= postAgent.getStartupState();
	HundredPercentO2Handler::ResetTimerRestartedFlag();
	

#ifdef SIGMA_DEVELOPMENT
	printf("GuiApp: BdReadyCallback  state = %d\n", rMessage.getState());
	printf("IsVentStartupSequenceComplete_ = %d\n", IsVentStartupSequenceComplete_);
	printf("IsDataKeyInstalled() = %d \n", IsDataKeyInstalled());
	printf("IsServiceRequired_ = %d, %d \n", IsServiceRequired_,
								rMessage.isServiceRequired());
#endif

	BdState_ = rMessage.getState();

												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeStateCallback
//
//@ Interface-Description
// Entry point for the GUI-Applications to process the current Gui messages.
//---------------------------------------------------------------------
//@ Implementation-Description
// It will instanciate the appropriated Upper and Lower Screens based on
// the GUI state passed by Sys-Init.  Note this is the only place to
// instanciate the upper and lower screen.  The state of communication
// does not impact the screen construction.  For any states other than
// STATE_ONLINE, STATE_SERVICE, STATE_SST the setting buttons will be
// locked due to system being inoperative or communication down.  Whenever
// GuiApp has serviced the ChangeStateCallback(), it will inform Sys-Init of
// GUI Task ready so that Sys-Init can transition to other state.  When
// the state of the system is established, it will inform the interested
// objects of the event (via the GuiEventRegistrar) to display themselves
// accordingly.
// $[00540] GUI shall enter service mode, if Service Requested.
// $[00544] If after 10 seconds from power-up the GUI-CPU does not receive
// any message from the BD CPU ....
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

void 
GuiApp::ChangeStateCallback(const ChangeStateMessage& changeStateMessage)
{
	CALL_TRACE("GuiApp::ChangeStateCallback(const ChangeStateMessage&)");


	GuiAppEvent guiAppEvent=UNDEFINED;

	IsGuiPostFailed_ = Post::IsPostFailed();
	IsGuiBackgroundFailed_ = Post::GetBackgroundFailed() > PASSED;

#ifdef SIGMA_DEVELOPMENT
	printf("GuiApp: Change state to %d\n", changeStateMessage.getState());
#endif // SIGMA_DEVELOPMENT

	// Disable settings change
	IsSettingsLockedOut_ = TRUE;

	GuiState_ = changeStateMessage.getState();


	switch (GuiState_)
	{
	case (STATE_START):									// $[TI1]
		CLASS_ASSERTION(INITIALIZING == MajorScreenMode_);
		break;

	case (STATE_INIT):									// $[TI2]
		if (INITIALIZING == MajorScreenMode_)
		{												// $[TI2.1]
	  		guiAppEvent = UNDEFINED;
		}												// $[TI2.2]
		break;

	case (STATE_INOP):									// $[TI3]
		// Whenever we are in STATE INOP state, we may/may not receive 
		// BdReadyMessage depending on the event which causes the GUI-INOP
		// to happened.  If the event is "Gui service required" then we will
		// receive a BdReadyMessage.  If the event is a "changeState-INOP"
		// then we will not receive a BdReadyMessage. In either case the
		// Gui should stay in GUI-INOP state, so we can instanciate all
		// the GUI objects and display the GUI-INOP messges now.
		if (INITIALIZING == MajorScreenMode_)
		{												// $[TI3.1]
			BeginNormalVentilation();
		}												// $[TI3.2]

		// Inform interested objects of the event via the
		// GuiEventRegistrar.
		guiAppEvent = INOP;
		break;

	case (STATE_ONLINE):								// $[TI4]
		// Check EST overall status from both GUI and BD sides.

		if ((IsServiceRequired_ = (IsServiceRequired_ || ServiceMode::IsServiceRequired())))
		{												// $[TI4.1.1]
			// Do nothing, IsSettingsLockedOut_ default to FALSE
			;
		}
		else if (BdState_ == STATE_ONLINE &&
				 GuiApp::SERIAL_NUMBER_OK == GuiApp::GetSerialNumberInfoState())
		{												// $[TI4.2.1]
			// Enable settings change
			IsSettingsLockedOut_ = FALSE;
		}												// $[TI4.2.2]

#ifdef SIGMA_DEVELOPMENT
		printf("IsServiceRequired_ = %d, %d \n", IsServiceRequired_,
								ServiceMode::IsServiceRequired());
#endif

		if (INITIALIZING == MajorScreenMode_)
		{												// $[TI4.3]
			BeginNormalVentilation();
		}												// $[TI4.4]

		// Inform interested objects of the event via the
		// GuiEventRegistrar.
		guiAppEvent = GUI_ONLINE_READY;
		break;

	case (STATE_SERVICE):								// $[TI5]
		if (BdState_ == STATE_SERVICE)
		{												// $[TI5.1]
			// Enable settings change
			IsSettingsLockedOut_ = FALSE;
		}												// $[TI5.2]

		if (INITIALIZING == MajorScreenMode_)
		{												// $[TI5.3]
			// Whenever we are in STATE SERVICE state, we will receive 
			// BdReadyMessage. However, service mode does not care 
			// about of Vent start up state flags.  So we ignore these
			// flags.
			BeginServiceMode();
		}												// $[TI5.4]

		// Inform interested objects of the event via the
		// GuiEventRegistrar.
		guiAppEvent = SERVICE_READY;
		break;

	case (STATE_SST):									// $[TI6]
		if (BdState_ == STATE_SST)
		{												// $[TI6.1]
			// Enable settings change
			IsSettingsLockedOut_ = FALSE;
		}												// $[TI6.2]

		if (NORMAL_VENTILATION == MajorScreenMode_)
		{												// $[TI6.3]
			// Whenever we are in STATE SST state, we will receive 
			// BdReadyMessage. However, SST mode does not care 
			// about of Vent start up state flags.  So we ignore these
			// flags.
			BeginServiceMode();
		}												// $[TI6.4]

		guiAppEvent = SERVICE_READY;
		break;

	case (STATE_TIMEOUT):								// $[TI7]
		if (INITIALIZING == MajorScreenMode_)
		{												// $[TI7.1]
			BeginNormalVentilation();
		}												// $[TI7.2]

		// Inform interested objects of the event via the
		// GuiEventRegistrar.
		guiAppEvent = INOP;
		break;

	default:											// $[TI8]
		CLASS_ASSERTION_FAILURE();
		break;
		}

	// Inform Sys-Init of GUI Task ready for this ChangeState().
	TaskControlAgent::ReportTaskReady(changeStateMessage);

#ifdef SIGMA_DEVELOPMENT
	printf("GuiApp: guiAppEvent= %d\n", guiAppEvent);
#endif // SIGMA_DEVELOPMENT

	if (guiAppEvent != UNDEFINED)
	{												// $[TI9]
		// Inform interested objects of the event via the GuiEventRegistrar.
		GuiEventRegistrar::GuiEventHappened(guiAppEvent);
	}												// $[TI10]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommDownCallback
//
//@ Interface-Description
// Entry point for the GUI-Applications to process the current comm down
// messages.
//---------------------------------------------------------------------
//@ Implementation-Description
// Generate a COMMUNICATIONS_DOWN event and inform the interested
// objects of the event via the GuiEventRegistrar.  Adjustment of the
// Setting will not be allowed once the communication is down.  It also
// informs the BdGuiEvent object in the Breath-Delivery subsystem.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

void 
GuiApp::CommDownCallback(const CommDownMessage&)
{
	CALL_TRACE("GuiApp::CommDownCallback(const CommDownMessage&)");


#ifdef SIGMA_DEVELOPMENT
	printf("GuiApp: Communications is down\n");
#endif // SIGMA_DEVELOPMENT

	if (!IsSettingsLockedOut_)
	{												// $[TI1]
		// Disable settings change
		IsSettingsLockedOut_ = TRUE; 

		// Inform interested objects of the event via the GuiEventRegistrar.
		GuiEventRegistrar::GuiEventHappened(COMMUNICATIONS_DOWN);
	}												// $[TI2]

	if (UpdateCompletedBatchProcessFlag_)
	{												// $[TI3]
		UpdateCompletedBatchProcessFlag_ = FALSE;
	}												// $[TI4]
	
	// Also inform the BdGuiEvent object (it's part of the Breath-Delivery
	// subsystem but does not have a message queue of its own so GUI-App
	// must pass comms down events directly to it).
	BdGuiEvent::CommsDown();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BeginNormalVentilation [public, static]
//
//@ Interface-Description
// Sets the GUI into Normal Ventilation display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Remove all drawables from Upper and Lower Base Container.
// Remove all possible adjust panel focus.  Create the objects used
// specifically for normal ventilation and inform the Upper and Lower screens
// of the event.  Switch on the Normal LED and switch off all Alarm LEDs.
//---------------------------------------------------------------------
//@ PreCondition
//	INITIALIZING == MajorScreenMode_
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::BeginNormalVentilation(void)
{
	CALL_TRACE("GuiApp::BeginNormalVentilation(void)");

	CLASS_PRE_CONDITION(INITIALIZING == MajorScreenMode_);

	// Clear the Upper and Lower base containers.
	GuiFoundation::GetLowerBaseContainer().removeAllDrawables();
	GuiFoundation::GetUpperBaseContainer().removeAllDrawables();
	GuiFoundation::GetLowerBaseContainer().repaint();
	GuiFoundation::GetUpperBaseContainer().repaint();

	// Make sure no object retains adjust panel focus
	AdjustPanel::TakePersistentFocus(NULL);
	AdjustPanel::TakeNonPersistentFocus(NULL);

	// make sure that all GUI-Apps observers are no longer attach to
	// any setting subjects, because the observers may be going away...
	Subject::DetachFromAllObservers();
	// Construct the Upper and Lower Screen containers.

	LowerScreen::Initialize();
	UpperScreen::Initialize();

	// Initialize some static data members.
	PPromptArea = LowerScreen::RLowerScreen.getPromptArea();
	PMessageArea = LowerScreen::RLowerScreen.getMessageArea();

	SAFE_CLASS_ASSERTION(PPromptArea != NULL);
	SAFE_CLASS_ASSERTION(PMessageArea != NULL);

	// Append the Upper and Lower Screen containers
	// to the appropriate GUI-Foundations base containers.
	GuiFoundation::GetLowerBaseContainer().addDrawable(&LowerScreen::RLowerScreen);
	GuiFoundation::GetUpperBaseContainer().addDrawable(&UpperScreen::RUpperScreen);

	// Create key handlers and miscellaneous BD event handler
	new (&RKeyHandlers) KeyHandlers;
	new (&RMiscBdEventHandler) MiscBdEventHandler;

	// Switch off all Alarm LEDs
	for (Int16 idx = Led::HIGH_ALARM_URGENCY; idx < Led::NUM_LED_ID; idx++)
	{
		Led::SetState((Led::LedId) idx, Led::OFF_STATE);
	}

	// Switch on the Normal LED.
	Led::SetState(Led::NORMAL, Led::ON_STATE);

	// Turn off nurse's call
	::ClearRemoteAlarm();

	/********************** TUV *********************************/
	if (GuiApp::IsSstFailure() || IsServiceRequired_)
	{
		Led::SetState(Led::NORMAL, Led::OFF_STATE);
		Led::SetState(Led::LOW_ALARM_URGENCY, Led::OFF_STATE);
		Led::SetState(Led::MEDIUM_ALARM_URGENCY, Led::OFF_STATE);
		Led::SetState(Led::HIGH_ALARM_URGENCY, Led::FAST_BLINK_STATE);
		Sound::Start(Sound::HIGH_URG_ALARM);
		::SetRemoteAlarm();
	}
	/********************** TUV *********************************/
	
	SequentialValue  alarmVolume =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::ALARM_VOLUME);

	// Pass it on to the Sound system
	Sound::SetAlarmVolume(Int32(alarmVolume));

#ifdef BTREE_TEST
	if (!GuiApp::IsSstFailure() && !IsServiceRequired_)
	{
		Sound::CancelAlarm();
	}
#endif

	// Set our state to normal ventilation
	MajorScreenMode_ = NORMAL_VENTILATION;

												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BeginServiceMode [public, static]
//
//@ Interface-Description
// Sets the GUI into Service display.
//---------------------------------------------------------------------
//@ Implementation-Description
// Remove all drawables from Upper and Lower Base Container.
// Remove all possible adjust panel focus.  Reinitialize other statics within
// this subsystem to make sure all possible registrar are initialized.  Create
// the objects used specifically for service mode.  Switch off all Alarm LEDs.
// Cancels the pulsing Medium or High Urgency Alarm sound.
// $[01261] On entry to SST, the green normal LED indicator, shall be off
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::BeginServiceMode(void)
{
	CALL_TRACE("GuiApp::BeginServiceMode(void)");

#ifdef SIGMA_CHINESE
	if (GetGuiState() == STATE_SERVICE)
	{
		// Force English for Service-Mode - SST remains in native language
		Language_ = LanguageValue::ENGLISH;
		LanguageIndex_ = 0;
	}
#endif

	MsgQueue userAnnunciationQ(USER_ANNUNCIATION_Q);

	// Clear the User Annunciation queue.
	userAnnunciationQ.flush();

	// Reset our initial state.normal ventilation
	MajorScreenMode_ = INITIALIZING;

	GuiFoundation::GetLowerBaseContainer().removeAllDrawables();
	GuiFoundation::GetUpperBaseContainer().removeAllDrawables();

	// repaint to clear changed drawables list before instantiating the 
	// service-mode sub-screens over the normal ventilation sub-screens
	GuiFoundation::GetLowerBaseContainer().repaint();
	GuiFoundation::GetUpperBaseContainer().repaint();

	// Clear the Upper and Lower base containers.
	// repaint to clear changed drawables list before instantiating the 
	// service-mode sub-screens over the normal ventilation sub-screens
	GuiFoundation::GetLowerBaseContainer().repaint();
	GuiFoundation::GetUpperBaseContainer().repaint();

	// Clear the Upper and Lower base containers.
    GuiFoundation::Initialize();

	// Make sure no object retains adjust panel focus
	AdjustPanel::TakePersistentFocus(NULL);
	AdjustPanel::TakeNonPersistentFocus(NULL);

	// make sure that all GUI-Apps observers are no longer attach to
	// any setting subjects, because the observers may be going away...
	Subject::DetachFromAllObservers();

	// Reinitialize other statics within this subsystem.
	GuiEventRegistrar::Initialize();
	GuiTimerRegistrar::Initialize();
	NovRamEventRegistrar::Initialize();
	PatientDataRegistrar::Initialize();
	AlarmRender::Initialize();
	ServiceDataRegistrar::Initialize();
	LaptopEventRegistrar::Initialize();

	// zero the screen memory that has been used for normal ventilation
	// sub-screens and is now being used for service-mode (SST) sub-screens
	// since constructors expect to be constructed in zerovars
	// TODO E600 replaced bzero with memeset
    //bzero((char*)(ScreenMemory), sizeof(ScreenMemory));
	memset((Char*)(ScreenMemory), 0, sizeof(ScreenMemory));

	// Construct the Upper and Lower Screen containers.
	ServiceLowerScreen::Initialize();
	ServiceUpperScreen::Initialize();

	// Register for Service Data events
	ServiceDataMgr::RegisterTestItemCallback(GuiApp::ServiceDataEventHappened);

	// Initialize remaining static data members.
	PMessageArea = ServiceLowerScreen::RServiceLowerScreen.getMessageArea();
	PPromptArea = ServiceLowerScreen::RServiceLowerScreen.getPromptArea();

	SAFE_CLASS_ASSERTION(PPromptArea != NULL);
	SAFE_CLASS_ASSERTION(PMessageArea != NULL);

	// Append the Upper and Lower Service Mode Screen containers
	// to the appropriate GUI-Foundations base containers.
	GuiFoundation::GetLowerBaseContainer().addDrawable(&ServiceLowerScreen::RServiceLowerScreen);
	GuiFoundation::GetUpperBaseContainer().addDrawable(&ServiceUpperScreen::RServiceUpperScreen);

	// Create key handlers and miscellaneous BD event handler
	new (&RKeyHandlers) KeyHandlers;
	new (&RMiscBdEventHandler) MiscBdEventHandler;

	// $[07035] While the ventilator is in service mide, the green ...
	// Switch off all Alarm LEDs
	for (Int16 idx = Led::HIGH_ALARM_URGENCY; idx < Led::NUM_LED_ID; idx++)
	{
		Led::SetState((Led::LedId) idx, Led::OFF_STATE);
	}

	// Cancels the pulsing Medium or High Urgency Alarm sound
	Sound::CancelAlarm();

	// Turn off nurse's call
	::ClearRemoteAlarm();

	// Reset the laptop communication flag.
	SetSerialCommunication(FALSE);

	// Set our state to service mode
	MajorScreenMode_ = SERVICE_MODE;

											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmEventHappened [public, static]
//
//@ Interface-Description
// This is the highest-level, global method which handles an alarm annunciation
// event.  This event occurs whenever there is a change in current annunciated
// alarms, there is a new entry in the alarm log, or there is an alarm silence
// request.  It is called on receiving an "alarm update" update event on the
// User Annunciation queue from the Alarm-Analysis subsystem.  We pass the
// event on to interested objects so that the proper alarm rendering can
// occur.
//---------------------------------------------------------------------
//@ Implementation-Description
// Inform the various interested objects of the event, the Alarm and Status
// Area, the More Alarms subscreen, the Alarm Log subscreen, the Upper
// Screen Select Area and switch on LEDs/sounds appropriately.
//
// $[01141] Root cause portion of medium/high alarms flash at LED rate ...
// $[01142] Alarm LED shall flash synchronously with root cause message(s).
// $[01221] The Normal LED must be lit to indicate no alarms.
// $[00430] The remote alarm is activatd during any high or medium urgency
// $[01360] Sigma is capable of creating the following user interface sound.
// $[05011] Low urgency alarm LED shall be continuously displayed.
// $[05014] Normal LED shall be continuously displayed.
// alarm conditions.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::AlarmEventHappened(void)
{
	CALL_TRACE("GuiApp::AlarmEventHappened(void)");

	AlarmStateManager::ProcessEventBasedAlarmUpdates();

	// Process the alarm related screen update.
	IsAlarmUpdateNeeded_ = TRUE;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PatientDataEventHappened [public, static]
//
//@ Interface-Description
// This is the highest-level, global method which handles a change to a
// patient data item. It is called automatically by the Patient-Data subsystem
// whenever any patient datum changes.
// Since this method is called by a higher level task we cannot handle
// the change immediately.  And due to various requirements for updating
// the patient data, we use PatientDataRegistrar::ChangeHappened() method
// to parse the type of patient data and display them accordingly thus
// thus allowing the event to be handled at the proper time.
//---------------------------------------------------------------------
//@ Implementation-Description
// Pop a message onto our own queue.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::PatientDataEventHappened(const PatientDataId::PatientItemId changedDatumId)
{
	CALL_TRACE("GuiApp::PatientDataEventHappened(PatientDataId::PatientItemId"
												" changedDatumId)");

	// Only after GUI screens are constructed, we need to process the
	// the patient data 
	if (MajorScreenMode_ == NORMAL_VENTILATION)
	{											// $[TI1]
		PatientDataRegistrar::ChangeHappened(changedDatumId);
	}
												// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceDataEventHappened [public, static]
//
//@ Interface-Description
// This is the highest-level, global method which handles a change to a
// service data item. It is called automatically by the Service-Data subsystem
// whenever any service datum changes.  Since this method is called by a
// higher level task we cannot handle the change immediately.  We instead
// post a message to the User Annunciation queue informing ourselves
// of the change and thus allowing the event to be handled at the proper
// time.
//---------------------------------------------------------------------
//@ Implementation-Description
// Pop a message onto our own queue.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::ServiceDataEventHappened(const TestDataId::TestDataItemId changedDatumId)
{
	CALL_TRACE("GuiApp::ServiceDataEventHappened(TestDataId::TestDataItemId changedDatumId)");

	ServiceDataRegistrar::ChangeHappened(changedDatumId);
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UserActivityHappened [public, static]
//
//@ Interface-Description
// This method should be called when any user activity occurs (knob is
// turned, keys or touch screen touched).  It resets the timers for
// timing out setting changes.
//---------------------------------------------------------------------
//@ Implementation-Description
// Restarts the timers for main setting and subscreen change timeouts.
//
// $[01074] Main setting button is deselected after 1 minute ...
// $[01087] After 3 minutes of user inactivity ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::UserActivityHappened(void)
{
	// Restart timers
	GuiTimerRegistrar::StartTimer(GuiTimerId::MAIN_SETTING_CHANGE_TIMEOUT);
	GuiTimerRegistrar::StartTimer(GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT);
									// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsSettingsLockedOut  [static]
//
//@ Interface-Description
// Returns a boolean which indicates whether changes to the
// Settings-Validation subsystem have been locked out or not.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply return the value of IsSettingsLockedOut_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
GuiApp::IsSettingsLockedOut(void)
{
	CALL_TRACE("GuiApp::IsSettingsLockedOut(void)");
	return (IsSettingsLockedOut_);						// $[TI1], $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsSettingsTransactionSuccess  [static]
//
//@ Interface-Description
// Returns a boolean which indicates whether the Settings-Validation subsystem
// had a problem sending the last batch of settings to the Breath-Delivery
// system.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply return the value of IsSettingsTransactionSuccess_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
GuiApp::IsSettingsTransactionSuccess(void)
{
	CALL_TRACE("GuiApp::IsSettingsTransactionSuccess(void)");

	return (IsSettingsTransactionSuccess_);				// $[TI1], $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetSettingsLockedOut(Boolean)  [static]
//
// Interface-Description
// Set the IsSettingsLockedOut_ to the given value.	
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================

void
GuiApp::SetSettingsLockedOut(Boolean value)
{
	CALL_TRACE("GuiApp::SetSettingsLockedOut(value)");

	IsSettingsLockedOut_ = value;						// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetSettingsTransactionSuccess [public, static]
//
//@ Interface-Description
// Sets the "settings transaction success" state.  This state is determined by
// whether the last accepted batch of settings was transmitted successfully to
// the Breath-Delivery system or not.  Passed a flag which should be set to
// FALSE if the transmission failed, TRUE otherwise.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the flag in IsSettingsTransactionSuccess_ and inform the
// GuiEventRegistrar of a change in the GUI state.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::SetSettingsTransactionSuccess(Boolean isSettingsTransactionSuccess)
{
	CALL_TRACE("GuiApp::SetSettingsTransactionSuccess(Boolean isSettingsTransactionSuccess)");

	if (UpdateCompletedBatchProcessFlag_)
	{													// $[TI2]
		if (isSettingsTransactionSuccess)
		{												// $[TI2.1]
			UpdateCompletedBatchProcessFlag_ = FALSE;
			GuiEventRegistrar::GuiEventHappened(SETTINGS_TRANSACTION_SUCCESS);
		}												// $[TI2.2]

		IsSettingsTransactionSuccess_ = isSettingsTransactionSuccess;
	}
	else
	{													// $[TI1]
		// If there is a change in the settings transaction success state then store
		// this fact and inform the GuiEventRegistrar so that it may
		// inform all interested objects of the fact.
		if (isSettingsTransactionSuccess != IsSettingsTransactionSuccess_)
		{												// $[TI1.1]
			IsSettingsTransactionSuccess_ = isSettingsTransactionSuccess;

			// If there was a transaction failure then clear the Adjust Panel
			// focus (thus cancelling any changes the operator may be making to
			// settings).
			if (! IsSettingsTransactionSuccess_)
			{											// $[TI1.1.1]
				AdjustPanel::TakeNonPersistentFocus(NULL);
				AdjustPanel::TakePersistentFocus(NULL);
			}											// $[TI1.1.2]

			GuiEventRegistrar::GuiEventHappened(SETTINGS_TRANSACTION_SUCCESS);
		}												// $[TI1.2]
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPressureUnitsSmall [public, static]
//
//@ Interface-Description
// Returns a StringId for the current pressure units (cmH20 or hPa) for
// small buttons.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

StringId
GuiApp::GetPressureUnitsSmall(void)
{
	// $[TI1.1] $[TI1.2]
	return ( PressUnitsSetting_ == PressUnitsValue::CMH2O_UNIT_VALUE ?
				MiscStrs::CM_H20_UNITS_SMALL :
				MiscStrs::HPA_UNITS_SMALL );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPressureUnitsLarge [public, static]
//
//@ Interface-Description
// Returns a StringId for the current pressure units (cmH20 or hPa) for
// large buttons.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

StringId
GuiApp::GetPressureUnitsLarge(void)
{
	// $[TI1.1] $[TI1.2]
	return ( PressUnitsSetting_ == PressUnitsValue::CMH2O_UNIT_VALUE ?
				MiscStrs::CM_H20_UNITS_LARGE :
				MiscStrs::HPA_UNITS_LARGE );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetSerialCommunication [public, static]
//
//@ Interface-Description
// Sets the "IsSerialCommunicationUp_" flag.
//---------------------------------------------------------------------
//@ Implementation-Description
// Store the IsSerialCommunicationUp_ flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::SetSerialCommunication(Boolean isSerialCommunicationUp)
{
	CALL_TRACE("GuiApp::SetSerialCommunication(Boolean isSerialCommunicationUp)");

	if (isSerialCommunicationUp != IsSerialCommunicationUp_)
	{													// $[TI1]
		IsSerialCommunicationUp_ = isSerialCommunicationUp;
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetSerialNumberInfoState()  [static]
//
// Interface-Description
// Return the following status value 
//	1. DATAKEY_NOT_INSTALLED	: Datakey not installed.
//	2. SERIAL_NUMBER_NOT_MATCHED: Datakey installed, but numbers don't match
//	3. SERIAL_NUMBER_OK			: Datakey installed, and numbers do match
// //---------------------------------------------------------------------
// Implementation-Description
// Check the status of the serial number based on
//	1. Is datakey installed.
//	2. Are the serial numbers in the datakey matching the ones on both
//	   BD and GUI flash.
// The only exception which we will skip the serial number checking
// is when there is a short power failure occured.  In this case,
// if the patient settings has been completed, then we will go ahead and
// start normal ventilation mode.
// $[00525] If short power failure or not AC switch, and ....., and serial
// numbers match ...
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================

GuiApp::SerialNumberStates
GuiApp::GetSerialNumberInfoState(void)
{
	CALL_TRACE("GuiApp::GetSerialNumberInfoState(void)");

#if defined SIGMA_DEVELOPMENT
	if (!GuiApp::IsDataKeyInstalled())
	{
		return(SERIAL_NUMBER_OK);
	}
	else if ( !SoftwareOptions::GetSoftwareOptions().isOptionsFieldOkay())
	{
		return( DATAKEY_INVALID);
	}
	else if (GuiApp::AreSerialNumbersMatched_())
	{
		return(SERIAL_NUMBER_OK);
	}
	else
	{
		return(SERIAL_NUMBER_NOT_MATCHED);
	}

#else
	if (GetGuiState() == STATE_ONLINE)
	{												// $[TI1.1]
		if (IsVentStartupSequenceComplete &&
			!LowerScreen::RLowerScreen.isInPatientSetupMode())
		{											// $[TI1.1.1]
			// In normal ventilation mode, a short power failure occured,
			// we continue to breathe once the patient settings are completed.
			// In a sense, we ignore the status of the datakey.
			return(SERIAL_NUMBER_OK);
		}											// $[TI1.1.2]
	}												// $[TI1.2]

	if (!GuiApp::IsDataKeyInstalled())
	{												// $[TI2.1]
		return(DATAKEY_NOT_INSTALLED);
	}
	else if ( !SoftwareOptions::GetSoftwareOptions().isOptionsFieldOkay())
	{												// $[TI2.4]
		return( DATAKEY_INVALID);
	}
	else if	(!GuiApp::AreSerialNumbersMatched_())
	{												// $[TI2.2]
		return(SERIAL_NUMBER_NOT_MATCHED);
	}
	else
	{												// $[TI2.3]
		return(SERIAL_NUMBER_OK);
	}

#endif	// SIGMA_DEVELOPMENT
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetCompressorSerialNum()  [static]
//
// Interface-Description
// Get the CompressorSerialNum_.
//---------------------------------------------------------------------
// Implementation-Description
// Return CompressorSerialNum_ value.
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
const SerialNumber&
GuiApp::GetCompressorSerialNum(void)
{
	return(CompressorSerialNum_);
												// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetProxFirmwareRev()  [static]
//
// Interface-Description
// Get the ProxFirmwareRev_.
//---------------------------------------------------------------------
// Implementation-Description
// Return ProxFirmwareRev_ value.
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
const BigSerialNumber&
GuiApp::GetProxFirmwareRev(void)
{
	if (ProxFirmwareRev_ == BigSerialNumber("NOT INSTALLED"))  
	{
		static BigSerialNumber NotInstalled_;
		// translate "NOT INSTALLED" as sent from BD(char *)

		// TODO E600 - This need to be revisited once BigSerialNumber will support Unicode data.
		// BigSerial Number takes MiscStrs::NOT_INSTALLED_STATUS_MSG which is a language specific wide character string.
		// So BigSerialNumber is need to be converted for wide character data, otherwise there will be loss of data.
		// Temporarily converted MiscStrs::NOT_INSTALLED_STATUS_MSG in char string for compilation.
		// 
		char pTemp[256];
		StringConverter::ToString(pTemp, 256, MiscStrs::NOT_INSTALLED_STATUS_MSG);
		NotInstalled_ = BigSerialNumber(pTemp);
		return NotInstalled_;
	}
	else
	{
		return ProxFirmwareRev_;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetProxSerialNum()  [static]
//
// Interface-Description
// Get the ProxSerialNum_.
//---------------------------------------------------------------------
// Implementation-Description
// Return ProxSerialNum_ value.
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
Uint32
GuiApp::GetProxSerialNum(void)
{
	return(ProxSerialNum_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetGuiDataKeySN()  [static]
//
// Interface-Description
// Get the DataKeyGuiSerialNumber_.
//---------------------------------------------------------------------
// Implementation-Description
// Return DataKeyGuiSerialNumber_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
const SerialNumber&
GuiApp::GetGuiDataKeySN(void)
{
	return(DataKeyGuiSerialNumber_);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetBdDataKeySN()  [static]
//
// Interface-Description
// Get the DataKeyBdSerialNumber_.
//---------------------------------------------------------------------
// Implementation-Description
// Return DataKeyBdSerialNumber_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
const SerialNumber&
GuiApp::GetBdDataKeySN(void)
{
	return(DataKeyBdSerialNumber_);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetGuiSN()  [static]
//
// Interface-Description
// Get the GuiSerialNumber_.
//---------------------------------------------------------------------
// Implementation-Description
// Return GuiSerialNumber_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
const SerialNumber&
GuiApp::GetGuiSN(void)
{
	return(GuiSerialNumber_);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetBdSN()  [static]
//
// Interface-Description
// Get the BdSerialNumber_.
//---------------------------------------------------------------------
// Implementation-Description
// Return BdSerialNumber_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
const SerialNumber&
GuiApp::GetBdSN(void)
{
	return(BdSerialNumber_);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetGuiNovRamSN()  [static]
//
// Interface-Description
// Get the GuiNovRamSerialNumber_.
//---------------------------------------------------------------------
// Implementation-Description
// Return GuiNovRamSerialNumber_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
const SerialNumber&
GuiApp::GetGuiNovRamSN(void)
{
	return(GuiNovRamSerialNumber_);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetBdNovRamSN()  [static]
//
// Interface-Description
// Get the BdNovRamSerialNumber_.
//---------------------------------------------------------------------
// Implementation-Description
// Return BdNovRamSerialNumber_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
const SerialNumber&
GuiApp::GetBdNovRamSN(void)
{
	return(BdNovRamSerialNumber_);
												// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetCompressorSerialNum()  [static]
//
// Interface-Description
// Set the compressorSerialNum_ to the given compressor number.
//---------------------------------------------------------------------
// Implementation-Description
// Set CompressorSerialNum_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void 
GuiApp::SetCompressorSerialNum(SerialNumber compressorSerialNum)
{
	CompressorSerialNum_ = compressorSerialNum;
												// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetProxFirmwareRev()  [static]
//
// Interface-Description
// Set the ProxFirmwareRev_ to the given PROX software rev.
//---------------------------------------------------------------------
// Implementation-Description
// Set ProxFirmwareRev_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void 
GuiApp::SetProxFirmwareRev(BigSerialNumber proxFirmwareRev)
{
	ProxFirmwareRev_ = proxFirmwareRev;
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetProxSerialNum()  [static]
//
// Interface-Description
// Set the ProxSerialNum_ to the given PROX serial number.
//---------------------------------------------------------------------
// Implementation-Description
// Set ProxSerialNum_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void 
GuiApp::SetProxSerialNum(Uint32 proxSerialNum)
{
	ProxSerialNum_ = proxSerialNum;
}

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetProxIsInstalled()  [static]
//
// Interface-Description
// Set the IsInstalled flag based on the board being present.
//---------------------------------------------------------------------
// Implementation-Description
// Set isProxInstall_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void 
GuiApp::SetProxIsInstalled(Boolean isProxInstalled)
{
	IsProxInstalled_ = isProxInstalled;
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetGuiSN()  [static]
//
// Interface-Description
// Set the GuiSerialNumber_ to the given Serial number.
//---------------------------------------------------------------------
// Implementation-Description
// Set GuiSerialNumber_.
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void 
GuiApp::SetGuiSN(SerialNumber guiSN)
{
	GuiSerialNumber_ = guiSN;
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetBdSN()  [static]
//
// Interface-Description
// Set the BdSerialNumber_ to the given Serial number.
//---------------------------------------------------------------------
// Implementation-Description
// Set BdSerialNumber_
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void 
GuiApp::SetBdSN(SerialNumber bdSN)
{
	BdSerialNumber_ = bdSN;
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  RaiseGuiEventPriority()  [static]
//
// Interface-Description
// An alternate way to allow user to modify the GUI event's priority
// for immediate attention.
//---------------------------------------------------------------------
// Implementation-Description
// We first grap the mutex to prevent multiple access to this data
// structure.  Then we assign the desired priority to the event.
// Recalculate the executionBiasCount and executionSequence to reflect
// the new priority.  Finally, we release the mutex.
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void 
GuiApp::RaiseGuiEventPriority(Int16 eventId, Int16 priority)
{
	SAFE_CLASS_ASSERTION(eventId > GUI_NO_EVENT &&
						eventId < MAX_GUI_EVENT);

	// To prevent problems with two tasks trying to access the
	// GuiEventInfoArray while we are inquring the information
	// at the same time.
	RGuiEventAccessMutEx_.request();

	GuiEventInfoArray_[eventId].priority = priority;
	GuiEventInfoArray_[eventId].executionBiasCount = 
	GuiEventInfoArray_[eventId].executionSequence =
				GuiEventInfoArray_[eventId].executionBiasCount +
							GuiEventInfoArray_[eventId].priority;


	// To release the mutex after we finished inquring the information
	RGuiEventAccessMutEx_.release();
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  AreSerialNumbersMatched_
//
// Interface-Description
// Return a boolean value with 'TRUE' being serial numbers on the BD and GUI
// boards matched the data key serial number.
//---------------------------------------------------------------------
// Implementation-Description
// Return TRUE when GuiDataKeySN matches GuiSN and BdDataKeySN matches
// BdSN.  All other cases return FALSE;
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================

Boolean 
GuiApp::AreSerialNumbersMatched_(void)
{
	CALL_TRACE("GuiApp::AreSerialNumbersMatched_(void)");

	Boolean areSerialNumbersMatched = FALSE ;
	SerialNumber defaultSn ;

	if (!SoftwareOptions::GetSoftwareOptions().isVerifySerialNumber())
	{
		// $[TI1]
		areSerialNumbersMatched = TRUE ;
	}
	else
	{
		// $[TI2]
		areSerialNumbersMatched = GetGuiDataKeySN() == GetGuiSN() &&
				GetBdDataKeySN() == GetBdSN() &&
				GetGuiDataKeySN() != defaultSn &&
				GetBdDataKeySN() != defaultSn ;
	}
	
	//return ( areSerialNumbersMatched);
	//TODO E600 - the serial number storage is yet to be sorted out. For now,
	//just return OK all the time
	return TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmEventChangeHappened_ [public, static]
//
//@ Interface-Description
// This is the highest-level, global method which handles an alarm annunciation
// event.  This event occurs whenever there is a change in current annunciated
// alarms, there is a new entry in the alarm log, or there is an alarm silence
// request.  It is called on receiving an "alarm update" update event on the
// User Annunciation queue from the Alarm-Analysis subsystem.  We pass the
// event on to interested objects so that the proper alarm rendering can
// occur.
//---------------------------------------------------------------------
//@ Implementation-Description
// Inform the various interested objects of the event, the Alarm and Status
// Area, the More Alarms subscreen, the Alarm Log subscreen, the Upper
// Screen Select Area and switch on LEDs/sounds appropriately.
//
// $[01141] Root cause portion of medium/high alarms flash at LED rate ...
// $[01142] Alarm LED shall flash synchronously with root cause message(s).
// $[01221] The Normal LED must be lit to indicate no alarms.
// $[00430] The remote alarm is activatd during any high or medium urgency
// alarm conditions.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiApp::AlarmEventChangeHappened_(void)
{
	CALL_TRACE("GuiApp::AlarmEventChangeHappened_(void)");

	if (GetGuiState() == STATE_SST ||
		GetGuiState() == STATE_SERVICE)
	{	// Must be in STATE_SST or STATE_SERVICE		// $[TI1.1]
		return;
	}													// $[TI1.2]

	// Check for a change in settings lockout state
	if (AlarmAnnunciator::GetNoGuiSettingChangesStatus() !=	IsSettingsLockedOut_)
	{													// $[TI2]
#if defined(SIGMA_PRODUCTION)
		// Store new state
		IsSettingsLockedOut_ = AlarmAnnunciator::GetNoGuiSettingChangesStatus();

		// Inform the GUI state registrar that there has been a change in
		// the state of the GUI
		if (IsSettingsLockedOut_)
		{												// $[TI2.1]
			GuiEventRegistrar::GuiEventHappened(SETTINGS_LOCKOUT);
		}
		else
		{	// This check is a preventive procedure to make sure of
			// GUI_ONLINE_READY event occured only when both Bd and Gui
			// are in online state.  Otherwise we initiate a setting lock
			// event.  In a sense, when either Bd or Gui is in vent inop
			// the setting will always be locked no matter of the result
			// from  AlarmAnnunciator::GetNoGuiSettingChangesStatus().
														// $[TI2.2]
			if (GuiApp::GetGuiState() == STATE_ONLINE &&
				GuiApp::GetBdState() == STATE_ONLINE &&
				!IsServiceRequired_)
			{											// $[TI2.2.1]
				GuiEventRegistrar::GuiEventHappened(GUI_ONLINE_READY);
			}
			else
			{											// $[TI2.2.2]
				IsSettingsLockedOut_ = TRUE;
				GuiEventRegistrar::GuiEventHappened(SETTINGS_LOCKOUT);
			}
		}
#endif // defined(SIGMA_PRODUCTION)
	}													// $[TI3]

	//
	// Notify the MoreAlarmsSubScreen to update its display.  This
	// message is ignored if the screen is not active.
	//
	UpperSubScreenArea::GetMoreAlarmsSubScreen()->updateMoreAlarmsDisplay();

	//
	// Notify the AlarmLogSubScreen to update its display.  This
	// message is ignored if the screen is not active.
	//
	UpperSubScreenArea::GetAlarmLogSubScreen()->updateAlarmLogDisplay();

	//
	// Forward alarm update message to UpperScreenSelectArea, so that it can
	// setup the alarm-related button graphics.
	//
	UpperScreen::RUpperScreen.getUpperScreenSelectArea()->updateButtonDisplay();

}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  BoostScreenUpdate_		[static]
//
// Interface-Description
// This method provides an opportunity to increase the priority
// of the upper/lower screen touch event update.
//---------------------------------------------------------------------
// Implementation-Description
// Based on the "screenTouched" argument, we increase the GUI
// GUI_REPAINT_UPPER_EVENT/GUI_REPAINT_LOWER_EVENT event priority
// to force an immediate screen update which will enhance the user
// action feedback.
//---------------------------------------------------------------------
// PreCondition
// CLASS_ASSERTION_FAILURE
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void
GuiApp::BoostScreenUpdate_(Int16 screenTouched)
{
	switch (screenTouched)
	{
	case Touch::NO_SCREEN_TOUCHED:				// $[TI1.1]
		break;
	case Touch::UPPER_SCREEN_TOUCHED:			// $[TI1.2]
		RaiseGuiEventPriority(GUI_REPAINT_UPPER_EVENT,
								GUI_SECOND_HIGHEST_PRIORITY);
		break;
	case Touch::LOWER_SCREEN_TOUCHED:			// $[TI1.3]
		RaiseGuiEventPriority(GUI_REPAINT_LOWER_EVENT,
								GUI_SECOND_HIGHEST_PRIORITY);
		break;
	case Touch::BOTH_SCREEN_TOUCHED:			// $[TI1.4]
		break;
	case Touch::NUM_SCREEN_TOUCHED:				// $[TI1.5]
	default:
		CLASS_ASSERTION_FAILURE();				// Illegal event
	break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  BoostKnobUpdate_
//				[static]
//
// Interface-Description
// This method provides an opportunity to increase the priority
// of the upper/lower screen knob event update.
//---------------------------------------------------------------------
// Implementation-Description
// Based on the "screenTouched" argument, we increase the GUI
// GUI_REPAINT_UPPER_EVENT/GUI_REPAINT_LOWER_EVENT event priority
// to force an immediate screen update which will enhance the user
// action feedback.
//---------------------------------------------------------------------
// PreCondition
// CLASS_ASSERTION_FAILURE
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void
GuiApp::BoostKnobUpdate_(Int16 screenTouched)
{
	switch (screenTouched)
	{
	case AdjustPanel::NO_FOCUS:					// $[TI1.1]
		break;
	case AdjustPanel::UPPER_SCREEN_FOCUS:		// $[TI1.2]
		break;
	case AdjustPanel::LOWER_SCREEN_FOCUS:		// $[TI1.3]
		RaiseGuiEventPriority(GUI_REPAINT_LOWER_EVENT,
								GUI_THIRD_HIGHEST_PRIORITY);
		RaiseGuiEventPriority(GUI_WAVEFORM_EVENT,
								GUI_THIRD_HIGHEST_PRIORITY);
		break;
	case AdjustPanel::NUM_FOCUS:				// $[TI1.4]
	default:
		CLASS_ASSERTION_FAILURE();				// Illegal event
	break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetConfigurationAgentFields_(const DataKeyAgent&, const ConfigurationAgent&)  
//				[static]
//
// Interface-Description
// Set the DataKeyAgent and ConfigurationAgent informatin through
// BdReadyMessage.
//---------------------------------------------------------------------
// Implementation-Description
// Set Compressor serial number, BD software version number,
// datakey installation flag and all types of serial numbers.
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void
GuiApp::SetConfigurationAgentFields_(const BdReadyMessage& rMessage)
{
	SerialNumber defaultSerialNumber;
	const DataKeyAgent& rDataKeyAgent = rMessage.getDataKeyAgent();
	const ConfigurationAgent& rConfigurationAgent = rMessage.getConfigurationAgent();
	const PostAgent &postAgent = rMessage.getPostAgent();

	// Set Compressor serial number
	SetCompressorSerialNum(rConfigurationAgent.getCompressorSerialNumber());

	// Set BD software version number
	SetBdSoftwareRevNum(rConfigurationAgent.getSoftwareRevNum());  

	// Set PROX Revision
	SetProxFirmwareRev(rConfigurationAgent.getProxFirmwareRev());

	// Set PROX Serial Number
	SetProxSerialNum(rConfigurationAgent.getProxSerialNum());

	// Set PROX IsInstalled
	SetProxIsInstalled(rConfigurationAgent.getIsProxInstalled());

	// Set datakey installation flag
	GuiApp::SetDataKeyInstalledFlag(rDataKeyAgent.isInstalled());

	// We check the BD and the GUI serial numbers only if
	// the data key is installed.

	if (DataKeyInstalledFlag_) 
	{												// $[TI1]
		DataKeyGuiSerialNumber_ = rDataKeyAgent.getGuiSerialNumber();
		DataKeyBdSerialNumber_ = rDataKeyAgent.getBdSerialNumber();

		GuiSerialNumber_ = RSystemConfiguration.getFlashSerialNumber();
		BdSerialNumber_ = rConfigurationAgent.getFlashSerialNumber();
		BdNovRamSerialNumber_ = rConfigurationAgent.getBdNovRamSerialNumber() ;
		NovRamManager::GetUnitSerialNumber(GuiNovRamSerialNumber_) ;
	}												// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  SetNextGuiEvent_()  [static]
//
// Interface-Description
// This method will assign the appropriated execution priority based
// on the GUI event's priority and how much time had been elasped since
// the event being executed.
//---------------------------------------------------------------------
// Implementation-Description
// Set lock on the mutex so it will have exclusive control for the access.
// Looping through the GUI events.  Priority assignment will only be
// processed based on the event's stateMask value.  For example,
// GUI_WAVEFORM_EVENT event will not be processed when GUI App is in
// SERVICE_MODE.  For each legible event, we first increment the its
// timeoutCount.  Then, we check the timeoutCount has reached the
// timeoutPeriodConst.  If it had then we increment the executionBiasCount.
// It is the executionBiasCount that we use to determine the
// executionSequence.  We release the mutex after we finished updating
// the  executionSequence.
//---------------------------------------------------------------------
// PreCondition
// None
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
void 
GuiApp::SetNextGuiEvent_(void)
{
	Int16 idx;
	SigmaState guiState = GuiState_;

	// To prevent problems with two tasks trying to access the
	// GuiEventInfoArray while we are updating the information
	// at the same time.
	RGuiEventAccessMutEx_.request();

	for (idx = 0; idx < GuiApp::MAX_GUI_EVENT; idx++)
	{												// $[TI1]
		if (GuiEventInfoArray_[idx].priority != 0)
		{											// $[TI1.1]
			// Determine if we need to execute this task based on the state
			// mask.
			if (GuiEventInfoArray_[idx].stateMask & guiState)
			{										// $[TI1.1.1]
				// Increment the timeoutCount due to a TimeOut happened
				GuiEventInfoArray_[idx].timeoutCount++;

				// Increment the executionBiasCount if the accumulated
				// timeoutCount reaches the timeoutPeriodConst
				if (GuiEventInfoArray_[idx].timeoutCount >=
					GuiEventInfoArray_[idx].timeoutPeriodConst)
				{									// $[TI1.1.1.1]
					GuiEventInfoArray_[idx].executionBiasCount++;
					GuiEventInfoArray_[idx].timeoutCount = 0;
				}									// $[TI1.1.1.2]

				if (GuiEventInfoArray_[idx].executionBiasCount != 0)
				{									// $[TI1.1.1.3]
					// Weight the execution sequence equally between
					// executionBiasCount and priority.
					GuiEventInfoArray_[idx].executionSequence =
							GuiEventInfoArray_[idx].executionBiasCount +
							GuiEventInfoArray_[idx].priority;
				}
				else
				{									// $[TI1.1.1.4]
					GuiEventInfoArray_[idx].executionSequence = 0;
				}
			}
			else
			{										// $[TI1.1.2]
				GuiEventInfoArray_[idx].timeoutCount = 0;
				GuiEventInfoArray_[idx].executionBiasCount = 0;
				GuiEventInfoArray_[idx].executionSequence = 0;
			}
		}											// $[TI1.2]
	}

	// To release the mutex after we finished updating the information
	RGuiEventAccessMutEx_.release();

#if defined FORNOW	
	for (idx = 0; idx < GuiApp::MAX_GUI_EVENT; idx++)
	{
printf("GuiEventInfoArray_[%d] = %04x, %4d, %4d, %4d, %4d, %04x\n",
			idx, 
			GuiEventInfoArray_[idx].stateMask,
			GuiEventInfoArray_[idx].priority,
			GuiEventInfoArray_[idx].timeoutPeriodConst,
			GuiEventInfoArray_[idx].timeoutCount,
			GuiEventInfoArray_[idx].executionBiasCount,
			GuiEventInfoArray_[idx].executionSequence);
			
	}
#endif	// FORNOW	
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  GetNextGuiEvent_()  [static]
//
// Interface-Description
// Return the next event ID to be execute.
//---------------------------------------------------------------------
// Implementation-Description
// Based on the accumulated executionBiasCount, whichever event has the
// highest count will be the next event to be executed.  If there are
// events share the same accumulated executionBiasCount, then the event has
// highest priority will be the next to be executed.
//---------------------------------------------------------------------
// PreCondition
// None
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
Int16 
GuiApp::GetNextGuiEvent_(void)
{
	Int16 idx;
	Int16 nextExecutableEventId=GUI_NO_EVENT;
	Int16 maxExecutionSequence=0;

	// To prevent problems with two events trying to access the
	// GuiEventInfoArray while we are inquring the information
	// at the same time.
	RGuiEventAccessMutEx_.request();

	for (idx = 0; idx < GuiApp::MAX_GUI_EVENT; idx++)
	{											// $[TI1]
		if (GuiEventInfoArray_[idx].priority != 0)
		{										// $[TI1.1]
			if (GuiEventInfoArray_[idx].executionSequence >
				maxExecutionSequence)
			{									// $[TI1.1.1]
				maxExecutionSequence = GuiEventInfoArray_[idx].executionSequence;
				nextExecutableEventId = idx;
			}									// $[TI1.1.2]
		}										// $[TI1.2]
	}											// $[TI2]

	// Once we have found the next event to execute, we should
	// clear the execution count, and executionSequence.
	// So, the next time, this event will start from basic ground.
	if (nextExecutableEventId != GUI_NO_EVENT)
	{											// $[TI3]
		GuiEventInfoArray_[nextExecutableEventId].priority =
					GuiEventInfoArray_[nextExecutableEventId].priorityConst;
		GuiEventInfoArray_[nextExecutableEventId].executionBiasCount = 0;
		GuiEventInfoArray_[nextExecutableEventId].executionSequence = 0;
	}											// $[TI4]

	// To release the mutex after we finished inquring the information
	RGuiEventAccessMutEx_.release();

	return(nextExecutableEventId);
}

//TODO E600 - may be revisited later
//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  AccumulateEvent_()  [static]
//
// Interface-Description
// Look ahead through the user annunciation queue to process as
// many events as possible to avoid of queue overflow.
//---------------------------------------------------------------------
// Implementation-Description
// Peek at the user annunciation queue.  Immediately process the
// following events to keep the queue size small.  The queue events
// that we are interested are: ALARM_UPDATE_EVENT, TASK_MONITOR_MSG,
// BD_GUI_EVENT, TIMER_EVENT, and PATIENT_DATA_TIMER_EVENT.
// We accept this queue event, the set up the appropriated flags to
// indicate which event had occured.  Finally, we process this
// event based on the type of the event.
//---------------------------------------------------------------------
// PreCondition
//---------------------------------------------------------------------
// PostCondition
// End-Method
//=====================================================================
/*void
GuiApp::AccumulateEvent_(MsgQueue &userAnnunciationQ)
{
	Int32 qWord;					// 32 bits from the queue
	UserAnnunciationMsg eventMsg;	// Structure of 32 bits qWord
	Boolean isCompleted = FALSE;
	
	while (!isCompleted)
	{															// $[TI1]
		TaskMonitor::Report();
		if (Ipc::OK == userAnnunciationQ.isMsgAvail())
		{														// $[TI1.1]
			// Plug event data into our UserAnnunciationMsg union so that
			// we can interpret the 32 bits in the message.
			eventMsg.qWord = qWord;

			switch (eventMsg.touchParts.eventType)
			{													// $[TI1.1.1]
			case UserAnnunciationMsg::ALARM_UPDATE_EVENT:		// $[TI1.1.1.1]
				userAnnunciationQ.acceptMsg(qWord);
				AlarmAnnunciator::Update();
				
				// If a new alarm occurs while the screen lock function is
				// active, the screen lock state must be canceled.
				if (AlarmAnnunciator::GetCancelScreenLockStatus() == TRUE)
				{											// $[TI1.1.1.1.1]
					RKeyHandlers.getScreenLockHandler().unlockScreen();
				}											// $[TI1.1.1.1.2]

				break;
			case TaskMonitorQueueMsg::TASK_MONITOR_MSG:			// $[TI1.1.1.2]
				userAnnunciationQ.acceptMsg(qWord);
				TaskMonitor::Report();
				break;
			case UserAnnunciationMsg::BD_GUI_EVENT:				// $[TI1.1.1.3]
				IsBdEventUpdateNeeded_ = TRUE;
				userAnnunciationQ.acceptMsg(qWord);
				BdEventRegistrar::ChangeHappened(
					   (EventData::EventId) eventMsg.bdEventParts.eventId,
					   (EventData::EventStatus) eventMsg.bdEventParts.eventStatus,
					   (EventData::EventPrompt) eventMsg.bdEventParts.eventPrompt);
				break;
			case UserAnnunciationMsg::TIMER_EVENT:				// $[TI1.1.1.4]
				// Inform the timer registrar which will inform the
				// appropriate object that it's timer has gone off.
				IsTimerEventUpdateNeeded_ = TRUE;
				userAnnunciationQ.acceptMsg(qWord);
				GuiTimerRegistrar::TimerChangeHappened(eventMsg);
				break;
			case UserAnnunciationMsg::PATIENT_DATA_TIMER_EVENT:	// $[TI1.1.1.5]
				// Special timer event generated by the Patient-Data subsystem.
				// Patient-Data timers put timer events on the GUI-App queue,
				// just forward notification of the event to the Patient-Data
				// subsystem and it will handle it.
				userAnnunciationQ.acceptMsg(qWord);
				PDTimerRegistrar::TimerEventHappened(eventMsg);
				break;

			default:											// $[TI1.1.1.6]
				isCompleted = TRUE;
				break;
			}
		}
		else
		{														// $[TI1.2]
			isCompleted = TRUE;
		}
	}

	if (IsBdEventUpdateNeeded_)
	{															// $[TI2]
		RaiseGuiEventPriority(GUI_BD_EVENT, GUI_SECOND_HIGHEST_PRIORITY);
	}															// $[TI3]
}*/

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessAlarmAnnunciations_ [private, static]
//
//@ Interface-Description
// Processes all entries on the AlarmUpdateBufferList by calling
// AlarmAnnunciator::Update(). GuiApp calls this method prior to an
// activity that requires an extended time in which it's unavailable
// to process the annunciator updates.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls AlarmAnnunciator::Update() for NUM_BUFF times to clear the
// alarm updates. Cancels the screen lock if a new alarm occurs while
// the screen is locked.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
GuiApp::ProcessAlarmAnnunciations_(void)
{
  Boolean cancelLock = FALSE;

  for (Uint i=0; i<NUM_BUFF; i++)
  {
    AlarmAnnunciator::Update();

    cancelLock = cancelLock || AlarmAnnunciator::GetCancelScreenLockStatus();
  }

  // If a new alarm occurs while the screen lock function is
  // active, the screen lock state must be canceled.
  if ( cancelLock )
  {                                           // $[TI1.1]
    GuiApp::RKeyHandlers.getScreenLockHandler().unlockScreen();
  }                                           // $[TI1.2]
}

#ifdef  BROADCAST_TOUCHED_POSITION
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: broadcastTouchedPosition_ [private, static]
//
//@ Interface-Description
// Broadcast touched position [x, y] to ethernet.
//---------------------------------------------------------------------
//@ Implementation-Description
// TOUCHED_POS_DATA type message will be broadcast to ethernet at port
// TOUCHED_POS_BROADCAST_PORT
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
GuiApp::broadcastTouchedPosition_(Int16 x, Int16 y)
{
 
    Touch::Position touchedPosition = { x, y };
    

    NetworkApp::BroadcastMsg( TOUCHED_POS_DATA, &touchedPosition, 
                        sizeof(Touch::Position), TOUCHED_POS_BROADCAST_PORT );
}
#endif

#if defined(SIGMA_DEVELOPMENT)
//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOn()  [static]
//
// Interface-Description
//	Turn all of the the debug flags for this subsystem on.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	(GuiApp::IsDebugOn(...any class id...))
// End-Method
//=====================================================================

void
GuiApp::TurnAllDebugOn(void)
{
	CALL_TRACE("TurnAllDebugOn()");

	for (Uint32 idx = 0; idx < GuiApp::GA_NUM_WORDS_; idx++)
		GuiApp::ArrDebugFlags_[idx] = ~(0u);
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOff()  [static]
//
// Interface-Description
//	Turn all of the the debug flags for this subsystem off.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//	none
//---------------------------------------------------------------------
// PostCondition
//	(!GuiApp::IsDebugOn(...any class id...))
// End-Method
//=====================================================================

void
GuiApp::TurnAllDebugOff(void)
{
	CALL_TRACE("GuiApp::TurnAllDebugOff()");

	for (Uint32 idx = 0; idx < GuiApp::GA_NUM_WORDS_; idx++)
		GuiApp::ArrDebugFlags_[idx] = 0u;
}


#endif  // defined(SIGMA_DEVELOPMENT)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
GuiApp::SoftFault(const SoftFaultID  softFaultID,
				  const Uint32       lineNumber,
				  const char*        pFileName,
				  const char*        pPredicate)  
{
	CALL_TRACE("GuiApp::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, GUIAPP,
							lineNumber, pFileName, pPredicate);
}
