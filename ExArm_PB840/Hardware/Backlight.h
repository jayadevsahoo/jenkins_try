//#############################################################################
//
//  Backlight_t.h - Header file for Backlight_t class.
//
//
//#############################################################################
#pragma once
#include "i2c.h"

class BackLight_t :
    public I2C
{
public:
    BackLight_t(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout = 30);
    ~BackLight_t(void);

    bool Write(uint8_t *SourceAddress);
    bool Read (uint8_t *DestinationAddress);
};

