
#ifndef ManeuverRefs_HH
#define ManeuverRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: ManeuverRefs - All the external references of various breath 
//		   phases. 
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ManeuverRefs.hhv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added new RM maneuver object references.
//
//  Revision: 001  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Bilevel
//       Description:
//		 	BiLevel initial version.
//
//====================================================================

// Insp Pause Maneuver
class InspPauseManeuver;
extern InspPauseManeuver& RInspPauseManeuver;

// Exp Pause Maneuver
class ExpPauseManeuver;
extern ExpPauseManeuver& RExpPauseManeuver;

class NifManeuver;
extern NifManeuver& RNifManeuver;

class P100Maneuver;
extern P100Maneuver& RP100Maneuver;

class VitalCapacityManeuver;
extern VitalCapacityManeuver& RVitalCapacityManeuver;

#endif // ManeuverRefs_HH 


