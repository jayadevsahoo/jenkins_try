#ifndef WaveformDataBuffer_HH
#define WaveformDataBuffer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: WaveformDataBuffer - A data buffer used for storing a stream
// of waveform data with enough data storage so as to display a full
// waveform on the Waveforms subscreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformDataBuffer.hhv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
//	WARNING:  There is a dependency issue here,
//	WaveformData.hh requires GuiAppClassIds.hh
#include "GuiAppClassIds.hh"
#include "WaveformData.hh"
#include "WaveformDataNum.hh"
//@ End-Usage

class WaveformDataBuffer
{
public:
	WaveformDataBuffer(void);
	~WaveformDataBuffer(void);

	friend class WaveformIntervalIter;		// Iterator is a friend for speedy
											// access purposes
	
	void addSample(WaveformData *pWaveformData);
	void clear(void);
	void operator=(const WaveformDataBuffer &waveformDataBuffer);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	WaveformDataBuffer(const WaveformDataBuffer&);		// not implemented...

	//@ Data-Member: firstSample_
	// Index of the first added value.  Begins at 0 until enough values are
	// added that the data stream wraps around to the start of the array
	// when firstSample_ will point to the value after lastSample_.
	Int32 firstSample_;

	//@ Data-Member: lastSample_
	// Index of the last added value.
	Int32 lastSample_;

	//@ Data-Member: wrappedAround_
	// Flag which indicates if the data stream has wrapped around at least
	// once to the start of the array.  Used to determine whether to move
	// firstSample_ as values are added.
	Boolean wrappedAround_;

	//@ Data-Member: points_
	// An array which stores all the values in the data stream.
	WaveformData samples_[NUM_WAVEFORM_SAMPLES];

	enum
	{
		//@ Constant: WDB_INVALID_INDEX_
		// Used to indicate that firstSample_ or lastSample_ have not been set
		// and cannot be used.
		WDB_INVALID_INDEX_ = -1
	};
};


#endif // WaveformDataBuffer_HH 
