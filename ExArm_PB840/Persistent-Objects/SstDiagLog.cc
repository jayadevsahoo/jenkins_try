#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: SstDiagLog - SST Diagnostic Code Log.
//---------------------------------------------------------------------
//@ Interface-Description
//  This persistent information log contains SST's diagnostic codes.
//  When an error status is detected during SST, a diagnostic code log
//  entry ('CodeLogEntry') is added to this log.  When an entry is
//  added at a time that the log is full, the oldest entry is removed
//  to make room for the new entry.
//
//  This log is derived from 'DiagCodeLog', which manages the internal
//  array of log entries.  There are no virtual methods -- THEY ARE NOT
//  ALLOWED IN NOVRAM!
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This log, working with 'DiagCodeLog', manages an internal fixed
//  array of log entries ('CodeLogEntry').  The fixed array is defined
//  in this class, with a pointer to the array stored in this class's base
//  class.  This allows common functionality between the the other diagnostic
//  logs to be shared in the common base class.
//
//  The base class is also responsible for protecting against multiple
//  threads, therefore this class should NOT be using any semaphores
//  to protect itself -- the base class already has protection.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/SstDiagLog.ccv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah    Date:  08-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Added calling of 'NovRamManager::ReportAuxillaryInfo()' to the
//	initialization method.  Also, changed 'SoftFault()' to a
//	non-inlined method.
//
//  Revision: 003  By:  sah    Date:  31-Jul-1997    DCS Number: 2228
//  Project:  Sigma (R8027)
//  Description:
//	Was passing wrong constant to the base class's 'backoutOverallDiag()'
//	method.
//
//  Revision: 002  By:  sah    Date:  15-Jul-1997    DCS Number: 2175
//  Project:  Sigma (R8027)
//  Description:
//	Added two static methods ('BackoutDiagsOfLastRun()' and
//	'BackoutOverallResultDiag()') for handling the forwarding
//	of these requests to the one instance of this class.  Also,
//	a similar change was made to SstResultTable.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "SstDiagLog.hh"

//@ Usage-Classes...
#include "Array_CodeLogEntry.hh"
#include "NonVolatileMemory.hh"
#include "NovRamManager.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SstDiagLog()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default diagnostic code log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//  (isEmpty())
//@ End-Method
//=====================================================================

SstDiagLog::SstDiagLog(void)
		       : DiagCodeLog(arrEntries_, ::MAX_SST_LOG_ENTRIES,
				     NovRamUpdateManager::SST_LOG_UPDATE)
{
  CALL_TRACE("SstDiagLog()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SstDiagLog()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this fault log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

SstDiagLog::~SstDiagLog(void)
{
  CALL_TRACE("~SstDiagLog()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  logDiagnostic(diagnosticCode)
//
//@ Interface-Description
//  Log the fault indicated by 'diagnosticCode' into this fault log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (!isEmpty())
//@ End-Method
//=====================================================================

void
SstDiagLog::logDiagnostic(const DiagnosticCode diagnosticCode)
{
  CALL_TRACE("logDiagnostic(diagnosticCode)");

  logDiagCode_(diagnosticCode);

  const DiagnosticCode::DiagCodeTypeId  DIAG_CODE_TYPE =
					  diagnosticCode.getDiagCodeTypeId();

  if (DIAG_CODE_TYPE == DiagnosticCode::SHORT_SELF_TEST_DIAG)
  {   // $[TI1] -- 'diagnosticCode' IS an SST diagnostic...
    const DiagnosticCode::ShortSelfTestBits  SST_CODE =
				    diagnosticCode.getShortSelfTestCode();

    if (SST_CODE.testNumber != ::MAX_SST_RESULTS)
    {   // $[TI1.1]
      SstResultTable::IntermediateResultLogged(diagnosticCode);
    }   // $[TI1.2] -- 'diagnosticCode' indicates the completion of SST...
  }
  else if (DIAG_CODE_TYPE == DiagnosticCode::SYSTEM_EVENT_DIAG)
  {   // $[TI2] -- 'diagnosticCode' is system event diagnostic...
    // do nothing...
  }
  else
  {
    // the type of 'diagnosticCode' is invalid for this log...
    CLASS_ASSERTION_FAILURE();
  }

  NovRamUpdateManager::UpdateHappened(NovRamUpdateManager::SST_LOG_UPDATE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize()  [const]
//
//@ Interface-Description
//  A reset sequence occurred, therefore verify the integrity of this
//  instance.  This verification process involves verifying the
//  integrity of this log's "head" and "tail" indices, and verifying
//  the integrity of each of the "active" entries of this log.  If this
//  log is determined to be valid, 'NovRamStatus::ITEM_VERIFIED' is returned.
//  If this log's entries are corrupted, but its infrastructure is valid,
//  the corrupted items are "corrected" and 'NovRamStatus::ITEM_RECOVERED'
//  is returned.  If this log's infrastructure is not valid, the log is
//  fully initialized (from scratch) and 'NovRamStatus::ITEM_INITIALIZED'
//  is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use the base class's initialization method.
//
//  The reason that the 'ReportAuxillaryINfo()' method is not called for
//  a "recovery", is that the log entries, themselves, report that type
//  of failure.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
SstDiagLog::initialize(void)
{
  CALL_TRACE("initialize()");

  NovRamStatus::InitializationId  initStatusId;

  // forward to base class's method...
  initStatusId = initLog_(::MAX_SST_LOG_ENTRIES);

  switch (initStatusId)
  {
  case NovRamStatus::ITEM_INITIALIZED :		// $[TI1]
    // a full initialization of this diagnostic log is needed...
    new (this) SstDiagLog();

    // a corruption was detected somewhere in the "base class" portion of this
    // instance's memory, therefore report to this effect...
    NovRamManager::ReportAuxillaryInfo(this, sizeof(DiagCodeLog),
				       initStatusId);
    break;
  case NovRamStatus::ITEM_RECOVERED :		// $[TI3]
    // a corruption was detected somewhere in the "code entries" portion of
    // this instance's memory, therefore report to this effect...
    NovRamManager::ReportAuxillaryInfo(arrEntries_, sizeof(arrEntries_),
				       initStatusId);
    break;
  case NovRamStatus::ITEM_VERIFIED :		// $[TI2]
    // do nothing...
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(initStatusId);
    break;
  };

  // at this point, the entire log should be valid...
  SAFE_CLASS_POST_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID),
  			    SstDiagLog);

  return(initStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrEntries(rArrEntries)  [const]
//
//@ Interface-Description
//  Initialize the array 'rArrEntries' to contain all of the current
//  diagnostic log entries, in an order from most recent -- at index '0'
//  -- to least recent.  If this log is not full, the entries at the end of
//  the returned array will be cleared to indicate that they are not part
//  of the current entries.  For example, if there are 14 current 
//  entries in this diagnostic log the entries at, and beyond, index
//  '14' will be cleared, and the entries from '0' thru '13' will not.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses base class to get a copy (in the proper order) of all of the
//  current entries of this log.
//---------------------------------------------------------------------
//@ PreCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

void
SstDiagLog::getCurrEntries(AbstractArray(CodeLogEntry)& rArrEntries) const
{
  CALL_TRACE("getCurrEntries()");
  CLASS_PRE_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID));
  SAFE_CLASS_PRE_CONDITION((rArrEntries.getNumElems() >=
						    ::MAX_SST_LOG_ENTRIES));

  // store all of the current entries from the base class into
  // 'rArrEntries'...
  copyCurrEntries_(rArrEntries);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BackoutDiagsOfLastRun(startingSeqNum)  [static]
//
//@ Interface-Description
//  Backout all diagnostics generated back to the sequence number at the
//  beginning of the test, given by 'startingSeqNum'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward this onto the one instance of the SST Diagnostic Log.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SstDiagLog::BackoutDiagsOfLastRun(const Uint startingSeqNum)
{
  CALL_TRACE("BackoutDiagsOfLastRun(startingSeqNum)");

  NonVolatileMemory::P_INSTANCE_->sstDiagLog_.backoutLastDiags(startingSeqNum);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BackoutOverallResultDiag()  [static]
//
//@ Interface-Description
//  Backout the latest overall result diagnostic stored in the SST
//  Diagnostic Log.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forward to the base class's method, passing in the total number of
//  SST tests.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SstDiagLog::BackoutOverallResultDiag(void)
{
  CALL_TRACE("BackoutOverallResultDiag()");

  NonVolatileMemory::P_INSTANCE_->sstDiagLog_.backoutOverallDiag(
							  ::MAX_SST_RESULTS
								);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//				[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
SstDiagLog::SoftFault(const SoftFaultID  softFaultID,
		      const Uint32       lineNumber,
		      const char*        pFileName,
		      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, SST_DIAG_LOG,
                          lineNumber, pFileName, pPredicate);
}
