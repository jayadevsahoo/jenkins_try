#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: FlashProgrammer - This class provides a list of programming
//                          algorithms such as write, erase, and copy 
//                          checksum functions.  Also, it provides a 
//                          way to distinguish which Flash Chip programming
//                          algorithm to execute based on the type of
//                          Flash Chip is currently installed.
//---------------------------------------------------------------------
//@ Interface-Description
//  The static initialize function must be called first before using other 
//  static functions of this class because it determines which Flash chip  
//  programming algorithm to use.   
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions and pre-conditions are used to
//  ensure correctness of software conditons.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/FlashProgrammer.ccv   25.0.4.0   19 Nov 2013 14:16:18   pvcs  $
//
//@ Modification-Log
//
//  Revision 002  By: Rhomere Jimenez  Date: 22-June-2006  DR Number: 6192
//   Project:   Xena
//   Description:
//      Added the capability to be backwards compatible with the pre-cost and
//      Cost reduced boards.
//
//  Revision 001  By: Rhomere Jimenez  Date: 11-April-2006  DR Number: 6192
//   Project:   Xena
//   Description:
//      Initial version.
//=====================================================================
#include "FlashProgrammer.hh"
#include "MemoryMap.hh"
#include "OsUtil.hh"
//@ Code...

//=====================================================================
//
//  Static Variables...
//
//=====================================================================

// Initialize Flash Chip Type to NoFlashChipDetected.
Uint32 FlashProgrammer::flashChipType_ = FlashDevice::NoFlashChipDetected;

// Initialize the FLASH checksum table to zero.
Uint32 FlashProgrammer::checkSumTable_[256] =
{
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize(void)
//
//@ Interface-Description
//       This static method determines what flash chip device is 
//       currenlty installed on this board.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
FlashProgrammer::Initialize(void)
{
    CALL_TRACE("FlashProgrammer::Initialize(void)");

    // If this board is a XENA board use FlashS29AL008D algorithms
    // otherwise use the Cost reduced algorithms.
    if(IsXenaConfig())
    {
        flashChipType_ = FlashDevice::FlashS29AL008D;
    }
    else
    {    
        flashChipType_ = FlashDevice::Flash28F400B;
    }

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlashEraseBlock(volatile Uint32 * address) 
//
//@ Interface-Description
//      This static method erases a block in Flash based on which Flash chip
//      is detected.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Calls the Flash programming algorithm based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. When the function 
//      fails during its operation,return a one. Otherwise it returns 
//      zero indicating a successful erase operation.
//---------------------------------------------------------------------
//@ PreCondition      
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int FlashProgrammer::FlashEraseBlock(volatile Uint32 * address)
{
    CALL_TRACE("FlashProgrammer::FlashEraseBlock(volatile Uint32 * address)") ;    
    Int status = 0;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
            status = Flash28F400B_EraseBlock(address);
            break;
        case FlashDevice::FlashS29AL008D:
            status = FlashS29AL008D_EraseBlock(address);
            break;
        default:
            // An Unexpected Flash Chip Type...         
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return status;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlashWriteWord (volatile Uint32 * address, const Uint data)
//
//@ Interface-Description
//      This static method takes the address of the flash memory word 
//      and the 4-byte data to be programmed.    
//---------------------------------------------------------------------
//@ Implementation-Description
//      Calls the Flash programming algorithm based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. When the function 
//      fails during its operation, return a one. Otherwise it returns 
//      zero indicating a successful write operation.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
Int FlashProgrammer::FlashWriteWord(volatile Uint32 * address, const Uint data)
{
    CALL_TRACE("FlashProgrammer::FlashWriteWord(volatile Uint32 * address, const Uint data)") ;    
    Int status = 0;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
            status = Flash28F400B_WriteWord(address, data);
            break;
        case FlashDevice::FlashS29AL008D:
            status = FlashS29AL008D_WriteWord(address, data);
            break;
        default:
            // An Unexpected Flash Chip Type...         
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return status;

}



//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: GetSubBlockSize(Uint8 index)
//
//@ Interface - Description
//      This static method takes an index to a Block Size array,
//      and returns the sub block size.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Calls the Flash programming function based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault.  Otherwise it returns 
//      the correct Flash sub block size.
//-----------------------------------------------------------------------
//@ PreCondition
//      Index cannot be greater than the number sub-blocks of the detected
//      Flash chip.
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
Uint32 FlashProgrammer::GetSubBlockSize(Uint8 index)
{
    CALL_TRACE("FlashProgrammer::GetSubBlockSize(Uint8 index)") ;    
    Uint32 SubBlockSize = 0;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
            // index must not be greater or equal to the number of sub-blocks.
            if(index >= Flash28F400B_GetNumSubBlocks())
            {
                FREE_ASSERTION(index, OS_PLATFORM , FLASH_PROGRAMMER );
            }
            SubBlockSize = Flash28F400B_GetFlashBlockSize(index);
            break;
        case FlashDevice::FlashS29AL008D:
            // index must not be greater or equal to the number of sub-blocks.
            if(index >= FlashS29AL008D_GetNumSubBlocks())
            {
                FREE_ASSERTION(index, OS_PLATFORM , FLASH_PROGRAMMER );
            }
            SubBlockSize = FlashS29AL008D_GetFlashBlockSize(index);
            break;
        default:
            // An Unexpected Flash Chip Type...         
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return SubBlockSize;

}


//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: GetWsmBusy(void)
//
//@ Interface - Description
//      This static method takes no argument and returns the wsm busy bit.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Based on the identification of the Flash Chip, it will return
//      the WSM busy bit. If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. 
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
Uint32 FlashProgrammer::GetWsmBusy(void)
{
    CALL_TRACE("FlashProgrammer::GetWsmBusy(void)") ;    
    Uint32 wsmBusy = 0;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
        case FlashDevice::FlashS29AL008D:
            wsmBusy = Flash28F400B_WSM_BUSY;
            break;
        default:
            // An Unexpected Flash Chip Type...         
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return wsmBusy;

}

//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: GetTotalSubBlocks(void)
//
//@ Interface - Description
//      This static method takes no argument and returns the number of 
//      total sub-blocks of the Flash Chip.  Since the Flash Chips are 
//      interleaved, the total number of sub-blocks is calculated by 
//      multiplying the number of sub-blocks of the Flash Chip by two.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Calls the Flash programming function based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. Otherwise it returns 
//      the correct total number of sub-blocks.
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
Uint32 FlashProgrammer::GetTotalSubBlocks(void)
{
    CALL_TRACE("FlashProgrammer::GetTotalSubBlocks(void)") ;    
    Uint32 totalSubBlocks = 0;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
            totalSubBlocks = Flash28F400B_GetTotalSubBlocks();
            break;
        case FlashDevice::FlashS29AL008D:
            totalSubBlocks = FlashS29AL008D_GetTotalSubBlocks();
            break;
        default:
            // An Unexpected Flash Chip Type...         
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return totalSubBlocks;

}

//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: GetNumOfSubBlocks(void)
//
//@ Interface - Description
//      This static method takes no argument and returns the number of 
//      sub-blocks of the Flash Chip.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Calls the Flash programming function based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. Otherwise it returns 
//      the correct number of sub-blocks of the Flash chip.
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
Uint32 FlashProgrammer::GetNumOfSubBlocks(void)
{
    CALL_TRACE("FlashProgrammer::GetNumOfSubBlocks(void)") ;    
    Uint32 numOfSubBlocks = 0;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
            numOfSubBlocks = Flash28F400B_GetNumSubBlocks();
            break;
        case FlashDevice::FlashS29AL008D:
            numOfSubBlocks = FlashS29AL008D_GetNumSubBlocks();
            break;
        default:
            // An Unexpected Flash Chip Type...         
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return numOfSubBlocks;

}


//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: FlashGetDeviceId( volatile Uint32 * address )
//
//@ Interface - Description
//      This static method takes an address and returns the Device ID of
//      the Flash Memory Chip.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Calls the Flash programming algorithm based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. Otherwise it returns 
//      the correct Device ID.
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
Int FlashProgrammer::FlashGetDeviceId( volatile Uint32 * address )
{
    CALL_TRACE("FlashProgrammer::FlashGetDeviceId( volatile Uint32 * address )") ;    
    Int deviceId = 0;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
            deviceId = Flash28F400B_GetDeviceId(address);
            break;
        case FlashDevice::FlashS29AL008D:
            deviceId = FlashS29AL008D_GetDeviceId(address);
            break;
        default:
            // An Unexpected Flash Chip Type...
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return deviceId;
}

//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: GetFlashBlockAddress(Uint8 index)
//
//@ Interface - Description
//      This static method takes an index to a Block Address array,
//      and returns the block address.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Calls the Flash programming function based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. Otherwise it returns 
//      the correct block address.
//-----------------------------------------------------------------------
//@ PreCondition
//      Index cannot be greater than the total sub-blocks of the detected
//      Flash chip.
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
Uint32 * FlashProgrammer::GetFlashBlockAddress(Uint8 index)
{
    CALL_TRACE("FlashProgrammer::GetFlashBlockAddress(Uint8 index)") ;    
    Uint32 * flashBlockAddress;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
            // index must not be greater than the total number of sub-blocks.
            if(index > Flash28F400B_GetTotalSubBlocks())
            {
                FREE_ASSERTION(index, OS_PLATFORM , FLASH_PROGRAMMER );
            }
            flashBlockAddress = Flash28F400B_GetFlashBlockAddress(index);
            break;
        case FlashDevice::FlashS29AL008D:
            // index must not be greater than the total number of sub-blocks.
            if(index > FlashS29AL008D_GetTotalSubBlocks())
            {
                FREE_ASSERTION(index, OS_PLATFORM , FLASH_PROGRAMMER );
            }
            flashBlockAddress = FlashS29AL008D_GetFlashBlockAddress(index);
            break;
        default:
            // An Unexpected Flash Chip Type...
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return flashBlockAddress;

}

//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: CopyCheckSumTable(void)
//
//@ Interface - Description
//      This static method takes no argument and returns no value. 
//      It copies the checksum table and stores them into memory.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Copies the checksum table from the start of FLASH_CHECKSUM_BASE to 
//      FLASH_CHECKSUM_BASE plus FLASH_CHECKSUM_SPACE into memory.
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
void
FlashProgrammer::CopyCheckSumTable(void)
{
    CALL_TRACE("FlashProgrammer::CopyCheckSumTable(void)") ;
    Uint8 i = 0;
    for(Uint32 * address = (Uint32 *) FLASH_CHECKSUM_BASE; 
         ((Uint32) address) < (FLASH_CHECKSUM_BASE + FLASH_CHECKSUM_SPACE); 
         address++, i++)
    {
        checkSumTable_[i] = *address;
    }

}


//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: WriteCheckSumTable(void)
//
//@ Interface - Description
//      This static method takes no argument and returns the write 
//      operation of Flash. This method restores the checksum table back 
//      into Flash. 
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Writes the checksum table from memory to the start of FLASH_CHECKSUM_BASE
//      to FLASH_CHECKSUM_BASE plus FLASH_CHECKSUM_SPACE into Flash.
//      If an error occurs during the write operation of Flash,
//      return one else return zero to indicate a successful write operation.
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
Int
FlashProgrammer::WriteCheckSumTable(void)
{
    CALL_TRACE("FlashProgrammer::WriteCheckSumTable(void)") ;
    Int status = 0;
    Uint8 i = 0;
    for(Uint32 * address = (Uint32 *) FLASH_CHECKSUM_BASE; 
         ((Uint32) address) < (FLASH_CHECKSUM_BASE + FLASH_CHECKSUM_SPACE); 
         address++, i++)
    {

         if(FlashWriteWord( address, checkSumTable_[i]))
         {
             status = 1;
             break;
         }
             
    }
    return status;

}


#if defined( SIGMA_COMMON ) || defined( SIGMA_UNIT_TEST )
//=====================================================================
//
//  FlashEraseBlockNonBlocking(), FlashCheck() and FlashEntireChipErase()
//  are used by the Download subsystem executing from PROM.  These 
//  functions must not be called from FLASH so they are defined only 
//  for the COMMON build.
//=====================================================================

//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: FlashEraseBlockNonBlocking(volatile Uint32 * address)
//
//@ Interface - Description
//      This static method takes no argument and does an erase of a 
//      block in Flash based on the given address.  It returns a zero 
//      if successful, otherwise return a one if it is unsuccessful.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Calls the Flash programming algorithm based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. Otherwise it returns 
//      zero indicating a successfull erase operation.
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//      none
//@ End - Method
//=======================================================================
Int FlashProgrammer::FlashEraseBlockNonBlocking(volatile Uint32 * address)
{
    CALL_TRACE("FlashProgrammer::FlashEraseBlockNonBlocking(volatile Uint32 * address)");
    Int status = 0;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
            status = Flash28F400B_EraseBlockNonBlocking(address);
            break;
        case FlashDevice::FlashS29AL008D:
            status = FlashS29AL008D_EraseBlockNonBlocking(address);
            break;
        default:
            // An Unexpected Flash Chip Type...
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return status;

}

//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: FlashCheck(volatile Uint32 * address)
//
//@ Interface - Description
//      Accepts the starting address of the flash memory block.  Returns 
//      the status of the current flash operation to the caller.  Returns
//      zero if the operation is complete with no errors.
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Calls the Flash programming algorithm based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. Otherwise it returns 
//      zero indicating a successful operation.
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//      none
//@ End - Method
//=======================================================================
Int FlashProgrammer::FlashCheck(volatile Uint32 * address)
{
    CALL_TRACE("FlashProgrammer::FlashCheck(volatile Uint32 * address)");
    Int status = 0;
    switch(flashChipType_)
    {
        case FlashDevice::Flash28F400B:
            status = Flash28F400B_Check(address);
            break;
        case FlashDevice::FlashS29AL008D:
            status = FlashS29AL008D_Check(address);
            break;
        default:
            // An Unexpected Flash Chip Type...
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;
    }
    return status;

}

//============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: FlashEntireChipErase(void) 
//
//@ Interface - Description
//      This static method takes no argument and does an erase of the 
//      entire Flash memory chip.  It returns a zero if successful, 
//      otherwise return a one if it is unsuccessful.  
//-----------------------------------------------------------------------
//@ Implementation - Description
//      Calls the Flash programming algorithm based on which Flash chip
//      is detected.  If the identification of the Flash chip cannot 
//      be determined, it will cause a soft fault. Otherwise it returns 
//      zero indicating a successful full erase operation.
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//      none
//@ End - Method
//=======================================================================
Int FlashProgrammer::FlashEntireChipErase(void)
{
    CALL_TRACE("FlashProgrammer::FlashEntireChipErase(void)");
    Int status = 0;
    switch(flashChipType_)
    {
        case FlashDevice::FlashS29AL008D:
            status = FlashS29AL008D_FullChipErase();
            break;
        case FlashDevice::Flash28F400B:  
            // A full erase algorithm is not necessary for this 
            // flash chip type since there is a different way to 
            // do a full erase operation.
        default:
            // An Unexpected Flash Chip Type...
            FREE_ASSERTION(flashChipType_, OS_PLATFORM , FLASH_PROGRAMMER );
            break;

    }
    return status;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
FlashProgrammer::SoftFault( const FaultType  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate)
{
    CALL_TRACE("FlashProgrammer::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    DownloadFault::SoftFault( softFaultID, OS_PLATFORM, FLASH_PROGRAMMER,
               lineNumber, pFileName, pPredicate);

}
#endif // SIGMA_COMMON || SIGMA_UNIT_TEST

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

