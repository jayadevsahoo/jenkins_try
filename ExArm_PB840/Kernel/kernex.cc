#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: kernex.cc - Power on self test Kernel Initialization
//
//---------------------------------------------------------------------
//@ Interface-Description
//
//  >Von
//  This file contains the processor initialization code for the 68040.
//  This is the first set of instructions that execute at CPU reset.
//
//  Free Functions:
//    KernelExecutive    - Test manager after CPU has been initialized
//    KernelError        - Error handler during Kernel testing
//
//  Structures:
//    KernelTests - Executive test structure
//
//  >Voff
//---------------------------------------------------------------------
//@ Rationale
//    This provides basic boot initialization of the processor, and
//    controls the flow of tests during Kernel testing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  >Von
//    The main kernel executive is defined in this module, along with
//    miscellaneous "C" utilities used during Kernel POST.  The
//    executive test process is run after the basic CPU has been
//    initialized.
//
//    KernelExecutive calls each test in sequence, then transfers to
//    either the boot downloader (if no application exists), or the
//    second portion of POST located in the application EEPROM.  Refer
//    to the KernelTests structure for the exact sequence of testing.
//
//  >Voff
//---------------------------------------------------------------------
//@ Fault Handling
//    Faults during POST cause the test in progress to fail.
//---------------------------------------------------------------------
//@ Restrictions
//    none
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/kernex.ccv   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007    By: Gary Cederquist  Date: 14-Dec-1998  DR Number: 5311
//    Project: 840 Cost Reduction
//    Description:
//       Initial version.
//
//  Revision: 006    By: Gary Cederquist  Date: 09-SEP-1997  DR Number: 2471
//    Project: Sigma (R8027)
//    Description:
//       Conditionally included misc.hh for DEBUG only.
//
//  Revision: 005    By: Gary Cederquist  Date: 19-AUG-1997  DR Number: 2361
//    Project: Sigma (R8027)
//    Description:
//       Removed interrupt controller test (68901) since the interrupt
//       controller features of the device are no longer used or enabled.
//
//  Revision: 004    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 003    By: Gary Cederquist  Date: 12-AUG-1997  DR Number: 2361
//    Project: Sigma (R8027)
//    Description:
//       Removed interrupt controller (68901) test.
//
//  Revision: 002    By: Gary Cederquist  Date: 25-APR-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added missing requirement numbers.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
#include "Sigma.hh"
#include "kpost.hh"
#include "kernel.hh"
#include "kernex.hh"
#include "postnv.hh"
#include "BdIoUtil.hh"
#include "Post_Library.hh"
#include "CpuDevice.hh"

// Individual kernel test definitions
#include "address.hh"
#include "kernvram.hh"
#include "rollthun.hh"
#include "tod.hh"
#include "timertst.hh"
#include "wdt.hh"
#include "checksum.hh"
//TODO E600 #include "boot.h"
#include "tables.hh"
#include "PbTimerTest.hh"

#if defined(SIGMA_DEBUG)
    #include "misc.hh"
    #include <stdio.h>
#endif

#if defined(SIGMA_UNIT_TEST)
    #include "DS1286.hh"
#endif

//TODO E600 port !?
/*const char CompilerVersion[] = _VERSION;
const char DateCompiled[]    = __DATE__;*/

//  This creates an XREF to the PromToc to include table.s
//  in the load module even though it's not referenced by 

#if SIGMA_BD_CPU
extern Uint PromToc;
const Uint* PPromToc         = &PromToc;

#endif

//  forward declaration
static void ExitKernel_(void);

// -------------------------------------------------------------------------
// This is the Kernel executive structure for tests AFTER stack DRAM
// has been validated.  Each test is executed in the order they appear
// in this structure, until pass 2 of the watchdog timer test.
//
//
// Structure Members:
//
// testName:        Text of test in progress, used to send status information
//                  to a debug port
//
// testNumber:      Value displayed on diagnostic LED indicator.
//
//
// pTestProcedure:  Pointer to test procedure to execute
// -----------------------------------------------------------------------------


// ------------------------------------------------------------------------
// A KernelTestType contains information needed to process
// an individual test.  The post test executive uses this structure to
// schedule the test, and process the test results.
// ------------------------------------------------------------------------

typedef Criticality (*TestProcedure)(void);

struct  PostTestItem
{
    char *        testName;             // Name of test as a text string
    int           testNumber;           // Diagnostic LED number
    TestProcedure pTestProcedure;       // Test procedure
};


const PostTestItem  KernelTests[] =
{

// Test                  Test                        Test
// Name                  Num                         procedure

    {"Addressing Mode",   POST_ERROR_ADDRESS_MODE,    AddressModeTest}
    ,{"Kernel Novram",     POST_ERROR_NOVRAM,          KernelNovramTest}
#if defined(SIGMA_PRODUCTION)
    ,{"Rolling Thunder",   POST_ERROR_ROLLING_THUNDER, RollingThunderTest}
#endif
    ,{"Time of Day Clock", POST_ERROR_TOD,             TimeOfDayClockTest}
    ,{"8254 Timer",        POST_ERROR_TIMER,           TimerTest}
    ,{"PB Timer",          POST_ERROR_TIMER,           PbTimerTest::Test}
    ,{"Watchdog Timer",    POST_ERROR_WATCHDOG,        WatchdogTimerTest}
    ,{"END OF TESTS",      POST_ERROR_UNUSED,          NULL}
};


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: KernelExecutive (void)
//
//@ Interface-Description
//  >Von
//    Kernel test executive
//
//    INPUTS:  NONE
//
//    OUTPUTS: NONE
//
//    TECHNIQUE
//
//    After stack DRAM is verified a kernel executive is used to call the
//    kernel tests in order.  All tests have the same calling sequence
//    and error handling interface if they fail.
//
//    The executive uses a structure of test pointers to perform each test.
//    The structure members are called in sequence until the watchdog
//    timer test.  After this test the structure index may jump past
//    previously tested tests that were exercised in pass 1 of the
//    watchdog timer test.
//
//    This routine is responsible for lighting the LED indicators before
//    each test starts, and declaring a Kernel error if the test fails.
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
void KernelExecutive(void)
{
	//TODO E600 port

/*
    Criticality testResult;

    CpuDevice::StrobeWatchdog();

#if defined(SIGMA_DEBUG)
    InitializeSerialPorts();
    dprintf("\n#########################\nSTART OF KERNEL EXECUTIVE\n");
#endif

    PKernelNovram->ioReg1Cache = 0;   // contents after kernel.s

    // enable the SAAS now so it is ready during phase 2 POST
    if ( CpuDevice::IsGuiCpu() )
    {
        // $[TI1.1]
        CpuDevice::SetBitsIoReg1( AUDIO_RESET_BIT );
    }
    // $[TI1.2]

    const PostTestItem * pTestItem = KernelTests;

    while (pTestItem->pTestProcedure != NULL)
    {
        // $[TI2.1]
        testResult = PASSED;

        // Set the diagnostic LEDS to the test number
        SetPostStep(pTestItem->testNumber);

#if defined(SIGMA_DEBUG)
        dprintf("Test: %s\n", pTestItem->testName);
#endif

        // Execute the Kernel POST test
        testResult = pTestItem->pTestProcedure();

        UNIT_TEST_POINT(POSTUT_KTEST_RETURN)

        SetTestResult(testResult);

        // Declare a kernel error if major failure occurred
        if ( testResult == MAJOR )
        {
            // $[TI3.1]
            KernelError();
        }
        // $[TI3.2]

        pTestItem++;
    }
    // $[TI2.2]

#if defined(SIGMA_UNIT_TEST)
    while (   PKernelNovram->testingWatchdog
              && !PKernelNovram->watchdogMinTimeExpired) ;
#endif
    UNIT_TEST_POINT(POSTUT_21_3)
*/

    ExitKernel_();     // Transfer control to Phase 2 POST.

}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: ExitKernel_ (void)
//
//@ Interface-Description
//  >Von
//    This function handles the transition processing between Kernel
//    POST and other phases of processing.  The Kernel executive calls
//    this function upon completing the Kernel tests except for
//    the Flash checksum test.  ExitKernel_ uses the results of the Flash
//    checksum test to determine if the bootstrap loader should be
//    invoked or if POST Phase 2 and the application can safely be called.
//    If the checksum test fails, this function automatically invokes
//    the bootstrap loader which downloads and programs the Flash
//    application memory.  If the checksum test passes, this function
//    turns off the BD vent alarm and the GUI remote alarm and calls
//    POST Phase 2 processing.  Upon successful completion, POST Phase
//    2 returns here and control is then transferred to the VRTX O/S
//    boot process.  $[11018] $[11019] $[00510] $[00512]
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

void  ExitKernel_(void)
{
	//TODO E600 port
/*
#if defined(SIGMA_DEBUG)
    dprintf ("\n!!!!! POST KERNEL PASSED !!!!!!");
    dprintf ("\nExitKernel - Transition to Flash\n");
#endif


#if defined( SIGMA_PRODUCTION )
    SetPostStep(POST_ERROR_EEPROMCHECKSUM);

    if (   FlashChecksumTest() == PASSED
           && PKernelNovram->downloadScheduled != SCHEDULE_DOWNLOAD )
    {
        // $[TI1.1]

#if defined(SIGMA_DEBUG)
        dprintf ("Flash Checksum valid\n");
        dprintf ("Calling POST Phase 2\n");
#endif

        //  Wait for the watchdog to reset the processor or until
        //  the maximum watchdog test timer expires causing a major
        //  POST fault.  There is no need to continue POST during
        //  the watchdog timer test at this point (after the flash
        //  is checksumed) since the processor will be reset soon.
        SetPostStep(POST_ERROR_WATCHDOG);
        while (PKernelNovram->testingWatchdog)
        {
            // $[TI2.1]
            // wait for watchdog reset or max timer expires
        }
        // $[TI2.2]

        SwitchAlarms( FALSE );

        UNIT_TEST_POINT(POSTUT_40_1)

        SetPostStep(POST_ERROR_PHASE_2);
        (*(EntryPtr)POST_PHASE2_ENTRY)();

        SetPostStep(POST_ERROR_SIGMA_OS_BOOT);

        //  for application OS boot
        //  allow watchdog strobing interrupt
        CpuDevice::EnableAutovector(ENABLE_AUTOVEC_3);

        // LAN and VRTX timer interrupts are enabled in InitializeSigma

        CpuDevice::InvalidateBothCaches();

        // turn on alarm in case we hang during initialization
        SwitchAlarms( TRUE );

        UNIT_TEST_POINT(POSTUT_40_2)

        (*(EntryPtr)SIGMA_OS_ENTRY)();
    }
    else
#endif // SIGMA_PRODUCTION

    {
        // $[TI1.2]

#if defined(SIGMA_DEBUG)
        dprintf ("Flash Checksum INVALID\n");
        dprintf ("Initializing DRAM\n");
#endif

        //  Wait for the watchdog to reset the processor or until
        //  the maximum watchdog test timer expires causing a major
        //  POST fault.  There is no need to continue POST during
        //  the watchdog timer test at this point (after the flash
        //  is checksumed) since the processor will be reset soon.
        SetPostStep(POST_ERROR_WATCHDOG);
        while (PKernelNovram->testingWatchdog)
        {
            // $[TI3.1]
            // wait for watchdog reset or max timer expires
        }
        // $[TI3.2]

#ifdef E600_840_TEMP_REMOVED
        ClearMemory((Uint*)APP_DRAM_BASE, APP_DRAM_LENGTH);
#endif

        //  Look for manufacturing test code (PBMON) located in FLASH.
        //  Transfer control there upon finding its "magic cookie"
        //  in a well known flash memory location
        if (*PBMON_MAGIC_COOKIE_MEM == PBMON_MAGIC_COOKIE)
        {
            // $[TI4.1]
            DISABLE_INTERRUPTS();
            // clear rolling thunder count before transferring to PBMON
            PKernelNovram->rollingThunderCount = 0;
            SetPostStep(POST_ERROR_PBMON_BOOT);
            (*(EntryPtr)PBMON_ENTRY)();    // does not return
        }
        // $[TI4.2]

        //  release the LAN reset since POST Phase 2 didn't run in this case
        CpuDevice::SetBitsIoReg1( LAN_RESET_BIT );

        SetPostStep(POST_ERROR_DOWNLOAD_BOOT);

        // NOTE: Do not attempt to print using dprintf after clearing RAM.

#ifdef SIGMA_DEVELOPMENT
        SwitchAlarms( FALSE );
        // xtrace boot - disable interrupts - clear any pending
        DISABLE_INTERRUPTS();
        CpuDevice::EnableAutovector(0);
        CpuDevice::ResetAutovectors();
        // LAN and VRTX timer interrupts enabled in boot method avecup
#endif

#ifdef SIGMA_PRODUCTION
        //  for download OS boot
        //  allow watchdog strobing interrupt
        CpuDevice::EnableAutovector(ENABLE_AUTOVEC_3);
        // LAN and VRTX timer interrupts are enabled in InitializeSigma
#endif

        //  disable the Safety Net 10 second POST timers.
        if ( CpuDevice::IsBdCpu() )
        {
            // $[TI5.1]
            *SAFETY_NET_CTL_A = 1;
            *SAFETY_NET_CTL_B = 1;
        }
        // $[TI5.2]

        //  Completed Kernel tests for Download so reset rolling thunder count
        PKernelNovram->rollingThunderCount = 0;

        //  initialize CPU library
        cpu_init();
        cpu_set_target(boot_item_target);

        boot_reset_counter = 0;      // make it a cold start

        CpuDevice::InvalidateBothCaches();
#if defined(SIGMA_UNIT_TEST)
        asm(" illegal ");            // not linked to OS for unit test
#endif
        boot_cold_start();           // boot the download OS
    }
*/

}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: KernelError
//
//@ Interface-Description
//  >Von
//    Stops Kernel testing when called.  The use of this function
//    should only be used for MAJOR failures.  There is no return from
//    this routine, as it will loop forever.
//
//    INPUTS:  NONE
//
//    OUTPUTS: NONE
//
//
//    TECHNIQUE
//
//    This routine loops indefinately strobing the watchdog.  This is
//    slightly different than the non-kernel error handler as it does
//    not record a diagnostic log.  In the case of a kernel failure you
//    might not be able to access memory.
//
//    If the CPU type is BD_CPU_TYPE, the audible alarm is sounded and
//    Vent-INOP is issued while strobing the watchdog.  The GUI sounds
//    only its SAAS alarm.
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

void KernelError (void)
{
    UNIT_TEST_POINT(POSTUT_KERNEL_ERROR)

    MajorPostFailure();

    // $[TI1]
}
