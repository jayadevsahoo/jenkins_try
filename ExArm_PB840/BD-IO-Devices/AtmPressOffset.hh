#ifndef AtmPressOffset_HH
#define AtmPressOffset_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AtmPressOffset - Atmospheric pressure sensor offset.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/AtmPressOffset.hhv   25.0.4.0   19 Nov 2013 13:53:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date: 25-Sep-1997    DR Number: DCS 2410
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
//@ End-Usage

class AtmPressOffset 
{
  public:
    AtmPressOffset( void) ;
    ~AtmPressOffset( void) ;
    AtmPressOffset( const AtmPressOffset&) ;
    void operator=( const AtmPressOffset&) ;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;

	inline Real32 getAtmPressOffset (void);
	inline void setAtmPressOffset ( Real32 offset) ;

  protected:

  private:

	//@ Data-Member: offset_
	// pressure sensor offset as compared to a standard
	Real32 offset_ ;
} ;

// Inlined methods
#include "AtmPressOffset.in"

#endif // AtmPressOffset_HH 
