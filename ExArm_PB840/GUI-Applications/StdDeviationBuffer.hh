
#ifndef StdDeviationBuffer_HH
#define StdDeviationBuffer_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: StdDeviationBuffer - class to calculate standard deriviation
//---------------------------------------------------------------------
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/StdDeviationBuffer.hhv   25.0.4.0   19 Nov 2013 14:08:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sph	   Date:  08-Feb-2000    DCS Number:  5504
//  Project:  NeoMode
//  Description:
//      Added recent-activity range to alarm bars whereby the standard deviation 
//      the last N consecutive breaths is displayed.
//
//  Revision: 001  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "SummationBuffer.hh"

//@ Usage-Classes
class Trigger;
class BreathPhase;
//@ End-Usage

class StdDeviationBuffer: public SummationBuffer
{
  public:
	StdDeviationBuffer(Real32 *pSummationBuffer, Real32 *pDeviationBuffer, Uint32 bufferSize);
	virtual ~StdDeviationBuffer(void);
  
	static void SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL, 
			      const char*       pPredicate = NULL);

	virtual void updateBuffer( const Real32 data) ;
	Real32 getStdDeviation( void) const ;
	Real32 getUpper( void) const ;
	Real32 getLower( void) const ;

  protected:

  private:
	StdDeviationBuffer(void);	    // Declared but not implemented
    StdDeviationBuffer(const StdDeviationBuffer&);    // Declared but not implemented
    void operator=(const StdDeviationBuffer&); // Declared but not implemented

	//@ Data-Member: varianceBuffer_
	// data buffer to collect the correspondable calculated variance.
	SummationBuffer varianceBuffer_;

};


#endif // StdDeviationBuffer_HH 
