#ifndef Arts_HH
#define Arts_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Arts - provides special hardware outputs required for ARTS testing.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Arts.hhv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  6-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes
//@ End-Usage

class Arts {
  public:

    static void SoftFault( const SoftFaultID	softFaultID,
							   const Uint32		lineNumber,
							   const char		*pFileName  = NULL, 
							   const char		*pPredicate = NULL) ;

	static void UpdateOutputs( void) ;
  
  protected:

  private:
    Arts( void) ;					// not implemented...
    ~Arts( void) ;					// not implemented...
    Arts( const Arts&) ;			// not implemented...
    void operator=( const Arts&) ;	// not implemented...

} ;


#endif // Arts_HH 
