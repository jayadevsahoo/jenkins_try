;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1998, Puritan-Bennett Corporation
;=====================================================================
;
;
;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: kernel.s - Initial Startup Code and kernel utilities
;
;---------------------------------------------------------------------
;@ Interface-Description
;  This contains the portion of the POST that is required to get to the
;  point where the first test is performed.  It also contains utilities 
;  written in assembly language used to support other POST tests.
;
;  Contents:
;   InitialVectorTable_          - Startup Vector table for 68040
;   PostVectorTable_             - Exception Table used during POST
;   P_startup                    - Initial startup code
;   _IllegalExceptionHandler__Fv - Unexpected exception/interrupt handler
;   fill_memory (UNIT TEST ONLY) - Fills memory with a counting pattern
;
;@ Implementation-Description
;
;  This module includes the initial Vector Table that is used up to the
;  point that the testing routines can start hooking in special test
;  handlers. The Vector Table also includes the starting address of POST
;  and the initial value of the Stack Pointer.  Initially the Boot Prom
;  appears at its normal address and at 0.  The processor fetches the
;  starting Stack pointer from address 0-3 and the starting program
;  counter from address 4-7.  The initial value for the stack pointer
;  is in the Non-Volatile memory.  This memory is reserved for POST
;  use.  The initial program counter will point to the boot PROM
;  entry point.
;
;  SEQUENCE OF EVENTS
;
;  Once the Stack Pointer and Program Counter are loaded, this module
;  programs the hardware and processor registers to enable the running
;  of the Kernel POST test.  It runs some preliminary processor confidence
;  tests and then transfers control to the KernelExecutive which exectutes 
;  each test in sequence.
;
;  First a portion of the DRAM is tested so that is can be used for a
;  stack and for variables.  Once the Kernel memory region is tested,
;  the Vector table is copied to the top of that region.  The stack
;  pointer is set to be just below the vector table (since it grows 
;  downwards in memory).  From that point on, tests that require special
;  interrupt handlers can modify the vector table.  
;
;@ Restrictions
;  
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
;
;@(#) $Header:   /840/Baseline/Kernel/vcssrc/kernel.s_v   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
;
;@ Modification-Log
;
;  Revision: 008    By: Gary Cederquist  Date: 20-Apr-1999  DR Number: 5366
;    Project: 840 Cost Reduction
;    Description:
;       Included 50us delay loop after recalling NOVRAM array contents to
;		SRAM. The Simtek specification implies that SRAM should not be
;		accessed during the 20us recall time.
;
;  Revision: 007    By: Gary Cederquist  Date: 14-Dec-1998  DR Number: 5311
;    Project: 840 Cost Reduction
;    Description:
;       Initial version.
;
;  Revision: 006    By: Gary Cederquist  Date: 04-DEC-1997  DR Number: 2516
;    Project: Sigma (R8027)
;    Description:
;       Manually recall NOVRAM array contents during power up.
;
;  Revision: 005    By: Gary Cederquist  Date: 09-SEP-1997  DR Number: 2471
;    Project: Sigma (R8027)
;    Description:
;       Conditionally included debug.inc for DEBUG only.
;
;  Revision: 005    By: Gary Cederquist  Date: 19-AUG-1997  DR Number: 2361
;    Project: Sigma (R8027)
;    Description:
;       Added check for presence of 68901 to enable the DRAM refresh
;       strobe or to ignore it for boards without a 68901.
;
;  Revision: 004    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;    Project: Sigma (R8027)
;    Description:
;       Added testable item annotations.
;
;  Revision: 003    By: Gary Cederquist  Date: 16-MAY-1997  DR Number: 2121/2010
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options from source, using command line instead.
;       Moved PROM checksum test before DRAM test to allow time for
;       DRAM controller start up using 8 external RAS only strobes from 
;       the 901.
;
;  Revision: 002    By: Gary Cederquist  Date: 25-APR-1997  DR Number: 1996
;    Project: Sigma (R8027)
;    Description:
;       Added missing requirement numbers.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================

       INCLUDE  kernel.inc
       IFDEF    SIGMA_DEBUG
       INCLUDE  debug.inc
       ENDC

       XDEF        _crt0_cold_start
       XDEF        _P_startup__Fv
       XDEF        IntegerUnitTestReturnLocation
       XDEF        KernelDramTestReturnLocation
       XDEF        _IllegalExceptionHandler__Fv
	   XDEF        KernelEndNovramProc
       XDEF        KernelNo68901

       XREF        KernelDramTest
       XREF        DRAMRefreshTimerTest
       XREF        _KernelExecutive__Fv
       XREF        _KernelError__Fv
       XREF        _UnexpectedInterruptHandler__FPc         
       XREF        _BootEpromTest__Fv
       XREF        _IntegerUnitTest__Fv
       XREF        _SetTestResult__FUc
       XREF        _ChecksumUnitTest__Fv
       XREF        _PrintInitInterrupt
       XREF        _DebugUnexpectedInterruptHandler__FPc

;---------  Data -------------------

; IMPORTANT: any "crt0" name is used by XTRACE and should not be renamed.

       XDEF        _crt0_init_stktop
_crt0_init_stktop   EQU  KRAM_START_ADR+KRAM_BYTES


;=====================================================================
;   InitialVectorTable_ is the exception vector table the 68040 uses 
;   at power-up.  The stack pointer is initialized to the end of POST 
;   NOVRAM (1k).  The initial PC is set to this module's entry point 
;   (P_startup).  This table is (re)located to the beginning of the 
;   EPROM.  This table is used a new table is created in DRAM after 
;   DRAM is tested.
;=====================================================================
     SECTION vectors,,R

InitialVectorTable_:

     DC.L  P_INITIAL_STACK             ; initial stack pointer
     DC.L  P_startup                   ; initial program counter
     DCB.L 30,_IllegalExceptionHandler__Fv  ; unexpected interrupt handler
     IFDEF  SIGMA_DEBUG                ; {
     DC.L  _PrintInitInterrupt         ; TRAP 0 - development print driver
     DCB.L 223,_IllegalExceptionHandler__Fv ; unexpected interrupt handler
     ELSEC                             ; } ELSEC { 
     DCB.L 224,_IllegalExceptionHandler__Fv ; unexpected interrupt handler
     ENDC                              ; } IFDEF  SIGMA_DEBUG 


;=====================================================================
;  PostVectorTable_ provides the Exception Vector Table for POST that
;  is used once the Kernel DRAM has been tested. From this point on,
;  tests can hook special exception handlers for performing their
;  tests.
;
; Note: this just provides a (relocatable with the linker) block off 
;       memory allocation large enough to hold a 68040 Exception Table.  
;       The actual contents will be copied and modified at run time.
;=====================================================================
        SECTION post_vectors,,D

PostVectorTable_:
         DS.L  256

;=====================================================================
;@ Free-Function: P_startup - the starting point for POST code
;
;@ Interface-Description
;
;  This is the initial starting point for the processor on powerup or when
;  it is reset.
;
;  This routine:
;      1. Sets the Diagnostic Leds to a value to indicate that POST has 
;         started
;
;      2. The Boot PROM initially appears at 2 locations in memory;
;	      it's permanent location and at 0.  This allows the 68040 to
;	      load it's initial program counter (PC) and Stack Pointed
;	      (SP).  The PC and SP are in a structure called the Exception
;	      Vector Table (also called the Vector Table).  The 68040 has a
;	      register called the Vector Base Register (VBR) that
;	      determines where the Vector Table is located in memory.  When
;	      the processor is initially turned on or reset, the VBR
;	      pointer to address 0.  Since the Boot PROM appears at 0 as
;	      well as it's permanent location, the PC and SP can be
;	      loaded.  After the PS and SP are loaded, the VBR is set to
;	      the Boot PROM permanent location and a special location is
;	      written that disables the Boot PROM from answering at address 0.
;
;      3. Setup the processor's Data and Code cache.
;
;      4. Enable NOVRAM autostore so POST test results are persistent.
;
;      5. Initialize the DRAM controller and performs the memory accesses
;         necessary to make it startup.
;
;      6. Setup and starts the DRAM refresh timer and checks that it is
;         running.
;
;         NOTE: The DRAM refresh timer is only checked to see if it is
;               running, it is not measured to see if it is running at
;               the proper frequency.
;
;               per the POST Design specification:
;                  If the timer is running too slow then parity errors will
;                  occur.  It is acceptable for the timer to run too fast.
;
;      7. Perform a test of the portion of DRAM that will be used by POST.
;
;      8. Setup the Vector Table in DRAM and point the VBR to it. Set up
;         the Stack Pointer.
;
;    Inputs:  NONE
;
;    Outputs: NONE  
;
;  REQUIREMENTS: $[11001] $[11008] $[11009]
;
;---------------------------------------------------------------------
;@ Implementation-Description
;
;
;  REGISTER USAGE: d0,d7,a0,a6
;
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================
        SECTION code,,C

; void Kernel()  {
_crt0_cold_start:
P_startup:
_P_startup__Fv:

; This function may be entered through power-up or through a branch to
; _crt0_cold_start from the VRTX boot code or development tools.
; On power-up, the stack pointer is initialized from location $0.  
; On entry from the boot code, the stack pointer is not initialized.  
; Therefore, these instructions support a direct branch back to POST 
; without going through power-up.  This is _required_ when using the
; VRTX boot code so it has someplace to go if things go wrong.

        IFDEF  SIGMA_DEVELOPMENT      ;{
        move.w   #$2700,SR            ; disable interrupts
        movea.l  #P_INITIAL_STACK,SP  ; set up initialization stack
        ENDC                          ;}

; set the processor's vector base register to point to the vector table
; in boot prom at it's permanent location               
        move.l   #InitialVectorTable_,D1
        movec    D1,VBR

  
        IFDEF  SIGMA_DEBUG
        BRANCH_IF_GUI not_bd
        move.b   DISABLE_TEN_SECOND_TIMER,SAFETY_NET_CONTROL_A
        move.b   DISABLE_TEN_SECOND_TIMER,SAFETY_NET_CONTROL_B
not_bd:
        ENDC  ;IFDEF SIGMA_DEBUG


;***************************************************************************
;**** Continue Processor Initialization
;***************************************************************************
        POST_STEP   POST_ERROR_PROCESSOR_INITIALIZATION


; CINVA invalidates the cache for data and instructions.  At this point,
; there is nothing in cache that we want pushed.  Dirty data
; is left in the data cache, but since every start through POST is
; a cold start there's nothing in cache that we want pushed to ram.
; CINVA invalidates cache therefore no dirty cache should be executed.
; A bus error at this point is trapped and declared as a major fault.
        cinva    bc

; The following line disables code/data caching
        move.l   #0,d1
        movec.l  d1,CACR

; Initialize MMU registers
        move.l   #0,d1
        movec.l  d1,TC          ; Translation control register
        movec.l  d1,DTT0        ; Data Transparent register
        movec.l  d1,DTT1        ; Data Translation register
        movec.l  d1,ITT0        ; Instruction Transparent register
        movec.l  d1,ITT1        ; Instruction Translation egister

;***************************************************************************
;**** enable instruction cache                                          ****
;**** VRTX enables data and instruction cache after initializing MMU    ****
;***************************************************************************
        move.l   #CACR_INITIAL_VALUE,d1      ; enable instruction caching
        movec.l  d1,CACR

;***************************************************************************
;    Enable NOVRAM autostore - where we store test results
;    reset the in82596a Ethernet controller  - for warmboot
;    assert SAAS reset - for warmboot
;
;    LAN is held in reset until xtrace boot (for DEVELOPMENT) 
;    and until the Ethernet Test in POST Phase 2 (for PRODUCTION)
;***************************************************************************

        move.b   #IO1_NOVRAMNE,IO_REGISTER_1  ; set NOVRAM command select

;    Recall the contents of the NOVRAM array if it has not already
;    been recalled as indicated by the IO1_BD_NV_RECALL_NOT_DONE flag
;    on the BD and the IO2_GUI_NV_RECALL_DONE flag on the GUI.

		move.b   IO_REGISTER_1,d2
		andi.b   #IO1_BD_CPU,d2             ; GUI or BD?
		beq.s    CheckGuiNovramState_		; // $[TI6.1] GUI
		                             		; // $[TI6.2] BD

; check BD NOVRAM state
		move.b   IO_REGISTER_1,d2
		andi.b   #IO1_BD_NV_RECALL_NOT_DONE,d2 ; NOVRAM recall not done?
		bne.s    RecallNovramArray_			; // $[TI7.1] recall not done
                                            ; // $[TI7.2] recall done
		bra      EnableAutostore_

CheckGuiNovramState_:
		move.b   IO_REGISTER_2,d2
		andi.b   #IO2_GUI_NV_RECALL_DONE,d2 ; NOVRAM recall done?
		bne      EnableAutostore_			; // $[TI8.1] recall done
                                            ; // $[TI8.2] recall not done

RecallNovramArray_:
        move.b  NMI_SOURCE_REGISTER,d0
        andi.b  #NMI_REV_MASK,d0
        cmpi.b  #NMI_REV_COST_REDUCE,d0     ; which NOVRAM version
        beq     SimtekNovramRecall_         ; // $[TI9.1] Simtek
                                            ; // $[TI9.2] Xicor
XicorNovramRecall_:
		move.w   NOVRAM_BANK0_RECALL,d0
		move.w   NOVRAM_BANK1_RECALL,d0
		move.w   NOVRAM_BANK2_RECALL,d0
		move.w   NOVRAM_BANK3_RECALL,d0
		bra      EndRecallProc_

SimtekNovramRecall_:
		move.w   SIMTEK_NOVRAM_BANK0_RECALL1,d0
		move.w   SIMTEK_NOVRAM_BANK0_RECALL2,d0
		move.w   SIMTEK_NOVRAM_BANK0_RECALL3,d0
		move.w   SIMTEK_NOVRAM_BANK0_RECALL4,d0
		move.w   SIMTEK_NOVRAM_BANK0_RECALL5,d0
		move.w   SIMTEK_NOVRAM_BANK0_RECALL6,d0    ; initiates RECALL cycle
		
EndRecallProc_:
		move.l   #300,d0			; wait for 50us
WaitForRecall_:
		dbra  	 d0,WaitForRecall_

; rewrite the POST step and error code in NOVRAM that was overwritten
; by the NOVRAM recall operation

        POST_STEP   POST_ERROR_PROCESSOR_INITIALIZATION

		move.b   #0,IO_NV_RECALL_DONE_REGISTER  ; set recall done

EnableAutostore_:

        move.b  NMI_SOURCE_REGISTER,d0
        andi.b  #NMI_REV_MASK,d0
        cmpi.b  #NMI_REV_COST_REDUCE,d0     ; which NOVRAM version
        beq     SimtekNovramStoreEnable_    ; // $[TI10.1] Simtek
                                            ; // $[TI10.2] Xicor
XicorNovramStoreEnable_:
        move.w   #NOVRAM_COMMAND_ENABLE,NOVRAM_BANK0_COMMAND0
        move.w   #NOVRAM_COMMAND_CONFIRM,NOVRAM_BANK0_COMMAND1
        move.w   #NOVRAM_ENABLE_AUTOSTORE,NOVRAM_BANK0_COMMAND0
  
        move.w   #NOVRAM_COMMAND_ENABLE,NOVRAM_BANK1_COMMAND0
        move.w   #NOVRAM_COMMAND_CONFIRM,NOVRAM_BANK1_COMMAND1
        move.w   #NOVRAM_ENABLE_AUTOSTORE,NOVRAM_BANK1_COMMAND0

        move.w   #NOVRAM_COMMAND_ENABLE,NOVRAM_BANK2_COMMAND0
        move.w   #NOVRAM_COMMAND_CONFIRM,NOVRAM_BANK2_COMMAND1
        move.w   #NOVRAM_ENABLE_AUTOSTORE,NOVRAM_BANK2_COMMAND0
  
        move.w   #NOVRAM_COMMAND_ENABLE,NOVRAM_BANK3_COMMAND0
        move.w   #NOVRAM_COMMAND_CONFIRM,NOVRAM_BANK3_COMMAND1
        move.w   #NOVRAM_ENABLE_AUTOSTORE,NOVRAM_BANK3_COMMAND0

		bra		KernelEndNovramProc
  
SimtekNovramStoreEnable_:
; The Simtek AutoStore feature is always enabled - no command necessary

;***************************************************************************
;    reset NOVRAM command mode
;***************************************************************************
KernelEndNovramProc:
        move.b   #0,IO_REGISTER_1             ; reset command mode

;***************************************************************************

; disable the special mapping of memory space that allowed the boot up
; this has the side-effect of masking autovectors 3,4,5,6 
        move.b   #0,P_BOOT_MAP_DISABLE_ADR

;******************************************************************************
;       Clear pending interrupts
;******************************************************************************
;   Clear the autovectors for the timers which may have had an initial 
;   interrupt pending before we initialize them (in timertst)
;   Since the interrupts are masked, initializing the counters is not
;   necessary at this point.  Any pending interrupt is cleared here
;   prior to any initialization.

;   counter/timer 0 connected to Autovector 3
        move.b   #0,AUTOVEC3_RESET_ADR
;   counter/timer 1 connected to Autovector 4
        move.b   #0,AUTOVEC4_RESET_ADR
;   counter/timer 2 connected to Autovector 6 - VRTX Clock
        move.b   #0,AUTOVEC6_RESET_ADR
;   Ethernet controller interrupt connected to Autovector 5
        move.b   #0,AUTOVEC5_RESET_ADR


;******************************************************************************
;              Integer Unit test
;******************************************************************************
Integer_Unit_Test:
        POST_STEP   POST_ERROR_INTEGER_UNIT
        jmp      _IntegerUnitTest__Fv
IntegerUnitTestReturnLocation:

        movea.l  #P_INITIAL_STACK,sp    ; reset stack pointer


;***************************************************************************
;              Enable level 7 "NMI" interrupts 
;***************************************************************************
        STROBE_WATCHDOG
        move.w   #$2600,SR      ; process any pending NMI

;***************************************************************************
;**** Enable the DRAM controller                                        ****
;***************************************************************************

        move.b   #0,P_DRAM_ENABLE_ADR

;***************************************************************************
;**** Start the DRAM Refresh Timer                                      ****
;***************************************************************************
        POST_STEP    POST_ERROR_DRAM_REFRESH_TIMER

        move.b  NMI_SOURCE_REGISTER,d0
                                            UNIT_TEST_POINT POSTUT_10_3
        andi.b  #NMI_REV_MASK,d0
        cmpi.b  #NMI_REV_COST_REDUCE,d0     ; 68901 present?
        beq     KernelNo68901               ; // $[TI5.1] no 68901
                                            ; // $[TI5.2] 68901 present

        move.b  #P_REFRESH_TIMER_COUNT,M68901_TADR
        move.b  #P_REFRESH_MODE_VALUE,M68901_TACR      

;***************************************************************************
;**** Test that the DRAM refresh Timer is running                       ****
;***************************************************************************
        STROBE_WATCHDOG
        move.l   #DRAMRefreshTimerTestReturnLocation,a6
        jmp      DRAMRefreshTimerTest
DRAMRefreshTimerTestReturnLocation:
        cmp.l    #PASSED,d7
        bne      kernel_test_failed         ; // $[TI1.1]  failed
                                            ; // $[TI1.2]  passed

KernelNo68901:
                                            UNIT_TEST_POINT POSTUT_REFRESH_OK
;*************************************************************************
;**** Verify the boot prom is valid.  Uses stack space in NOVRAM static RAM.
;**** This also gives the DRAM which requires 8 refresh cycles from the
;**** 901 (15us/cycle) to start plenty of time to start up.
;*************************************************************************
        POST_STEP   POST_ERROR_BOOT_EPROM
        jsr      _BootEpromTest__Fv
        cmp.l    #PASSED,d0
        bne      kernel_test_failed         ; // $[TI2.1] failed
                                            ; // $[TI2.2] passed


;***************************************************************************
;**** perform a memory test on the portion of DRAM used by the POST     ****
;***************************************************************************
        POST_STEP   POST_ERROR_DRAM_STACK
        move.l   #KernelDramTestReturnLocation,a6
        jmp      KernelDramTest
KernelDramTestReturnLocation:

        cmp.l    #PASSED,d7
        bne      kernel_test_failed         ; // $[TI3.1] failed
                                            ; // $[TI3.2] passed
                                            UNIT_TEST_POINT POSTUT_KDRAM_OK

;***********************************************************************
;**** Setup the Stack Pointer in DRAM now that it's tested          ****
;**** Done using NOVRAM stack at this point                         ****
;***********************************************************************
        move.l   #KERNEL_STACK_POINTER,sp


;*************************************************************************
;**** Setup the Exception Vector Table that will be used from here on ****
;*************************************************************************
        STROBE_WATCHDOG
        movea.l  #InitialVectorTable_,a0
        movea.l  #PostVectorTable_,a1
        move.l   #255,d0
copy_loop:
        move.l   (a0)+,(a1)+
        dbra     d0,copy_loop

; replace the initial SP and PC in case a spurious vector is presented
        movea.l  #PostVectorTable_,a1
        move.l   #_IllegalExceptionHandler__Fv,0(a1)
        move.l   #_IllegalExceptionHandler__Fv,4(a1)
  
        move.l   #PostVectorTable_,D0
        movec    D0,VBR
        STROBE_WATCHDOG

_main:
; // $[TI4.1]
                                            UNIT_TEST_POINT POSTUT_CHECKSUM_OK
        jmp      _KernelExecutive__Fv   ;go to the Kernel POST shell   

;--------------------------------------------------------------------------
;---- any test that fails in an unrecoverable manner should jump here  ----
;--------------------------------------------------------------------------
kernel_test_failed:
; // $[TI4.2]
                                            UNIT_TEST_POINT POSTUT_KTEST_FAILED
        pea      MAJOR   
        jsr      _SetTestResult__FUc         ; log the error
        jmp      _KernelError__Fv            ; end of the road

; }

;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: _IllegalExceptionHandler__Fv - Handler for unexpected
;                 processor exceptions
;
;@ Interface-Description
;
;  This provides a means of dealing with exceptions that are unexpected.
;  A pointer to this routine is placed in the Exception Vector Table for any
;  exceptions that are unexpected and when there are minimal resources
;  available to provide an indication of the failure that caused the
;  exception.
;
;    Inputs:  NONE
;
;    Outputs: NONE  
;
;---------------------------------------------------------------------
;@ Implementation-Description
;
;
;  REGISTER USAGE
;
;     Provide a list of registers affected and their use in this routine.
;
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================

; void  IllegalExceptionHandler() {
_IllegalExceptionHandler__Fv:
  
        IFDEF SIGMA_DEBUG 
        movem.l  a0-a6/d0-d7,-(sp)
        move.l   sp,-(sp) 
        jsr      _DebugUnexpectedInterruptHandler__FPc         
        addq.l   #4,sp
        movem.l (sp)+,a0-a6/d0-d7
        rte  

        ELSEC
        move.l   sp,-(sp) 
        jsr      _UnexpectedInterruptHandler__FPc         
        ENDC
; // $[TI1]
; }


;-----------------------------------------------------------------------------                  
        IFDEF    SIGMA_UNIT_TEST   ; {
        XREF     UT_POSTUT_16
;***************************************************************************
;**** This routine is used by unit test to fill memory with a  **************
;**** counting pattern. It is NOT part of the POST Code.      **************
;***************************************************************************

;***************************************************************************
;**** WARNING: This module can use d0, d1 and a0 only because **************
;****          of the placement of the emulator jump to it.   **************
;***************************************************************************
        XDEF     fill_memory
fill_memory:   
        move.l   #$40000,a0
        move.l   #$10000,d0
        moveq.l  #0,d1
fill_loop:     
        move.l   d0,(a0)+
        add.l    d0,d1
        subq.l   #1,d0
        bne.s    fill_loop
        jmp      UT_POSTUT_16

        ENDC                      ; } // SIGMA_UNIT_TEST

        END
