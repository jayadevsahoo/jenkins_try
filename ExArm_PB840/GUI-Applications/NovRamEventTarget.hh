#ifndef NovRamEventTarget_HH
#define NovRamEventTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

//====================================================================
// Class: NovRamEventTarget - A target to handle NovRam update events.
// Classes can specify this class as an additional parent class and register
// to be informed of NovRam update events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/NovRamEventTarget.hhv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "NovRamUpdateManager.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class NovRamEventTarget
{
public:
	virtual void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
																	updateId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	NovRamEventTarget(void);
	virtual ~NovRamEventTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	NovRamEventTarget(const NovRamEventTarget&);	// not implemented...
	void operator=(const NovRamEventTarget&);		// not implemented...
};


#endif // NovRamEventTarget_HH 
