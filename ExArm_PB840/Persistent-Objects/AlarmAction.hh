
#ifndef AlarmAction_HH
#define AlarmAction_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  AlarmAction - Alarm Event Item.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/AlarmAction.hhv   25.0.4.0   19 Nov 2013 14:18:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
#include "TimeStamp.hh"
#include "Array_MessageName_NUM_MESSAGE.hh"
//@ End-Usage


struct AlarmAction
{
  public:
    //@ Type:  SystemLevelId
    //  This defines the different types of system events that instances
    //  of this class can represent.
    enum SystemLevelId
    {
      // these must be unique from any Urgency ID...
      CORRECTED_ENTRY		= (Uint8)0xf0,
      CLOCK_UPDATE_PENDING	= (Uint8)0xf1,
      CLOCK_UPDATE_DONE		= (Uint8)0xf2
    };

    AlarmAction(const AlarmAction& alarmAction);
    AlarmAction(void);
    ~AlarmAction(void);

    void  operator=(const AlarmAction& alarmAction);

    //  Anonymous union of the first two fields in this class.
    union
    {
      //@ Data-Member:    systemLevelId
      // This is used for the (possible) system level ids needed for an
      // entry.
      Uint8  systemLevelId;

      //@ Data-Member:    currentUrgency
      // The urgency associated with the entry.
      Uint8  currentUrgency;
    };

    //@ Data-Member:    baseMessage
    // The text identifying the alarm or alarm event associated with the
    // entry.
    Uint8  baseMessage;

    //@ Data-Member:    analysisIndex
    // Indicates how many message enumerators are contained in
    // 'arrCurrentAnalysisMessage'.
    Uint8  analysisIndex;

    //@ Data-Member:    updateType
    // Contains an enumerator indicating the reason for the entry.
    Uint8  updateType;   

    //@ Data-Member:    whenOccurred
    // Contains the time the event causing the entry occurred.
    TimeStamp  whenOccurred;

    //@ Data-Member:    arrCurrentAnalysisMessages
    // The analysis messages associated with the entry.
    FixedArray(MessageName,NUM_MESSAGE) arrCurrentAnalysisMessages;
};


//====================================================================
//
//  External Functions...
//
//====================================================================

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
extern Ostream&  operator<<(Ostream& ostr, const AlarmAction& alarmAction);
#endif // defined(SIGMA_DEVELOPMENT)


#endif // AlarmAction_HH 
