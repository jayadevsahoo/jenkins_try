/*****************************************************************************/
/*   Module Name:    Packet.c                                            
/*   Purpose:      840 Ventilator Final Test                              
/*                                                                          
/*   Requirements:   This module implements requirements detailed in   
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Changed the "AlarmLog" to "AlrmLog"    RHJ               29 Feb,2008    
/*   in the header of the Alarm log file.                                                          
/*
/*   Added Alarm Log functions.             RHJ               27 Nov,2007   
/*
/*	 version.h is included to run  
/* 	 the software in demo mode.				Hamsavally(CGS)   27 Sep,2000
/*   functionality. 
/*
/*   Changed function KillLog to            Dave Richardson   27 Nov, 2002
/*   VentDoFunction and expanded function
/*   to include erasing BDU or GUI
/*
/*   $Revision::   1.2      $                                                
/*
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/sio/840Cmds.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:35:24   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   27 Nov 2002 08:15:22   Richardson
 * Updated per SCR # 26

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.

Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <userint.h> // ProcessSystemEvents()
#include <utility.h> // Delay()

#include "72361.h"
#include "840.h"
#include "840Msgs.h"
#include "common.h"
#include "cpvtmain.h" // gStoppedInd
#include "ErrCodes.h" // SUCCESS, FAILURE, etc.
#include "version.h"

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

VENTDATA Vent;
float DataStorage[MAX_RETURN_VALUE];      /* data point return from vent*/
char DsMirror[MAX_RETURN_VALUE];          /* mirror marker DataStorage */
int VentInfoActive;                       /* transfer status of info logs */
int UploadActive;                         /* status from vent to pc */
int UploadStatus;                         /* status from vent to pc */
FILE *UploadFile;                         /* file for vent log storage */

/***********************************************/
/*************** LOCAL VARIABLES ***************/
/***********************************************/

static int HighDataReturn = 0;            /* highest element data element from vent*/

/***********************************************/
/************ STRUCTURE DEFINITIONS ************/
/***********************************************/

union testResults {
	unsigned char tst[8];
	float serv;
} Test1;

union serialResults {
	unsigned char pt[4];
	float fValue;
} SerialResult;

char guiNoAlarmLogTable[MAX_NO_ALARMLOG_VENTS][VENT_VERSION_NUMBER_MAX] = 
{
    // US
    "4-070212-85-A",
    "4-070212-85-B",
    "4-070212-85-C",
    "4-070212-85-D",
    "4-070212-85-E",
    "4-070212-85-F",
    "4-070212-85-G",
    "4-070212-85-H",
    "4-070212-85-J",
    "4-070212-85-K",
    "4-070212-85-L",
    "4-070212-85-M",
    "4-070212-85-N",
    "4-070212-85-P",
    "4-070212-85-R",
    "4-070212-85-T",
    "4-070212-85-U",

    // NON-US
    "4-070000-85-A",
    "4-070000-85-B",
    "4-070000-85-C",
    "4-070000-85-D",
    "4-070000-85-E",
    "4-070000-85-F",
    "4-070000-85-G",
    "4-070000-85-H",
    "4-070000-85-J",
    "4-070000-85-K",
    "4-070000-85-L",
    "4-070000-85-M",
    "4-070000-85-N",
    "4-070000-85-P",
    "4-070000-85-R",
    "4-070000-85-T",
    "4-070000-85-U",
    "4-070000-85-V",
    "4-070000-85-W",
    "4-070000-85-AA",

    // Italian
    "4-070204-85-A",
    "4-070204-85-B",
    "4-070204-85-C",
    "4-070204-85-D",
    "4-070204-85-E",
    "4-070204-85-F",
    "4-070204-85-G",
    "4-070204-85-H",
    "4-070204-85-J",
    "4-070204-85-K",
    "4-070204-85-L",
    "4-070204-85-M",
    "4-070204-85-N",
    "4-070204-85-P",
    "4-070204-85-R",
    "4-070204-85-T",
    "4-070204-85-U",
    "4-070204-85-V",
    "4-070204-85-W",
    "4-070204-85-AA",

    // German
    "4-070203-85-A",
    "4-070203-85-B",
    "4-070203-85-C",
    "4-070203-85-D",
    "4-070203-85-E",
    "4-070203-85-F",
    "4-070203-85-G",
    "4-070203-85-H",
    "4-070203-85-J",
    "4-070203-85-K",
    "4-070203-85-L",
    "4-070203-85-M",
    "4-070203-85-N",
    "4-070203-85-P",
    "4-070203-85-R",
    "4-070203-85-T",
    "4-070203-85-U",
    "4-070203-85-V",
    "4-070203-85-W",
    "4-070203-85-AA",

    // French
    "4-070202-85-A",
    "4-070202-85-B",
    "4-070202-85-C",
    "4-070202-85-D",
    "4-070202-85-E",
    "4-070202-85-F",
    "4-070202-85-G",
    "4-070202-85-H",
    "4-070202-85-J",
    "4-070202-85-K",
    "4-070202-85-L",
    "4-070202-85-M",
    "4-070202-85-N",
    "4-070202-85-P",
    "4-070202-85-R",
    "4-070202-85-T",
    "4-070202-85-U",
    "4-070202-85-V",
    "4-070202-85-W",
    "4-070202-85-AA",

    // Polish
    "4-070208-85-A",
    "4-070208-85-B",
    "4-070208-85-C",
    "4-070208-85-D",
    "4-070208-85-E",
    "4-070208-85-F",
    "4-070208-85-G",
    "4-070208-85-H",

    // Russian
    "4-070209-85-A",
    "4-070209-85-B",
    "4-070209-85-C",
    "4-070209-85-D",
    "4-070209-85-E",
    "4-070209-85-F",
    "4-070209-85-G",

    // Spanish
    "4-070201-85-A",
    "4-070201-85-B",
    "4-070201-85-C",
    "4-070201-85-D",
    "4-070201-85-E",
    "4-070201-85-F",
    "4-070201-85-G",
    "4-070201-85-H",
    "4-070201-85-J",
    "4-070201-85-K",
    "4-070201-85-L",
    "4-070201-85-M",
    "4-070201-85-N",
    "4-070201-85-P",
    "4-070201-85-R",
    "4-070201-85-T",
    "4-070201-85-U",
    "4-070201-85-V",

    // Portuguese
    "4-070205-85-A",
    "4-070205-85-B",
    "4-070205-85-C",
    "4-070205-85-D",
    "4-070205-85-E",
    "4-070205-85-F",
    "4-070205-85-G",
    "4-070205-85-H",
    "4-070205-85-J",
    "4-070205-85-K",
    "4-070205-85-L",
    "4-070205-85-M",
    "4-070205-85-N",
    "4-070205-85-P",
    "4-070205-85-R",
    "4-070205-85-T",
    "4-070205-85-U",

    // Japanese
    "4-070206-85"
};

const int JAPANESE_INDEX_NUM = 147;

char langNum[4] = "";


/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

void StatusProcessing(   Packet inPacket);
void VentInformation(    Packet inPacket);
void ConditionProcessing(Packet inPacket);
void DataProcessing(     Packet inPacket);
void PromptProcessing(   Packet inPacket);
void (*DataProcessor)(int,float);         /* who handle data return from vent*/
void ClearDS(void) ;                      /* clear DataStorage */


/*****************************************************************************/
/* Name:    Decode                                                       
/*                                                                           
/* Purpose:  to decode incoming Packet from 840 Com Controller  
/*                                          
/*                                                                           
/* Example Call:   Decode(Packet)                                     
/*                                                                           
/* Input:    Packet         
/*                                                                           
/* Output:   <int>                                                          
/*                                                                           
/* Globals Changed:   None                                                          
/*                                                                           
/* Globals Used:  None                                                               
/*                                                  
/*                                                                           
/* Return:   None                                                         
/*                                                                           
/*****************************************************************************/
static Bool Decode(Packet inPacket)
{
	int x;

	DecodeFlag = OK_840;
	Delay(0.1);  /* for field service */

	switch(inPacket[MESSAGE_ID]){  // ENTER message handling here !!

		// the auto detection code will handle these, so should never happen here
		case LAPTOP_CONNECTED_COMMAND:      
		case COMMUNICATION_GRANTED_COMMAND: 
		case INVALID_MSG_COMMAND:           
			DecodeFlag = FAIL_840;
			break;

		case STATUS_COMMAND:                
			StatusProcessing(inPacket);
			break;

		case MESSAGE_ACK_COMMAND:           
			VentAckCommand = 1;
			break;

		case START_UPLOAD_COMMAND:          
		case DATA_UPLOAD_COMMAND:           
		case END_UPLOAD_COMMAND:            
			UploadProcessing(inPacket);
			break;

		case SOFTWARE_VERSION_COMMAND:      
		case UNIT_SERIAL_NUMBER_COMMAND:    
		case INVENTORY_INFO_COMMAND:        
		case UNIT_RUNTIME_INFO_COMMAND:     
			VentInformation(inPacket);
			break;

		case PROMPT_COMMAND:                
		case KEY_ALLOWED_COMMAND:           
			PromptProcessing(inPacket);
			break;

		case DATA_COMMAND:                  
			DataProcessing(inPacket);
			break;

		case CONDITION_COMMAND:             
			ConditionProcessing(inPacket);
			break;

		default:                            
			DecodeFlag = FAIL_840;
			break;
	} // end switch(inPacket[MESSAGE_ID])

	if(DecodeFlag == FAIL_840){
		return(FAIL_840);
	}
	else{
		return(OK_840);
	}
}


/*****************************************************************************/
/* Name:      statusProcessing                                                        
/*                                                                           
/* Purpose:  to process status command id's 
/*                                          
/*                                                                           
/* Example Call:   StatusProcessing(Packet)                                     
/*                                                                           
/* Input:    Packet          
/*                                                                           
/* Output:   None                                                          
/*                                                                           
/* Globals Changed:    None                                                                                                                
/*                                                                           
/* Globals Used:  None                                                            
/*                                                  
/*                                                                           
/* Return:   None                                                         
/*                                                                           
/*****************************************************************************/
void StatusProcessing(Packet StatusPacket)
{
	PromptReturn = NO_PROMPT_ALLOWED;

	// enter test hook for status here
	switch(StatusPacket[MESSAGE_TYPE]){

		case TEST_END:
			//end of current test
			TestActive = NO_840;
			break;

		case TEST_START:
			//start of current test
			KeyAllowedReturn = NO_KEYS_ALLOWED;
			TestFail = NO_840;
			TestActive = YES_840;
			ConditionCode = NO_840;
			PartInstalled = YES_840;
			ClearDS();
			break;

		case TEST_ALERT:
			//test alert
			TestFail = YES_840;
			break;

		case TEST_FAILURE:
			//test failure
			TestFail = YES_840;
			break;

		case TEST_OPERATOR_EXIT:
			//operator ends test
			break;

		case NOT_INSTALLED:
			PartInstalled = NO_840;
			break;

		default:
			DecodeFlag = FAIL_840;
			break;
	} // end switch(StatusPacket[MESSAGE_TYPE])

	return;
}


/*****************************************************************************/
/*Name:     UploadProcessing                                                 */
/*                                                                           
/* Purpose:  to decode upload packets  
/*                                          
/*                                                                           
/* Example Call:   UploadProcessing(Packet)                                      
/*                                                                           
/* Input:    Packet         
/*                                                                           
/* Output:   None                                                          
/*                                                                           
/* Globals Changed:   None                                                          
/*                                                                           
/* Globals Used:   None                                                         
/*                                                  
/*                                                                           
/* Return:   None                                                         
/*                                                                           
/*****************************************************************************/
void UploadProcessing(Packet inPacket)
{
	switch(inPacket[MESSAGE_ID]){

		case START_UPLOAD_COMMAND:
			if(UploadActive != YES_840){
				UploadStatus = FAIL_840;
			}
			else{
				UploadActive = YES_840;
				UploadStatus = OK_840;
			}
			break;

		case DATA_UPLOAD_COMMAND:
			if(UploadActive == YES_840){
				fprintf(UploadFile, "%s\n", (char *)&inPacket[MESSAGE_TYPE]);
			}
			break;

		case END_UPLOAD_COMMAND:
			UploadActive = NO_840;
			break;

		default:
			DecodeFlag = FAIL_840;
			break;
	} // end switch(inPacket[MESSAGE_ID])

	return;
}


/*****************************************************************************/
/* Name:     DataProcessing                                                           
/*                                                                           
/* Purpose:  to decode DATA_COMMANDS 
/*                                          
/*                                                                           
/* Example Call:   DataProcessing( Packet)                                        
/*                                                                           
/* Input:    Packet              
/*                                                                           
/* Output:   None                                                          
/*                                                                           
/* Globals Changed:   None                                                          
/*                                                                           
/* Globals Used:  None                                                           
/*                                                  
/*                                                                           
/* Return:   None                                                         
/*                                                                           
/*****************************************************************************/
void DataProcessing(Packet inPacket)
{
	DataAvailable = YES_840;                    /* big Endian Little Endian   */
	Test1.tst[0] = inPacket[11];                /* Vent = 68040, Computer x86 */
	Test1.tst[1] = inPacket[10];
	Test1.tst[2] = inPacket[ 9];
	Test1.tst[3] = inPacket[ 8];

	DataStorage[inPacket[7]] = Test1.serv;
	DsMirror[   inPacket[7]] = 1;
	if(inPacket[7] > HighDataReturn){
		HighDataReturn = inPacket[7];
	}

	return;
}


/*****************************************************************************/
/* Name:     GetDataStorage                                                           
/*                                                                           
/* Purpose:  to return vent data elements    
/*                                          
/*                                                                           
/* Example Call:   GetDataStorage( float *)                                    
/*                                                                           
/* Input:    Packet         
/*                                                                           
/* Output:   None                                                          
/*                                                                           
/* Globals Changed:      None                                                          
/*                                                                           
/* Globals Used:   None                                                         
/*                                                  
/*                                                                           
/* Return:   None                                                         
/*                                                                           
/*****************************************************************************/
int GetDataStorage(float *out)
{
	int x,
	lcount = 0;

	for(x = 0; x < MAX_RETURN_VALUE; x++){
		if(DsMirror[x] == 1){
			out[x] = DataStorage[x];
			lcount++;
		}
	}

	DataAvailable = NO_840;
	HighDataReturn = 0;

	return lcount;
}


/*****************************************************************************/
/* Name:    ConditionProcessing                                                         
/*                                                                           
/* Purpose:  set ConditionCode Flag     
/*                                          
/*                                                                           
/* Example Call:   DataProcessing( Packet)                                     
/*                                                                           
/* Input:    Packet        
/*                                                                           
/* Output:   None                                                          
/*                                                                           
/* Globals Changed:     ConditionCode                                                        
/*                                                                           
/* Globals Used: None                                                          
/*                                                  
/*                                                                           
/* Return:   None                                                        
/*                                                                           
/*****************************************************************************/
void ConditionProcessing(Packet inPacket)
{
	ConditionCode = YES_840;

	return;
}


/*****************************************************************************/
/* Name:     PromptProcessing                                                        
/*                                                                           
/* Purpose:  to decode prompt commands  
/*                                          
/*                                                                           
/* Example Call:   PromptProcessing( Packet)                                   
/*                                                                           
/* Input:   Packet      
/*                                                                           
/* Output:   None                                                          
/*                                                                           
/* Globals Changed:   KeyAllowedReturn, PromptReturn, DecodeFlag                                                          
/*                                                                           
/* Globals Used:  None                                                         
/*                                                  
/*                                                                           
/* Return:   None                                                         
/*                                                                           
/*****************************************************************************/
void PromptProcessing(Packet inPacket)
{
	switch(inPacket[MESSAGE_ID]){

		case KEY_ALLOWED_COMMAND:
			KeyAllowedReturn = inPacket[MESSAGE_TYPE];
			break;

		case PROMPT_COMMAND:
			PromptReturn = inPacket[MESSAGE_TYPE];
			break;

		default:
			DecodeFlag = FAIL_840;
			break;
	} // end switch(inPacket[MESSAGE_ID])

	return;
}


/*****************************************************************************/
/* Name:     AddVerStr                                                      
/*                                                                           
/* Purpose:  Adds the string ' Ver' before a : in a string.
/*                                          
/*                                                                           
/* Example Call:   AddVerStr(Vent.Bd.version)                                 
/*                                                                           
/* Input:    Char* String            
/*                                                                           
/* Output:   String with ' Ver' inserted                                                         
/*                                                                           
/* Globals Changed:   None                                                          
/*                                                                           
/* Globals Used:   None                                                          
/*                                                  
/*                                                                           
/* Return:   None                                                        
/*                                                                           
/*****************************************************************************/
void AddVerStr(char* String)
{
	int length;
	int firstcln = 0; // first  colon
	int i;

	// Find the first colon in the string
	length = strlen(String);
	while(String[firstcln] != ':' && firstcln <= length){
		firstcln++;
	}

	// If we didn't find a colon, leave.
	if(firstcln >= length){
		return;
	}

	// CAUTION CAUTION CAUTION
	// Caller MUST assure that if the string contains a colon ':'
	// there MUST be at least 4 unused character storage locations 
	// to the right of the end of the string.

	// Scoot everything to the right of the colon over by 4	
	for(i = length; i >= firstcln; i--){
		String[i+4] = String[i];
	}

	// Add the 'ver' string
	String[firstcln]   = ' ';	
	String[firstcln+1] = 'V';	
	String[firstcln+2] = 'e';	
	String[firstcln+3] = 'r';	

	return;
}


/*****************************************************************************/
/* Name:     VentProcessing                                                      
/*                                                                           
/* Purpose:  to decode vent info packets 
/*                                          
/*                                                                           
/* Example Call:   VentInformation (inPacket)                                 
/*                                                                           
/* Input:    Packet            
/*                                                                           
/* Output:   None                                                         
/*                                                                           
/* Globals Changed:   None                                                          
/*                                                                           
/* Globals Used:   None                                                          
/*                                                  
/*                                                                           
/* Return:   None                                                        
/*                                                                           
/*****************************************************************************/
void VentInformation(Packet inPacket)
{
	switch(inPacket[MESSAGE_ID]){

		case UNIT_SERIAL_NUMBER_COMMAND:
			strcpy(Vent.Gui.post, strtok((char *)&inPacket[MESSAGE_TYPE], ","));
			strcpy(Vent.Bd.post,           strtok(NULL, ","));
			strcpy(Vent.saas,              strtok(NULL, ","));
			strcpy(Vent.Gui.serial,        strtok(NULL, ","));
			strcpy(Vent.Bd.serial,         strtok(NULL, ","));
			strcpy(Vent.Compressor.serial, strtok(NULL, ","));
			AddVerStr(Vent.Gui.post);
			AddVerStr(Vent.Bd.post);
			AddVerStr(Vent.saas);
			break;

		case UNIT_RUNTIME_INFO_COMMAND:
			SerialResult.pt[0] = inPacket[7];
			SerialResult.pt[1] = inPacket[6];
			SerialResult.pt[2] = inPacket[5];
			SerialResult.pt[3] = inPacket[4];
			Vent.Compressor.runTimeHours = SerialResult.fValue;

			SerialResult.pt[0] = inPacket[11];
			SerialResult.pt[1] = inPacket[10];
			SerialResult.pt[2] = inPacket[ 9];
			SerialResult.pt[3] = inPacket[ 8];
			Vent.runTimeHours = SerialResult.fValue;
			break;
    
		case SOFTWARE_VERSION_COMMAND:
			strcpy(Vent.Gui.version, strtok( (char *)&inPacket[MESSAGE_TYPE], ","));
			strcpy(Vent.Bd.version, strtok( NULL , ","));
			AddVerStr(Vent.Gui.version);
			AddVerStr(Vent.Bd.version);
			break;

		default:
			DecodeFlag = FAIL_840;
			break;
	} // end switch(inPacket[MESSAGE_ID])

	VentInfoActive = NO_840;

	return;
}


/*****************************************************************************/
/* Name:     ProcessIncomingPacket(void)                                                       
/*                                                                           
/* Purpose:  retrieve Packet from vent     
/*                                          
/*                                                                           
/* Example Call:   ProcessIncomingPacket()                                   
/*                                                                           
/* Input:   none         
/*                                                                           
/* Output:   None                                                          
/*                                                                           
/* Globals Changed:    CommActive, comStatus                                                         
/*                                                                           
/* Globals Used:   none                                                          
/*                                                  
/*                                                                           
/* Return:   None                                                        
/*                                                                           
/*****************************************************************************/
int ProcessIncomingPacket(void)
{
	int  comStatus;
	char msg[MAX_COM_STR];
	int  actual;

	CommActive = COM_840_ACTIVE;

	comStatus = Recv840(msg, MAX_COM_STR, &actual, COM_TIMEOUT);
	if(comStatus == SUCCESS){
		Decode(msg);
	}

	CommActive = COM_840_INACTIVE;

	return comStatus;
}


/*****************************************************************************/
/* Name:      Prompt_Accept                                                       
/*                                                                           
/* Purpose:  accept or deny prompt   
/*                                          
/*                                                                           
/* Example Call:   Prompt_Accept( Yes )                                      
/*                                                                           
/* Input:    int        
/*                                                                           
/* Output:   None                                                          
/*                                                                           
/* Globals Changed:  None                                                          
/*                                                                           
/* Globals Used:  none                                                          
/*                                                  
/*                                                                           
/* Return:   error                                                         
/*                                                                           
/*****************************************************************************/
int Prompt_Accept(int ans)
{
	int err;
	Packet outBuff;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = TEST_EVENT;
	outBuff[4] = 0x01;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	if(ans == YES_840){
		outBuff[5] = KEY_ACCEPT;
	}
	else{
		outBuff[5] = KEY_CLEAR;
	}

	err = Send840(outBuff);

	return err;
}


/*****************************************************************************/
/* Name:      VentDoFunction                                                      
/*                                                                           
/* Purpose:  send command to vent to clear logs   
/*                                          
/*                                                                           
/* Example Call:   VentDoFunction( type)                                 
/*                                                                           
/* Input:    int             
/*                                                                           
/* Output:   None                                                          
/*                                                                           
/* Globals Changed:   None                                                          
/*                                                                           
/* Globals Used:   none                                                          
/*                                                  
/*                                                                           
/* Return:   error                                                         
/*                                                                           
/*****************************************************************************/
int VentDoFunction(int type)
{
	int  statusFlag = OK_840;
	BOOL doneFlag   = FALSE;
	double timeOut;
	double dMaxWait;
	int err;
	Packet outBuff;

	#ifdef DEMOMODE
	doneFlag = TRUE;
	#else
	ClearPacket(outBuff);

	switch(type){
		case BD_DOWNLOAD_ID:
			outBuff[MESSAGE_TYPE] = BD_DOWNLOAD_ID;
			dMaxWait = CLEAR_IMAGE_TIMEOUT;
			break;

		case GUI_DOWNLOAD_ID:
			outBuff[MESSAGE_TYPE] = GUI_DOWNLOAD_ID;
			dMaxWait = CLEAR_IMAGE_TIMEOUT;
			break;

		case DELETE_SYSTEM_DIAGNOSTIC_LOG_ID:
			outBuff[MESSAGE_TYPE] = DELETE_SYSTEM_DIAGNOSTIC_LOG_ID;
			dMaxWait = VENT_DATA_TIMEOUT;
			break;

		case DELETE_SYSTEM_INFORMATION_LOG_ID:
			outBuff[MESSAGE_TYPE] = DELETE_SYSTEM_INFORMATION_LOG_ID;
			dMaxWait = VENT_DATA_TIMEOUT;
			break;

		case DELETE_SELFTEST_LOG_ID:
			outBuff[MESSAGE_TYPE] = DELETE_SELFTEST_LOG_ID;
			dMaxWait = VENT_DATA_TIMEOUT;
			break;

		default:
			statusFlag = FAIL_840;
			break;
	} // end switch(type)

	if(statusFlag == OK_840){

		outBuff[0] = 0x00;
		outBuff[1] = 0x06;
		outBuff[3] = FUNCTION_EVENT;
		outBuff[5] = 0x00;
		outBuff[6] = 0x00;
		outBuff[7] = 0x00;

		err = Send840(outBuff);
		if(err){
			return FAILURE;
		}

		VentAckCommand = NO_840;
		doneFlag = FALSE;
		timeOut = Timer() + dMaxWait;
		while(!doneFlag && (Timer() < timeOut)){
			if(CharsIn840Q() && CommActive == COM_840_INACTIVE){
				err = ProcessIncomingPacket();
				if(err != SUCCESS){
					return FAILURE;
				}
			}

			if(VentAckCommand == YES_840){
				doneFlag = TRUE;
			}
		} // end while(!doneFlag && (Timer() < timeOut))
	}
	#endif // #ifdef DEMOMODE

	if(doneFlag){
		return SUCCESS;
	}
	else{
		return FAILURE;
	}
}


/*****************************************************************************/
/* Name:      GetVentLog                                                      
/*                                                                           
/* Purpose:  send command to vent to retreive the vent log 
/*                                          
/*                                                                           
/* Example Call:   GetVentLog();                                   
/*                                                                           
/* Input:    none       
/*                                                                           
/* Output:   int                                                            
/*                                                                           
/* Globals Changed:    none                                                   
/*                                                                           
/* Globals Used:    none                                                         
/*                                                  
/*                                                                           
/* Return:   int                                                          
/*                                                                           
/*****************************************************************************/
int GetVentLog(void)
{
	Packet outBuff;
	int statusFlag;

	statusFlag = OK_840;
	UploadStatus = OK_840 ;

	if((UploadFile = fopen(SYS_LOG_FILE, "a+")) == NULL ){
		return FAIL_840; // error condition
	}

	fprintf(UploadFile, "SysLog Pair of  %s &  %s\n", Vent.Bd.serial, Vent.Gui.serial);

	if(GetSystemLog() != SUCCESS){
		return FAIL_840; // error condition
	}

	fprintf(UploadFile,"End\n");
	fclose(UploadFile);

	if(UploadStatus == FAIL_840){
		statusFlag = FAIL_840;
	}

	UploadStatus = OK_840 ;

	if((UploadFile = fopen(INFO_LOG_FILE, "a+")) == NULL ){
		return FAIL_840; // error condition
	}

	fprintf(UploadFile, "InfoLog Pair of %s &  %s\n", Vent.Bd.serial, Vent.Gui.serial);

	if(GetInfoLog() != SUCCESS){
		statusFlag = FAIL_840;
	}

	fprintf(UploadFile,"End\n");
	fclose(UploadFile);

	if(UploadStatus == FAIL_840){
		statusFlag = FAIL_840;
	}

	UploadStatus = OK_840 ;

	if((UploadFile = fopen(ST_LOG_FILE, "a+")) == NULL ){
		return FAIL_840; // error condition
	}

	fprintf(UploadFile,"StLog Pair of  %s &  %s\n", Vent.Bd.serial, Vent.Gui.serial);

	if(GetStLog() != SUCCESS){
		statusFlag = FAIL_840;
	}

	fprintf(UploadFile,"End\n");
	fclose(UploadFile);

	if(UploadStatus == FAIL_840){
		statusFlag = FAIL_840;
	}

	UploadStatus = OK_840 ;


    // Checks if the Alarm Logs can be downloaded from the vent.
    if(IsAlarmLogDownloadable())
    {


    	if((UploadFile = fopen(ALARM_LOG_FILE, "a+")) == NULL ){
    		return FAIL_840; // error condition
    	}
    
    	fprintf(UploadFile,"AlrmLog Pair of  %s &  %s\n", Vent.Bd.serial, Vent.Gui.serial);
    
    	if(GetAlarmLog() != SUCCESS){
    		statusFlag = FAIL_840;
    	}
    
    	fprintf(UploadFile,"End\n");
    	fclose(UploadFile);
    
    	if(UploadStatus == FAIL_840){
    		statusFlag = FAIL_840;
    	}

    }


	return statusFlag;
}



/*****************************************************************************/
/* Name:      IsAlarmLogDownloadable()                                                   
/*                                                                           
/* Purpose:  Check if the vent matches the old rev of software.  If 
/*           they match then downloading Alarm Logs is not possible.  
/*           Otherwise Alarm Logs is possible.
/*                                                                           
/* Example Call:   IsAlarmLogDownloadable( )                                   
/*
/* Input:    none         
/*                                                                           
/* Output:   none                                                        
/*                                                                           
/* Globals Changed:   none                                                       
/*                                                                           
/* Globals Used:     none                                                         
/*                                                  
/*                                                                           
/* Return:   return true if downloading Alarm Logs is possible, else return false.                                                         
/*                                                                           
/*****************************************************************************/
Bool IsAlarmLogDownloadable(void)
{
     int index = 0;
     Bool flag = TRUE;
     char * ventNum = NULL;

     if(Vent.Gui.version == NULL)
     {
        flag = FALSE;
     }
     else
     {
         ventNum = strchr(Vent.Gui.version, ':') + 1;

         if(strncmp(ventNum, guiNoAlarmLogTable[JAPANESE_INDEX_NUM], 10) == 0)
         {
             flag = FALSE;
         }
         else
         {
         
             for(; index < MAX_NO_ALARMLOG_VENTS && guiNoAlarmLogTable[index] != NULL ; index++)
             {
                 
                 if(strcmp(ventNum, guiNoAlarmLogTable[index]) == 0)
                 {
        
                     flag = FALSE;
                     break;
                 }
             }
    
         }
     }
     return flag;
}

/*****************************************************************************/
/* Name:      GetAlarmLog                                                    
/*                                                                           
/* Purpose:  Send command to vent to retreive the Alarm logs
/*                                          
/*                                                                           
/* Example Call:   GetAlarmLog( )                                   
/*                                                                           
/* Input:    none         
/*                                                                           
/* Output:   none                                                        
/*                                                                           
/* Globals Changed:   none                                                       
/*                                                                           
/* Globals Used:     none                                                         
/*                                                  
/*                                                                           
/* Return:   none                                                          
/*                                                                           
/*****************************************************************************/
int GetAlarmLog(void)
{
	Packet outBuff;
	BOOL doneFlag;
	double timeOut;
	int err;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = UPLOAD_ALARM_LOG;
	outBuff[4] = 0x00;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	err = Send840(outBuff);
	if(err){
		return FAILURE;
	}

	UploadActive = YES_840;
	doneFlag = FALSE;
	timeOut = Timer() + VENT_DATA_TIMEOUT;
	while(!doneFlag && (Timer() < timeOut)){
		if(CharsIn840Q() && CommActive == COM_840_INACTIVE){
			err = ProcessIncomingPacket();
			if(err != SUCCESS){
				return FAILURE;
			}
		}

		if(UploadActive == NO_840){
			doneFlag = TRUE;
		}
	}

	return SUCCESS;
}

/*****************************************************************************/
/* Name:      GetStLog                                                    
/*                                                                           
/* Purpose:  send command to vent to retreive St log 
/*                                          
/*                                                                           
/* Example Call:   GetStLog( )                                   
/*                                                                           
/* Input:    none         
/*                                                                           
/* Output:   none                                                        
/*                                                                           
/* Globals Changed:   none                                                       
/*                                                                           
/* Globals Used:     none                                                         
/*                                                  
/*                                                                           
/* Return:   none                                                          
/*                                                                           
/*****************************************************************************/
int GetStLog(void)
{
	Packet outBuff;
	BOOL doneFlag;
	double timeOut;
	int err;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = UPLOAD_EST_SST_LOG;
	outBuff[4] = 0x00;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	err = Send840(outBuff);
	if(err){
		return FAILURE;
	}

	UploadActive = YES_840;
	doneFlag = FALSE;
	timeOut = Timer() + VENT_DATA_TIMEOUT;
	while(!doneFlag && (Timer() < timeOut)){
		if(CharsIn840Q() && CommActive == COM_840_INACTIVE){
			err = ProcessIncomingPacket();
			if(err != SUCCESS){
				return FAILURE;
			}
		}

		if(UploadActive == NO_840){
			doneFlag = TRUE;
		}
	}

	return SUCCESS;
}


/*****************************************************************************/
/* Name:     GetInfoLog                                                       
/*                                                                           
/* Purpose:  send command to vent to retreive Info log
/*                                          
/*                                                                           
/* Example Call:   GetInfoLog( )                                     
/*                                                                           
/* Input:    none         
/*                                                                           
/* Output:   none                                                       
/*                                                                           
/* Globals Changed:    none                                                           
/*                                                                           
/* Globals Used:   none                                                          
/*                                                  
/*                                                                           
/* Return:   none                                                          
/*                                                                           
/*****************************************************************************/
int GetInfoLog(void)
{
	Packet outBuff;
	BOOL doneFlag;
	double timeOut;
	int err;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = UPLOAD_SYS_INFO_LOG;
	outBuff[4] = 0x00;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	err = Send840(outBuff);
	if(err){
		return FAILURE;
	}

	UploadActive = YES_840;
	doneFlag = FALSE;
	timeOut = Timer() + VENT_DATA_TIMEOUT;
	while(!doneFlag && (Timer() < timeOut)){
		if(CharsIn840Q() && CommActive == COM_840_INACTIVE){
			err = ProcessIncomingPacket();
			if(err != SUCCESS){
				return FAILURE;
			}
		}

		if(UploadActive == NO_840){
			doneFlag = TRUE;
		}
	}

	return SUCCESS;
}


/*****************************************************************************/
/* Name:     GetSystemLog                                                         
/*                                                                           
/* Purpose:  send command to vent to retreive system log
/*                                          
/*                                                                           
/* Example Call:   GetSystemLog( )                                     
/*                                                                           
/* Input:    none            
/*                                                                           
/* Output:   none                                                             
/*                                                                           
/* Globals Changed:  none                                                             
/*                                                                           
/* Globals Used:   none                                                            
/*                                                  
/*                                                                           
/* Return:   none                                                            
/*                                                                           
/*****************************************************************************/
int GetSystemLog(void)
{
	Packet outBuff;
	BOOL doneFlag;
	double timeOut;
	int err;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = UPLOAD_SYSTEM_LOG;
	outBuff[4] = 0x00;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	err = Send840(outBuff);
	if(err){
		return FAILURE;
	}

	UploadActive = YES_840;
	doneFlag = FALSE;
	timeOut = Timer() + VENT_DATA_TIMEOUT;
	while(!doneFlag && (Timer() < timeOut)){
		if(CharsIn840Q() && CommActive == COM_840_INACTIVE){
			err = ProcessIncomingPacket();
			if(err != SUCCESS){
				return FAILURE;
			}
		}

		if(UploadActive == NO_840){
			doneFlag = TRUE;
		}
	}

	return SUCCESS;
}


/*****************************************************************************/
/* Name:     GetVentInfo                                                      
/*                                                                             
/* Purpose:  send command to vent to retreive various ventilator information 
/*                                          
/*                                                                           
/* Example Call:   GetVentInfo();                                    
/*                                                                           
/* Input:    none        
/*                                                                           
/* Output:   none                                                           
/*                                                                           
/* Globals Changed:   none                                                          
/*                                                                           
/* Globals Used:   none                                                           
/*                                                  
/*                                                                           
/* Return:   status                                                          
/*                                                                           
/*****************************************************************************/
int GetVentInfo(void)
{
	BOOL err,
	status = OK_840;

	// Get Vent serial numbers:
	err = GetVentDataSerial();
	if(err){
		status = FAIL_840;
	}

	FlushInQ840();
	FlushOutQ840();

	// Get Run time information
	err = GetVentDataVersion();
	if(err){
		status = FAIL_840;
	}

	FlushInQ840();
	FlushOutQ840();

	// Get Version numbers:
	err = GetVentDataRunTime();
	if(err){
		status = FAIL_840;
	}

	return status;
}


/*****************************************************************************/
/* Name:    GetVentDataRunTime                                                     
/*                                                                           
/* Purpose:  send command to vent to runtime info 
/*                                          
/*                                                                           
/* Example Call:   GetVentDataRunTime()                                       
/*                                                                           
/* Input:    none          
/*                                                                           
/* Output:   int                                                          
/*                                                                           
/* Globals Changed:    none                                                           
/*                                                                           
/* Globals Used:   none                                                            
/*                                                  
/*                                                                           
/* Return:   status                                                          
/*                                                                           
/*****************************************************************************/
static int GetVentDataRunTime(void)
{
	Packet outBuff;
	BOOL doneFlag;
	double timeOut;
	int err;

	#ifdef DEMOMODE
	Vent.runTimeHours = 99999.0;
	Vent.Compressor.runTimeHours = 99999.0;
	#else	

	ClearPacket(outBuff);
	TestActive = YES_840;

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = UNIT_RUNTIME_INFO;
	outBuff[4] = 0x00;
	outBuff[5] = 0x00;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	VentInfoActive = YES_840;

	err = Send840(outBuff);
	if(err){
		return FAILURE;
	}

	doneFlag = FALSE;
	timeOut = Timer() + VENT_DATA_TIMEOUT;
	while(!doneFlag && (Timer() < timeOut)){
		if(CharsIn840Q() && CommActive == COM_840_INACTIVE){
			err = ProcessIncomingPacket();
			if(err != SUCCESS){
				return FAILURE;
			}
		}

		if(VentInfoActive == NO_840){
			doneFlag = TRUE;
		}
	}
	#endif // #ifdef DEMOMODE

	if(!doneFlag){
		return FAILURE;
	}
	else{
		return SUCCESS;
	}
}


/*****************************************************************************/
/* Name:     GetVentDataVersion                                                       
/*                                                                           
/* Purpose:  send command to vent to version number info 
/*                                          
/*                                                                           
/* Example Call:   GetVentDataVersion()                                     
/*                                                                           
/* Input:    none             
/*                                                                           
/* Output:   int                                                           
/*                                                                           
/* Globals Changed:   VentInfoActive, TestActive                                                            
/*                                                                           
/* Globals Used:   none                                                             
/*                                                  
/*                                                                           
/* Return:   status                                                          
/*                                                                           
/*****************************************************************************/
static int GetVentDataVersion(void)
{
	Packet outBuff;
	BOOL doneFlag;
	double timeOut;
	int err;

	#ifdef DEMOMODE
	strcpy(Vent.Gui.version, "GUI:4- DEMO -85-D"); // GUI:4-070212-85-D
	strcpy(Vent.Bd.version,  " BD:4- DEMO -85-D"); //  BD:4-070212-85-D
	AddVerStr(Vent.Gui.version);
	AddVerStr(Vent.Bd.version);
	doneFlag = TRUE;
	#else	
	ClearPacket(outBuff);
	TestActive = YES_840;

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = SOFTWARE_VERSION;
	outBuff[4] = 0x00;
	outBuff[5] = 0x00;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	VentInfoActive = YES_840;
	err = Send840(outBuff);
	if(err){
		return FAILURE;
	}

	doneFlag = FALSE;
	timeOut = Timer() + VENT_DATA_TIMEOUT;
	while(!doneFlag && (Timer() < timeOut)){
		if(CharsIn840Q() && CommActive == COM_840_INACTIVE){
			err = ProcessIncomingPacket();
			if(err != SUCCESS){
				return FAILURE;
			}
		}

		if(VentInfoActive == NO_840){
			doneFlag = TRUE;
		}
	}
	#endif // #ifdef DEMOMODE

	if(!doneFlag){
		return FAIL_840;
	}
	else{
		return OK_840;
	}
}


/*****************************************************************************/
/* Name:     GetVentDataSerial                                                       
/*                                                                           
/* Purpose:  send command to vent to serial number info 
/*                                          
/*                                                                           
/* Example Call:   GetVentDataSerial()                                     
/*                                                                           
/* Input:    none             
/*                                                                           
/* Output:   int                                                           
/*                                                                           
/* Globals Changed:   VentInfoActive, TestActive                                                            
/*                                                                           
/* Globals Used:   none                                                             
/*                                                  
/*                                                                           
/* Return:   status                                                          
/*                                                                           
/*****************************************************************************/
static int GetVentDataSerial(void)
{
	Packet outBuff;
	BOOL doneFlag;
	double timeOut;
	int err;

	#ifdef DEMOMODE
	strcpy(Vent.Gui.post, "GUI Post:4- DEMO -85-E"); // GUI Post:4-070079-85-E
	strcpy(Vent.Bd.post,  " BD Post:4- DEMO -85-E"); //  BD Post:4-070079-85-E
	strcpy(Vent.saas, " Saas:4- DEMO -85-B");        //  Saas:4-072683-85-B
	strcpy(Vent.Gui.serial, " GUI SN:42* DEMO *");   //  GUI SN:4200122267
	strcpy(Vent.Bd.serial, " BD SN:42* DEMO *");     //  BD SN:4200120512
	strcpy(Vent.Compressor.serial, "CompressorSN:42* DEMO *");
	AddVerStr(Vent.Gui.post);
	AddVerStr(Vent.Bd.post);
	AddVerStr(Vent.saas);
	doneFlag = TRUE;
	#else
	ClearPacket(outBuff);
	TestActive = YES_840;

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = UNIT_SERIAL_NUMBER;
	outBuff[4] = 0x00;
	outBuff[5] = 0x00;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	VentInfoActive = YES_840;
	err = Send840(outBuff);
	if(err){
		return FAILURE;
	}

	doneFlag = FALSE;
	timeOut = Timer() + VENT_DATA_TIMEOUT;
	while(!doneFlag && (Timer() < timeOut)){
		if(CharsIn840Q() && CommActive == COM_840_INACTIVE){
			err = ProcessIncomingPacket();
			if(err != SUCCESS){
				return FAILURE;
			}
		}

		if(VentInfoActive == NO_840){
			doneFlag = TRUE;
		}
	}
	#endif // #ifdef DEMOMODE

	if(!doneFlag){
		return FAIL_840;
	}
	else{
		return OK_840;
	}
}


/*****************************************************************************/
/*Name:        SetCompressorData                                             */
/*                                                                           
/* Purpose:  send command to vent to serial number info  
/*                                          
/*                                                                           
/* Example Call:   SetCompressorData( type, str)                                     
/*                                                                           
/* Input:    int, char *          
/*                                                                           
/* Output:   int                                                           
/*                                                                           
/* Globals Changed:    none                                                           
/*                                                                           
/* Globals Used:  none                                                             
/*                                                  
/*                                                                           
/* Return:   status                                                        
/*                                                                           
/*****************************************************************************/
int SetCompressorData( int type, char *str)
{
	Packet outBuff;
	BOOL doneFlag;
	double timeOut;
	int err;

	unsigned char length;
	BOOL statusFlag = OK_840;

	#ifndef DEMOMODE
	ClearPacket(outBuff);

	switch(type){
		case REMOTE_TEST_SET_COMPRESSOR_SN_ID:
			outBuff[5] = REMOTE_TEST_SET_COMPRESSOR_SN_ID;
			break;

		case REMOTE_TEST_SET_COMPRESSOR_TIME_ID:
			outBuff[5] =  REMOTE_TEST_SET_COMPRESSOR_TIME_ID;
			break;

		default:
			statusFlag = FAIL_840;
			break;
	} // end switch(type)

	if(statusFlag == OK_840){

		outBuff[0] = 0x00;
		outBuff[1] = 0x06;
		outBuff[3] = TEST_EVENT;
		outBuff[4] = 0x00;
		outBuff[6] = 0x00;

		TestActive = YES_840;
		KeyAllowedReturn = NO_KEYS_ALLOWED;

		strcpy((char *)&outBuff[6], str);
		length = strlen(str) + 1;
		length += 6;
		outBuff[1] = length;

		err = Send840(outBuff);
		if(err){
			return FAILURE;
		}

		doneFlag = FALSE;
		timeOut = Timer() + VENT_DATA_TIMEOUT;
		while(!doneFlag && (Timer() < timeOut)){
			if(CharsIn840Q() && CommActive == COM_840_INACTIVE){
				err = ProcessIncomingPacket();
				if(err != SUCCESS){
					return FAILURE;
				}
			}

			if(TestActive == NO_840){
				doneFlag = TRUE;
			}

			ProcessSystemEvents();
		} // end while loop

		if(!doneFlag){
			statusFlag = FAIL_840;
		}
	} // endif(statusFlag == OK_840)
	#else
	switch(type){
		case REMOTE_TEST_SET_COMPRESSOR_SN_ID:
			sprintf(Vent.Compressor.serial, "CompressorSN:%s", str);
			break;

		case REMOTE_TEST_SET_COMPRESSOR_TIME_ID:
			sscanf(str, "%f", &Vent.Compressor.runTimeHours);
			break;

		default:
			statusFlag = FAIL_840;
			break;
	} // end switch(type)
	#endif // #ifndef DEMOMODE

	return(statusFlag);
}


/*****************************************************************************/
/* Name:     AtmosphereTest                                                        
/*                                                                           
/* Purpose:  send command to vent to execute Atompspheric pressure test
/*                                          
/*                                                                           
/* Example Call:   AtmosphereTest();                                      
/*                                                                           
/* Input:    none            
/*                                                                           
/* Output:   none                                                            
/*                                                                           
/* Globals Changed:   TestActive                                                           
/*                                                                           
/* Globals Used:   none                                                            
/*                                                  
/*                                                                           
/* Return:   none                                                          
/*                                                                           
/*****************************************************************************/
int AtmosphereTest(void)
{
	Packet outBuff;
	int err;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = TEST_EVENT;
	outBuff[4] = 0x00;
	outBuff[5] = REMOTE_TEST_ATMOS_PRESSURE_READ_ID;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	TestActive = YES_840;
	err = Send840(outBuff);
	Delay(0.1);

	return err;
}


/*****************************************************************************/
/* Name:     CircuitPressure                                                      
/*                                                                           
/* Purpose:  send command to vent to execute Circuit Pressure test 
/*                                          
/*                                                                           
/* Example Call:   CircuitPressure();                                    
/*                                                                           
/* Input:    none             
/*                                                                           
/* Output:   none                                                             
/*                                                                           
/* Globals Changed:  TestActive                                                             
/*                                                                           
/* Globals Used:   none                                                             
/*                                                  
/*                                                                           
/* Return:   none                                                         
/*                                                                           
/*****************************************************************************/
int CircuitPressure(void)
{
	Packet outBuff;
	int err;

	ClearPacket(outBuff);   

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = TEST_EVENT;
	outBuff[4] = 0x00;
	outBuff[5] = REMOTE_TEST_CIRCUIT_PRESSURE_ID;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	TestActive = YES_840;
	err = Send840(outBuff);
	Delay(0.1);

	return err;
}

/*****************************************************************************/
/* Name:     FlushGas                                                     
/*                                                                           
/* Purpose:  This function will flush the test circuit with AIR or O2         
/*          conversation 
/*                                                                           
/* Example Call:   int FlushGas(int type);                                    
/*                                                                           
/* Input:    <int>           
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int FlushGas(int type)
{
	int done = 0,
	err,
	comStatus;
	double timeOut;

	Packet outBuff;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = TEST_EVENT;
	outBuff[4] = 0x00;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	if(type == AIR_FLUSH){
		outBuff[5] = REMOTE_TEST_PURGE_WITH_AIR_ID;
	}
	else if(type == O2_FLUSH){
		outBuff[5] = REMOTE_TEST_PURGE_WITH_O2_ID;
	}

	TestActive = YES_840;

	FlushInQ840();
	FlushOutQ840();

	err = Send840(outBuff);
	if(err){
		return FAILURE;
	}

	timeOut = Timer() + (double)GAS_TIMEOUT;
	while(!done && ( Timer() < timeOut) ){
		ProcessSystemEvents();
		// check if user pressed abort
		if(gStoppedInd == 1){
			return FAILURE;
		}

		if(CharsIn840Q() && (CommActive == COM_840_INACTIVE)){
			comStatus = ProcessIncomingPacket();
			if(comStatus != SUCCESS){
				return FAILURE;
			}
		}

		if(TestActive == NO_840){
			done = 1;
		}
	}

	return SUCCESS;
}

/*****************************************************************************/
/* Name:    ClearPacket                                                        
/*                                                                           
/* Purpose:  to zero a Packet
/*                                          
/*                                                                           
/* Example Call:   ClearPacket(Packet)                                  
/*                                                                           
/* Input:   Packet          
/*                                                                           
/* Output:   none                                                          
/*                                                                           
/* Globals Changed:   none                                                          
/*                                                                           
/* Globals Used:   none                                                          
/*                                                  
/*                                                                           
/* Return:   none                                                         
/*                                                                           
/*****************************************************************************/
void ClearPacket(Packet target)
{
	int x;

	for(x = 0; x < MAX_PACKET_SIZE; x++){
		target[x] = 0;
	}

	return;
}


/*****************************************************************************/
/* Name:     ClearDS( )                                                          
/*                                                                           
/* Purpose:  to zero a DataStorage 
/*                                          
/*                                                                           
/* Example Call:   ClearDS();                                     
/*                                                                           
/* Input:    none         
/*                                                                           
/* Output:   none                                                       
/*                                                                           
/* Globals Changed:  DataStorage,DsMirror                                                         
/*                                                                           
/* Globals Used:    none                                                          
/*                                                  
/*                                                                           
/* Return:   none                                                          
/*                                                                           
/*****************************************************************************/
void ClearDS(void)
{
	int x;

	for(x = 0; x < MAX_RETURN_VALUE; x++){
		DataStorage[x] = 0.0;
		DsMirror[   x] = 0;
	}

	return;
}


/*****************************************************************************/
/* Name:     BdAudio                                                      
/*                                                                           
/* Purpose:  to command the vent to execute bd Alarm test 
/*                                          
/*                                                                           
/* Example Call:   BdAudio() ;                                   
/*                                                                           
/* Input:    none          
/*                                                                           
/* Output:   none                                                        
/*                                                                           
/* Globals Changed:  TestActive                                                         
/*                                                                           
/* Globals Used:  none                                                          
/*                                                  
/*                                                                           
/* Return:   none                                                       
/*                                                                           
/*****************************************************************************/
int BdAudio(void)
{
	Packet outBuff;
	int err;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = TEST_EVENT;
	outBuff[4] = 0x00;
	outBuff[5] = REMOTE_TEST_BD_AUDIO_ID;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	TestActive = YES_840;
	err = Send840(outBuff);

	return err;
}

/*****************************************************************************/
/* Name:     GuiAudio                                                    
/*                                                                           
/* Purpose:  to command the vent to execute bd Alarm test   
/*                                          
/*                                                                           
/* Example Call:   GuiAudio();                                    
/*                                                                           
/* Input:    <None>
/*                                                                           
/* Output:   <None>
/*                                                                           
/* Globals Changed:  TestActive
/*                                                                           
/* Globals Used:  <None>
/*                                                  
/*                                                                           
/* Return:   needs description
/*                                                                           
/*****************************************************************************/
int GuiAudio( void  )
{
    Packet outBuff;
	int err;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = TEST_EVENT;
	outBuff[4] = 0x00;
	outBuff[5] = REMOTE_TEST_GUI_AUDIO_ID;
	outBuff[6] = 0x00;
	outBuff[7] = 0x00;

	TestActive = YES_840;
	err = Send840(outBuff);

	return err;
}


/*****************************************************************************/
/* Name:     SetAirFlow                                                    
/*                                                                           
/* Purpose:  Command used to set the 840 air flow rate  
/*                                          
/*                                                                           
/* Example Call:   int SetAirFlow(20);
/*
/* Input:    flow - The value of desired flow         
/*                                                                           
/* Output:   <None>
/*                                                                           
/* Globals Changed:  <None>
/*                                                                           
/* Globals Used:  <None>
/*                                                  
/*                                                                           
/* Return:   success or unsuccessful                                                       
/*                                                                           
/*****************************************************************************/
int SetAirFlow(int flow)
{
	Packet outBuff;
	char tempBuff[16];
	int length;
	int err;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = TEST_EVENT;
	outBuff[4] = 0x00;
	outBuff[5] = REMOTE_TEST_SET_AIR_FLOW_ID;
	outBuff[6] = 5;

	sprintf(tempBuff,"%i", flow);   //slpm
	strcpy((char *)&outBuff[6], tempBuff);
	length = strlen(tempBuff) + 1;
	length += 6;
	outBuff[1] = length;

	err = Send840(outBuff);

	return err;
}


/*****************************************************************************/
/* Name:     SetO2Flow                                                    
/*                                                                           
/* Purpose:  Command used to set the 840 o2 flow rate  
/*                                          
/*                                                                           
/* Example Call:   int SetO2Flow(20);
/*
/* Input:    flow - The value of desired flow         
/*                                                                           
/* Output:   <None>
/*                                                                           
/* Globals Changed:  <None>
/*                                                                           
/* Globals Used:  <None>
/*                                                  
/*                                                                           
/* Return:   success or unsuccessful                                                       
/*                                                                           
/*****************************************************************************/
int SetO2Flow(int flow)
{
	Packet outBuff;
	char tempBuff[16];
	int length;
	int err;

	ClearPacket(outBuff);

	outBuff[0] = 0x00;
	outBuff[1] = 0x06;
	outBuff[3] = TEST_EVENT;
	outBuff[4] = 0x00;
	outBuff[5] = REMOTE_TEST_SET_O2_FLOW_ID;
	outBuff[6] = 5;

	sprintf(tempBuff,"%i", flow);   //slpm
	strcpy((char *)&outBuff[6], tempBuff);
	length = strlen(tempBuff) + 1;
	length += 6;
	outBuff[1] = length;

	err = Send840(outBuff);

	return err;
}
