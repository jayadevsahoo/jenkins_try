// TestResultLogSet.h : Declaration of the CTestResultLogSet

#pragma once

// code generated on Wednesday, October 13, 2010, 4:11 PM

class CTestResultLogSet : public CRecordset
{
public:
	CTestResultLogSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestResultLogSet)

// Field/Param Data

// The string types below (if present) reflect the actual data type of the
// database field - CStringA for ANSI datatypes and CStringW for Unicode
// datatypes. This is to prevent the ODBC driver from performing potentially
// unnecessary conversions.  If you wish, you may change these members to
// CString types and the ODBC driver will perform all necessary conversions.
// (Note: You must use an ODBC driver version that is version 3.5 or greater
// to support both Unicode and these conversions).

	CStringA	m_test_session_id;
	CStringA	m_test_script_name;
	CStringA	m_test_start_time;
	CStringA	m_test_user_name;
	CLongBinary 	m_test_report;
	CLongBinary 	m_test_status_log;
	CStringA	m_test_report_name;
	CStringA	m_test_status_log_name;
	CLongBinary	m_test_script_file;

// Overrides
	// Wizard generated virtual function overrides
	public:
	virtual CString GetDefaultConnect();	// Default connection string

	virtual CString GetDefaultSQL(); 	// default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX support

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};


