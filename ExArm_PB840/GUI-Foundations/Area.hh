#ifndef Area_HH
#define Area_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: Area - This class represents a rectangle.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Area.hhv   25.0.4.0   19 Nov 2013 14:11:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//        Removed ifndef / endif around include directives
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//        Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

//@ Usage-Classes
//@ End-Usage

//@ Type: Alignment
// This type lists the kinds of alignment that are supported.
// Note that this enum is not scoped to the Area class.
enum Alignment
{
	LEFT,
	RIGHT,
	CENTER
};

class Area 
{
public:

	Area(void);
	Area(Int x1, Int y1, Int x2, Int y2);
	Area(const Area& other);
	~Area(void);

	inline void setX1(Int x1);
	inline void setY1(Int y1);
	inline void setX2(Int x2);
	inline void setY2(Int y2);
	void set(Int x1, Int y1, Int x2, Int y2);

	void setX(Int x);
	void setY(Int y);
	void moveBy(Int xOffset, Int yOffset);

	inline Int getX1(void) const;
	inline Int getY1(void) const;
	inline Int getX2(void) const;
	inline Int getY2(void) const;

	inline Int getWidth(void) const;
	inline Int getHeight(void) const;

	inline void setWidth(Int width);
	inline void setHeight(Int height);

	Boolean overlapsArea(const Area& otherArea) const;
	Boolean makeOverlap(const Area& otherArea, Area& intersectionArea) const;
	void makeMerge(const Area& otherArea, Area& unionArea) const;
	Boolean containsPoint(Int x, Int y) const;
	Boolean containsArea(const Area& otherArea) const;

	void alignArea(Int width, Int height,
				   Alignment horzAlignment, Area& resultArea) const;
	Boolean subtractArea(const Area& otherArea);

	Boolean operator==(const Area &d) const;
	Boolean operator!=(const Area &d) const;
	void operator=(const Area& otherArea);

	static void SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName  = NULL,
						  const char*        pPredicate = NULL);

protected:

private:

	//@ Data-Member: x1_
	// X coordinate of the upper left point of the rectangle.
	Int x1_;	

	//@ Data-Member: y1_
	// Y coordinate of the upper left point of the rectangle.
	Int y1_;	

	//@ Data-Member: x2_
	// X coordinate of the lower right point of the rectangle.
	Int x2_;	

	//@ Data-Member: y2_
	// Y coordinate of the lower right point of the rectangle.
	Int y2_;	
};

// Inlined methods
#include "Area.in"

#endif // Area_HH 
