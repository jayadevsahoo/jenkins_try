
//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file CaptureSettingsDlg.h
//----------------------------------------------------------------------------


#pragma once
#include "afxcmn.h"


// CCaptureSettingsDlg dialog

class CCaptureSettingsDlg  : public CDialog
{
	DECLARE_DYNAMIC(CCaptureSettingsDlg)

public:
	CCaptureSettingsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCaptureSettingsDlg();

// Dialog Data
	enum { IDD = IDD_SETTINGS_CAPTURE_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_List;
	virtual BOOL OnInitDialog();
	void InsertColumns();
	void DisplayValues(CString strLine, bool bOnlyValue, int nColumnNumber);
	void Refresh();


	


	

protected:
	virtual void PostNcDestroy();
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnCustomDrawList1( NMHDR* pNMHDR, LRESULT* pResult );
	afx_msg void OnBnClickedButtonSave();
};
