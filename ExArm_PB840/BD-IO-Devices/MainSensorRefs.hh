
#ifndef MainSensorRefs_HH
#define MainSensorRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: MainSensorRefs - All the external references of various flow,
//		   temperature and pressure sensors. 
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/MainSensorRefs.hhv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 004 By: syw   Date: 13-Mar-1997   DR Number: DCS
//  	Project:  Sigma (R8027)
//		Description:
//			Eliminate rExhO2FlowSensor and rExhDryFlowSensor instance.
//
//  Revision: 003 By: syw   Date: 17-Jul-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added air and o2 flow and temperature sensors for GUI_CPU.
//
//  Revision: 002 By: syw    Date: 02-May-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added BD_CPU macros.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

// reference to instanciation of sensors
class FlowSensor;
class GasInletPressureSensor;

extern FlowSensor& RO2FlowSensor;
extern FlowSensor& RAirFlowSensor;

// TODO E600_LL: change the #include to forward declaration when possible
#include "ExhFlowSensor.h"
extern ExhFlowSensor_t& RExhFlowSensor;

class TemperatureSensor;

extern TemperatureSensor& RO2TemperatureSensor;
extern TemperatureSensor& RAirTemperatureSensor;

// TODO E600_LL: to be reviewed in the future to see if nec. to keep
//extern TemperatureSensor& RExhGasTemperatureSensor;

#if defined (SIGMA_BD_CPU)
 
class PressureSensor;
extern PressureSensor& RInspPressureSensor;
extern PressureSensor& RExhPressureSensor;
extern PressureSensor& RExhDrvPressureSensor;
extern GasInletPressureSensor& RAirInletPressureSensor;
extern GasInletPressureSensor& RO2InletPressureSensor;

class SensorMediator;
extern SensorMediator& RSensorMediator;

#endif // defined (SIGMA_BD_CPU)

#endif // MainSensorRefs_HH 




