#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: GuiIoUtil - POST GUI IO Utilities.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/GuiIoUtil.ccv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project:   Sigma   (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
//#include "Post_Library.hh"
#include "CpuDevice.hh"
#include "GuiIoUtil.hh"
#include "MemoryMap.hh"
//To do E600 Need to analyse requirement/hardware to generate sound from BD board
#ifdef SIGMA_GUI_CPU
#include "Sound.hh"
#endif
#include <stdio.h>


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: SwitchRemoteAlarm
//
//@ Interface-Description
//    This function activates or deactivates the remote alarm 
//    according to the boolean state parameter.  TRUE turns on the
//    alarm, FALSE turns it off.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================
void 
SwitchRemoteAlarm(Boolean state)
{
//To do E600 Need to analyse requirement/hardware to generate sound from BD board
#ifdef E600_840_TEMP_REMOVED
   if ( CpuDevice::IsGuiCpu() )
    {
        // $[TI1.1]
        if ( state )
        {
            // $[TI2.1]
            CpuDevice::ClearBitsIoReg1(REMOTE_ALARM_BIT);
        }
        else
        {
            // $[TI2.2]
            CpuDevice::SetBitsIoReg1(REMOTE_ALARM_BIT);
        }
    }
    // $[TI1.2]
#endif
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: SwitchSoundAlarm
//
//@ Interface-Description
//    This function activates or deactivates the SOUND high urgency
//    alarm according to the state parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================
void 
SwitchSoundAlarm(Boolean state)
{
//To do E600 Need to analyse requirement/hardware to generate sound from BD board
#ifdef SIGMA_GUI_CPU
    if ( CpuDevice::IsGuiCpu() )
    {		
        // $[TI1.1]
        if ( state )
        {
            // $[TI2.1]
			Sound::Start(Sound::HIGH_URG_ALARM);
        }
        else
        {
            // $[TI2.2]
			Sound::Start(Sound::NO_SOUND);
        }
    }
    // $[TI1.2]
#endif
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:  SwitchGuiLeds
//
//@ Interface-Description
//    This function activates or deactivates the GUI panel LEDs 
//    according to the boolean parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Free-Function
//=====================================================================

void 
SwitchGuiLeds(Boolean state)
{
//To do E600 Need to analyse requirement/hardware to generate LED light from BD board
#ifdef E600_840_TEMP_REMOVED
    if ( CpuDevice::IsGuiCpu() )
    {
        // $[TI1.1]
        if ( state )
        {
           // $[TI2.1]
           *FP_LED   = 0xFFFF;
           *KEYS_LED_1 = 0xFF;
        }
        else
        {
           // $[TI2.2]
           *FP_LED   = 0x0000;
           *KEYS_LED_1 = 0x00;
        }
    }
    // $[TI1.2]
#endif
}

