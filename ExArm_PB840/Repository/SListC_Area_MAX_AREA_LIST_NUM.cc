#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SListC_Area_MAX_AREA_LIST_NUM - Fixed Singly-Linked List of 'Area' Items.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a derived class of an abstract, size-independent
//  singly-linked list of 'Area' items ('AbstractSListC').  This class
//  provides an abstraction for a singly-linked list of generic data
//  items each of type 'Area'.  The data items of the list are copies
//  of the data items that are added to the list.  There are
//  non-constant, public methods to iterate through the contents of the
//  list, and there's also an iterator class (see 'SIteratorC<Area>')
//  to allow iteration of a constant list.  This is a fixed list where,
//  at most, 'MAX_AREA_LIST_NUM' items can be in the list at one time.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a standard abstraction for storing data items
//  in a dynamic collection, where initial placement within the
//  collection is quick and easy.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each list has an internal heap containing singly-linked
//  nodes ('SNodeC_Area').  The nodes are allocated from
//  the heap to contain copies of the inserted data.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions have to do with the data type that is chosen for
//  this list.  The data type must provide the following mechanisms,
//  to be allowed to be used in this list:  a copy constructor, an
//>Von
//      Area::Area(const Area& aType);            // copy constructor...
//      Area::operator=(const Area& aType);       // assignment operator...
//      Area::operator==(const Area& aType) const;// equivalence operator...
//      Area::operator!=(const Area& aType) const;// inequivalence operator...
//>Voff
//  For primitive (built-in) types this is already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      The assignment operator of any class used as a type of this list
//      must be implemented such that upon completion of an assignment
//      between two instances, the instances are equivalent (i.e.
//      'a = b; POST_CONDITION(a == b);').
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedSListC.C_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SListC_Area_MAX_AREA_LIST_NUM.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SListC_Area_MAX_AREA_LIST_NUM(sList)  [Copy Constructor]
//
//@ Interface-Description
//  Create a list equivalent to 'sList'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The address of this list's heap of nodes is passed to the base
//  class's constructor.  The copying of 'sList' MUST be done from
//  this class, as opposed to the base class, because the heap of
//  nodes won't be constructed until AFTER the base class's
//  constructor is run.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == sList  &&  (isEmpty()  ||
//                        currentItem() == sList.currentItem()))
//@ End-Method
//=====================================================================

SListC_Area_MAX_AREA_LIST_NUM::SListC_Area_MAX_AREA_LIST_NUM(
				     const SListC_Area_MAX_AREA_LIST_NUM& sList
					      )
				     : SListC_Area(&heapOfNodes_),
				       heapOfNodes_()
{
  CALL_TRACE("SListC_Area_MAX_AREA_LIST_NUM(sList)");

  operator=(sList);  // MUST be done here, not in base class...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SListC_Area_MAX_AREA_LIST_NUM()  [Default Constructor]
//
//@ Interface-Description
//  Create an empty list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The address of this list's heap of nodes is passed to the base
//  class's constructor.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

SListC_Area_MAX_AREA_LIST_NUM::SListC_Area_MAX_AREA_LIST_NUM(void)
				    : SListC_Area(&heapOfNodes_),
				      heapOfNodes_()
{
  CALL_TRACE("SListC_Area_MAX_AREA_LIST_NUM()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SListC_Area_MAX_AREA_LIST_NUM()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this list and each of its nodes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The emptying of this list MUST be done from this destructor, as
//  opposed to the base class's destructor, because by the time the
//  base class's destructor is called, the heap has already been
//  destroyed.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SListC_Area_MAX_AREA_LIST_NUM::~SListC_Area_MAX_AREA_LIST_NUM(void)
{
  CALL_TRACE("~SListC_Area_MAX_AREA_LIST_NUM()");

  clearList();  // this MUST be here, as opposed to the base class...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//	                [static]
//
//@ Interface-Description
//	Report the software fault that occured within the source code
//	of this class.  The fault, indicated by 'softFaultID', occured
//	at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
SListC_Area_MAX_AREA_LIST_NUM::SoftFault(const SoftFaultID softFaultID,
				  const Uint32      lineNumber,
				  const char*       pFileName,
				  const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, FIXED_SLISTC,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

#if defined(SIGMA_DEBUG)
#  include "Foundation.hh"
#endif  // defined(SIGMA_DEBUG)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr)  [const, virtual]
//
// Interface-Description
//  This method overrides the base class's pure virtual method, and
//  dumps out an ASCII representation of the current state of this
//  list to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
SListC_Area_MAX_AREA_LIST_NUM::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "SListC_Area_MAX_AREA_LIST_NUM {\n";

  if (!isEmpty())
  {
    const SNodeC_Area*  pNode = getFirst_();

    do
    {
      ostr << ((pNode == getCurrent_()) ? "->" : "  ")
	   << pNode->getItem() << '\n';
    } while ((pNode = pNode->getNextPtr()) != NULL);
  }

#if defined(SIGMA_DEBUG)
  if (Foundation::IsDebugOn(::ABSTRACT_HEAP)  ||
      Foundation::IsDebugOn(::FIXED_HEAP))
  {
    // debugging the heap, therefore print it out...
    ostr << "------\n";
    ostr << "heapOfNodes_ = " << heapOfNodes_ << '\n';
  }
#endif  // defined(SIGMA_DEBUG)

  ostr << "};\n";
  ostr.flush();

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
