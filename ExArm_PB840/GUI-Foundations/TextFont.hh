#ifndef TextFont_HH
#define TextFont_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: TextFont - This class defines textual fonts used by TextField
// and NumericField classes.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/TextFont.hhv   25.0.4.0   19 Nov 2013 14:11:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: gdc    Date:  03-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Improved font metrics.
// 
//  Revision: 012   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 011   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 010  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 009  By:  clw	   Date:  06-Mar-98    DR Number: 
//    Project:  Japanese
//    Description:
//		Added support for Japanese fonts. This involved adding a new JAPANESE
//      entry to the Style enum, and adding a new SIXTEEN entry to the Size enum.
//
//  Revision: 008  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 007  By:  hhd	   Date:  30-Sep-97    DR Number: 2512
//    Project:  Sigma (R8027)
//    Description:
//		Compiled declaration of method GetCommaHeightOffset() only for TARGET. 
//
//  Revision: 006  By:  hhd	   Date:  24-Sep-97    DR Number: 2512
//    Project:  Sigma (R8027)
//    Description:
//		Added a 'commaHeightOffset' variable to FontInfo structure. 
//		Added a query method for this value, to be used by the NumericField class .
//		Added new constants.
//
//  Revision: 005  By:  hhd	   Date:  18-Sep-97    DR Number: 2512
//    Project:  Sigma (R8027)
//    Description:
//		Added a 'height' variable to FontInfo structure. 
//		Added a query method for this value, to be used by the NumericField class .
//
//  Revision: 004  By: gdc      Date: 15-Feb-1996  DCR Number: 981
//    Project:  Sigma (R8027)
//    Description:
//	Corrected minor syntax errors detected by new compiler.
//
//  Revision: 003  By: gdc      Date: 15-Feb-1996  DCR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 002  By: yyy      Date: 13-Jun-1995  DCR Number: 476 
//    Project:  Sigma (R8027)
//    Description:
//      Added method GetStringSize() which will calculate the height and width of the
//      string passed to this method.  Due to the addition of this method, the SUN
//      version of FontInfo structure has one more field XFontStruc *xId.  This field
//      shall remember the font id for this structure and be used later by 
//      GetStringSize().
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

	//@ Type: FontID
	// This typedef establishes a Font identifier as an Int16.
	typedef Int16 FontID;

	//@ Type: FontInfo
	// This struct encapsulates all pertinent info for a font.
	struct FontInfo
	{
		FontID id;
		Int ascent;
		Int descent;
		Int width;
		Int height;
		Int maxAscent;
		Int maxDescent;
	};


class TextFont
{
public:
	//@ Data-Member: INVALID_FONT_ID_
	// This is a flag indicating a invalid (not supported) font ID.
	static const FontID INVALID_FONT_ID;
	
	//@ Data-Member: VGA_X_START
	// This is an arbitary x location passed to VGATextRect to ensure the x
	// coordinate stays positive during the rendering.
	static const Uint16 VGA_X_START;

	//@ Data-Member: VGA_Y_START
	// This is an arbitary y location passed to VGATextRect to ensure the y
	// coordinate stays positive during the rendering.
	static const Uint16 VGA_Y_START;

	//@ Type: Style
	// This enum lists the possible styles for the font.
	enum Style {
		THIN,
		NORMAL,
		ITALIC,
		JAPANESE,
		NUM_STYLE};

	//@ Type: Size
	// This enumeration lists the sizes which it is legitimate to use.
	enum Size {
		NO_SIZE = -1,
		MIN_SIZE = 6,
		SIX = 6,
		EIGHT = 8,
		TEN = 10,
		TWELVE = 12,
		FOURTEEN = 14, 
        SIXTEEN = 16,
		EIGHTEEN = 18,
		TWENTY_FOUR = 24,
		THIRTY_TWO = 32,
		SIXTY = 60,
		MAX_SIZE = 60 };
    
	static void Initialize(void);

	static FontID GetFont(TextFont::Size point, Style style);
	static Int GetAscent(TextFont::Size point, Style style);
	static Int GetDescent(TextFont::Size point, Style style);
	static Int GetMaxAscent(TextFont::Size point, Style style);
	static Int GetMaxDescent(TextFont::Size point, Style style);
	static Int GetWidth(TextFont::Size point, Style style);
	static Int GetHeight(TextFont::Size point, Style style);
	static void GetStringSize(wchar_t *string, TextFont::Size point, Style style,  
							Int32 &width, Int32 &height);
	static wchar_t GetFontStyleChar(TextFont::Style style);  

	static void SoftFault(const SoftFaultID softFaultID,
						 const Uint        lineNumber,
						 const char*       pFileName  = NULL, 
						 const char*       pPredicate = NULL);

protected:

private:
	TextFont(void);		// not implemented
	~TextFont(void);	// not implemented
	TextFont(const TextFont&);			// Declared but not implemented
	void operator=(const TextFont&);   // Declared but not implemented

	inline static Int getFontIndex_(TextFont::Size point,
                                    TextFont::Style style);

	//@ Data-Member: AllFonts_
	// This is an array which stores info regarding all fonts, a handle to
	// each font along with some font dimension info.
	static FontInfo AllFonts_[];

	//@ Data-Member: NUM_FONTS
	// This is an integer which stores the number of fonts allocated in
	// AllFonts_.
	static const Uint16 NUM_FONTS;
	
	//@ Data-Member: WIDTH_CHAR
	// This is an char array which stores the "Biggest" character in the font.
	static const char WIDTH_CHAR[];
	
	//@ Data-Member: HEIGHT_CHAR
	// This is an char array which stores the "Tallest" character in the font.
	static const char HEIGHT_CHAR[];
};

#include "TextFont.in"

#endif // TextFont_HH
