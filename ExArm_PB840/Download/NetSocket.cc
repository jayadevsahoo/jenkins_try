#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NetSocket - defines a network socket features to
//  	transmit and receive  network data.
//---------------------------------------------------------------------
//@ Interface-Description
//     The class provides interfaces to create TCP/IP socket object
//     and communicate using the socket. Some of the virtual methods
//     can be overriden to provide UDP type network communication.
//     All the communication mode is set to be blocking.
//---------------------------------------------------------------------
//@ Rationale
//     Provide an abstract class to encapsulate all the common network
//     socket features. The class can be a base class for all the network
//     communication socket classes.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The class provides all the TCP/IP socket communication features.
//    There are lots of common features that are shared among UDP/TCP
//    sockets. This class provides virtual methods so UDP or other type
//    of protocol socket can override them.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions  and pre-conditions are used to
//  ensure correctness of software conditons.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/NetSocket.ccv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 005   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 004  By: gdc    Date: 22-Feb-05    DR Number: 6148
//  Project:  NIV1
//  Description:
//      DCS 6148 - move to solaris
//      included "strings.h" containing bzero declarations on solaris
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  08-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

#include "NetSocket.hh"
#include "Download.hh"
#if	defined(SIGMA_SUN)
#include <sys/utsname.h>
#include <strings.h>
extern "C" int uname(struct utsname *);
#endif //defined(SIGMA_SUN)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NetSocket(void)  [Default Constructor]
//
//@ Interface-Description
//   Constructor. Leave the socket closed initially.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Set the socket id to -1.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

NetSocket::NetSocket(void)
{
	// $[TI1]
	socketId_= -1;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NetSocket(void)  [Destructor]
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Close the socket if it is opened()
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NetSocket::~NetSocket(void)
{
	if ( opened() ) // $[TI1]
	{
		NetSocket::close();
	} // else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: open(PortId portId, Int nListens)
//
//@ Interface-Description
//   Create a TCP socket and bind the socket to a given port id.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The SOCK_STREAM protocol type indicates it's a TCP/IP socket.
//   The method expects the createSocket_ method to return -1
//   when it fails to create a socket, then the socket would be still
//	 closed since the socketId_ is set to -1.
//   
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
NetSocket::open(PortId portId, Int nListens)
{
	// $[TI1]
	return(socketId_ = createSocket_(SOCK_STREAM, portId, nListens));
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: createSocket_(Int protocol,  PortId portId, Int numListen)
//
//@ Interface-Description
//   Create a socket of a given protocol type.
//   Bind the socket to a given port id. If the given numListen argument
//   is non-zero, the socket is will be allowed to establish up to
//   nListens number of socket connection.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Set the keep alive protocol mode so that TCP/IP will maintains
//   the Keep alive  protocol.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int
NetSocket::createSocket_(Int protocol,  PortId portId, Int numListen)
{
int opt=1; // turn keep_alive on
struct sockaddr_in  addr;
Int newSocket;

    if ((newSocket = ::socket(AF_INET, protocol, 0)) == -1) // $[TI1]
	{
		Download::ReportError("NetSocket::createSocket_():socket() cannot create a socket",
			errno);
        return(-1);
	} // else $[TI2]

    if ( ::setsockopt(newSocket, SOL_SOCKET, SO_REUSEADDR,(char *)&opt, sizeof(int))
		< 0  ) // $[TI3]
	{
		::close(newSocket);
		Download::ReportError("NetSocket::createSocket_():setsockopt() setsockopt(REUSEADDR) failed",
			errno);
        return(-1);
	} // else $[TI4]

	if ( portId ) // $[TI5]
	{
    	::bzero((char *) &addr,sizeof(addr));
    	addr.sin_addr.s_addr = htonl(INADDR_ANY);
    	addr.sin_port       = htons(portId);
    	addr.sin_family     = AF_INET;
    	if ( ::bind(newSocket,(sockaddr *)&addr,sizeof(addr)) < 0 ) // $[TI5.1]
		{
			Download::ReportError("NetSocket::createSocket_():bind() cannot bind a socket port",
			errno);
			::close(newSocket);
			return(-1);
		} // else $[TI5.2]
	} // else $[TI6]

	if ( numListen ) // $[TI7]
	{
		if (::listen(newSocket, numListen )) // $[TI7.1]
		{
			Download::ReportError("NetSocket::createSocket_():listen() listen failed", errno);
			::close(newSocket);
			return(-1);
		} // else $[TI7.2]
    } // else $[TI8]
	return( newSocket );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  readData(void *buf, int nbytes)
//
//@ Interface-Description
//    The readData method receives 'nbytes' of network data
//    from this socket into the 'buf' memory area. The method returns
//    a byte count of the received data. If any communication error,
//    the method returns -1. All the socket library call errors
//    are examined and logged internally.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    If the receive operation fails due to the lack of TCP/IP 
//    IO buffer, the method waits 20 milliseconds before it tries
//    to read again. Retry count for the no system buffer is set 
//    twice as many as the normal retry count.
// 
//    For a blocked io socket, the receive method would not return
//    until it receives 'nbytes' of data or a communication error.
//    For a non-block  mode socket, the receive method reads only
//    available bytes of data accumulated in the socket. 
//
//    For non-blocking mode socket, the errno 'EWOULDBLOCK' or
//    'EAGAIN' means that there is no data accumulated in the socket
//    input buffer. In this case, the method returns 0 value after
//    retries.
//--------------------------------------------------------------------- 
//@ PreCondition
// 		( buf && nbytes )
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
NetSocket::readData(void *buf, int nbytes)
{
int retry;
Int count;
int noBufferRetry;
Int totalBytes = 0;

	CLASS_PRE_CONDITION( buf && nbytes );

	if (!opened()) // $[TI1]
	{
		return(-1);
	} // else $[TI2]

	retry = DOWNLOAD_COMM_RETRY;
	noBufferRetry =  DOWNLOAD_COMM_RETRY * 2;
	while( totalBytes < nbytes ) // $[TI3]
	{
		while((count=receive_((char *)buf, nbytes))< 0) // $[TI3.1]
		{
			if ( errno == ENOBUFS || errno == EWOULDBLOCK || errno == EAGAIN ) // $[TI3.1.1]
			{
				Download::ReportError("NetSocket::readData():receive_()", errno);
			 	if ( --noBufferRetry < 0 ) // $[TI3.1.1.1]
			 	{
					return(-1);
				} // else $[TI3.1.1.2]
				continue;
			}
			else if ( --retry <  0 ) // $[TI3.1.2]
			{
				Download::ReportError(
					"NetSocket::readData():receive_() DOWNLOAD_COMM_RETRY", errno);
				return(-1);
			} // else $[TI3.1.3]
		} // $[TI3.2]
		totalBytes += count;
		buf = (char *)buf + count ;
		if ( totalBytes == nbytes ) // $[TI3.3]
		{
			return(nbytes);
		} // else $[TI3.4]
	} // no [TI] needed cause nbytes can't be 0
	return(nbytes);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  close(void)
//
//@ Interface-Description
//      close the socket.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Call the 'close' socket library call to close the socket.
//      While a socket is closed, socketId_ is set to -1.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
NetSocket::close(void)
{
	if (!opened()) // $[TI1]
	{
		return;
	} // else $[TI2]
	::close(socketId_);
	socketId_ = -1;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendData( char *buf, int nbytes)
//
//@ Interface-Description
//    This method is called to send 'nbytes' of data through this socket
//    connection.  All the network error is detected and handled within
//    this routine.  Retry mechanism is built into the routine to
//    guarantee all the requested data is sent to other board unless
//    communication errors are repeatedly detected. The method returns
//    number of bytes that it successfully transmitted. In the case of
//    repeated communication error, the method returns -1.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    If the receive operation fails due to the lack of TCP/IP 
//    IO buffer, the method waits 20 milliseconds before it tries
//    to read again. Retry count for the no system buffer is set 
//    twice as many as the normal retry count.
//    If there is no TCP/IP buffer, the method relinquishes
//    the processor by calling the Task::Delay method so other
//    tasks can empty out the TCP/IP data buffer.
//--------------------------------------------------------------------- 
//@ PreCondition
//    ( buf && nbytes)
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
NetSocket::sendData(char *buf, int nbytes)
{
Int count,  retry, noBufferRetry;

	CLASS_PRE_CONDITION(buf && nbytes);

	retry = DOWNLOAD_COMM_RETRY;
	noBufferRetry =  DOWNLOAD_COMM_RETRY * 2;
	if ( !opened() ) // $[TI1]
	{
		return(-1);
	} // else $[TI2]
	while((count=transmit_( buf, nbytes))<= 0) // $[TI3]
	{
		if ( errno == ENOBUFS || (errno == EWOULDBLOCK || errno == EAGAIN)) // $[TI3.1]
		{
			Download::ReportError( "NetSocket::sendData():transmit_() errno",  errno );
			if ( --noBufferRetry < 0 ) // $[TI3.1.1]
			{
			 	return(-1);
			}
			// else $[TI3.1.2]
#if	defined(SIGMA_TARGET)
			Download::DelayTask(20);
#endif //defined(SIGMA_TARGET);
		}
		else if ( --retry < 0 ) // $[TI3.2]
		{
			Download::ReportError( "NetSocket::sendData():transmit_() DOWNLOAD_COMM_RETRY",  errno );
			return(-1);
		} // else $[TI3.3]
		Download::ReportError( "NetSocket::sendData():transmit_() count less then 0",  errno );
	} // $[TI4]
	if ( count != nbytes ) // $[TI5]
	{
		Download::ReportError(
			"NetSocket::sendData():transmit_() wrong size transmitted", errno);
	} // else $[TI6]
	return(count);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: transmit_( char *buf, Int nbytes)
//
//@ Interface-Description
//    Transmit the data onto the given socket connection using the
//    TCP/IP socket interface.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    It calls the send socket library  routine to transmit the data.
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//@ End-Method 
//===================================================================== 
Int
NetSocket::transmit_( char *buf, Int nbytes)
{
	if ( !opened()) // $[TI1]
	{
		return -1;
	} // else $[TI2]
	return(::send( socketId_, buf, nbytes, 0) );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receive_( char *buf, Int nbytes)
//
//@ Interface-Description
//    Receive the data onto the given socket connection using the
//    TCP/IP socket interface.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    It calls the recv socket library  routine to receive the data.
//--------------------------------------------------------------------- 
//@ PreCondition
//		none
//@ End-Method 
//===================================================================== 
Int
NetSocket::receive_( char *buf, Int nbytes)
{
	if ( !opened()) // $[TI1]
	{
		return -1;
	} // else $[TI2]
	return( ::recv( socketId_, buf, nbytes, 0));
}

#ifdef SIGMA_SERVER
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getHostIpAddr( void )
//
//@ Interface-Description
//     Get an IP address of the local host.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//		The getsockname socket library routine should return
//      a socket address of local socket address.
//--------------------------------------------------------------------- 
//@ PreCondition
//		(opened())
//@ End-Method 
//===================================================================== 
Uint32
NetSocket::getHostIpAddr(void)
{
Saddr pAddr;
Int namelen = sizeof(pAddr.sa);
#if	defined(SIGMA_SUN)
struct utsname name;
register struct hostent  *hp;
#endif //defined(SIGMA_SUN)


	CLASS_PRE_CONDITION(opened());
#if	defined(SIGMA_SUN)
	uname(&name);

    hp = gethostbyname (name.nodename);
    if (hp != NULL)
    {
		bcopy ( hp->h_addr, &pAddr.in.sin_addr, hp->h_length);
	}
#else
	if (::getsockname(socketId_, &pAddr.sa, &namelen))
	{	
		Download::ReportError("NetSocket::getHostIpAddr():getsockname() failed in getting a Socket IP address",
			 errno);
		return(0);
	}
#endif //defined(SIGMA_SUN)
	return(pAddr.in.sin_addr.s_addr);
}
#endif // SIGMA_SERVER


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: callCreateSocket(Int protocol, PortId portId, Int numListen)
//
//@ Interface-Description
//		Provides access to the private method
//		NetSocket::createSocket_(Int protocol, PortId portId, Int numListen).
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int
NetSocket::callCreateSocket(Int protocol, PortId portId, Int numListen)
{
	return(createSocket_(protocol, portId, numListen));
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: callTransmit( char *buf, Int nbytes)
//
//@ Interface-Description
//		Provides access to the private method
//		NetSocket::transmit_(char *buf, Int nbytes).
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int
NetSocket::callTransmit( char *buf, Int nbytes)
{
	return(transmit_(buf, nbytes));
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receive_( char *buf, Int nbytes)
//
//@ Interface-Description
//		Provides access to the private method
//		NetSocket::receive_(char *buf, Int nbytes).
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int
NetSocket::callReceive( char *buf, Int nbytes)
{
	return(receive_(buf, nbytes));
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSocketIdOnly(Int socketId)
//
//@ Interface-Description
//    Set the SocketId_ to the specified value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
NetSocket::setSocketIdOnly( Int socketId )
{
	socketId_ = socketId;
}
#endif // SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
NetSocket::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
           const char*        pFileName,
           const char*        pPredicate)
{
      DownloadFault::SoftFault(softFaultID, DOWNLOAD,
        NETSOCKET_CLASS, lineNumber, pFileName, pPredicate);
}
