#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an processing retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//              Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//================== C L A S S   D E S C R I P T I O N ================
//@ Class: SmTestId - Defines master and subset test IDs and ID
//                    remapping functions for all Service Mode tests
//---------------------------------------------------------------------
//@ Interface-Description
//	This class defines test ID enums for the Service Mode tests as
//	master and subset ID's, and defines methods to convert between
//	the master and subset ID's (e.g. between master ID and EST ID).
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the Service Mode test ID enums and
//	methods for master and subset test ID conversion.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize() is used to create the test ID mapping tables.
//	6 static methods are defined to perform the various test ID conversions.
//	IsGuiTest() is used to determine if a master test ID corresponds
//	to a GUI-executed test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	This is a static class and may not be constructed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmTestId.ccv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: mnr    Date: 24-Mar-2010     DR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX SST related updates.
//
//  Revision: 008  By:  gdc    Date:  18-Feb-2009    SCR Number: 6480
//  Project:  840S
//  Description:
//		Implemented "set vent head operational hours" test.
//
//  Revision: 007  By: erm    Date: 22-Jan-2002     DR Number: 5984
//  Project: GuiComms
//  Description:
//   Added External LoopBack using SERIAL_LOOPBACK_TEST_ID
//
//  Revision: 006  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 005  By:  syw    Date: 27-Aug-1998    DR Number:
//       Project:  BiLevel
//       Description:
//          BiLevel Initial Release.  Added REMOTE_TEST_DK_COPY_HOURS_ID
//			to RemoteTestId_[].
//
//  Revision: 004  By:  gdc    Date: 23-Sep-1997    DR Number: DCS 2513
//       Project:  Sigma (R8027)
//       Description:
//            Added remote tests to support service/manufacturing tests.
//
//  Revision: 003  By:  quf    Date: 30-Jul-1997    DR Number: DCS 2279, 2205, 2013
//       Project:  Sigma (840)
//       Description:
//            Added new tests to test mapping arrays: flow sensor calibration,
//            exh valve velocity transducer, GUI nurse call.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  yyy    Date:  17-Jan-95    DR Number: none
//    Project:  Sigma (R8027)
//    Description:
//        Initial version
//
//====================================================================

#include  "SmTestId.hh"

//@ Code...
//
// Initialization of static data
//
SmTestId::ServiceModeTestId SmTestId::EstTestId_[EST_TEST_MAX];
SmTestId::ServiceModeTestId SmTestId::SstTestId_[SST_TEST_MAX];
SmTestId::ServiceModeTestId SmTestId::RemoteTestId_[REMOTE_TEST_MAX];

//====================================================================
//
//  Public Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  The SmTestId Class is never instantiated. All the data members
//  that are needed for the class are initialized in this method.
//	No arguments, no return value.
//----------------------------------------------------------------------
//@ Implementation-Description
//	Create three arrays which use EST, SST, and remote test ID's
//	as indexes and which store the corresponding Service Mode master
//	test ID's.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SmTestId::Initialize (void)
{
// $[TI1]
	CALL_TRACE("SmTestId::Initialize (void)");

	Int32 testIndex = 0;

	// Create the sst test ID mapping table
	SstTestId_[SST_TEST_CIRCUIT_RESISTANCE_ID]		= CIRCUIT_RESISTANCE_TEST_ID;
	testIndex++;
	SstTestId_[SST_TEST_EXPIRATORY_FILTER_ID]		= SST_FILTER_TEST_ID;
	testIndex++;
	SstTestId_[SST_TEST_CIRCUIT_LEAK_ID]			= SST_LEAK_TEST_ID;
	testIndex++;
	SstTestId_[SST_TEST_CIRCUIT_PRESSURE_ID]		= CIRCUIT_PRESSURE_TEST_ID;
	testIndex++;
	SstTestId_[SST_TEST_FLOW_SENSORS_ID]			= SST_FS_CC_TEST_ID;
	testIndex++;
	SstTestId_[SST_TEST_COMPLIANCE_CALIBRATION_ID]	= COMPLIANCE_CALIB_TEST_ID;
	testIndex++;
	SstTestId_[SST_TEST_PROX_ID]					= SM_SST_PROX_TEST_ID;
	testIndex++;


	SAFE_CLASS_ASSERTION (testIndex == SST_TEST_MAX);

	for(int i = 0; i < EST_TEST_MAX; i++)
		EstTestId_[i] = TEST_NOT_START_ID;

	testIndex = 0;

	// Create the est test ID mapping table
	EstTestId_[EST_TEST_GAS_SUPPLY_ID]				= GAS_SUPPLY_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_GUI_KEYBOARD_ID]			= GUI_KEYBOARD_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_GUI_KNOB_ID]				= GUI_KNOB_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_GUI_LAMP_ID]				= GUI_LAMP_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_BD_LAMP_ID]					= BD_LAMP_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_GUI_AUDIO_ID]				= GUI_AUDIO_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_BD_AUDIO_ID]				= BD_AUDIO_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_FS_CROSS_CHECK_ID]			= FS_CROSS_CHECK_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_CIRCUIT_PRESSURE_ID]		= CIRCUIT_PRESSURE_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_SAFETY_SYSTEM_ID]			= SAFETY_SYSTEM_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_EXH_VALVE_SEAL_ID]			= EXH_VALVE_SEAL_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_EXH_VALVE_ID]				= EXH_VALVE_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_SM_LEAK_ID]					= SM_LEAK_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_EXH_HEATER_ID]				= EXH_HEATER_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_GENERAL_ELECTRONICS_ID]		= GENERAL_ELECTRONICS_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_GUI_TOUCH_ID]				= GUI_TOUCH_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_GUI_SERIAL_PORT_ID]			= GUI_SERIAL_PORT_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_BATTERY_ID]					= BATTERY_TEST_ID;
	testIndex++;
	EstTestId_[EST_TEST_GUI_NURSE_CALL_ID]			= GUI_NURSE_CALL_TEST_ID;
	testIndex++;

	for(int i = 0; i < REMOTE_TEST_MAX; i++)
		RemoteTestId_[i] = TEST_NOT_START_ID;

	testIndex = 0;

	// Create the remote test ID mapping table
	RemoteTestId_[REMOTE_TEST_GAS_SUPPLY_ID]			= GAS_SUPPLY_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_GUI_KEYBOARD_ID]			= GUI_KEYBOARD_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_GUI_KNOB_ID]				= GUI_KNOB_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_GUI_LAMP_ID]				= GUI_LAMP_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_BD_LAMP_ID]				= BD_LAMP_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_GUI_AUDIO_ID]				= GUI_AUDIO_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_BD_AUDIO_ID]				= BD_AUDIO_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_LOW_FLOW_FS_CC_ID]		= LOW_FLOW_FS_CC_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_HIGH_FLOW_FS_CC_ID]		= HIGH_FLOW_FS_CC_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_CIRCUIT_PRESSURE_ID]		= CIRCUIT_PRESSURE_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_SAFETY_SYSTEM_ID]			= SAFETY_SYSTEM_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_EXH_VALVE_SEAL_ID]		= EXH_VALVE_SEAL_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_EXH_VALVE_ID]				= EXH_VALVE_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_EXH_VALVE_CALIB_ID]		= EXH_VALVE_CALIB_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_SM_LEAK_ID]				= SM_LEAK_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_EXH_HEATER_ID]			= EXH_HEATER_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_GENERAL_ELECTRONICS_ID]	= GENERAL_ELECTRONICS_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_GUI_TOUCH_ID]				= GUI_TOUCH_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_GUI_SERIAL_PORT_ID]		= GUI_SERIAL_PORT_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_BATTERY_ID]				= BATTERY_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_FS_CALIBRATION_ID]		= FS_CALIBRATION_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_GUI_NURSE_CALL_ID]		= GUI_NURSE_CALL_TEST_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_ATMOS_PRESSURE_READ_ID]	= ATMOS_PRESSURE_READ_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_SET_COMPRESSOR_SN_ID]		= SET_COMPRESSOR_SN_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_SET_COMPRESSOR_TIME_ID]	= SET_COMPRESSOR_TIME_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_SET_DATAKEY_TO_VENT_DATA_ID]	= SET_DATAKEY_TO_VENT_DATA_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_SET_AIR_FLOW_ID]			= SET_AIR_FLOW_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_SET_O2_FLOW_ID]			= SET_O2_FLOW_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_PURGE_WITH_AIR_ID]		= PURGE_WITH_AIR_ID;
	testIndex++;
	RemoteTestId_[REMOTE_TEST_PURGE_WITH_O2_ID]			= PURGE_WITH_O2_ID;
	testIndex++;

	RemoteTestId_[REMOTE_TEST_SET_VENT_HEAD_TIME_ID]	= SET_VENT_HEAD_TIME_ID;
	testIndex++;
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  GetEstIdFromSmId()  [static]
//
//@ Interface-Description
//
//	Pass in a Service Mode Test Id, return the corresponding
//	EST test Id.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Search the EstTestId_ array to find a match for the passed-in smId.  
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  idx < EST_TEST_MAX
//@ End-Method
//=====================================================================

SmTestId::EstTestId
SmTestId::GetEstIdFromSmId (SmTestId::ServiceModeTestId smId)
{
	CALL_TRACE("SmTestId::GetEstIdFromSmId (SmTestId)");

	Int32 idx;
	for ( idx = 0;
		  idx < EST_TEST_MAX && EstTestId_[idx] != smId;
		  idx++ )
	{
	// $[TI1]
	}

	SAFE_CLASS_ASSERTION (idx < EST_TEST_MAX
		&& EstTestId_[idx] != TEST_NOT_START_ID);

	return ((SmTestId::EstTestId) idx);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  GetSstIdFromSmId()  [static]
//
//@ Interface-Description
//
//	Pass in a Service Mode Test Id, return the corresponding
//	SST test Id.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Search the SstTestId_ array to find a match for the passed-in smId.  
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  idx < SST_TEST_MAX
//@ End-Method
//=====================================================================

SmTestId::SstTestId
SmTestId::GetSstIdFromSmId (SmTestId::ServiceModeTestId smId)
{
	CALL_TRACE("SmTestId::GetSstIdFromSmId (SmTestId)");

	Int32 idx;
	for ( idx = 0;
		  idx < SST_TEST_MAX && SstTestId_[idx] != smId;
		  idx++ )
	{
	// $[TI1]
	}

	SAFE_CLASS_ASSERTION (idx < SST_TEST_MAX);
	
	return ((SmTestId::SstTestId) idx);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  GetRemoteIdFromSmId()  [static]
//
//@ Interface-Description
//
//	Pass in a Service Mode Test Id, return the corresponding
//	remote test Id.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Search the RemoteTestId_ array to find a match for the passed-in smId.  
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  idx < REMOTE_TEST_MAX
//@ End-Method
//=====================================================================

SmTestId::RemoteTestId
SmTestId::GetRemoteIdFromSmId (SmTestId::ServiceModeTestId smId)
{
	CALL_TRACE("SmTestId::GetRemoteIdFromSmId (SmTestId::ServiceModeTestId smId)");

	Int32 idx;
	for ( idx = 0;
		  idx < REMOTE_TEST_MAX && RemoteTestId_[idx] != smId;
		  idx++ )
	{
	// $[TI1]
	}

	SAFE_CLASS_ASSERTION (idx < REMOTE_TEST_MAX
		&& RemoteTestId_[idx] != TEST_NO_START_ID);
	
	return ((SmTestId::RemoteTestId) idx);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  GetSmIdFromEstId()  [static]
//
//@ Interface-Description
//
//	Pass in an EST test ID, return the corresponding
//	Service Mode Test ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Index into the EstTestId_ array using the EST test ID to get the
//	Service Mode Test ID.
//---------------------------------------------------------------------
//@ PreCondition
//  estId < EST_TEST_MAX
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SmTestId::ServiceModeTestId
SmTestId::GetSmIdFromEstId (EstTestId estId)
{
// $[TI1]
	CALL_TRACE("SmTestId::GetSmIdFromEstId (EstTestId estId)");

	CLASS_PRE_CONDITION (estId < EST_TEST_MAX
		&& EstTestId_[estId] != TEST_NOT_START_ID);

	return (EstTestId_[estId]);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  GetSmIdFromSstId()  [static]
//
//@ Interface-Description
//
//	Pass in an SST test ID, return the corresponding
//	Service Mode Test ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Index into the SstTestId_ array using the SST test ID to get the
//	Service Mode Test ID.
//---------------------------------------------------------------------
//@ PreCondition
//  sstId < SST_TEST_MAX 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SmTestId::ServiceModeTestId
SmTestId::GetSmIdFromSstId (SstTestId sstId)
{
// $[TI1]
	CALL_TRACE("SmTestId::GetSmIdFromSstId (SstTestId sstId)");

	CLASS_PRE_CONDITION (sstId < SST_TEST_MAX);

	return (SstTestId_[sstId]);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  GetSmIdFromRemoteId()  [static]
//
//@ Interface-Description
//
//	Pass in a remote test ID, return the corresponding
//	Service Mode Test ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//	Index into the RemoteTestId_ array using the remote test ID to get
//	the Service Mode Test ID.
//---------------------------------------------------------------------
//@ PreCondition
//  remoteId < REMOTE_TEST_MAX 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SmTestId::ServiceModeTestId
SmTestId::GetSmIdFromRemoteId (RemoteTestId remoteId)
{
// $[TI1]
	CALL_TRACE("SmTestId::GetSmIdFromRemoteId (RemoteTestId remoteId)");

	CLASS_PRE_CONDITION (remoteId < REMOTE_TEST_MAX
		&& RemoteTestId_[remoteId] != TEST_NOT_START_ID);

	return (RemoteTestId_[remoteId]);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  IsGuiTest()  [static]
//
//@ Interface-Description
//
//	Determine if a test is a GUI test or not.  Pass in a Service Mode
//	Test ID and return TRUE if a GUI test, FALSE if not.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Compare the Service Mode Test ID to those of known GUI tests.
//  Return TRUE if the test is a GUI test, else return FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//  smId > TEST_NOT_START_ID && smId < NUM_SERVICE_TEST_ID
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
SmTestId::IsGuiTest (ServiceModeTestId smId)
{
	CALL_TRACE("SmTestId::IsGuiTest (ServiceModeTestId smId)");

	CLASS_PRE_CONDITION (
			smId > TEST_NOT_START_ID && smId < NUM_SERVICE_TEST_ID);

	Boolean isGuiTest;
	switch (smId)
	{
		case GUI_KEYBOARD_TEST_ID:
		case GUI_KNOB_TEST_ID:
		case GUI_LAMP_TEST_ID:
		case GUI_AUDIO_TEST_ID:
		case GUI_TOUCH_TEST_ID:
		case GUI_SERIAL_PORT_TEST_ID:
		case SERIAL_LOOPBACK_TEST_ID:
		case GUI_INIT_SERIAL_NUMBER_ID:
		case CAL_INFO_DUPLICATION_ID:
		case GUI_INIT_DEFAULT_SERIAL_NUMBER_ID:
		case GUI_NURSE_CALL_TEST_ID:
		// $[TI1]
			isGuiTest = TRUE;
			break;

		default:
		// $[TI2]
			isGuiTest = FALSE;
			break;
	}
		
	return (isGuiTest);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                      [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SmTestId::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_MODE,
		        SMTESTID, lineNumber,pFileName, pPredicate);
}
