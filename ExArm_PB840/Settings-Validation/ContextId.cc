#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  ContextId - Ids for the Contexts.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class encapsulates the enumerated ids for each of the contexts.
//  There is one static method to go along with the ids; this method
//  provides a convenient means of validating the correctness of an id
//  value against the actual id values.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a central repository for the context ids.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An extra enumerator is placed as the last item of the enumeration, 
//  which will always contain the total number of context ids available.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ContextId.ccv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ContextId.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
// Class-Method:  GetContextName(contextId)  [extern]
//
// Interface-Description
//  Return a character string representing the name of the context
//  given by 'contextId'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  (ContextId::IsId(contextId))
//---------------------------------------------------------------------
// PostCondition
//  (ContextId::GetContextName(contextId) != NULL)
// End-Free-Function
//=====================================================================

const char*
ContextId::GetContextName(const ContextId::ContextIdType contextId)
{
  CALL_TRACE("GetContextName(contextId)");

  const char*  pContextName = NULL;

  switch (contextId)
  {
#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
  case ContextId::ACCEPTED_CONTEXT_ID :
    pContextName = "Accepted Context";
    break;
  case ContextId::ADJUSTED_CONTEXT_ID :
    pContextName = "Adjusted Context";
    break;
  case ContextId::RECOVERABLE_CONTEXT_ID :
    pContextName = "Recoverable Context";
    break;
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
  case ContextId::PENDING_CONTEXT_ID :
    pContextName = "Pending Context";
    break;
  case ContextId::PHASED_IN_CONTEXT_ID :
    pContextName = "Phased-In Context";
    break;
#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
  case ContextId::TOTAL_CONTEXT_IDS :
  default:
    CLASS_ASSERTION_FAILURE();
    break;
  };

  return(pContextName);
}

#endif  // defined(SIGMA_DEVELOPMENT)


#if defined(SIGMA_DEVELOPMENT)

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextId::SoftFault(const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
                     const char*        pFileName,
                     const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, CONTEXT_ID_CLASS,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
