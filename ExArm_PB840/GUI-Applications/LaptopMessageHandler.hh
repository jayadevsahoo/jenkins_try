#ifndef LaptopMessageHandler_HH
#define LaptopMessageHandler_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: LaptopMessageHandler - 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LaptopMessageHandler.hhv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: gdc    Date: 26-May-2007   DCS Number:  6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 003  By: dosman    Date: 26-Feb-1999   DR Number: 5322
//  Project:  ATC
//  Description:
//	Implemented changes for ATC.
//	Updated code that did not meet our standards 
//	(public member functions should not end in underscore).
//	Added new method to send software options to remote PC.
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "LaptopEventId.hh"
#include "SerialInterface.hh"
#include "GuiAppClassIds.hh"

 
class LaptopMessageHandler
{
public:
	enum 
	{
		MAX_STRING_SIZE=256,
		MAX_BUFF_SIZE=516
	};


	static void ClearBuffer(void);


	// Methods to setup the outgoing buffer.
	static void SetMessageNo(void);
	static void SetCommand(Int commandId, Int commandValue=-1);
	static void SetSwRevisionCommand(const char *guiRevision, const char *bdRevision);
	static void SetSerialNumberCommand(const char *guiKernel, const char *bdKernel,
										   const char *guiSN, const char *bdSN,
										   const char *compressorSN);
	static void SetSwOptionsCommand(const char *ventilatorSoftwareOptionsString);
	static void SetRunTimeCommand(Real32 compressorValue, Real32 systemValue);
	static void SetDataCommand(Int dataIndex, Real32 dataValue);
	static void SetLogCommand(Int commandId, char *logMsg=NULL);

	// Methods to retrieve the index of the outgoing buffer.
	static Int16 GetBufferIndex(void);
	static char *GetBuffer(void);

	// Mothod to inform Serial laptop of the output availability.
	static void SerialOutputEventHappened(
						SerialInterface::SerialCommandCodes serialCommandCodes,
						char *msgBuffer, Uint16 length);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	LaptopMessageHandler(void);							// not implemented...
	LaptopMessageHandler(const LaptopMessageHandler&);	// not implemented...
	~LaptopMessageHandler(void);						// not implemented...
	void operator=(const LaptopMessageHandler&);		// not implemented...


	//@ Data-Member: OutgoingBuffer_
    // The string which holds the formatted laptop message.
	static char OutgoingBuffer_[LaptopMessageHandler::MAX_BUFF_SIZE];

	//@ Data-Member: BufferIndex_
    // The index which points to the next availale location in the OutgoingBuffer_[].
	static Uint16 BufferIndex_;

	//@ Data-Member: MessageNo_
    // The message number automatically incremented to sequence the message.
	static Uint8 MessageNo_;

};


#endif // LaptopMessageHandler_HH 
