#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: UiEvent - implements the handling of events originated by the UI.
//---------------------------------------------------------------------
//@ Interface-Description
// Examples for events that are originated by the UI are manual inspiration,
// 100% o2, alarm reset, expiratory pause, and service request. When an event
// occurs, it triggers a call back. The class provides the call back mechanism
// for these events.  At construction time, a data member of this class is set
// to point to the called back method. Each event is instantiated as a
// UiEvent object.  Access methods are implemented to get the event's id and
// the event's status.  A method to set the event status and post the event
// status to the BdStatusTask which then sends the status to the GUI is 
// implemented as well.
//---------------------------------------------------------------------
//@ Rationale
// This class defines how each user event is serviced by the BD sub-system.
//---------------------------------------------------------------------
//@ Implementation-Description
// The static method RequestUserEvent() is invoked every time a USER_EVENT_REQUEST_MSG
// is received by NetworkApp from the GUI.  The method updates the
// user event requested status. 
// Each object contains a data member to store its id and its status.
// A typedef of a pointer to a function that returns the event status and takes
// event id and a Boolean is declared. The Boolean argument indicates whether
// the request is to activate the event or to terminate it. A data member
// is declared with that typedef type.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/UiEvent.ccv   10.7   08/17/07 09:44:52   pvcs  
//
//@ Modification-Log
//
//  Revision: 009   By: rhj    Date: 06-May-2020    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added MANUAL_PURGE event in the NewCycle()
//
//  Revision: 008   By: rpr    Date: 29-Jan-2009    SCR Number: 6463
//  Project:  840S
//  Description:
//      During CPU assertions cancel O2 manuever if present.
// 
//  Revision: 007   By: rpr    Date: 23-Jan-2009    SCR Number: 6435  
//  Project:  840S
//  Description:
//      Modified for code review comments.
// 
//  Revision: 006   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 005   By: gdc   Date:  23-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Refactored for processing of RM maneuver requests.
//
//  Revision: 004   By: syw   Date:  25-Jul-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		resync PavManager class alarms during ALARM_RESET
//
//  Revision: 005   By: yyy    Date:  10-Aug-1999    DCS Number: 5504
//  Project: 840 Neonatal
//  Description:
//	Added cancel capability for percent O2 event.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  BiLevel (R8027)
//       Description:
//             Bilevel initial version.
//             Added inspiratory pause event.
//
//  Revision: 002  By:  syw    Date:  19-Aug-1997    DR Number: DCS 1656
//       Project:  Sigma (840)
//       Description:
//             Capitalize all global references per coding standard.
//
//  Revision: 001  By:  iv    Date:  27-Feb-1997    DR Number: DCS 1408, 1638
//       Project:  Sigma (840)
//       Description:
//             Initial version - per Breath Delivery formal code review
//             and unit test fixes. 
//
//=====================================================================

#include "UiEvent.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes
#include "BdAlarms.hh"
#include "Fio2Monitor.hh"
#include "VentAndUserEventStatus.hh"
#include "VentStatus.hh"
#include "BdSystemStateHandler.hh"
#include "Maneuver.hh"
#include "PavManager.hh"
#include "O2Mixture.hh"
#include "Post.hh"
#include "NovRamManager.hh"
#include "BreathMiscRefs.hh"
#include "FlowController.hh"
#include "TaskControlAgent.hh"
//@ End-Usage

//@ Code...
//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UiEvent
//
//@ Interface-Description
// The constructor takes a function pointer and the event id as arguments.
// The constructor is responsible to set the call back method for the user
// event at construction time.
//---------------------------------------------------------------------
//@ Implementation-Description
// The user event id, the call back function pointer and the event status
// are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UiEvent::UiEvent(const UserEventCallbackPtr pEventCallBack,
                     const EventData::EventId id)
{
// $[TI1]
	CALL_TRACE("UiEvent::UiEvent(const UserEventCallbackPtr pEventCallBack, \
	            const EventData::EventId id)");

	pUserEventCallBack_ = pEventCallBack;
	id_ = id;

	// For 100% O2, eventStatus_ is overwritten in Breath_Delivery::Initialize()
	// based on the status saved in NOVRAM
	eventStatus_ = EventData::IDLE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~UiEvent()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UiEvent::~UiEvent(void)
{
  CALL_TRACE("~UiEvent()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveManeuverRequest
//
//@ Interface-Description
//  This UiEvent method is called to process maneuver events from the GUI.
//  It accepts a user event state request as an argument. It returns
//  nothing. This method sends the user event request to the Maneuver
//  class for processing by the current active maneuver. If the current
//  maneuver approves continued processing of the maneuver request then 
//  the registered callback for this UiEvent is called to start or stop 
//  the maneuver and post its processing results to the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//  receiveRequest() and receiveManeuverRequest() should maybe be
//  implemented as the same virtual function in two different
//  (eg. UiEvent and UiManeuverEvent) but there would be so little 
//  difference between the two that it doesn't justify a new class. 
//  Therefore, we have implemented two different functions in one class 
//  rather than one virtual function in two classes.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
UiEvent::receiveManeuverRequest(EventData::RequestedState request)
{
	// process new event against any active maneuver
	if ( Maneuver::ProcessUserEvent(*this, request) )
	{
		// returns TRUE to continue processing new event
		// process callback and post status to GUI
		setEventStatus(pUserEventCallBack_(id_, (request == EventData::START)));
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveRequest
//
//@ Interface-Description
//  This UiEvent method is called to process non-maneuver events from the GUI.
//  It accepts a user event state request as an argument. It returns
//  nothing. The registered callback for this UiEvent is called to 
//  start or stop the user event and post its processing status to 
//  the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//  receiveRequest() and receiveManeuverRequest() should maybe be
//  implemented as the same virtual function in two different
//  (eg. UiEvent and UiManeuverEvent) but there would be so little 
//  difference between the two that it doesn't justify a new class. 
//  Therefore, we have implemented two different functions in one class 
//  rather than one virtual function in two classes.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void 
UiEvent::receiveRequest(EventData::RequestedState request)
{
	// process callback and set the event status and communicate status to GUI
	setEventStatus(pUserEventCallBack_(id_, (request == EventData::START)));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setEventStatus
//
//@ Interface-Description
// This method takes a user event status as an argument and allows the client
// to change the data member for the event status. This method
// invokes VentAndUserEventStatus::PostEventStatus() to post the event
// id and status to the BdStatusTask so that it can be communicated to
// the GUI.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
// The status passed to this method and the member id_ are posted to
// the BdStatusTask via the VentAndUserEventStatus::PostEventStatus()
// method.  The data member eventStatus_ is assigned the value
// of the parameter passed in if it is ACTIVE or PENDING.  Otherwise,
// the status is set to IDLE.  
//---------------------------------------------------------------------
//@ PreCondition
//    The user event status is checked to be in the following range:
//    { REJECTED || COMPLETE || CANCEL || IDLE || ACTIVE || PENDING}
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UiEvent::setEventStatus(const EventData::EventStatus status,
						const EventData::EventPrompt prompt)
{
    CALL_TRACE("UiEvent::setEventStatus(const EventData::EventStatus status, const EventData::EventPrompt prompt)");

	if (status != eventStatus_  || prompt != EventData::NULL_EVENT_PROMPT ||
		(id_ == EventData::PERCENT_O2) || (id_ == EventData::CALIBRATE_O2))
	{

		// $[TI1]
		// If the status is REJECTED, COMPLETE or CANCEL, set the status back to
		// IDLE; otherwise, set it to the passed status
		switch (status)
		{
		case EventData::REJECTED:
		case EventData::COMPLETE:
		case EventData::CANCEL:
		case EventData::IDLE:
			eventStatus_ = EventData::IDLE;
			VentAndUserEventStatus::PostEventStatus(id_, status, prompt);
			break;
		case EventData::ACTIVE:
		case EventData::PENDING:
			// $[TI1.2]
			eventStatus_ = status;
			VentAndUserEventStatus::PostEventStatus(id_, status, prompt);
			break;
		case EventData::PLATEAU_ACTIVE:
			VentAndUserEventStatus::PostEventStatus(id_, status, prompt);
			break;
		case EventData::ACTIVE_PENDING:
			eventStatus_ = EventData::PENDING;
			VentAndUserEventStatus::PostEventStatus(id_, status, prompt);
			break;
		case EventData::AUDIO_ACK:
			VentAndUserEventStatus::PostEventStatus(id_, status, prompt);
			break;
		case EventData::NO_ACTION_ACK:
			// do nothing
			break;
		default:
			AUX_CLASS_ASSERTION_FAILURE( status );
			break;
		}
	}
	// $[TI2]
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//=====================================================================
//
//  Static Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [static]
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.
//  This static method initializes the static data members and
//	registers the RequestUserEvent method with Network Apps so
//	that RequestUserEvent is invoked when a user event is requested
//	from the UI.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method invokes NetworkApp::RegisterRecvMsg() to register
//	RequestUserEvent as the callback method when USER_EVENT_REQUEST_MSG
//	messages are received.  If during a powerfailure and O2 was requested or
//  O2 calibration was taking place the manuever is reinitiated. All other 
//  reasons if the O2 manuever was going on it is just canceled .  (ie. like an
//  assertion)
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
UiEvent::Initialize(void)
{
    CALL_TRACE("UiEvent::Initialize(void)");

    // Register the UiEvent static method with Network Apps so that
    // all user event requests are sent to the UiEvent class
    NetworkApp::RegisterRecvMsg (USER_EVENT_REQUEST_MSG, UiEvent::RequestUserEvent);

    GetUiEventBuffer_().reset();

	const Boolean isO2RequestActive = BdSystemStateHandler::Is100PercentO2Requested();
	const Boolean isCalRequestActive = BdSystemStateHandler::IsCalibrateO2Requested();

	// Check if O2 request was active:
	if (isO2RequestActive || isCalRequestActive)
	{
		EventData msg;
		// $[TI1]
		// If a power fail occurs and O2 was active, restart the O2 Manuever
		// otherwise just cancel it.
		if (Post::GetStartupState() == POWERFAIL)
		{
			msg.guiToBdUserEvent.request = EventData::START;
		}
		else
		{
			msg.guiToBdUserEvent.request = EventData::STOP;
		}

		if (isCalRequestActive)
		{
			msg.guiToBdUserEvent.id = EventData::CALIBRATE_O2;
			msg.guiToBdUserEvent.moredata = EventData::HUNDREDPERCENT;
		}
		else
		{
			msg.guiToBdUserEvent.id = EventData::PERCENT_O2;
			msg.guiToBdUserEvent.moredata = EventData::NO_O2_DATA;
		}

		GetUiEventBuffer_().addMsg(msg);
	}

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RequestUserEvent [static]
//
//@ Interface-Description
//  This method is invoked by the communication link when a gui event
//  request is recognized.  The method takes the message Id, a pointer to the
//  message data and the message size as inputs.  It has no return value.
//  The message Id and size are not used but are required to conform to the
//  prototype defined in NetworkApp for the callback method.
//  The message data contains the EventId and the RequestedState.
//  This method takes that information out of the message and transfers
//  it to the UiEventBuffer object.  The NewCycle() method checks the
//  the UiEventBuffer object every BD Cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method casts the message data into EventData; it then
//  adds a message to the UiEventBuffer object.
//  This design depends on BD being higher priority than NetApp since
//  it is undesirable for this method to interrupt and run while
//  UiEvent::newCycle() is executing.
//---------------------------------------------------------------------
//@ PreCondition
//  pData != NULL && msgId == USER_EVENT_REQUEST_MSG
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
UiEvent::RequestUserEvent(XmitDataMsgId msgId, void *pData, Uint size)
{
    CALL_TRACE("UiEvent::RequestUserEvent(XmitDataMsgId msgId, void *pData, Uint size)");
    CLASS_PRE_CONDITION (pData != NULL && msgId == USER_EVENT_REQUEST_MSG);
    
    GetUiEventBuffer_().addMsg(*(EventData*)pData );
}	


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessEvent [static]
//
//@ Interface-Description
//  This static method takes an EventId and a Boolean that signifies the
//  event status as arguments and returns a EventStatus.
//  The method handles a gui fault event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls to VentStatus class to turn on the audible alarm and the loss
//  of user interface led are made.
//---------------------------------------------------------------------
//@ PreCondition
//  EventData::GUI_FAULT == id
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

EventData::EventStatus
UiEvent::ProcessEvent(const EventData::EventId id, const Boolean eventStatus)
{
	CALL_TRACE("UiEvent::ProcessEvent(EventData::EventId id, Boolean eventStatus)");
	UNUSED_SYMBOL(eventStatus);	

	AUX_CLASS_PRE_CONDITION (id == EventData::GUI_FAULT, id);
	VentStatus::LatchVentStatusService(VentStatus::BD_AUDIO_ALARM,
					  VentStatus::ACTIVATE);
	VentStatus::LatchVentStatusService(VentStatus::LOSS_OF_GUI_LED,
					  VentStatus::ACTIVATE);
	return (EventData::COMPLETE);
}	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NewCycle [static]
//
//@ Interface-Description
//    This method takes no arguments and returns nothing.  It is 
//    invoked every new cycle.  It reads the current message from the 
//    user event buffer and switches on the requested event ID to the
//    appropriate event process.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This methods checks the current message in the UiEventBuffer object.
//    if the message is not empty, a switch is done on the eventId.
//    The appropriate UiEvent object is called to process the user
//    event.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
UiEvent::NewCycle(void)    
{
	CALL_TRACE("UiEvent::NewCycle(void)");

	EventData eventData;

	if ( GetUiEventBuffer_().readCurrentMsg(eventData) == FALSE )
	{
		EventData::RequestedState request = eventData.guiToBdUserEvent.request;

		switch (eventData.guiToBdUserEvent.id)
		{
		case EventData::EXPIRATORY_PAUSE:
			RExpiratoryPauseEvent.receiveManeuverRequest(request);
			break;

		case EventData::INSPIRATORY_PAUSE:
			RInspiratoryPauseEvent.receiveManeuverRequest(request);
			break;

		case EventData::NIF_MANEUVER:
			RNifManeuverEvent.receiveManeuverRequest(request);
			break;

		case EventData::P100_MANEUVER:
			RP100ManeuverEvent.receiveManeuverRequest(request);
			break;

		case EventData::VITAL_CAPACITY_MANEUVER:
			RVitalCapacityManeuverEvent.receiveManeuverRequest(request);
			break;

		case EventData::CLEAR_KEY:
			Maneuver::TerminateManeuver();
			break;

		case EventData::MANUAL_INSPIRATION:
			RManualInspEvent.receiveManeuverRequest(request);
			break;

		case EventData::ALARM_RESET:
			// Notify the BdAlarms and Fio2Monitor objects
			RBdAlarms.resetAlarms();
			Fio2Monitor::SetAlarmResetStatus();
			RPavManager.resyncAlarms() ;
			RAlarmResetEvent.receiveRequest(request);
			break;

		case EventData::GUI_FAULT:
			RGuiFaultEvent.receiveRequest(request);
			break;

		case EventData::PERCENT_O2:
		case EventData::CALIBRATE_O2:
			// if not NO_DATA then neonatal ciruit in use
			// otherwise adult ciruit in use treat O2 as 100%
			if ((eventData.guiToBdUserEvent.moredata != EventData::NO_O2_DATA) || 
				(eventData.guiToBdUserEvent.id == EventData::CALIBRATE_O2))
			{
				O2Mixture::SetNeocalO2SensorRequested(TRUE);
				RCalO2Request.receiveRequest(request);
			}
			else
			{
				O2Mixture::SetNeocalO2SensorRequested(FALSE);
				RO2Request.receiveRequest(request);
			}
			break;

		case EventData::SST_CONFIRMATION:
			RServiceRequest.receiveRequest(request);
			break;

		case EventData::SST_EXIT_TO_ONLINE:
			RSstExitToOnlineEvent.receiveRequest(request);	
			break;
		case EventData::MANUAL_PURGE:
			RPurgeRequest.receiveRequest(request);
			break;
		default:
			AUX_CLASS_ASSERTION_FAILURE(eventData.guiToBdUserEvent.id);
		}        

	}  // end if $[TI2]
}    

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetUiEventBuffer_
//
//@ Interface-Description
// The method takes no arguments and returns a reference to a UiEventBuffer.
//---------------------------------------------------------------------
//@ Implementation-Description
// A UiEventBuffer is constructed and a its reference is returned
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UiEventBuffer&
UiEvent::GetUiEventBuffer_(void)
{
// $[TI1]
	CALL_TRACE("UiEvent::GetUiEventBuffer(void)");
    static UiEventBuffer UiBuffer;
	return (UiBuffer);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SstToOnlineEvent [static]
//
//@ Interface-Description
//  This static method takes an EventId and a Boolean that signifies the
//  event status as arguments and returns a EventStatus.
//  The method handles a Normal Mode GUI SST Exit to Online event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls to Task control agent to exit SST state and go back to Online
//  state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

EventData::EventStatus
UiEvent::SstToOnlineEvent(const EventData::EventId id, const Boolean eventStatus)
{
	RETAILMSG( TRUE, (L"Received a transition from SST to Online request\r\n") );
	CALL_TRACE("UiEvent::SstToOnlineEvent(EventData::EventId id, Boolean eventStatus)");

	//Update to the current compliance and resistance values measured during SST
	NovRamManager::GetCircuitComplianceTable(RCircuitCompliance);

	// TODO E600 Do we need to get these?
	//NovRamManager::GetInspResistance(RInspCircuitResistance);
	//NovRamManager::GetExhResistance(RExhCircuitResistance);

	//Update to PSOL calibration measured during SST
	//PSOL Liftoff is updated during SST FS Cross Check Test
	FlowController::updatePsolCalibration();

	//Restore Normal mode alpha for filtered values
	RO2FlowSensor.setAlpha(ALPHA);
	RAirFlowSensor.setAlpha(ALPHA);
	RExhFlowSensor.setAlpha(ALPHA);
	RExhFlowSensor.setUnoffsettedProcessedValuesAlpha(ALPHA);
	//RInspPressureSensor.setAlpha(ALPHA);
	//RExhPressureSensor.setAlpha(ALPHA);


	TaskControlAgent::ChangeState(STATE_ONLINE);
	return (EventData::COMPLETE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
UiEvent::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, UIEVENT,
                          lineNumber, pFileName, pPredicate);
}

