#ifndef WaveformPlot_HH
#define WaveformPlot_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: WaveformPlot - A waveform plotting base class that handles drawing
// of the static elements of all plots (grid, x and y axis labels etc) and
// provides a common interface for drawing of the actual plot.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformPlot.hhv   25.0.4.0   19 Nov 2013 14:08:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: rhj    Date:  20-July-2010    SCR Number: 6593
//  Project:  PROX
//  Description:
//     Added a generic message.
//
//  Revision: 006  By:  gdc	   Date:  25-Jan-2007    SCR Number: 6237
//  Project:  TREND
//  Description:
//      Trend project related changes. Changed to a LargeContainer from
//      old Container class.
//
//  Revision: 005   By: rhj    Date:  05-Jun-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Modified for Flow vs Volume Loop
// 
//  Revision: 004   By: sah    Date:  11-Jul-2000    DCS Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  changed constructor to take a boolean as to whether this plot is
//         an upper plot (plot #1) or a lower plot (plot #2)
//      *  added the shadow plot line (for histogram), along with the shadow
//         label box, as part of the new shadow trace functionality
//
//  Revision: 003   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include <math.h>
#include "Container.hh"
#include "LargeContainer.hh"
#include "Line.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
#include "PatientDataId.hh"
#include "TouchableText.hh"
//@ End-Usage

class WaveformIntervalIter;

class WaveformPlot : public LargeContainer
{
public:
	WaveformPlot(const Boolean isUpperPlot = TRUE);
	virtual ~WaveformPlot(void);

	virtual void activate(void);
	virtual void update(WaveformIntervalIter& segmentIter) = 0;
	virtual void endPlot(void) = 0;
	virtual void erase(void);

	virtual void setXLimits(Int32 upperXLimit, Int32 lowerXLimit, Uint32 xDivision,
									Boolean displayXValues = TRUE);
	virtual void setYLimits(Int32 upperYLimit, Int32 lowerYLimit, Uint32 yDivision,
									Boolean displayPositiveYValues = FALSE);

	virtual void setGridArea(Uint32 x, Uint32 y, Uint32 width, Uint32 height);

	virtual void setYAxisValue(Real32 yAxisValue);

	virtual void setXUnitsString(StringId xUnitsStrings, Int32 x, Int32 y);
	virtual void setYUnitsString(StringId yUnitsStrings, Int32 x, Int32 y);

	static void SoftFault(const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
							const char*       pPredicate = NULL);
    void displayMessage(StringId msg, Boolean showMessage);

protected:
	virtual void layoutGrid_(void);
	inline Int32 xValueToPoint_(Real32 xValue);
	inline Int32 yValueToPoint_(Real32 yValue);
	inline Real32 getYAxisValue_(void);

	//@ Data-Member: plotContainer_
	// Plot container which contains the grid lines and in which the waveform
	// will be plotted.
	LargeContainer plotContainer_;

	//@ Data-Member: plotLine_
	// The line used to draw each plot segment in the plotContainer_
	Line plotLine_;

	//@ Data-Member: currAuxYUnits_
	// The auxillary Y units patient data id which specifies
	// which patient data we should plot as a shadow trace, when appropriate.
	// This is what we need to draw during the right types of breaths.
	PatientDataId::PatientItemId  currAuxYUnits_;

	//@ Data-Member: activeAuxYUnits_
	// The auxillary Y units patient data id which specifies which patient data
	// is currently being drawn.  This is non-NULL, and set to 'currAuxYUnits_'
	// during the appropriate breaths.
	PatientDataId::PatientItemId  activeAuxYUnits_;

	//@ Data-Member: auxYLabelBox_
	// A box for the auxillary label text.
	Container  auxYLabelBox_;

	//@ Data-Member: auxYLabelText_
	// The label text for the auxillary trace.
	TouchableText  auxYLabelText_;

	//@ Data-Member: auxPlotLine_
	// The line used to draw the histogram.
	Line  auxPlotLine_;

private:
	// these methods are purposely declared, but not implemented...
	WaveformPlot(const WaveformPlot&);		// not implemented...
	void   operator=(const WaveformPlot&);	// not implemented...

	inline void gridChanged_(void);
	void calculateScaleFactors_(void);

	enum
	{
		MAX_GRID_LINES_ = 20
	};

	//@ Data-Member: upperXLimit_
	// The upper X limit, defining the last value on the X axis on the right
	// of the grid.
	Int32 upperXLimit_;

	//@ Data-Member: lowerXLimit_
	// The lower X limit, defining the first value on the X axis on the left
	// of the grid.
	Int32 lowerXLimit_;

	//@ Data-Member: xDivision_
	// The amount between each vertical grid line placed along the X axis, e.g.
	// if the X limits are -40 to 40 and xDivision_ is 20 then grid lines are
	// placed every 20, at -40, 20, 0, 20 and 40.
	Uint32 xDivision_;

	//@ Data-Member: displayXValues_
	// A flag which determines if WaveformPlot should label the X axis with
	// the X values.
	Boolean displayXValues_;

	//@ Data-Member: upperYLimit_
	// The upper Y limit, defining the last value on the Y axis at the top
	// of the grid.
	Int32 upperYLimit_;

	//@ Data-Member: lowerYLimit_
	// The lower Y limit, defining the first value on the Y axis at the bottom
	// of the grid.
	Int32 lowerYLimit_;

	//@ Data-Member: yDivision_
	// The amount between each horizontal grid line placed along the Y axis.
	Uint32 yDivision_;

	//@ Data-Member: displayPositiveYValues_
	// When labelling the Y axis with the various Y value positions of the
	// horizontal grid lines some applications want negative Y values to be
	// displayed as the corresponding positive value.  This flag is used for
	// setting this behaviour.
	Boolean displayPositiveYValues_;

	//@ Data-Member: xUnitsField_
	// The text field which labels the X units.
	TextField xUnitsField_;

	//@ Data-Member: yUnitsField_
	// The text field which labels the Y units.
	TextField yUnitsField_;

	//@ Data-Member: lastXPixel_
	// The last valid X pixel coordinate used when plotting the waveform, e.g.
	// if the grid is 500 pixels wide then lastXPixel_ would be 499.
	Uint32 lastXPixel_;

	//@ Data-Member: lastYPixel_
	// The last valid Y pixel coordinate used when plotting the waveform.
	Uint32 lastYPixel_;

	//@ Data-Member: xScaleFactor_
	// The scaling factor used for translating X values to pixel coordinates,
	// e.g. if the X axis goes from 0 to 10 and there are 500 pixels along
	// the X axis then xScaleFactor_ would be 50 approx. (49.9 to be exact,
	// an X value of 10 would scale to be an X pixel coordinate of 499, the
	// value of lastXPixel_!).
	Real32 xScaleFactor_;

	//@ Data-Member: yScaleFactor_
	// The scaling factor used for translating Y values to pixel coordinates,
	Real32 yScaleFactor_;

	//@ Data-Member: yAxisValue_
	// The Y value at which the highlighted Y axis should be plotted.
	// Defaults to 0.
	Real32 yAxisValue_;

	//@ Data-Member: xAxisLine_
	// A highlighted line which depicts the X axis.
	Line xAxisLine_;

	//@ Data-Member: yAxisLine_
	// A highlighted line which depicts the Y axis.  It's position can be
	// changed from the default of 0.
	Line yAxisLine_;

	//@ Data-Member: xGridLines_
	// The X grid lines, those lines parallel to the X axis which mark the
	// various divisions of the Y axis.
	Line xGridLines_[MAX_GRID_LINES_];

	//@ Data-Member: yGridLines_
	// The Y grid lines, those lines parallel to the Y axis which mark the
	// various divisions of the X axis.
	Line yGridLines_[MAX_GRID_LINES_];

	//@ Data-Member: xValues_
	// The X values which label the various grid lines along the X axis.
	TextField xValues_[MAX_GRID_LINES_];

	//@ Data-Member: yValues_
	// The Y values which label the various grid lines along the Y axis.
	TextField yValues_[MAX_GRID_LINES_];

	//@ Data-Member: IS_UPPER_PLOT_
	// The Y values which label the various grid lines along the Y axis.
	const Boolean   IS_UPPER_PLOT_;

	//@ Data-Member: message_
	// Display a generic message.
	TextField message_;

};

// Inlined methods
#include "WaveformPlot.in"

#endif // WaveformPlot_HH 
