#pragma once
#include "afxwin.h"


// CTabLogDlg dialog

class CTabLogDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabLogDlg)

public:
	CTabLogDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabLogDlg();

// Dialog Data
	enum { IDD = IDD_TAB_LOG_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:

	bool m_bHideError; //Hide errors if already shown once

	bool m_bFreeze; //freeze the log window to allowing scrolling


	CString m_strLogStatus;
	void UpdateSWATLog(CString &str);

	virtual BOOL OnInitDialog();
	CButton m_Chk_Critical;

	afx_msg void OnBnClickedCheckFreeze();

	afx_msg void OnBnClickedCheckPopErrorMsg();
	afx_msg void OnBnClickedButtonExpand();
};
