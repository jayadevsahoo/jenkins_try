
#ifndef PhaseInEvent_HH
#define PhaseInEvent_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PhaseInEvent - Identifiers for the "phase-in" events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PhaseInEvent.hhv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//            Initial version 
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct PhaseInEvent
{
  public:
    //@ Type:  PhaseInState
    // This indicates the state of breath that is currently being delivered,
    // as it relates to the phase-in of settings.
    enum PhaseInState
    {
      NON_BREATHING,
      START_OF_INSPIRATION,
      DURING_INSPIRATION,
      START_OF_EXHALATION,
      DURING_EXHALATION
    };
};


#endif // PhaseInEvent_HH 
