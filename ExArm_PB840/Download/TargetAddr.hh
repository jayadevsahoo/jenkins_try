#ifndef  TargetAddr_HH
#define	TargetAddr_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: TargetAddr - Defines a data structure that holds a target
//   machines's BD and GUI Ethernet address.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/TargetAddr.hhv   25.0.4.0   19 Nov 2013 14:02:52   pvcs  $ 
//@ Modification-Log
//
//  Revision: 001  By:  <jhv>    Date:  05-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

	enum TargetAddrCons
	{
		ADDR_LENGTH = 6,
		ADDR_STRING_LENGTH = 12
	};

	typedef struct EtherAddr
	{
		char *name;
		char *pBd;
		char *pGui;
		Uint32 clientIpAddr;
		Boolean pair;
		char guiAddr[ADDR_LENGTH];
		char bdAddr[ADDR_LENGTH];
	} TargetAddr;

#endif // TargetAddr_HH 
