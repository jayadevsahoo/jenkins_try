#pragma once

class Sys_Init
{
public:
    enum InitClassId {
        ID_SYS_INIT              = 1,
        ID_VRTXHOOKS             = 2,
        ID_IPCTABLE              = 3,
        ID_USERCLOCK             = 4,
        ID_TASKTABLE             = 5,
        ID_INITCONDITIONS        = 6,
        ID_TASKCONTROL           = 7,
        ID_FSM                   = 8,
        ID_FSMSTATE              = 9,
        ID_TASKCONTROLQUEUEMSG   = 10,
        ID_CRITICALSECTION       = 11,
        ID_STACKCHECK            = 12,

        ID_BDFSM                 = 13,
        ID_BDSTART               = 14,
        ID_BDINOP                = 15,
        ID_BDONLINE              = 16,
        ID_BDSERVICE             = 17,
        ID_BDSST                 = 18,

        ID_COMMTASKAGENT         = 19,
        ID_TASKAGENT             = 20,
        ID_TASKAGENTMANAGER      = 21,
        ID_TASKCONTROLAGENT      = 22,
        ID_SERVICEMODEAGENT      = 23,

        ID_APPCONTEXT            = 24,
        ID_TASKCONTROLMESSAGE    = 25,
        ID_CHANGESTATEMESSAGE    = 26,
        ID_COMMDOWNMESSAGE       = 27,
        ID_COMMUPMESSAGE         = 28,
        ID_BDREADYMESSAGE        = 29,
        ID_GUIREADYMESSAGE       = 30,
        ID_TASKREADYMESSAGE      = 31,
        ID_SYNCCLOCKMESSAGE      = 32,
        ID_TASKCONTROLTIMER      = 33,
        ID_STARTUPTIMER          = 34,
        ID_TRANSITIONTIMER       = 35,
        ID_SYNCCLOCKTIMER        = 36,

        ID_GUIFSM                = 37,
        ID_GUISTART              = 38,
        ID_GUIINOP               = 39,
        ID_GUIONLINE             = 40,
        ID_GUISERVICE            = 41,
        ID_GUISST                = 42,
        ID_GUITIMEOUT            = 43,
        ID_GUIONLINEINIT         = 44,
        ID_GUISERVICEINIT        = 45,
        ID_GUISSTINIT            = 46,

        ID_BDFAILEDMESSAGE       = 47,

        NUM_INIT_CLASSES
    };
};

class AppContext
{
};

class TaskControlMessage
{
public:
    TaskControlMessage( Sys_Init::InitClassId classId );

    virtual ~TaskControlMessage();

    virtual const char*  nameOf(void) const = 0;
    virtual void         dispatch(const AppContext& context) const = 0;

private:

    //@ Data-Member:    classId_
    //  Contains the class identifier of the derived or concrete class.
    enum Sys_Init::InitClassId  classId_;
};

class TimeStamp
{
public:
    TimeStamp();
    
    void now();

private:
    //@ Data-Member: year_
    // 0000-9999
    Uint16 year_;	

    //@ Data-Member: month_
    // 1-12
    Uint8  month_;	

    //@ Data-Member: dayOfMon_
    // 1-31, obviously month-dependent
    Uint8  dayOfMon_;	

    //@ Data-Member: hour_
    // 0-23
    Uint8  hour_;		

    //@ Data-Member: min_
    // 00-59
    Uint8  min_;	

    //@ Data-Member: sec_
    // 00-59
    Uint8  sec_;	

    //@ Data-Member: hsec_
    // 00-99
    Uint8  hsec_;	
};

class PostAgent
{

public:
    enum
    {
        PART_NO_SIZE = 13
    };

    PostAgent(void);
    ~PostAgent(void);

private:
    // data members placed largest first to pack bits at end and
    // conserve space for the network object

    //@ Data-Member:     powerDownTime_
    const TimeStamp      powerDownTime_;

    //@ Data-Member:     powerUpTime_
    const TimeStamp      powerUpTime_;

    //@ Data-Member:     downtimeStatus_
    const DowntimeStatus downtimeStatus_;

    //@ Data-Member:     startupState_
    const ShutdownState  startupState_;

    //@ Data-Member:     postFailed_
    const Boolean        postFailed_;

    //@ Data-Member:     backgroundFailed_
    const Criticality    backgroundFailed_;

    //@ Data-Member:     serviceModeRequested_
    const Boolean        serviceModeRequested_;

    //@ Data-Member:     kernelPartNumber_
    char                 kernelPartNumber_[PostAgent::PART_NO_SIZE+1];

};

class SerialNumber 
{
public:
    SerialNumber();

private:

    //@ Data-Member:    data_
    //  The serial number data itself.
    char   data_[SERIAL_NO_SIZE+1];

};

class GuiReadyMessage : TaskControlMessage
{
public:

    GuiReadyMessage(enum SigmaState state, Boolean service_required);
    GuiReadyMessage(const GuiReadyMessage& rGuiReady);

    ~GuiReadyMessage();

    const char* nameOf(void) const;
    void dispatch(const AppContext& context) const;

private:

    GuiReadyMessage();                        // declared only
    void operator=(const GuiReadyMessage&);   // declared only

    //@ Data-Member:    state_
    //  Reports the node is ready in this state.
    SigmaState       state_;

    //@ Data-Member:    serviceRequired_
    //  Indicates if the node requires service.
    Boolean          serviceRequired_;

    //@ Data-Member:    postAgent_
    //  Contains the PostAgent object from the reporting node
    //  providing access to various states
    const PostAgent  postAgent_;

    //@ Data-Member:    serialNumber_
    //  Contains the SerialNumber found in GUI FLASH
    const SerialNumber  serialNumber_;
};
