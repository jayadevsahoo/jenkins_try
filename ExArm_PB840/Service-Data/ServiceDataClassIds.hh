#ifndef ServiceDataClassIds_HH
#define ServiceDataClassIds_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  ServiceDataClassIds - Ids of all of the Service-Data Classes.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/ServiceDataClassIds.hhv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//  
//====================================================================

#include "Sigma.hh"


//@ Type:  ServiceDataClassId
// Ids for all of Service Data Subsystem classes.
enum ServiceDataClassId {
  SERVICE_DATA_MGR,
  TEST_INFO,
  TEST_DATA_INFO,
  TEST_COMMAND_INFO,
  TEST_STATUS_INFO,
  TEST_DATUM_HANDLE,
  SERVICE_DATA_TIMER_REGISTRAR,
  SERVICE_DATA_TIMER_TARGET,
  SERVICE_MODE_MANAGER,
  FAKE_SM_CONTROL_TIMER,
  TEST_RESULT_DATA,
  EST_TEST_RESULT_DATA,
  SST_TEST_RESULT_DATA,
  NUM_SERVICE_DATA_CLASSES
};


#endif // ServiceDataClassIds_HH 
