#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhFlowSensorFlowTest - BD Safety Net Test for the detection
//                                 of exhalation flow sensor reading OOR 
//---------------------------------------------------------------------
//@ Interface-Description
//  The checkCriteria() method of this class is called from the
//  newCycle() method of the SafetyNetTestMediator class.  The
//  checkCriteria() method invokes methods in the Sensor class to obtain 
//  the value of the Exhalation Flow Sensor Flow Signal in counts and
//  to obtain the exhaled flow reading in Lpm.
//---------------------------------------------------------------------
//@ Rationale
//  Used to check if the exhaled flow sensor flow signal is within a
//  valid range after accounting for the possibility of patient actions
//  such as coughing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The safety net test is defined in the checkCriteria() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExhFlowSensorFlowTest.ccv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  iv    Date: 23-Nov-1998     DR Number:DCS 5266
//       Project:  Sigma (R8027)
//       Description:
//           Replaced EXH_FLOW_MAX_LPM with
//           MAX_EXH_FLOW_TEST_COUNT_LIMIT.
//
//  Revision: 003 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added error code when fault is detected.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  05-Sep-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "ExhFlowSensorFlowTest.hh"
#include "MainSensorRefs.hh"

//@ Usage-Classes
#include "ExhFlowSensor.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhFlowSensorFlowTest()  
//
//@ Interface-Description
//  Default Constructor.  Passes two arguments to the base class
//  constructor to define the error code and time criterion.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The backgndEventId_ and maxCyclesCriteriaMet_ data members are
//  set by the base class constructor.  The background event ID is
//  a unique identifier that is placed in the diagnostic log if the
//  background check detects a problem.
//
//  For this class the maxCyclesCriteriaMet_ is not used and is set
//  to 0.  The data members numCyclesOorHigh_ and numCyclesOorLow_
//  are initialized to 0.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

ExhFlowSensorFlowTest::ExhFlowSensorFlowTest( void ) :
SafetyNetTest( BK_EXH_FLOW_OOR, 0 )
{
    CALL_TRACE("ExhFlowSensorFlowTest( void )");

    // $[TI1]
    numCyclesOorHigh_ = 0;
    numCyclesOorLow_ = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhFlowSensorFlowTest() 
//
//@ Interface-Description
//  Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

ExhFlowSensorFlowTest::~ExhFlowSensorFlowTest( void )
{
    CALL_TRACE("~ExhFlowSensorFlowTest( void )");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkCriteria()
//
//@ Interface-Description
//  This method takes no arguments and returns a BkEventName.
//  This method is responsible for detecting the exhalation flow
//  sensor reading OOR.
//  If the background check criteria have been met for the required
//  number of cycles, the backgndEventId for the class is returned;
//  otherwise BK_NO_EVENT is returned to indicate to the caller that
//  the Background subsystem should not be notified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method first checks the status of the backgndCheckFailed_
//  data member.  If it is TRUE, the test is no longer run since
//  Background events are not auto-resettable and the Background
//  subsystem only needs to be informed of an event once.  In this
//  case, BK_NO_EVENT is returned to the caller.
//
//  If the check has not already failed, the getValue() and getCount()
//  methods are invoked to get the exhalation flow sensor reading in
//  both counts and Lpm.
//
//  The following checks are then made:
//
//       (1)   Exh Flow > MAX_EXH_FLOW_TEST_COUNT_LIMIT 
//       (2)   Exh Flow < EXH_FLOW_MIN_COUNTS
//
//  If the first condition is met, the numCyclesOorHigh_ data item is
//  incremented and compared to a maximum value to determine if the
//  background subsystem needs to be informed.  The high flow test
//  takes into account the possiblity of high flow rates during
//  patient coughs.
//
//  If the second condition is met, the numCyclesOorLow_ data item is
//  incremented and compared to a maximum value to determine if the
//  background subsystem needs to be informed.
//
//  $[06013] $[06014]
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

BkEventName
ExhFlowSensorFlowTest::checkCriteria( void )
{
    CALL_TRACE("ExhFlowSensorFlowTest::checkCriteria (void)") ;

    BkEventName rtnValue = BK_NO_EVENT;

    // Only do the check if it hasn't previously failed
    if ( ! backgndCheckFailed_ )
    {
    // $[TI1]
// E600 BDIO
#if 0
		if ( RExhFlowSensor.getCount() > MAX_EXH_FLOW_TEST_COUNT_LIMIT )
        {
        // $[TI1.1]
            
            numCyclesOorLow_ = 0;
            
            if ( ++numCyclesOorHigh_ >= MAX_FLOW_HIGH_OOR_CYCLES )
            {
            // $[TI1.1.1]
                errorCode_ = (Uint16)(RExhFlowSensor.getValue()*100.0F);
                backgndCheckFailed_ = TRUE;
                rtnValue = backgndEventId_;
            }
            // $[TI1.1.2]
        }
        else if ( RExhFlowSensor.getCount() < MIN_FLOW_TEST_COUNT_LIMIT )
        {
        // $[TI1.2]
            
            numCyclesOorHigh_ = 0;
            
            if ( ++numCyclesOorLow_ >= MAX_FLOW_LOW_OOR_CYCLES )
            {
            // $[TI1.2.1]
                errorCode_ = (Uint16)(RExhFlowSensor.getValue()*100.0F);
                backgndCheckFailed_ = TRUE;
                rtnValue = backgndEventId_;
            }
            // $[TI1.2.2]
        }
        else
        {
        // $[TI1.3]
            numCyclesOorHigh_ = 0;
            numCyclesOorLow_ = 0;
        }
// E600 BDIO
#endif
    }
    // $[TI2]

    return( rtnValue );
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
ExhFlowSensorFlowTest::SoftFault( const SoftFaultID  softFaultID,
                                  const Uint32       lineNumber,
                                  const char*        pFileName,
                                  const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, EXHFLOWSENSORFLOWTEST,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

