#ifndef LargeContainer_HH
#define LargeContainer_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: LargeContainer - A Container containing a large number of
//                         Drawables.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/LargeContainer.hhv   25.0.4.0   19 Nov 2013 14:11:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date:  23-Jan-2007    SCR Number: 6237
//  Project: TREND
//  Description:
//	Initial version.
//
//====================================================================

#include "Container.hh"
#include "TemplateMacros.hh"
#include "SListR_Drawable_LG_DRAWABLE_LIST_NUM.hh"

class LargeContainer : public Container
{
public:
	LargeContainer(void);
	virtual ~LargeContainer(void);

private:
	LargeContainer(const LargeContainer&);		// Declared, not implemented.
	void operator=(const LargeContainer&);	// Declared, not implemented.

	//@ Data-Member: children_
	// A linked list which holds references to all of the contained
	// Drawables.
	FixedSListR(Drawable, LG_DRAWABLE_LIST_NUM) children_;
};

#endif // LargeContainer_HH 
