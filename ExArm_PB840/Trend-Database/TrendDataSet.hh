#ifndef TrendDataSet_HH
#define TrendDataSet_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDataSet.hh - An encapsulation of the request for trending
//  data and the returned data form the Trend-Datase subsystem.
// 
// 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDataSet.hhv   25.0.4.0   19 Nov 2013 14:34:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 001  By:  mjf    Date:  13-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//
//=====================================================================
//@ Usage-Classes
//@ End-Usage

#include "TimeStamp.hh"             // OS-Foundation
#include "TrendTimeScaleValue.hh"   // Setting-Validation

#include "TrendRequestInfo.hh"
#include "TrendDataValues.hh"
#include "TrendEvents.hh"



class TrendDataSet
{
public:    

	//@ Type: Status
	// Enum type for the set status
	enum Status
	{
		EMPTY_SET           = 0,	// state after clear()
		PROCESSING          = 1,	// Request sent to the TrendDataMgr
		PROCESS_OK          = 2,	// read request returned no errors
		PROCESS_ERROR       = 3		// Error in fullfillment of the request
	};

	TrendDataSet( void );
	~TrendDataSet( void );

	void clear( void );
	void setRequestInfo( const TrendRequestInfo & requestInfo );

	inline Status getStatus( void ) const;

	inline TrendTimeScaleValue::TrendTimeScaleValueId getTimescale( void ) const;
	inline Uint16 getDataSetSize( void ) const;
	inline const TrendDataValues & getTrendDataValues( Uint16 column ) const;
	inline const TrendEvents & getTrendEvents( Uint16 row ) const;
	inline const TimeStamp & getTrendTimeStamp( Uint16 row ) const;
	inline Int32 getSequenceNumber( Uint16 row ) const;
	inline Boolean isMostCurrent( void ) const;

	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:

	// Give special "friend" access to the Data Manager and Repositories, so
	// that they can load the trend data into the set.
	friend class TrendDataMgr;
	friend class TrendDbPatientRepository;
	friend class TrendDbSettingRepository;


	inline void setStatus_( Status status );
	inline void setTimescale_( TrendTimeScaleValue::TrendTimeScaleValueId timeScale );
	inline void setFrameSeqNumber_( Int32 seqNumber );
	inline void setDataSetSize_( Uint16 dataCount );
	inline TrendDataValues & getTrendDataValues_( Uint16 column );
	inline TrendEvents & getTrendEvents_( Uint16 row );
	inline void setTrendTimeStamp_( Uint16 row, const TimeStamp & timeStamp );
	inline void setSequenceNumber_( Uint16 row, Int32 seqNumber );
	inline void setMostCurrent_( Boolean isMostCurrrent );

	TrendRequestInfo & getRequestInfo_( void );
	


	// no copy or assignment constructor
	TrendDataSet( const TrendDataSet& );	// no implemented
	void operator=( const TrendDataSet& );		// no implemented


	//@ Data-Member: status_
	// Status of the data set
	Status					status_;

	//@ Data-Member: requestData_
	// Contains a copy of the request information
	TrendRequestInfo		requestData_;

	//@ Data-Member: timescale_
	// Timescale for the data in the set
	TrendTimeScaleValue::TrendTimeScaleValueId  timescale_;

	//@ Data-Member: dataCount_
	// The size "rows" of data in the data set
	Uint16					dataCount_;

	//@ Data-Member: trendValues_
	// An array of trend values for a column in the data set
	TrendDataValues			trendValues_[ TrendDataMgr::MAX_COLUMNS ];

	//@ Data-Member: trendEvents_
	// An array of events for each row in the data set
	TrendEvents            	trendEvents_[ TrendDataMgr::MAX_ROWS ];

	//@ Data-Member: trendTimeStamp_
	// An array of timestamps for each row in the data set
	TimeStamp               trendTimeStamp_[ TrendDataMgr::MAX_ROWS ];

	//@ Data-Member: sequenceNumber_
	// An array of frame sequence numbers for each row in the data set
	Int32                   sequenceNumber_[ TrendDataMgr::MAX_ROWS ];

	//@ Data-Member: isMostCurrent_
	// Flag which indicates if the data loaded is the most current in the database
	Boolean 				isMostCurrent_;
};


// Inlined methods
#include "TrendDataSet.in"

#endif // TrendDataSet_HH
