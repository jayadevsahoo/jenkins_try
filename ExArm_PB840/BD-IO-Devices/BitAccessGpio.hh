#ifndef BIT_ACCESS_GPIO_H
#define BIT_ACCESS_GPIO_H

#include "Sigma.hh"
#include "Gpio.h"
#include "AioDioDriver.h"

static const Uint16 MAX_BITS	= 196;

class BitAccessGpio
{
public:

	enum BitState { OFF=0, ON} ;

	BitAccessGpio( const BitState initState) ;
	virtual ~BitAccessGpio( void) ;

    void requestBitUpdate(const ApplicationDigitalOutputs_t bit, const BitState state) ;
    void newCycle( void) ;
    void enableLatch( const ApplicationDigitalOutputs_t bit, const BitState state) ;

    // Get the current state of an input pin
    BitAccessGpio::BitState getBitState( const ApplicationDigitalInputs_t bit) ;

    // Get the current state of an output pin
    BitAccessGpio::BitState getBitState( const ApplicationDigitalOutputs_t bit) ;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32        lineNumber,
						   const char*         pFileName  = NULL,
						   const char*         pPredicate = NULL) ;
protected:
    void InitGpioEntry(const ApplicationDigitalInputs_t bit);
    void InitGpioEntry(const ApplicationDigitalOutputs_t bit, const BitState initState);

private:
    Boolean			Latched[MAX_BITS];				// set to latch bit high
    Gpio_t*         pGpio[MAX_BITS];
};

#endif
