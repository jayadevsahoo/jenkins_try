#ifndef DisconnectTrigger_HH
#define DisconnectTrigger_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: DisconnectTrigger - Monitors the system for the specific 
//	conditions that cause patient circuit disconnect to be detected.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DisconnectTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 009  By:  iv    Date: 08-Sep-1998     DR Number:DCS 5140
//       Project:  BiLevel
//       Description:
//             Added member isSecondCriteriaDisabled_ and methods :
//             disableVolumeCriteria(void) and enableVolumeCriteria(void).
//
//  Revision: 008  By:  iv    Date: 18-Sep-1997     DR Number:DCS 1649
//       Project:  Sigma (R8027)
//       Description:
//             Deleted the mechanism to force disconnect.
//
//  Revision: 007  By:  sp    Date: 30-Sept-1996    DR Number:DCS 1388
//       Project:  Sigma (R8027)
//       Description:
//             Added the mechanism to force disconnect.
//
//  Revision: 006  By:  sp    Date:  18-Sept-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 005  By:  sp    Date:  3-Sept-1996    DR Number:DCS 1293
//       Project:  Sigma (R8027)
//       Description:
//             update per controller spec 5.0.
//
//  Revision: 004  By:  sp    Date:  9-Aug-1996    DR Number:DCS 1218
//       Project:  Sigma (R8027)
//       Description:
//             Add call back register for tidal volume change.
//
//  Revision: 003  By:  sp    Date:  30-Apr-1996    DR Number:DCS 927
//       Project:  Sigma (R8027)
//       Description:
//             new disconnect trigger.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
#  include "Sigma.hh"
 
#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "ModeTrigger.hh"

#include "SettingId.hh"

#include "BreathRecord.hh"
//@ End-Usage


class DisconnectTrigger : public ModeTrigger
{
  public:
    DisconnectTrigger(void);
    virtual ~DisconnectTrigger(void);
    virtual void enable(void);
   
    inline void setDesiredFlow(const Real32 desiredFlow);
    inline void setFlowCmdLimit(const Real32 cmdLimit);
    inline void setTimeLimit(const Int32 timeLimit);
    inline void disableVolumeCriteria(void);
    inline void enableVolumeCriteria(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);


  protected:
    virtual Boolean triggerCondition_(void);

  private:
    DisconnectTrigger(const DisconnectTrigger&);    // Declared but not implemented
    void operator=(const DisconnectTrigger&);   // Declared but not implemented

    Boolean detectDisconnect_(BreathRecord* pBreathRecord);
    Boolean detectSpontDisconnect_();

    //@ Data-Member:  firstCriteria_
    //  Stores the disconnect status of the first criteria 
    Boolean  firstCriteria_;

    //@ Data-Member:  secondCriteria_
    //  Stores the disconnect status of the first criteria 
    Boolean  secondCriteria_;

    //@ Data-Member:  criteria2Count_
    //  Counter for the disconnect status of the second criteria 
    Int32  criteria2Count_;

    //@ Data-Member:  isSecondCriteriaDisabled_
    //  flag to disable the second criteria 
	Boolean isSecondCriteriaDisabled_;

    //@ Data-Member:  flowHasResumed_
    // Flow resumed flag 
    Boolean  flowHasResumed_;

    //@ Data-Member:  desiredFlow_
    //  Desired flow.
    Real32 desiredFlow_;

    //@ Data-Member:  flowCmdLimit_
    //  Flow command limit.
    Real32 flowCmdLimit_;

    //@ Data-Member:  timeLimit_
    //  Maximum breath duration for SPONT breath.
    Int32 timeLimit_;

    //@ Data-Member:  delayTrig_
    //  Delay counter for first criteria
    Int32 delayTrig_;

    //@ Data-Member:  discCount1_
    // Counter for disconnect condition for the first 200 msec
    // conditions of the first criteria are satisfied.
    Int32  discCount1_;

    //@ Data-Member:  discCount2_
    // Counter for disconnect condition after the first 200 msec
    Int32  discCount2_;

};

// Inlined methods
#include "DisconnectTrigger.in"

#endif // DisconnectTrigger_HH 
