#ifndef VentTestSummarySubScreen_HH
#define VentTestSummarySubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VentTestSummarySubScreen - The screen selected by pressing the
// Ventilator Test Summary button on the Upper Other Screens subscreen.
// Displays a summary of the last run of EST, and SST.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VentTestSummarySubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  25-SEP-1997    DR Number:   1861
//    Project:  Sigma (R8027)
//    Description:
//      Updated access method for vent inop test due to changes in service
//		mode.
//
//  Revision: 001  By:  mpm    Date:  18-AUG-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"
#include "LogTarget.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "TestResultId.hh"
#include "GuiAppClassIds.hh"
class ScrollableLog;
//@ End-Usage

class SubScreenArea;
class TimeOfDay;

class VentTestSummarySubScreen : public SubScreen, public LogTarget
{
public:
	VentTestSummarySubScreen(SubScreenArea* pSubScreenArea);
	~VentTestSummarySubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	// Inherited from LogTarget
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
						Boolean &rIsCheapText, StringId &rString1,
						StringId &rString2, StringId &rString3,
						StringId &rString1Message);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	VentTestSummarySubScreen(void);							// not implemented...
	VentTestSummarySubScreen(const VentTestSummarySubScreen&);	// not implemented...
	void operator=(const VentTestSummarySubScreen&);			// not implemented...

	StringId getOutcomeString_(TestResultId resultId);
	void updateErrorMsg_(TextField &warningLabel, TextField &warningValue, Int32 &errorCount);

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen
	SubScreenTitleArea titleArea_;

	//@ Data-Member: pSummaryTable_
	// A ScrollableLog used to display the table of test summaries.
	ScrollableLog *pSummaryTable_;

	//@ Data-Member: calWarningValue_
	// A TextField objects used to display calibration warning message.
	// lower screen
	TextField calWarningValue_;

	//@ Data-Member: calWarningLabel_
	// A TextField objects used to display calibration warning label.
	// lower screen
	TextField calWarningLabel_;

	//@ Data-Member: flowSensorWarningValue_
	// A TextField objects used to display flow sensor calibration warning message.
	// lower screen
	TextField flowSensorWarningValue_;

	//@ Data-Member: flowSensorWarningLabel_
	// A TextField objects used to display flow sensor calibration warning label.
	// lower screen
	TextField flowSensorWarningLabel_;

	//@ Data-Member: testVentInopWarningValue_
	// A TextField objects used to display Test Vent Inop warning message.
	// lower screen
	TextField testVentInopWarningValue_;

	//@ Data-Member: testVentInopWarningLabel_
	// A TextField objects used to display Test Vent Inop warning label.
	// lower screen
	TextField testVentInopWarningLabel_;

	//@ Data-Member: pressureXducerCalWarningValue_
	// A TextField objects used to display pressure transducer calibration
	// warning message.
	// lower screen
	TextField pressureXducerCalWarningValue_;

	//@ Data-Member: pressureXducerCalWarningLabel_
	// A TextField objects used to display flow sensor calibration warning label.
	// lower screen
	TextField pressureXducerCalWarningLabel_;

};

#endif // VentTestSummarySubScreen_HH 
