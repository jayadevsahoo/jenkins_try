#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
 
//============================== C L A S S   D E S C R I P T I O N ====
//@ Filename: Flash28F400B.cc - FLASH Memory Programmer for (i.e) Intel
//---------------------------------------------------------------------
//@ Interface-Description
//  The Flash28F400B contains free functions to program the Flash memory
//  device.  The Flash28F400B_EraseBlock function erases a block of flash memory.  
//  The FlashWriteWord function writes a word to flash memory.  The Service-Mode
//  subsystem uses these functions to program the calibration tables
//  as well as the Serial Number and software configuration item to
//  flash memory.
//---------------------------------------------------------------------
//@ Rationale
//  The Flash28F400B class contains free functions required to program flash
//  during normal application execution.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The FlashEraseBlock function erases a flash memory block.  The 
//  FlashWriteWord function writes a single 4-byte word to a flash
//  memory location.
//
//  THIS CODE MUST EXECUTE IN DRAM/NOVRAM AND NOT IN FLASH MEMORY.
//  IN ADDITION, IT MUST NOT REFERENCE ANYTHING IN FLASH INCLUDING 
//  CONSTANTS OR SYSTEM SERVICES.  
//
//  The MRI compiler pragma option -NTvars generates this code in the
//  vars section instead of the default code section so the linker
//  locates the code in the vars section which is located in DRAM.
//
//---------------------------------------------------------------------
//@ Fault-Handling
//  Not applicable
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/Flash28F400B.ccv   25.0.4.0   19 Nov 2013 14:16:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision 001  By: Rhomere Jimenez  Date: 07-March-2006  DR Number: 6192
//   Project:   Xena
//   Description:
//      Initial version.
//=====================================================================

#include "Flash28F400B.hh"
#include "Watchdog.hh"    //  for STROBE_WATCHDOG
#include <cpu.h>
#include <vrtxil.h>

//  The -NTvars option directs compiled code to the "vars" section
//  instead of the "code" section.  This is necessary since flash memory
//  cannot be programmed (successfully) while executing code in flash.

#pragma option -NTvars 

//  The following must not compile or link into FLASH.  


// List of commands for the 28F400B Flash Chip
static const Uint32 FLASH_28F400B_ERASE_SETUP              = 0x00200020;
static const Uint32 FLASH_28F400B_WRITE_SETUP              = 0x00400040;
static const Uint32 FLASH_28F400B_CLEAR_STATUS_REGISTER    = 0x00500050;
static const Uint32 FLASH_28F400B_READ_STATUS_REGISTER     = 0x00700070;
static const Uint32 FLASH_28F400B_WSM_READY                = 0x00800080;
static const Uint32 FLASH_28F400B_ERASE_CONFIRM            = 0x00D000D0;
static const Uint32 FLASH_28F400B_READ_ARRAY               = 0x00ff00ff;
static const Uint32 FLASH_28F400B_IDENTIFY_DEVICE          = 0x00900090;
static const Uint32 FLASH_28F400B_PROGRAM_ERROR_DETECT     = 0x00100010;
static const Uint32 FLASH_28F400B_ERASE_ERROR_DETECT       = 0x00200020;
static const Uint32 FLASH_28F400B_ANY_ERROR_DETECT         = 0x00380038;

static const Uint32 FLASH_28F400B_NUM_INTERLEAVES          = 2;
static const Uint32   FLASH_28F400B_NUM_SUB_BLOCKS         = 7;  
static const Uint32   FLASH_28F400B_TOTAL_SUB_BLOCKS = 
                          FLASH_28F400B_NUM_INTERLEAVES * FLASH_28F400B_NUM_SUB_BLOCKS;

static const Uint32   FLASH_28F400B_SUB_BLOCK1_SIZE  = 0x4000;     // 16k word.
static const Uint32   FLASH_28F400B_SUB_BLOCK2_SIZE  = 0x2000;     // 8k words.
static const Uint32   FLASH_28F400B_SUB_BLOCK3_SIZE  = 0x2000;     // 8k words.
static const Uint32   FLASH_28F400B_SUB_BLOCK4_SIZE  = 0x18000;    // 96k words.
static const Uint32   FLASH_28F400B_SUB_BLOCK5_SIZE  = 0x20000;    // 128k words.
static const Uint32   FLASH_28F400B_SUB_BLOCK6_SIZE  = 0x20000;    // 128k words.
static const Uint32   FLASH_28F400B_SUB_BLOCK7_SIZE  = 0x20000;    // 128k words.

// Stores all of the block sizes of the flash chip.
static Int FLASH_28F400B_ALL_SUB_BLOCK_SIZES[FLASH_28F400B_NUM_SUB_BLOCKS] = 
{
    FLASH_28F400B_SUB_BLOCK1_SIZE ,
    FLASH_28F400B_SUB_BLOCK2_SIZE ,
    FLASH_28F400B_SUB_BLOCK3_SIZE ,
    FLASH_28F400B_SUB_BLOCK4_SIZE ,
    FLASH_28F400B_SUB_BLOCK5_SIZE ,
    FLASH_28F400B_SUB_BLOCK6_SIZE ,
    FLASH_28F400B_SUB_BLOCK7_SIZE
};

// Stores all of the block address of the flash chip
static const Uint32 FSTART = 0xff600000 ;
static Uint32 * FLASH_28F400B_BLOCK_ADDRESS[FLASH_28F400B_TOTAL_SUB_BLOCKS + 1] = {
    (Uint32 *)(FSTART),
    (Uint32 *)(FSTART+4*1024*(16)),                                             //  16 kByte
    (Uint32 *)(FSTART+4*1024*(16+8)),                                           //   8 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8)),                                         //   8 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96)),                                      //  96 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128)),                                  // 128 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128+128)),                              // 128 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128+128+128)),                          // 128 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128+128+128+16)),                       //  16 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128+128+128+16+8)),                     //   8 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128+128+128+16+8+8)),                   //   8 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128+128+128+16+8+8+96)),                //  96 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128+128+128+16+8+8+96+128)),            // 128 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128+128+128+16+8+8+96+128+128)),        // 128 kByte
    (Uint32 *)(FSTART+4*1024*(16+8+8+96+128+128+128+16+8+8+96+128+128+128))     // 128 kByte
} ;


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  Flash28F400B_EraseBlock - Erase FLASH memory block
//
//@ Interface-Description
//  Accepts the starting address of the flash memory block.  Initiates
//  a flash memory erase operation at that address.  Uses the
//  manufacturer's code from the flash device to determine the correct
//  erase command sequence.  Returns the completion status of the erase
//  operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Disables interrupts prior to the erase operation and enables them
//  after the operation completes.  This function continues to strobe 
//  the watchdog timer for the duration of the erase operation to 
//  avoid a reset during the operation. 
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
 
Int
Flash28F400B_EraseBlock( volatile Uint32 * address )
{
    int ret = 0;
    Uint32 status;

    cpu_interrupt_disable();

    for ( Uint interleave=0; 
          interleave<FLASH_28F400B_NUM_INTERLEAVES && ret == 0; 
          interleave++, address++ )
    {
        *address = FLASH_28F400B_CLEAR_STATUS_REGISTER;
        *address = FLASH_28F400B_ERASE_SETUP;
        *address = FLASH_28F400B_ERASE_CONFIRM;
        *address = FLASH_28F400B_READ_STATUS_REGISTER;

        do
        {
            STROBE_WATCHDOG;
            status = *address;
        } 
        while (   ((status & FLASH_28F400B_WSM_READY) != FLASH_28F400B_WSM_READY)
               && ((status & FLASH_28F400B_ERASE_ERROR_DETECT) != FLASH_28F400B_ERASE_ERROR_DETECT));
    
        ret = status & FLASH_28F400B_ANY_ERROR_DETECT;

        *address = FLASH_28F400B_READ_ARRAY;
        asm(" nop ");
    }

    cpu_interrupt_enable();
 
    // $[TI1]

    return ret;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  Flash28F400B_WriteWord - Writes a 4-byte word to flash memory
//
//@ Interface-Description
//  Accepts the address of the flash memory word and the 4-byte data
//  to be programmed.  Initiates a flash memory write operation at 
//  the specified address.  Uses the manufacturer's code from the 
//  flash device to determine the correct write word command sequence.
//  Returns the completion status of the write operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Disables interrupts prior to the write operation and enables them
//  after the operation completes.  Continues to strobe the watchdog
//  timer for the duration of the operation.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
 
Int
Flash28F400B_WriteWord(volatile Uint32 * address, const Uint32 data)
{
    Int ret = 0;
    Uint32 status;

    cpu_interrupt_disable();

    *address = FLASH_28F400B_CLEAR_STATUS_REGISTER;
    *address = FLASH_28F400B_WRITE_SETUP;
    *address = data;
    *address = FLASH_28F400B_READ_STATUS_REGISTER;

    do
    {
        STROBE_WATCHDOG;
        status = *address;
    } 
    while (   ((status & FLASH_28F400B_WSM_READY) != FLASH_28F400B_WSM_READY)
           && ((status & FLASH_28F400B_PROGRAM_ERROR_DETECT) != FLASH_28F400B_PROGRAM_ERROR_DETECT));
    

    ret = status & FLASH_28F400B_ANY_ERROR_DETECT;

    *address = FLASH_28F400B_READ_ARRAY;
    asm(" nop ");

    cpu_interrupt_enable();

    // $[TI1]
    return ret;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  Flash28F400B_GetDeviceId - Queries the device id from 
//                                        FLASH
//@ Interface-Description
//  Accepts the address of the flash memory block.  Initiates a flash 
//  memory read array operation at this address.  Then it is set to 
//  a Intelligent Identifier command mode which enables the ability to
//  read the device Id at that address.  After reading the device Id,
//  it is set back to the read array operation mode in order to get out 
//  of Intelligent Identifier command mode.  This function returns
//  the device id in 32 bits.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================
 
Int
Flash28F400B_GetDeviceId( volatile Uint32 * address )
{
    Uint32 deviceId = 0;
    *address = FLASH_28F400B_READ_ARRAY;
    *address = FLASH_28F400B_IDENTIFY_DEVICE;
    deviceId = *address;       
    *address = FLASH_28F400B_READ_ARRAY;
    asm(" nop ");

    return deviceId;
}
 
//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  Flash28F400B_GetFlashBlockAddress - returns the block   
//                                               address based on the 
//                                               specified index.
//
//@ Interface-Description
//  This method accepts an index to the array of block addresses and returns 
//  the block address of the given index.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It returns the static variable FLASH_28F400B_BLOCK_ADDRESS[index].
//---------------------------------------------------------------------
//@ PreCondition
//  The index argument must be within the range from zero to 
//  FLASH_28F400B_TOTAL_SUB_BLOCKS. 
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================

Uint32 * Flash28F400B_GetFlashBlockAddress(Uint8 index)
{
    return FLASH_28F400B_BLOCK_ADDRESS[index];       
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  Flash28F400B_GetFlashBlockSize - returns the block size  
//                                              based on the specified
//                                              index.
//
//@ Interface-Description
//  This method accepts an index to the array of block sizes and returns 
//  the block size of the given index.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It returns the static variable FLASH_28F400B_ALL_SUB_BLOCK_SIZES[index].
//---------------------------------------------------------------------
//@ PreCondition
//  The index argument must be within the range from zero to 
//  FLASH_28F400B_NUM_SUB_BLOCKS. 
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================

Int Flash28F400B_GetFlashBlockSize(Uint8 index)
{
    
    return FLASH_28F400B_ALL_SUB_BLOCK_SIZES[index];       
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  Flash28F400B_GetNumSubBlocks - returns the number of 
//                                            sub blocks per chip
//
//@ Interface-Description
//  This method has no arguments and returns the number of 
//  sub blocks of the Intel flash chip.
//---------------------------------------------------------------------
//@ Implementation-Description
/// Returns the static variable FLASH_28F400B_NUM_SUB_BLOCKS.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================

Uint32  Flash28F400B_GetNumSubBlocks(void)
{    
    return FLASH_28F400B_NUM_SUB_BLOCKS;       
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Function:  Flash28F400B_GetMaxTotalSubBlocks - returns the total 
//                                                 sub blocks
//
//@ Interface-Description
//  This method has no arguments and returns the total number sub blocks
//  of the Intel flash chip.
//---------------------------------------------------------------------
//@ Implementation-Description
/// Returns the static variable FLASH_28F400B_TOTAL_SUB_BLOCKS.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Function
//=====================================================================

Uint32  Flash28F400B_GetTotalSubBlocks(void)
{    
    return FLASH_28F400B_TOTAL_SUB_BLOCKS;       
}
      


#if defined( SIGMA_COMMON ) || defined( SIGMA_UNIT_TEST )
//=====================================================================
//
//  FlashEraseBlockNonBlocking() and FlashCheck() are used by the
//  Download subsystem executing from PROM.  These functions must not 
//  be called from FLASH so they are defined only for the COMMON build.
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  FlashEraseBlockNonBlocking - Erase FLASH memory block
//      without disabling interrupts
//
//@ Interface-Description
//  Accepts the starting address of the flash memory block.  Initiates
//  a flash memory erase operation at that address.  Returns the status
//  of the erase operation.  During the erase operation, the caller can
//  proceed with other non-flash operations.  It can check on the 
//  status of the erase operation by using the FlashCheck() method.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
 
Int
Flash28F400B_EraseBlockNonBlocking( volatile Uint32 * address )
{
    Int ret = 0;

    for ( Uint interleave=0; 
          interleave<FLASH_28F400B_NUM_INTERLEAVES && ret == 0; 
          interleave++, address++ )
    {
        *address = FLASH_28F400B_CLEAR_STATUS_REGISTER;
        *address = FLASH_28F400B_ERASE_SETUP;
        *address = FLASH_28F400B_ERASE_CONFIRM;
    }

    return Flash28F400B_Check(address);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  FlashCheck - Check Status of active flash operation
//
//@ Interface-Description
//  Accepts the starting address of the flash memory block.  Returns 
//  the status of the current flash operation to the caller.  Returns
//  zero if the operation is complete with no errors.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
 
Int
Flash28F400B_Check( volatile Uint32 * address )
{
    Int ret = 0;
    Uint32 status;

    for ( Uint interleave=0; 
          interleave<FLASH_28F400B_NUM_INTERLEAVES && ret == 0; 
          interleave++, address++ )
    {
        *address = FLASH_28F400B_READ_STATUS_REGISTER;

        status = *address;
    
        if ((status & FLASH_28F400B_WSM_READY) != FLASH_28F400B_WSM_READY)
        {                                                    // $[TI1.1]
            ret = Flash28F400B_WSM_BUSY;
        }
        else
        {                                                    // $[TI1.2]
            ret = status & FLASH_28F400B_ANY_ERROR_DETECT;    // mask out READY bit
            *address = FLASH_28F400B_READ_ARRAY;
            asm(" nop ");
        }
    }

    return ret;
}
#endif // SIGMA_COMMON || SIGMA_UNIT_TEST
