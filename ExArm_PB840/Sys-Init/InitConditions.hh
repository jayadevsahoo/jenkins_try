#ifndef InitConditions_HH
#define InitConditions_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  InitConditions - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/InitConditions.hhv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 12-Oct-1998 DCS Number: 5164
//      Project:   BiLevel
//      Description:
//         Removed IsServiceModeRequested() method that is redundant
//		   to the Post::IsServiceModeRequested() method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 
//@ Usage-Classes
#include "Sys_Init.hh"
//@ End-Usage

class InitConditions {
 
  public:
    static void    Initialize(void);

    static Boolean  IsServiceRequired(void);

    static void     SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL,
			      const char*       pPredicate = NULL);
  private:
    InitConditions();        //declared only
    ~InitConditions();       //declared only

    InitConditions(const InitConditions&);  // declared only
    void operator=(const InitConditions&);  // declared only
 
    //@ Data-Member:    ServiceRequired_
    //  Contains the processor's "service required" state. 
    static Boolean ServiceRequired_;

};
 

#endif  // InitConditions_HH
