#ifndef  FlashMem_HH
#define	FlashMem_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: FlashMem.hh - Provides interface to an entire Flash
//		memory of a Sigma CPU board as a new executable boot image
//      is downloaded into the flash memory.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/FlashMem.hhv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  05-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================


#include "DownloadTypes.hh"
#include "Semaphore.hh"
#include "BufQueue.hh"
#include "FlashMemCons.hh"
#include "FlashHardware.hh"
#include "DownloadFault.hh"
#include "cktable.hh"

class BufQueue;

class FlashMem 
{
  public:
    FlashMem(void);
    ~FlashMem(void);
	void task(void);
	static void Task(void);
	DownloadError initSerialNumber(void);
	void reInit(EepromStruct *pHostTbl);
	Boolean matchCheckSumTbl( EepromStruct *hostCheckSum );
	void completeNetDataChkSumTbl(void);
	void buildChkSumFromFlashMem(void);
    static void SoftFault(const FaultType softFaultID,
		const Uint32      lineNumber, const char*       pFileName  = NULL, 
		const char*       pPredicate = NULL);
#ifdef SIGMA_UNIT_TEST
	void setNumChecksums(Int32);
	void setHostNumChecksums(Int32);
	void setpStartAddress(void);
	void setEndImageAddr(void);
	void setLength(Uint32 num);
	void setAdditiveChecksum(Uint32 num);
	DownloadError callWriteBlockToFlash(NetworkBuf *pBuf);
#endif // SIGMA_UNIT_TEST

  protected:

  private:
    FlashMem(const FlashMem&);			// not implemented...
    void   operator=(const FlashMem&);	// not implemented...
	DownloadError writeBlockToFlash_(NetworkBuf *pBuf);

    //@ Data-Member:  RDataLock_
    //  Semaphore that is used to serialize the access to the boot image
	//  data block list.
	static Semaphore RDataLock_;

    //@ Data-Member:  startAddr_
    //  the starting address of the  Flash Memory where the boot image
	//  file should be downloaded into.
		Uint32 *startAddr_;

    //@ Data-Member:  endAddr_
    //  An ending address of the  Flash Memory where the download
	//  should initialize.
		Uint32 *endAddr_;

    //@ Data-Member:  endImageAddr_
    //  An ending address of the  Flash Memory where the boot image
	//  file is downloaded into.
		Uint32 *endImageAddr_;

    //@ Data-Member:  writeAddr_
    //  a Word boundary flash memory address to be written next.
		Uint32 *writeAddr_;

	//@Data-Member: checkSumTable_
	// checkSumTable_ contains the checksum values for all the blocks of
	// the down loaded flash memory image.
		EepromStruct checkSumTable_;

	//@Data-Member: hostCheckSumTbl_
	// HostChecksum_ contains the original checksum table
	// downloaded from a download host machine.
		EepromStruct hostCheckSumTbl_;

	//@Data-Member: pCurrentBlock_;
	// Check sum table entry for the current flash memory block being
	// burnt.
	ChecksumStruct *pCurrentBlock_;

    //@ Data-Member:  hardWare_
    //  Direct interface to the flash memory hardware object.
		FlashHardware hardWare_;
};


#endif // FlashMem_HH 
