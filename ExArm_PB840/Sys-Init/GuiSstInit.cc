#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: GuiSstInit - GUI State: SST.
//---------------------------------------------------------------------
//@ Interface-Description
//  The GuiSstInit class is a FsmState used by the GuiFsm finite state
//  machine.  The class represents the state that GUI Task Control
//  enters upon completing a GUI transition to the SST state.  It
//  contains methods for processing events while in this state.
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the GUI SST state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  GuiSstInit is derived from the FsmState class.  It overrides the
//  virtual method activate() to provide state specific behavior
//  when it is activated.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiSstInit.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 17-JUN-1997 DR Number:  1902
//      Project:   Sigma   (R8027)
//      Description:
//         Removed receiveBdReady() as "dead" code since GuiSstInit
//         is a transitory state only.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "GuiSstInit.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "GuiFsm.hh"
#include "GuiSst.hh"
//TODO E600 port Reset #include "Reset.hh"
#include "TaskControlAgent.hh"
#include "TaskAgentManager.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiSstInit [default constructor]
//
//@ Interface-Description
//  GuiSstInit default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiSstInit::GuiSstInit()
{
    CALL_TRACE("GuiSstInit()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiSstInit [destructor]
//
//@ Interface-Description
//  GuiSstInit class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiSstInit::~GuiSstInit()
{
    CALL_TRACE("~GuiSstInit()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

const char*
GuiSstInit::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "GuiSstInit";
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with
//  this GuiOnline state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the ONLINE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SigmaState
GuiSstInit::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_INIT;
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  This method is called upon entry into the SST-INIT state.  It 
//  immediately begins a transition to the SST state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
GuiSstInit::activate(void) const
{
    CALL_TRACE("activate(void)");
    GuiFsm::GetGuiSstState().startTransition();
    // $[TI1]
}
 
 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
GuiSstInit::SoftFault(const SoftFaultID  softFaultID,
                     const Uint32       lineNumber,
                     const char*        pFileName,
                     const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_GUISSTINIT,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




