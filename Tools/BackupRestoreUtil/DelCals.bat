@Echo OFF
rem ****** Create BD temp text file for ftp commands  ***************
echo anonymous>>temp.txt
echo abc@covidien.com>>temp.txt
echo cd ProgramFiles>>temp.txt
echo delete AppNovRam.dat>>temp.txt
echo delete PostNovRam.dat>>temp.txt
echo delete SnAddrFlash.dat>>temp.txt
echo delete AppNovRam.crc>>temp.txt
echo delete PostNovRam.crc>>temp.txt
echo delete SnAddrFlash.crc>>temp.txt
echo disconnect>>temp.txt
echo quit>>temp.txt

rem ****** Action *********************************
ftp -i -s:temp.txt 192.168.0.3

del temp.txt

:EXIT
rem *********** Script Ends *************************************