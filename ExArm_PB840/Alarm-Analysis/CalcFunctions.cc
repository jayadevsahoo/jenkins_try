#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CalcFunctions - Return values used by constraints and VHMs.
//---------------------------------------------------------------------
//@ Interface-Description
// This class defines static methods used by objects derived from pure virtual
// base class Constraint and objects derived from pure virtual base class
// ViolationHistoryManager.  The methods return values used by these classes.
// Some methods calculate a value based on a setting, and others are hardcoded
// to always return the same value.
//---------------------------------------------------------------------
//@ Rationale
// Some objects derived from Constraint require literal values, while others
// require a value which is calculated dynamically at runtime.  The same is
// true for objects derived from ViolationHistoryManager.  To keep the design
// simple, all of these objects take as input for construction, a pointer to a
// CalcFunction method.  Some CalcFunctions simply return a literal while
// others calculate a value based on values determined at runtime.
//---------------------------------------------------------------------
//@ Implementation-Description
// Each method returns a value; some are hardcoded while others are determined
// dynamically at runtime.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
// Default constructors and destructor are privately delcared so an object of
// this class is never instantiated.
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/CalcFunctions.ccv   25.0.4.0   19 Nov 2013 13:51:20   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct    Date: 05/22/95         DR Number:   7
//      Project:   Sigma   (R8027)
//      Description:
//         Added method Return3Sec() for added battery alarms as per changes 
//         to Non-Technical Alarm text and structure as per changes to SRS.
//  
//   Revision 003   By:   hct    Date: 01/18/96         DR Number:   659
//      Project:   Sigma   (R8027)
//      Description:
//         Use new interval timer provided by OsTimeStamp.hh.  Since it is
//         based on milliseconds, change over to using milliseconds. 
//  
//   Revision 004   By:   hct    Date: 12/16/97         DR Number:  2688
//      Project:   Sigma   (R8027)
//      Description:
//         Add hysteresis to LSP alarms.
//  
//   Revision 005   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 006   By:   hct    Date: 08/27/99         DR Number:  5513
//      Project:   NewAlarms
//      Description:
//         Added new methods to model escalation changes in patient data alarms.
//  
//   Revision 007   By:   hct    Date: 08/11/00         DR Number:  5313
//      Project:   PAV
//      Description:
//         Added new methods for PAV alarms.
//  
//  Revision: 008   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 009   By: gdc    Date: 18-Feb-2009    SCR Number: 6477
//  Project:  840S
//  Description:
//      Corrected Pvent escalation/deescalation criteria for Ta=OFF.
//
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "CalcFunctions.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#ifdef SIGMA_GUI_CPU
#  include "AcceptedContextHandle.hh"
#endif

//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return1Event
//
//@ Interface-Description
//      Return the value 1 as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return1Event(void)
{ 
  CALL_TRACE("Return1Event(void)");

  return(1);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return2Events
//
//@ Interface-Description
//      Return the value 2 as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return2Events(void)
{ 
  CALL_TRACE("Return2Events(void)");

  return(2);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return3Events
//
//@ Interface-Description
//      Return the value 3 as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return3Events(void)
{ 
  CALL_TRACE("Return3Events(void)");

  return(3);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return4Events
//
//@ Interface-Description
//      Return the value 4 as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return4Events(void)
{ 
  CALL_TRACE("Return4Events(void)");

  return(4);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return5Events
//
//@ Interface-Description
//      Return the value 5 as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return5Events(void)
{ 
  CALL_TRACE("Return5Events(void)");

  return(5);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return10Events
//
//@ Interface-Description
//      Return the value 10 as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return10Events(void)
{ 
  CALL_TRACE("Return10Events(void)");

  return(10);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return1Sec
//
//@ Interface-Description
//      Return 1 second in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return1Sec(void)
{ 
  CALL_TRACE("Return1Sec(void)");

  return(1000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return3Sec
//
//@ Interface-Description
//      Return 3 seconds in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return3Sec(void)
{ 
  CALL_TRACE("Return3Sec(void)");

  return(3000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return5Sec
//
//@ Interface-Description
//      Return 5 seconds in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return5Sec(void)
{ 
  CALL_TRACE("Return5Sec(void)");

  return(5000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return10Sec
//
//@ Interface-Description
//      Return 10 seconds in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return10Sec(void)
{ 
  CALL_TRACE("Return10Sec(void)");

  return(10000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return20Sec
//
//@ Interface-Description
//      Return 20 seconds in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return20Sec(void)
{ 
  CALL_TRACE("Return20Sec(void)");

  return(20000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return30Sec
//
//@ Interface-Description
//      Return 30 seconds in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return30Sec(void)
{ 
  CALL_TRACE("Return30Sec(void)");

  return(30000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return40Sec
//
//@ Interface-Description
//      Return 40 seconds in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return40Sec(void)
{ 
  CALL_TRACE("Return40Sec(void)");

  return(40000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return45Sec
//
//@ Interface-Description
//      Return 45 seconds in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return45Sec(void)
{ 
  CALL_TRACE("Return45Sec(void)");

  return(45000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return75Sec
//
//@ Interface-Description
//      Return 75 seconds in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return75Sec(void)
{ 
  CALL_TRACE("Return75Sec(void)");

  return(75000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return90Sec
//
//@ Interface-Description
//      Return 90 seconds in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return90Sec(void)
{ 
  CALL_TRACE("Return90Sec(void)");

  return(90000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return2Min
//
//@ Interface-Description
//      Return two minutes in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return2Min(void)
{ 
  CALL_TRACE("Return2Min(void)");

  return(120000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return10Min
//
//@ Interface-Description
//      Return ten minutes in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return10Min(void)
{ 
  CALL_TRACE("Return10Min(void)");

  return(600000);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Return15Min
//
//@ Interface-Description
//      Return fifteen minutes in milliseconds as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::Return15Min(void)
{ 
  CALL_TRACE("Return15Min(void)");

  return(900000);
  // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReturnMax60or3ApneaInts
//
//@ Interface-Description
//      Return MAX(60 seconds or 3 * accepted ApneaInterval) in milliseconds 
//      as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Uses MIN_VALUE of 60 seconds or Ta since the special case of
// 	Ta = OFF returns a value > 60 seconds. For purposes of alarms, the 
// 	maximum apnea interval is always 60 seconds.
//  $[LC04001]\a\ with Ta=OFF, Pvent alarm de-escalation same as Ta=60
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::ReturnMax60or3ApneaInts(void)
{ 
  CALL_TRACE("ReturnMax60or3ApneaInts(void)");
  Int32 tempInt = 0;

#ifdef SIGMA_GUI_CPU
  const Real32 apneaInterval = AcceptedContextHandle::GetBoundedValue(SettingId::APNEA_INTERVAL).value;

  if ( apneaInterval == SettingConstants::UPPER_ALARM_LIMIT_OFF )
  {
	  tempInt = 3 * 60000;
  }
  else
  {
	  // Query ApneaInterval setting to determine current value and return 
	  // MAX(60 seconds, current value * 3)
	  tempInt = 3 * Int32(apneaInterval);
	  if (tempInt < 60000)
	  {
		tempInt = 60000;
	  }
  }

#else
  CLASS_ASSERTION(FALSE);
#endif

  return(tempInt);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReturnFourApneaIntervals
//
//@ Interface-Description
//      Return the length of (4 * accepted ApneaInterval) in milliseconds
//      as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Uses MIN_VALUE of 60 seconds or Ta since the special case of
// 	Ta = OFF returns a value > 60 seconds. For purposes of alarms, the 
// 	maximum apnea interval is always 60 seconds.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::ReturnFourApneaIntervals(void)
{
  CALL_TRACE("ReturnFourApneaIntervals(void)");
  Int32 tempInt = 0;

#ifdef SIGMA_GUI_CPU
  const Real32 apneaInterval = AcceptedContextHandle::GetBoundedValue(SettingId::APNEA_INTERVAL).value;

  if ( apneaInterval == SettingConstants::UPPER_ALARM_LIMIT_OFF )
  {
	  tempInt = 4 * 60000;
  }
  else
  {
	  // Query ApneaInterval setting to determine current value and return 
	  // current value * 4
	  tempInt = 4 * Int32(apneaInterval);
  }
#else
  CLASS_ASSERTION(FALSE);
#endif

  return(tempInt);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReturnTenApneaIntervals
//
//@ Interface-Description
//      Return the length of (10 * accepted ApneaInterval) in milliseconds 
//      as an Int32.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Uses MIN_VALUE of 60 seconds or Ta since the special case of
// 	Ta = OFF returns a value > 60 seconds. For purposes of alarms, the 
// 	maximum apnea interval is always 60 seconds.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
CalcFunctions::ReturnTenApneaIntervals(void)
{
  CALL_TRACE("ReturnTenApneaIntervals(void)");

  Int32 tempInt = 0;

#ifdef SIGMA_GUI_CPU
  const Real32 apneaInterval = AcceptedContextHandle::GetBoundedValue(SettingId::APNEA_INTERVAL).value;

  if ( apneaInterval == SettingConstants::UPPER_ALARM_LIMIT_OFF )
  {
	  tempInt = 10 * 60000;
  }
  else
  {
	  // Query ApneaInterval setting to determine current value and return
	  // (current value * 10). 
	  tempInt = 10 * Int32(apneaInterval);
  }
#else
  CLASS_ASSERTION(FALSE);
#endif

  return(tempInt);
  // $[TI1]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
CalcFunctions::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::CALCFUNCTIONS, lineNumber,
                          pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

