#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MoreAlarmsTabButton - A specialized version of TabButton which
// is responsible for flashing the alarm bitmap to indicate an overflow
// of the current alarm display.
//---------------------------------------------------------------------
//@ Interface-Description
// A specialized TabButton which indicates whether or not there are
// unviewed current alarms.  The button indicates that there are
// unviewed current alarms by flashing its bitmap (icon).  This class
// interacts with the Alarms subsystem to determine whether or not there
// are unviewed current alarms.
//
// The leave() method is inherited from Button and is defined to override
// some automatic coloring behavior in Button.  updateDisplay() is
// inherited from TabButton and is called anytime the alarm bitmap needs
// to change its flashing state.
//---------------------------------------------------------------------
//@ Rationale
// This specialized TabButton handles the unique display requirements of
// the More Alarms screen select button.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this button is in the up state, it displays a steady
// (non-blinking) icon if there are two or fewer current alarms, or a
// flashing icon if there are three or greater current alarms.  In the
// down state, this button always displays a steady icon.
//
// This class provides an updateDisplay() method which decides whether
// or not to flash the button icon.  The updateDisplay() method fetches
// the number of current alarms from the Alarms subsystem.
//
// This class also redefines the virtual downHappened_() method in the
// parent in order to force the icon to a steady state when the button
// goes from the up state to the down state.  Also, the upHappened_()
// method is redefined to force evaluation of the icon state when the
// More Alarms subscreen is dismissed.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// n/a
//---------------------------------------------------------------------
//@ Invariants
// n/a
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/MoreAlarmsTabButton.ccv   25.0.4.0   19 Nov 2013 14:08:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 004  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  sah   Date:  28-Jul-1997   DCS Number:  2320
//	Project:  Sigma (R8027)
//	Description:
//      Now using 'AlarmStateManger' for retrieving the list of current
//		alarms.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "MoreAlarmsTabButton.hh"

//@ Usage-Classes
#include "AlarmAnnunciator.hh"
#include "SortIterC_AlarmUpdate.hh"
#include "AlarmStateManager.hh"
#include "Image.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BITMAP_X_ = 18;
static const Int32 BITMAP_Y_ = 3;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MoreAlarmsTabButton  [Constructor]
//
//@ Interface-Description
// Constructor.  Passed the following arguments:
// >Von
//	xOrg			The X coordinate of the button.
//	yOrg			The Y coordinate of the button.
//	width			Width of the button.
//	height			Height of the button.
//	buttonType		Type of the button, either Button::LIGHT,
//					Button::LIGHT_NON_LIT or Button::DARK.
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					Button::ROUND or Button::SQUARE.
//	title			The string displayed as the button's title.
//	pSubScreen		The subscreen which is selected by pressing this button.
//>Vof
//---------------------------------------------------------------------
//@ Implementation-Description
// Passes constructor arguments through to the TabButton constructor.
// Initializes all members and initializes the positions of the button
// bitmap (icon).  Adds the button bitmap to the parent container for
// display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MoreAlarmsTabButton::MoreAlarmsTabButton(Uint16 xOrg, Uint16 yOrg,
								 Uint16 width, Uint16 height,
								 Button::ButtonType buttonType,
								 Uint8 bevelSize,
								 Button::CornerType cornerType,
								 StringId title, SubScreen *pSubScreen) :
	TabButton(xOrg, yOrg, width, height, buttonType, bevelSize,
							cornerType, title, pSubScreen),
	bitmap_(Image::RMonoAlarmIcon)
{
	CALL_TRACE("MoreAlarmsTabButton::MoreAlarmsTabButton(xOrg, yOrg, width, "
				"height, buttonType, bevelSize, cornerType, title, pSubScreen, shortcutId)");

	//
	// Override default "top line" box color to match the background color
	// of the MoreAlarmsSubScreen.
	//
	setTopLineBoxColor_(Colors::BLACK);

	//
	// Position, then add bitmap to this button.
	//
	bitmap_.setX(BITMAP_X_);
	bitmap_.setY(BITMAP_Y_);
	addLabel(&bitmap_);			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MoreAlarmsTabButton  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MoreAlarmsTabButton::~MoreAlarmsTabButton(void)
{
	CALL_TRACE("MoreAlarmsTabButton::~MoreAlarmsTabButton(void)");

	// Do nothing.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: leave [public, virtual]
//
//@ Interface-Description
// Called with the given 'x' and 'y' coordinates when the user drags
// their finger off of this button.  We make sure that the alarm bitmap
// is displayed correctly.
//---------------------------------------------------------------------
//@ Implementation-Description
// This redefines the Button::leave() method to handle the special case
// where the the bitmap (icon) is flashing.  The "normal" case where the
// selected button is deselected back into the "up" state is handled by
// upHappened_().  This handles the "weird" case where the button pops
// back to the "up" state from the "touched was up" state (i.e.  the
// user drags their finger onto then off of the button).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsTabButton::leave(Int x, Int y)
{
	CALL_TRACE("MoreAlarmsTabButton::leave(x, y)");

	//
	// Do the default "leave" behavior....
	//
	TabButton::leave(x, y);

	//
	// ...  then make sure the alarm bitmap (icon) is in the proper
	// display state.  This is needed to override the default behavior
	// of Button where it sets its contents color to BLACK when the
	// button pops up.
	//
	updateDisplay();									// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay [public, virtual]
//
//@ Interface-Description
// Decides whether or not to display a steady or a flashing button bitmap
// (icon), based on the number of current alarms.  This method redefines the
// base class virtual function TabButton::updateDisplay().
//---------------------------------------------------------------------
//@ Implementation-Description
// This method is called in response to an alarm update message.  If
// this button is currently in the "up" state, then check the number of
// current alarms.  If there are more than two current alarms, set the
// alarm bitmap to the flashing state.  Otherwise, if there are two or
// fewer current alarms, set the alarm bitmap to the steady
// (non-flashing) state.
//
// If the button is in the "down" state, the bitmap should not be
// flashing.
//
// $[01069] More Alarms button indicates if there's more alarms by ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsTabButton::updateDisplay(void)
{
	CALL_TRACE("MoreAlarmsTabButton::updateDisplay(void)");

	if (UP == getState_())
	{													// $[TI1]
		Int32 numCurrentAlarms = 0;

		//
		// Fetch current alarm list from AlarmAnnunciation subsystem, then
		// determine how many entries there are in the list.
		//
		SortIterC_AlarmUpdate alarmUpdates(AlarmStateManager::GetCurrAlarmList());
		numCurrentAlarms = alarmUpdates.getNumItems();

		//
		// If there are more than two alarms, then blink the button icon.
		//
		if (numCurrentAlarms > 2)
		{												// $[TI2]
			bitmap_.setImage(Image::RBlinkingAlarmIcon);
		}
		else
		{												// $[TI3]
			bitmap_.setImage(Image::RMonoAlarmIcon);
		}
	}													// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downHappened_ [protected, virtual]
//
//@ Interface-Description
// Set the button bitmap (icon) to the steady (non-flashing) state when the
// button is pressed down.  The 'isByOperatorAction' parameter is TRUE when
// this method is called as a result of a user action, but it is not used here
// during display processing.
//---------------------------------------------------------------------
//@ Implementation-Description
// Unconditionally set the button bitmap to the steady (non-flashing)
// state.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsTabButton::downHappened_(Boolean isByOperatorAction)
{
	CALL_TRACE("MoreAlarmsTabButton::downHappened_(isByOperatorAction)");

	TabButton::downHappened_(isByOperatorAction);
	bitmap_.setImage(Image::RMonoAlarmIcon);			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: upHappened_ [protected, virtual]
//
//@ Interface-Description
// Force a call to 'updateDisplay()' to evaluate whether or not to set the
// button bitmap (icon) to the flashing state.  The 'isByOperatorAction'
// parameter is TRUE when this method is called as a result of a user action,
// but it is not used here during display processing.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the updateDisplay() method to determine whether or not to
// set the button bitmap to a flashing state.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsTabButton::upHappened_(Boolean isByOperatorAction)
{
	CALL_TRACE("MoreAlarmsTabButton::upHappened_(isByOperatorAction)");

	TabButton::upHappened_(isByOperatorAction);

	SAFE_CLASS_ASSERTION(UP == getState_());
	updateDisplay();									// $[TI1]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
MoreAlarmsTabButton::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("MoreAlarmsTabButton::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, MOREALARMSTABBUTTON,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

