#include "stdafx.h"
//======================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//======================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NovRamXaction - NOVRAM Transaction Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is responsible for managing the transaction tasks on the
//  two CPUs, and the transactions, themselves.  There are data items that
//  are stored in the BD's NOVRAM that are needed for display purposes on
//  the GUI CPU.  This class's task loop and transmission methods are used
//  to transmit updated BD data items to the GUI so as to keep the most
//  recent state of these data items available for display.
//
//  To send a message from one CPU to the other, the 'Send()' method
//  is to be called, passing the ID of the data item to be sent.  However,
//  it is not legal for the GUI version of 'Send()' to be passed any ID 
//  other than 'NovRamUpdateManager::NUM_BD_UPDATE_ITEMS'.  This ID is then
//  transmitted to the BD informing the BD to transmit all of the monitored
//  data items to the GUI.  Correspondingly, the BD transmits this
//  'NovRamUpdateManager::NUM_BD_UPDATE_ITEMS' message to the GUI when all
//  of the requested data items have been transmitted, thereby closing the
//  loop.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to provide a mechanism to transmit certain
//  NOVRAM data items from the BD to the GUI CPU.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The BD data that is tracked by the GUI is stored in (volatile) RAM, as
//  opposed to the GUI's NOVRAM.  This means that these data items must be
//  transmitted over to the GUI before any of these data items are displayed.
//  Since this data includes EST/SST results, which affect the start up of
//  the GUI Task and others, this data needs to be available when the GUI
//  system  transitions to its final startup state (e.g., 'ONLINE').
//  Furthermore, the BD CPU doesn't "know" the point that GUI is ready to
//  receive the initial data, without an explicit message from the GUI.
//  Therefore, the GUI CPU sends a request for all of the data items to
//  the BD CPU, when a change-state to the 'INIT' or 'INOP' state is pending.
//  When all of the initial data items are received by the GUI, the GUI will
//  then notify the task-control system that this task is ready to transition
//  to that 'INIT' or 'INOP' state.  This has the affect of providing a
//  guarantee to all of the other tasks that the NOVRAM data is available
//  when transitioning to the 'ONLINE' state.
//
//  When the GUI CPU receives messages from the BD CPU, the messages are
//  NOT put into the RAM manager ('VolatileMemory') via the callback method
//  that is registered with Network-Apps.  The callback method is run as
//  part of the Network-Apps Task, and, therefore, should ensure that the
//  Network-Apps Task is not held up on any resources.  Therefore, the
//  callback method dynamically allocates some memory space, copies in
//  the incoming message, then posts this message to the NovRamXaction Task.
//  Thereby ensuring that only the NovRamXaction Task, and not the Network-
//  Apps Task, is vulnerable to the protective semaphores used in the
//  management of the "BD" data area.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamXaction.ccv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 013   By:  gdc    Date:  22-Feb-2007    SCR Number: 5885
//  Project:  RESPM
//  Description:
//	Doubled size of diagnostic code log buffer pool to help obviate 
//  infrequent out of buffer condition during vent startup.
//
//  Revision: 012   By:  sah    Date:  29-Jul-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//	Added GUI development only code for locating static memory in
//      extended memory section, thereby allowing development downloads.
//
//  Revision:  011  By:  sah    Date:  11-May-1999    DCS Number: 5387
//  Project:   CostReduce
//  Description:
//	No longer activate alarm when able to fully recover from faulty
//	NOVRAM.
//
//  Revision: 010  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision:  009  By:  sah    Date:  14-May-1998    DCS Number: 5041
//  Project:  Color Screens
//  Description:
//	Incorporate change to 'CodeLogEntry::isValid()' interface.  Also,
//	eliminated call to 'isClear()', because its not needed (only
//	non-"clear" entries are transmitted).
//
//  Revision: 008    By: syw  Date: 09-APR-1998  DR Number: 5048
//  Project: Sigma (R8027)
//  Description:
//	Eliminate TIMING_TEST code.
//
//  Revision: 007  By:  sah    Date:  29-Sep-1997    DCS Number: 2525
//  Project:  Sigma (R8027)
//  Description:
//	Removed 'ventInopTestInfo_' data item.
//
//  Revision: 006  By:  sah    Date:  11-Sep-1997    DCS Number: 981
//  Project:  Sigma (R8027)
//  Description:
//      Eliminate the following compiler warning:
//"NovRamXaction.cc", line 363 pos 26; (W) #C0177-D variable "EVENT_ID" was declared but never referenced
//        const BkEventName  EVENT_ID =
//
//  Revision: 005  By:  sah    Date:  04-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Changed the name of the Communication Diagnostic Log to System
//	Information Log.
//
//  Revision: 004  By:  sah    Date:  07-Aug-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//	Changed all occurences of the 'NUM_UPDATE_ITEMS' constant, to the
//	new 'NUM_BD_UPDATE_ITEMS' constant.
//
//  Revision: 003  By:  sah    Date:  23-Jun-1997    DCS Number: 1902
//  Project:  Sigma (R8027)
//  Description:
//	While investigating this DCS, I discovered the possibility of
//	a case where a state change occurs while communications is
//	down, then another occurs when it comes back up, thus not
//	leaving the GUI in a "sync'ed" state.  That is, the 'Send()'
//	would fail with the first state change, and another 'Send()'
//	would not be issued with the second.
//
//  Revision: 002  By:  sah    Date:  15-May-1997    DCS Number: 2112
//  Project:  Sigma (R8027)
//  Description:
//	Adding tracing of '[08010]' and '[08014]'.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================
#include "NovRamXaction.hh"
#include "SigmaState.hh"

#if defined(SIGMA_DEVELOPMENT)
#  include "Ostream.hh"
#endif // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
#include "MsgQueue.hh"
#include "Task.hh"
#include "TaskControlQueueMsg.hh"
#include "TaskControlAgent.hh"
#include "TaskMonitor.hh"
#include "TaskMonitorQueueMsg.hh"
#include "NetworkApp.hh"
#include "NovRamManager.hh"
#include "NovRamUpdateManager.hh"
#include "NonVolatileMemory.hh"
#include "Array_CodeLogEntry_MAX_CODE_LOG_ENTRIES.hh"
#include "Array_ResultTableEntry_MAX_TEST_ENTRIES.hh"
#include "AppContext.hh"
#include "MemoryHandle.hh"
#include <memory.h>

#if defined(SIGMA_GUI_CPU)
#  include "ChangeStateMessage.hh"
#  include "VolatileMemory.hh"
#  include "BufferPool.hh"
#endif // defined(SIGMA_GUI_CPU)
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

#if defined(SIGMA_GUI_CPU)

#  if defined(SIGMA_DEVELOPMENT)
     // for development only, locate this module's static data into
     // development unit's extended memory...
#    pragma options -NZstacks
#  endif //defined(SIGMA_DEVELOPMENT)

// NOTE:  must include space for one long word for each buffer, hence the
//        adding in of '+ sizeof(Uint)'...
static const Uint  DIAG_LOG_BUFFER_WORDS_ =
    (sizeof(FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)) + sizeof(Uint) +
    		sizeof(Uint) - 1) / sizeof(Uint);

// due to potential communication failures and restarts during initialization
// the number of buffers required is 4 times the number of diagnostic logs
static const Uint  DIAG_LOG_BUFFER_COUNT_ =
	      (4 * (NovRamUpdateManager::HIGH_DIAG_LOG_VALUE -
		    NovRamUpdateManager::LOW_DIAG_LOG_VALUE  + 1));

static Uint  PDiagLogBufferPoolMemory_[
		    (::DIAG_LOG_BUFFER_COUNT_ * ::DIAG_LOG_BUFFER_WORDS_)
				      ];

static Uint  PDiagLogBufferPoolClass_[
	  ((sizeof(BufferPool) + sizeof(Uint) - 1) / sizeof(Uint))
				     ];


// NOTE:  must include space for one long word for each buffer, hence the
//        adding in of '+ sizeof(Uint)'...
static const Uint  RESULT_TABLE_BUFFER_WORDS_ =
    (sizeof(FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)) + sizeof(Uint) +
		  sizeof(Uint) - 1) / sizeof(Uint);

// multiply two times the total number of test result tables to get the number
// of desired buffers...
static const Uint  RESULT_TABLE_BUFFER_COUNT_ =
	      (2 * (NovRamUpdateManager::HIGH_RESULT_TABLE_VALUE -
		    NovRamUpdateManager::LOW_RESULT_TABLE_VALUE  + 1));

static Uint  PResultTableBufferPoolMemory_[
	      (::RESULT_TABLE_BUFFER_COUNT_ * ::RESULT_TABLE_BUFFER_WORDS_)
					  ];

static Uint  PResultTableBufferPoolClass_[
	    ((sizeof(BufferPool) + sizeof(Uint) - 1) / sizeof(Uint))
					 ];


// NOTE:  must include space for one long word for each buffer, hence the
//        adding in of '+ sizeof(Uint)'...
static const Uint  MISC_DATA_BUFFER_WORDS_ =
	  (NovRamXaction::MAX_MISC_DATA_ITEM_SIZE + sizeof(Uint) +
			    sizeof(Uint) - 1);

// multiply two times the total number of miscellaneous data items to get the
// number of desired buffers...
static const Uint  MISC_DATA_BUFFER_COUNT_ =
	      (2 * (NovRamUpdateManager::HIGH_MISC_DATA_VALUE -
		    NovRamUpdateManager::LOW_MISC_DATA_VALUE  + 1));

static Uint  PMiscDataBufferPoolMemory_[
		      (::MISC_DATA_BUFFER_COUNT_ * ::MISC_DATA_BUFFER_WORDS_)
				       ];

static Uint  PMiscDataBufferPoolClass_[
	    ((sizeof(BufferPool) + sizeof(Uint) - 1) / sizeof(Uint))
				      ];

#endif // defined(SIGMA_GUI_CPU)


//=====================================================================
//
//  Static Data Members...
//
//=====================================================================

#if defined(SIGMA_GUI_CPU)

ChangeStateMessage*  NovRamXaction::PChangeStateMsg_ = NULL;

BufferPool&  NovRamXaction::RDiagLogBufferPool_ =
			       *((BufferPool*)::PDiagLogBufferPoolClass_);

BufferPool&  NovRamXaction::RResultTableBufferPool_ =
			    *((BufferPool*)::PResultTableBufferPoolClass_);

BufferPool&  NovRamXaction::RMiscDataBufferPool_ =
			       *((BufferPool*)::PMiscDataBufferPoolClass_);

#endif // defined(SIGMA_GUI_CPU)
 

//=====================================================================
//
//  Static Functions...
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  DoInfiniteLoop_()  [static]
//
//@ Interface-Description
//  Used as a parameter to an infinite loop statement.
//---------------------------------------------------------------------
//@ Implementation-Description
//  By using this function in the infinite loop below, I avoid a warning --
//  pretty anal-retentive, huh?
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (TRUE)
//@ End-Free-Function
//=====================================================================

inline Boolean
DoInfiniteLoop_(void)
{
  return(TRUE);
}   // $[TI1]


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()  [static]
//
//@ Interface-Description
//  Initialize the static member of this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NovRamXaction::Initialize(void)
{
  CALL_TRACE("Initialize()");

#if defined(SIGMA_GUI_CPU)
  SAFE_CLASS_ASSERTION((sizeof(::PDiagLogBufferPoolClass_) >=
  							sizeof(BufferPool)));
  new (&RDiagLogBufferPool_) BufferPool(
				  ::NOVRAM_DIAG_LOG_BUFFER_MT,
				  ::PDiagLogBufferPoolMemory_,
				  sizeof(::PDiagLogBufferPoolMemory_),
				  (::DIAG_LOG_BUFFER_WORDS_ * sizeof(Uint))
				       );

  SAFE_CLASS_ASSERTION((sizeof(::PResultTableBufferPoolClass_) >=
  							sizeof(BufferPool)));
  new (&RResultTableBufferPool_) BufferPool(
				::NOVRAM_RESULT_TABLE_BUFFER_MT,
				::PResultTableBufferPoolMemory_,
				sizeof(::PResultTableBufferPoolMemory_),
				(::RESULT_TABLE_BUFFER_WORDS_ * sizeof(Uint))
				      	   );

  SAFE_CLASS_ASSERTION((sizeof(::PMiscDataBufferPoolClass_) >=
  							sizeof(BufferPool)));
  new (&RMiscDataBufferPool_) BufferPool(
				  ::NOVRAM_MISC_DATA_BUFFER_MT,
				  ::PMiscDataBufferPoolMemory_,
				  sizeof(::PMiscDataBufferPoolMemory_),
				  (::MISC_DATA_BUFFER_WORDS_ * sizeof(Uint))
					);
#endif // defined(SIGMA_GUI_CPU)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskDriver()
//
//@ Interface-Description
//  Task loop for the NovRam Transaction.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NovRamXaction::TaskDriver(void)
{
	CALL_TRACE("TaskDriver()");

	//This is where BD and GUI NovRAMs sync with each other.

  switch (NonVolatileMemory::InitializationStatusId_)
  {
  case NovRamStatus::ITEM_RECOVERED :
  case NovRamStatus::ITEM_VERIFIED :		// $[TI1]
    // do nothing; only report catastrophic failures...
    break;

  case NovRamStatus::ITEM_INITIALIZED :		// $[TI2]
    {
#if defined(SIGMA_PRODUCTION)
      // application NOVRAM initialization failure...
      const BkEventName  EVENT_ID =
#  if defined(SIGMA_GUI_CPU)
				::BK_GUI_NOVRAM_CHECKSUM;
#  elif defined(SIGMA_BD_CPU)
				::BK_BD_NOVRAM_CHECKSUM;
#  endif // defined(SIGMA_GUI_CPU)

      // log a diagnostic into the System Diagnostic Log indicating that
      // there was a problem detected during the intialization of NOVRAM,
      // and notify Safety-Net of the failure...
      Background::ReportBkEvent(EVENT_ID);
#else
      // do nothing for non-PRODUCTION runs...
#endif // defined(SIGMA_PRODUCTION)
    }   // end of block...
    break;

  default :
    // invalid initialization id...
    AUX_CLASS_ASSERTION_FAILURE(NonVolatileMemory::InitializationStatusId_);
    break;
  };
  			
  // register for reception of NOVRAM message; this is done here instead of
  // in 'Initialize()' so that 'PersistentObjs::Initialize()' can be called
  // before 'NetworkApps::Initialize()'...
  NetworkApp::RegisterRecvMsg(::BD_NOVRAM_DATA,
			      NovRamXaction::ReceiveXaction_);

  static AppContext  AppContext_;

  AppContext_.setCallback(NovRamXaction::ChangeStateCallback_);

  MsgQueue  novRamXactionQ(NOVRAM_XACTION_Q);

  Int32  qWord;
  Int32  rval;

  InterTaskMessage  msg;

  while (::DoInfiniteLoop_())
  {   // $[TI3] -- loop forever; this path is ALWAYS taken...
    rval = novRamXactionQ.pendForMsg(qWord);
    AUX_CLASS_ASSERTION((rval == Ipc::OK), rval);

    msg = *((struct InterTaskMessage*)&qWord);

    switch (msg.msgId)
    {
    case TaskControlQueueMsg::TASK_CONTROL_MSG :
    case TaskMonitorQueueMsg::TASK_MONITOR_MSG :	// $[TI3.1]
      // the received message is a task-control or task-monitory message,
      // therefore activate the dispatch of this message...
      AppContext_.dispatchEvent(qWord);
      break;

    case NovRamXaction::DATA_UPDATE :			// $[TI3.2]
      {
#if defined(SIGMA_GUI_CPU)
	if (msg.msgData == NovRamUpdateManager::NUM_BD_UPDATE_ITEMS)
	{   // $[TI3.2.1]
	  // BD completed sending all of the data items...
	  if (NovRamXaction::PChangeStateMsg_ != NULL)
	  {   // $[TI3.2.1.1] -- allow transition to stored state...
	    TaskControlAgent::ReportTaskReady(
					  *NovRamXaction::PChangeStateMsg_
					     );

	    // deallocate, and uninitialize, the static state-change pointer...
	    delete NovRamXaction::PChangeStateMsg_;
	    NovRamXaction::PChangeStateMsg_ = NULL;
	  }   // $[TI3.2.1.2] -- already transitioned to final state...
	}
	else
	{   // $[TI3.2.2]
	  // 'msg.msgData' contains a pointer to the block of received
	  // data...
	  NovRamXaction::ReceiveUpdate_(MemoryHandle::GetMemoryPtr(msg.msgData));
	}
#elif defined(SIGMA_BD_CPU)
	const NovRamUpdateManager::UpdateTypeId  UPDATE_ID =
			    (NovRamUpdateManager::UpdateTypeId)msg.msgData;

	if (UPDATE_ID < NovRamUpdateManager::NUM_BD_UPDATE_ITEMS)
	{   // $[TI3.2.3] -- one of BD NOVRAM's items have been updated...
	  // send notification of the requested update; this is done
	  // BEFORE, rather than after, the send, so that I don't
	  // need another semaphore protecting the update flag...
	  NovRamUpdateManager::UpdateCompleted(UPDATE_ID);
	}   // $[TI3.2.4] -- GUI is requesting a "sync" transaction...

	NovRamXaction::Send(UPDATE_ID);
#endif // defined(SIGMA_GUI_CPU)
      }   // end of 'FIRST_APPLICATION_MSG' block...
      break;

    default :
      // invalid message ID...
      AUX_CLASS_ASSERTION_FAILURE(msg.msgId);
      break;
    };

  }  // end of infinite loop...
  
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Send(novRamUpdateId)
//
//@ Interface-Description
//  Send the information represented by 'novRamUpdateId'.  On the GUI
//  CPU, 'NovRamUpdateManager::NUM_BD_UPDATE_ITEMS' is the only valid ID,
//  while all of them are valid for the BD CPU.  When the GUI sends a
//  'NovRamUpdateManager::NUM_BD_UPDATE_ITEMS' message, the BD responds by
//  sending over the current state of ALL of the monitored data items.
//  When the BD sends over a 'NovRamUpdateManager::NUM_BD_UPDATE_ITEMS'
//  message, it is used to inform the GUI that the requested transmission
//  of all of the data items has completed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[08010] -- system's operational time display resolution is one hour.
//  $[08014] -- compressor's operational time display resolution is one hour.
//---------------------------------------------------------------------
//@ PreCondition
//  'novRamUpdateId' is a valid ID
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NovRamXaction::Send(const NovRamUpdateManager::UpdateTypeId novRamUpdateId)
{
  CALL_TRACE("Send(novRamUpdateId)");


  void*  pXmitData       = NULL;
  Int    sizeofXmitBlock = 0;

#if defined(SIGMA_BD_CPU)
  NonVolatileMemory*  pNonVolatileMemory = NonVolatileMemory::GetAddress();
#endif // defined(SIGMA_BD_CPU)

  switch (novRamUpdateId)
  {
  case NovRamUpdateManager::SYS_INFO_LOG_UPDATE :
  case NovRamUpdateManager::SYSTEM_LOG_UPDATE :
  case NovRamUpdateManager::EST_LOG_UPDATE :
  case NovRamUpdateManager::SST_LOG_UPDATE :
#if defined(SIGMA_BD_CPU)
    {   // $[TI1]
      // reserve a block of memory to do these log transactions...
      static NovRamXaction::LogXmitBlock_  LogBlock_;

      FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)  currLogEntries;

      switch (novRamUpdateId)
      {
      case NovRamUpdateManager::SYSTEM_LOG_UPDATE :	// $[TI1.1]
	pNonVolatileMemory->systemDiagLog_.getCurrEntries(currLogEntries);
	break;
      case NovRamUpdateManager::SYS_INFO_LOG_UPDATE :	// $[TI1.2]
	pNonVolatileMemory->systemInfoLog_.getCurrEntries(currLogEntries);
	break;
      case NovRamUpdateManager::EST_LOG_UPDATE :	// $[TI1.3]
	pNonVolatileMemory->estDiagLog_.getCurrEntries(currLogEntries);
	break;
      case NovRamUpdateManager::SST_LOG_UPDATE :	// $[TI1.4]
	pNonVolatileMemory->sstDiagLog_.getCurrEntries(currLogEntries);
	break;
      default :
	// did someone add a new ID above, but forgot here?
        AUX_CLASS_ASSERTION_FAILURE(novRamUpdateId);
	break;
      };

	  // Took num declaration out of the for loop to get the code to compile
	  Uint16 num; 
      for ( num = 0u;
	   num < ::MAX_CODE_LOG_ENTRIES  &&  !currLogEntries[num].isClear();
	   num++)
      {   // $[TI1.5] -- at least one entry being sent...
	// store the code log entry into the transmission block...
	LogBlock_.arrLogEntries[num] = currLogEntries[num];
      }   // $[TI1.6] -- no entries being sent...

      // identify which log's entries are being transmitted...
      LogBlock_.updateId = (Uint16)novRamUpdateId;

      // store the number of code log entries that are to be transmitted,
      // which may be zero...
      LogBlock_.numOfEntries = num;

      pXmitData       = &LogBlock_;
      sizeofXmitBlock = CalcLogXmitBlockSize_(num);
    }   // end of block ([TI1])...
#elif defined(SIGMA_GUI_CPU)
    // invalid 'novRamUpdateId' for the GUI...
    AUX_CLASS_ASSERTION_FAILURE(novRamUpdateId);
#endif // defined(SIGMA_BD_CPU)
    break;

  case NovRamUpdateManager::EST_RESULT_TABLE_UPDATE :
  case NovRamUpdateManager::SST_RESULT_TABLE_UPDATE :
#if defined(SIGMA_BD_CPU)
    {   // $[TI2]
      // reserve a block of memory to do these table transactions...
      static NovRamXaction::TableXmitBlock_  TableBlock_;

      FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)  currTableEntries;

      Uint16  maxEntries = 0;

      if (novRamUpdateId == NovRamUpdateManager::EST_RESULT_TABLE_UPDATE)
      {   // $[TI2.1]
	pNonVolatileMemory->estResultTable_.getCurrEntries(
						      currTableEntries
							  );
	maxEntries = ::MAX_EST_RESULTS;
      }
      else if (novRamUpdateId == NovRamUpdateManager::SST_RESULT_TABLE_UPDATE)
      {   // $[TI2.2]
	pNonVolatileMemory->sstResultTable_.getCurrEntries(
						      currTableEntries
							  );
	maxEntries = ::MAX_SST_RESULTS;
      }
      else
      {
	// did someone add a new ID above, but forgot here?
        AUX_CLASS_ASSERTION_FAILURE(novRamUpdateId);
      }

      // identify which table's entries are being transmitted...
      TableBlock_.updateId     = (Uint16)novRamUpdateId;
      TableBlock_.numOfEntries = maxEntries;

      ResultEntryData  entryData;

      for (Uint16 num = 0u; num < maxEntries; num++)
      {
	// store the result table entry into the transmission block...
	currTableEntries[num].getResultData(entryData);
	TableBlock_.arrTableEntries[num] = entryData;
      }

      pXmitData       = &TableBlock_;
      sizeofXmitBlock = CalcTableXmitBlockSize_(maxEntries);
    }   // end of block ([TI2])...
#elif defined(SIGMA_GUI_CPU)
    // invalid 'novRamUpdateId' for the GUI...
    AUX_CLASS_ASSERTION_FAILURE(novRamUpdateId);
#endif // defined(SIGMA_BD_CPU)
    break;

  case NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE :
  case NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE :
  case NovRamUpdateManager::EXH_CCT_RESISTANCE_UPDATE :
  case NovRamUpdateManager::INSP_CCT_RESISTANCE_UPDATE :
#if defined(SIGMA_BD_CPU)
    {   // $[TI3]
      // reserve a block of memory to do these misc. data transactions...
      static NovRamXaction::MiscDataXmitBlock_  MiscXmitBlock_;

      switch (novRamUpdateId)
      {
      case NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE :
      case NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE :
	{   // $[TI3.1]
	  CLASS_ASSERTION((sizeof(Uint) <=
				    NovRamXaction::MAX_MISC_DATA_ITEM_SIZE));

	  OperationalTime  operationalMinutes;

	  if (novRamUpdateId == NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE)
	  {   // $[TI3.1.1]
	    pNonVolatileMemory->systemOperationalMinutes_.getData(
							  operationalMinutes
								 );
	  }
	  else
	  {   // $[TI3.1.2]
	    pNonVolatileMemory->compressorOperationalMinutes_.getData(
							  operationalMinutes
								     );
	  }

	  // calculate the operational HOURS from the stored operational
	  // MINUTES (no rounding) -- $[08010] and $[08014]...
	  *((Uint*)MiscXmitBlock_.pDataBlock) =
				    (operationalMinutes.getTimeCount() / 60);

	  MiscXmitBlock_.sizeOfDataBlock = sizeof(Uint);
	}   // end of block ([TI3.1])...
	break;
      case NovRamUpdateManager::INSP_CCT_RESISTANCE_UPDATE :
	{   // $[TI3.2]
	  AUX_CLASS_ASSERTION(sizeof(Resistance) <=
				    NovRamXaction::MAX_MISC_DATA_ITEM_SIZE,
			       sizeof(Resistance));

	  pNonVolatileMemory->inspCctResistance_.getData(
				    *((Resistance*)MiscXmitBlock_.pDataBlock)
						        );
	  MiscXmitBlock_.sizeOfDataBlock = sizeof(Resistance);
	}   // end of block ([TI3.2])...
	break;
      case NovRamUpdateManager::EXH_CCT_RESISTANCE_UPDATE :
	{   // $[TI3.3]

	  AUX_CLASS_ASSERTION(sizeof(Resistance) <=
				    NovRamXaction::MAX_MISC_DATA_ITEM_SIZE,
			       sizeof(Resistance));

	  pNonVolatileMemory->exhCctResistance_.getData(
				    *((Resistance*)MiscXmitBlock_.pDataBlock)
						       );
	  MiscXmitBlock_.sizeOfDataBlock = sizeof(Resistance);
	}   // end of block ([TI3.3])...
	break;
      default :
	// did someone add a new ID above, but forgot here?
        AUX_CLASS_ASSERTION_FAILURE(novRamUpdateId);
	break;
      };

      // identify which data item is being transmitted...
      MiscXmitBlock_.updateId = (Uint16)novRamUpdateId;

      pXmitData       = &MiscXmitBlock_;
      sizeofXmitBlock = sizeof(NovRamXaction::MiscDataXmitBlock_);
    }   // end of block ([TI3])...
#elif defined(SIGMA_GUI_CPU)
    // invalid 'novRamUpdateId' for the GUI...
    AUX_CLASS_ASSERTION_FAILURE(novRamUpdateId);
#endif // defined(SIGMA_BD_CPU)
    break;

  case NovRamUpdateManager::NUM_BD_UPDATE_ITEMS :
    {   // $[TI4]
      // reserve a block of memory to do these command transactions...
      static NovRamXaction::CommandXmitBlock_  CommandBlock_;

#if defined(SIGMA_BD_CPU)
      Uint  updateId;

      for (updateId = 0u; updateId < NovRamUpdateManager::NUM_BD_UPDATE_ITEMS;
	   updateId++)
      {
	// recursively call this method...
	NovRamXaction::Send((NovRamUpdateManager::UpdateTypeId)updateId);
      }
#endif // defined(SIGMA_BD_CPU)

      CommandBlock_.updateId = NovRamUpdateManager::NUM_BD_UPDATE_ITEMS;

      pXmitData       = &CommandBlock_;
      sizeofXmitBlock = sizeof(NovRamXaction::CommandXmitBlock_);
    }   // end of block ([TI4])...
    break;
  
  case NovRamUpdateManager::NUM_UPDATE_ITEMS :
  default :
    // invalid 'novRamUpdateId'...
    AUX_CLASS_ASSERTION_FAILURE(novRamUpdateId);
    break;
  };

  SAFE_AUX_CLASS_ASSERTION((pXmitData != NULL  &&  sizeofXmitBlock > 0),
  			   sizeofXmitBlock);
  //This is an internal network xmit between BD and GUI. If the receiving
  //procesor has a diferent endianness than the sender, endianness compatibility
  //should be provided by converting pXmitData endianness to network. and the
  //receiver needs to convert it back to host
  NetworkApp::XmitMsg(::BD_NOVRAM_DATA, pXmitData, sizeofXmitBlock);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NovRamXaction::SoftFault(const SoftFaultID  softFaultID,
			 const Uint32       lineNumber,
			 const char*        pFileName,
			 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, NOV_RAM_XACTION,
  			  lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ChangeStateCallback_(changeStateMsg)
//
//@ Interface-Description
//  This callback is called by the task-control system to notify each
//  of the tasks of a pending change to a new state.  Until all of the
//  tasks respond back that they are ready to transition to that new
//  state, the task-control system will hold off completing the transition.
//  The BD version of this method always responds back to the task-control
//  mechanism immediately that this task is ready to transition to the
//  new state.  However, this method's GUI version will hold off the
//  transition to either the 'INIT' state or the 'INOP' state, until all
//  of the monitored data items are transmitted from the BD.  Whereas,
//  the other states will be responded to immediately.
//---------------------------------------------------------------------
//@ Implementation-Description
//  On the GUI CPU, when a 'INIT' or 'INOP' notification is received, a
//  copy of the change-state message is stored into dynamically allocated
//  memory, and used by the 'TaskDriver()' method to respond back to the
//  task-control system once all of the data items have been received.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NovRamXaction::ChangeStateCallback_(const ChangeStateMessage& changeStateMsg)
{
  CALL_TRACE("ChangeStateCallback_(changeStateMsg)");

  switch (changeStateMsg.getState())
  {
  case ::STATE_INIT :
  case ::STATE_INOP :		// $[TI1]
#if defined(SIGMA_GUI_CPU)
    // notify the BD to send of ALL of the transaction data items...
    NovRamXaction::Send(NovRamUpdateManager::NUM_BD_UPDATE_ITEMS);

    if (NovRamXaction::PChangeStateMsg_ != NULL)
    {   // $[TI1.2] -- there is a state change currently being processed...
      // free up old state (e.g., 'INIT'), to allow storage of new state
      // (e.g., 'INOP')...
      delete NovRamXaction::PChangeStateMsg_;
    }   // $[TI1.1] -- no state change currently being processed...

    // store a copy of 'changeStateMsg', for later processing...
    NovRamXaction::PChangeStateMsg_ = new ChangeStateMessage(changeStateMsg);
    break;
#elif defined(SIGMA_BD_CPU)
    // fall through...
#endif // defined(SIGMA_GUI_CPU)
  case ::STATE_ONLINE :
  case ::STATE_SERVICE :
  case ::STATE_SST :
  case ::STATE_START :
  case ::STATE_TIMEOUT :	// $[TI2]
    // report back that NovRam is ready to change to the given state...
    TaskControlAgent::ReportTaskReady(changeStateMsg);
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(changeStateMsg.getState());
    break;
  };
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ReceiveXaction_(, pDataBlock, sizeOfDataBlock)
//
//@ Interface-Description
//  This method is registered with Network-Apps to receive
//  '::BD_NOVRAM_DATA' messages.  This method is called when a message
//  from the "other" CPU is received on this CPU.  To ensure that the
//  Network-Apps' task is NOT held up on NOVRAM semaphores, this method
//  forwards the messages on to the NovRamXaction Task (see
//  'ReceiveUpdate_()').
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is run only within the Network-Apps Task's thread.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NovRamXaction::ReceiveXaction_(XmitDataMsgId,
			       void* pDataBlock, Uint sizeOfDataBlock)
{
  CALL_TRACE("ReceiveXaction_(, pDataBlock, sizeOfDataBlock)");

  const NovRamUpdateManager::UpdateTypeId  UPDATE_ID =
		  (NovRamUpdateManager::UpdateTypeId)*((Uint16*)pDataBlock);

  InterTaskMessage  xactionMsg;
  void*             pXferBlock = NULL;

  xactionMsg.msgId = NovRamXaction::DATA_UPDATE;

  // check out the validity of the transmitted block...
  switch (UPDATE_ID)
  {
  case NovRamUpdateManager::SYSTEM_LOG_UPDATE :
  case NovRamUpdateManager::SYS_INFO_LOG_UPDATE :
  case NovRamUpdateManager::EST_LOG_UPDATE :
  case NovRamUpdateManager::SST_LOG_UPDATE :
#if defined(SIGMA_GUI_CPU)
    {   // $[TI1]
      const NovRamXaction::LogXmitBlock_*  pLogBlock =
				  (NovRamXaction::LogXmitBlock_*)pDataBlock;

      AUX_CLASS_ASSERTION((pLogBlock->numOfEntries <= ::MAX_CODE_LOG_ENTRIES),
      			  pLogBlock->numOfEntries);
      SAFE_CLASS_ASSERTION(
	(CalcLogXmitBlockSize_(pLogBlock->numOfEntries) == sizeOfDataBlock)
			  );

      pXferBlock =
	      NovRamXaction::RDiagLogBufferPool_.allocate(sizeOfDataBlock);
    }   // end of block ([TI1])...
#elif defined(SIGMA_BD_CPU)
    // invalid transaction ID for the BD...
    AUX_CLASS_ASSERTION_FAILURE(UPDATE_ID);
#endif // defined(SIGMA_GUI_CPU)
    break;

  case NovRamUpdateManager::EST_RESULT_TABLE_UPDATE :
  case NovRamUpdateManager::SST_RESULT_TABLE_UPDATE :
#if defined(SIGMA_GUI_CPU)
    {   // $[TI2]
      const NovRamXaction::TableXmitBlock_*  pTableBlock =
				 (NovRamXaction::TableXmitBlock_*)pDataBlock;

      AUX_CLASS_ASSERTION((pTableBlock->numOfEntries <= ::MAX_TEST_ENTRIES),
			  pTableBlock->numOfEntries);
      SAFE_CLASS_ASSERTION(
       (CalcTableXmitBlockSize_(pTableBlock->numOfEntries) == sizeOfDataBlock)
			  );

      pXferBlock =
	    NovRamXaction::RResultTableBufferPool_.allocate(sizeOfDataBlock);
    }   // end of block ([TI2])...
#elif defined(SIGMA_BD_CPU)
    // invalid transaction ID for the BD...
    AUX_CLASS_ASSERTION_FAILURE(UPDATE_ID);
#endif // defined(SIGMA_GUI_CPU)
    break;

  case NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE :
  case NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE :
  case NovRamUpdateManager::EXH_CCT_RESISTANCE_UPDATE :
  case NovRamUpdateManager::INSP_CCT_RESISTANCE_UPDATE :
#if defined(SIGMA_GUI_CPU)
    {   // $[TI3]
      SAFE_CLASS_ASSERTION(
	      (sizeof(NovRamXaction::MiscDataXmitBlock_) == sizeOfDataBlock)
			  );

      pXferBlock =
	      NovRamXaction::RMiscDataBufferPool_.allocate(sizeOfDataBlock);
    }   // end of block ([TI3])...
#elif defined(SIGMA_BD_CPU)
    // invalid transaction ID for the BD...
    AUX_CLASS_ASSERTION_FAILURE(UPDATE_ID);
#endif // defined(SIGMA_GUI_CPU)
    break;

  case NovRamUpdateManager::NUM_BD_UPDATE_ITEMS :	// $[TI4]
    xactionMsg.msgData = NovRamUpdateManager::NUM_BD_UPDATE_ITEMS;
    pXferBlock         = NULL;
    break;

  case NovRamUpdateManager::NUM_UPDATE_ITEMS :
  default :
    // invalid transaction ID...
    AUX_CLASS_ASSERTION_FAILURE(UPDATE_ID);
    break;
  };

  if (pXferBlock != NULL)
  {   // $[TI5]

	//TODO E600 removed the scope operator from memcopy
    memcpy(pXferBlock, pDataBlock, sizeOfDataBlock);
	xactionMsg.msgData = MemoryHandle::CreateHandle(pXferBlock);

    // make sure that 'pXferBlock' address doesn't overwrite the message ID...
    AUX_CLASS_ASSERTION((xactionMsg.msgId == NovRamXaction::DATA_UPDATE),
			xactionMsg.msgId);
  }   // $[TI6] -- no transfer block needed for 'NUM_BD_UPDATE_ITEMS' xaction...

  // post the message to the NOVRAM transaction queue...
  MsgQueue::PutMsg(NOVRAM_XACTION_Q, *((Int32*)&xactionMsg));
}


#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ReceiveUpdate_(pXferBlock)
//
//@ Interface-Description
//  This method receives the incoming updates via the NovRamXaction Task's
//  queue.  The 'pXferBlock' parameter points to a dynamically allocated
//  block of memory passed from 'ReceiveXaction_()', and contains the
//  new data to be incorporated.  The data that is received here, is stored
//  into the volatile area used on the GUI CPU to keep track of the state
//  of BD CPU data items.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is run only within the NovRamXaction Task's thread.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
NovRamXaction::ReceiveUpdate_(void* pXferBlock)
{
  CALL_TRACE("ReceiveUpdate_(pXferBlock)");

  const NovRamUpdateManager::UpdateTypeId  UPDATE_ID =
		  (NovRamUpdateManager::UpdateTypeId)*((Uint16*)pXferBlock);

  VolatileMemory*  pVolatileMemory = VolatileMemory::GetAddress();

  switch (UPDATE_ID)
  {
  case NovRamUpdateManager::SYSTEM_LOG_UPDATE :
  case NovRamUpdateManager::SYS_INFO_LOG_UPDATE :
  case NovRamUpdateManager::EST_LOG_UPDATE :
  case NovRamUpdateManager::SST_LOG_UPDATE :
    {   // $[TI1]
      const NovRamXaction::LogXmitBlock_*  pLogBlock =
				  (NovRamXaction::LogXmitBlock_*)pXferBlock;

      FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES)  currLogEntries;

      for (Uint16 num = 0u; num < pLogBlock->numOfEntries; num++)
      {   // $[TI1.1] -- at least one entry received...
	SAFE_AUX_CLASS_ASSERTION((pLogBlock->arrLogEntries[num].isValid()),
				 UPDATE_ID);

	// copy from the transmit block...
	currLogEntries[num] = pLogBlock->arrLogEntries[num];
      }   // $[TI1.2] -- NO entries received...

      // free up allocated block (BEFORE blocking on NOVRAM semaphore)...
      NovRamXaction::RDiagLogBufferPool_.freeThePool(pXferBlock);

      switch (UPDATE_ID)
      {
      case NovRamUpdateManager::SYS_INFO_LOG_UPDATE :	// $[TI1.3]
	// update the volatile memory's copy of the System Information Log...
	pVolatileMemory->systemInfoLog_.updateEntries(currLogEntries);
        break;
      case NovRamUpdateManager::SYSTEM_LOG_UPDATE :	// $[TI1.4]
	// update the volatile memory's copy of the System Diagnostic Log...
	pVolatileMemory->systemDiagLog_.updateEntries(currLogEntries);
        break;
      case NovRamUpdateManager::EST_LOG_UPDATE :	// $[TI1.5]
	// update the volatile memory's copy of the EST Diagnostic Log...
	pVolatileMemory->estDiagLog_.updateEntries(currLogEntries);
        break;
      case NovRamUpdateManager::SST_LOG_UPDATE :	// $[TI1.6]
	// update the volatile memory's copy of the SST Diagnostic Log...
	pVolatileMemory->sstDiagLog_.updateEntries(currLogEntries);
        break;
      default :
	// did someone add a new ID above, but forgot here?
        AUX_CLASS_ASSERTION_FAILURE(UPDATE_ID);
	break;
      };
    }   // end of block ([TI1])...
    break;

  case NovRamUpdateManager::EST_RESULT_TABLE_UPDATE :
  case NovRamUpdateManager::SST_RESULT_TABLE_UPDATE :
    {   // $[TI2]
      const NovRamXaction::TableXmitBlock_*  pTableBlock =
				 (NovRamXaction::TableXmitBlock_*)pXferBlock;

      FixedArray(ResultTableEntry,MAX_TEST_ENTRIES)  currTableEntries;

      for (Uint16 num = 0u; num < pTableBlock->numOfEntries; num++)
      {
	currTableEntries[num].updateResultData(
					  pTableBlock->arrTableEntries[num]
					      );
      }

      // free up allocated block (BEFORE blocking on NOVRAM semaphore)...
      NovRamXaction::RResultTableBufferPool_.freeThePool(pXferBlock);

      if (UPDATE_ID == NovRamUpdateManager::EST_RESULT_TABLE_UPDATE)
      {   // $[TI2.1]
	// update the volatile memory's copy of the EST Result Table...
	pVolatileMemory->estResultTable_.updateEntries(currTableEntries);
      }
      else if (UPDATE_ID == NovRamUpdateManager::SST_RESULT_TABLE_UPDATE)
      {   // $[TI2.2]
	// update the volatile memory's copy of the SST Result Table...
	pVolatileMemory->sstResultTable_.updateEntries(currTableEntries);
      }
      else
      {
	// did someone add a new ID above, but forgot here?
        AUX_CLASS_ASSERTION_FAILURE(UPDATE_ID);
      }
    }   // end of block ([TI2])...
    break;

  case NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE :
  case NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE :
  case NovRamUpdateManager::EXH_CCT_RESISTANCE_UPDATE :
  case NovRamUpdateManager::INSP_CCT_RESISTANCE_UPDATE :
    {   // $[TI3]
      const NovRamXaction::MiscDataXmitBlock_*  pMiscXmitBlock_ =
			    (NovRamXaction::MiscDataXmitBlock_*)pXferBlock;

      //----------------------------------------------------------------
      // NOTE:  currently none of these items need semaphore protection,
      //        therefore the 'pXferBlock' block is used directly here,
      //        and can't be deallocated until this processing is done...
      //----------------------------------------------------------------

      switch (UPDATE_ID)
      {
      case NovRamUpdateManager::SYSTEM_OPER_TIME_UPDATE :	// $[TI3.1]
	// update the volatile memory's copy of the System's Operational
	// Hours...
	pVolatileMemory->systemOperationalHours_ =
				      *((Uint*)pMiscXmitBlock_->pDataBlock);
        break;
      case NovRamUpdateManager::COMPRESSOR_OPER_TIME_UPDATE :	// $[TI3.2]
	// update the volatile memory's copy of the Compressor's Operational
	// Minutes...
	pVolatileMemory->compressorOperationalHours_ =
				      *((Uint*)pMiscXmitBlock_->pDataBlock);
	break;
      case NovRamUpdateManager::EXH_CCT_RESISTANCE_UPDATE :	// $[TI3.3]
	// update the volatile memory's copy of the exh. circuit resistance...
	pVolatileMemory->exhCctResistance_ =
			       *((Resistance*)pMiscXmitBlock_->pDataBlock);
	break;
      case NovRamUpdateManager::INSP_CCT_RESISTANCE_UPDATE :	// $[TI3.4]
	// update the volatile memory's copy of the insp. circuit resistance...
	pVolatileMemory->inspCctResistance_ =
			       *((Resistance*)pMiscXmitBlock_->pDataBlock);
	break;
      default :
	// did someone add a new ID above, but forgot here?
        AUX_CLASS_ASSERTION_FAILURE(UPDATE_ID);
	break;
      };

      // free up allocated block...
      NovRamXaction::RMiscDataBufferPool_.freeThePool(pXferBlock);

      // activate any registered callbacks...
      NovRamUpdateManager::UpdateHappened(UPDATE_ID);
    }   // end of block ([TI3])...
    break;

  case NovRamUpdateManager::NUM_BD_UPDATE_ITEMS :
  case NovRamUpdateManager::NUM_UPDATE_ITEMS :
  default :
    // invalid transaction ID...
    AUX_CLASS_ASSERTION_FAILURE(UPDATE_ID);
    break;
  };
}

#endif // defined(SIGMA_GUI_CPU)
