#include "stdafx.h"
#ifdef SIGMA_SERVER
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TimeTick - VRTX tick interval  Timer
//---------------------------------------------------------------------
//@ Interface-Description
//   Provides general interfaces to the VRTX OS clock tick timer.
//---------------------------------------------------------------------
//@ Rationale
//   Need a high level interface into the finer granularity timer
//   counts.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Uses the VRTX32 OS's ::sc_gtime() function to get the system clock
//   tick.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Download/vcssrc/TimeTick.ccv   10.7   08/17/07 09:49:40   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By: gdc    Date: 09-Aug-99    DR Number: 5499
//  Project:  ATC
//  Description:
//      Removed superfluous "#include" statements causing Solaris compile
//		problems.
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   jhv      Date: 07/28/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TimeTick.hh"
#if	defined(SIGMA_TARGET)
#include "vrtxil.h"
#elif defined(SIGMA_SUN)
#include <time.h>
#endif //defined(SIGMA_TARGET)
#include "stdio.h"

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimeTick(void)
//
//@ Interface-Description
//    TimeTick class constructor. Initializes the timeTick_ value
//    to the current VRTX32 Realtime ticks.
//---------------------------------------------------------------------
//@ Implementation-Description
//     If timeTick_ is zero, the timeTick_count is considered invalid.
//     Set the timeTick_ to non-zero value all the time.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
TimeTick::TimeTick(void)
{
#if	defined(SIGMA_TARGET)
    if (!(timeTick_  = (unsigned long) ::sc_gtime()))     // vrtx ticks
    {
	   timeTick_ = 1;
    }
#else 
time_t  timebuf;

	timeTick_  = 100*Uint32(time(&timebuf));  // UNIX time
#endif //defined(SIGMA_SUN);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimeTick(void)
//
//@ Interface-Description
//		TimeTick Class destructor. Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
TimeTick::~TimeTick(void)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  now(void)
//
//@ Interface-Description
//    Sets the time tick value to the current VRTX time value.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//		Call the ::sc_gtime() VRTX32 function to get the VRTX real time
//      value.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TimeTick::now(void)
{
#if	defined(SIGMA_TARGET)
    timeTick_  = (unsigned long) ::sc_gtime();     // vrtx ticks
#elif defined(SIGMA_SUN)
time_t  timebuf;
	timeTick_  = 100*Uint32(time(&timebuf));  // UNIX time
#endif //defined(SIGMA_SUN);
}


#ifdef SIGMA_DEVELOPMENT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: diffTime(const TimeTick &otherTime)
//
//@ Interface-Description
//		Returns the time difference between this TimeTick object and the
//      passed in TimeTick object. The passed in TimeTick object has to
//      be always more recent than the this time tick object for 
//      the diffTick method to return  a correct time difference between
//      two TimeTick objects. The unit of time difference returned is
//      in millisecond.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If there was wrap around in the VRTX time tick counter between
//    these two timeTick values were set, the more recent time tick
//    counter value will be smaller than the old one.
//    The VRTX time tick counter wraps around  every 497 days.
//---------------------------------------------------------------------
//@ PreCondition
//    (isValidTime() && otherTime.isValidTime());
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
unsigned long 
TimeTick::diffTime(const TimeTick &otherTime)
{
	CLASS_PRE_CONDITION(isValidTime() && otherTime.isValidTime());

	if ( timeTick_ <=  otherTime.timeTick_ )
	{
		return( otherTime.timeTick_ - timeTick_ );
	}
	else
	{
		return( WRAP_AROUND_TICK - timeTick_ + otherTime.timeTick_ );
	}
}
#endif // SIGMA_DEVELOPMENT


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPassedTime(void)
//
//@ Interface-Description
//		Returns the time difference between this timeTick and the
//		the current time in 10 millisecond unit.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The VRTX time tick is updated every 10 milli second.
//      On Unix, time of the day clock is updated every second.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
unsigned long 
TimeTick::getPassedTime(void)
{
unsigned long currentTick;

	CLASS_PRE_CONDITION(isValidTime());

#if	defined(SIGMA_TARGET)
	currentTick = (Int32) ::sc_gtime();
#elif defined(SIGMA_SUN)
time_t  timebuf;

	currentTick = 100*(time(&timebuf));  // UNIX time returns 1 second
										 // unit, make it to be 10 ms unit.
#endif //defined(SIGMA_SUN);
	if ( currentTick >= timeTick_ )
	{
		return( currentTick - timeTick_ );
	}
	else
	{
		return( WRAP_AROUND_TICK - timeTick_ + currentTick + 1 );
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TimeTick::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  DownloadFault::SoftFault(softFaultID, DOWNLOAD,
  	TIMETICK_CLASS, lineNumber, pFileName, pPredicate);
}

#endif // SIGMA_SERVER
