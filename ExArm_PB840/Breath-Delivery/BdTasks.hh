
#ifndef BdTasks_HH
#define BdTasks_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdTasks - Breath Delivery tasks.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdTasks.hhv   25.0.4.0   19 Nov 2013 13:59:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp   Date:  27-Feb-1997    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number:: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

# include "Sigma.hh"
# include "Breath_Delivery.hh"
# include "IpcIds.hh"

//@ Usage-Classes
#include "AppContext.hh"
//@ End-Usage


class BdTasks 
{
  public:

    static void BdExecutiveTask( void );
    static void BdMainCycle( void );

    static void BdSecondaryTask( void );

    static void BdStatusTask( void );

    static inline Boolean IsSstRequired( void );

    static Boolean IsServiceModeBackgroundEnabled( void );
    static void SetServiceModeBackgroundEnabled( const Boolean enableState );

    static void XmitFlashTableTask( void ) ;

    static void BdExecBdReadyCallback( const BdReadyMessage& rMessage ) ;
    static void BdChangeStateCallback( const ChangeStateMessage& rMessage ) ;
    static void GuiReadyCallback(const GuiReadyMessage& rMessage) ;

    static void SoftFault( const SoftFaultID softFaultID,
					   const Uint32      lineNumber,
					   const char*       pFileName  = NULL, 
					   const char*       pPredicate = NULL );
  
  protected:

  private:

    BdTasks(void);			// not implemented...
    ~BdTasks(void);			// not implemented...
    BdTasks(const BdTasks&);		// not implemented...
    void   operator=(const BdTasks&);	// not implemented...


    //@ Data-Member: IsSstRequired_
    // Indicates that SST is required
    static Boolean IsSstRequired_;

    //@ Data-Member: IsServiceModeBackgroundEnabled_
    // Indicates whether or not service mode background task is enabled
    static Boolean IsServiceModeBackgroundEnabled_;

};


// Inlined methods...
#include "BdTasks.in"


#endif // BdTasks_HH 
