#ifndef ServiceModeClassId_HH
#define ServiceModeClassId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceModeClassId - ID's for Service Mode classes
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ServiceModeClassId.hhv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc   Date: 26-Mar-2007   SCR Number: 6237
//       Project:  Trend
//       Description:
//          Added Compact Flash test.
//
//  Revision: 006   By: syw   Date: 27-Aug-1998   DR Number:
//       Project:  BiLevel
//       Description:
//          BiLevel Initial Release.  Added DATAKEYCOPY.
//
//  Revision: 005   By: quf   Date: 24-Sep-1997   DR Number: DCS 2410
//       Project:  Sigma (840)
//       Description:
//			Added ATMPRESXDUCERCALIBRATION.
//
//  Revision: 004  By:  gdc    Date: 23-Sep-1997    DR Number: DCS 2513
//       Project:  Sigma (R8027)
//       Description:
//          Added remote tests to support service/manufacturing tests.
//
//  Revision: 003   By: quf   Date: 15-Sep-1997   DR Number: DCS 2385
//       Project:  Sigma (840)
//       Description:
//			Added SERVICEMODENOVRAMDATA.
//
//  Revision: 002  By:  syw    Date:  03-Jul-97    DR Number: DCS 2279
//       Project:  Sigma (R8027)
//       Description:
//             Added FLOWSENSORCALIBRATION.
//
//  Revision: 001  By:  mad    Date:  08-Nov-95    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//====================================================================


//@ Usage-Classes

//@ End-Usage

enum ServiceModeClassID {
		SERVICEMODE = 0,
		SMFLOWCONTROLLER = 1,
		SMPRESSURECONTROLLER = 2,
		SMEXHVALVECONTROLLER = 3,
		TESTDATA = 4,
		SMCYCLETIMER = 5,
		EXHVALVECALIBRATION = 6,
		SMTASKS = 7,
		PSOLLIFTOFFCALIBRATION = 8,
		COMPLIANCECALIBRATION = 9,
		SMSENSORMEDIATOR = 10,
		SMUTILITIES = 11,
		GSCIRCUITATTACHEDTEST = 12,
		WYEBLOCKEDTEST = 13,
		SSTFILTERTEST = 14,
		GASSUPPLYTEST = 15,
		CIRCUITPRESSURETEST = 16,
		EXHVALVESEALTEST = 17,
		EXHVALVEPERFORMANCETEST = 18,
		ATMOSPHERICPRESSURETEST = 19,
		//remove un-used: PSOLLOOPBACKTEST, EXHVALVELOOPBACKTEST
		SAFETYSYSTEMTEST = 22,
		GENERALELECTRONICSTEST = 23,
		EXHHEATERTEST = 24,
		FIO2CALIBRATION = 25,
		FIO2TEST = 26,
		LEAKTEST = 27,
		PRESSURESENSORTEST = 28,
		FSCROSSCHECKTEST = 29,
		//remove un-used: COMPRESSORTEST, COMPRESSORLEAKTEST
		ENVIRONMENTAL = 32,
		SMTESTID = 33,
		SMMANAGER = 34,
		BDLAMPTEST = 35,
		BDAUDIOTEST = 36,
		FLOWSENSORINITIALIZE = 37,
		SMGASSUPPLY = 38,
		SERVICEMODEAGENT = 39,
		GUIIOTEST = 40,
		MEMORYTEST = 41,
		BATTERYTEST = 42,
		SERIALNUMBERINITIALIZE = 43,
		FORCEVENTINOPTEST = 44,
		TESTDRIVER = 45,
		CALINFODUPLICATION = 46,
		RESISTANCE = 47,
		INSPRESISTANCETEST = 48,
		//remove un-used: EXHVALVEVELOCITYXDUCERTEST
		FLOWSENSORCALIBRATION = 50,
		SERVICEMODENOVRAMDATA = 51,
		SMCOMPRESSOREEPROM = 52,
		SMDATAKEY = 53,
		SMSYSTEMLOG = 54,
		SMFLOWTEST = 55,
		ATMPRESXDUCERCALIBRATION = 56,
		DATAKEYCOPY = 57,
		COMPACTFLASHTEST = 58,
	    PROXTEST = 59
} ;

#endif // ServiceModeClassId_HH 
