
#ifndef ValveRefs_HH
#define ValveRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: ValveRefs - All the external references of various Psol,
//		   Solenoid, Safety Valve and Exhalation Valve.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ValveRefs.hhv   25.0.4.0   19 Nov 2013 13:54:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  syw   Date:  03-May-1996    DR Number: 562
//       Project:  Sigma (R8027)
//       Description:
//             Added scope of ExhalationValve for GUI CPU.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#if defined (SIGMA_BD_CPU)

class Psol;
extern Psol& RO2Psol;
extern Psol& RAirPsol;
extern Psol& RExhPsol;
extern Psol& RDesiredFlowPsol;

class Solenoid;
extern Solenoid& RInspAutozeroSolenoid;
extern Solenoid& RExhAutozeroSolenoid;
extern Solenoid& RCrossoverSolenoid;
extern Solenoid& RSafetyValve;

#endif // defined (SIGMA_BD_CPU)

class ExhalationValve;
extern ExhalationValve& RExhalationValve;

#endif // ValveRefs_HH




