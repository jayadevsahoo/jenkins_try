#ifndef LeakTest_HH
#define LeakTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LeakTest - Test for pneumatic leaks
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/LeakTest.hhv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 29-Jul-1997    DR Number: DCS 2266
//       Project:  Sigma (840)
//       Description:
//            Added final expiratory pressure check for the EST Leak
//            Test only, which generates Alert if final exp pressure
//            is out of range of the final insp pressure.
//
//  Revision: 003  By:  quf    Date: 28-Jul-1997    DR Number: DCS 2245
//       Project:  Sigma (840)
//       Description:
//            Removed high pressure exit threshold from
//            waitForPressureDrop_().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "SigmaState.hh"
#include "OsTimeStamp.hh"

//@ End-Usage

extern const Real32 LEAK_TEST_BUILD_PRESSURE;
extern const Real32 LEAK_TEST_START_PRESSURE;
extern const Real32 EST_FAILURE_DELTA_P;
extern const Real32 SST_ALERT_DELTA_P;
extern const Real32 SST_FAILURE_DELTA_P;
extern const Int32 LEAK_TEST_TIME;


class LeakTest
{
  public:
	enum LeakTestResult
	{
		PRESSURE_BUILD_FAILURE,
		UNDERSHOT_STARTING_PRESSURE,
		TIGHT_CIRCUIT,
		PROCEED
	};

	LeakTest(void);
	~LeakTest(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);
  
    void estTest(void);
    void sstTest(void);

  protected:

  private:
    LeakTest(const LeakTest&);			// not implemented...
    void   operator=(const LeakTest&);	// not implemented...

	LeakTestResult measureLeak_( SigmaState state,
								 Real32& pressureDrop);
	Real32 waitForPressureDrop_( const Real32 lowThreshold,
		const Uint32 timeout, OsTimeStamp& timer,
		Uint32& exitTime );

	// inspiratory and expiratory pressures at end of test loop
	Real32 inspPressure_;
	Real32 expPressure_;
};


#endif // LeakTest_HH 
