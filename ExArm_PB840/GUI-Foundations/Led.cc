#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Led - Interface to the hardware, allowing the activation
// of the various status and alarm LEDs that the ventilator displays.
//---------------------------------------------------------------------
//@ Interface-Description
// This class interfaces to the low-level hardware LED system.  The
// different LEDs that can be lit by the ventilator are identified by the
// enum in header file Led.hh.
//
// Most LEDs (apart from HIGH_ALARM_URGENCY and MEDIUM_ALARM_URGENCY) can be
// in one of two states: ON or OFF.  The HIGH_ALARM_URGENCY and
// MEDIUM_ALARM_URGENCY LEDs can be in an additional two states:
// SLOW_BLINK_STATE or FAST_BLINK_STATE.
//
// LEDs can be switched between states simply by calling SetState().  No other
// interface is needed to the LEDs.
//---------------------------------------------------------------------
//@ Rationale
// This class is necessary to control the lighting of LEDs on the
// ventilator GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
// Translates the user level LED state defined in the GuiFoundation to hardware
// level LED state defined in the VGA-Graphics-driver subsystem.  Then sets
// LED states by calling VgaBlink::SetLedStatus() from VGA-Graphics-Driver
// subsystem.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Led.ccv   25.0.4.0   19 Nov 2013 14:11:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 005 By: srp    Date: 28-May-2002   DR Number: 5906
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 004   By: sah    Date: 1-Jan-2000   DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Updated for NeoMode
//
//
//  Revision: 003   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 002  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.  Also,
//      added some AUX error codes to some assertions ("because I was
//      there").
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include "Led.hh"

//@ Usage-Classes
#include "VgaBlink.hh"
#include "GuiIoLed.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetState	[static]
//
//@ Interface-Description
// Sets the state of a particular Led, specified by the passed "ledID"
// parameter, to the passed "state" parameter. States include on (lit),
// off, slow blinking, and fast blinking.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the VgaBlink::SetLedStatus() routine.
//---------------------------------------------------------------------
//@ PreCondition
// Only the High and Medium urgency level LEDs can be blinked.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Led::SetState(LedId ledId, LedState state)
{
	CALL_TRACE("Led::SetState(LedId ledId, LedState state)");

	//
	// Only HIGH and MEDIUM alarm permits blink.
	// The bad combination is !(HIGH ||  MEDIUM) &&
	//						  (SLOW_BLINK_STATE || FAST_BLINK_STATE)
	// The good combination is !(!(HIGH || MEDIUM) &&
	//							 (SLOW_BLINK_STATE || FAST_BLINK_STATE))
	// which results in (HIGH || MEDIUM) ||
	//					(!SLOW_BLINK_STATE && !FAST_BLINK_STATE)
	// 
//	CLASS_PRE_CONDITION(((HIGH_ALARM_URGENCY == ledId) ||
//						 (MEDIUM_ALARM_URGENCY == ledId)) ||
//						((SLOW_BLINK_STATE != state) &&
//						 (FAST_BLINK_STATE != state)));

	Uint8 guiIoState;
	
	switch(state)
	{													// $[TI1]
	case ON_STATE:										// $[TI1.1]
		guiIoState = VgaBlink::BLINK_ON;
		break;
	case OFF_STATE:										// $[TI1.2]
		guiIoState = VgaBlink::BLINK_OFF;
		break;
	case SLOW_BLINK_STATE:									// $[TI1.3]
		guiIoState = VgaBlink::BLINK_SLOW;
		break;
	case FAST_BLINK_STATE:									// $[TI1.4]
		guiIoState = VgaBlink::BLINK_FAST;
		break;
	default:
		AUX_CLASS_ASSERTION_FAILURE(state);
		break;		
	}
	
	
	switch(ledId)
	{													// $[TI2]
	case HIGH_ALARM_URGENCY:							// $[TI2.1]
		VgaBlink::SetLedStatus(REDLEDSTRIP, guiIoState);
		break;
	case MEDIUM_ALARM_URGENCY:							// $[TI2.2]
		VgaBlink::SetLedStatus(YELLOWLEDSTRIP, guiIoState);
		break;
	case LOW_ALARM_URGENCY:								// $[TI2.3]
		VgaBlink::SetLedStatus(YELLOWLEDSTRIP, guiIoState);
		break;
	case NORMAL:										// $[TI2.4]
		VgaBlink::SetLedStatus(DS7, guiIoState);
		break;
	case VENTILATOR_INOPERATIVE:						// $[TI2.5]
		VgaBlink::SetLedStatus(DS8AND9, guiIoState);
		break;
	case SAFETY_VALVE_OPEN:								// $[TI2.6]
		VgaBlink::SetLedStatus(DS10AND11, guiIoState);
		break;
	case BATTERY_BACKUP_READY:							// $[TI2.7]
		VgaBlink::SetLedStatus(DS12, guiIoState);
		break;
	case ON_BATTERY_POWER:								// $[TI2.8]
		VgaBlink::SetLedStatus(DS13, guiIoState);
		break;
	case COMPRESSOR_READY:								// $[TI2.9]
		VgaBlink::SetLedStatus(DS14, guiIoState);
		break;
	case COMPRESSOR_OPERATING:							// $[TI2.10]
		VgaBlink::SetLedStatus(DS15, guiIoState);
		break;
	case HUNDRED_PERCENT_O2:							// $[TI2.11]
		VgaBlink::SetLedStatus(GREEN, guiIoState);
		break;
	case ALARM_SILENCE:									// $[TI2.12]
		VgaBlink::SetLedStatus(RIGHT_YELLOW, guiIoState);
		break;
	case SCREEN_LOCK:									// $[TI2.13]
		VgaBlink::SetLedStatus(LEFT_YELLOW, guiIoState);
		break;
	case NUM_LED_ID:
	default:
		AUX_CLASS_ASSERTION_FAILURE(ledId);
		break;		
	}
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
Led::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("Led::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, GUI_LED,
							lineNumber, pFileName, pPredicate);
}
