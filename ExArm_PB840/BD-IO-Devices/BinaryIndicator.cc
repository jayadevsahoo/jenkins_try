#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BinaryIndicator - digital port indicator class
//---------------------------------------------------------------------
//@ Interface-Description:
//
//  This class provides an interface to the system digital I/Os.  It
//  allow its user to read the desired bit on the designated
//  register to determine its state.
//---------------------------------------------------------------------
//@ Rationale:
//
//  Low-level device utilty.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  Reads the desired bit at the designated register high or low.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BinaryIndicator.ccv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BinaryIndicator.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BinaryIndicator()
//
//@ Interface-Description
//  	Constructor.  This method has a mask and a register as arguments and
//		returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The data members are initialized with the values passed in.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

BinaryIndicator::BinaryIndicator( const ApplicationDigitalInputs_t bit, BitAccessGpio& bitAccess )
              : rBitAccessGpio_(bitAccess)
{
    CALL_TRACE("BinaryIndicator::BinaryIndicator(const Uint16 mask, Register& reg)");

	bit_ = bit ;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BinaryIndicator()  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

BinaryIndicator::~BinaryIndicator(void)
{
    CALL_TRACE("BinaryIndicator::~BinaryIndicator(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getState()
//
//@ Interface-Description
//
//  This method accepts no parameters and returns a
//  BinaryIndicator::IndicatorState enumeration.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method utilitizes the Register class to read the targeted
//  digital I/O.  With the most current register image it returns the
//  state of the designated bit.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
const BinaryIndicator::IndicatorState
BinaryIndicator::getState( void )
{
    CALL_TRACE("BinaryIndicator::getState(void)");

	BinaryIndicator::IndicatorState state;

    if (rBitAccessGpio_.getBitState(bit_) == BitAccessGpio::ON)
    {
	// $[TI1]
		state = BinaryIndicator::ON;
    }
    else
    {
	// $[TI2]
		state = BinaryIndicator::OFF;
    }

    return( state );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
BinaryIndicator::SoftFault( const SoftFaultID  softFaultID,
                   			const Uint32       lineNumber,
                   			const char*        pFileName,
                   			const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, BINARYINDICATOR,
                           lineNumber, pFileName, pPredicate );
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

