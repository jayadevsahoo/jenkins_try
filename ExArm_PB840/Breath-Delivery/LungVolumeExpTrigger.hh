#ifndef LungVolumeExpTrigger_HH
#define LungVolumeExpTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LungVolumeExpTrigger - Triggers exhalation based on compensated
//      inspired volume.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/LungVolumeExpTrigger.hhv   10.7   08/17/07 09:38:30   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  syw    Date:  14-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"

//@ End-Usage

class LungVolumeExpTrigger : public BreathTrigger{
  public:
    LungVolumeExpTrigger( void) ;
    ~LungVolumeExpTrigger( void) ;

    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    LungVolumeExpTrigger( const LungVolumeExpTrigger&) ;  			// not implemented...
    void   operator=( const LungVolumeExpTrigger&) ; 	// not implemented...
	
} ;

#endif // LungVolumeExpTrigger_HH 
