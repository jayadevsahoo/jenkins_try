#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: InspiratoryPauseHandler - Handles control of Inspiratory Pause
// maneuver (initiated by the Inspiratory Pause off-screen key).
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class dedicated to the control of the Inspiratory Pause maneuver.
// Inspiratory Pause is set by pressing the Inspiratory Pause offscreen
// key when in auto mode, or is set by pressing down the Inspiratory
// Pause offscreen key, and holding it down until the pause is active,
// at which time the user can release the pause key anytime he/she wishes
// to end the pause for manual mode.
// The keyPanelPressHappened() and keyPanelReleaseHappened() methods
// inform us of Inspiratory Pause key events. Cancel() methods Notify the
// Breath-Delivery subsystem of the cancellation request.  This is a
// callBack method linked to the display panel.
//---------------------------------------------------------------------
//@ Rationale
// This class is to handle the Inspiratory Pause key maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply register for Inspiratory Pause Key event callbacks and inform
// the Breath Delivery system via an ambassador object of any key press
// event.  Key press events (key presses and key releases) are communicated
// to Breath-Delivery subsystems through the UserEventObject.
// When key events happen, this object activates the associated subscreen
// which is designed to handle all the maneuver processing.
//---------------------------------------------------------------------
//@ Fault-Handling
// None
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/InspiratoryPauseHandler.ccv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: rhj	Date: 20-Jul-2007   SCR Number:  6383
//  Project:  Trend
//  Description:
//		Added a PostEvent method which generates an auto insp pause event.
//
//  Revision: 007  By: gdc	Date: 26-May-2007   SCR Number:  6330
//  Project:  Trend
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 006  By: hhd	Date: 27-Apr-2000   DCS Number:  5657
//  Project:  Baseline
//  Description:
//		Modified to display Settings Locked Out prompt when the condition arises.
//
//  Revision: 005  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By:  yyy    Date:  01-APR-98    DR Number: 5252
//       Project:  Sigma (R8027)
//       Description:
//             Mapping SRS to code.
//
//  Revision: 002  By:  iv    Date:  18-Jan-1998    DR Number: None
//  Project:  Sigma (R8027)
//  Description:
//             Bilevel initial version.
//			   Implemented Insp Pause.
//
//  Revision: 001  By:  hhd		Date:  14-MAY-95	DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//             Integration baseline.
//=====================================================================

#include "InspiratoryPauseHandler.hh"

//@ Usage-Classes
#include "GuiApp.hh"
#include "KeyHandlers.hh"
#include "BdGuiEvent.hh"
#include "KeyPanel.hh"
#include "MessageArea.hh"
#include "MiscStrs.hh"
#include "Sound.hh"
#include "LowerScreen.hh"
#include "LowerSubScreenArea.hh"
#include "LowerOtherScreensSubScreen.hh"
#include "UpperScreen.hh"
#include "WaveformsSubScreen.hh"
#include "EventData.hh"
#include "BdEventRegistrar.hh"
#include "GuiEventRegistrar.hh"
#include "BreathPhaseType.hh"
#include "OffKeysSubScreen.hh"
#include "TrendDataMgr.hh"
#include "TrendEvents.hh"

//@ End-Usage
Boolean InspiratoryPauseHandler::pauseCanceled_;
//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InspiratoryPauseHandler()  [Default Constructor]
//
//@ Interface-Description
// Creates the Inspiratory Pause Handler object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Register for Inspiratory Pause Key event callbacks and Inspiratory Pause 
// status events.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

InspiratoryPauseHandler::InspiratoryPauseHandler(void)
{
	CALL_TRACE("InspiratoryPauseHandler::InspiratoryPauseHandler(void)");

	// Register for Inspiratory Pause key presses
	KeyPanel::SetCallback(KeyPanel::INSP_PAUSE_KEY, this);

	// Register for Breath-Delivery's Inspiratory pause status events
	BdEventRegistrar::RegisterTarget(EventData::INSPIRATORY_PAUSE, this);

	// Register for GUI event change callbacks.
	GuiEventRegistrar::RegisterTarget(this);

	pauseCanceled_ = TRUE;
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~InspiratoryPauseHandler  [Destructor]
//
//@ Interface-Description
// Destroys the Inspiratory Pause Handler object.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

InspiratoryPauseHandler::~InspiratoryPauseHandler(void)
{
	CALL_TRACE("InspiratoryPauseHandler::~InspiratoryPauseHandler(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened
//
//@ Interface-Description
// Called when the Inspiratory Pause key is pressed.	
//---------------------------------------------------------------------
//@ Implementation-Description
// We disply the offsreen key help messge and notify the Breath Delivery
// subsystem's User Event Object of the pause request.
//
// Note: pressing of this key is invalid during service mode so just make
// the invalid entry sound in this case.
// $[012XX] When the inspiratory pause key is first pressed, the gui ...
// $[012XX] To start an inspiratory pause, the user must be holding ...
// $[01289] During SST, Alarm silence, alarm reset, manual inspiration ...
// $[07034] During service mide, Alarm silence, alarm reset, help, ...
// $[01006] The GUI must be able to display the meaning of ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
InspiratoryPauseHandler::keyPanelPressHappened(KeyPanel::KeyId)
{
	CALL_TRACE("InspiratoryPauseHandler::keyPanelPressHappened(KeyPanel::KeyId)");

	// Display the offscreen key help message 
	GuiApp::PMessageArea->setMessage(
							MiscStrs::INSPIRATORY_PAUSE_HELP_MESSAGE,
							MessageArea::MA_HIGH);

	// Only react to key during normal ventilation mode.
	if (GuiApp::GetGuiState() == STATE_ONLINE &&
		!GuiApp::IsSettingsLockedOut() &&
		!LowerScreen::RLowerScreen.isInPatientSetupMode())
	{													// $[TI1]
		// Notify Breath-Delivery of the Pause request
		// $[BL01002] press the inspiratory key to start pause
		// $[BL01001] notify BD unit
		BdGuiEvent::RequestUserEvent(EventData::INSPIRATORY_PAUSE);
	}
	else
	{													// $[TI2]
		// Illegal key press during service mode
		// $[01360] Sigma is capable of creating the following user interface sound.
		// $[BL01024] When a maneuver is requested prior to the completion ...
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened
//
//@ Interface-Description
// Called when the Inspiratory Pause key is released. Cancel the inspiratory
// pause.
//---------------------------------------------------------------------
//@ Implementation-Description
// Notify Breath-Delivery of the cancel request.
//	Simply turn off the offscreen help message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
InspiratoryPauseHandler::keyPanelReleaseHappened(KeyPanel::KeyId keyId)
{
	CALL_TRACE("InspiratoryPauseHandler::keyPanelReleaseHappened(KeyPanel::KeyId)");

	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);

	if (GuiApp::GetGuiState() == STATE_ONLINE && keyId == KeyPanel::INSP_PAUSE_KEY &&
		!LowerScreen::RLowerScreen.isInPatientSetupMode())
	{												// $[TI1]
		// Notify Breath-Delivery of the Cancel request	     
		// $[BL01000] delivers one pause per key
		BdGuiEvent::RequestUserEvent(EventData::INSPIRATORY_PAUSE, EventData::STOP);
	}												// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
//  Called when Inspiratory Pause event happens and its status is notified
//  by the Breath-Delivery subsystems.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method is called with the following applicable event status:
//  PENDING or ACTIVE_PENDING : Activate WaveformsSubScreen on the
//								upper subscreen area.
//  ACTIVE or PLATEAU_ACTIVE  : Current pause data to appear at the
//								waveform subscreen.
//  CANCEL					  : Inform the WaveformsSubScreen of the
//								cancel command.
//  REJECTED				  : Rejection of exp pause request.
//  COMPLETE				  : Completion of exp pause request.
//  AUDIO_ACK				  : Produce CONTACT sound to indicate that
//								plateau is stablized
//  IDLE or NO_ACTION_ACK	  : Do nothing.
//
//  The associated display should be updated.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
InspiratoryPauseHandler::bdEventHappened(EventData::EventId,
								EventData::EventStatus eventStatus,
								EventData::EventPrompt eventPrompt)
{
    CALL_TRACE("BdEventTarget::bdEventHappened(EventData::EventId "
		", EventData::EventStatus eventStatus)");

	Boolean updateRequired = TRUE;
	Boolean notActivatedYet = TRUE;
    switch(eventStatus)
	{
	case EventData::PENDING:
	case EventData::ACTIVE_PENDING:					// $[TI1.1]
		// Activate WaveformsSubScreen on the upper subscreen area.  If the
		// waveform is frozen, it is unfrozen.
		// $[BL01003] pause request accepted, display waveform 
		UpperSubScreenArea::GetWaveformsSubScreen()->pauseRequested();

		notActivatedYet = TRUE;
		pauseCanceled_ = FALSE;
		break;

	case EventData::ACTIVE:	
	case EventData::PLATEAU_ACTIVE:					// $[TI1.2]
		// Cause current pause data to appear
		// of the waveform subscreen.
		UpperSubScreenArea::GetWaveformsSubScreen()->
								pauseActive(BreathPhaseType::INSPIRATORY_PAUSE);

		if (eventStatus == EventData::PLATEAU_ACTIVE ||
			eventStatus == EventData::ACTIVE && notActivatedYet)
		{											// $[TI1.2.1]
			// Produce CONTACT sound to indicate that pause is now active
			// $[01360] Sigma is capable of creating the following user interface sound.
			// $[BL01017] When pause starts and terminates, an audible ...
			Sound::Start(Sound::CONTACT);
			notActivatedYet = FALSE;
		}											// $[TI1.2.2]
		break;

	case EventData::CANCEL:							// $[TI1.3]
		// Indicate cancellation of the current maneuver
		Sound::Start(Sound::INVALID_ENTRY);

		// Activate WaveformsSubScreen on the upper subscreen area.  If the
		// waveform is frozen, it is unfrozen.
		if (pauseCanceled_)
		{										// $[TI1.3.1.1]
			UpperSubScreenArea::GetWaveformsSubScreen()->pauseRequested();
		}
		else
		{										// $[TI1.3.1.2]			
			UpperSubScreenArea::GetWaveformsSubScreen()->pauseCancelByAlarm();
		}
		break;

	case EventData::REJECTED:						// $[TI1.4]	
		// $[01255] rejection of exp pause request
		// Indicate cancellation to user's most recent maneuver request
		Sound::Start(Sound::INVALID_ENTRY);
		break;

	case EventData::COMPLETE:						// $[TI1.5]
		// $[BL01018] an audible indication when pause terminates
		// $[BL01017] When pause starts and terminates, an audible ...
		Sound::Start(Sound::CONTACT);

	    // Post the Inspiratory Pause event
		TrendDataMgr::PostEvent( TrendEvents::AUTO_EVENT_INSP_MANEUVER );

		break;

	case EventData::AUDIO_ACK:						// $[TI1.6]
		// Produce CONTACT sound to indicate that plateau is stablized
		// $[BL01011] audible indication for pressure stability
		Sound::Start(Sound::CONTACT);
		updateRequired = FALSE;
		break;

	case EventData::IDLE:
	case EventData::NO_ACTION_ACK:					// $[TI1.7]
	default:
		updateRequired = FALSE;
		break;
	}

	if (updateRequired)
	{												// $[TI2.1]
		// Update Offkeys subScreen
		LowerSubScreenArea::GetOffKeysSubScreen()->updatePanel(EventData::INSPIRATORY_PAUSE, eventStatus, eventPrompt);
	}												// $[TI2.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Cancel
//
//@ Interface-Description
// Called when the Cancel button is pressed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Simply inform the Breath Deliverery subSystem of the cancel
//  request.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
InspiratoryPauseHandler::Cancel(void)
{
	CALL_TRACE("InspiratoryPauseHandler::Cancel(void)");

	//								 $[TI1]
	// Notify Breath-Delivery subsystem
	pauseCanceled_ = TRUE;
	BdGuiEvent::RequestUserEvent(EventData::CLEAR_KEY);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: guiEventHappened
//
//@ Interface-Description
// Called when a registered change in the GUI App event happens.  Passed the id of the
// event which changed.  
//---------------------------------------------------------------------
//@ Implementation-Description
// If the eventId is INOP, SETTINGS_LOCKOUT, or COMMUNICATIONS_DOWN then display
// "Settings Locked out" prompt.  No action otherwise.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
InspiratoryPauseHandler::guiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("guiEventHappened(GuiApp::GuiAppEvent eventId)");

	switch (eventId)
	{
	case GuiApp::INOP:								// $[TI1]
	case GuiApp::COMMUNICATIONS_DOWN:
	case GuiApp::SETTINGS_LOCKOUT:

		// Set Primary prompt to nothing
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
						PromptArea::PA_LOW, NULL_STRING_ID);
		// Set Advisory prompt to "Settings have been locked out."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_HIGH, PromptStrs::SETTINGS_LOCKED_OUT_A);

		break;
		
	default:										// $[TI2]
		break;
	}
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
/*
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
*/
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
InspiratoryPauseHandler::SoftFault(const SoftFaultID  softFaultID,
								    const Uint32       lineNumber,
								    const char*        pFileName,
								    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, 
							INSPIRATORYPAUSEHANDLER,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
