#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SmUtilities - Service Mode Utilities
//---------------------------------------------------------------------
//@ Interface-Description
//	This class implements Service Mode utility functions.
//---------------------------------------------------------------------
//@ Rationale
//	This class is required to define Service Mode utility functions.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Various utility functions are defined for use in the Service Mode
//	tests.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmUtilities.ccv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//  Revision: 015  By: srp    Date: 23-Apr-2002     DR Number: 5904
//  Project:  VCP
//  Description:
//	Changed psolCount to DacCount which is currently defined as a Int16. DacCount
//  is the data type expected by RairPsol.updatePsol.  psolCount was previously Int32.
//
//  Revision: 014  By: cep    Date: 17-Apr-2002     DR Number: 5903
//  Project:  VCP
//  Description:
//	NEO_CKT_MAX_FLUSHING_TIME changed to Uint32.
//
//  Revision: 013  By: jja    Date: 31-May-2000     DR Number: 5736
//  Project:  NeoMode
//  Description:
//	Revise Flushing volume and time for neonatal circuit .
//
//  Revision: 012  By: quf    Date: 12-Apr-2000     DR Number: 5709
//  Project:  NeoMode
//  Description:
//	Fixes to prevent pressure oscillation during flow control
//	when high-pressure gas supply is used:
//  *  In flushTubing(), changed flushing flow from 200 to 120 Lpm
//	   for EST and SST with adult circuit.
//	*  In getMaximumFlow(), ramp the PSOL to the max DAC count rather
//	   than setting the max count immediately.
//
//  Revision: 011  By: sah    Date: 30-Jun-1999     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added neonatal-specific flushing volume, time and flow
//
//  Revision: 010  By: quf    Date: 13-Oct-1999     DR Number: 5540
//  Project:  ATC
//  Description:
//      Added utility functions atmPressureBounded() and btpsToStpd().
//
//  Revision: 009  By: dosman    Date: 20-Sep-1999     DR Number: 5526
//  Project:  ATC
//  Description:
//	Changed if-expression because it had an assignment operator
//	where it should have an equality operator.
//
//  Revision: 008  By: dosman    Date: 26-Feb-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release.
//	Changed determination of the maximum time before calling a timeout
//	in pressurizing the system for several tests.  The maximum compliance
//	value used now varies depending on the whether the type of test
//	is EST or external.
//
//  Revision: 007  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 006  By:  quf    Date: 15-Jan-1998    DR Number: DCS 2720
//       Project:  Sigma (840)
//       Description:
//			Added autozeroing of secondary Pexp sensor object in method
//			autozeroPressureXducers().
//
//  Revision: 005  By:  quf    Date: 07-Oct-1997    DR Number: DCS 2406
//       Project:  Sigma (840)
//       Description:
//			Implemented new PSOL exercise procedure, renamed popPsol()
//			as popPsols().
//
//  Revision: 004  By:  syw    Date: 29-Jul-1997    DR Number: DCS 2279
//       Project:  Sigma (840)
//       Description:
//			Added getMaximumFlow() method.
//
//  Revision: 003  By:  quf    Date: 03-Jul-1997    DR Number: DCS 2239
//       Project:  Sigma (840)
//       Description:
//            In flushTubing(), created separate flushing parameters
//            for pediatric circuit to resolve noise problem.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//            Minor rework to checkAcPower().
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "SmUtilities.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmGasSupply.hh"
#include "SmManager.hh"
#include "SmFlowController.hh"
#include "SmExhValveController.hh"
#include "TestData.hh"
#include "SmConstants.hh"

#include "Task.hh"

#include "ValveRefs.hh"
#include "VentObjectRefs.hh"
#include "MainSensorRefs.hh"
#include "MiscSensorRefs.hh"
#include "PressureSensor.hh"
#include "Solenoid.hh"
#include "Compressor.hh"
#include "Psol.hh"
#include "FlowSensor.hh"
#include "SystemBattery.hh"
#include "Barometer.hh"

#include "PhasedInContextHandle.hh"
#include "PatientCctTypeValue.hh"
#include "BDIORefs.hh"
#include "AioDioDriver.h"
#include "ExhalationValve.hh"

#if defined(SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif

#if defined SIGMA_UNIT_TEST || defined DCS_5540_UNIT_TEST
# include "SmUnitTest.hh"
# include "FakeIo.hh"
# include "FakeControllers.hh"
# include "CircuitPressureTest_UT.hh"
# include "FsCrossCheckTest_UT.hh"
# include "SmUtilities_UT.hh"
# include "FakeTask.hh"

#elif defined DCS_5709_UNIT_TEST
# include "FakeIo.hh"

#endif  // defined SIGMA_UNIT_TEST || defined DCS_5540_UNIT_TEST

//@ End-Usage


//@ Code...

//@ Constant: DRAIN_FLOW_RATE
const Real32 DRAIN_FLOW_RATE = 200.0;  // lpm

//@ Constant: EXH_VALVE_STEP_SIZE
// exh valve step size per iteration while ramping
const Int32  EXH_VALVE_STEP_SIZE = 30;  // DAC counts

//@ Constant: BATTERY_CHARGING_MASK
// mask to read the battery charging bit data
static const Uint8 BATTERY_CHARGING_MASK = 0x02;

//@ Constant: BATTERY_CHARGED_MASK
// mask to read the battery charged bit data
static const Uint8 BATTERY_CHARGED_MASK = 0x04;

//@ Constant: SERVICE_FLUSHING_VOLUME
// circuit flushing volume for service mode
const Real32 SERVICE_FLUSHING_VOLUME = 2500.0;  // ml

//@ Constant: SERVICE_MAX_FLUSHING_TIME
// max circuit flushing time for service mode
const Uint32 SERVICE_MAX_FLUSHING_TIME = 3000;  // msec


// adult circuit flushing volume for SST
const Real32 ADULT_CKT_FLUSHING_VOLUME = 15000.0;  // ml

// pediatric circuit flushing volume for SST
const Real32 PED_CKT_FLUSHING_VOLUME = 11000.0;  // ml

// neonatal circuit flushing volume for SST
const Real32 NEO_CKT_FLUSHING_VOLUME = 3500.0;  // ml


// max adult circuit flushing time for SST
const Uint32 ADULT_CKT_MAX_FLUSHING_TIME = 18000;  // msec

// max pediatric circuit flushing time for SST
const Uint32 PED_CKT_MAX_FLUSHING_TIME = 13200;  // msec

// max neonatal circuit flushing time for SST
const Uint32 NEO_CKT_MAX_FLUSHING_TIME = 10000;  // msec


//@ Constant: DRAIN_GAS_SUPPLY_HOSE_TIMEOUT
// max time allowed to drain trapped gas in air or O2 supply hose
const Uint32 DRAIN_GAS_SUPPLY_HOSE_TIMEOUT = 3000;  // msec


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: SmUtilities  [Default Constructor]
//
//@ Interface-Description
// Constructor.  No arguments.  Initializes the private data members.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the private data members.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

SmUtilities::SmUtilities( void) // E600 BDIO :
// E600 BDIO		bpsCharged_(BATTERY_CHARGED_MASK, RPowerSupplyReadPort),
// E600 BDIO    	bpsCharging_(BATTERY_CHARGING_MASK, RPowerSupplyReadPort)
{
// $[TI1]
	CALL_TRACE("SmUtilities::SmUtilities( void)");
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~SmUtilities  [Destructor]
//
//@ Interface-Description
//-----------------------------------------------------------------------
//@ Implementation-Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================

SmUtilities::~SmUtilities( void)
{
	CALL_TRACE("SmUtilities::~SmUtilities( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: verifyWallAirConnected
//
//@ Interface-Description
//	This method verifies that wall air is connected.  No argument,
//	returns a test result Id.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	If wall air is connected, return "gas connected" result ID,
//	else prompt user to connect air and wait for keypress.  If keypress
//	was EXIT button, return "operator exit" ID, else check again
//	for wall air - if connected, return "gas connected" ID, else return
//	"gas not connected" ID.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

SmUtilities::TestResult
SmUtilities::verifyWallAirConnected( void)
{
	CALL_TRACE("SmUtilities::verifyWallAirConnected( void)");
	
	TestResult testPassed;
	
	if (!RSmGasSupply.isWallAirPresent())
	{
	// $[TI1]
		if (messagePrompt( SmPromptId::CONNECT_AIR_PROMPT))
		{
		// $[TI1.1]
		    	if (!RSmGasSupply.isWallAirPresent())
		    	{
			// $[TI1.1.1]
		    		testPassed = GAS_NOT_CONNECTED;
		    	}
			else
			{
			// $[TI1.1.2]
				testPassed = GAS_CONNECTED;
			}
		}
		else
		{
		// $[TI1.2]
			testPassed = OPERATOR_EXIT;
		}
	}
	else
	{
	// $[TI2]
		testPassed = GAS_CONNECTED;
	}

	return( testPassed);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: verifyAirConnected
//
//@ Interface-Description
//	This method verifies that any air (wall or compressor) is connected.
//	No argument, returns a test result Id.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	If any air is connected, return "gas connected" result ID,
//	else prompt user to connect air and wait for keypress.  If keypress
//	was EXIT button, return "operator exit" ID, else check again
//	for any air - if connected, return "gas connected" ID, else return
//	"gas not connected" ID.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

SmUtilities::TestResult
SmUtilities::verifyAirConnected( void)
{
	CALL_TRACE("SmUtilities::verifyAirConnected( void)");
	
	TestResult testPassed;
	
	if (!RSmGasSupply.isAnyAirPresent())
	{
	// $[TI1]
		if (messagePrompt( SmPromptId::CONNECT_AIR_PROMPT))
		{
		// $[TI1.1]
			if (!RSmGasSupply.isAnyAirPresent())
			{
			// $[TI1.1.1]
				testPassed = GAS_NOT_CONNECTED;
	    		}
			else
			{
			// $[TI1.1.2]
				testPassed = GAS_CONNECTED;
			}
	    	}
		else
		{
		// $[TI1.2]
			testPassed = OPERATOR_EXIT;
		}
	}
	else
	{
	// $[TI2]
		testPassed = GAS_CONNECTED;
	}

	return( testPassed);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: verifyO2Connected
//
//@ Interface-Description
//	This method verifies that O2 is connected.
//	No argument, returns a test result Id.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	If O2 is connected, return "gas connected" result ID,
//	else prompt user to connect O2 and wait for keypress.  If keypress
//	was EXIT button, return "operator exit" ID, else check again
//	for O2 - if connected, return "gas connected" ID, else return
//	"gas not connected" ID.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End- Method
//=======================================================================

SmUtilities::TestResult
SmUtilities::verifyO2Connected( void)
{
	CALL_TRACE("SmUtilities::verifyO2Connected( void)");
	
	TestResult testPassed;
	
	if (!RSmGasSupply.isO2Present())
	{
	// $[TI1]
		if (messagePrompt( SmPromptId::CONNECT_O2_PROMPT))
		{
		// $[TI1.1]
			if (!RSmGasSupply.isO2Present())
			{
			// $[TI1.1.1]
	    			testPassed = GAS_NOT_CONNECTED;
	    		}
			else
			{
			// $[TI1.1.2]
				testPassed = GAS_CONNECTED;
			}
		}
	    	else
		{
		// $[TI1.2]
			testPassed = OPERATOR_EXIT;
		}
	}
	else
	{
	// $[TI2]
		testPassed = GAS_CONNECTED;
	}

	return( testPassed);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: verifyAnyGasConnected
//
//@ Interface-Description
//	This method verifies that any gas is connected (wall air, compressor
//	air, or O2). No argument, returns a test result Id.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	If any gas is connected, return "gas connected" result ID,
//	else prompt user to connect air and wait for keypress.  If keypress
//	was EXIT button, return "operator exit" ID, else check again
//	for any gas - if connected, return "gas connected" ID, else return
//	"gas not connected" ID.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

SmUtilities::TestResult
SmUtilities::verifyAnyGasConnected( void)
{
	CALL_TRACE("SmUtilities::verifyAnyGasConnected( void)");
	
	TestResult testPassed;
	
	if (!RSmGasSupply.isAnyGasPresent())
	{
	// $[TI1]
		if (messagePrompt( SmPromptId::CONNECT_AIR_PROMPT))
		{
		// $[TI1.1]
	    		if (!RSmGasSupply.isAnyGasPresent())
	    		{
			// $[TI1.1.1]
	    			testPassed = GAS_NOT_CONNECTED;
	    		}
			else
			{
			// $[TI1.1.2]
				testPassed = GAS_CONNECTED;
			}
	    	}
	    	else
		{
		// $[TI1.2]
			testPassed = OPERATOR_EXIT;
		}
	}
	else
	{
	// $[TI2]
		testPassed = GAS_CONNECTED;
	}

	return( testPassed);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: messagePrompt
//
//@ Interface-Description
//	This method takes a PromptId argument to generate an operator prompt
//	message, then waits for the operator response, returning TRUE if the
//	user hit the ACCEPT key or FALSE if user hit the EXIT button (no other
//	keys are accepted).
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Generate an operator prompt from the PromptId argument, indicating
//	ACCEPT key is the only accepted keypress (by default, EXIT button is
//	accepted also), then waiting for the keypress.  If EXIT was pressed,
//	then return FALSE, else return TRUE.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

Boolean
SmUtilities::messagePrompt( const SmPromptId::PromptId promptId)
{

	CALL_TRACE("SmUtilities::messagePrompt(const SmPromptId::PromptId promptId)");

	Boolean testPassed = FALSE;
	
	RSmManager.promptOperator( promptId, SmPromptId::ACCEPT_KEY_ONLY);

	SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();

	if (operatorAction == SmPromptId::USER_EXIT)
	{
	// $[TI1]
		testPassed = FALSE;
	}
	else if (operatorAction == SmPromptId::KEY_ACCEPT)
	{
	// $[TI2]
		testPassed = TRUE;
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(operatorAction);
	}
	// end else

	return( testPassed);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: drainAir
//
//@ Interface-Description
//	This method is used to drain any trapped air pressure in the wall
//	air supply hose and the compressor accumulator.
//	No arguments, no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Open safety valve, establish a drain flow, wait a fixed time for flow
//	to rise, then wait for flow to decay to 1 lpm or below or until timeout
//	occurs, stop the flow, close the safety valve.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

void
SmUtilities::drainAir( void)
{
	CALL_TRACE("SmUtilities::drainAir( void)");

	openSafetyValve();
	RSmFlowController.establishFlow( DRAIN_FLOW_RATE, DELIVERY, AIR);

	// Wait for flow to rise (in case gas is connected)
	Task::Delay( 0, 100);

	// Wait until flow drops below a threshold or until timeout
	for ( Uint32 timer = 0;
		  RAirFlowSensor.getValue() > 1.0 && timer < COMPRESSOR_BLEED_TIME;
		  timer += SM_CYCLE_TIME )
	{
	// $[TI1]
		Task::Delay( 0, SM_CYCLE_TIME);
	}	

#ifdef SIGMA_DEBUG
	printf("drain air time = %u msec\n", timer);
#endif  // SIGMA_DEBUG

#ifdef SIGMA_UNIT_TEST
	SmUnitTest::Timer = timer;
#endif  // SIGMA_UNIT_TEST

	RSmFlowController.stopFlowController();
   	closeSafetyValve();
}
	  

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: drainO2
//
//@ Interface-Description
//	This method is used to drain any trapped pressure in the O2 supply hose.
//	No arguments, no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Open safety valve, establish a drain flow, wait a fixed time for flow
//	to rise, then wait for flow to decay to 1 lpm or below or until timeout
//	occurs, stop the flow, close the safety valve.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

void
SmUtilities::drainO2( void)
{
	CALL_TRACE("SmUtilities::drainO2( void)");

	openSafetyValve();
	RSmFlowController.establishFlow( DRAIN_FLOW_RATE, DELIVERY, O2);

	// Wait for flow to rise (in case gas is connected)
	Task::Delay( 0, 100);

	// Wait until flow drops below a threshold or until timeout
	for ( Uint32 timer = 0;
		  RO2FlowSensor.getValue() > 1.0 && timer < DRAIN_GAS_SUPPLY_HOSE_TIMEOUT;
		  timer += SM_CYCLE_TIME )
	{
	// $[TI1]
		Task::Delay( 0, SM_CYCLE_TIME);
	}	

#ifdef SIGMA_DEBUG
	printf("drain O2 time = %u msec\n", timer);
#endif  // SIGMA_DEBUG

#ifdef SIGMA_UNIT_TEST
	SmUnitTest::Timer = timer;
#endif  // SIGMA_UNIT_TEST

	RSmFlowController.stopFlowController();
   	closeSafetyValve();
}

	  
// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: drainWallAir
//
//@ Interface-Description
//	This method is used to drain any trapped pressure in the air supply hose.
//	No arguments, no return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Open safety valve, establish a drain flow, wait a fixed time for air
//	to drain, stop the flow, close the safety valve.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

void
SmUtilities::drainWallAir( void)
{
// $[TI1]
#ifdef SIGMA_UNIT_TEST
	SmUnitTest::FunctionExecuted = TRUE;
#endif  // SIGMA_UNIT_TEST

	CALL_TRACE("SmUtilities::drainWallAir( void)");

	openSafetyValve();
	RSmFlowController.establishFlow( DRAIN_FLOW_RATE, DELIVERY, AIR);

	Task::Delay( 0, DRAIN_GAS_SUPPLY_HOSE_TIMEOUT);

	RSmFlowController.stopFlowController();
   	closeSafetyValve();
}
	  

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: autozeroPressureXducers
//
//@ Interface-Description
//	This method autozeros the inspiratory and expiratory pressure
//	transducers and returns a test result ID.  No argument.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Activate both autozero solenoids and delay to allow them to activate.
//	Loop to read the insp pressure multiple times to get an average
//	insp pressure ADC count at atmospheric pressure.
//	Loop to read the exp pressure multiple times to get an average
//	exp pressure ADC count at atmospheric pressure.  Note that both the
//	primary and secondary exp pressure sensor channels are autozeroed.
//	Bounds-check the averaged counts and return a test result ID indicating
//	the result of this check.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

SmUtilities::TestResult
SmUtilities::autozeroPressureXducers( void)
{
	CALL_TRACE("SmUtilities::autozeroPressureXducers( void)") ;

	TestResult result = AUTOZERO_PASSED;

  	RInspAutozeroSolenoid.open() ;
  	RExhAutozeroSolenoid.open() ;
  	Task::Delay( 0, 40) ;

	Int16 idx;
	Int16 sumCount = 0;

	for (idx = 0; idx < 8; idx++)
	{
	// $[TI1]
		sumCount += RInspPressureSensor.getCount();
		Task::Delay(0, SM_CYCLE_TIME);
	}
	// end for

	Int16 inspZeroCount = sumCount / idx;
  	RInspPressureSensor.setZeroOffset(inspZeroCount) ;



	if (inspZeroCount < MIN_ZERO_COUNT || inspZeroCount > MAX_ZERO_COUNT)
	{
	// $[TI2]
		result = INSP_AUTOZERO_FAILED;

#if defined(SIGMA_DEVELOPMENT) && !defined(SIGMA_UNIT_TEST)
	printf("SmUtilities:  INSP AUTOZERO FAILED!!\n");
	printf("Insp autozero count = %d\n", inspZeroCount);
#endif
	}
	// $[TI3]
	// implied else: insp autozero passed
			
	sumCount = 0;
	Uint16 sumCount2 = 0;
	for (idx = 0; idx < 8; idx++)
	{
	// $[TI4]
		sumCount += RExhPressureSensor.getCount();
		sumCount2 += RSecondExhPressureSensor.getCount();
		Task::Delay(0, SM_CYCLE_TIME);
	}
	// end for

	Int16 exhZeroCount = sumCount / idx;
  	RExhPressureSensor.setZeroOffset(exhZeroCount) ;
	Uint16 exhZeroCount2 = sumCount2 / idx;
  	RSecondExhPressureSensor.setZeroOffset(exhZeroCount2) ;



	if (exhZeroCount < MIN_ZERO_COUNT || exhZeroCount > MAX_ZERO_COUNT)
	{
	// $[TI5]
		
		if (result == INSP_AUTOZERO_FAILED)
		{
		// $[TI5.1]
			result = BOTH_AUTOZERO_FAILED;
		}
		else
		{
		// $[TI5.2]
			// insp autozero must have passed
			CLASS_ASSERTION (result == AUTOZERO_PASSED);

			result = EXH_AUTOZERO_FAILED;
		}			

#if defined(SIGMA_DEVELOPMENT) && !defined(SIGMA_UNIT_TEST)
	printf("SmUtilities: EXH AUTOZERO FAILED!!\n");
	printf("Exh autozero count = %d\n", exhZeroCount);
#endif
	}
	// $[TI6]
	// implied else: exh autozero passed

#ifdef SIGMA_DEBUG
	printf("Insp autozero count = %d\n", inspZeroCount);
	printf("Exh autozero count = %d\n", exhZeroCount);
#endif		

  	RInspAutozeroSolenoid.close() ;
  	RExhAutozeroSolenoid.close() ;
  	Task::Delay(0, 40) ;

	return (result);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: openSafetyValve
//
//@ Interface-Description
//	This method commands the safety valve open.  No argument, no
//	return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Command the safety valve open and wait for valve to fully open.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

void
SmUtilities::openSafetyValve( void)
{
// $[TI1]
	CALL_TRACE("SmUtilities::openSafetyValve( void)") ;

	// Open safety valve
	RSafetyValve.open();
	Task::Delay (0, 150);

#ifdef SIGMA_UNIT_TEST
	// CircuitPressureTest_UT::SvWasOpened = TRUE;
	SmUnitTest::SvWasOpened = TRUE;
#endif  // SIGMA_UNIT_TEST

}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: closeSafetyValve
//
//@ Interface-Description
//	This method commands the safety valve closed.  No argument, no
//	return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Command the safety valve closed and wait for valve to fully close.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

void
SmUtilities::closeSafetyValve( void)
{
// $[TI1]
	CALL_TRACE("SmUtilities::closeSafetyValve( void)") ;

	// Close safety valve
	RSafetyValve.close();
	Task::Delay (0, 500);

#ifdef SIGMA_UNIT_TEST
	// CircuitPressureTest_UT::SvWasClosed = TRUE;
	SmUnitTest::SvWasClosed = TRUE;
#endif  // SIGMA_UNIT_TEST

}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: flushTubing
//
//@ Interface-Description
//	This method is used to flush the system with a given gas to ensure
//	accurate expiratory flow readings.  Arguments are a GasType Id and
//	a SigmaState Id, no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Based on the SigmaStateId, determine the flushing volume, timeout, and flow.
//	Establish the flushing flow and wait until the flushing volume is reached
//	or until timeout occurs, then stop the flow.
//-----------------------------------------------------------------------
//@ PreCondition
//	gas type = AIR or O2
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

void
SmUtilities::flushTubing (GasType gasType, SigmaState state)
{
	CALL_TRACE("SmUtilities::flushTubing (GasType gasType, SigmaState state)");

	CLASS_PRE_CONDITION( gasType == AIR || gasType == O2);

	// calculate gas flushing volume, timeout, and flow
	Real32 volume = NEO_CKT_FLUSHING_VOLUME;
	Uint32 timeout = NEO_CKT_MAX_FLUSHING_TIME;
	Real32 flow = 0;
	
	if (state == STATE_SERVICE)
	{
	// $[TI1]
		// Service mode:
		volume = SERVICE_FLUSHING_VOLUME;
		timeout = SERVICE_MAX_FLUSHING_TIME;
		flow = 120.0;  // lpm
	}
	else if (state == STATE_SST)
	{
	// $[TI2]
		// SST:
		// get current circuit type from Settings-Validation
		const DiscreteValue  CIRCUIT_TYPE =
			PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

		switch (CIRCUIT_TYPE)
		{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		// $[TI2.2]
			volume  = PED_CKT_FLUSHING_VOLUME;
			timeout = PED_CKT_MAX_FLUSHING_TIME;
			flow    = 120.0;  // lpm
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :
		// $[TI2.1]
			volume  = ADULT_CKT_FLUSHING_VOLUME;
			timeout = ADULT_CKT_MAX_FLUSHING_TIME;
			flow    = 120.0;  // lpm
			break;
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
		// $[TI2.3]
			volume  = NEO_CKT_FLUSHING_VOLUME;
			timeout = NEO_CKT_MAX_FLUSHING_TIME;
			flow    = 30.0;  // lpm
			break;
		default :
			// unexpected circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(CIRCUIT_TYPE);
			break;
		}
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE (state);

	}
	// end else

	RTestData.resetDeliveredVolume();
	RSmFlowController.establishFlow( flow, DELIVERY, gasType);

	for ( Uint32 timer = 0;
		  RTestData.getDeliveredVolume() < volume && timer < timeout;
		  timer += SM_CYCLE_TIME )
	{
	// $[TI3]

	#ifdef SIGMA_UNIT_TEST
		SmUnitTest::ForLoopExecuted = TRUE;
	#endif  // SIGMA_UNIT_TEST

		// wait for flushing volume to be delivered or
		// until timeout
		Task::Delay( 0, SM_CYCLE_TIME);
	}
	// end for

#ifdef SIGMA_UNIT_TEST
	SmUnitTest::Volume = volume;
#endif  // SIGMA_UNIT_TEST

	RSmFlowController.stopFlowController();
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: commandExhValve
//
//@ Interface-Description
//	This method slowly ramps the exhalation valve from 0 counts to a final
//	level indicated by the argument.  No return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Starting at 0 DAC counts, increment the exhalation valve every service
//	mode cycle until the desired count is reached.
//-----------------------------------------------------------------------
//@ PreCondition
//	desiredCommand must be within min and max values.
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

void
SmUtilities::commandExhValve(Int16 desiredCommand)
{
	CALL_TRACE("SmUtilities::commandExhValve(Int16 desiredCommand)");

	CLASS_PRE_CONDITION( desiredCommand >= 0 &&
			 desiredCommand <= MAX_COUNT_VALUE );

	Int16 exhValveCommand = 0;

	// Ramp up to the desired command
	while (exhValveCommand < desiredCommand)
	{
	// $[TI1]

	#ifdef SIGMA_UNIT_TEST
		SmUnitTest::WhileDoLoop += 1;
	#endif  // SIGMA_UNIT_TEST

		exhValveCommand += EXH_VALVE_STEP_SIZE;

		// Limit the command to the desired command
		if (exhValveCommand > desiredCommand)
		{
		// $[TI1.1]

		#ifdef SIGMA_UNIT_TEST
			SmUnitTest::IfBranch += 1;
		#endif  // SIGMA_UNIT_TEST

			exhValveCommand = desiredCommand;
		}
		// $[TI1.2]
		// implied else

	#ifdef SIGMA_UNIT_TEST
		SmUnitTest::ImpliedElseBranch += 1;
	#endif  // SIGMA_UNIT_TEST

// E600 BDIO		RExhValveDacPort.writeRegister(exhValveCommand);

		Task::Delay (0, SM_CYCLE_TIME);
	}
	// end while
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: openExhValve
//
//@ Interface-Description
//	This method is used to command the exhalation valve open.  No arguments,
//	no return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Set EV damping gain DAC to min value, EV DAC to 0 counts, delay for
//	commands to take effect.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

void
SmUtilities::openExhValve(void)
{
	// $[TI1]
	CALL_TRACE("SmUtilities::openExhValve(void)");


// E600 BDIO	REVDampingGainDacPort.writeRegister(MIN_DAMPING_GAIN);
	RExhalationValve.commandValve( MIN_DAC_COUNT );

	Task::Delay( 0, 200);

#ifdef SIGMA_UNIT_TEST
	SmUnitTest::ExhValveWasOpened = TRUE;
#endif  // SIGMA_UNIT_TEST

}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: popPsols
//
//@ Interface-Description
//	This method is used to exercise ("pop") the PSOLs.  No argument, no
//	return value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Cycle the PSOLs open and closed at 10 Hz for 10 secs using
//	50% duty cycle and 600 mA current.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

void
SmUtilities::popPsols(void)
{
	CALL_TRACE("SmUtilities::popPsols(void)");

	const Int32 NUM_CYCLES = 100;
	const Uint32 PSOL_ON_TIME_MSECS = 50;
	const Uint32 PSOL_OFF_TIME_MSECS = 50;
	const Real32 POP_PSOL_CURRENT = 600.0;  // mA
	const Int16 POP_PSOL_COUNT =(Int16)
				(POP_PSOL_CURRENT / PSOL_COUNTS_TO_MA);  // psol dac cnts

	for (Int32 idx = 0; idx < NUM_CYCLES; idx++)
	{
	// $[TI1]
		RAirPsol.updatePsol(POP_PSOL_COUNT);
		RO2Psol.updatePsol(POP_PSOL_COUNT);
		Task::Delay( 0, PSOL_ON_TIME_MSECS);

		RAirPsol.updatePsol(0);
		RO2Psol.updatePsol(0);
		Task::Delay( 0, PSOL_OFF_TIME_MSECS);
	#ifdef SIGMA_UNIT_TEST
		SmUnitTest::AirPsolIsOpened = TRUE;
	#endif  // SIGMA_UNIT_TEST
	}
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: selectGasType
//
//@ Interface-Description
//	This method selects the gas type for use in a test based on what
//	gas sources are available and given a selection priority.  No
//	argument, returns a GasType Id for the selected gas.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	If wall air is present, return a selection of "air".
//	Else if O2 is present, return a selection of "O2".
//	Else return a selection of "air".
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=======================================================================

GasType
SmUtilities::selectGasType (void)
{
	CALL_TRACE("SmUtilities::selectGasType (void)");
	
	GasType desiredGas;

	if (RSmGasSupply.isWallAirPresent())
	{
	// $[TI1]
		desiredGas = AIR;
	}
	else if (RSmGasSupply.isO2Present())
	{
	// $[TI2]
		desiredGas = O2;
	}
	else
	{
	// $[TI3]
		// default to air if either compressor air is available
		// or no gas is available
		desiredGas = AIR;
	}

	return (desiredGas);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: getBpsStatus
//
//@ Interface-Description
//  This method is used to return the backup power system (BPS) status.
//  No arguments.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Read the BPS charged and charging bits to determine and return
//	the BPS status.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
SmUtilities::TestResult
SmUtilities::getBpsStatus( void)
{
	CALL_TRACE("SmUtilities::getBpsStatus( void)");

	TestResult bpsStatus;

	// Check if the BPS is installed
//	Real32 bps = RSystemBatteryModel.getValue();

// E600 BDIO	if ( (bps >= MIN_BPS_MODEL_1_VOLTAGE) && (bps <= MAX_BPS_MODEL_1_VOLTAGE) )
	{
	// $[TI1]
		// BPS is installed -- get its current status
// E600 BDIO	   	if ( ( bpsCharging_.getState() == BinaryIndicator::OFF ) &&
// E600 BDIO			 ( bpsCharged_.getState() == BinaryIndicator::ON ) )
	   	{
	   	// $[TI1.1]
// E600 BDIO	       	bpsStatus = CHARGING;
	   	}
// E600 BDIO	   	else if ( ( bpsCharged_.getState() == BinaryIndicator::OFF ) &&
// E600 BDIO	              ( bpsCharging_.getState() == BinaryIndicator::ON ) )
		{
		// $[TI1.2]
// E600 BDIO			bpsStatus = CHARGED;
		}
// E600 BDIO		else if ( ( bpsCharging_.getState() == BinaryIndicator::ON ) &&
// E600 BDIO		          ( bpsCharged_.getState() == BinaryIndicator::ON ) )
		{
		// $[TI1.3]
// E600 BDIO			bpsStatus = ON_BATTERY_POWER;
		}
// E600 BDIO		else 
		{
		// $[TI1.4]
			bpsStatus = UNKNOWN;
		}
	}
// E600 BDIO	else
	{
	// $[TI2]
		bpsStatus = NOT_INSTALLED;
	}

	return (bpsStatus);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: checkAcPower
//
//@ Interface-Description
//  This method is used to verify that AC power is connected.
//  No arguments, returns a test result ID.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	If BPS status indicates system is running on BPS power, return
//	"AC connected" result ID.
//	Else if BPS status indicates system is running on BPS power,
//	prompt operator to connect AC then wait for keypress.
//	If EXIT button was pressed, return "operator exit" result ID.
//	Else if ACCEPT was presed:
//		If BPS status still indicates BPS power use, return "AC not
//		connected" result ID.
//		Else if BPS status indicates not running on BPS power,
//		return "AC connected" result ID.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
SmUtilities::TestResult
SmUtilities::checkAcPower( void)
{
	CALL_TRACE("SmUtilities::checkAcPower( void)");

	TestResult status = AC_CONNECTED;

	if (getBpsStatus() == ON_BATTERY_POWER)
	{
	// $[TI1]
		if (!messagePrompt( SmPromptId::CONNECT_AC_PROMPT))
		{
		// $[TI1.1]
			status = OPERATOR_EXIT;
		}
		else
		{
		// $[TI1.2]
			// operator hit the ACCEPT key

			// check if system is still running on BPS power
			status = getBpsStatus();
#ifdef SIGMA_UNIT_TEST
			status = SmUtilities_UT::bpsStatus;
#endif  // SIGMA_UNIT_TEST

			if (status == ON_BATTERY_POWER)
			{
			// $[TI1.2.1]
				status = AC_NOT_CONNECTED;
			}
			else
			{
			// $[TI1.2.2]
				status = AC_CONNECTED;
			}
			// end else
		}
		// end else
	}
	else
	{
	// $[TI2]
		status = AC_CONNECTED;
	}
	// end else

	return( status);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: pressureTimeout
//
//@ Interface-Description
//	This method calculates a timeout for system pressurization.  Ventilator
//	state, target pressure, and pressurizing flow are arguments, and
//	a calculated timeout in msec is returned.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Determine worst-case compliance based on ventilator state
//	(if the state is service mode, worst-case compliance varies
//	depending on whether the test is remote or EST).
//	Adjust the input pressure upwards by the pressure measurement error.
//	Adjust the input flow downwards by the flow measurement error and
//	by a leak margin.
//	Calculate timeout based on worst-case compliance, adjusted pressure,
//	and adjusted flow, add an additional flow controller delay margin.
//	Return the calculated timeout.
//
//	Note that RSmManager.getCurrentTestType() can be SM_MISC_TYPE
//	when the test is exhalavation valve calibration.
//-----------------------------------------------------------------------
//@ PreCondition
//  ventilator state = Service Mode or SST, flow > 0
//
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Uint32
SmUtilities::pressureTimeout( SigmaState state,
							  Real32 pressure,
							  Real32 flow )
{
	CALL_TRACE("SmUtilities::pressureTimeout( void)");

	CLASS_PRE_CONDITION( flow > 0.0);

	//Neonantal value is given as initial value to remove warning.
	Real32 compliance = NEO_COMPLIANCE_FAILURE_HIGH;

	SmTestId::ServiceModeTestTypeId testType = RSmManager.getCurrentTestType();
	switch(testType)
	{
	case (SmTestId::SM_EXTERNAL_TYPE):
		// $[TI1]
		AUX_CLASS_ASSERTION(state == STATE_SERVICE, state);
		// NOTE: this compliance includes margin for VIPER
		compliance = REMOTE_TEST_MAX_COMPLIANCE;  // ml/cmH2O
		break;
	case (SmTestId::SM_EST_TYPE):
	case (SmTestId::SM_MISC_TYPE):
		// $[TI2]
		AUX_CLASS_ASSERTION(state == STATE_SERVICE, state);
		// for EST or Exhalation Valve Calibration
		compliance = EST_MAX_COMPLIANCE;  // ml/cmH2O
		break;
	case (SmTestId::SM_SST_TYPE):
		// $[TI3]
		AUX_CLASS_ASSERTION(state == STATE_SST, state);
		// for SST
		// add margin to the worst-case patient circuit compliance
		compliance = ADULT_COMPLIANCE_FAILURE_HIGH * 1.25F;  // ml/cmH2O
		break;
	default:
		AUX_CLASS_ASSERTION_FAILURE(testType);
		break;
	};

	// adjust the pressure upwards by the pressure measurement error
	Real32 pressureAdjusted =
		pressure * (1 + REL_PRESSURE_ERROR) + ABS_PRESSURE_ERROR;  // cm

	// adjust the flow downwards by the flow measurement error and
	// leak margin (assume worst-case leak of 5% of the flow)
	Real32 flowAdjusted =
		flow * (1 - flowError( flow) - 0.05F);  // lpm

	// calculate timeout based on compliance, adjusted pressure,
	// and adjusted flow, add 100 msec to allow flow to reach peak
	// after flow controller is started up
	Uint32 timeout =(Uint32)( 100 +
		compliance * pressureAdjusted / flowAdjusted * 60.0F);  // msec

	return( timeout);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: flowError
//
//@ Interface-Description
//	This method takes a flow level and a MeasurementSide as arguments,
//	and returns the corresponding flow sensor error.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Known flow error points are at 1, 5, 10, 60, and 120 lpm. For a given
//	input flow level and MeasurementSide, return the flow error for
//	the nearest known point below the input flow level, use the 1 lpm point
//	for flows less than 1 lpm.
//-----------------------------------------------------------------------
//@ PreCondition
//  side == DELIVERY or EXHALATION
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Real32
SmUtilities::flowError( Real32 flow, MeasurementSide side)
{
	CALL_TRACE("SmUtilities::calculatePressureTimeout( Real32 flow, \
											MeasurementSide side)");

	CLASS_PRE_CONDITION( side == DELIVERY || side == EXHALATION);

	Real32 error = 0.0;
	const Real32 OFFSET = 0.1;	// lpm
	const Real32 FLOW_5_LPM = 5.0;  // lpm
	const Real32 FLOW_10_LPM = 10.0;  // lpm
	const Real32 FLOW_60_LPM = 60.0;  // lpm
	const Real32 FLOW_120_LPM = 120.0;  // lpm

	if (flow < FLOW_5_LPM - OFFSET)
	{
	// $[TI1]
		// Flow error at 1 lpm
		if (side == DELIVERY)
		{
		// $[TI1.1]
			error = INSP_FLOW_ERROR_AT_1_LPM;
		}
		else
		{
		// $[TI1.2]
			error = EXP_FLOW_ERROR_AT_1_LPM;
		}
	}
	else if (flow < FLOW_10_LPM - OFFSET)
	{
	// $[TI2]
		// Flow error at 5 lpm
		if (side == DELIVERY)
		{
		// $[TI2.1]
			error = INSP_FLOW_ERROR_AT_5_LPM;
		}
		else
		{
		// $[TI2.2]
			error = EXP_FLOW_ERROR_AT_5_LPM;
		}
	}
	else if (flow < FLOW_60_LPM - OFFSET)
	{
	// $[TI3]
		// Flow error at 10 lpm
		if (side == DELIVERY)
		{
		// $[TI3.1]
			error = INSP_FLOW_ERROR_AT_10_LPM;
		}
		else
		{
		// $[TI3.2]
			error = EXP_FLOW_ERROR_AT_10_LPM;
		}
	}
	else if (flow < FLOW_120_LPM - OFFSET)
	{
	// $[TI4]
		// Flow error at 60 lpm
		if (side == DELIVERY)
		{
		// $[TI4.1]
			error = INSP_FLOW_ERROR_AT_60_LPM;
		}
		else
		{
		// $[TI4.2]
			error = EXP_FLOW_ERROR_AT_60_LPM;
		}
	}
	else
	{
	// $[TI5]
		// Flow error at 120 lpm
		if (side == DELIVERY)
		{
		// $[TI5.1]
			error = INSP_FLOW_ERROR_AT_120_LPM;
		}
		else
		{
		// $[TI5.2]
			error = EXP_FLOW_ERROR_AT_120_LPM;
		}
	}

	return( error);
}		

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: getMaximumFlow
//
//@ Interface-Description
//	This method has time to wait to obtain maximum flow and the gas type as
//	arguments.  This method returns the maximum flow that the ventilator
//	can deliver after the time passed in.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Ramp the psol to its maximum.  Wait for a duration given by the argument
//	passed in.  Sample the flow.  Return the measured flow.
//
//	NOTE: the psol is NOT closed at the end of this method -- the calling
//	method is responsible for closing it.
//-----------------------------------------------------------------------
//@ PreCondition
//	desiredGas == AIR || desiredGas == O2
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Real32
SmUtilities::getMaximumFlow( const Uint32 waitTimeSecs, const GasType gasType)
{
	CALL_TRACE("SmUtilities::getMaximumFlow( const Uint32 waitTimeSecs, \
				const GasType gasType)");

	// time in milliseconds to ramp psol from 0 to the max DAC count
	const Int32 PSOL_RAMP_TIME_MSECS = 500;
	// # of steps to ramp the psol
	const Int32 NUMBER_OF_PSOL_RAMP_STEPS =
		PSOL_RAMP_TIME_MSECS / SM_CYCLE_TIME;
	// psol current increment per step, in DAC counts
	const Int32 PSOL_INCREMENT =
		MAX_COUNT_VALUE / NUMBER_OF_PSOL_RAMP_STEPS;
	// psol current at start of ramp, in DAC counts
	const Int32 STARTING_PSOL_COUNT	=
			MAX_COUNT_VALUE % PSOL_INCREMENT;

	Real32 maxFlow = 0.0 ;
	
	if (gasType == AIR)
	{
	// $[TI1]
		for (	DacCounts psolCount = STARTING_PSOL_COUNT;
				psolCount <= MAX_COUNT_VALUE;
				psolCount += PSOL_INCREMENT )
		{		
		// $[TI1.1]
			RAirPsol.updatePsol( psolCount) ;
			Task::Delay( 0, SM_CYCLE_TIME);
		}

		Task::Delay( waitTimeSecs, 0) ;

		maxFlow = RAirFlowSensor.getValue() ;
	}
	else if (gasType == O2)
	{
	// $[TI2]
		for (	DacCounts psolCount = STARTING_PSOL_COUNT;
				psolCount <= MAX_COUNT_VALUE;
				psolCount += PSOL_INCREMENT )
		{		
		// $[TI2.1]
			RO2Psol.updatePsol( psolCount) ;
			Task::Delay( 0, SM_CYCLE_TIME);
		}

		Task::Delay( waitTimeSecs, 0) ;

		maxFlow = RO2FlowSensor.getValue() ;
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(gasType);
	}
	// end else

	return( maxFlow) ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: atmPressureBounded
//
//@ Interface-Description
//	This method has no arguments and returns the atmospheric pressure
//	transducer reading in cmH2O bounded to a min and max value.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Read the atmospheric pressure transducer.
//	Apply min and max bounds.
//	Return the bounded atmospheric pressure reading.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Real32
SmUtilities::atmPressureBounded(void)
{
	CALL_TRACE("SmUtilities::atmPressureBounded(void)");

	Real32 atmPressure = RAtmosphericPressureSensor.getValue();

	if (atmPressure < MIN_ATM_PRESSURE)
	{
	// $[TI1]
		atmPressure = MIN_ATM_PRESSURE;
	}
	else if (atmPressure > MAX_ATM_PRESSURE)
	{
	// $[TI2]
		atmPressure = MAX_ATM_PRESSURE;
	}
	// $[TI3]
	// implied else

	return (atmPressure);  // cmH2O
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: btpsToStpd
//
//@ Interface-Description
//	This method takes a BTPS value (flow or volume) and an atmospheric
//	pressure in cmH2O as arguments and returns the flow or volume
//	converted to STPD.
//-----------------------------------------------------------------------
//@ Implementation-Description
//	Convert input BTPS value to an STPD value.
//	Return the STPD value.
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=======================================================================
Real32
SmUtilities::btpsToStpd(Real32 btpsValue, Real32 atmPressure)
{
// $[TI1]
	CALL_TRACE("SmUtilities::btpsToStpd(Real32 btpsValue)");

	Real32 stpdValue = btpsValue *
		TEMPERATURE_SLPM / TEMPERATURE_BTPS *
		(atmPressure - VAPOR_PRESS_BTPS) /
		STD_ATM_PRESSURE;
	
	return (stpdValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SmUtilities::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
	 		const char*        pFileName,
	 		const char*        pPredicate)
{
	CALL_TRACE("SmUtilities::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, SMUTILITIES,
  				 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
