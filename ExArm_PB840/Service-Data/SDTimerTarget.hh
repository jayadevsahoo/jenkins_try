#if defined FAKE_SM
// This class is used only for test simulation.

#ifndef SDTimerTarget_HH
#define SDTimerTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SDTimerTarget - A target to handle Service Data  Timer events.
// Classes can specify this class as an additional parent class register to be
// informed of timer events.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/SDTimerTarget.hhv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
#include "SDTimerId.hh"
#include "ServiceDataClassIds.hh"
//@ End-Usage

class SDTimerTarget
{
public:
	virtual void timerEventHappened(SDTimerId::SDTimerIdType timerId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	SDTimerTarget(void);
	virtual ~SDTimerTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	SDTimerTarget(const SDTimerTarget&);	// not implemented...
	void operator=(const SDTimerTarget&);	// not implemented...
};


#endif // SDTimerTarget_HH 


#endif	// FAKE_SM
