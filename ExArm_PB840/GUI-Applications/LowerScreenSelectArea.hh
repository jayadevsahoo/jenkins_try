#ifndef LowerScreenSelectArea_HH
#define LowerScreenSelectArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LowerScreenSelectArea - The area in the Lower screen which
// contains the four sub-screen select buttons for Vent Setup, Apnea
// Setup, Alarm Setup and Other Screens.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LowerScreenSelectArea.hhv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: rhj    Date: 05-27-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added bdEventHappened and redrawButtonLabels_ methods.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"
#include "GuiEventTarget.hh"

//@ Usage-Classes
#include "Bitmap.hh"
#include "Box.hh"
#include "TabButton.hh"
#include "GuiAppClassIds.hh"
#include "BdEventRegistrar.hh"
#include "BdEventTarget.hh"
//@ End-Usage

class LowerScreenSelectArea : public Container, 
	                          public GuiEventTarget, 
	                          public BdEventTarget
{
public:
	LowerScreenSelectArea(void);
	~LowerScreenSelectArea(void);

	void initialize(void);
	void setBlank(Boolean blank);

	// GuiEventTarget virtual method
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	inline TabButton *getApneaSetupTabButton(void);
	inline TabButton *getLowerOtherScreensTabButton(void);
	void setVentSetupTabTitleText(StringId title);
	void setApneaSetupTabTitleText(StringId title);
	void setAlarmSetupTabTitleText(StringId title);

	// BdEventTarget virtual method
	virtual void bdEventHappened(EventData::EventId eventId,
								 EventData::EventStatus eventStatus,
								 EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
protected:

private:
	// these methods are purposely declared, but not implemented...
	LowerScreenSelectArea(const LowerScreenSelectArea&);		// not implemented...
	void   operator=(const LowerScreenSelectArea&);	// not implemented...

	void redrawButtonLabels_(Boolean enableProx);

	//@ Data-Member: ventSetupTabButton_
	// The Vent Setup tab button, displays SETUP and activates the
	// Vent Setup sub-screen.
	TabButton ventSetupTabButton_;

	//@ Data-Member: apneaSetupTabButton_
	// Apnea Setup tab button, displays APNEA and activates the Apnea
	// Setup sub-screen.
	TabButton apneaSetupTabButton_;

	//@ Data-Member: alarmSetupTabButton_
	// Alarm Setup tab button, displays an alarm bitmap and activates
	// the Alarm Setup sub-screen.
	TabButton alarmSetupTabButton_;

	//@ Data-Member: lowerOtherScreensTabButton_
	// Other Screens tab button, displays a multi-screen bitmap and
	// activates the Lower Other Screens sub-screen.
	TabButton lowerOtherScreensTabButton_;

	//@ Data-Member: alarmSetupBitmap_
	// Bitmap for the Alarm Setup tab button.
	Bitmap alarmSetupBitmap_;

	//@ Data-Member: lowerOtherScreensBitmap_
	// Bitmap for the Other Screens tab button.
	Bitmap lowerOtherScreensBitmap_;

	//@ Data-Member: topLineBox_
	// Box used for displaying white line at the top of this area
	Box topLineBox_;

};

// Inlined methods
#include "LowerScreenSelectArea.in"

#endif // LowerScreenSelectArea_HH 
