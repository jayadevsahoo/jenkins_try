#ifndef TubeIdSetting_HH
#define TubeIdSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TubeIdSetting - Tube I.D. (Interior Diameter) Setting
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TubeIdSetting.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
//
//  Revision: 004   By: sah   Date:  07-Aug-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added 'getAbsoluteMinValue_()' for new PA-based limit
//
//  Revision: 003   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  soft-limit framework was re-designed such that settings with
//         soft limits need only define new 'findSoftMinMaxValues_()'
//         method, therefore all other "soft" methods were removed
//
//  Revision: 002   By: sah   Date:  06-Jan-1999    DR Number:  5314
//  Project:  ATC
//  Description:
//     Added new 'getApplicability()' method.
//
//  Revision: 001   By: sah   Date:  06-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage


class TubeIdSetting : public BatchBoundedSetting
{
	public:
		TubeIdSetting(void); 
		virtual ~TubeIdSetting(void);

		virtual Applicability::Id  
		getApplicability( const Notification::ChangeQualifier qualifierId) const;

		virtual SettingValue
		getNewPatientValue(void) const;

		virtual void
		acceptTransition(const SettingId::SettingIdType settingId,
						 const DiscreteValue toValue,
						 const DiscreteValue fromValue);

		static void  SoftFault(const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL);

	protected:
		virtual void  updateConstraints_(void);

		virtual void  findSoftMinMaxValues_(Real32& rSoftMinValue,
											Real32& rSoftMaxValue) const;

		virtual Real32  getAbsoluteMinValue_(void) const;

	private:
		// not implemented...
		TubeIdSetting(const TubeIdSetting&);
		void  operator=(const TubeIdSetting&);
};


#endif // TubeIdSetting_HH 
