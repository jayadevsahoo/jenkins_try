#ifndef	NetworkMsgId_HH
#define	NetworkMsgId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename: NetworkMsgId.hh - Enum defines of the Network Communication 
//      Messages IDs
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/NetworkMsgId.hhv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 007   By: gdc   Date:  16-May-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       Added XmitDataMsgIds for the new maneuvers 
//        
//  Revision: 006   By: gdc   Date:  13-March-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Implement required changes for Phase 1 NIV 
//
//  Revision: 005   By: ljstar   Date:  24-March-2003  SCR Number: 5313
//  Project:  PAV
//  Description:
//      Added PAV code from PAV branch tip.
//  
//  Revision: 004   By: yakovb   Date:  12-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//      Handle VC+/VS Startup messages.
//
//  Revision: 003   By: syw   Date:  01-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added COMPUTED_PAV_DATA
//
//  Revision: 002  By:   yyy     Date:  26 JAN 98     DR Number: 
//       Project:  BiLevel
//       Description:
//       	BiLevel Initial Release.  Added BiLevel and pause related
//			message ids.
//
//  Revision: 001  By:  <jhv>    Date:  06-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================


enum XmitDataMsgId
{
// Do not insert or delete the following network message types,
// up to the "SETTINGS_TRANSACTION_MSG" message id. Outside devices
// are expecting the exact message ids.
	LOWEST_NETWORK_MSG_ID 	=  9900,
	END_OF_BREATH_DATA  	= 9900,
	REALTIME_BREATH_DATA 	= 9901, 
	AVERAGED_BREATH_DATA	=9902,
	WAVEFORM_BREATH_DATA	=9903, 	
	SETTINGS_PARAMETER_DATA =9904,
	ALARM_PARAMETER_DATA	=9905,
	SETTINGS_TRANSACTION_MSG=9906,
// You can add your own Network Message here.
	BD_ALIVE_MSG = 9907,				//Network-Application
	GUI_ALIVE_MSG = 9908,				//Network-Application
	ACK_MSG_FOR_RECVDATA= 9909,   	//Network-Application
	ALARM_UPDATE_DATA_MSG_ID= 9910,
    SEND_EVENT_MSG_ID= 9911,
	BD_NODE_READY_MSG_ID= 9912,
	GUI_NODE_READY_MSG_ID= 9913,
	BLOCKIO_TEST_MSG11= 9914,
	BLOCKIO_TEST_MSG99= 9915,
	TEST_PROMPT_DATA= 9916,
	TEST_STATUS_DATA= 9917,
	TEST_POINT_DATA= 9918,
	SYNC_CLOCK_MSG_ID= 9919,
	SERVICE_MODE_MSG= 9920,
	SERVICE_MODE_ACTION= 9921,
	BD_NOVRAM_DATA= 9922,
	USER_EVENT_REQUEST_MSG= 9923,
	EVENT_STATUS_MSG= 9924,
	BD_DEBUG_MSG= 9925,
	ALL_EVENT_STATUS_MSG= 9926,
	BD_MONITORED_DATA_MSG= 9927,
	EXH_VALVE_PRESS_MSG= 9928,
	BD_IP_ADDRESS_MSG= 9929,
	REBOOT_REQUEST_MSG_ID= 9930,
	PAUSED_PEEND_DATA= 9931, 
	CONFIRM_OFF_KEEP_ALIVE_MSG= 9932,
	CONFIRM_ON_KEEP_ALIVE_MSG= 9933,
	OFF_KEEP_ALIVE_MSG= 9934,
	ON_KEEP_ALIVE_MSG= 9935,
	PEEP_RECOVERY_MSG = 9936,
	FIO2_FOR_GUI_MSG = 9937,
	PAUSED_PIEND_DATA = 9938, 
	COMPUTED_INSP_PAUSE_DATA = 9939,
	BILEVEL_MAND_DATA = 9940,
	BILEVEL_MAND_EXH_DATA = 9941,
	END_EXP_PAUSE_PRESSURE_STATE = 9942,
	PAV_BREATH_DATA = 9943,
	PAV_PLATEAU_DATA = 9944,  
	PAV_STARTUP_DATA = 9945,
	VTPCV_STATE_DATA = 9946,
	NIF_PRESSURE_DATA = 9947, 
	P100_PRESSURE_DATA = 9948, 
	VITAL_CAPACITY_DATA = 9949, 
    TOUCHED_POS_DATA = 9950,
	HIGHEST_NETWORK_MSG_ID  = TOUCHED_POS_DATA
};  

#endif	// NetworkMsgId_HH
