#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== C L A S S   D E S C R I P T I O N ====
//@ Class: DciNumber - Store a Real32 value with an associated validity tag
//---------------------------------------------------------------------
//@ Interface-Description
// This object holds a Real32 value and a tag indicating whether the value is
// valid or not.  "Not" can be one of invalid, not supported, not in this
// release and to be determined.
//---------------------------------------------------------------------
//@ Rationale
// Need a numeric value in DCI that also has an indication of whether
// the datum is valid or not.
//---------------------------------------------------------------------
//@ Implementation-Description
// Derive from base class, DciValueStatus, the validity tag.  Whenever
// we put() a value, it is defined to be valid.
//---------------------------------------------------------------------
//@ Module-Decomposition
//	DciNumber [constructor]
//	~DciNumber [destrcutor]
//	get
//---------------------------------------------------------------------
//@ Fault-Handling
// none
//---------------------------------------------------------------------
//@ Restrictions
// Only floating-point numeric values are supported.
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/DCI/vcssrc/DciNumber.ccv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 17-DEC-1997  DR Number: 2605
//  Project: Sigma (R8027)
//  Description:
//      Restructuring of DCI to fix several problems. 
//
//  Revision: 1.0	By: jdm	Date: 7/26/95	DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "Sigma.hh"
#include "DciNumber.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DciNumber [constructor]
//
//@ Interface-Description
// Do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

DciNumber::DciNumber()
{
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DciNumber [destructor]
//
//@ Interface-Description
// Do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

DciNumber::~DciNumber()
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  get
//
//@ Interface-Description
// Return the stored floating point value's status by name.  Optionally,
// return the floating point value, if the user passed a buffer to hold
// it.
// NOTE: The burden is on the caller to verify that the value is valid prior
//	 to its use.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

DciValueStatus::DciValueStatusId
DciNumber::get(Real32 * pNumber)
{
	DciValueStatus::DciValueStatusId status = getStatus();

	if (pNumber != NULL)
	{						// $[TI1.1]
		if ( isOk() )
		{
			// $[TI2.1]
			*pNumber = number_;
		}
		else
		{
			// $[TI2.2]
			*pNumber = 0;
		}
	}
	// $[TI1.2]

	return(status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// A fault handling assertion has been taken.
//---------------------------------------------------------------------
//@ PostCondition
// There is no return from this function.
//@ End-Method
//=====================================================================

void
DciNumber::SoftFault(const SoftFaultID softFaultID,
		const Uint32	lineNumber,
		const char *	pFileName,
		const char *	pPredicate)
{
	FaultHandler::SoftFault(softFaultID, DCI_DEVICE, DCI_NUMBER_CLASS,
		lineNumber, pFileName, pPredicate);
}
