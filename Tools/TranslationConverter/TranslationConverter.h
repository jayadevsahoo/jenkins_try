//header file for TranslationConverter.cpp

#ifndef TRANSLATIONCONVERTER_H
#define TRANSLATIONCONVERTER_H

static const TCHAR      LF                                =   0x000A;
static const TCHAR      CR                                =   0x000D;
static const TCHAR      BACK_SLASH                        =   0x005C;
static const TCHAR      LOWER_CASE_N                      =   0x006E;
static const TCHAR      DOUBLE_QUOTE                      =   0x0022;
static const TCHAR      POUND_SIGN                        =   0x0023;
static const TCHAR      DOLLAR_SIGN                       =   0x0024;
static const TCHAR      LineTerminator[]                  =   _T("\r\n");
static const int        MAX_STRING_LENGTH                 =   2000;
static const int        MAX_STRING_LENGTH_IN_XML_FILE     =   10000;
static const int        MAX_FILE_PATH                     =   200;
static const int        MAX_LANGUAGES                     =   15;
static const int        MAX_DIRECTORY_NAME_LENGTH         =   200;
static const int        MAX_LANGUAGE_NAME_LENGTH          =   8;
static const int        EXTENSION_ETC_LENGTH              =   5;
static const int        LANGUAGE_NAME_LENGTH              =   25;
static const int        MAX_STRING_SIZE_LENGTH            =   100;
static const char       EXECUTABLE_FILE_NAME[]            =   "TranslationConverter.exe";
static const TCHAR      EndOfDataMarker[]                 =   _T("COMMENTS:");
static const TCHAR      FontTagName[]                     =   _T("Font");
static const TCHAR      FontSizeAttributeName[]           =   _T("html:Size");
static const TCHAR      FontNameAttributeName[]           =   _T("html:Face");
static const TCHAR      CellStyleAttributeName[]          =   _T("ss:StyleID");
static const TCHAR      StyleFontName[]                   =   _T("ss:FontName");
static const TCHAR      StyleFontSize[]                   =   _T("ss:Size");
static const TCHAR      Style_SS_ID[]                     =   _T("ss:ID");
static const TCHAR      StyleTag[]                        =   _T("Style");  //child node of Styles tag
static const TCHAR      StylesTag[]                       =   _T("Styles"); //parent node of Style tag
static const TCHAR      Font_Name_Medical[]               =   _T("R");
static const TCHAR      Font_Name_Arial[]                 =   _T("A");
static const TCHAR      Font_Name_MSUIGothic[]            =   _T("M");
static const TCHAR      Font_Name_SimSun[]                =   _T("S");
static const TCHAR      LOWER_CASE_SLASH_F[]              =   _T("\\f");
static const TCHAR      LOWER_CASE_SLASH_W[]              =   _T("\\w");
static const TCHAR      LOWER_CASE_SLASH_H[]              =   _T("\\h");
static const TCHAR      STRING_SIZE_INFO_DELIMETER[]      =   _T("$");
static const TCHAR      Font_Name_Medical_FullName[]      =   _T("Medical");
static const TCHAR      Font_Name_Arial_FullName[]        =   _T("Arial");
static const TCHAR      Font_Name_SimSun_FullName[]       =   _T("SimSun");
static const TCHAR      Font_Name_MSUIGothic_FullName[]   =   _T("MS UI Gothic");
static const TCHAR      LineFeedCharacter[]               =   _T("\n");
static const TCHAR      PoundSignCharacter[]              =   _T("#");
static const TCHAR      ENGLISH_LANGUAGE_NAME[]           =   _T("ENGLISH");
static const TCHAR      CONTROL_WIDTH_NAME[]              =   _T("WIDTH");
static const TCHAR      CONTROL_HEIGHT_NAME[]             =   _T("HEIGHT");
static const TCHAR      FONT_ESCAPE_SEQUENCE_SIZE         =   5;                // "\fxxF", xx=size, F=font code
static const TCHAR      FONT_CHAR_ESCAPE                  =   0x0066;           // Lower case f
static const TCHAR      BACK_SLASH_CHAR_ESCAPE            =   0x005C;           

enum Language_t
{
    LANGUAGE_ENGLISH = 1,
    LANGUAGE_SPANISH,
    LANGUAGE_CHINESE,
    LANGUAGE_VIETNAMESE,
    LANGUAGE_JAPANESE,
    LANGUAGE_RUSSIAN,
    LANGUAGE_ITALIAN,
    LANGUAGE_POLISH,
    LANGUAGE_FRENCH,
    LANGUAGE_GERMAN,
    LANGUAGE_PORTUGUESE,
    LANGUAGE_GREEK
};

#endif