#ifndef BdGuiEvent_HH
#define BdGuiEvent_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BdGuiEvent - handles event requests to the BD from the GUI
//    and event and ventilator status from the BD to the GUI
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdGuiEvent.hhv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
// 
//  Revision: 007   By: rpr    Date: 23-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Applied code review comments.
// 
//  Revision: 006  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 005   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 004   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Refactored code into CancelManeuver method to accomodate RM
//      maneuvers.
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per Breath-Delivery formal code review
//
//  Revision: 001  By:  kam    Date:  22-Sep-1995    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "EventData.hh"
#include "NetworkApp.hh"

class BdGuiEvent
{
  public:
    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

    static inline EventData::EventStatus GetEventStatus (const EventData::EventId id);
    
    static void RequestUserEvent(const EventData::EventId,
                                 const EventData::RequestedState request = EventData::START,
								 const EventData::MoreData = EventData::NO_O2_DATA);
                                            
    static void ReceiveEventStatus(XmitDataMsgId msgId, void *pData, Uint size);

    static void ReceiveAllEventStatus(XmitDataMsgId msgId, void *pData, Uint size);

    static void Initialize(void);

    static void CommsDown(void);

    static void CancelManeuver(const EventData::EventId eventId);

#if defined SIGMA_UNIT_TEST
    static inline void SetEventStatus (const EventData::EventStatus status, const EventData::EventId id);
#endif //SIGMA_UNIT_TEST

  protected:

	//@ Data-Member: EventStatus_
	// current status of each of the events
	static EventData::EventStatus EventStatus_[NUM_EVENT_IDS];
  
  private:
    BdGuiEvent(void);                      // not implemented...
    ~BdGuiEvent(void);                     // not implemented...
    BdGuiEvent (const BdGuiEvent&);        // not implemented...
    void operator= (const BdGuiEvent&);    // not implemented...
};

// Inlined methods...
#include "BdGuiEvent.in"

#endif  //BdGuiEvent_HH
