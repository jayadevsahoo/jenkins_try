//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================
#ifndef __Barometer_MPL115A2__
#define __Barometer_MPL115A2__

#include "I2C.h"
#include "Barometer.hh"

///@class Barometer_MPL115A2
///@detail Implements the Barometer interface using Freescale MPL115A2
///chip. It uses I2C-based communication.

class Barometer_MPL115A2: public Barometer, public I2C
{
public:
	///@detail Overriding the method from Barometer class. It returns the 
	///raw pressure reading from the sensor. For this implementation, it 
	///does have built-in componsation for temperature.
    virtual float getPressureRaw();

	///@detail Overriding the method from Barometer class. It returns the 
	///raw ADC count for the sensor. The value does not really make much
	///sense for anything other than debugging/engineering development. Unlike
	///the getPressureRaw() API, this value is not temperature-compensated.
	///@return 
	virtual Uint16 getCount();

	///@detail Returns the singleton instance of Barometer_MPL115A2 class
	static Barometer_MPL115A2* getInstance();

protected:
	///@detail Sends the command to MPL115A2 to start the "conversion" - i.e. to
	///convert the pressure/temperature values to its ADC equivalents.
    bool startConversion_();
	///@detail Reads the coefficient data from MPL115A2. The coefficients do not
	///do not change for a given chip over time. These coefficients are utilized
	///for converting ADC to real pressure value.
    bool readCoeficients_ ();
	///@details Reads the pressure and temperature ADC values into the buffer.
    bool readAdc_(uint8_t* buffer);
	///@detail Utiltiy API to convert the data from MPL115A2 into floating point
	///values. 
	float combineBits_(uint8_t msb, uint8_t lsb, uint8_t numTotalBits, 
					   bool isSigned, uint8_t numIntBits, uint8_t fractionZeroPad = 0);

	//@detail Constructor. Invoked only from the getInstance() API above.
	Barometer_MPL115A2(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout = 30);
    ~Barometer_MPL115A2(){}
	//The Coefficients read from MPL115A2. 
    float A0;
    float B1;
    float B2;
    float C12;

	// The last read ADC value
	Uint16 Padc;

	static Barometer_MPL115A2* m_instance;
};

#endif

