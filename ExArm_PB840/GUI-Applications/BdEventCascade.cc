#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdEventCascade - A BdEventTarget which stores a list
// of BdEventTargets.  Allows multiple BdEventTargets to
// register for event notices.
//---------------------------------------------------------------------
//@ Interface-Description
// BdEventCascade inherits directly from BdEventTarget so it can be a
// target for event notices.  The idea is that multiple objects may want to
// be informed when one particular event happens so this class accomplishes
// that by storing an array of BdEventTargets.  Targets can be registered
// via the addTarget() method.
//
// The cascade can then be registered with the particular event and, when it
// receives notification of an event happening via its EventHappened()
// method, it then calls the bdEventHappened() method of each object that
// has registered with it.  Hence, the change is cascaded to all interested
// objects.
//
// BdEventCascade is intended for use by the BdEventRegistrar class.
//---------------------------------------------------------------------
//@ Rationale
// Needed for multiple objects to be notified when a particular event happens
//---------------------------------------------------------------------
//@ Implementation-Description
// The cascade stores a list of BdEventTarget pointers.  As objects register
// with the cascade (via addTarget()) pointers to the objects are stored in the
// array (called targets_[]).  If the cascade is registered as a target of a
// event then it will receive event notice via the EventHappened()
// method.  This method just loops through the pointers in the targets_[] array
// and calls their bdEventHappened() methods.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// There is a limit to the number of targets stored in a cascade.  This number
// can be increased if the need arises.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BdEventCascade.ccv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd     Date:  18-MAY-95    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "BdEventCascade.hh"
#include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdEventCascade()  [Default Constructor]
//
//@ Interface-Description
// Constructs the BdEventCascade.  Needs no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply initializes the 'targets_[]' array.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdEventCascade::BdEventCascade(void) :
						numTargets_(0)
{
	CALL_TRACE("BdEventCascade::BdEventCascade(void)");

	// Initialize array
	for (Uint16 i = 0; i < BEC_MAX_TARGETS_; i++)
	{
		targets_[i] = NULL;
	}
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdEventCascade  [Destructor]
//
//@ Interface-Description
// Destroys the BdEventCascade.
//---------------------------------------------------------------------
//@ Implementation-Description
// Needs to do nothing
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdEventCascade::~BdEventCascade(void)
{
	CALL_TRACE("BdEventCascade::~BdEventCascade(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addTarget [public]
//
//@ Interface-Description
// Adds a BD event target to this cascade.  Passed 'pTarget', a
// pointer to the target object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Adds another target pointer to the 'targets_[]' array.
//---------------------------------------------------------------------
//@ PreCondition
// The target pointer must be non-null and the 'targets_[]' array must
// not be full.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BdEventCascade::addTarget(BdEventTarget* pTarget)
{
	CALL_TRACE("BdEventCascade::addTarget(BdEventTarget* pTarget)");

	CLASS_PRE_CONDITION(pTarget != NULL);
	CLASS_PRE_CONDITION(numTargets_ < BEC_MAX_TARGETS_);

	// Add target to the next empty element of the array.
	targets_[numTargets_] = pTarget;
	numTargets_++;										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
// Called when the event of which this cascade is a target happens.
// We simply call the corresponding method of all registered targets
// with the same parameters, described below:
// >Von
//	eventId			The enum id of the event which happens.
//	eventStatus		Status of the event which happens
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Walks the list of targets and calls their bdEventHappened()
// methods in turn.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
BdEventCascade::bdEventHappened(EventData::EventId eventId,
								EventData::EventStatus eventStatus,
								EventData::EventPrompt eventPrompt)	
	{
	CALL_TRACE("BdEventCascade::bdEventHappened(eventId, eventStatus, eventPrompt)");

	// Walk the 'targets_[]' array and call the eventChangeHappened()
	// method on each target.
	for (Uint16 i = 0; i < numTargets_; i++)
	{													// $[TI1]
		SAFE_CLASS_ASSERTION(NULL != targets_[i]);
		targets_[i]->bdEventHappened(eventId, eventStatus, eventPrompt);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
BdEventCascade::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, BDEVENTCASCADE,
									lineNumber, pFileName, pPredicate);
}
