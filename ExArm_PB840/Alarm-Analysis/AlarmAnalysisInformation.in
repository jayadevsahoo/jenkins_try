#ifndef AlarmAnalysisInformation_IN
#define AlarmAnalysisInformation_IN


//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//=====================================================================
// Class: AlarmAnalysisInformation - Initial creation.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmAnalysisInformation.inv   25.0.4.0   19 Nov 2013 13:51:12   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 003   By:   rhj      Date: 01/16/09        SCR Number:  6452
//      Project:  840S
//      Description:
//         Added setRemedyMessage().
// 
//=====================================================================


//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRemedyMessage
//
//@ Interface-Description
//        Return the remedyMessage enumerator.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
inline MessageName
AlarmAnalysisInformation::getRemedyMessage(void) const 
{
  CALL_TRACE("getRemedyMessage(void)");

  return(remedyMessage_);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getAnalysisMessage
//
//@ Interface-Description
//        Return the analysis message enumerator.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
inline MessageName
AlarmAnalysisInformation::getAnalysisMessage(void) const
{
  CALL_TRACE("getAnalysisMessage(void)");

  return(analysisMessage_);
  // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setRemedyMessage
//
//@ Interface-Description
//   Sets the remedyMessage enumerator based on the given 
//   messageName parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   The input parameter remedyMessageName must be within the 
//   MessageName enumerator list.  
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
inline void
	AlarmAnalysisInformation::setRemedyMessage(MessageName remedyMessageName) 
{
	CALL_TRACE("setRemedyMessage(void)");

	AUX_CLASS_PRE_CONDITION((MESSAGE_NAME_NULL <= remedyMessageName) &&
							(remedyMessageName < MAX_NUM_ALARM_MESSAGES),
							remedyMessageName);

	remedyMessage_  = remedyMessageName;
}


#endif // AlarmAnalysisInformation_IN 

