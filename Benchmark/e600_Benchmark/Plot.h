#pragma once

class Plot
{
	enum RectExtMethod
	{
		ExtLeft,
		ExtRight,
		ExtUp,
		ExtDown,
		ExtCenter
	};
protected:
	int m_width;			// plot width in pixels
	int m_height;			// plot height in pixels
	int m_timeLen;			// in seconds
	int m_dataPerSec;		//
	int m_maxDataCount;		// 
	HWND m_hWnd;

	int m_topMargin;
	int m_rightMargin;
	int m_dataDelta;
	int m_markLength;
	int m_waveWidth;
	int m_waveHeight;

	CRect m_waveRect;

	double m_maxData;
	double m_baseData;
	double m_minData;
	double m_pixelPerData;
	// double m_slicePixels;

	CPoint m_Origin;
	CString m_strBuf;
	bool m_bWiperReset;

	// when the data was reset
	DWORD m_dwWiperResetTime;

	// draw the gradient background
	void drawBackground_(CDC* pMemDC);

	// draw the axis
	void drawCoordinate_(CDC* pMemDC);

	// draw the plot
	void drawWaveForm_(CRect& rcUpdate);

	// the function is used to get a rectangle from a point
	void getRectByPt_(const CPoint& ptBase, int width, int height, RectExtMethod extMethod, CRect& rectRtn);
	void getRectByPt_(int x, int y, int width, int height, RectExtMethod extMethod, CRect& rectRtn);
	int  getYPos_(int data);
	int  getXPos_(int index);
	void resetWiper_();

	deque<int> m_data;
	deque<int> m_oldData;

	CDC m_dcMem;
	CDC m_dcBkgnd;
	CBitmap m_bmpBkgnd;
	CBitmap m_bmpMem;
	
	CBitmap* m_pOldMemBmp;
	CBitmap* m_pOldBkgndBmp;
	CDC* m_pWndDC;
	bool m_bBkgndInvalid;
	int m_drawnCount;

public:
	Plot(HWND hWnd, int width, int height, int timeLen, int dataPerSec);
	~Plot(void);
	
	
	void getWaveRect(CRect& rectWaveForm);
	int  getMaxDataCount();
	void setY(double min, double base, double max);
	void draw(int x, int y, const deque<int>& data, const deque<int>& oldData);
	void pushData(int data);

};
