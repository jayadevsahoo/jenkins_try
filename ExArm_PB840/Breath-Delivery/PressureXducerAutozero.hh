
#ifndef PressureXducerAutozero_HH
#define PressureXducerAutozero_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PressureXducerAutozero - Performs the pressure transducer
//			autozero maneuver.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureXducerAutozero.hhv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: erm     Date:  22-Nov-2002    DR Number: 6028
//  Project:  EMI
//  Description: 
//                defined new private members to determine autozero limit.
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005 By: syw    Date: 10-Sep-1997   DR Number: DCS 2421
//  	Project:  Sigma (R8027)
//		Description:
//			Defined Side enum and autozeroSide_ data memeber and getAutozeroSide().
//
//  Revision: 004 By: syw    Date: 17-Apr-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Changed MAX_ZERO_READING, MIN_ZERO_READING.
//
//  Revision: 003 By: syw    Date: 27-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Made interface change to isAutozeroInRange_() and
//			isAutozeroWithinDriftRange_() to handle	SafetyNetTests
//			design of reporting autozero failures independent of side
//			(insp or exh).  Previous design had the failures coupled. 
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#if defined (SIGMA_BD_CPU)

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "TimerTarget.hh"

class IntervalTimer ;

//@ End-Usage

#endif // defined (SIGMA_BD_CPU)

// $[04294]
//@ Constant: MAX_ZERO_READING
// maximum allowable zero reading
// TODO E600 The value needs to be revisited when we have P3.0 units
static const Int32 MAX_ZERO_READING = 2200 ;

//@ Constant: MIN_ZERO_READING
// minimum allowable zero reading
// TODO E600 The value needs to be revisited when we have P3.0 units
static const Int32 MIN_ZERO_READING = 1500 ;

#if defined (SIGMA_BD_CPU)

//@ Constant: ALLOWABLE_ZERO_SHIFT
// maximum allowable zero reading shift from last reading
static const Int32 MFG_ALLOWABLE_ZERO_SHIFT = 12 ;	// .02917 volts
static const Int32 ALLOWABLE_ZERO_SHIFT = 57 ;	// .139 volts  this is rounded of 4.8cmH20
                                                // counts are unsigned short 4.8 x 12 = 57.6                
//@ Constant: AZ_ALPHA
static const Real32 AZ_ALPHA = 0.5F;

//@ Constant: PRIMARY_AZ_VALUE
static const AdcCounts PRIMARY_AZ_VALUE = 1604;    //3.916 volts


class PressureXducerAutozero : public TimerTarget
{
  public:

	enum Side { INSP = 0, EXH };
        
    PressureXducerAutozero( IntervalTimer& rTimer) ;
    virtual ~PressureXducerAutozero( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

	inline Boolean getIsAutozeroInProgress( void) ;
	inline Side getAutozeroSide( void) const ;
    virtual void timeUpHappened( const IntervalTimer& timer) ;
    void newBreath( void) ;
    void newCycle( void) ;
    void initialize( void) ;

  protected:

  private:
    PressureXducerAutozero( void) ;								// not implemented...
    PressureXducerAutozero( const PressureXducerAutozero&) ;	// not implemented...
    void   operator=( const PressureXducerAutozero&);			// not implemented...

	inline Boolean isAutozeroInRange_( const Uint32 autozeroValue) ;
	inline Boolean isAutozeroWithinDriftRange_( const Uint32 autozeroValue, const Uint32 prevValue) ;
	inline AdcCounts alphaFilter_( const Uint32 autozeroValue, const Uint32 prevZeroValue );											
	
	//@ Data-Member:& rPressureXducerAutozeroTimer_
	// a reference to the timer server
	IntervalTimer& rPressureXducerAutozeroTimer_ ;

	//@ Data-Member: elapsedTimeMs_
	// time elapsed since autozero maneuver began
	Int32 elapsedTimeMs_ ;

	//@ Data-Member: rawInspPressureSum_
	// sum of insp pressure sensor readings during autozero period
	Int32 rawInspPressureSum_ ;

	//@ Data-Member: rawExhPressureSum_
	// sum of exh pressure sensor readings during autozero period
	Int32 rawExhPressureSum_ ;

	//@ Data-Member: timeSincePostMs_
	// time since post in msec
	Int32 timeSincePostMs_ ;

	//@ Data-Member: autozeroIntervalMs_
	// time interval until next autozero
	Int32 autozeroIntervalMs_ ;

	//@ Data-Member: isTimeToAutozero_
	// set if autozero interval time has expired
	Boolean isTimeToAutozero_ ;

	//@ Data-Member: isAutozeroInProgress_
	// set if autozero is in progress
	Boolean isAutozeroInProgress_ ;

	//@ Data-Member: autozeroSide_
	// indicated which side to autozero
	Side autozeroSide_ ;
	
	//@ Data-Member:numInspAZCylesDriftOOR
	// number INSP side of AZ cycles out of range
	Uint16 numInspAZCyclesDriftOOR_;
	
	//@ Data-Member:numExhAZCylesDriftOOR
	// number EXH side of AZ cycles out of range
	Uint16 numExhAZCyclesDriftOOR_;
		
	//@ Data-Member:numInspAZCylesRangeOOR
	// number INSP side of AZ cycles out of range
	Uint16 numInspAZCyclesRangeOOR_;
	
	//@ Data-Member:numeExhAZCylesRangeOOR
	// number EXH side of AZ cycles out of range
	Uint16 numExhAZCyclesRangeOOR_;

    //@ Data-Member: allowableZeroShift_
    // shift limit  dependant of DataKey
	Int32 allowableZeroShift_;

	//@ Data-Member: maxAZCyclesOOR_
	//max seq. faults allowed
	Uint16 maxAZCyclesOOR_;
} ;


// Inlined methods...
#include "PressureXducerAutozero.in"

#endif // defined (SIGMA_BD_CPU)

#endif // PressureXducerAutozero_HH 

