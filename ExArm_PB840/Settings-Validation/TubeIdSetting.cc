#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class: TubeIdSetting - Tube I.D. (Interior Diameter) Setting
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that is used to adjust the interior
//  diameter (I.D.) of the tube used on the current patient.  This class
//  inherits from 'BatchBoundedSetting' and provides the specific information
//  needed for representation of tube I.D..  This information includes the
//  interval and range of this setting's values, this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the contraints of
//  this setting.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings, and never needs to update its
//  bound values, therefore 'calcDependentValues_()' not is overridden,
//  while 'updateConstraints_()' is overridden.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TubeIdSetting.ccv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
//
//  Revision: 006  By: sah     Date:  02-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added support for new 'VS' spontaneous type
//
//  Revision: 005   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added support for new 'PA' spontaneous type
//      *  added 'getAbsoluteMinValue_()' for new PA-based limit
//
//  Revision: 004   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 003   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//      *  modified IBW soft bound table due to IBW's resolution changes
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  soft-limit framework was re-designed such that settings with
//         soft limits need only define new 'findSoftMinMaxValues_()'
//         method, therefore all other "soft" methods were removed
//
//  Revision: 002   By: sah   Date:  06-Jan-1999    DR Number:  5314
//  Project:  ATC
//  Description:
//     Added new 'getApplicability()' method.
//
//  Revision: 001   By: sah   Date:  06-Jan-1999    DR Number:  5322
//  Project:  ATC
//  Description:
//	New ATC-specific setting.
//
//=====================================================================

#include "TubeIdSetting.hh"
#include "SupportTypeValue.hh"

//@ Usage-Classes
#include "IdealBodyWeightSetting.hh"
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

struct RangeInfo_
{
	Real32  min;
	Real32  max;
};

struct IbwBoundInfo_
{
	// for this IBW range...
	RangeInfo_  ibwRange;
	// ...the following tube I.D. soft bounds apply...
	RangeInfo_  tubeIdRange;
};


// $[TC02031] -- soft bound limits...
const IbwBoundInfo_  IBW_SOFT_BOUND_TABLE_[] =
{
//          minIbw,        maxIbw,         minTubeId,      maxTubeId
	{ {         7.0f,        10.0f}, {        4.5f,       4.5f}},
	{ {        11.0f,        13.0f}, {        4.5f,       5.0f}},
	{ {        14.0f,        16.0f}, {        4.5f,       5.5f}},
	{ {        17.0f,        18.0f}, {        4.5f,       6.0f}},
	{ {        19.0f,        22.0f}, {        5.0f,       6.0f}},
	{ {        23.0f,        24.0f}, {        5.0f,       6.5f}},
	{ {        25.0f,        27.0f}, {        5.5f,       6.5f}},
	{ {        28.0f,        31.0f}, {        5.5f,       7.0f}},
	{ {        32.0f,        35.0f}, {        6.0f,       7.0f}},
	{ {        36.0f,        36.0f}, {        6.0f,       7.5f}},
	{ {        37.0f,        42.0f}, {        6.5f,       7.5f}},
	{ {        43.0f,        49.0f}, {        6.5f,       8.0f}},
	{ {        50.0f,        50.0f}, {        7.0f,       8.0f}},
	{ {        55.0f,        55.0f}, {        7.0f,       8.5f}},
	{ {        60.0f,        60.0f}, {        7.0f,       9.0f}},
	{ {        65.0f,        65.0f}, {        7.5f,       9.0f}},
	{ {        70.0f,        70.0f}, {        7.5f,       9.5f}},
	{ {        75.0f,        75.0f}, {        8.0f,       9.5f}},
	{ {        80.0f,       100.0f}, {        8.0f,      10.0f}},
	{ {       110.0f,       130.0f}, {        8.5f,      10.0f}},
	{ {       140.0f,       150.0f}, {        9.0f,      10.0f}},
	{ {         0.0f,         0.0f}, {        0.0f,       0.0f}}
};


//  $[TC02030] -- The setting's range...
//  $[TC02033] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	4.5f,		// absolute minimum...
	0.0f,		// unused...
	TENTHS,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	10.0f,		// absolute maximum...
	0.5f,		// resolution...
	TENTHS,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: TubeIdSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval range for the setting.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TubeIdSetting::TubeIdSetting(void)
: BatchBoundedSetting(SettingId::TUBE_ID,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  TUBE_ID_MAX_ID,	 // maxConstraintId...
					  TUBE_ID_MIN_ID)	 // minConstraintId...
{
	CALL_TRACE("TubeIdSetting()");

	// register this setting's soft bounds...
	registerSoftBound_(::TUBE_ID_SOFT_MIN_ID, Setting::LOWER);
	registerSoftBound_(::TUBE_ID_SOFT_MAX_ID, Setting::UPPER);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~TubeIdSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TubeIdSetting::~TubeIdSetting(void)
{
	CALL_TRACE("~TubeIdSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01081] -- breath settings applicability
//  $[01117] -- "more" settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
TubeIdSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id  applicability;

	if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::ATC)  ||
		 SoftwareOptions::IsOptionEnabled(SoftwareOptions::PAV) )
	{  // $[TI1] -- TC and/or PA is an active option...
		const Setting*  pSupportType =
		SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

		if ( pSupportType->getApplicability(qualifierId) !=
			 Applicability::INAPPLICABLE )
		{  // $[TI1.1] -- support type is applicable...
			DiscreteValue  supportTypeValue;

			switch ( qualifierId )
			{
				case Notification::ACCEPTED :	  // $[TI1.1.1]
					supportTypeValue = pSupportType->getAcceptedValue();
					break;
				case Notification::ADJUSTED :	  // $[TI1.1.2]
					supportTypeValue = pSupportType->getAdjustedValue();
					break;
				default :
					AUX_CLASS_ASSERTION_FAILURE(qualifierId);
					break;
			}

			switch ( supportTypeValue )
			{
				case SupportTypeValue::PSV_SUPPORT_TYPE :
				case SupportTypeValue::VSV_SUPPORT_TYPE :
				case SupportTypeValue::OFF_SUPPORT_TYPE : // $[TI1.1.4]
					applicability = Applicability::INAPPLICABLE;
					break;
				case SupportTypeValue::ATC_SUPPORT_TYPE :
				case SupportTypeValue::PAV_SUPPORT_TYPE : // $[TI1.1.3]
					applicability = Applicability::CHANGEABLE;
					break;
				default :
					// unexpected 'supportTypeValue'...
					AUX_CLASS_ASSERTION_FAILURE(supportTypeValue);
					break;
			};
		}
		else
		{  // $[TI1.2] -- support type is NOT applicable...
			// tube id can't be applicable if support type isn't...
			applicability = Applicability::INAPPLICABLE;
		}
	}
	else
	{  // $[TI2] -- TC and PA are not active options...
		applicability = Applicability::INAPPLICABLE;
	}

	return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TC02032] The setting's new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
TubeIdSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	Real32        dummyMinValue;
	BoundedValue  newPatient;

	// the new-patient value is the maximum allowable "soft" value for the
	// current IBW...
	findSoftMinMaxValues_(dummyMinValue, newPatient.value);

	newPatient.precision = TENTHS;

	return(newPatient);	  // $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TubeIdSetting::SoftFault(const SoftFaultID  softFaultID,
						 const Uint32       lineNumber,
						 const char*        pFileName,
						 const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							TUBE_ID_SETTING, lineNumber, pFileName,
							pPredicate);
}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TBD] -- transitioning to PAV
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TubeIdSetting::acceptTransition(const SettingId::SettingIdType settingId,
								const DiscreteValue toValue,
								const DiscreteValue fromValue)
{
	AUX_CLASS_PRE_CONDITION(SettingId::SUPPORT_TYPE == settingId, settingId);
	AUX_CLASS_PRE_CONDITION(SupportTypeValue::PAV_SUPPORT_TYPE == toValue, toValue);

	static const Real32 MIN_TUBE_ID_FOR_PAV_ = 6.0f;

	// $[LC02017] \b\ tube I.D. shall transition to its new patient value...
	if ( BoundedValue(getAdjustedValue()) < MIN_TUBE_ID_FOR_PAV_ )
	{
		// release prior constraints
		resetConstraints();

		// reactivate the soft bounds since they may have been deactivated 
		// during the previous updateAllSoftBoundStates_ when the new adjusted
		// value is on a hard-bound value equal to the soft-bound value
		activateSoftBound_(::TUBE_ID_SOFT_MAX_ID);
		activateSoftBound_(::TUBE_ID_SOFT_MIN_ID);

		// update the bounds based on the adjusted IBW
		updateConstraints_();

		setAdjustedValue(getNewPatientValue());

		// update this setting's soft-bound states...
		updateAllSoftBoundStates_(Setting::NON_CHANGING);

		setForcedChangeFlag();
	}
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TubeIdSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	Real32  softMinValue, softMaxValue; 

	// find the "soft" minimum and maximum tube id values, for the current IBW...
	findSoftMinMaxValues_(softMinValue, softMaxValue);

	//-------------------------------------------------------------------
	// determine tube id's maximum constraint...
	//-------------------------------------------------------------------
	{
		const Real32  ABSOLUTE_MAX_VALUE = getAbsoluteMaxValue_();

		BoundedRange::ConstraintInfo  maxConstraintInfo;

		if ( isSoftBoundActive_(TUBE_ID_SOFT_MAX_ID)  &&
			 softMaxValue < ABSOLUTE_MAX_VALUE )
		{	// $[TI1] -- 'TUBE_ID_SOFT_MAX_ID' soft bound is currently active, AND
			//           the "soft" max is more restrictive than absolute max...
			// initialize to soft bound maximum bound id and value...
			maxConstraintInfo.id     = TUBE_ID_SOFT_MAX_ID;
			maxConstraintInfo.value  = softMaxValue;
			maxConstraintInfo.isSoft = TRUE;
		}
		else
		{	// $[TI2] -- either 'TUBE_ID_SOFT_MAX_ID' soft bound is NOT active, OR
			//           the absolute max is more restrictive than the "soft" max...
			// initialize to absolute maximum bound id and value...
			maxConstraintInfo.id     = TUBE_ID_MAX_ID;
			maxConstraintInfo.value  = ABSOLUTE_MAX_VALUE;
			maxConstraintInfo.isSoft = FALSE;
		}

		// update tube ID's maximum constraint info...
		getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
	}


	//-------------------------------------------------------------------
	// determine tube id's minimum constraint...
	//-------------------------------------------------------------------
	{
		const Setting*  pSupportType =
		SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

		const DiscreteValue  SUPPORT_TYPE_VALUE = pSupportType->getAdjustedValue();

		BoundedRange::ConstraintInfo  minConstraintInfo;

		// start out setting the 'minConstraintInfo' without regard to the soft
		// limit...
		minConstraintInfo.id     = TUBE_ID_MIN_ID;
		minConstraintInfo.isSoft = FALSE;
		minConstraintInfo.value  = getAbsoluteMinValue_();

		if ( isSoftBoundActive_(TUBE_ID_SOFT_MIN_ID)  &&
			 softMinValue > minConstraintInfo.value )
		{	// $[TI3] -- 'TUBE_ID_SOFT_MIN_ID' soft bound is currently active, AND
			//           the "soft" min is more restrictive than absolute min...
			// initialize to soft bound minimum bound id and value...
			minConstraintInfo.id     = TUBE_ID_SOFT_MIN_ID;
			minConstraintInfo.value  = softMinValue;
			minConstraintInfo.isSoft = TRUE;
		}	// $[TI4] -- either 'TUBE_ID_SOFT_MIN_ID' soft bound is NOT active, OR
		//           the absolute min is more restrictive than the "soft" min...

		// update tube ID's minimum constraint info...
		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Based on the current ("adjusted") IBW value, determine the minimum
//  and maximum "soft" Tube ID constraints.  Return these values via the
//  references 'rSoftMinValue' and 'rSoftMaxValue', respectively.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TubeIdSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
									 Real32& rSoftMaxValue) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	for ( const ::IbwBoundInfo_* pBoundInfo = ::IBW_SOFT_BOUND_TABLE_;
		pBoundInfo->ibwRange.min != 0.0f; pBoundInfo++ )
	{  // $[TI1] -- ALWAYS takes this path...
		if ( IBW_VALUE <= pBoundInfo->ibwRange.max )
		{  // $[TI1.1] -- found the IBW range...
			rSoftMinValue = pBoundInfo->tubeIdRange.min;
			rSoftMaxValue = pBoundInfo->tubeIdRange.max;
			break;
		}  // $[TI1.2] -- keep looking for the matching IBW range...
	}
}


//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Use this to return the absolute minimum value that is currently available
//  from this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
TubeIdSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const Setting*  pSpontType =
	SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

	const DiscreteValue  SPONT_TYPE_VALUE = pSpontType->getAdjustedValue();

	switch ( SPONT_TYPE_VALUE )
	{
		case SupportTypeValue::OFF_SUPPORT_TYPE :
		case SupportTypeValue::PSV_SUPPORT_TYPE :
		case SupportTypeValue::ATC_SUPPORT_TYPE :
		case SupportTypeValue::VSV_SUPPORT_TYPE :	  // $[TI1]
			absoluteMinValue = ::LAST_NODE_.value;	// absolute minimum...
			break;
		case SupportTypeValue::PAV_SUPPORT_TYPE :	  // $[TI2]
			absoluteMinValue = 6.0f;
			break;
		default :
			// unexpected patient spont type value...
			AUX_CLASS_ASSERTION_FAILURE(SPONT_TYPE_VALUE);
			break;
	}

	return(absoluteMinValue);
}
