
#ifndef ApneaPeakInspFlowSetting_HH
#define ApneaPeakInspFlowSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ApneaPeakInspFlowSetting - Apnea Peak Inspiratory Flow Setting
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaPeakInspFlowSetting.hhv   25.0.4.0   19 Nov 2013 14:27:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//	*  added new 'getAbsolute{Min,Max}Value_()' methods to accomodate
//         new, dynamic limits
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
//@ End-Usage


class ApneaPeakInspFlowSetting : public BatchBoundedSetting 
{
  public:
    ApneaPeakInspFlowSetting(void); 
    virtual ~ApneaPeakInspFlowSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
                                   const DiscreteValue            newValue,
                                   const DiscreteValue            currValue);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif  // defined(SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual void  updateConstraints_(void);

    virtual BoundedValue  calcBasedOnSetting_(
				const SettingId::SettingIdType dependentId,
				const BoundedRange::WarpDir    warpDirection,
				const BasedOnCategory          basedOnCategory
					     ) const;

    virtual Real32  getAbsoluteMinValue_(void) const;
    virtual Real32  getAbsoluteMaxValue_(void) const;

  private:
    // not implemented...
    ApneaPeakInspFlowSetting(const ApneaPeakInspFlowSetting&);
    void  operator=(const ApneaPeakInspFlowSetting&);
};


#endif // ApneaPeakInspFlowSetting_HH 
