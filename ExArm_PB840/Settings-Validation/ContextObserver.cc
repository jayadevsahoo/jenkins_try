#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N ================
//@ Class:  ContextObserver - ContextObserver Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a base class for all classes that wish to monitor the state
//  of the Accepted, or Adjusted Contexts.  The public interface includes
//  three virtual methods that a derived class may override:
//  'batchSettingUpdate()' is to be overridden to respond to batch setting
//  updates; 'nonBatchSettingUpdate()' is to be overridden to respond to
//  non-batch setting updates; and 'doRetainAttachment()' is to be overridden
//  to return a 'TRUE' (instead of the default 'FALSE') for those observers
//  that wish to retain their subject attachments across transitions to and
//  from SST.
//
//  The derived classes of this class also get access to the following
//  protected methods:  'getSubjectPtr_()' which can provide access to a
//  particular subject; and 'attachToSubject_()' and 'detachFromSubject_()'
//  for managing the monitoring of subjects.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class keeps an internal pointer array of the various subjects
//  that can be monitored.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ContextObserver.ccv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah   Date:  26-Jan-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//     Created to work with new applicability functionality.
//
//===================================================================

#include "ContextObserver.hh"

//@ Usage-Classes
#include "ContextSubject.hh"
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "AdjustedContext.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Public Methods...
//
//====================================================================

//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method: ~ContextObserver()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this ContextObserver.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ContextObserver::~ContextObserver(void)
{
  CALL_TRACE("~ContextObserver()");
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  batchSettingUpdate(qualifierId, pSubject, settingId)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived
//  classes that wish respond to changes to any of the batch values of this
//  observer's monitored subject.  The default response is to ignore the
//  changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextObserver::batchSettingUpdate(const Notification::ChangeQualifier,
				    const ContextSubject*,
				    const SettingId::SettingIdType)
{
  CALL_TRACE("batchSettingUpdate(qualifierId, pSubject, settingId)");

  // the default behavior is to do nothing...
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  nonBatchSettingUpdate(qualifierId, pSubject, settingId)  [virtual]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived
//  classes that wish respond to changes to any of the non-batch values
//  of this observer's monitored subject.  The default response is to
//  ignore the changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextObserver::nonBatchSettingUpdate(const Notification::ChangeQualifier,
				       const ContextSubject*,
				       const SettingId::SettingIdType)
{
  CALL_TRACE("nonBatchSettingUpdate(qualifierId, pSubject, settingId)");

  // the default behavior is to do nothing...
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  This is a virtual method that is to be overridden by all derived
//  observer classes that wish to retain their attachment to the subject
//  indicated by 'pSubject'.  With task-level state changes, all subjects
//  are informed to detach from their observers.  This method allows for
//  those observers who will continue monitoring the subject across the
//  state changes, to stay attached.
//
//  This method returns the default response of 'FALSE' -- meaning don't
//  preserve the attachment.  Those observer classes that do want to
//  preserve the attachment, need to override this method and return 'TRUE'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
ContextObserver::doRetainAttachment(const ContextSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(FALSE);
}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextObserver::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, CONTEXT_OBSERVER,
                          lineNumber, pFileName, pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ContextObserver()  [Constructor]
//
//@ Interface-Description
//  Create a default setting subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ContextObserver::ContextObserver(void)
{
  CALL_TRACE("ContextObserver()");

  arrSubjectPtrs_[ContextId::ACCEPTED_CONTEXT_ID] =
					  &(ContextMgr::GetAcceptedContext());
  arrSubjectPtrs_[ContextId::ADJUSTED_CONTEXT_ID] =
					  &(ContextMgr::GetAdjustedContext());
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  attachToSubject_(subjectId, changeId)  [const]
//
//@ Interface-Description
//  Attach this observer to the subject identified by 'subjectId', for
//  monitoring the type of change indicated by 'changeId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (ContextId::IsId(subjectId))
//  (changeId < ContextSubject::NUM_CONTEXT_CHANGE_TYPES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextObserver::attachToSubject_(
			      const ContextId::ContextIdType      subjectId,
			      const Notification::ContextChangeId changeId
			         )
{
  CALL_TRACE("attachToSubject_(subjectId, changeId)");
  AUX_CLASS_PRE_CONDITION((ContextId::IsId(subjectId)), subjectId);
  AUX_CLASS_PRE_CONDITION((changeId < Notification::NUM_CONTEXT_CHANGE_TYPES),
			  ((subjectId << 16) | changeId));

  (arrSubjectPtrs_[subjectId])->attachObserver(this, changeId);
}   // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  detachFromSubject_(subjectId, changeId)  [const]
//
//@ Interface-Description
//  Detach this observer from monitoring the type of change indicated by
//  'changeId', of its subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (ContextId::IsId(subjectId))
//  (changeId < ContextSubject::NUM_CONTEXT_CHANGE_TYPES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextObserver::detachFromSubject_(
			      const ContextId::ContextIdType      subjectId,
			      const Notification::ContextChangeId changeId
				   ) const
{
  CALL_TRACE("detachFromSubject_(subjectId, changeId)");
  AUX_CLASS_PRE_CONDITION((ContextId::IsId(subjectId)), subjectId);
  AUX_CLASS_PRE_CONDITION((changeId < Notification::NUM_CONTEXT_CHANGE_TYPES),
			  ((subjectId << 16) | changeId));

  (arrSubjectPtrs_[subjectId])->detachObserver(this, changeId);
}   // $[TI1]
