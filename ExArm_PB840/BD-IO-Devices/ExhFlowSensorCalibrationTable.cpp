#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhFlowSensorCalibrationTable_t - Implements calibration for
//			exhalation flow sensor.
//---------------------------------------------------------------------
//@ Interface-Description
//      
//---------------------------------------------------------------------
//@ Rationale
//      
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      None
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//*********************************************************************
#include "ExhFlowSensorCalibrationTable.h"
#include "GlobalObjects.h"
#include "ServosGlobal.h"
#include "InspFlowCalibrationTable.h"
#include "ExhFlowSensor.h"
#include "MainSensorRefs.hh"

ExhFlowSensorCalibrationTable_t::ExhFlowSensorCalibrationTable_t(CalibrationID_t CalID, AnalogOutputs_t OutputAir, AnalogOutputs_t OutputO2) : 
	CalibrationTable_t(CalID)
{
	AnalogOutputAir = OutputAir;
	AnalogOutputO2 = OutputO2;
}

CalibrationStatus_t	ExhFlowSensorCalibrationTable_t::Calibrate(Flow_t FilteredFlow, InspFlowType_t FlowType)
{
	if(CalibrationStatus != CAL_PENDING)
	{
		CalibrationStatus = CAL_PENDING;
		CalibrationDone = false;
		AutozeroCount = 0;
		ExhCalState = FEXCAL_FLUSH_E;
		ControlPSOL(AnalogOutputAir, 0);
		ControlPSOL(AnalogOutputO2, 0);
		ControlPSOL(DAC_EXHALATION_VALVE, 0);

		if(FlowType == FLOW_TYPE_AIR)
			AnalogOutput = AnalogOutputAir;
		else
			AnalogOutput = AnalogOutputO2;
	}

	DoCalibration(FilteredFlow, FlowType);

	return CalibrationStatus;
}

static DWORD EXH_SENSOR_MAX_AUTOZERO_RETRY			= 1;
static DWORD EXH_SENSOR_AUTOZERO_WAIT_COUNT			= EXH_VALVE_CLOSE_TIME;
static DWORD EXH_SENSOR_ZERO_READ_WAIT_COUNT		= 200;
static DWORD EXH_SENSOR_READ_WAIT_COUNT				= 100;
static DWORD EXH_SENSOR_READ_DELAY_COUNT			= 1500;
static DWORD EXH_SENSOR_FLUSH_WAIT_COUNT			= 2000;

static DWORD EXH_SENSOR_TAB_LOW_SIZE				= 100;	// 0 - 10LPM
static DWORD EXH_SENSOR_TAB_MID_SIZE				= 20;	// 10 - 50LPM
static DWORD EXH_SENSOR_TAB_HIGH_SIZE				= 31;	// 50 - 200LPM
static DWORD EXH_SENSOR_TAB_LOWMID_SIZE				= EXH_SENSOR_TAB_LOW_SIZE + EXH_SENSOR_TAB_MID_SIZE;
static DWORD EXH_SENSOR_CAL_TABLE_SIZE				= EXH_SENSOR_TAB_LOWMID_SIZE + EXH_SENSOR_TAB_HIGH_SIZE;
static DWORD EXH_SENSOR_TAB_INDEX_MID_FLOW			= EXH_SENSOR_TAB_LOW_SIZE;	// starting index for mid flow
static DWORD EXH_SENSOR_TAB_INDEX_HIGH_FLOW			= EXH_SENSOR_TAB_LOW_SIZE + EXH_SENSOR_TAB_MID_SIZE;	// starting index for high flow

static Flow_t EXH_SENSOR_LOW_FLOW_INCREMENT			= 0.1;
static Flow_t EXH_SENSOR_MID_FLOW_INCREMENT			= 2.0;
static Flow_t EXH_SENSOR_HIGH_FLOW_INCREMENT		= 5.0;

static Flow_t EXH_SENSOR_MAX_LOW_FLOW				= (EXH_SENSOR_TAB_LOW_SIZE * EXH_SENSOR_LOW_FLOW_INCREMENT);
static Flow_t EXH_SENSOR_MAX_MID_FLOW				= (EXH_SENSOR_TAB_MID_SIZE * EXH_SENSOR_MID_FLOW_INCREMENT) + EXH_SENSOR_MAX_LOW_FLOW;

static uint16_t NUM_SAMPLES_FOR_AVG_CAL				= 30;
static uint16_t NUM_SAMPLES_FOR_FLOW_REACH			= 5;

static uint16_t NUM_DAC_INCREMENT_LOW_FLOW			= 1;
static uint16_t NUM_DAC_INCREMENT_MID_FLOW			= 5;
static uint16_t NUM_DAC_INCREMENT_HIGH_FLOW			= 10;

static uint16_t NUM_EXH_SENSOR_CAL_RANGES			= 3;
static Flow_t	EXH_SENSOR_MAX_FLUSH_FLOW			= 100.0;
static uint16_t NUM_DAC_INCREMENT_FLUSH_FLOW		= 5;

//*****************************************************************************
//  DetermineTargetFlow
//
//*****************************************************************************
Flow_t DetermineTargetFlow(uint16_t CalIndex)
{
	Flow_t TargetFlow;

	if (CalIndex < EXH_SENSOR_TAB_INDEX_MID_FLOW)
		TargetFlow = CalIndex * EXH_SENSOR_LOW_FLOW_INCREMENT;
	else if (CalIndex < EXH_SENSOR_TAB_INDEX_HIGH_FLOW)
		TargetFlow = ((CalIndex - EXH_SENSOR_TAB_LOW_SIZE) * EXH_SENSOR_MID_FLOW_INCREMENT) + EXH_SENSOR_MAX_LOW_FLOW;
	else
		TargetFlow = ((CalIndex - EXH_SENSOR_TAB_LOWMID_SIZE) * EXH_SENSOR_HIGH_FLOW_INCREMENT) + EXH_SENSOR_MAX_MID_FLOW;

	return TargetFlow;
}

//*****************************************************************************
//  DetermineDacIncrement
//
//*****************************************************************************
uint16_t DetermineDacIncrement(uint16_t CalIndex)
{
	uint16_t DacIncrement;

	if (CalIndex < EXH_SENSOR_TAB_INDEX_MID_FLOW)
		DacIncrement = NUM_DAC_INCREMENT_LOW_FLOW;
	else if (CalIndex < EXH_SENSOR_TAB_INDEX_HIGH_FLOW)
		DacIncrement = NUM_DAC_INCREMENT_MID_FLOW;
	else
		DacIncrement = NUM_DAC_INCREMENT_HIGH_FLOW;

	return DacIncrement;
}


//*****************************************************************************
//  ExhFlowSensorCalibrationTable_t::DoCalibration
//
//*****************************************************************************
bool ExhFlowSensorCalibrationTable_t::DoCalibration(Flow_t FilteredFlow, InspFlowType_t FlowType)
{
	uint16_t DacData;
	Flow_t FlowData;

	static Flow_t FlowSum = 0;		// for computing average of FilteredFlow
	static uint32_t ADCSum = 0;		// for computing average of Exh. ADC
	static DWORD CountToWait;
	static ExhCalState_t ExhCalStateAfterWait;
	Flow_t FlowAverage;
	uint16_t ADCAverage;

	static DWORD PrevTickCount;
	static uint16_t CalPos;
	DWORD ElapsedTickCount, CurrentTickCount;
	static uint16_t DacCount;
	static uint16_t SampleCount;

	uint16_t StatusData;
	uint16_t RawFilteredData;
	ExhFlowSensorStatus_t SensorStatus;
	float *pCalData = pTable;
	Flow_t TargetFlow = 0;

	UNUSED_SYMBOL(FlowType);

	if(RExhFlowSensor.IsError())
	{
		if(!RExhFlowSensor.IsAutozeroInProgress())
		{
			if(AutozeroCount++ < EXH_SENSOR_MAX_AUTOZERO_RETRY)
			{
				RExhFlowSensor.Autozero();
			}
			else
			{
				CalibrationStatus = CAL_FAILED;
				CalibrationDone = true;
				RETAILMSG(1, (L"Exh Flow Sensor BAD, Status = 0x%x\r\n", RExhFlowSensor.GetSensorStatus()));
			}
		}
		return CalibrationDone;
	}

	StatusData = RExhFlowSensor.GetStatusData();
	if((StatusData & 0xF8) == 0x80)			/* check every single data point */
	{
		RawFilteredData = RExhFlowSensor.GetRawFilteredData();
	}
	else
	{
		return CalibrationDone;
	}

    CurrentTickCount = GetTickCount();
	ElapsedTickCount = CurrentTickCount - PrevTickCount;

	switch(ExhCalState)
    {
	case FEXCAL_FLUSH_E:
		// TODO E600_LL: to review flush flow in this case statement later
		DacCount += NUM_DAC_INCREMENT_FLUSH_FLOW;
		if((DacCount > MAX_DAC_VALUE) || (FilteredFlow > EXH_SENSOR_MAX_FLUSH_FLOW))
		{
			WCHAR FilteredFlowStr[10];
			wsprintf(FilteredFlowStr, L"%3.2f", FilteredFlow);

			// if low or no flow, abort the calibration
			if(FilteredFlow < (EXH_SENSOR_MAX_FLUSH_FLOW / 2))
			{
				CalibrationDone = true;
				ExhCalState = FEXCAL_FAILED_E;

				RETAILMSG(1, (L"Exh Flow Sensor Calibration Failed, Flush Flow = %s\r\n", FilteredFlowStr));
			}
			else
			{
				PrevTickCount = CurrentTickCount;
				ExhCalState = FEXCAL_WAIT_E;
				ExhCalStateAfterWait = FEXCAL_AZ_INIT_E;
				CountToWait = EXH_SENSOR_FLUSH_WAIT_COUNT;
				ControlPSOL(AnalogOutput, 0);
				RETAILMSG(1, (L"Exh Flow Sensor Cal: FlushFlow=%s, Dac=%d\r\n", FilteredFlowStr, DacCount));
				DacCount = 0;
			}
		}
		else
		{
			// TODO E600_LL: to change way of updating psol throughout file
			ControlPSOL(AnalogOutput, DacCount);
		}
		break;
    case FEXCAL_AZ_INIT_E:
		PrevTickCount = CurrentTickCount;
		ExhCalState = FEXCAL_AZ_WAIT_E;
		AutozeroCount = 0;
		SampleCount = 0;
		break;
	case FEXCAL_AZ_WAIT_E:						/* wait 200ms for flow to settle */
		if(ElapsedTickCount > EXH_SENSOR_AUTOZERO_WAIT_COUNT)
		{
			PrevTickCount = CurrentTickCount;
			AutozeroCount = 0;
			RExhFlowSensor.Autozero();		// start an autozero
			ExhCalState = FEXCAL_AUTOZERO_E;
		}
		break;
    case FEXCAL_AUTOZERO_E:
		if(!RExhFlowSensor.IsAutozeroInProgress())
		{
			// autozero done, check status
			SensorStatus = RExhFlowSensor.GetSensorStatus();
			if(SensorStatus == STATUS_ZERO_OUT)
			{
				if(++AutozeroCount >= EXH_SENSOR_MAX_AUTOZERO_RETRY)
				{
					ExhCalState = FEXCAL_FAILED_E;		// bail out, "Exh Flow Sensor Bad"
					RETAILMSG(1, (L"Exh Flow Sensor Failing Autozero\r\n"));
				}
				else
				{
					RExhFlowSensor.Autozero();				// retry
					PrevTickCount = CurrentTickCount;		// reset timer
				}
			}
			else
			{
				ExhCalState = FEXCAL_INIT_E;
				FexAdcTab[0] = RExhFlowSensor.GetAzRawData();			// Use AZ ADC for 0 flow
				RETAILMSG(1, (L"FEXCAL_AUTOZERO_E: ZeroFlow ADC = %d\r\n", FexAdcTab[0]));
				FexFlowTab[0] = 0.0;								//  Lowest flow
			}
		}
		else if(ElapsedTickCount > EXH_SENSOR_AUTOZERO_WAIT_COUNT + 1000)		// fail if not response after this time
		{
			ExhCalState = FEXCAL_FAILED_E;				// bail out, "Exh Flow Sensor Bad"
			RETAILMSG(1, (L"Exh Flow Sensor NOT Responding\r\n"));
		}
		break;
    case FEXCAL_INIT_E:
		PrevTickCount = CurrentTickCount;
		CalPos = 0;
		ExhCalState = FEXCAL_ZERO_E;
		break;
    case FEXCAL_ZERO_E:
		if(ElapsedTickCount > EXH_SENSOR_ZERO_READ_WAIT_COUNT)
		{
			CalPos++;
			ExhCalState = FEXCAL_SERVO_E;
			// TODO E600_LL: to review min. DAC value used to start 
			DacCount = pAirValve->GetMinFlowDac();
			if(DacCount > 100)
				DacCount -= 100;
			else
				DacCount = 0;
		}
		break;
	case FEXCAL_SERVO_E:
		DacCount += DetermineDacIncrement(CalPos);
		if(DacCount > MAX_DAC_VALUE)
		{
			uint16_t slope1, slope2, slopeAvg = 0;

			// compute slope for extrapolation
			if(CalPos < (EXH_SENSOR_CAL_TABLE_SIZE - 1))
			{
				slope1 = (FexAdcTab[CalPos - 3] - FexAdcTab[CalPos - 13]) / 10;
				slope2 = (FexAdcTab[CalPos - 4] - FexAdcTab[CalPos - 14]) / 10;
				slopeAvg = (slope1 + slope2) / 2;
			}

			while(CalPos < EXH_FLOW_SUBTABLE_SIZE)								// fill the rest of table with same value
			{
				FexAdcTab[CalPos] = FexAdcTab[CalPos - 1] + slopeAvg;	// do extrapolation here
				FexFlowTab[CalPos++] = (Flow_t)(CalPos - 1);
			}
#ifdef DEBUG
			ExhCalState = FEXCAL_PRINT_E;
#else
			ExhCalState = FEXCAL_SAVE_E;
#endif
		}
		else
		{
	        // TODO E600_LL: to change way of updating psol throughout file
			//AioDioDriver.Write(AnalogOutput, DacCount);
			ControlPSOL(AnalogOutput, DacCount);

			PrevTickCount = CurrentTickCount;
	        CountToWait = EXH_SENSOR_READ_WAIT_COUNT;
			ExhCalStateAfterWait = FEXCAL_CHECK_E;
			ExhCalState = FEXCAL_WAIT_E;
		}
		break;
	case FEXCAL_CHECK_E:
		TargetFlow = DetermineTargetFlow(CalPos);
		if(FilteredFlow >= TargetFlow)
		{
			// must reach the target flow for a number of sample to be valid
			if(++SampleCount >= NUM_SAMPLES_FOR_FLOW_REACH)
			{
				SampleCount = 0;
				FlowSum = 0;
				ADCSum = 0;
				PrevTickCount = CurrentTickCount;
				CountToWait = EXH_SENSOR_READ_DELAY_COUNT;
				ExhCalStateAfterWait = FEXCAL_FLOW_AVG_E;
				ExhCalState = FEXCAL_WAIT_E;
			}
		}
		else
		{
			SampleCount = 0;
			ExhCalState = FEXCAL_SERVO_E;
		}
		break;
	case FEXCAL_FLOW_AVG_E:
		SampleCount++;
		FlowSum += FilteredFlow;		// accumulate flow
		ADCSum += RawFilteredData;		// accumulate ADC value from exh. sensor

		// flow must reach target for a number of samples before considered valid
		if(SampleCount >= NUM_SAMPLES_FOR_AVG_CAL)
		{
			FlowAverage = FlowSum / SampleCount;
			ADCAverage = ADCSum / SampleCount;

			if(ADCAverage < FexAdcTab[CalPos-1])	/* no lower than previous data */
			{
				DacData = FexAdcTab[CalPos-1];
				FlowData = FexFlowTab[CalPos-1];
			}
			else
			{
				DacData = ADCAverage;
				FlowData = FlowAverage;
			}
#ifdef DEBUG
			WCHAR StrFlow[10], StrTargetFlow[10];
			wsprintf(StrTargetFlow, L"%3.2f", DetermineTargetFlow(CalPos));
			wsprintf(StrFlow, L"%3.2f", FlowAverage);
			RETAILMSG(1, (L"FEXCAL: Dac=%d, TargetFlow=%s, FilFlow=%s, Adc=%d\r\n",DacCount,StrTargetFlow,StrFlow,ADCAverage));
#endif
			FexFlowTab[CalPos] = FlowData;
			FexAdcTab[CalPos++] = DacData;

			if(CalPos >= EXH_SENSOR_CAL_TABLE_SIZE)
			{
#ifdef DEBUG
			ExhCalState = FEXCAL_PRINT_E;
#else
			ExhCalState = FEXCAL_SAVE_E;
#endif
			}
			else	// go to the next flow cal
			{
				SampleCount = 0;
				PrevTickCount = CurrentTickCount;
				CountToWait = EXH_SENSOR_READ_WAIT_COUNT;
				ExhCalStateAfterWait = FEXCAL_SERVO_E;
				ExhCalState = FEXCAL_WAIT_E;
			}
		}
		break;
    case FEXCAL_WAIT_E:
        if(ElapsedTickCount > CountToWait)
		{
            ExhCalState = ExhCalStateAfterWait;
		}
        break;
	case FEXCAL_PRINT_E:
		WCHAR Str[10];

		RETAILMSG(1, (L"\r\nFlow_t DefaultFexFlowTab[EXH_FLOW_SUBTABLE_SIZE] =\r\n{ "));
		for(int i = 0; i < EXH_FLOW_SUBTABLE_SIZE; i++)
		{
			if((i != 0) && ((i % 10) == 0))
				RETAILMSG(1, (L"\r\n"));

			wsprintf(Str, L"%3.2f", FexFlowTab[i]);
			if(i < EXH_FLOW_SUBTABLE_SIZE - 1)
				RETAILMSG(1, (L"%s, ", Str));
			else
				RETAILMSG(1, (L"%s };\r\n", Str));
		}

		RETAILMSG(1, (L"\r\nuint16_t DefaultFexDacTab[EXH_FLOW_SUBTABLE_SIZE] =\r\n{ "));
		for(int i=0; i < EXH_FLOW_SUBTABLE_SIZE; i++)
		{
			if((i != 0) && ((i % 10) == 0))
				RETAILMSG(1, (L"\r\n"));

			if(i < EXH_FLOW_SUBTABLE_SIZE - 1)
				RETAILMSG(1, (L"%d, ", FexAdcTab[i]));
			else
				RETAILMSG(1, (L"%d };\r\n", FexAdcTab[i]));
		}

		ExhCalState = FEXCAL_SAVE_E;
		break;
	case FEXCAL_SAVE_E:
		for(int i = 0; i < EXH_FLOW_SUBTABLE_SIZE; i++)
		{
			*pCalData++ = FexFlowTab[i];
		}
		for(int i = 0; i < EXH_FLOW_SUBTABLE_SIZE; i++)
		{
			*pCalData++ = (float)FexAdcTab[i];
		}

        CalibrationStatus = CAL_PASSED;

        ExhCalState = FEXCAL_FLUSH_E;
		CalibrationDone = true;
		break;
	case FEXCAL_FAILED_E:
		ExhCalState = FEXCAL_FLUSH_E;
		CalibrationStatus = CAL_FAILED;
		CalibrationDone = true;
	default:
        break;
	}

	if(CalibrationDone)
	{
		DacCount = 0;
		ControlPSOL(AnalogOutput, DacCount);
	}
	return CalibrationDone;
}
