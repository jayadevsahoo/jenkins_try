#include "stdafx.h"
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ApneaVentilationSubScreen - An Upper screen subscreen
// which appears automatically when the patient enters apnea.  It
// displays the current apnea vent settings.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the SubScreen class. 
// It contains text fields, lines, boxes, discrete, timing and numeric
// setting buttons.
//
// The layout of the display mimics the SafetyVentilation subscreen on the upper 
// screen.  Thus, there is a "mode indicator" bar across the top of the
// display for showing the mode, mandatory type.  Also, there is a set of fake
// main setting buttons for the following Apnea PCV settings:
//>Von
//	1.  Inspiratory pressure.
//	2.  Inspiratory time
//>Voff
// The activate() method takes care of the details of initializing the
// numeric values displayed in these fake main setting buttons. 
//---------------------------------------------------------------------
//@ Rationale
// This class is dedicated to managing the display of the
// ApneaVentilation subscreen on the UpperSubScreenArea.
//---------------------------------------------------------------------
//@ Implementation-Description
// To support its display, this area displays textual fields, line            
// objects, box objects, and flattened numeric setting buttons.  The          
// display is "static" and does not change 
//---------------------------------------------------------------------
//@ Fault-Handling
//	none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//@(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ApneaVentilationSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 007  By:  gdc	   Date:  03-Jun-2007    SCR Number:  6237
//  Project:  Trend
//  Description:
//      Added positioning gravity for flow pattern as part of automated 
//      text positioning change.
//
//  Revision: 06   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 005   By: sah    Date: 05-May-2000     DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  swapped placement of Ti and Pi setting buttons
//      *  re-designed interface to discrete setting value strings, whereby
//         the point-size and style are inserted at run-time
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  24-JUL-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      02-JUL-97  Changed SUBTITLE_X_ from 96 to 6 due to changes in
//		MiscStrs::SAFETY_SUBSCREEN_SUBTITLE per translation.
//
//  Revision: 001  By:	hhd	Data:	01-MAY-95	DR Number:
//	Project:  Sigma (R8027)
//	Description:  Integration baseline.
//
//=====================================================================
//

#include "ApneaVentilationSubScreen.hh"

//
// GUI-App includes.
//
#include "UpperSubScreenArea.hh"
#include "TextFont.hh"
#include "MiscStrs.hh"
#include "FlowPatternValue.hh"
#include "MandTypeValue.hh"
#include "TriggerTypeValue.hh"
#include "ContextSubject.hh"
#include "GuiApp.hh"


//@ Usage-Classes
//@ End-Usage

//@ Code...
static const Int32 BUTTON_WIDTH_ = 98;
static const Int32 BUTTON_HEIGHT_ = 58;
static const Int32 STATUS_AREA_X1_ = 21;
static const Int32 STATUS_AREA_Y1_ = 82;
static const Int32 STATUS_AREA_WIDTH_ = (BUTTON_WIDTH_-1)*6 + 1;
static const Int32 STATUS_AREA_HEIGHT_ = 25;
static const Int32 BUTTON_AREA_X1_ = STATUS_AREA_X1_;
static const Int32 BUTTON_AREA_Y1_ = STATUS_AREA_Y1_ + STATUS_AREA_HEIGHT_ - 1;
static const Int32 LEFT_WING_X1_ = 140;
static const Int32 LEFT_WING_X2_ = 187;
static const Int32 RIGHT_WING_X1_ = 248;
static const Int32 RIGHT_WING_X2_ = 295;
static const Int32 WING_Y_ = 93;
static const Int32 WING_PEN_ = 2;
static const Int32 WINGTIP_WIDTH_ = 8;
static const Int32 WINGTIP_HEIGHT_ = 7;
static const Int32 TITLE_Y_ = 10;
static const Int32 SUBTITLE_Y_ = 52;
static const Int32 FOOTNOTE1_X_ = 18;
static const Int32 FOOTNOTE2_X_ = 18;
static const Int32 FOOTNOTE1_Y_ = 234;
static const Int32 FOOTNOTE2_Y_ = 250;
static const Int32 MODE_INDICATOR_X1_ = 21;
static const Int32 MODE_INDICATOR_Y1_ = 82;
static const Int32 MODE_INDICATOR_WIDTH_ = (BUTTON_WIDTH_ - 1) * 6 + 1;
static const Int32 MODE_INDICATOR_HEIGHT_ = 25;
static const Int32 NUMERIC_VALUE_CENTER_X_ = 36;
static const Int32 NUMERIC_VALUE_CENTER_Y_ = 39;

static Uint  FlowPatternInfoMemory_[(sizeof(DiscreteSettingButton::ValueInfo) +
			 (sizeof(StringId) * (FlowPatternValue::TOTAL_FLOW_PATTERNS - 1)))];

static DiscreteSettingButton::ValueInfo*  PFlowPatternInfo_ =
				 (DiscreteSettingButton::ValueInfo*)::FlowPatternInfoMemory_;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ApneaVentilationSubScreen()  [Constructor]
//
//@ Interface-Description
//  pSubScreenArea - ptr to the parent SubScreenArea which contains
//  this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Initializes all members and initializes the colors, sizes, and
// 	positions of all graphical objects and setting buttons.  Adds all
// 	graphical objects and setting buttons to the parent container for
// 	display.  The setting buttons are flattened so that they cannot be
// 	selected by the user.  Note the setting buttons are constructed
// 	without the normal setting id's, message id's, or subscreen
// 	associations.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ApneaVentilationSubScreen::ApneaVentilationSubScreen(SubScreenArea* 
													pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleText_(MiscStrs::APNEA_VENT_SUBSCREEN_TITLE),
	subTitleText_(MiscStrs::SAFETY_SUBSCREEN_SUBTITLE),
	footnoteText1_(MiscStrs::APNEA_VENT_SUBSCREEN_FOOTNOTE1),
	footnoteText2_(MiscStrs::APNEA_VENT_SUBSCREEN_FOOTNOTE2),
	modeText_(MiscStrs::MODE_APNEA_VENT_TITLE),
	leftWingtip_(LEFT_WING_X1_ - WINGTIP_WIDTH_, WING_Y_ + WINGTIP_HEIGHT_,
				LEFT_WING_X1_, WING_Y_, WING_PEN_),
	leftWing_(LEFT_WING_X1_, WING_Y_, LEFT_WING_X2_, WING_Y_, WING_PEN_),
	rightWing_(RIGHT_WING_X1_, WING_Y_, RIGHT_WING_X2_, WING_Y_, WING_PEN_),
	rightWingtip_(RIGHT_WING_X2_, WING_Y_, 
				RIGHT_WING_X2_ + WINGTIP_WIDTH_, WING_Y_ + WINGTIP_HEIGHT_, 
				WING_PEN_),
    modeBackground_(MODE_INDICATOR_X1_, MODE_INDICATOR_Y1_,
				MODE_INDICATOR_WIDTH_, MODE_INDICATOR_HEIGHT_),
	buttonBackground_(BUTTON_AREA_X1_, BUTTON_AREA_Y1_, STATUS_AREA_WIDTH_, 
				BUTTON_HEIGHT_*2 - 1),
	flowPatternButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*2, 
				BUTTON_AREA_Y1_ + (BUTTON_HEIGHT_-1),
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE, 
				Button::NO_BORDER, NULL_STRING_ID,
				SettingId::APNEA_FLOW_PATTERN, NULL, NULL, 24, TRUE),
	inspiratoryPressureButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1), 
				BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR, 
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::INSPIRATORYPRESSURE_BUTTON_TITLE_LARGE,
				GuiApp::GetPressureUnitsLarge(),
				SettingId::APNEA_INSP_PRESS, NULL, NULL, TRUE),
	inspiratoryTimeButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*2, 
				BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR, 
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::INSPIRATORY_TIME_BUTTON_TITLE_LARGE,
				MiscStrs::SEC_UNITS_LARGE,
				SettingId::APNEA_INSP_TIME, NULL, NULL, TRUE),
	oxygenPercentageButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*5, 
				BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::OXYGEN_PERCENTAGE_BUTTON_TITLE_LARGE,
				MiscStrs::PERCENT_UNITS_LARGE,
				SettingId::APNEA_O2_PERCENT, NULL, NULL, TRUE),
	peakFlowButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*2, BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::PEAK_FLOW_BUTTON_TITLE_LARGE,
				MiscStrs::L_PER_MIN_UNITS_LARGE,
				SettingId::APNEA_PEAK_INSP_FLOW, NULL, NULL, TRUE),
	respiratoryRateButton_(BUTTON_AREA_X1_, BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::RESPIRATORY_RATE_BUTTON_TITLE_LARGE,
				MiscStrs::ONE_PER_MIN_UNITS_LARGE,
				SettingId::APNEA_RESP_RATE, NULL, NULL, TRUE),
	tidalVolumeButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1), BUTTON_AREA_Y1_, 
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR, 
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::TIDAL_VOLUME_BUTTON_TITLE_LARGE,
				MiscStrs::ML_UNITS_LARGE,
				SettingId::APNEA_TIDAL_VOLUME, NULL, NULL, TRUE),
	peepButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*5,
				BUTTON_AREA_Y1_ + (BUTTON_HEIGHT_-1),
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::PEEP_BUTTON_TITLE_LARGE,
				GuiApp::GetPressureUnitsLarge(),
				SettingId::PEEP, NULL, NULL, TRUE),
	peepLowButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*5,
				BUTTON_AREA_Y1_ + (BUTTON_HEIGHT_-1),
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::PEEP_BUTTON_TITLE_LARGE,  // must use "PEEP" label...
				GuiApp::GetPressureUnitsLarge(),
				SettingId::PEEP_LOW, NULL, NULL, TRUE),
	flowSensitivityButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*4,
				BUTTON_AREA_Y1_,
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::FLOW_SENSITIVITY_BUTTON_TITLE_LARGE,
				MiscStrs::L_PER_MIN_UNITS_LARGE,
				SettingId::FLOW_SENS, NULL, NULL, TRUE),
	pressureSensitivityButton_(BUTTON_AREA_X1_ + (BUTTON_WIDTH_-1)*4,
				BUTTON_AREA_Y1_,
				BUTTON_WIDTH_, BUTTON_HEIGHT_, Button::DARK, 1, Button::SQUARE,
				Button::NO_BORDER, TextFont::TWENTY_FOUR,
				NUMERIC_VALUE_CENTER_X_, NUMERIC_VALUE_CENTER_Y_,
				MiscStrs::PRESSURE_SENSITIVITY_BUTTON_TITLE_LARGE,
				GuiApp::GetPressureUnitsLarge(),
				SettingId::PRESS_SENS, NULL, NULL, TRUE)
{
	CALL_TRACE("ApneaVentilationSubScreen(pSubScreenArea)");
	SAFE_CLASS_PRE_CONDITION(pSubScreenArea != NULL);

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setColor(Colors::LIGHT_BLUE);
	setFillColor(Colors::LIGHT_BLUE);

	//
	// Set colors, then add fixed backgrounds and fixed status area text.
	//
	buttonBackground_.setColor(Colors::DARK_BLUE);
	buttonBackground_.setFillColor(Colors::DARK_BLUE);
	addDrawable(&buttonBackground_);

    modeBackground_.setColor(Colors::BLACK);
    modeBackground_.setFillColor(Colors::BLACK);
    addDrawable(&modeBackground_);

	modeText_.setX(MODE_INDICATOR_X1_);
    modeText_.setY(MODE_INDICATOR_Y1_);
    modeText_.setColor(Colors::WHITE);
    addDrawable(&modeText_);

	titleText_.setY(TITLE_Y_);
	titleText_.setColor(Colors::BLACK);
	addDrawable(&titleText_);
	titleText_.positionInContainer(GRAVITY_CENTER);

	subTitleText_.setY(SUBTITLE_Y_);
	subTitleText_.setColor(Colors::BLACK);
	addDrawable(&subTitleText_);
	subTitleText_.positionInContainer(GRAVITY_CENTER);

	footnoteText1_.setX(FOOTNOTE1_X_);
	footnoteText1_.setY(FOOTNOTE1_Y_);
	footnoteText1_.setColor(Colors::BLACK);
	addDrawable(&footnoteText1_);

	footnoteText2_.setX(FOOTNOTE2_X_);
	footnoteText2_.setY(FOOTNOTE2_Y_);
	footnoteText2_.setColor(Colors::BLACK);
	addDrawable(&footnoteText2_);

	leftWingtip_.setColor(Colors::WHITE);
	addDrawable(&leftWingtip_);
	leftWing_.setColor(Colors::WHITE);
	addDrawable(&leftWing_);

	rightWing_.setColor(Colors::WHITE);
	addDrawable(&rightWing_);
	rightWingtip_.setColor(Colors::WHITE);
	addDrawable(&rightWingtip_);

	mandatoryTypeText_.setX(MODE_INDICATOR_X1_);
	mandatoryTypeText_.setY(MODE_INDICATOR_Y1_);
	mandatoryTypeText_.setColor(Colors::WHITE);
	addDrawable(&mandatoryTypeText_);

	triggerTypeText_.setX(MODE_INDICATOR_X1_+ (BUTTON_WIDTH_-1)*4);
	triggerTypeText_.setY(MODE_INDICATOR_Y1_);
	triggerTypeText_.setColor(Colors::WHITE);
	addDrawable(&triggerTypeText_);

	// set up values for apnea flow pattern....
	::PFlowPatternInfo_->numValues = FlowPatternValue::TOTAL_FLOW_PATTERNS;
	::PFlowPatternInfo_->arrValues[FlowPatternValue::SQUARE_FLOW_PATTERN] =
									MiscStrs::FLOW_PATTERN_SQUARE_VALUE_LARGE;
	::PFlowPatternInfo_->arrValues[FlowPatternValue::RAMP_FLOW_PATTERN] =
									MiscStrs::FLOW_PATTERN_RAMP_VALUE_LARGE;
	flowPatternButton_.setValueInfo(::PFlowPatternInfo_);
	flowPatternButton_.setValueTextPosition(::GRAVITY_TOP);

	//-----------------------------------------------------------------
	// initialize array of setting button pointers...
	//-----------------------------------------------------------------

	// add buttons to array...
	Uint  idx = 0u;
	arrSettingBtnPtrs_[idx++] = &flowPatternButton_;
	arrSettingBtnPtrs_[idx++] = &inspiratoryPressureButton_;
	arrSettingBtnPtrs_[idx++] = &inspiratoryTimeButton_;
	arrSettingBtnPtrs_[idx++] = &oxygenPercentageButton_;
	arrSettingBtnPtrs_[idx++] = &peakFlowButton_;
	arrSettingBtnPtrs_[idx++] = &respiratoryRateButton_;
	arrSettingBtnPtrs_[idx++] = &tidalVolumeButton_;
	arrSettingBtnPtrs_[idx++] = &peepButton_;
	arrSettingBtnPtrs_[idx++] = &peepLowButton_;
	arrSettingBtnPtrs_[idx++] = &flowSensitivityButton_;
	arrSettingBtnPtrs_[idx++] = &pressureSensitivityButton_;
	arrSettingBtnPtrs_[idx]   = NULL;
	AUX_CLASS_ASSERTION((idx <= MAX_SETTING_BUTTONS_), idx);

	//-----------------------------------------------------------------
	// add "setup" setting buttons to container...
	//-----------------------------------------------------------------

	for (idx = 0u; arrSettingBtnPtrs_[idx] != NULL; idx++)
	{
		// all of these buttons attach to this subscreen directly...
		addDrawable(arrSettingBtnPtrs_[idx]);
	}

	// this subscreen needs to ALWAYS be attached to the accepted context
	// subject...
	attachToSubject_(ContextId::ACCEPTED_CONTEXT_ID,
					 Notification::BATCH_CHANGED);
					 								// $[TI1]

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ApneaVentilationSubScreen  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	n/a 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ApneaVentilationSubScreen::~ApneaVentilationSubScreen(void)
{
	CALL_TRACE("ApneaVentilationSubScreen::~ApneaVentilationSubScreen(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate()
//
//@ Interface-Description
//	Prepares this subscreen for display. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Concrete implementation of a pure virtual function from the base class.
//  Handles any necessary initialization when this subscreen is about
//  to be displayed.
// $[01347] The apnea in progress subscreen shall display the currently ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaVentilationSubScreen::activate(void)
{
	CALL_TRACE("ApneaVentilationSubScreen::activate(void)");

	const ContextSubject *const  P_ACCEPTED_SUBJECT =
							getSubjectPtr_(ContextId::ACCEPTED_CONTEXT_ID);

	// activate, and flatten, all setting buttons...
	operateOnButtons_(arrSettingBtnPtrs_, &SettingButton::activate);
	operateOnButtons_(arrSettingBtnPtrs_, &SettingButton::setToFlat);

    // get the current apnea mandatory type value...
    const DiscreteValue  APNEA_MAND_TYPE_VALUE =
			P_ACCEPTED_SUBJECT->getSettingValue(SettingId::APNEA_MAND_TYPE);

	//
	// Then, show and activate the mode-sensitive buttons.
	//
	switch (APNEA_MAND_TYPE_VALUE)
	{
	case MandTypeValue::PCV_MAND_TYPE:					// $[TI1]
		mandatoryTypeText_.setText(MiscStrs::MAND_TYPE_APNEA_VENT_PCV_TITLE);
		break;
	case MandTypeValue::VCV_MAND_TYPE:					// $[TI2]
		mandatoryTypeText_.setText(MiscStrs::MAND_TYPE_APNEA_VENT_VCV_TITLE);
		break;
	default:
		// unexpected apnea mand type value...
		mandatoryTypeText_.setText(MiscStrs::EMPTY_STRING);
		break;
	}

    // get the current trigger type value...
    const DiscreteValue  TRIGGER_TYPE_VALUE =
				P_ACCEPTED_SUBJECT->getSettingValue(SettingId::TRIGGER_TYPE);

	StringId  titleFormatStr;

	// Do Trigger Type title...
    switch (TRIGGER_TYPE_VALUE)
    {
    case TriggerTypeValue::FLOW_TRIGGER:				// $[TI3]
		titleFormatStr = MiscStrs::TRIGGER_TYPE_FLOW_TITLE;
        break;

    case TriggerTypeValue::PRESSURE_TRIGGER:			// $[TI4]
		titleFormatStr = MiscStrs::TRIGGER_TYPE_PRESSURE_TITLE;
        break;

    default:											// $[TI5]
		// unexpected trigger type value...
		titleFormatStr = L"{p=12,x=20,y=16,%c:*****}";  
        break;
    }

	wchar_t  tmpString[TextField::MAX_STRING_LENGTH+1];

	// set the state of Trigger Type's title field...
	swprintf(tmpString, titleFormatStr, L'N'); // non-italicized...  
	triggerTypeText_.setText(tmpString);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate()
//
//@	Interface-Description
//	Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calling method to propagate the 'deactivate' command on the array
//  of setting button pointers.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaVentilationSubScreen::deactivate(void)
{
	CALL_TRACE("ApneaVentilationSubScreen::deactivate(void)");

	// deactivate all setting buttons...
	operateOnButtons_(arrSettingBtnPtrs_, &SettingButton::deactivate);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: batchSettingUpdate
//
//@ Interface-Description
//	Called normally when a setting has changed.
//	Passed the following parameters:
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.
//  settingId	The setting id which changed.
//---------------------------------------------------------------------
//@ Implementation-Description
// We are only interested in changes to the Accepted Context so we
// ignore all others.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaVentilationSubScreen::batchSettingUpdate(
							 const Notification::ChangeQualifier qualifierId,
							 const ContextSubject*               pSubject,
							 const SettingId::SettingIdType      settingId
											 )
{
	CALL_TRACE("batchSettingUpdate(qualifierId, pSubject, settingId)");

    if (isVisible()  &&  qualifierId == Notification::ACCEPTED)
	{													// $[TI1]
		// Call activate() to update the display of this screen
		activate();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaVentilationSubScreen::SoftFault(const SoftFaultID  softFaultID,
                                     const Uint32       lineNumber,
                                     const char*        pFileName,
                                     const char*        pPredicate)
{
    CALL_TRACE("ApneaVentilationSubScreen::SoftFault(softFaultID, lineNumber,"
							"pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							APNEAVENTILATIONSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
