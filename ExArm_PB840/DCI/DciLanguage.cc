#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// =========================== C L A S S   D E S C R I P T I O N ====
//@ Class: DciLanguage - Sigma DCI language-specific values
//---------------------------------------------------------------------
//@ Interface-Description
// This file encapuslates all DCI output values that are language-dependent.
// The initial implementation is in (American) English.  Other languages can be
// supported using the conditional compilation -Dxxx, where xxx is the
// language added below.
//---------------------------------------------------------------------
//@ Rationale
// Encapsulate language-dependent values.
//---------------------------------------------------------------------
//@ Implementation-Description
// All language-specific data structures for DCI report(s) are defined to
// be global and are initialized here.  The default language is (American)
// English.  Other languages can be supported by setting up the proper
// initializations, being sure to always keep the #else branch as the
// default language.
//---------------------------------------------------------------------
//@ Module-Decomposition
// none
//---------------------------------------------------------------------
//@ Fault-Handling
// Not applicable.
//---------------------------------------------------------------------
//@ Restrictions
// The ASCII character set is the basis for all output and, as such, must be
// "re-interpreted" in order to support non Latin-based character sets such as
// Cyrillic or Kanji.  The problem of alternate character sets is outside
// the purview of this file's scope.
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/DCI/vcssrc/DciLanguage.ccv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log 
// 
//  Revision: 010   By: mnr    Date: 15-Apr-2010    SCR Number: 6436  
//  Project: PROX
//  Description:
//      Removed PLeakEnabledStr6_ and PLeakDisabledStr6_.
// 
//  Revision: 009   By: mnr    Date: 24-Feb-2010    SCR Number: 6436  
//  Project: PROX
//  Description:
//      Modified to support PROX state (ON/OFF/FAULT).
//
//  Revision: 008    By: rhj              Date: 14-Oct-2009  SCR Number: 6544  
//  Project: XB2
//  Description:
//      Modified Leak Compensation enabled values from Enabled/Disabled to
//     ON/OFF.
//
//  Revision: 007    By: rpr              Date: 07-Nov-2008  DR Number: 6435
//  Project: S840
//  Description:
//      Added support for Leak Compensation.
//
//  Revision: 006    By: rhj              Date: 11-Jul-2006  DR Number: 5928
//  Project: RESPM
//  Description:
//      Added support for the new command SNDF.
//
//  Revision: 005    By: hct              Date: 19-OCT-2000  DR Number: 5757
//  Project: Baseline
//  Description:
//      Added functionality to support Marquette monitors.
//
//  Revision: 004    By: hct              Date: 22-FEB-1999  DR Number: 5322
//  Project: ATC
//  Description:
//      Added ATC functionality.
//
//  Revision: 003    By: hct              Date: 03-MAR-1998  DR Number: N/A
//  Project: BiLevel
//  Description:
//      Added Bilevel functionality.
//
//  Revision: 002    By: Gary Cederquist  Date: 17-DEC-1997  DR Number: 2605
//  Project: Sigma (R8027)
//  Description:
//      Restructuring of DCI to fix several problems. 
//
//  Revision: 1.0	By: jdm	Date: 9/5/95	DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "Sigma.hh"
#include "Dci.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

#ifdef	A_NEW_LANGUAGE_GOES_HERE

#else	// American English

		// used by Dci::reportHeader_()
	const	char *	Dci::DciAId_ =		"MISCA";
	const	char *	Dci::DciSNIDId_ =	"IDENT";
	const	char *	Dci::DciSLMTId_ =	"LMT";
	const	char *	Dci::DciSERId_ =	"ER";

    const   char *  Dci::DciFId_ =      "MISCF";

		// used by Dci::getDate_();
		// 3-character month abbreviations
	const	char	Dci::MonthNames_[12][4] =
	{
		"JAN", "FEB", "MAR", "APR", "MAY", "JUN",
		"JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
	};

		// used by Dci::getBatchSettings_()
		// NOTE:  string length is given by digit(s) in variable name
	const	char *	Dci::PBlankStr6_ =	"      ";

	const	char *	Dci::PCmvStr6_ =	"CMV   ";
	const	char *	Dci::PSimvStr6_ =	"SIMV  ";
	const	char *	Dci::PCpapStr6_ =	"CPAP  ";
	const	char *	Dci::PBilevlStr6_ =	"BILEVL";


    // Updated Modes
    const   char *  Dci::PACStr6_ =     "A/C   ";
    const   char *  Dci::PSpontStr6_ =  "SPONT ";

	const	char *	Dci::PSquareStr6_ =	"SQUARE";
	const	char *	Dci::PRampStr6_ =	"RAMP  ";

	const	char *	Dci::PNoopCStr6_ =	"NOOP C";

	const	char *	Dci::PITimeStr6_ =	"I-TIME";
	const	char *	Dci::PIeRatioStr6_ =	"I/E   ";
	const	char *	Dci::PETimeStr6_ =	"E-TIME";

    const   char *  Dci::PNIVStr9_      = "NIV      ";
    const   char *  Dci::PInvasiveStr9_ = "INVASIVE ";

    const   char *  Dci::PPTrigerTypeStr6_ = "P-Trig";
    const   char *  Dci::PVTrigerTypeStr6_ = "V-Trig";

    const   char *  Dci::PPCStr6_          = "PC    ";
    const   char *  Dci::PVCStr6_          = "VC    ";
    const   char *  Dci::PVCPlusStr6_      = "VC+   ";

    const   char *  Dci::PNoneStr6_        = "NONE  ";
    const   char *  Dci::PPSStr6_          = "PS    ";
    const   char *  Dci::PTCStr6_          = "TC    ";
    const   char *  Dci::PVSStr6_          = "VS    ";
    const   char *  Dci::PPAStr6_          = "PA    ";
    const   char *  Dci::PETStr6_          = "ET    ";
    const   char *  Dci::PTRACHStr6_       = "TRACH ";

    const   char *  Dci::PNonHeatedExpTubeStr18_  = "Non-Heated Exp.   ";
    const   char *  Dci::PHeatedExpTubeStr18_     = "Heated Exp.       ";
    const   char *  Dci::PHMEStr18_               = "HME               ";
    const   char *  Dci::PO2EnabledStr9_          = "Enabled  ";
    const   char *  Dci::PO2DisabledStr9_         = "Disabled ";
	const   char *  Dci::PDSensOFFStr6_           = "OFF   ";
	const   char *  Dci::PProxFaultStr6_	      = "FAULT ";

		// used by Dci::encodeOnOff_()
		// NOTE:  string length is given by digit(s) in variable name
	const	char *	Dci::POffStr6_ =	"OFF   ";
	const	char *	Dci::POnStr6_ =		"ON    ";

		// used by Dci::encodeNormalAlarmReset_()
		// NOTE:  string length is given by digit(s) in variable name
	const	char *	Dci::PNormalStr6_=	"NORMAL";
	const	char *	Dci::PAlarmStr6_ =	"ALARM ";
	const	char *	Dci::PResetStr6_ =	"RESET ";

		// used by Dci::generateSER_()
		// NOTE:  string length is given by digit(s) in variable name
	const	char *	Dci::PPassStr6_ =	"PASSED";


		// used by Dci::encodeNormalAlarmResetF_()
		// NOTE:  string length is given by digit(s) in variable name
	const	char *	Dci::PHighUrgencyAlarmStr6_ =	"HIGH  ";
	const	char *	Dci::PMediumUrgencyAlarmStr6_ =	"MEDIUM";
	const	char *	Dci::PLowUrgencyAlarmStr6_ =	"LOW   ";

    const   char *  Dci::PpediatricCircuitStr9_  = "PEDIATRIC";
    const   char *  Dci::PadultCircuitStr9_      = "ADULT    ";
    const   char *  Dci::PneonatalCircuitStr9_   = "NEONATAL ";

    const   char *  Dci::PAlertStr6_             = "ALERT ";

#endif	// default language
