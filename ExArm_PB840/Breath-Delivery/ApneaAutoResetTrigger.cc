#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ApneaAutoResetTrigger - Monitors the system for the specific
//      conditions that cause auto reset of apnea to be detected.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers a reset from apnea ventilation based on the volume 
//    exhaled and the volume inspired for the last two consecutive apnea PIMs.  
//    The trigger is enabled during apnea ventilation.  When the volume 
//    conditions calculated by this trigger are true, the trigger
//    is considered to have "fired".  Note that the trigger does not wait for
//    the breath to terminate before it fires. This trigger is kept on a list 
//    contained in the ApneaScheduler, and is only active during apnea
//    ventilation.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for detecting a patient reset 
//    from apnea, and triggers the transition from apnea ventilation.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether 
//    this trigger is enabled or not.  If the trigger is not enabled, 
//    it will always return a state of false.  The condition monitored by 
//    this trigger is only checked when at least two  breaths have been 
//    delivered.  If the criteria has been met, the trigger fires by 
//    notifying the currently active scheduler of its state.  This trigger 
//    is evaluated every BD cycle when apnea is the active mode.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ApneaAutoResetTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 005  By:  sp    Date: 29-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 004  By:  sp    Date:  25-Apr-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test item.
//
//  Revision: 003  By:  iv    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  iv   Date:  13-Dec-1995    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Added a condition for triggerCondition_ method to check if apnea
//             is possible.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ApneaAutoResetTrigger.hh"

//@ Usage-Classes
#include "BreathPhaseScheduler.hh"

#include "BreathRecord.hh"

#include "BreathSet.hh"

#include "Btps.hh"

#include "BreathMiscRefs.hh"

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ApneaAutoResetTrigger 
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The triggerId is set by the base class constructor.  
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
ApneaAutoResetTrigger::ApneaAutoResetTrigger(void) 
: ModeTrigger(APNEA_AUTORESET)
{
  CALL_TRACE("ApneaAutoResetTrigger(void)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ApneaAutoResetTrigger 
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
ApneaAutoResetTrigger::~ApneaAutoResetTrigger(void)
{
  CALL_TRACE("~ApneaAutoResetTrigger(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
ApneaAutoResetTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, APNEAAUTORESETTRIGGER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
// This method takes no parameters.  If the trigger condition has 
// occured, the method returns true, otherwise, the method returns false.
// The condition for this trigger is met if there have been two
// consecutive apnea PIMs delivered where the exhaled tidal volume is
// greater than half of the delivered tidal volume. If during apnea ventilation,
// apnea becomes impossible because of setting changes - the trigger condition
// is met as well.
//---------------------------------------------------------------------
//@ Implementation-Description
//    $[04268]
//    The first coondition is checked by using a static method to check for
//    apnea possible in the BreathPhaseScheduler class.
//    The second condition is only checked if there exists a previous breath.
//    If  (Vexh > 0.5 * Vinsp)  for the previous and
//    and current apnea PIMs, then this condition returns true.
//    Otherwise return false.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
static const Real32 VOLUME_FACTOR = 0.5F;
Boolean
ApneaAutoResetTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_(void)");


   BreathRecord* pCurrentBreath = RBreathSet.getCurrentBreathRecord();
   BreathRecord* pLastBreath = RBreathSet.getLastBreathRecord();
 
   CLASS_PRE_CONDITION(pCurrentBreath != NULL);

   Boolean retVal = FALSE;

   // if apnea is not possible - exit out of it.
   if ( BreathPhaseScheduler::CheckIfApneaPossible() == FALSE)	
   {
      // $[TI7]     
      retVal = TRUE;
   }	
   else if  (pLastBreath != NULL) 
   {
      // $[TI1]
      if ((pCurrentBreath->getSchedulerId() == SchedulerId::APNEA) &&
          (pLastBreath->getSchedulerId() == SchedulerId::APNEA) &&
          (pCurrentBreath->getBreathType() == ASSIST) &&
          (pLastBreath->getBreathType() == ASSIST) &&
	  (BreathRecord::GetPhaseType() == BreathPhaseType::EXHALATION) &&
          
          // Since exh dry factor is used for volume integration,
          // the volumes compared do not have to be BTPS compensated.
          (pCurrentBreath->getExpiredVolume() >=
              VOLUME_FACTOR *
              	pCurrentBreath->getInspiredVolume()) &&
          // The following volumes are BTPS compensated			
          (pLastBreath->getExpiredVolume() >=
              VOLUME_FACTOR * pLastBreath->getInspiredVolume()))
      {
         // $[TI3]
         retVal = TRUE;
      }
      // $[TI4]
   }
   // $[TI2]

   //FALSE: $[TI5]
   //TRUE: $[TI6]
   return (retVal);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
