/*****************************************************************************/
/*   Module Name:    version.h                                          
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>  
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Revision is been changed from F to G   Rhomere Jimenez   05 Oct,2007
/*   Revision is been changed from E to F   Rhomere Jimenez   18 Oct,2005
/*	 Version is been changed to 3.0 and D 	Hamsavally(CGS)   29 Nov,2000
/* 	 						
/*                                                                           
/*   $Revision::   1.3      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _VERSION
#define _VERSION

//define code version
#define VERSION "4.0  P/N 4-075359-00 G"

//define beta version
//#define BETA "Language DEMO"

//define to build object as nonfunctional demo
//#define DEMOMODE

//define to activate Crystal Reports engine
#define CRPE_ENABLED

#endif // #ifndef _VERSION
