//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file PDSimulationServer.hh
///
/// This file contains class PDSimulationServer definition. 
//------------------------------------------------------------------------------

#ifndef PDSIMULATIONSERVER_H
#define PDSIMULATIONSERVER_H


#if defined( INTEGRATION_TEST_ENABLE )

#include <winsock2.h>

#include "OsTimeStamp.hh"
#include "SigmaTypes.hh"
#include "NetworkApp.hh"

class PDSimulationServer{
    public:
        static void PDSimulationServerTask( void );

    private:

	void ServerLoop();

	Boolean OnRecvData(char *pBuffer, ULONG puiValue);
	void WaitForDevKey();
	void SetupSocket();
	void WaitForMessage();
	void WaitForData();
	void Restart();
	void Reset();
	void Halt();

	Int32 m_iState;
	SOCKET m_Socket;

	char m_Buffer[2000];

        static OsTimeStamp m_LastMsgOsTimeStamp;

        enum {
            SETUP_SOCKET           = 1,
            WAIT_FOR_MESSAGE       = 2,
            HALT                   = 3,
            RESET                  = 4,
            RESTART                = 5,
            WAIT_FOR_DATA          = 6,
            WAIT_FOR_DEVKEY        = 7
        };


        static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);


};

#endif	//INTEGRATION_TEST_ENABLE

#endif	//PDSIMULATIONSERVER_H
