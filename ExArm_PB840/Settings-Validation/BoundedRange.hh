
#ifndef BoundedRange_HH
#define BoundedRange_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BoundedRange - Range of 'BoundedValue' Intervals.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BoundedRange.hhv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: sah   Date:  21-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to support dynamic definition of bounded ranges, so
//         that the new, circuit-based alarm limits can be changed to the
//         'OFF' value
//      *  merged all functionality of '{Single,Double,Triple,...}IntervalRange'
//         classes into here, using a combination of a linked-list of intervals,
//         and generic algorithms to process through this list
//
//  Revision: 005   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  changed 'get{Max,Min}ConstraintValue_()' to public method to
//	   support scaling the Volume Limit alarm bar based on its current
//	   constraint, rather than its absolute constraints.
//
//  Revision: 004   By: dosman Date:  07-May-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	removed dead code: methods setAbsoluteMax() and setAbsoluteMin()
//	after sah's restructuring, these are no longer needed
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: dosman Date:  23-Feb-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	initial BiLevel version.
//	Added setAbsoluteMax() and setAbsoluteMin()
//	  (see Method Description for details).
//
//  Revision: 002   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	It is no longer an implementation requirement that all setting
//	values reside at a resolution value within its interval, therefore
//	a new 'checkWarping' parameter is added to 'isLegalValue()'.
//	Furthermore, a new method ('getValuePrecision()') has been added
//	for filling in the precision of a bounded value.  Also, removed
//	some unit test accessing code, because this can be done within
//	the unit test source code, instead.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "Precision.hh"
#include "SettingClassId.hh"
#include "MathUtilities.hh"
#include "SettingBoundId.hh"
#include "SettingBoundTypes.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "Ostream.hh"
#endif  // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
#include "BoundedInterval.hh"
#include "BoundedValue.hh"
#include "BoundStatus.hh"
//@ End-Usage


class BoundedRange
{
#if defined(SIGMA_DEVELOPMENT)
    // Friend:  operator<<(ostr, range)
    // This needs access to the private members.
    friend Ostream&  operator<<(Ostream& ostr, const BoundedRange& range);
#endif  // defined(SIGMA_DEVELOPMENT)

  public:
    struct ConstraintInfo
    {
      Real32          value;
      SettingBoundId  id;
      Boolean         isSoft;
    };

    enum WarpDir
    {
      WARP_UP,
      WARP_NEAREST,
      WARP_DOWN
    };

    BoundedRange(const BoundedInterval* pIntervalList,
		 const SettingBoundId   maxConstraintId,
		 const SettingBoundId   minConstraintId,
		 const Boolean          useEpsilonFactoring = TRUE);
    ~BoundedRange(void);

    Boolean  isLegalValue(const BoundedValue& boundedValue) const;

    inline Real32  getMaxValue(void) const;
    inline Real32  getMinValue(void) const;

    inline Real32  getMaxConstraintValue(void) const;
    inline Real32  getMinConstraintValue(void) const;

    void  testValue(BoundedValue& rProposedValue,
		    BoundStatus&  rBoundStatus) const;
    void  increment(Uint          numClicks,
		    BoundedValue& rCurrValue,
		    BoundStatus&  rBoundStatus) const;
    void  decrement(Uint          numClicks,
		    BoundedValue& rCurrValue,
		    BoundStatus&  rBoundStatus) const;

    void  warpValue(BoundedValue&               rCurrValue,
		    const BoundedRange::WarpDir warpDirection =
					     BoundedRange::WARP_NEAREST,
		    const Boolean               clipToRange = TRUE) const;

    inline void  resetMaxConstraint(void);
    inline void  resetMinConstraint(void);

    inline void  updateMaxConstraint(const ConstraintInfo& maxConstraintInfo);
    inline void  updateMinConstraint(const ConstraintInfo& minConstraintInfo);

    inline void  getValuePrecision(BoundedValue& rBoundedValue,
				   const Boolean isIncreasing = TRUE) const;

    void  updateIntervalList(const BoundedInterval* pIntervalList);

    inline const BoundedInterval*  getIntervalList(void) const;

    static inline Boolean  IsLegalInterval(const Real32    intervalMax,
					   const Real32    intervalRes,
					   const Precision intervalPrec,
					   const Real32    intervalMin);

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  private:
    BoundedRange(const BoundedRange&);		// not implemented...
    BoundedRange(void);				// not implemented...
    void  operator=(const BoundedRange&);	// not implemented...

    inline Boolean  isUpperBoundEnabled_(void) const;
    inline Boolean  isLowerBoundEnabled_(void) const;

    Boolean  isAnIntervalMatch_(const Real32    currValue,
				const Boolean   isIncreasing,
				const Real32    nextIntervalMax,
				const Precision nextIntervalPrec) const;

    void  getIntervalValues_(const Real32  currValue,
			     Real32&       rIntervalMax,
			     Real32&       rIntervalRes,
			     Precision&    rIntervalPrec,
			     Real32&       rIntervalMin,
			     const Boolean isIncreasing) const;

    Uint  getClickNum_(const Real32 currValue,
		       const Real32 intervalRes,
		       const Real32 intervalMin) const;

    //@ Data-Member:  pIntervalList_
    // Pointer to the first interval in the linked list of intervals
    // that define this range.
    const BoundedInterval*  pIntervalList_;

    //@ Data-Member:  pLastNode_
    // Pointer to the last (node) interval in the linked list of intervals
    // that define this range; used to quickly get the absolute minimum.
    const BoundedInterval*  pLastNode_;

    //@ Data-Member:  maxConstraintInfo_
    // This is the information for the maximum constraint.
    ConstraintInfo  maxConstraintInfo_;

    //@ Data-Member:  minConstraintInfo_
    // This is the information for the minimum constraint.
    ConstraintInfo  minConstraintInfo_;

    //@ Constant:  EPSILON_FACTOR_
    // This constant is used for calculating an "epsilon" range around
    // an interval's minimum and maximum, allowing equivalence within
    // the range.  A non-static member is needed to allow a factor of
    // '0.0' (i.e., no epsilon factoring) for those instances that have
    // large resolutions, with respect to their min/max values (e.g.,
    // I:E ratio).
    const Real32  EPSILON_FACTOR_;
};


// Inlined methods...
#include "BoundedRange.in"


#endif  // BoundedRange_HH
