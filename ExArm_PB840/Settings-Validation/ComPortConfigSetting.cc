#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  ComPortConfigSetting - Communications Port Device Selection
//          Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the selected device
//	for a ventilator communication port (printer or DCI).  This
//  class inherits from 'BatchDiscreteSetting' and provides the specific
//  information needed for the representation of the selected device.  This
//  information includes the set of values that this setting can have (see
//  'ComPortConfigValue.hh'), and this batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ComPortConfigSetting.ccv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
// Revision: 004    By: gdc       Date: 09-Aug-2010 SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet serial protocol. Waveforms retricted to
//  COM3 due to serial controller 1 transmit interrupts interfering
//  with serial controller 2 receive interrupts.
// 
// Revision: 003    By: erm       Date: 5-Nov-2008  SCR Number: 6441
//  Project: 840S
//  Description:
//  Added support for RT waveform to serial port
// 
//  Revision: 002   By: hlg    Date:  09-OCT-2001    DR Number: 5966
//  Project: GUIComms
//  Description:
//	Added SRS tracing for print functionality.
//
//  Revision: 001   By: hct    Date:  14-FEB-2000    DR Number: 5493
//  Project: GUIComms
//  Description:
//	Initial version.
//
//=====================================================================

#include "ComPortConfigSetting.hh"
#include "ComPortConfigValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
#include "OsUtil.hh"
//@ End-Usage

//@ Code...
// This array is set up dynamically via this class's 'calcDependentValues_()',
// because all dependents are not applicable in 'SPONT' mode...
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
{
    SettingId::COM1_BAUD_RATE,
    SettingId::COM2_BAUD_RATE,
    SettingId::COM3_BAUD_RATE,

    SettingId::NULL_SETTING_ID
};
//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ComPortConfigSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  $[GC01001] The Communications Setup subscreen shall allow ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ComPortConfigSetting::ComPortConfigSetting(SettingId::SettingIdType settingId)
: BatchDiscreteSetting(settingId,
                       ::ArrDependentSettingIds_,
                       ComPortConfigValue::TOTAL_CONFIG_VALUE),
settingId_(settingId)

{
    CALL_TRACE("ComPortConfigSetting()");
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~ComPortConfigSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ComPortConfigSetting::~ComPortConfigSetting(void)
{
    CALL_TRACE("~ComPortConfigSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return an Applicability
//  ID indicating this setting's Applicability.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Comm port device selection setting is ALWAYS changeable.
//
//  $[GC01001] The Communications Setup subscreen shall allow ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ComPortConfigSetting::getApplicability(const Notification::ChangeQualifier) const
{
    CALL_TRACE("getApplicability(qualifierId)");

    return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ComPortConfigSetting::getNewPatientValue(void) const
{
    CALL_TRACE("getNewPatientValue()");


    const AcceptedContext&  rAcceptedContext = ContextMgr::GetAcceptedContext();

    DiscreteValue  newPatientValue;

    if (rAcceptedContext.areBatchValuesInitialized())
    {   // $[TI1]
        // when the Accepted Context has previosly-initialized values, use the
        // previous value of this setting...
        newPatientValue = rAcceptedContext.getBatchDiscreteValue(settingId_);
		
		// reset to DCI if the previous setup is no longer valid 
		// can occur when VentSet is selected with development data key and
		// then data key changed to a production data key
		if ( !isEnabledValue(newPatientValue) )
		{
			newPatientValue = ComPortConfigValue::DCI_VALUE;
		}
    }
    else
    {   // $[TI2]
        //  $[GC01001] The Communications Setup subscreen shall allow ...
        newPatientValue = ComPortConfigValue::DCI_VALUE;
    }

    return(newPatientValue);
}
//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  isEnabledValue(value)  [virtual]
//
//@ Interface-Description
//  This virtual method is to be overridden by all discrete settings
//  whose values have varying states of applicability.  This version of
//  this method always returns 'TRUE', irregardless of 'value'.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[BL04079]
// [[LC01001] Real-time waveform output shall only 
//            be active on at most one serial port at a time.
//---------------------------------------------------------------------
//@ PreCondition
//  (isDiscreteSetting())
//---------------------------------------------------------------------
//@ PostCondition
//  (TRUE)
//@ End-Method
//=====================================================================

Boolean
ComPortConfigSetting::isEnabledValue(const DiscreteValue value) const
{
    CALL_TRACE("isEnabledValue(value)");
    Boolean isEnabled = TRUE;

    CLASS_PRE_CONDITION( settingId_ == SettingId::COM1_CONFIG ||
                         settingId_ == SettingId::COM2_CONFIG ||
                         settingId_ == SettingId::COM3_CONFIG )

    if ( value == ComPortConfigValue::VSET_VALUE )
    {
        isEnabled = SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions();
#if defined(API_INVESTIGATIONAL_DEVICE)
        isEnabled = TRUE;
#endif
		if (settingId_ == SettingId::COM3_CONFIG)
		{
			isEnabled = FALSE;
		}
    }
    else if ( value == ComPortConfigValue::SENSOR_VALUE &&
              (settingId_ == SettingId::COM1_CONFIG || settingId_ == SettingId::COM2_CONFIG) )
    {
        isEnabled = FALSE;
    }
    else if ( value == ComPortConfigValue::PRINTER_VALUE &&
              (settingId_ == SettingId::COM2_CONFIG || settingId_ == SettingId::COM3_CONFIG) )
    {
        isEnabled = FALSE;
    }

    return(isEnabled);
}  


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02255] -- this setting's dependent settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
ComPortConfigSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
    CALL_TRACE("calcDependentValues_()");
    Uint  idx = 0u;


    // all three timing dependents are active here...
    ArrDependentSettingIds_[idx++] = SettingId::COM1_BAUD_RATE;
    ArrDependentSettingIds_[idx++] = SettingId::COM2_BAUD_RATE;
    ArrDependentSettingIds_[idx++] = SettingId::COM3_BAUD_RATE;

    // terminate with the null id...
    ArrDependentSettingIds_[idx] = SettingId::NULL_SETTING_ID;

    // forward on to base class...
    return(Setting::calcDependentValues_(isValueIncreasing));
}

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ComPortConfigSetting::SoftFault(const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

    FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
                            COM_PORT_CONFIG_SETTING, lineNumber, pFileName,
                            pPredicate);
}
