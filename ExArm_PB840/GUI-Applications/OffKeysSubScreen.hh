#ifndef OffKeysSubScreen_HH
#define OffKeysSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OffKeysSubScreen - The subscreen activated by the offkeys.
// Allows performing alarm silence, O2 calibration, and maneuvers.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/OffKeysSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  mnr    Date:  24-Feb-2010    SCR Number: 6555
//  Project:  NEO
//  Description:
//		Screen Lock panel related updates.
//
//  Revision: 003  By:  rpr    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 002  By:  hhd	   Date:  08-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//		Modified to provide additional off-screen key management, whereby 100% O2
//		and Alarm Silence can be cancelled.
//
//  Revision: 001  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//	(hhd) 	29-Nov-99	Modified to fix bugs.
//====================================================================

#include "SubScreen.hh"
#include "GuiTimerTarget.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "GuiAppClassIds.hh"
#include "TextButton.hh"
#include "OffKeyPanel.hh"
#include "EventData.hh"
#include "AlarmOffKeyPanel.hh"
#include "Line.hh"
//@ End-Usage

class OffKeysSubScreen : public SubScreen, public GuiTimerTarget
{
public:
	OffKeysSubScreen(SubScreenArea *pSubScreenArea);
	~OffKeysSubScreen(void);

	// Overload SubScreen methods
	virtual void activate(void);
	virtual void deactivate(void);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	void updatePanel(EventData::EventId eventId, EventData::EventStatus eventStatus,
					 EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT,
					 Int32 elapseTime=0);

	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


protected:

private:
	// these methods are purposely declared, but not implemented...
	OffKeysSubScreen(void);							// not implemented..
	OffKeysSubScreen(const OffKeysSubScreen&);	// not implemented..
	void operator=(const OffKeysSubScreen&);			// not implemented..

	void registerCallbackPtr_(EventData::EventId eventId);
	void updateDisplay_(Boolean status=FALSE);
	
	//@ Data-Member: isPlus20Enabled_
	// flag used to control code execution of plus 20 O2 feature
	// returns TRUE if option is enabled and neo ciruit is in use
	Boolean isPlus20Enabled_;

	//@ Data-Member: alarmSilencePanel_
	// An area to display locked alarm silence information.
	AlarmOffKeyPanel alarmSilencePanel_;

	//@ Data-Member: hundredPercentO2Panel_
	// An area to display Hundred Percent O2 calibration information.
	AlarmOffKeyPanel hundredPercentO2Panel_;

	//@ Data-Member: maneuverPanel_
	// An area to display maneuver information.
	OffKeyPanel maneuverPanel_;

	//@ Data-Member: horizLine_
	// Horizontal line dividing Alarm Silence Panel and % O2 Panel.
	Line horizLine_;

	//@ Data-Member: vertLine_
	// Vertical line dividing left and right panels
	Line vertLine_;

	//@ Data-Member: alarmSilencePanelStatus_
	// An area to display locked alarm silence information.
	Boolean alarmSilencePanelStatus_;

	//@ Data-Member: hundredPercentO2PanelStatus_
	// An area to display Hundred Percent O2 calibration information.
	Boolean hundredPercentO2PanelStatus_;

	//@ Data-Member: maneuverPanelStatus_
	// An area to display maneuver information.
	Boolean maneuverPanelStatus_;

	//@ Data-Member: maneuverCommand_
	// A message which informs the user what to do.
	wchar_t maneuverCommand_[80];  

	//@ Data-Member: displayCancelButton_
	// A flag to indicate if Cancel button is to be displayed. 
	Boolean displayCancelButton_;

};

#endif // OffKeysSubScreen_HH 
