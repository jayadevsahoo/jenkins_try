// MessageBox.cpp : implementation file
//

#include "stdafx.h"
#include "MessageBox.h"
#include "Swat.h"


// CMessageBox dialog

IMPLEMENT_DYNAMIC(CMessageBox, CDialog)

CMessageBox::CMessageBox(CWnd* pParent /*=NULL*/)
	: CDialog(CMessageBox::IDD, pParent)
	, m_nBoxTypeRepeat(0)
{

}


CMessageBox::~CMessageBox()
{
}

void CMessageBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMessageBox, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_STOP_SCRIPT, &CMessageBox::OnBnClickedButtonStopScript)
	ON_BN_CLICKED(IDOK, &CMessageBox::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_REPEAT, &CMessageBox::OnBnClickedButtonRepeat)
	ON_BN_CLICKED(IDC_BUTTON_CONTINUE, &CMessageBox::OnBnClickedButtonContinue)
END_MESSAGE_MAP()


// CMessageBox message handlers

BOOL CMessageBox::OnInitDialog()
{
	CDialog::OnInitDialog();
	GetDlgItem(IDC_STATIC_MESSAGE)->SetWindowTextA(m_strMessage);

	//GetDlgItem(IDOK)->SetWindowTextA("Cancel");
	if(m_nBoxTypeRepeat == 0)
	{
		GetDlgItem(IDC_BUTTON_REPEAT)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_BUTTON_CONTINUE)->ShowWindow(SW_HIDE);
	}
	else if(m_nBoxTypeRepeat == 1)
	{
		GetDlgItem(IDOK)->ShowWindow(SW_HIDE);
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
void CMessageBox::OnBnClickedButtonStopScript()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	pApp->LogStatus("-->Stop triggered by the user. Please wait...");

	pApp->m_bRun = false; //stop running script
	pApp->m_bUserStopped = true;

	pApp->m_bRecord = false; //stop recording

	UpdateData(true);
	OnOK();
}

void CMessageBox::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}

void CMessageBox::OnBnClickedButtonRepeat()
{
	m_nReturn = 1;
	OnOK();
}

void CMessageBox::OnBnClickedButtonContinue()
{
	m_nReturn = 0;
	OnOK();
}
