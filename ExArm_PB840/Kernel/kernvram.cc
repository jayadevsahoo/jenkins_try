#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ F I L E       D E S C R I P T I O N ====
//@ Filename: kernvram.cc - POST Novram data declaration, test and 
//                          initialization functions
//
//---------------------------------------------------------------------
//@ Interface-Description
//  >Von
//    This module defines the Novram data used during POST and the 
//    following support routines to manage the data.
//
//    Free Functions:
//      KernelNovramTest      - Multi pattern test using a single Novram location
//      KernelNovramDataTest  - Data integrity verification of Novram flags.
//      InitKernelNovram        - Initializes all of POST Novram data.
//
//  >Voff
//---------------------------------------------------------------------
//@ Rationale
//    This module implements Novram test and initialization functions of POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  >Von
//
//  The Kernel uses several flags that require persistent memory to remain
//  intact after a CPU reset.  This memory must be tested prior to its
//  use, but it cannot overwrite flags that are being currently used. The
//  Kernel Novram is a limited test that reads and writes multiple
//  patterns to a single Novram location.   The patterns of 0's, 1's, and
//  alternating 1&0's is used when writing to the variable 
//  nvPatternTestLocation.
//  
//  Once a pattern write has been verified the flags used during POST are
//  checked for their valid states.  If a variable is not one of its
//  proper values it is set to a default state.   This is an important
//  design consideration.  Failing POST because a variable is corrupt may
//  not indicate bad Novram.   This test will fail if it cannot write
//  properly to Novram, but uninitialized flags will be set to a known state.
//  
//  A Novram test failure from this module indicates that the pattern test was 
//  unsuccessful.  It is possible for NOVRAM to be corrupt and still pass this
//  test.  Separate POST tests that use NOVRAM are designed to detect the 
//  corruption of their variables.  This module simply initializes NOVRAM 
//  to known good values.
//
//  A separate area of NOVRAM from the applications NovRamManager
//  is required for POST data.  The following variables are defined for 
//  public use.  
//
//   nvPatternTest - Used during Kernel NOVRAM test as a place to write
//	    and verify multiple test patterns.  This is a 32 bit long word.
//
//	    Values:  all 0, all 1, alternating 0,1's.  
//        Default: N/A
//
//	    of shutdown.  Used during POST to determine shutdown
//	    actions.
//
//	    Values:  EXCEPTION, ACSWITCH, POWERFAIL, UNKNOWN 
//        Default: ACSWITCH
//
//   rollingThunderCount - Using during BD-POST to determine number of
//	    incomplete POST cycles.  It is set to zero during the power
//	    fail NMI and at the end of POST.  It is incremented and
//	    tested during the Rolling Thunder test.
//
//	    Values:  0-2  (3 cycles) Default: 0
//
//	 testingWatchdog - State of the watchdog timer test.  This test
//	    executes in 2 passes, in pass 1 the state is TRUE, after
//	    pass 2 is complete the state is FALSE.
//
//	    Values:  TRUE, FALSE   Default: FALSE
//
//   watchdogMinTimeExpired - Indicator that the watchdog timer reset
//		properly after a minimum timeout period.  This is used
//		during the watchdog timer test.
//
//      Values:  TRUE, FALSE   Default: FALSE
//  
//   checksumIndexInProgress - This is an index to the last checksum
//	    test in progress during the Kernel Flash checksum test.
//	    It is used in the second pass of the checksum test to
//	    bypass previously tested devices.  This is now verified in 
//      the FlashChecksumTest.
//
//  >Voff
//---------------------------------------------------------------------
//@ Fault-Handling
//    Kernel test failures POST stop testing.  No error code is logged as
//    the integrity to write a value is in question.
//---------------------------------------------------------------------
//@ Restrictions
//    The data segment of this module must be located in Non volatile 
//    RAM memory in order to work properly.
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/kernvram.ccv   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 004    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 003    By: Gary Cederquist  Date: 15-MAY-1997  DR Number: 2085
//    Project: Sigma (R8027)
//    Description:
//       Added initialization of auxiliary data region in NOVRAM.
//
//  Revision: 002    By: hct              Date: 15-MAY-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added SRS requirement number 11027.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "kpost.hh"
#include "kernel.hh"
#include "postnv.hh"
#include "cktable.hh"
#include "kernvram.hh"
#include "CpuDevice.hh"
#include "AuxResultLog.hh"

static void InitKernelNovram(void);
static Criticality KernelNovramDataTest(void);

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function:  InitKernelNovram 
//
//@ Interface-Description
//  >Von
//    Sets POST's NOVRAM data to default values.  
//
//    INPUTS:  NONE
//
//    OUTPUTS: NONE
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================


void InitKernelNovram(void)
{
   KernelNovram * pKN = PKernelNovram;   // so compiler optimizes to reg

   //  lastTestInProgress and errorCode are already in use 
   //  so they must NOT be initialized here

   for (int i=0; i<countof(pKN->results); i++)
   {
       pKN->results[i] = PASSED;
   }
   pKN->versionCode             = KERNEL_NOVRAM_VERSION;
   pKN->nvPatternTest           = 0;
   pKN->shutdownState           = ACSWITCH;
   pKN->rollingThunderCount     = 0;
   pKN->testingWatchdog         = FALSE;
   pKN->watchdogMinTimeExpired  = FALSE;
   pKN->postFailed              = FALSE;
   pKN->checksumIndexInProgress = 0;
   pKN->downloadScheduled       = 0;
   pKN->watchdogRetries         = 0;
   AuxResultLog::GetPostAuxResultLog();  // self initializing
   // $[TI1]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: KernelNovramTest 
//
//@ Interface-Description
//  >Von
//    Performs a multi pattern write/read verification on the NOVRAM 
//    variable nvPatternTest.  $[11027] $[11086]
//
//    INPUTS:  NONE
//
//    OUTPUTS: PASS, MAJOR
//
//    An array of test patterns are written the NOVRAM variable nvPatternTest.
//    The test patterns are designed to implement sequences of marching 
//    1's & zeros.
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

Criticality KernelNovramTest  (void)
{
   Criticality    result = PASSED;
   KernelNovram * pKN = PKernelNovram;   // so compiler optimizes to reg

   static const Uint32 pattern[] = {

      0x80000000L, 0xc0000000L, 0xe0000000L, 0xf0000000L, 
      0xf8000000L, 0xfc000000L, 0xfe000000L, 0xff000000L,
      0xff800000L, 0xffc00000L, 0xffe00000L, 0xfff00000L,
      0xfff80000L, 0xfffc0000L, 0xfffe0000L, 0xffff0000L,
      0xffff8000L, 0xffffc000L, 0xffffe000L, 0xfffff000L,
      0xfffff800L, 0xfffffc00L, 0xfffffe00L, 0xffffff00L,
      0xffffff80L, 0xffffffc0L, 0xffffffe0L, 0xfffffff0L,
      0xfffffff8L, 0xfffffffcL, 0xfffffffeL, 0xffffffffL,
      0x7fffffffL, 0x3fffffffL, 0x1fffffffL, 0x0fffffffL,
      0x07ffffffL, 0x03ffffffL, 0x01ffffffL, 0x00ffffffL,
      0x007fffffL, 0x003fffffL, 0x001fffffL, 0x000fffffL,
      0x0007ffffL, 0x0003ffffL, 0x0001ffffL, 0x0000ffffL,
      0x00007fffL, 0x00003fffL, 0x00001fffL, 0x00000fffL,
      0x000007ffL, 0x000003ffL, 0x000001ffL, 0x000000ffL,
      0x0000007fL, 0x0000003fL, 0x0000001fL, 0x0000000fL,
      0x00000007L, 0x00000003L, 0x00000001L, 0x00000000L,
      0x00000007L, 0x00000003L, 0x00000001L, 0x00000000L,
      0xAAAAAAAAL, 0x55555555L, 0xAAAA5555L, 0x5555AAAAL,  // alternating bits
      0xff0000ffL, 0x00ffff00L, 0xffffffffL, 0x00000000L,

      };



   for (int i=0; i< countof(pattern); i++)
   {
      pKN->nvPatternTest = pattern[i];

UNIT_TEST_POINT(POSTUT_35_1)
      if (pKN->nvPatternTest != pattern[i])
      {
         // $[TI1.1]
         result = MAJOR;
         break;
      }
      // $[TI1.2]
   }

   if (result == PASSED)
   {
      // $[TI2.1]
      result = KernelNovramDataTest();
   }
   // $[TI2.2]

   return result;

}



//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: KernelNovramDataTest
//
//@ Interface-Description
//  >Von
//    Data integrity verification of Novram flags.
//
//    INPUTS:  NONE
//
//    OUTPUTS: PASS
//
//    TECHNIQUE
//
//    For each of the Kernel Novram variables, initialize them to
//    their default values if an invalid state exists.
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//    See free function description described above.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
Criticality KernelNovramDataTest(void)
{
    KernelNovram * pKN = PKernelNovram;   // so compiler optimizes to reg

    if ( sizeof(*PKernelNovram) > 512 ) 
    {
       // $[TI1.1]
       return MAJOR;
    }
    // $[TI1.2]
UNIT_TEST_POINT(POSTUT_35)

    if ( pKN->versionCode != KERNEL_NOVRAM_VERSION )
    {
       // $[TI2.1]
       InitKernelNovram();
       return MINOR;
    }
    // $[TI2.2]
 
    Criticality status = PASSED;

    switch (pKN->shutdownState)
    {
       case UNKNOWN:
       case EXCEPTION:
       case ACSWITCH:
       case POWERFAIL:
       case WATCHDOG:
       case INTENTIONAL: 
          // $[TI3.1]
          break; 
       default:
          // $[TI3.2]
          pKN->shutdownState       = ACSWITCH;
          status = MINOR;
    }
 
    for (Uint i=0; i<countof(pKN->results); i++)
    {
        switch ( pKN->results[i] )
        {
            case PASSED:
            case MINOR:
            case SERVICE_REQUIRED:
            case MAJOR:
               // $[TI4.1]
               break;
            default:
               // $[TI4.2]
               pKN->results[i] = PASSED;
               status = MINOR;
        }
    }

    if (   pKN->testingWatchdog != TRUE
        && pKN->testingWatchdog != FALSE )
    {
        // $[TI5.1]
        pKN->testingWatchdog = FALSE;
        status = MINOR;
    }          
    // $[TI5.2]
 
    if (   pKN->watchdogMinTimeExpired != TRUE
        && pKN->watchdogMinTimeExpired != FALSE)
    {
        // $[TI6.1]
        pKN->watchdogMinTimeExpired = FALSE;
        status = MINOR;
    }          
    // $[TI6.2]
 
    if (   pKN->downloadScheduled != SCHEDULE_DOWNLOAD
        && pKN->downloadScheduled != 0 )
    {
        // $[TI7.1]
        pKN->downloadScheduled = 0;
        status = MINOR;
    }          
    // $[TI7.2]

    if (   pKN->postFailed != FALSE
        && pKN->postFailed != TRUE)
    {
        // $[TI8.1]
        pKN->postFailed = FALSE;
        status = MINOR;
    }
    // $[TI8.2]
 
    if ( AuxResultLog::GetPostAuxResultLog().verify() )
    {
        // $[TI9.1]
        status = MINOR;
    }
    // $[TI9.2]

    return status;
}

