#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  DciBaudRateSetting - DCI Baud Rate Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the baud rate for
//  the ventilator's serial Digital Communication Interface (DCI).  This
//  class inherits from 'BatchDiscreteSetting' and provides the specific
//  information needed for the representation of DCI's baud rate.  This
//  information includes the set of values that this setting can have (see
//  'BaudRateValue.hh'), and this batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DciBaudRateSetting.ccv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009    By: mnr      Date: 03-Mar-2010  SCR Number: 6460
//  Project: NEO
//  Description:
//  Enhanced SCR 6460 fix to revert to user selection (if any).
// 
//  Revision: 008    By: mnr      Date: 24-Feb-2010  SCR Number: 6460
//  Project: NEO
//  Description:
//  Fixed SCR 6460 regarding Baud Rate not reverting back.
// 
//  Revision: 007    By: erm       Date: 5-Nov-2008  SCR Number: 6441
//  Project: 840S
//  Description:
//  Added support for RT waveform to serial port
// 
//  Revision: 006   By: hct    Date: 03-DEC-1999    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Corrected incomplete comment.
//
//  Revision: 004   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "DciBaudRateSetting.hh"
#include "BaudRateValue.hh"
#include "ComPortConfigValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  DciBaudRateSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01191] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DciBaudRateSetting::DciBaudRateSetting(SettingId::SettingIdType settingId):
BatchDiscreteSetting(settingId, Setting::NULL_DEPENDENT_ARRAY_,
					 BaudRateValue::TOTAL_BAUD_RATES),
	settingId_(settingId)
{
	CALL_TRACE("DciBaudRateSetting()");
}  // $[TI1]


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~DciBaudRateSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DciBaudRateSetting::~DciBaudRateSetting(void)
{
	CALL_TRACE("~DciBaudRateSetting(void)");
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAdjustedValue(newAdjustedValue)  [virtual]
//
//@ Interface-Description
//  This virtual method is called to set the adjusted value of this 
//  setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the Setting base class method to actually store the new value
//  in the adjusted context. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void DciBaudRateSetting::setAdjustedValue(const SettingValue& newValue)
{
	AUX_CLASS_ASSERTION(isEnabledValue(DiscreteValue(newValue)), DiscreteValue(newValue));

	// call the base class to set the value into the adjusted context
	Setting::setAdjustedValue(newValue);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  DCI baud rate is ALWAYS changeable.
//
//  $[01191] -- communication settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
DciBaudRateSetting::getApplicability(const Notification::ChangeQualifier) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	return(Applicability::CHANGEABLE);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
DciBaudRateSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");


	const AcceptedContext&  rAcceptedContext = ContextMgr::GetAcceptedContext();

	DiscreteValue  newPatientValue;

	if ( rAcceptedContext.areBatchValuesInitialized() )
	{	// $[TI1]
		// when the Accepted Context has previosly-initialized values, use the
		// previous value of this setting...
		newPatientValue = rAcceptedContext.getBatchDiscreteValue(settingId_);
	}
	else
	{	// $[TI2]
		// $[01191] The setting's new-patient value ...
		newPatientValue = BaudRateValue::BAUD_9600_VALUE;
	}

	return(newPatientValue);
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of 'this' objects primary setting values has changed, so the settings
//  value will be based on the new values, indicaticated by primaryId. 
//  If this setting's bound is violated by the primary setting's newly 
//  "adjusted" value, then the setting's value is "clipped" to that bound. 
//  Otherwise, a value of 'NULL' is returned to indicate no dependent 
//  bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02002] -- new dependent settings based on proposed primary settings...
//  $[LC01013] When Real-time waveform output is selected 
//            and the baud rate for the corresponding serial port 
//            is set below 38400, then the baud rate shall be set to 38400.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
DciBaudRateSetting::acceptPrimaryChange(const SettingId::SettingIdType primaryId)
{
	CALL_TRACE("acceptPrimaryChange(primaryId)");


	const Setting*  pComPort =
		SettingsMgr::GetSettingPtr(primaryId);

	const DiscreteValue GOING_TO_VALUE =
		pComPort->getAdjustedValue();

	// Clamped to a min baud of  38400
	if ( GOING_TO_VALUE == ComPortConfigValue::SENSOR_VALUE )
	{
		if ( settingId_ == SettingId::COM1_BAUD_RATE && primaryId == SettingId::COM1_CONFIG )
		{
			setAdjustedValue(DiscreteValue(BaudRateValue::BAUD_38400_VALUE));
		}
		else if ( settingId_ == SettingId::COM2_BAUD_RATE && primaryId == SettingId::COM2_CONFIG )
		{
			setAdjustedValue(DiscreteValue(BaudRateValue::BAUD_38400_VALUE));
		}
		else if ( settingId_ == SettingId::COM3_BAUD_RATE && primaryId == SettingId::COM3_CONFIG )
		{
			setAdjustedValue(DiscreteValue(BaudRateValue::BAUD_38400_VALUE));
		}
	}
	else
	{
		const AcceptedContext& rAcceptedContext = ContextMgr::GetAcceptedContext();

		DiscreteValue BAUD_RATE_VALUE;

			BAUD_RATE_VALUE = rAcceptedContext.getSettingValue(settingId_);

		if ( settingId_ == SettingId::COM1_BAUD_RATE && primaryId == SettingId::COM1_CONFIG )
		{
            setAdjustedValue(BAUD_RATE_VALUE);
		}
		else if ( settingId_ == SettingId::COM2_BAUD_RATE && primaryId == SettingId::COM2_CONFIG )
		{
            setAdjustedValue(BAUD_RATE_VALUE);
		}
		else if ( settingId_ == SettingId::COM3_BAUD_RATE && primaryId == SettingId::COM3_CONFIG )
		{
			setAdjustedValue(BAUD_RATE_VALUE);
		}
	}

	return(NULL);
}
//================ M E T H O D   D E S C R I P T I O N ================
//@ Method:  isEnabledValue(value)  [virtual]
//
//@ Interface-Description
//  This virtual method is to be overridden by all discrete settings
//  whose values have varying states of applicability.  This version of
//  this method always returns 'TRUE', irregardless of 'value'.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[BL04079]
//---------------------------------------------------------------------
//@ PreCondition
//  (isDiscreteSetting())
//---------------------------------------------------------------------
//@ PostCondition
//  (TRUE)
//@ End-Method
//=====================================================================

Boolean
DciBaudRateSetting::isEnabledValue(const DiscreteValue value) const
{
	Boolean isEnabled = TRUE;

	SettingId::SettingIdType comPortTarget;

	if ( settingId_ == SettingId::COM1_BAUD_RATE )
	{
		comPortTarget = SettingId::COM1_CONFIG;
	}
	else if ( settingId_ == SettingId::COM2_BAUD_RATE )
	{
		comPortTarget = SettingId::COM2_CONFIG;
	}
	else if ( settingId_ == SettingId::COM3_BAUD_RATE )
	{
		comPortTarget = SettingId::COM3_CONFIG;
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(settingId_);
	}


	const Setting*  pComPort =
		SettingsMgr::GetSettingPtr(comPortTarget);

	const DiscreteValue GOING_TO_VALUE =
		pComPort->getAdjustedValue();

	if ( GOING_TO_VALUE == ComPortConfigValue::SENSOR_VALUE )
	{
		if ( (value == BaudRateValue::BAUD_1200_VALUE) ||
			 (value == BaudRateValue::BAUD_2400_VALUE) ||
			 (value == BaudRateValue::BAUD_4800_VALUE) ||
			 (value == BaudRateValue::BAUD_9600_VALUE) ||
			 (value == BaudRateValue::BAUD_19200_VALUE)
		   )
		{
			isEnabled = FALSE;
		}

	}

	return(isEnabled);   
}

//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
DciBaudRateSetting::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							DCI_BAUD_RATE_SETTING, lineNumber, pFileName,
							pPredicate);
}
