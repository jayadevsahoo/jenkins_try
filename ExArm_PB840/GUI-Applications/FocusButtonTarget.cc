#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FocusButtonTarget -  A target class for receiving 
//                              AdjustPanelTarget's adjustKnobHappened 
//                              events.  
//---------------------------------------------------------------------
//@ Interface-Description
// Any class interested in receiving user's knob events must inherit 
// this class.  They must also have a callback routine adjustKnobHappened()
// method where the user's knob event messages are provided.
//---------------------------------------------------------------------
//@ Rationale
// Used to implement the callback mechanism needed to communicate user's
// knob interactions.
//---------------------------------------------------------------------
//@ Implementation-Description
// Classes which create a FocusButton object must do two things: 
// 1) Inherit from this class and 2) define the adjustKnobHappened() 
// method of this class.  Those classes will then be automatically 
// informed of any interactions with the FocusButton object.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// There is no provision for an object creating more than one FocusButton 
// object and then being able to distinguish which FocusButton was 
// scrolled when adjustKnobHappened() is called.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/FocusButtonTarget.ccv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  rhj    Date:  06-Jun-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version
//=====================================================================

#include "FocusButtonTarget.hh"


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FocusButtonTarget()  [Protected constructor]
//
//@ Interface-Description
// The constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FocusButtonTarget::FocusButtonTarget(void)
{
	CALL_TRACE("FocusButtonTarget::FocusButtonTarget(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FocusButtonTarget()  [Protected destructor]
//
//@ Interface-Description
// FocusButtonTarget destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FocusButtonTarget::~FocusButtonTarget(void)
{
	CALL_TRACE("FocusButtonTarget::~FocusButtonTarget(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustKnobHappened(Int32 delta)
//
//@ Interface-Description
// Called when the operator turns the knob.  The result is an integer 
// which indicates the amount of knob clicks was turned by the user.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FocusButtonTarget::adjustKnobHappened(Int32 delta)
{
	CALL_TRACE("FocusButtonTarget::adjustKnobHappened(Int32 delta)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void
FocusButtonTarget::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, FOCUSBUTTONTARGET,
							lineNumber, pFileName, pPredicate);
}

