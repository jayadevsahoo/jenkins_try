#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureSetInspTrigger - Triggers if conditions for pressure 
//	triggering based on a set pressure trigger are met.
//---------------------------------------------------------------------
//@ Interface-Description 
//    This class is derived from PressureInspTrigger. Most of its 
//    functionality resides in the base class. This class only provides
//    the virtual method required to return a user set pressure
//    sensitivity from the Settings subsystem. Refer to the base class
//    for its interface and implementation description.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for detecting patient inspiratory 
//    effort when pressure triggering has been selected by the operator. 
//    Since the pressure sensitivity for the backup pressure trigger
//    and the user set pressure trigger differ algorithmically, this 
//    class provides the derived method for retrieving the user set
//    pressure sensitivity. The base class provides the method for a 
//    fixed pressure sensitivity established at construction.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class is derived from PressureInspTrigger. The base class
//    references the virtual method getPressureSens() to determine the 
//    pressure sensitivity for this trigger. This class returns the 
//    pressure sensitivity set by the user.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureSetInspTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial implementation.
//
//=====================================================================

#include "PressureSetInspTrigger.hh"

//@ Usage-Classes
#include "PhasedInContextHandle.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureSetInspTrigger
//
//@ Interface-Description
//      This constructor takes triggerId as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using PatientTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureSetInspTrigger::PressureSetInspTrigger(
	const Trigger::TriggerId triggerId)
:
PressureInspTrigger(triggerId, 0.0)
{
	CALL_TRACE("PressureSetInspTrigger(triggerId)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureSetInspTrigger()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureSetInspTrigger::~PressureSetInspTrigger(void)
{
  CALL_TRACE("~PressureSetInspTrigger()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPressureSens [virtual]
//
//@ Interface-Description
//  This method takes no parameters.  Returns the user set pressure 
//  sensitivity from the phased in settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Returns the value of the pressure sensitivity setting.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Real32
PressureSetInspTrigger::getPressureSens(void) const
{
  CALL_TRACE("getPressureSens()");

  return PhasedInContextHandle::GetBoundedValue(SettingId::PRESS_SENS).value;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PressureSetInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PRESSURESETINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

