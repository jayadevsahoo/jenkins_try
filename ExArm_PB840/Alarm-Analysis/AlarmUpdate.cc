#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmUpdate - Info passed by AlarmManager to AlarmAnnunciator.
//---------------------------------------------------------------------
//@ Interface-Description

// The Alarm Analysis cluster constructs AlarmUpdate objects in a list which is
// passed as a parameter to the AlarmAnnunciation::Update_ method.

//---------------------------------------------------------------------
//@ Rationale
// All alarm information necessary for the Alarm Annunciation cluster to update
// the Alarm Summary, Alarm Audio, Alarm History Log and Current Alarm Log 
// is included in the AlarmUpdate record by the Alarm Analysis cluster.
//---------------------------------------------------------------------
//@ Implementation-Description
// Once constructed, an AlarmUpdate object isn't changed; it is only read.
// Read access methods are provided for the information stored in the object.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmUpdate.ccv   25.0.4.0   19 Nov 2013 13:51:18   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 06/29/95         DR Number:   499
//      Project:   Sigma   (R8027)
//      Description:
//         Added a constructor to build an AlarmUpdate from and AlarmComm.
//  
//   Revision 003   By:   hct      Date: 06/29/95         DR Number:   499
//      Project:   Sigma   (R8027)
//      Description:
//         Implemented operator= method to accomodate changes to the
//         AlarmUpdateList.
//  
//   Revision 004   By:   hct      Date: 06/29/95         DR Number:   499
//      Project:   Sigma   (R8027)
//      Description:
//         Made method getCurrentPriority() always present for use by the
//         AlarmComm struct.
//  
//   Revision 005   By:   hct      Date: 08/02/95         DR Number:   502
//      Project:   Sigma   (R8027)
//      Description:
//         Added attribute isDependent_ and method getIsDependent.  Added
//         code to handle isDependent_.  The DCI functionality requires that
//         the status of Alarm::isDependent_ be sent to the 
//         AlarmAnnunciation cluster via the AlarmUpdate object.
//  
//   Revision 006   By:   hct      Date: 01/11/96         DR Number:   649
//      Project:   Sigma   (R8027)
//      Description:
//         Changed conditional compilation label from SIGMA_DEVELOPMENT to
//         SIGMA_SIMULATOR.
//  
//   Revision 007   By:   gbs      Date: 05/17/96         DR Number:   995
//      Project:   Sigma   (R8027)
//      Description:
//         Fix CALL_TRACE and add passed parameter comments.
//         These changes do not affect Unit Test.
//  
//   Revision 008   By:   hct      Date: 02/10/97         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.  Don't need AlarmComm anymore.
//  
//   Revision 009   By:   hct      Date: 24 APR 1997      DR Number:  1994
//      Project:   Sigma   (R8027)
//      Description:
//         Deleted requirement number 00627.
//  
//   Revision 010   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 011   By:   yyy      Date: 08/18/99         DR Number:   5513
//      Project:   ATC
//      Description:
//         Modified to handle alarm lock and break through.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "AlarmUpdate.hh"

//@ Usage-Classes
#include "Alarm.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmUpdate
//
//@ Interface-Description
//   Default Constructor.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

AlarmUpdate::AlarmUpdate(void)
{
  CALL_TRACE("AlarmUpdate(void)");

  name_ = ALARM_NAME_NULL;
  isActive_ = FALSE;
  isDependent_ = FALSE;
  currentUrgency_ = Alarm::NORMAL_URGENCY;
  previousUrgency_ = Alarm::NORMAL_URGENCY;
  currentPriority_ = 0;
  baseMessage_ = MESSAGE_NAME_NULL;
  updateType_ = Alarm::UPDATE_TYPE_NULL;
  analysisIndex_ = 0;
  remedyIndex_ = 0;
  breakThroughSilence_ = FALSE;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmUpdate
//
//@ Interface-Description
//   Copy Constructor.
//
// >Von
//   rUpdate  Reference to object to be copied from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

AlarmUpdate::AlarmUpdate(const AlarmUpdate& rUpdate) :
                       name_(rUpdate.name_),
                       isActive_(rUpdate.isActive_),
                       isDependent_(rUpdate.isDependent_),
                       whenOccurred_(rUpdate.whenOccurred_),
                       currentUrgency_(rUpdate.currentUrgency_),
                       previousUrgency_(rUpdate.previousUrgency_),
                       currentPriority_(rUpdate.currentPriority_),
                       baseMessage_(rUpdate.baseMessage_),
                       currentAnalysisMessage_(rUpdate.currentAnalysisMessage_),
                       currentRemedyMessage_(rUpdate.currentRemedyMessage_),
                       updateType_(rUpdate.updateType_),
                       analysisIndex_(rUpdate.analysisIndex_),
                       remedyIndex_(rUpdate.remedyIndex_),
		               breakThroughSilence_(rUpdate.breakThroughSilence_)
{
  CALL_TRACE("AlarmUpdate(rUpdate)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmUpdate
//
//@ Interface-Description
//   Constructor accepting an Alarm object as input parameter.
//
// >Von
//   rAlarm  Reference to the object to be used for construction.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   If the Alarm being used to construct the AlarmUpdate object was updated 
//   because it was augmented or became independent, use the Alarm object's 
//   whenAugmented_ attribute to fill the AlarmUpdate object's whenOccurred_ 
//   attribute; otherwise, use the Alarm object's whenOccurred_ attribute.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

AlarmUpdate::AlarmUpdate(const Alarm& rAlarm) :
                    name_(rAlarm.getName()),
                    isActive_(rAlarm.getIsActive()),
                    isDependent_(rAlarm.getIsDependent()),
                    currentUrgency_(rAlarm.getCurrentUrgency()),
                    previousUrgency_(rAlarm.getPreviousUrgency()),
                    currentPriority_(rAlarm.getCurrentPriority()),
                    baseMessage_(rAlarm.getBaseMessage()),  
                    currentAnalysisMessage_(rAlarm.getCurrentAnalysisMessage()),
                    currentRemedyMessage_(rAlarm.getCurrentRemedyMessage()),
                    updateType_(rAlarm.getUpdateType()),
                    analysisIndex_(rAlarm.getAnalysisIndex()),
                    remedyIndex_(rAlarm.getRemedyIndex()),
		            breakThroughSilence_(rAlarm.getBreakThroughSilence())
{
  CALL_TRACE("AlarmUpdate(rAlarm)");

  if ((updateType_ == Alarm::AUGMENTATION_UT) || 
      (updateType_ == Alarm::INDEPENDENT_UT)) // $[TI1]
  {
    whenOccurred_ = rAlarm.getWhenAugmented();
  }
  else // $[TI2]
  {
    whenOccurred_ = rAlarm.getWhenOccurred();
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmUpdate
//
//@ Interface-Description
//   Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

AlarmUpdate::~AlarmUpdate(void)
{
  CALL_TRACE("~AlarmUpdate(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//   Assignment operator.
//
// >Von
//   rUpdate  Reference to object to be copied from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AlarmUpdate::operator=(const AlarmUpdate& rUpdate)
{
  CALL_TRACE("operator=(rUpdate)");

  name_ = rUpdate.name_;
  isActive_ = rUpdate.isActive_;
  isDependent_ = rUpdate.isDependent_;
  whenOccurred_ = rUpdate.whenOccurred_;
  currentUrgency_ = rUpdate.currentUrgency_;
  previousUrgency_ = rUpdate.previousUrgency_;
  currentPriority_ = rUpdate.currentPriority_;
  baseMessage_ = rUpdate.baseMessage_;
  currentAnalysisMessage_ = rUpdate.currentAnalysisMessage_;
  currentRemedyMessage_ = rUpdate.currentRemedyMessage_;
  updateType_ = rUpdate.updateType_;
  analysisIndex_ = rUpdate.analysisIndex_;
  remedyIndex_ = rUpdate.remedyIndex_;
  breakThroughSilence_ = rUpdate.breakThroughSilence_;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
//   Equality operator.
//
// >Von
//   rUpdate  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   Two AlarmUpdate objects are equal if their currentUrgency_ and 
//   currentPriority_ attributes are equal.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

Boolean
AlarmUpdate::operator==(const AlarmUpdate& rUpdate) const
{
  CALL_TRACE("operator==(rUpdate)");

  return((currentUrgency_  == rUpdate.currentUrgency_) &&
         (currentPriority_ == rUpdate.currentPriority_)); // $[TI1] $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
//   Inequality operator.
//
// >Von
//   rUpdate  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   Two AlarmUpdate objects are not equal if their currentUrgency_ or
//   currentPriority_ attributes are not equal.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

Boolean
AlarmUpdate::operator!=(const AlarmUpdate& update) const
{
  CALL_TRACE("operator!=(rUpdate)");

  return (!(*this == update)); // $[TI1] $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator<
//
//@ Interface-Description
//   Less than operator which tests to see if this AlarmUpdate object is less
//   than the AlarmUpdate object referenced by the passed parameter, update.
//
// >Von
//   rUpdate  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method is provided to be used by a sorted list Foundation class.  The
//   AlarmUpdate object is used in conjunction with a sorted list to annunciate
//   the Current Alarm Log which is annunciated in descending order of
//   urgency/priority; therefore, the greater the urgency, the closer to the
//   beginning of the list the AlarmUpdate object appears.  If urgencies are
//   equal, then the priority is examined.  The (numerically) lower the priority,
//   the closer to the beginning of the list the AlarmUpdate object appears.
//   This AlarmUpdate object is less than the referenced AlarmUpdate object
//   (appears earlier in the list) if the current urgency of this AlarmUpdate
//   is greater than the current urgency of the AlarmUpdate object referenced
//   by the passed parameter, update, OR if the current urgencies are equal,
//   then if the current priority of this AlarmUpdate object is less than the
//   current priority of the AlarmUpdate object referenced by the passed
//   parameter, update.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

Boolean
AlarmUpdate::operator<(const AlarmUpdate& rUpdate) const
{
  CALL_TRACE("operator<(rUpdate)");

  Boolean tempBool = FALSE;

  if (currentUrgency_ > rUpdate.currentUrgency_) // $[TI1]
  {
    tempBool = TRUE;
  }
  else if (currentUrgency_ == rUpdate.currentUrgency_) // $[TI2]
  {
    tempBool = currentPriority_ < rUpdate.currentPriority_;
  } // else $[TI3]

  return(tempBool);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator>
//
//@ Interface-Description
//   Greater than operator which tests to see if this AlarmUpdate object is
//   greater than the AlarmUpdate object referenced by the passed parameter,
//   update.
//
// >Von
//   rUpdate  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method is provided to be used by a sorted list Foundation class.  The
//   AlarmUpdate object is used in conjunction with a sorted list to annunciate
//   the Current Alarm Log which is annunciated in descending order of
//   urgency/priority; therefore, the greater the urgency, the closer to the
//   beginning of the list the AlarmUpdate object appears.  If urgencies are
//   equal, then the priority is examined.  The (numerically) lower the priority,
//   the closer to the beginning of the list the AlarmUpdate object appears.
//   This AlarmUpdate object is greater than the referenced AlarmUpdate object
//   (appears later in the list) if the current urgency of this AlarmUpdate is
//   less than the current urgency of the AlarmUpdate object referenced by the
//    passed parameter, update, OR if the current urgencies are equal, then if
//   the current priority of this AlarmUpdate object is greater than the current
//   priority of the AlarmUpdate object referenced by the passed parameter, update.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

Boolean
AlarmUpdate::operator>(const AlarmUpdate& rUpdate) const
{
  CALL_TRACE("operator>(rUpdate)");

  Boolean tempBool = FALSE;

  if (currentUrgency_ < rUpdate.currentUrgency_) // $[TI1]
  {
    tempBool = TRUE;
  }
  else if (currentUrgency_ == rUpdate.currentUrgency_) // $[TI2]
  {
    tempBool = currentPriority_ > rUpdate.currentPriority_;
  } // else $[TI3]

  return(tempBool);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getUrgencyMessage
//
//@ Interface-Description
//   Return the MessageName enumerator associated with attribute,
//   currentUrgency_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

MessageName
AlarmUpdate::getUrgencyMessage(void) const
{
  CALL_TRACE("getUrgencyMessage(void)");

  MessageName tempMessage = MESSAGE_NAME_NULL;

  switch (currentUrgency_)
  {
    case (Alarm::NORMAL_URGENCY): // $[TI1]
      tempMessage = NORMAL_URGENCY_MSG;
      break;
    case (Alarm::LOW_URGENCY): // $[TI2]
      tempMessage = LOW_URGENCY_MSG;
      break;
    case (Alarm::MEDIUM_URGENCY): // $[TI3]
      tempMessage = MEDIUM_URGENCY_MSG;
      break;
    case (Alarm::HIGH_URGENCY): // $[TI4]
      tempMessage = HIGH_URGENCY_MSG;
      break;
    default: 
      CLASS_ASSERTION((Alarm::URGENCY_TYPE_NULL < currentUrgency_) &&
                      (currentUrgency_ < Alarm::MAX_NUM_URGENCIES));
      break;
  }
  return(tempMessage);
}



#ifdef SIGMA_SIMULATOR

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getUpdateTypeMessage
//
//@ Interface-Description
//   Return the MessageName enumerator associated with attribute, updateType_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

MessageName
AlarmUpdate::getUpdateTypeMessage(void) const
{
  CALL_TRACE("getUpdateTypeMessage(void)");

  MessageName tempMessage = MESSAGE_NAME_NULL;

  switch (updateType_)
  {
    case (Alarm::AUTO_RESET_UT):
      tempMessage = AUTO_RESET_UT_MSG;
      break;
    case (Alarm::PAT_DATA_RESET_UT):
      tempMessage = PAT_DATA_RESET_UT_MSG;
      break;
    case (Alarm::ALARM_SILENCE_UT):
      tempMessage = ALARM_SILENCE_UT_MSG;
      break;
    case (Alarm::END_ALARM_SILENCE_UT):
      tempMessage = END_ALARM_SILENCE_UT_MSG;
      break;
    case (Alarm::AUGMENTATION_UT):
      tempMessage = AUGMENTATION_UT_MSG;
      break;
    case (Alarm::DETECTION_UT):
      tempMessage = DETECTION_UT_MSG;
      break;
    case (Alarm::MANUAL_RESET_UT):
      tempMessage = MANUAL_RESET_UT_MSG;
      break;
    case (Alarm::INDEPENDENT_UT):
      tempMessage = INDEPENDENT_UT_MSG;
      break;
    case (Alarm::END_ALARM_SILENCE_HIGH_URGENCY_UT):
      tempMessage = END_ALARM_SILENCE_HIGH_URGENCY_UT_MSG;
      break;
    case (Alarm::UPDATE_TYPE_NULL):
    case (Alarm::MAX_NUM_UPDATE_TYPE):
    default: 
      CLASS_ASSERTION((Alarm::UPDATE_TYPE_NULL < updateType_) &&
                      (updateType_ < Alarm::MAX_NUM_UPDATE_TYPE));
      break;
  }
  return(tempMessage);
}


#endif // SIGMA_SIMULATOR



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
AlarmUpdate::SoftFault(const SoftFaultID  softFaultID,
                       const Uint32       lineNumber,
                       const char*        pFileName,
                       const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::ALARMUPDATE, lineNumber,
                          pFileName, pPredicate);
}
