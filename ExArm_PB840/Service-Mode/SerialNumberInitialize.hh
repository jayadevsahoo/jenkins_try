#ifndef SerialNumberInitialize_HH
#define SerialNumberInitialize_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SerialNumberInitialize - to copy serial number from data key to
//			flash.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SerialNumberInitialize.hhv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  27-Mar-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version.
//
//=====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

//@ End-Usage


class SerialNumberInitialize {
  public:
	SerialNumberInitialize( void) ;
	~SerialNumberInitialize( void) ;

	static void SoftFault( const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL) ;

	void copySerialNumber( void) ;

#ifdef SIGMA_DEVELOPMENT
	void storeXs( void) ;
#endif // SIGMA_DEVELOPMENT

  protected:

  private:
    SerialNumberInitialize( const SerialNumberInitialize&) ;// not implemented...
    void operator=( const SerialNumberInitialize&) ;		// not implemented...

} ;


#endif // SerialNumberInitialize_HH 
