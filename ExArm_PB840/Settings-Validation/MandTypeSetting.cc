#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  MandTypeSetting - Mandatory Type Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates what type of mandatory
//  breaths (i.e., "volume-controlled" or "pressure-controlled") are to be
//  applied to the patient.  This setting is also a Main Control Setting,
//  and, therefore, activates the Transition Rules of the settings that
//  are dependent on this setting's transitions.  This class inherits from
//  'BatchDiscreteSetting' and provides the specific information needed for the
//  representation of mandatory type.  This information includes the set
//  of values that this setting can have (see 'MandTypeValue.hh'), this
//  setting's activation of Transition Rules (see 'calcTransition()'), and
//  this batch setting's new-patient value.
//
//  Because this is a Main Control Setting, the changing of this setting
//  is done differently than the other settings.  The direct changing of a
//  Main Control Setting does not update the settings that are dependent on
//  its value -- unlike those settings that have dependent settings and
//  override 'calcDependentValues_()'.  The updating of the "dependent"
//  settings of Main Control Settings is done by the Adjusted Context AFTER
//  all of the Main Control Settings have been changed and approved by the
//  operator.  This update is done via the Transition Rules defined for this
//  setting (see 'calcTransition()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/MandTypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 024   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 023   By: mnr    Date: 07-Jan-2010    SCR Number: 6437
//  Project:  NEO
//  Description:
//		SCR number added to header for revision 021.
//
//  Revision: 022   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Transition based on NeoMode Advanced instead of Update option.
//
//  Revision: 021   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Trigger Peak flow rules in NEONATAL VC+ to VC transition.
//
//  Revision: 020   By: mnr    Date: 28-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Adherence to coding standards according to the action items.
//
//  Revision: 019   By: mnr    Date: 27-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		From VC+ to VC check Tidal volume lower bounds.
//
//  Revision: 018   By: gdc    Date: 31-Dec-2008    SCR Number: 6177
//  Project:  840S
//  Description:
//      Restructured class to correctly revert to the user-selected value 
// 		when an "observed" setting has forced a change to the value of
// 		this setting. Changed getNewPatient method to provide default value 
// 		for	this setting based on the settings it depends upon. Implemented
// 		derived method setAdjustedValue to save the user-selected value for
// 		so it can be used by valueUpdate in handling changes to higher level
// 		settings such as vent-type.
//
//  Revision: 017   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 016   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 015  By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//     
//  Revision: 014  By: gdc    Date: 24-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Reworked per code review.  
//   
//  Revision: 013  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      NIV mods. Added SRS requirement numbers.  
//
//  Revision: 012  By: gdc    Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//   
//  Revision: 011 By: jja     Date:  16-Apr-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//	Fix transition case when VC to VC+ should use circuit based rules.
//
//  Revision: 010 By: jja     Date:  21-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//	Added Vt/Vti/Vsupp setting interaction/limitations for VC+ and VS
//
//  Revision: 009  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added new members, 'isNonBilevelStored_' and 'isLastMandValueVtpc_',
//         for supporting the automatic changing of this setting, based on
//         changes to mode (see 'valueUpdate()')
//      *  added 'isEnabledValue()' method to control access to 'VC+' value
//      *  changed 'calcTransition()' to support 'VC+' and new conditional
//         transition rules
//      *  updated 'valueUpdate()' to handle changing this value, while mode
//         is changing
//      *  added override of 'calcNewValue()' to clear new
//         'isLastMandValueVtpc_' flag, when the user changes the setting
//         directly
//
//  Revision: 008   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 007   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  incorporated circuit-specific new-patient values
//	*  fixed comments
//
//  Project:  NeoMode
//  Description:
//	Incorporated initial specifications for NeoMode Project.
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added special processing for BiLevel (hard-coded to 'PC'),
//	   thereby removing this special processing from non-related
//	   classes
//	*  added new 'getApplicability()' method
//	*  simplified 'calcTransition()' method implementation
//	*  removed unneeded 'acceptTransition()' method (now handled via
//	   the observer mechanism -- see 'valueUpdate()')
//	*  now an observer of mode setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden
//	   from observer base class
//
//  Revision: 005   By: dosman Date:  07-May-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	add TIs
//
//  Revision: 004   By: dosman Date:  19-Apr-1998    DR Number: 14 
//  Project:  BILEVEL
//  Description:
//	fixed so that we cannot get VCV in BILEVEL
//	by being in BILEVEL,
//	changing the mode, then changing to VC, 
//	then changing back to BILEVEL, and continuing,....
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: dosman Date:  19-Feb-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//	added acceptTransition() (see Method Description for details)
//      Added requirement numbers for tracing to SRS. 
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "MandTypeSetting.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "PatientCctTypeValue.hh"
#include "VentTypeValue.hh"
#include "TidalVolumeSetting.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
//@ End-Usage

//@ Code...

static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"PC", MandTypeValue::PCV_MAND_TYPE},
	{"VC+", MandTypeValue::VCP_MAND_TYPE},
	{"VC", MandTypeValue::VCV_MAND_TYPE},
	{NULL , NULL}
};


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  MandTypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

MandTypeSetting::MandTypeSetting(void)
: BatchDiscreteSetting(SettingId::MAND_TYPE,
					   Setting::NULL_DEPENDENT_ARRAY_,
					   MandTypeValue::TOTAL_MAND_TYPES),
	isChangeForced_(FALSE),
	operatorSelectedValue_(NO_SELECTION_)
{
	setLookupTable(ParameterLookup_);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~MandTypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

MandTypeSetting::~MandTypeSetting(void)
{
	CALL_TRACE("~MandTypeSetting(void)");
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAdjustedValue(newAdjustedValue)  [virtual]
//
//@ Interface-Description
//  This virtual method is called to set the adjusted value of this 
//  setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the Setting base class method to actually store the new value
//  in the adjusted context. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void MandTypeSetting::setAdjustedValue(const SettingValue& newValue)
{
	AUX_CLASS_ASSERTION(isEnabledValue(DiscreteValue(newValue)), DiscreteValue(newValue));

	// call the base class to set the value into the adjusted context
	Setting::setAdjustedValue(newValue);

	if ( !isChangeForced_ )
	{
		operatorSelectedValue_ = newValue;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When in BILEVEL mode, this setting is fixed (viewable) at PC, otherwise
//  its changeable.
//
//  $[01080] -- main control setting applicability
//  $[02190] -- fixed at 'PC' when in BILEVEL mode
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id MandTypeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

	DiscreteValue  modeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			modeValue = pMode->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			modeValue = pMode->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicabilityId;

	switch ( modeValue )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :
		case ModeValue::CPAP_MODE_VALUE :
		case ModeValue::SPONT_MODE_VALUE :		  // $[TI3]
			applicabilityId = Applicability::CHANGEABLE;
			break;
		case ModeValue::BILEVEL_MODE_VALUE :	  // $[TI4]
			applicabilityId = Applicability::VIEWABLE;
			break;
		default :
			// unexpected mode value...
			AUX_CLASS_ASSERTION_FAILURE(modeValue);
			break;
	}

	if ( qualifierId == Notification::ADJUSTED && 
		 ContextMgr::GetAdjustedContext().getAllSettingValuesKnown() &&
		 SettingsMgr::GetSettingPtr(SettingId::IBW)->isChanged() )
	{
		applicabilityId = Applicability::VIEWABLE;
	}

	return(applicabilityId);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isEnabledValue()  [const, virtual]
//
//@ Interface-Description
//  Is this setting's value given by 'value', a currently-enabled value?
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02190] -- 'VC' restricted to A/C, SIMV & SPONT; 'VC+' to A/C and SIMV
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean MandTypeSetting::isEnabledValue(const DiscreteValue value) const
{
	Boolean  isEnabled;

	switch ( value )
	{
		case MandTypeValue::PCV_MAND_TYPE :		  // $[TI1] -- standard, unrestricted...
			// this standard, unrestricted values is ALWAYS enabled...
			isEnabled = TRUE;
			break;

		case MandTypeValue::VCV_MAND_TYPE :		  // $[TI2] -- standard, but restricted...
			{
				const DiscreteValue  MODE_VALUE =
					SettingsMgr::GetSettingPtr(SettingId::MODE)->getAdjustedValue();

				switch ( MODE_VALUE )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SIMV_MODE_VALUE :
					case ModeValue::CPAP_MODE_VALUE :
					case ModeValue::SPONT_MODE_VALUE :	  // $[TI2.1]
						isEnabled = TRUE;
						break;
					case ModeValue::BILEVEL_MODE_VALUE :  // $[TI2.2]
						// VC not allowed for BiLevel mode...
						isEnabled = FALSE;
						break;
					default :
						// unexpected mode value...
						AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
						break;
				}
			}
			break;

		case MandTypeValue::VCP_MAND_TYPE :		  // $[TI3] -- optional, and restricted...
			if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::VTPC) )
			{  // $[TI3.1] -- VC+ option is enabled, check for restrictions...
				const DiscreteValue  VENT_TYPE =
					SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();
				const DiscreteValue  MODE_VALUE =
					SettingsMgr::GetSettingPtr(SettingId::MODE)->getAdjustedValue();

				switch ( MODE_VALUE )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SIMV_MODE_VALUE :	  // $[TI3.1.1]
						// VC+ allowed for A/C and SIMV only... and not with NIV
						isEnabled = (VENT_TYPE == VentTypeValue::INVASIVE_VENT_TYPE);
						break;
					case ModeValue::CPAP_MODE_VALUE :
					case ModeValue::SPONT_MODE_VALUE :
					case ModeValue::BILEVEL_MODE_VALUE :  // $[TI3.1.2]
						// VC+ not allowed for CPAP/SPONT or BiLevel modes...
						isEnabled = FALSE;
						break;
					default :
						// unexpected mode value...
						AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
						break;
				}
			}
			else
			{  // $[TI3.2] -- VC+ option is not enabled...
				isEnabled = FALSE;
			}
			break;

		default :
			// unexpected mandatory type value...
			AUX_CLASS_ASSERTION_FAILURE(value);
			break;
	};

	return(isEnabled);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  Calculate, and initialize to, the new discrete value.  The value of
//  'knobDelta' is only used in so far as the "direction" of change; the
//  value, itself, is ignored a delta of '1' is applied in the direction
//  of change.
//
//  This does some specific work for this class, then forwards on to base
//  class's implementation.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus& MandTypeSetting::calcNewValue(const Int16 knobDelta)
{
	CALL_TRACE("calcNewValue(knobDelta)");

	// forward to base class...
	return(DiscreteSetting::calcNewValue(knobDelta));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcTransition(toValue, fromValue)  [virtual]
//
//@ Interface-Description
//  The purpose of this method is to inform the settings effected
//  by a state transition of this setting. So they can adjust
//  their value(s) appropriately.
//
//  This method deals with state transitions where-as dependent settings
//  calculations typically deal with bounded settings and incremental
//  changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02020] -- settings to remain constant during a mandatory type change...
//---------------------------------------------------------------------
//@ PreCondition
//  (toValue == MandTypeValue::PCV_MAND_TYPE  ||
//   toValue == MandTypeValue::VCV_MAND_TYPE  ||
//   toValue == MandTypeValue::VCP_MAND_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void MandTypeSetting::calcTransition(const DiscreteValue toValue,
									 const DiscreteValue fromValue)
{
	CALL_TRACE("calcTransition(toValue, fromValue)");

	//=====================================================================
	// activate the transition rules of low circuit pressure limit...
	// $[VC02006] - transition to VC+
	// $[NI02005] - low circ pressure active in NIV and VC+ only
	//=====================================================================
	SettingsMgr::GetSettingPtr(SettingId::LOW_CCT_PRESS)->
		acceptTransition(getId(), toValue, fromValue);

	// $[LC02009]
	SettingsMgr::GetSettingPtr(SettingId::LEAK_COMP_ENABLED)->
		acceptTransition(getId(), toValue, fromValue);
	SettingsMgr::GetSettingPtr(SettingId::DISCO_SENS)->
		acceptTransition(getId(), toValue, fromValue);

    const Setting* P_PATIENT_CCT_TYPE = 
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		P_PATIENT_CCT_TYPE->getAdjustedValue();

	switch ( toValue )
	{
		//=====================================================================
		//  $[02021] -- transition to PC...
		//=====================================================================
		case MandTypeValue::PCV_MAND_TYPE :		  // $[TI1]
			SettingsMgr::GetSettingPtr(SettingId::INSP_PRESS)->
				acceptTransition(getId(), toValue, fromValue);

			//-----------------------------------------------------------------
			// activate the transition rule of inspiratory time; this is NOT
			// a formal requirement, but an implementation needed to resolve
			// the differences between inspiratory time's VCV resolution (20ms)
			// and PCV resolution (10ms)...
			//-----------------------------------------------------------------

			SettingsMgr::GetSettingPtr(SettingId::INSP_TIME)->
				acceptTransition(getId(), toValue, fromValue);
			break;

			//=====================================================================
			//  $[02022]   -- transition to VC...
			//  $[VC02006] -- transition to VC+...
			//=====================================================================
		case MandTypeValue::VCV_MAND_TYPE :
		case MandTypeValue::VCP_MAND_TYPE :		  // $[TI2]
			{
				//-----------------------------------------------------------------
				// activate the transition rules of plateau time...
				//-----------------------------------------------------------------

				SettingsMgr::GetSettingPtr(SettingId::PLATEAU_TIME)->
					acceptTransition(getId(), toValue, fromValue);

				//-----------------------------------------------------------------
				// activate the transition rules of flow pattern; this must be done
				// AFTER plateau time is activated...
				//-----------------------------------------------------------------

				SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN)->
					acceptTransition(getId(), toValue, fromValue);

				//-----------------------------------------------------------------
				// test the transition state of peak insp flow; this must be done
				// AFTER plateau time and flow pattern are activated...
				//-----------------------------------------------------------------

				// store the peak flow's transition state that results from its
				// transition; if 'STAND_ALONE' is returned, then peak flow is
				// already done with its transition...
				const Setting::TransitionState  XITION_STATE =
					SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW)->
					testTransition(getId(), toValue, fromValue);

				if ( fromValue    == MandTypeValue::PCV_MAND_TYPE  ||
					 XITION_STATE == Setting::CONDITIONAL )
				{  // $[TI2.1] -- need to activate circuit-based rules...
					//=============================================================
					//  $[02022]\c\,\d\,\i\   -- activate Vt and Vmax, if from-PC
					//                           or needed...
					//  $[VC02006]\d\i,\g\,\h\ -- activate Vt and Vmax, if from-PC
					//                            or needed...
					//=============================================================

					//-----------------------------------------------------------------
					// activate the transition rules of tidal volume; this must be done
					// AFTER plateau time and flow pattern are activated...
					// NOTE: Override fromValue, use PCV to force circuit based rules
					// to execute in the CONDITIONAL case
					//-----------------------------------------------------------------

					SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME)->
						acceptTransition(getId(), toValue, MandTypeValue::PCV_MAND_TYPE);

					//-----------------------------------------------------------------
					// activate the transition rule of peak inspiratory flow; this must
					// be done AFTER flow pattern and tidal volume are activated...
					//-----------------------------------------------------------------

					SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW)->
						acceptTransition(getId(), toValue, fromValue);

					//-----------------------------------------------------------------
					// activate the transition rule of inspiratory time; this must
					// be done AFTER all of the VC settings are activated...
					//-----------------------------------------------------------------

					SettingsMgr::GetSettingPtr(SettingId::INSP_TIME)->
						acceptTransition(getId(), toValue, fromValue);

					//-----------------------------------------------------------------
					// Go back and ensure the tidal volume value is still within a valid
					// range. At low IBW, sometimes inspiratory time can compute to below
					// 0.2 seconds then warped back to the minimum of 0.2. This adjustment
					// requires that tidal volume be re-computed to ensure its within range.
					//-----------------------------------------------------------------
					if (toValue == MandTypeValue::VCV_MAND_TYPE)
					{
						TidalVolumeSetting* tidalVolSet = (TidalVolumeSetting*)SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
						tidalVolSet->correctForMinimumTi();
					}

					switch ( toValue )
					{
						case MandTypeValue::VCV_MAND_TYPE :	// $[TI2.1.1]
							// do nothing; leave the calculations where inspiratory time is based
							// on the values of tidal volume and peak insp flow...
							break;
						case MandTypeValue::VCP_MAND_TYPE :	// $[TI2.1.2]
							// re-run 'testTransition()' to, based on the resulting tidal volume
							// and inspiratory time from above, ensure that the peak insp flow
							// has a value that is calculated based on Vt and Ti, and not just
							// the circuit-based formulas...
							SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW)->
								testTransition(getId(), toValue, fromValue);
							break;
						case MandTypeValue::PCV_MAND_TYPE :
						default :
							AUX_CLASS_ASSERTION_FAILURE(toValue);
							break;
					}
				}
				else
				{  
					// set the forced-change flag of the two settings...
					SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME)->
						setForcedChangeFlag();

					switch ( toValue )
					{
						case MandTypeValue::VCV_MAND_TYPE :	// $[TI2.2.1]
							// $[02251] from 'VC+' to 'VC': check the tidal volume and peak flow 
							// 	bounds if cct type is NEONATAL and NeoMode Advanced is enabled  
							if( SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_ADVANCED) &&
								PATIENT_CCT_TYPE_VALUE == PatientCctTypeValue::NEONATAL_CIRCUIT )
							{
								SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME)->
									acceptTransition(getId(), toValue, fromValue);
								SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW)->
									acceptTransition(getId(), toValue, fromValue);
							}
							// from 'VC+' to 'VC':  re-calculate where inspiratory time is based
							// on the values of tidal volume and peak insp flow, rather than
							// VC+'s way where peak insp flow is based on tidal volume and
							// inspiratory time...
							SettingsMgr::GetSettingPtr(SettingId::INSP_TIME)->
								acceptTransition(getId(), toValue, fromValue);	
							break;
						case MandTypeValue::VCP_MAND_TYPE :	// $[TI2.2.2]
							// $[TI2.2] -- leave tidal volume and inspiratory time unchanged...
							//=============================================================
							//  $[VC02006]\d\ii -- leave Vt and Ti unchanged (sort of)...
							//=============================================================
							// from 'VC' to 'VC+':  peak insp flow is already based on the
							// values of tidal volume and inspiratory time (from the
							// 'testTransition()' call above), therefore leave as is...
							SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME)->
								acceptTransition(getId(), toValue, fromValue);
							SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW)->
								acceptTransition(getId(), toValue, fromValue);
							break;
						case MandTypeValue::PCV_MAND_TYPE :
						default :
							AUX_CLASS_ASSERTION_FAILURE(toValue);
							break;
					}

				}

				//-----------------------------------------------------------------
				// finally, activate the transition rules of expiratory time and I:E
				// ratio; these must be done AFTER inspiratory time has been
				// activated...
				//-----------------------------------------------------------------

				SettingsMgr::GetSettingPtr(SettingId::EXP_TIME)->
					acceptTransition(getId(), toValue, fromValue);
				SettingsMgr::GetSettingPtr(SettingId::IE_RATIO)->
					acceptTransition(getId(), toValue, fromValue);


				if ( toValue == MandTypeValue::VCP_MAND_TYPE )
				{  // $[TI2.3] -- transition "to VC+"...
					SettingsMgr::GetSettingPtr(SettingId::CONSTANT_PARM)->
						acceptTransition(getId(), toValue, fromValue);
					SettingsMgr::GetSettingPtr(SettingId::LOW_CCT_PRESS)->
						acceptTransition(getId(), toValue, fromValue);
				}  // $[TI2.4]
			}
			break;

		default :
			// unexpected "to" value...
			AUX_CLASS_ASSERTION_FAILURE(toValue);
			break;
	};
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Returns this setting's new-patient (default) value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a default setting value for mandatory type based on the 
//  current circuit type and mode.
//  $[02191] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue MandTypeSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	DiscreteValue  newPatientValue = MandTypeValue::PCV_MAND_TYPE;

	const DiscreteValue  PATIENT_CCT_TYPE =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAdjustedValue();

	const DiscreteValue MODE = 
		SettingsMgr::GetSettingPtr(SettingId::MODE)->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			newPatientValue = MandTypeValue::PCV_MAND_TYPE;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2]
			if (MODE == ModeValue::BILEVEL_MODE_VALUE)
			{
				newPatientValue = MandTypeValue::PCV_MAND_TYPE;
			}
			else
			{
				newPatientValue = MandTypeValue::VCV_MAND_TYPE;
			}
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
			break;
	}

	return(newPatientValue);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  resetState()  [virtual]
//
//@ Interface-Description
//  Reset the state of this setting, preparing it for changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void MandTypeSetting::resetState(void)
{
	CALL_TRACE("resetState()");

	// forward up to base class...
	Setting::resetState();

	operatorSelectedValue_ = getAcceptedValue();
}


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to mode and IBW value changes, by, possibly, updating mand type's
//  applicability and value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02025]\g\   -- from AC to BiLevel, mand type fixed at 'PC'...
//  $[02025]\h\   -- from AC/VC+ to SPONT, mand type changed to 'VC'/'PC'...
//  $[TC02000]\d\ -- from SIMV to BiLevel, mand type fixed at 'PC'...
//  $[TC02000]\e\ -- from SIMV/VC+ to SPONT, mand type changed to 'VC'/'PC'...
//  $[02026]\g\   -- from SPONT to BiLevel, mand type fixed at 'PC'...
//  $[NI02010]\b\ -- from Invasive/VC+ to NIV, mand type seto the VC
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void MandTypeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
								  const SettingSubject*               pSubject)
{
	if ( qualifierId == Notification::ADJUSTED )
	{
		const SettingId::SettingIdType SETTING_ID = pSubject->getId();
		if ( SETTING_ID == SettingId::MODE || SETTING_ID == SettingId::VENT_TYPE )
		{
			const DiscreteValue CURRENT_VALUE = getAdjustedValue();

			isChangeForced_ = TRUE;
			// attempt to revert to the operator selection first if enabled
			if ( operatorSelectedValue_ != NO_SELECTION_ && isEnabledValue(operatorSelectedValue_) )
			{
				if ( CURRENT_VALUE != operatorSelectedValue_ )
				{
					setAdjustedValue(operatorSelectedValue_);
				}
			}
			else
			{
				setAdjustedValue(getNewPatientValue());
			}
			// else - no change required
			isChangeForced_ = FALSE;

			updateApplicability(qualifierId);
		}
		else if ( SETTING_ID == SettingId::IBW )
		{
			updateApplicability(qualifierId);
		}
		else
		{
			AUX_CLASS_ASSERTION_FAILURE(SETTING_ID);
		}
	}  // -- don't care about accepted value changes...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean MandTypeSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of Mode and IBW, therefore
//  this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void MandTypeSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	// monitor changes to these settings...
	attachToSubject_(SettingId::MODE, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::VENT_TYPE, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::IBW, Notification::VALUE_CHANGED);
}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void MandTypeSetting::SoftFault(const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
								const char*        pFileName,
								const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							MAND_TYPE_SETTING, lineNumber, pFileName,
							pPredicate);
}
