#include "stdafx.h"

#if        defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BoundedDataInfo -  Breath Data Processing agent class 
//              for the bounded breath data item types.
//---------------------------------------------------------------------
//@ Interface-Description
//
//>Von
//  Description:
//  ------------
//  The 'BoundedDataInfo' class processes bounded Patient data types.
//  A 'BounedDataInfo' class object  maintains upper and lower boundry
//  values, data precision information for a specific bounded breath
//  item type.  A new data value is clipped to fit within upper and lower
//  boundry values of the data type.
//  This object buffers the latest data within its double buffer
//  to maintain data integrity between a reader and a writer of the data.
//
//  Calss Key Methods:
//  -----------------
//     modifyData_  -- the method clips the raw breath data value, 
//             determines which precision to use for a data value,
//             and buffers the data. The method also sends out a Breath
//             Item change notification.
//@ Rationale
//     The purpose of this class is to put all the procssing
//     smarts of a bounded breath item type into one object to
//     avoid these smarts spreading all over the patient data manager.
//       
//---------------------------------------------------------------------
//@ Implementation-Description
//     The class is a subclass of the "BreathItemDataInfo" class.
//     The parent class handles most of the double buffer indexing,
//     provides data access methods to outside and data update notice.
//     This class  maintains minimum and maximum allowed value range,
//     upper and lower precisions that can be used for a data value.
//     The PreciseValue routine computes a data value which fits to
//     a given precision, this computation process is called warping.
//     This method saves a warped data value in its double buffer.
//---------------------------------------------------------------------
//@ Fault-Handling
//    Follows the standard Sigma contract programming model, Class
//    precodition and Class assertion are used to raise a fatal software
//    fault.
//>Voff
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/BoundedDataInfo.ccv   25.0.4.0   19 Nov 2013 14:17:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc      Date:  16-May-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project related changes.
//
//  Revision: 005  By: sah   Date:  09-FEB-99    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added requirement numbers for ATC.
//
//  Revision: 004  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  gdc    Date:  26-MAR-1997 DR Number: 5059
//  Project:  Color
//  Description:
//      Fixed precision adjustment in modifyData_() to use IsEquivalent.
//      Fixed precision adjustment in processData_() to use IsEquivalent.
//
//  Revision: 002  By:  hct    Date:  28-APR-1997 DR Number: 1996
//  Project:  Sigma (R8027)
//  Description:
//      Added software requirement numbers 03025, 03026, 03036, 03037.
//
//  Revision: 001  By:  jhv    Date:  16-Feb-95   DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//      Initial version (Integration baseline).
//
//=====================================================================

#include "BoundedDataInfo.hh"
#include "MathUtilities.hh"
#include "PatientDataId.hh"
#include "PatientDataMgr.hh"

static const Uint32 MANTISSA_ERROR    = 0x7f800000;
static const Uint32 POSITIVE_INFINITY = 0x7f800000;
static const Uint32 NEGATIVE_INFINITY = 0xff800000;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BoundedDataInfo( const PatientDataId::PatientItemId id,
//    const Real32 min, const Real32 mid,const Real32 max,
//    const Real32 changePoint, const Precision lowerP, const Precision higherP)
//    Constructor
//
//@ Interface-Description
//    Initializes  all the private and inherited member variables.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

BoundedDataInfo::BoundedDataInfo(
    const PatientDataId::PatientItemId id, const Real32 min,
	const Real32 max, const Real32 changePoint, const Precision lowerP,
    const Precision higherP)
        :BreathItemDataInfo(id), MIN_VALUE_(min),MAX_VALUE_(max),
     MID_CHANGE_POINT_(changePoint), LOWER_PRECISION_(lowerP),
     UPPER_PRECISION_(higherP)
{
  CALL_TRACE("BoundedDataInfo()");
  SAFE_CLASS_PRE_CONDITION( PatientDataMgr::IsBoundedId(id));
  // $[TI1]
  data_[index_.active].bValue.timedOut = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BoundedDataInfo()  [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

BoundedDataInfo::~BoundedDataInfo(void)
{
  CALL_TRACE("~BoundedDataInfo()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: modifyData_(void *pValue) [virtual]
//
//@ Interface-Description
//     Processes a raw breath data value and maintains the modified data
//     in its buffer. Each data processing agent maintains upper and lower
//     boundry values. The method clips a raw value to a boundary limit 
//     value if a data value is outside of these two boundries.
//     It chooses a precision to use based on the data value.
//     Initially, modified data is always considered fresh( not timed out).
//     The method informs other subsystems of a new data change.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The modifyData_ method is called whenever a  new breath data 
//      is received from the Breath Delivery System.
//      Processed data is put into a network buffer to avoid over writing
//      an active data that might have been in the middle of being
//      accessed by another task before the NetworkApp task interrupted.
//
//      The parent class's BreathItemDataInfo::modifyData_ is called
//      to send data change notification and make the newly received data
//      data to be an active one.
//      A precision choice is made once more after a data is warped
//      since warping a number using a precision can round up a number
//      to a next precision level.
//
//     $[03001], $[03013], $[03014], $[03016], $[03017],
//     $[03020], $[03021], $[03028], $[03029], $[03032], $[03033],
//     $[03039], $[03040], $[03048], $[03049], $[03059], $[03060],
//     $[03063], $[03064], $[XXX001], $[XXX002], $[YYY001], $[YYY002]
//     $[03025], $[03026], $[03036], $[03037]
//     $[TC03001], $[TC03002]
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
BoundedDataInfo::modifyData_( void *pValue)
{
Uint32 rawData;
Real32 rawValue;
Real32 warpValue;

  SAFE_CLASS_PRE_CONDITION(pValue);

  rawData  = *((Uint32 *) pValue);
  rawValue = *((Real32 *) pValue);
  Precision precision;

  if ((rawData & MANTISSA_ERROR) == MANTISSA_ERROR)
  {
	  if (rawData == POSITIVE_INFINITY)
	  {
		  rawValue = MAX_VALUE_ + 1.0;
	  }
	  else if (rawData == NEGATIVE_INFINITY)
	  {
		  rawValue = MIN_VALUE_ - 1.0;
	  }
	  else
	  {
		  // NAN
		  rawValue = MIN_VALUE_ - 1.0;
	  }
  }

  if ( rawValue < MIN_VALUE_ )
  {                                // $[TI1]
    data_[index_.network].bValue.data.value = MIN_VALUE_;   
    data_[index_.network].bValue.data.precision = LOWER_PRECISION_;
    data_[index_.network].bValue.clippingState = BoundedValue::ABSOLUTE_CLIP;
  }                                                        
  else if ( rawValue > MAX_VALUE_ )
  {                                // $[TI2]
    data_[index_.network].bValue.data.value =  MAX_VALUE_;
    data_[index_.network].bValue.data.precision = UPPER_PRECISION_;
    data_[index_.network].bValue.clippingState = BoundedValue::ABSOLUTE_CLIP;
  }
  else  // value is within given boundaries                
  {    // $[TI3]
        if (rawValue < MID_CHANGE_POINT_)
        {                    //    $[TI3.1]
            precision = LOWER_PRECISION_;
        }
        else
        {    //    $[TI3.2]
            precision = UPPER_PRECISION_;
        }

        warpValue=::PreciseValue(rawValue, precision);

        // need to check precision again since the Warping might have pushed 
        // the number to next range.
        if (warpValue >= MID_CHANGE_POINT_  ||
            ::IsEquivalent(warpValue, MID_CHANGE_POINT_, LOWER_PRECISION_))
        {    //    $[TI3.5]
            precision    = UPPER_PRECISION_;
			warpValue = ::PreciseValue(rawValue,precision);
        }//else $[TI3.6]

        data_[index_.network].bValue.data.value = warpValue;   
        data_[index_.network].bValue.data.precision = precision;
		data_[index_.network].bValue.clippingState = BoundedValue::UNCLIPPED;
   }
   data_[index_.network].bValue.timedOut = FALSE;    
   BreathItemDataInfo::modifyData_(pValue);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processData_(const Real32 rawValue) 
//
//@ Interface-Description
//      The method processes a given breath item value according to
//      this object's processing specification and returns the 
//      processed value.
//    The method clips an out of boundary value to a boundary limit
//    value and  warps the raw value to a nearest warp value based
//    on the precision that the raw value supposed to use.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This object maintains minimum and maximum allowed value range,
//   upper and lower precisions that can be used for a data value.
//   The PreciseValue routine warps the data value based on the
//   precision passed.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Real32 
BoundedDataInfo::processData_( Real32 rawValue )
{
Uint32 rawData = *(Uint32*)(&rawValue);
Precision precision;
Real32 warpValue;
  
	if ((rawData & MANTISSA_ERROR) == MANTISSA_ERROR)
	{
		if (rawData == POSITIVE_INFINITY)
		{
			rawValue = MAX_VALUE_ + 1.0;
		}
		else if (rawData == NEGATIVE_INFINITY)
		{
			rawValue = MIN_VALUE_ - 1.0;
		}
		else
		{
			// NAN
			rawValue = MIN_VALUE_ - 1.0;
		}
	}

	if ( rawValue < MIN_VALUE_ )
	{                                // $[TI1]
        rawValue =  MIN_VALUE_;
        precision = LOWER_PRECISION_;
	}                                                        
	else if ( rawValue > MAX_VALUE_ )
	{                                // $[TI2]
        precision = UPPER_PRECISION_;
        rawValue =  MAX_VALUE_ ;
	}
	else  // value is within given boundaries                
	{    // $[TI3]
        if    (rawValue < MID_CHANGE_POINT_)
        {                    //    $[TI3.1]
            precision    = LOWER_PRECISION_;
        }
        else
        {				    //    $[TI3.2]
            precision    = UPPER_PRECISION_;
        }
        warpValue=::PreciseValue(rawValue, precision);

        // need to check precision again since the Warping might have pushed 
        // the number to next range.
        if    (warpValue >= MID_CHANGE_POINT_
			|| ::IsEquivalent(warpValue, MID_CHANGE_POINT_, LOWER_PRECISION_) )
        {				    //    $[TI3.5]
            precision    = UPPER_PRECISION_;
			warpValue = ::PreciseValue(rawValue,precision);
        }					//else $[TI3.6]

        rawValue = warpValue;   
   }

     return(rawValue);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  setDataTimeout(Boolean SendNotice) 
//
//@ Interface-Description
//      Time out this data and send out a data change notice
//      to other interested subsystems.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Set the timeout flag of the active data and call the
//      'PatientDataMgr::MakeItemCalls()' to call pre-registered 
//      callback routines.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none 
//@ End-Method
//=====================================================================

void
BoundedDataInfo::setDataTimeout()
{
  CALL_TRACE("setDataTimeout()");

  data_[index_.active].bValue.timedOut = TRUE;
  PatientDataMgr::MakeItemCalls( DATA_ID_ );  
  //$[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BoundedDataInfo::SoftFault(const SoftFaultID  softFaultID,
						   const Uint32       lineNumber,
						   const char*        pFileName,
						   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, PATIENT_DATA,
  BOUNDED_DATA_INFO, lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

#endif        //defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
