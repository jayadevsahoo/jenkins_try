#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: WaveformSet - Set of waveform records.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class manages a set of WaveformRecord objects. Methods are provided
//  for the following functionality :
//  - Add a WaveformRecord to the set,
//  - retrieve the WaveformRecord added,
//  - check if the set is full,
//  - reset all data members.
//---------------------------------------------------------------------
//@ Rationale
//	This class manages a collection of WaveformRecords.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The set of WaveformRecord objects is implemented by using an array of
//	WaveformRecords. The data member numRecords_ keeps track of the 
//	number of WaveformRecord objects that have been added. The data member
//	currentRecord_ is used by the method getNextWaveformRecord to 
//	traverse the array data member waveformRecord_.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//	none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/WaveformSet.ccv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  17-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "WaveformSet.hh"

//@ Usage-Classes
//@ End-Usage


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaveformSet()  
//
//@ Interface-Description
//	Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformSet::WaveformSet(void)
{
  CALL_TRACE("WaveformSet()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~WaveformSet()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformSet::~WaveformSet(void)
{
  CALL_TRACE("~WaveformSet()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
WaveformSet::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, WAVEFORMSET,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)





//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//=====================================================================
//
//  Private Methods...
//
//=====================================================================

