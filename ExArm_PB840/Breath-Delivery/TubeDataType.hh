
#ifndef TubeDataType_HH
#define TubeDataType_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TubeData - the base class for all possible breath phases.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/TubeDataType.hhv   25.0.4.0   19 Nov 2013 14:00:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  yyy    Date:  07-Jan-1999    DR Number: DCS 5322
//       Project:  ATC (R8027)
//       Description:
//             Initial version
//
//=====================================================================


struct TubeDataType
{
    Real32 totalIntercept;
    Real32 totalSlope;
};

#endif // TubeDataType_HH 
