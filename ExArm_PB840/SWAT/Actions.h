//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#ifndef ACTIONS_H
#define ACTIONS_H

#include "TimeStamp.h"

namespace swat
{

    class BatchExecuter;

    //-----------------------------------------------------------------
    /// COAction
    ///
    /// Parent class for all coroutines.
    ///
    /// To add a new coroutine, all children classes must,
    ///     - be included in the union in the class named Context in Context.h
    ///     - be added as a friend class to BatchExecuter in BatchExecuter.h
    ///     - if called from batch script, add an enumeration to BatchCommand in BatchCommand.h.
    ///     - if called from batch script, add to DispatchContext::doAction the demultiplexing of
    ///       the batch command.
    //-----------------------------------------------------------------
    class COAction
    {
    };

    //-----------------------------------------------------------------
    /// WaitState
    ///
    /// A coroutine that defines a wait state
    //-----------------------------------------------------------------
    class WaitState: private COAction
    {
        private:
            friend class WaitEvent;
            friend class WaitTime;

            TimeStamp m_WaitTime;
            int m_iEventId;
            unsigned int m_iEventStatus;
            bool m_bEventHasTimeout;

    };

    //-----------------------------------------------------------------
    /// WaitEvent
    ///
    /// A coroutine that waits for an event to occur.
    //-----------------------------------------------------------------
    class WaitEvent: private WaitState
    {

        private:

            //--------------------------------------------------------------------
            /// waitevent_continue
            ///
            /// Completion routine for WaitEvent.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_WAIT, if there is no event or timeout that has occured,
            ///             - BatchExecuter::STATE_CORETURN_TIMEOUT, if timeout defined by m_iWaitTime that has occured.
            ///             - BatchExecuter::STATE_CORETURN_CONTINUE, If event defined by m_iEventId that has occured.
            //--------------------------------------------------------------------
            static int waitevent_continue(BatchExecuter *pCMgr);

        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Start routine for WaitEvent.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_WAIT
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// WaitTime
    ///
    /// A coroutine that waits for a period of time.
    //-----------------------------------------------------------------
    class WaitTime: private WaitState
    {
        private:

            //--------------------------------------------------------------------
            /// waittime_continue
            ///
            /// Completion routine for WaitTime.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            //--------------------------------------------------------------------
            static int waittime_continue(BatchExecuter *pCMgr);

        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Start routine for WaitTime.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_WAIT
            ///             - BatchExecuter::STATE_CONTINUE, if the time passed in is zero.
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// UnsetSensor
    ///
    /// A coroutine that unsets a sensor.
    //-----------------------------------------------------------------
    class UnsetSensor: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Unset a Sensor.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };


    //-----------------------------------------------------------------
    /// UnsetRawSensor
    ///
    /// A coroutine that unsets a sensor.
    //-----------------------------------------------------------------
    class UnsetRawSensor: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Unset a Sensor.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };


    //-----------------------------------------------------------------
    /// SetSensor
    ///
    /// A coroutine that sets a sensors value.
    //-----------------------------------------------------------------
    class SetSensor: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Set a Sensor.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };


    //-----------------------------------------------------------------
    /// SetRawSensor
    ///
    /// A coroutine that sets a sensors value.
    //-----------------------------------------------------------------
    class SetRawSensor: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Set a Sensor.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };


    //-----------------------------------------------------------------
    /// AddSensor
    ///
    /// A coroutine that sets a sensors value.
    //-----------------------------------------------------------------
    class AddSensor: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Add value to sensor.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };


    //-----------------------------------------------------------------
    /// AddRawSensor
    ///
    /// A coroutine that sets a sensors value.
    //-----------------------------------------------------------------
    class AddRawSensor: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Add value to sensor.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

	//-----------------------------------------------------------------
    /// SimulateGuiTouch
    ///
    /// A coroutine that simulate a touch on GUI
    //-----------------------------------------------------------------
    class SimulateGuiTouch: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Simulate a GUI touch on a location
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

	//-----------------------------------------------------------------
    /// SimulateGuiBezelKey
    ///
    /// A coroutine that simulate bezel key press on GUI
    //-----------------------------------------------------------------
    class SimulateGuiBezelKey: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Simulate a bezel key press
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// ResetAll
    ///
    /// A coroutine that unsets a sensor.
    //-----------------------------------------------------------------
    class ResetAll: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Reset all batch.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_RESETALL
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// SignalEvent
    ///
    /// A coroutine that unsets a sensor.
    //-----------------------------------------------------------------
    class SignalEvent: private COAction
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Signal an event.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// MarkState
    ///
    /// Parent class that is common to all coroutines that involve repeats and mark.
    //-----------------------------------------------------------------
    class MarkState: private COAction
    {

        private:
            friend class SkipBufferIndex;
            friend class SetBufferIndex;
            friend class SetSensorBufferValue;
            friend class Mark;
            friend class RepeatEvent;
            friend class RepeatTime;
            friend class RepeatN;
            friend class MarkEvent;

            TimeStamp m_WaitTime;
            unsigned int i_EventState;
            unsigned int m_iMark;
            unsigned int iMarkCount;
            bool bEventCaptured;
            unsigned int m_buffer_index;
    };

    //-----------------------------------------------------------------
    /// MarkEvent
    ///
    /// A coroutine to capture a event state.
    //-----------------------------------------------------------------
    class MarkEvent: private MarkState
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Capture event state.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// RepeatN
    ///
    /// A coroutine that repeats the sequence.
    //-----------------------------------------------------------------
    class RepeatN: private MarkState
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Repeat N times.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// RepeatTime
    ///
    /// A coroutine that repeats the sequence for a given time.
    //-----------------------------------------------------------------
    class RepeatTime: private MarkState
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Repeat for a period of time.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// RepeatEvent
    ///
    /// A coroutine that repeats a sequence until an event occurs or for a given amount of time.
    //-----------------------------------------------------------------
    class RepeatEvent: private MarkState
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Repeat sequence for a period of time or until event happens.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// Mark
    ///
    /// A coroutine that marks
    //-----------------------------------------------------------------
    class Mark: private MarkState
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Mark
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// BufferedSensor
    //-----------------------------------------------------------------
    class BufferedSensor: protected MarkState
    {
    };

    //-----------------------------------------------------------------
    /// SetSensorBufferValue
    //-----------------------------------------------------------------
    class SetSensorBufferValue: private BufferedSensor
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// SetSensorBufferValue
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// SetBufferIndex
    //-----------------------------------------------------------------
    class SetBufferIndex: private BufferedSensor
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// SetBufferIndex
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    /// @class SkipBufferIndex
    ///
    /// @brief
    //-----------------------------------------------------------------
    /// SkipBufferIndex
    //-----------------------------------------------------------------
    class SkipBufferIndex: private BufferedSensor
    {
        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// SkipBufferIndex
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

    //-----------------------------------------------------------------
    /// DispatchContext
    ///
    /// A coroutine that executes batch commands.
    ///
    /// This class implments an interpreter coroutine that dispatches commands to other COActions.
    //-----------------------------------------------------------------
    class DispatchContext: private COAction
    {
        private:

            //--------------------------------------------------------------------
            /// continueAction
            ///
            /// Dispatch coroutine for processing of the batch script.
            ///
            /// As long as the following conditions are met, continue to process
            /// batch commands:
            ///     - There are batch commands to run.
            ///     - The returned result from each action is BatchExecuter::STATE_CONTINUE.
            ///     - The batch commands are valid.
            ///     - The batch command BatchCommand::CMD_END has not been received.
            /// When the command BatchCommand::CMD_END is processed or there are no more commands to process,
            /// BatchExecuter::STATE_END is returned.
            /// When the command is not valid, BatchExecuter::STATE_INVALID_BATCH_COMMAND is returned.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_END
            ///             - BatchExecuter::STATE_CONTINUE, indicating that there are more batch commands to execute in cycle.
            ///             - BatchExecuter::STATE_WAIT, indicating that batch processing should not continue in cycle.
            ///             - BatchExecuter::STATE_CORETURN_TIMEOUT, indicating that a timeout condition occurred and that batch should stop.
            ///             - BatchExecuter::STATE_CORETURN_CONTINUE, indicating that coroutine is complete.
            //--------------------------------------------------------------------
            static int continueAction(BatchExecuter *pCMgr);

            friend class Mark;
            friend class MarkEvent;
            friend class BatchExecuter;

        public:

            //--------------------------------------------------------------------
            /// doAction
            ///
            /// Startup coroutine for processing the batch script.
            ///
            /// @param[in] pCMgr A pointer to the current context.
            /// @return Completion state:
            ///             - BatchExecuter::STATE_CONTINUE, indicating that there are more batch commands to execute in cycle.
            //--------------------------------------------------------------------
            inline static int doAction(BatchExecuter *pCMgr);
    };

}
#endif
