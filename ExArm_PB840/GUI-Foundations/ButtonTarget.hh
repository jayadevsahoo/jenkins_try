#ifndef ButtonTarget_HH
#define ButtonTarget_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: ButtonTarget - This is an abstract class that allows callbacks
// from Button objects.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/ButtonTarget.hhv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date: 21-Sep-1998     DCS Number: 5098
//  Project:  Color
//  Description:
//      Added new method for supporting new bi-state buttons.
//
//  Revision: 003  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

//@ Usage-Classes
class Button;
//@ End-Usage

class ButtonTarget 
{
public:
	virtual void buttonDownHappened(Button *pButton,
									Boolean isByOperatorAction);
									
	virtual void buttonUpHappened(Button *pButton,
									Boolean isByOperatorAction);
									
protected:
	ButtonTarget(void);
	virtual ~ButtonTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	ButtonTarget(const ButtonTarget&);		// not implemented...
	void operator=(const ButtonTarget&);	// not implemented...
};

#endif // ButtonTarget_HH 
