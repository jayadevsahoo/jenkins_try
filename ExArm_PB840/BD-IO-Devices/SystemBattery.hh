#ifndef SystemBattery_HH
#define SystemBattery_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: SystemBattery - implements the functionality required for 
//                        interfacing with the BPS 
//-------------------------------------------------------------
//@ Version-Information
//@ (#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/SystemBattery.hhv   10.7   08/17/07 09:32:08   pvcs  
//
//@ Modification-Log
//
//  Revision: 008  By:  iv    Date:   03-May-1999    DR Number: DCS 5376
//       Project:  ATC
//       Description:
//			Added enum for BPS events.
//
//  Revision: 007  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 006  By:  iv    Date:   18-Nov-1997    DR Number: DCS 2610
//       Project:  Sigma (R8027)
//       Description:
//			Turn off BPS Ready LED when BPS quality is low. Updated the typedef
//          PBatteryAlarmCallBack to eliminate one argument.
//
//  Revision: 005  By: iv    Date:   22-Aug-1997    DR Number: DCS 2032, 1474
//      Project:  Sigma (840)
//        Description:
//              Completed changes per DRs - added a data member:
//                      Boolean notificationRequired_
//
//  Revision: 004  By: iv    Date:   08-Aug-1997    DR Number: DCS 2032
//      Project:  Sigma (840)
//        Description:
//              Update per new battery spec.
//
//  Revision: 003  By:  iv    Date:  09-May-1997    DR Number: DCS 2041, 2038
//       Project:  Sigma (840)
//       Description:
//             Fixed battery to correct alarms and GUI notifications
//
//  Revision: 002  By:  by    Date:  11-Nov-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Provide access for extern consts for service mode battery
//             test - per Service Mode code review.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
#include "LinearSensor.hh"
#include "BinaryIndicator.hh"
#include "BinaryCommand.hh"
#include "MiscSensorRefs.hh"


//@ End-Usage

extern const Real32 MAX_BPS_MODEL_1_VOLTAGE;
extern const Real32 MIN_BPS_MODEL_1_VOLTAGE;

extern const Real32 SYSBATT_VOLTSENSOR_VOLT_MIN;
extern const Real32 SYSBATT_VOLTSENSOR_VOLT_MAX;
extern const Real32 SYSBATT_VOLTSENSOR_ENG_MIN;
extern const Real32 SYSBATT_VOLTSENSOR_ENG_MAX;

class SystemBattery 
{
    public:

        enum BpsStatus { STARTUP,
                         NOT_INSTALLED,
                         CHARGED,
                         CHARGING,
                         ON_BATTERY_POWER,
                         BPS_FAULT };

        enum BpsQuality { FIRST_BPS_QUALITY,
                          NONE = FIRST_BPS_QUALITY,
                          INOPERATIVE,
                          LOW,
                          GOOD,
                          LAST_BPS_QUALITY = GOOD };

        enum BpsEvent { BAD_LED_EVENT = 1,  
                        ON_BATTERY_POWER_EVENT,
                        ON_BATTERY_POWER_EVENT_RESET,
                        OPEN_CIRCUIT_EVENT,
                        LESS_THAN_TWO_MINUTES_EVENT };

        //@ Type: PBatteryAlarmCallBack
        // Format for the callback method invoked when an
        // alarm is detected by the SystemBattery object
        typedef void (*PBatteryAlarmCallBack) ( const BpsQuality newBpsQuality,
                                                const Boolean initInProg );

        SystemBattery( void );
        ~SystemBattery( void );

        static void SoftFault( const SoftFaultID  softfaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName = NULL,
							   const char*        pPredicate = NULL);

        inline SystemBattery::BpsStatus getBpsStatus( void ) const;
        inline void registerAlarmCallBack(PAlarmCallBack pAlarmCallBack);
        inline void
        registerBatteryAlarmCallBack( PBatteryAlarmCallBack pAlarmCallBack );
        inline void
        registerVentStatusCallBack( PVentStatusCallBack pVentStatusCallBack );

        void initialize( void );
        void newCycle( void );
        void initialStatusNotification( void );
        Uint8 getStateOfCharge( void );

    protected:
        enum { VOLTAGE_BUFFER_SIZE = 30 };
    
    private:

        SystemBattery(const SystemBattery&);         // not implemented
        void operator=(const SystemBattery&);        // not implemented

        inline void setPowerModeOn_( void );

        inline Real32 calculateAvgVbatt_( void ) const;

        inline void resetVbatt_( void );
        inline void sampleVbatt_( void );

        void monitorStartupState_( void );
        void monitorChargingState_( void );
        void monitorChargedState_( void );
        void monitorOnBatteryPowerState_( void );
        void monitorBpsFaultState_( void );
        void monitorNotInstalledState_( void );

        Real32 getOldestVoltageInBuffer_( void );
        Real32 getLatestVoltageInBuffer_( void );
        Real32 getRecentVoltageChange_( void );
        void   saveVoltageToBuffer_( Real32 volt );

        Boolean transition1_( void );
        Boolean transition2_( void );
        Boolean transition3_( void );
        Boolean transition4_( void );
        Boolean transition5_( void );
        Boolean transition6_( void );
        Boolean transition7_( void );
        Boolean transition8_( void );
        Boolean transition9_( void );

        void resetAllTransCounters_( void );

        //@ Data-Member: bpsVbattAccumulator_
        //  sampled BPS voltage are stored in this accumulator 
        Real32 bpsVbattAccumulator_;
 
        //@ Data-Member: bpsVbattSampleCtr_ 
        //  counter for the number of BPS voltage samples currently
        //  stored in bpsVbattAccumulator_ 
        //  ( average VBATT = bpsVbattAccumulator_ / bpsVbattSampleCtr_ )
        Uint16 bpsVbattSampleCtr_;

        //@ Data-Member: currentBpsStatusTimer_ 
        //  indicates the elapsed time 
        //  since the start of current battery state 
        Uint32 currentBpsStatusTimer_;

        //@ Data-Member: bpsStatus_ 
        //  indicates the current status of the system battery
        BpsStatus bpsStatus_;

        //@ Data-Member: currentBpsQuality_ 
        //  indicates the current quality of the system battery
        BpsQuality currentBpsQuality_;

        //@ Data-Member: pBatteryAlarmCallBack_
        //  pointer to the function called when battery conditions that the
        //  Alarm-Analysis needs to know about are detected
        PBatteryAlarmCallBack pBatteryAlarmCallBack_;
                                 
        //@ Data-Member: pVentStatusCallBack_
        //  pointer to the function called when system battery conditions that
        //  VentAndUserEventStatus needs to know about are detected so that
        //  the GUI can be informed
        PVentStatusCallBack pVentStatusCallBack_;

        //@ Data-Member: pAlarmCallBack_
        //  pointer to the function called when AC power conditions that the
        //  Alarm-Analysis needs to know about are detected
        PAlarmCallBack pAlarmCallBack_;

        //@ Data-Member: transition_1_Counter_
        //  Counts transition requests for transition 1
        Int8 transition_1_Counter_;

        //@ Data-Member: transition_2_Counter_
        //  Counts transition requests for transition 2
        Int8 transition_2_Counter_;

        //@ Data-Member: transition_3_Counter_
        //  Counts transition requests for transition 3
        Int8 transition_3_Counter_;

        //@ Data-Member: transition_4_Counter_
        //  Counts transi3ion requests for transition 4
        Int8 transition_4_Counter_;

        //@ Data-Member: transition_5_Counter_
        //  Counts transition requests for transition 5
        Int8 transition_5_Counter_;

        //@ Data-Member: transition_6_Counter_
        //  Counts transition requests for transition 6
        Int8 transition_6_Counter_;

        //@ Data-Member: transition_7_Counter_
        //  Counts transition requests for transition 7
        Int8 transition_7_Counter_;

        //@ Data-Member: transition_8_Counter_
        //  Counts transition requests for transition 8
        Int8 transition_8_Counter_;

        //@ Data-Member: transition_9_Counter_
        //  Counts transition requests for transition 9
        Int8 transition_9_Counter_;

        //@ Data-Member: transition_1_
        //  Indicates transition 1 is active
        Boolean transition_1_;

        //@ Data-Member: transition_2_
        //  Indicates transition 2 is active
        Boolean transition_2_;

        //@ Data-Member: transition_3_
        //  Indicates transition 3 is active
        Boolean transition_3_;

        //@ Data-Member: transition_4_
        //  Indicates transition 4 is active
        Boolean transition_4_;

        //@ Data-Member: transition_5_
        //  Indicates transition 5 is active
        Boolean transition_5_;

        //@ Data-Member: transition_6_
        //  Indicates transition 6 is active
        Boolean transition_6_;

        //@ Data-Member: transition_7_
        //  Indicates transition 7 is active
        Boolean transition_7_;

        //@ Data-Member: transition_8_
        //  Indicates transition 8 is active
        Boolean transition_8_;

        //@ Data-Member: transition_9_
        //  Indicates transition 9 is active
        Boolean transition_9_;

        //@ Data-Member: newState_
        //  Allows retransmitals of alarms and user events when set to TRUE 
        Boolean newState_;

        //@ Data-Member: onBattery_
        //  indicates on battery state while status of BPS is FAULT.
        Boolean onBattery_;

        //@ Data-Member: previouslyOnBattery_
        //  indicates previous on battery state while status of BPS is FAULT.
        Boolean previouslyOnBattery_;

        //@ Data-Member: notificationRequired__
        //  indicates whether or not alarm and user notification is required.
        Boolean notificationRequired_;

        //@ Data-Member: voltageBuffer_
        //  an array to store the last recent one minute voltage samples
        Real32  voltageBuffer_[VOLTAGE_BUFFER_SIZE];

        //@ Data-Member: voltageBufferIndex_
        //  index for the start of the buffer
        Uint16  voltageBufferIndex_;
};

// Inlined methods...
#include "SystemBattery.in"

#endif  //SystemBattery_HH
