#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LinearRegression - class to support linear regression.
//---------------------------------------------------------------------
//@ Interface-Description
// 	Methods are provided to update the (x,y) data to be regressed, to
//	get the predicted y value based on the linear model, get the slope,
// 	and to reset the data.
//---------------------------------------------------------------------
//@ Rationale
//	To encapulate calculations for linear regression
//---------------------------------------------------------------------
//@ Implementation-Description
//	The data are stored in SummationBuffers and are filled via the updateValues()
//	method.  The slope and estimated y value are computed with the information
//	in the buffer.  This class does not manange the size of the data buffers,
//	which is why buffer pointers are used.
//	 $[BL04055]
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/LinearRegression.ccv   10.7   08/17/07 09:38:08   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  05-Mar-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "LinearRegression.hh"

//@ Usage-Classes

#include "MathUtilities.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LinearRegression()  
//
//@ Interface-Description
//	Default Constructor.  This method has pointers to the x, y, x^2,
//	and xy buffers and the buffer size as arguments.	
//---------------------------------------------------------------------
//@ Implementation-Description
//  The pointers passed in are used to initialized the SummationBuffer data
//	members.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

LinearRegression::LinearRegression( Real32 *pX, Real32 *pXy,
		Real32 *pY, Real32 *pX2, Uint32 bufferSize )
: x_( pX, bufferSize), xy_( pXy, bufferSize), y_( pY, bufferSize),
	x2_( pX2, bufferSize)
{
	CALL_TRACE("LinearRegression::LinearRegression( Real32 *pX, Real32 *pXy, \
				Real32 *pY, Real32 *pX2, Uint32 bufferSize )") ;
	
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LinearRegression()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

LinearRegression::~LinearRegression(void)
{
	CALL_TRACE("LinearRegression::~LinearRegression(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValues()
//
//@ Interface-Description
//	This method has the x and y data as arguments and returns nothing.  This
//	method is called to fill the buffers with the data to be regressed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The x, y, x^2, and xy buffers are filled with the corresponding
//	data.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LinearRegression::updateValues( const Real32 xValue, const Real32 yValue)
{
	CALL_TRACE("LinearRegression::updateValues( const Real32 xValue, \
				const Real32 yValue)") ;

	// $[TI1]

	x_.updateBuffer( xValue) ;
	xy_.updateBuffer( xValue * yValue) ;
	y_.updateBuffer( yValue) ;
	x2_.updateBuffer( xValue * xValue) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSlope()
//
//@ Interface-Description
//	This method has no arguments and returns the slope of the regressed
//	data.
//---------------------------------------------------------------------
//@ Implementation-Description
//	A check for divide by zero is made.  The method returns zero to avoid
//	the divide by zero, otherwise the slope is computed.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
LinearRegression::getSlope( void)
{
	CALL_TRACE("LinearRegression::getSlope( void)") ;

	Uint32 n = x_.getNumSamples() ;
	Real32 xySum= xy_.getSum() ;
	Real32 xBar = x_.getAverage() ;
	Real32 yBar = y_.getAverage() ;
	Real32 x2Sum = x2_.getSum() ;
	Real32 slope = 0.0 ;

	if (n > 1 && !IsEquivalent( x2Sum, n * xBar * xBar, THOUSANDTHS))
	{
		// $[TI1]
		slope = (xySum - n * xBar * yBar) / (x2Sum - n * xBar * xBar) ;
	}	// implied else $[TI2]
	
	return( slope) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reset()
//
//@ Interface-Description
//	This method has no arguments and returns nothing.  This method is called
//	to reinitialize all the regressed data to zero.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The reset() method of each buffer is zeroed.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LinearRegression::reset( void)
{
	CALL_TRACE("LinearRegression::reset( void)") ;

	x_.reset();
	xy_.reset();
	y_.reset();
	x2_.reset();	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getEstimateYValue()
//
//@ Interface-Description
//	This method has the x value as an argument and returns the estimated
// 	y value based on "best fit curve" at the x value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The y value is computed and returned.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
LinearRegression::getEstimateYValue( const Real32 xValue)
{
	// $[TI1]

	Real32 slope = getSlope() ;
	Real32 xBar = x_.getAverage() ;
	Real32 yBar = y_.getAverage() ;
	
	Real32 intercept = yBar - slope * xBar ;

	Real32 yValue = intercept + slope * xValue ;

	return( yValue) ;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
LinearRegression::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, LINEARREGRESSION,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================
