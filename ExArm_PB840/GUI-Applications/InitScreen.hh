#ifndef InitScreen_HH
#define InitScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: InitScreen - Screen displayed on the Upper Screen
// when initializing the GUI, before online or service mode has been
// entered.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/InitScreen.hhv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 	Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "TextField.hh"
//@ End-Usage

class InitScreen : public Container
{
public:
	InitScreen();
	~InitScreen(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	InitScreen(const InitScreen&);		// not implemented...
	void   operator=(const InitScreen&);	// not implemented...

	//@ Data-Member: initializeText_
	// The text which displays the message that the GUI is initializing.
	TextField initializeText_;

};


#endif // InitScreen_HH 
