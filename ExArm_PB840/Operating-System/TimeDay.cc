#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: TimeDay.cc - 840 Time of Day clock functionality
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the functions to set and read the system time
//  of day clock.  This module is target and device specific and as
//  such is non-portable.
//---------------------------------------------------------------------
//@ Module-Decomposition
//  SetTimeOfDay
//  ReadTimeOfDay
//---------------------------------------------------------------------
//@ Rationale
//  Isolate low-level target-specific time-of-day hardware from 
//  the application code.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The time of day device is the Dallas Semiconductor DS1286.  
//  Numbers are are presented as Binary-Coded Decimal (BCD).
//
//  The Dallas Semiconductor DS1286 base address is FFBE8040.  
//
//  Registers are:
//    +0    fractional second (bits7-4=0.1 second in BCD,
//          bits3-0=0.01 second in BCD)
//    +1    second (bit7=0, bits6-4=10 second in BCD,
//          bits3-0=second in BCD)
//    +2    minute (bit7=0, bits6-4=10 minute in BCD,
//          bits3-0=minute in BCD)
//    +4    hour (bit7=0, bit6=12/24 format (1=12 hour format,
//          bit5=AM/PM flag [if bit6==1] else 20 hour in BCD,
//          bit4=10 hour in BCD, bits3-0=hour in BCD)
//    +6    day  (bits7-3=0, bits2-0=date in BCD; where 1=Sunday)
//    +8    date (bits7-6=0, bits5-4=10 days in BCD, bits3-0=days in BCD)
//    +9    month (bit7=clock enable [iff 0], bit6=square wave enable [iff
//          0], bit5=0, bit4=10 months in BCD, bits3-0=months in
//          BCD)
//    +A    year (bits7-4=10 years in BCD, bits3-0=years in BCD)
//    +B    control (bit7=Transfer Enable [iff 1], other bits unused
//          by this code)
//---------------------------------------------------------------------
//@ Fault-Handling
//  Standard assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  n/a
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Operating-System/vcssrc/TimeDay.ccv   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005    By: Cederquist     Date: 27-Jan-2011  SCR Number: 6706
//       Added pragma option to compile zerovars into postzerovars
//       so POST can access these functions and use this modules static 
//       data. Corrected IsTimeOfDayOn() for Xena2 to assure clock is 
//       running and registers are updating.
//
//  Revision: 004    By: Cederquist     Date: 27-Jan-2011  SCR Number: 6671
//       Updated for new ST Micro M48T58 clock. Cleaned up functions
//       separating DS1286 functions from the new Xena2 TOD clock 
//       functions.
//
//  Revision: 003    By: Mitesh Raval   Date: 14-Oct-2010  SCR Number: 6671
//       Occurrence of DS1286 renamed to Delay.
//       New methods added for Xena2 version specific implementation.
//       Xena2 BD RTC related updates.
//
//  Revision: 002   By: sah    Date:  15-Oct-1997    DR Number: 1529
//  Project: Sigma (R8027)
//  Description:
//  Incorporated new module ID.
//
//   Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================

// allocate zerovars in postzerovars so POST can use these functions
#pragma option -NZpostzerovars

#include "TimeDay.hh"
#include "CpuDevice.hh"
#include "OsClassId.hh"
#include "DS1286.hh"
#include "STM48T58.hh"

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  ReadTimeOfDay
//
//@ Interface-Description
//  Reads the Time of Day clock into the specified argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the appropriate "read clock" function based on platform
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
void
ReadTimeOfDay(TimeOfDay * const time)
{
//if ported to Windows CE or Win32
#if defined(_WIN32_WCE) || defined(WIN32)
	SYSTEMTIME osTime;
	GetLocalTime( &osTime );

	//our structure uses 2 digits for year..
	//this should be good until year 9999. BASE_CENTURY needs update every century
	time->year		= osTime.wYear % 100;	
    time->month		= (Uint8)osTime.wMonth;
    time->date		= (Uint8)osTime.wDay;
	//Note: currently there is no use of DayOfWeek in the app level, and TimeStamp
	//struct does not even have a member for it.
    time->hour		= (Uint8)osTime.wHour;
	time->minute    = (Uint8)osTime.wMinute;
	time->second    = (Uint8)osTime.wSecond;
	time->milliseconds     = osTime.wMilliseconds;
#else
//TODO E600 VM: create a TimeOfDay constructor that zeros the members
//and a overload an assignment instead..
	time->year     = 00;
    time->month    = 1;
    time->date	   = 1;
    time->hour     = 0;
    time->minute   = 0;
    time->second   = 0;
	time->hundredth_second    = 0;

#endif //(_WIN32_WCE) || defined(WIN32)
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  SetTimeOfDay
//
//@ Interface-Description
//  This function sets the 840 Time of Day clock to the time and
//  date specified.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the appropriate "set clock" function based on platform
//---------------------------------------------------------------------
//@ PreCondition
//     (time->year     <= 99)
//  && (time->month    >= 1)
//  && (time->month    <= 12)
//  && (time->date     >= 1)
//  && (time->date     <= 31)
//  && (time->hour     <= 23)
//  && (time->minute   <= 59)
//  && (time->second   <= 59)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================
void
SetTimeOfDay(const TimeOfDay * const time)
{
    FREE_ASSERTION(   (time->year     <= MAX_EPOCH_YEAR)  
                      && (time->month    >= 1) 
                      && (time->month    <= 12)  
                      && (time->date     >= 1) 
                      && (time->date     <= 31)
                      && (time->hour     <= 23)  
                      && (time->minute   <= 59)  
                      && (time->second   <= 59) 
					  && (time->milliseconds <= 999), OS_PLATFORM, TIME_DAY_ID);
//if ported to Windows CE or Win32
#if defined(_WIN32_WCE) || defined(WIN32)
	SYSTEMTIME newTime;
	//Windows uses 4 digits for year, ours is 2 digits.. add century
	newTime.wYear = time->year + BASE_CENTURY;//BASE_CENTURY need update every century
	newTime.wMonth = time->month;
	newTime.wDay = time->date;
	//We are not using DayOfWeek in the higher level TimeStamp structure and apps,
	//and Windows ignores wDayOfWeek (see MSDN SetLocalTime) so ignore time->day but
	//initialize wDayOfWeek to something;
	newTime.wDayOfWeek = 0;
	newTime.wHour = time->hour;
	newTime.wMinute = time->minute;
	newTime.wSecond = time->second;
	newTime.wMilliseconds = time->milliseconds;


	// TODO E600 MS For now just disable daylight saving for 
	// whatever timezone it is currently set to.
	TIME_ZONE_INFORMATION  timeZoneInfo;
	ZeroMemory( &timeZoneInfo, sizeof(timeZoneInfo) );

	GetTimeZoneInformation( &timeZoneInfo );

	// Setting the month for standard and daylight saving will
	// disable the daylight saving feature.
	// For more information loot at the SetTimeZoneInformation in MSDN
	timeZoneInfo.DaylightDate.wMonth = 0;
	timeZoneInfo.StandardDate.wMonth = 0;

	SetTimeZoneInformation( &timeZoneInfo ) ;
	//SetLocalTime should return non-zero if successful
	FREE_ASSERTION(SetLocalTime(&newTime) != 0, OS_PLATFORM, (Uint32)GetLastError());

#endif //(_WIN32_WCE) || defined(WIN32)
}
