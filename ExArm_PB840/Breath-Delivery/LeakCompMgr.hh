#ifndef LeakCompMgr_HH
#define LeakCompMgr_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2008, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: LeakCompMgr - A central location to manage leak compensation
//                       related data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/LeakCompMgr.hhv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: rhj   Date:  25-Oct-2010    SCR Number: 6694  
//  Project:  PROX
//  Description:
//      Added getAverageNetFlowLeak() and getAverageExhPressure() and
//      removed prevActualPeep_ and sqrActualPeep_.
//                              
//  Revision: 007   By: rhj   Date:  05-May-2010    SCR Number: 6436  
//  Project:  PROX
//  Description:
//      Optimize leakcomp algorithms by minimizing the use of 
//      math api calls.
//
//  Revision: 006   By: rhj   Date:  12-May-2009    SCR Number:  6511 
//  Project:  840S2
//  Description:
//      isLeakCompEnabled_ is now being checked during a 
//      non-breathing mode and added isBreathingMode method to 
//      to this class.
// 
//  Revision: 005   By: rhj   Date:  13-Apr-2009    SCR Number:  6497 
//  Project:  840S2
//  Description:
//      Prevent leak compensation calculating every BD cycle during 
//      a non-breathing mode.
//
//  Revision: 004   By: rhj   Date:  13-Mar-2009    SCR Number: 6482
//  Project:  840S2
//  Description:
//      Added additional checks to the circuit disconnect algorithms
//      when leaks exceed the disconnect sensitivity.           
//
//  Revision: 003   By: rhj   Date:  12-Mar-2009    SCR Number: 6492 
//  Project:  840S2
//  Description:
//		Enhanced Leak Compensation algorithms to handle no leaks when 
//      exhalation leak rate is underestimated.
//
//  Revision: 002   By: rhj   Date:  19-Jan-2009    SCR Number: 6455 & 6452
//  Project:  840S
//  Description:
//		Enhanced Leak Compensation algorithms to handle no leaks and
//      added a limit to the disconnect sensitivity when a compressor
//      is active.
//
//  Revision: 001   By: rhj   Date:  18-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//		Initial version.
//
//=====================================================================

#include "DiscreteValue.hh"
#include "Resistance.hh"
#include "BreathPhaseType.hh"
#include "SummationBuffer.hh"

class LeakCompMgr 
{
public:

	enum LeakCompPhases
	{
		LEAK_COMP_STARTUP,				  // startup
		LEAK_COMP_INHALATION,			  // inhalation
		LEAK_COMP_EXHALATION
	};			  // exhalation
	enum
	{
		MAX_LEAK_COMP_DATA = 10
	};

    LeakCompMgr(Real32 lPerMinToMl);
    ~LeakCompMgr(void);


	void newInspiration(void);
	void newExhalation(Real32 exhTime);
	void setInspTime(Uint32 inspTime);
	Boolean isEnabled(void);
	void newCycle(void);
	Boolean isExhFlowFinished(void);
	void updateAvgExhPress(void);
	void updateAvgExhPress( Real32 peep);
	void update(void);
	Real32 computeCompliance(Real32 prevComputeCompliance);

	Real32 getPrevLeakVolume(void);

	Real32 getLeakFlow(void);
	Real32 getK1Gain(void);
	Real32 getK2Gain(void);
	Real32 getLeakCompPhases(void);
	Real32 getNetVolume(void);
	Real32 getAvgSteadyLeakRate(void);
	Boolean isSteadyLeak(void);

	Real32 getFlowSlope1(void);
	Real32 getPressureSlope1(void);

	Real32 getFilteredLeakFlow(void);

	void  setEffectivePressure(Real32 effectivePressure);
	void  setPressureTrajectory(Real32 pressureTrajectory);
	Real32 getPercentLeak(void);
	Real32 getMaxPreviousLeakVolume(void);
	void   resetLeakCompParms(void);
	Real32 getExhLeakRate(void);
	Real32 getFinalK1Gain(void);
	Boolean isLeakDetected(void);
	Real32  getInspLeakVol(void);
    Real32 getNetFlow(void);
    Real32 getDiscoSensitivity(void);
    Boolean isDiscoSensLimitByComp(void);
	Real32 getExhFlowOffset(void);
	Real32 getAvgSteadyExpPressure(void);
    Boolean isDisconnectDetected(void);
 	Boolean isPressureAndFlowSteady(void);
    Boolean isBreathingMode(void);

    Real32 getAverageNetFlowLeak();
    Real32 getAverageExhPressure();

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

private:
	LeakCompMgr(const LeakCompMgr&);	 // not implemented...
	void operator=(const LeakCompMgr&);	   // not implemented...

	void calculateK1K2Leak_(void);
	void checkExhalationFlow_(void);


	//@ Data-Member: LeakCompPhases leakCompPhases_
	// use to determine which Leak Compensation phase the breath is in.
	LeakCompPhases leakCompPhases_ ;

	//@ Data-Member:  prevLeakVolume_
	// The amount of leak volume from the previous breath.
	Real32  prevLeakVolume_;

	//@ Data-Member:  netVolume_
	// The net amount of volume per breath.
	Real32  netVolume_;

	//@ Data-Member:  k1Gain_
	// Used for Leak Compensation algorithm.
	Real32  k1Gain_;

	//@ Data-Member:  finalK1Gain_
	// Used for Leak Compensation algorithm.
	Real32  finalK1Gain_;

	//@ Data-Member:  k2Gain_
	// Used for Leak Compensation algorithm.
	Real32  k2Gain_;

	//@ Data-Member:  pressureSum1_
	// Used for Leak Compensation algorithm.
	Real32  pressureSum1_;

	//@ Data-Member:  pressureSum2_
	// Used for Leak Compensation algorithm.
	Real32  pressureSum2_;

	//@ Data-Member:  prevPressureSum1_
	// Used for Leak Compensation algorithm.
	Real32  prevPressureSum1_;

	//@ Data-Member:  prevPressureSum2_
	// Used for Leak Compensation algorithm.
	Real32  prevPressureSum2_;

	//@ Data-Member:  leakFlow_
	// Stores the leak flow .
	Real32  leakFlow_;

	//@ Data-Member:  leakCompIndex_
	// current index into NetFlowLeakData, and ExhPressureData array
	Uint32  leakCompIndex_;

	//@ Data-Member:  isSteadyLeak_ 
	// The status of a steady leak.
	Boolean  isSteadyLeak_;

	//@ Data-Member:  avgSteadyLeakRate_
	// Average steady leak rate
	Real32  avgSteadyLeakRate_;

	//@ Data-Member:  avgSteadyExpPressure_
	// Average steady expiratory pressure
	Real32  avgSteadyExpPressure_;


	//@ Data-Member:  pressureSlope1_
	// Pressure slope is used for leak compensation algorithm.
	Real32  pressureSlope1_;

	//@ Data-Member:  pressureSlope2_
	// Pressure slope is used for leak compensation algorithm.
	Real32  pressureSlope2_;

	//@ Data-Member:  flowSlope1_
	// Flow slope is used for leak compensation algorithm.
	Real32  flowSlope1_;

	//@ Data-Member:  flowSlope2_
	// Flow slope is used for leak compensation algorithm.
	Real32  flowSlope2_;

	//@ Data-Member:  isPressureAndFlowSteady_
	// Determines whether the pressure and flow is steady.
	Boolean  isPressureAndFlowSteady_;

	//@ Data-Member:  leakCompIndex2_
	// current index into NetFlowLeakData, and ExhPressureData array
	Uint32  leakCompIndex2_;

	//@ Data-Member:  filteredNetLeakFlow_
	// Stores the filtered net flow leak.
	Real32  filteredNetLeakFlow_[MAX_LEAK_COMP_DATA];

	//@ Data-Member:  filteredNetLeakFlow_
	// Stores the filtered exhalation pressure .
	Real32  filteredExhPressure_[MAX_LEAK_COMP_DATA];

	//@ Data-Member:  currentFilExhPressureData_
	// Stores the current filtered exhalation pressure .
	Real32  currentFilExhPressureData_;

	//@ Data-Member:  k1AndK2Counter_
	// Keeps track on the number of K1 and K2 changed
	Uint32 k1AndK2Counter_;


	//@ Data-Member:  effectivePressure_
	// Stores the effective pressure of this breath
	Real32 effectivePressure_;

	//@ Data-Member:  pressureTrajectory_
	// Stores the pressure trajectory of this breath
	Real32 pressureTrajectory_;

	//@ Data-Member:  currentBreathPhaseType_
	// data is available from the beginning of inspiration.
	BreathPhaseType::PhaseType currentBreathPhaseType_;

	//@ Data-Member:  maxLeakVolume_
	// Stores the maximum leak volume per breath.
	Real32 maxLeakVolume_;

	//@ Data-Member:  maxK1Gain_
	// Stores the maximum k1 gain.
	Real32 maxK1Gain_;

	//@ Data-Member:  percentLeak_
	// Stores the percent leak per breath.
	Real32 percentLeak_;

	//@ Data-Member:  prevK1Gain_
	// Stores the previous k1 gain.
	Real32 prevK1Gain_ ;

	//@ Data-Member:  averageNetFlowLeak_
	// Stores the average net flow leak .
	Real32 averageNetFlowLeak_;

	//@ Data-Member:  averageExhPressure_
	// Stores the average exhalation pressure .
	Real32 averageExhPressure_;

	//@ Data-Member:  sqrExhPress_
	// Used for Leak Compensation algorithm.
	Real32 sqrExhPress_;

	//@ Data-Member:  filteredLeakFlow_
	// Stores the filtered leak flow .
	Real32 filteredLeakFlow_;

	//@ Data-Member:  maxPreviousLeakVolume_
	// Stores the previous maximum leak volume.
	Real32 maxPreviousLeakVolume_;

	//@ Data-Member:  prevInspTime_
	// Stores the previous inspiratory time.
	Uint32  prevInspTime_;

	//@ Data-Member:  prevExhTime_
	// Stores the previous exhalation time.
	Uint32  prevExhTime_;

	//@ Data-Member:  prevInspLeakVolume_
	// Stores the previous inspiratory leak volume.
	Real32 prevInspLeakVolume_ ;


	//@ Data-Member:  inspLeakVol_
	// Stores the inspiratory leak volume.
	Real32 inspLeakVol_;


	//@ Data-Member:  isLeakDetected_
	// Used to determine whether a leak is present
	Boolean isLeakDetected_;

	//@ Data-Member:  exhLeakRate_
	// Stores the exhalation leak rate.
	Real32 exhLeakRate_;

	//@ Data-Member:  partialSteadyAvgPress_
	// Stores the partial steady average pressure
	Real32 partialSteadyAvgPress_;


	//@ Data-Member:  partialPrevLeakVolume_
	// Stores the partial previous leak volume for Bilevel
	Real32 partialPrevLeakVolume_;

	//@ Data-Member:  isPartialSteadyLeak_
	// The status of a partial steady leak for Bilevel
	Boolean isPartialSteadyLeak_;

	//@ Data-Member:  ieTransition_
	// A flag for tracking the ie transitions.
	Boolean ieTransition_;

	//@ Data-Member:  partialAvgSteadyLeakRate_
	// Stores the partial average steady leak rate for Bilevel
	Real32 partialAvgSteadyLeakRate_;

	//@ Data-Member:  partialPressureSum1_
	// Stores the partial pressure sum 1 for Bilevel
	Real32 partialPressureSum1_;

	//@ Data-Member:  partialPressureSum2_
	// Stores the partial pressure sum 2 for Bilevel
	Real32 partialPressureSum2_;

	//@ Data-Member:  prevFilteredNetFlow_
	// Stores previous filtered net flow 
	Real32 prevFilteredNetFlow_;

	//@ Data-Member:  estimatedLeakError_
	// Stores the estimated leak error
	Real32 estimatedLeakError_;

	//@ Data-Member:  k2Counter_
	// A counter for K2 increments under non-steady
	// conditions.
	Uint32 k2Counter_;

	//@ Data-Member:  prevK2Gain_
	// Stores the previous k2 gain.
	Real32 prevK2Gain_ ;

	//@ Data-Member:  prevAverageExhPressureData_
	// Stores the previous average exhalation pressure  
	Real32 prevAverageExhPressureData_;

	//@ Data-Member:  modeValue_
	// Stores the current mode setting
	DiscreteValue modeValue_;

	//@ Data-Member:  circuitType_
	// Stores the current circuit type setting.
	DiscreteValue circuitType_; 

	//@ Data-Member:  netFlow_
	// Stores net flow
	Real32 netFlow_;

	//@ Data-Member:  RInspResistance_
	// inspiration side tubing resistance
	Resistance  rInspResistance_;

	//@ Data-Member:  RExhResistance_
	// exhalation side tubing resistance
	Resistance  rExhResistance_;


	//@ Data-Member:  isLeakCompEnabled_
	// Set if leak comp enabled setting is enabled or disabled.
    Boolean isLeakCompEnabled_;

	//@ Constant: L_PER_MIN_TO_ML_
	// L/min to mL convertion factor
	const Real32 L_PER_MIN_TO_ML_;

	//@ Data-Member:  discoSensitivity_
	// Stores the disconnect sensitivity setting.
    Real32 discoSensitivity_;

	//@ Data-Member:  isDiscoSensLimitByComp_
	// Determines whether the disconnect sensitivity setting
	// reached its limit.
	Boolean isDiscoSensLimitByComp_;

	//@ Data-Member:  exhFlowOffset_
	// Exhalation flow offset
    Real32 exhFlowOffset_;

	//@ Data-Member:  isExhLeakHigh_
	// Flag for exhalation leak exceeding disconnect sensitivity
    Boolean isExhLeakHigh_;

	//@ Data-Member:  isBreathingMode_
	// Flag for determining if the vent is currently in breathing mode.
	Boolean isBreathingMode_;

	//@ Data-Member: netLeakFlowBuffer_
	// data buffer to collect the net leak flow 
	SummationBuffer netLeakFlowBuffer_;

	//@ Data-Member: exhPressureBuffer_
	// data buffer to collect the exhalation pressure
	SummationBuffer exhWyePressureBuffer_;

	//@ Data-Member:  exhPressure_
	// Stores the current exhalation pressure .
	Real32 exhPressure_;

	//@ Data-Member:  filteredNetFlow_
	// Stores filtered net flow 
	Real32 filteredNetFlow_;

};


#endif // LeakCompMgr_HH 

