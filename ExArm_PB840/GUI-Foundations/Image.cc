#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

// ============================= F I L E     D E S C R I P T I O N ====
//@ Class: Image - Represents monochrome and color images displayed by Bitmap objects.
//---------------------------------------------------------------------
//@ Interface-Description
// This class has only static data members and has no member functions.
// This is a static class which cannot be instantiated.  There is one
// public DibInfo member (const) for each monochrome/color image
// supported by Sigma.
//---------------------------------------------------------------------
//@ Rationale
// This is needed to abstract the platform-specific image
// representations used by the Bitmap class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes the platform-specific DibInfo members. The
// image data is stored in private data members as bit arrays.
//
// IMPORTANT NOTE: the initializers for the COLOR bit maps in this file must
// specify color codes explicitly, while in other files these are referred to
// via the Colors::ColorS enum.  These codes are embedded in the large
// blocks of hexadecimal numbers; each nybble (2 per byte) represents
// one of the 16 possible colors.  If the values assigned to the enum
// members change, these initializers may also need to be updated.
//---------------------------------------------------------------------
//@ Fault-Handling
// None.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Image.ccv   25.0.4.0   19 Nov 2013 14:11:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 011   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 010   By: rhj    Date:  21-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Added TrendIcon.
// 
//  Revision: 009   By: btray    Date:  20-Jul-1999    DCS Number: 5424
//  Project: 840 Neonatal
//  Description:
//	Added new CheckMarkIcon for Development Options subscreen.	
//
//  Revision: 008   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 007   By: hhd    Date: 08-Jun-1999   DR Number: 5423
//  Project:  ATC
//  Description:
//      Removed 'CHECKMARK_ICON' bitmap; Added 'ARROW_ICON' bitmap.
//
//  Revision: 006   By: sah    Date: 29-Apr-1999   DR Number: 5365
//  Project:  ATC
//  Description:
//      Added new 'CHECKMARK_ICON' bitmap, to be used for the new
//      flashing, verification-needed mechanism.
//
//  Revision: 005   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 004  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 003  By: mpm      Date: 29-Mar-1996   DCR Number: 654
//    Project:  Sigma (R8027)
//    Description:
//      Made color alarm icon blink.
//
//  Revision: 002  By: mpm      Date: 22-Jan-1996   DCR Number: 654
//    Project:  Sigma (R8027)
//    Description:
//      Added COLOR_BLINKING_ALARM_ICON.
//  
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//=====================================================================

#include <string.h>
#include "Image.hh"
#include "DisplayContext.hh"
#include "VgaDib.hh"
#include "VgaDisplayId.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//
// DibInfos for each image are defined in their own modules
//
extern DibInfo MonoAlarmIcon;
extern DibInfo WaveformIcon;
extern DibInfo TrendIcon;
extern DibInfo BlinkingAlarmIcon;
extern DibInfo AlarmLogIcon;
extern DibInfo MoreDataIcon;
extern DibInfo OtherScreensIcon;
extern DibInfo AlertIcon;
extern DibInfo LockedPadlockIcon;
extern DibInfo UnlockedPadlockIcon;
extern DibInfo XAxisIcon;
extern DibInfo YAxisIcon;
extern DibInfo LeftArrowIcon;
extern DibInfo RightArrowIcon;
extern DibInfo CheckArrowIcon;
extern DibInfo CheckMarkIcon;

DibInfo & Image::RMonoAlarmIcon = MonoAlarmIcon;
DibInfo & Image::RWaveformIcon = WaveformIcon;
DibInfo & Image::RTrendIcon = TrendIcon;
DibInfo & Image::RBlinkingAlarmIcon = BlinkingAlarmIcon;
DibInfo & Image::RAlarmLogIcon = AlarmLogIcon;
DibInfo & Image::RMoreDataIcon = MoreDataIcon;
DibInfo & Image::ROtherScreensIcon = OtherScreensIcon;
DibInfo & Image::RAlertIcon = AlertIcon;
DibInfo & Image::RLockedPadlockIcon = LockedPadlockIcon;
DibInfo & Image::RUnlockedPadlockIcon = UnlockedPadlockIcon;
DibInfo & Image::RXAxisIcon = XAxisIcon;
DibInfo & Image::RYAxisIcon = YAxisIcon;
DibInfo & Image::RLeftArrowIcon = LeftArrowIcon;
DibInfo & Image::RRightArrowIcon = RightArrowIcon;
DibInfo & Image::RCheckArrowIcon = CheckArrowIcon;
DibInfo & Image::RCheckMarkIcon = CheckMarkIcon;

DibInfo * const Image::PDibInfos_[] =
{
  &MonoAlarmIcon,
  &WaveformIcon,
  &BlinkingAlarmIcon,
  &AlarmLogIcon,
  &MoreDataIcon, 
  &OtherScreensIcon,
  &AlertIcon, 	
  &LockedPadlockIcon,
  &UnlockedPadlockIcon,
  &XAxisIcon,
  &YAxisIcon,
  &LeftArrowIcon,
  &RightArrowIcon,
  &CheckArrowIcon,
  &CheckMarkIcon,
  &TrendIcon
};

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize [static]
//
//@ Interface-Description
//  Initializes the DibInfos contained in this class. This
//  process allocates the colormap entries in both displays for each
//  image.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the VgaDib::RemapColors method to allocate the colormap entries.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void
Image::Initialize(void)
{
  for ( Uint index=0; index<countof(PDibInfos_); index++)
  {
	DibInfo * pDibInfo = PDibInfos_[index];
	
	VgaDib::AllocateColors(*pDibInfo);
  }
  // $[TI1]
}

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================
void 
Image::SoftFault(const SoftFaultID  softFaultID,
				 const Uint32       lineNumber,
				 const char*        pFileName,
				 const char*        pPredicate)  
{
	CALL_TRACE("Image::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, IMAGE,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
