#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DisconnectPhase - active phase during circuit disconnect phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  Virtual
//		methods are implemented to initialize the DisconnectPhase and to
//		perform DisconnectPhase activities each BD cycle.
//---------------------------------------------------------------------
//@ Rationale
//      This class implements the algorithms during disconnect phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//      An IDLE_FLOW is delivered by controlling the Psols every BD cycle.
//      The exhalation valve is opened when the phase is activated.  It
//      is not controlled every cycle.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//      Only one instance can be instanciated.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DisconnectPhase.ccv   25.0.4.0   19 Nov 2013 13:59:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By: rhj     Date: 05-Oct-2010     SCR Number: 6627
//  Project:  PROX
//  Description:
//      Added a method to reset estimated wye pressure during a disconnect.
//
//  Revision: 011  By: rhj     Date: 12-May-2010     SCR Number: 6627
//  Project:  PROX
//  Description:
//      Added a method to reset prox substitute during a disconnect.
//
//  Revision: 010  By: rhj     Date: 20-July-2010    SCR Number: 6608 
//  Project:  MAT
//  Description:
//      If the current circuit is Neonatal, IDLE_FLOW is used instead
//      of LEAK_COMP_IDLE_FLOW.
//  
//  Revision: 009  By: rhj     Date: 07-July-2008    SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
// 
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By: syw   Date:  15-Sep-1997    DR Number: 2430
//  	Project:  Sigma (840)
//		Description:
//			Added requirement traceability.
//
//  Revision: 006 By: syw     Date: 25-Aug-1997   DR Number: DCS 2419
//  	Project:  Sigma (R8027)
//		Description:
//			Call TemperatureSensor::SetAlpha() during newBreath() and
//			relinquishControl().
//
//  Revision: 005 By: iv     Date: 18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//			Removed the varialble IDLE_FLOW.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003 By: syw   Date: 16-Aug-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added call to updateFlowControllerFlags_().
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "DisconnectPhase.hh"

#include "ControllersRefs.hh"
#include "ValveRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "FlowController.hh"
#include "ExhalationValve.hh"
#include "O2Mixture.hh"
#include "BD_IO_Devices.hh"
#include "TemperatureSensor.hh"
#include "BreathTrigger.hh"
#include "LeakCompEnabledValue.hh"
#include "PendingContextHandle.hh"
#include "LeakCompMgr.hh"
#include "ExhalationPhase.hh"
#include "PeepController.hh"
#include "ExhalationPhase.hh"
#include "PhaseRefs.hh"
#include "BreathRecord.hh"
#include "PhasedInContextHandle.hh"
#include "PatientCctTypeValue.hh"
//@ End-Usage

//@ Code...

//@ Constant: LEAK_COMP_IDLE_FLOW
// constant base flow during disconnect in lpm only when
// leak compensation is enabled.
static const Real32 LEAK_COMP_IDLE_FLOW = 20.0F ;	// $[LC24040]

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisconnectPhase
//
//@ Interface-Description
//      Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type as an argument.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

DisconnectPhase::DisconnectPhase(void)
 : BreathPhase(BreathPhaseType::NON_BREATHING) 		   	// $[TI1]

{
	CALL_TRACE("DisconnectPhase::DisconnectPhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DisconnectPhase
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

DisconnectPhase::~DisconnectPhase(void)
{
	CALL_TRACE("DisconnectPhase::~DisconnectPhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//      called every BD cycle during disconnect phase to deliver the IDLE_FLOW
//      or LEAK_COMP_IDLE_FLOW during a disconnect.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The flow controller flags are updated and the desiredO2Flow and
//		desiredAirFlow are computed based on the o2 mix.  The flowControllers
//		are called to deliver the flows.  The exhalation valve does not need
//		to be updated since it is commanded open at the beginning of the phase.
//      $[LC24040] -- When the Leak Compensation is ON, the magnitude of 
//      delivered flow during disconnect is LEAK_COMP_IDLE_FLOW.
// 		$[04214]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
DisconnectPhase::newCycle(void)
{
	CALL_TRACE("DisconnectPhase::newCycle(void)") ;

   	// $[TI1]
	updateFlowControllerFlags_() ;

    Real32 desiredO2Flow = 0.0F;
    Real32 desiredAirFlow = 0.0F;

	const DiscreteValue  CIRCUIT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	// $[LC24040] -- When the Leak Compensation is ON, the magnitude of 
	// delivered flow during the  "Waiting For Patient Connection"... 
	if (RLeakCompMgr.isEnabled() && (CIRCUIT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT))
	{
		desiredO2Flow = LEAK_COMP_IDLE_FLOW * RO2Mixture.calculateO2Fraction() ;
		desiredAirFlow = LEAK_COMP_IDLE_FLOW - desiredO2Flow ;
	}
	else
	{
		desiredO2Flow = IDLE_FLOW * RO2Mixture.calculateO2Fraction() ;
		desiredAirFlow = IDLE_FLOW - desiredO2Flow ;
	}

    RAirFlowController.updatePsol( desiredAirFlow) ;
    RO2FlowController.updatePsol( desiredO2Flow) ;

    elapsedTimeMs_ += CYCLE_TIME_MS ;	

	// Reset Leak Compensation Parameters during 
	// disconnect
	RLeakCompMgr.resetLeakCompParms();   
	RExhalationPhase.resetLeakAdd();
	RPeepController.resetLeakAdd();
    BreathRecord::ResetProxSubPress();
    BreathRecord::ResetWyePressEst();

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called at the beginning of each disconnect phase to initialize the
//      disconnect phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//      newBreath() methods are called for the AirFlowController,
//      O2FlowController.  The exhalation valve is commanded to zero
//      with a NORMAL damping and other data members are initialized..
// 		$[04213] all triggers are disabled by scheduler 
// 		$[04214] $[04332]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
DisconnectPhase::newBreath(void)
{
	CALL_TRACE("DisconnectPhase::newBreath(void)") ;
	
   	// $[TI1]
    RAirFlowController.newBreath() ;
    RO2FlowController.newBreath() ;
	
	RExhalationValve.updateValve( 0.0F, ExhalationValve::NORMAL) ;

    elapsedTimeMs_ = 0 ;
   	TemperatureSensor::SetAlpha( TRIGGER_ALPHA) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
//      This method takes a BreathTrigger& as an argument and has no return
//      value.  This method is called at the beginning of the next phase to
//      wrap up the current breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Call TemperatureSensor::SetAlpha().
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
DisconnectPhase::relinquishControl( const BreathTrigger& trigger)
{
	CALL_TRACE("BreathPhase::relinquishControl( BreathTrigger& trigger)") ;
	
   	// $[TI1]
   	TemperatureSensor::SetAlpha( NOMINAL_ALPHA) ;
	UNUSED_SYMBOL(trigger);
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
DisconnectPhase::SoftFault(	const SoftFaultID  softFaultID,
                   			const Uint32       lineNumber,
		   					const char*        pFileName,
		   					const char*        pPredicate)
{
  	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, DISCONNECTPHASE,
                             lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================













