#ifndef TrendDbTableBuffer_HH
#define TrendDbTableBuffer_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbTableBuffer
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbTableBuffer.hhv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  mjf    Date:  18-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
// 
// 
//=====================================================================
//@ Usage-Classes
//@ End-Usage

#include <string.h>
#include "TrendDataMgr.hh"


// forward declare
class TrendDbRecord;

class TrendDbTableBuffer
{
public:

	TrendDbTableBuffer( MemPtr pMemory, Uint8 blockCount,
						Int16 blockSize, Int16 recordSize  );

	~TrendDbTableBuffer();

	inline void clear( void );

	// Following methods return the properties in bytes
	inline Uint32 getBufferCapacity( void ) const;
	inline Int16 getRecordSize( void ) const;
	inline  Int16 getBlockSize( void ) const;

	// Following methods return the properties in blocks
	inline Uint8 getBlockCount( void ) const;

	// Following methods return the properties in sample records "rows"
	inline Int16 getRecordCapacity( void ) const;
	inline Int16 getRecordsPerBlock( void ) const;
	inline Int16 getRecordPosition( void ) const;

	// Following method is used to parse over the data in the buffer
	void setRecordPosition( Int16 position );
	void setLastPosition( Int16 lastPosition );
	inline Boolean atEnd( void ) const;
	inline Boolean atStart( void ) const;
	void setFirstRecord( TrendDbRecord & record );
	void setLastRecord( TrendDbRecord & record );
	SigmaStatus gotoNextRecord( TrendDbRecord & record, Boolean goForward );
	MemPtr getBlockMemory( Uint8 blockNum );


	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:

	MemPtr getRecordOffset_( Int16 position );

	// no copy or assignment constructor
	TrendDbTableBuffer( const TrendDbTableBuffer& );	// not implemented
	void operator=( const     TrendDbTableBuffer& );	// not implemented

	//@ Data-Member: pMemoryBuffer_
	// A raw pointer to the memory
	MemPtr pMemoryBuffer_;

	//@ Data-Member: blockCount_ 
	//  Number of blocks in the buffer
	Uint8 blockCount_;

	//@ Data-Member: blockSize_
	//  Number of bytes in each block
	Int16 blockSize_;

	//@ Data-Member: recordSize_
	//  Number of bytes in each record
	Int16 recordSize_;

	//@ Data-Member: recordPerBlock_
	//  Computed value of blockSize/recordSize
	Int16  recordPerBlock_;

	//@ Data-Member: recordPosition_
	//  Position of the record access
	Int16 recordPosition_;

	//@ Data-Member: lastPosition_
	//  Used to mark/track the last position in the buffer
	Int16 lastPosition_;
};

// Inlined methods
#include "TrendDbTableBuffer.in"

#endif // TrendDbTableBuffer_HH
