#pragma once

#include "BaseContainerView.h"

// UpperBaseContainerView view

class CUpperBaseContainerView : public BaseContainerView
{
	DECLARE_DYNCREATE(CUpperBaseContainerView)
public:
	void CreateUpperBaseViewCDC();

protected:
	CUpperBaseContainerView();           // protected constructor used by dynamic creation
	virtual ~CUpperBaseContainerView();

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
public:

};


