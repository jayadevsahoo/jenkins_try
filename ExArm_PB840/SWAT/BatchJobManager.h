//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
/// @file BatchJobManager.h
///
/// @brief Class BatchJobManager


#ifndef BATCHJOBMANAGER_H
#define BATCHJOBMANAGER_H

#include "BatchExecuter.h"

namespace swat{

    /// @class BatchJobManager
    ///
    class BatchJobManager{

        private:

            class ListNode{
                public:
                    BatchExecuter clBatchExecuter;
                    ListNode *pNext;
            };

            ListNode *freeList;
            ListNode *activeList;


            BatchCommandInfo * (*m_pfnGetNextBatch)();



            ListNode *freeNode(ListNode *pLast, ListNode *pCur);


            bool hasFreeNode();


            ListNode *getFreeNode();

            void appendToActiveList(ListNode *p);



            BatchCommandInfo *getNextBatch();


        public:
            BatchJobManager(int cnt, BatchCommandInfo * (*getNextBatch)()=0);
            ~BatchJobManager();

            ///
            /// Cycles through all the active BatchExecuter's and runs
            void RunCycle();
    };

}

#endif
