#ifndef BreathPhaseScheduler_HH
#define BreathPhaseScheduler_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: BreathPhaseScheduler - the base class for all breath phase schedulers
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathPhaseScheduler.hhv   25.0.4.0   19 Nov 2013 13:59:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 018   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 017   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added methods to accept and reject RM maneuver requests.
//
//  Revision: 016  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 015  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  BiLevel (R8027)
//       Description:
//             Bilevel initial version
//             Added methods: RejectInspiratoryPause_(), AcceptInspiratoryPause_().
//
//  Revision: 014  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review for BD Schedulers.
//
//  Revision: 013  By:  iv    Date:  27-Mar-1997    DR Number: 1876
//       Project:  Sigma (840)
//       Description:
//             Removed non used unit test methods.
//
//  Revision: 012  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 011 By:  iv   Date:   06-Feb-1997    DR Number: DCS 1737 
//       Project:  Sigma (840)
//       Description:
//             Added methods Get/SetIsStandbyRequired() and data member
//             IsStandbyRequired.
//
//  Revision: 010  By:  iv    Date:  18-Dec-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 009  By:  iv    Date:  04-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changes for unit test.
//
//  Revision: 008 By:  iv   Date:   05-June-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Added a data member PPreviousScheduler_; Added a method
//             GetPreviousScheduler().
//
//  Revision: 007 By:  iv   Date:   07-May-1996    DR Number: DCS 967 
//       Project:  Sigma (R8027)
//       Description:
//             Added a new enum type PeepRecoveryState, and a new static protected
//             data memeber PeepRecoveryState PeepRecovery_.
//
//  Revision: 006 By:  iv   Date:   18-Apr-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Added a method void getId(char *, SchedulerId::SchedulerIdValue)
//             for development only.
//
//  Revision: 005 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 004  By:  iv   Date:  13-Dec-1995    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Changed the method that checks if apnea is possible from protected to public.
//
//  Revision: 003  By:  kam   Date:  07-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed takeControl() to be a virtual vs. a pure virtual method so
//             that common functionality can be added to the base class takeControl();
//             all schedulers invoke the base class takeControl() method from their
//             takeControl() method.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

//@ Usage-Classes
#  include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "SchedulerId.hh"
#include "SettingId.hh"
#include "Trigger.hh"

//@ Usage-Classes
class ModeTrigger;
class BreathTrigger;
#include "EventData.hh"
//@ End-Usage

//@ const MAX_MODE_TRIGGER
// The maximum number of mode triggers
const Int32 MAX_MODE_TRIGGERS = Trigger::NUM_TRIGGER - Trigger::LAST_BREATH_TRIGGER;

class BreathPhaseScheduler
{
  public:
	BreathPhaseScheduler(const SchedulerId::SchedulerIdValue id);
	virtual ~BreathPhaseScheduler(void);

   static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	enum PeepRecoveryState {START, ACTIVE, OFF};
	enum BlTransitionState {LOW_TO_HIGH, HIGH_TO_LOW, NULL_TRANSITION};

    virtual void determineBreathPhase(const BreathTrigger& breathTrigger) = 0;
    virtual void relinquishControl(const ModeTrigger& modeTrigger) = 0;
    virtual void takeControl(const BreathPhaseScheduler& prevScheduler);
	inline SchedulerId::SchedulerIdValue getId(void) const;
	static inline SchedulerId::SchedulerIdValue GetPreviousSchedulerId(void);
#ifdef SIGMA_DEVELOPMENT
	void getId(char* idString, SchedulerId::SchedulerIdValue id) const;
#endif // SIGMA_DEVELOPMENT
	
	static void SettingEventHappened(SettingId::SettingIdType id);
	static void NewCycle(void);
    static inline BreathPhaseScheduler& GetCurrentScheduler(void);
    static inline Boolean GetIsStandbyRequired(void);
    static inline void SetIsStandbyRequired(const Boolean isStandbyRequired);
    static inline BlTransitionState GetBlPeepTransitionState(void);
    static inline void SetBlPeepTransitionState(BlTransitionState blState);
    static inline void SignalBilevelLoTransition(void);
    static BreathPhaseScheduler& EvaluateSetScheduler(void);
    static BreathPhaseScheduler& EvaluateSetScheduler(const DiscreteValue mode);
	static EventData::EventStatus ReportEventStatus(const EventData::EventId id,
													 const Boolean eventStatus);
	static void Initialize(void);
	static Boolean CheckIfApneaPossible(void);
		
  protected:
	static inline void SetCurrentScheduler_(BreathPhaseScheduler& scheduler);
	static EventData::EventStatus
					RejectManualInspiration_(const Boolean eventStatus);
	static EventData::EventStatus
					AcceptManualInspiration_(const Boolean eventStatus);
	static EventData::EventStatus
					RejectExpiratoryPause_(const Boolean eventStatus);
	static EventData::EventStatus
					AcceptExpiratoryPause_(const Boolean eventStatus);
	static EventData::EventStatus
					RejectInspiratoryPause_(const Boolean eventStatus);
	static EventData::EventStatus
					AcceptInspiratoryPause_(const Boolean eventStatus);
	static EventData::EventStatus
					RejectNifManeuver_(const Boolean isStartRequested);
	static EventData::EventStatus
					AcceptNifManeuver_(const Boolean isStartRequested);
	static EventData::EventStatus
					RejectP100Maneuver_(const Boolean isStartRequested);
	static EventData::EventStatus
					AcceptP100Maneuver_(const Boolean isStartRequested);
	static EventData::EventStatus
					RejectVitalCapacityManeuver_(const Boolean isStartRequested);
	static EventData::EventStatus
					AcceptVitalCapacityManeuver_(const Boolean isStartRequested);
	static EventData::EventStatus
					AcceptAlarmReset_(const Boolean eventStatus);
    static void VerifyModeSynchronization_(const SchedulerId::SchedulerIdValue id);
	virtual void enableTriggers_(void) = 0;
	virtual EventData::EventStatus reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus) = 0;
	virtual void settingChangeHappened_(const SettingId::SettingIdType id);
	
	
	//@ Data-Member: modeTriggerList[MAX_MODE_TRIGGERS]
	// A list of mode trigger pointers, active for this mode
	ModeTrigger* pModeTriggerList_[MAX_MODE_TRIGGERS];

	//@ Data-Member: Boolean RateChange_
	// state variable to indicate rate change is in process;
	static Boolean RateChange_;

	//@ Data-Member: Boolean ModeChange_
	// state variable to indicate mode change is in process;
	static Boolean ModeChange_;

	//@ Data-Member: PeepRecoveryState PeepRecovery_
	// Indicates whether or not peep recovery is in process;
	static PeepRecoveryState PeepRecovery_;

    //@ Data-Member PPreviousScheduler_
	// pointer to the previous scheduler
    static BreathPhaseScheduler* PPreviousScheduler_;

	//@ Data-Member: Real32 BlPeepTransitionState_
	// Flag for the state of peep transition
	static BlTransitionState BlPeepTransitionState_ ;
	
  private:
	BreathPhaseScheduler(const BreathPhaseScheduler&);// Declared but not implemented
	void operator=(const BreathPhaseScheduler&);// Declared but not implemented
	BreathPhaseScheduler(void);		// Declared but not implemented

    //@ Data-Member PCurrentBreathPhaseScheduler_
	// pointer to the active scheduler
    static BreathPhaseScheduler* PCurrentBreathPhaseScheduler_;

    //@ Data-Member: schedulerId_
    // scheduler id
    const SchedulerId::SchedulerIdValue schedulerId_;

	//@ Data-Member: Boolean IsRateChangePending_
	// state variable to indicate rate change is pending;
	static Boolean IsRateChangePending_;

	//@ Data-Member: Boolean IsBLInspTimeChangePending_
	// state variable to indicate BiLevel Insp. time change is pending;
	static Boolean IsBLInspTimeChangePending_;

	//@ Data-Member: Boolean IsApneaRateChangePending_
	// state variable to indicate apnea rate change is pending;
	static Boolean IsApneaRateChangePending_;

	//@ Data-Member: Boolean IsModeChangePending_
	// state variable to indicate mode change is pending;
	static Boolean IsModeChangePending_;

	//@ Data-Member: Boolean IsSpontTypeChangePending_
	// state variable to indicate spont type change is pending;
	static Boolean IsSpontTypeChangePending_;

	//@ Data-Member: Boolean IsStandbyRequired_
	// state variable to indicate that return to standby from SvoScheduler
	// is required.
	static Boolean IsStandbyRequired_;

	//@ Data-Member: ImmediateChangeSettingId_
	// Contains the setting ID that has triggered an immediate setting phase
	// in (used during CPAP)
	static SettingId::SettingIdType ImmediateChangeSettingId_;
};

// Inlined methods
#include "BreathPhaseScheduler.in"

#endif // BreathPhaseScheduler_HH 
