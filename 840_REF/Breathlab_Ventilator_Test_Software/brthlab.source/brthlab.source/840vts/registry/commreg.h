/*****************************************************************************/
/*   Module Name:    commreg.h                                         
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _COMMREG
#define _COMMREG

#include "registry.h"

typedef struct {
    // if connection is made, this is what we connected at
    int port;
    int baud;
    int databits;
    int stopbits;
    int parity;
	int autoDetectDisabled;
} RegComStruct;


int Get840CommParams(RegComStruct* Params);
int GetPTSCommParams(RegComStruct* Params);
int Set840CommParams(RegComStruct* Params);
int SetPTSCommParams(RegComStruct* Params);
int GetCommParams(RegComStruct* Params, char* DevKey);
int SetCommParams(RegComStruct* Params, char* DevKey);

#endif // #define _COMMREG
