#ifndef PostErrors_HH
#define PostErrors_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
//@ Filename: PostErrors.hh - Phase 2 & 3 POST Test Number and Error 
//                            Code enumerators.
//
//  This file in conjunction with the kernel post header file (kerrors.hh)
//  define the LED codes that can appear during POST.
//
//---------------------------------------------------------------------
//@ Modification-Log
//
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997 DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

// When adding new codes, be sure that there are no duplicates (conflicts)
// with kerrors.hh.  kerrors.hh included to have compiler identify 
// duplicate names more readily.  It also defined POST_ERROR_UNUSED.


#include "kerrors.hh"

// Common Post errors (on both cpu's) 
enum PostErrors
{

// $0 -$1F are defined in kernel.inc and kerrors.hh for Kernel tests
// $20-$2F reserved for POST exception handler
// $30-$4F reserved for downloader

      POST_ERROR_MMU                          = 0x51
    , POST_ERROR_BTO                          = 0x52
    , POST_ERROR_NMI_REGISTER                 = 0x53
    , POST_ERROR_DRAM_MEMORY                  = 0x54
    , POST_ERROR_LAN_SELFTEST_START           = 0x55
    , POST_ERROR_LAN_SELFTEST_END             = 0x56
    , POST_ERROR_UNEXPECTED_RESET_UMPIRE      = 0x57 
    , POST_ERROR_POST_NOVRAM                  = 0x58 
    , POST_ERROR_FPU                          = 0x59 
    , POST_ERROR_DRAM_PARITY                  = 0x5A 

    // GUI Post errors
    , POST_ERROR_SAASTEST_START               = 0x61 
    , POST_ERROR_SAASTEST_END                 = 0x62 
    , POST_ERROR_RESONANCE_START              = 0x63 
    , POST_ERROR_RESONANCE_END                = 0x64 

    // BD Post errors
    , POST_ERROR_VENTINOP                     = 0x70 
    , POST_ERROR_ANALOG_INTERFACE_PCB         = 0x71    
    , POST_ERROR_ADC                          = 0x72    
    , POST_ERROR_DAC                          = 0x73    
    , POST_ERROR_ANALOG_DEVICES               = 0x74    
    , POST_ERROR_SERIAL_DEVICE                = 0x75 
    , POST_ERROR_SAFESTATE_SYSTEM             = 0x78    
    , POST_ERROR_SERVICE_SWITCH               = 0x79 
    , POST_ERROR_AC_VOLTAGE                   = 0x7A
						  
// $70-$7f reserved in kerrors.hh
};

#endif   // #ifndef PostErrors_HH

