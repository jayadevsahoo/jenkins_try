// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#pragma comment(linker, "/nodefaultlib:libc.lib")
#pragma comment(linker, "/nodefaultlib:libcd.lib")

// NOTE - this is value is not strongly correlated to the Windows CE OS version being targeted
#define WINVER _WIN32_WCE

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit
#ifdef _CE_DCOM
#define _ATL_APARTMENT_THREADED
#endif

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <ceconfig.h>
#if defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
#define SHELL_AYGSHELL
#endif

#include "winsock2.h"
#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif



#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT



#if defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
#ifndef _DEVICE_RESOLUTION_AWARE
#define _DEVICE_RESOLUTION_AWARE
#endif
#endif

#ifdef _DEVICE_RESOLUTION_AWARE
#include "DeviceResolutionAware.h"
#endif


#if (_WIN32_WCE < 0x500) && ( defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP) )
	#pragma comment(lib, "ccrtrtti.lib")
	#ifdef _X86_	
		#if defined(_DEBUG)
			#pragma comment(lib, "libcmtx86d.lib")
		#else
			#pragma comment(lib, "libcmtx86.lib")
		#endif
	#endif
#endif

#include <altcecrt.h>

#ifdef USE_PCH
#include "SigmaTypes.hh"
#include "Sigma.hh"
#include "CallTrace.hh"
#include "TextButton.hh"
#include "Alarm.hh"
#include "TextField.hh"
#include "LargeContainer.hh"
#include "UpperScreen.hh"
#include "SettingValue.hh"
#include "DiscreteSettingButton.hh"
#include "PatientDataMgr.hh"
#include "LowerScreen.hh"
#include "AlarmAnnunciator.hh"
#include "MsgQueue.hh"
#include "FaultHandlerMacros.hh"
#include "NetworkAppClassIds.hh"
#include "GuiAppClassIds.hh"
#include "Box.hh"
#include "OperatingGroup.hh"
#include "NetworkApp.hh"
#include "SettingSubject.hh"
#include "TrendDataMgr.hh"
#include "ViolationHistoryManager.hh"
#include "AlarmConstants.hh"
#include "GuiFoundation.hh"
#include "TrendSubScreen.hh"
#include "BatchSettingValues.hh"
#include "GuiSize.hh"
#include "LowerScreenSelectArea.hh"
#include "AlarmOffKeyPanel.hh"
#include "GuiTestManager.hh"
#include "TemplateMacros.hh"
#include "Setting.hh"
#include "UpperScreenSelectArea.hh"
#include "BatchSettingsSubScreen.hh"
#include "NumericField.hh"
#include "DisplayContext.hh"
#include "OsTimeStamp.hh"
#endif    
