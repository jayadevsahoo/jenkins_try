
#ifndef PauseTypes_HH
#define PauseTypes_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Enum: PauseTypes - Type of breath that occurs.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PauseTypes.hhv   25.0.4.0   19 Nov 2013 14:00:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 002  By:  swyw   Date:  21-Oct-1998    DR Number: DCS 5223
//       Project:  BiLevel
//       Description:
//             Changed enum order.
//
//  Revision: 001  By:  yyy    Date:  24-Mar-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
//@ Usage-Classes
//@ End-Usage

union PauseTypes
{
	//@ Type: ManeuverType
	// This enum represents the type of Inspiratory pause.
  	enum ManeuverType
  	{
  		MANUAL,
  		AUTO
  	};
  
	//@ Type: ManeuverState
	// This enum represents the state of Inspiratory pause.
	// qPat, pEexh, and pEd.
  	enum ManeuverState
  	{
  		IDLE,				// No maneuver requested
  		PENDING,			// Maneuver is just requested
  		ACTIVE,				// Maneuver is active
  		IDLE_PENDING,		// Maneuver is completed but data not
  							// been sent yet.
  		ACTIVE_PENDING		// Maneuver is completed but data not
  							// been sent yet.  However, the user
  							// is requesting for another pause.
  	};
  
	//@ Type: PedState
	// This enum represents the state for pEd calculation.
	// qPat, pEexh, and pEd.
  	enum PedState
  	{
  		PED_IDLE,
  		PED_ACTIVE,
  		PED_COMPLETE
  	};
  
	//@ Type: PpiState
	// This enum represents the state for pPi calculation.
	// qPat, pEexh, and pEd.
	enum PpiState
	{
  		UNSTABLE,
		STABLE
  	};
  
	//@ Type: ComplianceState
	// This enum represents the state for pPi calculation.
	// qPat, pEexh, and pEd.

	// order of the following enumeration is important.  They are ordered from
	// the lowest to highest state of the pause result.
	enum ComplianceState
	{
		LOWEST_STATIC_STATE, 
		VALID = LOWEST_STATIC_STATE,// data is reliable
		OUT_OF_RANGE,				// data is questionable -  Number and "()" is displayed
									// a stability problem.
		CORRUPTED_MEASUREMENT,		// data is questionable -  Number and "()" is displayed
									// a stability problem.
		SUB_THRESHOLD_INPUT,		// data is questionable -  Number and "()" is displayed
									// a stability problem.
  		UNAVAILABLE,				// data can not be calculataed - "***" and "()" is displayed
									// a stability problem.
  		EXH_TOO_SHORT,				// exh too short -  Number and "()" is displayed
									// a stability problem.
  		NOT_STABLE,					// plateau is not stable -  Number and "()" is displayed
									// a stability problem.
  		NOT_REQUIRED,				// data is not required for this case - display dash
									// a stability problem.
  		HIGHEST_STATIC_STATE = NOT_REQUIRED
  	};
};

#endif // PauseTypes_HH 
