#ifndef	DciNumber_IN
#define	DciNumber_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ====================================================================
//@ Class: DciNumber - Store a Real32 value with an associated validity tag
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/DCI/vcssrc/DciNumber.inv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 17-DEC-1997  DR Number: 2605
//  Project: Sigma (R8027)
//  Description:
//      Restructuring of DCI to fix several problems. 
//
//  Revision  1.0	By: jdm	Date: 8/15/95	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

//@ Usage-Classes
//@ End-Usage


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  put (a Real32)
//
//@ Interface-Description
// Initialize the class object with a floating point value.  By definition,
// a value that is "put" is considered to be valid.
//---------------------------------------------------------------------
//@ Implementation-Description
// Save the numeric value and set the status valid.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

inline Real32
DciNumber::put(Real32 number)
{
	setOk();	// tag the value as valid

	// $[TI1]
	return (number_ = number);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  put (a Int32)
//
//@ Interface-Description
// Initialize the class object with an integer value.  By definition,
// a value that is "put" is considered to be valid.
//---------------------------------------------------------------------
//@ Implementation-Description
// Save the numeric value as a float and set the status valid.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

inline Real32
DciNumber::put(Int32 number)
{
	setOk();	// tag the value as valid

	// $[TI1]
	return (number_ = (Real32) number);
}

#endif	// DciNumber_IN
