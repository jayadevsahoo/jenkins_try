/*****************************************************************************/
/*   Module Name:    regpwd.h                                        
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _REGPWD
#define _REGPWD

enum PWD_Errors{
	PWD_UNKNOWN_ERROR=-1,
	PWD_OK=0,
	PWD_SET_ERROR,
	PWD_NOT_FOUND,
	PWD_MAX_USERS,
	PWD_USER_EXISTS,
	PWD_BAD_PASSWORD,
};

extern char NameListDelimiter[];

extern int 	CurrUserCount(void);
extern int 	AddUserToRegList(char* UserName);
extern int 	UserInRegList(char* UserName);
extern void GetPasswdFromReg(char* UserName, char* PassWord);
extern int 	GetPasswdForUser(char* UserName, char* PassWord);
extern int 	CreateUserKey_n_Passwd(char* UserName, char* PassWord);
extern int 	AddUser(char* UserName, char* PassWord);
extern int 	DeleteUserFromList(char* UserName);
extern void DeleteUserKey(char* UserName);
extern void DeleteUser(char* UserName);
extern int 	ChangeUserPasswd(char* UserName, char* PassWord);
extern char* GetUserList(void);
extern int  ValidatePassword(char* UserName, char* PassWord);

#endif // #ifndef _REGPWD
