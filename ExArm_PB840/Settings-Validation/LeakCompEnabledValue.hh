
#ifndef LeakCompEnabledValue_HH
#define LeakCompEnabledValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2008, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  LeakCompEnabledValue - Values of the Leak Compensation 
//                                Enabled Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/LeakCompEnabledValue.hhv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: rhj    Date:  05-Aug-2008    SCR Number: 6435
//  Project: 840S
//  Description:
//		Initial version.
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct LeakCompEnabledValue
{
  //@ Type:  LeakCompEnabledValueId 
  // All of the possible values of the Leak Compensation Enabled Setting.
  enum LeakCompEnabledValueId
  {
    // Values of the range of Leak Compensation Enabled Setting...
    LEAK_COMP_DISABLED,
    LEAK_COMP_ENABLED,

    TOTAL_LEAK_COMP_ENABLED_VALUES
  };
};


#endif // LeakCompEnabledValue_HH 
