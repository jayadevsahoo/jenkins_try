#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TaskInfo - Used to store and retrieve Task data.
//---------------------------------------------------------------------
//@ Interface-Description
// The TaskInfo is a holder of task information and it is a manager of
// task's.
//---------------------------------------------------------------------
//@ Rationale
// The TaskInfo class was created to be used by the Task class, to hold
// information for each task that will run in the application.  It
// also serves to provide information to the running tasks, and for
// debugging.
//---------------------------------------------------------------------
//@ Implementation-Description
//  TaskInfo uses the information from TaskTable to initialize it's
//  private member data. The TaskInfo class provides member functions
//  related to task managing.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre and
//  post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/TaskInfo.ccv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number:1943/1958
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.  Added ANSI prototype for fpopt().
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================
#include "Sigma.hh"
#include "OsFoundation.hh" 
#include "TaskInfo.hh"
#include "TaskTable.hh"
#include "Sys_Init.hh"

#ifdef SIGMA_DEVELPOMENT
# include "Ostream.hh"
#endif // SIGMA_DEVELPOMENT
//TODO E600 this is used in enter() which shall be ported, obcoleted, or changed;
// anyway, will not be used any more. Remove when the TODO in ::enter() is resolved
//#include "fpopt.h"
#include "TaskFlags.hh"

//@ Usage-Classes
//@ End-Usage

// ==========
// class Statics
// ==========
Int32    TaskInfo::TaskCount_ = 0;
Boolean  TaskInfo::Initialized_ = FALSE;
 
// TaskInfo Object Memory
static char TaskInfoMemory[sizeof(TaskInfo) * (TaskInfo::MAX_NUM_TASKS + 1)];
TaskInfo* TaskInfo::PTaskInfoObjects_ = (TaskInfo*)::TaskInfoMemory;

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
// This is a static method to be called before tasking begins to
// initialize the Task class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each Task object in the PTaskInfoObjects array is constructed using
//  the corresponding TaskTableEntry entry in the TaskTable.
//---------------------------------------------------------------------
//@ PreCondition
//		Sys_Init::IsTaskingOn() == FALSE
//		TaskInfo is NOT initialized
//		TaskTable is initialize
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void 
TaskInfo::Initialize(void) 
{
    CLASS_PRE_CONDITION(Sys_Init::IsTaskingOn() == FALSE);
    CLASS_PRE_CONDITION(TaskInfo::Initialized_ == FALSE);
    CLASS_PRE_CONDITION( TaskTable::IsInitialized() );
    SAFE_CLASS_ASSERTION( (sizeof(::TaskInfoMemory) 
        >= (sizeof(TaskInfo) * (TaskInfo::MAX_NUM_TASKS+1))));

    // Create a Task object for each potential task
    for (Int32 tIdx=1; tIdx <= TaskTable::GetNumEntries(); tIdx++)
    {
        CLASS_ASSERTION( TaskInfo::IsValidIndex_(tIdx) );
        (void)new (&TaskInfo::PTaskInfoObjects_[tIdx]) 
                      TaskInfo(tIdx, TaskTable::GetTaskTableEntry(tIdx));
        ++TaskCount_;
    }
    TaskInfo::Initialized_ = TRUE;	// $[TI1]
}

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskInfo() [constructor]
//
//@ Interface-Description
//  Takes a constant Int32 taskId and a constant reference tableEntry as
//  input and initializes private data from task table information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes TaskInfo object associated with the passed in taskIndex
//  with data from TaskTable. taskIndex is an internal ID, it can be assigned
//  as the actual thread ID for certain RTOS like VRTX
//---------------------------------------------------------------------
//@ PreCondition
//  TaskTable::IsInitialized() == TRUE
//  TaskInfo::IsValidIndex_( taskIndex )
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
TaskInfo::TaskInfo(const Int32            taskIndex,
                   const TaskTableEntry&  tableEntry)
{
    CLASS_PRE_CONDITION( TaskTable::IsInitialized() );
    CLASS_PRE_CONDITION( TaskInfo::IsValidIndex_( taskIndex ) );

	//this is the internal index assigned by us to reference the task.
	//it can be the task ID as well for certain RTOSs. If this is how
	//the OS works, then assign this ID to the osTaskId and use it as
	//a task (thread) ID.
    taskIdx_        = taskIndex;
	//this is the thread ID assigned (returned) by the OS (if applicable)
	osTaskId_		= NULL_TASK_ID;
	pThread_		= NULL;
    state_          = ::TASK_INACTIVE;
    pName_          = tableEntry.taskName;
    entryPoint_     = tableEntry.entryPoint;
    basePriority_   = tableEntry.priority;
    pStack_         = tableEntry.pStack;
    stackSize_      = tableEntry.stackSize;
    flags_          = tableEntry.flags;
    queue_          = tableEntry.taskIpq;
    monitorPeriod_  = tableEntry.monitorPeriod;
    monitorQueue_   = tableEntry.monitorQueue;
  						// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Create()
//
//@ Interface-Description
//  create() creates the task using the parameters set during
//  TaskInfo object construction.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Ported to the actual used OS to create the OS task.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
TaskInfo::Create()
{
//If ported to WinCE or WIN32 PC app
#if ( defined(_WIN32_WCE) || defined(WIN32) )
	//These parameters are added to fit the arg of the Windows CreateThread()
	LPSECURITY_ATTRIBUTES lpsa = NULL; //not used
	LPVOID lpvThreadParam = NULL; //not used
	//the thread will be created with an auto-start option. If desired to create
	//with suspended state, add CREATE_SUSPENDED to the flags, and later call
	//Resume() for the TaskInfo object of this thread.
	DWORD fdwCreate = STACK_SIZE_PARAM_IS_A_RESERVATION;
	DWORD  thrdId;

	HANDLE thrdHandle = CreateThread(lpsa,
									stackSize_,
									reinterpret_cast<LPTHREAD_START_ROUTINE>(entryPoint_),
									lpvThreadParam,
									fdwCreate,
									&thrdId);

	//Save the thread ID that the OS assigned to this thread.
	if(thrdHandle != NULL)
	{
		//this state is not the actual returned state from the OS call.
		//It is just an internal enum, and is really useless!! an OS call
		//should be made to know the actual state of a thread.
		state_ = TASK_RUNNING;	//if created with SUSPEND option, remove this
		osTaskId_ = static_cast<Uint32>(thrdId);
		pThread_ = (Uint32*)thrdHandle;
		DEBUGMSG( FALSE, (_T("\nSuccessfully created Thread #%i   assigned ID: %u \n"), this->GetIndex(), osTaskId_) );
		if(SetThreadPriority_(basePriority_) == FALSE)
		{
			DEBUGMSG( FALSE, ( _T("\nFailed to set thread %i priority, error code: %u \n"), this->GetIndex(), (Uint32)GetLastError() ) );
		}	
	}
	else
	{
		DEBUGMSG( FALSE, ( _T("\nFailed to create Thread #%i, error code: %u \n"), this->GetIndex(), (Uint32)GetLastError() ) );
	}
#endif //WinCE or Win32
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Resume()
//
//@ Interface-Description
//  This method is called by the current task thread to Resume the task
//	thread of "this" TaskInfo object. The caller has to have a pointer
//	to a TaskInfo object related to (or contains the) info of a thread.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method calls the corresponding ported OS resume function for
//  the thread handle or ID defined in this TaskInfo object. The caller
//	thread gets a pointer to the TaskInfo of the intended thread to be
//	resumed (see GetTaskInfo(idx)) and call its Resume()
//---------------------------------------------------------------------
//@ PreCondition
//		Sys_Init::IsTaskingOn() == TRUE
//		TaskInfo is initialized
//		Task ID is initialized && Task Handle is initialized
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
TaskInfo::Resume(void)
{
	CLASS_PRE_CONDITION(Sys_Init::IsTaskingOn() == TRUE);
    CLASS_PRE_CONDITION(TaskInfo::Initialized_ == TRUE);
	CLASS_PRE_CONDITION( (osTaskId_ != NULL_TASK_ID) &&
						 (pThread_ != NULL) )
//If ported to WinCE or WIN32 PC app
#if ( defined(_WIN32_WCE) || defined(WIN32) )
	if(ResumeThread(pThread_) == 0xffffffff)
	{
		DEBUGMSG( FALSE, ( _T("\nFailed to resume thread %i, error code: %u \n"), this->GetIndex(), (Uint32)GetLastError() ) );
	}
	else
	{
		state_ = TASK_RUNNING;	//assuming it was suspended.. 
	}
#endif //WinCE or Win32
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MapOsPriority()
//
//@ Interface-Description
//  Helper function to map an actual task priority number from the
//	internal range to the applicable OS thread priority number.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      valid priority range
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
TaskInfo::MapOsPriority(Int32 priority)
{
	CLASS_PRE_CONDITION(priority >= TaskInfo::HIGHEST_PRIORITY &&
						priority <= TaskInfo::LOWEST_PRIORITY)
	Int32 osPriority = INVALID_PRIORITY;//internal priorit error, not OS specific..
#if defined(_WIN32_WCE)
	//Only for WinCE/Embedded, assign the actual priority because WinCE takes
	//an actual number from 0 to 255 real-time thread priorities
	osPriority = priority;
#elif defined(WIN32)
	if(priority == LOWEST_PRIORITY_NOT_IDLE)
	{
		osPriority = THREAD_PRIORITY_LOWEST;
	}
	else if(priority == HIGHEST_PRIORITY)
	{
		osPriority = THREAD_PRIORITY_HIGHEST;
	}
	else if(priority == IDLE_PRIORITY)
	{
		osPriority = THREAD_PRIORITY_IDLE;
	}
	//ignore all other values for Win32 and consider them NORMAL
	//priority. Or keep mapping the value to a Windows enum!
	else
	{
		osPriority = THREAD_PRIORITY_NORMAL;
	}
#endif

	return osPriority;
}

#ifdef SIGMA_DEVELOPMENT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free-Function: operator << ()
//
//@ Interface-Description
//      This method allows a TaskInfo object to be put into
//      a cout statement and print the class' fields to stdout.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Ostream & operator<<(Ostream &os, const TaskInfo &rNode)
{
    os << dec
       <<  "TASK:ID=" << (int)rNode.getId()
       <<  ":Name=" << rNode.getName();

    if (rNode.getId() > 0)
    {
        os <<  ":Fptr=" << (void *)rNode.getEntryPtr()
           <<  ":Pri=" << rNode.getPriority()
           <<  ":Dbg=" << (void *)rNode.getDebugFlags()
           <<  ":St=" << rNode.getState();
    }
    return os;
}

#endif  // SIGMA_DEVELOPEMENT


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
TaskInfo::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::TASK_INFO,
                          lineNumber, pFileName, pPredicate);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetThreadPriority_() [private]
//
//@ Interface-Description
//  Helper function (do not attempt to make an API and get confused by a Task
//  SetPriority).
//	Sets the priority of a thread created for (or from) "this" TaskInfo object.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The Creat function calls it after it created a thread for this TaskInfo object
//	Returns TRUE if success
//---------------------------------------------------------------------
//@ PreCondition
//		tasking is on
//      thread handle != NULL
//      valid priority range
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
TaskInfo::SetThreadPriority_(Int32 priority)
{
	CLASS_PRE_CONDITION( Sys_Init::IsTaskingOn() )
	CLASS_PRE_CONDITION(pThread_ != NULL)
    CLASS_PRE_CONDITION(priority >= TaskInfo::HIGHEST_PRIORITY &&
						priority <= TaskInfo::LOWEST_PRIORITY)
#if defined(_WIN32_WCE)
	//Only for WinCE/Embedded, assign the actual priority because it take from 0 to 255
	//real-time thread priorities.
	return (CeSetThreadPriority(pThread_, priority)?1:0);
#elif defined(WIN32)
	Int32 win32Priority = MapOsPriority(priority);
	return (SetThreadPriority(pThread_, win32Priority)?1:0);
#endif
}
