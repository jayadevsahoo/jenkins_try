#include "stdafx.h"

//======================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//======================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SettingsXaction - Settings Transaction Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  There are two "faces" of this class.  This class is "split" across the
//  two CPU boards such that the actual transmission of settings from the
//  GUI board to the BD board is done in the internals of this class -- the
//  transmission is transparent to the users of this class.  By encapsulating
//  the "distributiveness" of this class, within this class, all of the
//  implementation details of the transmission (e.g., the data packaging,
//  the "handshakes") is kept within a single source file.  This Client/Server
//  role, also, allows easier testing of the communication interface in a
//  "common" (i.e., NOT across CPUs; hence, 'SIGMA_COMMON' conditional
//  compilation) executable.
//
//  Each CPU has a Settings Xaction Task, that conducts the transactions.
//  The transactions are only initiated on the GUI side, by one of two
//  methods:  upon the acceptance of new BD settings, the Accepted Context
//  calls this class' 'Initiate()' method, which posts a message to the
//  Settings Xaction queue; and changes in Task-Control states can cause
//  the Settings Xaction task to initiate a transaction, itself.
//
//  Following each transaction attempt, a status that indicates whether the
//  transaction succeeded, or not, is posted onto the GUI-Apps' queue.  If
//  a transaction failure occurs while communication stays "up", the Settings
//  Xaction task will continuously re-attempt the transaction -- posting
//  its status to the GUI-Apps' queue each time.
//
//  This class is available -- with different interfaces -- on both the
//  Breath-Delivery and Graphical User Interface CPUs.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for the conductance of a Settings' Transaction.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no non-static members, and, as such, cannot be instantiated.
//  All interactions are via this class' static methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingsXaction.ccv   25.0.4.0   19 Nov 2013 14:27:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 024  By:  mnr    Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Changed to invalidate "same patient" batch if NeoMode Advanced 
//		or Lockout is not enabled and Vt, Apnea Vt or IBW are lower 
//		than allowed.
//	
//  Revision: 023  By:  gdc    Date:  18-Feb-2009    SCR Number: 6479
//  Project:  840S
//  Description:
//		Changed to invalidate "same patient" batch if leak comp is 
//		enabled and the leak comp option has been disabled.
//
//  Revision: 022  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 021  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 020  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//		Removed DELTA project single screen support.
//
//  Revision: 019   By: rhj   Date:  20-Oct-2008    SCR Number: 6435
//  Project:  S840
//  Description:
//      Added Leak Comp Software option under BdReadyCallback_().
// 
//  Revision: 018  By: sah    Date:  11-Nov-2000    DR Number: 5789
//  Project:  VTPC
//  Description:
//      As part of this DCS, fixing testable item discrepencies found
//      during unit test review.
//
//  Revision: 017   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//	PAV project-related changes:
//	*  added disabling of Same-Patient values when spontaneous type
//         is PA when PAV option is disabled
//
//  Revision: 016  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//	*  added disabling of Same-Patient values when mandatory type
//         is VC+, or when spontaneous type is VS, while the VTPC option
//         is disabled
//
//  Revision: 015   By: gdc    Date: 29-Aug-2000    DR Number: 5753
//  Project:  Delta
//  Description:
//	Added handling of SINGLE_SCREEN option in BdReadyMessage.
//
//  Revision: 014   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 013   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  added disabling of Same-Patient values when circuit type
//         mismatches available options
//
//  Revision: 012   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  incorporated new 'SettingOptions' functionality, and added
//	   checking of ATC option against same-patient values
//
//  Revision: 011   By: dosman Date: 16-Jun-1998    DR Number: 120
//  Project:  BILEVEL
//  Description:
//	added check in bdReadyCallback() such that 
//	if same patient settings are available in novram,
//	and if the mode stored in novram is BILEVEL,
//	and if the data key does not enable BILEVEL, then 
//	the same patient settings in novram are no longer valid and no longer available
//
//  Revision: 010   By: dosman Date: 30-Apr-1998    DR Number: 4
//  Project:  BILEVEL
//  Description:
//	removed the check of whether bilevel is enabled: instead of doing
//	the check early on, it is done when it is needed
//
//  Revision: 009   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 008   By: dosman Date: 18-Feb-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//      Initial BiLevel version.
//      changed BdReadyCallback() to include checking whether bilevel
//      is enabled based on the data key, and if it is not, then
//      take appropriate action so that the choice will not appear
//      on the mode button
//  
//  Revision: 007   By: sah    Date: 20-Nov-1997    DR Number: 5002
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.50.1.0) into Rev "BiLevel" (1.50.2.0)
//	Modified 'ChangeStateCallback()' to initiate a transaction when
//	in SST mode.
//  
//  Revision: 006   By: sah    Date: 08-Oct-1997    DR Number: 2541
//  Project: Sigma (R8027)
//  Description:
//	Changed calls of 'NovRamManager::LogSystemDiagnostic()' to calls
//	of 'FaultHandler::LogDiagnosticCode()', so that 'FaultHandler' can
//	"decide" which log these diagnostics go into.
//  
//  Revision: 005   By: sah    Date: 05-Aug-1997    DR Number: 2347
//  Project: Sigma (R8027)
//  Description:
//	Changed the initiation of the initial "synchronization" transaction
//	from the BD-Ready callback to the Change-State callback, so as
//	to reduce the amount of communications traffic following initial
//	communication establishment.
//  
//  Revision: 004   By: sah    Date: 14-Jul-1997    DR Number: 2263
//  Project: Sigma (R8027)
//  Description:
//	Modified mechansism that initiates transactions and monitors
//	failed transactions to eliminate race condition.
//  
//  Revision: 003   By: sah    Date: 15 May 1997    DR Number: 2109
//  Project: Sigma (R8027)
//  Description:
//	Added tracing of requirement [00517] to 'ChangeStateCallback_()'.
//  
//  Revision: 002   By: sah    Date: 04 Feb 1997    DR Number: 1752
//  Project: Sigma (R8027)
//  Description:
//	Fixed problem where failed transaction due to BD-Ready message
//	was not re-tried.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "SettingsXaction.hh"
#include "SigmaState.hh"
#include "SettingXmitId.hh"

// TODO E600 added this to use memcmp
#include <memory.h>
//@ Usage-Classes

#include "AppContext.hh"
#include "ChangeStateMessage.hh"
#include "MsgQueue.hh"
#include "NetworkApp.hh"
#include "NovRamManager.hh"
#include "Task.hh"
#include "TaskControlAgent.hh"
#include "TaskControlQueueMsg.hh"
#include "TaskMonitor.hh"
#include "TaskMonitorQueueMsg.hh"
#include "SettingConstants.hh"

#if defined(SIGMA_DEVELOPMENT)
	#include "Ostream.hh"
#endif // defined(SIGMA_DEVELOPMENT)

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
	#include "AcceptedContext.hh"
	#include "AppContext.hh"
	#include "BatchSettingValues.hh"
	#include "BdReadyMessage.hh"
	#include "ContextMgr.hh"
	#include "DiagnosticCode.hh"
	#include "FaultHandler.hh"
	#include "LeakCompEnabledValue.hh"
	#include "MandTypeValue.hh"
	#include "ModeValue.hh"
	#include "PatientCctTypeValue.hh"
	#include "ServiceMode.hh"
	#include "SettingContextHandle.hh"
	#include "SettingsMgr.hh"
	#include "SoftwareOptions.hh"
	#include "SupportTypeValue.hh"
	#include "UserAnnunciationMsg.hh"
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
	#include "ContextMgr.hh"
	#include "PendingContext.hh"
	#include "Post.hh"

    #ifdef SIGMA_DEBUG
        #include <syslog.h>
    #endif

#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================

#if defined(SIGMA_COMMON)
SettingXactionBlock*  PInitXactionBlock_ = NULL;
#endif // defined(SIGMA_COMMON)

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

SettingsXactionId  SettingsXaction::FailedXactionId_ =
	::SETTINGS_XACTION_COMPLETE;

//caution: this value is based on the fact that our socket APIs return -1 for invalid
//sockets or failure. It does not necessarily represent the OS value for invalid socket
const Int  SettingsXaction::INVALID_SOCKET_ID_ = -1;

// NOTE:  MRI compiler won't let me use 'CalcXactionBlockSize_()' here...
const Uint  SettingsXaction::MAX_ELEMS_BLOCK_SIZE_ =
	((Uint)&(((SettingXactionBlock*)NULL)->arrXmitElems[::NUM_BD_SETTING_IDS]));

// NOTE:  MRI compiler won't let me use 'CalcXactionBlockSize_()' here...
const Uint  SettingsXaction::ZERO_ELEMS_BLOCK_SIZE_ =
	((Uint)&(((SettingXactionBlock*)NULL)->arrXmitElems[0u]));

const Uint  SettingsXaction::BLOCK_RECV_WAIT_TIME_ = 600u
#if defined(SIGMA_DEVELOPMENT)
													 * 2
#endif // defined(SIGMA_DEVELOPMENT)
													 ; // milliseconds...


#if defined(SIGMA_UNIT_TEST)
// used by Unit Tests to turn on/off the initiation of a Settings'
// Transaction...
Boolean  SettingsXaction::DoInitiateXaction = FALSE;
#endif // defined(SIGMA_UNIT_TEST)

#ifdef SIGMA_DEBUG
 
// DEBUG
#if defined(SIGMA_GUI_CPU) 
Uint SettingsXaction::FailSettingsTransaction = FALSE;
#endif

#endif
//=====================================================================
//
//  Static Functions...
//
//=====================================================================

inline Boolean
	DoInfiniteLoop_(void)
{
	// by using this function in the infinite loop below, I avoid a warning --
	// pretty anal-retentive, huh?...
	return(TRUE);
}	// $[TI1]


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskDriver()
//
//@ Interface-Description
//  Task loop for the Settings Transaction.  It is this thread in which
//  all of the Settings' Transactions occur.  This task responds to both
//  Task-Control and Task-Monitor messages, and to its internal messages
//  relating to the initiation of a transaction.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When a transaction fails while communications is still up, the info
//  regarding the failed transaction is saved, and a time-limited pend
//  on this task's queue is done.  There are really three different ways
//  for the failure response to be implemented:  do a delay before trying
//  the same transaction again (without pending on the task's queue); put
//  the message back on the task's queue, to try again; or store the info
//  and do a time-limited pend on the task's queue (which is what is being
//  done).  The reason the time-limited pend is used, as opposed to the
//  other two, is because this approach eliminates any negative side-effects.
//  If the "delay" technique were used, responses task monitor and task
//  control messages would be delayed with each failure.  Whereas,  the
//  putting of the message back on the queue corrupts the order of the
//  incoming transaction requests (e.g., you could get a user-update
//  event before that vent-startup event).  The time-limited pend, on the
//  other hand, solves both of these negative side-effects.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::TaskDriver(void)
{
	CALL_TRACE("TaskDriver()");

#if defined(SIGMA_BD_CPU)
	// register for the opening of the Settings-Transaction Socket...
	NetworkApp::RegisterForOpenSocket(&SettingsXaction::EstablishConnection_);
#endif // defined(SIGMA_BD_CPU)

	static AppContext  AppContext_;

#if defined(SIGMA_GUI_CPU)
	AppContext_.setCallback(SettingsXaction::BdReadyCallback_);
#endif // defined(SIGMA_GUI_CPU)
	AppContext_.setCallback(SettingsXaction::ChangeStateCallback_);

	MsgQueue          settingsXactionQ(SETTINGS_XACTION_Q);
	InterTaskMessage  msg ={0};
	Int32             qWord, rval;
	Uint32            waitTime = (Uint32)Ipc::NO_TIMEOUT;

	while ( ::DoInfiniteLoop_() )
	{	// $[TI1] -- loop forever; this path is ALWAYS taken...
		// wait for the next event on the BD Server Task Queue...
		rval = settingsXactionQ.pendForMsg(qWord, waitTime);

		if ( rval == Ipc::OK )
		{	// $[TI1.1] -- got a new queue message...
			msg = *((struct InterTaskMessage*)&qWord);
		}
#if defined(SIGMA_GUI_CPU)
		else if ( rval == Ipc::TIMED_OUT )
		{	// $[TI1.2] -- timeout occurred (GUI only)...
			// timeout should only occur when a previous transaction failed, and
			// that transaction's information is stored in 'FailedXactionId_'...
			AUX_CLASS_ASSERTION(
							   (SettingsXaction::FailedXactionId_ != ::SETTINGS_XACTION_COMPLETE),
							   (Uint)SettingsXaction::FailedXactionId_
							   );

			// setup 'msg' to initiate another transaction attempt...
			SettingsXaction::SetupQueueMsg_(msg, ::INITIATE_TRANSACTION,
											SettingsXaction::FailedXactionId_);
		}
#endif // defined(SIGMA_GUI_CPU)
		else
		{
			// unexpected error...
			AUX_CLASS_ASSERTION_FAILURE(rval);
		}

		switch ( msg.msgId )
		{
			case TaskControlQueueMsg::TASK_CONTROL_MSG :
			case TaskMonitorQueueMsg::TASK_MONITOR_MSG :	// $[TI1.3]
				// the received message is a task-control or task-monitor message,
				// therefore activate the dispatch of this message...
				AppContext_.dispatchEvent(qWord);
				break;

			case SettingsXaction::XACTION_INITIATION_MSG :	// $[TI1.4]
				{
					const SettingsXactionId  INITIATION_ID =
						SettingsXaction::GetXactionState_(msg);

#if defined(SIGMA_GUI_CPU)
					//-------------------------------------------------------------
					//  BEGIN:  GUI-only block of code...
					//-------------------------------------------------------------

					SAFE_FREE_ASSERTION((INITIATION_ID == ::INITIATE_TRANSACTION),
										SETTINGS_VALIDATION, SETTINGS_XACTION);

					// save before "clearing"...
					const SettingsXactionId  XACTION_ID =
						(SettingsXactionId)SettingsXaction::GetXactionInfo_(msg);

					Boolean  isXactionSuccessful;

					// begin a Settings' Transaction...
					isXactionSuccessful = SettingsXaction::BeginClientXaction_(
																			  XACTION_ID
																			  );

					if ( !isXactionSuccessful  &&
						 TaskControlAgent::GetPeerState() != ::STATE_UNKNOWN )
					{	// $[TI1.4.1] -- the transaction failed, but comm is still up...
						// store failed transaction ID, for new attempts...
						SettingsXaction::FailedXactionId_ = XACTION_ID;

						// pend on the queue for the blocked-receive timeout time...
						waitTime = SettingsXaction::BLOCK_RECV_WAIT_TIME_;
					}
					else
					{	// $[TI1.4.2] -- the transaction succeeded, OR comm is down...
						// ensure this failure flag is clear...
						SettingsXaction::FailedXactionId_ = ::SETTINGS_XACTION_COMPLETE;

						// set back to indefinite pending...
						waitTime = Ipc::NO_TIMEOUT;
					}

					//-------------------------------------------------------------
					//  END:  GUI-only block of code...
					//-------------------------------------------------------------
#elif defined(SIGMA_BD_CPU)
					//-------------------------------------------------------------
					//  BEGIN:  BD-only block of code...
					//-------------------------------------------------------------

					SAFE_FREE_ASSERTION((INITIATION_ID == ::CONNECTION_INITIATED),
										SETTINGS_VALIDATION, SETTINGS_XACTION);

					const Int  XACTION_SOCKET_ID = SettingsXaction::GetXactionInfo_(msg);

					// a connection is being initiated on the Settings-Transaction
					// Socket...
					SettingsXaction::BeginServerXaction_(XACTION_SOCKET_ID);

					//-------------------------------------------------------------
					//  END:  BD-only block of code...
					//-------------------------------------------------------------
#endif // defined(SIGMA_GUI_CPU)
				}	// end of 'SettingsXaction::XACTION_INITIATION_MSG' block...
				break;
			default :
				// 'msg.msgId' is an invalid message ID...
				AUX_CLASS_ASSERTION_FAILURE(msg.msgId);
				break;
		};
	}	// end of infinite 'while' loop...

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							SETTINGS_XACTION, lineNumber, pFileName,
							pPredicate);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeStateCallback_(changeStateMsg)
//
//@ Interface-Description
//  Callback used to respond to the task-control state changes.  This
//  callback is responsible for notifying Sys-Init when this, the Settings'
//  Xaction Task, is ready to transition to the proposed state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[00517] -- New-Patient Setup required after entrance into Service Mode
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::ChangeStateCallback_(
										 const ChangeStateMessage& changeStateMsg
										 )
{
	CALL_TRACE("ChangeStateCallback_(changeStateMsg)");

	switch ( changeStateMsg.getState() )
	{
		case STATE_SERVICE :	  // $[TI1]
			// set the Patient-Settings-Avail Flag to 'FALSE', because a
			// New-Patient Setup is required following entrance into Service Mode...
			NovRamManager::UpdatePatientSettingsFlag(FALSE);

#if defined(SIGMA_GUI_CPU)
			// fall through...
		case STATE_SST :
		case STATE_ONLINE :		  // $[TI2]
			if ( NovRamManager::AreBatchSettingsInitialized() )
			{	// $[TI2.1] -- the GUI has batch settings, therefore sync with BD...
				SettingsXaction::Initiate(::ONLINE_SYNC_XMITTED);
			}	// $[TI2.2] -- the GUI has no batch settings, therefore no sync...
#endif // defined(SIGMA_GUI_CPU)
			break;

#if defined(SIGMA_BD_CPU)
		case STATE_SST :
		case STATE_ONLINE :
#endif // defined(SIGMA_BD_CPU)
		case STATE_INOP :
		case STATE_INIT :
		case STATE_START :
		case STATE_TIMEOUT :	  // $[TI3]
			// do nothing...
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(changeStateMsg.getState());
			break;
	};

	// report back that Settings is ready to change to the given state...
	TaskControlAgent::ReportTaskReady(changeStateMsg);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessFailedXmit_(xactionSocketId)
//
//@ Interface-Description
//  This method processes the failure of an attempted transmission during
//  the Settings' Transaction.  This handles the logging of the failure, and
//  the "cleanup" for the next attempt.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::ProcessFailedXmit_(const Int xactionSocketId)
{
	CALL_TRACE("ProcessFailedXmit_(xactionSocketId)");

	NetworkApp::BCloseSocket(xactionSocketId);

#if defined(SIGMA_GUI_CPU)
	SettingsXaction::ReportXactionStatus_(SettingsXaction::XMIT_FAILURE);
#endif // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_DEVELOPMENT)
	if ( Settings_Validation::IsDebugOn(::SETTINGS_XACTION) )
	{
		cout << "TRANSMIT FAILURE!" << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessFailedRecv_(xactionSocketId)
//
//@ Interface-Description
//  This method processes the failure of an attempted receival, during
//  the Settings' Transaction.  This handles the logging of the failure, and
//  the "cleanup" for the next attempt.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::ProcessFailedRecv_(const Int xactionSocketId)
{
	CALL_TRACE("ProcessFailedRecv_(xactionSocketId)");
	CLASS_ASSERTION((xactionSocketId != SettingsXaction::INVALID_SOCKET_ID_));

	SettingXactionBlock  nackSendBlock;

	nackSendBlock.xactionId    = ::NACK_OR_ERROR;
	nackSendBlock.numXmitElems = 0u;

#if defined(SIGMA_UNIT_TEST)
	if ( FALSE )
	{  // when I Unit Test this class, I don't want to do this transmission...
#endif // defined(SIGMA_UNIT_TEST)

		XmitSettingXactionBlockMsg_(xactionSocketId, ::SETTINGS_TRANSACTION_MSG,
							 &nackSendBlock, ZERO_ELEMS_BLOCK_SIZE_);

#if defined(SIGMA_UNIT_TEST)
	}
#endif // defined(SIGMA_UNIT_TEST)

	NetworkApp::BCloseSocket(xactionSocketId);

#if defined(SIGMA_GUI_CPU)
	SettingsXaction::ReportXactionStatus_(SettingsXaction::RECV_FAILURE);
#endif // defined(SIGMA_GUI_CPU)

#if defined(SIGMA_DEVELOPMENT)
	if ( Settings_Validation::IsDebugOn(::SETTINGS_XACTION) )
	{
		cout << "RECEIVE FAILURE!" << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)
}	// $[TI1]


#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

//=====================================================================
//
//  GUI-Only Methods...
//
//=====================================================================

//=====================================================================
//
//  GUI-Only Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initiate(xactionId)
//
//@ Interface-Description
//  This method is used by the Accepted Context to initiate a Settings'
//  Transaction due to an update of the accepted settings.  This method
//  notifies the Settings Xaction Task to conduct the transaction, passing
//  'xactionId' as an identifier as to the type of acceptance (vent-startup,
//  or not) that occurred.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If this method is called when a previously-initiated transaction hasn't
//  yet completed, the currently requested transaction ID will not be
//  saved in the static data member if a Vent-Startup is requested.  This
//  is because Vent-Startup transmissions have special processing on the
//  BD CPU (see 'PendingContext'), whereas the other transaction types
//  don't.
//
//  This method of this class is run in the GUI Task thread.
//---------------------------------------------------------------------
//@ PreCondition
//  (xactionId == ::USER_UPDATE_XMITTED  ||
//   xactionId == ::ONLINE_SYNC_XMITTED  ||
//   xactionId == ::VENT_STARTUP_UPDATE_XMITTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::Initiate(const SettingsXactionId xactionId)
{
	CALL_TRACE("Initiate(xactionId)");
	AUX_CLASS_PRE_CONDITION((xactionId == ::USER_UPDATE_XMITTED  ||
							 xactionId == ::ONLINE_SYNC_XMITTED  ||
							 xactionId == ::VENT_STARTUP_UPDATE_XMITTED),
							xactionId);

#if defined(SIGMA_GUI_CPU)

	if ( SettingsXaction::FailedXactionId_ == ::SETTINGS_XACTION_COMPLETE )
	{	// $[TI1] -- there is no failed transaction pending...
		MsgQueue          settingsXactionQ(SETTINGS_XACTION_Q);
		InterTaskMessage  beginXactionMsg;

		SettingsXaction::SetupQueueMsg_(beginXactionMsg, ::INITIATE_TRANSACTION,
										xactionId);

#if defined(SIGMA_UNIT_TEST)
		if ( SettingsXaction::DoInitiateXaction )
		{  // may not post to the queue during Unit Testing...
#endif // defined(SIGMA_UNIT_TEST)

			// post the transaction request to the setting transaction queue...
			settingsXactionQ.putMsg(*((Int32*)&beginXactionMsg));

#if defined(SIGMA_UNIT_TEST)
		}
#endif // defined(SIGMA_UNIT_TEST)
	}	// $[TI2] -- there is a failed transaction pending; do nothing...

#elif defined(SIGMA_COMMON)

	SettingsXaction::BeginClientXaction_(xactionId);

#endif // defined(SIGMA_GUI_CPU)
}


//=====================================================================
//
//  GUI-Only Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdReadyCallback_(bdReadyMsg)
//
//@ Interface-Description
//  Respond to the BD-Ready message given in 'bdReadyMsg'.  For 'ONLINE'
//  and 'SERVICE' messages, a transaction trasmitting the current GUI
//  settings may be initiated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When reacting to an online state (when there are patient settings
//  available), a call to 'Initiate()' is made instead of a direct call
//  to 'BeginClientXaction_()', because by going through the task queue
//  failures of the transaction will be logged and re-tried properly.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::BdReadyCallback_(const BdReadyMessage& bdReadyMsg)
{
	CALL_TRACE("BdReadyCallback_(bdReadyMsg)");

	//-----------------------------------------------------------------
	//  NOTE:  this method is NOT explicitly tested during Unit Tests,
	//	     because of the difficulty in setting up the various
	//         scenarios.
	//-----------------------------------------------------------------

	switch ( bdReadyMsg.getState() )
	{
		case STATE_ONLINE :		  // $[TI1]
			{
				const Boolean  ARE_BD_PATIENT_SETTINGS_AVAIL =
					bdReadyMsg.areBdSettingsInitialized();
				const Boolean  ARE_GUI_BATCH_SETTINGS_AVAIL =
					NovRamManager::AreBatchSettingsInitialized();

				if ( ARE_BD_PATIENT_SETTINGS_AVAIL  &&
					 (!ARE_GUI_BATCH_SETTINGS_AVAIL  ||
					  !NovRamManager::ArePatientSettingsAvailable()) )
				{	// $[TI1.1] -- BD has patient settings, but GUI doesn't...
					DiagnosticCode  settingsInopDiagCode;

					// setup a diagnostic to indicate that there is a mismatch of the BD
					// and GUI settings flags (most likely due to "corrupt" NOVRAM); the
					// auxillary error code will tell me whether there are NO available
					// settings, or only the non-patient settings are available...
					settingsInopDiagCode.setSystemEventCode(
														   DiagnosticCode::SETTINGS_STATE_MISMATCH,
														   ARE_GUI_BATCH_SETTINGS_AVAIL
														   );

					// log this diagnostic to ensure tracking of why we're switching to
					// the INOP state...
					FaultHandler::LogDiagnosticCode(settingsInopDiagCode);

					// transition to an INOP state...
					TaskControlAgent::ChangeState(STATE_INOP);
				}
				else
				{
					// $[TI1.2] -- no problem...
					// notify the Accepted Context of BD's Safety-PCV state...
					ContextMgr::GetAcceptedContext().setBdSafetyPcvFlag(
																	   !ARE_BD_PATIENT_SETTINGS_AVAIL
																	   );
				} // endelse (no problem)

#if defined(SIGMA_GUI_CPU)
				if ( SettingContextHandle::IsSamePatientValid() )
				{  // $[TI1.3] -- stored same patient values are available...
					BatchSettingValues  samePatientValues;
					Boolean             samePatientValuesAreValid = TRUE;

					NovRamManager::GetAcceptedBatchSettings(samePatientValues);

					for ( Uint idx = 0u;
						samePatientValuesAreValid  &&  idx < SoftwareOptions::NUM_OPTIONS;
						idx++ )
					{
						const SoftwareOptions::OptionId  OPTION_ID =
							(SoftwareOptions::OptionId)idx;

						switch ( OPTION_ID )
						{
							case SoftwareOptions::BILEVEL :	  // $[TI1.3.1]
								if ( !SoftwareOptions::IsOptionEnabled(OPTION_ID) )
								{	// $[TI1.3.1.1] -- BiLevel option is NOT active...
									// it is invalid for mode to be set to 'BILEVEL'...
									const DiscreteValue  MODE_VALUE =
										samePatientValues.getDiscreteValue(SettingId::MODE);

									// $[TI1.3.1.1.1] (TRUE)  $[TI1.3.1.1.2] (FALSE)
									samePatientValuesAreValid =
										(MODE_VALUE != ModeValue::BILEVEL_MODE_VALUE);
								}	// $[TI1.3.1.2] -- BiLevel option is active...
								break;

							case SoftwareOptions::ATC :		  // $[TI1.3.2]
								if ( !SoftwareOptions::IsOptionEnabled(OPTION_ID) )
								{	// $[TI1.3.2.1] -- ATC option is NOT active...
									// it is invalid for support type to be set to 'ATC'...
									const DiscreteValue  SUPPORT_TYPE_VALUE =
										samePatientValues.getDiscreteValue(SettingId::SUPPORT_TYPE);

									// $[TI1.3.2.1.1] (TRUE)  $[TI1.3.2.1.2] (FALSE)
									samePatientValuesAreValid =
										(SUPPORT_TYPE_VALUE != SupportTypeValue::ATC_SUPPORT_TYPE);
								}	// $[TI1.3.2.2] -- ATC option is active...
								break;

							case SoftwareOptions::NEO_MODE :  // $[TI1.3.3]
								if ( !SoftwareOptions::IsOptionEnabled(OPTION_ID) )
								{	// $[TI1.3.3.1] -- NeoMode option is NOT active...
									// it is invalid for circuit type to be set to 'NEONATAL'...
									const DiscreteValue  CIRCUIT_TYPE_VALUE =
										samePatientValues.getDiscreteValue(SettingId::PATIENT_CCT_TYPE);

									// $[TI1.3.3.1.1] (TRUE)  $[TI1.3.3.1.2] (FALSE)
									samePatientValuesAreValid =
										(CIRCUIT_TYPE_VALUE != PatientCctTypeValue::NEONATAL_CIRCUIT);
								}	// $[TI1.3.3.2] -- NeoMode option is active...
								break;

							case SoftwareOptions::RESERVED1 : // $[TI1.3.4]
								// do nothing
								break;

							case SoftwareOptions::VTPC :	  // $[TI1.3.6]
								if ( !SoftwareOptions::IsOptionEnabled(SoftwareOptions::VTPC) )
								{	// $[TI1.3.6.1] -- VTPC option is NOT active...
									// it is invalid for mand type to be set to 'VC+'...
									const DiscreteValue  MAND_TYPE_VALUE =
										samePatientValues.getDiscreteValue(SettingId::MAND_TYPE);
									// it is invalid for support type to be set to 'VS'...
									const DiscreteValue  SUPPORT_TYPE_VALUE =
										samePatientValues.getDiscreteValue(SettingId::SUPPORT_TYPE);

									// $[TI1.3.6.1.1] (TRUE)  $[TI1.3.6.1.2] (FALSE)
									samePatientValuesAreValid =
										(MAND_TYPE_VALUE != MandTypeValue::VCP_MAND_TYPE  &&
										 SUPPORT_TYPE_VALUE != SupportTypeValue::VSV_SUPPORT_TYPE);
								}	// $[TI1.3.6.2] -- VTPC option is active...
								break;

							case SoftwareOptions::PAV :		  // $[TI1.3.7]
								if ( !SoftwareOptions::IsOptionEnabled(OPTION_ID) )
								{	// $[TI1.3.7.1] -- PAV option is NOT active...
									// it is invalid for support type to be set to 'PA'...
									const DiscreteValue  SUPPORT_TYPE_VALUE =
										samePatientValues.getDiscreteValue(SettingId::SUPPORT_TYPE);

									// $[TI1.3.7.1.1] (TRUE)  $[TI1.3.7.1.2] (FALSE)
									samePatientValuesAreValid =
										(SUPPORT_TYPE_VALUE != SupportTypeValue::PAV_SUPPORT_TYPE);
								}	// $[TI1.3.7.2] -- PAV option is active...
								break;

							case SoftwareOptions::NEO_MODE_ADVANCED :
								if( !SoftwareOptions::IsOptionEnabled(OPTION_ID) )
								{
									const BoundedValue TIDAL_VOLUME = 
										samePatientValues.getBoundedValue(SettingId::TIDAL_VOLUME);

									const BoundedValue APNEA_TIDAL_VOLUME = 
										samePatientValues.getBoundedValue(SettingId::APNEA_TIDAL_VOLUME);

									const BoundedValue IBW = 
										samePatientValues.getBoundedValue(SettingId::IBW);

									// [01008] If NeoMode Advanced is not enabled, check for proper
									//         Vt, Apnea Vt and IBW setting values
									samePatientValuesAreValid = 
										(  TIDAL_VOLUME.value >= DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC
										&& APNEA_TIDAL_VOLUME.value >= DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC
										&& IBW.value >= SettingConstants::IBW_ABSOLUTE_MIN_VALUE );
								}
								break;

							case SoftwareOptions::NEO_MODE_LOCKOUT :
							case SoftwareOptions::RESP_MECH :
							case SoftwareOptions::TRENDING :
								// do nothing
								break;

							case SoftwareOptions::LEAK_COMP :
								if ( !SoftwareOptions::IsOptionEnabled(OPTION_ID) )
								{	// leak comp option is NOT enabled...
									// it is invalid for leak comp to be enabled...
									const DiscreteValue  LEAK_COMP_ENABLED_VALUE =
										samePatientValues.getDiscreteValue(SettingId::LEAK_COMP_ENABLED);

									samePatientValuesAreValid =
										(LEAK_COMP_ENABLED_VALUE != LeakCompEnabledValue::LEAK_COMP_ENABLED);
								}
								break;

							case SoftwareOptions::NEO_MODE_UPDATE :
								if ( !SoftwareOptions::IsOptionEnabled(OPTION_ID) )
								{	// NeoMode Update option is NOT enabled...
									// it is invalid for mode to be set to 'CPAP'...
									const DiscreteValue  MODE_VALUE =
										samePatientValues.getDiscreteValue(SettingId::MODE);

									samePatientValuesAreValid =
										(MODE_VALUE != ModeValue::CPAP_MODE_VALUE);
								}
								break;


							case SoftwareOptions::NUM_OPTIONS :
							default :
								AUX_CLASS_ASSERTION_FAILURE(idx);
								break;
						}  // end of switch...
					}  // end of for...

					if ( !samePatientValuesAreValid )
					{  // $[TI1.3.4]
						// $[01008] -- if the previous patient settings mismatch the available
						//             options, then the previous patient settings shall be
						//             invalidated...
						NovRamManager::UpdatePatientSettingsFlag(FALSE);
					}  // $[TI1.3.5]
				}  // $[TI1.4] -- same-patient values are not available...
#endif // defined(SIGMA_GUI_CPU)
			} // end case (STATE_ONLINE)
			break;
		case STATE_SERVICE :
		case STATE_INOP :
		case STATE_FAILED :
		case STATE_SST :
		case STATE_INIT :
		case STATE_START :		  // $[TI2]
			// do nothing...
			break;
		case STATE_TIMEOUT :
		default :
			AUX_CLASS_ASSERTION_FAILURE(bdReadyMsg.getState());
			break;
	};
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BeginClientXaction_(initXactionId)
//
//@ Interface-Description
//  This method conducts the "client-side" transaction.  This is a 7-stage
//  transaction that results in ALL of the currently-accepted settings
//  being transmitted to the BDU.  A 'TRUE' is returned when successful,
//  while a 'FALSE' is returned when the transaction fails.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Only one attempt at completing the transaction is conducted before a
//  failure is reported.
//
//  $[02031] -- all BD settings shall be delivered to the BDU...
//---------------------------------------------------------------------
//@ PreCondition
//  (initXactionId == ::VENT_STARTUP_UPDATE_XMITTED ||
//   initXactionId == ::USER_UPDATE_XMITTED         ||
//   initXactionId == ::ONLINE_SYNC_XMITTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	SettingsXaction::BeginClientXaction_(const SettingsXactionId initXactionId)
{
	CALL_TRACE("BeginClientXaction_(initXactionId)");
	AUX_CLASS_PRE_CONDITION((initXactionId == ::USER_UPDATE_XMITTED  ||
							 initXactionId == ::ONLINE_SYNC_XMITTED  ||
							 initXactionId == ::VENT_STARTUP_UPDATE_XMITTED),
							initXactionId);

#if defined(SIGMA_DEVELOPMENT)
	const Boolean  IS_DEBUG_ON =
		Settings_Validation::IsDebugOn(::SETTINGS_XACTION);
#endif // defined(SIGMA_DEVELOPMENT)

	//===================================================================
	//  create the transaction block...
	//===================================================================

	SettingXactionBlock  initXactionBlock;

	// set up the transaction block with ALL of the accepted BD values...
	initXactionBlock.xactionId    = initXactionId;
	initXactionBlock.numXmitElems = ::NUM_BD_SETTING_IDS;
#ifdef SIGMA_DEBUG
 
#if defined(SIGMA_GUI_CPU)
    // DEBUG ONLY - Purposely fail settings transaction.
    if (FailSettingsTransaction > 0)
    {
       initXactionBlock.numXmitElems = ::NUM_BD_SETTING_IDS - 1;
       printf(  "Changed!!!!!\n\n" );
       FailSettingsTransaction --;
    }
#endif // defined(SIGMA_GUI_CPU)

#endif // define SIGMA_DEBUG
	BatchSettingValues  acceptedBatchValues;

	// get the currently-accepted batch setting values from NOVRAM...
	CLASS_PRE_CONDITION((NovRamManager::AreBatchSettingsInitialized()));
	NovRamManager::GetAcceptedBatchSettings(acceptedBatchValues);

	Uint  idx;

	//-------------------------------------------------------------------
	//  NOTE:  the 'settingId' fields of 'initXactionBlock' are NOT set
	//         because all transactions originating from the GUI send
	//         across ALL settings in their relative indexed location.
	//         Only external interfacing (e.g., EPT testing) need define
	//         the 'settingId' fields.  The message IDs indicate whether
	//         an internal transmission, or external transmission
	//         ('EXTERNAL_UPDATE_XMITTED') are sent.
	//-------------------------------------------------------------------

	// for ALL of the BD, bounded setting values...
	for ( idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
		idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++ )
	{
		const BoundedValue&  R_BOUNDED_VALUE =
			acceptedBatchValues.getBoundedValue((SettingId::SettingIdType)idx);

		// store the data of this setting into this array...
		initXactionBlock.arrXmitElems[idx].boundedPrec   =
			R_BOUNDED_VALUE.precision;
		initXactionBlock.arrXmitElems[idx].boundedValue  =
			R_BOUNDED_VALUE.value;
	}

	// the IDs are to be continuous between the different categories...
	SAFE_CLASS_ASSERTION((idx == SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE));

	// for ALL of the BD, discrete setting values...
	for ( idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
		idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++ )
	{
		const DiscreteValue  DISCRETE_VALUE =
			acceptedBatchValues.getDiscreteValue((SettingId::SettingIdType)idx);

		// store the data of this setting into this array...
		initXactionBlock.arrXmitElems[idx].discreteValue = DISCRETE_VALUE;
	}

	// to ensure that there are no new "categories" of BD settings...
	SAFE_CLASS_ASSERTION((idx == ::NUM_BD_SETTING_IDS));

#if defined(SIGMA_GUI_CPU)

	//===================================================================
	//  conduct transaction...
	//===================================================================

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "BEGIN TRANSACTION..." << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	SettingXactionBlock  recvBlock;
	Int                  recvBlockSize;

	Int  xmitSize;

	//===================================================================
	// OPEN Settings Transaction Socket...
	//===================================================================

	Int  xactionSocketId;

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<CLIENT>>  Opening connection:";
	}
#endif // defined(SIGMA_DEVELOPMENT)

	xactionSocketId = NetworkApp::BOpenSocket();

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "  socket ID = " << xactionSocketId << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	if ( xactionSocketId == SettingsXaction::INVALID_SOCKET_ID_ )
	{	// $[TI1]
		SettingsXaction::ReportXactionStatus_(
											 SettingsXaction::OPEN_SOCKET_FAILURE
											 );
        #ifdef SIGMA_DEBUG
            printf(  "Failed open socket!!!!\n" );
        #endif

		return(FALSE);
	}	// $[TI2]

	//===================================================================
	// RECEIVE BD's response to the OPEN...
	//===================================================================

	recvBlockSize = RecvSettingXactionBlockMsg_(xactionSocketId,
										 ::SETTINGS_TRANSACTION_MSG,
										 &recvBlock, ZERO_ELEMS_BLOCK_SIZE_,
										 BLOCK_RECV_WAIT_TIME_);

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<CLIENT>>  Received response:"
			<< recvBlock << ", size = " << recvBlockSize << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	if ( recvBlockSize != ZERO_ELEMS_BLOCK_SIZE_  ||
		 recvBlock.xactionId != ::CONNECTION_COMPLETED )
	{	// $[TI3]
		SettingsXaction::ProcessFailedRecv_(xactionSocketId);
 
       #ifdef SIGMA_DEBUG
           printf(  "Failed recvBlockSize(%d) || xactionId(%d) !!!!\n",recvBlockSize, recvBlock.xactionId  );
       #endif

		return(FALSE);
	}	// $[TI4]

	//===================================================================
	// Step #1:  SEND the first message of the four-way transaction...
	//===================================================================

	// calculate the size of the data block...
	const Uint  SETTINGS_BLOCK_SIZE =
		SettingsXaction::CalcXactionBlockSize_(initXactionBlock.numXmitElems);

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<CLIENT>>  Transmitting settings:"
			<< initXactionBlock << ", size = " << SETTINGS_BLOCK_SIZE
			<< endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	xmitSize = XmitSettingXactionBlockMsg_(xactionSocketId,
									::SETTINGS_TRANSACTION_MSG,
									&initXactionBlock,
									SETTINGS_BLOCK_SIZE);

	if ( xmitSize != SETTINGS_BLOCK_SIZE )
	{	// $[TI5]
		SettingsXaction::ProcessFailedXmit_(xactionSocketId);
        #ifdef SIGMA_DEBUG
           printf(  "Failed xmitSize(%d) !!!!\n",xmitSize  );
        #endif
		return(FALSE);
	}	// $[TI6]

	//===================================================================
	// Step #2:  RECEIVE the first response from the BD...
	//===================================================================

	recvBlockSize = RecvSettingXactionBlockMsg_(xactionSocketId,
										 ::SETTINGS_TRANSACTION_MSG,
										 &recvBlock, SETTINGS_BLOCK_SIZE,
										 BLOCK_RECV_WAIT_TIME_);

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<CLIENT>>  Received response:"
			<< recvBlock << ", size = " << recvBlockSize << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	// get the size (in bytes) of the setting data...
	const Uint  SETTING_VALUES_SIZE = (SETTINGS_BLOCK_SIZE - 
									   ZERO_ELEMS_BLOCK_SIZE_);

	if ( recvBlockSize != SETTINGS_BLOCK_SIZE                ||
		 recvBlock.xactionId != ::VALIDATE_XMITTED_SETTINGS  ||
		 ::memcmp(initXactionBlock.arrXmitElems,
				  recvBlock.arrXmitElems, SETTING_VALUES_SIZE) != 0 )
	{	// $[TI7]
		SettingsXaction::ProcessFailedRecv_(xactionSocketId);

        #ifdef SIGMA_DEBUG
            printf(  "Failed Step2 recvBlockSize(%d) || xactionId(%d) !!!!\n",recvBlockSize, recvBlock.xactionId  );
        #endif

		return(FALSE);
	}	// $[TI8]

	//===================================================================
	// Step #3:  SEND the third message of the four-way transaction...
	//===================================================================

	SettingXactionBlock  sendBlock;

	sendBlock.xactionId    = ::ACCEPT_XMITTED_SETTINGS;
	sendBlock.numXmitElems = 0u;

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<CLIENT>>  Settings verified:"
			<< sendBlock << ", size = " << ZERO_ELEMS_BLOCK_SIZE_ << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	xmitSize = XmitSettingXactionBlockMsg_(xactionSocketId,
									::SETTINGS_TRANSACTION_MSG, &sendBlock,
									ZERO_ELEMS_BLOCK_SIZE_);

	if ( xmitSize != ZERO_ELEMS_BLOCK_SIZE_ )
	{	// $[TI9]
		SettingsXaction::ProcessFailedXmit_(xactionSocketId);
 
        #ifdef SIGMA_DEBUG
           printf(  "Failed Step 3 xmitSize(%d) !!!!\n",xmitSize  );
        #endif

		return(FALSE);
	}	// $[TI10]

	//===================================================================
	// Step #4:  RECEIVE the final response from the BD...
	//===================================================================

	recvBlockSize = RecvSettingXactionBlockMsg_(xactionSocketId,
										 ::SETTINGS_TRANSACTION_MSG,
										 &recvBlock, ZERO_ELEMS_BLOCK_SIZE_,
										 BLOCK_RECV_WAIT_TIME_);

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<CLIENT>>  Received response:"
			<< recvBlock << ", size = " << recvBlockSize << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	if ( recvBlock.xactionId != ::SETTINGS_XACTION_COMPLETE )
	{	// $[TI11]
		SettingsXaction::ProcessFailedRecv_(xactionSocketId);
  
        #ifdef SIGMA_DEBUG
            printf(  "Failed Step 4 xactionId(%d) !!!!\n",recvBlock.xactionId  );
        #endif

		return(FALSE);
	}	// $[TI12]

	//===================================================================
	// CLOSE Settings Transaction Socket...
	//===================================================================

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<CLIENT>>  Closing connection!" << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	NetworkApp::BCloseSocket(xactionSocketId);

#elif defined(SIGMA_COMMON)

	::PInitXactionBlock_ = &initXactionBlock;
	SettingsXaction::BeginServerXaction_(-1);

#endif // defined(SIGMA_GUI_CPU)

	SettingsXaction::ReportXactionStatus_(SettingsXaction::XACTION_SUCCESSFUL);

	return(TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ReportXactionStatus_(xactionStatusId)
//
//@ Interface-Description
//  This method is responsible for reporting any transaction failure, and
//  any successful transaction following a failure.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::ReportXactionStatus_(
										 const SettingsXaction::XactionStatusId_ xactionStatusId
										 )
{
	CALL_TRACE("ReportXactionStatus_(xactionStatusId)");

	static Boolean  IsFailureLogged_ = FALSE;

	switch ( xactionStatusId )
	{
		case SettingsXaction::XACTION_SUCCESSFUL :		  // $[TI1]
			if ( IsFailureLogged_ )
			{	// $[TI1.1] -- previous failure; log a successful completion...
				DiagnosticCode  xactionResetDiag;

				xactionResetDiag.setSystemEventCode(
												   DiagnosticCode::SETTINGS_XACTION_SUCCEEDED
												   );

				FaultHandler::LogDiagnosticCode(xactionResetDiag);
				IsFailureLogged_ = FALSE;
			}	// $[TI1.2] -- successful completion, but no previous failure...
			break;
		case SettingsXaction::OPEN_SOCKET_FAILURE :
		case SettingsXaction::XMIT_FAILURE :
		case SettingsXaction::RECV_FAILURE :		  // $[TI2]
			if ( !IsFailureLogged_ )
			{	// $[TI2.1] -- not an ongoing failure; log a failure diagnostic...
				DiagnosticCode  xactionFailDiag;

				xactionFailDiag.setSystemEventCode(
												  DiagnosticCode::SETTINGS_XACTION_FAILED,
												  xactionStatusId
												  );

				FaultHandler::LogDiagnosticCode(xactionFailDiag);
				IsFailureLogged_ = TRUE;
			}	// $[TI2.2] -- ongoing failure, don't log again...
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(xactionStatusId);
			break;
	};

	MsgQueue             userAnnunciationQ(USER_ANNUNCIATION_Q);
	UserAnnunciationMsg  xactionStatusMsg;

	xactionStatusMsg.settingsParts.eventType =
		UserAnnunciationMsg::SETTINGS_EVENT;
	xactionStatusMsg.settingsParts.transactionSuccess = !IsFailureLogged_;

#if defined(SIGMA_UNIT_TEST)
	// during unit testing, I don't want to be required to have the
	// GUI task running...
	if ( !userAnnunciationQ.isFull() )
	{
#endif // defined(SIGMA_UNIT_TEST)

		// notify GUI of the settings' transaction...
		userAnnunciationQ.putMsg(*((Int32*)&xactionStatusMsg));

#if defined(SIGMA_UNIT_TEST)
	}
#endif // defined(SIGMA_UNIT_TEST)
}

#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)


//=====================================================================
//
//  BD-Only Methods...
//
//=====================================================================

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

//=====================================================================
//
//  BD-Only Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  EstablishConnection_(xactionSocketId)  [static]
//
//@ Interface-Description
//  This is a callback that is activated when the GUI opens the settings
//  transaction socket.  The ID of the socket is passed in 'xactionSocketId'.
//  This method is responsible for notify the Settings Xaction task,
//  that the GUI is initiating a settings transaction.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::EstablishConnection_(Int xactionSocketId)
{
	CALL_TRACE("EstablishConnection(xactionSocketId)");

	MsgQueue             bdSettingsXactionQ(SETTINGS_XACTION_Q);
	InterTaskMessage     socketConnectionMsg;

	SettingsXaction::SetupQueueMsg_(socketConnectionMsg,
									::CONNECTION_INITIATED,
									xactionSocketId);

	bdSettingsXactionQ.putMsg(*((Int32*)&socketConnectionMsg));
}	// $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BeginServerXaction_(xactionSocketId)
//
//@ Interface-Description
//  This method conducts the "server-side" transaction.  This is a 7-stage
//  transaction that results in ALL of the currently-accepted settings
//  being transmitted from the GUI CPU.
//
//  This method is passed the ID of the socket ('xactionSocketId') to
//  which the transaction is to be conducted on.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02031] -- all BD settings shall be delivered to the BDU...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	SettingsXaction::BeginServerXaction_(const Int xactionSocketId)
{
	CALL_TRACE("BeginServerXaction_(xactionSocketId)");

#if defined(SIGMA_DEVELOPMENT)
	const Boolean  IS_DEBUG_ON =
		Settings_Validation::IsDebugOn(::SETTINGS_XACTION);
#endif // defined(SIGMA_DEVELOPMENT)

#if defined(SIGMA_BD_CPU)
	//===================================================================
	//  conduct four-way receival...
	//===================================================================

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "RESPONDING TO TRANSACTION..." << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	SettingXactionBlock  recvBlock, sendBlock, initXactionBlock;
	Uint                  recvBlockSize;

	Uint  xmitSize;

	//===================================================================
	// SEND response to the OPEN...
	//===================================================================

	sendBlock.xactionId    = ::CONNECTION_COMPLETED;
	sendBlock.numXmitElems = 0u;

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<SERVER>>  Responding to connection:"
			<< sendBlock << ", size = " << ZERO_ELEMS_BLOCK_SIZE_ << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	xmitSize = XmitSettingXactionBlockMsg_(xactionSocketId,
									::SETTINGS_TRANSACTION_MSG,
									&sendBlock, ZERO_ELEMS_BLOCK_SIZE_);

	if ( xmitSize != ZERO_ELEMS_BLOCK_SIZE_ )
	{	// $[TI1]
		SettingsXaction::ProcessFailedXmit_(xactionSocketId);
		return;
	}	// $[TI2] -- connection transmission succeeded...

	//===================================================================
	// Step #1:  RECEIVE the first message of the four-way transaction...
	//===================================================================

	recvBlockSize = RecvSettingXactionBlockMsg_(xactionSocketId,
										 ::SETTINGS_TRANSACTION_MSG,
										 &initXactionBlock,
										 MAX_ELEMS_BLOCK_SIZE_,
										 BLOCK_RECV_WAIT_TIME_);

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<SERVER>>  Received response:"
			<< initXactionBlock << ", size = " << recvBlockSize << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	if ( (initXactionBlock.xactionId != VENT_STARTUP_UPDATE_XMITTED  &&
		  initXactionBlock.xactionId != USER_UPDATE_XMITTED          &&
		  initXactionBlock.xactionId != ONLINE_SYNC_XMITTED          &&
		  initXactionBlock.xactionId != EXTERNAL_UPDATE_XMITTED)     ||
		 recvBlockSize != CalcXactionBlockSize_(initXactionBlock.numXmitElems) )
	{	// $[TI3]
		SettingsXaction::ProcessFailedRecv_(xactionSocketId);
		return;
	}	// $[TI4]

    static Boolean FailedNumTransaction_ = 0;
    static const Uint MAX_NUM_TRANSACTION_FAILURES = 5;

    if( initXactionBlock.xactionId != ::EXTERNAL_UPDATE_XMITTED )
    {   // $[TI11] -- this is an internal transmission being received...

	    // The number of xmit elements must equal to number of 
	    // bd settings.  If not, there's possibly a transmission 
	    // issue here.  Log a transmission failure and try to 
	    // re-send.  This fix below may reduce the number of occurance on
	    // pre-Xena II boards in the field due to adding a 
	    // number of retries.
	    if(initXactionBlock.numXmitElems != ::NUM_BD_SETTING_IDS)
	    {
	        FailedNumTransaction_ ++;

	        #ifdef SIGMA_DEBUG
	           syslog(LOG_INFO,"Num Strikes (%d)\n",FailedNumTransaction_);
	        #endif 
  
	        // If we are not receiving the correct number of settings
	        // up to MAX_NUM_TRANSACTION_FAILURES, then we must prevent 
	        // infinite number of looping failures.  
	        // There is a possibility where BD and GUI's software revisions 
	        // may not match, thus this check below should assert instead 
	        // of forcing the GUI to re-send inifinite times.  Therefore, this check
	        // wasn't added to the above check where xactionId not equal to 
	        // EXTERNAL_UPDATE_XMITTED, or ONLINE_SYNC_XMITTED and etc.
	        if (FailedNumTransaction_ >= MAX_NUM_TRANSACTION_FAILURES)
	        {
	        AUX_CLASS_ASSERTION(
	                           (initXactionBlock.numXmitElems == ::NUM_BD_SETTING_IDS),
	                           initXactionBlock.numXmitElems 
	                           );
	        }
	        else
	        {
	            SettingsXaction::ProcessFailedRecv_(xactionSocketId);
	            return;
	        }
	    }
	    else
	    {
	        FailedNumTransaction_ = 0;
	    }
    }
	//===================================================================
	// Step #2:  SEND the first response to the GUI...
	//===================================================================

	// save for later...
	const SettingsXactionId  INIT_XACTION_ID =
		(SettingsXactionId)initXactionBlock.xactionId;

	// respond with transmitted block, but new message ID...
	initXactionBlock.xactionId = ::VALIDATE_XMITTED_SETTINGS;

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<SERVER>>  Returning settings:"
			<< initXactionBlock << ", size = " << recvBlockSize << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	xmitSize = XmitSettingXactionBlockMsg_(xactionSocketId,
									::SETTINGS_TRANSACTION_MSG,
									&initXactionBlock, recvBlockSize);

	if ( xmitSize != recvBlockSize )
	{	// $[TI5]
		SettingsXaction::ProcessFailedXmit_(xactionSocketId);
		return;
	}	// $[TI6] -- Step #2 succeeded...

	//===================================================================
	// Step #3:  RECEIVE the third message of the four-way transaction...
	//===================================================================

	recvBlockSize = RecvSettingXactionBlockMsg_(xactionSocketId,
										 ::SETTINGS_TRANSACTION_MSG,
										 &recvBlock, ZERO_ELEMS_BLOCK_SIZE_,
										 BLOCK_RECV_WAIT_TIME_);

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<SERVER>>  Received response:"
			<< recvBlock << ", size = " << recvBlockSize << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	if ( recvBlockSize != ZERO_ELEMS_BLOCK_SIZE_  ||
		 recvBlock.xactionId != ACCEPT_XMITTED_SETTINGS )
	{	// $[TI7]
		SettingsXaction::ProcessFailedRecv_(xactionSocketId);
		return;
	}	// $[TI8]

	//===================================================================
	// Step #4:  SEND the final response from the BD...
	//===================================================================

	sendBlock.xactionId    = ::SETTINGS_XACTION_COMPLETE;
	sendBlock.numXmitElems = 0u;

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<SERVER>>  Completing transaction:"
			<< sendBlock << ", size = " << ZERO_ELEMS_BLOCK_SIZE_ << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	xmitSize = XmitSettingXactionBlockMsg_(xactionSocketId,
									::SETTINGS_TRANSACTION_MSG,
									&sendBlock, ZERO_ELEMS_BLOCK_SIZE_);

	if ( xmitSize != ZERO_ELEMS_BLOCK_SIZE_ )
	{	// $[TI9]
		SettingsXaction::ProcessFailedXmit_(xactionSocketId);
		return;
	}	// $[TI10] -- Step #2 succeeded...

	//===================================================================
	// CLOSE Settings Transaction Socket...
	//===================================================================

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "<<SERVER>>  Closing connection!" << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

	NetworkApp::BCloseSocket(xactionSocketId);

#if defined(SIGMA_DEVELOPMENT)
	if ( IS_DEBUG_ON )
	{
		cout << "SUCCESSFUL" << endl;
	}
#endif // defined(SIGMA_DEVELOPMENT)

#elif defined(SIGMA_COMMON)

	SettingXactionBlock  initXactionBlock = *::PInitXactionBlock_;
	const SettingsXactionId  INIT_XACTION_ID =
		(SettingsXactionId)initXactionBlock.xactionId;

#endif // defined(SIGMA_BD_CPU)

	//===================================================================
	// process received transmission...
	//===================================================================

	PendingContext&  rPendingContext = ContextMgr::GetPendingContext();
	BdSettingValues  xmittedBdValues;
	Uint             idx;

	//-------------------------------------------------------------------
	//  NOTE:  the processing of internal and external transmissions are
	//         separated to allow for more optimized code for the internal
	//         transmissions.
	//-------------------------------------------------------------------

	if ( INIT_XACTION_ID != ::EXTERNAL_UPDATE_XMITTED )
	{	// $[TI11] -- this is an internal transmission being received...
		// ALL settings should be transmitted for an internal transmission...

        #ifdef SIGMA_DEBUG
            syslog(LOG_INFO,"BD numXmit (%d)", initXactionBlock.numXmitElems);
        #endif 

		AUX_CLASS_ASSERTION(
						   (initXactionBlock.numXmitElems == ::NUM_BD_SETTING_IDS),
						   initXactionBlock.numXmitElems 
						   );
        #ifdef SIGMA_DEBUG
            syslog(LOG_INFO,"PASSED");
        #endif

		// for ALL of the BD, bounded setting values...
		for ( idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
			idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++ )
		{
			BoundedValue  newBoundedValue;

			// get the precision and value of the transmitted bounded setting
			// value identified by 'idx'...
			newBoundedValue.precision =
				(Precision)initXactionBlock.arrXmitElems[idx].boundedPrec;
			newBoundedValue.value =
				initXactionBlock.arrXmitElems[idx].boundedValue;

			xmittedBdValues.setBoundedValue((SettingId::SettingIdType)idx,
											newBoundedValue);
		}

		// the IDs are to be continuous between the different categories...
		SAFE_AUX_CLASS_ASSERTION(
								(idx == SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE), idx
								);

		// for ALL of the BD, discrete setting values...
		for ( idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
			idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++ )
		{
			xmittedBdValues.setDiscreteValue((SettingId::SettingIdType)idx,
											(DiscreteValue) initXactionBlock.arrXmitElems[idx].discreteValue);
		}

		// to ensure that there are no new "categories" of BD settings...
		SAFE_AUX_CLASS_ASSERTION((idx == ::NUM_BD_SETTING_IDS), idx);
	}
	else
	{	// this is an external transmission being received...
		//-----------------------------------------------------------------
		//  NOTE:  this branch is purposely NOT counted as a testable item,
		//	       because this block of code is only used by the external
		//         test groups (e.g., ARTS) that by-pass the GUI to make
		//         setting changes.  Therefore, the passing of the external
		//	       tests, themselves, verify the validity of this code block.
		//-----------------------------------------------------------------

		// less than all settings can be transmitted for an external
		// transmission...
		AUX_CLASS_ASSERTION(
						   (initXactionBlock.numXmitElems <= ::NUM_BD_SETTING_IDS),
						   initXactionBlock.numXmitElems 
						   );

		// since a "sparse" transmission is valid here, make sure that
		// 'xmittedBdValues' is initialized to the current set of BD setting
		// values...
		xmittedBdValues = rPendingContext.getPendingValues();

		for ( idx = 0; idx < initXactionBlock.numXmitElems; idx++ )
		{	// always enters this branch...
			const SettingXmitId::SettingXmitIdType  SETTING_XMIT_ID =
				(SettingXmitId::SettingXmitIdType)
			initXactionBlock.arrXmitElems[idx].settingXmitId;
			const SettingId::SettingIdType  SETTING_ID =
				SettingXmitId::ConvertToSettingId(SETTING_XMIT_ID);

			if ( SettingId::IsBdBoundedId(SETTING_ID) )
			{	// element is a bounded, BD setting...
				BoundedValue  newBoundedValue;

				// get the precision and value of the transmitted bounded setting
				// value identified by 'SETTING_ID' (and 'idx')...
				newBoundedValue.precision =
					(Precision)initXactionBlock.arrXmitElems[idx].boundedPrec;
				newBoundedValue.value =
					initXactionBlock.arrXmitElems[idx].boundedValue;

				xmittedBdValues.setBoundedValue(SETTING_ID, newBoundedValue);
			}
			else if ( SettingId::IsBdDiscreteId(SETTING_ID) )
			{	// element is a discrete, BD setting...
				xmittedBdValues.setDiscreteValue(SETTING_ID,
												 (DiscreteValue)initXactionBlock.arrXmitElems[idx].discreteValue);
			}
			else
			{
				// the ID is neither a bounded, nor discrete setting identifier...
				AUX_CLASS_ASSERTION_FAILURE(SETTING_ID);
			}  // end of if...
		}  // end of for...
	}  // end of if...

	// forward the transmitted setting values to the Pending Context...
	rPendingContext.processTransmission(INIT_XACTION_ID, xmittedBdValues);

	// a transaction has completed, therefore set the Sys-Init Complete
	// Flag...
	Post::SetSysInitComplete(TRUE);
}

#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Free-Function:  operator<<(ostr, xaction)  [static]
//
// Interface-Description
//  Print out 'xaction'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Ostream&
	operator<<(Ostream& ostr, const SettingXactionBlock& xaction)
{
	CALL_TRACE("operator<<(ostr, xaction)");

	ostr << "{xactionId = "     << xaction.xactionId
		<< ", numXmitElems = " << xaction.numXmitElems << "}"
		<< endl;

	return(ostr);
}

#endif // defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  RecvSettingXactionBlockMsg_()  [static]
//
//@ Interface-Description
//  Helper function to receive a network message of data type  
//	SettingXactionBlock. If it receives the right number of bytes,
//	it converts data endiannes properly as fits this data structure..
//	To transmit other data types, different API should be created that 
//	converts endianness in the proper way for that data type..
//---------------------------------------------------------------------
//@ Implementation-Description
//  It calls NetworkApp::BRecvMsg which is the specialized API for recieving
//	Settings Xactions for this class.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int
SettingsXaction::RecvSettingXactionBlockMsg_(Int socketId,
											 XmitDataMsgId msgId,
											 SettingXactionBlock *pData,
											 Uint32 nbytes, Uint waitDuration)
{
	Int receivedBytes = NetworkApp::BRecvMsg(socketId, msgId, (void*) pData, 
											 nbytes, waitDuration);
	pData->convNtoH();

	return receivedBytes;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  XmitSettingXactionBlockMsg_()  [static]
//
//@ Interface-Description
//  Helper function to transmit a network message of data type  
//	SettingXactionBlock.
//	It converts data endiannes from H to N for compatibility. To transmit 
//	other data types, different API should be created that converts endianness
//	in the proper way for that data type..
//---------------------------------------------------------------------
//@ Implementation-Description
//  It calls NetworkApp::BXmitMsg which is the specialized API for sending
//	Settings Xactions for this class.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
const Int
SettingsXaction::XmitSettingXactionBlockMsg_(Int socketId,
											 XmitDataMsgId msgId,
											 const SettingXactionBlock *pData,
											 Uint32 nBytes)
{
	//copy data into local copy because we do not want to change the original
	SettingXactionBlock sendData;	
	memcpy((void*)&sendData, (const void*)pData, nBytes);
	sendData.convHtoN();
	Int sentBytes = NetworkApp::BXmitMsg(socketId, msgId, (void*) &sendData, nBytes);
	return sentBytes;
}

