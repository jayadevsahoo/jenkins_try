#ifndef PressureSensor_HH
#define PressureSensor_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: PressureSensor - Class for pressure measuring sensors that
//      incorporate a calibrated zero offset.  NOTE: The atmospheric pressure
//      sensor is a linear sensor because the zero point is fixed.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/PressureSensor.hhv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004 By: syw    Date: 04-Mar-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added acess method getZeroOffset().
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SafetyNetSensorData * to constructor arguments.  Added
//			setAlpha() method.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "BD_IO_Devices.hh"

//@ Usage-Classes
#include "AdcChannels.hh"
#include "Sensor.hh"
#include "ProcessedValues.hh"

class PressureXducerAutozero ;

//@ End-Usage

class PressureSensor : public Sensor
{
  public:

    PressureSensor( SafetyNetSensorData *pData,
    				const AdcChannels::AdcChannelId AdcId,
    				const Real32 alpha) ;
    virtual ~PressureSensor( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;
   
    friend class PressureXducerAutozero ;
	   
    virtual void updateValue( void) ;
    inline Real32 getSlope( void) ;
    inline Real32 getFilteredSlopeValue( void) ;
    inline Real32 getFilteredValue( void) ;
    inline void initValues( const Real32 filtered, const Real32 slope,
                            const Real32 filteredSlope, const Real32 prevValue) ;
    inline void setZeroOffset( const AdcCounts offset) ;
	inline void setSpanCounts( const AdcCounts count) ;
    inline void setAlpha( const Real32 alpha) ;
    inline AdcCounts getZeroOffset( void) ;

  protected:

    virtual Real32 rawToEngValue_( const AdcCounts count) const ;

	    //@ Data-Member: zeroOffset_
    // The value which represents zero to the pressure sensor
    AdcCounts zeroOffset_ ;

	//@ Data-Member: spanCounts_
    // The value which represents the highest calibration pressure 
    AdcCounts spanCounts_ ;

  private:

    // these methods are purposely declared, but not implemented...
    PressureSensor(void) ;                     // default constructor
    PressureSensor( const PressureSensor&) ;   // not implemented
    void operator=( const PressureSensor&) ;   // not implemented
 

    //@ Data-Member: processedValues_
    // for processing filter, filter slope and slope signal
    ProcessedValues processedValues_ ;
};

// Inlined methods
#include "PressureSensor.in"

#endif // PressureSensor_HH 
