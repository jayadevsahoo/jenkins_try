/*****************************************************************************/
/*   Module Name:   helpinit.h                                   
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _HELPINIT_H_
#define _HELPINIT_H_

#include <userint.h>

#define MAX_HELP_CMD_LEN 64

#define GENERAL_HELP (VAL_F1_VKEY)
#define CONTEXT_HELP (VAL_F1_VKEY | VAL_SHIFT_MODIFIER)


enum HLP_ERROR_CODES{
	HLP_NOERROR=0,
	HLP_ERROR,
	HLP_NO_EXT,
	HLP_NO_APP
};

int GetCHMExecutableName(char** ComBuffer);
int ShowHelp(int panel, int control, int nTypeOfHelp);
void ShowHelpAbout(int hParentPanel);

#endif //_HELPINIT_H_
