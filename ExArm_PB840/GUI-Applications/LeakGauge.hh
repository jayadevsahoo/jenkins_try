#ifndef LeakGauge_HH
#define LeakGauge_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: LeakGauge - Displays a leak slider for setting high and low
// leak limits, containing a pointer displaying the current value of the
// associated service datum.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LeakGauge.hhv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "Box.hh"
#include "Container.hh"
#include "NumericField.hh"
#include "TextField.hh"
#include "Triangle.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class LeakGauge : public Container
{
public:
	LeakGauge(Real32 minValue, Real32 maxValue,
				StringId passMessageId, StringId failMessageId,
				Real32 upperValue, Real32 lowValue = 0.0, 
				StringId alertMessageId=NULL_STRING_ID);
	~LeakGauge(void);

	// Drawable virtual method
	virtual void activate(void);

	void setValue(Real32 value);
	
    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  


protected:

private:
	LeakGauge(void);							// not implemented...
	LeakGauge(const LeakGauge&);			// not implemented...
	void operator=(const LeakGauge&);			// not implemented...

	Int16 valueToPoint_(Real32 sliderValue);

	void updateValueField_(void);

	//@ Data-Member: sliderBox_
	// The box for displaying the leak slider.
	Box sliderBox_;

	//@ Data-Member: passBox_
	// The box for displaying the pass box.
	Box passBox_;

	//@ Data-Member: failBox_
	// The box for displaying the fail box.
	Box failBox_;

	//@ Data-Member: min_
	// The minimum value on the slider.
	Real32 min_;

	//@ Data-Member: max_
	// The maximum value on the slider.
	Real32 max_;

	//@ Data-Member: minField_
	// The numeric field used to display the minimum value of the slider.
	NumericField minField_;

	//@ Data-Member: maxField_
	// The numeric field used to display the maximum value of the slider.
	NumericField maxField_;
	
	//@ Data-Member: lower_
	// The lower value on the slider.
	Real32  lower_;

	//@ Data-Member: upper_
	// The upper value on the slider.
	Real32  upper_;

	//@ Data-Member: lowerField_
	// The numeric field used to display the lower value of the slider.
	NumericField lowerField_;

	//@ Data-Member: upperField_
	// The numeric field used to display the upper value of the slider.
	NumericField upperField_;

	//@ Data-Member: passMessageId_
	// The string which represents the test result is within the valid range.
	TextField passMessageId_;

	//@ Data-Member: failMessageId_
	// The string which represents the test result is outside the valid range.
	TextField failMessageId_;

	//@ Data-Member: alertMessageId_
	// The string which represents the test result is inbetween the valid range.
	TextField alertMessageId_;
	
	//@ Data-Member: valueContainer_
	// The container used to group the drawables which make up the service
	// datum drawables at the left side of the slider.
	Container valueContainer_;

	//@ Data-Member: value_
	// The current value of the service datum on the slider.
	Real32 value_;

	//@ Data-Member: valueField_
	// A numeric field used for displaying the current value of the service
	// datum.
	NumericField valueField_;

	//@ Data-Member: valueBox_
	// The white box on which the value of the service datum is displayed.
	Box valueBox_;

	//@ Data-Member: valueTriangle_
	// The triangle used as the ``point'' on the service datum pointer.
	Triangle valueTriangle_;

	//@ Data-Member: scaleFactor_
	// A scaling factor used in converting setting and service id values to
	// a corresponding pixel value for positioning graphicals along the alarm
	// slider.
	Real32 scaleFactor_;

};

#endif // LeakGauge_HH 
