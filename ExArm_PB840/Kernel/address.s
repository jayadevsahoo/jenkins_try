;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================


;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: address.s - POST Addressing Mode Test
;
;---------------------------------------------------------------------
;@ Interface-Description
;
;  Free-Functions
;    AddressModeTest - Performs address test
;
;---------------------------------------------------------------------
;@ Rationale
;    This is a subset of the processor tests that must be performed
;    during POST.
;---------------------------------------------------------------------
;@ Implementation-Description
;    The ability to access the processor in selected addressing modes of
;    operation is tested in this module.
;  
;---------------------------------------------------------------------
;@ Fault Handling
;    A failure of this test is a MAJOR post failure.
;---------------------------------------------------------------------
;@ Restrictions
;    None
;---------------------------------------------------------------------
;@ Invariants
;    None
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
; @(#) $Header:   /840/Baseline/Kernel/vcssrc/address.s_v   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
;
;@ Modification-Log
;
;
;  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;    Project: Sigma (R8027)
;    Description:
;       Added testable item annotations.
;
;  Revision: 002    By: Gary Cederquist  Date: 16-MAY-1997  DR Number: 2121
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options from source, using command line instead.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;
;=====================================================================

;------------------------------------------------------------------------
;------------- Unit Test Definitions  -----------------------------------
;------------------------------------------------------------------------
                XDEF        _AddressModeTest__Fv
                SECTION     code,,C

                INCLUDE  kernel.inc

TEST_PATTERN_1 EQU      $12345678
TEST_PATTERN_2 EQU      $87654321
TEST_PATTERN_3 EQU      $7239


;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: AddressModeTest 
;
;@ Interface-Description
;
;  >Von
;    This function tests the various addressing modes of the 68040.
;    For each of the applicable addressing modes, a value is written
;    to memory or address locations and compared to expected values.
;    $[11013]
;
;   INPUTS:  a6.l - return address
;   OUTPUTS: d0.l = PASSED, MAJOR
;
;   REGISTER USAGE
;
;   Register Modified: d0,d7,a0
;    
;  TECHNIQUE
;
;  This verifies the CPU in the following modes of operation:
;
;  Data Register Direct
;  Immediate Data
;  Address Register Direct
;  Address Register Indirect
;  Address Register Indirect with Postincrement
;  Address Register Indirect with Predecrement
;  Address Register Indirect with Index Displacement
;  Absolute short addressing
;  Absolute long  addressing
;
;  >Voff
;---------------------------------------------------------------------
;@ Implementation-Description
;
;    REGISTER USAGE
;
;    d0,d1,a0,a1
;
;    A stack of 12 words is used. 
;---------------------------------------------------------------------
;@ PreCondition
;    A portion of pretested DRAM is available.  
;    A stack must be available.
;    The basic integer unit tests have been executed.
;---------------------------------------------------------------------
;@ PostCondition
;    none
;@ End-Free-Function
;=====================================================================

; void AddressModeTest() {
_AddressModeTest__Fv:
                                             UNIT_TEST_POINT POSTUT_43
      link     A6,#-12                       ; scratch location = a6-4
      move.l   #TEST_PATTERN_1,d1            ; Data Register direct
                                             UNIT_TEST_POINT POSTUT_43_1
      cmp.l    #TEST_PATTERN_1,d1            ; and Immediate data
      bne      testFailed                    ; // $[TI1.1] failed
                                             ; // $[TI1.2] passed

      move.l   a6,a0                         ; Address register direct
                                             UNIT_TEST_POINT POSTUT_43_2
      cmp.l    a6,a0
      bne      testFailed                    ; // $[TI2.1] failed
                                             ; // $[TI2.2] passed

      move.l   #TEST_PATTERN_1,-4(a6)        ; Address register indirect
                                             UNIT_TEST_POINT POSTUT_43_3
      cmp.l    #TEST_PATTERN_1,-4(a6)
      bne      testFailed                    ; // $[TI3.1] failed
                                             ; // $[TI3.2] passed

      move.l   #TEST_PATTERN_2,d1            ; Address register postincrement
      move.l   a6,a0
      sub.l    #12,a0
      move.l   d1,(a0)+                           
      move.l   d1,(a0)+                        
                                             UNIT_TEST_POINT POSTUT_43_4
      cmp.l    #TEST_PATTERN_2,-4(a0)
      bne      testFailed                    ; // $[TI4.1] failed
                                             ; // $[TI4.2] passed
                                             UNIT_TEST_POINT POSTUT_43_5
      cmp.l    #TEST_PATTERN_2,-8(a0)
      bne      testFailed                    ; // $[TI5.1] failed
                                             ; // $[TI5.2] passed

      move.l   a6,a0
      move.l   #TEST_PATTERN_1,-(a0)         ; Address register with predecrement
      move.l   #TEST_PATTERN_1,-(a0) 
                                             UNIT_TEST_POINT POSTUT_43_6
      cmp.l    #TEST_PATTERN_1,4(a0)
      bne      testFailed                    ; // $[TI6.1] failed
                                             ; // $[TI6.2] passed

      move.l   #$01020304,(a0)               ; Address register indirect with index
                                             UNIT_TEST_POINT POSTUT_43_7
      cmpi.b   #$03,(2,a0)       
      bne      testFailed                    ; // $[TI7.1] failed
                                             ; // $[TI7.2] passed

      move.w   #KRAM_START_ADR,a0            ; Absolute Data Addressing Short
      clr.l    (a0)
      move.w   #TEST_PATTERN_3,d0
      move.w   #TEST_PATTERN_3,(KRAM_START_ADR).W        
                                             UNIT_TEST_POINT POSTUT_43_8
      cmp.w    (a0),d0
      bne      testFailed                    ; // $[TI8.1] failed
                                             ; // $[TI8.2] passed

      move.l   #KRAM_START_ADR,a0            ; Absolute Data Addressing Long
      clr.l    (a0)
      move.l   #TEST_PATTERN_2,d0
      move.l   #TEST_PATTERN_2,(KRAM_START_ADR).L        
                                             UNIT_TEST_POINT POSTUT_43_9
      cmp.l    (a0),d0
      bne      testFailed                    ; // $[TI9.1] failed
                                             ; // $[TI9.2] passed

      nop
      nop

      moveq.l  #PASSED,d0                    ; Exit - Test Passed
      unlk     A6

; // $[TI10.1]
      rts



testFailed:
      moveq.l  #MAJOR,d0                    ; Exit - Test Failed
      unlk     A6
; // $[TI10.2]
      rts

; }
      END
