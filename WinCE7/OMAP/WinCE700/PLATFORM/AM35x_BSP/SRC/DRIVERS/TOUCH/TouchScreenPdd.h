//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/

#ifndef __TOUCHPDD_H
#define __TOUCHPDD_H

// undef here since these are already defined.
#undef ZONE_INIT
#undef ZONE_ERROR

#include <tchddsi.h>

//------------------------------------------------------------------------------
// Sample rate during polling.
#define DEFAULT_SAMPLE_RATE          100
#define TOUCHPANEL_SAMPLE_RATE_LOW          DEFAULT_SAMPLE_RATE
#define TOUCHPANEL_SAMPLE_RATE_HIGH         DEFAULT_SAMPLE_RATE
#define DEFAULT_THREAD_PRIORITY             109

// Number of samples to discard when pen is initially down
#define DEFAULT_INITIAL_SAMPLES_DROPPED    1

#define RK_HARDWARE_DEVICEMAP_TOUCH     (TEXT("HARDWARE\\DEVICEMAP\\TOUCH"))
#define RV_CALIBRATION_DATA             (TEXT("CalibrationData"))

#define DIFF_X_MAX            10
#define DIFF_Y_MAX            10

#define FILTEREDSAMPLESIZE              3
#define SAMPLESIZE                      1

//------------------------------------------------------------------------------
// Defines
//

#define COMMAND_CNV_SELECTXY			0x00
#define COMMAND_ENABLE_READ                 0x3
#define COMMAND_ENABLE_WRITE			0x2

#define MAX_PTS                     3
#define DELTA                       30

#define CAL_DELTA_RESET             20
#define CAL_HOLD_STEADY_TIME        1500
#define RANGE_MIN                   0
#define RANGE_MAX                   4096
#define CFG0_REG						0xC
#define CFG2_REG						0xE


// CFG2 bit definitions
//MSB
#define PINTS1							(1 << 7)
#define PINTS0							(1 << 6)
#define MAV_M0M1_FLTR_SIZE_15			(3 << 4)
#define MAV_W0W1_FLTR_SIZE_7_8		(2 << 2)
//LSB
#define MAV_FLTR_EN_X					(1<<4)
#define MAV_FLTR_EN_Y					(1<<3)


//------------------------------------------------------------------------------
// local data structures
//

typedef struct  
{
    BOOL       bInitialized;

    DWORD   nSampleRate;    
    DWORD   nInitialSamplesDropped;
    LONG      nPenGPIO;    
    DWORD   PenUpDebounceMS;
    ULONG    nI2CAddr;
    ULONG    nI2CBaudrateIndex;
    DWORD  nI2CBusIndex;

    DWORD  dwSysIntr;
    DWORD  dwSamplingTimeOut;
    BOOL      bTerminateIST;
    HANDLE  hTouchPanelEvent;
    DWORD  dwPowerState;
    HANDLE  hIST;
    HANDLE  hGPIO;
    HANDLE  hI2C;
    LONG    nPenIRQ;
    DWORD       dwISTPriority;
}TOUCH_DEVICE;


//------------------------------------------------------------------------------
//  Device registry parameters
static const DEVICE_REGISTRY_PARAM s_deviceRegParams[] = {
    {
        L"SampleRate", PARAM_DWORD, FALSE, offset(TOUCH_DEVICE, nSampleRate),
        fieldsize(TOUCH_DEVICE, nSampleRate), (VOID*)DEFAULT_SAMPLE_RATE
    },
    {
        L"PenGPIO", PARAM_DWORD, TRUE, offset(TOUCH_DEVICE, nPenGPIO),
        fieldsize(TOUCH_DEVICE, nPenGPIO), 0
    },
    {
        L"I2CBus", PARAM_DWORD, TRUE, offset(TOUCH_DEVICE, nI2CBusIndex),
        fieldsize(TOUCH_DEVICE, nI2CBusIndex), 0
	}, {
        L"I2CAddr", PARAM_DWORD, TRUE, offset(TOUCH_DEVICE, nI2CAddr),
        fieldsize(TOUCH_DEVICE, nI2CAddr), 0
	}, {
        L"I2CBaudrateIndex", PARAM_DWORD, FALSE, offset(TOUCH_DEVICE, nI2CBaudrateIndex),
        fieldsize(TOUCH_DEVICE, nI2CBaudrateIndex), (void*)0
    },
    {
        L"InitialSamplesDropped", PARAM_DWORD, FALSE, offset(TOUCH_DEVICE, nInitialSamplesDropped),
        fieldsize(TOUCH_DEVICE, nInitialSamplesDropped), (VOID*)DEFAULT_INITIAL_SAMPLES_DROPPED
    },
    {
        L"Priority256", PARAM_DWORD, FALSE, offset(TOUCH_DEVICE, dwISTPriority),
        fieldsize(TOUCH_DEVICE, dwISTPriority), (VOID*)DEFAULT_THREAD_PRIORITY
    },
    {
        L"SysIntr", PARAM_DWORD, FALSE, offset(TOUCH_DEVICE, dwSysIntr),
        fieldsize(TOUCH_DEVICE, dwSysIntr), (VOID*)SYSINTR_NOP
    }
};


//------------------------------------------------------------------------------
// global variables
//

static TOUCH_DEVICE s_TouchDevice =
    {
    FALSE,                                          //bInitialized
    DEFAULT_SAMPLE_RATE,                            //nSampleRate
    0,                                              //nInitialSamplesDropped
    0,                                              //nPenGPIO
    0,                                              //PenUpDebounceMS
    0,                                              //nI2CAddr
    0,                                              //nI2CBaudrateIndex
    0,                                              //nI2CBusIndex
    SYSINTR_NOP,                                    //dwSysIntr
    0,                                              //dwSamplingTimeOut
    FALSE,                                          //bTerminateIST
    0,                                              //hTouchPanelEvent
    D0,                                             //dwPowerState
    0,                                              //hIST
    0,                                              //hGPIO
    0,                                              //hI2C
    0,                                              //nPenIRQ
    DEFAULT_THREAD_PRIORITY                         //dwISTPriority
    };


// Internal functions.
static HRESULT PDDCalibrationThread();
void PDDStartCalibrationThread();

BOOL PDDGetTouchIntPinState( VOID );
BOOL PDDGetTouchData(UINT32 * pxPos, UINT32 * pyPos);
BOOL PDDInitializeHardware(LPCTSTR pszActiveKey );
VOID PDDDeinitializeHardware( VOID );
VOID  PDDTouchPanelDisable();
BOOL  PDDTouchPanelEnable();
ULONG PDDTouchIST(PVOID   reserved);
void PDDTouchPanelPowerHandler(BOOL boff);

//TCH PDD DDSI functions
extern "C" DWORD WINAPI TchPdd_Init(
    LPCTSTR pszActiveKey,
    TCH_MDD_INTERFACE_INFO* pMddIfc,
    TCH_PDD_INTERFACE_INFO* pPddIfc,
    DWORD hMddContext
    );

void WINAPI TchPdd_Deinit(DWORD hPddContext);
void WINAPI TchPdd_PowerUp(DWORD hPddContext);
void WINAPI TchPdd_PowerDown(DWORD hPddContext);
BOOL WINAPI TchPdd_Ioctl(
    DWORD hPddContext,
    DWORD dwCode,
    PBYTE pBufIn,
    DWORD dwLenIn,
    PBYTE pBufOut,
    DWORD dwLenOut,
    PDWORD pdwActualOut
    );

#endif
