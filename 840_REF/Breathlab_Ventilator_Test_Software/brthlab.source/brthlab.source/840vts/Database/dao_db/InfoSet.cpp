// InfoSet.cpp : implementation file
//

#include "stdafx.h"
#include "daocpvtdb.h"
#include "InfoSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInfoSet

IMPLEMENT_DYNAMIC(CInfoSet, CDaoRecordset)

CInfoSet::CInfoSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(CInfoSet)
	m_ID = 0;
	m_Info1 = _T("");
	m_Info2 = _T("");
	m_Info3 = _T("");
	m_Info4 = _T("");
	m_Info5 = _T("");
	m_LastSeq = 0;
	m_nFields = 7;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString CInfoSet::GetDefaultDBName()
{
	return _T(DB_PATH_NAME);
}

CString CInfoSet::GetDefaultSQL()
{
	return _T("[CPVTInfo]");
}

void CInfoSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(CInfoSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[ID]"), m_ID);
	DFX_Text(pFX, _T("[Info1]"), m_Info1);
	DFX_Text(pFX, _T("[Info2]"), m_Info2);
	DFX_Text(pFX, _T("[Info3]"), m_Info3);
	DFX_Text(pFX, _T("[Info4]"), m_Info4);
	DFX_Text(pFX, _T("[Info5]"), m_Info5);
	DFX_Long(pFX, _T("[LastSeq]"), m_LastSeq);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CInfoSet diagnostics

#ifdef _DEBUG
void CInfoSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void CInfoSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
