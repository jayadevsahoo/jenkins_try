// OATestSet.cpp : implementation file
//

#include "stdafx.h"
#include "daocpvtdb.h"
#include "OATestSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COATestSet

IMPLEMENT_DYNAMIC(COATestSet, CDaoRecordset)

COATestSet::COATestSet(CDaoDatabase* pdb)
	: CDaoRecordset(pdb)
{
	//{{AFX_FIELD_INIT(COATestSet)
	m_TID = 0;
	m_TName = 0;
	m_TPFInd = 0;
	m_Sequence = 0;
	m_nFields = 4;
	//}}AFX_FIELD_INIT
	m_nDefaultType = dbOpenDynaset;
}


CString COATestSet::GetDefaultDBName()
{
	return _T(DB_PATH_NAME);
}

CString COATestSet::GetDefaultSQL()
{
	return _T("[OATests]");
}

void COATestSet::DoFieldExchange(CDaoFieldExchange* pFX)
{
	//{{AFX_FIELD_MAP(COATestSet)
	pFX->SetFieldType(CDaoFieldExchange::outputColumn);
	DFX_Long(pFX, _T("[TID]"), m_TID);
	DFX_Long(pFX, _T("[TName]"), m_TName);
	DFX_Long(pFX, _T("[TPFInd]"), m_TPFInd);
	DFX_Long(pFX, _T("[Sequence]"), m_Sequence);
	//}}AFX_FIELD_MAP
}

/////////////////////////////////////////////////////////////////////////////
// COATestSet diagnostics

#ifdef _DEBUG
void COATestSet::AssertValid() const
{
	CDaoRecordset::AssertValid();
}

void COATestSet::Dump(CDumpContext& dc) const
{
	CDaoRecordset::Dump(dc);
}
#endif //_DEBUG
