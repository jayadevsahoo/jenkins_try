#ifndef FlowController_HH
#define FlowController_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: FlowController - Implements a flow controller that is using a 
// 		PSOL as a valve.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/FlowController.hhv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: syw     Date:  09-Mar-2000    DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Added PsolSide data member and added PsolSide to constructor arguments.
//		Added computeFeedForward_() and DeterminePsolLookupFlags() methods.
//		Added PsolCalibration_ data member.
//		Added Boolean argument to updatePsol() and computeKi_().
//
//  Revision: 008  By: yyy     Date:  29-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added computeKi_() to adjust the intergal gain every time it change.
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006 By: syw    Date: 21-Aug-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added psolStuckEventOccurred_ code to avoid multiple declarations.
//
//  Revision: 005 By: syw    Date: 16-Jul-1996   DR Number: DCS 1076
//  	Project:  Sigma (R8027)
//		Description:
//			Added controllerShutdown_ to shutdown controller when desired flow
//			is zero.  Remove startupGain_.
//
//  Revision: 004 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Implemented SafetyNetPsolStuckOpen test.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- removed isControllerActive related code since controller will
//			  explicitly command zero psol command if flow <= ZERO FLOW.
//			- closeLevel is no longer needed.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "PsolLiftoff.hh"

//@ Usage-Classes

class FlowSensor ;
class Psol ;

//@ End-Usage

//@ Constant: INTEGRAL_GAIN
// nominal flow controller integral gains
extern const Real32 INTEGRAL_GAIN ;				// in counts/lpm-msec

//@ Constant: ZERO_FLOW
// zero flow value 
extern const Real32 ZERO_FLOW ;

class FlowController 
{
  public:

	FlowController( Psol& psol, const FlowSensor& flowSensor, const PsolLiftoff::PsolSide side) ;
	virtual ~FlowController( void) ;
    	
	static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

	inline void setControllerShutdown( const Boolean shutdown) ;
    virtual void updatePsol( const Real32 desiredFlow, const Boolean usePsolLookup = FALSE) ;
	virtual void newBreath( void) ;
	inline Real32 getDesiredFlow( void) const ;

	static void DeterminePsolLookupFlags( const Real32 airFlow, const Real32 o2Flow,
										  Boolean& useAirLookup, Boolean& useO2Lookup) ;

	static void setAFactor( Real32 factor);
	static Real32 getAFactor();

	static void setBFactor( Real32 factor);
	static Real32 getBFactor();

	static void setCFactor( Real32 factor);
	static Real32 getCFactor();

	static void setDFactor( Real32 factor);
	static Real32 getDFactor();

	static void updatePsolCalibration(void);
	
  protected:

	inline Real32 computeFeedForward_( const Boolean usePsolLookup) ;
	void computeKi_( const Boolean usePsolLookup) ;
	void calculateGainTable();

	//@ Data-Member:::PsolSide psolSide_
	// air or o2 side
	PsolLiftoff::PsolSide psolSide_ ;

	//@ Data-Member: prevGain_
	// previous integral gain in counts/lpm-msec
	Real32 prevGain_ ;

	//@ Data-Member: ki_
	// integral gain in counts/lpm-msec
	Real32 ki_ ;

	//@ Data-Member: kff_
	// psol feedforward gain in counts/lpm
	Real32 kff_ ;
	
	//@ Data-Member: liftoff_
	// in counts.   psol command that achieves liftoff flow
	Real32 liftoff_ ;

	//@ Data-Member: integrator_ 
	// integral of the flow error
	Real32 integrator_ ;

	//@ Data-Member: desiredFlow_
	// input command into the flow controller
	Real32 desiredFlow_ ;

	//@ Data-Member: rPsol_
	// the controlled psol
	Psol& rPsol_ ;

	//@ Data-Member: rFlowSensor_
	//  the sensing element in the control circuit
	const FlowSensor& rFlowSensor_ ;

	//@ Data-Member: controllerShutdown_
	// to shut down controller when desired flow is zero.
	Boolean controllerShutdown_ ;

  private:
	FlowController( const FlowController&) ;	// Declared but not implemented
	FlowController( void) ;   					// Declared but not implemented
	void operator=( const FlowController&) ;   	// Declared but not implemented

	//@ Data-Member: Real32 aFactor_
	// The A factor for calculating the gain table
	static Real32 aFactor_;

	//@ Data-Member: Real32 bFactor_
	// The B factor for calculating the gain table
	static Real32 bFactor_;

	//@ Data-Member: Real32 cFactor_
	// The C factor for calculating the gain table
	static Real32 cFactor_;

	//@ Data-Member: Real32 dFactor_
	// The D factor for calculating the gain table
	static Real32 dFactor_;

	//@ Data-Member: Boolean factorChanged_
	// Indicates whether A, B, C, D factors have changed
	// causing a recalculation of the gain table.
	static Boolean factorChanged_;
	
	//@ Data-Member: PsolLiftoff PsolCalibration_
	// to store psol calibration data
	static PsolLiftoff PsolCalibration_ ;
} ;

// Inlined methods
#include "FlowController.in"

#endif // FlowController_HH 








