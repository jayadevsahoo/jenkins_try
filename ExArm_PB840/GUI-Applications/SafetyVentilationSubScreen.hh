#ifndef SafetyVentilationSubScreen_HH
#define SafetyVentilationSubScreen_HH

//=====================================================================
//	This is a proprietary work to which Puritan-Bennett corporation of
//	California claims exclusive right.  No part of this work may be used,
//	disclosed, reproduced, stored in an information retrieval system, or
//	transmitted by any means, electronic, mechanical, photocopying,
//	recording, or otherwise without the prior written permission of
//	Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//	Class: SafetyVentilationSubScreen - Automatically displays the current
//	"Safety PCV" ventilation settings.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SafetyVentilationSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Integration baseline.
//====================================================================

#include "SubScreen.hh"

//@ Usage-Classes
#include "Box.hh"
#include "Line.hh"
#include "NumericSettingButton.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class SafetyVentilationSubScreen : public SubScreen
{
public:
	SafetyVentilationSubScreen(SubScreenArea* pSubScreenArea);
	~SafetyVentilationSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	static void SoftFault(const SoftFaultID softFaultID,
							     const Uint32      lineNumber,
							     const char*       pFileName  = NULL, 
							     const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	SafetyVentilationSubScreen(void);					// not implemented...
	SafetyVentilationSubScreen(const SafetyVentilationSubScreen&);// not implemented...
	void operator=(const SafetyVentilationSubScreen&);	// not implemented...

    //@ Data-Member: titleText_
    // Subscreen title text.
    TextField titleText_;

    //@ Data-Member: subTitleText_
    // Subscreen sub-title text.
    TextField subTitleText_;
						 
    //@ Data-Member: footnoteText_
    // Subscreen footnote text.
    TextField footnoteText_;

    //@ Data-Member: modeText_
	// Mode readout for mode indicator bar (always AV for safety PCV).
    TextField modeText_;

    //@ Data-Member: mandatoryTypeText_
	// Mandatory type readout for mode indicator bar (always PCV
    // for safety PCV).
    TextField mandatoryTypeText_;

    //@ Data-Member: leftWingtip_
	// Left-hand "wingtip" line for the mode indicator bar.
    Line leftWingtip_;

    //@ Data-Member: leftWing_
	// Left-hand "wing" line for the mode indicator bar.
    Line leftWing_;

	//@ Data-Member: rightWing_
    // Right-hand "wing" line for the mode indicator bar.
   	Line rightWing_;

    //@ Data-Member: rightWingtip_
    // Right-hand "wingtip" line for the mode indicator bar.
    Line rightWingtip_;
 
    //@ Data-Member: triggerTypeText_
    // Trigger type readout for the mode indicator bar (always
    // P-trig for safety PCV).
    TextField triggerTypeText_;
 
    //@ Data-Member: modeBackground_
    // Black background for the mode indicator bar.
    Box modeBackground_;
 
    //@ Data-Member: buttonBackground_
    // Dark gray background for the button area.
    Box buttonBackground_;
 
	//@ Data-Member: flowAccelerationButton_
	// The Flow Acceleration setting button.
	NumericSettingButton flowAccelerationButton_;

    //@ Data-Member: inspiratoryPressureButton_
    // The fake Inspiratory Pressure main setting button.
    NumericSettingButton inspiratoryPressureButton_;
 
    //@ Data-Member: inspiratoryTimeButton_
    // The fake Inspiratory Time main setting button.
    NumericSettingButton inspiratoryTimeButton_;
 
    //@ Data-Member: oxygenPercentageButton_
    // The fake Oxygen Percentage main setting button.
    NumericSettingButton oxygenPercentageButton_;
 
    //@ Data-Member: peepButton_
    // The fake Peep main setting button.
    NumericSettingButton peepButton_;
 
    //@ Data-Member: pressureSensitivityButton_
    // The Pressure Sensitivity main setting button
    NumericSettingButton pressureSensitivityButton_;
 
    //@ Data-Member: respiratoryRateButton_                   
    // The fake Respiratory Rate main setting button.         
    NumericSettingButton respiratoryRateButton_;              
                                                
};

#endif // SafetyVentilationSubScreen_HH 
