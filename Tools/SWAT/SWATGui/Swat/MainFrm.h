// MainFrm.h : interface of the CMainFrame class
//


#pragma once

#include "SwatView.h"

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// Implementation
public:
	virtual ~CMainFrame();

	CSwatView *m_pMainView;



#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CReBar      m_wndReBar;
	CDialogBar      m_wndDlgBar;

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg LRESULT OnMessage_logRcvd(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessage_NewSettingsRcvd(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessage_TurnOnOffVent(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessage_CriticalErrorRcvd(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnMessage_ShowMessage(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessage_HideMessage(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessage_BreathPhase(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessage_ReportLog(WPARAM wParam, LPARAM lParam);
	afx_msg void OnWindowPosChanged(WINDOWPOS* lpwndpos);
};


