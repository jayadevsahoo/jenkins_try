#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Ostream - Stream debug output facility for multitasking
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is used for streaming debuging output messages
//  in a multitasking environment.
//---------------------------------------------------------------------
//@ Rationale
//  Provides debug information to help us resolve issues and
//  increase our productivity during the software development.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Overrides the iostream methods and operators to add additional 
//  debug information.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with 
//  pre-conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/Ostream.ccv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: rhj    Date:  03-Aug-2007    SCR Number: 6397 
//  Project: Trend
//  Description:
//	   Removed #define SIGMA_OFFICIAL and added comments.
//
//  Revision: 001    By: Gary Cederquist  Date: 15-APR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//=====================================================================

#include "OsFoundation.hh"
#include "Ostream.hh"
#include "Task.hh"

//@ Code...

#include <string.h>
#include <stdio.h>
//TODO E600 #include <bstring.h>


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize 
//
//@ Interface-Description
//	Initialize the ostream class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void OstreamTbl::Initialize(void)
{
	//  get one to initialize the function static
	Ostream& ostream = GetOstream();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getOstream 
//
//@ Interface-Description
//    Return a static instance of the Ostream.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  None
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream & OstreamTbl::getOstream(void)
{
    return GetOstream();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetOstream 
//
//@ Interface-Description
//    Return a static instance of the Ostream.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  None
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream & OstreamTbl::GetOstream(void)
{
    static Ostream ostream[Task::MAX_NUM_TASKS + 1];

    return ostream[Task::GetId()];
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Ostream 
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply call init() to handle the initialization of this class.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Ostream::Ostream()
{
    init();
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: init 
//
//@ Interface-Description
//	Initialize Ostream class with default values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void Ostream::init()
{
	hexFlag_=FALSE;
	charCount_=0;
	placePtr_ = buf_;
	taskName_ = NULL;
//TODO E600 	bzero(buf_,sizeof(buf_));
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Ostream 
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Ostream::~Ostream()
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator << (const char *s)
//
//@ Interface-Description
//    This method takes a pointer to a string, copies it into 
//    a buffer and returns a reference pointer of the Ostream class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  If the buffer is full or the string length is greater 
//    than the maximum BUF_SIZE, then flush the buffer.  Otherwise,
//    place the string into the buffer and retreive the number of 
//    characters in the string.
//---------------------------------------------------------------------
//@ PreCondition
//    The parameter must not be null.
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& Ostream::operator<<(const char *s)
{
	AUX_CLASS_PRE_CONDITION( s != NULL, *s);

	Int32 count = 0;

    // If the buffer is full or the string length is greater 
    // than the maximum BUF_SIZE, then flush the buffer. 
	if (bufferFull_() || (strlen(s) >= (Ostream::BUF_SIZE - charCount_)))
	{
		flush();
	}
	else 
	{
        // Otherwise, place the string into the buffer and retreive
	    // the number of characters in the string.
		count = sprintf(placePtr_,"%s",s);
		placePtr_ += count;
		charCount_ += count;
	}

	return *this;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator << (const void *p)
//
//@ Interface-Description
//    This method takes a pointer to a function, copies it into 
//    a buffer and returns a reference pointer of the Ostream class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Calls checkOverFlow method to flush the buffer if it is full.  
//    Then store the function pointer into the buffer.
//---------------------------------------------------------------------
//@ PreCondition
//    The parameter must not be null.
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& Ostream::operator<<(const void *p)
{
	AUX_CLASS_PRE_CONDITION( p != NULL, (Int32) p);
	Int32 count = 0;
	
	// Flush the buffer if it is full.
	checkOverFlow_();
	
	// Store the function pointer into the buffer
	count = sprintf(placePtr_,"0x%X",(Int32)p);
	
	// Store char counts.
	placePtr_ += count;
	charCount_ += count;
	return *this;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator << (const char c)
//
//@ Interface-Description
//    This method takes a pointer to a function, copies it into 
//    a buffer and returns a reference pointer of the Ostream class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Calls checkOverFlow method to flush the buffer if it is full.  
//    Then stores the char into the buffer.  If the hexFlag_ is TRUE,
//    then it stores the char as a hex value into the buffer.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& Ostream::operator<<(const char c)
{
	Int32 count = 0;
	
	// Flush the buffer if it is full.
	checkOverFlow_();
	
	// If hexFlag is true, store the char as an
	// hex value, else store the char to the buffer.
	if (!hexFlag_)
	{
		count = sprintf(placePtr_,"%c",c);
	}
	else
	{
		count = sprintf(placePtr_,"0x%x",c);
	}
	
	// Store char counts.
	placePtr_ += count;
	charCount_ += count;
	return *this;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator << (const int i)
//
//@ Interface-Description
//    This method takes a pointer to a function, copies it into 
//    a buffer and returns a reference pointer of the Ostream class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Calls checkOverFlow method to flush the buffer if it is full.  
//    Then stores the int into the buffer.  If the hexFlag_ is TRUE,
//    then it stores the int as a hex value into the buffer.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& Ostream::operator<<(const int i)
{
	Int32 count = 0;
	
	// Flush the buffer if it is full.
	checkOverFlow_();
	
	// If hexFlag is true, store the int as an
	// hex value, else store the int to the buffer.
	if (!hexFlag_)
	{
		count = sprintf(placePtr_,"%d",i);
	}
	else
	{
		count = sprintf(placePtr_,"0x%x",i);
	}
	
	// Store char counts.
	placePtr_ += count;
	charCount_ += count;
	return *this;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator << (const unsigned int u)
//
//@ Interface-Description
//    This method takes a pointer to a function, copies it into 
//    a buffer and returns a reference pointer of the Ostream class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Calls checkOverFlow method to flush the buffer if it is full.  
//    Then stores the unsigned int into the buffer.  If the hexFlag_ is TRUE,
//    then it stores the unsigned int as a hex value into the buffer.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& Ostream::operator<<(const unsigned int u)
{
	Int32 count = 0;

	// Flush the buffer if it is full.
	checkOverFlow_();
	
	// If hexFlag is true, store the unsigned int as an
	// hex value, else store the unsigned int to the buffer.
	if (!hexFlag_)
	{
		count = sprintf(placePtr_,"%u",u);
	}
	else
	{
		count = sprintf(placePtr_,"0x%x",u);
	}
	
	// Store char counts.
	placePtr_ += count;
	charCount_ += count;
	return *this;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator << (const float f)
//
//@ Interface-Description
//    This method takes a pointer to a function, copies it into 
//    a buffer and returns a reference pointer of the Ostream class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Calls checkOverFlow method to flush the buffer if it is full.  
//    Then stores the float into the buffer.  If the hexFlag_ is TRUE,
//    then it stores the float as a hex value into the buffer.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& Ostream::operator<<(const float f)
{
	Int32 count = 0;
	
	// Flush the buffer if it is full.
	checkOverFlow_();
	
	// Store the floating number into the buffer
	count = sprintf(placePtr_,"%f",f);

	// Store char counts.
	placePtr_ += count;
	charCount_ += count;
	return *this;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator << (const double d)
//
//@ Interface-Description
//    This method takes a pointer to a function, copies it into 
//    a buffer and returns a reference pointer of the Ostream class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Calls checkOverFlow method to flush the buffer if it is full.  
//    Then stores the double into the buffer.  If the hexFlag_ is TRUE,
//    then it stores the double as a hex value into the buffer.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& Ostream::operator<<(const double d)
{
	Int32 count = 0;
	
	// Flush the buffer if it is full.
	checkOverFlow_();

	// Store the double number into the buffer
	count = sprintf(placePtr_,"%e",d);

	// Store char counts.
	placePtr_ += count;
	charCount_ += count;
	return *this;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator<<(OmanipFunc func)
//
//@ Interface-Description
//   This method takes a function and returns a pointer of this 
//   class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  None
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& Ostream::operator<<(OmanipFunc func)  
{ 
    return (*func)(*this); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator<<(VOmanipFunc func)  
//
//@ Interface-Description
//    Method overrides the operator << (VOmanipFunc func).
//---------------------------------------------------------------------
//@ Implementation-Description
//	  None
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void Ostream::operator<<(VOmanipFunc func)  
{ 
    (*func)(*this); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: flush 
//
//@ Interface-Description
//    This method takes no parameters and flushes the buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Simply gets the current task name and uses printf to print the 
//    messages out.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void Ostream::flush(void)
{
    // If there are chars in the buffer,
	// then we must flush them out.
	if (charCount_ > 0)
	{
	    //  Note:
		//  output exceeding 64 characters during VRTX initialization 
		//  produces "unpredictable results" so output is not allowed
		//  prior to tasking

		if (Sys_Init::IsTaskingOn())
		{
			if (taskName_ == NULL)
			{
				taskName_ = Task::GetTaskInfo().getName();
            }
			// Put the message out using printf.
			printf("%-10.10s:%s", taskName_, buf_);
			// Task::Delay(0,200); add when the debugging gets tough
		}
		else 
		{
		    // no printf's before Tasking is on.
		}
	}
    // reset the number of chars and 
	// point to the current buffer.
	charCount_=0;
	placePtr_=buf_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkOverFlow_ 
//
//@ Interface-Description
//    This method takes no parameters and flushes the buffer if an 
//    over flow is detected.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Simply calls flush method if the number of characters is 
//    greater than the flush mark value.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void Ostream::checkOverFlow_(void)
{
	if (charCount_ >= (Int32)Ostream::FLUSH_MARK)
	{
		flush();
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bufferFull_ 
//
//@ Interface-Description
//    Returns TRUE if the buffer is full, otherwise it returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Simply compares the number of chars to the flush mark value.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Boolean Ostream::bufferFull_(void)
{
	return (charCount_ >= (Int32)Ostream::FLUSH_MARK);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dec 
//
//@ Interface-Description
//    Uses decimal format.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Sets the hexFlag to FALSE. 
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void Ostream::dec() 
{ 
	hexFlag_=FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hex 
//
//@ Interface-Description
//    Uses hex format. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Sets the hexFlag to TRUE. 
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void Ostream::hex() 
{ 
	hexFlag_=TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
Ostream::SoftFault(const SoftFaultID  softFaultID,
						                      const Uint32       lineNumber,
						                      const char*        pFileName,
						                      const char*        pPredicate)
{
	  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	  FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::OSTREAM, 
							  lineNumber, pFileName, pPredicate);
}


//=====================================================================
//                        FREE FUNCTIONS
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: endl
//
//@ Interface-Description
//    Adds a "\n" character and flushes the buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Adds a "\n" and calls the flush method to flush the buffer.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void endl(Ostream& os)
{
	os << "\n";
	os.flush();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: flush(Ostream& os)
//
//@ Interface-Description
//    Flushes the buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Simply calls the flush method.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
void flush(Ostream& os)
{
	os.flush();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: hex(Ostream& os)
//
//@ Interface-Description
//    Sets the output format to hex and return this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Simply calls the hex method and returns this class.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& hex(Ostream& os)
{
	os.hex();
	return os;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dec(Ostream& os)
//
//@ Interface-Description
//    Sets the output format to decimal and return this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Simply calls the decimal method and returns this class.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//@ End-Method
//=====================================================================
Ostream& dec(Ostream& os)
{
	os.dec();
	return os;
}





