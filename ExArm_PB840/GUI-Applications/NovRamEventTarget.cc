#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NovRamEventTarget - A target to handle NovRam update events.
// Classes can specify this class as an additional parent class and register
// to be informed of NovRam update events.
//---------------------------------------------------------------------
//@ Interface-Description
// Any classes which need to be informed of NovRam update events (where updates
// to NovRam data are communicated to interested tasks) need to multiply
// inherit from this class.  Then, when they register for update events with
// the NovRamEventRegistrar class, they will be informed of events via the
// novRamUpdateEventHappened() method.
//---------------------------------------------------------------------
//@ Rationale
// Used to implement the callback mechanism needed to communicate
// NovRam update events to objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// If classes need to be informed of NovRam update events they must do 3
// things: a) multiply inherit from this class, b) define the
// novRamUpdateEventHappened() method of this class and c) register for change
// notices via the NovRamEventRegistrar class.  They will then be informed of
// Novram events via the novRamUpdateEventHappened() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/NovRamEventTarget.ccv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "NovRamEventTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: novRamUpdateEventHappened	[virtual]
//
//@ Interface-Description
// This method should be redefined in derived classes.  It is via this method
// that NovRam update events are communicated.  It is passed one parameter,
// 'updateId', which identifies which NovRam data storage was updated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
NovRamEventTarget::novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId)
{
	CALL_TRACE("NovRamEventTarget::novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NovRamEventTarget()  [Default Constructor, Protected]
//
//@ Interface-Description
// The constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NovRamEventTarget::NovRamEventTarget(void)
{
	CALL_TRACE("NovRamEventTarget::NovRamEventTarget(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NovRamEventTarget  [Destructor, Protected]
//
//@ Interface-Description
// Nothing to destruct!
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

NovRamEventTarget::~NovRamEventTarget(void)
{
	CALL_TRACE("NovRamEventTarget::~NovRamEventTarget(void)");

	// Do nothing
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
NovRamEventTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("NovRamEventTarget::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, NOVRAMEVENTTARGET,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
