/*****************************************************************************/
/*   Module Name:    prmptpnl.h                                          
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _PRMPTPNL
#define _PRMPTPNL

int PromptDisplay(int panelhandle);
int PromptDiscard(int panelhandle);
int TestPrompt(int State);

typedef enum {
	Initial = 0,
	Clear,
	Discard,
	BDUAlarm,
	GUIAlarm,
	Met1,
	Met2,
	Met3,
	CPT1,
	CPT2,
	SSTPanel,
	DoneType,
	YesNoType,
} PPstates;

#endif // #ifndef _PRMPTPNL
