#include "stdafx.h"
//----------------------------------------------------------------------------
//            Copyright (c) 2014-2025 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------
#include "GasInletPressureSensor.hh"
#include "TransducerCalibrationTable.h"


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
GasInletPressureSensor::GasInletPressureSensor( SafetyNetSensorData *pData,
			const AdcChannels::AdcChannelId AdcId,
			const Real32 alpha,
			TransducerCalibrationTable_t* transducerCalTable ):
	PressureSensor (pData, AdcId, alpha )
{
	CLASS_ASSERTION(transducerCalTable != NULL )

	zeroOffset_ = transducerCalTable->GetZeroCounts();
	spanCounts_ = transducerCalTable->GetSpanCounts();
}

//------------------------------------------------------------------------------
// rawToEngValue_
//------------------------------------------------------------------------------
Real32
GasInletPressureSensor::rawToEngValue_( const AdcCounts count) const
{
	// Maximum and minimum value readings for the inlet pressure sensor
	static const Real32 SPAN_CAL_PRESSURE = 15.0f;
	static const Real32 ZERO_CAL_PRESSURE = 0.0f;

	Real32 	engValue = ( (SPAN_CAL_PRESSURE - ZERO_CAL_PRESSURE) / (spanCounts_ - zeroOffset_) )
			* ( count - zeroOffset_ );

	return engValue;
}


//------------------------------------------------------------------------------
// Destrcutor
//------------------------------------------------------------------------------
GasInletPressureSensor::~GasInletPressureSensor(void)
{
}

//------------------------------------------------------------------------------
// SoftFault
//------------------------------------------------------------------------------

void
GasInletPressureSensor::SoftFault( const SoftFaultID  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate )
{
      CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

      FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, GASINLETPRESSURESENSOR,
                               lineNumber, pFileName, pPredicate );
}
