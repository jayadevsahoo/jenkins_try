
#ifndef BdAlarmId_HH
#define BdAlarmId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct: BdAlarmId - defines all BD alarm IDs
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BdAlarmId.hhv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//   Revision 010   By: rhj    Date: 05-May-2010    SCR Number:  6436
//   Project:  PROX
//   Description:
//      Added two new alarms BDALARM_NON_FUNCTIONAL_PROX     
//      and BDALARM_FUNCTIONAL_PROX.
//      
//   Revision 009   By: rhj    Date: 26-Jan-2009    SCR Number:  6452
//   Project:  840S
//   Description:
//      Added a new alarm BDALARM_DISCONNECT_WITH_COMPRESSOR.
//      
//  Revision: 008   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 007   By: gdc   Date:  16-Feb-2005    DR Number: 6144
//  Project:  NIV1
//  Description:
//      DCS 6144 - NIV1
//      removed obsolete BDALARM_LOW_INSP_PRESSURE alarm event
//      replaced with user settable low circ pressure (low Ppeak) alarm
//
//  Revision: 006   By: heatherw   Date:  7-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added BDALARM_INSPIRED_MAND_VOLUME_LIMIT and BDALARM_INSPIRED_SPONT_PRESSURE_LIMIT
//      Changed BDALARM_INSPIRED_VOLUME_LIMIT to BDALARM_INSPIRED_SPONT_VOLUME_LIMIT.
//
//  Revision: 005   By: jja   Date:  11-Sept-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added BDALARM_LOW_INSP_PRESSURE, BDALARM_VCP_VOLUME_NOT_DELIVERED,
//		BDALARM_VS_VOLUME_NOT_DELIVERED
//
//  Revision: 004   By: syw   Date:  25-Jul-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added BDALARM_PAV_STARTUP, BDALARM_NOT_PAV_STARTUP,
//			  BDALARM_PAV_ASSESSMENT, BDALARM_NOT_PAV_ASSESSMENT
//
//  Revision: 003  By:  healey     Date: 14-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial release
//             Added a new bd alarms BDALARM_COMPENSATION_LIMIT and 
//             BDALARM_INSPIRED_VOLUME_LIMIT
//
//  Revision: 002  By:  iv     Date: 26-Sep-1997    DR Number: DCS 1475
//       Project:  Sigma (R8027)
//       Description:
//             Added a new bd alarm for volume limit.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

//@ Usage-Classes
//@ End-Usage

struct BdAlarmId
{
  public:
    //@ Type: BdAlarmIdType
    // Ids for all of the BD Alarms
    enum BdAlarmIdType
    {
        BDALARM_WALL_AIR_PRESENT,
        BDALARM_NOT_WALL_AIR_PRESENT,
        BDALARM_COMP_AIR_PRESENT,
        BDALARM_NOT_COMP_AIR_PRESENT,
        BDALARM_COMP_OPTION_PRESENT,
        BDALARM_NOT_COMP_OPTION_PRESENT,
        BDALARM_FIO2_HIGH,
        BDALARM_FIO2_NORMAL,
        BDALARM_FIO2_LOW,
        BDALARM_WALL_O2_PRESENT,
        BDALARM_NOT_WALL_O2_PRESENT,
        BDALARM_O2_MONITOR_INSTALLED,
        BDALARM_NOT_O2_MONITOR_INSTALLED,
        BDALARM_INTERNAL_BATTERY_POWER,
        BDALARM_NOT_INTERNAL_BATTERY_POWER,
        BDALARM_BATTERY_TIME_LEFT_LT_2_MIN,
        BDALARM_NOT_BATTERY_TIME_LEFT_LT_2_MIN,
        BDALARM_NON_FUNCTIONAL_BATTERY,
        BDALARM_NOT_NON_FUNCTIONAL_BATTERY,
        BDALARM_CHARGE_TIME_GT_8_HRS,
        BDALARM_NOT_CHARGE_TIME_GT_8_HRS,
        BDALARM_LOW_AC_POWER,
        BDALARM_NOT_LOW_AC_POWER,
        BDALARM_100_PERCENT_O2_REQUEST,
        BDALARM_NOT_100_PERCENT_O2_REQUEST,
        BDALARM_VOLUME_LIMIT,

        BDALARM_INSP_TIME_TOO_LONG,
        BDALARM_HIGH_CIRCUIT_PRESSURE,
        BDALARM_VENT_PRESSURE,
		BDALARM_COMPENSATION_LIMIT,
		BDALARM_INSPIRED_SPONT_VOLUME_LIMIT,
        BDALARM_INSPIRED_SPONT_PRESSURE_LIMIT,
        BDALARM_INSPIRED_MAND_VOLUME_LIMIT,
		
		BDALARM_PAV_STARTUP,
		BDALARM_NOT_PAV_STARTUP,
		BDALARM_PAV_ASSESSMENT,
		BDALARM_NOT_PAV_ASSESSMENT,
		
		BDALARM_VCP_VOLUME_NOT_DELIVERED, 
		BDALARM_VS_VOLUME_NOT_DELIVERED, 

		BDALARM_NON_FUNCTIONAL_PROX,     
        BDALARM_FUNCTIONAL_PROX,          		

        FIRST_SCHEDULER_ALARM,
        BDALARM_IN_APNEA_VENTILATION = FIRST_SCHEDULER_ALARM,
        BDALARM_DISCONNECT,
        BDALARM_DISCONNECT_WITH_COMPRESSOR,
        BDALARM_STARTUP_DISCONNECT,
        BDALARM_PROCEDURE_ERROR,
        BDALARM_SEVERE_OCCLUSION,
        LAST_SCHEDULER_ALARM = BDALARM_SEVERE_OCCLUSION,

        FIRST_NOT_SCHEDULER_ALARM,
        BDALARM_NOT_IN_APNEA_VENTILATION = FIRST_NOT_SCHEDULER_ALARM,
        BDALARM_NOT_DISCONNECT,
        BDALARM_NOT_STARTUP_DISCONNECT,
        BDALARM_NOT_PROCEDURE_ERROR,
        BDALARM_NOT_SEVERE_OCCLUSION,
        LAST_NOT_SCHEDULER_ALARM = BDALARM_NOT_SEVERE_OCCLUSION,

        NUM_TOTAL_BDALARMS
    };
};

#endif // BdAlarmId_HH 


