#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DisconnectTrigger - Monitors the system for the specific
//      conditions that cause patient circuit disconnect to be detected.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers a change to disconnect mode based on three
//    criteria. The first criteria has two conditions that are based on 
//    exhalation pressure and flow readings. The first condition is only valid for 
//    the first 200 msec of exhalation. The second condition is valid for
//    the entire period of exhalation. The second criteria is based on 
//    the delivered volume and the exhaled volume.
//    The first and second criteria are applied to non SPONT breaths only. 
//    The third criteria is based on delivered flow, flow comand limit, 
//    inspiratory time and breath duration. The third criteria applied only to 
//    SPONT breaths.
//    The trigger can be enabled or disabled, depending upon
//    the current mode of breathing and the applicability of the trigger.
//    When the trigger conditions calculated by this trigger are true, 
//    the trigger is considered to have "fired".  Any scheduler needing 
//    this trigger must have it on its ModeTrigger list.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for detecting a patient circuit 
//    disconnect, and triggers the transition to the disconnect mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of 
//    whether this trigger is enabled or not.  If the trigger is not 
//    enabled, it will always return state of false. The trigger
//    condition is based on three criteria.
//    If these criteria have been met, the trigger fires by notifing 
//    the currently active  scheduler of its state. 
//    This trigger resides on the lists of any BreathPhaseScheduler that
//    requires detection of disconnect to be active.  When any of those
//    schedulers are active, the trigger is monitored every BD cycle.
//    However, the trigger only checks its condition at the beginning of a
//    new breath.
//---------------------------------------------------------------------
//@ Fault-Handling
//   n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DisconnectTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 037   By:   rhj    Date: 13-Mar-2009      SCR Number:  6482     
//  Project:  840S2
//  Description:
//       Moved the leak compensation circuit disconnect algorithms to 
//       the LeakCompMgr class.
//             
//  Revision: 036   By:   rhj    Date: 19-Jan-2009      SCR Number:   6452   
//  Project:  840S
//  Description:
//       Changed the way to retreive the disconnect sensitivity setting
//       by using RLeakCompMgr.getDiscoSensitivity() instead of using the
//       PhasedInContextHandle::GetBoundedValue() method.
//             
//  Revision: 035   By:   rhj    Date: 07-July-2008      SCR Number: 6435 
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 034   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 033  By: gdc     Date:  20-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//      DCS6170 - NIV2
//      Disabled spontaneous breath disconnect detection if Dsens is OFF
//
//  Revision: 032  By: gdc     Date:  23-Nov-2004    DR Number: 6144
//  Project:  NIV1
//  Description:
//		Changed disconnect algorithm to accomodate sensitivity = OFF
//		See detectDisconnect_() comments
//
//  Revision: 031  By: syw     Date:  13-Jul-1999    DR Number: 5472
//  Project:  ATC
//  Description:
//		set factor = discSen when the last 2 end exh pressure differ by less
//		than 0.3 during VCV.
//
//  Revision: 030  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 029  By:  iv    Date: 08-Sep-1998     DR Number:DCS 5140
//       Project:  BiLevel
//       Description:
//          Changed volume criteria to use new member isSecondCriteriaDisabled_
//          and do not disable volume criteria when not volume base by setting
//          factor = discSen.
//
//  Revision: 028  By: syw     Date:  14-May-1998    DR Number: DCS 5095
//  	Project:  Sigma (840)
//		Description:
//			Use getMaxExpiredVolume() instead of getExpiredVolume() to
//			prevent declaring disconnect if there is a lung leak.
//
//  Revision: 027  By:  syw    Date: 08-Jan-1998     DR Number:
//       Project:  Sigma (R8027)
//       Description:
//			BiLevel Initial Version.
//
//  Revision: 026  By:  iv    Date: 18-Sep-1997     DR Number: DCS 1649
//       Project:  Sigma (R8027)
//       Description:
//             Deleted the mechanism to force disconnect.
//
//  Revision: 025  By: iv    Date:  20-Aug-1997    DR Number: DCS 2403
//  	Project:  Sigma (840)
//		Description:
//			Added a condition in detectDisconnect_() to check for autozero
//          maneuver.
//
//  Revision: 024  By: syw   Date:  23-May-1997    DR Number: DCS 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 023  By:  sp    Date: 1-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 022  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//             Changed rExhDryFlowSensor.getValue() to rExhFlowSensor.getDryValue().
//
//  Revision: 021  By:  sp    Date: 14-Feb-1997    DR Number: DCS 1755
//       Project:  Sigma (R8027)
//       Description:
//		Changed the factor as the control spec specified.
//
//  Revision: 020  By:  sp    Date: 09-Jan-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework - added DCS 1093.
//
//  Revision: 019  By:  sp    Date: 29-Oct-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 018  By:  sp    Date: 16-Oct-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Replaced MIN_VALUE with MAX_VALUE in second criteria.
//
//  Revision: 017  By:  sp    Date: 10-Oct-1996    DR Number:DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Traceability.
//
//  Revision: 016  By:  sp    Date: 30-Sept-1996    DR Number:DCS 1388
//       Project:  Sigma (R8027)
//       Description:
//             Added the mechanism to force disconnect.
//
//  Revision: 015  By:  sp    Date: 24-Sept-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added unit test items. Fixed comments.
//
//  Revision: 014  By:  sp    Date: 23-Sept-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed the check for interval number to 1.
//             Modified the disconnect sensitivity factor.
//             Move the peep recovery condition to the second criteria only.
//
//  Revision: 013  By:  sp    Date: 18-Sept-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed MIN_VALUE to MAX_VALUE. Modified usage of DISCO_SENS. 
//             Added unit test items and unit test methods were removed. 
//
//  Revision: 012  By:  sp    Date: 13-Sept-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Change GetIsFlowResumed to IsFlowResumed.
//
//  Revision: 011  By:  sp    Date: 12-Sept-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Disconnect sensitivity is in percentage.
//
//  Revision: 010  By:  sp    Date: 3-Sept-1996    DR Number:DCS 1293, 1060, 1079, 792, 1093
//       Project:  Sigma (R8027)
//       Description:
//             update per controller spec 5.0.
//
//  Revision: 009  By:  sp    Date: 19-Aug-1996    DR Number:DCS 1218
//       Project:  Sigma (R8027)
//       Description:
//             Initialize IsTidalVolumeChanged_ to FALSE.
//
//  Revision: 008  By:  sp    Date: 13-Aug-1996    DR Number:DCS 1218
//       Project:  Sigma (R8027)
//       Description:
//             Add call back register for tidal volume change.
//
//  Revision: 007  By:  iv    Date:  3-July-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fixed an assertion in detectDisconnet_(). Added a check
//             for inspiration phase when checking volume limit.
//
//  Revision: 006  By:  sp    Date:  1-July-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fixed false disconnect in apnea.
//
//  Revision: 005  By:  sp    Date:  6-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added requirement number 4142 and 4144.
//
//  Revision: 004  By:  sp    Date:  28-May-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fix comment.
//
//  Revision: 003  By:  sp    Date:  30-Apr-1996    DR Number: DCS 927
//       Project:  Sigma (R8027)
//       Description:
//             new disconnect trigger.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "DisconnectTrigger.hh"

//@ Usage-Classes
#include "BreathSet.hh"
#include "BreathMiscRefs.hh"
#include "MainSensorRefs.hh"
#include "Sensor.hh"
#include "ExhFlowSensor.hh"
#include "PressureSensor.hh"
#include "MathUtilities.hh"
#include "PhasedInContextHandle.hh"
#include "PressureXducerAutozero.hh"
#include "MandTypeValue.hh"
#include "LeakCompEnabledValue.hh"
#include "Peep.hh"
#include "LeakCompMgr.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisconnectTrigger 
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The triggerId is set by the base class constructor.  
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
DisconnectTrigger::DisconnectTrigger(void) 
: ModeTrigger(DISCONNECT)
{
  CALL_TRACE("DisconnectTrigger(void)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DisconnectTrigger 
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
DisconnectTrigger::~DisconnectTrigger(void)
{
  CALL_TRACE("~DisconnectTrigger(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable
//
//@ Interface-Description
//    This method takes no parameters and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The state data member isEnabled_ is set to TRUE, enabling this trigger.  
//    Data members discCount1_, discCount2_, criteria2Count_ and desiredFlow_ 
//    are set to 0. Data members flowHasResumed_, firstCriteria_ and secondCriteria_
//    are set to false. Data members flowCmdLimit_ and timeLimit_ are set 
//    to artificially high value to prevent false triggering 
//    during the first cycle.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
DisconnectTrigger::enable(void)
{
  CALL_TRACE("enable(void)");

  // $[TI1]
  firstCriteria_ = FALSE; 
  secondCriteria_ = FALSE; 
  discCount1_ = 0;
  discCount2_ = 0;
  desiredFlow_ = 0;
  criteria2Count_ = 0;
  flowCmdLimit_ = 9999.99F;
  timeLimit_ = 9999;
  isEnabled_ = TRUE;
  isSecondCriteriaDisabled_ = FALSE;
  flowHasResumed_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
DisconnectTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, DISCONNECTTRIGGER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If current breath is not NON_MEASURED then method detectDisconnect_ is called
//    to determine patient disconnect. Method detectSpontDisconnect_ is called to
//    detect disconnect for SPONT or peep recovery breath or biLevel low to high 
//    mandatory breath.  If patient disconnect is detected then returns true else 
//    returns false.
// $[04147]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
DisconnectTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean disconnect = FALSE;
  Boolean spontDisconnect = FALSE;
  BreathRecord* pCurrentBreath = RBreathSet.getCurrentBreathRecord();
  CLASS_PRE_CONDITION( pCurrentBreath != NULL);
  
  if (pCurrentBreath->getBreathType() != ::NON_MEASURED)
  {

    // $[TI5]
    disconnect = detectDisconnect_(pCurrentBreath);

    if ( (pCurrentBreath->getBreathType() == SPONT) ||
         BreathRecord::IsPeepRecovery() || BreathRecord::GetIsMandBreath())
    {
      // $[TI1]
      spontDisconnect = detectSpontDisconnect_();
    }
    // $[TI2]

    delayTrig_ += CYCLE_TIME_MS;
  }
  // $[TI6]

  // TRUE: $[TI3]
  // FALSE: $[TI4]
  return (disconnect || spontDisconnect);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: detectDisconnect_
//
//@ Interface-Description
//    This method takes a pointer to BreathRecord as a parameter.  
//    If the disconnect condition has occured, the method returns 
//    true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    They are two disconnect criteria:
//    (1a) (exh pressure >= -.5) AND
//        (exh pressure <= .5 ) AND
//        (exh flow <= .5) AND
//        ( no pressure transducer autozero maneuver ) 
//        for the first 200 ms of exhalation for 20 consecutive cycles.
//    (1b) (exh pressure >= -.5) AND
//        (exh pressure <= .5 ) AND
//        (exh flow <= flow target * disconnect sensitivity)
//        for a duration of 10 seconds.
//    (2a) If Leak compensation is enabled, check the following criteria:
//         leak volume > maximum leak volume OR
//         VCV == MAND_TYPE_VALUE AND EIP < PEEP + 1 AND
//         EIP > - 1
//    (2b) ( exh volume < (inps volume * factor) )
//        factor = (EIP - EEP)/(EIP - last EEP) * disconnect sensitivity
//        for 3 consecutive breaths. 
//    If disconnect sensitivity setting is OFF: 
//      1a) remains in effect
//      1b) remains in effect except a disconnect sensitivity of 95%
//          is used in the algorithm
//       2) is bypassed entirely
//
//    If one of the conditions is true, then this method returns TRUE.  
//    Otherwise, this method returns FALSE.
// $[04318] $[04319]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
DisconnectTrigger::detectDisconnect_(BreathRecord* pBreathRecord)
{
  CALL_TRACE("detectDisconnect_(BreathRecord* pBreathRecord)");

  const Real32 DISCO_SENS_SETTING = RLeakCompMgr.getDiscoSensitivity();

  Real32 exhPressure = ((Sensor&)RExhPressureSensor).getValue();
  Boolean condition2 = FALSE;
  
  if (BreathRecord::GetIntervalNumber() == 1) 
  {
    // $[TI1]
    if ( !flowHasResumed_ && secondCriteria_)
    {
      // $[TI3]
      criteria2Count_++;
    }
    // $[TI4]
    
    // reset counter if the previous breath is not disconnect
    if (!secondCriteria_)
    {
      // $[TI5]
      criteria2Count_ = 0;
    }
    // $[TI6]

    isSecondCriteriaDisabled_ = FALSE;
    flowHasResumed_ = FALSE;
  }
  // $[TI2]

  // first criteria
  // first condition
  if (BreathRecord::GetPhaseType() == BreathPhaseType::EXHALATION)
  {
    // $[TI7]
    Int32 exhTime = (Int32) (pBreathRecord->getBreathDuration() -
                            BreathRecord::GetInspiratoryTime());
    if (exhTime <= 200)
    {
      // $[TI8]
      if ( (exhPressure >= -.5F) && (exhPressure <= .5F) &&    
           ( RExhFlowSensor.getDryValue() <= .5F ) &&
           ( !RPressureXducerAutozero.getIsAutozeroInProgress() ) )
      {
        // $[TI9]
        discCount1_ += CYCLE_TIME_MS;
      }
      else
      {
        // $[TI10]
        discCount1_ = 0;
      }
    }
    // $[TI11]

    //second condition
    Real32 discSen;

    // If Leak Compensation is enabled or when it is disabled 
    // with disconnect sensitivity is off, set disconnect senstivity
    // to 5%.
	if (DISCO_SENS_SETTING > 100.0F || RLeakCompMgr.isEnabled())
	{
	  // $[TI28.1]
	  discSen = 0.05F;
	}
	else
	{
	  // $[TI28.2]
	  discSen = 1.0F - DISCO_SENS_SETTING / 100.0F;
	}

    Real32 flowLimit = MAX_VALUE(BreathRecord::GetFlowTarget() * discSen, .5F);

    if ( (exhPressure >= -.5F) && (exhPressure <= .5F) &&    
         ( RExhFlowSensor.getDryValue() <= flowLimit ) )
    {
      // $[TI12]
      discCount2_ += CYCLE_TIME_MS;
    }
    else
    {
      // $[TI13]
      discCount2_ = 0;
    }
  
    if (discCount1_ == 100)
    {
      // $[TI14]
      delayTrig_ = exhTime;
      firstCriteria_ = TRUE;
    }
    else if (discCount2_ == 10000)
    {
      // $[TI15]
      condition2 = TRUE;
    }
    // $[TI16]

    // second criteria
    Real32 endInspPress = BreathRecord::GetEndInspPressure();
    Real32 numerator = endInspPress - BreathRecord::GetEndExpPressureComp();
    Real32 denominator = endInspPress - BreathRecord::GetLastEndExpPressure();
    // this will disable disconnect detection
    Real32 factor = 0.0F;
    // but, if not VCV phase, there is no reason to disable disconnect detection  
    if (pBreathRecord->getMandatoryType() != MandTypeValue::VCV_MAND_TYPE)
    {
        // $[TI26]
        factor = discSen;
    }
	// $[TI27]

    if (denominator > 0.1F)
    {
        // $[TI24]
        factor = numerator / denominator * discSen;
    }
    else
    {
	    // $[TI25]
	    Real32 diff = BreathRecord::GetEndExpPressureComp() - BreathRecord::GetLastEndExpPressure() ;

	    if (ABS_VALUE( diff) < 0.3 && pBreathRecord->getMandatoryType() == MandTypeValue::VCV_MAND_TYPE)
	    {
			// $[TI28]
	        factor = discSen ;
	    }
		// $[TI29]
    } 

    // $[LC24039] -- When the Leak Compensation is ON, the Circuit Disconnect condition...
	if (RLeakCompMgr.isEnabled())
	{
        
		const DiscreteValue  MAND_TYPE_VALUE =
			PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);
	    if ( ( (RLeakCompMgr.isDisconnectDetected()) || 
			   ( (MAND_TYPE_VALUE == MandTypeValue::VCV_MAND_TYPE) &&  (endInspPress < RPeep.getActualPeep() + 1.0F)  && 
				 (endInspPress > -1.0F)) ) &&
			 !BreathRecord::IsPeepRecovery() && !isSecondCriteriaDisabled_ )
		{
		  // $[TI17]
		  secondCriteria_ = TRUE;
		}
		else
		{
		  // $[TI18]
		  secondCriteria_ = FALSE;
		}

	}
	else
	{
#if 0 // TODO E600_LL: This detection method does not work currently; disabled for now and will revisit
		// when exh flow sensor and peep controller have been fully implemented
		if ( DISCO_SENS_SETTING < 100.0F &&
			(pBreathRecord->getMaxExpiredVolume() < (pBreathRecord->getInspiredVolume() * factor)) &&
			 !BreathRecord::IsPeepRecovery() && !isSecondCriteriaDisabled_ )
		{
		  // $[TI17]
		  secondCriteria_ = TRUE;
		}
		else
#endif
		{
		  // $[TI18]
		  secondCriteria_ = FALSE;
		}

	}

    if (BreathRecord::IsExhFlowFinished() && secondCriteria_ && !flowHasResumed_)
    {
      // $[TI19]
      flowHasResumed_ = TRUE;
      criteria2Count_++;
    }
    // $[TI20]
  } 
  // $[TI21]


  // FALSE: $[TI22]
  // TRUE: $[TI23]
  return ( (firstCriteria_ && (delayTrig_ >= 300)) || condition2 || criteria2Count_ >= 3);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: detectSpontDisconnect_
//
//@ Interface-Description
//    This method takes no parameters.  If the disconnect condition for SPONT 
//    has occured, the method returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The disconnect criteria for SPONT breaths:
//    (1a) (desiredFlow_ >= flowCmdLimit_) AND (inspTime >= timeLimit_)
//        AND (disconnectSensitivity is not OFF)
//    (1b) (desiredFlow_ >= flowCmdLimit_) AND (inspTime >= timeLimit_)
//         AND (leak volume > maximum leak volume)
//    If the condition is true, then this method returns TRUE.  
//    Otherwise, this method returns FALSE.
// $[04320]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
DisconnectTrigger::detectSpontDisconnect_()
{
  CALL_TRACE("detectSpontDisconnect_()");

  Boolean condition4 = FALSE;

  // Use the original disconnect sensitivity when leak comp is disabled.
  // Otherwise turn off disconnect sensitivity.
  if (!RLeakCompMgr.isEnabled())
  {
	 condition4 =  (RLeakCompMgr.getDiscoSensitivity() 
			< 100.0f);
  }
  else
  {
     condition4 = ( (RLeakCompMgr.getPrevLeakVolume() > RLeakCompMgr.getMaxPreviousLeakVolume() ));
  }

  if ( ( (desiredFlow_ > flowCmdLimit_) || 
         IsEquivalent(desiredFlow_, flowCmdLimit_, TENTHS) ) &&
       (BreathRecord::GetInspiratoryTime() >= timeLimit_) &&
	    condition4 )
  {
    // $[TI1]
    return TRUE;
  }
  else 
  {
    // $[TI2]
    return FALSE;
  }                        
}

