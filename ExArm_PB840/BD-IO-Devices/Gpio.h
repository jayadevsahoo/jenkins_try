#ifndef GPIO_H
#define GPIO_H

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

#include "Sigma.hh"
#include "AioDioDriver.h"

class Gpio_t
{
    public:
        // Only friends get direct access to the hardware.
        // A friend needs to run in the context of a high priority thread
        friend class BitAccessGpio;

        Gpio_t(const ApplicationDigitalOutputs_t bit, Uint8 initState);
        Gpio_t(const ApplicationDigitalInputs_t bit);
        virtual ~Gpio_t();

        Uint8 Get() { return currentState_; }

        GpioDirection_t GetGpioDirection()
        {
            return GpioDirection;
        }


    protected:
        virtual void Set();
        virtual void Clear();
        virtual Uint8 Read();
        virtual void  SetPendingState(Uint8 newState);
		virtual Uint8 ReadPendingState();
        virtual void  newCycle();

        Uint16  bit_;
        Uint8   currentState_;
        Uint8   pendingState_;

        GpioDirection_t GpioDirection;

    private:
        Gpio_t();           // Don't allow instantiation of default contructor
};

#endif
