#ifndef	SensorDataBuffer_HH
# define SensorDataBuffer_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 2008, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  SensorDataBuffer
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SensorDataBuffer.hhv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: ERM      Date: 5-Feb-2008  DCS Number: 6441
//  Project: 840S
//  Description:
//  Introduced encapsulated version of class IDs.
//
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SerialBuffer.hh"

//@ Usage-Classes
//@ End-Usage


class MsgQueue;

class SensorDataBuffer : public SerialBuffer
{
  public:
      enum { MAX_SENSOR_DATA_POINTS = 600 };      //(1 second / 20 millseconds) * 30 seconds 

      struct SensorEntry {    //max per breath data
             Real32 pressureValues[MAX_SENSOR_DATA_POINTS]; 
             Real32 flowValues[MAX_SENSOR_DATA_POINTS ];
             Uint32 count;    //number of points used  

      };

	enum
	{
		 SENSOR_DATA_SIZE  = (sizeof(SensorDataBuffer::SensorEntry) + sizeof(Uint32)) 
		,SENSOR_HOLD_COUNT = 2        // holds upto  2 breath
	};

	SensorDataBuffer(void);
	virtual ~SensorDataBuffer(void);

    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32      lineNumber,
                           const char*       pFileName  = NULL,
                           const char*       pPredicate = NULL);

  protected:

  private:

	//@ Data-Member: packetMemory_
	//  Contains the SMCP message request packets.
	Uint packetMemory_[  SensorDataBuffer::SENSOR_DATA_SIZE
	                   * SensorDataBuffer::SENSOR_HOLD_COUNT  / sizeof(Uint) ];

	//@ Data-Member: packetMap_
	//  Array containing pointers to each packet in packetMemory_.
	Uint * packetMap_[ SensorDataBuffer::SENSOR_HOLD_COUNT  ];
};

#endif	// ! SensorDataBuffer_HH
