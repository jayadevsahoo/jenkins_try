#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaPlateauTimeSetting - Apnea Plateau Time Setting
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the amount of time (in
//  milliseconds) an inspiration is held in the patient's airway after an
//  inspiratory flow has ceased during apnea ventilation.  This class
//  inherits from 'BatchBoundedSetting' and provides the specific information
//  needed for representation of apnea plateau time.  This information
//  includes the interval and range of this setting's values, this setting's
//  response to a Main Control Setting's transition (see
//  'acceptTransition()'), and this batch setting's new-patient value (see
//  'getNewPatientValue()').
//
//  This setting is a constant setting, and, as such, never has an
//  applicability of CHANGEABLE.  The 'calcNewValue()' method defined
//  by one of the base classes ensures that non-CHANGEABLE settings
//  are never changed.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings or dynamic constraints.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaPlateauTimeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 004   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed now-unneeded 'calcNewValue()'
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaPlateauTimeSetting.hh"
#include "MandTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02088] -- The setting's range ...
static const BoundedInterval  LAST_NODE_ =
  {
    0.0f,		// absolute minimum...
    0.0f,		// unused...
    HUNDREDS,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    0.0f,		// absolute maximum...
    0.0f,		// resolution...
    HUNDREDS,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaPlateauTimeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the value interval
//  is initialized.
//
//  Since this setting's value remains constant, the constraint IDs are set
//  to 'NULL_SETTING_BOUND_ID'.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaPlateauTimeSetting::ApneaPlateauTimeSetting(void)
	: BatchBoundedSetting(SettingId::APNEA_PLATEAU_TIME,
			      Setting::NULL_DEPENDENT_ARRAY_,
			      &::INTERVAL_LIST_,
			      NULL_SETTING_BOUND_ID,	// maxConstraintId...
			      NULL_SETTING_BOUND_ID)	// minConstraintId...
{
  CALL_TRACE("ApneaPlateauTimeSetting()");
	isATimingSetting_ = TRUE;
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaPlateauTimeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaPlateauTimeSetting::~ApneaPlateauTimeSetting(void)
{
  CALL_TRACE("~ApneaPlateauTimeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaPlateauTimeSetting::getApplicability(
			      const Notification::ChangeQualifier qualifierId
				     ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pApneaMandType =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

  DiscreteValue  apneaMandTypeValue;

  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    apneaMandTypeValue = pApneaMandType->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    apneaMandTypeValue = pApneaMandType->getAdjustedValue();
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }

  return((apneaMandTypeValue == MandTypeValue::VCV_MAND_TYPE)
	   ? Applicability::VIEWABLE		// $[TI3]
	   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaPlateauTimeSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;
  
  // $[02089] The setting's new-patient value ...
  newPatient.value     = 0.0f;
  newPatient.precision = HUNDREDS;

  return(newPatient);   // $[TI1]
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  This setting is a constant setting, therefore this method does nothing,
//  but is provided so that the Main Control Setting that its Transition
//  Rule is based on (i.e., apnea mandatory type) doesn't have to "know"
//  that this a constant setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId == SettingId::APNEA_MAND_TYPE    &&
//   newValue  == MandTypeValue::VCV_MAND_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//   None
//@ End-Method
//=====================================================================

void
ApneaPlateauTimeSetting::acceptTransition(
				   const SettingId::SettingIdType settingId,
				   const DiscreteValue            newValue,
				   const DiscreteValue            currValue
					 )
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  // apnea plateau time's only transition is due to an apnea mandatory type
  // change from 'VCV' to 'PCV'...
  SAFE_AUX_CLASS_PRE_CONDITION((settingId == SettingId::APNEA_MAND_TYPE),
			       settingId);
  SAFE_AUX_CLASS_PRE_CONDITION((newValue  == MandTypeValue::VCV_MAND_TYPE),
			       newValue);

  // do nothing...
}   // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaPlateauTimeSetting::SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName,
				   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_PLATEAU_TIME_SETTING, lineNumber, pFileName,
			  pPredicate);
}
