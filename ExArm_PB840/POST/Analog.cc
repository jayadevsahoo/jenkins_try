#include "stdafx.h"
#ifdef SIGMA_BD_CPU      // compiled only for BD CPU

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================



//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Analog.cc - Power on self test Analog Interface tests 
//
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides tests and support functions for the Analog 
//  Interface specific to the Breath Delivery CPU.
//---------------------------------------------------------------------
//@ Implementation-Description
//  >Von
//
//  Analog Interface System
//
//  The analog to digital conversion system consists primarily of a
//  small amount of dual ported RAM, a  RAM controller to arbitrate
//  simultaneous access, a sequencer that continuously samples all ADC
//  channels, and a successive approximation ADC.   The sequencer
//  controls a MUX that selects each of 32 channels, beginning at
//  channel 0.  As each channel is selected a sample and hold circuit
//  latches  the sampled signal, where the ADC converts it to a digital
//  value.  When conversion is complete the ADC stores the sampled
//  value into RAM.   This test performs an initial sampling of the
//  analog devices, checking for basic conversion capability and data
//  validity.
//
//
//  Serial Devices
//
//  The Analog Interface PCB contains 5 serial device interfaces, 4
//  EEPROMS and 1 timer.  The serial interface of this board provides
//  control and buffering of the data into and out of these serial
//  devices.
//
//  Each of the flow sensor serial device contains a flow coeffecient
//  table.  The compressor module contains 2 serial devices.  A serial
//  EEPROM contains the running  total  of  elapsed time, and other
//  service information. An elapsed timer is the second serial device.
//
//  The test for the serial devices only performs an access to the
//  serial device register.
//
//  Analog Devices
//
//  Once the ADC acquisition system has been verified it can be used to
//  test the individual analog devices of the system.  The ADC
//  sequencer reads 32 channels of analog data in each cycle.  The
//  analog device test during post verifies that the readings of  each
//  connected sensor is reasonable during the power on state (safe
//  state) of the ventilator.   A single reading is taken for each
//  channel.
//
//  The value of each channel is compared to the acceptable limits in
//  the table.  All channels are tested, samples outside these limits
//  will fail this test.
//
//  >Voff
//---------------------------------------------------------------------
//@ Fault Handling
//    Faults during POST cause the test in progress to fail.
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/Analog.ccv   25.0.4.0   19 Nov 2013 14:17:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008    By: erm  Date: 15-May-2003  DR Number: 6045
//    Project: AVER
//    Description:
//       Widen Limits on: 12 & 5 volt Gui Sentry, O2 Sensor, Battery Current and Voltage
//       Change DeviceTest to average reading before testing for failure.
//
//  Revision: 007    By: Gary Cederquist  Date: 10-AUG-1999  DR Number: 5502
//    Project: 806 Compressor
//    Description:
//       Removed AC Voltage Test which was redundant to background test.
//
//  Revision: 006    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 005    By: Gary Cederquist  Date: 10-JUN-1997  DR Number: 1902
//    Project: Sigma (R8027)
//    Description:
//       Returning PASSED if NOVRAM is not initialized during AC 
//       voltage test.
//
//  Revision: 004    By: Gary Cederquist  Date: 06-JUN-1997  DR Number: 2183
//    Project: Sigma (R8027)
//    Description:
//       Added logging of sense information for the Analog Device Test.
//
//  Revision: 003    By: Gary Cederquist  Date: 14-APR-1997  DR Number: 1936
//    Project: Sigma (R8027)
//    Description:
//       Corrected ordering of submux channels in PostAnalogDeviceTestTable.
//
//  Revision: 002    By: Gary Cederquist  Date: 08-APR-1997  DR Number: 1907
//    Project: Sigma (R8027)
//    Description:
//       Wait for new ADC samples after selecting SUBMUX channel.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Analog.hh"
#include "NominalLineVoltValue.hh"
#include "Dram.hh"
#include "PostNovram.hh"
#include "BdIoUtil.hh"
#include "Post_Library.hh"

#if defined(SIGMA_DEBUG) || defined(SIGMA_SP_UNIT_TEST)
#include <stdio.h>
#endif //defined(SIGMA_DEBUG) || defined(SIGMA_SP_UNIT_TEST)

const Uint32 GENERAL_RAM_TEST_PASSED = 0xffffffff;
const Uint32 NUM_DAC_WRAP_CYCLES = 5;
const Uint32 NUM_CHANNEL_READ_CYCLES = 5;
const Uint32 POST_AI_SAMPLE_CYCLES = 20;


const Int32 NUM_SUBMUX = 4;
const Int32 NUM_ANALOG_DEVICES = PostAdcChannels::NUM_ADC_CHANNELS + NUM_SUBMUX;

/**************************************************************************

   PostAnalogDeviceTestTable

   This table contains the upper and lower limits used during the
   BD Post Analog Device test.  This table assumes the devices are
   operating during SAFE STATE and that the low voltage reference 
   has been subtracted.

   Reference:  muxlim1.mcd    7/07/95 Ron Gross
               adc_errs.mcd   7/21/95 Danis Carter

Channel  Description                Low     High       Low     High  Criticality
Number                              (volts) (volts)    counts  counts (see FEMA)
*******************************************************************************/
// $[11051] $[11052] $[11055] $[11089] $[11056] $[11090]

const TestTableStruct PostAnalogDeviceTestTable[NUM_ANALOG_DEVICES] =
{
/* 0   Insp pressure filtered       0.760V   8.312V  */ {  311, 3405, MAJOR },
/* 1   Exh pressure filtered        0.760    8.312   */ {  311, 3405, MAJOR },
/* 2   Q(O2)  filtered              0.284    1.676   */ {  116,  687, MINOR },
/* 3   Q(O2)  temperature           1.087    8.175   */ {  445, 3349, MINOR },
/* 4   Q(air) filtered              0.284    1.676   */ {  116,  687, MINOR },
/* 5   Q(air) temperature           1.087    8.175   */ {  445, 3349, MINOR },
/* 6   Q(exh) filtered              0.284    8.562   */ {  116, 3508, MAJOR },
/* 7   Q(exh) temperature           1.087    8.175   */ {  445, 3349, MINOR },
/* 8   SUBMUX 0: DACWRAP           -0.001   10.001   */ {    0, 4095, MAJOR },
/* 9   Safety Valve Switched side  -0.001    0.023   */ {    0,   10, MAJOR },
/* 10  12V GUI Sentry              -0.001   10.001   */ {    0, 4095, MINOR },
/* 11  Alarm Cable voltage          0.594    5.250   */ {  243, 2151, MINOR },
/* 12  O2  PSOL current            -0.001    0.024   */ {    0,   11, MAJOR },
/* 13  Air PSOL current            -0.001    0.024   */ {    0,   11, MAJOR },
/* 14  Low Voltage Reference        0.027    0.070   */ {   11,   29, MAJOR },
/* 15  Atmospheric Sensor           2.771    4.858   */ { 1135, 1990, MINOR },
/* 16  Exhalation Coil Temperature  0.130    8.095   */ {   53, 3317, MINOR },
/* 17  Exh Pressure Filtered        0.284    8.562   */ {  116, 3508, MAJOR }, 
/* 18  O2 Sensor                   -0.001   10.001   */ {    0, 4095, MINOR },
/* 19  GUI 5VDC Sentry             -0.001   10.001   */ {    0, 4095, MINOR },
/* 20  12VDC Sentry                 6.997    7.990   */ { 2861, 3274, MAJOR },
/* 21  Safety Valve Current        -0.001    0.209   */ {    0,   86, MAJOR },
/* 22  +15V Sentry                  7.245    7.743   */ { 2967, 3172, MAJOR },
/* 23  -15V Sentry                  7.245    7.743   */ { 2967, 3172, MAJOR },
/* 24  Powerfail Cap voltage       -0.001    5.155   */ {    0, 2112, MINOR },
/* 25  Exh Manifold Heater Temp     0.771    8.096   */ {  315, 3317, MINOR },
/* 26 #BPS Battery Voltage Signal  -0.001   10.001   */ {    0, 4095, MINOR },
/* 27  5VDC Venthead                4.668    5.340   */ { 1911, 2188, MAJOR },
/* 28 #BPS Battery Current         -0.001   10.001   */ {    0, 4095, MINOR },
/* 29  AC Line Voltage signal      -0.010   10.008   */ {    0, 4095, MAJOR },
/* 30  Exh Motor current           -0.001    0.029   */ {    0,   12, MAJOR },
/* 31  10V Sentry                   8.895    9.287   */ { 3643, 3805, MAJOR },

/* SUBMUX */
/* 0   DACWRAP                     -0.001   10.001   */ {    0, 4095, MAJOR},
/* 1   AI Revision Signal           0.194   10.001   */ {   79, 4095, MINOR}, 
/* 2  #BPS Model Signal             0.472   10.001   */ {  193, 4095, MINOR},
/* 3   Spare                        0.000   10.001   */ {    0, 4095, MINOR}
};

// #NOTE: POST assumes the BPS may or may not be present and uses the worst
//        case range of voltage values for these BPS channels.
//        The voltage ranges in the comment are for a ventilator with a BPS.
//        The ADC counts in the table are for a ventilator with or without 
//        a BPS.

////////////////////////////////////////////////////////////////////////////////
//
//  The following test limits are derived from the gain & offset limits
//  of the ADC and DAC systems.  These limits are compounded for a true
//  worst case.
//
//  ADC gain error = +-0.57%  
//  Offset = +-19.5mV before offset correction, 10mv after
//
//  DAC gain error = +-1.0%   Offset = 10mv
//  (Note: Spec of DAC indicates 5% but analysis shows .622%, 
//   thus 1.0 was recommended)
//
//  Max V = Vsig * (1+ADCgain) * (1+DACgain) + (Offset(ADC) + Offset(DAC))
//  Min V = Vsig * (1-ADCgain) * (1-DACgain) - (Offset(ADC) - Offset(DAC))
//
//  Max V = Vsig * 1.0160 + (19.5 + 10)mV   (without offset correction)
//  Min V = Vsig * 0.9843 - 29.5mV
//
//  Where Vsig = Signal applied to DAC during the test
//
//  Voltage is converted to counts using 10V=4095 counts
//  Negative voltages=0, voltages above 10V=4095 counts
//
//  Ron Gross 8/25/95
//
//  Note: 0.5V & 9.5V are chosen so that min/max limits are not outside 0-4095.
////////////////////////////////////////////////////////////////////////////////
// Constructing table Test3Table using test points for case 3 for DACTest 
//  0000000xxxxx            01FH
//  000000x00000            020 
//  00000x000000            040 
//  0000x0000000            080 
//  000x00000000            100 
//  00x000000000            200 
//  0x0000000000            400 
//  x00000000000            800 
//  xxxxxxxxxxxx            FFF 
////////////////////////////////////////////////////////////////////////////////
const Int32 NUM_TEST_DAC3 = 9;
const TestDacStruct Analog::TestDac3ATable_[] =
{
//test point  min count   max count  criticality
   {0x1F,          1,          60,      MAJOR},
   {0xb020,        1,          62,      MAJOR},
   {0xA040,       33,          94,      MAJOR},
   {0x9080,       96,         159,      MAJOR},
   {0x7100,      222,         289,      MAJOR},
   {0x6200,      474,         549,      MAJOR},
   {0x5400,      978,        1069,      MAJOR},
   {0x3800,     1986,        2110,      MAJOR},
   {0xFFFF,     4001,        4190,      MAJOR},
};

const TestDacStruct Analog::TestDac3BTable_[] =
{
//test point  min count   max count  criticality
   {0xFFFF,     4001,        4190,      MAJOR},
   {0x3800,     1986,        2110,      MAJOR},
   {0x5400,      978,        1069,      MAJOR},
   {0x6200,      474,         549,      MAJOR},
   {0x7100,      222,         289,      MAJOR},
   {0x9080,       96,         159,      MAJOR},
   {0xA040,       33,          94,      MAJOR},
   {0xb020,        1,          62,      MAJOR},
   {0x1F,          1,          60,      MAJOR},
};

////////////////////////////////////////////////////////////////////////////////
// Constructing table Test4Table using test points for case 4 for DACTest 
//  Bits tested             DAC values used
//  =======================================
//  0000000xxxxx            01FH
//  000000xxxxxx            03F
//  00000xxxxxxx            07F
//  0000xxxxxxxx            0FF
//  000xxxxxxxxx            1FF
//  00xxxxxxxxxx            3FF
//  0xxxxxxxxxxx            7FF
//  xxxxxxxxxxxx            FFF
////////////////////////////////////////////////////////////////////////////////
const Int32 NUM_TEST_DAC4 = 8;
const TestDacStruct Analog::TestDac4ATable_[] =
{
//test point  min count   max count 
   {0x1F,          1,         60,      MAJOR},
   {0xB03F,       32,         93,      MAJOR},
   {0x107F,       95,        158,      MAJOR},
   {0x80FF,      221,        288,      MAJOR},
   {0xF1FF,      473,        548,      MAJOR},
   {0x93FF,      977,       1068,      MAJOR},
   {0xC7FF,     1985,       2109,      MAJOR},
   {0xFFFF,     4001,       4190,      MAJOR},
};

const TestDacStruct Analog::TestDac4BTable_[] =
{
//test point  min count   max count 
   {0xFFFF,     4001,       4190,      MAJOR},
   {0xC7FF,     1985,       2109,      MAJOR},
   {0x93FF,      977,       1068,      MAJOR},
   {0xF1FF,      473,        548,      MAJOR},
   {0x80FF,      221,        288,      MAJOR},
   {0x107F,       95,        158,      MAJOR},
   {0xB03F,       32,         93,      MAJOR},
   {0x1F,          1,         60,      MAJOR},
};

//=====================================================================
//
//  Public Methods
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: DelayForDacWrap
//
//@ Interface-Description
//  Introduces a delay required by the DAC WRAP port to echo the 
//  data back to the CPU memory mapped interface.
//
//  The duration for this delay loop has been determined empirically
//  at 150-200 loop iterations.  To allow for uncertainties and 
//  future increases in CPU clock speed, this count is 5X minimum.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
#define DAC_WRAP_DELAY_COUNT  (1000)

inline void
DelayForDacWrap()
{
    for (Int32 jj=0; jj<DAC_WRAP_DELAY_COUNT; jj++)
    {
        // delay
    }
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestAnalogInterfacePCB
//
//@ Interface-Description
//  Tests the existance of the analog interface PCB to the 
//  industry pack interface bus.
//
//  This sends a reset and resynch commands to the Analog Interface
//  board.  If no bus timeouts occur the IP interface, DAC controller,
//  and decoding GAL are assumed to be operational.  A bus timeout
//  will cause the exception handler to declare a MAJOR failure for
//  the current test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Criticality
Analog::TestAnalogInterfacePCB  (void)
{
   //  create a local pointer so emulator can modify for unit test
   volatile Uint16 * volatile pAdcSoftResetPort = ADC_SOFT_RESET_PORT;

UNIT_TEST_POINT(POSTUT_28)
   // you can write any data value
   *pAdcSoftResetPort = ADC_SOFT_RESET_CMD;

   // reset automatically turns the alarm on.
   *WRITE_IO_PORT = BD_AUDIO_ALARM_OFF;

   // $[TI1]
   return PASSED;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestADC
//
//@ Interface-Description
//  >Von
//  Test the integrity of the analog to digital converter and FPGA
//  sequencer on the analog interface PCB.
//
//  TECHNIQUE
//
//  Perform a RAM test of the ADC ram.  Initialize the FPGA 
//  sequencer.  Read ADC h/w status while cycling the sequencer for
//  at least five complete 32 channels cycles.
//  
//   
//  Step 1:  Test ADC RAM.   A pattern & address test is used.  The
//  test fails if the RAM test fails.  $[11050]
//
//  Step 2:   Initialize the ADC by sending the soft reset.
//  
//  Step 3:   Test the FPGA sequencer is operational and counting.
//  This is done by allowing all channels to be sampled by the
//  sequencer for a number of cycles. The sequencer increments the ADC
//  counter after all the channels have been sampled. The ADC counter
//  is monitored for at least 5 complete cycles.  $[11094]
//
//  What are tested:
//
//  ADC RAM and controller, Sequencer, ADC circuitry, MUX, sample and
//  hold circuit, and the basic conversion capability of the ADC and
//  the sample cycling of the sequencing system.  Individual device
//  voltage limits are checked in the next steps.  The resync function
//  of the ADC is tested.
//
//
//  Pass criteria:    ADC RAM test passed passed, ADC counter resumes 
//                    when RESYNC is issued.  
//
//  Failure criteria: ADC RAM test failed, ADC counter does not resume 
//                    when resynch
//                    command is issued, Sequencer status fails.
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  >Von
//
//  FPGA sequencer status (a hardware conversion status register) is
//  latched high when a bad channel is sampled, and remains latched if
//  other good channels are converted.  This bad conversion status is
//  mapped to an interrupt.  The signal labeled ERROR* in the AI PCBA
//  requirements monitors ADC operations as stated and generates an
//  interrupt through the bus interface when it fails.
//
//  This interrupt is mapped to an NMI on the BD CPU.  During POST 
//  this unexpected interrupt will fail POST.  When the sequencer is 
//  operational a cycle counter is incremented after each channel has
//  been sampled.  The freeze function stops the ADC counter, the 
//  resync command initializes the sequencer.
//  >Voff
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Criticality
Analog::TestADC (void)
{
   Uint16 adcChannels[NUM_ANALOG_DEVICES]; 

UNIT_TEST_POINT(POSTUT_29)

   *ADC_FREEZE_PORT = ADC_FREEZE_CMD;

   // Test 1
   Uint32 ramStatus = Analog::TestRAM_( (Uint16*) ADC_READ_PORT, 
                                        PostAdcChannels::NUM_ADC_CHANNELS);

UNIT_TEST_POINT(POSTUT_29_1)
   if (ramStatus != GENERAL_RAM_TEST_PASSED)
   {
      // $[TI1.1]
      return MAJOR;
   }
   // $[TI1.2]

   // Test 2
   // initialize adc
   *ADC_RESYNCH_PORT = ADC_RESYNCH_CMD;

   // Test 3
   Boolean failed = FALSE;

   for (Int32 ii = NUM_CHANNEL_READ_CYCLES ; ii>0 && !failed; ii--) 
   {
      // ReadAnalogChannel guarantees change in the monitor counter
      // before all the channels are read. 
      failed = ReadAnalogChannels_(adcChannels);
   }

   if (failed)
   {
      // $[TI2.1]
      return MAJOR;
   }
   // $[TI2.2]

   return PASSED;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestDAC
//
//@ Interface-Description
//  >Von
//  Tests the digital to analog converter of the analog interface PCB.
//
//  This test uses the DACWRAP function of the AI system to apply
//  a loopback voltage from the DAC back to the ADC.  The 10V reference
//  is checked, the sequencer is commanded to repeat acquisition to
//  the DACWRAP port, and a ramp (triangle wave) is applied to the DAC.  
//  Finally, a nominal test voltage is applied to test stability.
//
//
//  What is tested 
//
//  The analog interface PCB contains six DAC channels.  Only one channel 
//  (DACWRAP) is tested.  The other 5 channels are not tested since the 
//  safe state disables power to the DAC channels that control the
//  pneumatic devices.
//
//  The basic ability to command the DAC to apply a voltage and the
//  ability to read the voltage back is tested.  The individual voltage
//  steps are tested for monotonicity, i.e.-voltage can be ramped up in
//  a continuous positive manner.
//
//  Basic conversion, timing and accuracy of DAC system are tested.
//  
//  The MUX, sample and hold circuit and general ADC/DAC system are
//  implicitly tested.
//
//
//  TEST METHOD
//  
//  Step 1:  Before the DAC can be used its reference voltage must be
//  tested.  This reference voltage, the 10V sentry, is a 9.09VDC
//  ratiometric signal used for the pressure sensors. It is
//  proportional to the 10V reference.  Verify the reference voltage is
//  within limits and FPGA status is valid.
//
//  Step 2:  Command the sequencer to REPEAT mode. REPEAT mode will
//  cause the ADC to continuously sample the DACWRAP channel.The REPEAT
//  command stops the sequencer from counting.
//
//  Step 3: This step verifies the DAC-ADC is monotonic.  A ramping
//  test pattern from 0V - 10V - 0V is applied over the range of this
//  12 bit DAC.  The voltage value applied to the DAC is read back from
//  the ADC and compared to a test margin.  Selected limits around the
//  transition points of the DAC circuit are used.
//
//  The following table indicates the values used.  The test applies
//  the values in a positive ramp, then repeats the test from the
//  maximum value back down to zero.  Note:  4095 counts = 10V.
//
//  
//  Bits tested        DAC values used      Tests DAC around    Equivalent Volts
//  ============================================================================
//  0000000xxxxx       01FH                 0-16 counts          0-49  mv
//  000000x00000       020                  32                   61-85 mv
//  00000x000000       040                  64                   147-171 mv
//  0000x0000000       080                  128                  293-342 mv
//  000x00000000       100                  256                  586-635 mv
//  00x000000000       200                  512                  1.22 - 1.27 V
//  0x0000000000       400                  1024                 2.44 - 2.54 v
//  x00000000000       800                  2048                 4.93 - 5.03 v
//  xxxxxxxxxxxx       FFF                  4096                 9.89 - 10.00 v
//  Reapeat the same seqeunce in reverse
//
//  Step 4: This step is a variation of step 3, except a monotonic
//  check is made.  Continuous increasing values in the positive ramp
//  and decreasing values in the negative ramp is tested by comparing
//  each value against the previous point.
//
//  Bits tested             DAC values used
//  =======================================
//  0000000xxxxx            01FH
//  000000xxxxxx            03F
//  00000xxxxxxx            07F
//  0000xxxxxxxx            0FF
//  000xxxxxxxxx            1FF
//  00xxxxxxxxxx            3FF
//  0xxxxxxxxxxx            7FF
//  xxxxxxxxxxxx            FFF
//  Repeat the same sequence in reverse
//
//  $[11053]
//
//  Step 5: Test the ADC/DAC DACWRAP system for stability by
//  continuously applying a test voltage at 50% of maximum, and monitor
//  the sample for 5 cycles.
//
//  Pass criteria: The test value must be measured to within design
//  limits of the value applied to the DAC.  Failure criteria:
//  Measurement is outside of range, or FPGA status is not valid.
//
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  Assumptions A set of ramped voltages when output to the DAC produce
//  a monotonic response when read back from the ADC.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Criticality
Analog::TestDAC (void)
{
UNIT_TEST_POINT(POSTUT_30)

   //intialize ADC
   *ADC_RESYNCH_PORT = ADC_RESYNCH_CMD;

   WaitAdcNewSample();

   // Test 1 - 10VDC sentry tested in Analog::TestAnalogDevices

   // Test 2
   *WRITE_IO_PORT = SELECT_SUBMUX1_CMD | BD_AUDIO_ALARM_OFF;
   *ADC_REPEAT_PORT = ADC_REPEAT_CMD;

   // Test 3, 4

   if ( TestDacPoints_(TestDac3ATable_, NUM_TEST_DAC3) == MAJOR )
   {
       // $[TI1.1]
       return MAJOR;
   }
   else if ( TestDacPoints_(TestDac3BTable_, NUM_TEST_DAC3) == MAJOR )
   {
       // $[TI1.2]
       return MAJOR;
   }
   else if ( TestDacPoints_(TestDac4ATable_, NUM_TEST_DAC4) == MAJOR )
   {
       // $[TI1.3]
       return MAJOR;
   }
   else if ( TestDacPoints_(TestDac4BTable_, NUM_TEST_DAC4) == MAJOR )
   {
       // $[TI1.4]
       return MAJOR;
   }
   // $[TI1.5]

   // Test 5
   for (Uint32 ii = NUM_DAC_WRAP_CYCLES; ii > 0; ii--)
   {
      if ( TestDacPoints_(&TestDac4ATable_[6], 1) == MAJOR )
      {
          // $[TI2.1]
          return MAJOR;
      }
      // $[TI2.2]
   }

   return PASSED;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestAnalogDevices
//
//@ Interface-Description
//  >Von
//     Verify the accuracy of all ADC channels during safe state.  
//     The PostAnalogDeviceTestTable structure determines the limits for 
//     each channel. This test is called after method TestADC.
//
//  This test reads each of the NUM_ANALOG_DEVICES channels a number of
//  times (5) and compares the average of the reading to known limits 
//  based on safe state values.
//  
//
//  Note: Some channels will have no limits, the readings are valid in
//  in the entire 0-10V range.  But since all NUM_ANALOG_DEVICES
//  channels are sampled with each reading, a comparison to a table
//  takes (minimal) additional time.
//
//  For this test to pass, the system must be in the safe state.  This
//  test is placed after the TestAnalogInterfacePCB test so that test
//  can reset the Analog Interface and allow time for voltages to
//  stabilize before testing them here.
//  
//  >Voff
// $[11051] $[11052] $[11055] $[11089] $[11056] $[11090]
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Criticality
Analog::TestAnalogDevices (void)
{
   Uint16 channels[NUM_ANALOG_DEVICES];
   Uint16 channelsAvg[NUM_ANALOG_DEVICES];
   Uint32 loops = 0;
   static Uint32 const MAX_LOOPS = 10;
   Criticality status = PASSED;
   
  
	for (loops = 0; loops < MAX_LOOPS; loops++)
	{
		status = PASSED;
		for (Int32 clearIndex = 0; clearIndex < NUM_ANALOG_DEVICES; clearIndex++)
		{
			channelsAvg[clearIndex] = 0u;
		}

		*ADC_RESYNCH_PORT = ADC_RESYNCH_CMD;

		for (Int32 ii = 0; ii < NUM_CHANNEL_READ_CYCLES; ii++)
		{
			ReadAnalogChannels_(channels);

			for (Int32 jj = 0; jj < NUM_ANALOG_DEVICES; jj++)
			{
				channelsAvg[jj] += channels[jj];
			}

			for (Int32 sampleIndex = 0; sampleIndex < POST_AI_SAMPLE_CYCLES; sampleIndex++)
			{
				WaitAdcNewSample();
			}    
		}

		for (Int32 avgIndex = 0; avgIndex < NUM_ANALOG_DEVICES; avgIndex++)
		{
			Uint16 avg = (Uint16)(channelsAvg[avgIndex] / NUM_CHANNEL_READ_CYCLES);
			if (OutOfRange( avg , PostAnalogDeviceTestTable[avgIndex]))
			{
				status = PostAnalogDeviceTestTable[avgIndex].criticality;
				SetSense( (avgIndex << 16) | avg );

				// show easy readable bad channel in errorCode 
				SetErrorCode( (Uint8) avgIndex ); 

				// return error code if were at end of the max test iteration
				if ((status == MAJOR) && (loops >= MAX_LOOPS-1))
				{
					return status;
				}
			}
		}

		if (status == PASSED)
		{
			break;
		}

      }
      
  
   return status;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestSerialDevice
//
//@ Interface-Description
//  >Von
//  Test the serial devices by accessing and writing to their io 
//  register.  An access fault at this point will cause a MAJOR failure.
//  >Voff
//  $[11054]
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Criticality
Analog::TestSerialDevice (void)
{
UNIT_TEST_POINT(POSTUT_31)
   *SERIAL_RW_EEPROMS = POST_SERIAL_RW_IMAGE;
   *SERIAL_RW_EEPROMS;

   // $[TI1]
   return PASSED;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OutOfRange
//
//@ Interface-Description
//  >Von
//  Determine if the argument val is within the limits defined by
//  TestTableStruct.  If the argument val is out of range returns TRUE
//  else returns FALSE.
//  >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
Analog::OutOfRange (Uint16 val, TestTableStruct range)
{ 
   // $[TI1.1]  $[TI1.2]
   return ( range.lowLimit > val || val > range.highLimit );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReadAdc
//
//@ Interface-Description
//  Returns the specified ADC channel value compensated to the low voltage 
//  reference.  Note that this method does not read the submux channel.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Uint16
Analog::ReadAdc(PostAdcChannels::AdcChannelId adcId)
{
    const Int16 NOM_REF_COUNTS = 20;
    const Int16 MAX_COUNT = 4095;

    static Int16 offset = NOM_REF_COUNTS 
          - (  ReadWord(ADC_READ_PORT + PostAdcChannels::LOW_VOLTAGE_REFERENCE)
             & ADC_MASK);
 
    Int16 value = (Int16)ReadWord(ADC_READ_PORT + adcId ) & ADC_MASK;

    value += offset;

    if (value < 0)
    {
        // $[TI1.1]
        value = 0;
    }
    else if (value > MAX_COUNT)
    {
        // $[TI1.2]
        value = MAX_COUNT;
    }
    // $[TI1.3]

    return value;
}

//=====================================================================
//
//  protected Methods
//
//=====================================================================

//=====================================================================
//
//  private Methods
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestDacPoints_
//
//@ Interface-Description
//  Iterates through a table of test points writing them to the 
//  DAC wrap write port.  After waiting for the DAC wrap delay, it reads
//  the feedback signal from the ADC read port.  If all channels feedback 
//  within range, the test returns PASSED otherwise it returns a MAJOR
//  failure.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Criticality
Analog::TestDacPoints_(const TestDacStruct * pTestTable, const Int32 maxIndex)
{
	Uint32 loops = 0;
	static Uint32 const MAX_LOOPS = 10;
	Criticality status = PASSED;

	for (loops = 0; loops < MAX_LOOPS; loops++)
	{
		status = PASSED;
		for (Int32 ii = 0; ii < maxIndex; ii++)
		{
			*DACWRAP_WRITE_PORT = pTestTable[ii].testDacPoint;

			DelayForDacWrap();

			Uint16 dacFeedback = 
			ReadWord(ADC_READ_PORT + PostAdcChannels::SUBMUX_SENSORS) & ADC_MASK;

			UNIT_TEST_POINT(POSTUT_30_2)
			if (OutOfRange(dacFeedback, pTestTable[ii].checkPoint))
			{
				SetSense( (ii << 16) | dacFeedback );
				status = MAJOR;
				if (loops >= MAX_LOOPS-1)
				{	
					return status;
				}
			}
		}

		// exit loop if status is okay
		if (status == PASSED)
		{
			break;
		}
	}

   return status;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReadAnalogChannels_
//
//@ Interface-Description
//  Reads all channels of the mux and submux from the AI sequencer 
//  circuit into a specified array.  The array must accomodate 
//  NUM_ANALOG_DEVICES entries plus four submux entries.  The method
//  returns FALSE if there was no problem reading the channels.  It
//  returns TRUE if there was an error indicating a timeout on the
//  seqeuncer.  All values returned in the array are compensated to 
//  the low voltage reference.  This test returns a failure if the
//  sequencer is not running.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Boolean
Analog::ReadAnalogChannels_(Uint16 *pChannelBuffer)
{
UNIT_TEST_POINT(POSTUT_29_2)
    Boolean   failed = WaitAdcNewSample() || !pChannelBuffer;

    if ( !failed )
    {
		Uint ii;
       // $[TI1.1]
       // read the first 32 channels without submux
       for ( ii = 0 ; ii < PostAdcChannels::NUM_ADC_CHANNELS ; ii++ )
       {
           pChannelBuffer[ii] 
               = Analog::ReadAdc(PostAdcChannels::AdcChannelId(ii));
       }

       // to read the 4 submux channels
       failed = ReadSubmux_( SELECT_SUBMUX1_CMD
                            ,pChannelBuffer[ii++] );

       failed = failed || ReadSubmux_( SELECT_SUBMUX2_CMD
                                      ,pChannelBuffer[ii++] );

       failed = failed || ReadSubmux_( SELECT_SUBMUX3_CMD
                                      ,pChannelBuffer[ii++] );

       failed = failed || ReadSubmux_( SELECT_SUBMUX4_CMD
                                      ,pChannelBuffer[ii++] );
   }
   // $[TI1.2]

   return failed;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReadSubmux_
//
//@ Interface-Description
//  Select the submux as indicated by the argument selectSubmux and 
//  wait for 2 msec before reading the value to ensure a fresh value is
//  being read. The value read is place in argument pSubmuxRead. 
//  If the read is successful this method returns FALSE else returns TRUE. 
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
Analog::ReadSubmux_( Uint16 selectSubmux, Uint16 & rSubmuxValue )
{ 
    *WRITE_IO_PORT = selectSubmux | BD_AUDIO_ALARM_OFF;

    //  wait for sequencer to cycle in new A/D samples
    Boolean failed = WaitAdcNewSample();

UNIT_TEST_POINT(POSTUT_29_3)
    if ( !failed )
    {                   
       // $[TI1.1]
       rSubmuxValue = ReadAdc( PostAdcChannels::SUBMUX_SENSORS );
    }
	// $[TI1.2]

    return failed;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaitAdcNewSample -
//
//@ Interface-Description
//    This method takes no argument and returns Boolean type.
//    If the FPGA is sequencing then returns FALSE.  Returns TRUE
//    if the FPGA fails to sequence.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Wait for the monitor count to change. If the monitor count has not
//    change within FPGA_TIME_OUT cycles then returns TRUE else FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//    None
//---------------------------------------------------------------------
//@ PostCondition
//    None
//
//@ End-Method
//=====================================================================
static const Uint32 FPGA_TIME_OUT = 50000;
static const Uint32 NUM_NEW_SAMPLES = 2;
Boolean
Analog::WaitAdcNewSample(void)
{
    Uint16 ii = 0;
    Uint16 kk = 0;
 
    for ( Uint32 i = NUM_NEW_SAMPLES; i > 0; i--)
    {
        ii = *ADC_MONITOR_COUNT_PORT & 0x000F;
        kk = *ADC_MONITOR_COUNT_PORT & 0x000F;
 
        for ( Uint32 jj = FPGA_TIME_OUT ; ii == kk && jj > 0 ; jj-- )
        {
            kk = *ADC_MONITOR_COUNT_PORT & 0x000f;
        }
 
        //  if we didn't get a new sample then quit trying
UNIT_TEST_POINT(POSTUT_29_4)
        if ( ii == kk )
        {
            // $[TI1.1]
            break;
        }
        // $[TI1.2]
    }
 
UNIT_TEST_POINT(POSTUT_29_5)
    // $[TI2.1]  $[TI2.2]
    return ( ii == kk );
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestRAM
//
//@ Interface-Description
// >Von
//  This method tests the Data Acquisition RAM on the Data Acquisition 
//  board.  It performs a data pattern and address pattern test for each
//  channel as specified by the calling routine.  
//
//  This method essentially performs a RAM data and address pattern test.  
//  It writes and reads back a set of patterns.  The address test is 
//  writes and compares unique values to the different memory locations.
//  
//  The 16 bit offset from the starting address is returned if the memory
//  test fails.  This offset is returned as 32 bit value so the passed 
//  condition can be distinguished uniquely.
//
//  INPUTS:  starting address
//           length 0 - 0xFFFF,  a length of 0 will return the passed condition
//
//  OUTPUTS: GENERAL_RAM_TEST_PASSED (if successfull)
//           offset to bad memory (if failed)
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Uint32 
Analog::TestRAM_(Uint16 * const startAddress,const Uint16 length)
{
   static const Uint16 RAM_TEST_PATTERNS[]
       = {0x0000,0xffff,0x5555,0xaaaa,0xcccc,0x3333};

   Boolean failed = FALSE;    
   volatile Uint16 * temp;    // temporary pointer to memory under test
   Uint16 position = 0;           // position of bad memory if any
   Uint16 testPattern;        // pattern under test

   // Test that memory can hold 1's & 0's, by writing 
   // a test pattern and its complement pattern
   for (Uint16 i=0; i < countof(RAM_TEST_PATTERNS) && !failed; i++)
   {
      temp = startAddress;
      testPattern = RAM_TEST_PATTERNS[i];

      for (position = 0; position < length && !failed; position++)
      {
         *(temp) = testPattern;
UNIT_TEST_POINT(POSTUT_29_7)
         if (*temp++ != testPattern)
         {
            // $[TI1.1]
            failed = TRUE;
         }
         // $[TI1.2]
      }
   }


   // Address/Amnesia test.  Write & test a unique pattern to memory.
   if ( !failed )
   {
      // $[TI2.1]
      temp = startAddress;
      for (position=0; position < length; position++)
      {
         *(temp++) = position;
      }

UNIT_TEST_POINT(POSTUT_29_8)
      temp = startAddress;
      for (position=0; position < length && !failed; position++)
      {
         if (*(temp++) != position)
         {
            // $[TI3.1]
            failed = TRUE;
         }
         // $[TI3.2]
      }
   }
   // $[TI2.2]


   if ( failed )
   {
      // $[TI4.1]
      return ((Uint32) position);
   }
   else
   {
      // $[TI4.2]
      return GENERAL_RAM_TEST_PASSED;
   }
}

#endif  // SIGMA_BD_CPU

