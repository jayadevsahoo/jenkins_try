#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2014, Covidien/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: SocketWrap.cc - implementation of SocketWrap class
//======================================================================

#include "SocketWrap.hh"

//Add function definitions for socket wraper APIs

Uint32 SocketWrap::HTONL(Uint32 num)
{
#if defined(_WIN32_WCE) || defined(WIN32)
	return htonl(num);
#endif
}

Uint16 SocketWrap::HTONS(Uint16 num)
{
#if defined(_WIN32_WCE) || defined(WIN32)
	return htons(num);
#endif
}

Uint32 SocketWrap::NTOHL(Uint32 num)
{
#if defined(_WIN32_WCE) || defined(WIN32)
	return ntohl(num);
#endif
}

Uint16 SocketWrap::NTOHS(Uint16 num)
{
#if defined(_WIN32_WCE) || defined(WIN32)
	return ntohs(num);
#endif
}

Real32 SocketWrap::HTONF(Real32 realNum)
{
#if defined(_WIN32_WCE) || defined(WIN32)
	Uint32* temp = (Uint32*)(&realNum);
	*temp = htonl(*temp);
	return realNum;
#endif
}

Real32 SocketWrap::NTOHF(Real32 realNum)
{
#if defined(_WIN32_WCE) || defined(WIN32)
	Uint32* temp = (Uint32*)(&realNum);
	*temp = ntohl(*temp);
	return realNum;
#endif
}


