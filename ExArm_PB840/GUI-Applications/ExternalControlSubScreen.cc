#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExternalControlSubScreen - The sub-screen in the Lower screen
// selected by pressing the External Remote Test button on the Other Screens
// subscreen.  It allows the operator to establish external control
// communication and execute remote command.
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen establishes external control communication by accepting the
// LAPTOP_CONNECTED command from the PC and returning COMMUNICATION_GRANTED.
// The issue of the SOFTWARE_VERSION command shall send the software revision
// information to the remote PC.
// The issue of the UNIT_SERIAL_NUMBER command shall send all the serial
// number information to the remote PC.
// The issue of the SOFTWARE_OPTIONS command shall send the software options
// information to the remote PC.
// The issue of the INVENTORY_INFO command shall send an acknowledge command
// back to the remote PC (not implemented yet).
// The issue of the UNIT_RUNTIME_INFO command shall send all the runtime
// information to the remote PC.
// The issue of the MESSAGE_ACK command shall send an acknowledge command
// back to the remote PC.
// The issue of the TEST_EVENT command shall send a test response to/from
// the remote PC.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// Adjust Panel events are communicated via the
// various adjustPanel methods.  Laptop events result in a call to
// laptopRequestHappened() or laptopFailureHappened().
//---------------------------------------------------------------------
//@ Rationale
// Groups the components of the External process control in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// After serial communication is established, this class waits
// for a Serial-IO event received via laptopRequestHappened(),
// or laptopFailureHappened().  Once a test is running,  this class also waits
// for test result data and test commands received from Service-Data and
// dispatched to the virtual methods processTestPrompt(), processTestKeyAllowed(), 
// processTestResultStatus(), processTestResultCondition(), and processTestData().
// It is these virtual methods that format the laptop messages and inform
// the GUI-Serial-IO subsytem to transmit it to the remote PC.
//
// The START_LISTENING command is issued when the subscreen is activated.
// From this point, all the laptop communication is via the methods
// laptopRequestHappened() and laptopFailureHappened().
// 
// The LaptopEventRegistrar is used to register for laptop events.
// The ServiceDataRegistrar is used to register for servicd data events.
// All other functionality is inherited from the SubScreenArea class.
// $[BL00400]
//---------------------------------------------------------------------
//@ Fault-Handling
// none
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created (by ServiceLowerScreen).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ExternalControlSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 025  By:  mnr    Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Support added for new software options Advanced and Lockout		
//
//  Revision: 024  By:  gdc    Date:  24-Feb-2009    SCR Number: 6483
//  Project:  840S
//  Description:
//		Improved handling of invalid test or function ids by returning
// 		an "invalid message" response to the serial port.
//
//  Revision: 023  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 022  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 021  By: rhj		Date: 07-Nov-2008   SCR Number:  6435
//  Project:  840S
//  Description:
//  Added Leak Compensation strings to the list of all available 840 options.
// 
//  Revision: 020  By: gdc		Date: 26-May-2007   SCR Number:  6330
//  Project:  Trend
//  Description:
//  Removed SUN prototype code.
//
//  Revision: 019  By: rhj		Date: 25-Sep-2006   SCR Number:  6236
//  Project:  RESPM
//  Description:
//  Added Respiratory Mechanics and Trending strings to the list of 
//  all available 840 options.
//
//  Revision: 018  By: quf		Date: 15-Oct-2001   DCS Number:  5970
//  Project:  GUIComms
//  Description:
//  Added START_DELTA_CODE and END_DELTA_CODE comments.
//
//  Revision: 017  By: gdc		Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 016   By: sah   Date:  02-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added support for new VTPC option
//
//  Revision: 015   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added support for new PAV option
//
//  Revision: 014  By: sah		Date: 08-Mar-2000   DCS Number:  5683
//  Project:  NeoMode
//  Description:
//      Re-named the option labels, for more consistency.
//
//  Revision: 013   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 012  By:  dosman   Date:  26-Feb-1999    DR Number: 5322
//       Project:  ATC
//       Description:
//		Implemented changes for ATC.
//		Updated code that did not meet our standards
//		(public member functions should not end in underscore).
//		Added a new method to send software options to remote PC.
//		Traced SRS requirements to code.
//		Made some methods const that did not and should not change object.
//
//  Revision: 011  By:  syw   Date:  16-Nov-1998    DR Number: 5252
//       Project:  BiLevel
//       Description:
//			Added requirement tracing.
//
//  Revision: 010  By:  syw	   Date:  24-Aug-1998    DCS Number: 
//  Project:  BiLevel
//  Description:
//		BiLevel Initial version.  Replaced IsDemoDataKey() with
//		IsStandardDevelopmentOptions().
//
//  Revision: 009  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 008  By: yyy      Date: 16-Dec-1997  DR Number: 2684
//    Project:  Sigma (R8027)
//    Description:
//     Added mapping for SRS 07018.
//
//  Revision: 007  By: gdc      Date: 24-Nov-1997  DR Number: 2605
//    Project:  Sigma (R8027)
//    Description:
//     Changed interface to GUI-Serial-Interface as part of its 
//     restructuring.
//
//  Revision: 006  By: gdc      Date: 10-Oct-1997  DR Number: 2172
//    Project:  Sigma (R8027)
//    Description:
//     Added missing requirement annotations.
//
//  Revision: 005  By: gdc      Date: 23-Sep-1997  DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//     Cleaned up Service-Mode interfaces to support remote test.
//     Removed redundant setup screen for entry into external control.
//	   Cleaned up for SUN build.
//
//  Revision: 004  By:  yyy    Date:  17-Sep-97    DR Number: 2507
//    Project:  Sigma (R8027)
//    Description:
//      Added INVENTORY_INFO to the laptop registrar.
//
//  Revision: 003  By:  yyy    Date:  10-Sep-97    DR Number: 2289
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated DEMO datakey type handle.
//
//  Revision: 002  By:  yyy    Date:  24-JUL-97    DR Number: 2247
//    Project:  Sigma (R8027)
//    Description:
//      24-JUL-97  Sending an INVALID_MSG_COMMAND rather than assert.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ExternalControlSubScreen.hh"

#include "LaptopMessageHandler.hh"
#include "PromptArea.hh"
#include "PromptStrs.hh"
#include "MiscStrs.hh"
#include "NovRamManager.hh"

#include "MsgQueue.hh"
#include "TaskMonitor.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "BaseContainer.hh"
#include "Sound.hh"
#include "SubScreenArea.hh"
#include "SoftwareRevNum.hh"
#include "LaptopEventRegistrar.hh"
#include "Post.hh"
#include "SerialInterface.hh"
#include "SoftwareOptions.hh"
#if defined FAKE_SM
	#include "FakeServiceModeManager.hh"
	#define SmManager FakeServiceModeManager
#else		
	#include "SmManager.hh"
#endif	// FAKE_SM
#include "GuiApp.hh"
#include "StringConverter.hh"  

//@ End-Usage

static const Int32 LOWER_STATUS_Y_ = 160;
static const Int32 UPPER_STATUS_Y_ = 220;

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExternalControlSubScreen() 
//
//@ Interface-Description
//  Constructs an ExternalControlSubScreen.  Only one instance of this class
//  should be created.  One parameter is needed and that is a pointer
//  to the subscreen area in which this subscreen will be displayed,
//  i.e. a pointer to the Service Lower Subscreen Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	The subscreen is sized and all drawable components (status text,
// 	and subscreen title area) are added to the displayable
// 	container.  Nine laptop events register this object as a callback target.
// 	The guiTestMgr_ sets up the ResultData array and the test command array
// 	for this test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExternalControlSubScreen::ExternalControlSubScreen(SubScreenArea *pSubScreenArea):
SubScreen(pSubScreenArea),
	readyToProcess_(FALSE),
	statusText_(MiscStrs::ESTABLISHING_CONNECTION_IN_PROGRESS_MESSAGE),
	currentState_(NULL_SYSTEM_MODE_ID),
	currentTestId_(SmTestId::TEST_NOT_START_ID),
	guiTestMgr_(this),
	titleArea_(NULL_STRING_ID, SubScreenTitleArea::SSTA_CENTER)
{
	CALL_TRACE("ExternalControlSubScreen::ExternalControlSubScreen(void)");

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);


	// Size of the status text
	statusText_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);

	// Add the title area
	addDrawable(&titleArea_);

	// Add the following drawables
	addDrawable(&statusText_);
	statusText_.setY(LOWER_STATUS_Y_);
	statusText_.positionInContainer(GRAVITY_CENTER);

	//
	// Register for laptop event callbacks.
	//
#if defined FORNOW
	printf("in ExternalControlSubScreen, this = %d, eventId_ = %d\n",
		   this, LaptopEventId::LAPTOP_CONNECTED);
	printf("in ExternalControlSubScreen, this = %d, eventId_ = %d\n",
		   this, LaptopEventId::COMMUNICATION_GRANTED);
#endif	// FORNOW

	LaptopEventRegistrar::RegisterTarget(LaptopEventId::LAPTOP_CONNECTED, this);
	LaptopEventRegistrar::RegisterTarget(LaptopEventId::COMMUNICATION_GRANTED, this);
	LaptopEventRegistrar::RegisterTarget(LaptopEventId::MESSAGE_ACK, this);
	LaptopEventRegistrar::RegisterTarget(LaptopEventId::SOFTWARE_VERSION, this);
	LaptopEventRegistrar::RegisterTarget(LaptopEventId::UNIT_SERIAL_NUMBER, this);
	LaptopEventRegistrar::RegisterTarget(LaptopEventId::SOFTWARE_OPTIONS, this);
	LaptopEventRegistrar::RegisterTarget(LaptopEventId::INVENTORY_INFO, this);
	LaptopEventRegistrar::RegisterTarget(LaptopEventId::UNIT_RUNTIME_INFO, this);
	LaptopEventRegistrar::RegisterTarget(LaptopEventId::TEST_EVENT, this);
	LaptopEventRegistrar::RegisterTarget(LaptopEventId::FUNCTION_EVENT, this);

	guiTestMgr_.setupTestResultDataArray();
	guiTestMgr_.setupTestCommandArray();
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExternalControlSubScreen()  [Destructor]
//
//@ Interface-Description
// Destroys the Service Lower Subscreen Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Empty 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ExternalControlSubScreen::~ExternalControlSubScreen(void)
{
	CALL_TRACE("ExternalControlSubScreen::~ExternalControlSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// 	Called by LowerSubScreenArea just before this subscreen is displayed
// 	on the screen.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	The first thing to do is to display the subscreen title, display the
//  appropriated prompts, register to Servce-Data registrar to inherit
//  the test results.   And finally, it informs the Serial Interface Queue
//  to start listening to the Laptop communication.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::activate(void)
{
	CALL_TRACE("ExternalControlSubScreen::activate(void)");

#if defined FORNOW
	printf("ExternalControlSubScreen::activate() at line  %d\n", __LINE__);
#endif	// FORNOW

	// Display the "current" title
	titleArea_.setTitle(MiscStrs::EXTERNAL_CONTROL_SUBSCREEN_TITLE);

	// Set next system test state.
	currentState_ = SM_LAPTOP_CONNECTED;

	// Set PRIMARY prompt
	setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
				  PromptStrs::CONNECT_CABLE_TO_REMOTE_COMPUTER_P);
	setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
				  NULL_STRING_ID);
	setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_LOW,
				  PromptStrs::OTHER_SCREENS_CANCEL_S);

	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	guiTestMgr_.registerTestResultData();
	guiTestMgr_.registerTestCommands();


	// Inform the Serial Interface to start listening to the Laptop
	// communication.
	SerialInterface::Start();
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called when this subscreen is being deactivated, i.e. removed from
//  the display, allowing us to do any necessary cleanup.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We perform any necessary cleanup.  We clear the Prompt Area of any prompts
//  that we may have set.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::deactivate(void)
{
	CALL_TRACE("ExternalControlSubScreen::deactivate(void)");

	// Clear the Primary, Advisory and Secondary prompts
	setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
				  NULL_STRING_ID);
	setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
				  NULL_STRING_ID);
	setTestPrompt(PromptArea::PA_SECONDARY,
				  PromptArea::PA_HIGH, NULL_STRING_ID);

	setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
				  NULL_STRING_ID);
	setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
				  NULL_STRING_ID);
	setTestPrompt(PromptArea::PA_SECONDARY,
				  PromptArea::PA_LOW, NULL_STRING_ID);

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Show the following drawables
	statusText_.setShow(TRUE);

	// Set next system test state.
	currentState_ = NULL_SYSTEM_MODE_ID;

	// Set the serial communication flag.
	GuiApp::SetSerialCommunication(FALSE);
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the External control
// process.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call GuiManager's virtual method to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("ExternalControlSubScreen::adjustPanelRestoreFocusHappened(void)");

#if defined FORNOW
	printf("adjustPanelRestoreFocusHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	restoreTestPrompt();
	// $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetVentilatorOptionsString_() [static]
//
//@ Interface-Description
//  Method checks all available options enabled and creates a string to
//  be displayed in the ventilator option screen.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

 
const wchar_t*
	ExternalControlSubScreen::GetVentilatorOptionsString_(void)
{
	// $[TI1]

	wchar_t customerOptionsStr[100+1];

	customerOptionsStr[0]   = L'\0';	// terminate at first element...
	wchar_t separator[] = L", "; 

	for ( Uint optionIdx = 0u; optionIdx < SoftwareOptions::NUM_OPTIONS;
		optionIdx++ )
	{	// $[TI1]
		switch ( optionIdx )
		{
			case SoftwareOptions::BILEVEL :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL) )
				{	// $[TI1.1]
					if ( customerOptionsStr[0] != L'\0' )
					{	// $[TI1.1.1]
						wcscat(customerOptionsStr, separator);
					}	// $[TI1.1.2]

					wcscat(customerOptionsStr, MiscStrs::BILEVEL_OPTION_LABEL);
				}	// $[TI1.2]
				break;
			case SoftwareOptions::NEO_MODE :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE) )
				{	// $[TI1.3]
					if ( customerOptionsStr[0] != L'\0' )
					{	// $[TI1.3.1]
						wcscat(customerOptionsStr, separator);
					}	// $[TI1.3.2]

					wcscat(customerOptionsStr, MiscStrs::NEO_MODE_OPTION_LABEL);
				}	// $[TI1.4]
				break;
			case SoftwareOptions::ATC :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::ATC) )
				{	// $[TI1.5]
					if ( customerOptionsStr[0] != L'\0' )
					{	// $[TI1.5.1]
						wcscat(customerOptionsStr, separator);
					}	// $[TI1.5.2]

					wcscat(customerOptionsStr, MiscStrs::TC_OPTION_LABEL);
				}	// $[TI1.6]
				break;
			case SoftwareOptions::VTPC :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::VTPC) )
				{	// $[TI1.7]
					if ( customerOptionsStr[0] != L'\0' )
					{	// $[TI1.7.1]
						wcscat(customerOptionsStr, separator);
					}	// $[TI1.7.2]

					wcscat(customerOptionsStr, MiscStrs::VTPC_OPTION_LABEL);
				}	// $[TI1.8]
				break;
			case SoftwareOptions::PAV :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::PAV) )
				{	// $[TI1.9]
					if ( customerOptionsStr[0] != L'\0' )
					{	// $[TI1.9.1]
						wcscat(customerOptionsStr, separator);
					}	// $[TI1.9.2]

					wcscat(customerOptionsStr, MiscStrs::PAV_OPTION_LABEL);
				}	// $[TI1.10]
				break;
			case SoftwareOptions::RESP_MECH :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH) )
				{
					if ( customerOptionsStr[0] != L'\0' )
					{
						wcscat(customerOptionsStr, separator);
					}
					wcscat(customerOptionsStr, MiscStrs::RESP_MECH_OPTION_LABEL);
				}
				break;
			case SoftwareOptions::TRENDING :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::TRENDING) )
				{
					if ( customerOptionsStr[0] != L'\0' )
					{
						wcscat(customerOptionsStr, separator);
					}
					wcscat(customerOptionsStr, MiscStrs::TRENDING_OPTION_LABEL);
				}
				break;
			case SoftwareOptions::LEAK_COMP :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP) )
				{
					if ( customerOptionsStr[0] != L'\0' )
					{
						wcscat(customerOptionsStr, separator);
					}
					wcscat(customerOptionsStr, MiscStrs::LEAK_COMP_OPTION_LABEL);
				}
				break;
			case SoftwareOptions::NEO_MODE_UPDATE :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_UPDATE) )
				{
					if ( customerOptionsStr[0] != L'\0' )
					{
						wcscat(customerOptionsStr, separator);
					}
					wcscat(customerOptionsStr, MiscStrs::NEO_MODE_UPDATE_OPTION_LABEL);
				}
				break;
			case SoftwareOptions::NEO_MODE_ADVANCED :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_ADVANCED) )
				{
					if ( customerOptionsStr[0] != L'\0' )
					{
						wcscat(customerOptionsStr, separator);
					}
					wcscat(customerOptionsStr, MiscStrs::NEO_MODE_OPTION_LABEL);
					wcscat(customerOptionsStr, separator);
					wcscat(customerOptionsStr, MiscStrs::NEO_MODE_UPDATE_OPTION_LABEL);
					wcscat(customerOptionsStr, separator);
					wcscat(customerOptionsStr, MiscStrs::NEO_MODE_ADVANCED_OPTION_LABEL);
				}
				break;
			case SoftwareOptions::NEO_MODE_LOCKOUT :
				if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_LOCKOUT) )
				{
					if ( customerOptionsStr[0] != L'\0' )
					{
						wcscat(customerOptionsStr, separator);
					}
					wcscat(customerOptionsStr, MiscStrs::NEO_MODE_OPTION_LABEL);
					wcscat(customerOptionsStr, separator);
					wcscat(customerOptionsStr, MiscStrs::NEO_MODE_UPDATE_OPTION_LABEL);
					wcscat(customerOptionsStr, separator);
					wcscat(customerOptionsStr, MiscStrs::NEO_MODE_ADVANCED_OPTION_LABEL);
					wcscat(customerOptionsStr, separator);
					wcscat(customerOptionsStr, MiscStrs::NEO_MODE_LOCKOUT_OPTION_LABEL);
				}
				break;
			default :
				// unexpected option index...
				AUX_CLASS_ASSERTION_FAILURE(optionIdx);
				break;
		}
	}

	return(customerOptionsStr);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: performTest_ 	[private]
//
//@ Interface-Description
//	Process and dispatch Laptop test message into Service Mode test commands.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the incoming command is START_TEST_COMMAND, we check the ready flag.
//	If flag is not set, this means no test is running currently; we set this flag
//	then prepare to start Service-Mode's test command.  If this flag is
//	set, we prevent this test from starting by sending off an "Invalid
//	message".
//	If the incoming command is USER RESPONSE, we inform Service-Mode about
//	the user's key press.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::performTest_(Uint8 *pMsgData)
{
	SmTestId::ServiceModeTestId smTestId;

	switch ( pMsgData[LaptopEventId::INCOMING_LT_COMMAND_IDX] )
	{
		case LaptopEventId::START_TEST_COMMAND:			// $[TI1]
			if ( !readyToProcess_ )
			{											// $[TI2]
				const SmTestId::RemoteTestId& remoteId = 
					SmTestId::RemoteTestId(pMsgData[LaptopEventId::INCOMING_LT_COMMAND_VALUE_IDX]);

				if ( remoteId <= SmTestId::REMOTE_TEST_NO_START_ID ||
					 remoteId >= SmTestId::REMOTE_TEST_MAX )
				{
					sendCommand_(Uint8(LaptopEventId::INVALID_MSG_COMMAND));
				}
				else
				{
					smTestId = SmTestId::GetSmIdFromRemoteId(remoteId);

					if ( smTestId <= SmTestId::TEST_NOT_START_ID ||
						 smTestId >= SmTestId::NUM_SERVICE_TEST_ID )
					{
						sendCommand_(Uint8(LaptopEventId::INVALID_MSG_COMMAND));
					}
					else
					{
						// test id is OK - start the test
						testStart_();

						// Let the service mode  manager knows the type of Service mode.
						SmManager::DoFunction(SmTestId::SET_TEST_TYPE_REMOTE_ID);

						// Prepare to start the test by setting the private member test id
						currentTestId_ = SmTestId::ServiceModeTestId(remoteId);

						SmManager::DoTest( smTestId, FALSE, (char*)(&pMsgData[LaptopEventId::INCOMING_LT_COMMAND_PARAM_IDX]) );
					}
				}
			}
			else
			{
				sendCommand_(Uint8(LaptopEventId::INVALID_MSG_COMMAND));
			}
			break;

		case LaptopEventId::USER_RESPONSE_COMMAND:					// $[TI4]
			CLASS_ASSERTION(currentTestId_ != SmTestId::TEST_NOT_START_ID);

			SmPromptId::ActionId userKeyPressedId;
			userKeyPressedId = 
				(SmPromptId::ActionId) pMsgData[LaptopEventId::INCOMING_LT_COMMAND_VALUE_IDX];

			CLASS_ASSERTION(userKeyPressedId > SmPromptId::NULL_ACTION_ID &&
							userKeyPressedId < SmPromptId::NUM_ACTION_ID);

			// Need to check if the test number is out of range, and convert to
			// ServiceModeTestId from RemoteTestId
			smTestId = SmTestId::GetSmIdFromRemoteId((SmTestId::RemoteTestId) currentTestId_);

			CLASS_ASSERTION(smTestId > SmTestId::TEST_NOT_START_ID &&
							smTestId < SmTestId::NUM_SERVICE_TEST_ID);

			// Signal the Service Test Manager to continue the test.
			SmManager::OperatorAction(smTestId, userKeyPressedId);
			break;

		default:
			CLASS_ASSERTION_FAILURE();
			break;
	} // switch
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: performFunction_ 	[private]
//
//@ Interface-Description
//	Process and dispatch Laptop test message into Service Mode function commands.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the incoming function is supported, we inform SmManager to
//	perform the DoFunction() function.  Any other function is not supported
//	currently.
//
//  $[08045] Requests to reprogram Flash EEPROM must originate through
//  the GUI serial interface.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::performFunction_(Uint8 *pMsgData)
{
	CALL_TRACE("ExternalControlSubScreen::performFunction_("
			   "Uint8 *pMsgData)");

	const SmTestId::FunctionId& functionId 
		= SmTestId::FunctionId(pMsgData[LaptopEventId::INCOMING_LT_COMMAND_IDX]);

	if (functionId <= SmTestId::FUNCTION_ID_START ||
		functionId >= SmTestId::FUNCTION_ID_MAX)
	{
		sendCommand_(Uint8(LaptopEventId::INVALID_MSG_COMMAND));
	}
	else
	{
		switch ( functionId )
		{
			case SmTestId::FORCE_EST_OVERIDE_ID:			// $[TI1]
				if ( SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions() )
				{											// $[TI1.1]
					SmManager::DoFunction( functionId );
					sendCommand_( Uint8(LaptopEventId::MESSAGE_ACK_COMMAND) );
				}
				else
				{											// $[TI1.2]
					sendCommand_( Uint8(LaptopEventId::INVALID_MSG_COMMAND) );
				}
				break;
	
			case SmTestId::BD_DOWNLOAD_ID:					// $[TI3]
			case SmTestId::GUI_DOWNLOAD_ID:
			case SmTestId::DELETE_SYSTEM_DIAGNOSTIC_LOG_ID:
			case SmTestId::DELETE_SYSTEM_INFORMATION_LOG_ID:
			case SmTestId::DELETE_SELFTEST_LOG_ID:
	
				SmManager::DoFunction( functionId );
				sendCommand_( Uint8(LaptopEventId::MESSAGE_ACK_COMMAND) );
				break;
	
			default:										// $[TI2]
				sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
				break;
		}
	
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: laptopRequestHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an LaptopEventTarget.  It is
// called when the GuiApp task queue detected an request/response from the
// Serial I/O interface.  Each registered target shall process this
// request/response and an appropriated action shall be conducted.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the laptop event is LAPTOP_CONNECTED and the current state of the
// screen is not set, then set the current state to communication granted 
// mode, set the serial communication flag on, blank
// both the upper and lower service screen, display the remote test in
// progress message and respond to GUI-Serial-Io with COMMUNICATION_GRANTED_COMMAND.
// If the laptop event is SOFTWARE_VERSION command then simply respond to
// GUI-Serial-Io with the software revision information.
// If the laptop event is UNIT_SERIAL_NUMBER command then simply respond to
// GUI-Serial-Io with the serial number information.
// If the laptop event is SOFTWARE_OPTIONS command then simply respond to
// GUI-Serial-Io with the software options information.
// If the laptop event is INVENTORY_INFO command then simply respond to
// GUI-Serial-Io with the acknowledge command (not defined yet).
// If the laptop event is UNIT_RUNTIME_INFO then simply respond to
// GUI-Serial-Io with the runtime information.
// If the laptop event is MESSAGE_ACK command then simply respond to
// GUI-Serial-Io with the acknowledge command.
// If the laptop event is TEST_EVENT command then simply respond to
// GUI-Serial-Io with the test response information.
// If the laptop event is FUNCTION_EVENT command then simply respond to
// GUI-Serial-Io with the test response information.
//
//  $[08046] Each EST test may be invoked individually through the GUI
//  serial interface.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::laptopRequestHappened(LaptopEventId::LTEventId eventId,
													Uint8 *pMsgData)
{
	CALL_TRACE("ExternalControlSubScreen::laptopRequestHappened("
			   "LaptopEventId::LTEventId eventId, Uint8 *pMsgData)");

#if defined FORNOW
	printf("ExternalControlSubScreen::laptopRequestHappened at line  %d, eventId = %d, currentState_ = %d\n",
		   __LINE__, eventId, currentState_);
#endif	// FORNOW

	// Strobing TaskMonitor to keep alive
	TaskMonitor::Report();


	switch ( eventId )
	{
		case LaptopEventId::LAPTOP_CONNECTED:
			// $[TI1]
			switch ( currentState_ )
			{
				case SM_LAPTOP_CONNECTED:						// $[TI2]
					// Deactivate this subscreen
					getSubScreenArea()->deactivateSubScreen();

					// Blank both the upper and lower LCD screens
					GuiFoundation::GetLowerBaseContainer().removeAllDrawables();
					GuiFoundation::GetUpperBaseContainer().removeAllDrawables();
					GuiFoundation::GetLowerBaseContainer().setFillColor(Colors::MEDIUM_BLUE);
					GuiFoundation::GetUpperBaseContainer().setFillColor(Colors::MEDIUM_BLUE);

					// Display a message indicating test is in progress.
					// $[07018] When the PC initiates external control, the lower LCD ...
					statusText_.setText(MiscStrs::REMOTE_SYSTEM_TEST_IN_PROGRESS_MESSAGE);
					statusText_.setY(UPPER_STATUS_Y_);
					statusText_.positionInContainer(GRAVITY_CENTER);
					GuiFoundation::GetUpperBaseContainer().addDrawable(&statusText_);

					// Set next system test state.
					currentState_ = SM_COMMUNICATION_IN_PROGRESS;

					// Set the serial communication flag.
					GuiApp::SetSerialCommunication(TRUE);

					sendCommand_((Uint8) LaptopEventId::COMMUNICATION_GRANTED_COMMAND);

					break;

				default:										// $[TI6]
					sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
					break;
			}
			break;

		case LaptopEventId::SOFTWARE_VERSION:				// $[TI7]
			if ( !GuiApp::IsSerialCommunicationUp() )
			{												// $[TI7.1]
				sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
			}
			else
			{												// $[TI7.2]
				 
				sendSwRevisionCommand_(SoftwareRevNum::GetInstance().GetAsShortString(), 
					GuiApp::GetBdSoftwareRevNum());
				 
			}
			break;

		case LaptopEventId::UNIT_SERIAL_NUMBER:				// $[TI8]
			if ( !GuiApp::IsSerialCommunicationUp() )
			{												// $[TI8.1]
				sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
			}
			else
			{												// $[TI8.2]
				sendSerialNumberCommand_();
			}
			break;

		case LaptopEventId::SOFTWARE_OPTIONS:				// $[TI15]
			if ( !GuiApp::IsSerialCommunicationUp() )
			{
				// $[TI15.1]
				sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
			}
			else
			{
				// $[TI15.2]
				sendSwOptionsCommand_();
			}
			break;

		case LaptopEventId::INVENTORY_INFO:					// $[TI9]
			if ( !GuiApp::IsSerialCommunicationUp() )
			{												// $[TI9.1]
				sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
			}
			else
			{												// $[TI9.2]
				sendCommand_((Uint8) LaptopEventId::MESSAGE_ACK_COMMAND);
			}
			break;

		case LaptopEventId::UNIT_RUNTIME_INFO:				// $[TI10]
			if ( !GuiApp::IsSerialCommunicationUp() )
			{												// $[TI10.1]
				sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
			}
			else
			{												// $[TI10.2]
				sendRunTimeCommand_(NovRamManager::GetCompressorOperationalHours(),
									NovRamManager::GetSystemOperationalHours());
			}
			break;

		case LaptopEventId::MESSAGE_ACK:					// $[TI11]
			sendCommand_((Uint8) LaptopEventId::MESSAGE_ACK_COMMAND);
			break;

		case LaptopEventId::TEST_EVENT:						// $[TI12]
			if ( !GuiApp::IsSerialCommunicationUp() )
			{												// $[TI12.1]
				sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
			}
			else
			{												// $[TI12.2]
				performTest_(pMsgData);
			}
			break;

		case LaptopEventId::FUNCTION_EVENT:					// $[TI13]
			if ( !GuiApp::IsSerialCommunicationUp() )
			{												// $[TI13.1]
				sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
			}
			else
			{												// $[TI13.2]
				performFunction_(pMsgData);
			}
			break;

		default:											// $[TI14]
			sendCommand_((Uint8) LaptopEventId::INVALID_MSG_COMMAND);
			break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: laptopFailureHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an LaptopEventTarget.  It is
// called when the GuiApp task queue detected an failure response from the
// Serial I/O interface.  Each registerated target shall process this
// failure response and an appropriated action shall be conducted.
//---------------------------------------------------------------------
//@ Implementation-Description
// Resend the command to serial I/O handler.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::laptopFailureHappened(SerialInterface::SerialFailureCodes
													serialFailureCodes,
													Uint8 eventId)
{
	CALL_TRACE("ExternalControlSubScreen::laptopFailureHappened("
			   "SerialInterface::SerialFailureCodes serialFailureCodes)");

#if defined FORNOW
	printf("ExternalControlSubScreen::laptopFailureHappened() at line  %d, "
		   "serialFailureCodes=%d, eventId = %d\n",
		   __LINE__, serialFailureCodes, eventId);
#endif	// FORNOW
	// $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendCommand_
//
//@ Interface-Description
//	Called by this class to fill a message with the given command in the
//  proper format and then send this message to the Serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, clear the outgoing buffer.  Then fill it with the message number 
//  and the command.  Finally, post the message to the Serial I/O queue.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::sendCommand_(Uint8 command) const
{
	CALL_TRACE("ExternalControlSubScreen::sendCommand_(Uint8 command)");


#if defined FORNOW
	printf("ExternalControlSubScreen::sendCommand_() at line  %d, command=%d\n",
		   __LINE__, command);
#endif	// FORNOW

	// Build message buffer to serial I/O handler to respond to
	// the laptop request.
	LaptopMessageHandler::ClearBuffer();
	LaptopMessageHandler::SetMessageNo();
	LaptopMessageHandler::SetCommand(command);

	// Ask GuiApp to post the message to the Serial I/O queue.
	LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
													LaptopMessageHandler::GetBuffer(),
													LaptopMessageHandler::GetBufferIndex());
	// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendSwRevisionCommand_
//
//@ Interface-Description
//	Called by this class to fill a message with the given software revision
//  in the proper format and then send this message to the Serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, clear the outgoing buffer.  Then fill it with the message number 
//  and the software revision command.  Finally, post the message to the Serial
//  I/O queue.
//
//  $[08050] The serial numbers and software revision information shall be
//  returned to the PC upon request through the serial interface.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
	ExternalControlSubScreen::sendSwRevisionCommand_(const char *guiRevision, const char *bdRevision) const
{
	CALL_TRACE("ExternalControlSubScreen::sendSwRevisionCommand_(char *guiRevision, char *bdRevision)");


#if defined FORNOW
	printf("ExternalControlSubScreen::sendSwRevisionCommand_() at line  %d\n",  __LINE__);
	printf("	    guiRevision=%s, bdRevision=%s\n", guiRevision, bdRevision);
#endif	// FORNOW

	// Build message buffer to serial I/O handler to respond to
	// the laptop request.
	LaptopMessageHandler::ClearBuffer();
	LaptopMessageHandler::SetMessageNo();

	LaptopMessageHandler::SetSwRevisionCommand(guiRevision, bdRevision);

	// Ask GuiApp to post the message to the Serial I/O queue.
	LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
													LaptopMessageHandler::GetBuffer(),
													LaptopMessageHandler::GetBufferIndex());
	// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendSwOptionsCommand_
//
//@ Interface-Description
//	Called by this class to fill a message with the 
//	software options available in the proper format and then send this
//  message to the Serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, clear the outgoing buffer.  
//	Then fill it with the message number 
//  	and the software options command.  
//	Finally, post the message to the Serial I/O queue.
//
//  $[TC08001] The software options information shall be
//  returned to the PC upon request through the serial interface.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::sendSwOptionsCommand_(void) const
{
	CALL_TRACE("ExternalControlSubScreen::sendSwOptionsCommand_(void)");

#if defined FORNOW
	printf("ExternalControlSubScreen::sendSwOptionsCommand_() at line  %d\n",   __LINE__);
#endif	// FORNOW

	// Build message buffer to serial I/O handler to respond to
	// the laptop request.
	LaptopMessageHandler::ClearBuffer();
	LaptopMessageHandler::SetMessageNo();

	// TODO E600 Unicode Text 
	// This is an external communication interface class and not sure that other side is ready to .
	// accept wide char hence converting to char
	char cTempBuff[256]; 
	StringConverter::ToString( cTempBuff, 256, GetVentilatorOptionsString_() );
	LaptopMessageHandler::SetSwOptionsCommand(cTempBuff);

	// Ask GuiApp to post the message to the Serial I/O queue.
	LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
													LaptopMessageHandler::GetBuffer(),
													LaptopMessageHandler::GetBufferIndex());
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendSerialNumberCommand_
//
//@ Interface-Description
//	Called by this class to fill a message with the Serial numbers 
//  in the proper format and then send this message to the Serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, clear the outgoing buffer.  Then fill it with the message number 
//  and the serial number revision command.  Finally, post the message to
//  the Serial I/O queue.
//
//  $[08050] The serial numbers and software revision information shall be
//  returned to the PC upon request through the serial interface.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::sendSerialNumberCommand_(void) const
{
	CALL_TRACE("ExternalControlSubScreen::sendSerialNumberCommand_(void)");


#if defined FORNOW
	printf("ExternalControlSubScreen::sendSerialNumberCommand_() at line  %d\n",    __LINE__);
#endif	// FORNOW

	// Build message buffer to serial I/O handler to respond to
	// the laptop request.
	LaptopMessageHandler::ClearBuffer();
	LaptopMessageHandler::SetMessageNo();

 



	LaptopMessageHandler::SetSerialNumberCommand(Post::GetKernelPartNumber(),
												 GuiApp::GetBdKernelPartNumber(),
												 GuiApp::GetGuiSN().getString(),
												 GuiApp::GetBdSN().getString(),
												 GuiApp::GetCompressorSerialNum().getString());

	// Ask GuiApp to post the message to the Serial I/O queue.
	LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
													LaptopMessageHandler::GetBuffer(),
													LaptopMessageHandler::GetBufferIndex());
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendRunTimeCommand_
//
//@ Interface-Description
//	Called by this class to fill a message with the runtime values 
//  in the proper format and then send this message to the Serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, clear the outgoing buffer.  Then fill it with the message number 
//  and the runtime information command.  Finally, post the message to
//  the Serial I/O queue.
//
//  $[08051] The operational hours for the ventilator and the compressor
//  shall be returned to the PC upon request through the serial interface.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::sendRunTimeCommand_(Real32 compressorValue, Real32 systemValue) const
{
	CALL_TRACE("ExternalControlSubScreen::sendRunTimeCommand_(Real32 compressorValue, Real32 systemValue)");


#if defined FORNOW
	printf("ExternalControlSubScreen::sendRunTimeCommand_() at line  %d\n", __LINE__);
#endif	// FORNOW

	// Build message buffer to serial I/O handler to respond to
	// the laptop request.
	LaptopMessageHandler::ClearBuffer();
	LaptopMessageHandler::SetMessageNo();
	LaptopMessageHandler::SetRunTimeCommand(compressorValue, systemValue);

	// Ask LaptopMessageHandler to post the message to the Serial I/O queue.
	LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
													LaptopMessageHandler::GetBuffer(),
													LaptopMessageHandler::GetBufferIndex());
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestPrompt
//
//@ Interface-Description
// Handle the new prompt command.
//---------------------------------------------------------------------
//@ Implementation-Description
// The new prompt command can't be processed unless the user has set the
// readyToProcess_ flag.
// If the test is invoked by the laptop serial communication, then simply
// construct the corresponding message  and post the message to the Serial
// I/O queue.
//
//  $[08049] All prompts and required user actions associated with EST test
//  invoked through the GUI serial interface, are transmitted/received
//  through the serial interface only.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("ExternalControlSubScreen::processTestPrompt(Int command)");

#if defined FORNOW
	printf("ExternalControlSubScreen::processTestPrompt at line  %d, command = %d\n",
		   __LINE__, command);
#endif	// FORNOW


	if ( !readyToProcess_ )
	{															// $[TI1]
		return;
	}															// $[TI2]

	if ( GuiApp::IsSerialCommunicationUp() )
	{															// $[TI3]
		// Setup message buffer to send to serial I/O handler.
		LaptopMessageHandler::ClearBuffer();
		LaptopMessageHandler::SetMessageNo();
		LaptopMessageHandler::SetCommand(LaptopEventId::PROMPT_COMMAND, command);

		// Ask LaptopMessageHandler to post the message to the Serial I/O queue.
		LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
														LaptopMessageHandler::GetBuffer(),
														LaptopMessageHandler::GetBufferIndex());
	}															// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
// Handle the new key allowed command.
//---------------------------------------------------------------------
//@ Implementation-Description
// The new Key Allowed command can't be processed unless the user has set the
// readyToProcess_ flag.
/// If the test is invoked by the laptop serial communication, then simply
// construct the corresponding message  and post the message to the Serial
// I/O queue.
//
//  $[08049] All prompts and required user actions associated with EST test
//  invoked through the GUI serial interface, are transmitted/received
//  through the serial interface only.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::processTestKeyAllowed(Int keyAllowed)
{
	CALL_TRACE("ExternalControlSubScreen::processTestKeyAllowed(Int keyAllowed)");

#if defined FORNOW
	printf("ExternalControlSubScreen::processTestKeyAllowed at line  %d, keyAllowed = %d\n",
		   __LINE__, keyAllowed);
#endif	// FORNOW


	if ( !readyToProcess_ )
	{														// $[TI1]
		return;
	}														// $[TI2]

	if ( GuiApp::IsSerialCommunicationUp() )
	{														// $[TI3]
		// Setup message buffer to send to serial I/O handler.
		LaptopMessageHandler::ClearBuffer();
		LaptopMessageHandler::SetMessageNo();
		LaptopMessageHandler::SetCommand(LaptopEventId::KEY_ALLOWED_COMMAND, keyAllowed);

		// Ask GuiApp to post the message to the Serial I/O queue.
		LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
														LaptopMessageHandler::GetBuffer(),
														LaptopMessageHandler::GetBufferIndex());
	}														// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
// Handle the new status command.
//---------------------------------------------------------------------
//@ Implementation-Description
// The new status command can't be processed unless the user has set the
// readyToProcess_ flag.
// If the test is invoked by the laptop serial communication, then simply
// construct the corresponding message  and post the message to the Serial
// I/O queue.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("ExternalControlSubScreen::processTestResultStatus(Int resultStatus)");

	CLASS_ASSERTION((resultStatus >= (Int) SmStatusId::TEST_END) &&
					(resultStatus < (Int) SmStatusId::NUM_TEST_DATA_STATUS));
#if defined FORNOW
	printf("ExternalControlSubScreen::processTestResultStatus at line  %d, resultStatus= %d\n",
		   __LINE__, resultStatus);
#endif	// FORNOW


	if ( !readyToProcess_ )
	{													// $[TI1]
		return;
	}													// $[TI2]

	if ( GuiApp::IsSerialCommunicationUp() )
	{													// $[TI3]
		// Setup message buffer to send to serial I/O handler.
		LaptopMessageHandler::ClearBuffer();
		LaptopMessageHandler::SetMessageNo();
		LaptopMessageHandler::SetCommand(LaptopEventId::STATUS_COMMAND, resultStatus);

		// Ask LaptopMessageHandler to post the message to the Serial I/O queue.
		LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
														LaptopMessageHandler::GetBuffer(),
														LaptopMessageHandler::GetBufferIndex());

		if ( resultStatus == SmStatusId::TEST_END )
		{												// $[TI4]
			testEnd_();
		}												// $[TI5]
	}													// $[TI6]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
// Handle the new condition command.
//---------------------------------------------------------------------
//@ Implementation-Description
// The new condition command can't be processed unless the user has set the
// readyToProcess_ flag.
// If the test is invoked by the laptop serial communication, then simply
// construct the corresponding message  and post the message to the Serial
// I/O queue.
//---------------------------------------------------------------------
//@ PreCondition
// The given `newScreenId' must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("ExternalControlSubScreen::processTestResultCondition(Int resultCondition)");

#if defined FORNOW
	printf("ExternalControlSubScreen::processTestResultCondition at line  %d, resultCondition=%d\n",
		   __LINE__, resultCondition);
#endif	// FORNOW

	// Filling the condition up. This will be updated to NVram.

#if defined (FORNOW)
	printf("ExternalControlSubScreen::processTestResultCondition() should update condition\n");
#endif 	// FORNOW


	if ( !readyToProcess_ )
	{													// $[TI1]
		return;
	}													// $[TI2]

	if ( resultCondition == SmStatusId::NULL_CONDITION_ITEM ||
		 resultCondition == SmStatusId::NUM_CONDITION_ITEM )
	{													// $[TI3]
		return;
	}

	if ( GuiApp::IsSerialCommunicationUp() )
	{													// $[TI4]
		// Setup message buffer to send to serial I/O handler.
		LaptopMessageHandler::ClearBuffer();
		LaptopMessageHandler::SetMessageNo();
		LaptopMessageHandler::SetCommand(LaptopEventId::CONDITION_COMMAND, resultCondition);

		// Ask LaptopMessageHandler to post the message to the Serial I/O queue.
		LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
														LaptopMessageHandler::GetBuffer(),
														LaptopMessageHandler::GetBufferIndex());
	}													// $[TI5]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
// Handle the new data command.
//---------------------------------------------------------------------
//@ Implementation-Description
// The new data command can't be processed unless the user has set the
// readyToProcess_ flag.
// If the test is invoked by the laptop serial communication, then simply
// construct the corresponding message  and post the message to the Serial
// I/O queue.
//
//  $[08047] For any EST test invoked through the GUI serial interface,
//  Sigma shall return each flow and pressure tested data point.
//---------------------------------------------------------------------
//@ PreCondition
// The given `newScreenId' must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("ExternalControlSubScreen::processTestData(Int dataIndex, TestResult *pResult)");

#if defined FORNOW
	printf("ExternalControlSubScreen::processTestData at line  %d, dataIndex = %d\n",
		   __LINE__, dataIndex);
#endif	// FORNOW

	CLASS_ASSERTION(dataIndex < TestDataId::LAST_DATA_ITEM &&
					dataIndex >= 0);

	if ( !readyToProcess_ )
	{														// $[TI1]
		return;
	}														// $[TI2]

#if defined FORNOW
	printf("pResult->getTestDataValue = %f\n", pResult->getTestDataValue());
#endif	// FORNOW

	// Filling the condition up. This will be updated to NVram.

	if ( GuiApp::IsSerialCommunicationUp() )
	{														// $[TI3]
		// Setup message buffer to send to serial I/O handler.
		LaptopMessageHandler::ClearBuffer();
		LaptopMessageHandler::SetMessageNo();
		LaptopMessageHandler::SetDataCommand(dataIndex, pResult->getTestDataValue());

		// Ask LaptopMessageHandler to post the message to the Serial I/O queue.
		LaptopMessageHandler::SerialOutputEventHappened(SerialInterface::SERIAL_OUTPUT_AVAILABLE,
														LaptopMessageHandler::GetBuffer(),
														LaptopMessageHandler::GetBufferIndex());
	}														// $[TI4]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
	ExternalControlSubScreen::SoftFault(const SoftFaultID  softFaultID,
										const Uint32       lineNumber,
										const char*        pFileName,
										const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							EXTERNALCONTROLSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

