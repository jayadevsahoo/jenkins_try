#ifndef BackgroundMemoryApp_HH
#define BackgroundMemoryApp_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================ H E A D E R   D E S C R I P T I O N ====
//  Class: BackgroundMemoryApp - Background memory testing application
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundMemoryApp.hhv   25.0.4.0   19 Nov 2013 14:20:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//
//  Revision: 002 By: iv     Date: 11-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//		   Added Uint16 &errorCode argument for memory tests.
//
//  Revision: 001    By: by Date: 11-Nov-1996 DR Number: None
//    Project: Sigma (840)
//    Description:
//       Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "cktable.hh"
#include "Background.hh"

class BackgroundMemoryApp
{
    public:

        static void Task( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL, 
			       const char*       pPredicate = NULL );

    private:

        BackgroundMemoryApp( void );      // not implemented
        ~BackgroundMemoryApp( void );     // not implemented           

        static BkEventName FlashMemoryTest_( Uint16 &errorCode );

        static SigmaStatus
        VerifyAppsChecksum32_( const EepromStruct* pFlashMemory, Uint16 &errorCode );

        //@ Data-Member: HasFlashMemoryTestFailed_ 
        //  indicates the current status of Flash Memory test
        static Boolean HasFlashMemoryTestFailed_;

#if defined (SIGMA_UNIT_TEST)
        static void SetupMemoryBlock_( void );
        static Boolean FailChecksum_;
#endif // defined (SIGMA_UNIT_TEST)

};
           

#endif    // #ifndef BackgroundMemoryApp_HH
