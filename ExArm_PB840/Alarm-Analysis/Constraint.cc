#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Constraint - Holds the common info for temporal and count constraints.
//---------------------------------------------------------------------
//@ Interface-Description
// The Constraint class contains minimum and maximum Boolean operators, 
// values and methods used to determine the state of a constraint. 
// Constraint declares a pure virtual function, evaluate, which is defined by
// TemporalConstraint and CountConstraint.
//---------------------------------------------------------------------
//@ Rationale
// Some of the condition augmentations from the Non-Technical and Technical
// Alarm Matrices in the Software Requirements Specification define conditions
// which are constrained by time (TemporalConstraint) or number of
// violations (CountConstraint).  The pure virtual base class Constraint
// provides the common data and functionality for its derived classes,
// TemporalConstraint and CountConstraint to model these constraints.
//---------------------------------------------------------------------
//@ Implementation-Description
// Assume a matrix row defines a temporal constraint as "Detection criteria met 
// for >= 20 seconds and < 40 seconds".  The attributes contained in Constraint 
// would be defined during construction of the TemporalConstraint as follows:
//   minBooleanOp_ = GE,
//   maxBooleanOp_ = LT,
//   pMinCalcFunction = &CalcFunctions::Return20Sec(),
//   pMaxCalcFunction = &CalcFunctions::Return40Sec().
//
// When messaged, CalcFunctions::Return20Sec and CalcFunctions::Return40Sec
// return 20 seconds and 40 seconds respectively.
//
// When this constraint is evaluated, if the detection criteria for the alarm
// is TRUE, the length of time it has been TRUE will be calculated
// and compared against the bounds specified in this constraint.  If the
// duration falls within the bounds of the constraint, the OperatingGroup
// object associated with the constraint will be set to active and its
// augmentation included in the Alarm object's current annunciation.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/Constraint.ccv   25.0.4.0   19 Nov 2013 13:51:20   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 003   By:   hct      Date: 04/22/99         DR Number:   5359
//      Project:   ATC
//      Description:
//         Added functionality to store values of pMinCalcFunction_ and
//         pMaxCalcFunction_ when associated OperatingGroup becomes active.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "Constraint.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Constraint
//
//@ Interface-Description
//   Constructor.
//
// >Von
//   minBooleanOp      Enumerator representing the Boolean operator used for 
//                     evaluating the Constraint minimum.
//   maxBooleanOp      Enumerator representing the Boolean operator used for 
//                     evaluating the Constraint maximum.
//   pMinCalcFunction  Pointer to a function which returns the minimum value.
//   pMaxCalcFunction  Pointer to a function which returns the maximum value.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      ((Constraint::BOOLEAN_OP_NULL < minBooleanOp) &&
//       (minBooleanOp < Constraint::MAX_NUM_BOOLEAN_OPS))
//      ((Constraint::BOOLEAN_OP_NULL < maxBooleanOp) &&
//       (maxBooleanOp < Constraint::MAX_NUM_BOOLEAN_OPS))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

Constraint::Constraint(Constraint::BooleanOperator minBooleanOp, 
                       Constraint::BooleanOperator maxBooleanOp, 
                       Constraint::CalcFunctionPtrType pMinCalcFunction,
                       Constraint::CalcFunctionPtrType pMaxCalcFunction):
                       minBooleanOp_(minBooleanOp),
                       maxBooleanOp_(maxBooleanOp),
                       pMinCalcFunction_(pMinCalcFunction),
                       pMaxCalcFunction_(pMaxCalcFunction)
                       
{
  CALL_TRACE("Constraint(minBooleanOp, maxBooleanOp, pMinCalcFunction, pMaxCalcFunction)");

  CLASS_PRE_CONDITION((Constraint::BOOLEAN_OP_NULL < minBooleanOp) &&
                      (minBooleanOp < Constraint::MAX_NUM_BOOLEAN_OPS));
  CLASS_PRE_CONDITION((Constraint::BOOLEAN_OP_NULL < maxBooleanOp) &&
                      (maxBooleanOp < Constraint::MAX_NUM_BOOLEAN_OPS));
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Constraint
//
//@ Interface-Description
//   Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

Constraint::~Constraint(void)
{
  CALL_TRACE("~Constraint(void)");
}


//=====================================================================
//
//      Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateBoolean_
//
//@ Interface-Description
//   Return the Boolean value of op1 boolOp op2 (e.g. return(1 >= 2)).
//
// >Von
//   op1     Left hand operand of boolean expression.
//   op2     Right hand operand of boolean expression.
//   boolOp  Enumerator representing a boolean operator.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   boolOp == EQ or NE or LT or GT or GE or LE
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
Boolean
Constraint::evaluateBoolean_(Int32 op1, 
                             Int32 op2, 
                             Constraint::BooleanOperator boolOp) const
{
  CALL_TRACE("evaluateBoolean_(op1, op2, boolOp)");


  Boolean result;


  // BooleanOperator is an enumeration representing Boolean operators.
  switch (boolOp)
  {
    case (Constraint::EQ): // $[TI1]
      result = (op1 == op2); // $[TI1.1]  $[TI1.2]
      break;
    case (Constraint::NE): // $[TI2]
      result = (op1 != op2); // $[TI2.1] $[TI2.2]
      break;
    case (Constraint::LT): // $[TI3]
      result = (op1 < op2); // $[TI3.1] $[TI3.2]
      break;
    case (Constraint::GT): // $[TI4]
      result = (op1 > op2); // $[TI4.1] $[TI4.2]
      break;
    case (Constraint::GE): // $[TI5]
      result = (op1 >= op2); // $[TI5.1]// $[TI5.2]
      break;
    case (Constraint::LE): // $[TI6]
      result = (op1 <= op2); // $[TI6.1] $[TI6.2]
      break;
    case (Constraint::BOOLEAN_OP_NULL): 
    case (Constraint::MAX_NUM_BOOLEAN_OPS):
    default:
      CLASS_PRE_CONDITION((Constraint::BOOLEAN_OP_NULL < boolOp) && 
                          (boolOp < Constraint::MAX_NUM_BOOLEAN_OPS));
      break;
  }
  return(result);     
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateAutoReset()
//
//@ Interface-Description
//   This virtual method should never be called.  It will be defined in the
//   derived classes which require it.
// >Von
//   rChildGroup  Reference to the OperatingGroup which refers to the 
//                ViolationHistoryManager which tracks violations for this
//                Constraint.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
Constraint::evaluateAutoReset(OperatingGroup& rChildGroup) const
{
  CALL_TRACE("evaluateAutoReset(rChildGroup)");

  CLASS_ASSERTION(FALSE);

  return(FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMinNum
//
//@ Interface-Description
//   Return the value of the Constraint minimum returned by executing the
//   method pointed to by attribute pMinCalcFunction_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
Int32
Constraint::getMinNum(void) const
{
  CALL_TRACE("getMinNum(void)");

  Int32 tempInt = 0;
  if (pMinCalcFunction_ != NULL) // $[TI1]
  {
    tempInt = (*pMinCalcFunction_)();
  } // $[TI2]
  return(tempInt);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
Constraint::SoftFault(const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const char*        pFileName,
                      const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::CONSTRAINT, lineNumber,
                          pFileName, pPredicate);
}
