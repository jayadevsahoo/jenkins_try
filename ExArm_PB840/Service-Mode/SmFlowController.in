#ifndef SmFlowController_IN
#define SmFlowController_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: SmFlowController - Flow controller for Service Mode
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmFlowController.inv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		Added getFlowTarget() if development options are allowed
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date: 21-Aug-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isFlowStable(void)
//
//@ Interface-Description
//	This method takes no arguments and returns Boolean for flow
//	stability status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Return flow stability status.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
inline Boolean
SmFlowController::isFlowStable (void) const
{
// $[TI1]
	CALL_TRACE("SmFlowController::isFlowStable (void)");

	return( flowStable_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: gasUsed(void)
//
//@ Interface-Description
//	This method takes no arguments and returns the gas being used
//	by the flow controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Return gas type used.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
inline GasType
SmFlowController::gasUsed (void) const
{
// $[TI1]
	CALL_TRACE("SmFlowController::gasUsed (void)");

	return( gasUsed_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFlowTarget()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

inline Real32
SmFlowController::getFlowTarget(void) const
{
	CALL_TRACE("SmFlowController::getFlowTarget(void)") ;
	
	// $[TI1]
	return (flowTarget_);
}



#ifdef SIGMA_DEVELOPMENT
inline Real32
SmFlowController::getStableFlowHighBound(void)
{
	return (stableFlowHighBound_);
}

inline Real32
SmFlowController::getStableFlowLowBound(void)
{
	return (stableFlowLowBound_);
}

inline void
SmFlowController::resetFlowStabilityParams(void)
{
	stableFlowHighBound_ = flowTarget_;
	stableFlowLowBound_ = flowTarget_;
	flowBecameUnstable_ = FALSE;
	flowInstabilityCounts_ = 0;
}

inline Boolean
SmFlowController::flowBecameUnstable(void)
{
	return (flowBecameUnstable_);
}

inline Int32
SmFlowController::getFlowInstabilityCounts(void)
{
	return (flowInstabilityCounts_);
}

#endif  // SIGMA_DEVELOPMENT


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================z


#endif // SmFlowController_IN 
