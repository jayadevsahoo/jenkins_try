//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#ifndef SWATBATCHUSER_H
#define SWATBATCHUSER_H


#include "ModuleDataIds.hh"

#ifndef __MINMAX_DEFINED

#ifndef max
#define max(a,b)    (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)    (((a) < (b)) ? (a) : (b))
#endif

#endif

#define MAX_BATCH_DEPTH 15


#define SWATBUFFER_BUFFER_NSENSORS         3

#define SWATBUFFER_BUFFER_NSECONDS        60

#define SWATBUFFER_BUFFER_NCYCLES_SECOND  200

#define SWATBUFFER_BUFFER_SIZE  (SWATBUFFER_BUFFER_NSENSORS * SWATBUFFER_BUFFER_NSECONDS * SWATBUFFER_BUFFER_NCYCLES_SECOND)


#define NMEMORY_SLOTS 30


#define VIRTUALSENSOR_BUFFER  max(64, ModuleDataIds::MODULE_DATA_IDS_UNSIGNED_MAX)

#define VIRTUALRAWSENSOR_BUFFER  max(64, ModuleDataIds::MODULE_DATA_IDS_UNSIGNED_MAX)

#define EVENTMANAGER_NEVENTS 128



#endif
