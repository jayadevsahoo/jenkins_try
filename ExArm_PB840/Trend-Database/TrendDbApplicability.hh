#ifndef TrendDbApplicability_HH
#define TrendDbApplicability_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendDbApplicability
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Trend-Database/vcssrc/TrendDbApplicability.hhv   10.7   08/17/07 11:01:46   pvcs  
// 
//@ Modification-Log
//
//  Revision: 003   By: rhj   Date: 25-Oct-2010     SCR Number: 6603
//  Project:  PROX
//  Description:
//      Added the ability to log prox enable and disable events.
// 
//  Revision: 002   By: rpr    Date: 25-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 001  By:  ksg    Date:  17-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//====================================================================

//@ Usage-Classes
#include "SettingId.hh"
#include "PatientDataId.hh"
#include "BreathDatumHandle.hh"
#include "DiscreteValue.hh"
//@ End-Usage

class TrendDbApplicability
{
public:
	TrendDbApplicability(void);
	~TrendDbApplicability(void);

	Boolean isApplicable(SettingId::SettingIdType settingId) const;
	Boolean isApplicable(PatientDataId::PatientItemId patientId, BoundedBreathDatum boundedDatum) const;    
	void updateSettings(void);
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:
	// these methods are purposely declared, but not implemented...
	TrendDbApplicability(const TrendDbApplicability&);				// not implemented...
	void operator=(const TrendDbApplicability&);			// not implemented...
	struct applicabilityData
	{
		Int8 mode;
		Int8 ventilationType;
		Int8 triggerType;
		Int8 mandBreathType;
		Int8 spontBreathType;
	};

	//@ Data-Member: applicabilityPatientArray_
	// Used to store bit vectors containing patient data parameter context restrictions
	applicabilityData applicabilityPatientArray_[PatientDataId::NUM_TOTAL_PATIENT_DATA];

	//@ Data-Member: applicabilitySettingArray_
	// Used to store bit vectors containing setting parameter context restrictions
	applicabilityData applicabilitySettingArray_[SettingId::TOTAL_SETTING_IDS];

	//@ Data-Member: mode_
	// Local copy of current mode setting
	Uint8 mode_;

	//@ Data-Member: ventType_
	// Local copy of current vent type setting
	Uint8 ventType_;

	//@ Data-Member: triggerType_
	// Local copy of current trigger type setting
	Uint8 triggerType_;

	//@ Data-Member: spontBreathType_
	// Local copy of current spont breath type setting
	Uint8 spontBreathType_;

	//@ Data-Member: mandBreathType_
	// Local copy of current mand breath type setting
	Uint8 mandBreathType_;

	//@ Data-Member: fio2Setting_
	// Local copy of current fio2 enabled setting
	Uint8 fio2Setting_;

	//@ Data-Member: leakCompEnabledSetting_
	// Local copy of current Leak Compensation enabled setting
	Uint8 leakCompEnabledSetting_;

	//@ Data-Member: proxSetting_
	// Local copy of current PROX enabled setting
	DiscreteValue proxSetting_;

};
#endif // TrendDbApplicability_HH





