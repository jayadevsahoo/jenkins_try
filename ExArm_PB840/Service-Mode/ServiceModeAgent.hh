#ifndef ServiceModeAgent_HH
#define ServiceModeAgent_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class: ServiceModeAgent - Interface Agent to the Service-Mode data.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ServiceModeAgent.hhv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 007   By:  quf   Date:  16-Oct-1997   DR Number: DCS 2567
//       Project:  Sigma (840)
//       Description:
//			Removed fio2 calibration status.
//
//  Revision: 006   By:  quf   Date:  24-Sep-1997   DR Number: DCS 2410
//       Project:  Sigma (840)
//       Description:
//			Added private data member atmPresXducerCalStatus_ and
//			access method getAtmPresXducerCalStatus(), updated constructors.
//
//  Revision: 005   By:  quf   Date:  18-Sep-1997   DR Number: DCS 1861, 2525
//       Project:  Sigma (840)
//       Description:
//			Added private data member ventInopTestStatus_ and
//			access method getVentInopTestStatus(), updated constructors,
//			eliminated vent inop test Boolean data members and access methods,
//			changed enum type of evCalStatus_.
//
//  Revision: 004   By:  quf   Date:  15-Sep-1997   DR Number: DCS 2385
//       Project:  Sigma (840)
//       Description:
//			Added private data member flowSensorCalStatus_ and
//			access method getFlowSensorCalStatus(), updated constructors.
//
//  Revision: 003  By:  quf    Date:  11-Aug-97    DR Number: DCS 2107
//       Project:  Sigma (840)
//       Description:
//			Due to change from Boolean to 3-state EV cal novram flag,
//          exhValveRequired_ was changed to exhValveCalStatus_,
//          isExhValveCalRequired() was changed to getExhValveCalStatus(),
//          fixed constructor and copy constructor.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================
 

//@ Usage-Classes
#include "ServiceMode.hh"
#include "ServiceModeNovramData.hh"
#include "ResultEntryData.hh"
//@ End-Usage

class ServiceModeAgent
{
  public:
    ServiceModeAgent();
    ~ServiceModeAgent();
    ServiceModeAgent(const ServiceModeAgent& rServiceModeAgent);

    inline Boolean  isFlowSensorInfoRequired(void) const;

	inline ServiceMode::TestStatus
    							getFlowSensorCalStatus( void) const ;

	inline ServiceMode::TestStatus
    							getAtmPresXducerCalStatus( void) const ;

    inline ServiceMode::TestStatus getExhValveCalStatus( void)const;

	inline ServiceMode::TestStatus
    							getVentInopTestStatus( void) const ;

    Boolean  isSstRequired( void) const ;

    inline const ResultEntryData&  getOverallBdEstResult(void) const;
    inline const ResultEntryData&  getOverallBdSstResult(void) const;

    static void     SoftFault(const SoftFaultID softFaultID,
							  const Uint32      lineNumber,
							  const char*       pFileName  = NULL,
							  const char*       pPredicate = NULL);

  private:
    void operator=(const ServiceModeAgent&);  // not implemented...

    //@ Data-Member:    flowSensorInfoRequired_
    //  Flow sensor calibration information required flag
    const Boolean       flowSensorInfoRequired_;

    //@ Data-Member:    flowSensorCalStatus_
    //  Flow sensor offset calibration status flag
	const ServiceMode::TestStatus flowSensorCalStatus_;

    //@ Data-Member:    atmPresXducerCalStatus_
    //  atmospheric pressure transducer calibration status flag
	const ServiceMode::TestStatus atmPresXducerCalStatus_;

    //@ Data-Member:    exhValveCalStatus_
    //  Exhalation valve calibration status flag
    const ServiceMode::TestStatus exhValveCalStatus_;

	//@ Data-Member: ventInopTestStatus_
	// vent inop test status flag
	const ServiceMode::TestStatus ventInopTestStatus_ ;

    //@ Data-Member:       bdEstOverallResult_
    //  BD overall EST result flag
    ResultEntryData  bdEstOverallResult_;
 
    //@ Data-Member:       bdSstOverallResult_
    //  BD overall SST result flag
    ResultEntryData  bdSstOverallResult_;

    //@ Data-Member: isSstRequired_
    // SST required flag
    Boolean isSstRequired_ ;

};


// Inlined methods...
#include "ServiceModeAgent.in"

#endif // ServiceModeAgent_HH
