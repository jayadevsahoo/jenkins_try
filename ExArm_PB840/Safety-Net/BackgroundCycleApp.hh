#ifndef BackgroundCycleApp_HH
#define BackgroundCycleApp_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================ H E A D E R   D E S C R I P T I O N ====
//  Class: BackgroundCycleApp - Background cycle testing application
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundCycleApp.hhv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: rpr   Date:  05-Nov-2010    SCR Number: 6690
//    Project:  PROX
//    Description:
//      Reverted revision 013.  The clock variance test has been removed so the 
//		test of the rtc has been brought back.  
//
//  Revision: 003  By: gdc   Date:  18-Aug-2009    SCR Number: 6147
//    Project:  XB
//    Description:
//      Removed background Time-of-Day clock test. Moved this function to
// 		the cycle-to-cycle and interval time variance tests in BdMonitoredData.
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//
//  Revision: 001    By: by    Date: 03-Dec-1996 DR Number: None
//    Project: Sigma (840)
//    Description:
//       Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "SafetyNet.hh"
#include "BackgroundCycleTimer.hh"

class BackgroundCycleApp
{
    public:

        static void Initialize( void );

        static void Task( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL, 
			       const char*       pPredicate = NULL );

#if defined( SIGMA_BD_CPU )

        enum BkCycleTaskId
        {
            NOVRAM_CHECKSUM_TEST,
            TOD_TEST,
            AUDIBLE_ALARM_CAPACITOR_TEST,
            AUDIBLE_ALARM_DISCONNECT_TEST,
            ADC_TEST,
            AC_SWITCH_POSITION_TEST,
            BATTERY_TEST,

            MAX_BK_CYCLE_TASKS
        };

#elif defined( SIGMA_GUI_CPU )

        enum BkCycleTaskId
        {
            NOVRAM_CHECKSUM_TEST,
            TOD_TEST,
            KEYBOARD_TEST,

            MAX_BK_CYCLE_TASKS
        };

#endif // elif defined( SIGMA_GUI_CPU ) 

    private:

        BackgroundCycleApp( void );              
        ~BackgroundCycleApp( void );             

        static BackgroundCycleTimer*
        PCycleTimers_[ BackgroundCycleApp::MAX_BK_CYCLE_TASKS ];

};


#endif    // #ifndef BackgroundCycleApp_HH
