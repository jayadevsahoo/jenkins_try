//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIValue.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "PatientDataUIValue.h"

//-------------------------------------
// C O D E
//-------------------------------------

Value::Value(char *name):m_pszName(name)
{
}

char *Value::getName()
{
    return m_pszName;
}

bool Value::invalidate()
{
    m_bIsSet    =false;
    return true;
}

bool Value::unsetValue()
{
    m_bIsDesired=false;
    return true;
}

bool Value::isValid()
{
    return m_bIsSet;
}

bool Value::isEnum()
{
    return false;
}

bool Value::isFloat()
{
    return false;
}

bool Value::isInt()
{
    return false;
}

bool Value::isWaveform()
{
    return false;
}

bool Value::isSinSignal()
{
    return false;
}

bool Value::isTrigger()
{
    return false;
}


bool Value::setFloat(float fValue)
{
    return false;
}

bool Value::setInt  (int   iValue)
{
    return false;
}

bool Value::setEnum (unsigned int eValue)
{
    return false;
}

bool Value::setSinSignal(double dFrequency,double dPhase,double dMin,double dMax)
{
    return false;
}


bool Value::setTriggerSignal(double dFrequency,double dPhase,double dMin,double dMax, double trigger_high, double trigger_low)
{
    return false;
} 

bool Value::setIsSetFlag()
{
    m_bIsSet=true;
    return true;
}

char *Value::getEnumValue()
{
    return "";
}          

float Value::getFloatValue()
{
    return 0.0; 
}


int   Value::getIntValue()
{
    return 0;
}         

char * Value::getEnum(int cnt)
{
    return (char *)0;
}

double Value::getFrequency()
{
    return 0.0;
}         

double Value::getPhase(){
    return 0.0;
}         

double Value::getMax()
{
    return 0.0;
}

double Value::getMin()
{
    return 0.0;
}

double Value::getTriggerHigh()
{
    return 0.0; 
}

double Value::getTriggerLow ()
{
    return 0.0; 
}

