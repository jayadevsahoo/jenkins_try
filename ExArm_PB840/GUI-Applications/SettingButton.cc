#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SettingButton -  A setting button which interfaces to the
// Settings sub-system and allows a setting to be changed by the
// operator via the Adjust Panel.
//---------------------------------------------------------------------
//@ Interface-Description
// SettingButton contains all the functionality for tying a button to a
// setting, allowing that setting to be adjusted/cleared/accepted by the Adjust
// Panel.  SettingButton is never instantiated directly.  Only derived classes
// will be created.
//
// SettingButton is derived from a) SubScreenButton - allowing a setting button
// to select a subscreen (as in the case of Main Settings), b) SettingObserver
// - for receiving updates to the current setting, and c) AdjustPanelTarget -
// for receiving knob clicks and key presses from the adjust panel.
//
// The usePersistentFocus() method needs to be called for all setting buttons
// that are used on the Upper screen, it insulates them from events that might
// clear the normal focus on the Lower screen.
//
// Some settings are not clearable by the user and the makeUnclearable() method
// is used to inform the setting button of this fact.  The setFocusSubScreen()
// method is used to specify the subscreen which receives the Accept key events
// when this button is active.
//
// The activate() method should always be called just before the setting button
// is displayed.  It makes sure that the button will display the current
// setting value (note that setting buttons ignore settings changes when they
// are invisible, hence they may get out of sync with their settings).
//
// You can query the button for the id of the setting it adjusts with
// getId().
//
// Setting buttons which are part of the Main Settings Area (called Main
// Setting buttons) are required to timeout after a certain time if they are
// pressed down and there has been no user activity that during time.  We use a
// timer, the Main Setting Change timer, to do this.  It is started when this
// button is pressed down and reset in the main GUI event loop every time user
// activity of any kind is detected.  Timeout events are announced via the
// timerEventHappened() method.
//
// Changes to settings in the Settings-Validation subsystem are communicated
// via the activate() method.  This method then looks after
// updating the button's display if needed.
//
// Classes which derive from this class must define the updateDisplay_()
// method.  This method is called when the button's display must be updated.
// This method is the main difference between all derived SettingButton
// classes.
//---------------------------------------------------------------------
//@ Rationale
// There are numerous types of setting buttons in the Sigma GUI application
// (currently they are :- DiscreteSettingButton, NumericSettingButton,
// AlarmLimitSettingButton, IeRatioSettingButton, TimingSettingButton and
// IbwSettingButton).  Since the method in which these buttons interface to the
// settings sub-system is the same for each, with the only difference being in
// the way which each button displays their setting, it was necessary to take
// this common functionality and place it in SettingButton.
//---------------------------------------------------------------------
//@ Implementation-Description
// The interface from the button to its setting is implemented through a
// SettingObserver (a class provided by the Settings-Validation
// sub-system) which is created with a SettingId::SettingIdType (an enumerated
// type, with each setting in the Settings-Validation sub-system having a
// unique enum).  The SettingObserver provides all the methods needed to
// interface directly to a setting and it is used extensively in SettingButton
// to pass Adjust Panel events to the setting and, in derived classes, to
// obtain current display states and values.
//
// In order to keep coding simple and located in one place SettingButton
// contains additional methods and members to facilitate some external
// requirements.  For example, when a SettingButton is pressed up by the
// operator it loses the Adjust Panel focus as expected.  The problem is that
// now Accept key events have nowhere to go when they should be going to the
// current subscreen.  To solve this problem, SettingButton stores a pointer to
// the "focus" subscreen so that when SettingButton is pressed up Adjust Panel
// focus is passed automatically to the appropriate subscreen.  Also, all
// Accept key presses received by this button are immediately sent to this
// subscreen for handling.
//
// There is special handling for Main Settings to deal with the fact that these
// type of buttons are sometimes tied to a setting's current value and
// sometimes to a setting's adjusted value.  This handling is performed in
// activate().
//
// $[01035] $[01053]
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Cannot be instantiated directly, must be inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SettingButton.ccv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 024   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
//
//  Revision: 023   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 022   By: gdc    Date:  09-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Modified to support single setting accept and getTargetType.
//
//  Revision: 021   By: erm    Date:  03-Dec-2001    DCS Number: 5902
//  Project: GuicComms
//  Description:
//      Modified timerEvent to allow event handling only if SubScreen is visible
//
//  Revision: 020  By: sah     Date:  21-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  moved activating user-event callaback to AFTER
//         'SubScreenButton::downHappened_()' call, to help support new
//         tracking settings
//      *  to support new tracking settings, removed any showing of viewable
//         setting buttons (see 'DropDownSettingButton' for handling a viewable
//         state as flat, but still visible)
//
//  Revision: 019   By: hhd    Date:  01-May-2000    DCS Number: 5710
//  Project:  NeoMode
//  Description:
//		Modified to cancel the Main Setting Inactivity Timer if a Main Setting
//      button is unselected.
//	
//  Revision: 018   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified constructor to take arguments for auxillary title text
//      and position, and forward on to SubScreenButton base class.  This
//      functionality will be used to plug-in the "above PEEP" phrases
//      to the "Pi" and "Psupp" setting buttons.
//
//  Revision: 017  By:  sah	   Date:  21-Mar-2000    DCS Number: 5656
//  Project:  NeoMode
//  Description:
//      Removed obsoleted requirement.
//
//  Revision: 016  By:  hhd	   Date:  07-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//      Added batch change capability to the MainSettingsArea, whereby multiple
//      settings can be changed and accepted as a group.
//
//  Revision: 015   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//      Optimized graphics library. Implemented Direct Draw mode for drawing
//       outside of BaseContainer refresh cycle.
//       Tie main setting buttons with BreathTiming subscreen through callback
//       pointer to perform batch processing.
//
//	17-Nov-1999 (hhd)	- Modified to reduce header file dependencies.
//
//  Revision: 014  By: hhd    Date: 13-Aug-99    DCS Number: 5411
//  Project:  ATC
//  Description:
//		Implemented a mechanism to freeze the blinking of the attention icons 
//      when the buttons which they are on are touched.
//
//  Revision: 013  By: hhd    Date: 17-Jun-99    DCS Number: 5439
//  Project:  ATC
//  Description:
//      Invoked activate() in upHappened() only when the button is a
//      MainSetting's button.
//
//  Revision: 012  By: hhd    Date: 14-Jun-99    DCS Number: 5415
//  Project:  ATC
//  Description:
//      Fixed problem in button states when selecting the two buttons High Vt
//      Mand and High Vt Spont sequentially.
//  
//  Revision: 011  By: sah    Date: 10-Jun-99    DCS Number: 5406
//  Project:  ATC
//  Description:
//      Fixed problem when high soft-bound is at same spot as minimum
//      hard bound, and the soft-bound button and propmpt are not removed
//      properly.
//
//  Revision: 010  By: hhd    Date: 07-June-1999  DCS Number: 5423 
//  Project:  ATC
//  Description:
//		Used ARROW_ICON instead for setting verification.
//
//  Revision: 009  By: hhd    Date: 05-May-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//		Fixed problem of icons not shown in New Patient Setup screen.
//	  
//  Revision: 008  By: sah    Date: 29-Apr-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//		Added ability to turn on and off new verify-needed icon, for use
//      with ultra-important settings.
//
//  Revision: 007  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 006  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 004  By:  yyy    Date:  01-Sep-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//             Initial release.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/SettingButton.ccv   1.13.1.0   07/30/98 10:21:58   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SettingButton.hh"

#include "GuiTimerRegistrar.hh"
#include "LowerScreen.hh"


//@ Usage-Classes
#include "AdjustPanel.hh"
#include "AdjustPanelTarget.hh"
#include "BoundStatus.hh"
#include "BoundStrs.hh"
#include "Image.hh"
#include "LowerScreen.hh"
#include "LowerSubScreenArea.hh"
#include "MessageArea.hh"
#include "PromptArea.hh"
#include "SettingContextHandle.hh"
#include "SettingSubject.hh"
#include "Sound.hh"
#include "SubScreen.hh"
//@ End-Usage

//@ Code...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SettingButton
//
//@ Interface-Description
// As normal with a button, SettingButton is first passed the dimensions, type,
// bevel size, corner type and border type of the button.  It is then passed
// the extra parameters for being a SubScreenButton, i.e.  the titleText of the
// button (displayed within the button's label container) and the subscreen
// which is activated when this button is pressed (NULL if none).  The
// parameters particular to SettingButton are as follows:
// >Von
//	settingId		an enumerated type identifying the setting to which this
//					button is tied.  Can be NULL_SETTING_ID if this button
//					is not going to be used as a SettingButton (feature used
//					by AlarmSlider).
//	pFocusSubScreen	the subscreen which gains the Adjust Panel focus after
//					this button is pressed up by the operator.
//	messageId		the help message displayed in the Message Area when
//					this button is pressed down by the operator.
//	isMainSetting	identifies this button to be a Main Setting button.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Passes any required parameters to the parent class and
// initializes all data members.  Then we register for changes to
// the setting.
//
// $[01039] A select setting button must display a white background
//---------------------------------------------------------------------
//@ PreCondition
// The button type cannot be LIGHT_NON_LIT 'cos background of this button
// must be white when selected.
// Button type must be DARK if it's a main setting 'cos main setting buttons
// must be distinguishable from batch setting buttons.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SettingButton::SettingButton(Uint16 xOrg, Uint16 yOrg, Uint16 width,
							 Uint16 height, Button::ButtonType buttonType,
							 Uint8 bevelSize, Button::CornerType cornerType,
							 Button::BorderType borderType, StringId titleText,
							 SettingId::SettingIdType settingId,
							 SubScreen *pFocusSubScreen, StringId messageId,
							 Boolean isMainSetting, StringId auxTitleText,
							 Gravity auxTitlePos) :
SubScreenButton         (xOrg, yOrg, width, height, buttonType,
						 bevelSize, cornerType, borderType, titleText, 
						 NULL, auxTitleText, auxTitlePos),
SUBJECT_ID_             (settingId),
activityState_          (SettingButton::NO_ACTIVITY),
pFocusSubScreen_        (pFocusSubScreen),
IS_MAIN_SETTING_        (isMainSetting),
IS_NON_BATCH_SETTING_   (SettingId::IsNonBatchId(settingId)),
isVerifyNeeded_         (FALSE),
verifyBitmap_           (Image::RCheckArrowIcon),
usePersistentFocus_     (FALSE),
isClearable_            (TRUE),
messageId_              (messageId),
losingFocus_            (FALSE),
isPressedDown_          (FALSE),
softBoundHappened_      (FALSE),
isSingleSettingAcceptEnabled_(FALSE),
showViewable_           (FALSE)
{
	CLASS_PRE_CONDITION(buttonType != Button::LIGHT_NON_LIT);
	CLASS_PRE_CONDITION(!isMainSetting || (buttonType == Button::DARK));
	// $[TI1]

	// visible, and blinking, only in special cases (see
	// 'applicabilityUpdate()' method)...
	verifyBitmap_.setX(3);
	verifyBitmap_.setY(3);
	verifyBitmap_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
	verifyBitmap_.setShow(FALSE);
	//addDrawable(&verifyBitmap_);  added as required
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SettingButton  [Destructor]
//
//@ Interface-Description
// Called automatically on destruction of an instance of this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SettingButton::~SettingButton(void)
{
	CALL_TRACE("SettingButton::~SettingButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  This method is to be called when a subscreen that MAY need this button
//  displayed, has been activated.  This mechanism will determine whether
//  it is appropriate for this setting button to be shown at this time.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	Setting Id is specified (non-null).
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::activate(void)
{
	CALL_TRACE("activate()");

	CLASS_PRE_CONDITION((SUBJECT_ID_ != SettingId::NULL_SETTING_ID));

	activityState_ = SettingButton::ACTIVATION_IN_PROGRESS;

	attachToSubject_(SUBJECT_ID_, Notification::APPLICABILITY_CHANGED);
	attachToSubject_(SUBJECT_ID_, Notification::VALUE_CHANGED);

	if (isVerifyNeeded_)
	{
		if (getSubjectPtr_(SUBJECT_ID_)->isChanged())
		{
			verifyBitmap_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
			verifyBitmap_.setShow(TRUE);
		}
		else
		{
			verifyBitmap_.setColor(Colors::BLACK);
			verifyBitmap_.setShow(FALSE);
		}
	}
	
	// notify this setting button's subject, that it is now active...
	if ( IS_MAIN_SETTING_  ||  IS_NON_BATCH_SETTING_ )
	{													// $[TI1]
		// button is a main setting button, or it monitors a non-batch
		// setting...
		activateSubject_(SUBJECT_ID_, Notification::ACCEPTED);
	}
	else
	{													// $[TI2]
		activateSubject_(SUBJECT_ID_, Notification::ADJUSTED);
	}

	if ( IS_MAIN_SETTING_ && getButtonType() != Button::DARK )
	{  // $[TI3]
		setButtonType(Button::DARK);
	}  // $[TI4]

	activityState_ = SettingButton::NO_ACTIVITY;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  This method is to be called when a subscreen that activated this setting
//  button, is being deactivated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First, detach the object itself from the SettingSubject, then reset
//  the show flag and activity state.  If softbound happened prior to
//  deactivation of the subscreen, reset primary prompt, hide the override
//  button then reset softbound flag.
//---------------------------------------------------------------------
//@ PreCondition
//	Setting Id is specified (non-null).
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::deactivate(void)
{
	CALL_TRACE("deactivate()");

	activityState_ = SettingButton::DEACTIVATION_IN_PROGRESS;

	CLASS_PRE_CONDITION((SUBJECT_ID_ != SettingId::NULL_SETTING_ID));

	detachFromSubject_(SUBJECT_ID_, Notification::APPLICABILITY_CHANGED);
	detachFromSubject_(SUBJECT_ID_, Notification::VALUE_CHANGED);

	if ( softBoundHappened_ )
	{  // $[TI1] -- the last bound violation was a "soft" violation...
		// clear prompts and hide override button.
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_HIGH, ::NULL_STRING_ID);
		GuiApp::PPromptArea->hideOverrideButton();

		softBoundHappened_ = FALSE;
	}  // $[TI2] -- the last bound violation was a "hard" violation...

	setShow(FALSE);

	activityState_ = SettingButton::NO_ACTIVITY;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the Main Setting Change timer that we started runs out as a
// consequence of there being no user activity on the GUI.  Passed the id of
// the timer.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must deactivate our associated subscreen and pop ourselves up.
//
// There is a possibility that the event occurs after this button has been
// set to the up position.  In this case we ignore the event.
//
// $[01074] Main setting button is deselected after 2 minutes ...
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the Screen Inactivity Timer id.
// We must be a Main Setting button.
//--------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("SettingButton::timerEventHappened("
			   "GuiTimerId::GuiTimerIdType timerId)");

	CLASS_PRE_CONDITION(timerId == GuiTimerId::MAIN_SETTING_CHANGE_TIMEOUT);

	// The SubScreen must be visible to handle this event
	if ( ! pFocusSubScreen_->isVisible() )
		return;

	// If we're down then set ourselves up by deactivating our subscreen
	if ( getState_() == DOWN || IS_MAIN_SETTING_ )
	{													// $[TI1]
		if ( IS_MAIN_SETTING_ )
		{	// $[TI1.1]
			CLASS_ASSERTION(pUserEventCallBack_ != NULL);
			(*pUserEventCallBack_)(getSettingId(), TIMER, FALSE);
		}	// $[TI1.2]

		getSubScreen_()->getSubScreenArea()->deactivateSubScreen();

		// Release Adjust Panel focus
		if ( usePersistentFocus_ )
		{												// $[TI2]
			AdjustPanel::TakePersistentFocus(NULL);
		}
		else
		{												// $[TI3]
			AdjustPanel::TakeNonPersistentFocus(NULL);
		}
	}													// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Protected, Pure virtual]
//
//@ Interface-Description
// Called when the display of this setting button needs to be updated with the
// latest setting value.  Passed a setting context id informing us the setting
// context from which we should retrieve the new setting value.  Derived
// classes define this method and in it place the class-specific setting
// display code.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downHappened_
//
//@ Interface-Description
// This is a virtual method inherited from being a Button.  It is called when
// this button is sent into the Down state, either by the operator pressing us
// down (byOperatorAction = TRUE) or by the application calling the method
// setToDown() (byOperatorAction = FALSE).
//---------------------------------------------------------------------
//@ Implementation-Description
// For main setting buttons, inform the interested subscreen of the
// button DOWN being pressed, so that subscreen can react accordingly.
// We pass the event onto the inherited SubScreenButton allowing it to do
// its stuff first.  We then grab the Adjust Panel focus.  If it was the
// operator that pressed us down, we inform the setting that it is has been
// activated.  If we have an associated message then this must be sent to the
// Message Area.  We also display a "Use knob to adjust" prompt.
//
// If we're a Main Setting button then we should start the Main Setting
// Change timer.
//
// $[01037] To adjust a setting its setting button should be selected.
// $[01040] On selection, instructions on what to do next should appear in
//			the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//	Setting Id is specified (non-NULL)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::downHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("SettingButton::downHappened_(Boolean byOperatorAction)");

	CLASS_PRE_CONDITION(SUBJECT_ID_ != SettingId::NULL_SETTING_ID);

	// Second, let SubScreenButton do its stuff.
	SubScreenButton::downHappened_(byOperatorAction);

	// stop the blinking arrow - operator has acknowledged setting
	if (isVerifyNeeded_)
	{
		verifyBitmap_.setColor(Colors::BLACK);
	}

	if ( IS_MAIN_SETTING_ )
	{	// $[TI3]
		CLASS_ASSERTION(pUserEventCallBack_ != NULL);

		// this must be done AFTER 'SubScreenButton::downHappened_()'...
		(*pUserEventCallBack_)(getSettingId(), DOWN, byOperatorAction);

		// start the Main Setting Change timer...
		GuiTimerRegistrar::StartTimer(GuiTimerId::MAIN_SETTING_CHANGE_TIMEOUT,
									  this);
	}	// $[TI4]

	//
	// Tell the setting that it is now the active one.  This must
	// happen *after* setting up the adjusted context.  Otherwise,
	// the context setup blows away the fact that the activation
	// occurred.
	//
	getSubjectPtr_(SUBJECT_ID_)->activationHappened();

	// Grab the Adjust Panel focus for the button and the associated
	// setting subscreen.
	if ( usePersistentFocus_ )
	{													// $[TI1]
		AdjustPanel::TakePersistentFocus(this);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
	}
	else
	{													// $[TI2]
		AdjustPanel::TakeNonPersistentFocus(this);
	}

	//
	// Send help message, if any, to the Message Area.
	// $[01006] All shorthand symbols in setting buttons must display
	// 			their meaning in the Message Area ...
	//
	if ( messageId_ != NULL_STRING_ID )
	{													// $[TI7]
		GuiApp::PMessageArea->setMessage(messageId_, MessageArea::MA_MEDIUM);
	}													// $[TI8]

	// Send "Use knob to adjust" message to Prompt Area
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
								   PromptStrs::USE_KNOB_TO_ADJUST_P);

	isPressedDown_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: upHappened_
//
//@ Interface-Description
// This is a virtual method inherited from being a Button.  It is called when
// this button is sent into the Up state, either by the operator pressing us
// down (byOperatorAction = TRUE) or by the application calling the method
// setToUp() (byOperatorAction = FALSE).
//---------------------------------------------------------------------
//@ Implementation-Description
// For main setting buttons, inform the interested subscreen of the
// button UP being pressed, so that subscreen can react accordingly.
// If the button was pressed up by the operator then we release
// the Adjust Panel focus.  We also update our display to make sure
// the correct setting value is displayed.  We do none of the above if
// we were pressed up due to losing the focus.  
//
// If this button had an associated message then we must clear the Message
// Area.  The Prompt Area Confirmation prompt must be cleared also, if one was
// displayed.
//
// $[01042] Deselection of a batch button will not affect its adjusted value.
// $[01260] The GUI ignores the knob when not adjusting setting.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::upHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("SettingButton::upHappened_(Boolean byOperatorAction)");

	Boolean newAdjustPanelTarget = FALSE;

	if ( IS_MAIN_SETTING_ )
	{	// $[TI12.1]
		CLASS_ASSERTION(pUserEventCallBack_ != NULL);

		newAdjustPanelTarget = (*pUserEventCallBack_)(getSettingId(), UP, byOperatorAction);
		GuiTimerRegistrar::CancelTimer(GuiTimerId::MAIN_SETTING_CHANGE_TIMEOUT);
	}	// $[TI12.2]

	// Only clear the prompt area and message areas if we have 
	// been pressed down previously. If we're set up but do not
	// have focus, then don't mess with the message and prompt areas.
	if ( isPressedDown_ && !IS_MAIN_SETTING_ )
	{													// $[TI1]
		// Clear the Message Area
		GuiApp::PMessageArea->clearMessage(MessageArea::MA_MEDIUM);

		// Clear Primary, Advisory and Secondary prompts
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, 
									   PromptArea::PA_HIGH, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									   PromptArea::PA_HIGH, NULL_STRING_ID);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									   PromptArea::PA_HIGH, NULL_STRING_ID);
		isPressedDown_ = FALSE;
	}													// $[TI2]

	// our associated subscreen if we are losing focus as well as if
	// the operator presses this button up.
	if ( IS_MAIN_SETTING_ )
	{													// $[TI3.1]
		(LowerScreen::RLowerScreen.getLowerSubScreenArea())->resetCurrentSubScreenButton();

		// remove any auxillary title text that may be shown (this is normally
		// done via 'SubScreenButton::upHappened_()', which is not called for
		// Main Setting buttons)...
		setAuxTitleShowFlag_(FALSE);

		isPressedDown_ = FALSE;
	}
	else
	{													// $[TI3.2]
		SubScreenButton::upHappened_(byOperatorAction || losingFocus_);
	}

	// If we're invisible, do nothing else
	if ( !isVisible() )
	{													// $[TI5]
		return;
	}													// $[TI6]

	// Check to see if we're not in the process of losing Adjust Panel focus
	if ( !losingFocus_ && byOperatorAction )
	{													// $[TI7]
		// Release the Adjust Panel focus if the operator pressed us up
		if ( usePersistentFocus_ )
		{												// $[TI9]
			AdjustPanel::TakePersistentFocus(NULL);
		}
		else
		{												// $[TI10]
			if ( !newAdjustPanelTarget )
			{											// $[TI10.1]
				// Release adjust panel focus for all main setting
				// buttons.
				AdjustPanel::TakeNonPersistentFocus(NULL);
			}											// $[TI10.2]
			// This is when the main setting button goes to
			// batch setting mode and being deselected.  We
			// do not want to release the focus.
		}
	}													// $[TI11]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelKnobDeltaHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  If this
// button has the Adjust Panel focus then we will be called when the
// operator turns the knob.  It is passed an integer corresponding to the
// amount the knob was turned.  We inform the Settings-Validation subsystem
// of the delta value and any resultant hard bound violation is displayed
// in the Prompt Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// For main setting buttons, inform the interested subscreen of the
// KNOB event happened, so that subscreen can react accordingly.
// Any deltas received by this button are sent immediately to the setting via
// the calcNewValue() method of getSubjectPtr_()->.  If the delta causes this or
// any other setting to change then all updates will be passed by the
// Settings-Validation subsystem to the appropriate objects via the
// SettingObserver before the calcNewValue() method returns.  The return
// value of calcNewValue() indicates the current hard bound status of the
// setting.  If there is a "hard bound violation" we will display an Advisory
// message in the Prompt Area.
//
// We must make the Invalid Entry sound if a hard bound was struck.
//
// $[01037] The rotary knob is used to adjust a setting
// $[01052] Display hard bound prompt and beep if ...
// $[01259] If the user turns the knob the setting shall change by an amount ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::adjustPanelKnobDeltaHappened(Int32 delta)
{
	CALL_TRACE("SettingButton::adjustPanelKnobDeltaHappened(Int32 delta)");

	// Pass the knob delta on to the setting
	const BoundStatus&  rBoundStatus =
	getSubjectPtr_(SUBJECT_ID_)->calcNewValue(delta);

	if ( IS_MAIN_SETTING_ )
	{	// $[TI3]
		CLASS_ASSERTION(pUserEventCallBack_ != NULL);
		(*pUserEventCallBack_)(getSettingId(), KNOB, getSubjectPtr_(SUBJECT_ID_)->isChanged());
	}	// $[TI4]

	const SettingBoundId  SETTING_BOUND_ID = rBoundStatus.getBoundId();

	if ( SETTING_BOUND_ID != ::NULL_SETTING_BOUND_ID )
	{  // $[TI1] -- setting bound violation occurred...
		// get the string id appropriate to the bound id...
		const StringId  BOUND_MSG =
		BoundStrs::GetSettingBoundString(SETTING_BOUND_ID);

		// Display the Advisory message corresponding to the bound id...
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									   PromptArea::PA_HIGH, BOUND_MSG);

		// is the violated bound a "soft" bound...
		softBoundHappened_ = rBoundStatus.isSoftBound();

		if ( softBoundHappened_ )
		{  // $[TI1.1] -- the violated bound is a "soft" bound...
			// $[BL01025] The remind sound shall be produced whenever ...
			// $[BL01019] The GUI must provide a distinctive audible signal ...
			Sound::Start(Sound::REMIND);

			// $[BL01019] ... display a prompt ...
			// Display the Primary message to prompt user how to override soft
			// bound value.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										   PromptArea::PA_HIGH,
										   PromptStrs::TOUCH_TO_CONTINUE_CHANGE_P);

			SettingSubject*  pViolatedSubject =
			getSubjectPtr_(rBoundStatus.getViolatedSettingId());

			// Secondary message stays the same.
			GuiApp::PPromptArea->showOverrideButton(pViolatedSubject,
													SETTING_BOUND_ID);
		}
		else
		{  // $[TI1.2] -- the violated bound is a "hard" bound...
			// make sure soft-bound primary prompt is cleared...
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										   PromptArea::PA_HIGH,
										   PromptStrs::USE_KNOB_TO_ADJUST_P);

			// ensure soft-bound override button is hidden...
			GuiApp::PPromptArea->hideOverrideButton();

			Sound::Start(Sound::INVALID_ENTRY);
		}
	}
	else
	{  // $[TI2] -- no setting bound violation occurred...
		// clear the advisory prompt area...
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									   PromptArea::PA_HIGH, NULL_STRING_ID);

		if ( softBoundHappened_ )
		{  // $[TI2.1] -- the last bound violation was a "soft" violation...
			// Restore prompts and hide override button.
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										   PromptArea::PA_HIGH,
										   PromptStrs::USE_KNOB_TO_ADJUST_P);

			// Hide button.
			GuiApp::PPromptArea->hideOverrideButton();

			softBoundHappened_ = FALSE;
		}  // $[TI2.2] -- the last bound violation was a "hard" violation...
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses down the Accept key to accept this setting
// or a batch of settings.  If we have a focus subscreen then the event is
// passed on to it otherwise we sound the Invalid Entry tone.
//---------------------------------------------------------------------
//@ Implementation-Description
// For main setting buttons, inform the interested subscreen of the
// ACCEPT event happened, so that subscreen can react accordingly.
// All Accept key events are immediately sent to the "focus" subscreen, if
// specified, via its acceptHappened() method and all handling is done there.
// If no focus is specified then the Accept key event is invalid so
// we make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("SettingButton::adjustPanelAcceptPressHappened(void)");


	if ( IS_MAIN_SETTING_ )
	{	// $[TI3.1]
		CLASS_ASSERTION(pUserEventCallBack_ != NULL);

		(*pUserEventCallBack_)(getSettingId(), ACCEPT_KEY, TRUE);
	}	// $[TI3.2]

	// Pass on Accept key event to "focus" subscreen, if specified
	if ( pFocusSubScreen_ && ((!IS_NON_BATCH_SETTING_) || getSubjectPtr_(SUBJECT_ID_)->isChanged()) )
	{											 // $[TI2.1]
		pFocusSubScreen_->acceptHappened();
	}
	else if ( isSingleSettingAcceptEnabled_ )
	{
		setToUp();

		// the upHappened_ callback does not release focus so do it here
		CLASS_ASSERTION(usePersistentFocus_);
		AdjustPanel::TakePersistentFocus(NULL);

		// We chose to not use ACCEPT sound and use it for ACCEPT batch only.
		// If we want the accept sound for single setting accept, uncomment
		// the following line.
		//Sound::Start(Sound::ACCEPT);
	}
	else
	{											 // $[TI1.2]
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses the Clear key to reset the setting to its
// previous value (the value the setting had when the operator last pressed
// this button down).
//---------------------------------------------------------------------
//@ Implementation-Description
// For main setting buttons, inform the interested subscreen of the
// CLEAR event happened, so that subscreen can react accordingly.
// We simply need to inform the setting via our setting handle to go back to
// its previous value.  Any updates will occur automatically via the
// SettingObserver.
//
// Some setting buttons are unclearable however.  In this case we just
// make the Invalid Entry sound.
//
// $[01256] If a setting button is selected and the user presses Clear then ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("SettingButton::adjustPanelClearPressHappened(void)");

	// Check to see if we're clearable
	if ( isClearable_ )
	{													// $[TI1]
		// Clear the setting by setting it to its previous value
		getSubjectPtr_(SUBJECT_ID_)->resetToStoredValue();

		// Clear Advisory prompts in case the upper or lower limits
		// had been reached.
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									   PromptArea::PA_HIGH, NULL_STRING_ID);
		if ( IS_MAIN_SETTING_ )
		{	 // $[TI1.1]
			CLASS_ASSERTION(pUserEventCallBack_ != NULL);
			(*pUserEventCallBack_)(getSettingId(), CLEAR_KEY, TRUE);
		}	 // $[TI1.2]

		if ( softBoundHappened_ )
		{  // $[TI1.3] -- the last bound violation was a "soft" violation...
			// Make sure soft-bound primary prompt is cleared...
			GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
										   PromptArea::PA_HIGH,
										   PromptStrs::USE_KNOB_TO_ADJUST_P);

			// Reset flag and  hide override button.
			GuiApp::PPromptArea->hideOverrideButton();
			softBoundHappened_ = FALSE;
		}  // $[TI1.4] 
	}
	else
	{													// $[TI2]
		// Invalid clear event so make the Invalid Entry sound
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelLoseFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is called when this button is about to lose the focus of
// the Adjust Panel.  This normally happens when some other button is
// pressed down or when this button is pressed up.  We must make sure
// this button pops up.
//---------------------------------------------------------------------
//@ Implementation-Description
// When we lose the focus we simply set this button to the Up state,
// if not already up.
//
// We use the private losingFocus_ to indicate why this button is
// being set up.  The setToUp() method ends up calling the
// upHappened_() method below.  The upHappened_() method normally clears
// the Adjust Panel focus because when a setting button is pressed
// up it releases the Adjust Panel focus.  In this case, however,
// the setting button is being set to up due to, say, another button
// being set down.  If we clear the Adjust Panel focus then the button
// which grabbed the focus will lose it.  upHappened_() will be called
// in this case with byOperatorAction set to FALSE. If byOperatorAction
// is FALSE, the adjust panel focus is not cleared.
//
// $[01030] Button in same selection group should be deselected on selection
//			of another button in the same group
// $[01036] All setting buttons belong to a single, global selection group.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::adjustPanelLoseFocusHappened(void)
{
	CALL_TRACE("SettingButton::adjustPanelLoseFocusHappened(void)");

	losingFocus_ = TRUE;

	setToUp();
	losingFocus_ = FALSE;

	if ( softBoundHappened_ )
	{  // $[TI1] -- the last bound violation was a "soft" violation...
		// hide override button.
		GuiApp::PPromptArea->hideOverrideButton();

		softBoundHappened_ = FALSE;
	}  // $[TI2] -- the last bound violation was a "hard" violation...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: applicabilityUpdate
//
//@ Interface-Description
//  Called when applicability of the setting ids have changed.
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update the subscreen according to the applicability of the setting ids
//  on this subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//  id of subject passed in is valid. 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::applicabilityUpdate(
								  const Notification::ChangeQualifier qualifierId,
								  const SettingSubject*               pSubject
								  )
{
	CALL_TRACE("applicabilityUpdate(qualifierId, pSubject)");

	SAFE_AUX_CLASS_PRE_CONDITION((SUBJECT_ID_ == pSubject->getId()),
								 ((SUBJECT_ID_ << 16) | pSubject->getId()));

	if (isVerifyNeeded_)
	{
		if (getSubjectPtr_(SUBJECT_ID_)->isChanged())
		{
			verifyBitmap_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
			verifyBitmap_.setShow(TRUE);
		}
		else
		{
			verifyBitmap_.setColor(Colors::BLACK);
			verifyBitmap_.setShow(FALSE);
		}
	}

	Boolean  noProcessingNeeded;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :			 // $[TI1]
			// no processing needed if this is for a non-main, non-batch setting...
			noProcessingNeeded = (!IS_MAIN_SETTING_  &&  !IS_NON_BATCH_SETTING_);
			break;
		case Notification::ADJUSTED :			 // $[TI2]
			// no processing needed if this is for a main or non-batch setting...
			noProcessingNeeded = (IS_MAIN_SETTING_  ||  IS_NON_BATCH_SETTING_);
			break;
		default :
			// unexpected qualifier...
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	if ( noProcessingNeeded )
	{  // $[TI3]
		return;
	}  // $[TI4]

	const Applicability::Id  NEW_APPLICABILITY =
	pSubject->getApplicability(qualifierId);

	switch ( NEW_APPLICABILITY )
	{
		case Applicability::CHANGEABLE :					// $[TI5]
			if ( !IS_MAIN_SETTING_ )
			{  // $[TI5.1] -- this button is NOT a main setting button...
				setToUp();
			}  // $[TI5.2] -- leave state control to Main Settings Area...
			setShow(TRUE);
			break;
		case Applicability::VIEWABLE :
			if ( showViewable_ )
			{
				setToFlat();
				setShow(TRUE);
			}
			else
			{
				setShow(FALSE);
			}
			break;
		case Applicability::INAPPLICABLE :					// $[TI6]
			setShow(FALSE);
			break;
		default :
			// unexpected applicability...
			AUX_CLASS_ASSERTION_FAILURE(NEW_APPLICABILITY);
			break;
	}

	if ( getShow() )
	{			// $[TI8]
		// Update our display according to the new setting value.
		updateDisplay_(qualifierId, pSubject);
	}		   // $[TI9]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//  Called when setting values on this subscreen has changed.
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update display according to new setting values.
//---------------------------------------------------------------------
//@ PreCondition
//  id of subject passed in is valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::valueUpdate(const Notification::ChangeQualifier qualifierId,
						   const SettingSubject*               pSubject)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	// If we're invisible then ignore the event
	if ( !isVisible() )
	{													// $[TI1]
		// Ignore event
		return;
	}													// $[TI2]

	SAFE_AUX_CLASS_PRE_CONDITION((SUBJECT_ID_ == pSubject->getId()),
								 ((SUBJECT_ID_ << 16) | pSubject->getId()));

	if (isVerifyNeeded_ && !isPressedDown_)
	{
		if (getSubjectPtr_(SUBJECT_ID_)->isChanged())
		{
			verifyBitmap_.setColor(Colors::BLACK_ON_YELLOW_BLINK_SLOW);
			verifyBitmap_.setShow(TRUE);
		}
		else
		{
			verifyBitmap_.setColor(Colors::BLACK);
			verifyBitmap_.setShow(FALSE);
		}
	}

	Boolean  doUpdateDisplay = FALSE;
	if ( IS_MAIN_SETTING_ )
	{  // $[TI3] -- this is a main setting, check for proper state...
		switch ( qualifierId )
		{
			case Notification::ACCEPTED :					// $[TI3.1]
				doUpdateDisplay = TRUE;
				break;
			case Notification::ADJUSTED :					// $[TI3.2]
				// if we're a Main Setting button and we get an update from the
				// Adjusted context then only process this event if we're in the
				// DOWN ("adjusted") state...
				if ( isPressedDown_ )
				{											// $[TI3.2.1]
					doUpdateDisplay = TRUE;

					if ( pSubject->isChanged() )
					{										// $[TI3.2.1.1]
						// Set the button type to light
						// when the value is changed
						setButtonType(Button::LIGHT);
					}
					else
					{										// $[TI3.2.1.2]
						setButtonType(Button::DARK);
					}
				}											// $[TI3.2.2]
				break;
			default :
				// unexpected qualifier...
				AUX_CLASS_ASSERTION_FAILURE(qualifierId);
				break;
		}

		if ( pValueUpdateCallBack_ != NULL )
		{	 // $[TI3.3]
			(*pValueUpdateCallBack_)(qualifierId, pSubject);
		}	 // $[TI3.4]
	}
	else if ( IS_NON_BATCH_SETTING_ )
	{  // $[TI4] -- this is a non-batch setting...
		doUpdateDisplay = (qualifierId == Notification::ACCEPTED);
	}
	else
	{  // $[TI5] -- this is neither a main setting, nor a non-batch setting...
		doUpdateDisplay = (qualifierId == Notification::ADJUSTED);
	}

	if ( doUpdateDisplay )
	{  // $[TI6] -- update our display according to the new setting value...
		updateDisplay_(qualifierId, pSubject);
	}  // $[TI7] -- display update not needed...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerCallbackPtr
//
//@ Interface-Description
// Called to register the callBack pointer for the interested subScreen
// for updating setting changing event. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply update the pValueUpdateCallBack_ value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::registerValueUpdateCallbackPtr(ValueUpdateCallbackPtr pValueUpdateCallBack)
{
	CALL_TRACE("SettingButton::registerValueUpdateCallbackPtr(ValueUpdateCallbackPtr pValueUpdateCallBack)");

	CLASS_ASSERTION(pValueUpdateCallBack != NULL);
	pValueUpdateCallBack_ = pValueUpdateCallBack;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerUserEventCallbackPtr
//
//@ Interface-Description
// Called to register the callBack pointer for the interested subScreen
// for updating user events.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply update the pUserEventCallBack value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SettingButton::registerUserEventCallbackPtr(UserEventCallbackPtr pUserEventCallBack)
{
	CALL_TRACE("SettingButton::registerUserEventCallbackPtr(UserEventCallbackPtr pUserEventCallBack)");

	CLASS_ASSERTION(pUserEventCallBack != NULL);
	pUserEventCallBack_ = pUserEventCallBack;
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTargetType [virtual]
//
//@ Interface-Description
// Returns the AdjustPanelTarget::TargetType for this object. Virtual
// function of AdjustPanelTarget.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns TARGET_TYPE_SETTING for this setting button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AdjustPanelTarget::TargetType SettingButton::getTargetType(void) const
{
	CALL_TRACE("getTargetType(void)");

	return(AdjustPanelTarget::TARGET_TYPE_SETTING);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SettingButton::SoftFault(const SoftFaultID  softFaultID,
						 const Uint32       lineNumber,
						 const char*        pFileName,
						 const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SETTINGBUTTON,
							lineNumber, pFileName, pPredicate);
}
