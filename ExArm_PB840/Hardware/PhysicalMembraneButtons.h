
//*****************************************************************************
//
//  PhysicalMembraneButtons.h - Header file containing membrane button defs.
//      See MembraneButtons.h for logocal membrane buttons
//
//*****************************************************************************

#ifndef PHYSICAL_MEMBRANE_BUTTONS_H

#define PHYSICAL_MEMBRANE_BUTTONS_H

// The enum order MUST correspond to the bit order of the buttons input to the 74ALS251 hardware register
enum PhysicalMembraneButtons_t
{
	PHYSICAL_MEMBRANE_BUTTON_ACCEPT,
	PHYSICAL_MEMBRANE_BUTTON_CANCEL,
	PHYSICAL_MEMBRANE_BUTTON_HOME,
    PHYSICAL_MEMBRANE_BUTTON_MAN_BREATH,
    PHYSICAL_MEMBRANE_BUTTON_RESERVED1,
    PHYSICAL_MEMBRANE_BUTTON_RESERVED2,
	PHYSICAL_MEMBRANE_BUTTON_RESET,
    PHYSICAL_MEMBRANE_BUTTON_SILENCE,
    NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS
};


#endif

