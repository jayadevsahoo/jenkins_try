#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VitalCapacitySubScreen - Subscreen for activating or 
//                                  deactivating a Vital Capacity 
//                                  Maneuver.
//---------------------------------------------------------------------
//@ Interface-Description
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// timer event is passed into timerEventHappened() to display maneuver
// complete, cancel, or reject messages.  updatePanel() method is the
// main display engine to control the messages being displayed based
// on the various input. 
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the VitalCapacitySubScreen
// in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VitalCapacitySubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date: 15-Jan-2009    SCR Number: 6311
//  Project:  840S
//  Description:
//      Removed START/CANCEL help messages to avoid leaving incorrect
//      message in message area during loss of communications.
//
//  Revision: 005   By: rhj    Date: 10-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Added a message indicating a leak has been detected.
//
//  Revision: 004   By: gdc    Date: 11-Aug-2008    SCR Number: 6439
//  Project:  840S
//  Description:
//      Corrected comment for timerHappened.
//
//  Revision: 003  By:  rhj	   Date:  08-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:     
//      Added a three min. time out for automatically accepting
//      a VC maneuver.  Also, RM data results can now be saved 
//      to the database, when a new RM data results is generated.
//
//  Revision: 002   By: rhj   Date:  29-Nov-2006    SCR Number: 6318
//  Project:  RESPM
//  Description:
//       Fixed SCR 6318.  Changed the panels' text messages to point 
//       to RM_PAUSE_PENDING_MSG and RM_PAUSE_ACTIVE_MSG instead of 
//       PAUSE_PENDING_MSG, and PAUSE_ACTIVE_MSG.
//
//  Revision: 001   By: rhj   Date:  10-May-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//       RESPM Project Initial Version
//
//=====================================================================

#include "VitalCapacitySubScreen.hh"

//@ Usage-Classes
#include "GuiApp.hh"
#include "PromptStrs.hh"
#include "PromptArea.hh"
#include "ButtonTarget.hh"
#include "BdEventRegistrar.hh"
#include "BdGuiEvent.hh"
#include "Sound.hh"
#include "MiscStrs.hh"
#include "SubScreenArea.hh"
#include "UpperSubScreenArea.hh"
#include "MessageArea.hh"
#include "WaveformsSubScreen.hh"
#include "TextUtil.hh"
#include "TimeStamp.hh"
#include "PatientDataId.hh"
#include "PatientDataRegistrar.hh"
#include "BoundedValue.hh"
#include "BreathDatumHandle.hh"
#include "BreathDatum.hh"
#include "LowerScreen.hh"
#include "TrendDataMgr.hh"
#include "TrendEvents.hh"
#include "TrendValue.hh"

//@ End-Usage
// TODO E600 removed
//#include "Ostream.hh"
//@ Code...

// Initialize static constants.
static const Int32 PANEL_HEIGHT = LOWER_SUB_SCREEN_AREA_HEIGHT;
static const Int32 HALF_PANEL_WIDTH = LOWER_SUB_SCREEN_AREA_WIDTH / 2;
static const Int32 TOP_PANEL_Y = 0;
static const Int32 LEFT_PANEL_X = 1;
static const Int32 RIGHT_PANEL_X = LEFT_PANEL_X + HALF_PANEL_WIDTH;
static const Int32 DATA_X_ = 20;
static const Int32 DATA_Y_ = 30;
static const Int32 DATA_VALUE_WIDTH_  = 60 ;
static const Int32 DATA_VALUE_HEIGHT_ = 20 ;

static const Int32 BUTTON_HEIGHT_ = 32;
static const Int32 BUTTON_WIDTH_ = 80;
static const Int32 PANEL_TITLE_Y_ = 8;

static const Int32 PANEL_MSG_Y_ = PANEL_TITLE_Y_ + 21;
static const Int32 PANEL_COMMAND_Y_ = PANEL_MSG_Y_ + 21;
static const Int32 BUTTON_Y_ = PANEL_COMMAND_Y_ + 21;
static const Int32 BUTTON_BORDER_ = 3;

static const Int32 DATA_BUTTON_HEIGHT_ = 32;
static const Int32 DATA_BUTTON_WIDTH_ = 70;
static const Int32 LEAK_DETECTED_MSG_Y_ = BUTTON_Y_ + BUTTON_HEIGHT_ + 21 ;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VitalCapacitySubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the VitalCapacitySubScreen.  Passed a pointer to the subscreen
// area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various panels displayed in the subscreen and adds them
// to the container.
// $[RM12226]
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VitalCapacitySubScreen::VitalCapacitySubScreen(SubScreenArea *pSubScreenArea) :

SubScreen(pSubScreenArea),
titleArea_(MiscStrs::VITAL_CAPACITY_SUBSCREEN_TITLE, SubScreenTitleArea::SSTA_CENTER),
controlPanel_(),
startButton_(0, BUTTON_Y_,
             BUTTON_WIDTH_, BUTTON_HEIGHT_,
             Button::LIGHT, BUTTON_BORDER_,
             Button::ROUND, Button::NO_BORDER,
             MiscStrs::START_MANEUVER_BUTTON_TITLE,
             ::NULL_STRING_ID,
             ::GRAVITY_RIGHT,
             Button::PB_TOGGLE),
cancelButton_(0, BUTTON_Y_,
             BUTTON_WIDTH_, BUTTON_HEIGHT_,
             Button::LIGHT, BUTTON_BORDER_,
             Button::ROUND, Button::NO_BORDER,
             MiscStrs::CANCEL_MANEUVER_BUTTON_TITLE,
             ::NULL_STRING_ID,
             ::GRAVITY_RIGHT,
             Button::PB_TOGGLE),
rejectDataButton_( 0, 0,
                   DATA_BUTTON_WIDTH_, DATA_BUTTON_HEIGHT_,
                   Button::LIGHT, BUTTON_BORDER_,
                   Button::ROUND, Button::NO_BORDER,
                   MiscStrs::REJECT_MANEUVER_DATA_BUTTON_TITLE,
                   ::NULL_STRING_ID,
                   ::GRAVITY_RIGHT,
                   Button::PB_MOMENTARY),
acceptDataButton_( 0, 0,
                   DATA_BUTTON_WIDTH_, DATA_BUTTON_HEIGHT_,
                   Button::LIGHT, BUTTON_BORDER_,
                   Button::ROUND, Button::NO_BORDER,
                   MiscStrs::ACCEPT_MANEUVER_DATA_BUTTON_TITLE,
                   ::NULL_STRING_ID,
                   ::GRAVITY_RIGHT,
                   Button::PB_MOMENTARY),
panelTitle_(::NULL_STRING_ID),
panelMsg_(::NULL_STRING_ID), 
leakDetectMsg_(MiscStrs::LEAK_DETECTED_MSG),
notActivatedYet_(FALSE)
{
    CALL_TRACE("VitalCapacitySubScreen::VitalCapacitySubScreen(SubScreenArea*)");

    setX(0);
    setY(0);
    setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
    setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);
    setFillColor(Colors::LIGHT_BLUE);

    addDrawable(&titleArea_);
    addDrawable(&controlPanel_);

    // Specify control panel's attributes.
    controlPanel_.setWidth(HALF_PANEL_WIDTH-1);
    controlPanel_.setHeight(PANEL_HEIGHT-titleArea_.getHeight());
    controlPanel_.setX(LEFT_PANEL_X);
    controlPanel_.setY(TOP_PANEL_Y+titleArea_.getHeight());
    controlPanel_.setFillColor(Colors::MEDIUM_BLUE);

    // Add the Panel Title
    controlPanel_.addDrawable(&panelTitle_);
    panelTitle_.setText(MiscStrs::VITAL_CAPACITY_MANEUVER_TITLE);
    panelTitle_.positionInContainer(GRAVITY_CENTER);
    panelTitle_.setY(PANEL_TITLE_Y_);
    panelTitle_.setColor(Colors::WHITE);

    // Add the Panel status Message.
    controlPanel_.addDrawable(&panelMsg_);
    panelMsg_.setText(MiscStrs::VITAL_CAPACITY_START_MSG);
    panelMsg_.positionInContainer(GRAVITY_CENTER);
    panelMsg_.setY(PANEL_MSG_Y_);
    panelMsg_.setColor(Colors::WHITE);

    // Add start and cancel button to the screen
    controlPanel_.addDrawable(&startButton_);
    controlPanel_.addDrawable(&cancelButton_);

    const Int32 START_BUTTON_X = controlPanel_.getWidth() / 3 - startButton_.getWidth() / 2;
    const Int32 CANCEL_BUTTON_X = (controlPanel_.getWidth() / 3) * 2 - cancelButton_.getWidth() / 2;

    startButton_.setX(START_BUTTON_X);
    cancelButton_.setX(CANCEL_BUTTON_X);

    // Register for callbacks from start/cancel button
    startButton_.setButtonCallback(this);
    cancelButton_.setButtonCallback(this);

    // Change the Cancel button to Flat and the Start button to up position.
    updateStartCancelButtons_(FALSE);

    // default to IDLE
    eventStatus_ = EventData::IDLE;

    //
    // size and position the data panels
    //
    const Int32 DATA_PANEL_HEIGHT = (PANEL_HEIGHT - titleArea_.getHeight()) / NUM_DATA_PANELS - 1;
    const Int32 DATA_PANEL_Y = TOP_PANEL_Y + titleArea_.getHeight();

    // Get the current time stamp.
    TimeStamp invalidTime;
    invalidTime.invalidate();

    // Create the Data Panels
    for (Int32 i=0; i<NUM_DATA_PANELS; i++)
    {

        addDrawable(&pDataPanel_[i]);

        // Set Data Panel's Color, width, height, X,and Y attributes.
        pDataPanel_[i].setWidth(HALF_PANEL_WIDTH);
        pDataPanel_[i].setHeight(DATA_PANEL_HEIGHT);
        pDataPanel_[i].setX(RIGHT_PANEL_X);
        pDataPanel_[i].setY(DATA_PANEL_Y + i * DATA_PANEL_HEIGHT + i);
        pDataPanel_[i].setFillColor(Colors::BLACK);

        pDataPanel_[i].addDrawable(&pDataSymbol_[i]);
        pDataPanel_[i].addDrawable(&pDataValue_[i]);
        pDataPanel_[i].addDrawable(&pDataUnits_[i]);
        pDataPanel_[i].addDrawable(&pDataDateTime_[i]);

        // Set the text and help messages.
        pDataSymbol_[i].setText(MiscStrs::TOUCHABLE_VITAL_CAPACITY_LABEL);
        pDataSymbol_[i].setMessage(MiscStrs::TOUCHABLE_VITAL_CAPACITY_MSG);
        pDataUnits_[i].setText(MiscStrs::TOUCHABLE_VITAL_CAPACITY_UNIT);

        // Set the current time.
        pDataTimeStamp_[i] = invalidTime;

        // Set each data object's Color, width, height, X,and Y attributes.
        pDataSymbol_[i].setColor(Colors::WHITE);
        pDataValue_[i].setColor(Colors::WHITE);
        pDataUnits_[i].setColor(Colors::LIGHT_BLUE);
        pDataDateTime_[i].setColor(Colors::WHITE);
        pDataDateTime_[i].setX(DATA_X_);
        pDataSymbol_[i].setX(DATA_X_);
        pDataValue_[i].setX(DATA_X_ + pDataSymbol_[i].getWidth() + 10);
        pDataValue_[i].setWidth(DATA_VALUE_WIDTH_);
        pDataValue_[i].setHeight(DATA_VALUE_HEIGHT_);
        pDataUnits_[i].setX(pDataValue_[i].getX() + pDataValue_[i].getWidth() + 10);
        pDataSymbol_[i].setY(DATA_Y_);
        pDataValue_[i].setY(DATA_Y_);
        pDataUnits_[i].setY(DATA_Y_);

        // results data is shown when received from patient data
        pDataDateTime_[i].setShow(FALSE);
        pDataSymbol_[i].setShow(FALSE);
        pDataValue_[i].setShow(FALSE);
        pDataUnits_[i].setShow(FALSE);
    }

    // Add a Reject Data Button.
    pDataPanel_->addDrawable(&rejectDataButton_);
    rejectDataButton_.setX(pDataPanel_->getWidth() - DATA_BUTTON_WIDTH_);
    rejectDataButton_.setY(pDataPanel_->getHeight()/2 - DATA_BUTTON_HEIGHT_/2);
    rejectDataButton_.setShow(FALSE);

    // Add an Accept Data Button.
    pDataPanel_->addDrawable(&acceptDataButton_);
    acceptDataButton_.setX(pDataPanel_->getWidth() - DATA_BUTTON_WIDTH_*2);
    acceptDataButton_.setY(pDataPanel_->getHeight()/2 - DATA_BUTTON_HEIGHT_/2);
    acceptDataButton_.setShow(FALSE);

    // Register for callbacks from reject button
    rejectDataButton_.setButtonCallback(this);
    acceptDataButton_.setButtonCallback(this);

    // Register for changes to VitalCapacity pressure patient data
    PatientDataRegistrar::RegisterTarget(PatientDataId::VITAL_CAPACITY_ITEM, this);

    BdEventRegistrar::RegisterTarget(EventData::VITAL_CAPACITY_MANEUVER, this);

	//
	// Register for timer event callbacks.
	//
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT, this);



    // Register for leak detection
	PatientDataRegistrar::RegisterTarget(PatientDataId::IS_LEAK_DETECTED_ITEM, this);

    controlPanel_.addDrawable(&leakDetectMsg_);
	leakDetectMsg_.positionInContainer(GRAVITY_CENTER);
    leakDetectMsg_.setY(LEAK_DETECTED_MSG_Y_);
    leakDetectMsg_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
	leakDetectMsg_.setShow(FALSE);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VitalCapacitySubScreen  [Destructor]
//
//@ Interface-Description
// Destroys VitalCapacitySubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VitalCapacitySubScreen::~VitalCapacitySubScreen(void)
{
    CALL_TRACE("VitalCapacitySubScreen::~VitalCapacitySubScreen(void)");

    // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set a default panel message and update the prompt area during this 
//  subscreen's activation.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VitalCapacitySubScreen::activate(void)
{
    CALL_TRACE("VitalCapacitySubScreen::activate(void)");

    // If the vent is still in active or pending mode,
    // update the panel with the associated message.
    // Otherwise, update the panel with the default message.
    if( eventStatus_ == EventData::ACTIVE ||
        eventStatus_ == EventData::ACTIVE_PENDING ||
        eventStatus_ == EventData::PENDING )
    {
        updatePanel_(eventStatus_, EventData::NULL_EVENT_PROMPT);
    }
    else
    {
        panelMsg_.setText(MiscStrs::VITAL_CAPACITY_START_MSG);
        panelMsg_.positionInContainer(GRAVITY_CENTER);
        updateStartCancelButtons_(FALSE);
    }

    // Update the Prompt Area
    updatePromptArea_( (!acceptDataButton_.getShow()) );

    // Update the data panel
    for (Int32 i=0; i<NUM_DATA_PANELS; i++)
    {
        if (!pDataTimeStamp_[i].isInvalid())
        {
            wchar_t      cheapText[100];  
            TextUtil::FormatTime(cheapText, MiscStrs::TIME_DATE_PANEL_DATA_FORMAT, 
                                 pDataTimeStamp_[i]);
            pDataDateTime_[i].setText(cheapText);
            pDataDateTime_[i].setShow(TRUE);
            pDataSymbol_[i].setShow(TRUE);
            pDataValue_[i].setShow(TRUE);
            pDataUnits_[i].setShow(TRUE);
        }
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed.  
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VitalCapacitySubScreen::deactivate(void)
{
    CALL_TRACE("VitalCapacitySubScreen::deactivate(void)");

    // Clear the Primary, Advisory and Secondary prompts
    GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
                                            NULL_STRING_ID);
    GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
                                            NULL_STRING_ID);
    GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
                                        PromptArea::PA_LOW, NULL_STRING_ID);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePanel_
//
//@ Interface-Description
// Called to update the control based on the current maneuver status
// >Von
//  eventStatus     event status
//  eventPrompt     event prompt
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// For cancel, reject, complete, pending, and active events, update
// the associated panel display with the associated message and
// set the appropriate start and cancel button's state (up or flat). 
// $[RM12081] $[RM12062] 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VitalCapacitySubScreen::updatePanel_(EventData::EventStatus eventStatus,
                           EventData::EventPrompt eventPrompt)
{
    CALL_TRACE("VitalCapacitySubScreen::updatePanel_(EventData::EventId eventId,"
                                        " EventData::EventStatus eventStatus)");

    Boolean isStartButtonFlat = FALSE;

    switch (eventStatus)
    {
    case EventData::REJECTED:
        // Set a reject message to the panel, raise up the start button,
        // and flatten the cancel button.
        panelMsg_.setText(MiscStrs::VITAL_CAPACITY_REJECTED_MSG);             
        isStartButtonFlat = FALSE;
        break;
    case EventData::CANCEL:
    case EventData::COMPLETE: 
        // Set a start message to the panel, raise up the start button,
        // and flatten the cancel button.        
        panelMsg_.setText(MiscStrs::VITAL_CAPACITY_START_MSG);
        isStartButtonFlat = FALSE;
        break;
    case EventData::PENDING:
    case EventData::ACTIVE_PENDING:
        // Set a pause pending message to the panel, flatten the start button
        // and raise up the cancel button.
        panelMsg_.setText(MiscStrs::RM_PAUSE_PENDING_MSG);
        isStartButtonFlat = TRUE;
        break;

    case EventData::ACTIVE: 
        // Set a pause active message to the panel, flatten the start button
        // and raise up the cancel button.
        panelMsg_.setText(MiscStrs::RM_PAUSE_ACTIVE_MSG);
        isStartButtonFlat = TRUE;

        break;

    case EventData::IDLE:
    case EventData::AUDIO_ACK:
    case EventData::NO_ACTION_ACK:
    case EventData::PLATEAU_ACTIVE:
        // do nothing
        break;

    default :
        AUX_CLASS_ASSERTION_FAILURE(eventStatus);
        break;
    }

    eventStatus_ = eventStatus;

    // set the start and cancel positions.
    updateStartCancelButtons_(isStartButtonFlat);

    // Center the panel message.
    panelMsg_.positionInContainer(GRAVITY_CENTER);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Handles button down events for all of the "instant action" screen
// select buttons on this subscreen. The `pButton' parameter is the button 
// that was pressed down.  The `isByOperatorAction' flag is TRUE if the 
// given button was pressed by operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// If the start button is pressed then we send a maneuver request to the
// Breath-Delivery and display a Vital Capacity Start key help message.
// $[RM12080] 
//
// If the cancel button is pressed then we send a cancel maneuver request
// to the Breath-Delivery and display a Vital Capacity cancel key help message.
//
// If the reject data button is pressed then we clear the first data entry,
// hide the data buttons and "unfoist" the waveform subscreen.  $[RM12228]
//
// If the accept data button is pressed then we hide the accept/reject 
// buttons since the data is OK.    $[RM12229]
//---------------------------------------------------------------------
//@ PreCondition
// The isByOperatorAction must be set to TRUE and the pButton cannot be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VitalCapacitySubScreen::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{
    CALL_TRACE("VitalCapacitySubScreen::buttonDownHappened(Button *pButton,"
                                                          "Boolean byOperatorAction)");

    CLASS_PRE_CONDITION(pButton != NULL);
    CLASS_PRE_CONDITION(byOperatorAction);

    if (pButton == &startButton_)
    { 
        AdjustPanel::TakePersistentFocus(NULL);// Release focus      

		if (acceptDataButton_.getShow())
        {
            // hide the accept/reject buttons since the data is OK
            rejectDataButton_.setShow(FALSE);
            acceptDataButton_.setShow(FALSE);

            // Update the prompt area.
            updatePromptArea_(TRUE);

	        // Populate the TrendValue struct with the latest VC value. 
	        // Then pass it to the TrendDataMgr class to post the event.	
			TrendValue vcValue;
			vcValue.isValid = TRUE;
			vcValue.data.realValue = pDataValue_->getValue();
            TrendDataMgr::PostEventValue(TrendEvents::AUTO_VC_MANEUVER_ACCEPTED, vcValue );
        }

        // Notify Breath-Delivery of the maneuver request
        BdGuiEvent::RequestUserEvent(EventData::VITAL_CAPACITY_MANEUVER, EventData::START);

    }
    else if (pButton == &cancelButton_)
    { 
        AdjustPanel::TakePersistentFocus(NULL);// Release focus      

        // unfortunately, this will cancel ANY maneuver so we need to make this a toggle button
        // Notify Breath-Delivery of the cancel request
        BdGuiEvent::RequestUserEvent(EventData::CLEAR_KEY); 

    }
    else if (pButton == &rejectDataButton_)  
    { 
        // clear the first data entry and hide the data buttons
        pDataTimeStamp_->invalidate();
        pDataDateTime_->setShow(FALSE);
        pDataSymbol_->setShow(FALSE);
        pDataValue_->setShow(FALSE);
        pDataUnits_->setShow(FALSE);
        rejectDataButton_.setShow(FALSE);
        acceptDataButton_.setShow(FALSE);
        // unfreeze waveform.
        UpperSubScreenArea::GetWaveformsSubScreen()->unFreezeWaveform();

        // Update the prompt area
        updatePromptArea_(TRUE);       

        if (GuiTimerRegistrar::isActive(GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT))
        {
            GuiTimerRegistrar::CancelTimer(GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT);

        }


    }
    else if (pButton == &acceptDataButton_) 
    { 
        // hide the accept/reject buttons since the data is OK
        rejectDataButton_.setShow(FALSE);
        acceptDataButton_.setShow(FALSE);

	    // Populate the TrendValue struct with the latest VC value. 
	    // Then pass it to the TrendDataMgr class to post the event.	
		TrendValue vcValue;
		vcValue.isValid = TRUE;
		vcValue.data.realValue = pDataValue_->getValue();
		TrendDataMgr::PostEventValue( TrendEvents::AUTO_VC_MANEUVER_ACCEPTED, vcValue );
			
        // Update the prompt area
        updatePromptArea_(TRUE);   

        if (GuiTimerRegistrar::isActive(GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT))
        {
            GuiTimerRegistrar::CancelTimer(GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT);

        }


    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when the the button is set to up. 
//---------------------------------------------------------------------
//@ Implementation-Description
// If the button was force or set to up position, call this registered 
// callback routine to clear the message area.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VitalCapacitySubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
    CALL_TRACE("buttonUpHappened(Button *pButton, Boolean byOperatorAction)");

    if(pButton == &startButton_ || pButton == &cancelButton_) 
    {
        // Clear the help message
        GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
    }
    // do nothing with the other buttons
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened
//
//@ Interface-Description
//  Called when Breath-Delivery notifies us of a VitalCapacity Maneuver event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is called with the following applicable event status:
//  PENDING or ACTIVE_PENDING : Activate WaveformsSubScreen on the
//                              upper subscreen area.
//  ACTIVE or PLATEAU_ACTIVE  : Current pause data to appear at the
//                              waveform subscreen.
//  CANCEL                    : Inform the WaveformsSubScreen of the
//                              cancel command.
//  REJECTED                  : Rejection of VitalCapacity pause request.
//  COMPLETE                  : Completion of VitalCapacity pause request.
//  AUDIO_ACK                 : Produce CONTACT sound to indicate that
//                              plateau is stablized
//  IDLE or NO_ACTION_ACK     : Do nothing.
//
//  The associated display should be updated.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
VitalCapacitySubScreen::bdEventHappened(EventData::EventId,
                                EventData::EventStatus eventStatus,
                                EventData::EventPrompt eventPrompt)
{
    CALL_TRACE("VitalCapacitySubScreen::bdEventHappened(EventData::EventId,"
                                       " EventData::EventStatus eventStatus,"
                                       " EventData::EventPrompt eventPrompt)");

    switch(eventStatus)
    {
    case EventData::PENDING:
        // Activate WaveformsSubScreen on the upper subscreen area.  If the
        // waveform is frozen, it is unfrozen.
        // Pause request accepted, display waveform    
        UpperSubScreenArea::GetWaveformsSubScreen()->pauseRequested();

        // Save current waveform settings.
        UpperSubScreenArea::GetWaveformsSubScreen()->saveCurrentWaveformSettings();       

        // Force VC settings.
        UpperSubScreenArea::GetWaveformsSubScreen()->setupVcSettings();       

        notActivatedYet_ = TRUE;
        break;

    case EventData::ACTIVE: 
        // Cause current pause data to appear at the upper right edge
        // of the waveform subscreen.
        UpperSubScreenArea::GetWaveformsSubScreen()->
                                pauseActive(BreathPhaseType::VITAL_CAPACITY_INSP);

        if (notActivatedYet_)
        { 
            // Produce CONTACT sound to indicate that pause is now active
            // Sigma is capable of creating the following user interface sound.
            // When pause starts and terminates, an audible ...
            // not sure we should produce a sound since this alerts the patient to
            // the impending maneuver -- cite journal article
            Sound::Start(Sound::CONTACT);
            notActivatedYet_ = FALSE;
        } 
        break;

    case EventData::CANCEL: 

        // Restore previous waveform settings.
        UpperSubScreenArea::GetWaveformsSubScreen()->restoreWaveformSettings();               
        
        // Indicate cancellation of the current maneuver
        Sound::Start(Sound::INVALID_ENTRY);

        // Inform the WaveformsSubScreen of the cancel command.
        UpperSubScreenArea::GetWaveformsSubScreen()->pauseCancelByAlarm();
        
        break;

    case EventData::REJECTED:
         // Rejection of VitalCapacity request
         // Indicate cancellation to user's most recent maneuver request
         Sound::Start(Sound::INVALID_ENTRY);

        break;

    case EventData::COMPLETE:

        // Post a VC maneuver event, if the first maneuver results wasn't accepted.
        if (acceptDataButton_.getShow())
        {
            TrendDataMgr::PostEvent( TrendEvents::AUTO_VC_MANEUVER_ACCEPTED );   
        }

        // An audible indication when pause terminates
        // When pause starts and terminates, an audible ...
        Sound::Start(Sound::CONTACT);

        // Update the prompt area
        updatePromptArea_(FALSE);    

        GuiTimerRegistrar::StartTimer(GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT, this );

        break;

    case EventData::AUDIO_ACK:
        Sound::Start(Sound::CONTACT);
        break;

    case EventData::IDLE:
    case EventData::NO_ACTION_ACK:
    default:
        break;
    }

    // Update the panel.
    updatePanel_(eventStatus, eventPrompt);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened
//
//@ Interface-Description
//  Called when a registered patient data value has changed.  
//  It adds a new data record to the Panel when patient data value 
//  has changed.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  When a registered patient data value has occured, this method will 
//  add a new data record to the panel.  It will also show the accept
//  and reject buttons for the new data record.  In addition, if the value
//  is out of range, the data will be flashing.
//  $[RM12211]
//  $[LC12000] If Leak Compensation is ENABLED and LEAK is > 1 lpm, 
//  a message indicating a leak has been detected shall be displayed 
//  below the START button for each maneuver.
//---------------------------------------------------------------------
//@ PreCondition
//  The patientDataId must be a VITAL_CAPACITY_ITEM id.  
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
VitalCapacitySubScreen::patientDataChangeHappened(PatientDataId::PatientItemId
                                              patientDataId)
{
    CALL_TRACE("VitalCapacitySubScreen::patientDataChangeHappened(patientDataId)");

    AUX_CLASS_PRE_CONDITION(patientDataId == PatientDataId::VITAL_CAPACITY_ITEM ||
	                        patientDataId == PatientDataId::IS_LEAK_DETECTED_ITEM, patientDataId);
	
	if (patientDataId == PatientDataId::IS_LEAK_DETECTED_ITEM)
	{

		// Get the new value of the patient datum
		BreathDatumHandle dataHandle(patientDataId);
		Boolean isLeakDetected = dataHandle.getDiscreteValue().data;
		leakDetectMsg_.setShow(isLeakDetected);

	}
	else
	{




		if (!pDataTimeStamp_->isInvalid())
		{
			for (Int32 i=NUM_DATA_PANELS-1; i>0; i--)
			{
				// age the previous data to the "older" data panels
				pDataValue_[i].setValue(pDataValue_[i-1].getValue());
				pDataValue_[i].setPrecision(pDataValue_[i-1].getPrecision());
				pDataValue_[i].setColor(pDataValue_[i-1].getColor());
				pDataTimeStamp_[i] = pDataTimeStamp_[i-1];
			}
		}
	
		// set the time the data was received which will also make the TimeStamp valid
		pDataTimeStamp_->now();
	
		// show the accept/reject data buttons now that we have data 
		rejectDataButton_.setShow(TRUE);
		acceptDataButton_.setShow(TRUE);
	
		BoundedBreathDatum pdDatum = 
			BreathDatumHandle(patientDataId).getBoundedValue();
	
		// FYI: pDataValue_-> is the same as pDataValue[0].
		// Set the new value and precision of the current (top) data panel
		pDataValue_->setValue(pdDatum.data.value);
		pDataValue_->setPrecision(pdDatum.data.precision);
	
		// Flash the datum if out of range else display in white.
		if (pdDatum.clippingState == BoundedValue::ABSOLUTE_CLIP)
		{
			pDataValue_->setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
		}
		else
		{
			pDataValue_->setColor(Colors::WHITE);
		}
	
		for (Int32 i=0; i<NUM_DATA_PANELS; i++)
		{
			if (!pDataTimeStamp_[i].isInvalid())
			{
				wchar_t      cheapText[100];  
				TextUtil::FormatTime(cheapText, MiscStrs::TIME_DATE_PANEL_DATA_FORMAT, 
									 pDataTimeStamp_[i]);
				pDataDateTime_[i].setText(cheapText);
				pDataDateTime_[i].setShow(TRUE);
				pDataSymbol_[i].setShow(TRUE);
				pDataValue_[i].setShow(TRUE);
				pDataUnits_[i].setShow(TRUE);
			}
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateStartCancelButtons_
//
//@ Interface-Description
// This method provides a way to toogle the Start and cancel button 
// from the up state to the flat state and vice versa.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It takes in a isStartButtonFlat flag which allows to toggle 
//  the start and cancel button's state.  If it is set to TRUE, the start button's 
//  state is set to flat while the cancel button is set to up.  
//  Otherwise, the start button's state is set to up while the cancel
//  button is set to flat.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VitalCapacitySubScreen::updateStartCancelButtons_(Boolean isStartButtonFlat)
{

    CALL_TRACE("VitalCapacitySubScreen::updateStartCancelButtons_("
                                           "Boolean isStartButtonFlat)");

    // If isStartButtonFlat is set to TRUE, then set the start button to flat
    // and the cancel button to up. Otherwise set the start button to up
    // and the cancel button to flat.
    if(isStartButtonFlat)
    {
        startButton_.setToFlat();
        cancelButton_.setToUp();

    }
    else
    {
        startButton_.setToUp();
        cancelButton_.setToFlat();        
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void
VitalCapacitySubScreen::SoftFault(const SoftFaultID  softFaultID,
           const Uint32       lineNumber,
           const char*        pFileName,
           const char*        pPredicate)  
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
            VITALCAPACITYSUBSCREEN, lineNumber, pFileName, pPredicate);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePromptArea_
//
//@ Interface-Description
//  This method provides a way to toogle the prompt message area to 
//  either the default "Start" message or the accept or reject 
//  data results message.
//---------------------------------------------------------------------
//@ Implementation-Description
//  It takes in a isStartMsg flag which allows to toggle prompt message 
//  area to either the default "Start" message or the accept or reject 
//  data results message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void VitalCapacitySubScreen::updatePromptArea_(Boolean isStartMsg)
{
    CALL_TRACE("VitalCapacitySubScreen::updatePromptArea_("
                                           "Boolean isStartMsg)");
    SubScreenArea * pSubScreenArea = (SubScreenArea *) LowerScreen::RLowerScreen.getLowerSubScreenArea();
    if (pSubScreenArea->isCurrentSubScreen(this))
    {
    
        if(isStartMsg)
        {
            // Set Primary prompt to "Touch START button to begin maneuver.
            //                       "Touch CANCEL button to cancel active maneuver."
            GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
                        PromptArea::PA_LOW, PromptStrs::RM_VC_START_ADVISORY_P);
        }
        else
        {
            // Set Primary prompt to "Touch ACCEPT to store data.  "
            //                       "Touch REJECT to discard data."
            GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
                        PromptArea::PA_LOW, PromptStrs::RM_COMPLETE_ADVISORY_S);
               
        }
        // Set Secondary prompt to tell user to press OTHER SCREENS button
        // to cancel
        GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
                    PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);
    }

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// When the RESPM_VC_MANEUVER_TIMEOUT occurs, we will automatically 
// accept the current maneuver and post a VC event to the database.
//---------------------------------------------------------------------
//@ Implementation-Description
// When the RESPM_VC_MANEUVER_TIMEOUT occurs we will automatically 
// accept the current maneuver, hide the accept/reject buttons,
// change the prompts, populate a TrendValue struct with the latest 
// VC value and providing it to the TrendDataMgr class to post a
// VC event.
//---------------------------------------------------------------------
//@ PreCondition
// The timer id must be the RESPM_VC_MANEUVER_TIMEOUT id.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
VitalCapacitySubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("VitalCapacitySubScreen::timerEventHappened(timerId)");

	switch (timerId)
	{
	
	case  GuiTimerId::RESPM_VC_MANEUVER_TIMEOUT:

		// hide the accept/reject buttons since the data is OK
		rejectDataButton_.setShow(FALSE);
		acceptDataButton_.setShow(FALSE);

		if (isVisible() )
		{
			// Update the prompt area.
			updatePromptArea_(TRUE);
		}

		// Populate the TrendValue struct with the latest VC value. 
		// Then pass it to the TrendDataMgr class to post the event.	
		TrendValue vcValue;
		vcValue.isValid = TRUE;
		vcValue.data.realValue = pDataValue_->getValue();
		TrendDataMgr::PostEventValue(TrendEvents::AUTO_VC_MANEUVER_ACCEPTED, vcValue);

		break;
	default:
		AUX_CLASS_ASSERTION_FAILURE(timerId);
		break;
	}

}


