////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
SCM
Name:		SWATInterface.h

Purpose:
	This file defines the Software Automation Test (SWAT) moudule interfaces.

REVISION HISTORY:

REVISION: 002	BY: SF		DATE: 17-FEB-2014
PROJECT: 880 vent
DR NUMBER: N / A
DESCRIPTION:
	add more comments to explain the SWAT design


REVISION: 001	BY: SF		DATE: 23-JUL-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/

#pragma once

#include <Windows.h>
#include "SWATTypes.h"

/*
	Interface for SWAT service provider, SWAT service provider should implement 
	this interface.	this clas provides an external interface to the SWAT 
	subsystem, and can be accessed by SWAT subsystem also. After a SWAT command 
	server parsed the SWAT command from ethernet, different methods in this 
	interface will be called by server to handle different SWAT command.
*/
class ISWATService
{
public:
	// virtual destructor
	virtual ~ISWATService() {}

	// Simulate a touch on location at [x,y]
	virtual SWATStatus SimulateTouch(int x, int y) = 0;

	// Simulate a push/release on a bezel key
	virtual SWATStatus SimulateBezelKey(int keyId, bool onOff) = 0;

	// simulate knob rotates
	virtual SWATStatus SimulateKnob(int clicks) = 0;

	// Capture screen and save to file 'fileName'
	virtual SWATStatus CaptureScreen(const char* fileName) = 0;

	// Return screen capture queue size.
	virtual SWATStatus QueryScreenshotQueueSize(int& queueSize) = 0;	

	// Remove all screen capture files.
	virtual SWATStatus ClearScreenshots() = 0;

    // Query screen capture ftp address for downloading
	virtual SWATStatus QueryScreenshotPath(int index, char* scPath) = 0;
};
