#pragma once
#include "tinyxml.h"
#include "GuiIOServer.h"

typedef struct
{
    INT                 transactionId;
    INT                 seconds;
    INT                 microseconds;
} GuiIOMsgHeader;

struct QGuiIOActionRespEvent
{
	typedef enum ResponseType
	{
		INVALID_RESPONSE_TYPE,
		ACK_NAK_RESP,
		QUERY_XY_RESP,
		QUERY_WH_RESP,
		GET_PROPERTIES_RESP,
		GET_OBJECT_VALUE_RESP,
		GET_FOCUS_RESP,
		UI_OUTPUT_RESP,
		VENT_INFO_RESP,
		SCREEN_CAPTURE_BUFFER_INFO_RESP,
		ALTIA_POLL_EVENT_RESP,
		QUERY_SCREENSHOT_PATH_RESP
	};

	ResponseType		responseType;
	GuiIOMsgHeader 		header;
	union
	{
		GuiIOAckNakResp				ackNakResp;
		GuiIOQueryXYResp			queryXYResp;
		GuiIOQueryWHResp			queryWHResp;
		GuiIOGetPropertiesResp		getPropertiesResp;
		GuiIOGetObjectValueResp		getObjectValueResp;
		GuiIOGetFocusResp			getFocusResp;
		GuiIOUIOutputResp			uiOutputResp;
		GuiIOVentInfoResp			ventInfoResp;
		GuiIOAltiaPollEventResp		altiaPollEventResp;
		GuiIOScreenCaptureBufferInfoResp	screenCaptureBufferInfoResp;
		GuiIOQueryScreenshotPathResp			queryScreenshotPathResp;
	} msg;
};

class Parser
{
public:
	Parser(void);
	~Parser(void);

	INT ParseCmd(const CHAR* buffer, INT maxLen, INT startIndex, QGuiIOActionRespEvent& action);
	TiXmlElement* ParseAckNakResp(TiXmlElement* ele, GuiIOAckNakResp& s);
	TiXmlElement* ParseQueryXYResp(TiXmlElement* ele, GuiIOQueryXYResp& s);
	TiXmlElement* ParseQueryWHResp(TiXmlElement* ele, GuiIOQueryWHResp& s);
	TiXmlElement* ParseGetPropertiesResp(TiXmlElement* ele, GuiIOGetPropertiesResp& s);
	TiXmlElement* ParseGetObjectValueResp(TiXmlElement* ele, GuiIOGetObjectValueResp& s);
	TiXmlElement* ParseGetFocusResp(TiXmlElement* ele, GuiIOGetFocusResp& s);
	TiXmlElement* ParseUIOutputResp(TiXmlElement* ele, GuiIOUIOutputResp& s);
	TiXmlElement* ParseScreenCaptureBufferInfoResp(TiXmlElement* ele, GuiIOScreenCaptureBufferInfoResp& s);
	TiXmlElement* ParseVentInfoResp(TiXmlElement* ele, GuiIOVentInfoResp& s);
	TiXmlElement* ParseAltiaPollEventResp(TiXmlElement* ele, GuiIOAltiaPollEventResp& s);
	TiXmlElement* ParseQueryScreenshotPathResp(TiXmlElement* ele, GuiIOQueryScreenshotPathResp& s);

	INT BuildSimulateTouch(int x, int y);
	INT BuildClearScreenshots();
	INT BuildQueryScreenshotPath(int index);

	// Build functions for Viking vent
	INT BuildQueryXY(const CHAR* objectId);
	INT BuildQueryWH(const CHAR* objectId);
	INT BuildGetProperties(const CHAR* objectId, int row, int col);
	INT BuildGetFocus();
	INT BuildMousePos(int x, int y);
	INT BuildLButtonDown(int x, int y);
	INT BuildLButtonUp(int x, int y);
	INT BuildButtonClick(int x, int y);
	INT BuildGetObjectValue(const CHAR* objectId, int row, int col);
	INT BuildUIOutputOnOff(bool onOff);
	INT BuildCursor(bool onOff);
	INT BuildScreenCapture(const CHAR* fileName);
	INT BuildScreenCaptureBufferInfo();
	INT BuildSimulateBDAlarm(const CHAR* alarmId, const CHAR* condAugOptions, const CHAR* depenAugOptions);
	INT BuildVentInfo();
	INT BuildBezelKeyPress(int keyId, bool onOff);
	INT BuildKnob(int delta);
	INT BuildAltiaPollEvent(const CHAR* eventName);
	INT BuildAltiaSendEvent(const CHAR* eventName, int eventValue);
	INT BuildBye();
	const CHAR* GetBuiltMsg() { return m_msg; }

	enum { MAX_MSG_LENGTH = 4096 };

private:
	TiXmlElement* ParseHeader(TiXmlElement* ele, GuiIOMsgHeader& s);

	CHAR m_msg[MAX_MSG_LENGTH];
	int transactionId;
    TiXmlDocument m_doc;			
    bool m_parseErr;				

    //--------------------------------------------------------------------------
    // static members
    //--------------------------------------------------------------------------
    static const CHAR* MESSAGE_ROOT;	///< Message root XML element
    static const CHAR* HEADER;			///< Header XML element
	static const CHAR* ACK_NAK_RESP;		///< ACK/NAK Respose XML element
    static const CHAR* QUERY_XY_RESP;		///< Query XY XML element
    static const CHAR* QUERY_WH_RESP;		///< Query WH XML element
	static const CHAR* GET_PROPERTIES_RESP;	///< Get Enabled XML element
    static const CHAR* GET_OBJECT_VALUE_RESP;///< Get object value XML element
	static const CHAR* GET_FOCUS_RESP;		 ///< Get focus XML element
	static const CHAR* UI_OUTPUT_RESP;		 ///< UI output XML element
	static const CHAR* SCREEN_CAPTURE_BUFFER_INFO_RESP;	 ///< Screen capture buffer info XML element
	static const CHAR* VENT_INFO_RESP;		 ///< Vent info output XML element
	static const CHAR* ALTIA_POLL_EVENT_RESP; ///< Altia poll event response XML element
	static const CHAR* QUERY_SCREENSHOT_PATH_RESP;		///< Query Screen Capture Address XML element
};
