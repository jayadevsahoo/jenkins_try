 
#ifndef Plot2TypeSetting_HH
#define Plot2TypeSetting_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class:  Plot2TypeSetting - Plot 2 Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Plot2TypeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  now an observer of plot #1 type setting, therefore
//	   'valueUpdate()', 'doRetainAttachment()' and
//	   'settingObserverInit()' overridden from observer base class
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
#include "NonBatchDiscreteSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class Plot2TypeSetting : public NonBatchDiscreteSetting, public SettingObserver
{
  public:
    Plot2TypeSetting(void);
    virtual ~Plot2TypeSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual Boolean isEnabledValue(const DiscreteValue value) const;

    virtual SettingValue  getDefaultValue(void) const;

    // SettingObserver methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

    virtual void  settingObserverInit(void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    Plot2TypeSetting(const Plot2TypeSetting&);  // not implemented...
    void  operator=(const Plot2TypeSetting&);   // not implemented...
};


#endif // Plot2TypeSetting_HH
 
