#ifndef BackgroundBdTests_HH
#define BackgroundBdTests_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//  Class: BackgroundBdTests - Individual background tests for the BD.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundBdTests.hhv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
// 
//  Revision: 007  By: rpr   Date:  05-Nov-2010    SCR Number: 6690
//    Project:  PROX
//    Description:
//      Reverted revision 006.  The clock variance test has been removed so the 
//		test of the rtc has been brought back.  
//
//  Revision: 006  By: gdc   Date:  18-Aug-2009    SCR Number: 6147
//    Project:  XB
//    Description:
//      Removed background Time-of-Day clock test. Moved this function to
// 		the cycle-to-cycle and interval time variance tests in BdMonitoredData.
//
//  Revision: 005  By: iv    Date:  20-May-1999    DR Number: DCS 5397
//    Project:  ATC (840)
//    Description:
//      Added data members to control the ADC test.
//
//  Revision: 004  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: iv     Date: 11-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//		   Added error code argument to all tests.
//
//  Revision: 002   By: iv    Date: 06-May-1997   DR Number: DCS 2027 
//    Project:  Sigma (R8027)
//    Description:
//      Eliminated the data member WasAudioAlarmActivated_ . 
//
//  Revision: 001    By: by Date: 12-Dec-1995 DR Number: None
//    Project: Sigma (840)
//    Description:
//       Initial version
//
//=====================================================================

#include "Background.hh"
#include "TimeStamp.hh"

#define ADC_TEST_POINT_1 205

typedef struct
{
    Uint16   testValue;
    Uint16   lowLimit;
    Uint16   highLimit;

} TestTableStruct;
          

class BackgroundBdTests
{
    public:

           static BkEventName AudibleAlarmCapacitorTest( Uint16 &errorCode );
           static BkEventName AudibleAlarmDisconnectTest( Uint16 &errorCode );
           static BkEventName BatteryTest( Uint16 &errorCode );
           static BkEventName AdcTest( Uint16 &errorCode );
           static BkEventName AcSwitchPositionTest( Uint16 &errorCode );
           static BkEventName NovRamChecksumTest( Uint16 &errorCode );
           static BkEventName TodTest( Uint16 &errorCode );

           static void SoftFault( const SoftFaultID softFaultID,
				  const Uint32      lineNumber,
				  const char*       pFileName  = NULL, 
				  const char*       pPredicate = NULL );

    private:

        BackgroundBdTests ( void );   // not implemented            
        ~BackgroundBdTests( void );   // not implemented           

        //@ Data-Member: AdcTestIndex_
        // indicates test point for ADC test 
        static Int32 AdcTestIndex_;

        //@ Data-Member: FaultCounter_
        // counts number of faults 
        static Int32 AdcFaultCounter_;

        //@ Data-Member: AdcTestFailed_ 
        //  flag to indicate that this background test has failed
        static Boolean AdcTestFailed_; 

        //@ Data-Member: AcSwitchPositionTestFailed_ 
        //  flag to indicate that this background test has failed
        static Boolean AcSwitchPositionTestFailed_; 

        //@ Data-Member: NovRamChecksumTestFailed_ 
        //  flag to indicate that this background test has failed
        static Boolean NovRamChecksumTestFailed_; 

        //@ Data-Member: TodTestFailed_ 
        //  flag to indicate that this background test has failed
        static Boolean TodTestFailed_; 

        //@ Data-Member: NumberAcSwitchCycles_ 
        //  indicate the duration of the current AC switch
        static Uint8 NumberAcSwitchCycles_; 

        //@ Data-Member: TodTestInitialized_
        //  indicate whether the TOD test is initialized 
        static Boolean TodTestInitialized_; 

        //@ Data-Member: PreviousTime_ 
        //  stores the previous time for the TOD Test 
        static TimeStamp PreviousTime_; 

        //@ Data-Member: CurrentTime_ 
        //  stores the current time for the TOD Test 
        static TimeStamp CurrentTime_; 

};


#endif    // #ifndef BackgroundBdTests_HH
