#ifndef DevelopmentOptionsSubScreen_HH
#define DevelopmentOptionsSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2008, Covidien
//=====================================================================

//====================================================================
// Class: DevelopmentOptionsSubScreen - The screen selected by pressing the
// More screen button on the Upper Other Screens Subscreen.
// Displays and executes development option buttons.
// (GUI, vent head, and compressor).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DevelopmentOptionsSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 008  By: rhj    Date: 07-Nov-2008   SCR Number:  6435
//  Project:  840S
//  Description:
//      Moved Software options functionality to the Software options subscreen.
//
//  Revision: 007  By: gdc    Date: 26-May-2007   DCS Number:  6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 006 By: erm    Date: 16-DEC-2002   DR Number: 6028
//  Project:  EMI
//	Description:
//		Added the abilty to change Manufacturing Datakeys
//              to production Datakeys.
//
//  Revision: 005 By: srp    Date: 28-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 004  By: gdc    Date: 28-Aug-2000   DCS Number:  5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 004  By: sah    Date: 17-Jul-2000   DCS Number:  5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  standardized static data types
//      *  added support for VTPC option
//
//  Revision: 003   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added support for PAV option
//
//  Revision: 002  By: sah    Date: 06-Jul-1999   DCS Number:  5424
//  Project:  NeoMode
//  Description:
//      Removed obsoleted buttons; added new circuit type-specific,
//      SST override buttons; added ability to activated/deactivate
//      software options.
//
//  Revision: 001  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//====================================================================


#include "SubScreen.hh"
#include "AdjustPanelTarget.hh"
#include "ServicePromptMgr.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "GuiAppClassIds.hh"
#include "TextField.hh"
#include "Bitmap.hh"
#include "Button.hh"
#include "SoftwareOptionsSubScreen.hh"

//@ End-Usage

// FORNOW
#include "TextButton.hh"

class SubScreenArea;

class DevelopmentOptionsSubScreen : public SubScreen,
								public AdjustPanelTarget,
								public ServicePromptMgr
{
public:
	DevelopmentOptionsSubScreen(SubScreenArea* pSubScreenArea);
	~DevelopmentOptionsSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	// FORNOW
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	void buttonUpHappened(Button *, Boolean);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelRestoreFocusHappened(void);
	virtual void adjustPanelAcceptPressHappened(void);
				

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
    static inline void setLastDisplay(Uint32 classId);

protected:

private:
	// these methods are purposely declared, but not implemented...
	DevelopmentOptionsSubScreen(void);								// not implemented...
	DevelopmentOptionsSubScreen(const DevelopmentOptionsSubScreen&);	// not implemented...
	void operator=(const DevelopmentOptionsSubScreen&);			// not implemented...

	void clearDownLoadButtonRequest_(Button *pButton);
	void clearMfgToProdDatakeyRequest_(Button *pButton);

    void  displaySoftwareOptionsSubScreen_(void);

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen
	SubScreenTitleArea titleArea_;


	//@ Data-Member: byPassFlashButton_
	// Used to override calibrations.
	TextButton  byPassFlashButton_;


	//@ Data-Member: setEstPassButton_
	// Used to override EST.
	TextButton  setEstPassButton_;


	//@ Data-Member: neoSstOverrideButton_
	// Used to override neonatal SST.
	TextButton  neoSstOverrideButton_;

	//@ Data-Member: pedSstOverrideButton_
	// Used to override Ped SST.
	TextButton  pedSstOverrideButton_;

	//@ Data-Member: adultSstOverrideButton_
	// Used to override Adult SST.
	TextButton  adultSstOverrideButton_;

	//@ Data-Member: guiDownloadButton_
	// The Button used to allow the GUI into download mode.
	TextButton  guiDownloadButton_;

	//@ Data-Member: bdDownloadButton_
	// The Button used to allow the BD into download mode.
	TextButton  bdDownloadButton_;
	
	//@ Data-Member: mfgToProdDatakeyButton_
	// The Button used to convert Mfg Datakeys to Production Datakeys
	TextButton  mfgToProdDatakeyButton_;


	//@ Data-Member: pCurrentButton_
	// The pCurrentButton_ is used for remembering the previously button
	// which was pressed by the user.
	Button *pCurrentButton_;

    //@ Data-Member:  softwareOptionsButton_
	// A Software Option button allows a transition from the Development Options
	//  Subscreen screen to the Software Options Subscreen.
	TextButton softwareOptionsButton_;
	
    //@ Data-Member:: lastDisplay_
    // Flag indicates the previous subscreen.
    static Uint32 lastDisplay_;

};


#include "DevelopmentOptionsSubScreen.in"


#endif // DevelopmentOptionsSubScreen_HH 
