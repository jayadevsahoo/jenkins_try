#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: InitConditions - System Initialization Conditions.
//---------------------------------------------------------------------
//@ Interface-Description
//  The InitConditions class contains the initial system conditions for
//  service mode request and service required.  The class combines the 
//  hardware state information provided by the Post class on the BD and
//  GUI.  On the BD, the ServiceRequired and ServiceModeRequested
//  states are both valid.  On the GUI, only the ServiceRequired 
//  state is valid.  As such, this class uses conditional compilation
//  to define only those accessor methods appropriate to the BD or GUI.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the initial conditions and hardware state
//  information for task control.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class provides accessor methods for each of the states;
//  ServiceRequired and ServiceModeRequested.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/InitConditions.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 004   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 003   By:   gdc      Date: 12-Oct-1998 DCS Number: 5164
//      Project:   BiLevel
//      Description:
//         Removed IsServiceModeRequested() method that is redundant
//		   to the Post::IsServiceModeRequested() method.
//
//   Revision 002   By:   gdc      Date: 21-Oct-1997 DR Number:   2573
//      Project:   Sigma   (R8027)
//      Description:
//         Checking EST completion status so GUI does not transition
//         to online prior to completing its EST tests.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "InitConditions.hh"

//@ Usage-Classes
#include "Post.hh"

#ifdef SIGMA_BD_CPU
#include "ServiceMode.hh"
#endif
#ifdef SIGMA_GUI_CPU
#include "NovRamManager.hh"
#endif
#ifdef SIGMA_DEVELOPMENT
#include "Ostream.hh"
#endif
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

Boolean InitConditions::ServiceRequired_ = FALSE;
 
//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize()
//
//@ Interface-Description
//  This static method called during system initialization sets the
//  internal operating condition states.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The ServiceRequired_ flags are initialized from the Post 
//  and Service-Mode classes.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
InitConditions::Initialize(void)
{
    CALL_TRACE("Initialize(void)");

#ifdef SIGMA_GUI_CPU

#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_INITCONDITIONS) >= 1)
    {
        char * pString = NULL;
        switch ( Post::GetBackgroundFailed() )
        {
            case PASSED:
                 pString = "PASSED";
                 break;
            case MINOR:           
                 pString = "MINOR";
                 break;
            case SERVICE_REQUIRED: 
                 pString = "SERVICE_REQUIRED";
                 break;
            case MAJOR:
                 pString = "MAJOR";
                 break;
        }
        cout << "Post::GetBackgroundFailed() is " << pString << endl;

        cout << "Post::IsBdFailed() is " << 
                (Post::IsBdFailed()?"TRUE":"FALSE") << endl;
        cout << "Post::IsPostFailed() is " << 
                (Post::IsPostFailed()?"TRUE":"FALSE") << endl;
    }
#endif

    InitConditions::ServiceRequired_ 
        =    Post::GetBackgroundFailed() == MAJOR  // only MAJOR
          || Post::IsPostFailed()
          || !NovRamManager::HasGuiEstPassed(); // $[TI1] $[TI2]
#endif // SIGMA_GUI_CPU

#ifdef SIGMA_BD_CPU

#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_INITCONDITIONS) >= 1)
    {
        char * pString = NULL;
        switch ( Post::GetBackgroundFailed() )
        {
            case PASSED:
                 pString = "PASSED";
                 break;
            case MINOR:           
                 pString = "MINOR";
                 break;
            case SERVICE_REQUIRED: 
                 pString = "SERVICE_REQUIRED";
                 break;
            case MAJOR:
                 pString = "MAJOR";
                 break;
        }
        cout << "Post::GetBackgroundFailed() is " << pString << endl;

        cout << "Post::IsPostFailed() is " << 
                (Post::IsPostFailed()?"TRUE":"FALSE") << endl;
        cout << "ServiceMode::IsServiceRequired() is " << 
                (ServiceMode::IsServiceRequired()?"TRUE":"FALSE") << endl;
        cout << "Post::IsServiceModeRequested() is " << 
                (Post::IsServiceModeRequested()?"TRUE":"FALSE") << endl;
        
        char * pStartupString = NULL;
        switch ( Post::GetStartupState() )
        {
            case UNKNOWN:
                 pStartupString = "UNKNOWN";
                 break;
            case EXCEPTION:           
                 pStartupString = "EXCEPTION";
                 break;
            case ACSWITCH: 
                 pStartupString = "ACSWITCH";
                 break;
            case POWERFAIL:
                 pStartupString = "POWERFAIL";
                 break;
            case WATCHDOG:
                 pStartupString = "WATCHDOG";
                 break;
            case INTENTIONAL:           
                 pStartupString = "INTENTIONAL";
                 break;
        }
        cout << "Post::GetStartupState() is " << pStartupString << endl;

    }
#endif
    InitConditions::ServiceRequired_ 
        =    Post::GetBackgroundFailed() == MAJOR
          || Post::IsPostFailed()
          || ServiceMode::IsServiceRequired(); // $[TI3] $[TI4]

#endif // SIGMA_BD_CPU

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsServiceRequired
//
//@ Interface-Description
//  Returns the processor's "service required" state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns internal variable ServiceRequired_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
InitConditions::IsServiceRequired(void)
{
	// TODO E600 MS Made this return false until EST is ported.
	return FALSE;
    CALL_TRACE("IsServiceRequired(void)");
    //return ServiceRequired_;
    // $[TI1]
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
InitConditions::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_INITCONDITIONS,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

