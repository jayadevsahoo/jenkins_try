#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: ModeSettingsCapture - Control settings display area screen capture
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is derived from ScreenCapture and provides for the
//	capture of pixel data from the control settings display area,
//	i.e. the portion of the main settings area where mode, mand type,
//	spont type, trigger type, and IBW are displayed.
//---------------------------------------------------------------------
//@ Rationale
//	Provides specific handling of pixel capture from the given screen area.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method GetModeSettingsCapture() is provided to instantiate an
//	ModeSettingsCapture object and also to access this object.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness of 
//  parameterized data.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/ModeSettingsCapture.ccv   25.0.4.0   19 Nov 2013 14:12:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: quf         Date: 24-Sep-2001  DR Number: 5942
//  Project: GuiComms
//  Description:
//  Code/comment/header cleanup.
//
//  Revision: 001    By: gdc         Date: 23-Jan-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//=====================================================================

#include "Sigma.hh"
#include "ModeSettingsCapture.hh"

//@ Usage-Classes
#include "BaseContainer.hh"
#include "GuiFoundation.hh"
#include "DisplayContext.hh"
#include "Colors.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ModeSettingsCapture [constructor]
//
//@ Interface-Description
//  Constructor for the ModeSettingsCapture class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
ModeSettingsCapture::ModeSettingsCapture( Uint32* pRasterData, 
							  Uint32  sizeofRasterData,
							  DisplayContext& rDisplayContext,
							  Int32 x0, Int32 y0,
	                          Uint32 xWidth, Uint32 yHeight )
	: ScreenCapture(pRasterData,
		  sizeofRasterData,
		  rDisplayContext,
		  x0, y0,
		  xWidth, yHeight)
{
// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ModeSettingsCapture [destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
ModeSettingsCapture::~ModeSettingsCapture()
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: filterPixel_
//
//@ Interface-Description
//	Convert the input screen pixel value to the appropriate printer
//	pixel value and return this value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Byte
ModeSettingsCapture::filterPixel_(Byte pixel) const
{
// $[TI1]
  return    (pixel == Colors::WHITE)
		 || (pixel == Colors::LIGHT_BLUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetModeSettingsCapture [static]
//
//@ Interface-Description
//  Defines and returns a reference to the control settings display area
//	capture object.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
ModeSettingsCapture &
ModeSettingsCapture::GetModeSettingsCapture(void)
{
// $[TI1]
  static const Uint32 AREA_HEIGHT_ = 25;
  static Uint32 RasterData_[(GuiFoundation::SCREEN_WIDTH_PIXELS+31)/32*AREA_HEIGHT_];
  static ModeSettingsCapture ModeSettingsCapture_(
	  RasterData_,
	  sizeof(RasterData_),
      GuiFoundation::GetLowerBaseContainer().getDisplayContext(),
	  0,			// x0
	  0,			// y0
	  GuiFoundation::SCREEN_WIDTH_PIXELS,
	  AREA_HEIGHT_);

  return ModeSettingsCapture_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
 
void    
ModeSettingsCapture::SoftFault(  const SoftFaultID softFaultID
                           , const Uint32      lineNumber
                           , const char *      pFileName
                           , const char *      pPredicate )
{
    FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
		GUI_Serial_Interface::MODE_SETTINGS_CAPTURE_CLASS, 
		lineNumber, pFileName, pPredicate);
}
