#ifndef VgaTriangle_HH
#define VgaTriangle_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VgaTriangle - Interface class for VGA triangle-drawing functions.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaTriangle.hhv   25.0.4.0   19 Nov 2013 14:38:22   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001   By: sah    Date:  01-Aug-1997    DCS Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "VgaGraphicsDriver.hh"

//@ Usage-Classes
#include "VgaLine.hh"
//@ End-Usage

class DisplayContext;
class GraphicsContext;

class VgaTriangle 
{
  public:
    static void  DrawTriangle(DisplayContext & rDc, GraphicsContext & rGc,
				  Int x0, Int y0,
				  Int x1, Int y1,
				  Int x2, Int y2 );

    static void  FillTriangle(DisplayContext & rDc, GraphicsContext & rGc,
				  Int x0, Int y0,
			      Int x1, Int y1,
			      Int x2, Int y2 );
			      

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    static void  FillUp_  (DisplayContext & rDc, GraphicsContext & rGc,
			  Int ax, Int ay,
			  Int bx, Int by,
			  Int dx, Int dy,
			  Int y0 );

    static void  FillDown_(DisplayContext & rDc, GraphicsContext & rGc,
			  Int ax, Int ay,
			  Int bx, Int by,
			  Int cx, Int cy,
			  Int y0 );
};


#endif // VgaTriangle_HH 
