
// ****************************************************************************
//
//  UtilityFunctions.cpp - This contains misc global functions which are not
//          associated with a class.
//
// ****************************************************************************

#include "StdAfx.h"
#include "UtilityFunctions.h"

// To disable file validation comment out the following line.
//#define VALIDATE_FILE_REQUIRED

//********************************************************************
//
//  ValidateFile - 
//  This function shall return true if the 32-bit CRC calculated from
//  the FileName matches with the CRC read from the CrcFileName.
//  Otherwise, this function shall return false.
//
//********************************************************************
bool ValidateFile(LPCTSTR FileName, LPCTSTR CrcFileName)
{
#ifdef VALIDATE_FILE_REQUIRED
	// open the file to calculate the CRC
    HANDLE FileHandle = CreateFile(FileName,
                                   GENERIC_READ,
                                   0,
                                   NULL,
                                   OPEN_EXISTING,
                                   FILE_ATTRIBUTE_NORMAL,
                                   NULL);

    if(FileHandle == INVALID_HANDLE_VALUE)
    {
        return false;
    }

    // open the Crc file
    HANDLE CrcFileHandle = CreateFile(CrcFileName,
                                      GENERIC_READ,
                                      0,
                                      NULL,
                                      OPEN_EXISTING,
                                      FILE_ATTRIBUTE_NORMAL,
                                      NULL);

    if(CrcFileHandle == INVALID_HANDLE_VALUE)
    {
        CloseHandle(FileHandle);

        return false;
    }

    DWORD FileSize    = GetFileSize(FileHandle,NULL);     // find out the file size
    DWORD CrcFileSize = GetFileSize(CrcFileHandle,NULL);  // find out the Crc file size
    unsigned long CrcFromFile   = 0;
    unsigned long CalculatedCrc = 0;

    // handle the empty file case
    if(FileSize == 0 || CrcFileSize == 0)
    {
        CloseHandle(FileHandle);

        CloseHandle(CrcFileHandle);

        return false;
    }

    // allocate a memory to hold the data read from the file
    BYTE* Buffer = new BYTE[FileSize];

    if(!Buffer)
    {
        CloseHandle(FileHandle);

        CloseHandle(CrcFileHandle);
        
        RETAILMSG(TRUE, (_T("Failed to allocate memory to check crc\r\n")));

        return false;
    }
    
    DWORD BytesRead;

    // read data from the file into memory, so we can calculate the crc
    ReadFile(FileHandle,Buffer,FileSize,&BytesRead,NULL);

    // incorrect number of bytes read?
    if(BytesRead != FileSize)
    {
        if(Buffer != NULL)
        {
            delete [] Buffer;
        }

        CloseHandle(FileHandle);

        CloseHandle(CrcFileHandle);

        return false;
    }
    
    // this file is no longer needed, close it
    CloseHandle(FileHandle);
    
    // read the stored crc into the local variable CrcFromFile
    ReadFile(CrcFileHandle,&CrcFromFile,CrcFileSize,&BytesRead,NULL);

    // incorrect number of bytes read?
    if(BytesRead != CrcFileSize)
    {
        if(Buffer != NULL)
        {
            delete [] Buffer;
        }

        CloseHandle(CrcFileHandle);

        return false;
    }

    CalculatedCrc = CalculateCrc((BYTE *)Buffer,FileSize);

    // compare the crc here
    if(CalculatedCrc != CrcFromFile)
    {
        if(Buffer != NULL)
        {
            delete [] Buffer;
        }

        CloseHandle(CrcFileHandle);

        return false;
    }

    // deallocate the buffer
    if(Buffer != NULL)
    {
        delete [] Buffer;
    }

    // the crc file is no longer needed, close it
    CloseHandle(CrcFileHandle);

    RETAILMSG(TRUE, (_T("ExLaunch: The file %s passed the validation!\r\n"), FileName, GetLastError()));
    RETAILMSG(TRUE, (_T("ExLaunch: The file %s passed the validation!\r\n"), CrcFileName, GetLastError()));
#endif	// VALIDATE_FILE_REQUIRED
    return true;

}
