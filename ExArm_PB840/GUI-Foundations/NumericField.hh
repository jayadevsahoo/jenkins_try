#ifndef NumericField_HH
#define NumericField_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: NumericField - This is a Drawable object which displays a numeric
// value.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/NumericField.hhv   25.0.4.0   19 Nov 2013 14:11:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//  Removed SUN prototype code.
//
//  Revision: 009   By: rhj    Date:  05-Feb-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//  Added a function to change the font size on the fly.
//
//  Revision: 008   By: rhj    Date:  02-Oct-2006    SCR Number: 6236
//  Project: RESPM
//  Description:
//	Added default constructor parameters. 
//
//  Revision: 007   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 006   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 005  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 004  By: yyy      Date: 03-Apr-1997  DCR Number: 1885
//    Project:  Sigma (R8027)
//    Description:
//      Change MAX_NUMERIC_STRING_LENGTH to public scope
//
//  Revision: 003  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 002  By: mpm      Date: 02-Aug-1995   DCR Number: 486
//    Project:  Sigma (R8027)
//    Description:
//      Made destructor virtual as per coding standard requirements.
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "Drawable.hh"
#include "TextFont.hh"

//@ Usage-Classes
#include "Precision.hh"
//@ End-Usage

class NumericField : public Drawable
{
public:
	NumericField(TextFont::Size size=TextFont::EIGHTEEN, 
				 Uint numberOfDigits=0,
				 Alignment alignment=CENTER);

	virtual ~NumericField();

	void setValue(Real32 value);
	void setItalic(Boolean isItalic);
	void setPrecision(Precision precision);
	void render(wchar_t * pString); 
    void setFontSize(TextFont::Size pointSize);

	inline Real32 getValue(void) const;
	inline Boolean getItalic(void) const;
	inline Precision getPrecision(void) const;

	// virtual from Drawable
	virtual void draw(const Area &clipArea);

	static void SoftFault(const SoftFaultID softFaultID,
	   const Uint32      lineNumber,
	   const char*       pFileName  = NULL, 
	   const char*       pPredicate = NULL);

	//@ Data-Member: MAX_NUMERIC_STRING_LENGTH
	// The maximum length of a numeric string, includes all digits and
	// any decimal point character.  The numberOfDigits_ data member cannot
	// exceed this value.
	static const Int MAX_NUMERIC_STRING_LENGTH;

protected:

private:
	NumericField(const NumericField&);         // Declared, but not implemented
	void operator=(const NumericField&);       // Declared, but not implemented

	FontID getFont_(void) const;

	//@ Data-Member: pointSize_
	// This enumeration represents the font size used to display the number.
	TextFont::Size pointSize_;

	//@ Data-Member: value_
	// This is the value which is displayed on the screen. It is stored 
	// here in an unrounded form.
	Real32 value_;

	//@ Data-Member: numberOfDigits_
	// This is the maximum number of digits that can be displayed in the 
	// NumericField.
	Uint numberOfDigits_;

	//@ Data-Member: precision_
	// This is the number of digits that are displayed on the 
	// right hand side of the decimal point. 
	Precision precision_;

	//@ Data-Member: alignment_
	// This member stores the alignment of the number.  The Alignment type
	// is defined globally in Area.hh.
	Alignment alignment_;

	//@ Data-Member: isItalic_
	// This typeface of the number is either italic or non-italic.
	// When this member is TRUE it is italic. It defaults to FALSE.
	Boolean isItalic_;

};

// Inlined methods
#include "NumericField.in"

#endif // NumericField_HH 
