;
; Copyright (c) Microsoft Corporation.  All rights reserved.
;
;
; Use of this sample source code is subject to the terms of the Microsoft
; license agreement under which you licensed this sample source code. If
; you did not accept the terms of the license agreement, you are not
; authorized to use this sample source code. For the terms of the license,
; please see the license agreement between you and Microsoft or, if applicable,
; see the LICENSE.RTF on your install media or the root of your tools installation.
; THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
;
; Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

;
;==============================================================================
;             Texas Instruments OMAP(TM) Platform Software
; (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
;
; Use of this software is controlled by the terms and conditions found
; in the license agreement under which this software has been supplied.
;
;==============================================================================
;
;------------------------------------------------------------------------------
;
;  File:  memory_cfg.inc
;
;  This file is used to define g_oalAddressTable. This table is passed to
;  KernelStart to estabilish physical to virtual memory mapping. This table
;  is used also in memory OAL module to map between physical and virtual
;  memory addresses via OALPAtoVA/OALVAtoPA functions.
;
;  The config.bib file defines image memory layout ant it contains virtual
;  cached memory addresses which must be synchronized with g_oalAddressTable.
;  With each table change make sure that those constant are still valid.
;
;------------------------------------------------------------------------------
;  Export Definition
        EXPORT  g_oalAddressTable[DATA]
        EXPORT  g_oalEbootAddressTable[DATA]
;
; !!! IF YOU MODIFY THE TABLE YOU MUST MODIFY 
; !!! "THE MEMORY_REGISTER_ENTRY _pRegEntries" in the
; !!! \platform\common\src\soc\COMMON_TI_V1\AM389X\CEDDK\MAP\map.c
; 
;----------------------- CE7 Device Table ------------------------------------------------ 
g_oalCE7DeviceTable
        DCD     0x8F000000, 0x8F0000,  0x1000000,  0x00C00      ; 16MB uncached memory for device buffers 
        DCD     0x90000000, 0x480000,  0x0300000,  0x00C00      ; Slow Periferal domain L4
        DCD     0x90300000, 0x4A0000,  0x0200000,  0x00C00      ; Fast Periferal domain L4
        DCD     0x90500000, 0x47C000,  0x0400000,  0x00C00      ; L4 Firewall Pripheral
        DCD     0x90900000, 0x403000,  0x0100000,  0x00C00      ; OCMC SRAM
        DCD     0x90A00000, 0x080000,  0x1000000,  0x00C00      ; NAND Flash SC0  
        DCD     0x91A00000, 0x460000,  0x1000000,  0x00C00      ; McASP & HDMI  
        DCD     0x92A00000, 0x470000,  0x0800000,  0x00C00      ; McBSP & USB/CPPI
        DCD     0x93200000, 0x490000,  0x0100000,  0x00C00      ; TPCC  
        DCD     0x93300000, 0x498000,  0x0400000,  0x00C00      ; TPTC
        DCD     0x93700000, 0x550000,  0x1000000,  0x00C00      ; Ducati
        DCD     0x94700000, 0x560000,  0x1000000,  0x00C00      ; SGX530

;        DCD     0x95700000, 0x580000,  0x1000000,  0x00C00      ; IVAHD0 host port
;        DCD     0x97300000, 0x590000,  0x1000000,  0x00C00      ; IVAHD0 SL2 port
        
;========================================================================================
; Memory allocation for Display/DSS.
; If you modify that memory allocation, update OEMRamTable accordingly !!!
; Detailed breakup of Display memory can be found in BSP\AM387X_BSP\FILES\ti816x_hdvpss.xem3.map
;--------------------------------------------------------------------------------------
        DCD     0x8A200000, 0x8A2000, 0x4E00000, 0x00C00    ; HDVPSS Code, Data, Syslink shared: 78MB(0x8A20000-0x8F000000)
; 16MB from 0x8F00000-0x8FFFFFFF reserved for WinCE driver
        DCD     0xA0000000, 0xA00000, 0x0400000, 0x00C00    ; HDVPSS desc mem, shared mem: 4MB(0xA0000000-0xA0400000). Frame buf does not need static mapping
;======================================================================================
;       DCD     0xA0400000, NEXT    , 0x1000000, 0x00C00    ; Placeholder 
        DCD     0x00000000, 0x000000, 0x0000000, 0x00000    ; end of table

;======================================================================================
;  Table format: cached address, physical address, size

g_oalAddressTable
        DCD     0x87654321, g_oalCE7DeviceTable,0  ; Flag to use new CE7 mapping scheme
        DCD     0x80000000, 0x80000000, 162        ; SDRAM
        DCD     0x98000000, 0xA8000000,  96        ; SDRAM for RAMDISK on second bank
        DCD     0x00000000, 0x00000000,   0        ; end of table
 
;------------------------------------------------------------------------------

; Table used in eboot only when enabling mmu
g_oalEbootAddressTable
        DCD     0x80000000, 0x80000000,  256            ; 0x10000000 SDRAM
        DCD     0x90000000, 0x48000000,  3              ; Slow Periferal domain L4
        DCD     0x90300000, 0x4A000000,  2              ; Fast Periferal domain L4
        DCD     0x90500000, 0x47C00000,  4              ; L4 Firewall Pripheral
        DCD     0x90900000, 0x40300000,  1              ; OCMC SRAM
        DCD     0x90A00000, 0x08000000,  16             ; NAND Flash SC0  
        DCD     0x91A00000, 0x46000000,  16             ; McASP & HDMI  
        DCD     0x92A00000, 0x47000000,  8              ; McBSP & USB/CPPI
        DCD     0x93200000, 0x49000000,  1              ; TPCC  
        DCD     0x93300000, 0x49800000,  4              ; TPTC
        DCD     0x93370000, 0x50000000,  1              ; GPMC
        DCD     0x00000000, 0x00000000,  0              ; end of table

        END
