
#ifndef VgaBlink_HH
#define VgaBlink_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VgaBlink -- Provides blinking of display colors and LEDs.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaBlink.hhv   25.0.4.0   19 Nov 2013 14:37:34   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001   By: sah    Date:  01-Aug-1997    DCS Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "VgaGraphicsDriver.hh"
#include "GuiIoLed.hh"

//@ Usage-Classes
//@ End-Usage


struct RgbQuad;

class VgaBlink 
{
  public:
    //@  Type:  BlinkStatus
    //  Blink status enumerations.
    enum BlinkStatus
    {
      BLINK_OFF,
      BLINK_ON,
      BLINK_SLOW,
      BLINK_FAST
    };

	//@Type: AlarmLedColors
	// Alarm Led Colors
	enum AlarmLedColors
	{
		ALARM_LED_COLOR_RED,
		ALARM_LED_COLOR_YELLOW,
		ALARM_LED_COLOR_RED_OFF,
		ALARM_LED_COLOR_YELLOW_OFF,
		ALARM_LED_COLOR_OFF
	};

    static void  Initialize(void);

    static void   SetBlinkColor(
		const Uint8  index, const VgaBlink::BlinkStatus status,
		const RgbQuad & rgbOn, const RgbQuad & rgbOff);
    static void   SetLedStatus(const Uint8 index, const Uint8 status);

    static VgaBlink::BlinkStatus  GetLedStatus(const Uint8 index);

    static void  TaskDriver(void);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

#if defined(SIGMA_UNIT_TEST)
	static VgaBlink::BlinkStatus GetBlinkStatus(const Uint8  index);
#endif
protected:
	void static SetAlarmLedColor(AlarmLedColors LedColor);

  private:
    //@ Constant:  BLINK_PERIOD_
    // Period of time, in milliseconds, between processing of the blink
    // states.
    static const Uint  BLINK_PERIOD_;
};


#endif // VgaBlink_HH 
