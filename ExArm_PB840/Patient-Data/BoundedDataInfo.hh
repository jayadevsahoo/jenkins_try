
#ifndef BoundedDataInfo_HH
#define BoundedDataInfo_HH

#if        defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//       Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class: BoundedDataInfo -  Breath Data Processing agent class 
//      for those patient data items that have upper and lower boundary
//    value requirements along with a precision requirement.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/BoundedDataInfo.hhv   10.7   08/17/07 10:27:10   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//  
//  Revision: 002  By:  yyy    Date:  27-Jan-98    DR Number:
//    Project:  Sigma (R8027)
//    Description:
//        Initial version for Bilevel.
//  
//  Revision: 001  By:  jhv    Date:  26-Jan-94    DR Number:
//    Project:  Sigma (R8027)
//    Description:
//        Initial version (Integration baseline).
//
//====================================================================


#include  "PatientDataId.hh"
#include  "BreathItemDataInfo.hh"
#include  "BreathDatum.hh"
class PEEPDataInfo;
class InspPauseDataInfo;

#include  "BufferIndex.hh"

class BoundedDataInfo:public BreathItemDataInfo 
{
  friend class PEEPDataInfo;
  friend class InspPauseDataInfo;
#if	defined(SIGMA_UNIT_TEST)
  friend void DoTest(void);
#endif //defined(SIGMA_UNIT_TEST)

  public:
    BoundedDataInfo(const PatientDataId::PatientItemId id, const Real32 min,
    const Real32 max, const Real32 changePoint,
    const Precision lowerP, const Precision higherP);
    virtual ~BoundedDataInfo(void); 
    virtual void setDataTimeout(void);

    static void  SoftFault(const SoftFaultID softFaultID, 
						   const Uint32      lineNumber, 
						   const char*       pFileName = NULL, 
						   const char*       pBoolTest = NULL); 
  protected:
    virtual void modifyData_(void *pValue);
    virtual Real32  processData_(Real32 rawValue);

    //@ Constant:    MIN_VALUE_
    // This is the minimum value allowed for this data type. 
    const Real32  MIN_VALUE_;
     
    //@ Constant:    MAX_VALUE_
    // This is the maximum value allowed for this data type. 
    const Real32  MAX_VALUE_;
     
    //@ Constant:    LOWER_PRECISION_
    // This is a precision used for a data value that is smaller
    // than the mid change point.
    const Precision  LOWER_PRECISION_;

    //@ Constant:    MID_CHANGE_POINT_
    // This is a precision delimeter value.
    const Real32  MID_CHANGE_POINT_;

    //@ Constant:    UPPER_PRECISION_
    // This is a precision used for a data value larger than the 
    // mid change point.
    const Precision  UPPER_PRECISION_;

  private:
    BoundedDataInfo(void);                            // not implemented ...
    BoundedDataInfo(const BoundedDataInfo& range); // not implemented ...
    void  operator=(const BoundedDataInfo&);       // not implemented...
};


#endif        //defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

#endif  // BoundedDataInfo_HH
