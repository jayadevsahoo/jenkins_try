
// ****************************************************************************
//
//  UtilityFunctions.h - This contains misc global functions which are not
//          associated with a class.
//
// ****************************************************************************

#ifndef UTILITY_FUNCTIONS_H

#define UTILITY_FUNCTIONS_H

#include "SigmaTypes.hh"

void InitiateSystemRestart();

void TrapDebugAssert(TCHAR *SourceFile, uint32_t SourceLine);
void TrapDebugAssert(TCHAR *Message, TCHAR *SourceFile, uint32_t SourceLine);


bool FileExists(LPCTSTR FileName);
bool DeleteDirectory(LPCTSTR SourcePath);

bool ValidFileCrc(LPCTSTR FileName, LPCTSTR CrcFileName);


#endif



