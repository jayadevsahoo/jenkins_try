#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ScreenContrastHandler - Handles setting of Screen Contrast 
// (initiated by the Screen Contrast off-screen key).
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class dedicated to the setting of the Screen Contrast.  The
// Screen Contrast is set by pressing down the Screen Contrast offscreen key and
// while keeping it pressed, using the rotary knob to adjust.
//
// The keyPanelPressHappened() and keyPanelReleaseHappened() methods
// inform us of Screen Contrast key events.  We use the adjustPanel?() methods
// to trap knob events and valueUpdateed() tells us when the
// Screen Contrast setting changes so that we can communicate this event
// to the GUI-IO-Devices system.
//---------------------------------------------------------------------
//@ Rationale
// Something needs to handle the Screen Contrast key and this is it.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply register for Screen Contrast Key event callbacks
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScreenContrastHandler.ccv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012   By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
// 		Removed SUN prototype code.
//
//  Revision: 011   By: hlg    Date:  20-Sep-2001    DCS Number: 5947
//  Project: GUIComms
//  Description:
// 		Removed callback to display messages when using the cost
// 		reduced GUI this because key is not functional.
//
//  Revision: 010   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 009  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 007  By:  sah	   Date:  15-Sep-1998    DCS Number: 5133
//  Project:  Color
//  Description:
//		Added mapping for new [CL01003] requirement.
//
//  Revision: 006  By:  gdc    Date:  02-Sep-1998    DCS Number: 5153
//  Project:  Color
//  Description:
//      Added traceable ID's for Color Project requirements.
//
//  Revision: 005  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/ScreenContrastHandler.ccv   1.25.1.0   07/30/98 10:19:34   gdc
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By:  sah   Date:  16-Jul-1997    DCS Number:  2270
//  Project:  Sigma (R8027)
//  Description:
//      Fixed problem where, at a certain point, the adjustment of contrast
//      delta setting towards its minimum, would result in a "jump" to
//      the maximum value.  This problem was caused by a summation of
//      two signed integers being stored into an unsigned integer.  Also,
//      moved this code to a new static method, for use by other classes
//      as well.
//
//  Revision: 002  By:  sah   Date:  12-Jun-1997    DCS Number:  2208
//  Project:  Sigma (R8027)
//  Description:
//      Modified code to incorporate new VGA interface.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//@ Version-Information
//=====================================================================
#include "ScreenContrastHandler.hh"

//@ Usage-Classes
#include "VgaDevice.hh"

#include "AdjustPanel.hh"
#include "BoundStrs.hh"
#include "KeyPanel.hh"
// TODO E600 remove
//#include "OsUtil.hh"			// for ::IsUpperDisplayColor() ::IsGuiCommsConfig()
#include "PromptArea.hh"
#include "SettingConstants.hh"
#include "Sound.hh"
#include "MiscStrs.hh"
#include "MessageArea.hh"
#include "SettingSubject.hh"
#include "BoundStatus.hh"
#include "GuiApp.hh"
//@ End-Usage

//@ Code...
// Minimum contrast value is 0; Maximum contrast value is 255
const Uint16 ScreenContrastHandler::MIN_CONTRAST_ = 0;	
const Uint16 ScreenContrastHandler::MAX_CONTRAST_ = 255;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ScreenContrastHandler()  [Default Constructor]
//
//@ Interface-Description
// Creates the Screen Contrast object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Register for Screen Contrast Key event callbacks and Screen Contrast setting
// changes.    Call valueUpdateed() in order to initialize the
// display with the current value of Screen Contrast.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScreenContrastHandler::ScreenContrastHandler(void)
{
	CALL_TRACE("ScreenContrastHandler::ScreenContrastHandler(void)");

	// Register for Screen Contrast key presses
	if (!::IsGuiCommsConfig())
	{  // $[TI3] -- only functional if button is labeled (old gui)...
		KeyPanel::SetCallback(KeyPanel::SCREEN_CONTRAST_KEY, this);
	}  // $[TI4]

	if (!::IsUpperDisplayColor())
	{  // $[TI1] -- only monitor if non-color screens...
		attachToSubject_(SettingId::DISPLAY_CONTRAST_SCALE,
						 Notification::VALUE_CHANGED);
		attachToSubject_(SettingId::DISPLAY_CONTRAST_DELTA,
						 Notification::VALUE_CHANGED);
	}  // $[TI2] -- don't monitor if color screens...

	// Fake a setting change event so as to initialize the display with the
	// current screen contrast value.
	valueUpdate(Notification::ACCEPTED,
				getSubjectPtr_(SettingId::DISPLAY_CONTRAST_SCALE));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ScreenContrastHandler  [Destructor]
//
//@ Interface-Description
//	Destroys the Screen Contrast object.  Does nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	n/a 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ScreenContrastHandler::~ScreenContrastHandler(void)
{
	CALL_TRACE("ScreenContrastHandler::~ScreenContrastHandler(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelPressHappened
//
//@ Interface-Description
// Called when the Screen Contrast key is pressed.  We begin the Screen Contrast
// adjustment process.
//---------------------------------------------------------------------
//@ Implementation-Description
// We grab the Adjust Panel focus (for knob events), and post appropriate 
// prompts.  If Settings has been locked out then we ignore the event.
// $[01237] While the display contrast key is held down, the Gui shall ...
// $[CL01003] Display Contrast is available for grey-scale only.
// $[CL02002] Display Contrast is available for grey-scale only.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenContrastHandler::keyPanelPressHappened(KeyPanel::KeyId)
{
	CALL_TRACE("ScreenContrastHandler::keyPanelPressHappened(KeyPanel::KeyId)");

	// Grab the Adjust Panel focus
	AdjustPanel::TakePersistentFocus(this);

	// $[CL02002] option available for grey-scale only
	if ( !IsUpperDisplayColor() )
	{ 														// $[TI1.1]
		// Display a Primary and Secondary prompt, and clear Advisory prompt
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
					PromptArea::PA_HIGH, PromptStrs::USE_KNOB_TO_ADJUST_P);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, PromptStrs::SCREEN_CONTRAST_CANCEL_S);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
	}
	else
	{														// $[TI1.2]
		// Display Primary and Secondary prompts, and clear Advisory prompt
		// not available on color screens
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, 
					PromptArea::PA_HIGH, PromptStrs::EMPTY_A);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, PromptStrs::SCREEN_CONTRAST_NOT_AVAILABLE_A);
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
					PromptArea::PA_HIGH, PromptStrs::EMPTY_A);

		// Make invalid sound
		Sound::Start(Sound::INVALID_ENTRY);
	}

	// Display the offscreen key help message 
	GuiApp::PMessageArea->setMessage(
							MiscStrs::SCREEN_CONTRAST_HELP_MESSAGE,
									MessageArea::MA_HIGH);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: keyPanelReleaseHappened
//
//@ Interface-Description
// Called when the Screen Contrast key is released.  Stop the Screen Contrast
// adjustment.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Call AdjustPanel class method TakePersistentFocus to clear the focus. 
//	Simply turn off the offscreen help message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenContrastHandler::keyPanelReleaseHappened(KeyPanel::KeyId)
{
	CALL_TRACE("ScreenContrastHandler::keyPanelReleaseHappened(KeyPanel::KeyId)");

	// Clear the Adjust Panel focus
	AdjustPanel::TakePersistentFocus(NULL);

	//								 $[TI1]
	// Clear the off screen key message
	GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelKnobDeltaHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  If this
// object has the Adjust Panel focus (as we do during the Screen Contrast
// adjustment) then we will be called when the operator turns the knob.  It is
// passed an integer corresponding to the amount the knob was turned.  We
// inform the Settings-Validation subsystem of the delta value and any
// resultant hard bound violation is displayed in the Prompt Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// Any deltas received by this button are sent immediately to the setting via
// the calcNewValue() method of ScreenContrastHandle_.  If the delta causes this
// setting to change then an update will be passed by the Settings-Validation
// subsystem back to us via the SettingObserver before the calcNewValue()
// method returns.  The return value of calcNewValue() indicates the current
// hard bound status of the Screen Contrast setting.  If there is a "hard bound
// violation" we will display an Advisory message in the Prompt Area.
//
// $[01236] The gui shall put the new display contrast setting into effect ..
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenContrastHandler::adjustPanelKnobDeltaHappened(Int32 delta)
{
	CALL_TRACE("ScreenContrastHandler::adjustPanelKnobDeltaHappened(Int32 delta)");

	if ( !::IsUpperDisplayColor() )
	{														// $[TI3.1]
		SettingSubject*  pSubject =
							getSubjectPtr_(SettingId::DISPLAY_CONTRAST_SCALE);

		// Pass the knob delta on to the Screen Contrast setting
		const BoundStatus&  boundStatus = pSubject->calcNewValue(delta);

		// Get the string id appropriate to the hard bound id (if any)
		StringId hardBoundStr =
					BoundStrs::GetSettingBoundString(boundStatus.getBoundId());

		// Display the Advisory message corresponding to the hard bound id
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, 
						PromptArea::PA_HIGH,
						hardBoundStr == NULL_STRING_ID ?
						PromptStrs::EMPTY_A : hardBoundStr);
	
		// If a hard bound was struck then make the Invalid Entry sound
		if (hardBoundStr != NULL_STRING_ID)
		{													// $[TI1]
			Sound::Start(Sound::INVALID_ENTRY);
		}													// $[TI2]
	}
	else
	{														// $[TI3.2]
		// control not applicable to color
		Sound::Start(Sound::INVALID_ENTRY);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses down the Accept key when we have the
// Adjust Panel focus.  This is an invalid operation so sound the Invalid
// Entry sound.
//---------------------------------------------------------------------
//@ Implementation-Description
// Tell GUI-Foundation's Sound class to make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenContrastHandler::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("ScreenContrastHandler::adjustPanelAcceptPressHappened(void)");

	// Make the Invalid Entry sound
	Sound::Start(Sound::INVALID_ENTRY);					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// called when the operator presses the Clear key.  This is an invalid
// operation so sound the Invalid Entry sound.
//---------------------------------------------------------------------
//@ Implementation-Description
// Tell GUI-Foundation's Sound class to make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenContrastHandler::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("ScreenContrastHandler::adjustPanelClearPressHappened(void)");

	// Make the Contact sound
	Sound::Start(Sound::INVALID_ENTRY);					// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelLoseFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is called when the Screen Contrast is about to lose the focus of
// the Adjust Panel.  This normally happens when some other button is
// pressed down or when the Screen Contrast key is pressed up.  We must
// terminate the Screen Contrast adjustment.
//---------------------------------------------------------------------
//@ Implementation-Description
// We tell GUI-IO-Devices to stop adjusting the Screen Contrast
// and remove any prompts we may have displayed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenContrastHandler::adjustPanelLoseFocusHappened(void)
{
	CALL_TRACE("ScreenContrastHandler::adjustPanelLoseFocusHappened(void)");

	// Cancel the Screen Contrast (call the GUI-IO-Devices subsystem's function)
	// Function to be implemented.

	// Clear the Primary, Secondary and Advisory prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
									PromptArea::PA_HIGH, NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
									PromptArea::PA_HIGH, NULL_STRING_ID);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
// Called to update the display with the current screen contrast value. 
// Passed the following parameters:
// >Von
//	qualifierId	The context in which the change happened, either
//  pSubject	Pointer to the Setting's Context Subject.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// The Screen Contrast has changed so get its value and send it on to
// the VGA-Graphics-Driver system.  Note that the value of screen contrast
// stored in settings is a percentage value (0 to 100) and we use this
// value to pick a relative VGA contrast value between the predetermined
// min and max contrast values allowable.
// $[01236] The gui shall put the new display contrast setting into effect ..
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ScreenContrastHandler::valueUpdate(
								const Notification::ChangeQualifier qualifierId,
							    const SettingSubject*               pSubject
								  )
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	const SettingSubject*  pContrastScale =
							getSubjectPtr_(SettingId::DISPLAY_CONTRAST_SCALE);
	const SettingSubject*  pContrastDelta =
							getSubjectPtr_(SettingId::DISPLAY_CONTRAST_DELTA);

	// get the current contrast setting values...
	const SequentialValue  RAW_CONTRAST_SCALE =
										  pContrastScale->getAcceptedValue();
	const SequentialValue  RAW_CONTRAST_DELTA =
				(qualifierId == Notification::ACCEPTED)
				 ?  pContrastDelta->getAcceptedValue()			// $[TI1]
				 :  pContrastDelta->getAdjustedValue();			// $[TI2]

	// Scale the RAW_CONTRAST_SCALE and RAW_CONTRAST_DELTA values to the
	// allowable contrast range (must use signed integer)...
	const Int32  COOKED_UPPER_CONTRAST = MIN_CONTRAST_ +
				(RAW_CONTRAST_SCALE * (MAX_CONTRAST_ - MIN_CONTRAST_)) 
							/ SettingConstants::MAX_DISPLAY_CONTRAST_SCALE;
	const Int32 COOKED_CONTRAST_DELTA = MIN_CONTRAST_ +
				(RAW_CONTRAST_DELTA * (MAX_CONTRAST_ - MIN_CONTRAST_)) 
							/ SettingConstants::MAX_DISPLAY_CONTRAST_DELTA;

	// Lower screen contrast is calculated as the upper screen constrast value
	// plus contrast delta.
	Int32  cookedLowerContrast = (COOKED_UPPER_CONTRAST + COOKED_CONTRAST_DELTA);

	if (cookedLowerContrast < MIN_CONTRAST_)
	{													// $[TI3]
		cookedLowerContrast = MIN_CONTRAST_;
	}													// $[TI4]

	if (cookedLowerContrast > MAX_CONTRAST_)
	{													// $[TI5]
		cookedLowerContrast = MAX_CONTRAST_;
	}													// $[TI6]
			
	VgaDevice::SetContrast(cookedLowerContrast, COOKED_UPPER_CONTRAST);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ScreenContrastHandler::SoftFault(const SoftFaultID  softFaultID,
								    const Uint32       lineNumber,
								    const char*        pFileName,
								    const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, 
							SCREENCONTRASTHANDLER,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
