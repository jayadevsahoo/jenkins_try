#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaMinInspFlowSetting - Apnea Minimum Inspiratory Flow Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the minimum rate of
//  delivery (in liters-per-minute) of the specified "tidal volume" during
//  mandatory, volume-based apnea breaths (apnea VCV).  This class inherits
//  from 'BatchBoundedSetting' and provides the specific information needed
//  for representation of apnea minimum inspiratory flow.  This information
//  includes the interval and range of this setting's values, this setting's
//  response to a Main Control Setting's transition (see
//  'acceptTransition()'), and this batch setting's new-patient value (see
//  'getNewPatientValue()').
//
//  This class provides a static method for calculating apnea minimum
//  inspiratory flow based on the value of other settings.  This calculation
//  method provides a standard way for all settings (as needed) to calculate
//  an apnea minimum inspiratory flow value.
//
//  This class cannot be directly changed by the operator, yet it is not
//  fixed.  This setting is strictly changed via changes to apnea peak
//  inspiratory flows, via a dependent relationship.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings, or dynamic contraints.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaMinInspFlowSetting.ccv   25.0.4.0   19 Nov 2013 14:27:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: sah   Date:  16-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  modified 'getApplicability()' to base this tracking setting's
//         applicability purely on the applicability of the settings it
//         tracks
//      *  changed this setting from a "dependent setting" of apnea peak
//         insp flow, to using the Observer/Subject mechanism as a
//         "tracking setting"; as a tracking setting, this now needs to
//         override some 'SettingObserver' methods ('valueUpdate()',
//         'doRetainAttachment()' and 'settingObserverInit()'), and no
//         longer needs dependent and transition methods
//         ('acceptPrimaryChange()' and 'acceptTransition()')
//
//  Revision: 005   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  removed now-unneeded 'calcNewValue()'
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged  Rev "Color" (1.19.1.0) into Rev "BiLevel" (1.19.2.0)
//	Modified resolution to improve accuracies of calculations
//	based on this setting.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaMinInspFlowSetting.hh"
#include "MandTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02074] -- The setting's range...which is based on [02083]...
//  $[02076] -- The setting's resolution...
static const BoundedInterval  LAST_NODE_ =
  {
    0.8f,		// absolute minimum...
    0.0f,		// unused...
    HUNDREDTHS,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    10000.0f,		// absolute maximum...
    0.01f,		// resolution...
    HUNDREDTHS,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaMinInspFlowSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaMinInspFlowSetting::ApneaMinInspFlowSetting(void)
       : BatchBoundedSetting(SettingId::APNEA_MIN_INSP_FLOW,
			     Setting::NULL_DEPENDENT_ARRAY_,
			     &::INTERVAL_LIST_,
			     NULL_SETTING_BOUND_ID,	// maxConstraintId...
			     NULL_SETTING_BOUND_ID)	// minConstraintId...
{
  CALL_TRACE("ApneaMinInspFlowSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaMinInspFlowSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaMinInspFlowSetting::~ApneaMinInspFlowSetting(void)
{
  CALL_TRACE("~ApneaMinInspFlowSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaMinInspFlowSetting::getApplicability(
			      const Notification::ChangeQualifier qualifierId
				     ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pApneaPeakFlow =
		   SettingsMgr::GetSettingPtr(SettingId::APNEA_PEAK_INSP_FLOW);

  const Applicability::Id  APNEA_PEAK_FLOW_APPLIC_ID =
				 pApneaPeakFlow->getApplicability(qualifierId);

  return((APNEA_PEAK_FLOW_APPLIC_ID != Applicability::INAPPLICABLE)
	  ? Applicability::VIEWABLE		// $[TI1]
	  : Applicability::INAPPLICABLE);	// $[TI2]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaMinInspFlowSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  const BoundedValue  NEW_PATIENT_APNEA_MIN_FLOW =
			 calcBasedOnSetting_(SettingId::APNEA_PEAK_INSP_FLOW,
					     BoundedRange::WARP_NEAREST,
					     BASED_ON_NEW_PATIENT_VALUES);

  return(NEW_PATIENT_APNEA_MIN_FLOW);
}   // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to value changes of tidal volume and respiratory rate, by
//  re-calculating current value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[VC02009] -- equal to (Vt * f * 0.0001)
//---------------------------------------------------------------------
//@ PreCondition
//  (pSubject->getId() == SettingId::TIDAL_VOLUME  ||
//   pSubject->getId() == SettingId::RESP_RATE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaMinInspFlowSetting::valueUpdate(
			    const Notification::ChangeQualifier qualifierId,
			    const SettingSubject*               pSubject
				    )
{
  CALL_TRACE("valueUpdate(qualifierId, pSubject)");
  SAFE_AUX_CLASS_PRE_CONDITION(
			(pSubject->getId() == SettingId::APNEA_PEAK_INSP_FLOW),
		        pSubject->getId()
			      );

  if (qualifierId == Notification::ADJUSTED)
  {  // $[TI1] -- peak flow's adjusted value changed...
    const BoundedValue  APNEA_MIN_INSP =
			 calcBasedOnSetting_(SettingId::APNEA_PEAK_INSP_FLOW,
					     BoundedRange::WARP_NEAREST,
					     BASED_ON_ADJUSTED_VALUES);

    // store the value...
    setAdjustedValue(APNEA_MIN_INSP);
  }  // $[TI2] -- don't care about accepted value changes...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
ApneaMinInspFlowSetting::doRetainAttachment(const SettingSubject*) const
{
  CALL_TRACE("doRetainAttachment(pSubject)");

  return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting needs to monitor the value of apnea peak inspiratory flow,
//  therefore this virtual method is overridden to provide a point during
//  initialization to attach to those (subject) settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaMinInspFlowSetting::settingObserverInit(void)
{
  CALL_TRACE("settingObserverInit()");

  attachToSubject_(SettingId::APNEA_PEAK_INSP_FLOW,
		   Notification::VALUE_CHANGED);
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaMinInspFlowSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      // is the the accepted apnea minimum inspiratory flow value consistent
      // with the accepted apnea peak inspiratory flow-based bound?...
      isValid = (getAcceptedValue() ==
		 calcBasedOnSetting_(SettingId::APNEA_PEAK_INSP_FLOW,
				     BoundedRange::WARP_NEAREST,
				     BASED_ON_ACCEPTED_VALUES));
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaMinInspFlowSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
    {
      // is the the adjusted apnea minimum inspiratory flow value consistent
      // with the adjusted apnea peak inspiratory flow-based bound?...
      isValid = (BoundedValue(Setting::getAdjustedValue()) ==
		 calcBasedOnSetting_(SettingId::APNEA_PEAK_INSP_FLOW,
				     BoundedRange::WARP_NEAREST,
				     BASED_ON_ADJUSTED_VALUES));
    }
  }

  return(isValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaMinInspFlowSetting::SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName,
				   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_MIN_INSP_FLOW_SETTING, lineNumber,
			  pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnSettingId == SettingId::APNEA_PEAK_INSP_FLOW  &&
//  (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//   basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
ApneaMinInspFlowSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
					    ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  AUX_CLASS_PRE_CONDITION((basedOnSettingId == SettingId::APNEA_PEAK_INSP_FLOW),
      			  basedOnSettingId);

  const Setting*  pApneaPeakInspFlow = 
		SettingsMgr::GetSettingPtr(SettingId::APNEA_PEAK_INSP_FLOW);

  Real32  apneaPeakInspFlowValue;

  switch (basedOnCategory)
  {
  case BASED_ON_NEW_PATIENT_VALUES :	// $[TI1]
    // get the new-patient value of apnea peak inspiratory flow...
    apneaPeakInspFlowValue =
	      BoundedValue(pApneaPeakInspFlow->getNewPatientValue()).value;
    break;
  case BASED_ON_ADJUSTED_VALUES :		// $[TI2]
    // get the "adjusted" value of apnea peak inspiratory flow...
    apneaPeakInspFlowValue =
	      BoundedValue(pApneaPeakInspFlow->getAdjustedValue()).value;
    break;
  case BASED_ON_ACCEPTED_VALUES :
    // fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
    // get the "accepted" value of apnea peak inspiratory flow...
    apneaPeakInspFlowValue =
	      BoundedValue(pApneaPeakInspFlow->getAcceptedValue()).value;
    break;
#endif // defined(SIGMA_DEVELOPMENT)
  default :
    // unexpected 'basedOnCategory' value..
    AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
    break;
  };

  BoundedValue  apneaMinInspFlow;

  // $[02019](1) -- formula for calculating apnea minimum inspriatory flow...
  apneaMinInspFlow.value = MAX_VALUE(0.8f,			      // $[TI3]
				     (apneaPeakInspFlowValue * 0.1f));// $[TI4]

  // warp the calculated value to a "click" boundary...
  getBoundedRange().warpValue(apneaMinInspFlow, warpDirection, FALSE);

  return(apneaMinInspFlow);
}
