//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file CDatabase.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"CDatabase.h"
#include"libpq-fe.h"
#include"CLogger.h"

//-------------------------------------
// C O D E
//-------------------------------------

//Dummy global used to return null result.
CCol CRow::m_Col;

//---------------------------
// CCol::CCol
//---------------------------
CCol::CCol():m_string()
{
}

//---------------------------
// CCol::get
//---------------------------
std::string &CCol::get()
{
    return m_string;
}

//---------------------------
// CCol::CCol
//---------------------------
CCol::CCol(std::string s):m_string(s)
{
}

//---------------------------
// CRow::CRow
//---------------------------
CRow::CRow(std::vector<std::string> &rColNameMap):
    m_ColNameMap(rColNameMap),
    m_CellVector()
{
}

//---------------------------
// CRow::operator=
//---------------------------
CRow& CRow::operator=(const CRow &rCRow)
{
    m_ColNameMap = rCRow.m_ColNameMap;
    return *this;
}

//---------------------------
// CRow::setCol
//---------------------------
bool CRow::setCol(std::string name, std::string value)
{
    std::vector<std::string>::iterator itr;
    unsigned int cnt = 0;

    for(itr = m_ColNameMap.begin(); itr != m_ColNameMap.end(); itr++)
    {
        if(name.compare(*itr) == 0)
        {
            m_CellVector.insert(std::pair<unsigned int,std::string>(cnt, value));
            return true;
        }
        cnt++;
    }
    return false;
}

//---------------------------
// CRow::getCol
//---------------------------
CCol &CRow::getCol(std::string name)
{
    std::vector<std::string>::iterator itr;
    unsigned int cnt = 0;

    for(itr = m_ColNameMap.begin(); itr != m_ColNameMap.end(); itr++)
    {
        if(name.compare(*itr) == 0)
        {
            return m_CellVector[cnt];   
        }
        cnt++;
    }
    return m_Col;   
}

//---------------------------
// CResult::CResult
//---------------------------
CResult::CResult():m_ColNameMap(),m_CRowVector(),m_CResult()
{
}

//---------------------------
// CResult::addColumn
//---------------------------
bool CResult::addColumn(std::string colName)
{
    m_ColNameMap.push_back(colName);
    return true;
}

//---------------------------
// CResult::getCRowVector
//---------------------------
const std::vector<CRow> &CResult::getCRowVector() const
{
    return m_CRowVector;
}

//---------------------------
// CResult::newCRow
//---------------------------
CRow *CResult::newCRow()
{
    m_CRowVector.push_back(CRow(m_ColNameMap));
    return &m_CRowVector.back();
}

//---------------------------
// CDatabase::CDatabase
//---------------------------
CDatabase::CDatabase(std::string ConnectionInfoString )
    :m_bConnected(false),m_pPGconn()
{

    m_DatabaseMutex = new Mutex();


    m_pPGconn = PQconnectdb(ConnectionInfoString.c_str());
    
    if(PQstatus(m_pPGconn) != CONNECTION_OK)
    {
        LOG_MESSAGE(LM_CRITICAL,"CDatabase(%X)::CDatabase Failed to connect w/ PQstatus=%i"
                ,this,PQstatus(m_pPGconn) );


        PQfinish(m_pPGconn);

        //TODO[ME]: Good place to throw.
    }
    else 
    {
        
        LOG_MESSAGE(LM_INFO,"Connected to database.");
        m_bConnected = true;
    }
}

//---------------------------
// CDatabase::~CDatabase
//---------------------------
CDatabase::~CDatabase()
{
    if(m_bConnected)
    {
        LOG_MESSAGE(LM_INFO,"Disconnected from database.");
        PQfinish(m_pPGconn);
        delete m_DatabaseMutex;
    }
}

//---------------------------
// CDatabase::insert_query
//---------------------------
bool CDatabase::insert_query(std::string query)
{ 
    PGresult *pPGresult; 
    bool bRetval = false;

    //TODO[ME]: On failure to run query, 
    //          check PQstatus(m_pPGconn) and recover w/ a PQreset 
    if(m_bConnected)
    {

        m_DatabaseMutex->lock();

        pPGresult    = PQexec(m_pPGconn, query.c_str());

        m_DatabaseMutex->unlock();


        if(pPGresult == NULL)
        {
            LOG_MESSAGE(LM_ERROR,"INSERT/UPDATE/VACCUM query failed w/ pPGresult"
                    " = NULL.");
			LOG_MESSAGE(LM_ERROR, (LPSTR)query.c_str());
            return false;
        }


        if(PQresultStatus(pPGresult) == PGRES_COMMAND_OK)
        {
            bRetval = true;
        }
        else
        {
            LOG_MESSAGE(LM_ERROR,"INSERT/UPDATE/VACCUM query failed w/ "
                    "PQresultStatus=%i"
                    ,PQresultStatus(pPGresult));

			LOG_MESSAGE(LM_ERROR, (LPSTR)query.c_str());
            //TODO[ME]: Should throw error.
        }

        PQclear(pPGresult);

    }
    return bRetval;
}

//---------------------------
// CDatabase::select_query
//---------------------------
CResult* CDatabase::select_query(std::string query)
{ 

    CResult *pCResult     = new CResult();

    //TODO[ME]: On failure to run query, 
    //          check PQstatus(m_pPGconn) and recover w/ a PQreset 
    if(m_bConnected)
    {
        PGresult *pPGresult; 

        m_DatabaseMutex->lock();

        pPGresult = PQexec(m_pPGconn, query.c_str());

        m_DatabaseMutex->unlock();

        if(pPGresult == NULL)
        {
            LOG_MESSAGE(LM_ERROR,"SELECT query failed w/ pPGresult = NULL.");
            return pCResult;
            //TODO[ME]: Should throw error.
        }

        if(PQresultStatus(pPGresult) == PGRES_TUPLES_OK)
        {
            int nFields = PQnfields(pPGresult);
            

            for (int j = 0; j < nFields; j++)
            {
                char *col_name = PQfname(pPGresult, j);

                if(col_name)
                {
                    pCResult->addColumn(col_name);
                }
            }

            for (int i = 0; i < PQntuples(pPGresult); i++)
            {
                CRow *pCRow = pCResult->newCRow();

                for (int j = 0; j < nFields; j++)
                {
                    char *cell_value = PQgetvalue (pPGresult, i, j);

                    if(cell_value)
                    {
                        pCRow->setCol(PQfname(pPGresult, j), cell_value);
                    }
                }
            }
        }
        else 
        {
            LOG_MESSAGE(LM_ERROR,"SELECT query failed w/ PQresultStatus=%i"
                    ,PQresultStatus(pPGresult));
            //TODO[ME]: Should throw error.
        }

        PQclear(pPGresult);
    }
    return pCResult;
}


