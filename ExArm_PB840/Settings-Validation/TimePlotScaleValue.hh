
#ifndef TimePlotScaleValue_HH
#define TimePlotScaleValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TimePlotScaleValue - Y-Axis Scale Values for the Time Plot.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TimePlotScaleValue.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date:  10-May-2007    SCR Number: 6375
//  Project:  Trend
//  Description:
//		Added 30 second timescale setting.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct TimePlotScaleValue
{
  //@ Type:  TimePlotScaleValueId
  // All of the possible values for TimePlotScaleSetting.
  enum TimePlotScaleValueId
  {
    // $[01156] -- values of the range of Time Plot Axis Setting...
    FROM_0_TO_3_SECONDS,			
    FROM_0_TO_6_SECONDS,
    FROM_0_TO_12_SECONDS,
    FROM_0_TO_24_SECONDS,
    FROM_0_TO_30_SECONDS,
    FROM_0_TO_48_SECONDS,

    TOTAL_TIME_SCALE_VALUES
  };
};


#endif // TimePlotScaleValue_HH 
