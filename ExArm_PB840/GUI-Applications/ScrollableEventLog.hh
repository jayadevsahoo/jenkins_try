
#ifndef ScrollableEventLog_HH
#define ScrollableEventLog_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ScrollableEventLog - A scrollable event log which displays
//                             a list of event ids and the description 
//                             of each event.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollableEventLog.hhv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  rhj    Date:  06-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//       Trend releated changes.
//====================================================================

#include "GuiAppClassIds.hh"
#include "GuiTable.H"

//@ Usage-Classes
#include "FocusButtonTarget.hh"
#include "SettingScrollbar.hh"
//@ End-Usage

static const int EVENT_LOG_ROWS = 5;
static const int EVENT_LOG_COLS = 2;

class LogTarget;

class ScrollableEventLog 
:	public GuiTable<EVENT_LOG_ROWS,EVENT_LOG_COLS>, 
	public FocusButtonTarget
{
public:
	ScrollableEventLog( LogTarget &eventDetailItems,
						Uint16 numDisplayRows,
						Uint16 numDisplayColumns, 
						Uint16 rowHeight,
						Boolean isDisplayScrollbar);

	~ScrollableEventLog(void);

	// FocusButtonTarget virtual
	virtual void adjustKnobHappened(Int32 delta);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);



private:
	// these methods are purposely declared, but not implemented...
	ScrollableEventLog(const ScrollableEventLog&);	  // not implemented...
	void   operator=(const ScrollableEventLog&); // not implemented...

	// GuiTable virtual
	virtual void updateScroll_(Uint16 newSelectedEntry);
	virtual void refreshCell_(Uint16 row, Uint16 col);


	//@ Data-Member: scrollbar_
	// The small scrollbar on the right of the event detail window
	SettingScrollbar scrollbar_;

	//@ Data-Member: rMenuItems_
	// A reference to the LogTarget that provides the table cell data
	LogTarget &rLogTarget;

};

#endif // ScrollableEventLog_HH
