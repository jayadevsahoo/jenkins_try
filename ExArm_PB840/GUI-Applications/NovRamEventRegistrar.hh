#ifndef NovRamEventRegistrar_HH
#define NovRamEventRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

//====================================================================
// Class: NovRamEventRegistrar - The central registration, control, and
// dispatch point for NovRam update NovRam events received by GUI-Apps.  The
// update events are received from the Persistent-Objects subsystem via the GUI
// task's User Annunciation queue.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/NovRamEventRegistrar.hhv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  yyy    Date:  04-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Registered for DATE/TIME settings to capture the setting change
//		events and update the date/time accordingly.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "UserAnnunciationMsg.hh"
#include "NovRamUpdateManager.hh"
#include "NovRamEventCascade.hh"
class NovRamEventTarget;
//@ End-Usage


class NovRamEventRegistrar
{
public:
	static void EventHappened(UserAnnunciationMsg &eventMsg);
	static void RegisterTarget(NovRamUpdateManager::UpdateTypeId updateId,
							   NovRamEventTarget* pTarget);
	static void Initialize(void);
	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	NovRamEventRegistrar(void);							// not implemented...
	NovRamEventRegistrar(const NovRamEventRegistrar&);	// not implemented...
	~NovRamEventRegistrar(void);						// not implemented...
	void operator=(const NovRamEventRegistrar&);		// not implemented...

	//@ Data-Member: CascadeArray_
	// A static array of NovRamEventCascade objects, implemented as
	// a simple pointer to an object.  The index to the array corresponds
	// to the SettingId::SettingIdType provided by the Settings-Validation
	// subsystem. This allows a fast lookup.
	static NovRamEventCascade *CascadeArray_;

};


#endif // NovRamEventRegistrar_HH 
