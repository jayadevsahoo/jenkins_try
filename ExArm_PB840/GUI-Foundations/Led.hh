#ifndef Led_HH
#define Led_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Led - Interface to the hardware, allowing the activation
// of the various status and alarm LEDs that the ventilator displays.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Led.hhv   25.0.4.0   19 Nov 2013 14:11:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 003   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//      Removed ifndef / endif around include directives
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//      Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

//@ Usage-Classes
//@ End-Usage


class Led
{
public:
	//@ Type: LedId
	// An enumerated type listing the different Leds that can be lit.
	//
	enum LedId
	{
		HIGH_ALARM_URGENCY,
		MEDIUM_ALARM_URGENCY,
		LOW_ALARM_URGENCY,
		NORMAL,
		VENTILATOR_INOPERATIVE,
		SAFETY_VALVE_OPEN,
		BATTERY_BACKUP_READY,
		ON_BATTERY_POWER,
		COMPRESSOR_READY,
		COMPRESSOR_OPERATING,
		HUNDRED_PERCENT_O2,
		ALARM_SILENCE,
		SCREEN_LOCK,
		NUM_LED_ID
	};

	//@ Type: LedState
	// An enumerated type listing the different states that a LED can be in,
	// Specifically: ON_STATE, OFF_STATE, SLOW_BLINK_STATE, FAST_BLINK_STATE.
	//
	enum LedState
	{
		ON_STATE,
		OFF_STATE,
		SLOW_BLINK_STATE,
		FAST_BLINK_STATE
	};

	static void SetState(LedId ledId, LedState state);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	Led(void);					// Declared but not implemented
	~Led(void);					// Declared but not implemented
	Led(const Led&);			// Declared but not implemented
	void operator=(const Led&);	// Declared but not implemented
};


#endif // Led_HH 
