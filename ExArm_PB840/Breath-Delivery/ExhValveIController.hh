#ifndef ExhValveIController_HH
#define ExhValveIController_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ExhValveIController - Controller for the exhalation valve during
//      inspiration phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExhValveIController.hhv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: rhj     Date:  07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//		Added getErrorGain method.
// 
//  Revision: 006  By: syw     Date:  18-Nov-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//		Added isOpenLoopControl_ data member and access methods.
//
//  Revision: 005  By: syw     Date:  26-Apr-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//		Added totalTrajLimitReached_, factorConditionMet_, totalTrajLimit_ and
//		access methods.  Change interface to newBreath() and updateExhalationValve()
//		for TC.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Controls Rev 04 changes:
//			- changed argument types passed in to newBreath() and
//			  updateExhalationValve() methods.
//			- added determineErrorGain_() method.
//			- changed errorGain from Int32 to Real32.
//			- added isFlowControllerActive_ flag to replace 
//			  if errorGain_ != 0 condition.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes
//@ End-Usage

class ExhValveIController {
  public:
    ExhValveIController( void) ;
    ~ExhValveIController( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL) ;

    void newBreath( const Real32 totalTrajLimit = 100.0) ;
    void updateExhalationValve( const Real32 targetPressure,
    							const Real32 reliefFlowTarget,
    							const Boolean newController = FALSE,
    							const Real32 factor = 1.0) ; 

	inline void setTotalTrajLimitReached( void)  ;
	inline void setFactorConditionMet( void) ;
	inline void setIsOpenLoopControl( const Boolean value) ;
    inline Real32 getErrorGain(void);

#if CONTROLS_TUNING	
	//Returns the Kp gain
    Real32 getKp() { return kp_; }

    //Returns the Ki gain
    Real32 getKi() { return ki_; }

	void setKp( const Real32 gain);

    //Sets the Ki gain.
	void setKi( const Real32 gain );
#endif

  protected:

  private:
     ExhValveIController( const ExhValveIController&) ;		// not implemented...
     void operator=(const ExhValveIController&) ;			// not implemented...

	void determineErrorGain_( const Real32 exhFlow, const Real32 desiredReliefFlow) ;

     //@ Data-Member:  errorGain_
     // gain on error.  set to non-zero to activate flow controller
     Real32 errorGain_ ;	

     //@ Data-Member:  integrator_
     // integral of flow error
     Real32  integrator_ ;

     //@ Data-Member:  desiredReliefFlow_
     //	the desired relief flow out of the exhalation valve
     Real32  desiredReliefFlow_ ;

     //@ Data-Member:  kp_ 
     // proportional gain (cmh20/lpm)
     Real32 kp_ ;

     //@ Data-Member:  ki_ 
     // integral gain (cmh20/lpm-msec)
     Real32 ki_ ;

     //@ Data-Member: qexhLessThanDesiredFlag_
     // set true if exhFlow < desiredReliefFlow
     Boolean qexhLessThanDesiredFlag_ ;

     //@ Data-Member: pexhLessThanTargetFlag_
     // set true if patExhPressure < targetPressure
     Boolean pexhLessThanTargetFlag_ ;

     //@ Data-Member: targetPressure_
     // input command to controller.
     Real32 targetPressure_ ;

	//@ Data-Member: isFlowControllerActive_
	// set to true when the flow controller is active
	Boolean isFlowControllerActive_ ;

	//@ Data-Member: totalTrajLimitReached_
	// set when total trajectory limit is reached
	Boolean totalTrajLimitReached_ ;

	//@ Data-Member: factorConditionMet_
	// set if the factor condition is met
	Boolean factorConditionMet_ ;

	//@ Data-Member: totalTrajLimit_
	// maximum traj limit
	Real32 totalTrajLimit_ ;

	//@ Data-Member: isOpenLoopControl_
	// set if open loop control is desired
	Boolean isOpenLoopControl_ ;

#if CONTROLS_TUNING	
	Real32 kpTuning_;
	Real32 kiTuning_;
#endif

} ;

// Inlined methods
#include "ExhValveIController.in"

#endif // ExhValveIController_HH 
