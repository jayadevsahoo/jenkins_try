#ifndef	VsetInterface_HH
#define VsetInterface_HH

//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2010, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Class:  VsetInterface - virtual Settings Interface
//---------------------------------------------------------------------
//@ Version-Information
//  @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/VsetInterface.hhv   25.0.4.0   19 Nov 2013 14:12:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  22-Feb-2010    SCR Number: 6663
//  Project:  API
//  Description:
//	Initial version.
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SerialInterface.hh"
#include "SerialQueue.hh"

//@ Usage-Classes
//@ End-Usage


class VsetServer;
class Dci;

class VsetInterface : public SerialInterface
{
public:

    VsetInterface( SerialPort & rSerialPort, SerialInterface::PortNum portNum );
    ~VsetInterface();

    // virtual from SerialInterface
    virtual Boolean startPrint(void);
    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32 lineNumber,
                           const char * pFileName = NULL,
                           const char * pPredicate = NULL);

protected:
    // virtual from SerialInterface
    virtual void processRequests_(void);
    virtual void setCurrentPrinter_(void);

private:
    enum
    {
        NAK_CHECKSUM_ERROR = 1,
        NAK_UNRECOGNIZED = 2,
        NAK_TIMEOUT = 3,
        NAK_COM_ERROR = 4
    };

    void processCmd_(const char* cmd);
    void sendNak_(Int32 reason);

    SerialQueue rxQueue_;
    VsetServer& rVsetServer_;
    Dci& rDci_;
};

#endif	// ! VsetInterface_HH
