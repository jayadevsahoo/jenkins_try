#ifndef BreathSet_HH
#define BreathSet_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BreathSet - Set of breath records.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathSet.hhv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 009  By:  syw    Date: 08-Jan-1998    DR Number: 
//      Project:  Sigma (R8027)
//      Description:
//			Bilevel Initial Release
//			Added isBiLevelLowToHigh = FALSE to newBreathRecord() args.
//
//  Revision: 008  By:  iv    Date: 03-Sep-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Reworked per formal code review.
//
//  Revision: 007  By:  iv    Date: 29-May-1997    DR Number: DCS 2116
//       Project:  Sigma (R8027)
//       Description:
//             changed BREATH_HISTORY_TABLE_SIZE from 20 to 30.
//
//  Revision: 006  By:  sp    Date: 10-Apr-1997    DR Number: DCS 1867
//       Project:  Sigma (R8027)
//       Description:
//             Fixed Spont min volume calculation.
//
//  Revision: 005  By:  sp    Date: 24-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 004  By:  sp    Date:  9-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 003  By:  sp    Date:  24-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Add default argument to newBreathRecord.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number: DCS 1674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"

#include "Breath_Delivery.hh"

//@ Usage-Classes
#include "BreathRecord.hh"

//@ End-Usage


//@ Constant: BREATH_HISTORY_TABLE_SIZE
//size for an array of breath record
static const Int32 BREATH_HISTORY_TABLE_SIZE = 30;

//@ Constant: NUM_PREV_BREATH_RECORDS
//number of breath records you can go back
extern const Int32 NUM_PREV_BREATH_RECORDS;

class BreathSetIterator;

class BreathSet {
    //@ Friend: BreathSetIterator
    friend class BreathSetIterator;
  public:
    BreathSet(void);
    ~BreathSet(void);
    inline BreathRecord* getCurrentBreathRecord(void);
    BreathRecord* getLastBreathRecord(void);
    void initialize(void);
    Int32 getQueMsg(void);
    void newBreathRecord(
	const SchedulerId::SchedulerIdValue schedulerId,
        const DiscreteValue mandatoryType,
        const BreathType breathType,
	const BreathPhaseType::PhaseType phaseType,
	const Boolean peepRecovery = FALSE,
	const Boolean isMandBreath = FALSE);


    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
  protected:

  private:

    BreathSet(const BreathSet&);		// not emplemented...
    void   operator=(const BreathSet&);	// not implemented...

    void checkSettingChange_(const SchedulerId::SchedulerIdValue schedulerId);

    //@ Data-Member: breathRecord_
    //set of breath record
    BreathRecord  breathRecord_[BREATH_HISTORY_TABLE_SIZE];	

    //@ Data-Member:  CurrentRecord_
    //index to current record
    Int32 currentRecord_;

    //@ Data-Member:  numRecords_
    //The number of BreathRecords currently stored in the set
    Int32 numRecords_;

    //@ Data-Member:  numRespRateRecords_
    // this is the maximum number of breath records we can trace back
    // for average breath data calculation since the last change
    // in respiratory rate.
    Int32 numRespRateRecords_;
 
    //@ Data-Member:  numTidalVolumeRecords_
    // this is the maximum number of breath records we can trace back
    // for average breath data calculation since last change in 
    // tidal volume setting.
    Int32 numTidalVolumeRecords_;

    //@ Data-Member:  tidalVolume_
    //copy tidalVolume from setting that is being used
    Real32 tidalVolume_;

    //@ Data-Member:  respRate_
    //copy respiratory rate from setting that is being used
    Real32 respRate_;

    //@ Data-Member:  isUsingProxFlow_
    //a state flag indicating whether the current breath being added is using prox flow or system
    Boolean isUsingProxFlow_;
#ifdef SIGMA_UNIT_TEST
public:
    void write(void);
#endif
};


// Inlined methods...
#include "BreathSet.in"


#endif // BreathSet_HH 
