#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ServiceLowerOtherScreensSubScreen - The subscreen selected by the
// Other Screens button on the Lower screen.  Allows access to the
// ServiceMode Setup, External test control, Exh. valve calibration, and
// VentInop test.
//---------------------------------------------------------------------
//@ Interface-Description
// All Lower Screen subscreens during normal Service Mode excluding EST,
// Date/Time, Exit, and Other Screens subscreens are accessed via this subscreen.
// The subscreen displays navigation buttons, one for each destination subscreen.
//
// This class collaborates with the LowerSubScreenArea to activate the
// subscreens associated with the screen select buttons contained in this
// subscreen.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// The buttonDownHappened() method is called when any button in
// the subscreen is pressed down allowing us to activate its associated
// subscreen.
//---------------------------------------------------------------------
//@ Rationale
// Used to allow access to lower screen subscreens which do not have a
// dedicated subscreen button in the lower screen select area.  Allows
// addition of an arbitrary number of lower subscreens for future expansion.
//---------------------------------------------------------------------
//@ Implementation-Description
// In the constructor we create and display one button for every
// subscreen for which access is required.  At this time we register
// for button press event callbacks.  When any button is pressed down
// our buttonPressDownHappened() method is called and this function
// then determines which subscreen should be activated (depending on
// which button was pressed down).  Activation of the new subscreen
// will automatically deactivate this subscreen but our screen select button
// will remain selected.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created (in LowerSubScreenArea).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceLowerOtherScreensSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By:  rhj	   Date:  24-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:     
//      Added Compact Flash Test Subscreen.
//
//  Revision: 007   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//      Project:  NeoMode
//      Description:
//          Added Modification log. Updated for NeoMode  03/01/00
//
//
//  Revision: 007   By: quf    Date: 23-Jan-2002    DR Number: 5984
//  Project:  GUIComms
//  Description:
//	Added button for Serial Loopback Test.
//
//  Revision: 006  By:  yyy    Date:  01-Sep-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//             Initial release.
//
//  Revision: 005  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 004 By:  yyy    Date:  26-SEP-97    DR Number: 2410
//    Project:  Sigma (R8027)
//    Description:
//      Updated access method for pressure transducer calibrationtest due to changes
//		in service mode.
//
//  Revision: 003  By: gdc      Date: 23-Sep-1997  DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//     Cleaned up Service-Mode interfaces to support remote test.
//
//  Revision: 002  By:  yyy    Date:  30-JUL-97    DR Number: 2313,2279
//    Project:  Sigma (R8027)
//    Description:
//      30-JUL-97 Added SM_FLOW_SENSOR_CAL_, and SM_TAU_CALIBRATION_ buttons.
//		13-AUG-97  Removed all references for TauCharacterization
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ServiceLowerOtherScreensSubScreen.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "CalibrationSetupSubScreen.hh"
#include "CompactFlashTestSubScreen.hh"
#include "ExternalControlSubScreen.hh"
#include "FlowSensorCalibrationSubScreen.hh"
#include "MiscStrs.hh"
#include "OperationHourCopySubScreen.hh"
#include "OsUtil.hh"
#include "PressureXducerCalibrationSubScreen.hh"
#include "PromptStrs.hh"
#include "SerialLoopbackTestSubScreen.hh"
#include "ServiceInitializationSubScreen.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceModeSetupSubScreen.hh"
#include "SoftwareOptions.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 SM_SETUP_BUTTON_X_ = 30;
static const Int32 SM_BUTTON_X_GAP_ = 20;
static const Int32 SM_BUTTON_Y_ = 100;

// This is only temporarily provide a access method to do download.
#ifndef FORNOW
static const Int32 SM_BUTTON_Y_GAP_ = 50;
#else
static const Int32 SM_BUTTON_Y_GAP_ = 70;
#endif	// FORNOW

static const Int32 BUTTON_WIDTH_ = 178;
static const Int32 BUTTON_HEIGHT_ = 50;
static const Int32 BUTTON_BORDER_ = 3;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceLowerOtherScreensSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructs this subscreen.  Sizes the subscreen and adds the
// required graphics to the displayable container.  No parameters
// needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// Create the moreSettingsButton_ and titleArea_, size the container
// and add the two drawables.  Also, register for callbacks from the
// more settings button.
// $[07045] The other screen subscreen shall provide access to the ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceLowerOtherScreensSubScreen::ServiceLowerOtherScreensSubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	smSetupButton_(
				SM_SETUP_BUTTON_X_,
				SM_BUTTON_Y_,
				BUTTON_WIDTH_, 
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SM_SETUP_BUTTON_TITLE),
	smSystemTestButton_(
				SM_SETUP_BUTTON_X_ + 1 * (BUTTON_WIDTH_ + SM_BUTTON_X_GAP_),
				SM_BUTTON_Y_,
				BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SM_SYSTEM_TEST_BUTTON_TITLE),
	smCalibrationButton_(
				SM_SETUP_BUTTON_X_ + 2 * (BUTTON_WIDTH_ + SM_BUTTON_X_GAP_),
				SM_BUTTON_Y_,
				BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SM_CALIBRATION_BUTTON_TITLE),
	ventInopButton_(
				SM_SETUP_BUTTON_X_, 
				SM_BUTTON_Y_ + BUTTON_HEIGHT_ + SM_BUTTON_Y_GAP_,
				BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SM_VENT_INOP_BUTTON_TITLE),
	smFlowerSensorCalibrationButton_(
				SM_SETUP_BUTTON_X_ + 1 * (BUTTON_WIDTH_ + SM_BUTTON_X_GAP_),
				SM_BUTTON_Y_ + BUTTON_HEIGHT_ + SM_BUTTON_Y_GAP_,
				BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SM_FLOW_SENSOR_CAL_BUTTON_TITLE),
	smPressureXducerCalibrationButton_(
				SM_SETUP_BUTTON_X_ + 2 * (BUTTON_WIDTH_ + SM_BUTTON_X_GAP_),
				SM_BUTTON_Y_ + BUTTON_HEIGHT_ + SM_BUTTON_Y_GAP_,
				BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SM_PRESSURE_XDUCER_CAL_BUTTON_TITLE),
	operationHourCopyButton_(
				SM_SETUP_BUTTON_X_, 
				SM_BUTTON_Y_ +  2 * (BUTTON_HEIGHT_ + SM_BUTTON_Y_GAP_),
				BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SM_OPERATION_HOUR_COPY_BUTTON_TITLE),
	serialLoopbackTestButton_(
				SM_SETUP_BUTTON_X_ + 1 * (BUTTON_WIDTH_ + SM_BUTTON_X_GAP_),
				SM_BUTTON_Y_ +  2 * (BUTTON_HEIGHT_ + SM_BUTTON_Y_GAP_),
				BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SM_SERIAL_LOOPBACK_TEST_BUTTON_TITLE),
	compactFlashTestButton_(
				SM_SETUP_BUTTON_X_ + 2 * (BUTTON_WIDTH_ + SM_BUTTON_X_GAP_),
				SM_BUTTON_Y_ +  2 * (BUTTON_HEIGHT_ + SM_BUTTON_Y_GAP_),
				BUTTON_WIDTH_,
				BUTTON_HEIGHT_, Button::LIGHT, BUTTON_BORDER_,
				Button::SQUARE, Button::NO_BORDER,
				MiscStrs::SM_COMPACT_FLASH_TEST_BUTTON_TITLE),
	pCurrentButton_(NULL),
	titleArea_(MiscStrs::OTHER_SCREENS_TITLE, SubScreenTitleArea::SSTA_CENTER)
{
	CALL_TRACE("ServiceLowerOtherScreensSubScreen::ServiceLowerOtherScreensSubScreen(SubScreenArea "
													"*pSubScreenArea)");

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Register for callbacks to Service Mode Calibration button
	smCalibrationButton_.setButtonCallback(this);
	smSetupButton_.setButtonCallback(this);
	smSystemTestButton_.setButtonCallback(this);
	ventInopButton_.setButtonCallback(this);
	smFlowerSensorCalibrationButton_.setButtonCallback(this);
	smPressureXducerCalibrationButton_.setButtonCallback(this);
	operationHourCopyButton_.setButtonCallback(this);
	serialLoopbackTestButton_.setButtonCallback(this);
	compactFlashTestButton_.setButtonCallback(this);

	// Add the title area and button
	addDrawable(&titleArea_);
	addDrawable(&smCalibrationButton_);
	addDrawable(&smSetupButton_);
	addDrawable(&smSystemTestButton_);
	addDrawable(&ventInopButton_);	
	addDrawable(&smFlowerSensorCalibrationButton_);	
	addDrawable(&smPressureXducerCalibrationButton_);	
	addDrawable(&operationHourCopyButton_);	
	addDrawable(&compactFlashTestButton_);	

	// Only display the Serial Loopback Test button and register
	// for a callback if this is a 3-port (GUIComms) GUI
	if (IsGuiCommsConfig())
	{
	// $[TI1]
		serialLoopbackTestButton_.setButtonCallback(this);
		addDrawable(&serialLoopbackTestButton_);
	}
	// $[TI2]
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceLowerOtherScreensSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys the ServiceLowerOtherScreensSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceLowerOtherScreensSubScreen::~ServiceLowerOtherScreensSubScreen(void)
{
	CALL_TRACE("ServiceLowerOtherScreensSubScreen::~ServiceLowerOtherScreensSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate [Virtual]
//
//@ Interface-Description
// Called by the LowerSubScreenArea just before this subscreen is
// displayed on-screen. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Show the compact flash test button if this is a Xena board.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerOtherScreensSubScreen::activate(void)
{
	CALL_TRACE("ServiceLowerOtherScreensSubScreen::activate(void)");

	compactFlashTestButton_.setShow( IsXenaConfig() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by the LowerSubScreenArea when this subscreen is being removed from
// the screen.  Nothing special to do.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerOtherScreensSubScreen::deactivate(void)
{
	CALL_TRACE("ServiceLowerOtherScreensSubScreen::deactivate(void)");

	if (pCurrentButton_ == &ventInopButton_)
	{	// Previously user had pressed vent inop button
		// Pop up the button.
		// $[TI1]
		pCurrentButton_->setToUp();
	}
		// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when any button in ServiceLowerOtherScreensSubScreen is pressed down.
// We must display the screen corresponding to the pressed button while still
// keeping the screen select button for the ServiceLowerOtherScreensSubScreen
// pressed down.  Passed a pointer to the button as well as 'byOperatorAction',
// a flag which indicates whether the operator pressed the button down or whether
// the setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// Check the button pressed and then activate the corresponding subscreen.
// We must also make sure the tab button for Other Screens subscreen stays 
// pressed down (it pops up when this subscreen is deactivated as a result of 
// the new Settings subscreens being activated).
//---------------------------------------------------------------------
//@ PreCondition
// Callback must have been generated by an operator action.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerOtherScreensSubScreen::buttonDownHappened(Button *pButton,
												Boolean byOperatorAction)
{
	CALL_TRACE("ServiceLowerOtherScreensSubScreen::buttonDownHappened("
							"Button *pButton, Boolean byOperatorAction)");
	CLASS_PRE_CONDITION(byOperatorAction);

	SAFE_CLASS_ASSERTION(pButton != NULL);

	if (!isVisible())
	{													// $[TI1]
		return;
	}		

	// Another button is selected.  Deselect the previously selected button
	if (pCurrentButton_ != pButton && pCurrentButton_ != NULL)
	{	// $[TI2]
		pCurrentButton_->setToUp();
		
		// Lose Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);
	}	// $[TI3]


	// Activate the corresponding sub-screen and force the Lower
	// Other Screens tab button down with it. $[01063]
	if (pButton == &smCalibrationButton_)
	{	// $[TI4]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetCalibrationSetupSubScreen(),
			ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->
						getServiceLowerOtherScreensTabButton());
	}
	else if (pButton == &smSetupButton_)
	{	// $[TI5]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetServiceModeSetupSubScreen(),
			ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->
						getServiceLowerOtherScreensTabButton());
	}
	else if (pButton == &smSystemTestButton_)
	{	// $[TI6]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetExternalControlSubScreen(),
			ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->
						getServiceLowerOtherScreensTabButton());
	}
	else if (pButton == &smFlowerSensorCalibrationButton_)
	{	// $[TI6.1]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetFlowSensorCalibrationSubScreen(),
			ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->
						getServiceLowerOtherScreensTabButton());
	}
	else if (pButton == &smPressureXducerCalibrationButton_)
	{	// $[TI6.2]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetPressureXducerCalibrationSubScreen(),
			ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->
						getServiceLowerOtherScreensTabButton());
	}
	else if (pButton == &operationHourCopyButton_)
	{	// $[TI6.3]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetOperationHourCopySubScreen(),
			ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->
						getServiceLowerOtherScreensTabButton());
	}
	else if (pButton == &serialLoopbackTestButton_)
	{	// $[TI6.4]
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetSerialLoopbackTestSubScreen(),
			ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->
						getServiceLowerOtherScreensTabButton());
	}
	else if (pButton == &compactFlashTestButton_)
	{
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetCompactFlashTestSubScreen(),
			ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->
						getServiceLowerOtherScreensTabButton());
	}

	if (pButton == &ventInopButton_)
	{	// $[TI7]

		// $[07058] The Force Safe State Test subscreen shall provide a button...
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();
	
		//
		// Set all prompts 
		//
		setTestPrompt(PromptArea::PA_PRIMARY, 
								PromptArea::PA_HIGH,
								PromptStrs::SM_KEY_ACCEPT_P);
		setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_HIGH,
								NULL_STRING_ID);
		setTestPrompt(PromptArea::PA_ADVISORY, 
								PromptArea::PA_LOW,
								NULL_STRING_ID);
		setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH,
								PromptStrs::OTHER_SCREENS_CANCEL_S);
		
	}
	else
	{
		if (pCurrentButton_ == &ventInopButton_)
		{	// Previously user had pressed vent inop button
			// Pop up the button.
		// $[TI8]

			// Pop up the button.
			pCurrentButton_->setToUp();
		}
		
		// $[TI9]
		// Bounce the currently held down button.  
		pButton->setToUp();

	}

	pCurrentButton_ = pButton;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in Vent Setup is pressed up.  Passed a pointer to the
// button as well as 'byOperatorAction', a flag which indicates whether the
// operator pressed the button up or whether the setToUp() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
// We change some prompts if the proceed button was pressed.
//
// If the Respiratory Rate, Inspiratory Time, Expiratory Time or I:E Ratio
// buttons were pressed up then we need to inform the breath timing diagram
// that there is no active setting so that it can unhighlight the setting
// in question.
//
// There is a possibility that this event occurs after this subscreen is
// removed from the screen (when this subscreen is deactivated and the
// Adjust Panel focus is lost).  In this case we do nothing, simply return.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerOtherScreensSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	CALL_TRACE("ServiceLowerOtherScreensSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)");

#if defined FORNOW
printf("buttonUpHappened at line  %d, pButton=%d, pCurrentButton=%d\n"
		, __LINE__, pButton, pCurrentButton_);
#endif	// FORNOW
		
	if (pButton == &ventInopButton_)
	{	// Previously user had pressed vent inop button
		// Pop up the button.
		// $[TI1]
		clearVentInopButtonRequest_(&ventInopButton_);
	}	
		// $[TI2]
}	


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the default adjust panel focus is restored.  Tell the
// subscreen to restore the appropriated prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the Same Patient is the currently selected option then simply inform the
// Lower Screen of the event, else make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerOtherScreensSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("ServiceLowerOtherScreensSubScreen::adjustPanelRestoreFocusHappened(void)");

#if defined FORNOW
printf("adjustPanelRestoreFocusHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	restoreTestPrompt();
		// $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting of start test process.  Then it builds the laptop communication
// granted command and send it over to serial I/O handler.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the Same Patient is the currently selected option then simply inform the
// Lower Screen of the event, else make the Invalid Entry sound.
// $[07059] After the Accept key is pressed to start the Force Safe test ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerOtherScreensSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("ServiceLowerOtherScreensSubScreen::adjustPanelAcceptPressHappened(void)");

#if defined FORNOW
printf("ServiceLowerOtherScreensSubScreen::adjustPanelAcceptPressHappened() at line  %d\n", __LINE__);
#endif	// FORNOW

	if (pCurrentButton_ == &ventInopButton_)
	{ 	
		// $[TI1]
		// Pop up the button.
		pCurrentButton_->setToUp();

		GuiApp::SetGuiVentInopTestInProgressFlag(TRUE);
		GuiApp::SetBdVentInopTestInProgressFlag(TRUE);
		
		getSubScreenArea()->activateSubScreen(
			LowerSubScreenArea::GetServiceInitializationSubScreen(), NULL);
	}
		// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clearVentInopButtonRequest_
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting of start test process.  Then it builds the laptop communication
// granted command and send it over to serial I/O handler.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the Same Patient is the currently selected option then simply inform the
// Lower Screen of the event, else make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerOtherScreensSubScreen::clearVentInopButtonRequest_(TextButton *pButton)
{
	CALL_TRACE("ServiceLowerOtherScreensSubScreen::clearVentInopButtonRequest_(TextButton *pButton)");

#if defined FORNOW
printf("ServiceLowerOtherScreensSubScreen::clearVentInopButtonRequest_() at line  %d\n", __LINE__);
#endif	// FORNOW

	// Service mode had request either an ACCEPT  prompt action.
	// Make sure that nothing in this subscreen keeps focus.
	// Clear the Adjust Panel focus, make the Accept sound
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompts
	setTestPrompt(PromptArea::PA_PRIMARY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);
			
	// Clear the Advisory prompts
	setTestPrompt(PromptArea::PA_ADVISORY,
					 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Primary prompts
	setTestPrompt(PromptArea::PA_PRIMARY,
					 PromptArea::PA_LOW, NULL_STRING_ID);
			
	// Clear the Advisory prompts
	setTestPrompt(PromptArea::PA_ADVISORY,
					 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, NULL_STRING_ID);
		// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServiceLowerOtherScreensSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
			SERVICELOWEROTHERSCREENSSUBSCREEN, lineNumber, pFileName, pPredicate);
}

