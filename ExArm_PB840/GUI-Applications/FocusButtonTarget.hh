#ifndef FocusButtonTarget_HH
#define FocusButtonTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: FocusButtonTarget -  A target class for receiving 
//                              AdjustPanelTarget's adjustKnobHappened 
//                              events.  
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/FocusButtonTarget.hhv   25.0.4.0   19 Nov 2013 14:07:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  rhj    Date:  06-Jun-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version
//====================================================================

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage

class FocusButtonTarget
{
public:
	virtual void adjustKnobHappened(Int32 delta);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	FocusButtonTarget(void);
	virtual ~FocusButtonTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	FocusButtonTarget(const FocusButtonTarget&);		// not implemented...
	void   operator=(const FocusButtonTarget&);		// not implemented...

};


#endif // FocusButtonTarget_HH
