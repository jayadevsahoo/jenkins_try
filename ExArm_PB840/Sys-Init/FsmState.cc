#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: FsmState - Finite-State-Machine State Base Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  Class interface description
//  FsmState is a virtual base class for all states in the Task-Control
//  finite state machines.  As a virtual base class, it provides the
//  default behavior for all FsmStates through virtual functions that
//  receive various TaskControlMessage and TaskControlTimer objects.
//  Upon receiving a TaskControlMessage or TaskControlTimer on its
//  queue, the Task-Control task sends the message to the current
//  FsmState for processing.  A separate interface is provided to
//  receive each type of TaskControlMessage and TaskControlTimer.  The
//  derived FsmState can override the virtual function provided by the
//  base class to provide state- specific behavior for these messages.
//  The FsmState class also maintains the current state of the finite
//  state machine as well as the SigmaState for the GUI and the BD.  It
//  provides accessor methods to retrieve these states.  Only the
//  derived FsmState classes can set these states to provide a state
//  transition.  When the current state changes, this class will 
//  message TaskControlAgent::Update to update its local state.
//---------------------------------------------------------------------
//@ Rationale
//  FsmState contains the default behavior for processing 
//  TaskControlMessage and TaskControlTimer objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This base class provides a virtual function for each type of
//  TaskControlMessage or TaskControlTimer that will be received by 
//  Task-Control.  The default behavior provided by these virtual
//  functions is described in the function interface.  The public
//  accessor methods GetCurrentState(), GetGuiState() and GetBdState()
//  provide access to current states of the finite state machine.
//  Protected methods SetCurrentState(), SetGuiState(), and 
//  SetBdState() allow derived FsmStates to set the current state of
//  the finite state machine.  
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/FsmState.ccv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//   Revision 004   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 003   By:   gdc      Date: 09-Oct-1997 DR Number: 1923
//      Project:   Sigma   (R8027)
//      Description:
//         Log system event when transition timeout occurs.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Added IsTransitioning() method to determine when the 
//         finite state machine is in transition.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "FsmState.hh"
#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

//@ Usage-Classes
#include "BdFsm.hh"
#include "BdInop.hh"
#include "BdFailedMessage.hh"
#include "BdReadyMessage.hh"
#include "ChangeStateMessage.hh"
#include "DiagnosticCode.hh"
#include "FaultHandler.hh"
#include "GuiFsm.hh"
#include "GuiInop.hh"
#include "GuiReadyMessage.hh"
#include "Post.hh"
#include "SyncClockMessage.hh"
#include "SyncClockTimer.hh"
#include "TaskControl.hh"
#include "TaskControlAgent.hh"
#include "TaskControlMessage.hh"
#include "TaskAgentManager.hh"
#include "TransitionTimer.hh"
#include "TaskReadyMessage.hh"
#include "UserClock.hh"

//TODO E600 this #if section is temp only to allow for hacking Activate_
// remove when code porting is done and th ehacking is no longer needed
#if defined(SIGMA_BD_CPU)
#include "BdStart.hh"
#elif defined(SIGMA_GUI_CPU)
#include "GuiStart.hh"
#endif

//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Data-Member:    PCurrentState_
//  Pointer to the current FsmState object.
static const FsmState*   PCurrentState_ = NULL;

//@ Data-Member:    PTargetState_
//  Pointer to the target FsmState object.
static const FsmState*   PTargetState_ = NULL;

//@ Data-Member:    GuiState_
//  Current state of the GUI.  On the BD, this contains the GUI
//  state from the last GuiReadyMessage received.
static SigmaState        GuiState_ = STATE_START;
 
//@ Data-Member:    BdState_
//  Current state of the BD.  On the GUI, this contains the BD
//  state from the last BdReadyMessage received.
static SigmaState        BdState_ = STATE_START;

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FsmState [default constructor]
//
//@ Interface-Description
//  FsmState default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FsmState::FsmState()
{
    CALL_TRACE("FsmState()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FsmState [destructor]
//
//@ Interface-Description
//  FsmState destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


FsmState::~FsmState()
{
    CALL_TRACE("~FsmState()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  FsmState calls this virtual method upon completing a state 
//  transition.  The base class method here does nothing.  The derived
//  class overrides this method to provide state specific behavior.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
FsmState::activate(void) const
{   
    CALL_TRACE("activate(void)");
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveChangeState
//
//@ Interface-Description
//  The FsmState receiveChangeState method receives a ChangeStateMessage.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a ChangeStateMessage when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  This method initiates a transition to the INOP state for a 
//  ChangeState-INOP message.  A ChangeStateMessage for any other state
//  is considered invalid.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Gets the INOP state from the appropriate GuiFsm or BdFsm and 
//  transitions to it.  Reports the state change to the application
//  tasks through the TaskAgentManager.  Sends a BdReady/GuiReady-INOP 
//  message to the peer processor through TaskControlAgent.  Calls 
//  FsmState::invalid for an invalid state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveChangeState(const ChangeStateMessage& message) const
{
    CALL_TRACE("receiveChangeState(const ChangeStateMessage&)");
    switch (message.getState())
    {
        case STATE_INOP: // $[TI1]
#ifdef SIGMA_BD_CPU
            BdFsm::GetBdInopState().startTransition();
#endif
#ifdef SIGMA_GUI_CPU
            GuiFsm::GetGuiInopState().startTransition();
#endif
            break;
        default:  // $[TI2]
            invalid((const TaskControlMessage&)message);
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveSyncClock
//
//@ Interface-Description
//  The FsmState receiveSyncClock method receives a SyncClockMessage.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a SyncClockMessage when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  This method will invoke the UserClock to synchronize the local 
//  clock to the time contained in the message.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls UserClock::Set with the time contained in the message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveSyncClock(const SyncClockMessage& message) const
{
    CALL_TRACE("receiveSyncClock(const SyncClockMessage&)");
    UserClock::Set(message.getTimeConstructed(), message.isUserInitiated());
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveBdReady
//
//@ Interface-Description
//  The FsmState receiveBdReady method receives a BdReadyMessage.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a BdReadyMessage when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  The default behavior presented by this method is to ignore the
//  message.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveBdReady(const BdReadyMessage& ) const
{
    CALL_TRACE("receiveBdReady(const BdReadyMessage&)");
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveGuiReady
//
//@ Interface-Description
//  The FsmState receiveGuiReady method receives a GuiReadyMessage.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a GuiReadyMessage when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  The default behavior presented by this method is to ignore the
//  message.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveGuiReady(const GuiReadyMessage& ) const
{
    CALL_TRACE("receiveGuiReady(const GuiReadyMessage&)");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveCommUp
//
//@ Interface-Description
//  The FsmState receiveCommUp method receives a CommUpMessage.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a CommUpMessage when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  The default behavior presented by this method is to ignore the
//  message.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveCommUp(const CommUpMessage& ) const
{
    CALL_TRACE("receiveCommUp(const CommUpMessage&)");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveCommDown
//
//@ Interface-Description
//  The FsmState receiveCommDown method receives a CommDownMessage.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a CommDownMessage when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  The default behavior presented by this method is to ignore the
//  message.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveCommDown(const CommDownMessage& message) const
{
    CALL_TRACE("receiveCommDown(const CommDownMessage&)");
#ifdef SIGMA_GUI_CPU
    BdState_ = STATE_UNKNOWN;
#endif
#ifdef SIGMA_BD_CPU
    GuiState_ = STATE_UNKNOWN;
#endif
    TaskControlAgent::Update();

    TaskAgentManager::Send(message);
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveStartupTimer
//
//@ Interface-Description
//  The FsmState receiveStartupTimer method receives a StartupTimer.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a StartupTimer when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  The default behavior presented by this method is to ignore the
//  message.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveStartupTimer(const StartupTimer& ) const
{
    CALL_TRACE("receiveStartupTimer(const StartupTimer&)");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveSyncClockTimer
//
//@ Interface-Description
//  The FsmState receiveSyncClockTimer method receives a SyncClockTimer.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a SyncClockTimer when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  This method invokes the TaskControlAgent to synchronize the BD clock
//  with the GUI clock.  It then resets the timer for the next interval.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveSyncClockTimer(const SyncClockTimer& ) const
{
    CALL_TRACE("receiveSyncClock(const SyncClockTimer&)");
#ifdef SIGMA_GUI_CPU
    TaskControlAgent::SyncClocks();
    SyncClockTimer::GetTimer().set();
#endif
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveTransitionTimer
//
//@ Interface-Description
//  The FsmState receiveTransitionTimer method receives a TransitionTimer.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a TransitionTimer when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  This message indicates at least one task has failed to respond with 
//  a TaskReady within the required time period.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveTransitionTimer(const TransitionTimer& timer) const
{
    CALL_TRACE("receiveTransitionTimer(const TransitionTimer&)");
 
    if (timer.isExpired()) // $[TI1]
    {
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_FSMSTATE) >= 1)
        cout << "STATE " << nameOf() 
             << " received valid " <<timer.nameOf() << endl;
#endif
        DiagnosticCode  diagnosticCode;
 
        // log a diagnostic indicating a transition timeout
#ifdef SIGMA_BD_CPU
        diagnosticCode.setSystemEventCode(
                        DiagnosticCode::TASK_CONTROL_TRANSITION_TIMEOUT,
                        BdState_ );
#endif
#ifdef SIGMA_GUI_CPU
        diagnosticCode.setSystemEventCode(
                        DiagnosticCode::TASK_CONTROL_TRANSITION_TIMEOUT,
                        GuiState_ );
#endif

	FaultHandler::LogDiagnosticCode( diagnosticCode );
 
        FsmState::Activate_();
    }
    // $[TI2] else old timer ignored
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveTaskReady
//
//@ Interface-Description
//  The FsmState receiveTaskReady method receives a TaskReadyMessage.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a TaskReadyMessage when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  The TaskAgentManager calls this method only when it has received
//  all outstanding (expected) TaskReadyMessages.  The pending state 
//  transition completes upon receiving this final TaskReadyMessage.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveTaskReady(const TaskReadyMessage& rMessage) const
{
    CALL_TRACE("receiveTaskReady(const TaskReadyMessage&)");
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_FSMSTATE) >= 1)
        cout << "STATE " << nameOf() 
             << " received " << rMessage.nameOf() << endl;
#endif
    TransitionTimer::GetTimer().cancel();
    FsmState::Activate_();
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveBdFailed
//
//@ Interface-Description
//  The FsmState receiveBdFailed method receives a BdFailedMessage.
//  This virtual method provides the default behavior for the FsmState
//  class for processing a BdFailedMessage when no state specific 
//  behavior is defined (ie. the virtual method is not overriden).
//  Upon receiving the BdFailedMessage, this method starts a transition
//  to the GUI-INOP state.  This message is valid on the GUI only.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::receiveBdFailed(const BdFailedMessage& rMessage) const
{
    CALL_TRACE("receiveBdFailed(const BdFailedMessage&)");
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_FSMSTATE) >= 1)
        cout << "STATE " << nameOf() 
             << " received " << rMessage.nameOf() << endl;
#endif

#ifdef SIGMA_GUI_CPU
    BdState_ = STATE_FAILED;
    GuiFsm::GetGuiInopState().startTransition();
#endif

#ifdef SIGMA_BD_CPU
    invalid(rMessage);
#endif
    // $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: invalid
//
//@ Interface-Description
//  The FsmState 'invalid' method receives a TaskControlMessage 
//  considered invalid.  The FsmState calls this function when it
//  receives a message not appropriate to the state.  During development 
//  an error message is logged to the console and an assertion occurs.  
//  During production, only the assertion occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::invalid(const TaskControlMessage& message) const
{
    CALL_TRACE("invalid(const TaskControlMessage&)");
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_FSMSTATE) >= 1)
        cout << "protocol botch: current state = " << nameOf() 
             << ", message received = " << message.nameOf() << endl;
#endif
    AUX_CLASS_ASSERTION_FAILURE(message.isA());
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetCurrentState
//
//@ Interface-Description
//  The static GetCurrentState method returns a FsmState reference to 
//  current state of the finite state machine.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const FsmState&
FsmState::GetCurrentState(void)
{
    CALL_TRACE("GetCurrentState(void)");
    CLASS_ASSERTION(PCurrentState_ != NULL);
    return *PCurrentState_;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTargetState
//
//@ Interface-Description
//  The static GetTargetState method returns a FsmState reference to 
//  target state of the finite state machine.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const FsmState&
FsmState::GetTargetState(void)
{
    CALL_TRACE("GetTargetState(void)");
    CLASS_ASSERTION(PTargetState_ != NULL);
    return *PTargetState_;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  The static Initialize method initializes the finite state machine 
//  states using the GuiFsm or BdFsm container class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::Initialize(void)
{
#ifdef SIGMA_GUI_CPU
    GuiFsm::Initialize();
    BdState_ = Post::IsBdFailed() ? STATE_FAILED : STATE_UNKNOWN;  // $[TI2] $[TI3]
#endif
#ifdef SIGMA_BD_CPU
    BdFsm::Initialize();
    GuiState_ = STATE_UNKNOWN;
#endif
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Start
//
//@ Interface-Description
//  The static Start method starts the finite state machine with an
//  initial state from the processor specific finite state container
//  GuiFsm or BdFsm.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::Start(void)
{
    CALL_TRACE("Start(void)");
#ifdef SIGMA_GUI_CPU
    PTargetState_ = &(const FsmState&)GuiFsm::GetGuiStartState();
    Activate_();
#endif
#ifdef SIGMA_BD_CPU
    PTargetState_ = &(const FsmState&)BdFsm::GetBdStartState();
    Activate_();
#endif
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Activate
//
//@ Interface-Description
//  The static Activate method sets the current FsmState to the target
//  state and messages the TaskAgentManager and TaskControlAgent
//  of the completed transition.  It then messages the current state
//  to "activate".
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::Activate_(void)
{
    CALL_TRACE("Activate_(void)");

    // transition complete so set target state to new state
    PCurrentState_ = &FsmState::GetTargetState();
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_FSMSTATE) >= 1)
        cout << "STATE CHANGED TO: "
             << FsmState::GetCurrentState().nameOf() << endl;
#endif
#ifdef SIGMA_GUI_CPU
    GuiState_ = PCurrentState_->getSigmaState();
#endif
#ifdef SIGMA_BD_CPU
    BdState_ = PCurrentState_->getSigmaState();
#endif
    TaskControlAgent::Update();

    TaskAgentManager::ReportNodeReady();
    TaskControlAgent::ReportNodeReady();

    PCurrentState_->activate();

    // $[TI1]	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTransition
//
//@ Interface-Description
//  The static StartTransition method sets the target FsmState to that
//  specified.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
FsmState::startTransition(void) const
{
    CALL_TRACE("startTransition(void)");

    PTargetState_ = this;

#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_FSMSTATE) >= 1)
        cout << "TARGET STATE CHANGED TO: "
             << FsmState::GetTargetState().nameOf() << endl;
#endif
    TaskControlAgent::Update();
    TaskAgentManager::ChangeState();
    TransitionTimer::GetTimer().set();
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetGuiState
//
//@ Interface-Description
//  Returns the current SigmaState of the GUI processor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the GUI state stored in the finite state machine.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
SigmaState
FsmState::GetGuiState(void)
{
    CALL_TRACE("GetGuiState(void)");
 
    return GuiState_; 
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetBdState
//
//@ Interface-Description
//  Returns the current SigmaState of the BD processor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the BD state stored in the finite state machine.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
SigmaState
FsmState::GetBdState(void)
{
    CALL_TRACE("GetBdState(void)");
 
    return BdState_;
    // $[TI1]
}
 
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
FsmState::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_FSMSTATE,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetGuiState
//
//@ Interface-Description
//   Sets the current GUI state information maintained by the finite state
//   machine.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
FsmState::SetGuiState(const GuiReadyMessage& message)
{
    CALL_TRACE("SetGuiState(GuiReadyMessage)");
    GuiState_ = message.getState();
    TaskControlAgent::Update();
    // $[TI1]
}
 
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetBdState
//
//@ Interface-Description
//   Sets the current BD state information maintained by the finite state
//   machine.  This method does not update the BD state if TUV has
//   declared a BD failure and the BD state is not SERVICE.  This means
//   that the GUI will only accept a state transition to SERVICE for
//   a BD in the FAILED state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
FsmState::SetBdState(const BdReadyMessage& message)
{
    CALL_TRACE("SetBdState(BdReadyMessage)");

    if ( BdState_ != STATE_FAILED || message.getState() == STATE_SERVICE )
    {
        // $[TI1]
        BdState_ = message.getState();
        TaskControlAgent::Update();
    }
    // $[TI2]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsTransitioning
//
//@ Interface-Description
//   Returns TRUE is the state machine is in transition from one state
//   to another.  This is TRUE when the target state is different from
//   the current state.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
Boolean
FsmState::IsTransitioning(void)
{
    CALL_TRACE("IsTransitioning(void)");

    // $[TI1.1]  $[TI1.2]
    return ( PTargetState_ != PCurrentState_ );
}

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

