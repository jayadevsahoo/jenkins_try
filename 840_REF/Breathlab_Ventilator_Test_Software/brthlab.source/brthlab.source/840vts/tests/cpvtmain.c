/*****************************************************************************/
/*   Module Name:   cpvtmain.c                                       
/*   Purpose:      Initialization of the application and support functions for       
/*                 main panels. General application flow control.                                                                                                         
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93> 
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Close the DAO connection or a			Hamsavally(CGS)   15 Oct,2000
/*	 ASSERT will fail.
/*	 
/*	 Printengine of crystal report is 		Hamsavally(CGS)   15 Oct,2000
/*	 closed.
/*
/*	 Fireupengine functionality is 
/* 	 replaced for test Executive removal.	Hamsavally(CGS)   27 Sep,2000
/*
/*   Add program revision to windows title bar
/*   Set command button color to transparent to make square outline 
/*    of rectangular region around round buttons disappear
/*   Align panels as loaded at runtime
/*                                          Dave Richardson   18 Nov, 2002
/*
/*   $Revision::   1.2         $                                          
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <windows.h>
#include <cvirte.h>		/* Needed if linking in external compiler; harmless otherwise */
#include <formatio.h>
#include <utility.h>

#include "globals.h"  // various typedefs and defines
#include "72361.h"
#include "72500.h"
#include "840.h"      // Init840ComStruct(), Is840Connected()
#include "840suprt.h"
#include "cpvtmain.h"
#include "cvicpreh.h"
#include "dbapi.h"
#include "detfun.h"
#include "easytab.h"
#include "helpinit.h"
#include "ldb.h"
#include "logonfun.h" // login(Ventfound, PTSfound)
#include "mainpnls.h"
#include "MsgPopup.h"
#include "pts.h"      // InitPTSComStruct(), IsPTSConnected()
#include "RT_BMPs.h"
#include "registry.h"
#include "regpwd.h"
#include "sysmenu.h"
#include "testing.h"
#include "testpnl.h"
#include "tt.h"
#include "usradmin.h"
#include "version.h"

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

int gPanelHandle[PANELS];
int gOPanelHandle[OPTIONPANELS];
int Months[12]=
{
	Ldb_Jan_Abrv,
	Ldb_Feb_Abrv,
	Ldb_Mar_Abrv,
	Ldb_Apr_Abrv,
	Ldb_May_Abrv,
	Ldb_Jun_Abrv,
	Ldb_Jul_Abrv,
	Ldb_Aug_Abrv,
	Ldb_Sep_Abrv,
	Ldb_Oct_Abrv,
	Ldb_Nov_Abrv,
	Ldb_Dec_Abrv
};

char *gSeqDescription;
char *gRevision = "$Revision::   1.2    $";   /* PVCS Revision string */
char gProgramRev[20] = "";
char gUUTPN[20] = "";
char gWorkOrderNumber[20] = "";
char gVentAssetNumber[20] = "";
char gCurrOperator[30] = "";
char gStationID[MAX_COMPUTERNAME_LENGTH + 1] = ""; // ASCII text report
char gRunNumberFile[ MAX_PATHNAME_LEN]; /* Name of .run file */
char gTestResFile[   MAX_PATHNAME_LEN]; /* Name of .db1 file */
char gTestRunFile[   MAX_PATHNAME_LEN]; /* Name of .db2 file */
char gSeqReportFile[ MAX_PATHNAME_LEN];
char gSerialNum[20] = "";
int gTxState;
int gSequenceRunning = FALSE;	//Set to 1 if in a sequence run, 0 otherwise
int gMustReset840 = 0; 
int gNumTests = 0;
int gPausedInd = 0;
int gStoppedInd = 0;
int gOverallpass = FAIL;
int gStationNum = 0;
int gJetSeqID;
long int gRunNumber = -1;
short gRptFileLock;

int sStatusDispVal = STAT_STOPPED;
int sStopOnFail = 1;
int sAbortFlag;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

void LoadPanels(int state);
static int  ExitTestExecutive(void);
static void SetupDevices(void);
void DisplayTestPanel(void);  

/*****************************************************************************/
/* Name:     main                                                   
/*                                                                           
/* Purpose:  Inital program function. Handles startup then launches to UI
/*                                          
/*                                                                           
/* Example Call:   int main(int argc, char *argv[]);                                    
/*                                                                           
/* Input:    argc - number of arguments passed
/*			 argv - array of arguments
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                         
/*                                                                           
/*****************************************************************************/
int main(int argc, char *argv[])
{
	LoadUserOptions(NULL);	
	LoadPanels(INITIAL);
	LoadPanels(SPLASHINIT);
	InitPTSComStruct();	
	Init840ComStruct();
	SetupDevices();
	
	LoadPanels(APPSTART1);
	if(!CurrUserCount()){
		UIAddUser();
	}
	login(Is840Connected()/*Ventfound*/, IsPTSConnected()/*PTSfound*/);
	RunUserInterface();

	return 0;
}																										 


/*****************************************************************************/
/* Name:     SetupDevices                                                    
/*                                                                           
/* Purpose:  Initiates detection of the devices
/*                                          
/*                                                                           
/* Example Call:   static void SetupDevices(void);                                    
/*                                                                           
/* Input:    <None>        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void SetupDevices(void)
{
	int PTSfound, Ventfound;

	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_PICTURE, ATTR_CTRL_VAL, 2);
	ProcessDrawEvents();
	Register_Splash_CB();

    PTSfound = !EstablishPTS(AUTO_DETECT);

	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_PICTURE, ATTR_CTRL_VAL, 1);
	ProcessDrawEvents();
	
	Ventfound = !Establish840(AUTO_DETECT);

	if(PTSfound){
		if(Ventfound){
			SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_PICTURE, ATTR_CTRL_VAL, 0);
			ProcessDrawEvents();
		}else{
			SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_PICTURE, ATTR_CTRL_VAL, 2);
			ProcessDrawEvents();
		}
	}
	else{
		if(Ventfound){
			SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_PICTURE, ATTR_CTRL_VAL, 1);
			ProcessDrawEvents();
		}else{
			SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_PICTURE, ATTR_CTRL_VAL, 2);
			ProcessDrawEvents();
		}
	}
	return;
}


/*****************************************************************************/
/* Name:     SetSplashVersionNum                                                     
/*                                                                           
/* Purpose:  Displays the software version on the splash panel
/*                                          
/*                                                                           
/* Example Call:   void SetSplashVersionNum(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void SetSplashVersionNum(void)
{
	char cBuff[256];
	char VersionStr[64];
	char EvalMsgStr[64];

	strcpy(cBuff, LDBf(Ldb_Splash_ProgramName));
	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_SW_LBL, ATTR_CTRL_VAL, cBuff);

	strcpy(cBuff, LDBf(Ldb_Copyright_Str));
	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_COPYRIGHT, ATTR_CTRL_VAL, cBuff);

	strcpy(cBuff, LDBf(Ldb_Copyright_Rights));
	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_COPYRIGHT_2, ATTR_CTRL_VAL, cBuff);

#ifndef BETA
	sprintf(VersionStr, "%s %s", LDBf(Ldb_Version_Str), VERSION);
	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_EVALMSG, ATTR_VISIBLE, FALSE);
#else
	sprintf(EvalMsgStr, "%s",        LDBf(Ldb_Splash_Eval_Msg));
	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_EVALMSG, ATTR_CTRL_VAL, EvalMsgStr);
	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_EVALMSG, ATTR_VISIBLE,  TRUE);
	sprintf(VersionStr, "%s %s. %s", LDBf(Ldb_Version_Str), VERSION, BETA);
#endif
	SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_SWVERSION, ATTR_CTRL_VAL, VersionStr);
}


extern void SetUpLanguagePanel(int EasyTab);

/*****************************************************************************/
/* Name:     LoadPanels                                                
/*                                                                           
/* Purpose:  Loads and displays panels based on the state of the app
/*                                          
/*                                                                           
/* Example Call:   void LoadPanels(int state);                                    
/*                                                                           
/* Input:    state - what state to go to       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void LoadPanels(int state)
{
	int nParentPanelHeight, nParentPanelWidth;
	int nPanelHeight, nPanelWidth;
	int nCtrlTop, nCtrlLeft, nCtrlHeight, nCtrlWidth;
	int nNewCtrlTop, nNewCtrlLeft, nNewCtrlHeight, nNewCtrlWidth;
	int nTempValue;
	HCTRL hCtrl;
	int EasyTab;
	int EasyTabWidth;
	Rect EasyTabBounds;
	int nBasePanelMenuHeight;
	char cBuff[256];

	switch(state){
		case INITIAL:
			gPanelHandle[SPLASH]    = LoadPanel(0, LDBf(Ldb_MainpanelFile), SPLASH);
			SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_SEARCHCANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			gPanelHandle[BASEPANEL] = LoadPanel(0, LDBf(Ldb_MainpanelFile), BASEPANEL);
			sprintf(cBuff, "%s - %s %s", LDBf(Ldb_ProgramName), LDBf(Ldb_Version_Str), VERSION);
			SetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_TITLE, cBuff);
			#ifdef DEMOMODE
			GetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_TITLE, cBuff);
			strcat(cBuff, LDBf(Ldb_DemoTitle));
			SetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_TITLE, cBuff);
			#endif
			SetSplashVersionNum();
			break;
		case SPLASHINIT:
			DisplayPanel(gPanelHandle[SPLASH]);
			break;
		case APPSTART1:
			DiscardPanel(gPanelHandle[SPLASH]);
			gPanelHandle[SPLASH] = 0;
			HidePanel(gPanelHandle[BASEPANEL]);
			SetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_VISIBLE, FALSE);
			break;
		case APPSTART2:
			SetMouseCursor(VAL_HOUR_GLASS_CURSOR);
			DiscardPanel(gPanelHandle[BASEPANEL]);
			gPanelHandle[BASEPANEL] = LoadPanel(0, LDBf(Ldb_MainpanelFile), BASEPANEL);
			sprintf(cBuff, "%s - %s %s", LDBf(Ldb_ProgramName), LDBf(Ldb_Version_Str), VERSION);
			SetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_TITLE, cBuff);
			#ifdef DEMOMODE
			GetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_TITLE, cBuff);
			strcat(cBuff, LDBf(Ldb_DemoTitle));
			SetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_TITLE, cBuff);
			#endif
			DisplayPanel(gPanelHandle[BASEPANEL]);
			SetActivePanel(gPanelHandle[BASEPANEL]); // active panel receives keyboard events

			gPanelHandle[SYSPANEL]   = LoadPanel(gPanelHandle[BASEPANEL], LDBf(Ldb_MainpanelFile), SYSPANEL);
			gPanelHandle[ADMINPANEL] = LoadPanel(gPanelHandle[SYSPANEL],  LDBf(Ldb_MainpanelFile), ADMINPANEL);
			gPanelHandle[PTSPANEL]   = LoadPanel(gPanelHandle[SYSPANEL],  LDBf(Ldb_MainpanelFile), PTSPANEL);
			gPanelHandle[_840PANEL]  = LoadPanel(gPanelHandle[SYSPANEL],  LDBf(Ldb_MainpanelFile), _840PANEL);
			gPanelHandle[TESTPANEL]  = LoadPanel(gPanelHandle[SYSPANEL],  LDBf(Ldb_MainpanelFile), TESTPANEL);
			gPanelHandle[LOOPPANEL]  = LoadPanel(gPanelHandle[LOOPPANEL], LDBf(Ldb_MainpanelFile), LOOPPANEL);

			InitTestReport();
			ChangeReportScreenOption(RPT_ALL);
			ChangeReportFileOption(RPT_ALL);
	  		GetReportFileName();
			InitDataRecBuffer();
		    ResetSeqDisplay();
		    OpenSeq();
		    Set840Panel();
			MainClockTimer(gPanelHandle[SYSPANEL], SYSPANEL_CLOCKTIMER, EVENT_TIMER_TICK, NULL, 0, 0);

			// Run-time adjustments until panel redesign
	        SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_STATUS_ACK,   ATTR_VISIBLE,     FALSE);
	        SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_SYSTEMSTATUS, ATTR_VISIBLE,     FALSE);
	        SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_SYSMESSAGE,   ATTR_VISIBLE,     FALSE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY,  ATTR_EXTRA_LINES, -1);

			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_HELP,   ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_ADMIN,  ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_REPORT, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_EXIT,   ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_TESTS,  ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_840,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_CMD_PTS,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			SetCtrlAttribute(gPanelHandle[PTSPANEL], PTSPANEL_CALIBRATE,  ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[PTSPANEL], PTSPANEL_RESET,      ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_CAPDIAGLOG,      ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_CLRDIAGLOG,      ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_SETCOMPHOURS,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_SETCOMPSERIAL,   ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_LOAD840FIRMWARE, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTUUT_DEV,  ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_PAUSE_BTN,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_STOP_BTN,     ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_RUNTEST_DEV,  ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_LOOPTEST_DEV, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			SetCtrlAttribute(gPanelHandle[LOOPPANEL], LOOPPANEL_OK,     ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gPanelHandle[LOOPPANEL], LOOPPANEL_CANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetPanelAttribute(gPanelHandle[LOOPPANEL], ATTR_CLOSE_CTRL, LOOPPANEL_CANCEL);

			// Pause and Stop not enabled until running a test
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_PAUSE_BTN,    ATTR_DIMMED,  TRUE);
			SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_STOP_BTN,     ATTR_DIMMED,  TRUE);
		    SetSysMenuMode(SYS_TEST);
		    
			SetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_FLOATING, VAL_FLOAT_NEVER);
			SetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_VISIBLE,  TRUE);

			EasyTab = EasyTab_Create(gPanelHandle[ADMINPANEL], 106, 170, 0, 0);
			EasyTab_LoadPanels(gPanelHandle[ADMINPANEL], EasyTab, 1,
			                   LDBf(Ldb_AdminpanelFile), __CVIUserHInst,
			                   USERCONFIG, &gOPanelHandle[USERCONFIG],
			                   TESTOPTS,   &gOPanelHandle[TESTOPTS], 
			                   LOGGINOPTS, &gOPanelHandle[LOGGINOPTS], 
			                   LANGCONF,   &gOPanelHandle[LANGCONF], 
			                   COMSETTING, &gOPanelHandle[COMSETTING],
			                   DB_CONF,    &gOPanelHandle[DB_CONF],
			                   0);
			GetPanelAttribute(gPanelHandle[ADMINPANEL], ATTR_WIDTH, &nPanelWidth);
			EasyTab_GetBounds(gPanelHandle[ADMINPANEL], EasyTab, VAL_EASY_TAB_INTERIOR_BOUNDS, &EasyTabBounds);
            EasyTabBounds.left = (nPanelWidth-EasyTabBounds.width)/2;
			// set width to that with all panels so when we hide the db panel we 
			// don't have to adjust all other panels
			// EasyTabBounds.width=549;
			EasyTab_SetBounds(gPanelHandle[ADMINPANEL], EasyTab, VAL_EASY_TAB_INTERIOR_BOUNDS, EasyTabBounds);
            // auto-detect popup is not a tab, so load separately
            gOPanelHandle[AUTODETECT] = LoadPanel(gOPanelHandle[COMSETTING], LDBf(Ldb_AdminpanelFile), AUTODETECT);
			SetCtrlAttribute(gOPanelHandle[AUTODETECT], AUTODETECT_ACCEPT, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			SetActiveCtrl(gOPanelHandle[USERCONFIG], USERCONFIG_ADDUSER);

			SetCtrlAttribute(gOPanelHandle[USERCONFIG], USERCONFIG_ADDUSER,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gOPanelHandle[USERCONFIG], USERCONFIG_DELETEUSER, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gOPanelHandle[USERCONFIG], USERCONFIG_USERPWD,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			SetCtrlAttribute(gOPanelHandle[TESTOPTS],   TESTOPTS_ENABLEALL,    ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			SetCtrlAttribute(gOPanelHandle[COMSETTING], COMSETTING_AUTO_DETECT_PTS, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
			SetCtrlAttribute(gOPanelHandle[COMSETTING], COMSETTING_AUTO_DETECT_840, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			SetCtrlAttribute(gOPanelHandle[DB_CONF], DB_CONF_PACKDB, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

			EasyTab_SetAttribute(gPanelHandle[ADMINPANEL], EasyTab, ATTR_EASY_TAB_LABEL_COLOR, VAL_WHITE);
			UserConfig(gOPanelHandle[USERCONFIG], USERCONFIG_USERLIST, -99, NULL, 0, 0);
			SetUpLanguagePanel(EasyTab);
			//Use the value sStopOnFails is created with for the default
			//NO, don't do that. It will remember it after you logout. Set the default here.
			SetCtrlVal(gOPanelHandle[TESTOPTS], TESTOPTS_STOPONFAILS, TRUE);
			//this sets the default startup panel
			SetPanelAttribute(gPanelHandle[TESTPANEL], ATTR_ZPLANE_POSITION, 0);

			GetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_MENU_HEIGHT, &nBasePanelMenuHeight);

			GetPanelAttribute(gPanelHandle[SYSPANEL],  ATTR_WIDTH,       &nParentPanelWidth);
			GetPanelAttribute(gPanelHandle[SYSPANEL],  ATTR_HEIGHT,      &nParentPanelHeight);
			GetCtrlAttribute(gPanelHandle[SYSPANEL],  SYSPANEL_SYSTEM_PANEL, ATTR_TOP, &nCtrlTop);
			nPanelHeight = nCtrlTop - nBasePanelMenuHeight - 1;
			nPanelWidth = nParentPanelWidth;

			SetPanelAttribute(gPanelHandle[PTSPANEL],   ATTR_LEFT,   0);
			SetPanelAttribute(gPanelHandle[PTSPANEL],   ATTR_TOP,    0 + nBasePanelMenuHeight);
			SetPanelAttribute(gPanelHandle[PTSPANEL],   ATTR_WIDTH,  nPanelWidth);
			SetPanelAttribute(gPanelHandle[PTSPANEL],   ATTR_HEIGHT, nPanelHeight);
			
			GetCtrlAttribute(gPanelHandle[PTSPANEL],  PTSPANEL_DECORATION, ATTR_TOP,    &nCtrlTop);
			GetCtrlAttribute(gPanelHandle[PTSPANEL],  PTSPANEL_DECORATION, ATTR_HEIGHT, &nCtrlHeight);
			nNewCtrlHeight = nPanelHeight - nCtrlTop;
			SetCtrlAttribute(gPanelHandle[PTSPANEL],  PTSPANEL_DECORATION, ATTR_HEIGHT, nNewCtrlHeight);

			GetCtrlAttribute(gPanelHandle[PTSPANEL],  PTSPANEL_DECORATION_3, ATTR_TOP,    &nCtrlTop);
			GetCtrlAttribute(gPanelHandle[PTSPANEL],  PTSPANEL_DECORATION_3, ATTR_HEIGHT, &nCtrlHeight);
			nNewCtrlHeight = nPanelHeight - nCtrlTop;
			SetCtrlAttribute(gPanelHandle[PTSPANEL],  PTSPANEL_DECORATION_3, ATTR_HEIGHT, nNewCtrlHeight);

			GetPanelAttribute(gPanelHandle[PTSPANEL], ATTR_NUM_CTRLS, &nTempValue);
			hCtrl = NewCtrl(gPanelHandle[PTSPANEL], CTRL_FLAT_BOX, "", -1, -1);
			SetCtrlAttribute(gPanelHandle[PTSPANEL], hCtrl, ATTR_WIDTH,       nPanelWidth+2);
			SetCtrlAttribute(gPanelHandle[PTSPANEL], hCtrl, ATTR_HEIGHT,      nPanelHeight+2);
			SetCtrlAttribute(gPanelHandle[PTSPANEL], hCtrl, ATTR_FRAME_COLOR, VAL_BLACK);
			SetCtrlAttribute(gPanelHandle[PTSPANEL], hCtrl, ATTR_CTRL_MODE,   VAL_INDICATOR);
			SetCtrlAttribute(gPanelHandle[PTSPANEL], hCtrl, ATTR_ZPLANE_POSITION, nTempValue);

			SetPanelAttribute(gPanelHandle[_840PANEL],  ATTR_LEFT,   0);
			SetPanelAttribute(gPanelHandle[_840PANEL],  ATTR_TOP,    0 + nBasePanelMenuHeight);
			SetPanelAttribute(gPanelHandle[_840PANEL],  ATTR_WIDTH,  nPanelWidth);
			SetPanelAttribute(gPanelHandle[_840PANEL],  ATTR_HEIGHT, nPanelHeight);
			
			GetCtrlAttribute(gPanelHandle[_840PANEL],  _840PANEL_DECORATION, ATTR_TOP,    &nCtrlTop);
			GetCtrlAttribute(gPanelHandle[_840PANEL],  _840PANEL_DECORATION, ATTR_HEIGHT, &nCtrlHeight);
			nNewCtrlHeight = nPanelHeight - nCtrlTop;
			SetCtrlAttribute(gPanelHandle[_840PANEL],  _840PANEL_DECORATION, ATTR_HEIGHT, nNewCtrlHeight);

			GetCtrlAttribute(gPanelHandle[_840PANEL],  _840PANEL_DECORATION_3, ATTR_TOP,    &nCtrlTop);
			GetCtrlAttribute(gPanelHandle[_840PANEL],  _840PANEL_DECORATION_3, ATTR_HEIGHT, &nCtrlHeight);
			nNewCtrlHeight = nPanelHeight - nCtrlTop;
			SetCtrlAttribute(gPanelHandle[_840PANEL],  _840PANEL_DECORATION_3, ATTR_HEIGHT, nNewCtrlHeight);

			GetPanelAttribute(gPanelHandle[_840PANEL], ATTR_NUM_CTRLS, &nTempValue);
			hCtrl = NewCtrl(gPanelHandle[_840PANEL], CTRL_FLAT_BOX, "", -1, -1);
			SetCtrlAttribute(gPanelHandle[_840PANEL], hCtrl, ATTR_WIDTH,       nPanelWidth+2);
			SetCtrlAttribute(gPanelHandle[_840PANEL], hCtrl, ATTR_HEIGHT,      nPanelHeight+2);
			SetCtrlAttribute(gPanelHandle[_840PANEL], hCtrl, ATTR_FRAME_COLOR, VAL_BLACK);
			SetCtrlAttribute(gPanelHandle[_840PANEL], hCtrl, ATTR_CTRL_MODE,   VAL_INDICATOR);
			SetCtrlAttribute(gPanelHandle[_840PANEL], hCtrl, ATTR_ZPLANE_POSITION, nTempValue);

			SetPanelAttribute(gPanelHandle[ADMINPANEL], ATTR_LEFT,   0);
			SetPanelAttribute(gPanelHandle[ADMINPANEL], ATTR_TOP,    0 + nBasePanelMenuHeight);
			SetPanelAttribute(gPanelHandle[ADMINPANEL], ATTR_WIDTH,  nPanelWidth);
			SetPanelAttribute(gPanelHandle[ADMINPANEL], ATTR_HEIGHT, nPanelHeight);
			
			GetCtrlAttribute(gPanelHandle[ADMINPANEL],  ADMINPANEL_DECORATION, ATTR_TOP,    &nCtrlTop);
			GetCtrlAttribute(gPanelHandle[ADMINPANEL],  ADMINPANEL_DECORATION, ATTR_HEIGHT, &nCtrlHeight);
			nNewCtrlHeight = nPanelHeight - nCtrlTop;
			SetCtrlAttribute(gPanelHandle[ADMINPANEL],  ADMINPANEL_DECORATION, ATTR_HEIGHT, nNewCtrlHeight);

			GetPanelAttribute(gPanelHandle[ADMINPANEL], ATTR_NUM_CTRLS, &nTempValue);
			hCtrl = NewCtrl(gPanelHandle[ADMINPANEL], CTRL_FLAT_BOX, "", -1, -1);
			SetCtrlAttribute(gPanelHandle[ADMINPANEL], hCtrl, ATTR_WIDTH,       nPanelWidth+2);
			SetCtrlAttribute(gPanelHandle[ADMINPANEL], hCtrl, ATTR_HEIGHT,      nPanelHeight+2);
			SetCtrlAttribute(gPanelHandle[ADMINPANEL], hCtrl, ATTR_FRAME_COLOR, VAL_BLACK);
			SetCtrlAttribute(gPanelHandle[ADMINPANEL], hCtrl, ATTR_CTRL_MODE,   VAL_INDICATOR);
			SetCtrlAttribute(gPanelHandle[ADMINPANEL], hCtrl, ATTR_ZPLANE_POSITION, nTempValue);

			SetPanelAttribute(gPanelHandle[TESTPANEL],  ATTR_FRAME_STYLE, VAL_HIDDEN_FRAME);
			SetPanelAttribute(gPanelHandle[TESTPANEL],  ATTR_LEFT,   0);
			SetPanelAttribute(gPanelHandle[TESTPANEL],  ATTR_TOP,    0 + nBasePanelMenuHeight);
			SetPanelAttribute(gPanelHandle[TESTPANEL],  ATTR_WIDTH,  nPanelWidth);
			SetPanelAttribute(gPanelHandle[TESTPANEL],  ATTR_HEIGHT, nPanelHeight);

			GetCtrlAttribute(gPanelHandle[TESTPANEL],  TESTPANEL_DECORATION, ATTR_TOP,    &nCtrlTop);
			GetCtrlAttribute(gPanelHandle[TESTPANEL],  TESTPANEL_DECORATION, ATTR_HEIGHT, &nCtrlHeight);
			nNewCtrlHeight = nPanelHeight - nCtrlTop;
			SetCtrlAttribute(gPanelHandle[TESTPANEL],  TESTPANEL_DECORATION, ATTR_HEIGHT, nNewCtrlHeight);

			GetCtrlAttribute(gPanelHandle[TESTPANEL],  TESTPANEL_TESTDISPLAY, ATTR_TOP,    &nCtrlTop);
			GetCtrlAttribute(gPanelHandle[TESTPANEL],  TESTPANEL_TESTDISPLAY, ATTR_HEIGHT, &nCtrlHeight);
			if(nCtrlTop + nCtrlHeight > nPanelHeight - 2){
				GetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, ATTR_VISIBLE_LINES, &nTempValue);
				while(nCtrlTop + nCtrlHeight > nPanelHeight - 2 && nTempValue > 1){
					nTempValue -= 1;
					SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, ATTR_VISIBLE_LINES, nTempValue);
					GetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, ATTR_TOP,    &nCtrlTop);
					GetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTDISPLAY, ATTR_HEIGHT, &nCtrlHeight);
				}
			}

			DisplayPanel(gPanelHandle[ADMINPANEL]);
			SetSysPanelMessage(""); // ensure status message area is blank
		    DisplayTestPanel();
			SetPanelAttribute(gPanelHandle[ADMINPANEL], ATTR_VISIBLE, TRUE);
			SetPanelAttribute(gPanelHandle[TESTPANEL],  ATTR_VISIBLE, TRUE);
			SetPanelAttribute(gPanelHandle[SYSPANEL],   ATTR_VISIBLE, TRUE);
       		SetTestButtonMode(TB_NORMAL);
			//if either of the devices are not detected disable the test buttons
			#ifndef DEMOMODE
			if(!IsPTSConnected() || !Is840Connected()){
				SetTestButtonMode(TB_DEVICE_NOT_DETECTED);
			}
			#endif // #ifndef DEMOMODE
			SetMouseCursor(VAL_DEFAULT_CURSOR);
			break;
		case APPRESTART:
			PauseTips();

            DiscardPanel(gOPanelHandle[AUTODETECT]);
            gOPanelHandle[AUTODETECT] = 0;

			DiscardPanel(gPanelHandle[PTSPANEL]);
			gPanelHandle[PTSPANEL] = 0;

			DiscardPanel(gPanelHandle[_840PANEL]);
			gPanelHandle[_840PANEL] = 0;

			DiscardPanel(gPanelHandle[ADMINPANEL]);
			gPanelHandle[ADMINPANEL] = 0;

			DiscardPanel(gPanelHandle[TESTPANEL]);
			gPanelHandle[TESTPANEL] = 0;

			DiscardPanel(gPanelHandle[SYSPANEL]);
			gPanelHandle[SYSPANEL] = 0;

			SetPanelAttribute(gPanelHandle[BASEPANEL], ATTR_VISIBLE, FALSE);
			ProcessDrawEvents();
			break;
	} // end switch(state)

	return;
}


/*****************************************************************************/
/* Name:     ExitTestExecutive                                                     
/*                                                                           
/* Purpose:  Exit the test executive, giving the  
/*      user a chance to save their work 
/*				JKE (8-17-99): Also close the DAO db connection
/*                                                                           
/* Example Call:   static int  ExitTestExecutive(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:  1 if quitting, 0 otherwise                                                   
/*                                                                           
/*****************************************************************************/
static int  ExitTestExecutive(void)
{
//	int popupResponse;

//	PBSR_Destroy(gNumTests);

	#ifdef CRPE_ENABLED
	CloseCrystalReportPrintEngine();	// Close Crystal Reports
	#endif
	CloseJetDB(); //we have to make sure we close the DAO connection or a ASSERT will fail.
	QuitUserInterface(1);

	return 1;
}


/*****************************************************************************/
/* Name:     ExitWrapper                                                 
/*                                                                           
/* Purpose:  Just calls the Exit function
/*                                          
/*                                                                           
/* Example Call:  int ExitWrapper(void);                                    
/*                                                                           
/* Input:    <None>      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   return value of ExitTestExecutive                                                        
/*                                                                           
/*****************************************************************************/
int ExitWrapper(void)
{
	PauseTips();
	ListDispose(TipList);

	return ExitTestExecutive();
}


/*****************************************************************************/
/* Name:     MainClockTimer                                                    
/*                                                                           
/* Purpose:  Timer to update system panel time
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK MainClockTimer(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments     
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                
/*                                                                           
/*****************************************************************************/
int CVICALLBACK MainClockTimer(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int Month, Day, Year;
	int Hour, Minute, Seconds;
	char DateString[13];
	char TimeString[9];

	switch(event){
		case EVENT_TIMER_TICK:
			GetSystemDate(&Month, &Day, &Year);
			sprintf(DateString, "%s %d, %d", LDBf(Months[Month-1]), Day, Year);
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_DATE, ATTR_CTRL_VAL, DateString);
			GetSystemTime(&Hour, &Minute, &Seconds);
			if(Minute >= 10){
				if(Seconds>=10){
					sprintf(TimeString, "%d:%d:%d", Hour, Minute, Seconds);
				}
				else{
					sprintf(TimeString, "%d:%d:0%d", Hour, Minute, Seconds);
				}
			}
			else{
				if(Seconds >= 10){
					sprintf(TimeString, "%d:0%d:%d", Hour, Minute, Seconds);
				}
				else{
					sprintf(TimeString, "%d:0%d:0%d", Hour, Minute, Seconds);
				}
			}
			SetCtrlAttribute(gPanelHandle[SYSPANEL], SYSPANEL_TIME, ATTR_CTRL_VAL, TimeString);
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:      MainCallback                                                   
/*                                                                           
/* Purpose:  Update time display, status LED display,
/*      handle close box on main panel, handle the   
/*      EVENT_END_TASK message.     
/*                                                                           
/* Example Call:   static int MainCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    input1 -	description          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   standard callback                                                         
/*                                                                           
/*****************************************************************************/
static int MainCallback(int panel, int control, int event,
                        void *callbackData, int eventData1, int eventData2)
{
	int returnVal = 0;

	if(event == EVENT_IDLE){
	; /*TimeFunction()*/
	}
	else{
		if(event == EVENT_CLOSE && panel == gPanelHandle[BASEPANEL]){
			LoadPanels(APPRESTART);
			login(Is840Connected()/*Ventfound*/, IsPTSConnected()/*PTSfound*/);
		}
	}

	return returnVal;
}


extern void DrawResultsOptMenu(void);

/*****************************************************************************/
/* Name:      MainPanelCallback                                                    
/*                                                                           
/* Purpose:  This is the main callback for Testpanel buttons
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK MainPanelCallback(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments        
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Always 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK MainPanelCallback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	int attrib;

	if(event == EVENT_COMMIT){
		switch(control){
			case TESTPANEL_TESTUUT_DEV:
				Register_Sys_CB();
				SetSysMenuMode(SYS_ALL_OFF);
				if(gPausedInd){
					gPausedInd = 0;
	            	SetTestButtonMode(TB_RUNNING);
				}
				else if(gStoppedInd){
					gStoppedInd = 0;
					SetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_TESTUUT_DEV, ATTR_DIMMED, TRUE);
				}
				else{
	            	// If you ever want to advise the operator if the .MDB file is
					// getting too large, uncomment the following line:
					// CheckDBSize();
	            	SetTestButtonMode(TB_RUNNING);
					PauseTips();
					RunTestUUT();
					UnPauseTips();
	            	SetTestButtonMode(TB_NORMAL);
					SetSysMenuMode(SYS_TEST);
				}
				break;
            case TESTPANEL_RUNTEST_DEV:
            case TESTPANEL_SEQUENCEDISPLAY:
				GetCtrlAttribute(panel, TESTPANEL_RUNTEST_DEV, ATTR_DIMMED, &attrib);
				if(attrib){
					break;
				}
				Register_Sys_CB();
            	SetSysMenuMode(SYS_ALL_OFF);
            	SetTestButtonMode(TB_RUNNING_SINGLE);
                RunSingleTest();
                SetSysMenuMode(SYS_TEST);
            	SetTestButtonMode(TB_NORMAL);
                break;
			case TESTPANEL_LOOPTEST_DEV:
				Register_Sys_CB();
				SetSysMenuMode(SYS_ALL_OFF);
            	SetTestButtonMode(TB_RUNNING);
				InstallPopup(gPanelHandle[LOOPPANEL]);
				SetSysMenuMode(SYS_TEST);
            	SetTestButtonMode(TB_NORMAL);
				break;
            case TESTPANEL_PAUSE_BTN:
				GetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_PAUSE_BTN, ATTR_DIMMED, &attrib);
            	if(!attrib){
					Register_Sys_CB();
            		SetSysPanelMessage(LDBf(Ldb_PauseMsg1));
            		gPausedInd = 1;
				}
            	break;
            case TESTPANEL_STOP_BTN:
				GetCtrlAttribute(gPanelHandle[TESTPANEL], TESTPANEL_STOP_BTN, ATTR_DIMMED, &attrib);
            	if(!attrib){
					Register_Sys_CB();
            		gStoppedInd = 1;
            		if(gPausedInd){
            			gPausedInd = 0;
            		}
                   if(!gMustReset840){
                        gMustReset840 = 1;
	            	SetTestButtonMode(TB_STOPPED);
						CPVTMessagePopup(LDBf(Ldb_test_aborted), LDBf(Ldb_cycle_before_test1), OK_ONLY_DLG, NO_HELP);
				  }		
            }
            	break;
		} // end switch(control)
	} // end if(event == EVENT_COMMIT)
	else if(event==EVENT_RIGHT_CLICK){
		switch(control){
			case TESTPANEL_TESTDISPLAY:
				DrawResultsOptMenu();
				break;
		} // end switch(control)
	} // end if(event==EVENT_RIGHT_CLICK)

	return 0;
}


/*****************************************************************************/
/* Name:      LoopCallback                                                      
/*                                                                           
/* Purpose:  Callback function for the loop test popup panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK LoopCallback(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                     
/*                                                                           
/*****************************************************************************/
int CVICALLBACK LoopCallback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	if(event == EVENT_COMMIT){
		switch(control){
            case LOOPPANEL_OK:
				SetSysMenuMode(SYS_ALL_OFF);
            	SetTestButtonMode(TB_RUNNING);
            	LoopTest();
				SetSysMenuMode(SYS_TEST);
            	SetTestButtonMode(TB_NORMAL);
	            // RemovePopup(0); // done by LoopTest(), above
	            break;
            case LOOPPANEL_CANCEL:
	            RemovePopup(0);
            	break;
		} // end switch(control)
	} // if(event == EVENT_COMMIT)
	return 0;
}


/*****************************************************************************/
/* Name:   BaseCallback                                                
/*                                                                           
/* Purpose:  Callback to handle pressing the windows X to close the program
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK BaseCallback(int panel, int event, void *callbackData,
/*		int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard panel callback arguments          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                   
/*                                                                           
/*****************************************************************************/

#ifdef DEMOMODE
static short icon_test_state = 0;
static short icon_test_mode  = 0;
#endif // #ifdef DEMOMODE

int CVICALLBACK BaseCallback(int panel, int event, void *callbackData,
		int eventData1, int eventData2)
{
	#ifdef DEMOMODE
	int state, i;
	#endif // #ifdef DEMOMODE

	switch(event){
		case EVENT_GOT_FOCUS:
			break;
		case EVENT_LOST_FOCUS:
			break;
		case EVENT_CLOSE:
			ExitWrapper();
			break;
		#ifdef DEMOMODE
		case EVENT_KEYPRESS:
			switch(eventData1){
			case VAL_UP_ARROW_VKEY:
				if(icon_test_state == 0 || icon_test_state == 1){
					icon_test_state++;
				}
				else{
					icon_test_state = 0;
				}
				break;
			case VAL_DOWN_ARROW_VKEY:
				if(icon_test_state == 2 || icon_test_state == 3){
					icon_test_state++;
				}
				else{
					icon_test_state = 0;
				}
				break;
			case VAL_LEFT_ARROW_VKEY:
				if(icon_test_state == 4 || icon_test_state == 6){
					icon_test_state++;
				}
				else{
					icon_test_state = 0;
				}
				break;
			case VAL_RIGHT_ARROW_VKEY:
				if(icon_test_state == 5 || icon_test_state == 7){
					icon_test_state++;
				}
				else{
					icon_test_state = 0;
				}
				break;
			} // end switch(eventData1)
			if(icon_test_state == 8){
				icon_test_state = 0;
				icon_test_mode++;
//				icon_test_mode %= 3;
				icon_test_mode %= 8;
				switch(icon_test_mode){
					case 0:
						state = TEST_CLEAR;
						break;
					case 1:
						state = TEST_RUNNING;
						break;
					case 2:
						state = TEST_PASS;
						break;
					case 3:
						state = TEST_FAIL;
						break;
					case 4:
						state = TEST_SKIP;
						break;
					case 5:
						state = TEST_LOOPING;
						break;
					case 6:
						state = TEST_ABORT;
						break;
					case 7:
						state = TEST_ERROR;
						break;
				} // end switch(icon_test_mode)
				for(i=0; i<gNumTests; i++){
//					SetCheckXTest(i, state);
					UpdateTestListing(i, state);
				}
			} // end if(icon_test_state == 8)
			break;
		#endif // #ifdef DEMOMODE
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:      SplashCallback                                                    
/*                                                                           
/* Purpose:  Handles pressing the cancel button during device detect on the splash panel
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK SplashCallback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   always 0                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK SplashCallback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event){
		case EVENT_COMMIT:
			StopDetect();
			SetCtrlVal(gPanelHandle[SPLASH], SPLASH_COMSTATUS, LDBf(Ldb_Cncling_Pls_Wait));
			SetCtrlAttribute(gPanelHandle[SPLASH], SPLASH_SEARCHCANCEL, ATTR_DIMMED, TRUE);
			break;
	} // end switch(event)

	return 0;
}


