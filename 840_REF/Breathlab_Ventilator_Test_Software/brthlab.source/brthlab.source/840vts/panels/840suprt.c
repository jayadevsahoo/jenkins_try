/*****************************************************************************/
/*   Module Name:    840SUPRT.c                                            
/*   Purpose:      allback and support functions for the 840 panel.                               
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Changed the "AlarmLog" to "AlrmLog"    RHJ               29 Feb,2008    
/*   in the header of the Alarm log file.                                                          
/*
/*   Added AlarmLog function.               RHJ               27 Nov,2007    
/*
/*   Critical Observation during Phase1 	Kathirvel(CGS)    27 Sep,2000
/* 	 is incorportated.						
/*    
/*   Added event handler for button on      Dave Richardson   15 Nov, 2002
/*   840 functions panel that starts
/*   the software download procedure
/*     
/*   $Revision::   1.3      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <utility.h>
#include <userint.h>

#include "840.h"
#include "840cmds.h"
#include "cpvtmain.h"
#include "detfun.h"
#include "helpinit.h"
#include "ldb.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "registry.h"
#include "sysmenu.h"
#include "testing.h"
#include "version.h"

extern int StartDload(void); // 75677.c

/***********************************************/
/******************* DEFINES *******************/
/***********************************************/

#ifdef DEMOMODE
//#define DEMOWRAP(x) OK_840; Delay(3);
#define DEMOWRAP(x)	x 
#else
#define DEMOWRAP(x)	x 
#endif

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

static int CompEnterWasPressed=0;

#ifdef DEMOMODE
// DiagLog
static char *(DemoDiagLog[]) = {
	{"13:00:29 10 May 00	Date/time before change	E00001		Event  0x0"},
	{"14:55:48 18 Apr 00	Service Switch Stuck Test	LP0121	Alert	PC: 0x00000000 EV: 0	NMI: 0xC0 ErrCode: 0xCC"}
};

// InfoLog
static char *(DemoInfoLog[]) = {
"13:00:19 10 May 00	Date/time vent clock synchronized	E00007  ",	
"09:13:45 02 May 00	Init Resume GUI communication	LB0083	",
"09:13:44 02 May 00	Init Resume BD communication	ZB0084	",
"12:32:13 01 May 00	Date/time vent clock synchronized	E00007	",
"12:32:27 01 May 00	Init Resume GUI communication	LB0083	",
"12:32:13 01 May 00	Init Resume BD communication	ZB0084	",
"15:12:52 18 Apr 00	Compressor S/N updated	LB0087	",
"14:59:15 18 Apr 00	Init Resume GUI communication	LB0083	",
"14:59:15 18 Apr 00	Init Resume BD communication	ZB0084	",
"14:59:03 18 Apr 00	DCI input buffer overflow error	ZC2001	Alert",
"14:55:49 18 Apr 00	Init Resume GUI communication	LB0083	",
"14:55:49 18 Apr 00	Init Resume BD communication	ZB0084	",
"14:49:35 18 Apr 00	Date/time vent clock synchronized	E00007	",
"14:49:36 18 Apr 00	Init Resume GUI communication	LB0083	",
"14:49:35 18 Apr 00	Init Resume BD communication	ZB0084	",
"13:05:01 17 Apr 00	Date/time vent clock synchronized	E00007	",
"13:05:04 17 Apr 00	Init Resume GUI communication	LB0083	",
"13:05:00 17 Apr 00	Init Resume BD communication	ZB0084	"
};

// TestLog
static char *(DemoTestLog[]) = {
"16:02:38 18 Apr 00	{T:SST: {N:COMPLETED}}		{T:Outcome:  {N:PASSED}}"
};
#endif // #ifdef DEMOMODE
/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

int CVICALLBACK CompPanelCB(int panel, int event, void *callbackData,
		int eventData1, int eventData2)
{
	switch(event){
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_ENTER_VKEY:
					CompEnterWasPressed = 1;
					break;
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return 0;
}


/*****************************************************************************/
/* Name:      Set840Panel                                                  
/*                                                                           
/* Purpose:  Sets the 840 panel buttons 
/*                                          
/*                                                                           
/* Example Call:  void Set840Panel(void);                                    
/*                                                                           
/* Input:    <none>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void Set840Panel(void)
{
	int nVisibility;

	if(GetCSEEnabled()){
		nVisibility = TRUE;
	}
	else{
		nVisibility = FALSE;
	}

	SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_SETCOMPHOURS,          ATTR_VISIBLE, nVisibility);
	SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_RESET_COMP_HRS_LABEL,  ATTR_VISIBLE, nVisibility);

	SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_SETCOMPSERIAL,         ATTR_VISIBLE, nVisibility);
	SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_SET_COMP_SERNUM_LABEL, ATTR_VISIBLE, nVisibility);

	return;
}


/*****************************************************************************/
/* Name:     Set840PnlDimm                                               
/*                                                                           
/* Purpose:  Dimms or undimms buttons on the 840 panel
/*                                          
/*                                                                           
/* Example Call:   static void Set840PnlDimm(int dimm);                                    
/*                                                                           
/* Input:    dimm - 0=undimm buttons, 1=dimm buttons         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void Set840PnlDimm(int dimm)
{
	SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_CAPDIAGLOG,      ATTR_DIMMED, dimm);
	SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_CLRDIAGLOG,      ATTR_DIMMED, dimm);
	SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_SETCOMPHOURS,    ATTR_DIMMED, dimm);
	SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_SETCOMPSERIAL,   ATTR_DIMMED, dimm);
	SetCtrlAttribute(gPanelHandle[_840PANEL], _840PANEL_LOAD840FIRMWARE, ATTR_DIMMED, dimm);

	return;
}	

/*****************************************************************************/
/* Name:     Set840PnlStatus                                                      
/*                                                                           
/* Purpose:  Sets the 840 panel to display a message
/*                                          
/*                                                                           
/* Example Call:   static void Set840PnlStatus(char* cbuff);                                    
/*                                                                           
/* Input:    cbuff - message to display         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void Set840PnlStatus(char* cbuff)
{
	if(cbuff != NULL){
		SetSysMenuMode(SYS_ALL_OFF);
		SetSysPanelMessage(cbuff);
		Set840PnlDimm(TRUE);
	}
	else{
		SetSysMenuMode(SYS_840);
		SetSysPanelMessage("");
		Set840PnlDimm(FALSE);
	}

	return;
}


extern int UploadStatus;
extern FILE* UploadFile;

/*****************************************************************************/
/* Name:    DiagLog                                                    
/*                                                                           
/* Purpose:  Gets the Diagnostic log from the 840
/*                                          
/*                                                                           
/* Example Call:  static int DiagLog(char* Filename, int CSEenabled);                                    
/*                                                                           
/* Input:    Filename - name of file to append log to.
/*			 CSEenabled - if 1 appends log to standard CSE file.
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   OK_840 on success, else FAIL_840                                                         
/*                                                                           
/*****************************************************************************/
static int DiagLog(char* Filename, int CSEenabled)
{
	int statusFlag;
	char filepath[MAX_PATHNAME_LEN];
	char* filename;
	char* pathname;

	//filename = malloc(MAX_PATHNAME_LEN);
	//pathname = malloc(MAX_PATHNAME_LEN);
	filename = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.
	pathname = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.
	
	statusFlag = OK_840;
	UploadStatus = OK_840 ;

	if(CSEenabled){
		strcpy(filepath, LDBf(Ldb_CSE_Dir));
//		if(SetDir(LDBf(Ldb_CSE_Dir)) == -1){
//			MakeDir(LDBf(Ldb_CSE_Dir));
//		}
		strcat(filepath, LDBf(Ldb_CSE_DiagLog));
		strcpy(Filename, filepath);
	}
	if((UploadFile = fopen(Filename, "a+")) == NULL){
		free(filename);
		free(pathname);
		return FAIL_840; // error condition
	}
	fprintf(UploadFile, "SysLog Pair of  %s &  %s\n", Vent.Bd.serial, Vent.Gui.serial);

	#ifndef DEMOMODE
	if(GetSystemLog() != SUCCESS){
		statusFlag = FAIL_840;
	}
	#else
	{
		int i, j;
		j = sizeof(DemoDiagLog)/sizeof(char *);
		for(i = 0; i < j; i++){
			fprintf(UploadFile, "%s\n", DemoDiagLog[i]);
		}
	}
	#endif

	fprintf(UploadFile, "End\n");
	fclose(UploadFile);

	if(UploadStatus == FAIL_840){
		statusFlag = FAIL_840;
	}

	free(filename);
	free(pathname);

	return statusFlag;
}


/*****************************************************************************/
/* Name:    InfoLog                                                    
/*                                                                           
/* Purpose:  Gets the Info log from the 840
/*                                          
/*                                                                           
/* Example Call:  static int InfoLog(char* Filename, int CSEenabled);                                    
/*                                                                           
/* Input:    Filename - name of file to append log to.
/*			 CSEenabled - if 1 appends log to standard CSE file.
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   OK_840 on success, else FAIL_840                                                         
/*                                                                           
/*****************************************************************************/
static int InfoLog(char* Filename, int CSEenabled)
{
	int statusFlag;
	char filepath[MAX_PATHNAME_LEN];
	char* filename;
	char* pathname;

	filename = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.
	pathname = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.
	
	statusFlag = OK_840;
	UploadStatus = OK_840 ;

	if(CSEenabled){
		strcpy(filepath, LDBf(Ldb_CSE_Dir));
//		if(SetDir(LDBf(Ldb_CSE_Dir)) == -1){
//			MakeDir(LDBf(Ldb_CSE_Dir));
//		}
		strcat(filepath, LDBf(Ldb_CSE_InfoLog));
		strcpy(Filename, filepath);
	}
	if((UploadFile = fopen(Filename, "a+")) == NULL){
		free(filename);
		free(pathname);
		return FAIL_840; // error condition
	}
	fprintf(UploadFile, "InfoLog Pair of %s &  %s\n", Vent.Bd.serial, Vent.Gui.serial);

	#ifndef DEMOMODE
	if(GetInfoLog() != SUCCESS){
		statusFlag = FAIL_840;
	}
	#else
	{
		int i, j;
		j = sizeof(DemoInfoLog)/sizeof(char *);
		for(i = 0; i < j; i++){
			fprintf(UploadFile, "%s\n", DemoInfoLog[i]);
		}
	}
	#endif

	fprintf(UploadFile,"End\n");
	fclose(UploadFile);

	if(UploadStatus == FAIL_840){
		statusFlag = FAIL_840;
	}

	free(filename);
	free(pathname);

	return statusFlag;
}


/*****************************************************************************/
/* Name:    TestLog                                                    
/*                                                                           
/* Purpose:  Gets the Test log from the 840
/*                                          
/*                                                                           
/* Example Call:  static int TestLog(char* Filename, int CSEenabled);                                    
/*                                                                           
/* Input:    Filename - name of file to append log to.
/*			 CSEenabled - if 1 appends log to standard CSE file.
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   OK_840 on success, else FAIL_840                                                         
/*                                                                           
/*****************************************************************************/
static int TestLog(char* Filename, int CSEenabled)
{
	int statusFlag;
	char filepath[MAX_PATHNAME_LEN];
	char* filename;
	char* pathname;

	filename = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.
	pathname = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.
	
	statusFlag = OK_840;
	UploadStatus = OK_840 ;

	if(CSEenabled){
		strcpy(filepath, LDBf(Ldb_CSE_Dir));
//		if(SetDir(LDBf(Ldb_CSE_Dir)) == -1){
//			MakeDir(LDBf(Ldb_CSE_Dir));
//		}
		strcat(filepath, LDBf(Ldb_CSE_TestLog));
		strcpy(Filename, filepath);
	}
	if((UploadFile = fopen(Filename, "a+")) == NULL){
		free(filename);
		free(pathname);
		return FAIL_840; // error condition
	}
	fprintf(UploadFile, "StLog Pair of  %s &  %s\n", Vent.Bd.serial, Vent.Gui.serial);

	#ifndef DEMOMODE
	if(GetStLog() != SUCCESS){
		statusFlag = FAIL_840;
	}
	#else
	{
		int i, j;
		j = sizeof(DemoTestLog)/sizeof(char *);
		for(i = 0; i < sizeof(DemoTestLog)/sizeof(char *); i++){
			fprintf(UploadFile, "%s\n", DemoTestLog[i]);
		}
	}
	#endif

	fprintf(UploadFile,"End\n");
	fclose(UploadFile);
	if(UploadStatus == FAIL_840){
		statusFlag = FAIL_840;
	}

	free(filename);
	free(pathname);

	return statusFlag;
}


/*****************************************************************************/
/* Name:    AlarmLog                                                    
/*                                                                           
/* Purpose:  Gets the Alarm log from the 840
/*                                          
/*                                                                           
/* Example Call:  static int AlarmLog(char* Filename, int CSEenabled);                                    
/*                                                                           
/* Input:    Filename - name of file to append log to.
/*			 CSEenabled - if 1 appends log to standard CSE file.
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   OK_840 on success, else FAIL_840                                                         
/*                                                                           
/*****************************************************************************/
static int AlarmLog(char* Filename, int CSEenabled)
{
	int statusFlag;
	char filepath[MAX_PATHNAME_LEN];
	char* filename;
	char* pathname;

	filename = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.
	pathname = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.
	
	statusFlag = OK_840;
	UploadStatus = OK_840 ;

	if(CSEenabled){
		strcpy(filepath, LDBf(Ldb_CSE_Dir));
//		if(SetDir(LDBf(Ldb_CSE_Dir)) == -1){
//			MakeDir(LDBf(Ldb_CSE_Dir));
//		}
		strcat(filepath, LDBf(Ldb_CSE_AlarmLog));
		strcpy(Filename, filepath);
	}
	if((UploadFile = fopen(Filename, "a+")) == NULL){
		free(filename);
		free(pathname);
		return FAIL_840; // error condition
	}
	fprintf(UploadFile, "AlrmLog Pair of  %s &  %s\n", Vent.Bd.serial, Vent.Gui.serial);

	#ifndef DEMOMODE
	if(GetAlarmLog() != SUCCESS){
		statusFlag = FAIL_840;
	}
	#else
	{
		int i, j;
		j = sizeof(DemoTestLog)/sizeof(char *);
		for(i = 0; i < sizeof(DemoTestLog)/sizeof(char *); i++){
			fprintf(UploadFile, "%s\n", DemoTestLog[i]);
		}
	}
	#endif

	fprintf(UploadFile,"End\n");
	fclose(UploadFile);
	if(UploadStatus == FAIL_840){
		statusFlag = FAIL_840;
	}

	free(filename);
	free(pathname);

	return statusFlag;
}


/*****************************************************************************/
/* Name:     SetCompressorSerial                                                    
/*                                                                           
/* Purpose:  UI to change the compressor serial number
/*                                          
/*                                                                           
/* Example Call:   void SetCompressorSerial(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void SetCompressorSerial(void)
{
	int err;
	int status;
	int handle;
	int answer;
	int control;
	int doneFlag;
	int compPanel;
	char compBuff[256];
	int nCtrlLeft;
	int nCtrlWidth;

	/* Retrieve the front panel */
	compPanel = LoadPanel(0, LDBf(Ldb_MainpanelFile), COMP_PANEL);
	SetCtrlAttribute(compPanel, COMP_PANEL_COMP_SERIAL, ATTR_VISIBLE, TRUE);
	SetCtrlAttribute(compPanel, COMP_PANEL_COMP_TIME,   ATTR_VISIBLE, FALSE);

	// Run-time adjustments until panel redesign
	GetCtrlAttribute(compPanel, COMP_PANEL_DECORATION, ATTR_LEFT,  &nCtrlLeft);
	GetCtrlAttribute(compPanel, COMP_PANEL_DECORATION, ATTR_WIDTH, &nCtrlWidth);
	SetCtrlAttribute(compPanel, COMP_PANEL_OLDINFO, ATTR_LEFT,  nCtrlLeft + 2);
	SetCtrlAttribute(compPanel, COMP_PANEL_OLDINFO, ATTR_WIDTH, nCtrlWidth - 4);

	SetCtrlAttribute(compPanel, COMP_PANEL_COMP_DONE,   ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(compPanel, COMP_PANEL_COMP_CANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

	SetCtrlVal(compPanel, COMP_PANEL_OLDINFO, Vent.Compressor.serial);
	SetActiveCtrl(compPanel, COMP_PANEL_COMP_SERIAL);

	status = OK_840;
	DisplayPanel(compPanel);
	doneFlag = 0;

	while(!doneFlag){ /* Wait for operator to select OK  */
		DisplayPanel(compPanel);
		GetUserEvent(0, &handle, &control); /* Check for user event */
		if(CompEnterWasPressed){
			handle = compPanel; //get past the next check
			control = COMP_PANEL_COMP_DONE; //fake that the done button was pressed
			CompEnterWasPressed = 0; //clear the flag
		}
		if(handle != compPanel){
			continue;
		}

		switch(control){
			case COMP_PANEL_COMP_CANCEL:
				DiscardPanel(compPanel);
				Set840PnlStatus(NULL);
				return;			

			case COMP_PANEL_COMP_DONE:
				status = OK_840;
				answer = CPVTConfirmPopup(LDBf(Ldb_Warning), LDBf(Ldb_Set_Compressor), OK_CANCEL_DLG, NO_HELP);
				if(answer){
					GetCtrlVal(compPanel, COMP_PANEL_COMP_SERIAL, compBuff);
					Set840PnlStatus(LDBf(Ldb_Set_Comp_SN_status));
					err = DEMOWRAP(SetCompressorData(REMOTE_TEST_SET_COMPRESSOR_SN_ID, compBuff));
					if(err){
						status = FAIL;
					}
					else{
						// RFH: For some reason, the vent takes awhile to update the serial number even
						// though it acknowledges right away.  Hence the importance here of actually
						// verifying that the serial number was set as appropriate. Change this delay.
						Delay(3);
						GetVentInfo(); // Update values.
					}
					Set840PnlStatus(NULL);
				}
				doneFlag = 1;
				break;

			default:
				continue;
		} // end switch(control)

		if(PartInstalled == NO_840){
			CPVTMessagePopup(LDBf(Ldb_status), LDBf(Ldb_Comp_not_installed), OK_ONLY_DLG, NO_HELP);
			TestFail = YES_840;
		}

		if(TestFail == YES_840){  /* TestFail gets refresh during TestStart */
			answer = CPVTConfirmPopup(LDBf(Ldb_Comp_set_fail), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP);
			if(!answer){
				doneFlag = 1;
				status = FAIL;
			}
			else{
				TestFail = NO_840;        /* reset test flag */
				PartInstalled = YES_840;
			}
		}
	} // end while(!doneFlag)
	DiscardPanel(compPanel);

	return;
}


/*****************************************************************************/
/* Name:     SetCompressorHours                                                    
/*                                                                           
/* Purpose:  UI to set compressor run time hours
/*                                          
/*                                                                           
/* Example Call:   void SetCompressorHours(void);                                    
/*                                                                           
/* Input:    <none>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void SetCompressorHours(void)
{
	int err;
	int status;
	int handle;
	int answer;
	int control;
	int doneFlag;
	int compPanel;
	char compBuff[256];
	int nCtrlLeft;
	int nCtrlWidth;

	/* Retrieve the front panel */
	compPanel = LoadPanel(0, LDBf(Ldb_MainpanelFile), COMP_PANEL);
	SetCtrlAttribute(compPanel, COMP_PANEL_COMP_SERIAL, ATTR_VISIBLE, FALSE);
	SetCtrlAttribute(compPanel, COMP_PANEL_COMP_TIME,   ATTR_VISIBLE, TRUE);

	// Run-time adjustments until panel redesign
	GetCtrlAttribute(compPanel, COMP_PANEL_DECORATION, ATTR_LEFT,  &nCtrlLeft);
	GetCtrlAttribute(compPanel, COMP_PANEL_DECORATION, ATTR_WIDTH, &nCtrlWidth);
	SetCtrlAttribute(compPanel, COMP_PANEL_OLDINFO, ATTR_LEFT,  nCtrlLeft + 2);
	SetCtrlAttribute(compPanel, COMP_PANEL_OLDINFO, ATTR_WIDTH, nCtrlWidth - 4);

	SetCtrlAttribute(compPanel, COMP_PANEL_COMP_DONE,   ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(compPanel, COMP_PANEL_COMP_CANCEL, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);

	sprintf(compBuff, LDBf(Ldb_comp_hours_f), Vent.Compressor.runTimeHours);
	SetCtrlVal(compPanel, COMP_PANEL_OLDINFO, compBuff);
	SetActiveCtrl(compPanel, COMP_PANEL_COMP_TIME);

	status = OK_840;
	DisplayPanel(compPanel);
	doneFlag = 0;

	while(!doneFlag){ /* Wait for operator to select OK  */
		DisplayPanel(compPanel);
		GetUserEvent(0, &handle, &control); /* Check for user event */
		if(CompEnterWasPressed){
			handle = compPanel; //get past the next check
			control = COMP_PANEL_COMP_DONE; //fake that the done button was pressed
			CompEnterWasPressed = 0; //clear the flag
		}
		if(handle != compPanel){
			continue;
		}

		switch(control){
			case COMP_PANEL_COMP_CANCEL:
				DiscardPanel(compPanel);
				Set840PnlStatus(NULL);
				return;			

			case COMP_PANEL_COMP_DONE:
				status = OK_840;
				answer = CPVTConfirmPopup(LDBf(Ldb_Warning), LDBf(Ldb_Set_Compressor), OK_CANCEL_DLG, NO_HELP);
				if(answer){
					GetCtrlVal(compPanel, COMP_PANEL_COMP_TIME, compBuff);
					Set840PnlStatus(LDBf(Ldb_Set_Comp_hours_status));
					err = DEMOWRAP(SetCompressorData(REMOTE_TEST_SET_COMPRESSOR_TIME_ID, compBuff));
					if(err){
						status = FAIL;
					}
					else{
						// RFH: For some reason, the vent takes awhile to update the serial number even
						// though it acknowledges right away.  Hence the importance here of actually
						// verifying that the serial number was set as appropriate. Change this delay.
						Delay(3);
						GetVentInfo(); // Update values.
					}
					Set840PnlStatus(NULL);
				}
				doneFlag = 1;
				break;

			default:
				continue;
		} // end switch(control)

		if(PartInstalled == NO_840){
			CPVTMessagePopup(LDBf(Ldb_status), LDBf(Ldb_Comp_not_installed), OK_ONLY_DLG, NO_HELP);
			TestFail = YES_840;
		}

		if(TestFail == YES_840){  /* TestFail gets refresh during TestStart */
			answer = CPVTConfirmPopup(LDBf(Ldb_Comp_set_fail), LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP);
			if(!answer){
				doneFlag = 1;
				status = FAIL;
			}
			else{
				TestFail = NO_840;        /* reset test flag */
				PartInstalled = YES_840;
			}
		}
	} /* end while */
	DiscardPanel(compPanel);

	return;
}


/*****************************************************************************/
/* Name:      Set840Callback                                                    
/*                                                                           
/* Purpose:  840 panel callback
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK Set840Callback(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments      
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK Set840Callback(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	char filepath[MAX_PATHNAME_LEN];
	char* filename;
	char* pathname;
	int response, selstat;

	filename = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.
	pathname = (char *)malloc(MAX_PATHNAME_LEN); // Modified the statement due to Critical Observation.

	switch(event){
		case EVENT_COMMIT:

		// Dont even allow any of these functions unless the 840 is detected.
		Register_Sys_CB();
		Set840PnlStatus(LDBf(Ldb_EstabCom_840));
		if(Establish840(NO_AUTO_DETECT) != SUCCESS){
			CPVTMessagePopup(LDBf(Ldb_Com_Error), LDBf(Ldb_840_Com_Error), OK_ONLY_DLG, NO_HELP);
			Set840PnlStatus(NULL);
			free(filename);
			free(pathname);
			return 0; 
		}
//		Set840PnlStatus(NULL);

		switch(control){
			case _840PANEL_CAPDIAGLOG:
				Set840PnlStatus(LDBf(Ldb_Retrieving_logs));
//				if(GetCSEEnabled())
//				{
					DEMOWRAP(DiagLog(filepath, 1));
					DEMOWRAP(InfoLog(filepath, 1));
					DEMOWRAP(TestLog(filepath, 1));

                    // Checks if the Alarm Logs can be downloaded from the vent.
                    if(IsAlarmLogDownloadable())
                    {
                        DEMOWRAP(AlarmLog(filepath,1));
                    }
//				}else{
//					CheckForNoNameLog(&pathname, &filename, TEST_LOG);
//					selstat = FileSelectPopup(pathname, filename, "", LDBf(Ldb_Select_Filename),
//											   VAL_SAVE_BUTTON, 0, 0, 1, 1,
//											   filepath);
//					if(selstat == VAL_NO_FILE_SELECTED)
//					{
//						Set840PnlStatus(NULL);
//						free(filename);
//						free(pathname);
//						return(FAIL_840);
//					}
//				    SetLogInfo(pathname, LOG_TESTPATH_KEY);
//					SetLogInfo(filename, LOG_TESTFILE_KEY);
//					DEMOWRAP(DiagLog(filepath, 0));
//					DEMOWRAP(InfoLog(filepath, 0));
//					DEMOWRAP(TestLog(filepath, 0));
//				}
//				Set840PnlStatus(NULL);
				break;

			case _840PANEL_CLRDIAGLOG:
				Set840PnlStatus(LDBf(Ldb_Clear_sys_logs));
				response = CPVTConfirmPopup(LDBf(Ldb_Confirm_clear), LDBf(Ldb_Clear_Sys_logs), OK_CANCEL_DLG, NO_HELP);
				if(response){
					DEMOWRAP(VentDoFunction(DELETE_SYSTEM_DIAGNOSTIC_LOG_ID));
					DEMOWRAP(VentDoFunction(DELETE_SYSTEM_INFORMATION_LOG_ID));
					DEMOWRAP(VentDoFunction(DELETE_SELFTEST_LOG_ID));
				}
				break;

			case _840PANEL_SETCOMPSERIAL:
				Set840PnlStatus(NULL);
				SetCompressorSerial();
				break;

			case _840PANEL_SETCOMPHOURS:
				Set840PnlStatus(NULL);
				SetCompressorHours();
				break;

			default:
				break;
		} // end switch(control)

		Set840PnlStatus(NULL);
		break;

	} // end switch(event)

	free(filename);
	free(pathname);

	return 0;
}


/*****************************************************************************/
/* Name:      Load840SftwCallback                                                    
/*                                                                           
/* Purpose:  Handler for COMMITT event on command button that starts
/*           software download procedure
/*                                          
/*                                                                           
/* Example Call:   int CVICALLBACK Load840SftwCallback(int panel, int control, int event,
/*		void *callbackData, int eventData1, int eventData2);                                    
/*                                                                           
/* Input:    standard callback arguments      
/*                                                                           
/* Output:   software download procedure                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                  
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
int CVICALLBACK Load840SftwCallback (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch(event){
		case EVENT_COMMIT:
			Set840PnlStatus(NULL);
			StartDload();
			Set840PnlStatus(NULL);

			break;

		default:
			break;
	}

	return 0;
}
