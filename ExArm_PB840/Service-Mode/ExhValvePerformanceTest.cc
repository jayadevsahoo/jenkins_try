#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  ExhValvePerformanceTest - Test exhalation valve performance
//	against the calibration table
//---------------------------------------------------------------------
//@ Interface-Description
//	This class implements the exhalation valve calibration table test,
//	which verifies that the exhalation valve calibration table
//	contains valid exhalation valve command vs pressure data.  This
//	is done by checking that the EV commands corresponding to
//	three test pressures will cause the EV to create those test
//	pressures within margin.
//
//	NOTE: the 840 Ventilator Self-Tests Spec refers to this test as the
//	"Expiratory Valve Calibration Table Test", and the GUI display refers
//	to this test as the "Exp Valve Test".
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the exhalation valve calibration table test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs the exhalation valve calibration table test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ExhValvePerformanceTest.ccv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date: 10-Oct-1997    DR Number: DCS 2262
//       Project:  Sigma (840)
//       Description:
//			Added comment to class-level Interface-Description indicating
//			alternate names for this test.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "ExhValvePerformanceTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmFlowController.hh"
#include "SmUtilities.hh"
#include "SmManager.hh"
#include "SmConstants.hh"

#include "Task.hh"

#include "ValveRefs.hh"
#include "MainSensorRefs.hh"
#include "ExhalationValve.hh"
#include "PressureSensor.hh"

#if defined(SIGMA_DEVELOPMENT)
#include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
#endif

//@ End-Usage

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ExhValvePerformanceTest
//
//@ Interface - Description
// Constructor.  No arguments.  Initializes the array of test pressures.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Initialize test pressure array.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
ExhValvePerformanceTest::ExhValvePerformanceTest( void)
{
// $[TI1]
	CALL_TRACE("ExhValvePerformanceTest::ExhValvePerformanceTest( void)") ;

	Uint32 index = 0;
	evTestPressure_[index++] = 10.0;
	evTestPressure_[index++] = 45.0;
	evTestPressure_[index++] = 95.0;
	
	CLASS_ASSERTION( index == EV_PERF_TEST_NUM_TEST_POINTS);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~ExhValvePerformanceTest
//
//@ Interface - Description
//-----------------------------------------------------------------------
//@ Implementation - Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
ExhValvePerformanceTest::~ExhValvePerformanceTest( void)
{
	CALL_TRACE("ExhValvePerformanceTest::~ExhValvePerformanceTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTest
//
//@ Interface - Description
//	This method performs the exhalation valve calibration table test.
//	It has no arguments and has no return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	$[09071]
//	Check if exh valve calibration is required.  If so, generate a FAILURE
//	and exit.
//	Autozero the pressure sensors.
//	Establish an expiratory-controlled flow.
//	For each test pressure:
//		Adjust the test pressure level for zero flow.
//		Using the EV calibration table, command EV to the adjusted test pressure.
//		Correct the EV damping gain to the desired level.
//		Wait for flow to stabilize.  If flow doesn't stabilize within the
//		timeout, generate a FAILURE and exit loop.
//		Delay then read alpha-filtered expiratory pressure.
//		If exp pressure is out of range with respect to the adjusted
//		test pressure, generate a FAILURE.
//	Stop the flow.
//	Open the exhalation valve.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
void
ExhValvePerformanceTest::runTest( void)
{
	CALL_TRACE("ExhValvePerformanceTest::runTest( void)") ;

	if (ServiceMode::IsExhValveCalRequired())
	{	
	// $[TI1]
		// EV calibration table is invalid
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_2);
	}
	else
	{
	// $[TI2]
		const Real32 FLOW = 5.0;  // lpm
		const Int16 DAMPING_GAIN_COUNT = 205;  // = 0.5 V

		RSmUtilities.autozeroPressureXducers();

		RSmFlowController.establishFlow( FLOW, EXHALATION, AIR);
		Boolean calTableOk = TRUE;

	  	for ( Int32 index = 0;
	  		  index < EV_PERF_TEST_NUM_TEST_POINTS;
	  		  index++ )
	  	{
		// $[TI2.1]
			// Adjust pressure for 0 lpm per 72100-45 before looking up in the
			// EV cal table, since cal table is zero-flow-adjusted
			Real32 adjustedTestPressure =
				evTestPressure_[index] * EV_PRESSURE_CONVERSION_MULTIPLIER -
											EV_PRESSURE_CONVERSION_OFFSET;

			RExhalationValve.updateValve( adjustedTestPressure,
										ExhalationValve::NORMAL);
			
//TODO : E600 BDIO		const Int16 DAMPING_GAIN_COUNT = 205; //= 0.5 V	    	
// E600 BDIO			REVDampingGainDacPort.writeRegister(DAMPING_GAIN_COUNT);

			if (!RSmFlowController.waitForStableFlow())
			{
			// $[TI2.1.1]
				// Can't establish exh flow
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_3);

				break;
			}
			// $[TI2.1.2]
			// implied else

			// delay then read alpha-filtered pressure
			Task::Delay(0, 300);
			RSmManager.signalTestPoint();
			Real32 exhPressure =
						RExhPressureSensor.getFilteredValue();
			RSmManager.sendTestPointToServiceData(exhPressure);

			Real32 maxError = 
				evTestPressure_[index] * REL_PRESSURE_ERROR +
				ABS_PRESSURE_ERROR + EV_P5_REPEATABILITY;

			if ( ABS_VALUE( exhPressure -
						evTestPressure_[index] ) > maxError )
			{
			// $[TI2.1.3]
				// exp pressure error is out of range
				calTableOk = FALSE;
			}
			// $[TI2.1.4]
			// implied else

#ifdef SIGMA_DEBUG
			printf("%3.0f  %7.3f  %6.3f  %6.3f\n",
				evTestPressure_[index], exhPressure,
				ABS_VALUE(exhPressure - evTestPressure_[index]),
				maxError );
#endif  // SIGMA_DEBUG

		}
		// end for

		if (!calTableOk)
		{
		// $[TI2.2]
			// EV calibration table doesn't match with
			// EV performance
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_1);
		}
		// $[TI2.3]
		// implied else

	  	RSmFlowController.stopFlowController();
	  	RSmUtilities.openExhValve();
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ExhValvePerformanceTest::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("ExhValvePerformanceTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, EXHVALVEPERFORMANCETEST,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
