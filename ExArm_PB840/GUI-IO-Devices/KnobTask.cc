#include "stdafx.h"
#ifdef SIGMA_GUI_CPU
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: KnobTask.cc - Task that handles GUI rotary knob input
//---------------------------------------------------------------------
//@ Interface-Description
// Respond to knob events and pass the knob data to GUI-Applications
// subsystem.
//---------------------------------------------------------------------
//@ Module-Decomposition
//	KnobDrvTask
//	KnobDrvTaskInit
//	PostKnob
//	RouteKnobEvents
//	ResetKnob
//	GetknobDelta
//	SetKnobDelta
//---------------------------------------------------------------------
//@ Rationale
// Needed to acquire knob data in real time
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize GUI rotary knob hardware. Poll the knob register at interval
// NOMINAL_KNOB_WAIT_TIME (ms).  The value read is the number of knob
// clicks accumulated since the last read.  This value is processed to
// produce a velocity-based "knob magnification" factor.  The alogorithm
// limits the rate at which this factor is allowed to decrease, to ensure
// that magnification is not lost due to a change of knob direction.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-IO-Devices/vcssrc/KnobTask.ccv   25.0.4.0   19 Nov 2013 14:12:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  08-Jul-1998   DCS Number: 5058
//  Project:  Color
//  Description:
//		Increased deflection per click to decrease knob sensitivity.  
//		Smoothed acceleration weighting current sample less than average.
//		Decreased acceleration decay time to 100ms from 1sec. 
//		Changed to accumulate left-over knob deltas between polling 
//		periods.
//		Added B-TREE conditionals to inhibit knob acceleration and
//		use 4 pulses per "click" instead of 12 pulses. Maintains
//		compatibility with the current B-TREE hardware.
//
//  Revision: 005  By:  sah    Date:  02-Jul-1997   DCS Number: 1848
//  Project:  Sigma (R8027)
//  Description:
//       Added this task into the task-monitoring mechanism.
//
//  Revision: 004  By:  sah    Date:  27-Jun-1997   DCS Number: 2023
//  Project:  Sigma (R8027)
//  Description:
//       Changed name of 'Knob.inl' file to 'Knob.in'.
//
//  Revision: 003  By:  gdc    Date:  28-MAY-1997    DR Number: 2008
//       Project:  Sigma (R8027)
//       Description:
//             Removed #include "Ipic.hh - obsolete.
//
//  Revision: 002  By:  clw    Date:  09-Apr-1997    DR Number: 1899
//       Project:  Sigma (R8027)
//       Description:
//             GetKnobDelta now re-enables knob posting, so the application
//             no longer needs to call AckKnobEvent().
//
//  Revision: 001  By:  clw    Date:  28-Mar-1997    DR Number: <none>
//       Project:  Sigma (R8027)
//       Description:
//             Initial version to coding standards (Integration baseline).
//=====================================================================
#include "Sigma.hh"
#include "CriticalSection.hh"
#include "MsgQueue.hh"
#include "IpcIds.hh"
#include "MailBox.hh"
#include "UserAnnunciationMsg.hh"
#include "Sound.hh"
#include "GuiIo.hh"
#include "OsTimeStamp.hh"
#include "MemoryMap.hh"
#include <stdio.h>
#include "TaskMonitor.hh"
#include "AioDioDriver.h"


// define own absolute value macro since
//	#include <stdlib.h>
// for abs() prototype leads to compile error beginning 18-Oct-95.  Problem is
// that both <stdlib.h> and <types.h> define a C entry to malloc()
#define	ABS(x)	((x >= 0) ? (x) : (-x))


//@ Code...

static const Int32	NOMINAL_KNOB_WAIT_TIME	= 25;	// ms to wait between readings

#ifdef SIGMA_GUIPC_CPU
Int32	KnobDelta_ = 0;	            // Making it global for GUI HW Simulation access
#else
static	Int32	KnobDelta_ = 0;	            // SetKnobDelta() adds to it,
#endif


#ifdef SIGMA_GUIPC_CPU
Boolean KnobAcknowledged_ = TRUE;   // set FALSE upon sending notice, Making it global for GUI HW Simulation access
#else
static	Boolean KnobAcknowledged_ = TRUE;   // set FALSE upon sending notice
#endif

// Q to post to when knob event occurs
static	const	IpcId	DEFAULT_KNOB_Q = USER_ANNUNCIATION_Q;
static	IpcId	KnobQ;


static uint32_t  AverageKnobRate = 0;
 
enum AssertErrs
{
	ABS_VEL_LESS_THAN_ZERO,
	BAD_MSG_QUEUE_ID,
	KNOB_MAG_OVERFLOW
};


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  PostKnob
//
//@ Interface-Description
// Post a knob event message to the established MsgQueue
//---------------------------------------------------------------------
//@ Implementation-Description
// Form the standard UserAnnunciationMsg knob event message and post it
// to the MsgQueue most recently established by calling RouteKnobEvents
// NOTE: when this is called from SetKnobDelta, it is under protection of
// the KNOB_DELTA_MT mutex.
//---------------------------------------------------------------------
//@ PreCondition
// A notifiable knob event has occurred
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================
#if !defined(SIGMA_UNIT_TEST)
static
#endif
void PostKnob(void)
{
    if ( KnobAcknowledged_ )
	{			//$[TI1]
	    UserAnnunciationMsg knobMsg;			// appl message
	    MsgQueue	userInputQueue(KnobQ);		// appl queue

	    knobMsg.qWord = 0;
	    knobMsg.keyParts.eventType = UserAnnunciationMsg::INPUT_FROM_KNOB;
    
	    DBG1("PostKnob(): posting KNOB TURN to MQ %d\n", KnobQ);
	    userInputQueue.putMsg((Uint32)knobMsg.qWord);
    
        KnobAcknowledged_ = FALSE;
    }
							//$[TI2]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  RouteKnobEvents
//
//@ Interface-Description
// Until a subsequent call to this function, post knob event messages
// to the passed MsgQueue ID
//---------------------------------------------------------------------
//@ Implementation-Description
// Assert if the passed MsgQueue ID is not a valid ID or 0 (NULL_ID).
// The special NULL_ID, a default value for the argument, re-establishes
// the DEFAULT_KNOB_Q as the MsgQueue to post to.
// Also, reset the knob click accumulator used by KnobTask and enable
// posting by setting the KnobAcknowledged_ flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

void
RouteKnobEvents(const IpcId newQId)
{
	// trap bogus MsgQueue IDs
	FREE_ASSERTION((newQId >= NULL_ID  &&  newQId < MAX_QID),
			GUI_IO_DEVICES, BAD_MSG_QUEUE_ID);

	// special case of NULL_ID re-establishes the default queue.
	if(newQId == NULL_ID)
	{			//$[TI1]
		KnobQ = DEFAULT_KNOB_Q;
	}
	else
	{			//$[TI2]
		KnobQ = newQId;
	}
	KnobDelta_ = 0;
    KnobAcknowledged_ = TRUE;

	DBG2("RouteKnobEvents(): old=%d new=%d\n", oldQId, newQId);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  GetKnobDelta
//
//@ Interface-Description
// Safely returns the shared global rotary knob delta (difference)
// since the previous reading.  This is the "consumer" interface
// into the rotary knob value.
//---------------------------------------------------------------------
//@ Implementation-Description
// The global rotary knob delta is semaphore-protected since there
// are multiple writers (the producer updates it when the knob is
// turned [and an interrupt occurs], while the consumer reads it
// and sets it [back] to 0 to prevent an ever-accumulating delta).
// Now that the knob has been read, re-enable knob posting by
// setting KnobAcknowledged_ to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

Int16
GetKnobDelta(void)		// the consumer
{
    CriticalSection  mutex(KNOB_DELTA_MT);

    Int16 delta = KnobDelta_;
    KnobDelta_ = 0;

    KnobAcknowledged_ = TRUE;

	DBG1("GetKnobDelta(): Knob delta = %d\n", delta);
    return delta;
			//$[TI1]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  SetKnobDelta
//
//@ Interface-Description
// Safely adds the new rotary knob delta (difference) to any that is
// already accumulated.  If the consumer has read the previous delta,
// inform it via a Q message that there is another delta to read.
// This is the "producer" interface into the rotary knob value.
//---------------------------------------------------------------------
//@ Implementation-Description
// The global rotary knob delta is semaphore-protected since there
// are multiple writers (the producer updates it when the knob is
// turned, while the consumer reads it
// and sets it [back] to 0 to prevent an ever-accumulating delta).
// A zero rotary knob value is taken to mean that the consumer is
// current with the knob. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

void
SetKnobDelta(Int16 delta)	// the producer
{
	CriticalSection mutex(KNOB_DELTA_MT);

    // inform consumer of new knob delta
	PostKnob();

	KnobDelta_ += delta;

	// Don't allow knob clicks to accumulate outside +/- max Int16 range:
    //  Note that KnobDelta_ is type Int32.
    if ( KnobDelta_ >= MAX_INT16_VALUE )
    {			//$[TI1]
        KnobDelta_ = MAX_INT16_VALUE;
    }
    else if ( KnobDelta_ <= MIN_INT16_VALUE )
    {			//$[TI2]
        KnobDelta_ = MIN_INT16_VALUE;
    }			//$[TI3]

	DBG1("SetKnobDelta(): Knob delta = %d\n", delta);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  AckKnobEvent
//
//@ Interface-Description
//  The consumer acknowledges receiving the knob event message using this
//  function.  This function provides a gating mechanism for knob events.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The knob delta mutex is used to protect updating the 
//  KnobAcknowledged_ variable.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

void
AckKnobEvent(void)
{
    CriticalSection mutex(KNOB_DELTA_MT);

    KnobAcknowledged_ = TRUE;
    			//$[TI1]
}

//====================== P R O C E D U R E   D E S C R I P T I O N ====
//@ Procedure:  KnobDrvTaskInit
//
//@ Interface-Description
// This initialization function is called before application tasks are
// executed.  All Knob task initialization is done here
//---------------------------------------------------------------------
//@ Implementation-Description
// Reset the knob counter registers.
//---------------------------------------------------------------------
//@ PreCondition
// Application tasks have not started execution
//---------------------------------------------------------------------
//@ PostCondition
// Knob interrupt is setup and disabled
//
//@ End-Procedure
//=====================================================================

void
KnobDrvTaskInit(void)
{
	RouteKnobEvents(DEFAULT_KNOB_Q);
					//$[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function: KnobDrvTask
//
//@ Interface-Description
// This task's sole purpose is to poll for rotary knob activity and
// to post the knob delta to the registered queue. Knob activity 
// is defined to be any rotation of the GUI keyboard knob.
//---------------------------------------------------------------------
//@ Implementation-Description
// This task polls the knob registers every NOMINAL_KNOB_WAIT_TIME ms.
// Each time we poll and there is a change between the most recent two
// readings, we compute a delta between the previous reading and the current
// reading.  This delta can be positive (clockwise rotation) or negative
// (counterclockwise rotation).  All non-zero delta computations are passed,
// along with an indication of knob rotation activity, to a higher-level
// application task for application-specific processing.  This algorithm
// supports knob acceleration computations. The acceleration is based on a
// running average of velocity, with protection against a rapid loss of
// acceleration.  To ensure accurate velocity computation, the time interval
// between readings is measured using a OsTimeStamp object.  The max amount
// of knob magnification is limited to a factor of MAX_MAG_FACTOR.
//---------------------------------------------------------------------
//@ PreCondition
// KnobDrvTaskInit has already been called
//---------------------------------------------------------------------
//@ PostCondition
// Since this is a task, it never returns and should only die when the
// system is reset
//
//@ End-Free-Function
//=====================================================================

void
KnobDrvTask(void)
{
	Uint8 CountFromStart = 0;
	for (;;)	// never stop
	{
		//$[TI1] always enters here
		// report back to the task-monitoring mechanism...
		TaskMonitor::Report();

			// delay 100*NOMINAL_KNOB_WAIT_TIME before reading knob clicks
			if (++CountFromStart > 100)
			{
				CountFromStart = 100;

				uint8_t			WheelCount=0;
				uint8_t			WheelDirection=0;
				uint32_t		accFactor = 1000;
				bool WheelRtn=false;
				#if !defined(SIGMA_GUIPC_CPU)
					WheelRtn = AioDioDriver_t::GetAioDioDriverHandle().ReadKnob(WheelCount, WheelDirection);
				#endif

				if (WheelRtn)
				{
					uint32_t SampleRate0 = WheelCount * accFactor;  // rate is (40 * counts per second) for 25 ms sampling rate

					//  Average the instantaneous velocity into the moving average.
					//  This is a weighted average of the last sample plus the
					//  average of all previous samples.
					AverageKnobRate = (3 * AverageKnobRate + SampleRate0 ) / 4;

					// accelerationFactor = 10 * 1000 * (knobRate/1000 * knobRate/1000) scaled
					uint32_t AccelerationFactor = AverageKnobRate * (AverageKnobRate / 100);

					if (AccelerationFactor < accFactor)
					{
						AccelerationFactor = accFactor;
					}

					uint32_t AcceleratedKnobCount = (WheelCount * AccelerationFactor) / 1000;
					if (AcceleratedKnobCount > 0)
					{
						if (!WheelDirection)
						{
							AcceleratedKnobCount *= -1;
						}
						SetKnobDelta((Int16)AcceleratedKnobCount);

						// single sound per knob read
						Sound::KnobRotateSound();
					}
				}
			}

			//--------------------------------------
			// Wait for the nominal wait time:
			Task::Delay(0, NOMINAL_KNOB_WAIT_TIME);

	}
	// end of main task FOREVER loop

}
#endif // SIGMA_GUI_CPU
