#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathTrigger - This class is an abstract base class for all 
//      breath triggers the Breath Delivery system may need.
//---------------------------------------------------------------------
//@ Interface-Description
//    A BreathTrigger may be enabled or disabled.  When enabled, it notifies 
//    the active BreathPhaseScheduler and the BreathTriggerMediator when
//    the condition that it has been set up to detect has occured.
//---------------------------------------------------------------------
//@ Rationale
//    There are many types of conditions in the breath delivery system 
//    that may trigger a change in the active BreathPhase.  These
//    conditions are monitored by BreathTriggers.
//---------------------------------------------------------------------
//@ Implementation-Description
//     BreathTrigger has a boolean state data member that is used to determine 
//     if the trigger is active. There are methods for setting the state 
//     (enabling and disabling the BreathTrigger). The method for checking the 
//     condition that the trigger is set up to detect is a pure virtual 
//     function, and must be defined for each derived breath trigger class.
//     The method triggerAction is called whenever the state of the trigger 
//     is checked, to trigger any possible transition in the breath phase.
//     Any BreathTrigger that requires special initialization upon being
//     enabled must replace the virtual enable method with its own enable method. 
//---------------------------------------------------------------------
//@ Fault-Handling
//     n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    This is an abstract base class for many types of breath triggers.  
//    It can not be instanciated.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 005  By: gdc     Date:  17-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//       DCS6170 - NIV2
//       Modifications to support sending trigger id to GUI patient-data
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BreathTrigger.hh"

//@ Usage-Classes
#include "BreathPhaseScheduler.hh"
#include "BreathRecord.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathTrigger 
//
//@ Interface-Description
//	Constructor takes unique trigger id as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ passing the argument id to BreathTrigger's 
//      constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
BreathTrigger::BreathTrigger(const Trigger::TriggerId id)
: Trigger(id)
{
  CALL_TRACE("BreathTrigger(const TriggerId id)");
  // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathTrigger 
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
BreathTrigger::~BreathTrigger()
{
  CALL_TRACE("~BreathTrigger()");
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
BreathTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BREATHTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerAction_ 
//
//@ Interface-Description
//	This method takes trigCond of type const Boolean as an argument and calls
//	determineBreathPhase() if trigCond is true. This method has no return value.
//	This method is only invoked from Trigger::determineState.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If trigger condition passed in is true, a method in breath phase
//	scheduler is called to manage transition of a breath. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BreathTrigger::triggerAction_(const Boolean trigCond)
{
  CALL_TRACE("triggerAction_(const Boolean trigCond)");

  if (trigCond)
  {
    // $[TI1]
	BreathRecord::SetBreathTriggerId(triggerId_);
    (BreathPhaseScheduler::GetCurrentScheduler()).determineBreathPhase(*this);
  }
  // $[TI2]
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


