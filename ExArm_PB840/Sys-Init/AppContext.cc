#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: AppContext - Application Context.
//---------------------------------------------------------------------
//@ Interface-Description
//  The AppContext class receives and dispatches TaskControlMessages 
//  and TaskControlTimers.  Each AppContext represents a different 
//  application context with different callback functions to process
//  these messages.  An AppContext object contains a pointer
//  to an application callback function for each TaskControlMessage and 
//  TaskControlTimer message class.  An application creates an 
//  AppContext and registers each of its callback functions with that
//  AppContext.  When the AppContext receives a message through its 
//  dispatchEvent() method, it calls the TaskControlMessage or
//  TaskControlTimer dispatch() virtual function.  The derived 
//  TaskControlMessage or TaskControlTimer message receives 
//  the AppContext and sends itself back to the AppContext 
//  appropriately cast to be dispatched to the callback function
//  registered for this type of message.  An AppContext can be 
//  constructed with or without an embedded MsgQueue.  When constructed
//  with a MsgQueue, the AppContext can receive messages from the
//  queue in nextEvent().  The mainLoop() facilitates this message
//  processing further by calling nextEvent() and then dispatching
//  the message using dispatchEvent().  It repeats this event 
//  processing loop indefinitely.
//---------------------------------------------------------------------
//@ Rationale
//  This AppContext class contains TaskControlMessage and 
//  TaskControlTimer message processing facilities.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The AppContext contains pointers to application callback functions
//  for each type of TaskControlMessage or TaskControlTimer.  The 
//  application registers these callbacks using setCallback().  The 
//  queue event data processed by dispatchEvent() uses a TaskControl-
//  QueueMessage to extract a type field and an object address.  The
//  TaskControlQueueMsg is the object queued on a MsgQueue.  The 
//  TaskControlQueueMsg message type is either a TASK_CONTROL_MSG
//  or TASK_CONTROL_TIMER which correspond to the base classes 
//  TaskControlMessage and TaskControlTimer.  The TaskControlMessages
//  are contained in the BufferPool and are freed with the delete 
//  operator to the BufferPool after processing.  TaskControlTimers are 
//  statically allocated and are not freed or deleted.  dispatchEvent
//  sends this AppContext reference to the message's dispatch function
//  which in turn returns itself to the appropriate AppContext dispatch
//  function.  The AppContext dispatch function then forwards the 
//  typed message to the application callback function which was 
//  registered using setCallback(). 
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/AppContext.ccv   25.0.4.0   19 Nov 2013 14:33:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================
#include "AppContext.hh"
#include "IpcIds.hh"

//@ Usage-Classes
#include "MsgQueue.hh"
#include "TaskControlMessage.hh"
#include "TaskControlTimer.hh"
#include "TaskControlQueueMsg.hh"
#include "TaskMonitor.hh"
#include "TaskMonitorQueueMsg.hh"
#include <afx.h>
#include "MemoryHandle.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AppContext [default constructor]
//
//@ Interface-Description
//  Default constructor.  Assigns the specified message queue pointer
//  to the internal queue used by the nextEvent() and pending() 
//  functions.  If the queue pointer is NULL, nextEvent(), pending(),
//  and mainLoop() cannot be used.  In this case, the calling 
//  application dispatches the message directly through the 
//  dispatchEvent() interface.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AppContext::AppContext(MsgQueue* pMsgQueue) : pMsgQueue_(pMsgQueue)
{
    CALL_TRACE("AppContext()");
    pCommDownCallback_ = NULL;
    pCommUpCallback_ = NULL;
    pChangeStateCallback_ = NULL;
    pBdReadyCallback_ = NULL;
    pGuiReadyCallback_ = NULL;
    pSyncClockCallback_ = NULL;
    pStartupTimerCallback_ = NULL;
    pSyncClockTimerCallback_ = NULL;
    pTransitionTimerCallback_ = NULL;
    pTaskReadyCallback_ = NULL;
    pBdFailedCallback_ = NULL;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AppContext [destructor]
//
//@ Interface-Description
//  AppContext destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
AppContext::~AppContext()
{
    CALL_TRACE("~AppContext()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  nameOf
//
// Interface-Description
//  This method returns name of the object.
//---------------------------------------------------------------------
// Implementation-Description
//  This method returns a pointer to const character string.
//---------------------------------------------------------------------
// PreCondition
//   none
//---------------------------------------------------------------------
// PostCondition
//   none
// End-Method
//=====================================================================
 
const char*
AppContext::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "AppContext";
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nextEvent
//
//@ Interface-Description
//  The nextEvent method pends for a message (event) on this 
//  AppContext's MsgQueue.  It returns the dequeued message to the 
//  caller.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses MsgQueue::pendForMsg to wait for and retrieve the
//  first message on this AppContext's message queue.  It then returns
//  a copy of the queueEvent to the calling application.
//---------------------------------------------------------------------
//@ PreCondition
//  pMsgQueue_ != NULL
//---------------------------------------------------------------------
//@ PostCondItion
//  none
//@ End-Method
//=====================================================================

Int32 
AppContext::nextEvent(void)
{
    CALL_TRACE("nextEvent(Int32&)");
 
    CLASS_PRE_CONDITION(pMsgQueue_ != NULL);
    Int32  queueEvent;
    Int32 status = pMsgQueue_->pendForMsg(queueEvent);
    AUX_CLASS_ASSERTION(status == Ipc::OK, status);  // event was read
    return queueEvent; // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: pending
//
//@ Interface-Description
//  The pending method returns TRUE if a message is waiting on this
//  AppContext's message queue.  It will return FALSE if there is no
//  message waiting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determines if a message is waiting using MsgQueue::isMsgAvail().
//---------------------------------------------------------------------
//@ PreCondition
//  pMsgQueue_ != NULL
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
AppContext::pending(void)
{
    CALL_TRACE("pending(void)");
 
    CLASS_PRE_CONDITION(pMsgQueue_ != NULL);

	return pMsgQueue_->isMsgAvail(); // $[TI1] $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatchEvent
//
//@ Interface-Description
//  The dispatchEvent method dispatches the specified queueEvent to 
//  the appropriate TaskControlMessage or TaskControlTimer dispatch 
//  routine which in turn calls the appropriate AppContext dispatch
//  function.  It accepts a TASK_CONTROL_MSG or TASK_CONTROL_TIMER
//  queueEvent defined by TaskControlQueueMsg and TASK_MONITOR_MSG
//  as defined by TaskMonitorQueueMsg.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses the queueEvent parameter to construct a
//  TaskControlQueueMsg to provide access to the event type and
//  event data parts of the message.  This method processes only
//  TASK_CONTROL_MSG, TASK_CONTROL_TIMER and TASK_MONITOR_MSG  events.
//  Other event types cause an assertion.  A TASK_CONTROL_MSG event
//  contains a pointer to a TaskControlMessage object in the event
//  data.  This event data is cast to the TaskControlMessage base class
//  which messages its virtual dispatch method with a pointer to this
//  AppContext.  The derived TaskControlMessage processes the event
//  when the virtual method dispatch is called.  Similarly the
//  TASK_CONTROL_TIMER event data is cast to the TaskControlTimer base
//  class and dispatched.  The TASK_MONITOR_MSG invokes the
//  TaskMon::Report to return the event to the Task-Monitor as a "keep
//  alive" message.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void                 
AppContext::dispatchEvent(Int32 queueEvent)
{
    CALL_TRACE("dispatchEvent(Int32)");
 
    TaskControlQueueMsg  msg(queueEvent);
 
    switch ( msg.getEventType() )
    {
        case TaskControlQueueMsg::TASK_CONTROL_MSG: // $[TI1]
		{
			// Used MemoryHandle utility to get Memory pointer of message data.
			MemPtr mPtr = MemoryHandle::GetMemoryPtr((MemHandle)msg.getEventData()); 
			CLASS_ASSERTION( mPtr != NULL );		
			TaskControlMessage* pMessage = static_cast<TaskControlMessage*>(mPtr);

			pMessage->dispatch(*this);

			MemoryHandle::DeleteMemoryPtr((MemHandle)msg.getEventData());
			delete pMessage;
				
            break;
		}
        case TaskControlQueueMsg::TASK_CONTROL_TIMER: // $[TI2]
        {
			MemPtr mPtr = MemoryHandle::GetMemoryPtr((MemHandle)msg.getEventData());
			CLASS_ASSERTION(mPtr != NULL)

            TaskControlTimer* pMessage = static_cast<TaskControlTimer*>(mPtr);

            pMessage->dispatch(*this);
            // not deleted since timer messages are static
            break;
        }

        case TaskMonitorQueueMsg::TASK_MONITOR_MSG:  // $[TI3]
        {
            TaskMonitor::Report();
            break;
        }

        default:  // $[TI4]
        {
			AUX_CLASS_ASSERTION_FAILURE(msg.getEventType());     
            break;
        }
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: mainLoop
//
//@ Interface-Description
//  The mainLoop method facilitates message processing of 
//  TaskControlMessages and TaskControlTimers.  It dequeues messages
//  from this AppContext's message queue and dispatches them 
//  to the appropriate callback functions using a continuous loop
//  calling nextEvent() and dispatchEvent().  This method never returns.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method enters an infinite loop calling this AppContext's
//  nextEvent and dispatchEvent methods.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
AppContext::mainLoop(void)
{
    CALL_TRACE("mainLoop(void)");
#ifndef SIGMA_UNIT_TEST
    for (;;)   // $[TI1]
    {
        dispatchEvent(nextEvent());
    }
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(CommDownCallbackPtr)
//
//@ Interface-Description
//  Assigns the CommDownMessage callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
AppContext::setCallback(AppContext::CommDownCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(CommDownCallbackPtr)");
    pCommDownCallback_ = pCallback;
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(CommUpCallbackPtr)
//
//@ Interface-Description
//  Assigns the CommUpMessage callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
AppContext::setCallback(AppContext::CommUpCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(CommUpCallbackPtr)");
    pCommUpCallback_ = pCallback;
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(ChangeStateCallbackPtr)
//
//@ Interface-Description
//  Assigns the ChangeStateMessage callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::setCallback(AppContext::ChangeStateCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(ChangeStateCallbackPtr)");
    pChangeStateCallback_ = pCallback;
    // $[TI3]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(BdReadyMessageCallback)
//
//@ Interface-Description
//  Assigns the BdReadyMessage callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::setCallback(AppContext::BdReadyCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(BdReadyCallbackPtr)");
    pBdReadyCallback_ = pCallback;
    // $[TI4]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(GuiReadyMessageCallback)
//
//@ Interface-Description
//  Assigns the GuiReadyMessage callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::setCallback(AppContext::GuiReadyCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(GuiReadyCallbackPtr)");
    pGuiReadyCallback_ = pCallback;
    // $[TI5]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(SyncClockCallbackPtr)
//
//@ Interface-Description
//  Assigns the SyncClockMessage callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::setCallback(AppContext::SyncClockCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(SyncClockCallbackPtr)");
    pSyncClockCallback_ = pCallback;
    // $[TI6]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(StartupTimerCallbackPtr)
//
//@ Interface-Description
//  Assigns the StartupTimerMessage callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::setCallback(AppContext::StartupTimerCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(StartupTimerCallbackPtr)");
    pStartupTimerCallback_ = pCallback;
    // $[TI7]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(SyncClockTimerCallbackPtr)
//
//@ Interface-Description
//  Assigns the SyncClockTimer callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::setCallback(AppContext::SyncClockTimerCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(SyncClockTimerCallbackPtr)");
    pSyncClockTimerCallback_ = pCallback;
    // $[TI8]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(TransitionTimerCallbackPtr)
//
//@ Interface-Description
//  Assigns the TransitionTimer callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::setCallback(AppContext::TransitionTimerCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(TransitionTimerCallbackPtr)");
    pTransitionTimerCallback_ = pCallback;
    // $[TI9]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(TaskReadyCallbackPtr)
//
//@ Interface-Description
//  Assigns the TaskReady callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::setCallback(AppContext::TaskReadyCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(TaskReadyCallbackPtr)");
    pTaskReadyCallback_ = pCallback;
    // $[TI10]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(CommDownMessage)
//
//@ Interface-Description
//  Dispatches the CommDownMessage to this AppContext's CommDownMessage
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the CommDownMessage to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const CommDownMessage& rMessage) const
{
    CALL_TRACE("dispatch(const CommDownMessage&)");
    if (pCommDownCallback_ != NULL)
    {   // $[TI1]
        (*pCommDownCallback_)(rMessage);
    }
    // else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(CommUpMessage)
//
//@ Interface-Description
//  Dispatches the CommUpMessage to this AppContext's CommUpMessage
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the CommUpMessage to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const CommUpMessage& rMessage) const
{
    CALL_TRACE("dispatch(const CommUpMessage&)");
    if (pCommUpCallback_ != NULL) 
    {   // $[TI3]
        (*pCommUpCallback_)(rMessage);
    }
    // else $[TI4]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(ChangeStateMessage)
//
//@ Interface-Description
//  Dispatches the ChangeStateMessage to this AppContext's ChangeStateMessage
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the ChangeStateMessage to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const ChangeStateMessage& rMessage) const
{
    CALL_TRACE("dispatch(const ChangeStateMessage&)");
    if (pChangeStateCallback_ != NULL)
    {   // $[TI5]
        (*pChangeStateCallback_)(rMessage);
    }
    // else $[TI6]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(BdReadyMessage)
//
//@ Interface-Description
//  Dispatches the BdReadyMessage to this AppContext's BdReadyMessage
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the BdReadyMessage to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const BdReadyMessage& rMessage) const
{
    CALL_TRACE("dispatch(const BdReadyMessage&)");
    if (pBdReadyCallback_ != NULL) 
    {   // $[TI7]
        (*pBdReadyCallback_)(rMessage);
    }
    // else $[TI8]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(GuiReadyMessage)
//
//@ Interface-Description
//  Dispatches the GuiReadyMessage to this AppContext's GuiReadyMessage
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the GuiReadyMessage to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const GuiReadyMessage& rMessage) const
{
    CALL_TRACE("dispatch(const GuiReadyMessage&)");
    if (pGuiReadyCallback_ != NULL)
    {    // $[TI9]
        (*pGuiReadyCallback_)(rMessage);
    }
    // else $[TI10]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(SyncClockMessage)
//
//@ Interface-Description
//  Dispatches the SyncClockMessage to this AppContext's SyncClockMessage
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the SyncClockMessage to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const SyncClockMessage& rMessage) const
{
    CALL_TRACE("dispatch(const SyncClockMessage&)");
    if (pSyncClockCallback_ != NULL) 
    {   // $[TI11]
        (*pSyncClockCallback_)(rMessage);
    }
    // else $[TI12]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(StartupTimer)
//
//@ Interface-Description
//  Dispatches the StartupTimer to this AppContext's StartupTimer
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the StartupTimer to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const StartupTimer& rMessage) const
{
    CALL_TRACE("dispatch(const StartupTimer&)");
    if (pStartupTimerCallback_ != NULL)
    {    // $[TI13]
        (*pStartupTimerCallback_)(rMessage);
    }
    // else $[TI14]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(SyncClockTimer)
//
//@ Interface-Description
//  Dispatches the SyncClockTimer to this AppContext's SyncClockTimer
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the SyncClockTimer to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const SyncClockTimer& rMessage) const
{
    CALL_TRACE("dispatch(const SyncClockTimer&)");
    if (pSyncClockTimerCallback_ != NULL)
    {   // $[TI15]
        (*pSyncClockTimerCallback_)(rMessage);
    }
    // else $[TI16]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(TransitionTimer)
//
//@ Interface-Description
//  Dispatches the TransitionTimer to this AppContext's TransitionTimer
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the TransitionTimer to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const TransitionTimer& rMessage) const
{
    CALL_TRACE("dispatch(const TransitionTimer&)");
    if (pTransitionTimerCallback_ != NULL) 
    {   // $[TI17]
        (*pTransitionTimerCallback_)(rMessage);
    }
    // else $[TI18]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(TaskReadyMessage)
//
//@ Interface-Description
//  Dispatches the TaskReadyMessage to this AppContext's TaskReadyMessage
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the TaskReadyMessage to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const TaskReadyMessage& rMessage) const
{
    CALL_TRACE("dispatch(const TaskReadyMessage&)");
    if (pTaskReadyCallback_ != NULL) 
    {   // $[TI19]
        (*pTaskReadyCallback_)(rMessage);
    }
    // else $[TI20]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCallback(BdFailedCallbackPtr)
//
//@ Interface-Description
//  Assigns the BdFailed callback function for this AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Assigns the internal function pointer to the specified function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::setCallback(AppContext::BdFailedCallbackPtr pCallback)
{
    CALL_TRACE("setCallback(BdFailedCallbackPtr)");
    pBdFailedCallback_ = pCallback;
    // $[TI21]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch(BdFailedMessage)
//
//@ Interface-Description
//  Dispatches the BdFailedMessage to this AppContext's BdFailedMessage
//  callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the BdFailedMessage to the registered callback function.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
AppContext::dispatch(const BdFailedMessage& rMessage) const
{
    CALL_TRACE("dispatch(const BdFailedMessage&)");
    if (pBdFailedCallback_ != NULL) 
    {   // $[TI22]
        (*pBdFailedCallback_)(rMessage);
    }
    // else $[TI23]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
AppContext::SoftFault(const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const char*        pFileName,
                      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_APPCONTEXT, 
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

