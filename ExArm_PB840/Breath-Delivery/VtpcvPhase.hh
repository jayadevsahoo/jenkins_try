
#ifndef VtpcvPhase_HH
#define VtpcvPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VtpcvPhase - Implements volume targeted pressure control
//					   ventilation phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VtpcvPhase.hhv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  syw    Date:  03-Sep-2000    DR Number: 5755
//       Project:  VTPC
//       Description:
//             Initial version
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "SettingId.hh"
#include "PcvPhase.hh"

//@ End-Usage

class VtpcvPhase : public PcvPhase
{
  public:
    VtpcvPhase( const SettingId::SettingIdType tidalVolumeId,
    	      	const SettingId::SettingIdType inspTimeId,
    	      	const SettingId::SettingIdType flowAccelPercentId) ;
    virtual ~VtpcvPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;

  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;

  private:
    VtpcvPhase( void) ;						// not implemented...
    VtpcvPhase( const VtpcvPhase&) ;		// not implemented...
    void operator=( const VtpcvPhase&) ;	// not implemented...

	//@ Data-Member: tidalVolumeId_
	// tidal volume id
	SettingId::SettingIdType tidalVolumeId_ ;

} ;

#endif // VtpcvPhase_HH 
