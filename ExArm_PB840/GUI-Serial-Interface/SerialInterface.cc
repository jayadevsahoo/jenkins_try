#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== C L A S S   D E S C R I P T I O N ====
//@ Class: SerialInterface - Sigma GUI Serial Port Interface.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is the base class for the serial port driver
//  classes. It provides functions and data common to all the types of 
//  protocols. It also provides the entry point for the GUI-Serial-
//  Interface tasks and static methods for accessing the instantiated
//  protocol handlers.
//  
//  The class provides the interface to the GUI serial ports for the 
//  840 application subsystems. The SerialInterface::Task's are started
//  during system initialization. It task waits until the ventilator is 
//  transitioned to a normal (ventilating) state or service state to 
//  instantiate either the normal mode interfaces or the SmcpInterface for 
//  processing data to and from the serial port. The DciInterface 
//  provides the online Digital Communications Interface (DCI) whereas
//  the SmcpInterface provides the Service Mode Communications Protocol
//  interface. The PrinterInterface provides support to capture and print 
//  via the HP PCL printer language, the patient data, settings and waveforms.
//  External subsystems use the SerialInterface class static methods
//  to access the normal mode or SmcpInterface protocol handlers. 
//  The SerialInterface class provides read and write access to the serial
//  port protocol handlers as well as facilities to set the serial
//  port parameters such as baud rate. Once the protocol handler is
//  instantiated, the SerialInterface passes these requests along to
//  the instantiated handler. This mechanism provides a convenient and
//  safe interface for the application to the protocol handlers. The
//  details of the buffering mechanisms and protocols handling are hidden
//  from the external subsystems. In addition to the read and write
//  access to the serial port, the SerialInterface class provides
//  methods for enabling or disabling serial port loopback while in
//  Service-Mode.
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulate the Sigma serial port access.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/SerialInterface.ccv   25.0.4.0   19 Nov 2013 14:12:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 014    By: mnr   Date: 23-Feb-2010  SCR Number: 6556
//  Project: NEO
//  Description:
//  	Philips related updates.
//  
//  Revision: 013    By: gdc   Date: 13-Jan-2009  SCR Number: 5944
//  Project: 840S
//  Description:
//  	Modified to allow changes to service mode baud rate without
//  	requiring power cycle.
//
//  Revision: 012    By: erm    Date: 07-Oct-2008  SCR Number: 6441
//  Project: 840S
//  Description:
//   Added support for to send wavesform data out to serial port
// 
//  Revision: 011    By: gdc   Date: 07-Mar-2008  SCR Number: 6430
//  Project: TREND2
//  Description:
//  Created Start*Interface static methods called from the Task method
//  to reduce stack usage. Prior to this modification, stack space was
//  allocated to all three interfaces in each task. With this change
//  stack is allocated only when the Start*Interface is called. This 
//  allows tasks that don't instantiate the SmcpInterface to reduce
//  their stack size to only accomodate the much smalled DciInterface and
//  PrinterInterfaces.
//
//  Revision: 010   By: erm    Date:  02-Jun-2003   DR Number: 6058
//  Project: AVER
//  Description:
//   Added support for spaceLab monitors.
//
//  Revision: 009   By: erm    Date:  23-Jan-2002    DR Number: 5984
//  Project: GUIComms
//  Description:
//   Added External LoopBack functionality.
//
//  Revision: 008   By: hlg    Date:  01-Oct-2001    DR Number: 5966
//  Project: GUIComms
//  Description:
//	Added mapping to SRS print requirements.
//
//  Revision: 007   By: quf    Date:  17-Sep-2001    DR Number: 5943
//  Project: GUIComms
//  Description:
//	Removed comm device "None" setting.
//
//  Revision: 006    By: quf           Date: 13-Sep-2001  DR Number: 5493
//  Project: GUIComms
//  Description:
//	Print implementation changes:
//	- Removed "print configure" setting.
//	- Made only com1 selectable for printer, with com2 and com3 forced to DCI.
//  - Cleaned up the print cancel and print done mechanisms.
//
//  Revision: 005    By: gdc           Date: 20-Sep-2000  DR Number: 5769
//  Project: GuiComms
//  Description:
//      Added 38400 baud rate.
//
//  Revision: 004    By: hct           Date: 29-JUL-1999  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//  Revision: 003    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//  Revision: 002	By: Gary Cederquist  Date: 10-Oct-1997  DR Number: 2172
//	Project:  Sigma (R8027)
//	Description:
//		Added annotations for requirements 07047 and 07048.
//
//  Revision: 1.0	By: jdm	Date: 3/28X/96	DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SerialInterface.hh"
#include "Task.hh"

//@ Usage-Classes
#include "FaultHandler.hh"
#include "TaskControlAgent.hh"
#include "MsgQueue.hh"
#include "SmcpInterface.hh"
#include "DciInterface.hh"
#include "PrinterInterface.hh"
#include "SensorInterface.hh"
#include "SerialPort.hh"
#include "ComPortConfigValue.hh"
#include "AcceptedContextHandle.hh"
#include "SettingSubject.hh"
#include "OsUtil.hh"
#include "BatchSettingValues.hh"
#include "BaudRateValue.hh"
#include "DciDataBitsValue.hh"
#include "DciParityModeValue.hh"
#include "NovRamManager.hh"
#include "ScreenCapture.hh"
#include "VsetInterface.hh"
#include "MathUtilities.hh"
#include <string.h>
//@ End-Usage

//@ Code...

Boolean SerialInterface::IsPrintInProgress_ = FALSE;

static SerialInterface* PActivePrinter_ = NULL;

static SerialInterface* 
PSerialInterface_[SerialInterface::MAX_NUM_SERIAL_PORTS];

static const IpcId SMCP_QUEUE_ID_[SerialInterface::MAX_NUM_SERIAL_PORTS] = 
{
    SMCP_INTERFACE1_Q,
    SMCP_INTERFACE2_Q,
    SMCP_INTERFACE3_Q
};

struct PortSettingId
{
    SettingId::SettingIdType  comConfigId;
    SettingId::SettingIdType  baudRateId;
    SettingId::SettingIdType  dataBitsId;
    SettingId::SettingIdType  parityId;
};

static const PortSettingId PORT_SETTING_ID_[] =
{
    { 
        SettingId::COM1_CONFIG,
        SettingId::COM1_BAUD_RATE,
        SettingId::COM1_DATA_BITS,
        SettingId::COM1_PARITY_MODE
    },
    { 
        SettingId::COM2_CONFIG,
        SettingId::COM2_BAUD_RATE,
        SettingId::COM2_DATA_BITS,
        SettingId::COM2_PARITY_MODE
    },
    { 
        SettingId::COM3_CONFIG,
        SettingId::COM3_BAUD_RATE,
        SettingId::COM3_DATA_BITS,
        SettingId::COM3_PARITY_MODE
    },
};


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SerialInterface [constructor]
//
//@ Interface-Description
//  Base class constructor for the serial interface protocol handler.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SerialInterface::SerialInterface( SerialPort & rSerialPort, 
                                  SerialInterface::PortNum portNum ) 
:  isSerialPortActivated_( FALSE )
,rSerialPort_( rSerialPort )
,portNum_( portNum )
,comConfigId_( PORT_SETTING_ID_[portNum].comConfigId )
,baudRateId_( PORT_SETTING_ID_[portNum].baudRateId )
,dataBitsId_( PORT_SETTING_ID_[portNum].dataBitsId )
,parityId_( PORT_SETTING_ID_[portNum].parityId )
,areSettingsChanged_(FALSE)
,isPrintRequested_(FALSE)
,isCurrentPrinter_(FALSE)
,isPacketProtocolEnabled_(FALSE)
{
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SerialInterface [destructor]
//
//@ Interface-Description
//  Base class destructor for the serial interface protocol handler.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SerialInterface::~SerialInterface()
{
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCurrentPrinter_
//
//@ Interface-Description
//  Assigns this SerialInterface as the current print screen device.
//	Note that only serial port #1 is currently assignable as the print
//	screen device.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// $[GC01001] The Communication Setup subscreen shall allow ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void SerialInterface::setCurrentPrinter_(void)
{
    if ( SERIAL_PORT_1 == portNum_ )
    {
        // $[TI1]
        DiscreteValue com1DeviceSetting =
        AcceptedContextHandle::GetDiscreteValue(SettingId::COM1_CONFIG);

        isCurrentPrinter_ = (com1DeviceSetting == ComPortConfigValue::PRINTER_VALUE);

        if ( isCurrentPrinter_ )
        {
            // $[TI1.1]
            UserAnnunciationMsg msg;
            msg.alarmParts.eventType = UserAnnunciationMsg::PRINTER_ASSIGNMENT_EVENT;
            CLASS_ASSERTION( MsgQueue::PutMsg( USER_ANNUNCIATION_Q, 
                                               (Int32)msg.qWord ) == Ipc::OK );
        }
        // $[TI1.2]
    }
    else
    {
        // $[TI2]
        isCurrentPrinter_ = FALSE;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: attachToSubjects_
//
//@ Interface-Description
//  Setup the Settings-Validation observer/subject system so the serial ports 
//  can update their communications parameters (ie. baud rate) when a user 
//  makes an associated setting change.
//---------------------------------------------------------------------
//@ Implementation-Description
//  For the serial port specified by attribute portNum_, message the 
//  attachToSubject_ method in the Settings-Validation subsystem.  When the 
//  associated setting changes, method valueUpdate will be called so the 
//  communications parameters can be updated in the uart. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::attachToSubjects_(void)
{
    // $[TI1]
    attachToSubject_(comConfigId_, Notification::VALUE_CHANGED);
    attachToSubject_(baudRateId_, Notification::VALUE_CHANGED);
    attachToSubject_(dataBitsId_, Notification::VALUE_CHANGED);
    attachToSubject_(parityId_, Notification::VALUE_CHANGED);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: detachFromSubjects_
//
//@ Interface-Description
//  Cancel the observer/subject relationship for the serial port specified by
//  attribute portNum_.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calling the Settings-Validation method detachFromSubject_ cancels the
//  observer/subject relationship set up in the attachToSubject_ call.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::detachFromSubjects_(void)
{
    // $[TI1]
    detachFromSubject_(comConfigId_, Notification::VALUE_CHANGED);
    detachFromSubject_(baudRateId_, Notification::VALUE_CHANGED);
    detachFromSubject_(dataBitsId_, Notification::VALUE_CHANGED);
    detachFromSubject_(parityId_, Notification::VALUE_CHANGED);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//  This method is called as part of Settings' observer/subject
//  mechanism, when a setting that this class is "observing" changes.
//  The type of change (accepted vs. adjusted) and a pointer to the
//  changed subject are passed as parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method asserts areSettingsChanged_.  The associated
//	instance of SerialInterface child class is looping continuously
//	in method processRequests_ to process serial requests (DCI
//	reports, upload to printer).  As part of this
//	loop, attribute areSettingsChanged_ is checked.  If it is
//	set, method processRequests_ is exited and a new driver is
//	called (Printer or DCI) and serial communication
//	parameters are set to the currently accepted setting values.
//
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void SerialInterface::valueUpdate(const Notification::ChangeQualifier qualifierId,
                                  const SettingSubject* pSubject)
{
    CALL_TRACE("valueUpdate(qualifierId, pSubject)");

    if ( qualifierId == Notification::ACCEPTED )
    {                                             // $[TI1.1]
        areSettingsChanged_ = TRUE;
    }                                             // $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  Initializes the GUI-Serial-Interface subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::Initialize(void)
{
    //$[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: start
//
//@ Interface-Description
//  Set attribute isSerialPortActivated_ to TRUE
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::start(void)
{
    // $[TI1]
    isSerialPortActivated_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAuxSerialPort
//
//@ Interface-Description
//  Returns a SerialPort reference to the static instantiation of the
//  SerialPort class specified by parameter portNum, but only for serial
//	ports 2 and 3. On first entry, it instantiates the SerialPort and
//	returns its reference.  Subsequent calls only return the reference
//	to the static instantiation.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return a reference to the static instantiation of the serial port 
//  specified by parameter portNum.
//---------------------------------------------------------------------
//@ PreCondition
//  portNum is SERIAL_PORT_2 or SERIAL_PORT_3
//---------------------------------------------------------------------
//@ PostCondition
//  none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================
SerialPort & SerialInterface::GetAuxSerialPort(SerialInterface::PortNum portNum)
{
    static SerialPort SerialPort2_(SerialInterface::SERIAL_PORT_2);
    static SerialPort SerialPort3_(SerialInterface::SERIAL_PORT_3);

    SerialPort* pSerialPort = NULL;
    switch ( portNum )
    {
    case SerialInterface::SERIAL_PORT_2: // $[TI1.1]
        pSerialPort = &SerialPort2_;
        break;
    case SerialInterface::SERIAL_PORT_3: // $[TI1.2]
        pSerialPort = &SerialPort3_;
        break;
    default:
        CLASS_ASSERTION_FAILURE();      
    }
    return(*pSerialPort);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSerialPort
//
//@ Interface-Description
//  Returns a SerialPort reference to the static instantiation of the
//  SerialPort class specified by parameter portNum. On first 
//  entry, it instantiates the SerialPort and returns its reference. 
//  Subsequent calls only return the reference to the static instantiation.
//
//  Note that only the static instantiation of the SerialPort #1 object is
//	done directly in this method; for the SerialPort #2 and #3 objects this
//	is done via GetAuxSerialPort().
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return a reference to the static instantiation of the serial port 
//  specified by parameter portNum.
//---------------------------------------------------------------------
//@ PreCondition
//  portNum is SERIAL_PORT_1, SERIAL_PORT_2 or SERIAL_PORT_3
//---------------------------------------------------------------------
//@ PostCondition
//  none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================
SerialPort & SerialInterface::GetSerialPort(SerialInterface::PortNum portNum)
{
    static SerialPort SerialPort1_(SerialInterface::SERIAL_PORT_1);

    SerialPort* pSerialPort = NULL;
    switch ( portNum )
    {
    case SerialInterface::SERIAL_PORT_1: // $[TI1.1]
        pSerialPort = &SerialPort1_;
        break;
    case SerialInterface::SERIAL_PORT_2: // $[TI1.2]
        pSerialPort = &SerialInterface::GetAuxSerialPort(portNum);
        break;
    case SerialInterface::SERIAL_PORT_3: // $[TI1.3]
        pSerialPort = &SerialInterface::GetAuxSerialPort(portNum);
        break;
    default:
        CLASS_ASSERTION_FAILURE();      
    }
    return(*pSerialPort);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: driver_ [protected, virtual]
//
//@ Interface-Description
//  Entry point for starting the protocol handler on the serial port. 
//  Sets the serial port parameters (baud rate etc.), flushes any
//  waiting data in the serial receive buffer, then enters the request
//  processing loop contained in processRequests_().
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::driver_(void)
{
    areSettingsChanged_ = FALSE;
    attachToSubjects_();
    setSerialParameters_();
    purgeline();
    setCurrentPrinter_();
    processRequests_();
    detachFromSubjects_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartSmcpInterface_
//
//@ Interface-Description
//  Instantiates an SMCP protocol handler and assigns it to the 
//  specified port.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void SerialInterface::StartSmcpInterface_(SerialInterface::PortNum portNum)
{
    SerialPort & rSerialPort = GetSerialPort(portNum);
    SmcpInterface smcpInterface(rSerialPort, portNum, SMCP_QUEUE_ID_[portNum]);
    PSerialInterface_[portNum] = &smcpInterface;
    PSerialInterface_[portNum]->driver_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartPrinterInterface_
//
//@ Interface-Description
//  Instantiates a Printer protocol handler and assigns it to the 
//  specified port.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void SerialInterface::StartPrinterInterface_(SerialInterface::PortNum portNum)
{
    SerialPort & rSerialPort = GetSerialPort(portNum);
    PrinterInterface printerInterface(rSerialPort, portNum);
    PSerialInterface_[portNum] = &printerInterface;
    PSerialInterface_[portNum]->driver_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartDciInterface_
//
//@ Interface-Description
//  Instantiates a DCI protocol handler and assigns it to the 
//  specified port.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SerialInterface::StartDciInterface_(SerialInterface::PortNum portNum)
{
    SerialPort & rSerialPort = GetSerialPort(portNum);
    DciInterface dciInterface(rSerialPort, portNum);
    PSerialInterface_[portNum] = &dciInterface;
    PSerialInterface_[portNum]->driver_();
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartSensorInterface_
//
//@ Interface-Description
//  Instantiates an Sensor waveform protocol handler and assigns it to the 
//  specified port.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void SerialInterface::StartSensorInterface_(SerialInterface::PortNum portNum)
{
    SerialPort & rSerialPort = GetSerialPort(portNum);
    SensorInterface sensorInterface(rSerialPort, portNum);
    PSerialInterface_[portNum] = &sensorInterface;
    PSerialInterface_[portNum]->driver_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartVsetInterface_
//
//@ Interface-Description
//  Instantiates a VSET protocol handler and assigns it to the 
//  specified port.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void SerialInterface::StartVsetInterface_(SerialInterface::PortNum portNum)
{
    SerialPort & rSerialPort = GetSerialPort(portNum);
    VsetInterface vsetInterface(rSerialPort, portNum);
    PSerialInterface_[portNum] = &vsetInterface;
    PSerialInterface_[portNum]->driver_();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task
//
//@ Interface-Description
//  Called by the entry point for the GUI-Serial-Interface subsystem tasks.
//  Instantiates
//  the appropriate protocol handler based on vent operation mode and the
//  communications configuration setting value (Printer, DCI).
//  Passes control to the instantiated handler's driver. The 
//  SerialInterface::Task must run with supervisor privileges allowing
//  it to call the logio device functions directly.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SerialInterface::Task(SerialInterface::PortNum portNum)
{
    AUX_CLASS_ASSERTION(0 <= portNum 
                        && portNum < SerialInterface::MAX_NUM_SERIAL_PORTS, portNum);

    while ( TaskControlAgent::GetMyState() == STATE_START || 
            TaskControlAgent::GetMyState() == STATE_INIT )
    {                                                 // $[TI1.1]
        Task::Delay(0,10);
    }                                                 // $[TI1.2]

    SerialPort & rSerialPort = GetSerialPort(portNum);

    if ( TaskControlAgent::GetMyState() == STATE_SERVICE )
    {                                                 // $[TI2.1]
        for (;;)
        {
            StartSmcpInterface_(portNum);
        }
    }
    else
    {                                                 // $[TI2.2]
        for ( ;; )
        {
            switch ( AcceptedContextHandle::GetDiscreteValue( PORT_SETTING_ID_[portNum].comConfigId) )
            {
            case (ComPortConfigValue::PRINTER_VALUE): 
                {                                           // $[TI3.1]
                    StartPrinterInterface_(portNum);
                    break;
                }
            case (ComPortConfigValue::PHILIPS_VALUE):
            case (ComPortConfigValue::SPACELAB_VALUE):
            case (ComPortConfigValue::DCI_VALUE):    
                {                                           // $[TI3.2]
                    StartDciInterface_(portNum);
                    break;
                }
            case (ComPortConfigValue::VSET_VALUE):
                {
                    StartVsetInterface_(portNum);
                    break;
                }
            case (ComPortConfigValue::SENSOR_VALUE):
                {
                    StartSensorInterface_(portNum);
                    break;
                }
            default:
                AUX_CLASS_ASSERTION_FAILURE(AcceptedContextHandle::GetDiscreteValue(
                                                                                   PORT_SETTING_ID_[portNum].comConfigId));
            }
        }
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task1
//
//@ Interface-Description
//  Entry point for the GUI-Serial-Interface subsystem task 1.  Calls
//  method Task with the enumerator for serial port number 1.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SerialInterface::Task1(void)
{
    // $[TI1]

	// TODO E600 MS Take this out when this becomes available
	while ( 1 )
	{
		Task::Delay( 1 );
	}

    //SerialInterface::Task(SerialInterface::SERIAL_PORT_1);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task2
//
//@ Interface-Description
//  Entry point for the GUI-Serial-Interface subsystem task 2.  Calls
//  method Task with the enumerator for serial port number 2.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SerialInterface::Task2(void)
{
	// TODO E600 MS Take this out when this becomes available
	while ( 1 )
	{
		Task::Delay( 1 );
	}

	/*
    if ( IsGuiCommsConfig() )
    {                                                         // $[TI1.1]
        SerialInterface::Task(SerialInterface::SERIAL_PORT_2);
    }                                                         // $[TI1.2]

    for ( ;; )
    {
        Task::Delay(60);
    } 
	*/

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task3
//
//@ Interface-Description
//  Entry point for the GUI-Serial-Interface subsystem task 3.  Calls
//  method Task with the enumerator for serial port number 3.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SerialInterface::Task3(void)
{

	// TODO E600 MS Take this out when this becomes available
	while ( 1 )
	{
		Task::Delay( 1 );
	}

	/*
    if ( IsGuiCommsConfig() )
    {                                                 // $[TI1.1]
        SerialInterface::Task(SerialInterface::SERIAL_PORT_3);
    }                                                 // $[TI1.2]

    for ( ;; )
    {
        Task::Delay(60);
    }

	*/

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetupSerialPort
//
//@ Interface-Description
//  Inform serial interface protocol handler that there has been a 
//  change to the serial port's communication attributes. Called by 
//  Settings-Validation when a change occurs.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SigmaStatus SerialInterface::SetupSerialPort(void)
{
    //$[TI1]
    return(SUCCESS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSerialParameters_
//
//@ Interface-Description
//  Retrieves the DCI serial port settings from NOVRAM and sets the 
//  serial port accordingly.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls NovRamManager to retrieve the latest DCI serial port settings.
//  Translates these values to SerialPort values and calls 
//  SerialPort::setParameters() to initiate the change.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::setSerialParameters_(void)
{
    Uint                baudRate;
    Uint                dataBits;
    SerialPort::Parity  parity;

    BatchSettingValues settings;
    NovRamManager::GetAcceptedBatchSettings( settings );

    DiscreteValue baudSetting   = settings.getDiscreteValue( baudRateId_ );
    DiscreteValue bitsSetting   = settings.getDiscreteValue( dataBitsId_ );
    DiscreteValue paritySetting = settings.getDiscreteValue( parityId_ );

    switch ( baudSetting )
    {
    case BaudRateValue::BAUD_38400_VALUE:  // $[TI1.6]
        baudRate = 38400;
        break;
    case BaudRateValue::BAUD_19200_VALUE:  // $[TI1.1]
        baudRate = 19200;
        break;
    case BaudRateValue::BAUD_9600_VALUE:  // $[TI1.2]
        baudRate = 9600;
        break;
    case BaudRateValue::BAUD_4800_VALUE:  // $[TI1.3]
        baudRate = 4800;
        break;
    case BaudRateValue::BAUD_2400_VALUE:  // $[TI1.4]
        baudRate = 2400;
        break;
    case BaudRateValue::BAUD_1200_VALUE:  // $[TI1.5]
        baudRate = 1200;
        break;
    default:
        AUX_CLASS_ASSERTION_FAILURE( baudSetting );
    }

    switch ( bitsSetting )
    {
    case DciDataBitsValue::BITS_7_VALUE:  // $[TI2.1]
        dataBits = 7;
        break;
    case DciDataBitsValue::BITS_8_VALUE:  // $[TI2.2]
        dataBits = 8;
        break;
    default:
        AUX_CLASS_ASSERTION_FAILURE( bitsSetting );
    }

    switch ( paritySetting )
    {
    case DciParityModeValue::EVEN_PARITY_VALUE:  // $[TI3.1]
        parity = SerialPort::EVEN;
        break;
    case DciParityModeValue::ODD_PARITY_VALUE:  // $[TI3.2]
        parity = SerialPort::ODD;
        break;
    case DciParityModeValue::NO_PARITY_VALUE:  // $[TI3.3]
        parity = SerialPort::NONE;
        break;
    default:
        AUX_CLASS_ASSERTION_FAILURE( paritySetting );
    }

    const Uint32  stopBits = 1;

    rSerialPort_.setParameters( baudRate, dataBits, parity, stopBits );

    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Write
//
//@ Interface-Description
//  Invoke the protocol handler to write data from the callers buffer
//  to the serial interface. Transfers up to the specified number of
//  bytes or less depending on the protocol handler's buffering 
//  capability. Waits up to the specified timeout (in milliseconds) for
//  the transfer to occur.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Uint SerialInterface::Write( const void * pBuffer, Uint count, Uint milliseconds,
                             SerialInterface::PortNum portNum )
{
    SAFE_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL );

    // $[TI1]
    return PSerialInterface_[portNum]->write(pBuffer, count, milliseconds);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Read
//
//@ Interface-Description
//  Invoke the protocol handler to transfer data from the 
//  serial interface into the caller's buffer up to the specified 
//  number of bytes. Waits up to the specified timeout (in milliseconds)
//  for data to become available before returning. Returns the number
//  of bytes actually written to the caller's buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Uint SerialInterface::Read( void * pBuffer, Uint count, Uint milliseconds,
                            SerialInterface::PortNum portNum )
{
    AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

    // $[TI1]
    return PSerialInterface_[portNum]->read(pBuffer, count, milliseconds);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Start
//
//@ Interface-Description
//  Signal the protocol handler to start the interface. Initially, the
//  interface is disabled, this enables it.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::Start( void )
{
    for ( Uint32 portNum = SerialInterface::SERIAL_PORT_1; 
        portNum < SerialInterface::MAX_NUM_SERIAL_PORTS; portNum++ )
    {
        if ( PSerialInterface_[portNum] )
        {                                           // $[TI1.1]
            PSerialInterface_[portNum]->start();
        }                                           // $[TI1.2]
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EnableLoopback
//
//@ Interface-Description
//  Calls the SmcpInterface handler to enable loopback mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SerialInterface::EnableLoopback( SerialInterface::PortNum portNum )
{
    AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

    // $[TI1]
    PSerialInterface_[portNum]->enableLoopback();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisableLoopback
//
//@ Interface-Description
//  Calls the SmcpInterface handler to disable loopback mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SerialInterface::DisableLoopback( SerialInterface::PortNum portNum )
{
    AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

    // $[TI1]
    PSerialInterface_[portNum]->disableLoopback();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EnableExternalLoopback
//
//@ Interface-Description
//  Calls the SmcpInterface handler to enable external loopback mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SerialInterface::EnableExternalLoopback( SerialInterface::PortNum portNum )
{
    AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

    // $[TI1]
    PSerialInterface_[portNum]->enableExternalLoopback();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisableExternalLoopback
//
//@ Interface-Description
//  Calls the SmcpInterface handler to disable external loopback mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SerialInterface::DisableExternalLoopback( SerialInterface::PortNum portNum )
{
    AUX_CLASS_ASSERTION( PSerialInterface_[portNum] != NULL, portNum );

    // $[TI1]
    PSerialInterface_[portNum]->disableExternalLoopback();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPCurrentPrinter
//
//@ Interface-Description
//  Returns a pointer to the SerialInterface that is assigned as the
//  current print destination.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
SerialInterface * SerialInterface::GetPCurrentPrinter(void)
{
    SerialInterface * pCurrentPrinter = NULL;

    if ( PSerialInterface_[0]->isCurrentPrinter_ )
    {
        // $[TI1]
        pCurrentPrinter = PSerialInterface_[0];
    }
    // $[TI2]

    return pCurrentPrinter;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: write
//
//@ Interface-Description
//  Writes the specified number of bytes from the buffer to the 
//  serial port. Waits up to the the specified timeout (in milliseconds)
//  for the data to enter the transmitter before returning. Returns
//  the number of bytes written to the serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to implement the serial port write operation.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

static const Uint16 MAX_PACKET_SIZE = 1500;
static const Uint16 SOM_INDEX = 4;
static const Uint16 CHKSUM_LENGTH = 4;
static const unsigned char SYNC1_CHAR = 0xAA;
static const unsigned char SYNC2_CHAR = 0xAB;

Uint SerialInterface::write( const void* message, Uint msglen, Uint milliseconds )
{
    Uint byteCount = 0;

    if (isPacketProtocolEnabled_)
    {
        unsigned char packet[MAX_PACKET_SIZE];
        unsigned char& sync1 = packet[0];
        unsigned char& sync2 = packet[1];
        unsigned char& nbf1 = packet[2];
        unsigned char& nbf2 = packet[3];

        CLASS_ASSERTION(msglen+SOM_INDEX+CHKSUM_LENGTH <= MAX_PACKET_SIZE)

        memcpy(packet + SOM_INDEX, message, msglen);
        // add a null char for odd-length buffers for fletcher checksum
        if (msglen % 2)
        {
            *(packet + SOM_INDEX + (msglen++)) = '\0';
        }

        const Uint16 packetLen = msglen + CHKSUM_LENGTH + 4;
        const Uint16 nbf = msglen + CHKSUM_LENGTH;
        sync1 = SYNC1_CHAR;
        sync2 = SYNC2_CHAR;
        nbf1 = nbf >> 8;
        nbf2 = nbf & 0xFF;

        Uint32 chksum = Fletcher32((Uint16*)(packet), (packetLen - CHKSUM_LENGTH)/sizeof(Uint16));
        memcpy(packet+packetLen-sizeof(chksum), &chksum, sizeof(chksum));

        //CLASS_ASSERTION( isChecksumValid_(packet, packetLen) )

        byteCount = rSerialPort_.write( packet, packetLen, milliseconds );
        rSerialPort_.wait( milliseconds );
    }
    else
    {
        byteCount = rSerialPort_.write( message, msglen, milliseconds );
        rSerialPort_.wait( milliseconds );
    }

    // $[TI1]
    return byteCount;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: read
//
//@ Interface-Description
//  Reads the specified number of bytes in the buffer. Waits up to 
//  the specified timeout (in milliseconds) for the operation to 
//  complete before returning. Returns the number of bytes actually read.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the SerialPort class to implement the serial port read operation.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint SerialInterface::read( void * pBuffer, Uint count, Uint milliseconds )
{
    // $[TI1]
    return rSerialPort_.read( pBuffer, count, milliseconds );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableLoopback
//
//@ Interface-Description
//  This method is implemented in derived classes only. This method 
//  asserts when called for the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void SerialInterface::enableLoopback(void)
{
    AUX_CLASS_ASSERTION_FAILURE(portNum_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableLoopback
//
//@ Interface-Description
//  This method is implemented in derived classes only. This method 
//  asserts when called for the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void SerialInterface::disableLoopback(void)
{
    AUX_CLASS_ASSERTION_FAILURE(portNum_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExternalLoopback
//
//@ Interface-Description
//  This method is implemented in derived classes only. This method 
//  asserts when called for the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void SerialInterface::enableExternalLoopback(void)
{
    AUX_CLASS_ASSERTION_FAILURE(portNum_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableExternalLoopback
//
//@ Interface-Description
//  This method is implemented in derived classes only. This method 
//  asserts when called for the base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See interface description.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void SerialInterface::disableExternalLoopback(void)
{
    AUX_CLASS_ASSERTION_FAILURE(portNum_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartPrint
//
//@ Interface-Description
//  Initiate screen dump and print operation to serial interfaces.
//  Returns TRUE for failure to initiate a print operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean SerialInterface::StartPrint(void)
{
    Boolean failed = FALSE;

    if ( PActivePrinter_ )
    {
        // $[TI1]
        failed = TRUE;
    }
    else
    {
        // $[TI2]
        if ( PActivePrinter_ = GetPCurrentPrinter() )
        {
            // $[TI2.1]
            failed = PActivePrinter_->startPrint();
        }
        else
        {
            // $[TI2.2]
            failed = TRUE;
        }
    }
    return failed;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelPrint
//
//@ Interface-Description
//  Cancel the print operation on all serial interfaces.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::CancelPrint(void)
{
    if ( PActivePrinter_ )
    {
        // $[TI1]
        PActivePrinter_->cancelPrint();
        PActivePrinter_ = NULL;
    }
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsPrintRequested
//
//@ Interface-Description
//  Returns TRUE if a print request is active, otherwise FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean SerialInterface::IsPrintRequested(void)
{
// $[TI1]
    return PActivePrinter_ != NULL;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsPrinterConfigured
//
//@ Interface-Description
//  Returns TRUE if a print device is configured to handle the PRINT
//  operation from the Waveforms sub-screen.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean SerialInterface::IsPrinterConfigured(void)
{
// $[TI1]
    return GetPCurrentPrinter() != NULL;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsPrintInProgress
//
//@ Interface-Description
//  Returns TRUE if print is in progress in the serial task.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean SerialInterface::IsPrintInProgress(void)
{
    return IsPrintInProgress_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: purgeline
//
//@ Interface-Description
//    Flush the receive buffer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::purgeline(void)
{
    // $[TI1]
    rSerialPort_.flushReceiveBuffer();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startPrint
//
//@ Interface-Description
//  Initiate the print operation on this interface.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean SerialInterface::startPrint(void)
{
    // $[TI1]
    isPrintRequested_ = TRUE;

    // Send message to GuiApp to capture screen dump
    // The higher priority GuiApp runs and captures the screen prior
    // to the serial interface tasks transferring the data to their devices
    UserAnnunciationMsg msg;
    msg.alarmParts.eventType = UserAnnunciationMsg::CAPTURE_PATIENT_DATA_EVENT;
    CLASS_ASSERTION(MsgQueue::PutMsg(USER_ANNUNCIATION_Q, 
                                     (Int32)msg.qWord) == Ipc::OK);

    return FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: cancelPrint
//
//@ Interface-Description
//  Cancel the print operation on this interface.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::cancelPrint(void)
{
    // $[TI1]
    isPrintRequested_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: endPrint
//
//@ Interface-Description
//  Complete the print operation on this interface.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Clear the active printer static pointer and the print requested flag.
//  Inform GuiApp that the interface is done printing and allow changes
//  to the printer configuration setting.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SerialInterface::endPrint(void)
{
    // $[TI1]
    PActivePrinter_ = NULL;
    isPrintRequested_ = FALSE;

    UserAnnunciationMsg msg;
    msg.alarmParts.eventType = UserAnnunciationMsg::CAPTURE_DONE_EVENT;
    CLASS_ASSERTION(MsgQueue::PutMsg(USER_ANNUNCIATION_Q, 
                                     (Int32)msg.qWord) == Ipc::OK);

    // GuiApp calls CancelPrint upon receiving CAPTURE_DONE_EVENT
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isChecksumValid
//
//@ Interface-Description
//  Verifies checksum on specified packet.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
Boolean SerialInterface::isChecksumValid_(const unsigned char *buffer, Uint16 length) const
{
    return IsFletcherChecksumValid(buffer, length);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//  Implicitly gain execution control when a software assertion macro
//  detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
//  macros (among others) detect a fault.  We just call FaultHandler::SoftFault
//  to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void SerialInterface::SoftFault(  const SoftFaultID softFaultID
                                  , const Uint32      lineNumber
                                  , const char *      pFileName
                                  , const char *      pPredicate )
{
    FaultHandler::SoftFault(softFaultID, GUI_SERIAL_INTERFACE,
                            GUI_Serial_Interface::SERIAL_INTERFACE_CLASS, 
                            lineNumber, pFileName, pPredicate);
}

