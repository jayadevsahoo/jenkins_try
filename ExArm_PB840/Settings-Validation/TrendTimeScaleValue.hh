#ifndef TrendTimeScaleValue_HH
#define TrendTimeScaleValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TrendTimeScaleValue - Values of the Trend Time Scale Setting
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TrendTimeScaleValue.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc    Date:  11-Jan-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Initial version.
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct TrendTimeScaleValue
{
	//@ Type:  TrendTimeScaleValue
	// All of the possible values of the Trend Time Scale Setting
	// $[TR01142] -- Displayed Trend values shall represent 
	//               compressed trend data for a specified sample period...
	enum TrendTimeScaleValueId
	{
		TREND_TIME_SCALE_1HR,
		TREND_TIME_SCALE_2HR,
		TREND_TIME_SCALE_4HR,
		TREND_TIME_SCALE_8HR,
		TREND_TIME_SCALE_12HR,
		TREND_TIME_SCALE_24HR,
		TREND_TIME_SCALE_48HR,
		TREND_TIME_SCALE_72HR,
		TOTAL_TREND_TIME_SCALE_VALUES
	};
};


#endif // TrendTimeScaleValue_HH 
