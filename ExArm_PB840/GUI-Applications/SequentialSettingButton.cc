#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SequentialSettingButton - A setting button which displays a
// title (along with possible units text) and the value of a bounded setting in
// a variable precision numeric field.  Most setting buttons fall into this
// category.
//---------------------------------------------------------------------
//@ Interface-Description
// The Numeric Setting button is the most widely used setting button and is
// used to adjust floating point (bounded) settings.  The Numeric Setting
// button consists of a title which identifies the setting (usually a symbol
// along with the symbol for the units in which the setting is measured) and
// the floating point setting value itself, positioned in the lower half of the
// button.  Current setting values are displayed in normal text, adjusted
// values in italic text.
//
// Once created, the Numeric Setting Button normally operates without
// intervention except for needing an activate() call before being displayed.
// However, there are two special methods provided for the Safety Ventilation
// subscreen.  These are setValue() and setPrecision() which allow external
// control of the numeric display of this button.  This is needed because the
// Safety Ventilation subscreen needs to display setting buttons that have
// special values that are not taken straight from the Accepted or Adjusted
// contexts of Settings-Validation subsystem.  Also, the setItalic() member is
// provided for use by the derived TimingSettingButton class (which also uses
// setValue() and setPrecision()).
//
// The updateDisplay_() is called via SettingButton when the button must update
// its displayed setting value.
//---------------------------------------------------------------------
//@ Rationale
// Implements the functionality for adjusting a floating point setting in a
// single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The construction parameters set up practically everything for the Numeric
// Setting button: its position, size, color, title, the size and position of
// the numeric value within the button, the enum id of the setting which it
// displays, the subscreen which it may activate, a help message to display
// when it is pressed down and whether it is a main setting button or not.
//
// After construction, most of the operation of this setting button is handled
// by the inherited SettingButton class.  All SequentialSettingButton really needs
// to do is to react to changes of the setting which it controls and update the
// numeric value display accordingly.  When the setting changes the
// updateDisplay_() method will be called.  This method handles the updating of
// the numeric value.  Note that setValue(), setPrecision() and setItalic()
// provide a "back door" method for controlling the numeric value display.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SequentialSettingButton.ccv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  hhd	   Date:  24-May-1999    DR Number: 5369 
//    Project:  Sigma (R8027)
//    Description:
//		Removed references to Button::setButtonType() (empty) method.
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SequentialSettingButton.hh"
#include "Colors.hh"

//@ Usage-Classes
#include "SequentialValue.hh"
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SequentialSettingButton()  [Constructor]
//
//@ Interface-Description
// Creates a Numeric Setting Button. Passed the following arguments:
// >Von
//	xOrg			The X coordinate of the button.
//	yOrg			The Y coordinate of the button.
//	width			Width of the button.
//	height			Height of the button.
//	buttonType		Type of the button, one of Button::(LIGHT, LIGHT_NON_LIT,
//					DARK).
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					Button::(ROUND or SQUARE).
//	borderType		The border type (whether it has an external border or not).
//	numberSize		The size of the numeric field, one from
//					TextFont::(SIX, EIGHT, TEN, TWELVE, FOURTEEN, EIGHTEEN,
//					TWENTY_FOUR).
//	valueCenterX	The X coordinate around which the the numeric value field
//					will be centered within the button.
//	valueCenterY	The Y coordinate around which the the numeric value field
//					will be centered within the button.
//	titleText		The string displayed as the title of the button's numeric
//					value.
//	settingId		The setting whose value the button displays and modifies.
//	pFocusSubScreen	The subscreen which receives Accept key events from the
//					button, normally the subscreen which contains the button.
//	messageId		The help message displayed in the Message Area when this
//					button is pressed down.
//	isMainSetting	Flag which specifies whether this setting button is a
//					"Main" setting.  Main setting buttons have slightly
//					different functionality than normal setting buttons.
//>Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the passed-in parameters are passed straight on to the inherited
// SettingButton.  The main work done is creating and positioning the numeric
// value field which is used to display the setting.  We are passed the center
// point of where this field should be so we calculate the position and size of
// this field, assuming it takes up all the space in the bottom left part of
// the button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SequentialSettingButton::SequentialSettingButton(Int16 xOrg, Int16 yOrg,
					Int16 width, Int16 height, Button::ButtonType buttonType,
					Int16 bevelSize, Button::CornerType cornerType,
					Button::BorderType borderType,
					TextFont::Size numberSize,
					Int16 valueCenterX, Int16 valueCenterY,
					StringId titleText, SettingId::SettingIdType settingId,
					SubScreen *pFocusSubScreen, StringId messageId,
					Boolean isMainSetting) :
			SettingButton(xOrg, yOrg, width, height, buttonType, bevelSize,
							cornerType, borderType, titleText, settingId,
							pFocusSubScreen, messageId, isMainSetting),
			buttonValue_(numberSize, 0, CENTER)
							// Set number of digits to 1 to obtain the
							// character height. Width is overridden in 
							// to center the value appropriately.
{
	CALL_TRACE("SequentialSettingButton::SequentialSettingButton(Int16 xOrg, "
					"Int16 yOrg, Int16 width, Int16 height, "
					"ButtonType buttonType, Int16 bevelSize, "
					"CornerType cornerType, BorderType borderType, "
					"TextFont::NumberSize numberSize, "
					"Int16 valueCenterX, Int16 valueCenterY, "
					"StringId titleText, SettingId::SettingIdType settingId, "
					"SubScreen *pFocusSubScreen, StringId messageId, "
					"Boolean isMainSetting)");

	// Add and position numeric value field to button's label container
	addLabel(&buttonValue_);
	buttonValue_.setX(0);
	buttonValue_.setWidth(2 * valueCenterX);

	buttonValue_.setY( valueCenterY - buttonValue_.getHeight() / 2 );
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SequentialSettingButton  [Destructor]
//
//@ Interface-Description
// Destroys a SequentialSettingButton.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SequentialSettingButton::~SequentialSettingButton(void)
{
	CALL_TRACE("SequentialSettingButton::~SequentialSettingButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_ [Protected, virtual]
//
//@ Interface-Description
// Called when the display of this setting button needs to be updated with
// the latest setting value.  Passed a setting context id informing us the
// setting context from which we should retrieve the new setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Retrieve the latest setting value from the Settings-Validation subsystem
// and display it.
//---------------------------------------------------------------------
//@ PreCondition
// qualifierId must be a valid value.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SequentialSettingButton::updateDisplay_(Notification::ChangeQualifier qualifierId,
										const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");

	// Get the current value and precision from the setting and set
	// the button's numeric value accordingly
	SequentialValue settingValue;

	if (qualifierId == Notification::ACCEPTED)
	{													// $[TI3]
		settingValue = pSubject->getAcceptedValue();
	}
	else if (qualifierId == Notification::ADJUSTED)
	{													// $[TI4]
		settingValue = pSubject->getAdjustedValue();
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(qualifierId);
	}

	buttonValue_.setValue(settingValue);
	buttonValue_.setPrecision(ONES);

	// If it's an Adjusted setting check to see if it's changed and set
	// the value to italic if so.
	if (qualifierId == Notification::ADJUSTED  &&  pSubject->isChanged())
	{													// $[TI1]
		buttonValue_.setItalic(TRUE);
		buttonValue_.setHighlighted(TRUE);
	}
	else
	{													// $[TI2]
		buttonValue_.setItalic(FALSE);
		buttonValue_.setHighlighted(FALSE);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SequentialSettingButton::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							SEQUENTIALSETTINGBUTTON,
							lineNumber, pFileName, pPredicate);
}
