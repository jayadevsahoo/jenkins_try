#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VHMCurrBreathCountDur - Type "Criteria met for 2 breaths in 60 sec."
//---------------------------------------------------------------------
//@ Interface-Description
// VHMCurrBreathCountDur defines the method update, declared as pure virtual in
// base class ViolationHistoryManager.
//---------------------------------------------------------------------
//@ Rationale
// VHMCurrBreathCountDur tracks the number of breaths that violate the
// detection criteria for CountConstraint objects of type "Detection criteria
// is met for 2 breaths in the last 60 seconds".  For VHMCurrBreathCountDur
// the violation is detected during the breath.
//---------------------------------------------------------------------
//@ Implementation-Description
// Violations such as High Circuit Pressure must be detected during a breath,
// as soon as they occur.  The update method for VHMCurrBreathCountDur is
// called two times for every violation breath and one time for non-violation
// breaths.
//
// For violation breaths, update is called once when the violation is detected
// and once when the breath ends.  The reason behind this logic is that a timer
// must be started to delete the ViolationHistory object after the duration
// interval has elapsed, but the timer must be based on the end of the breath.
// For example, assume an alarm is high urgency if there are three or more
// violation breaths in the past 60 seconds.  The oldest breath is not deleted
// from the ViolationHistoryList when the START time of the breath is more than
// 60 seconds in the past but rather when the END time of the breath is more
// than 60 seconds in the past.  The 60 seconds can be thought of as a sliding
// window.  If any portion of a violation breath is still in the 60 second
// window, the breath is still counted as a violation.  When no part of the
// breath is in the 60 second window, the violation breath is deleted from the
// ViolationHistoryList.
//
// For non-violation breaths update is called at the end of the breath but does
// nothing.
//
// When the CountConstraint objects associated with the parent OperatingGroup
// objects of the child OperatingGroup associated with the
// VHMCurrBreathCountDur object are evaluated, they query the
// VHMCurrBreathCountDur object for the current number of violations.  This
// number is compared with the bounds in the CountConstraint objects and if it
// falls within the range, the OperatingGroup associated with the
// CountConstraint is active.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//   none
//---------------------------------------------------------------------
//@ Invariants
//   none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/VHMCurrBreathCountDur.ccv   25.0.4.0   19 Nov 2013 13:51:26   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "VHMCurrBreathCountDur.hh"

//@ Usage-Classes
#include "PatientDataMgr.hh"
#include "MsgTimer.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VHMCurrBreathCountDur
//
//@ Interface-Description
//   Constructor.
//
// >Von
//   pDurationFunction  Attribute pDurationFunction is a constant pointer to a 
//                      function returning an Int32.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      (pDurationFunction != NULL)
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

VHMCurrBreathCountDur::VHMCurrBreathCountDur(
           ViolationHistoryManager::DurationFunctionPtrType pDurationFunction):
           ViolationHistoryManager(pDurationFunction)
{
  CALL_TRACE("VHMCurrBreathCountDur(pDurationFunction)");

  CLASS_PRE_CONDITION(pDurationFunction != NULL);
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VHMCurrBreathCountDur
//
//@ Interface-Description
//   Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

VHMCurrBreathCountDur::~VHMCurrBreathCountDur(void)
{
  CALL_TRACE("~VHMCurrBreathCountDur(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
//   A VHMCurrBreathCountDur violation manager is messaged to update itself
//   during a violation breath, and at the end of every breath. This
//   object tracks violations detected during the breath for CountConstraint
//   objects modeling constraints of the form "Two breaths in the last 60 
//   seconds violated the user set limit"; therefore, there is a maximum 
//   number of violations to be kept track of.
//
//   When messaged, this method examines the state of the OperatingGroup 
//   associated with the ViolationHistoryManager, passed in as groupState.
//   If groupState is TRUE, update has been messaged due to the detection of
//   a violation.  The members_ list is updated by messaging
//   updateViolationList_ and a new ViolationHistory object is created and
//   appended to the members_ list with the ViolationHistory::eventDetected_
//   attribute set to TRUE.  If groupState is FALSE, update has been messaged
//   due to an "end of breath" event.  If there is a ViolationHistory object
//   on the members_ list whose eventDetected_ attribute is TRUE, this is the 
//   end of a violation breath.  The eventDetected_ attribute is cleared and a
//   timer is started to delete the ViolationHistory object at the end of the
//   constraint time.
//
// >Von
//   groupState  State of the OperatingGroup pointed to by pGroup.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   none
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
VHMCurrBreathCountDur::update(Boolean groupState)
{
  CALL_TRACE("update(groupState)");

  // If the state of the associated OperatingGroup (pointed to by attribute
  // pGroup_) is TRUE, the violation was just detected.
  if (groupState)  // $[TI1]
  {
    updateViolationList_();

    // Find an available ViolationHistory object and set it active and set 
    // attribute eventDetected_ to TRUE.
    findAvailableViolationHistory_()->setActive(TRUE);
  }
  // Otherwise, this cycle of execution is due to and end of breath event.
  else  // $[TI2]
  {
    ViolationHistory* tempPtr = findFirstViolationHistory_();

    // If there are any active ViolationHistory objects on the members_ list
    // and one of them has ordinalValue_ attribute == 1 and attribute 
    // eventDetected == TRUE, this is the end of a violation breath,
    // otherwise, this is the end of a normal breath.
    if ((tempPtr != NULL) &&  (tempPtr->getEventDetected() == TRUE))  // $[TI2.1]
    {
      // Clear the eventDetected_ attribute.
      tempPtr->clearEventDetected();

      // Start timer for this ViolationHistory object.
      tempPtr->setTimerMsec((*pDurationFunction_)());
    }  // $[TI2.2] Implied else
  }
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
VHMCurrBreathCountDur::SoftFault(const SoftFaultID  softFaultID,
                                 const Uint32       lineNumber,
                                 const char*        pFileName,
                                 const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::VHMCURRBREATHCOUNTDUR, lineNumber,
                          pFileName, pPredicate);
}

