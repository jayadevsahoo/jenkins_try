#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SettingMenuItems -  A LogTarget for setting menu items
//---------------------------------------------------------------------
//@ Interface-Description 
//  SettingMenuItems is a LogTarget that encapsulates provides for
//  retrieval of setting menu items by DiscreteSettingButton and
//  ScrollableMenu classes.
//---------------------------------------------------------------------
//@ Rational 
//  Combines the interface to access setting menu data for both
//  DiscreteSettingButton and ScrollableMenu classes.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The SettingMenuItems class is a base class for derived classes
//  containing setting menu items. Specifically, TrendSelectMenuItems
//  and TrendTimeScaleMenuItems are derived from SettingMenuItems. They
//  provide menu item data to the ScrollableMenu class via the
//  LogTarget::getLogEntryColumn method and menu data to the
//  DiscreteSettingButton class via the getValueInfo method. The two
//  interfaces are supported so neither the LogTarget and its clients
//  nor the DiscreteSettingButton and its clients must be modified for
//  yet another interface.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//  This class should not be instantiated directly.  This class has use
//	only when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SettingMenuItems.ccv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc    Date: 15-Jan-2007   SCR Number: 6237
//  Project:  TREND
//  Description:
//      Initial versison.
//
//=====================================================================

#include "SettingMenuItems.hh"
#include "Setting.hh"
#include "SettingsMgr.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SettingMenuItems()  [Constructor, Protected]
//
//@ Interface-Description
//  Constructor. Passed a pointer to the ValueInfo structure in the
//  derived class.
//---------------------------------------------------------------------
//@ Implementation-Description 
//	none
//---------------------------------------------------------------------
//@ PreCondition
//	The ValueInfo pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SettingMenuItems::SettingMenuItems(DiscreteSettingButton::ValueInfo* pMenuInfo)
: pMenuInfo_(pMenuInfo)
{
	CLASS_PRE_CONDITION(pMenuInfo != NULL);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SettingMenuItems()  [Destructor, Protected]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SettingMenuItems::~SettingMenuItems(void)
{
	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of a log entry.  Passed the following
// parameters:
// >Von
//	entryNumber			The index of the log entry
// 
//	columnNumber		The column number of the log entry
// 
//	rIsCheapText		Output parameter.  A reference to a flag which
//						determines whether the text content of the log
//                      entry is specified as cheap text or not.
// 
//	rString1			String id of the first string. Can be
//						set to NULL_STRING_ID to indicate an empty entry
//                      column.
// 
//	rString2			String id of the second string.
// 
//	rString3			String id of the third string.  Not used if
//                      rIsCheapText is FALSE.
// 
//	rString1Message		A help message that will be displayed in the
//						Message Area when rString1 is touched.  This only
//                      works when rIsCheapText is TRUE.
// 
// >Voff
// We retrieve information for the appropriate column, make sure it's
// formatted correctly and return it to ScrollableLog.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	The entryNumber must be in range of the array of menu items.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SettingMenuItems::getLogEntryColumn(Uint16 entryNumber, 
										 Uint16 columnNumber,
										 Boolean &rIsCheapText, 
										 StringId &rString1,
										 StringId &rString2, 
										 StringId &rString3,
										 StringId &rString1Message)
{
	AUX_CLASS_PRE_CONDITION(entryNumber < pMenuInfo_->numValues, entryNumber);

	rString1 = pMenuInfo_->arrValues[entryNumber];
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getValueInfo
//
//@ Interface-Description 
//  Returns a reference to the DiscreteSettingButton::ValueInfo
//  structure contained in the derived class to supply setting menu data
//  to the DiscreteSettingButton class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
DiscreteSettingButton::ValueInfo& SettingMenuItems::getValueInfo(void) const
{
	return *pMenuInfo_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void SettingMenuItems::SoftFault(const SoftFaultID  softFaultID,
								const Uint32       lineNumber,
								const char*        pFileName,
								const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SETTINGMENUITEMS,
							lineNumber, pFileName, pPredicate);
}
