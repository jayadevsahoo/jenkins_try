#ifndef LaptopEventId_HH
#define LaptopEventId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: LaptopEventId - A list of enumerated ids for Laptop events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LaptopEventId.hhv   25.0.4.0   19 Nov 2013 14:08:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: rhj       Date: 20-April-2006   DR Number: 6169
//  Project:  RESPM
//  Description:
//      Added enum value UPLOAD_ALARM_LOG in order to make available
//      Alarm logs accessible to remote PC
//
//  Revision: 005  By:  dosman    Date:  02-Mar-1999  DR Number: 5322
//    Project:  ATC
//    Description:
//	Implemented changes for ATC.
//	Added three new enum values in order to make available
//	software options string accessible to remote PC.
//	It would be more organized to put SOFTWARE_OPTIONS
//	and SOFTWARE_OPTIONS_COMMAND with other similar enum values 
// 	in the order.  However, backwards-compatibility is desired,
//	so new enum values must be placed at the end.
//
//  Revision: 004  By:  gdc    Date:  23-Sep-97    DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//      Added INCOMING_LT_COMMAND_PARAM_IDX in support of parameterized
//      test information from the service/manufacturing PC.
//
//  Revision: 003  By:  yyy    Date:  11-Sep-97    DR Number: 2289
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated DEMO datakey type handle.
//
//  Revision: 002  By:  yyy    Date:  10-Sep-97    DR Number: 1923
//    Project:  Sigma (R8027)
//    Description:
//      Changed the name of communications Diagnostic log to the system
//		information log.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

class LaptopEventId
{
public:
	//@ Type: LTEventId
	// This enum lists all of the EVENTs supported by the laptop
	// communication.
	enum LTEventId
	{
		NULL_EVENT_ID = -1,

		// Event generated.
		LAPTOP_CONNECTED=0,
		COMMUNICATION_GRANTED=1,
		MESSAGE_ACK=2,
		SOFTWARE_VERSION=3,
		UNIT_SERIAL_NUMBER=4,
		INVENTORY_INFO=5,
		UNIT_RUNTIME_INFO=6,
		UPLOAD_SYSTEM_LOG=7,
		UPLOAD_SYS_INFO_LOG=8,
		UPLOAD_EST_SST_LOG=9,
		TEST_EVENT=10,
		FUNCTION_EVENT=11,
		SOFTWARE_OPTIONS=12,
        UPLOAD_ALARM_LOG = 13,
		NUMBER_OF_EVENT=14			// UPDATE this field is NECESSARY,
									// whenever the enum is added or deleted.
	};

	//@ Type: IncomingCommandId
	// This enum lists all of the commands for each LTEventId to complete
	// the incoming laptop communication packet for TEST_EVENT.
	enum IncomingCommandId
	{
		NULL_INCOMING_COMMAND_ID=-1,

		START_TEST_COMMAND=0,
		USER_RESPONSE_COMMAND=1,
		NUMBER_OF_INCOMING_COMMAND=2		// UPDATE this field is NECESSARY,
											// whenever the enum is added or deleted.
	};

	//@ Type: OutgoingCommandId
	// This enum lists all of the commands for each LTEventId to complete
	// the outgoing laptop communication packet.
	enum OutgoingCommandId
	{
		NULL_OUTGOING_COMMAND_ID=-1,

		LAPTOP_CONNECTED_COMMAND=0,
		COMMUNICATION_GRANTED_COMMAND=1,
		MESSAGE_ACK_COMMAND=2,
		SOFTWARE_VERSION_COMMAND=3,
		UNIT_SERIAL_NUMBER_COMMAND=4,
		INVENTORY_INFO_COMMAND=5,
		UNIT_RUNTIME_INFO_COMMAND=6,
		
		// Commands used by UPLOAD_SYSTEM_LOG, UPLOAD_SYS_INFO_LOG,
		// and UPLOAD_EST_SST_LOG.
		//
		START_UPLOAD_COMMAND=7,
		DATA_UPLOAD_COMMAND=8,
		END_UPLOAD_COMMAND=9,
		
		// Commands used by TEST_EVENT
		//
		PROMPT_COMMAND=10,
		KEY_ALLOWED_COMMAND=11,
		STATUS_COMMAND=12,
		CONDITION_COMMAND=13,
		DATA_COMMAND=14,

		// Commands used when invalid event occured.
		//
		INVALID_MSG_COMMAND=15,

		// New commands
		SOFTWARE_OPTIONS_COMMAND=16,

		NUMBER_OF_OUTGOING_COMMAND=17	// UPDATE this field is NECESSARY,
										// whenever the enum is added or deleted.
	};
	
	//@ Type: IncomingLTPHeaderFields
	// This enum lists all of the incoming fields in the header of 
	// the laptop communication packet.
	enum IncomingLTPHeaderFields
	{
		INCOMING_NULL_FIELD_ID=-1,
		INCOMING_LT_MSG_NUMBER_IDX=0,
		INCOMING_LT_EVENT_IDX=1,
		INCOMING_LT_COMMAND_IDX=2,
		INCOMING_LT_COMMAND_VALUE_IDX=3,
		INCOMING_LT_COMMAND_PARAM_IDX=4,
		INCOMING_NUMBER_OF_HDR_FIELDS=5			// UPDATE this field is NECESSARY,
										// whenever the enum is added or deleted.
	};

	//@ Type: OutgoingLTPHeaderFields
	// This enum lists all of the Outgoing fields in the header of 
	// the laptop communication packet.
	enum OutgoingLTPHeaderFields
	{
		OUTGOING_NULL_FIELD_ID=-1,
		OUTGOING_MSG_NUMBER_IDX=0,
		OUTGOING_COMMAND_ID_IDX=1,

		// Generic data enum shared by all types of outgoing message.
		OUTGOING_START_VALUE_IDX=2,

		// Used for command related outgoing message
		OUTGOING_COMMAND_VALUE_IDX=OUTGOING_START_VALUE_IDX,

		// Used for log related outgoing message
		OUTGOING_LOG_DATA_IDX=OUTGOING_START_VALUE_IDX,

		// Used for data related outgoing message
		OUTGOING_DATA_POINT_IDX=OUTGOING_START_VALUE_IDX,
		OUTGOING_DATA_VALUE_IDX=OUTGOING_DATA_POINT_IDX+ 4,

		// Used for Run Time command related outgoing message
		OUTGOING_SW_REVISION_VALUE_IDX=OUTGOING_START_VALUE_IDX,

		// Used for Unit serial number command related outgoing message
		OUTGOING_UNIT_SERIAL_NUMBER_VALUE_IDX=OUTGOING_START_VALUE_IDX,

		// Used for software options command related outgoing message
		OUTGOING_SOFTWARE_OPTIONS_VALUE_IDX=OUTGOING_START_VALUE_IDX,

		// Used for Run Time command related outgoing message
		OUTGOING_COMPRESSOR_RUN_TIME_VALUE_IDX=OUTGOING_START_VALUE_IDX,
		OUTGOING_SYSTEM_RUN_TIME_VALUE_IDX=OUTGOING_COMPRESSOR_RUN_TIME_VALUE_IDX + 4,

		OUTGOING_NUMBER_OF_HDR_FIELDS			// UPDATE this field is NECESSARY,
												// whenever the enum is added or deleted.
	};
};

#endif // LaptopEventId_HH 
