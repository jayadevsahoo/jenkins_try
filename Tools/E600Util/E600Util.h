
// E600Util.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CE600UtilApp:
// See E600Util.cpp for the implementation of this class
//

class CE600UtilApp : public CWinAppEx
{
public:
	CE600UtilApp();

// Overrides
	public:
	virtual BOOL InitInstance();
	virtual int Run();
	virtual int ExitInstance();

	BOOL m_bWait;

// Implementation

	DECLARE_MESSAGE_MAP()
	afx_msg void OnFileOpenfakegui();
};

extern CE600UtilApp theApp;