/*****************************************************************************/
/*   Module Name:   MsgPopup.c                                          
/*   Purpose:       Contains procedures to handle generic popup dialogs.
/*					The following dialog box types are handled:
/*					- OK button only
/*					- OK and Cancel button
/*					- OK, Cancel and help button
/*					- OK and Help button
/*					- Data entry dialog with OK button
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Dynamically resize to accomodate       Dave Richardson   20 Nov, 2002
/*   variable length text
/*    
/*   $Revision::   1.2      $                                                
/*                                                                          
/*   $Log:   //pleasa1apo2/swqa/data/arc/840_CPVT_VTS/Program/testutil/MsgPopup.c-arc  $
 * 
 *    Rev 1.2   04 Dec 2002 15:36:36   Buchman
 * 840 VTS build done
 * 
 *    Rev 1.1   20 Nov 2002 14:45:46   Richardson
 * Updated per SCRs # 27

/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#ifdef DEMOMODE
#include "72361.h" // DEMO_TIMEOUT
#endif
#include "ldb.h"
#include "MsgPopup.h"
#include "rt_bmps.h"

/***********************************************/
/****************** DEFINE'S *******************/
/***********************************************/

#define MINIMUM_PANEL_WIDTH	200
#define STANDARD_HEIGHT  	120
#define WIDTH_TO_ADD		26
#define DIALOG_BGND_COLOR 	0XC0C0C0 // 192, 192, 192 0XB0B0B0 // 176, 176, 176
#define DIALOG_LIGHT_GRAY   0X505050 //  80,  80,  80
#define MESSAGE_X_POSITION	20
// NOTE: This BUTTON_X_POSITION works fine if there is not more than \n in the string.
// A better way to do it would be to count the \n's and adjust the button position accordingly.
#define BUTTON_X_POSITION	(MESSAGE_X_POSITION + 40)
#define MESSAGE_BOX_POINT_SIZE 14
#define CTRL_VERT_SPACING	10
#define CTRL_PADDING	10

// Variables:
static BOOL EnterWasPressed = FALSE;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:    MessagePopupPanelCB
/*                                                                           
/*                                                                           
/*****************************************************************************/

int CVICALLBACK MessagePopupPanelCB(int panel, int event, void *callbackData,
		int eventData1, int eventData2)
{
	switch(event){
		case EVENT_KEYPRESS:
			if(eventData1 == VAL_ENTER_VKEY){
				EnterWasPressed = TRUE;
			}
			break;
	}

	return 0;
}


/*****************************************************************************/
/* Name:    CPVTMessagePopup                                                     
/*                                                                           
/* Purpose: This function builds a dialog box with the look and feel required for
/*			the CPVT program. Bitmap buttons are created and the title and message are
/*			displayed.
/*                                          
/*                                                                           
/* Example Call:   CPVTMessagePopup("Title of message", "Message", OK_ONLY_DLG, NO_HELP);
/*                                                                           
/* Input:   Title: String to display in title area of dialog box
/*			Message: Message to display in message are of dialog box
/*			eDIALOGTYPE: Which type of dialog to display: OK or OK and Help
/*			helpID: Help topic to launch if eDIALOGTYPE == OK_HLP_DLG
/*                                                                           
/* Output:   <none>                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   error code as defined by output of CVI routines: MessagePopup                                                        
/*                                                                           
/*****************************************************************************/

int CPVTMessagePopup(char title[], char message[], eDIALOGTYPE dlgType, int helpID)
{
	int hPanel = 0;	 
	int error = 0;
	int tempPanelHandle;
	int tempCntrlHandle;
	int panelHeight;
   	int panelWidth;
   	int messageHeight;
   	int messageWidth;
   	int nAdjustHeight;
   	int nCtrlHeight;
   	int buttonWidth;
	int MESSAGE_LINE       = 0; // control identifier
	int DIALOG_OK_BUTTON   = 0; // control identifier
	int DIALOG_HELP_BUTTON = 0; // control identifier
	
	// Error check:
	if((title == NULL) ||
	   (message == NULL) ||
	   ((dlgType != OK_HLP_DLG) && (dlgType != OK_ONLY_DLG))  ||
	   ((dlgType == OK_HLP_DLG) && (helpID <=0)))
	{
		MessagePopup(LDBf(Ldb_programming_err), LDBf(Ldb_sw_fault_1));
		return -200;
	}
	// Create generic panel
	errChk(hPanel = NewPanel(0, title, 0, 0, STANDARD_HEIGHT, MINIMUM_PANEL_WIDTH));
	
	// General panel attributes
	errChk(SetPanelAttribute(hPanel, ATTR_BACKCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetPanelAttribute(hPanel, ATTR_SIZABLE,      FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_MOVABLE,      TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MINIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MAXIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_PARENT_SHARES_SHORTCUT_KEYS, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_ACTIVATE_WHEN_CLICKED_ON,    TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_FLOATING,                    VAL_FLOAT_ALWAYS));
	errChk(SetPanelAttribute(hPanel, ATTR_CALLBACK_FUNCTION_POINTER,   &MessagePopupPanelCB));
	// NOTE: Need to setup the callback so the system panel X works

	// Build Message:
   	errChk(MESSAGE_LINE = NewCtrl(hPanel, CTRL_TEXT_MSG, message, MESSAGE_X_POSITION, (WIDTH_TO_ADD/2)));
	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_CALLBACK_FUNCTION_POINTER, 0));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_SHORTCUT_KEY,    0));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_WIDTH,           strlen(message)));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_HEIGHT,          MESSAGE_BOX_POINT_SIZE +4));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_CTRL_MODE,       VAL_INDICATOR));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_TEXT_POINT_SIZE, MESSAGE_BOX_POINT_SIZE));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_SIZE_TO_TEXT,    TRUE));
   	
   	// Figure out how big message is:
	GetCtrlBoundingRect(hPanel, MESSAGE_LINE, 0, 0, &messageHeight, &messageWidth);
	
	// And size panel appropriately:
	panelWidth = messageWidth + WIDTH_TO_ADD;
	if(panelWidth < MINIMUM_PANEL_WIDTH){
		panelWidth = MINIMUM_PANEL_WIDTH;
	}
	if(panelWidth < ((strlen(title) * 7) + 20)){
		panelWidth = ((strlen(title) * 7) + 20);
	}
	nAdjustHeight = MESSAGE_X_POSITION + messageHeight + CTRL_VERT_SPACING - BUTTON_X_POSITION;
	SetPanelSize(hPanel, STANDARD_HEIGHT + nAdjustHeight, panelWidth);
	// Place the message in the middle
	SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_LEFT, (panelWidth/2)-(messageWidth/2));
	
	// Build OK bitmap button
	errChk(DIALOG_OK_BUTTON = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, BUTTON_X_POSITION + nAdjustHeight, 0));
	setButtonBMP(hPanel, DIALOG_OK_BUTTON, BMP_Okay192);	
	SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE);

	// Build Help bitmap button
	if(dlgType == OK_HLP_DLG){
		errChk(DIALOG_HELP_BUTTON = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, BUTTON_X_POSITION + nAdjustHeight, 0));
		setButtonBMP(hPanel, DIALOG_HELP_BUTTON, BMP_QMark192);	
		SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
		SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE);
	}

	// Place the buttons in the middle:
	GetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_WIDTH, &buttonWidth);
	if(dlgType == OK_ONLY_DLG){
		SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON,   ATTR_LEFT, (panelWidth/2)-(buttonWidth/2));
	}
	else if(dlgType == OK_HLP_DLG){
		SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON,   ATTR_LEFT, (  panelWidth/3)-(buttonWidth/2));
		SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON, ATTR_LEFT, (2*panelWidth/3)-(buttonWidth/2));
	}

   	// Set up TAB order if necessary
	if(dlgType == OK_HLP_DLG){
		errChk(SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON,   ATTR_CTRL_TAB_POSITION, 0));
   		errChk(SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON, ATTR_CTRL_TAB_POSITION, 1));
	}

	// Finalize panel colors, positioning
	errChk(SetPanelAttribute(hPanel, ATTR_CONFORM_TO_SYSTEM, /*TRUE*/ FALSE));
   	errChk(SetPanelAttribute(hPanel, ATTR_TOP,  VAL_AUTO_CENTER));
   	errChk(SetPanelAttribute(hPanel, ATTR_LEFT, VAL_AUTO_CENTER));

   	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL, DIALOG_OK_BUTTON));

	// And set active control to OK button;
	errChk(SetActiveCtrl(hPanel,DIALOG_OK_BUTTON));
	
	// Finally, Display the panel:
	errChk(InstallPopup(hPanel));
	do
	{
		GetUserEvent(0, &tempPanelHandle, &tempCntrlHandle);
		if(tempCntrlHandle == DIALOG_HELP_BUTTON){
			// Launch help here using: helpID as index into help file 
		}
		
	} while(tempCntrlHandle != DIALOG_OK_BUTTON && !EnterWasPressed);

	RemovePopup(0);
	DiscardPanel(hPanel);
	if(EnterWasPressed ){
		EnterWasPressed = FALSE;
		return 1;
	}
	return error;	

Error:
	if(hPanel){
		DiscardPanel(hPanel);
	}

	return error;
}


/*****************************************************************************/
/* Name:    CPVTConfirmPopup                                                     
/*                                                                           
/* Purpose: This function builds a confirm popup dialog box with the look and feel required for
/*			the CPVT program. Bitmap buttons are created and the title and message are
/*			displayed.
/*                                          
/*                                                                           
/* Example Call:   CPVTConfirmPopup("Title of message", "Message", OK_CANCEL_DLG, NO_HELP);
/*                                                                           
/* Input:   Title: String to display in title area of dialog box
/*			Message: Message to display in message are of dialog box
/*			eDIALOGTYPE: Which type of dialog to display: OK_CANCEL_DLG or OK_CANCEL_HLP_DLG
/*			helpID: Help topic to launch if eDIALOGTYPE == OK_HLP_DLG
/*                                                                           
/* Output:   <none>                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:	1 - User selected 'Yes'
/*			0 - User selected 'No'
/*			Negative number: Error occured
/*                                                                           
/*****************************************************************************/

int CPVTConfirmPopup(char title[], char message[], eDIALOGTYPE dlgType, int helpID)
{
	int hPanel = 0;	 
	int error  = 0;
	int tempPanelHandle;
	int tempCntrlHandle;
	int panelHeight;
   	int panelWidth;								   
   	int messageHeight;
   	int messageWidth;
   	int nAdjustHeight;
   	int nCtrlHeight;
   	int buttonWidth;
	int MESSAGE_LINE         = 0; // control identifier
	int DIALOG_OK_BUTTON     = 0; // control identifier
	int DIALOG_CANCEL_BUTTON = 0; // control identifier
	int DIALOG_HELP_BUTTON   = 0; // control identifier
	
	// Error check:
	if((title == NULL) ||
	   (message == NULL) ||
	   ((dlgType != OK_CANCEL_DLG) && (dlgType != OK_CANCEL_HLP_DLG))  ||
	   ((dlgType == OK_CANCEL_HLP_DLG) && (helpID <=0)))
	{
		MessagePopup(LDBf(Ldb_programming_err), LDBf(Ldb_sw_fault_2));
		return -201;
	}
	// Create generic panel
	errChk(hPanel = NewPanel(0, title, 0, 0, STANDARD_HEIGHT, MINIMUM_PANEL_WIDTH));
	
	// General panel attributes
	errChk(SetPanelAttribute(hPanel, ATTR_BACKCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetPanelAttribute(hPanel, ATTR_SIZABLE,      FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_MOVABLE,      TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MINIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MAXIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_PARENT_SHARES_SHORTCUT_KEYS, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_ACTIVATE_WHEN_CLICKED_ON,    TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_FLOATING,                    VAL_FLOAT_ALWAYS));
	errChk(SetPanelAttribute(hPanel, ATTR_CALLBACK_FUNCTION_POINTER, &MessagePopupPanelCB));
	// NOTE: Need to setup the callback so the system panel X works

	// Build Message:
   	errChk(MESSAGE_LINE = NewCtrl(hPanel, CTRL_TEXT_MSG, message, MESSAGE_X_POSITION, (WIDTH_TO_ADD/2)));
	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_CALLBACK_FUNCTION_POINTER, 0));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_SHORTCUT_KEY,    0));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_WIDTH,           strlen(message)));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_HEIGHT,          MESSAGE_BOX_POINT_SIZE +4));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_CTRL_MODE,       VAL_INDICATOR));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_TEXT_POINT_SIZE, MESSAGE_BOX_POINT_SIZE));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_SIZE_TO_TEXT,    TRUE));
   	
   	// Figure out how big message is:
	GetCtrlBoundingRect(hPanel, MESSAGE_LINE, 0, 0, &messageHeight, &messageWidth);
	
	// And size panel appropriately:
	panelWidth = messageWidth + WIDTH_TO_ADD;
	if(panelWidth < MINIMUM_PANEL_WIDTH){
		panelWidth = MINIMUM_PANEL_WIDTH;
	}
	if(panelWidth < ((strlen(title) * 7) + 20)){
		panelWidth = ((strlen(title) * 7) + 20);
	}
	nAdjustHeight = MESSAGE_X_POSITION + messageHeight + CTRL_VERT_SPACING - BUTTON_X_POSITION;
	SetPanelSize(hPanel, STANDARD_HEIGHT + nAdjustHeight, panelWidth);
	// Place the message in the middle
	SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_LEFT, (panelWidth/2)-(messageWidth/2));
	
	// Build OK bitmap button
	errChk(DIALOG_OK_BUTTON = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, BUTTON_X_POSITION + nAdjustHeight, 0));
	setButtonBMP(hPanel, DIALOG_OK_BUTTON, BMP_Okay192);	
	SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE);
	
	// Build Cancel bitmap button
	errChk(DIALOG_CANCEL_BUTTON = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, BUTTON_X_POSITION + nAdjustHeight, 0));
	setButtonBMP(hPanel, DIALOG_CANCEL_BUTTON, BMP_Cancel192);	
	SetCtrlAttribute(hPanel, DIALOG_CANCEL_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(hPanel, DIALOG_CANCEL_BUTTON, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE);

	// Build Help bitmap button
	if(dlgType == OK_CANCEL_HLP_DLG){
		errChk(DIALOG_HELP_BUTTON = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, BUTTON_X_POSITION + nAdjustHeight, 0));
		setButtonBMP(hPanel, DIALOG_HELP_BUTTON, BMP_QMark192);	
		SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
		SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE);
	}

	// Place the buttons in the middle:
	GetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_WIDTH, &buttonWidth);
	if(dlgType == OK_CANCEL_DLG){
		SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON,     ATTR_LEFT, (  panelWidth/3)-(buttonWidth/2));
		SetCtrlAttribute(hPanel, DIALOG_CANCEL_BUTTON, ATTR_LEFT, (2*panelWidth/3)-(buttonWidth/2));
	}
	else if(dlgType == OK_CANCEL_HLP_DLG){
		SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON,     ATTR_LEFT, (  panelWidth/4)-(buttonWidth/2));
		SetCtrlAttribute(hPanel, DIALOG_CANCEL_BUTTON, ATTR_LEFT, (2*panelWidth/4)-(buttonWidth/2));
		SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON,   ATTR_LEFT, (3*panelWidth/4)-(buttonWidth/2));
	}

   	// Set up TAB order
	errChk(SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON,     ATTR_CTRL_TAB_POSITION, 0));
   	errChk(SetCtrlAttribute(hPanel, DIALOG_CANCEL_BUTTON, ATTR_CTRL_TAB_POSITION, 1));
	if(dlgType == OK_CANCEL_HLP_DLG){
   		errChk(SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON, ATTR_CTRL_TAB_POSITION, 2));
   	}

	// Finalize panel colors, positioning
	errChk(SetPanelAttribute(hPanel, ATTR_CONFORM_TO_SYSTEM, /*TRUE*/ FALSE));
   	errChk(SetPanelAttribute(hPanel, ATTR_TOP,  VAL_AUTO_CENTER));
   	errChk(SetPanelAttribute(hPanel, ATTR_LEFT, VAL_AUTO_CENTER));

   	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL, DIALOG_CANCEL_BUTTON));

	// And set active control to OK button;
	errChk(SetActiveCtrl(hPanel, DIALOG_OK_BUTTON));
	
	// Finally, Display the panel:
	errChk(InstallPopup(hPanel));
	do
	{
		GetUserEvent(0, &tempPanelHandle, &tempCntrlHandle);
		if(tempCntrlHandle == DIALOG_HELP_BUTTON){
			// Launch help here using: helpID as index into help file 
		}
		
	} while((tempCntrlHandle != DIALOG_OK_BUTTON) && (tempCntrlHandle != DIALOG_CANCEL_BUTTON) && !EnterWasPressed);

	RemovePopup(0);
	DiscardPanel(hPanel);
	if(EnterWasPressed){
		EnterWasPressed = FALSE;
		return 1;
	}
	if(tempCntrlHandle == DIALOG_OK_BUTTON){
		return 1;
	}
	else{
		return 0;
	}

Error:
	if(hPanel){
		DiscardPanel(hPanel);
	}

	return error;
}


/*****************************************************************************/
/* Name:    CPVTPromptPopup                                                     
/*                                                                           
/* Purpose: This function builds a prompt popup dialog box with the look and feel required for
/*			the CPVT program. Bitmap buttons are created and the title and message are
/*			displayed.
/*                                          
/*                                                                           
/* Example Call:   CPVTPromptPopup("Title of message", "Message", OK_CANCEL_DLG, NO_HELP,
									buffer, 50);
/*                                                                           
/* Input:   Title: String to display in title area of dialog box
/*			Message: Message to display in message are of dialog box
/*			eDIALOGTYPE: Which type of dialog to display: OK_CANCEL_DLG or OK_CANCEL_HLP_DLG
/*			helpID: Help topic to launch if eDIALOGTYPE == OK_HLP_DLG
/*          responseBuffer: String to store response to
/*			maxResponseLen: Maximum response length (size of responseBuffer)
/* Output:   <none>                                                         
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:	1 - User selected 'OK'
/*			0 - User selected 'Cancel'
/*			Negative number: Error occured
/*                                                                           
/*****************************************************************************/

int CPVTPromptPopup(char title[], char message[], eDIALOGTYPE dlgType, int helpID, 
						char* responseBuffer, int maxResponseLen)
{
	int hPanel = 0;	 
	int error = 0;
	int tempPanelHandle;
	int tempCntrlHandle;
   	int panelHeight;
   	int panelWidth;	
   	int messageHeight;
   	int messageWidth;
   	int promptHeight;
	int buttonTop;
	int buttonHeight;
   	int buttonWidth;
	int promptLen;
	int MESSAGE_LINE         = 0; // control identifier
	int DIALOG_PROMPT_AREA   = 0; // control identifier
	int DIALOG_OK_BUTTON     = 0; // control identifier
	int DIALOG_CANCEL_BUTTON = 0; // control identifier
	int DIALOG_HELP_BUTTON   = 0; // control identifier
	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	double demoTimeOut;
	#endif
	#endif
	
	// Error check:
	if((title == NULL) ||
	   (message == NULL) ||
	   (responseBuffer == NULL) ||
	   ((dlgType != OK_CANCEL_DLG) && (dlgType != OK_CANCEL_HLP_DLG))  ||
	   ((dlgType == OK_CANCEL_HLP_DLG) && (helpID <=0)))
	{
		MessagePopup(LDBf(Ldb_programming_err), LDBf(Ldb_sw_fault_3));
		return -202;
	}
	// Create generic panel
	errChk(hPanel = NewPanel(0, title, 0, 0, STANDARD_HEIGHT, MINIMUM_PANEL_WIDTH));
	
	// General panel attributes
	errChk(SetPanelAttribute(hPanel, ATTR_CALLBACK_FUNCTION_POINTER, &MessagePopupPanelCB));
	errChk(SetPanelAttribute(hPanel, ATTR_BACKCOLOR,    DIALOG_BGND_COLOR));
	errChk(SetPanelAttribute(hPanel, ATTR_SIZABLE,      FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_MOVABLE,      TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MINIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MAXIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_PARENT_SHARES_SHORTCUT_KEYS, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_ACTIVATE_WHEN_CLICKED_ON,    TRUE));
	errChk(SetPanelAttribute(hPanel, ATTR_FLOATING,                    VAL_FLOAT_ALWAYS));
    errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_ITEM_VISIBLE,          FALSE));
	
	// Build Message:
   	errChk(MESSAGE_LINE = NewCtrl(hPanel, CTRL_TEXT_MSG, message, MESSAGE_X_POSITION, (WIDTH_TO_ADD/2)));
	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_CALLBACK_FUNCTION_POINTER, 0));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_SHORTCUT_KEY,    0));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_WIDTH,           strlen(message)));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_HEIGHT,          MESSAGE_BOX_POINT_SIZE +4));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_CTRL_MODE,       VAL_INDICATOR));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_TEXT_POINT_SIZE, MESSAGE_BOX_POINT_SIZE));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_TEXT_BGCOLOR,    DIALOG_BGND_COLOR));
   	errChk(SetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_SIZE_TO_TEXT,    TRUE));
   	
   	// Figure out how big message is:
	GetCtrlBoundingRect(hPanel, MESSAGE_LINE, 0, 0, &messageHeight, &messageWidth);
	
	// And size panel appropriately:
	panelWidth = messageWidth + WIDTH_TO_ADD;
	if(panelWidth < MINIMUM_PANEL_WIDTH){
		panelWidth = MINIMUM_PANEL_WIDTH;
	}
	if(panelWidth < ((strlen(title) * 7) + 20)){
		panelWidth = ((strlen(title) * 7) + 20);
	}
	SetPanelSize(hPanel, STANDARD_HEIGHT, panelWidth);
	
	// Build Prompt area
	GetCtrlAttribute(hPanel, MESSAGE_LINE, ATTR_HEIGHT, &messageHeight);
	errChk(DIALOG_PROMPT_AREA = NewCtrl(hPanel, CTRL_TEXT_BOX, 0,
	                                    MESSAGE_X_POSITION + messageHeight + CTRL_VERT_SPACING, 0));
	SetCtrlAttribute(hPanel, DIALOG_PROMPT_AREA, ATTR_VISIBLE_LINES, 1);
	SetCtrlAttribute(hPanel, DIALOG_PROMPT_AREA, ATTR_WIDTH,         (panelWidth - (16 * CTRL_PADDING)));
	SetCtrlAttribute(hPanel, DIALOG_PROMPT_AREA, ATTR_LEFT,          (8 * CTRL_PADDING));
	GetCtrlAttribute(hPanel, DIALOG_PROMPT_AREA, ATTR_HEIGHT,        &promptHeight);
	
	buttonTop = MESSAGE_X_POSITION + messageHeight + promptHeight + (CTRL_VERT_SPACING * 2);
	
	// Build OK bitmap button
	errChk(DIALOG_OK_BUTTON = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, buttonTop, 0));
	setButtonBMP(hPanel, DIALOG_OK_BUTTON, BMP_Okay192);	
	SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
	SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE);
	
	// Build Help bitmap button
	if(dlgType == OK_CANCEL_HLP_DLG){
		errChk(DIALOG_HELP_BUTTON = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, buttonTop, 0));
		setButtonBMP(hPanel, DIALOG_HELP_BUTTON, BMP_QMark192);	
		SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT);
		SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON, ATTR_FIT_MODE,         VAL_SIZE_TO_IMAGE);
	}

	// Place the buttons in the middle:
	GetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_WIDTH, &buttonWidth);
	if(dlgType == OK_CANCEL_DLG){
		SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON,     ATTR_LEFT, (  panelWidth/2)-(buttonWidth/2));
	}
	else if(dlgType == OK_CANCEL_HLP_DLG){
		SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON,     ATTR_LEFT, (  panelWidth/3)-(buttonWidth/2));
		SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON,   ATTR_LEFT, (2*panelWidth/3)-(buttonWidth/2));
	}

   	// Set up TAB order
	errChk(SetCtrlAttribute(hPanel, DIALOG_PROMPT_AREA, ATTR_CTRL_TAB_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, DIALOG_OK_BUTTON,   ATTR_CTRL_TAB_POSITION, 1));
	if(dlgType == OK_CANCEL_HLP_DLG){
   		errChk(SetCtrlAttribute(hPanel, DIALOG_HELP_BUTTON,   ATTR_CTRL_TAB_POSITION, 3));
   	}
	SetActiveCtrl(hPanel, DIALOG_PROMPT_AREA);

	// Finalize panel colors, positioning
	errChk(GetCtrlAttribute(hPanel, DIALOG_OK_BUTTON, ATTR_HEIGHT, &buttonHeight));
	panelHeight = buttonTop + buttonHeight + CTRL_VERT_SPACING;
	errChk(SetPanelAttribute(hPanel, ATTR_HEIGHT, panelHeight));
	
	errChk(SetPanelAttribute(hPanel, ATTR_CONFORM_TO_SYSTEM, /*TRUE*/ FALSE));
   	errChk(SetPanelAttribute(hPanel, ATTR_TOP,  VAL_AUTO_CENTER));
   	errChk(SetPanelAttribute(hPanel, ATTR_LEFT, VAL_AUTO_CENTER));

	// And set active control to OK button;
	errChk(SetActiveCtrl(hPanel,DIALOG_PROMPT_AREA));

	// Initialize the response
	promptLen = strlen(responseBuffer);
	if(promptLen <= maxResponseLen){
		ResetTextBox(hPanel, DIALOG_PROMPT_AREA, responseBuffer);
	}
	// Finally, Display the panel:
	errChk(InstallPopup(hPanel));
	#ifdef DEMOMODE
	#ifdef USE_DEMO_TIMEOUT
	demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
	#endif
	#endif
	do{
		GetUserEvent(0, &tempPanelHandle, &tempCntrlHandle);
		#ifdef DEMOMODE
		#ifdef USE_DEMO_TIMEOUT
		if(Timer() >= demoTimeOut){
			tempCntrlHandle = DIALOG_OK_BUTTON;
			Beep();
		}
		#endif
		#endif
		if(tempCntrlHandle == DIALOG_HELP_BUTTON){
			// Launch help here using: helpID as index into help file 
			#ifdef DEMOMODE
			#ifdef USE_DEMO_TIMEOUT
			demoTimeOut = Timer() + (double)DEMO_TIMEOUT;
			#endif
			#endif
		}
		
	} while((tempCntrlHandle != DIALOG_OK_BUTTON) && (tempCntrlHandle != DIALOG_CANCEL_BUTTON) && !EnterWasPressed);

	GetTextBoxLineLength(hPanel, DIALOG_PROMPT_AREA, 0, &promptLen);
	if(promptLen <= maxResponseLen){
		GetTextBoxLine(hPanel, DIALOG_PROMPT_AREA, 0, responseBuffer);
	}
	else{
		responseBuffer[0] = '\0';
	}
	
	RemovePopup(0);
	DiscardPanel(hPanel);
	if(EnterWasPressed){
		EnterWasPressed = FALSE;
		return 1;
	}
	if(tempCntrlHandle == DIALOG_OK_BUTTON){
		return 1;
	}
	else{
		return 0;
	}

Error:
	if(hPanel){
		DiscardPanel(hPanel);
	}

	return error;
}

