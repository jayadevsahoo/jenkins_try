#ifndef SerialLoopbackTestSubScreen_HH
#define SerialLoopbackTestSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2002, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SerialLoopbackTestSubScreen - Serial Loopback Test Subscreen
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SerialLoopbackTestSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:24   pvcs  $
//
//
//@ Modification-Log
//
//  Revision: 001  By:  quf    Date:  23-Jan-2002    DR Number: 5984
//  Project: GUIComms
//  Description:
//  Initial coding.
//====================================================================

#include "SubScreen.hh"
#include "AdjustPanelTarget.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "GuiTestManager.hh"
#include "ServiceStatusArea.hh"
#include "SmPromptId.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
class TestResult;
//@ End-Usage

class SerialLoopbackTestSubScreen : public SubScreen,
							public AdjustPanelTarget,
							public GuiTestManagerTarget 
{
public:
	SerialLoopbackTestSubScreen(SubScreenArea *pSubScreenArea);
	~SerialLoopbackTestSubScreen(void);

	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// ButtonTarget virtual method
	virtual void buttonDownHappened(Button *pButton,
												Boolean byOperatorAction);
	virtual void buttonUpHappened(Button *pButton,
												Boolean byOperatorAction);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelRestoreFocusHappened(void);

	// GuiTestMangerTarget virtual methods
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	enum TestScreenId
	{
		TEST_PROMPT_ACCEPTED_CLEARED,
		TEST_SETUP,
		TEST_START_ACCEPTED,
		MAX_TEST_SCREENS
	};

	enum TestPromptId
	{
		LOOPBACK_CONNECT_CABLE,
		MAX_TEST_PROMPTS
	};

	enum TestStateId
	{
		PROMPT_ACCEPT_BUTTON_PRESSED,
		START_BUTTON_PRESSED,
		UNDEFINED_TEST_STATE,
		MAX_TEST_STATES
	};

	// these methods are purposely declared, but not implemented...
	SerialLoopbackTestSubScreen(void);						// not implemented...
	SerialLoopbackTestSubScreen(const SerialLoopbackTestSubScreen&);	// not implemented...
	void operator=(const SerialLoopbackTestSubScreen&);		// not implemented...

	void layoutScreen_(TestScreenId testScreenId);

	void setPromptTable_(void);
	void startTest_(SmTestId::ServiceModeTestId currentTestId);
	void setupForRestart_(void);

	//@ Data-Member: testStatus_
	// A container for displaying the current test status.
	ServiceStatusArea testStatus_;

	//@ Data-Member: errorHappened_
	// Flag that indicates status of test
	Int	errorHappened_;

	//@ Data-Member: keyAllowedId_
	Int keyAllowedId_;

	//@ Data-Member: promptId_
	// The prompt id.
	SmPromptId::PromptId promptId_[MAX_TEST_PROMPTS];
	
	//@ Data-Member: promptName_
	// The prompts.
	StringId promptName_[MAX_TEST_PROMPTS];

	//@ Data-Member: startTestButton_
	// The StartTest button is used to start the testing.
	TextButton startTestButton_;
	
	//@ Data-Member: testStateId_
	// The test status identifier.
	TestStateId testStateId_;
	
	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: guiTestMgr_ 
	// Object instance of GuiTestManager
	GuiTestManager guiTestMgr_;
};

#endif // SerialLoopbackTestSubScreen_HH 
