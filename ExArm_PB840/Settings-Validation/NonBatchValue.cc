#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NonBatchValue - Union of the possible values for a non-batch setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a union of the three type of values that a non-batch setting
//  can contain -- bounded, discrete and sequential.  This type provides
//  a means of storing each of the three possible non-batch setting values
//  into a single location/variable.  Instances of this type are initialized
//  to a default value, and can be set and queried based on which of the
//  three values is represented by the instances.  There is no means by
//  which the instances of this union can guarantee that the client operates
//  on the value type that is stored within this instance (i.e., bounded,
//  discrete or sequential values); it is left as the responsibility of the
//  client to ensure that the value type of these instances are treated
//  consistently.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a generic non-batch storage type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This type is the union of a bounded value, a discrete value and a
//  sequential value.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/NonBatchValue.ccv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "NonBatchValue.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  NonBatchValue(void)  [Default Constructor]
//
//@ Interface-Description
//  Construct a default non-batch value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is method is needed out-of-line for array initialization.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NonBatchValue::NonBatchValue(void)
{
  CALL_TRACE("NonBatchValue()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~NonBatchValue()  [Destructor]
//
//@ Interface-Description
//  Destroy this non-batch value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is method is needed out-of-line for array initialization.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NonBatchValue::~NonBatchValue(void)
{
  CALL_TRACE("~NonBatchValue()");
}
