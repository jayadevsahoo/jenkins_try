#ifndef  BuildFlash_HH
#define	BuildFlash_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor/Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1996-1997, Nellcor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: BuildFlash.hh - Class definition of the building an 
//     absolute Flash memory image based on a user specified number 
//     of Motorola S record object files. The class recognizes
//     three target flash memory types, Sigma BD, Sigma GUI and Sigma
//     Boot prom. 
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/BuildFlash.hhv   25.0.4.0   19 Nov 2013 14:02:48   pvcs  $ 
//@ Modification-Log
//
//  Revision: 005   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 004  By: gdc    Date: 22-Feb-05    DR Number: 6148
//  Project:  NIV1
//  Description:
//       DCS 6148 - move to solaris
//       moved destructor to public scope as required by GNU compiler/linker
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <jhv>    Date:  27-Feb-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

#include "DownloadTypes.hh"
#include "DownloadError.hh"
#include "DownloadFault.hh"
#include "BootFile.hh"
#include "stdio.h"
#include "cktable.hh"
#include "Srecord.hh"
#include <malloc.h>

enum TargetType
{
	BD_FLASH = 0,
	GUI_FLASH = 1,
	BOOT_PROM = 2,
	NO_TARGET = 3,
	NUM_TYPES = NO_TARGET
};

enum OutputImageFormat
{
	BINARY_FORMAT,
	SRECORD_FORMAT,
	NO_FORMAT
};

struct NoChkSumRange
{
	Uint32 *pStart;
	Uint32 *pEnd;
};

// The following constants cannot be defined through enum since the MRI
// compiler doesn't allow an enum value bigger than 0x7fffffff.
#define MEMORY_FILL_PATTERN  0xFEFEFEFE

enum BuildFlashCons
{
	MAX_INPUT_FILES = 10,
	MAX_EXCLUSIONS = 5
};

struct TypeTable { char *name; TargetType type;};

class BuildFlash 
{
  public:
    BuildFlash(TargetType type, char **sRecordFiles,
			char *outFile, OutputImageFormat oForm,
				NoChkSumRange exAddr, Boolean perform);
    ~BuildFlash(void);
	SigmaStatus writeByte(Byte *flashAddr, Byte data);
	static void PrintErr(char *format, char *pArg); 
	static void PrintErr(char *format, Int arg); 
	static void PrintErr(char *pMsg); 
	static TargetType FindType(char *pStr);
    static void SoftFault(const FaultType softFaultID,
		const Uint32      lineNumber,
		const char*       pFileName  = NULL, 
		const char*       pPredicate = NULL);
	SigmaStatus task(void);
	static char *ProgName;

  protected:

  private:
    BuildFlash(const BuildFlash&);			// not implemented...
    void   operator=(const BuildFlash&);	// not implemented...
	SigmaStatus initMemory_(void);
	Byte *convertAddr_(Byte *flashAddr);
	inline void freeMemory_(void);
	void buildChkSumTbl_(void);
	void compBootpromChkSum_(void);
	SigmaStatus writeImageToFile_(void);
	SigmaStatus writeImageAsSrecord_(void);

	//@Data-Member: flashStartAddr_
	// Starting address of the target flash memory.
		Byte *flashStartAddr_ ;

	//@Data-Member: flashChkSumTblAddr_
	// The Post checksum table in the flash  memory.
		Byte *flashChkSumTblAddr_;

	//@Data-Member: flashEndAddr_
	// Ending address of the target flash memory.
		Byte  *flashEndAddr_ ;

	//@Data-Member: downloadSize_
	// size of the target flash memory where the executable is
	// is downloaded.
		Int downloadSize_ ;

	//@Data-Member: pImageMem_
	// size of the target flash memory in bytes.
		Byte	 *pImageMem_ ;

	//@Data-Member: flashEndImageAddr_;
	// Last flash memory address of a download executable image.
		Byte 	 *flashEndImageAddr_ ;

    //@ Data-Member: pFileNames_
    // a list of input S record file names.
		char **pFileNames_;

    //@ Data-Member:  inputFile__
    // the current Srecord object file being processed.
		BootFile inputFile_;

    //@ Data-Member:  outputFile_
    // the output file that the built flash memory image is written out to.
		BootFile outputFile_;

    //@ Data-Member:  pOutFileName_
    // the output file name
		char *pOutFileName_;

    //@ Data-Member:  sParser_
    // S record parser object
		Srecord sParser_;
	
    //@ Data-Member:  performance_
    // Indicator not to generate flash fill character records
	// to speed up the download.
		Boolean performance_;
	
	//@ Data-Member: PtypeTable_
	//  A pointer to a table of target type names and its type.
	static TypeTable *PtypeTable_;

	//@ Data-Member: exclude_
	//  A flash memory address range that do not require
	//  memory checksum checks by the POST.
		NoChkSumRange exclude_;

	//@Data-Member: chkSumTbl_
	// chkSumTbl_ contains the checksum values for all the 
	// memory blocks in a flash memory image.
		EepromStruct chkSumTbl_;
	
	//@Data-Member: OutputImageFormat_
	// format of the flash image output file.
		OutputImageFormat outputFormat_;

	//@Data-Member: type_
	// flash memory target type.
		 TargetType type_;
};

// Inlined methods...
#include "BuildFlash.in"

#endif // BuildFlash_HH 
