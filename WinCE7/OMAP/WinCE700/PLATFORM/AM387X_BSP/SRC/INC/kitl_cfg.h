//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this source code is subject to the terms of the Microsoft end-user
// license agreement (EULA) under which you licensed this SOFTWARE PRODUCT.
// If you did not accept the terms of the EULA, you are not authorized to use
// this source code. For a copy of the EULA, please see the LICENSE.RTF on your
// install media.
//
//------------------------------------------------------------------------------
//
//  File:  kitl_cfg.h
//
#ifndef __KITL_CFG_H
#define __KITL_CFG_H

#include "am387x_base_regs.h"

BOOL   Cpsw3gInit(UINT8 *pAddress, UINT32 offset, UINT16 mac[3]);
UINT16 Cpsw3gSendFrame(UINT8 *pBuffer, UINT32 length);
UINT16 Cpsw3gGetFrame(UINT8 *pBuffer, UINT16 *pLength);
VOID   Cpsw3gEnableInts();
VOID   Cpsw3gDisableInts();

VOID Cpsw3gCurrentPacketFilter(UINT32 filter);
BOOL Cpsw3gMulticastList(UINT8 *pAddresses, UINT32 count);


#define BSP_ETHDRV_AM387X   { \
    Cpsw3gInit, NULL, NULL, Cpsw3gSendFrame, Cpsw3gGetFrame, \
    Cpsw3gEnableInts, Cpsw3gDisableInts, \
    NULL, NULL,  Cpsw3gCurrentPacketFilter, Cpsw3gMulticastList  \
}

//------------------------------------------------------------------------------

OAL_KITL_ETH_DRIVER g_kitlEthLan_AM387X = BSP_ETHDRV_AM387X;

OAL_KITL_DEVICE g_kitlDevices[] = {
    { 
		L"Internal EMAC", Internal, AM387X_EMACSW_REGS_PA, 
        0, OAL_KITL_TYPE_ETH, &g_kitlEthLan_AM387X
    }, {
        NULL, 0, 0, 0, 0, NULL
    }
};    

#endif
