//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmStrs - Contains the Italian alarm message text and name enumerations.
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
// TextInfo_ contains all the text for the alarm annunciation messages
// specified in the Technical and Non-Technical Alarm Matrices in the Software
// Requirements Specification.  messageName contains the name enumerations used 
// to access the text.
//---------------------------------------------------------------------
//@ Implementation-Description
// TextInfo_ is an array of TextInfoStructs, each of which holds
// an Alarm Message Name and an Alarm text description.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// The order of alarm message name enumerations must match the order of alarm
// text messages.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ItalianAlarmStrs.LAv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 026  By: rhj     Date:  24-Jan-2011   SCR Number: 6738
//  Project:  PROX
//  Description:
//       Incorporated changes for PROX related strings.
//
//  Revision: 025  By: mnr     Date:  30-Nov-2009   SCR Number: 6526, 6543
//  Project:  S840BUILD3
//  Description:
//       Incorporated changes for 840S related strings.
//
//  Revision: 024  By: rhj     Date:  16-Aug-2005   DR Number: 6166
//  Project:  NIV2
//  Description:
//       Incorporated changes for NIV Phase 2.
//
//  Revision: 023  By: rhj     Date:  24-Mar-2005   DR Number: 6166
//  Project:  NIV
//  Description:
//       Incorporated changes for NIV Project.
//
//  Revision: 022  By: rhj     Date:  31-Aug-2004   DR Number: 5819
//  Project:  PAV
//  Description:
//       Incorporated changes for PAV Project.
//
//  Revision: 021   By: erm    Date: 20-Sep-2002    DR Number: 5818
//  Project:  VCP
//  Description:
//       Incorporated changes for VC+ Project from SME
//
//  Revision: 020   By: jja    Date: 09-Jul-2002    DR Number: 5818
//  Project:  VCP
//  Description:
//       Incorporated changes for VC+ Project
//
//  Revision: 019  By: sah     Date:  23-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//
//  Revision: 018  By: sah      Date: 25-May-2000    DCS Number: 5702
//  Project:  NeoMode
//  Description:
//      Project-related translations:
//      *  replaced 'HI Pcirc' with 'HI Ppeak'
//
//  Revision: 017  By:  btray	Date: 10-May-1999    DCS Number: 5374 
//  Project: ATC 
//  Description:
//	Use [Hi Pcirc] instead of PCIRC in COMPENSATION_LIMIT_LOW/MED/HIGH
//	_MSG.
//
//  Revision: 016  By:  btray	Date: 02-Feb -1998    DR Number: 5322
//  Project: ATC
//  Description:
//	*  Added new ATC strings. This are untranslated and marked as
//	   "TBT - BR" (02-Feb-1999).
//	*  Updated with changes based on SME's comments (28-Jun-1999).
//
//  Revision: 015  By:  btray	Date:  23-Nov-1998    DR Number: 
//  Project: BiLevel 
//  Description:
//	Restored HIGH_PRES_LOW_MSG, HIGH_PRES_MED_MSG and
//	HIGH_PRES_HIGH_MSG to their correct format. Somehow the
//	correct version of these strings were lost in the last revision.
//
//  Revision: 014  By:  btray	Date:  03-Nov-1998    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//	Round 1 of BiLevel Translations
//
//  Revision: 013  By:  sah    Date:  31-Jan-1998    DR Number: 5004
//  Project:  Sigma (R8027)
//  Description:
//      Incorporated new, shortened symbol names, and transferred from
//      this file's '.in' counterpart to this '.IN' file -- to allow
//      pre-compilation processing of symbol substitutions.
//
//--Break in Revision numbers--
//  
//   Revision 001   By:   hhd      Date: 08/19/97         DR Number:   2265
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version  
//
//   Revision 002   By:   hct    Date: 08/27/97         DR Number:  2322
//      Project:   Sigma   (R8027)
//      Description:
//         Add analysis message and remedy message for BTAT 8.
//  
//   Revision 003   By:   hhd	Date: 09/11/97         DR Number:  2265
//      Project:   Sigma   (R8027)
//      Description:
//			Final update after Internal SME's review.
//
//   Revision 004   By:   clw   Date: 09/12/97         DR Number:  2265
//      Project:   Sigma   (R8027)
//      Description:
//          Final final update after Internal SME's review. Removed some dollar signs from last 
//          word of some remedy messages.
//  
//   Revision 005   By:   hct    Date: 09/16/97         DR Number:  2503
//      Project:   Sigma   (R8027)
//      Description:
//         Changed messages for BTAT 6 (Touch screen blocked).
//  
//   Revision 006   By:   hct    Date: 09/18/97         DR Number:  2511
//      Project:   Sigma   (R8027)
//      Description:
//         Use symbol for O2 in change made for DCS 2322.
//  
//   Revision 007   By:   clw    Date: 09/25/97         DR Number:  2265
//      Project:   Sigma   (R8027)
//      Description:
//         Translated new Screen Blocked messages into Italian.
//
//   Revision 008   By:   hct    Date: 09/30/97         DR Number:  1475
//      Project:   Sigma   (R8027)
//      Description:
//         Add VOLUME LIMIT alarm.
//  
//   Revision 009   By:   hhd	Date: 12/08/97         DR Number: 2265 
//      Project:   Sigma   (R8027)
//      Description:
//		   Stripped out redundant hyphens (dollar signs) to improve performace.
//  
//   Revision 010   By:   clw	Date: 12/17/97         DR Number: none needed, not baselined
//      Project:   Sigma   (R8027)
//      Description:
//		   Inserted translations from Marina Cerrai, changed TBT to TDO (Translation DOne).
//  
//   Revision 011   By:   clw   Date: 01/12/98         DR Number: none needed, not baselined
//      Project:   Sigma   (R8027)
//      Description:
//         Based on SME Federico Capozzi's review of Marina's translation, made minor correction
//         to string VOLUME_LIMIT_MSG.  
//
//   Revision 012   By:   clw   Date: 01/16/98         DR Number: 2725
//      Project:   Sigma   (R8027)
//      Description:
//         Based on SME Federico Capozzi's review of Marina's translation, corrected 
//         string VOLUME_LIMIT_REMEDY_MSG. This was accidentally missed at the last update.
//
//=====================================================================

#include "AlarmStrs.hh"

//@ Usage-Classes

//@ End-Usage

//@ Code...

AlarmStrs::AlarmInfo  AlarmStrs::TextInfo_[] = {
   {  DEVICE_ALERT_MSG,  "AT$TEN$ZIONE VENTILATORE " },

   {  NO_VENTILATION_MSG,  "T:Ventilazione mancante. Valvola di sicurezza aperta." },
   {  NO_VENTILATION_SAFETY_VALVE_CLOSED_MSG,  "T:Ventilazione mancante." },

   {  PROVIDE_OTHER_VENTILATION_MSG,  "T:Fornire ventila$zione alternativa. Sostituire il ventilatore ed effettuare la manuten$zione." },

   {  VENTILATION_CONTINUES_MSG,  "T:La ventila$zio$ne con$ti$nua se$con$do le imposta$zio$ni." },
   {  VENTILATION_CONTINUES_BD_SPIRO_MSG,  "T:La ventilazione continua secondo le impostazioni.  L'erogazione della ventilaz. o la spirometria potreb$bero essere compromesse." },

   {  REPLACE_VENTILATOR_SERVICE_MSG,  "T:Sostituire il venti$latore ed effettuare la manutenzione." },

   {  CHECK_PATIENT_REPLACE_VENT_MSG,  "T:Controllare il paziente. Sostituire il ventilatore ed effettuare la ma$nutenzione." },

   {  SERVICE_REQUIRED_MSG,  "T:Effettuare la manutenzio$ne." },

   {  BD_NOT_AFFECTED_MSG,  "T:Erogazione della ventilaz. non interessata." },

   {  SCREEN_BLOCK_MSG,  "SCHERMO BLOCCATO" },
   {  SCREEN_BLOCK_ANALYSIS_MSG,  "T:Schermo bloccato o guasto." }, 
   {  SCREEN_BLOCK_REMEDY_MSG,  "T:Rimuovere impurit[`a]  o eseguire manu$tenzione." }, 

   {  NO_BD_STATUS_MSG,  "T:Imp. determinare lo stato dell'erogazione venti$lazione." },

   {  AC_LOSS_DEVICE_ALERT_MSG,  "T:Si sono verifi$ca$te una cadu$ta ed un ripri$sti$no dell'a$limen$ta$zio$ne con una con$di$zio$ne AT$TEN$ZIO$NE VEN$TI$LA$TO$RE gi�/ e$si$sten$te." },

   {  CHECK_ALARM_LOG_MSG,  "T:Con$trollare il log allarmi. Eseguire EST." },

   {  VENTILATION_WITH_100_O2_MSG,  "T:La ventilazione continua secondo le impostazioni. Solo [O2] disponibi$le." },
   {  VENTILATION_EXCEPT_100_O2_MSG,  "T:La ventilazione continua secondo le impostazioni, fatta eccezione per [O2] = 100%." },
   {  VENTILATION_WITH_21_O2_MSG,  "T:La ventilazione continua secondo le impostazioni. Solo Aria disponi$bile." },
   {  VENTILATION_EXCEPT_21_O2_MSG,  "T:La ventilazione continua secondo le impostazioni, fatta eccezione per [O2] = 21%." },

   {  COMPROMISED_AIR_DELIVERY_MSG,  "T:Erogazione Aria compromessa." },
   {  COMPROMISED_O2_DELIVERY_MSG,  "T:Erogazione [O2] compromessa." },

   {  CHECK_PATIENT_MSG,  "T:Controllare il paziente." },

   {  VENTILATION_UNAFFECTED_MSG,  "T:Erogazione ventilaz. non interessata." },

   {  O2_SENSOR_MSG,  "SENSORE [O2]" },
   {  O2_SENSOR_OOR_MSG,  "T:Sensore [O2] non calibrato/guasto. Calibrare, sostituire o {I:disabilitare}." },

   {  BD_OK_BAD_SPIRO_MSG,  "T:Erogazione ventilaz. non interessata. Spirometria compromessa." },
   {  BD_OK_BAD_SPIRO_PRESS_TRIG_MSG,  "T:Erogazione ventilaz. non interessata. Spirometria compromessa. Trigger = pressione." },
   {  BD_NOT_AFFECTED_OTHER_FUNCTIONS_MSG,  "T:Erogazione ventilaz non interessata. � possibile che risultino com$promesse altre funzioni." },

   {  APNEA_MSG,  "AP$NEA" },
   {  CHECK_PATIENT_SETTINGS_MSG,  "T:Con$trollare il pazien$te e le impostazioni." },
   {  APNEA_SHORT_DUR_ONE_EVENT_MSG,  "T:Ventilazione di apnea. Intervallo respiri > intervallo di apnea." },
   {  APNEA_LONG_DUR_TWO_EVENTS_MSG,  "T:Durata di apnea estesa {I:o} eventi di apnea multipli." },

   {  HIGH_MINUTE_VOL_MSG,  "[HI Vdot e tot]" }, // $[05042] },
   {  HIGH_MINUTE_VOL_LOW_MSG,  "T:[Vdot e tot] [>=] limi$te impo$sta$to per [<=] 30 sec." },
   {  HIGH_MINUTE_VOL_MED_MSG,  "T:[Vdot e tot] [>=] li$mi$te im$po$sta$to per > 30 sec." },
   {  HIGH_MINUTE_VOL_HIGH_MSG,  "T:[Vdot e tot] [>=] li$mi$te im$po$sta$to per > 120 sec." },

   {  HIGH_TIDAL_VOL_MSG,  "[HI Vte]" }, // $[05042]
   {  HIGH_TIDAL_VOL_REMEDY_MSG,  "T:Controllare le impo$stazioni e le variazioni di Resistenza e Compliance del paziente." },
   {  HIGH_TIDAL_VOL_LOW_MSG,  "T:Ultimi 2 respiri [>=] limite impostato." },
   {  HIGH_TIDAL_VOL_MED_MSG,  "T:Ultimi 4 respiri [>=] limite impostato." },
   {  HIGH_TIDAL_VOL_HIGH_MSG,  "T:Ultimi 10 o pi� respiri [>=] limite impostato." },

   {  HIGH_O2_MSG,  "[HI O2]" },
   {  HIGH_O2_REMEDY_MSG,  "T:Controllare il paziente, le sorgenti dei gas, l'analizzatore [O2] ed il ventilatore." },
   {  HIGH_O2_LOW_ANALYSIS_MSG,  "T:[O2] misurato > impostazioni per [>=] 30 sec., ma < 2 min." },
   {  HIGH_O2_MEDIUM_ANALYSIS_MSG,  "T:[O2] misurato > impostazioni per [>=] 2 min." },

   {  HIGH_PRES_CIRC_MSG,  "[HI Ppeak]" },
   {  HIGH_PRES_REMEDY_MSG,  "T:Con$trol$lare il pazien$te, il circui$to ed il tubo ET." },
   {  HIGH_PRES_LOW_MSG,  "T:Ultimi respiro [>=] limite impostato." },
   {  HIGH_PRES_MED_MSG,  "T:Ultimi 3 respiri [>=] limite impostato." },
   {  HIGH_PRES_HIGH_MSG,  "T:Ultimi 4 o pi� respiri [>=] limite impostato." },

   {  HIGH_PRES_VENT_MSG,  "[HI Pvent]" },
   {  HIGH_PRES_VENT_LOW_MSG,  "T:1 respiro [>=] limite." },
   {  HIGH_PRES_VENT_MED_MSG,  "T:2 respiri [>=] limite." },
   {  HIGH_PRES_VENT_HIGH_MSG,  "T:3 o pi� respiri [>=] limite." },

   {  HIGH_RESP_RATE_MSG,  "[HI ftot]" }, // $[05042]
   {  HIGH_RESP_RATE_LOW_MSG,  "T:[ftot] [>=] limite impostato per [<=] 30 sec." },
   {  HIGH_RESP_RATE_MED_MSG,  "T:[ftot] [>=] limite impostato per > 30 sec." },
   {  HIGH_RESP_RATE_HIGH_MSG,  "T:[ftot] [>=] limite impostato per > 120 sec." },

   {  AC_POWER_LOSS_MSG,  "NO ALIMENT. CA" },
   {  AC_POWER_LOSS_ANALYSIS_MSG,  "T:Funzionamento a batteria." },
   {  LOW_OPERATIONAL_TIME_MSG,  "T:Tempo di funzionamento < 2 minuti." },
   {  PREPARE_FOR_POWER_LOSS_MSG,  "T:Prepararsi per perdita alimentazione." },

   {  INOPERATIVE_BATTERY_MSG,  "BAT$TE$RIA NON OPE$RA$TI$VA" },
   {  NON_FUNC_BATTERY_ANALYSIS_MSG,  "T:Carica non adeguata o batteria non funzionante." },
   {  NON_FUNC_BATTERY_REMEDY_MSG,  "T:Sostituire la batteria." },

   {  LOW_BATTERY_MSG,  "BATT. QUA$SI SCARI$CA" },
   {  LOW_BATTERY_ANALYSIS_MSG,  "T:Tempo di funzionamento < 2 minuti." },
   {  LOW_BATTERY_REMEDY_MSG,  "T:Sostituire o ricaricare la batteria." },

   {  LOW_AC_POWER_MSG,  "A$LI$MEN$TAZ. CA INSUFFICIENTE" },
   {  LOW_AC_POWER_ANALYSIS_MSG,  "T:Ventilatore attualmente non interessato." },
   {  LOW_AC_POWER_REMEDY_MSG,  "T:� possibile un'interruzione dell'alimentazione." },

   {  LOW_MAND_TIDAL_VOL_MSG,  "[LO Vte mand]" }, // $[05042]
   {  LOW_MAND_TIDAL_VOL_REMEDY_MSG,  "T:Controllare eventuali perdite e modifiche in Resistenza e Compliance del paziente." },
   {  LOW_MAND_TIDAL_VOL_LOW_MSG,  "T:Ultimi 2 respiri controllati [<=] limite impo$stato." },
   {  LOW_MAND_TIDAL_VOL_MED_MSG,  "T:Ultimi 4 respiri controllati [<=] limite impo$stato." },
   {  LOW_MAND_TIDAL_VOL_HIGH_MSG,  "T:Ultimi 10 o pi� respiri controllati [<=] limite impo$stato." },

   {  LOW_MINUTE_VOL_MSG,  "[LO Vdot e tot]" }, // $[05042]
   {  LOW_MINUTE_VOL_LOW_MSG,  "T:[Vdot e tot] [<=] limite impo$stato per [<=] 30 sec." },
   {  LOW_MINUTE_VOL_MED_MSG,  "T:[Vdot e tot] [<=] limite impo$stato per > 30 sec." },
   {  LOW_MINUTE_VOL_HIGH_MSG,  "T:[Vdot e tot] [<=] limite impo$stato per > 120 sec." },

   {  LOW_SPONT_TIDAL_VOL_MSG,  "[LO Vte spont]" }, // $[05042]
   {  LOW_SPONT_TIDAL_VOL_LOW_MSG,  "T:Ultimi 2 respiri spontanei [<=] limite impo$sta$to." },
   {  LOW_SPONT_TIDAL_VOL_MED_MSG,  "T:Ultimi 4 respiri spontanei [<=] limite impo$sta$to." },
   {  LOW_SPONT_TIDAL_VOL_HIGH_MSG,  "T:Ultimi 10 o pi� respiri spontanei [<=] limite impo$sta$to." },

   {  LOW_O2_MSG,  "[LO O2]" },
   {  LOW_O2_ANALYSIS_MSG,  "T:[O2] misurata < [O2] impostata." },
   {  LOW_O2_REMEDY_MSG,  "T:Controllare il paziente, le sorgenti di gas, l'analizzatore [O2] ed il ventilatore." },

   {  NO_AIR_SUPPLY_MSG,  "NO A$LI$MENT. ARIA" },
   {  NO_AIR_SUPPLY_REMEDY_MSG,  "T:Controllare la sorgente di aria." },
   {  CHECK_PATIENT_AIR_SOURCE_MSG,  "T:Controllare il paziente e la sorgente di aria." },

   {  COMPRESSOR_INOP_WITH_100_O2_MSG,  "T:Compressore non operativo.  La ventilazione conti$nua secondo le im$postazioni. Solo [O2] disponibile." },
   {  COMPRESSOR_INOP_EXCEPT_100_O2_MSG,  "T:Compressore non operativo.  La ventilazione conti$nua secondo le im$postazioni, fatta eccezione per [O2] = 100%." },
   {  NO_VENT_CHECK_GAS_MSG,  "T:Fornire ventila$zione alternativa. Controllare le due sorgenti di gas." },

   {  NO_O2_SUPPLY_MSG,  "NO A$LI$MENT. [O2]" },
   {  NO_O2_SUPPLY_REMEDY_MSG,  "T:Controllare la sorgente di [O2]." },

   {  CHECK_PATIENT_O2_SOURCE_MSG,  "T:Controllare il paziente e la sorgente di [O2]." },

   {  COMPRESSOR_INOPERATIVE_MSG,  "COMP. NON OPERATI$VO" },
   {  COMPRESSOR_INOPERATIVE_ANALYSIS_MSG,  "T:Aria compressore mancante." },
   {  REPLACE_COMPRESSOR_MSG,  "T:Sostituire il compressore." },
   {  COMPRESSOR_NOT_DURING_LOW_AC_POWER_MSG,  "T:Funzionamento interrotto durante alimentazione CA insufficiente." },
   {  COMPRESSOR_NOT_DURING_AC_POWER_LOSS_MSG,  "T:Funzionamento interrotto durante la perdita di ali$mentazione CA." },

   {  CIRCUIT_DISCONNECT_MSG,  "DISCON$NESSIONE CIRCUI$TO" },
   {  CIRCUIT_DISCONNECT_REMEDY_MSG,  "T:Controllare il paziente. Riconnettere circuito." },
   {  CIRCUIT_DISCONNECT_COMPRESSOR_REMEDY_MSG,  "T:Controllare il paziente. Riconnettere circuito. La perdita  potrebbe superare il valore di compensazione max per il compressore." },
   {  CHECK_PATIENT_VENTILATOR_MSG,  "T:Controllare il paziente e lo stato del ventila$tore." },

   {  SEVERE_OCCLUSION_MSG,  "GRA$VE OCCLUSIONE" },
   {  LITTLE_NO_VENTILATION_MSG,  "T:Ventilazione ridotta/mancante." },
   {  SEVERE_OCCLUSION_REMEDY_MSG,  "T:Controllare il paziente. Fornire venti$lazione alternativa.  Liberare le occlusioni e pulire il circuito." },

   {  INSP_TOO_LONG_MSG,  "INSP. TROP$PO LUN$GA" },
   {  INSP_TOO_LONG_REMEDY_MSG,  "T:Con$trollare il paziente. Verificare eventuali perdite." },
   {  INSP_TOO_LONG_LOW_MSG,  "T:Ultimi 2 respiri spontanei = limite [Ti] ba$sato sul [IBW]." },
   {  INSP_TOO_LONG_MED_MSG,  "T:Ultimi 4 respiri spontanei = limite [Ti] basato sul [IBW]." },
   {  INSP_TOO_LONG_HIGH_MSG,  "T:Ultimi 10 o pi� respiri spontanei = limite [Ti] basato sul [IBW]." },

   {  PROCEDURE_ERROR_MSG,  "ER$RO$RE DI PROCEDU$RA" },
   {  PROCEDURE_ERROR_ANALYSIS_MSG,  "T:Paziente collegato prima di aver completato l'impo$stazione." },
   {  PROCEDURE_ERROR_REMEDY_MSG,  "T:Fornire ventilazione alternativa. Completare la procedura di impostazione." },

   {  VOLUME_LIMIT_MSG, "[Vt] LIMITATO IN COMPLIANCE" }, // TDO
   {  VOLUME_LIMIT_ANALYSIS_MSG,   "T:Limite max. compensazione compliance." }, // TDO
   {  VOLUME_LIMIT_REMEDY_MSG,   "T:Volume inspirato < impostato. Controllare il paziente ed il tipo di circuito." }, // TDO

   {  INSPIRED_SPONT_VOLUME_LIMIT_MSG,  "[HI Vti spont]" },

   {  INSPIRED_SPONT_VOLUME_LIMIT_REMEDY_MSG,  "T:Con$trollare il pazien$te e le impostazioni." },
   {  INSPIRED_SPONT_VOLUME_LIMIT_LOW_MSG,  "T:Ultimi respiro spontaneo [>=] limite impostato." },
   {  INSPIRED_SPONT_VOLUME_LIMIT_MED_MSG,  "T:Ultimi 3 respiri spontanei [>=] limite impostato." },
   {  INSPIRED_SPONT_VOLUME_LIMIT_HIGH_MSG,  "T:Ultimi 4 o pi� respiri spontanei [>=] limite impostato." },

   {  INSPIRED_SPONT_PRESSURE_LIMIT_MSG,  "[HI Vti spont]" },
   {  INSPIRED_SPONT_TC_PRESSURE_LIMIT_REMEDY_MSG,  "T:Verificare eventuali perdite, l'impostazione del tipo di tubo e l'impostazione I.D." },
   {  INSPIRED_SPONT_PAV_PRESSURE_LIMIT_REMEDY_MSG,  "T:Controllare le eventuali perdite, il diametro del tubo / cannula utilizzata, la % di Supporto impostata e lo stato di agitazione del paziente." },
   {  INSPIRED_SPONT_PRESSURE_LIMIT_LOW_MSG,  "T:Ultimi respiro spontaneo [>=] limite impostato." },
   {  INSPIRED_SPONT_PRESSURE_LIMIT_MED_MSG,  "T:Ultimi 3 respiri spontanei [>=] limite impostato." },
   {  INSPIRED_SPONT_PRESSURE_LIMIT_HIGH_MSG,  "T:Ultimi 4 o pi� respiri spontanei [>=] limite impostato." },


   {  INSPIRED_MAND_VOLUME_LIMIT_MSG,  "[HI Vti mand]" },
   {  INSPIRED_MAND_VOLUME_LIMIT_REMEDY_MSG,  "T:Con$trollare il pazien$te e le impostazioni." },
   {  INSPIRED_MAND_VOLUME_LIMIT_LOW_MSG,  "T:ultimo respiro controllato [>=] limite impostato." },
   {  INSPIRED_MAND_VOLUME_LIMIT_MED_MSG,  "T:ultimi 3 respiri controllato [>=] limite impostato." },
   {  INSPIRED_MAND_VOLUME_LIMIT_HIGH_MSG,  "T:ultimi 4 o pi� respiri controllato [>=] limite impostato." },

   {  COMPENSATION_LIMIT_MSG,  "[HI Pcomp]" },
   {  COMPENSATION_TC_LIMIT_REMEDY_MSG,  "T:Verificare eventuali perdite, l'impostazione del tipo di tubo e l'impostazione I.D." },
   {  COMPENSATION_PAV_LIMIT_REMEDY_MSG,  "T:Controllare le eventuali perdite, il diametro del tubo / cannula utilizzata, la % di Supporto impostata e lo stato di agitazione del paziente." },
   {  COMPENSATION_LIMIT_LOW_MSG,  "T:Ultimo respiro spontaneo [>=] limite [HI Ppeak] impostato - 5." },
   {  COMPENSATION_LIMIT_MED_MSG,  "T:Ultimi 3 respiri spontanei [>=] limite [HI Ppeak] impostato - 5." },
   {  COMPENSATION_LIMIT_HIGH_MSG,  "T:Ultimi 4 o pi� respiri spontanei [>=] limite [HI Ppeak] impostato - 5." },

   {  LONG_PAV_STARTUP_MSG,  "AVVIO PAV TROPPO LUNGO" },
   {  LONG_PAV_STARTUP_REMEDY_MSG,  "T:Verificare eventuali perdite, respirazione superficiale e impostazioni per [HI Vti spont] e [HI Ppeak]." },
   {  LONG_PAV_STARTUP_LOW_MSG,  "T:Avvio di PAV non completato per [>=] 45 s." },
   {  LONG_PAV_STARTUP_MED_MSG,  "T:Avvio di PAV non completato per [>=] 90 s." },
   {  LONG_PAV_STARTUP_HIGH_MSG, "T:Avvio di PAV non completato per [>=] 120 s." },

   {  PAV_RC_NOT_ASSESSED_MSG,  "R e C di PAV NON VALUTATE" },
   {  PAV_RC_NOT_ASSESSED_REMEDY_MSG,  "T:Verificare eventuali perdite, respirazione superficiale e impostazioni per I.D. tubo, [HI Vti spont] e [HI Ppeak]." },
   {  PAV_RC_NOT_ASSESSED_LOW_MSG,  "T:R e/o C per pi� di 15 minuti." },
   {  PAV_RC_NOT_ASSESSED_MED_MSG,  "T:R e/o C per pi� di 30 minuti." },

   {  VOLUME_NOT_DELIVERED_MSG,  "VOLUME NON SOMMINISTRATO" },
   {  VOLUME_NOT_DELIVERED_SPONT_LOW_MSG,  "T:Ultimi 2 respiri spontanei, pressione > al livello massimo consentito." },
   {  VOLUME_NOT_DELIVERED_SPONT_MED_MSG,  "T:Ultimi 10 o pi� respiri spontanei, pressione > al livello massimo consentito." },
   {  VOLUME_NOT_DELIVERED_MAND_LOW_MSG,  "T:Ultimi 2 respiri controllati, pressione > al livello massimo consentito." },
   {  VOLUME_NOT_DELIVERED_MAND_MED_MSG,  "T:Ultimi 10 o pi� respiri controllati, pressione > al livello massimo consentito." },
   {  VOLUME_NOT_DELIVERED_REMEDY_MSG,  "T:Controllare il paziente e l�impostazione di [HI Ppeak]." },

   {  LOW_PRES_CIRC_MSG, "[LO Ppeak]" },
   {  LOW_PRES_LOW_MSG,  "T:Ultimi 2 respiri, pressione [<=] limite impost." },
   {  LOW_PRES_MED_MSG,  "T:Ultimi 4 respiri, pressione [<=] limite impost." },
   {  LOW_PRES_HIGH_MSG, "T:Ultimi 10 o pi� respiri, pressione [<=] limite impost." },
   {  LOW_PRES_REMEDY_MSG,  "T:Verificare eventuali perdite." },

   {  AUTO_RESET_UT_MSG,   "T:Azzeramen$to automati$co" },
   {  PAT_DATA_RESET_UT_MSG,   "T:Azzeramen$to da$ti pa$zien$te" },
   {  ALARM_SILENCE_UT_MSG,   "T:Tacitazio$ne allarmi" },
   {  END_ALARM_SILENCE_UT_MSG,   "T:Fine tacitazio$ne allar$mi" },
   {  AUGMENTATION_UT_MSG,   "T:Prio$ri$t� aumen$ta$ta" },
   {  DETECTION_UT_MSG,   "T:Rileva$men$to" },
   {  MANUAL_RESET_UT_MSG,  "T:Azze$ra$men$to ma$nua$le " },
   {  INDEPENDENT_UT_MSG,  "T:In$di$pen$den$te" },
   {  END_ALARM_SILENCE_HIGH_URGENCY_UT_MSG,  "T:Nuo$vo al$lar$me al$ta priorit�" },
   {  ERROR_IN_UPDATE_TYPE_MSG,  "ERROR IN UPDATE TYPE" },
   {  LOW_URGENCY_MSG,  "LOW URGENCY" },
   {  MEDIUM_URGENCY_MSG,  "MEDIUM URGENCY" },
   {  HIGH_URGENCY_MSG,  "HIGH URGENCY" },
   {  HIGH_URGENCY_AUTO_RESET_MSG,  "HIGH URGENCY AU$TO RESET" },
   {  NORMAL_URGENCY_MSG,  "NORMAL URGENCY" },
   {  ERROR_IN_URGENCY_MSG,  "ERROR IN URGENCY" },
   {  LOW_URGENCY_AUDIO_MSG,  "LOW URGENCY AU$DIO" },
   {  MEDIUM_URGENCY_AUDIO_MSG,  "MEDIUM URGENCY AU$DIO" },
   {  HIGH_URGENCY_AUDIO_MSG,  "HIGH URGENCY AU$DIO" },
   {  ALARM_SUMMARY_MSG,  "ALARM SUMMARY: " },
   {  ALARM_AUDIO_MSG,  "ALARM AU$DIO: " },
   {  NORMAL_URGENCY_AUDIO_MSG,  "NORMAL URGENCY (No Au$dio)" },
   {  ALARM_SILENCE_AUDIO_MSG,  "ALARM SILENCE (No Au$dio)" },
   {  NOTE_MSG,  "Vedere" },
   {  PROX_INOPERATIVE_MSG,  "PROX Non Operativo" },
   {  PROX_INOPERATIVE_ANALYSIS_MSG,  "T:Dati da Sensore Flusso Prossimale non utilizzati." },
   {  PROX_INOPERATIVE_REMEDY_MSG,  "T:Controllare conness. Sens. Flusso Pross. e circ. non occlusi/con perdite." }

   };


//=====================================================================
//
//      Public Methods...
//
//=====================================================================

