#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================ C L A S S   D E S C R I P T I O N ====
//@ Class: NetworkApp - Network Interface Application. The class
//       provides highlevel API for the various Network communcation 
//       services mainly between the GUI and BD boards.
//---------------------------------------------------------------------
//@ Interface-Description
//
//>Von
//
//  The	NetworkApp class provides high level API interfaces 
//  that enable Sigma Software components to send and receive
//  network messages between the GUI and BD boards. The class 
//  provides high level API interfaces to broadcast Sigma data.
//  The class also provides Blocked IO mode communication interfaces.
//  The lower level communication to the TCP/IP stack is handled
//  internally within this class and other related Communication
//  Socket support classes.
//
//  All the communication errors are detected and handled
//  within this class. Several accumulated communication error
//  conditions will be escalated into a communcation loss state.
//  There will be an independent NetworkApp task running which
//  monitors the communication connection between the GUI
//  and BD boards to guarantee that the connection is active and sound.
//  
//  When the task detects one of the communication loss conditions,
//  it raises the Communication loss state to the Sys-Init task
//  and reports the error condition to the Back-Ground task.
//  In the case of the communication loss between two boards, 
//  the Network Application task will continually try to
//  re-establish the communication.
//  
//  This class is never instantiated. All the interfaces to this
//  class is done through static methods.
// 
//---------------------------------------------------------------------
//@ Rationale
//    This class maintains all the application level network communication
//    code and data so other sub-systems need not to deal with lower
//    level network communication logic and data.
//    This class also provides one central task to monitor network link.
//    This class handles all the application level communication error
//    and its recovery.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The class initializes two permanent socket connections one to 
//  receive data and one to send data over to the other board.
//  Since sending and receiving network data is performed by different
//  tasks, two independent uni-directional socket connections are used
//  to avoid socket contention and out of sync data transmission.
//  The one socket is used only to receive data from the other board
//  by the Network Application task only.
//  The other socket is used to transmit data to the other board by
//  the various tasks including the Network Application task.
//  The other board uses these two socket connections in a reverse
//  direction. The send only socket operatates in a non-blocking IO 
//  mode socket to avoid making a task with a send message request
//  wait while the lower level TCP/IP code completes data transmission.
//  Another reason was that if the other board goes down while this 
//  board is waiting for the TCP/IP ack message, the Stack-Ware puts 
//  the task indefinitely waiting an ack that will never come. For the
//  same reason, the UDP socket for the broadcast data is also 
//  non-blocking.
// 

//---------------------------------------------------------------------
//@ Fault-Handling
//    Repeated socket library call failures are logged to the
//    non-volatile RAM through POST's DiagnosticCode class.
//    Other Communcation errors are also logged to the non-volatile RAM.
//    Repeated failures to re-establish  communication link will not
//    cause software fault. The communication link state changes
//    are reported to the Back Ground task. Which logs a system fault
//    error message to the non-volatile  and generates an alarm 
//    message. The change in the communication link is reported 
//    to the Sys-Init task. The Software logic errors are detected with
//    the class pre-conditions and class assertion macros.
//    
//>Voff
//---------------------------------------------------------------------
//@ Restrictions
//    This class shouldn't be instantiated.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/NetworkApp.ccv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By: gdc     Date: 03-Jan-2011  SCR Number: 6728
//  Project:  XENA2
//  Description:
//      Removed local communications logic from development only code.
//
//  Revision: 013  By: gdc     Date: 02-Jan-2011  SCR Number: 6726
//  Project:  XENA2
//  Description:
//      Changed WaitForInput to use maximum socket id plus one for
//      select call.
//
//  Revision: 012  By: gdc     Date: 30-Dec-2010  SCR Number: 6723
//  Project:  XENA2
//  Description:
//      Changed all BlockIOSocket class references to NetworkSocket. 
//
//  Revision: 011  By: gdc     Date: 30-Dec-2010  SCR Number: 5738
//  Project:  XENA2
//  Description:
//      Corrected to call "gated" version of close() socket that
//      elevates task priority to "network high" before closing
//      socket. Ungated version caused race condition with Stack-Ware
//      task by queuing a FIN packet to the interface queue at the 
//      same time the Stack-Ware task was dequeuing mbufs.
//
//  Revision: 010  By: gdc     Date: 30-Dec-2010  SCR Number: 6717
//  Project:  XENA2
//  Description:
//      Corrected creation and initialization of all socket types.
//
//  Revision: 009  By: gdc     Date: 30-Dec-2010  SCR Number: 6697
//  Project:  XENA2
//  Description:
//      Removed code to broadcast BD IP address as this logic is no 
//      longer used and consumes all mbufs causing a BD reset
//      during communication loss with the GUI.
//
//  Revision: 008  By: gdc     Date: 30-Dec-2010  SCR Number: 6716
//  Project:  XENA2
//  Description:
//      Use thread-safe socket library.
//
//  Revision: 007  By: gdc     Date: 30-Dec-2010  SCR Number: 6712
//  Project:  XENA2
//  Description:
//      Use socket sendmsg() instead of send() to eliminate copy 
//      operation.
//
//  Revision: 006  By: erm  Date: 02-Apr-2007  Dr Number 6381            
//  Project: Trend
//  Description:
//     Remove the attempt to re-read on the Recv-Socket after a valid read.
//     Calling SELECT with zero wait time with data above low-water mark,
//     can result in receiving an incomplete PB header. 
//
//  Revision: 005  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 04  By: hct   Date:  26-JUN-1997    DR Number: 1779
//  	Project:  Sigma (840)
//	Description:
//		Add TaskMonitor::Report to WaitForInput().  WaitForInput() was
//              running for up to three seconds during a communcation failure
//              without reporting to the TaskMonitor (which was set up for a
//              2600ms timeout).
//
//  Revision: 03  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  hct    Date:  25-APR-1997   DR Number: 1996
//       Project:  Sigma (R8027)
//       Description:
//             Added requirement number 00532.
//
//  Revision: 001  By:  jhv    Date:  16-Feb-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "CommFaultId.hh"
#include "NetworkApp.hh"
#include "RecvSocket.hh"
#include "SendSocket.hh"
#include "BroadcastSocket.hh"
#include "TimeStamp.hh"
#include "FaultHandler.hh"
#include "DiagnosticCode.hh"
#include "TaskMonitor.hh"
#include "Background.hh"
#include "Post.hh"
#include "Task.hh"
#include "CommTaskAgent.hh"
#include "CriticalSection.hh"
#if defined (SIGMA_BD_CPU)
#include "NetworkUtest.hh"
#endif


//@ Code...

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

Boolean NetworkApp::CommunicationUp_ = FALSE;
Boolean NetworkApp::KeepAliveFeature_ = TRUE;
Boolean NetworkApp::ConfirmOffKeepAlive_ = FALSE;


NetworkApp::CallbackMsg *NetworkApp::PCallbackTbl_[TOTAL_MSG_NUMBER];

#if defined( INTEGRATION_TEST_ENABLE )
#if defined( SIGMA_GUI_CPU )
Uint32                  NetworkApp::m_iIsCallbackTbl[TOTAL_MSG_NUMBER];
#endif
#endif

OpenCallBackPtr     NetworkApp::POpenCallback_ = 0;
Uint NetworkApp::NextBufAvail_ = 0;

static Uint Qmem[(sizeof(NetworkSocket)+sizeof(Uint) -1)/sizeof(Uint)];
static Uint Rmem[(sizeof(RecvSocket)+sizeof(Uint) -1)/sizeof(Uint)];
static Uint Smem[(sizeof(SendSocket)+sizeof(Uint) -1)/sizeof(Uint)];
static Uint Bmem[(sizeof(BroadcastSocket)+sizeof(Uint) -1)/sizeof(Uint)];
NetworkSocket &NetworkApp::RSettingsSocket_ =*((NetworkSocket *) Qmem);
SendSocket &NetworkApp::RSendSocket_ = *((SendSocket *) Smem);
RecvSocket &NetworkApp::RRecvSocket_ = *((RecvSocket *) Rmem);
BroadcastSocket &NetworkApp::RBroadcastSocket_= *((BroadcastSocket *) Bmem);

NetworkApp::CallbackMsg NetworkApp::BufPool_[TOTAL_RECVMSG_CALLBACKS];

//static const Uint32  BD_IP_ADDR = 0x92D691CF;  // 146.214.145.207
static const Uint32  BD_IP_ADDR = IP_ADDRESS(192, 168, 0, 3);

#if	defined(SIGMA_BD_CPU) 
static Uint Nmem[(sizeof(NetworkSocket)+sizeof(Uint) -1)/sizeof(Uint)];
NetworkSocket &NetworkApp::RSettingsContactSocket_=*((NetworkSocket *) Nmem);
XmitDataMsgId NetworkApp::OtherBoardAliveMsg = GUI_ALIVE_MSG;
XmitDataMsgId NetworkApp::ThisBoardAliveMsg  = BD_ALIVE_MSG;
static const BkEventName  COMM_LOSS_ID_ = BK_LOSS_OF_GUI_COMM;
static const BkEventName  COMM_RESUME_ID_ = BK_RESUME_GUI_COMM;
static const BkEventName  COMM_INIT_RESUME_ID_ = BK_INIT_RESUME_GUI_COMM;
static const BkEventName  COMM_INIT_LOSS_ID_ = BK_INIT_LOSS_GUI_COMM;
#elif   defined(SIGMA_GUI_CPU)
XmitDataMsgId NetworkApp::OtherBoardAliveMsg = BD_ALIVE_MSG;
XmitDataMsgId NetworkApp::ThisBoardAliveMsg  = GUI_ALIVE_MSG;
static const BkEventName  COMM_LOSS_ID_ = BK_LOSS_OF_BD_COMM;
static const BkEventName  COMM_RESUME_ID_ = BK_RESUME_BD_COMM;
static const BkEventName  COMM_INIT_RESUME_ID_ = BK_INIT_RESUME_BD_COMM;
static const BkEventName  COMM_INIT_LOSS_ID_ = BK_INIT_LOSS_BD_COMM;
#endif //defined(SIGMA_GUI_CPU)

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize(void)
//
//@ Interface-Description
//    Initializes class static members to initial value.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class doesn't get instantiated. The Initialize method
//    should be called at the system initialization time or before the
//    Network Application task activation. This static method should be
//    before any of the NetworkApp class methods are called from
//    outside.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition 
//	none
//@ End-Method
//=====================================================================

void
NetworkApp::Initialize()
{
#if defined(SIGMA_BD_CPU)
    new (&RSettingsContactSocket_ ) NetworkSocket();
#endif
    new (&RSettingsSocket_ ) NetworkSocket();
    new (&RRecvSocket_ ) RecvSocket();
    new (&RSendSocket_ ) SendSocket();
    new (&RBroadcastSocket_ ) BroadcastSocket();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task()
//
//@ Interface-Description
//   Task is the main task loop for the Network Application task.
//   It establishes four sockets, two for direct communication between
//   the GUI and BD boards, one for the blocked io and one for the 
//   data broadcasting. After successful socket connections,
//   the method goes on an infinite loop. Each cycle waits on two
//   sockets for either incoming data from the other Sigma board
//   or a blocked IO socket connection request from any network node.
//   The task ensures all the incoming network messages are passed
//   to the pre-registered callback routines. If there is a Blocked
//   IO socket connection request, the task processes the request
//   and ensures the Blocked IO connection is made available to
//   a pre-registered callback routine for a Blocked IO socket open.
//
//   The task detects the communication loss if one of the following
//   conditions occur;
//     1. It did not receive any network messages from the other
//        Sigma board for an entire second. 
//     2. If an message ACK message is not received for an already
//        transmitted network message at least one second.
//     3. A user request to send a network message failed due to 
//        repeated socket library failures.
//     4. Initial communications fails to establish within 10
//        seconds of powerup. $[00532]
//
//   When the loss of communication occurs, the task closes all the
//   active system sockets. And then, the task informs the communication
//   loss state change to the Background task and the Sys-Init task.
//	 During the communication loss state, the task repeatedly tries to
//   re-establish all the communication socket connections between the
//   BD and GUI boards. The task also informs the communication resume
//   state to the Background taks and the Sys-Init task.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//   The Network Application task runs at a higher priority than
//   most of the SIGMA application tasks that uses the Network 
//   communication facility. The task needs to be activated every
//   200 milli seconds to do the house keeping activities.
//
//   The task calls the select socket library call with 200 ms wait
//   time duration. The task is put to sleep by the Stack-Ware code
//   until it receives any incoming network request or wait time runs
//   out.
//
//   For the blocked IO support, the task creates two sockets, 
//   RSettingsContactSocket_ and RSettingsSocket_. 
//   The RSettingsContactSocket_ is the settings IO contact point.
//   When someone requests a socket connection to this socket, a new
//   settings IO socket is made to the RSettingsSocket_ socket object.
//   Then on, all the settings IO data transmission is done through
//   the RSettingsSocket_ socket object.
//
//   One of the house keeping work that the Network Application task
//   does is to make sure that at least one network message is sent
//   out at every 200 ms. If there was no network message sent out
//   within last 200 ms, the task sends out an "I am Alive" message
//   to other board. It expects to receive at least one network message
//   every 200 ms from the other board, also.  If no data comes over
//   for greather than KEEP_ALIVE_INTERVAL * MAX_ALIVE_MSG_MISS ms, the
//   communication connections are considered dead.  $[06089] $[06091]
//   In that case, the task closes all the active sockets and
//   stops sending any "I am Alive" messages so it forces the other
//   board to a loss of communication state.
//   At this point, both boards will go into socket re-initialization
//   phase. All the open sockets are closed first and reopened.
//
//   After the initial socket connections, the GUI and BD boards behave
//   exactly same and symmetric. Some of the class static members,
//   ThisBoardAliveMsg and OtherBoardAliveMsg are initialized to
//   different network message ids depending on the execution board.
//
//   While waiting for more input from the other board or Block IO
//   socket connection request, the select socket library call releases
//   the processor to other tasks. The static class member, RRecvSocket_
//   is a RecvSocket object. This object receives all the network 
//   messages that comes from the other Sigma board and handles them
//   within the RecvSocket class.  The RRecvSocket_ object is used only
//   by the NetworkApp task. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetworkApp::Task()
{
	//TODO E600 VM: create SocketWrap OS independent wrapper APIs for
	// fd_set and other socket calls
    fd_set readMask;
    TimeStamp currentTime;
    Boolean forever = TRUE;
    Boolean firstFailure = TRUE;
    int nfds;

    // Initialize OS network socket service
	while(startupOsNetworkSockets() == FALSE)
	{			                                  
		TaskMonitor::Report();
        Task::Delay(0, 500);
	}

    RegisterRecvMsg(ON_KEEP_ALIVE_MSG, NetworkApp::RecvOnKeepAliveMsg_);
    RegisterRecvMsg(OFF_KEEP_ALIVE_MSG, NetworkApp::RecvOffKeepAliveMsg_);
    RegisterRecvMsg(CONFIRM_OFF_KEEP_ALIVE_MSG, NetworkApp::RecvConfirmMsg_);
    RegisterRecvMsg(CONFIRM_ON_KEEP_ALIVE_MSG, NetworkApp::RecvConfirmMsg_);

#if defined(SIGMA_GUI_CPU)
    // This initial delay on the GUI allows time for the BD to start listening.
    // This resolves the ACK/RST from the BD in response to initial SYN from
    // the GUI if the BD sockets aren't open yet
    for ( Int i=0; i<6; i++ )
    {
        Task::Delay(0,100);
        TaskMonitor::Report();
    }
#endif

	//Only for unit-test on BD. This is an internal SW test (no actual connection)
	//therefore, done before establishing the connection..
#if defined (SIGMA_BD_CPU) && defined(RUN_NETWORK_UNIT_TEST)
	NetworkUtest::runEndiannessUtest();
	while(1)
	{
		Task::Delay(10);
	}
#endif

	//Open Broadcast Socket first (before trying to open the BD-GUI connected
	//TCP sockets) so that the subsystem can still broadcast even if the other
	//TCP socket creation failed.
	//Broadcast has nothing to do with communication status between BD and GUI
	//so its failure is not considered a comm-down
	while (RBroadcastSocket_.openUdpBroadcast() == -1 )
    { 
        TaskMonitor::Report();
        Task::Delay(0,100);
    }

    while ( OpenConnections_() == FAILURE )
    {
        currentTime.now();
        if ( ((currentTime - Post::GetPowerUpTime()) > MAX_NETWORK_WAIT_TIME )  
             && ( firstFailure  == TRUE ) )
        {
            firstFailure = FALSE;
            CloseActiveConnectedSockets_();
            TaskMonitor::Report();
            PRINTF(("NetworkApp : Initial Communication is down.\n"));
            Background::ReportBkEvent(COMM_INIT_LOSS_ID_); 
            TaskMonitor::Report();
            CommTaskAgent::NetworkDown();
        }
        else
        {
            CloseActiveConnectedSockets_(); 
        }
        TaskMonitor::Report();
        Task::Delay(0,100);
    }
    Background::ReportBkEvent(COMM_INIT_RESUME_ID_);
    PRINTF(("NetworkApp : Initial Communication is Up.\n"));
    TaskMonitor::Report();
    CommTaskAgent::NetworkUp();

    while (forever)
    {
        if (( KeepAliveFeature_ &&  RRecvSocket_.isMaxActiveMsgDelay()) ||
            RSendSocket_.anyLostMsg() || RRecvSocket_.anyLostMsg() )
        {
            CloseActiveConnectedSockets_(); 
            Background::ReportBkEvent(COMM_LOSS_ID_);
            PRINTF(("NetworkApp : Communication is down.\n"));
            CommTaskAgent::NetworkDown();
            while ( OpenConnections_() == FAILURE )
            {
                CloseActiveConnectedSockets_(); 
                TaskMonitor::Report(); 
                Task::Delay(0,100);
            }
            Background::ReportBkEvent(COMM_RESUME_ID_);
            PRINTF(("NetworkApp : Communication is Up.\n"));
            CommTaskAgent::NetworkUp(); 
        }

        FD_ZERO(&readMask);
        FD_SET(RRecvSocket_.getSocketId(), &readMask);
        nfds = RRecvSocket_.getSocketId() + 1;
#if defined(SIGMA_BD_CPU)
        FD_SET(RSettingsContactSocket_.getSocketId(), &readMask);
        nfds = MAX_VALUE(nfds, RSettingsContactSocket_.getSocketId() + 1);
#endif

		if ( NetworkSocket::WaitForInput(nfds, &readMask, KEEP_ALIVE_INTERVAL ) <= 0 )
        {
            if ( KeepAliveFeature_ )
            {
                RRecvSocket_.incrementDelayCounter(); 
            }
        }
        else
        {
            if ( FD_ISSET( RRecvSocket_.getSocketId(), &readMask))
            {
                if ( RRecvSocket_.readMainData() < 0 )
                {
                    continue; 
                }
            }
#if defined(SIGMA_BD_CPU)
            if ( FD_ISSET( RSettingsContactSocket_.getSocketId(), &readMask))
            {
                AcceptBlockIOConnection_(); 
            }
#endif
        }
        if ( KeepAliveFeature_ )
        {
            RSendSocket_.sendAliveMsg(ThisBoardAliveMsg );
        }

        TaskMonitor::Report();

    }
}

//--------------------------------------------------------------------
//@ Method: OpenConnections_()
//
//@ Interface-Description
//   OpenConnections_ establishes several network sockets
//   to provide communication between the BD board and the GUI
//   boards, to broadcast network data and to do blocked IO.
//   It establishes two TCP/IP socket connections between the two
//   boards, the GUI and BD boards. The first connection is used 
//   to send data to the GUI board from the BD board. The second 
//   connection is used to receive data from the GUI to the BD. 
//   The method also opens up a TCP/IP socket for the Blocked IO
//   socket contact point at the BLOCKED_IO_TCPPORT port id. 
//   One UDP socket is opened for the broadcasting.
//--------------------------------------------------------------------
//@ Implementation-Description
//    A BD board plays a server and a GUI board plays a client role
//    while two matching boards are trying to establish communication
//    socket connections. 
//
//    The BD board method opens a socket and binds the socket to 
//    a pre-determined BD Board TCP/IP port number.
//    The GUI board makes socket connections to this port id and 
//    a BD board IP address  and the BD board accepts the connection
//    request. The GUI uses a static IP address for the BD.
//
//    The BD board calls the acceptConnection method to make a socket
//    connection for the send only socket, RSendSocket_. The first
//    socket conneciton is not made within two seconds, the method
//    returns a failure status. The second acceptConnection 
//    method tries to establish a receive only socket with a half
//    second wait time.
//
//    For the GUI board, the openTcpClient() method is called to
//    make a socket connection to the BD board. 
//
//    All the open sockets are closed if one of the socket connections
//    fail to disallow any network communication.
//---------------------------------------------------------------------
//@ PreCondition
//	( IsCommunicationUp() == FALSE )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
NetworkApp::OpenConnections_()
{
    CLASS_PRE_CONDITION( IsCommunicationUp() == FALSE );

#if	defined(SIGMA_BD_CPU)
    NetworkSocket contactSocket;

    static const Int NUM_CLIENTS = 2;
    if ( contactSocket.openTcpServer( BD_BOARD_TCPPORT, NUM_CLIENTS ) == -1 )
    {
        return FAILURE;  
    }
	//TODO E600, VM: the task may suspend for the whole select() timeout value
    if ( contactSocket.acceptConnection( RSendSocket_, 2000 ) == -1 )
    {
        contactSocket.down();
        return FAILURE;  
    }
    if ( contactSocket.acceptConnection( RRecvSocket_, 500 ) == -1 )
    {
        contactSocket.down();
        return FAILURE; 
    }
    contactSocket.down(); 

    if ( RSettingsContactSocket_.openTcpServer( BLOCK_IO_TCPPORT, 1 ) == -1 )
    { 
        return FAILURE;
    }
#elif defined(SIGMA_GUI_CPU)
      Uint32 bdIP = BD_IP_ADDR;
    if ( RRecvSocket_.openTcpClient(bdIP, BD_BOARD_TCPPORT) == -1
         || RSendSocket_.openTcpClient(bdIP, BD_BOARD_TCPPORT) == -1 )
    {
#if defined(SIGMA_GUIPC_CPU)
            //For GUIPC we will keep trying both a local PC (i.e. simulator) or
            //the real BD.
            bdIP = IP_ADDRESS(127,0,0,1);
            if ( RRecvSocket_.openTcpClient(bdIP, BD_BOARD_TCPPORT) == -1
                  || RSendSocket_.openTcpClient(bdIP, BD_BOARD_TCPPORT) == -1 )
#endif
        return FAILURE; 
    }
#endif  //defined(SIGMA_GUI_CPU)

    SetCommUp_();
    if ( KeepAliveFeature_ == TRUE )
    {
        TurnOnKeepAlive(); 
    }
    else
    {
        TurnOffKeepAlive(); 
    }

    return SUCCESS; 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterForOpenSocket( OpenCallBackPtr pCallback )
//
//@ Interface-Description
//     The method registers a given callback routine to be called when
//     a blocked IO socket connection is made from an outside network
//     node. Only one callback routine at a time is supported.
//     The callback routine registered latest overrides the previous one.
//     A NULL callback function pointer is allowed to turn off the 
//     previous call routine.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The static member 'POpenCallback_' holds a callback routine 
//     function pointer. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetworkApp::RegisterForOpenSocket(OpenCallBackPtr pCallback) 
{
    CriticalSection mutex(NETAPP_QUEUE_ACCESS_MT);
    POpenCallback_  = pCallback;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterRecvMsg( XmitDataMsgId msgId,
//                             XmitCallBackPtr pCallback) 
//
//@ Interface-Description
//     Registers a given callback routine to be called when a given
//     network message type data is received from the other board.
//     A NULL function pointer is not allowed.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The static table 'PCallbackTbl_' holds all the registered
//     callback routines. The network message id is used to index into
//     the first callback entry for the network type. If more than one
//     callback routine is registerd for a netowrk message type, these
//     entries are linked. The 'TOTAL_RECVMSG_CALLBACKS' is the total
//     number of network message callback buffers. Each buffer can hold
//     information about a callback routine. Implementation assumes
//     that these callback routines are only added.
//     
//---------------------------------------------------------------------
//@ PreCondition
//  ( IsValidId(msgId))
//  ( pCallback)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetworkApp::RegisterRecvMsg( XmitDataMsgId msgId, XmitCallBackPtr pCallback) 
{
    CallbackMsg *pBuf;
    Uint index = msgId - LOWEST_NETWORK_MSG_ID;

    CLASS_PRE_CONDITION( IsValidId(msgId));
    CLASS_PRE_CONDITION(pCallback != NULL);

    {
        CriticalSection mutex(NETAPP_QUEUE_ACCESS_MT);
        CLASS_ASSERTION( NextBufAvail_ < TOTAL_RECVMSG_CALLBACKS );
    
        pBuf = &BufPool_[NextBufAvail_++];
        pBuf->callback = pCallback;
        if ( PCallbackTbl_[index] == NULL )
        {
            pBuf->pNext = NULL;  
            PCallbackTbl_[index] = pBuf;
        }
        else
        {
            pBuf->pNext = PCallbackTbl_[index];
            PCallbackTbl_[index] = pBuf; 
        }
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: XmitMsg( XmitDataMsgId msgId, void pData, Uint size)
//
//@ Interface-Description
//    Transmits a given network data message to the other board so the
//    message can be received by a pre-registered callback routine on
//    the other Sigma board. This method runs on a calling task thread
//    and it can be pre-emptied by a higher priority task (for an 
//    instance, Network App task). The XmitMsg guarantees that a given
//    network message is transmitted as a complete message.
//    During the communication loss state, the method would not transmit
//    a given network message. The given message would be lost.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Delegates the actual sending of the data to the RSendSocket_ object.
//    The RSendSocket_ SendSocket object internally handles 
//    synchronization of the transmission data among multiple tasks
//    with different task priorties.
//---------------------------------------------------------------------
//@ PreCondition
//    (IsValidId(msgId))
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
NetworkApp::XmitMsg( XmitDataMsgId msgId, void *pData, Uint size ) 
{
    CLASS_PRE_CONDITION(IsValidId(msgId));
    SAFE_CLASS_PRE_CONDITION(size <= MAX_NETWORK_BUF_SIZE);

    if ( !IsCommunicationUp())
    {
        return; 
    }
    RSendSocket_.sendMsg(msgId, (char *) pData, size); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ReportCommError(char *funcname, int error)
//
//@ Interface-Description
//    Logs a communication error to the Diagnostic Non-Volatile RAM.
//--------------------------------------------------------------------
//@ Implementation-Description
//   The DiagnosticCode::setCommunicationCode set the diagnostic
//   error message type to a communication fault type and the
//   given error code.
//---------------------------------------------------------------------
//@ PreCondition
// (error >= LOW_NETAPP_FAULT_VALUE)&&(error <= HIGH_NETAPP_FAULT_VALUE))
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetworkApp::ReportCommError(char *errmsg,  int error)
{
    PRINTF(("ReportCommError: %s error: %d\n" ,errmsg , error));
	UNUSED_SYMBOL(errmsg);

	//TODO E600 VM: the proprietry error codes need to be mapped or oprted to
	//				the actual OS codes.. Enable this when done.
    //CLASS_PRE_CONDITION(( error >= LOW_NETAPP_FAULT_VALUE ) 
    //                    && (error <= HIGH_NETAPP_FAULT_VALUE ));
    if ( error == 0 )
    {
        return;
    }
    DiagnosticCode diagnosticCode;

    // set the diagnostic code to a communication fault with the given id...
    diagnosticCode.setCommunicationCode((CommFaultID) error);

    // log the diagnostic code...
	FaultHandler::LogDiagnosticCode(diagnosticCode);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BOpenSocket()
//
//@ Interface-Description
//    Creates a new Blocked IO socket connection to the other Sigma
//    board. Only one Blocked IO socket connection is allowed at one
//    time. A Block IO socket cannot be opened before the Network 
//	  Application task initializes all the main sockets and the
//    Communication is up.  The method returns -1 if the Block IO socket
//    is alreay opened or the socket connection attempt fails due to 
//    lower level communication errors.
//    
//--------------------------------------------------------------------
//@ Implementation-Description
//    A blocked IO socket connection to the other board is made using 
//    the BLOCKED_IO_TCPPORT port id and the other board's IP address. 
//    The other board's IP address is obtained through already 
//    established socket connection between two boards, the RrecvSocket
//    socket.
//  
//    The method returns -1 if the RSettingsSocket_ socket is alreay
//    opened or the 'connect' socket library fails repeatedly up to
//    four times. Each time, the connect call fails, it waits for
//    a half second and tries again.
//    
//---------------------------------------------------------------------
//@ PreCondition
//	 	( !RSettingsSocket_.opened() ), shouldn't open an already
//      opened socket.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int
NetworkApp::BOpenSocket(void)
{
    if ( !IsCommunicationUp() )
    {
        return -1;  
    }
    if ( RSettingsSocket_.opened() )
    {
        return -1; 
    }

    if ( RSettingsSocket_.openTcpClient(BD_IP_ADDR, BLOCK_IO_TCPPORT) == -1 )
    {
        RSettingsSocket_.down();
        return -1;
    }

    return RSettingsSocket_.getSocketId();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BXmitMsg(Int socketId,  XmitDataMsgId msgId, void *pData,
//			Uint32 size)
//
//@ Interface-Description
//    Transmits a given network message on the Blocked IO socket
//    connection. The method returns the number of bytes that have been
//    transmitted on the Block IO socket connection. The method returns
//    -1 if there is a communication error. The given socketId should
//    match the Block IO socket id.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Call the sendMsg method on the RSettingsSocket_ BlockIOSocket
//     object. The RSettingsSocket_ is a blocked mode socket. The TCP/IP
//     would not return the program control until all the data is
//     passed to the other board completely. 
//---------------------------------------------------------------------
//@ PreCondition
//    ( IsValidId(msgId) && pData )
//    ( size <= MAX_NETWORK_BUF_SIZE )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int 
NetworkApp::BXmitMsg(Int socketId, XmitDataMsgId msgId, void *pData,
                     Uint32 size)
{

    CLASS_PRE_CONDITION( IsValidId(msgId) && pData );
    CLASS_PRE_CONDITION( size <= MAX_NETWORK_BUF_SIZE );

    if ( !RSettingsSocket_.opened() )
    {
        return -1;  
    }
    CLASS_ASSERTION( socketId == RSettingsSocket_.getSocketId() );
    return RSettingsSocket_.sendMsg( msgId, (char *)pData,size); 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BRecvMsg(Int socketId,  XmitDataMsgId msgId, void *pData,
// 			Uint32 maxBytes, Uint waitTime)
//
//@ Interface-Description
//    Receives a given network message type into the pData area.
//    The method waits up to 'waitTime' waiting for a specified
//    network message. If the method doesn't receive a given
//    type of network message, the method generates an assert.
//    The method returns a number of user data bytes received.
//    If the Blocked IO socket is not opened or a communication error,
//    the method returns -1. If no network input data on the Blocked
//    IO socket is received during the given 'waitTime' duration,
//    the method returns zero.
//---------------------------------------------------------------------
//@ Implementation-Description
//     There is only one Block IO socket active at one time.
//	   The passed in socket id should be same as the block IO socket id.
//     The actual socket IO is done by the readMsg method of the
//     Blocked IO socket.
//---------------------------------------------------------------------
//@ PreCondition
//    (IsValidId(msgId) && pData )
//    ( maxBytes <= MAX_NETWORK_BUF_SIZE )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int 
NetworkApp::BRecvMsg(Int socketId, XmitDataMsgId msgId, void *pData,
                     Uint32 maxBytes, Uint waitTime)
{
    fd_set readMask;
    Int nret;

    CLASS_PRE_CONDITION( IsValidId(msgId) && pData );
    CLASS_PRE_CONDITION( maxBytes <= MAX_NETWORK_BUF_SIZE );

    if ( !RSettingsSocket_.opened() )
    {
        return -1;     
    }
    CLASS_ASSERTION( RSettingsSocket_.getSocketId() == socketId );

    FD_ZERO(&readMask);
    FD_SET(RSettingsSocket_.getSocketId(), &readMask);
	if ((nret = NetworkSocket::WaitForInput(RSettingsSocket_.getSocketId()+1, &readMask, waitTime)) <= 0 ) // timeout or error
    {
        ReportCommError("NetworkApp::BRecvMsg()", NETWORKAPP_RECV_TIMEOUT);
        return nret;
    }

    return RSettingsSocket_.recvMsg(msgId, (char *)pData, maxBytes);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BroadcastMsg( XmitDataMsgId msgId, void pData, Uint size
//				PortId udpPortId )
//
//@ Interface-Description
//    Broadcasts a given network message to the network on a given
//    UDP port. During the comunication loss state, the method doesn't
//	  transmit a given network message and the network message would
//	  be lost. A network message size cannot exceed 1000 bytes.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Data is transmitted through the Broadcast socket object, which
//   is a UDP type socket. The UDP limits each message size to
//   a maximum packet size (1024).
//---------------------------------------------------------------------
//@ PreCondition
//    ( size <= MAX_BROADCAST_MSG_SIZE )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
NetworkApp::BroadcastMsg( XmitDataMsgId msgId, void* pData, Uint size,
                          PortId  udpPortId ) 
{
    CLASS_PRE_CONDITION( size <= MAX_BROADCAST_MSG_SIZE );

    return(RBroadcastSocket_.broadcastMsg(msgId, pData,
                                          size, udpPortId));  
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AcceptBlockIOConnection_(void)
//
//@ Interface-Description
//    The method accepts a socket connection request from an outside
//    network node and initializes the systems's only one Block mode
//    socket object, RSettingsSocket_ object with the newly accepted
//    TCP/IP socket connection id.  The method informs a user by calling
//    a pre-registered callback routine. The newly opened socket id is 
//    passed to the callback routine.
//    The method returns a failure status if the Block IO socket is
//    already open since only one Block IO socket connection allowed for
//    the system.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//	  If the accept socket call fails, the method gives up after four
//    repeated tries. Even when a Block IO socket is already open,
//    the method goes ahead to accept a connect request then it closes
//    immediately to send a socket close status to the other side.
//    This method should more appropriately be a member function of 
//    BlockIOSocket.
//--------------------------------------------------------------------- 
//@ PreCondition
//	 ( POpenCallback_ )
//---------------------------------------------------------------------
//@ PostCondition 
//---------------------------------------------------------------------
//@ End-Method 
//===================================================================== 

#if defined(SIGMA_BD_CPU)
SigmaStatus 
NetworkApp::AcceptBlockIOConnection_(void)
{
    // can we move this to NetworkSocket::acceptConnection with some mods?? TBD
    SAFE_CLASS_PRE_CONDITION( POpenCallback_ );

	//TODO E600 VM: create wrappers for socket calls and port th ewrappers instead
	//of the #ifdef here
	//If ported to WinCE or WIN32
#if defined(_WIN32_WCE) || defined(WIN32)

    int error;
    Int newSocket;

	//accept connection on the new socket (we do not need the returned buffer adress)
	newSocket = accept(RSettingsContactSocket_.getSocketId(), (struct sockaddr *)0, 0);
    if(newSocket == INVALID_SOCKET)
    {
		error = WSAGetLastError();
		NetworkApp::ReportCommError("NetworkSocket::acceptConnection:accept()", error);
        return FAILURE;
    }
	// in spite of the name "BlockIOConnection", make the new socket non-blocking
    ULONG nonblockingon = 1;
    if ( ioctlsocket(newSocket, FIONBIO, &nonblockingon) != 0 )
    {
		error = WSAGetLastError();
        NetworkApp::ReportCommError("NetworkSocket::acceptConnection: ioctlsocket(FIONBIO)", error);
        closesocket(newSocket);
        return FAILURE; 
    }

    if ( RSettingsSocket_.opened() )
    {
        NetworkApp::ReportCommError("NetworkApp::AcceptBlockIOConnection_(), already opened",
                                    NETWORKAPP_BLOCKIO_SOCKET_ALREADY_OPENED);
        closesocket(newSocket); 
        return FAILURE;
    }

    CLASS_ASSERTION( POpenCallback_ != NULL );

    RSettingsSocket_.reInit( newSocket );
    (*POpenCallback_)( newSocket ); 
    return SUCCESS;
#else
	return FAILURE;
#endif
}
#endif

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: XmitAndBroadcastMsg( XmitDataMsgId msgId, void pData, Uint size
//			PortId udpPortId)
//
//@ Interface-Description
//		Transmits a given network message to the other Sigma board using
//		the dedicated TCP/IP socket connection between two boards.
//      The method also broadcasts the same message to the network.
//      The given UDP port is set to be a destination port id.
//      During the communication loss state, a given network message 
//      wouldn't be transmitted. A network message data size cannot
//		exceed 1000 bytes. The method returns a number of network message
//	    data bytes successfully transmitted. The method returns a negative
//      value when it cannot successfully transmit a given network message.
//---------------------------------------------------------------------
//@ Implementation-Description
//      RSendSocket_ is a SendSocket object which handles all the
//      network messages sent to other board using the one way
//      dedicated TCP/IP socket connection. RBroadcastSocket_ is 
//      a BroadcastSocket object, which handles all the network messages
//      that are broadcasted. 
//---------------------------------------------------------------------
//@ PreCondition
//	( size <= MAX_BROADCAST_MSG_SIZE )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================

Int
NetworkApp::XmitAndBroadcastMsg( XmitDataMsgId msgId, void *pData,
                                 Uint size, PortId udpPortId ) 
{
    CLASS_PRE_CONDITION( size <= MAX_BROADCAST_MSG_SIZE );

	//NOTE: broadcastMsg and sendMsg return -1 if not open
    RBroadcastSocket_.broadcastMsg(msgId, pData,  size, udpPortId);

    if (RSendSocket_.sendMsg( msgId,(char *)pData,size ) != size )
    {
        return -1; 
    }
    else
    {
        return size;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CloseActiveConnectedSockets_()
//
//@ Interface-Description
//    Closes all the open "connected" sockets only (disregard the broadcast). 
//	  After all the sockets are closed, the communication is considered down.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Each Socket class provides a close method, it takes care of
//     closing of an open socket and other Socket  type dependent
//     cleaning up.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================

void 
NetworkApp::CloseActiveConnectedSockets_( void)
{
    SetCommDown_();   
    RRecvSocket_.down();
    RSendSocket_.down();
    RSettingsSocket_.down();
#if defined(SIGMA_BD_CPU)
    RSettingsContactSocket_.down();
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BCloseSocket(Int fd)
//
//@ Interface-Description
//		Closes the system's Blocked IO socket connection.
//      The 'socketId' should be same as the socket id of the current
//      Blocked IO socket.
//--------------------------------------------------------------------
//@ Implementation-Description
//   Close the RBlockedIOSocket_ socket object.  
//---------------------------------------------------------------------
//@ PreCondition
//   (!RSettingsSocket_.opened() &&
//			( socketId == RSettingsSocket_.getSocketId()));
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void 
NetworkApp::BCloseSocket(Int socketId)
{
    if (!RSettingsSocket_.opened())
    {
        return; 
    }
    CLASS_ASSERTION( socketId == RSettingsSocket_.getSocketId());
    RSettingsSocket_.down();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RecvOffKeepAliveMsg(XmitDataMsgId msgId, void *pPacket,
//   	Uint size)
//@ Interface-Description
//     A network message callback routine that turns off the keep
//     alive message tracking feature.
//     After the feature is turned off, the method sends back
//     a confirmation network message to the other board.
//--------------------------------------------------------------------
//@ Implementation-Description
//    This method is registered with the NetworkApp task to receive
//    a OFF_KEEP_ALIVE_MSG network message from the other board.
//---------------------------------------------------------------------
//@ PreCondition
//  (msgId == OFF_KEEP_ALIVE_MSG)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetworkApp::RecvOffKeepAliveMsg_(XmitDataMsgId msgId, void *,Uint )
{
    CLASS_PRE_CONDITION(msgId == OFF_KEEP_ALIVE_MSG);

    KeepAliveFeature_ = FALSE;
    NetworkApp::XmitMsg( CONFIRM_OFF_KEEP_ALIVE_MSG, NULL, 0);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RecvConfirmMsg_(XmitDataMsgId msgId, void *, Uint)
//@ Interface-Description
//   A callback routine that is registered to receive a confirmation
//   message from the other board. Doesn't do anything, but reguired
//   to receive a confirmation message just in case nobodyelse 
//   registers a callback method.
//    
//--------------------------------------------------------------------
//@ Implementation-Description
//    This method is registered with the NetworkApp task to receive
//    a confirmation message of the on or off keep alive tracking 
//    message sent.  The NetworkApp task generates a softfault 
//    if an arrived message doesn't have a callback routine
//    registered to receive that message type.
//	  Turn on the keep alive feature when it receives a partner's
//    confirmation message so it would not sure that a partner
//    is really ready to start the keep alive protocol.
//---------------------------------------------------------------------
//@ PreCondition
//  ((msgId == CONFIRM_ON_KEEP_ALIVE_MSG) ||
//   ( msgId ==  CONFIRM_OFF_KEEP_ALIVE_MSG ))
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetworkApp::RecvConfirmMsg_(XmitDataMsgId msgId,void *,Uint)
{
    CLASS_PRE_CONDITION ((msgId == CONFIRM_ON_KEEP_ALIVE_MSG) ||
                         ( msgId ==  CONFIRM_OFF_KEEP_ALIVE_MSG ));
    if ( msgId ==  CONFIRM_OFF_KEEP_ALIVE_MSG )
    {
        ConfirmOffKeepAlive_ = TRUE;
    }
    else
    {
        KeepAliveFeature_ = TRUE;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RecvOnKeepAliveMsg_(XmitDataMsgId msgId,void *,Uint)
//@ Interface-Description
//     A network message callback routine that turns on the keep
//     alive message tracking feature.
//     After the feature is turned on, the method sends back
//     a confirmation network message to the other board.
//    
//--------------------------------------------------------------------
//@ Implementation-Description
//    This method is registered with the NetworkApp task to receive
//    a ON_KEEP_ALIVE_MSG network message from the other board.
//---------------------------------------------------------------------
//@ PreCondition
//  (msgId == ON_KEEP_ALIVE_MSG)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetworkApp::RecvOnKeepAliveMsg_(XmitDataMsgId msgId,void *,Uint)
{
    CLASS_PRE_CONDITION(msgId == ON_KEEP_ALIVE_MSG);
    RRecvSocket_.setMsgDelayCounter(0); 
    KeepAliveFeature_ = TRUE;
    NetworkApp::XmitMsg( CONFIRM_ON_KEEP_ALIVE_MSG, NULL, 0);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TurnOffKeepAlive()
//
//@ Interface-Description
//      Turn off the keep alive message tracking feature of both
//      the GUI and BD board. A confirmation message will be sent back
//      to this board after the partner board turns off the feature.
//      In order to recevie this confirmation message, 
//  	CONFIRM_OFF_KEEP_ALIVE_MSG, A callback method for
//  	this confirmation message can be registered.
//--------------------------------------------------------------------
//@ Implementation-Description
//  	The method sends a OFF_KEEP_ALIVE_MSG message to the partner
//      for the other board to turn off the keep alive message
//      tracking feature on that board.
//	    Turn off the keep alive feature immediately instead of
//      waiting untile the partner sends a confirmation message unlike
//      the turning it on.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetworkApp::TurnOffKeepAlive()
{
    XmitMsg(OFF_KEEP_ALIVE_MSG, NULL, 0);
    KeepAliveFeature_ = FALSE;
    RRecvSocket_.setMsgDelayCounter(0);
    ConfirmOffKeepAlive_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TurnOnKeepAlive()
//
//@ Interface-Description
//   Turn on the keep alive message tracking feature of both the
//   GUI and BD boards.
//--------------------------------------------------------------------
//@ Implementation-Description
//    Send the OFF_KEEP_ALIVE_MSG message to the other board to
//    request it to turn on the keep alive protocol on the partner
//    board.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
NetworkApp::TurnOnKeepAlive()
{
    XmitMsg(ON_KEEP_ALIVE_MSG, NULL, 0);
    RRecvSocket_.setMsgDelayCounter(0); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  startupOsNetworkSockets(void) [static]
//                
//@ Interface-Description
//      This is a helper function to startup the OS network sockets
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This function is ported to the corresponding OS in use to startup
//      the network sockest if/as applies to this particular OS.
//		It returns TRUE if it finishes successuly; otherwise, FALSE
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Boolean
NetworkApp::startupOsNetworkSockets()
{
//if ported to WinCE or Windows32
#if defined(_WIN32_WCE) || defined(WIN32)
	WSADATA wsaData;
	int error;
	//The requested socket DLL version is 2.2
	const int VERSION_HIBYTE = 2;
	const int VERSION_LOBYTE = 2;
	WORD version;
	version = MAKEWORD(VERSION_HIBYTE, VERSION_LOBYTE);
	 
	error = WSAStartup( version, &wsaData );
	if ( error != 0 )
	{
		return FALSE;
	}
	 
	// Confirm that the WinSock DLL matches the requested version number
	if ( (LOBYTE( wsaData.wVersion ) != VERSION_LOBYTE) ||
			(HIBYTE( wsaData.wVersion ) != VERSION_HIBYTE) )
	{
		return FALSE; 
	}

	return TRUE;
//#elseif OTHER_OS
#else
	return FALSE;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
NetworkApp::SoftFault(const SoftFaultID  softFaultID,
                      const Uint32       lineNumber,
                      const char*        pFileName,
                      const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, NETWORK_APPLICATION, NETWORK_APP_CLASS,
                            lineNumber, pFileName, pPredicate);
}
