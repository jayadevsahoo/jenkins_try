
/*****************************************************************************/
/*   Module Name:    CPVT_DB_exports.cpp                                            
/*   Purpose:      Purpose                                 
/*                                                                          
/*   Description of Change : Selected Reports to preview or delete  functions are
/*							 added.
/*   Modified By		   : Lakshmi K.S.
/*   Modified Date         : 21th Dec.2000 
/*                                                                           
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#include "stdafx.h"

#include "daocpvtdb.h"
#include "sequenceSet.h"
#include "testrunsSet.h"
#include "InfoSet.h"
#include "OATestSet.h"
#include "LogInfoSet.h"
#include "LogHeaderSet.h"
#include "SeqSNDateSet.h"

#define DIE_IF_NO_RECORDS(x) if(x->IsEOF()) return 0;
#define DIE_IF_NULL(x)	if(x==NULL) return 0;
#define OPENRSET(x) 	TRY \
							x->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);\
						CATCH(CDaoException, pEx)\
							pEx->ReportError(); return -2;\
						END_CATCH

CLogInfoSet* LogInfo=NULL;
CSequenceSet* Sequences=NULL;
COATestSet* OATests=NULL;
CTestrunsSet* TestRuns=NULL;
CInfoSet* CPVTInfo=NULL;
CLogHeaderSet* Headers=NULL;
CSeqSNDateSet* SeqSNDate=NULL;
struct SSNdate{
				long ID;
				char UUTSN[255];
				char Date[25];
			}SNDate;


char DB_PATH_NAME[256] = "c:\\brthlab\\840vts\\database\\cpvtdb.mdb";
/*****************************************************************************/
/* Name:     AllocSeqID                                                   
/*                                                                           
/* Purpose:  This allocates a new sequence run record in the database.
/*                                          
/*                                                                           
/* Example Call:   int AllocSeqID(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ID of new Sequence Record                                                        
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int AllocSeqID(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	DIE_IF_NULL(Sequences)
	int recordID;
	OPENRSET(Sequences)
	Sequences->AddNew();
	Sequences->m_time=Sequences->m_time.GetCurrentTime();
	Sequences->Update();
	//We need to return the ID so go to the last record (that was just added. and copy
	//it before we close down.
	Sequences->MoveLast();
	recordID=Sequences->m_ID;
	Sequences->Close();
	if(recordID==0){
		Sequences->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
		Sequences->MoveLast();
		recordID=Sequences->m_ID;
		Sequences->Close();
	}
	return recordID;
}


/*****************************************************************************/
/* Name:    AddSequence                                                 
/*                                                                           
/* Purpose:  Fills in the information of a sequence run to the specified record
/*                                          
/*                                                                           
/* Example Call:   int AddSequence(char* User, int PFtext, int ID, char* UUTPN,
/*								   char* UUTBDSN, char* UUTRev, char* BDPost,
/*								   char* GUISN, char* GUIPost, char* Saas,
/*								   char* CompSN, char* CompRunTime,
/*								   char* PTSSN, char* PTSVer, char* PTSLastCal,
/*								   char* PTSNextCal, int PTSOutofCal);
/*                                    
/*                                                                           
/* Input:    User		 - 	Sequence information
/*			 PFtext		 - 	Sequence information
/*			 ID			 - 	Sequende record to add information to
/*			 UUTPN		 - 	Sequence information
/*			 UUTBDSN	 - 	Sequence information
/*			 UUTBDRunTime- 	Sequence information
/*			 UUTRev		 - 	Sequence information
/*			 BDPost		 - 	Sequence information
/*			 GUISN		 - 	Sequence information
/*			 GUIPost	 - 	Sequence information
/*			 Saas		 - 	Sequence information
/*			 CompSN		 - 	Sequence information
/*			 CompRunTime - 	Sequence information
/*			 PTSSN 		 - 	Sequence information
/*			 PTSVer 	 - 	Sequence information
/*			 PTSLastCal	 - 	Sequence information
/*			 PTSNextCal	 - 	Sequence information
/*			 PTSOutofCal - 	Sequence information
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Id of record updated, -1 if error                                                        
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int AddSequence(char* User, int PFtext, int ID, char* UUTPN, char* UUTBDSN, char* UUTBDRunTime, char* UUTRev,
												 char* BDPost, char* GUISN, char* GUIPost, char* Saas, char* CompSN, char* CompRunTime,
												 char* PTSSN, char* PTSVer, char* PTSLastCal, char* PTSNextCal, int PTSOutofCal,
												 char* VentAsset, char* WorkOrder)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Open the table, add a new record and enter the field values.
	//DON'T set the value for m_ID. This will be set automatically by the Jet engine (autonumber).
	DIE_IF_NULL(Sequences)
	OPENRSET(Sequences)
	Sequences->MoveFirst();
	while(!Sequences->IsEOF())
		if(Sequences->m_ID == ID)
			break;
		else
			Sequences->MoveNext();
	if(Sequences->m_ID != ID)
		return -1;
	Sequences->Edit();
	Sequences->m_time=Sequences->m_time.GetCurrentTime();
	Sequences->m_User = User;
	Sequences->m_P_F = PFtext;
	Sequences->m_UUTPN = UUTPN;
	Sequences->m_UUTBDSN = UUTBDSN;
	Sequences->m_UUTBDRunTime = UUTBDRunTime;
	Sequences->m_UUTRev = UUTRev;
	Sequences->m_BDPost = BDPost;
	Sequences->m_GUISN = GUISN;
	Sequences->m_GUIPost = GUIPost;
	Sequences->m_Saas = Saas;
	Sequences->m_CompSN = CompSN;
	Sequences->m_CompRunTime = CompRunTime;
	Sequences->m_PTSSN = PTSSN;
	Sequences->m_PTSVer = PTSVer;
	Sequences->m_PTSLastCal = PTSLastCal;
	Sequences->m_PTSNextCal = PTSNextCal;
	Sequences->m_PTSOutofCal = PTSOutofCal;
	Sequences->m_VentAsset = VentAsset;
	Sequences->m_WorkOrder = WorkOrder;
	Sequences->Update();
	Sequences->Close();
	return ID;
}

/*****************************************************************************/
/* Name:     AllocOATestID                                                    
/*                                                                           
/* Purpose:  This allocates a Overall test record in the database
/*                                          
/*                                                                           
/* Example Call:   int AllocOATestID(void);                                    
/*                                                                           
/* Input:    <None>
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Id of the allocated record, -1 if error.                                                       
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int AllocOATestID(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	DIE_IF_NULL(OATests)
	int recordID;
	OPENRSET(OATests)
	OATests->AddNew();
	OATests->m_Sequence=0;
	OATests->m_TName=0;
	OATests->m_TPFInd=0;
	OATests->Update();
	//We need to return the ID so go to the last record (that was just added. and copy
	//it before we close down.
	OATests->MoveLast();
	recordID=OATests->m_TID;
	OATests->Close();
	if(recordID==0){
		OATests->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
		OATests->MoveLast();
		recordID=OATests->m_TID;
		OATests->Close();
	}
	return recordID;
}

/*****************************************************************************/
/* Name:     AddOATest                                                    
/*                                                                           
/* Purpose:  Adds overall test information to the record specified
/*                                          
/*                                                                           
/* Example Call:   int AddOATest(int ID, int Type, int PF, int Sequence);                                    
/*                                                                           
/* Input:    ID		  -	record to update
/*			 Type	  -	overall test information
/*			 PF		  -	overall test information
/*			 Sequence -	parent sequence record
/*
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Id of the updated record, -1 if error. 
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int AddOATest(int ID, int Type, int PF, int Sequence)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	//Open the table, add a new record and enter the field values.
	//DON'T set the value for m_ID. This will be set automatically by the Jet engine (autonumber).
	DIE_IF_NULL(OATests)
	OPENRSET(OATests)
	OATests->MoveFirst();
	while(!OATests->IsEOF())
		if(OATests->m_TID == ID)
			break;
		else
			OATests->MoveNext();
	if(OATests->m_TID != ID)
		return -1;
	OATests->Edit();
	OATests->m_TName = Type;
	OATests->m_TPFInd = PF;
	OATests->m_Sequence = Sequence;
	OATests->Update();
	OATests->Close();
	return ID;
}

/*****************************************************************************/
/* Name:     AddTestRun                                                    
/*                                                                           
/* Purpose:  adds a record with test run information to the database
/*                                          
/*                                                                           
/* Example Call:   int AddTestRun(int PFflag, char* Fact, double Min,
/*								  double rdg, double Max, int TestID, int Type); 
/*                                                                           
/* Input:    PFflag -	test run information
/*			 Fact 	-	test run information
/*			 Min 	-	test run information
/*			 rdg 	-	test run information
/*			 Max 	-	test run information
/*			 TestID -	Parent overall test record
/*			 Type 	-	test run information
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Id of the new record, -1 if error                                                         
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int AddTestRun(int PFflag, char* Fact, double Min, double rdg, double Max, int TestID, int Type)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	DIE_IF_NULL(TestRuns)
	int recordID;
	//Open the table, add a new record and enter the field values.
	//DON'T set the value for m_ID. This will be set automatically by the Jet engine (autonumber).
	OPENRSET(TestRuns)
	TestRuns->AddNew();
	TestRuns->m_pf_flag=PFflag;
	TestRuns->m_Fact=Fact;
	TestRuns->m_MinRdg=Min;
	TestRuns->m_MaxRdg=Max;
	TestRuns->m_Rdg=rdg;
	TestRuns->m_TEST_ID=TestID;
	TestRuns->m_Type=Type;
	TestRuns->Update();
	//We need to return the ID so go to the last record (that was just added. and copy
	//it before we close down.
	TestRuns->MoveLast();
	recordID=TestRuns->m_ID;
	TestRuns->Close();
	if(recordID==0){
		TestRuns->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
		TestRuns->MoveLast();
		recordID=TestRuns->m_ID;
		TestRuns->Close();
	}
	return recordID;
}

/*****************************************************************************/
/* Name:     SetInfo                                                    
/*                                                                           
/* Purpose:  adds company information to record of info table
/*                                          
/*                                                                           
/* Example Call:   int SetInfo(char* Info1, char* Info2, char* Info3,
/*							   char* Info4, char* Info5, int CurrSeq);                                    
/*                                                                           
/* Input:    Info1   -	company information       
/*			 Info2	 -	company information
/*			 Info3	 -	company information
/*			 Info4	 -	company information
/*			 Info5	 -	company information
/*			 CurrSeq -	last sequence which was run
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   0 if success, -1 if error                                                         
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int SetInfo(char* Info1, char* Info2, char* Info3, char* Info4, char* Info5, int CurrSeq)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	DIE_IF_NULL(CPVTInfo)
	OPENRSET(CPVTInfo)
	if(CPVTInfo->CanScroll() && !CPVTInfo->IsBOF())
	{
		CPVTInfo->MoveFirst();
		while(!CPVTInfo->IsEOF())
		{
			CPVTInfo->MoveNext();
		}
	}
	
	CPVTInfo->AddNew();
	
	CPVTInfo->m_Info1=Info1;
	CPVTInfo->m_Info2=Info2;
	CPVTInfo->m_Info3=Info3;
	CPVTInfo->m_Info4=Info4;
	CPVTInfo->m_Info5=Info5;
	CPVTInfo->m_LastSeq=CurrSeq;
	CPVTInfo->Update();
	CPVTInfo->Close();
	return 0;
}

/*****************************************************************************/
/* Name:     AddLogHeader                                                    
/*                                                                           
/* Purpose:  Adds header information to the database
/*                                          
/*                                                                           
/* Example Call:   int AddLogHeader(char* FileName, char* BDSN, char* GUISN);                                    
/*                                                                           
/* Input:    FileName -	header information
/*			 BDSN	  -	header information
/*			 GUISB	  -	header information          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Id of added record, -1 on error
/*
/*	This function is not used. It was added to provide an easy way to expand the abilities
/*	of the log reporting in CPVT                                                     
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int AddLogHeader(char* FileName, char* BDSN, char* GUISN)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	DIE_IF_NULL(Headers)
	int ID;
	//Open the table, add a new record and enter the field values.
	//DON'T set the value for m_ID. This will be set automatically by the Jet engine (autonumber).
	OPENRSET(Headers)
	Headers->AddNew();
	Headers->m_FileName = FileName;
	Headers->m_BDSN = BDSN;
	Headers->m_GUISN = GUISN;
	ID=Headers->m_ID;
	Headers->Update();
	Headers->Close();
	return ID;
}

/*****************************************************************************/
/* Name:     AddLogInfo                                                    
/*                                                                           
/* Purpose:  Adds a line of log information to the database
/*                                          
/*                                                                           
/* Example Call:   int AddLogInfo(char* line, int header);                                    
/*                                                                           
/* Input:    line	-	text line from log file
/*			 header	-	parent header ID (not used)
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   ID of added record, -1 if error                                                         
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int AddLogInfo(char* line, int header)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	DIE_IF_NULL(LogInfo)
	int ID;
	//Open the table, add a new record and enter the field values.
	//DON'T set the value for m_ID. This will be set automatically by the Jet engine (autonumber).
	OPENRSET(LogInfo)
	LogInfo->AddNew();
	LogInfo->m_Line = line;
	LogInfo->m_Header = header;
	ID=LogInfo->m_ID;
	LogInfo->Update();
	LogInfo->Close();
	return ID;
}

/*****************************************************************************/
/* Name:     ClearLogInfo                                                    
/*                                                                           
/* Purpose:  Clears the log info database
/*                                          
/*                                                                           
/* Example Call:   int ClearLogInfo(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   Number of items deleted from log info                                                         
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int ClearLogInfo(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	DIE_IF_NULL(LogInfo)
	int i=0;
	OPENRSET(LogInfo)
	DIE_IF_NO_RECORDS(LogInfo)
	LogInfo->MoveFirst();
	if(LogInfo->IsEOF())
		return 0;
	while(!LogInfo->IsEOF())
	{
		LogInfo->Delete();
		LogInfo->MoveNext();
		i++;
	}
	LogInfo->Close();
	return i;	
}

/*****************************************************************************/
/* Name:     CompactDB                                                    
/*                                                                           
/* Purpose:  compacts the database
/*                                          
/*                                                                           
/* Example Call:   void CompactDB(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                        
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) void CompactDB(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	CString lpMsgBuf;
	int error;
	CDaoWorkspace* CPVTdb;
	delete LogInfo;
	delete Sequences;
	delete OATests;
	delete TestRuns;
	delete CPVTInfo;
	delete Headers;
	delete SeqSNDate;
	CPVTdb=new CDaoWorkspace;
	DeleteFile(DB_TMP_PATH_NAME);

	CPVTdb->CompactDatabase(DB_PATH_NAME, DB_TMP_PATH_NAME);
	delete CPVTdb;
	DeleteFile(DB_PATH_NAME);
	if(MoveFile(DB_TMP_PATH_NAME, DB_PATH_NAME)==0){
		//Try it one more time. If it still fails show an error
		error=MoveFile(DB_TMP_PATH_NAME, DB_PATH_NAME);
		if(error==0){
			FormatMessage( 
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				GetLastError(),
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				(LPTSTR) &lpMsgBuf,
				0,
				NULL);
			AfxMessageBox(lpMsgBuf);
		}
	}
	LogInfo		= new CLogInfoSet;
	Sequences	= new CSequenceSet;
	OATests		= new COATestSet;
	TestRuns	= new CTestrunsSet;
	CPVTInfo	= new CInfoSet;
	Headers		= new CLogHeaderSet;
	SeqSNDate   = new CSeqSNDateSet;
//	CPVTdb.RepairDatabase(DB_PATH_NAME);
	return;
}

/*****************************************************************************/
/* Name:     CloseDAOConnect                                                    
/*                                                                           
/* Purpose:  Terminates DAO. This is required to avoid an assert fail at app termination
/*                                          
/*                                                                           
/* Example Call:   void CloseDAOConnect(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                        
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) void CloseDAOConnect(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	delete LogInfo;
	delete Sequences;
	delete OATests;
	delete TestRuns;
	delete CPVTInfo;
	delete Headers;
	delete SeqSNDate;
	AfxDaoTerm();
	return;
}

/*****************************************************************************/
/* Name:     OpenDAOConnect                                                    
/*                                                                           
/* Purpose:  Initializes DAO. 
/*                                          
/*                                                                           
/* Example Call:   void OpenDAOConnect(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                        
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) void OpenDAOConnect(char* dbFileName)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if(dbFileName != NULL){
		strcpy(DB_PATH_NAME, dbFileName);
	}
	LogInfo		= new CLogInfoSet;
	Sequences	= new CSequenceSet;
	OATests		= new COATestSet;
	TestRuns	= new CTestrunsSet;
	CPVTInfo	= new CInfoSet;
	Headers		= new CLogHeaderSet;
	SeqSNDate	= new CSeqSNDateSet;	
	return;
}

extern "C" __declspec(dllexport) int ClearSequenceSet(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int i=0;
	DIE_IF_NULL(Sequences)
	Sequences->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
	DIE_IF_NO_RECORDS(Sequences)
	Sequences->MoveFirst();
	if(Sequences->IsEOF())
		return 0;
	while(!Sequences->IsEOF())
	{
		Sequences->Delete();
		Sequences->MoveNext();
		i++;
	}
	Sequences->Close();
	return i;	
}

int ClearOATestSet(void)
{
	int i=0;
	DIE_IF_NULL(OATests)
	OATests->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
	DIE_IF_NO_RECORDS(OATests)
	OATests->MoveFirst();
	if(OATests->IsEOF())
		return 0;
	while(!OATests->IsEOF())
	{
		OATests->Delete();
		OATests->MoveNext();
		i++;
	}
	OATests->Close();
	return i;	
}


int ClearTestrunsSet(void)
{
	int i=0;
	DIE_IF_NULL(TestRuns)
	TestRuns->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
	DIE_IF_NO_RECORDS(TestRuns)
	TestRuns->MoveFirst();
	if(TestRuns->IsEOF())
		return 0;
	while(!TestRuns->IsEOF())
	{
		TestRuns->Delete();
		TestRuns->MoveNext();
		i++;
	}
	TestRuns->Close();
	return i;	
}

extern "C" __declspec(dllexport) int ClearInfoSet(void)
{
	int i=0;
	DIE_IF_NULL(CPVTInfo)
	CPVTInfo->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
	DIE_IF_NO_RECORDS(CPVTInfo)
	CPVTInfo->MoveFirst();
	if(CPVTInfo->IsEOF())
		return 0;
	while(!CPVTInfo->IsEOF())
	{
		CPVTInfo->Delete();
		CPVTInfo->MoveNext();
		i++;
	}
	CPVTInfo->Close();
	return i;	
}

/*****************************************************************************/
/* Name:     ClearTables                                                    
/*                                                                           
/* Purpose:  Clears all tables in the database where test information is stored.
/*                                          
/*                                                                           
/* Example Call:   int ClearTables(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   The total number of records deleted                                                  
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int ClearTables(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int totalremoved=0;
	//Let's pack the DB to make sure everything is clear
	CompactDB();
	totalremoved+=ClearLogInfo();
	totalremoved+=ClearSequenceSet();
	totalremoved+=ClearOATestSet();
	totalremoved+=ClearTestrunsSet();
	totalremoved+=ClearInfoSet();
	//ClearLogHeaderSet(); //This table is not used so the function is not defined
	//Now we pack the DB again to remove all records marked for delete
	CompactDB();
	return totalremoved;
}

/*****************************************************************************/
/* Name:     DeleteSequence                                                   
/*                                                                           
/* Purpose:  To delete the selected records from the database.
/*                                          
/*                                                                           
/* Example Call:   int DeleteSequence(int SeqID);                                    
/*                                                                           
/* Input:    int SeqID          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   returns success or failure                                               
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) int DeleteSequence(int SeqID)
{
	int i=0, status;
	DIE_IF_NULL(OATests)
	OATests->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
	DIE_IF_NO_RECORDS(OATests)
	
	DIE_IF_NULL(TestRuns)
	TestRuns->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
	DIE_IF_NO_RECORDS(TestRuns)
	
	DIE_IF_NULL(Sequences)
	Sequences->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
	DIE_IF_NO_RECORDS(Sequences)

	OATests->MoveFirst();
	if(OATests->IsEOF())
		return 0;
	
	//delete records from OATests table and TestRuns table.
	for(OATests->MoveFirst(); !OATests->IsEOF();OATests->MoveNext())
	{
		if(OATests->m_Sequence==SeqID)
		{
			for(TestRuns->MoveFirst(); !TestRuns->IsEOF();TestRuns->MoveNext())
			{
				if(TestRuns->m_TEST_ID==OATests->m_TID)
					TestRuns->Delete();
			}
			OATests->Delete();
		}
	}

	// to delete the contents of cpvtinfo table if exists
	status=ClearInfoSet();
	//delete record from sequence table
	for(Sequences->MoveFirst();!Sequences->IsEOF(); Sequences->MoveNext())
	{
		if(Sequences->m_ID==SeqID)
		{
			Sequences->Delete();
			break;
		}

	}

	TestRuns->Close();
	OATests->Close();
	Sequences->Close();
	return 1;	
}

/*****************************************************************************/
/* Name:     SelectSeqID                                                   
/*                                                                           
/* Purpose:  To select the record for the given record id from the Database
/*                                          
/*                                                                           
/* Example Call:   struct SSNdate SelectSeqID(int RecID);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   returns the struct SSNdate containing record information                                              
/*                                                                           
/*****************************************************************************/
extern "C" __declspec(dllexport) struct SSNdate SelectSeqID(int RecID)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	int recordID,i,RecCnt=0,CurrentRec;
	int Day,month,year,hour,min,sec;
	char cDate[10];
	
	SNDate.ID=0;
	strcpy(SNDate.Date," ");
	strcpy(SNDate.UUTSN,"");
	if(SeqSNDate==NULL) return SNDate;
 	TRY 
		SeqSNDate->Open(AFX_DB_USE_DEFAULT_TYPE, NULL, NULL);
	CATCH(CDaoException, pEx)
		SNDate.ID=-2;
		strcpy(SNDate.Date," ");
		strcpy(SNDate.UUTSN,"");
		pEx->ReportError(); return SNDate;
	END_CATCH
	
	recordID=SeqSNDate->GetRecordCount();
	CurrentRec=recordID-RecID;
	if(CurrentRec >= 0)
	{
	 	SeqSNDate->MoveFirst();
		for(i=0; i<CurrentRec; i++)
		{
			SeqSNDate->MoveNext();
		}
		
		SNDate.ID=SeqSNDate->m_ID;
		strcpy(SNDate.UUTSN,SeqSNDate->m_UUTBDSN);
		Day=SeqSNDate->m_time.GetDay();
		month=SeqSNDate->m_time.GetMonth();
		year=SeqSNDate->m_time.GetYear();
		hour=SeqSNDate->m_time.GetHour();
		min=SeqSNDate->m_time.GetMinute();
		sec=SeqSNDate->m_time.GetSecond();
		sprintf(cDate, "%d", month);
		strcat(cDate,"/");
		strcpy(SNDate.Date,cDate);
		sprintf(cDate, "%d",Day);
		strcat(cDate,"/");
		strcat(SNDate.Date,cDate);
		sprintf(cDate, "%4d", year);
		strcat(SNDate.Date,cDate);
		strcat(SNDate.Date," ");
		sprintf(cDate, "%d", hour);
		strcat(SNDate.Date,cDate);
		strcat(SNDate.Date,":");
		sprintf(cDate, "%d", min);
		strcat(SNDate.Date,cDate);
		strcat(SNDate.Date,":");
		sprintf(cDate, "%d", sec);
		strcat(SNDate.Date,cDate);
		
		SeqSNDate->Close();
		return SNDate; 
    }
	else
    {
		SNDate.ID=0;
		strcpy(SNDate.UUTSN,"");
		strcpy(SNDate.Date,"");
		SeqSNDate->Close();
		return SNDate;
	}	
	
}

