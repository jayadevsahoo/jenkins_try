#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SmFlowController - Flow controller for Service Mode
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is used to control gas flow during the Service Mode
//	tests.  It identical to the Breath-Delivery flow controller,
//	except that the integral gain is adjusted for the Service Mode
//	cycle time.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the methods for flow control for the
//	Service Mode tests.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method establishFlow() is used to start the flow controller,
//	stopFlowController() stops the controller, and newCycle() executes
//	the flow control algorithm.
//	NOTE: as a Service Mode utility class, there is no direct SRS
//	traceability.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only two instances of this class are allowed: one instance of a
//	flow controller object and one instance of a pressure control
//	object (which uses SmFlowController as a base class).
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmFlowController.ccv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		Made getFlowTarget() inline
//
//  Revision: 006  By: dosman    Date: 15-Jul-1999     DR Number: 5478
//  Project:  ATC
//  Description:
//	Changed flow stability criterion to be different depending
//	on whether compressor is running or not.
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  syw    Date:  03-Jul-97    DR Number: DCS 2286
//       Project:  Sigma (R8027)
//       Description:
//       	Changed SM_INTEGRAL_GAIN to 0.310 ;
//
//  Revision: 003  By:  syw    Date:  03-Jul-97    DR Number: DCS 2279
//       Project:  Sigma (R8027)
//       Description:
//       	Prevent resetting the flow controller if the controller is already
//			active.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//            Removed use of "this" pointer to determine flow control
//            owner, use an ID (initialized during construction) instead.
//            Changed stableFlowHighBound_, stableFlowLowBound_,
//            flowBecameUnstable_, and flowInstabilityCounts_ to
//            static variables to avoid object size change in
//            production vs development builds.
//
//  Revision: 001  By:  mad    Date:  23-Aug-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "SmFlowController.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmGasSupply.hh"
#include "SmUtilities.hh"
#include "SmManager.hh"

#include "Task.hh"

#include "MainSensorRefs.hh"
#include "ValveRefs.hh"
#include "BD_IO_Devices.hh"

#ifdef SIGMA_DEBUG
#include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# define RSmGasSupply fakeSmGasSupply
# include "FakeSmGasSupply.hh"
extern FakeSmGasSupply fakeSmGasSupply;
#endif  // SIGMA_UNIT_TEST

//@ End-Usage


//@ Code...

SmFlowController::FlowControlOwnerId
	SmFlowController::FlowControlOwner_ = SmFlowController::NONE;

//@ Constant: MIN_CYCLES_FOR_STABILITY
// # of cycles that flow must be within bounds before stability
// is declared
const Int32 MIN_CYCLES_FOR_STABILITY = 20;  // # of iterations

//@ Constant: FLOW_STABLE_BOUND_WITH_COMPRESSOR_ABS
// minimum absolute flow error for stability with compressor
const Real32 FLOW_STABLE_BOUND_WITH_COMPRESSOR_ABS = 1.2;  // lpm, absolute error

//@ Constant: FLOW_STABLE_BOUND_WITHOUT_COMPRESSOR_ABS
// minimum absolute flow error for stability without compressor
const Real32 FLOW_STABLE_BOUND_WITHOUT_COMPRESSOR_ABS = 0.5;  // lpm, absolute error

//@ Constant: FLOW_STABLE_BOUND_REL
// minimum relative flow error for stability
const Real32 FLOW_STABLE_BOUND_REL = 0.05;  // relative error

//@ Constant: FEEDFORWARD_GAIN
// flow controller feedforward gain
const Real32 FEEDFORWARD_GAIN = 12.0;

//@ Constant: SM_INTEGRAL_GAIN
// flow controller integral gain for Service Mode
const Real32 SM_INTEGRAL_GAIN = 0.310 ;


#ifdef SIGMA_DEVELOPMENT
Real32 SmFlowController::stableFlowHighBound_ = 0.0;
Real32 SmFlowController::stableFlowLowBound_ = 999.0;
Boolean SmFlowController::flowBecameUnstable_ = FALSE;
Int32 SmFlowController::flowInstabilityCounts_ = 0;
#endif  // SIGMA_DEVELOPMENT


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmFlowController()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//	Initializes private data members.  No arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize private data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmFlowController::SmFlowController(void) :
    requestForFlowControl_(FALSE),
    flowControllerActive_(FALSE),
	establishingFlow_(FALSE),
    flowStable_( FALSE),
	keepFlowOn_( FALSE),
	gasUsed_( AIR),
    flowTarget_(0.0),
    startupGain_(0.0),
    flowError_(0.0),
	flowCount_(0),
    pFlowSensor_(&RAirFlowSensor),
    pPsol_(&RAirPsol)
{
// $[TI1]
	CALL_TRACE("SmFlowController::SmFlowController(void)");

	flowControlId_ = FLOW;

#ifdef SIGMA_DEBUG
	printf("flowControlId_ = %d\n", flowControlId_);
#endif  // SIGMA_DEBUG
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmFlowController()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmFlowController::~SmFlowController(void)
{
	CALL_TRACE("~SmFlowController()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: establishFlow()
//
//@ Interface-Description
//	This method is used to activate the flow controller.  Three arguments:
//	desired flow rate, flow sensor side to use for control (inspiratory
//	or expiratory), and gas type desired (air, O2, or either).  No return
//	value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize flow controller parameters to start flow control.
//	If gas type == EITHER, select gas using algorithm in SmUtilities.
//---------------------------------------------------------------------
//@ PreCondition
//
//	flowControlId_ == FLOW or PRESSURE
//
//	The "owner" of the flow controller can only be NONE
//  (no flow control currently), FLOW (the flow controller object
//  is controlling flow), or PRESSURE (the pressure controller object
//	is controlling flow).
//
//	flow control measurement side = DELIVERY or EXHALATION
//	gas type = AIR, O2, and EITHER.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmFlowController::establishFlow( Real32          desiredFlow,
                                 MeasurementSide desiredSide,
                                 GasType         desiredGas )
{
	CALL_TRACE("SmFlowController::establishFlow( Real32 desiredFlow, \
                                 MeasurementSide desiredSide, \
                                 GasType         desiredGas)");

	CLASS_PRE_CONDITION (
				flowControlId_ == FLOW ||
				flowControlId_ == PRESSURE );

	CLASS_PRE_CONDITION ( FlowControlOwner_ == NONE ||
						  FlowControlOwner_ == FLOW ||
						  FlowControlOwner_ == PRESSURE );

	CLASS_PRE_CONDITION ( desiredSide == DELIVERY ||
						  desiredSide == EXHALATION );

	CLASS_PRE_CONDITION ( desiredGas == AIR ||
						  desiredGas == O2 ||
						  desiredGas == EITHER );

#ifdef SIGMA_DEBUG
//	printf("Kff = %4.1f, Ki = %6.3f\n", FEEDFORWARD_GAIN, SM_INTEGRAL_GAIN);
//	printf("CYCLE_TIME_MS = %u, SM_CYCLE_TIME = %u\n", CYCLE_TIME_MS, SM_CYCLE_TIME);
#endif   // SIGMA_DEBUG

	// flag newCycle() to skip 1 iteration if there's an existing flow
	// to avoid a race
	establishingFlow_ = TRUE;

	// set flow control variables for the new flow
	flowStable_ = FALSE;
	flowCount_ = 0;

	if (!requestForFlowControl_)
	{
	// $[TI7]
		flowError_ = 0.0;
	    startupGain_ = 0.0;
	}
	// $[TI8]
	// implied else

	flowTarget_ = desiredFlow;

#ifdef SIGMA_DEVELOPMENT
	stableFlowHighBound_ = desiredFlow;
	stableFlowLowBound_ = desiredFlow;
	flowBecameUnstable_ = FALSE;
	flowInstabilityCounts_ = 0;
#endif  // SIGMA_DEVELOPMENT

	// Ensure flow controller and pressure controller are not both active
	if (flowControlId_ == FLOW)
	{
	// $[TI1]
		// if method was called by the flow controller object, make sure
		// pressure controller is NOT the current owner
		CLASS_ASSERTION ( FlowControlOwner_ != PRESSURE );

		// set current flow control owner to flow controller
		FlowControlOwner_ = FLOW;
	}
	else
	{
	// $[TI2]
		// if method was called by the pressure controller object, make sure
		// flow controller is NOT the current owner
		CLASS_ASSERTION ( FlowControlOwner_ != FLOW );

		// set current flow control owner to pressure controller
		FlowControlOwner_ = PRESSURE;
	}
	// end else

	// If either gas can be used, select one
	if (desiredGas == EITHER)
  	{
	// $[TI3]
		desiredGas = RSmUtilities.selectGasType();
  	}
	// $[TI4]
	// implied else: a specific gas type was requested

	// store the selected gas type
	gasUsed_ = desiredGas;

  	if (desiredGas == AIR)
  	{
	// $[TI5]
		// gas type = air
    	pPsol_ = &RAirPsol;

    	if (desiredSide == DELIVERY)
    	{
		// $[TI5.1]
			// air, delivery control
      		pFlowSensor_ = &RAirFlowSensor;
    	}
    	else
    	{
		// $[TI5.2]
			// air, exhalation control
#ifdef SIGMA_UNIT_TEST
     		pFlowSensor_ = &RExhFlowSensor;
#else
     		pFlowSensor_ = (FlowSensor*)&RExhFlowSensor;
#endif  // SIGMA_UNIT_TEST
    	}
		// end else
  	}
  	else
  	{
	// $[TI6]
		// gas type = O2
    	pPsol_ = &RO2Psol;

    	if (desiredSide == DELIVERY)
    	{
		// $[TI6.1]
			// O2, delivery control
      		pFlowSensor_ = &RO2FlowSensor;
    	}
    	else
    	{
		// $[TI6.2]
			// O2, exhalation control
// E600 BDIO
#if 0
#ifdef SIGMA_UNIT_TEST
     		pFlowSensor_ = &RExhO2FlowSensor;
#else
     		pFlowSensor_ = (FlowSensor *)&RExhO2FlowSensor;
#endif  // SIGMA_UNIT_TEST
#endif
#ifdef SIGMA_UNIT_TEST
            pFlowSensor_ = &RExhFlowSensor;
#else
            pFlowSensor_ = (FlowSensor *)&RExhFlowSensor;
#endif  // SIGMA_UNIT_TEST

    	}
		// end else
  	}
	// end else

	// do these last to prevent race condition
	establishingFlow_ = FALSE;
  	requestForFlowControl_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: stopFlowController()
//
//@ Interface-Description
//  This method is used to stop the flow controller.  One argument:
//	Boolean flag indicating if flow is to be kept on after stopping
//	the controller (default = FALSE).  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set controllers flags to deactivate flow control.
//	Wait for flow controller to deactivate via newCycle().
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmFlowController::stopFlowController( Boolean keepFlowOn)
{
  	CALL_TRACE("SmFlowController::stopFlowController( Boolean keepFlowOn)");

	// Tell the flow controller to keep the PSOL at its last command
	// instead of setting it to 0.
	keepFlowOn_ = keepFlowOn;

  	// Tell the flow controller to stop controlling.
  	requestForFlowControl_ = FALSE;

  	// Wait for the flow controller to release control
  	while (flowControllerActive_)
	{
	// $[TI1]
		Task::Delay(0, SM_CYCLE_TIME);

#ifdef SIGMA_UNIT_TEST
		// Since the service mode background task is not running,
		// we need to call newCycle() here to test this loop,
		// otherwise there's no way to exit the loop!
		newCycle();
#endif  // SIGMA_UNIT_TEST

	}
	// end while

	// set flow control owner to none
	FlowControlOwner_ = NONE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//	This method implements the flow control algorithm.
//	No arguments and no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If flow control is requested, set the flow control active flag TRUE
//	and use a PI control algorithm to control the target flow.  When
//	flow is within the defined bounds of the flow target for the required
//	number of cycles, set the flow stability flag TRUE.
//
//	If flow control is not requested and flow controller is active,
//	either turn the controlled PSOL off or keep it at its last value,
//	as indicated by the argument of stopFlowController() (default =
//	set PSOL to 0), and set the flow controller acitve flag FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
SmFlowController::newCycle(void)
{
	CALL_TRACE("SmFlowController::newCycle(void)");

	static Real32 currentFlow;
	static Real32 error;
	static DacCounts psolCommand;

	if (requestForFlowControl_)
	{
	// $[TI1]
		// Foreground task requested flow control

		if (!establishingFlow_)
		{
		// $[TI1.1]
			// Not establishing a flow in the foreground task, so
			// it's safe to proceed

			// Set the controller active indicator
			flowControllerActive_ = TRUE;

			// read flow and calculate flow error
			currentFlow = pFlowSensor_->getValue();
			error = flowTarget_ - currentFlow;

#ifdef SIGMA_DEVELOPMENT
			if (flowStable_)
			{
				if (currentFlow > stableFlowHighBound_)
					stableFlowHighBound_ = currentFlow;
				else if (currentFlow < stableFlowLowBound_)
					stableFlowLowBound_ = currentFlow;
			}
#endif  // SIGMA_DEVELOPMENT

			// The absolute minimum flow value for determining flow stability
			// is different depending on whether or not
			// compressor air and wall air are present.

			Real32 flowStableBoundAbs;
			if ( (RSmGasSupply.isCompressorAirPresent()) && (!RSmGasSupply.isWallAirPresent()) )
			{
				// $[TI1.1.6]
				flowStableBoundAbs = FLOW_STABLE_BOUND_WITH_COMPRESSOR_ABS;
			}
			else
			{
				// $[TI1.1.7]
				flowStableBoundAbs = FLOW_STABLE_BOUND_WITHOUT_COMPRESSOR_ABS;
			}
	
			// check for flow stability
			if ( (ABS_VALUE(error) < FLOW_STABLE_BOUND_REL * flowTarget_) ||
			                                    (ABS_VALUE(error) < flowStableBoundAbs) )
			{
				// $[TI1.1.1]
				// flow is within error bounds this cycle
				if (++flowCount_ >= MIN_CYCLES_FOR_STABILITY)
				{
				// $[TI1.1.1.1]
					// flow is within error bounds for the entire
					// stability check period
					flowStable_ = TRUE;
				}
				// $[TI1.1.1.2]
				// implied else: flow is NOT stable yet
			}
			else
			{
			// $[TI1.1.2]
				// flow error is too large, so reset stability counter
				flowCount_ = 0;

#ifdef SIGMA_DEVELOPMENT
				// check if flow became unstable after stability criteria
				// was reached initially
				if (flowStable_)
				{
					flowBecameUnstable_ = TRUE;
					flowInstabilityCounts_++;
				}
#endif  // SIGMA_DEVELOPMENT

			}
			// end else

			// Integrate the flow error
			// startupGain_ = 0 in first iteration to prevent
			// the first error from being integrated
			flowError_ += SM_CYCLE_TIME * startupGain_ * error;

			// Enable startup gain to allow flow error integration
			// after first iteration
			startupGain_ = 1.0f;

			// Calculate PSOL command
			psolCommand = (DacCounts)
				(FEEDFORWARD_GAIN * flowTarget_ + pPsol_->getLiftoff() +
											SM_INTEGRAL_GAIN * flowError_);

			// Ensure the PSOL command is with its limits
			if (psolCommand < 0)
			{
			// $[TI1.1.3]
				psolCommand = 0;

				// Limit the error integrator also
				flowError_ = -(FEEDFORWARD_GAIN * flowTarget_
								+ pPsol_->getLiftoff() ) / SM_INTEGRAL_GAIN;

		    }
			else if(psolCommand > MAX_COUNT_VALUE)
			{
			// $[TI1.1.4]
				psolCommand = MAX_COUNT_VALUE;

				// Limit the error integrator also
				flowError_ = (MAX_COUNT_VALUE - FEEDFORWARD_GAIN * flowTarget_
									- pPsol_->getLiftoff() ) / SM_INTEGRAL_GAIN;
		    }

			// $[TI1.1.5]
			// implied else: psol command is within limits

		    // Update the Psol with the new Psol command
		    pPsol_->updatePsol(psolCommand);
		}
		// $[TI1.2]
		// implied else: foreground task is setting up a new flow
		// while the background task is controlling an existing flow,
		// so skip one flow controller iteration, otherwise there's
		// a race condition
	}
	else
	{
	// $[TI2]
		// Foreground task requested flow control to stop

	    if (flowControllerActive_)
	    {
		// $[TI2.1]
			// If controller is active, deactivate it

			// Set PSOL to 0 or keep at at its last value
			if (!keepFlowOn_)
			{
			// $[TI2.1.1]
				// Turn off the PSOL
				pPsol_->updatePsol(0);
			}
			else
			{
			// $[TI2.1.2]
				// Don't turn off the psol.  User
				// will be responsible for setting
				// the PSOL back to 0.

				// Set flag back to default state
				keepFlowOn_ = FALSE;
			}
			// end else

			// Clear the active indicator
			flowControllerActive_ = FALSE;
	    }
		// $[TI2.2]
		// implied else
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: waitForStableFlow()
//
//@ Interface-Description
//	This method is used to wait until either the flow controller
//	reaches stability or until the defined timeout is reached.
//	It takes a timeout argument (in msec) and returns a Boolean
//	indicating if flow stability was reached within the timeout.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Reset the flow stability variables, then wait until the flow stability
//	flag turns TRUE or until the timeout occurs.  Return the state of the
//	flow stability flag.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
SmFlowController::waitForStableFlow(Int32 timeoutMS)
{
	CALL_TRACE("SmFlowController::waitForStableFlow(Int32 timeoutMS)");

	// reset flow stability variables
	flowStable_ = FALSE;
	flowCount_ = 0;

	Int32 timer = 0;
	do
	{
	// $[TI1]
#ifndef SIGMA_UNIT_TEST
		// Don't delay here during unit test for greater speed
		Task::Delay (0, SM_CYCLE_TIME);
#endif  // SIGMA_UNIT_TEST

		timer += SM_CYCLE_TIME;

#ifdef SIGMA_UNIT_TEST
		// Since the service mode background task is not running,
		// we need to call newCycle() here to test this loop,
		// otherwise there's no way to exit the loop!
		newCycle();
#endif  // SIGMA_UNIT_TEST

	// TODO E600_LL: for now, do not care about SmManager since the calibration
	// could be hacked and come from BDIOTest
	}  while( !flowStable_ && timer < timeoutMS /*&& !SmManager::CommDied()*/ );

	return (flowStable_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
SmFlowController::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SMFLOWCONTROLLER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
