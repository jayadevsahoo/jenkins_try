#ifndef TextUtil_HH
#define TextUtil_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TextUtil - Low-level text utility routines for word-wrapping, etc.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TextUtil.hhv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 001  By:  mpm    Date:  26-SEP-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "TextField.hh"
#include "TextFont.hh"
#include "GuiAppClassIds.hh"
class TimeStamp;
//@ End-Usage

class TextUtil
{
public:
	static void Initialize(void);

	static Int32 WordWrap(
					wchar_t *pString, wchar_t* pResultString1, wchar_t* pResultString2,
					TextFont::Style fontStyle, TextFont::Size fontSize,
					Int32 areaWidth, Int32 areaHeight, Alignment alignment,
					Int32 margin);

	 
	static void FormatTime(wchar_t *pResultString, const wchar_t *pFormat,
													const TimeStamp &timeStamp);
	 
	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	TextUtil(void);					// not implemented...
	~TextUtil(void);					// not implemented...
	TextUtil(const TextUtil&);	// not implemented...
	void operator=(const TextUtil&);	// not implemented...

	//@ Data-Member: Buf_
	// General-purpose buffer for message tokens (normal C strings, not
	// Cheap Text).
	static wchar_t Buf_[1000];

	//@ Data-Member: CheapText_
	// Buffer for holding a single line's worth of Cheap Text.
	static wchar_t CheapText_[500];

	//@ Data-Member: rTextField_
	// A reference to a text field used for checking the width and height
	// of cheap text strings so that they can be auto-positioned.
	static TextField &RTextField_;

	//@ Data-Member: textFieldMemory_
	// The TextField reference by rTextField_ is created in this memory area.
	static Int TextFieldMemory_[(sizeof(TextField) + sizeof(Int) - 1)
															/ sizeof(Int)];
};

#endif // TextUtil_HH 
