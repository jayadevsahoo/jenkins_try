#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: GuiEventRegistrar - The central registration, control, and
// dispatch point for all GUI event change events.
//---------------------------------------------------------------------
//@ Interface-Description
// Objects which handle GUI event change events use the RegisterTarget() method
// to register for a callback.  The callback is the virtual method
// GuiEventTarget::guiEventHappened() -- therefore all objects which need to
// receive GUI event change callbacks are derived from the GuiEventTarget
// class.
//
// When a GUI event change is detected this registrar is informed via
// GuiEventHappened() which will then look after informing all interested
// parties.
//---------------------------------------------------------------------
//@ Rationale
// This class is necessary to register and centrally dispatch GUI event change
// events which are typically received from the Sys-Init subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
// Only one instance of this class is created.  The class is implemented
// with the TargetArray_[] as the centerpiece.  This is an array of
// pointers to GuiEventTarget objects.  As objects register via
// RegisterTarget() a pointer to the object is added to TargetArray_[].
//
// GUI event change events are communicated to the registrar via the
// GuiEventHappened() method.  This then calls the guiEventHappened() of
// all registered objects.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// There is a limit to the number of objects that can register with
// GuiEventRegistrar.  Currently this is set to 50 but this can be increased
// if needed.  "Un-registering" is not allowed, if you register for GUI
// event changes you stay registered forever!
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiEventRegistrar.ccv   25.0.4.0   19 Nov 2013 14:07:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  29-AUG-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "GuiEventRegistrar.hh"
#if defined FORNOW
#include <stdio.h>
#endif // FORNOW


//@ Usage-Classes
#include "GuiEventTarget.hh"
//@ End-Usage

//=====================================================================
//
//		Static Data...
//
//=====================================================================

GuiEventTarget *GuiEventRegistrar::TargetArray_[];
Int32 GuiEventRegistrar::NumTargets_ = 0;


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiEventHappened
//
//@ Interface-Description
// Called due to a change in a GUI-App event.  It is passed the id of
// the event.  The objects registered with this class are then informed
// of the event change.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call all objects registered in the TargetArray_[] and pass them the
// id of the event.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiEventRegistrar::GuiEventHappened(GuiApp::GuiAppEvent eventId)
{
	CALL_TRACE("GuiEventRegistrar::GuiEventHappened(GuiApp::GuiAppEvent eventId)");

#if defined FORNOW
printf("GuiEventRegistrar::GuiEventHappened: eventId=%d\n", eventId);
#endif // FORNOW

	// Walk the 'TargetArray_[]' array and call the guiEventHappened()
	// method on each target.
	for (Uint16 i = 0; i < NumTargets_; i++)
	{													// $[TI1]
		TargetArray_[i]->guiEventHappened(eventId);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterTarget [public, static]
//
//@ Interface-Description
// Objects needing to know when GUI event change events occur use this method
// to register themselves for notices.  One parameter is needed, a
// GuiEventTarget pointer to the object.
//---------------------------------------------------------------------
//@ Implementation-Description
// The target pointer is stored in TargetArray_[].  Increment the count of
// registerated target.
//---------------------------------------------------------------------
//@ PreCondition
// The target pointer must be non-null and there must be space to store it in
// the array.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiEventRegistrar::RegisterTarget(GuiEventTarget* pTarget)
{
	CALL_TRACE("GuiEventRegistrar::RegisterTarget(GuiEventTarget* pTarget)");

	CLASS_PRE_CONDITION(pTarget != NULL);
	CLASS_PRE_CONDITION(NumTargets_ < MAX_TARGETS_);

	// Store the pointer	
	TargetArray_[NumTargets_] = pTarget;
	NumTargets_++;										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
GuiEventRegistrar::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("GuiEventRegistrar::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, GUIEVENTREGISTRAR,
							lineNumber, pFileName, pPredicate);
}
