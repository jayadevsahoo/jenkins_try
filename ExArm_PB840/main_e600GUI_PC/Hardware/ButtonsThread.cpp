#include "stdafx.h"
//*****************************************************************************
//
//  ButtonsThread.cpp - Implementation of the ButtonsThread_t class.
//
//*****************************************************************************

#ifndef __SPLIT__GUI__PC__
#include <windows.h>
#endif
#include "ButtonsThread.h"
#include "GlobalObjects.h"
#include "Resource.h"
#include "TopBaseWindow.h"
#include "ExceptionHandler.h"


static const int MIN_BUTTON_PRESSSED_COUNT = 2;
static const int MIN_BUTTON_RELEASED_COUNT = 2;

static const ULONG BUTTON_READ_INTERVAL    =   19;

static UINT ButtonIds[NUMBER_OF_MEMBRANE_BUTTONS];

//*****************************************************************************
//
//  ButtonsThread_t::ButtonsThread_t - ButtonsThread_t constructor
//
//*****************************************************************************

ButtonsThread_t::ButtonsThread_t()
{
    int i;
	Running=true;

    for (i=0; i<NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS; i++)
    {
        ButtonPressedCounter[i] = 0;
        ButtonReleasedCounter[i] = MIN_BUTTON_RELEASED_COUNT;
        LogicalButtonPressed[i] = false;
    }
    for (i=0; i<NUMBER_OF_MEMBRANE_BUTTONS; i++)
    {
        WakeupButtonsPressed[i] = 0;
    }

	//ButtonIds[0]=
    ButtonIds[MEMBRANE_BUTTON_ON_OFF]		=IDC_BTN_POWER;
    ButtonIds[MEMBRANE_BUTTON_UP]			=IDC_BTN_UP;
    ButtonIds[MEMBRANE_BUTTON_DIMNESS]		=IDC_BTN_BRIGHTNESS;
    ButtonIds[MEMBRANE_BUTTON_ACCEPT]		=IDC_BTN_ACCEPT;
    ButtonIds[MEMBRANE_BUTTON_MAN_BREATH]	=IDC_BTN_MANUAL_INFLATION;
    ButtonIds[MEMBRANE_BUTTON_SILENCE_RESET]=IDC_BTN_ALARM_SILENCE;
    ButtonIds[MEMBRANE_BUTTON_CANCEL]		=IDC_BTN_CANCEL;
    ButtonIds[MEMBRANE_BUTTON_DOWN]			=IDC_BTN_DOWN;
    ButtonIds[MEMBRANE_BUTTON_STORE_BMP]	=0;// This is a combination on UP and DOWN

	}


//*****************************************************************************
//
//  ButtonsThread_t::~ButtonsThread_t - ButtonsThread_t destructor
//
//*****************************************************************************

ButtonsThread_t::~ButtonsThread_t()
{
}


//*****************************************************************************
//
//  ButtonsThread_t::GetButtonsPressed
//
//*****************************************************************************

void ButtonsThread_t::GetButtonsPressed(bool *Pressed, uint8_t Size)
{

CButton	*cButtPtr;
UINT	State;

int i;
	State = BST_UNCHECKED;
    for (i=0; i<Size; i++)
    {
        //Pressed[i] = LogicalButtonPressed[i];


		if (ButtonIds[i]>0)
			{
			cButtPtr = (CButton *)TopBaseWindow->GetDlgItem(ButtonIds[i]);
			if (cButtPtr)
			{
			State = cButtPtr->GetState();
			State = State & BST_PUSHED;
			}
			if (State && cButtPtr)
				Pressed[i]=1;
			else
				Pressed[i]=0;
			}
		else
			Pressed[i]=0;

		}
}


//*****************************************************************************
//
//  ButtonsThread_t::ThreadProcedure
//
//*****************************************************************************

uint32_t ButtonsThread_t::ThreadProcedure(void *ProcedureArg)
{
#ifndef __SPLIT__GUI__
    DEBUG_ASSERT(SetEvent(ThreadReadyEvent));                           // Let other threads know we're ready to go

    bool HardwareButtons[NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS];

    while (!AioDioDriver.AioDioInitComplete())
    {
        Sleep(5);
    }

    AioDioDriver.ReadButtons(HardwareButtons);

	// For the first NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS, MEMBRANE_BUTTON_xx are equal to PHYSICAL_MEMBRANE_BUTTON_xx

    int i;
    for (i=0; i<NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS; i++)
    {
        WakeupButtonsPressed[i]    = HardwareButtons[i];
    }

    WakeupButtonsPressed[MEMBRANE_BUTTON_STORE_BMP]       = false;
    LogicalButtonPressed[MEMBRANE_BUTTON_STORE_BMP]       = false;

    while (true)
    {
        Sleep(BUTTON_READ_INTERVAL);

        AioDioDriver.ReadButtons(HardwareButtons);

        for (i=0; i<NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS; i++)
        {
            if (HardwareButtons[i])                     // is the button currently active?
            {
                if (!DebouncedPhysicalButtonPressed[i])
                {
                    if (ButtonReleasedCounter[i] >= MIN_BUTTON_RELEASED_COUNT)
                    {
                        if (++ButtonPressedCounter[i] > MIN_BUTTON_PRESSSED_COUNT)
                        {
                            DebouncedPhysicalButtonPressed[i] = true;
                            ButtonReleasedCounter[i] = 0;
                        }
                    }
                }
            }
            else                                        // button is not active
            {
                if (DebouncedPhysicalButtonPressed[i])
                {
                    if (++ButtonReleasedCounter[i] >= MIN_BUTTON_RELEASED_COUNT)
                    {
                        DebouncedPhysicalButtonPressed[i] = false;
                        ButtonPressedCounter[i] = 0;
                    }
                }
            }

            LogicalButtonPressed[i] = DebouncedPhysicalButtonPressed[i];
        }

// This stuff isn't normally used. Definitely not used in production!
#if DUMP_REAL_TIME_DATA || ALLOW_SAVE_WINDOWS
        // Up and down together is the "store BMP" command, not the individual commands
        if (LogicalButtonPressed[MEMBRANE_BUTTON_UP] && LogicalButtonPressed[MEMBRANE_BUTTON_DOWN])
        {
            LogicalButtonPressed[MEMBRANE_BUTTON_UP] = false;
            LogicalButtonPressed[MEMBRANE_BUTTON_DOWN] = false;
            LogicalButtonPressed[MEMBRANE_BUTTON_STORE_BMP]    = true;
        }
        else
        {
            LogicalButtonPressed[MEMBRANE_BUTTON_STORE_BMP]    = false;
        }
#endif

        ServosThread.SetThreadActive(THREAD_ID_SMARTIO);
    }
#endif
    return 0;
}



//*****************************************************************************
//
//  ThreadStartUp - This function gives the Win32 function ::CreateThread an
//      address to be passed as the startup location.
//
//*****************************************************************************

static DWORD WINAPI ThreadStartUp(PVOID ThreadArg)
{
    uint32_t ThreadResult;

    __try
    {
        ThreadResult = ButtonsThread.ThreadProcedure(ThreadArg);
    }
    __except(ExceptionHandler.EvaluateException(GetExceptionInformation()))
    {
        // Get exception info
        int32_t Code = _exception_code();

        // Call exception handler
        ExceptionHandler.VentilateWithUi(Code, "Buttonsthread");
    }

    return ThreadResult;

}


//*****************************************************************************
//
//  ButtonsThread_t::CreateThread - This function creates the thread
//
//*****************************************************************************

void ButtonsThread_t::CreateThread(PVOID ThreadArgument)
{
    ThreadHandle = ::CreateThread(NULL, 0, ThreadStartUp, ThreadArgument, 0, &ThreadId);
}



