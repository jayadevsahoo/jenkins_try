//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUISinSignal.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"PatientDataUISinSignal.h"

#include<math.h>

#ifndef M_PI
#define M_PI 3.141592653589793238462643 
#endif

//-------------------------------------
// C O D E
//-------------------------------------

bool SinSignal::isWaveform() 
{
    return true;
}

bool SinSignal::isSinSignal()
{
    return true;
}

bool SinSignal::setSinSignal(double dFrequency,double dPhase,double dMin,double dMax)
{
    m_dMax     = dMax;
    m_dMin     = dMin;
    m_dFreqency= dFrequency;
    m_dPhase   = (dPhase*M_PI)/180.0;
    m_dRange   = (dMax-dMin)  /  2.0;

    m_bIsDesired  = true;
    UserFloat::unsetValue();
    return true;
}


bool SinSignal::setFloat(float fValue)
{
    m_bIsDesired=false;
    UserFloat::setFloat(fValue);
    return true;    
}

bool SinSignal::unsetValue()
{
    m_bIsDesired=false;
    UserFloat::unsetValue();
    return true;
}

double SinSignal::getFrequency()
{
    return m_dFreqency;
}         

double SinSignal::getPhase()
{
    return m_dPhase;
}         

double SinSignal::getMax()
{
    return m_dMax;
}

double SinSignal::getMin()
{
    return m_dMin;
}    




SinSignal::SinSignal(
        char *pName,
        unsigned int &uiTime, 
        double dFrequency,
        double dPhase,
        double dMin,
        double dMax
        ):
    UserFloat(pName,0.0f),
    m_uiTime(uiTime),
    m_dFreqency(dFrequency),
    m_dPhase( (dPhase*M_PI)/180.0),
    m_dMin(dMax),
    m_dMax(dMax),m_bIsDesired(false)
{
    m_dRange=(dMax-dMin)/2.0;

}



void SinSignal::updatePacket(void *address)
{
    float fValue;

    if(m_bIsDesired == true)
    {
        m_dTime=(double)m_uiTime;

        fValue = static_cast<float>( m_dRange * 
            sin( ((2.0*M_PI*m_dFreqency*m_dTime)/(50.0) ) + m_dPhase )
            + m_dRange + m_dMin );

        ((unsigned int *)address)[0]=htonl(*((unsigned int *)(&fValue)));
    }
    else if(UserFloat::m_bIsDesired==true){
        UserFloat::updatePacket(address);
    }
}




