//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file multipool_allocator.h
///
/// This file contains a multiple memory pool version of the STL-compatible
/// fixed memory block allocator. The size of the memory block returned for
/// a multipool_allocator varies based on the size requested. Each discrete
/// block size is controlled by a different fixed block allocator pool
/// regions. The blocks are sized to to fit within one of the discrete pools
/// regions regardless of what the underlying STL library requests.
///
/// This class is useful for providing an alternate STL allocator for
/// STL containers the require contiguous memory regions like std::string.
//------------------------------------------------------------------------------
#ifndef MULTIPOOL_ALLOCATOR_H
#define MULTIPOOL_ALLOCATOR_H

//------------------------------------------------------------------------------
// I N C L U D E S
//------------------------------------------------------------------------------
#include "pool_allocator.hh"
#include "SigmaTypes.hh"
#include "FaultHandlerMacros.hh"
#include "UtilityClassId.hh"

extern "C" Allocator* xallocator_get_allocator(size_t size);

//-----------------------------------------------------------------------------
/// multipool_allocator with a void type.
//-----------------------------------------------------------------------------
template <typename T> class multipool_allocator;
template <> class multipool_allocator<void> : public pool_allocator<void>
{
public:
	//-------------------------------------------------------------------------
    /// Convert an multipool_allocator<void> to an multipool_allocator<U>.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    template <class U>
        struct rebind { typedef multipool_allocator<U> other; };
};

//-----------------------------------------------------------------------------
/// multipool_allocator with a T type. T is the type of object being allocated.
//-----------------------------------------------------------------------------
template <typename T>
class multipool_allocator : public pool_allocator<T>
{
public:
	//-------------------------------------------------------------------------
    /// Convert an multipool_allocator<void> to an multipool_allocator<U>.
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
    template <class U>
        struct rebind { typedef multipool_allocator<U> other; };

	//-------------------------------------------------------------------------
    /// Get an instance of the memory allocator.
	/// @return		An instance of the allocator used to create variable sized
	///	byte blocks depending on the size requested.
	/// @param[in]	n - size of memory
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
	virtual Allocator* get_allocator(size_t n)
	{
		// get an Allocator instance based upon the requested block size
		return xallocator_get_allocator(n);
	}

	//-------------------------------------------------------------------------
    /// Constructor
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
	multipool_allocator(){}

	//-------------------------------------------------------------------------
    /// Copy constructor
    /// @pre        none
    /// @post       none
    //-------------------------------------------------------------------------
	template <class U> multipool_allocator(const multipool_allocator<U>&){}

private:
	//--------------------------------------------------------------------------
	///  This method is called when a software fault is detected by the
	///  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
	///  and 'lineNumber' are essential pieces of information.  The
	/// 'pFileName' and 'pPredicate' strings may be defaulted in the macro
	/// to reduce code space.
	///
	/// @param[in] SoftFaultID The ID of the softFault.
	/// @param[in] lineNumber The line number of the file.
	/// @param[in] pFileName The filename string.
	/// @param[in] pPredicate The boolean expression causing the fault
	/// @param[out] none.
	/// @return     none.
	///
	/// @pre  none.
	/// @post A software fault is reported.
	//--------------------------------------------------------------------------
	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const Char*       pFileName  = NULL,
						   const Char*       pPredicate = NULL);
};

//------------------------------------------------------------------------------
// SoftFault
//------------------------------------------------------------------------------
template <typename T>
void multipool_allocator<T>::SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const Char*        pFileName,
						  const Char*        pPredicate)
{
	FaultHandler::SoftFault(softFaultID, TOOLS,
			MULTIPOOL_ALLOCATOR, lineNumber, pFileName, pPredicate);
}

#endif // MULTIPOOL_ALLOCATOR_H
