#include "stdafx.h"
//------------------------------------------------------------------------------
//                   Copyright (c) 2008 - 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// @file UDPProxyServer.cc
///
/// This file contains implementation of class UDPProxyServer. This file is
/// conditionaly complied for GUI INTEGRATION TEST purposes to overwrite
/// patient data on GUI
//------------------------------------------------------------------------------


#include "UDPProxyServer.hh"

#include "MsgQueue.hh"
#include "MsgTimer.hh"
#include "SafetyNet.hh"
#include "Task.hh"
#include "SoftwareOptions.hh"
#include "MessageProcessor.h"

namespace swat{

static UDPProxyServer g_UDPProxyServerInstance;

OsTimeStamp UDPProxyServer::m_LastMsgOsTimeStamp;



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Boolean UDPProxyServer::OnRecvData()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean UDPProxyServer::OnRecvData(char *pBuffer, ULONG uiSize)
{



    swat::MessageProcessor::ProcessPacket(pBuffer,uiSize);

    return TRUE;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  void UDPProxyServer::WaitForDevKey()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void UDPProxyServer::WaitForDevKey()
{
	for(;;)
    {
        Task::Delay(0,5000);

        if (SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions())
        {
            m_iState = SETUP_SOCKET;
            break;

        }
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  void UDPProxyServer::Halt()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void UDPProxyServer::Halt()
{
	for(;;)
    {
        Task::Delay(0,5000);
    }

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UDPProxyServer::SetupSocket()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void UDPProxyServer::SetupSocket()
{

    UINT status;

    // Create a UDP socket.
    m_Socket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if ( m_Socket == INVALID_SOCKET )
    {
        m_iState = HALT;
        return;
    }

    SOCKADDR_IN listen_addr;

	memset(&listen_addr, 0, sizeof(listen_addr));
	listen_addr.sin_family = AF_INET;
	listen_addr.sin_port = htons( SWAT_BD_COMMAND_UDP_PORT );
	listen_addr.sin_addr.s_addr = INADDR_ANY;

	if(bind(m_Socket, (SOCKADDR*)&listen_addr,sizeof(SOCKADDR_IN)) < 0) 
	{
		RETAILMSG(true, (_T("ERROR binding in the server socket.\r\n")));
        m_iState = HALT;
        closesocket( m_Socket );
		return;
	}

    m_iState = WAIT_FOR_MESSAGE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UDPProxyServer::WaitForMessage()
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void UDPProxyServer::WaitForMessage()
{
    int uiBytesRead;
    OsTimeStamp clMsgOsTimeStamp;

    // initialize fd_set
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(m_Socket, &fds);

    // Wait until a message is received
    int selectResult = select(sizeof(fds)*8, &fds, NULL, NULL, NULL /*forever*/);

    // SOCKET_ERROR or TIMEOUT. Because we already set the TIMEOUT parameter
    // to 'forever', so 'select' will never timeout.
    if ( selectResult <= 0 )
    {
        RETAILMSG(true, 
            ( _T("ReadData: ERROR in select, %d\r\n"), WSAGetLastError()) );

        m_iState = RESTART;
        return;
    }

    // selectResult > 0
    SOCKADDR_IN clientaddr;
	int len = sizeof(clientaddr); 
	
    //Read message out of buffer
    int recvResult = recvfrom(m_Socket, m_Buffer, sizeof(m_Buffer), 0, 
		(sockaddr*)&clientaddr, &len );

    if ( recvResult <= 0 )
    {
        // recvResult = SOCKET_ERROR, use WSAGetLastError to get error code
		RETAILMSG(true, 
            ( _T("ReadData: ERROR in recvfrom, %d\r\n"), WSAGetLastError()) );
        
        m_iState = RESTART;
        return;
    }

    // recvResult > 0
    uiBytesRead = recvResult;	

    //Get now time.
    clMsgOsTimeStamp.now();


    //Process incomming data
    if(OnRecvData((char *)m_Buffer,uiBytesRead))
    {
        m_LastMsgOsTimeStamp=clMsgOsTimeStamp;
    }

    //Change state
    m_iState = WAIT_FOR_MESSAGE;

}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UDPProxyServer::Restart()
//
//@ Interface-Description
//      This method recreates the listening socket.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void UDPProxyServer::Restart()
{
    int status = 0;

    // since Winsock doesn't support socket unbind, here we close it.
    status = closesocket( m_Socket );

    // make sure no error occurred
    if ( status == SOCKET_ERROR )
    {
        m_iState = HALT;
        return;
    }

    Reset();

    m_iState = SETUP_SOCKET;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UDPProxyServer::Reset()
//
//@ Interface-Description
//      This method resets the IsCallback table.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void UDPProxyServer::Reset()
{
    //Reset all flags in system to accept data.

    m_iState = WAIT_FOR_MESSAGE;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UDPProxyServer::ServerLoop()
//
//@ Interface-Description
//      This method is the loop in which the UDPProxyServer thread
//      executes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void UDPProxyServer::ServerLoop()
{

    m_iState = WAIT_FOR_DEVKEY;

	for(;;)
    {
        switch(m_iState)
        {
            case WAIT_FOR_DEVKEY:
                WaitForDevKey();
                break;

            case WAIT_FOR_MESSAGE:
                WaitForMessage();
                break;

            case SETUP_SOCKET:
                SetupSocket();
                break;

            case RESET:
                 Reset();
                 break;

            case RESTART:
                 Restart();
                 break;

            case HALT:
                 Halt();
                 break;
        }
    }

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  UDPProxyServer::UDPProxyServerTask(Uint32 iJunk)
//                [static]
//
//@ Interface-Description
//      This method is the entry function to the UDPProxyServer task.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Call ServerLoop().
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void UDPProxyServer::UDPProxyServerTask( void )
{
    g_UDPProxyServerInstance.ServerLoop();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
UDPProxyServer::SoftFault(const SoftFaultID  softFaultID,
					  const Uint32       lineNumber,
					  const char*        pFileName,
					  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, NETWORK_APPLICATION, 1,
                          lineNumber, pFileName, pPredicate);
}

}
