#ifndef Touch_HH
#define Touch_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Touch -  This class directs input from a touch device to
// the appropriate drawable.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Touch.hhv   25.0.4.0   19 Nov 2013 14:11:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 005   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 004   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 003  By: yyy      Date: 06-Feb-1997  DR Number: 1526
//    Project:  Sigma (R8027)
//    Description:
//	Changed TouchEvent() nd ReleaseEvent() from void to Int16 return type
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"

//@ Usage-Classes
class Drawable;

//@ End-Usage

class Touch 
{
public:
	//@ Type: TouchStatus
	// Identifies the three major states the touch events can be in:
	enum TouchStatus
	{
		NO_SCREEN_TOUCHED,
		UPPER_SCREEN_TOUCHED,
		LOWER_SCREEN_TOUCHED,
		BOTH_SCREEN_TOUCHED,
		OFF_SCREEN_TOUCHED,
		NUM_SCREEN_TOUCHED
	};

    struct Position
    {
        Int16 x;
        Int16 y;
    };

	static void Initialize(void);
	static Int16 TouchEvent(Int16 x, Int16 y);
	static Int16 ReleaseEvent(Int16 x, Int16 y);
	
	inline static void Enable(void);
	inline static void Disable(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32	   lineNumber,
						  const char*	   pFileName  = NULL,
						  const char*	   pPredicate = NULL);

#if defined SIGMA_UNIT_TEST
	static Drawable *getPFocus(void);
	static Boolean getEnabled(void);
#endif	// SIGMA_UNIT_TEST
								 
								 

protected:

private:

	Touch();						// Declared but not implemented
	~Touch();						// Declared but not implemented
	Touch(const Touch&);			// Declared but not implemented
	void operator=(const Touch&);	// Declared but not implemented

	//@ Data-Member: PFocus_
	// This member points at the drawable which successfully received
	// the last touch event, if there is one. If not this member has 
	// a null value.
	static Drawable *PFocus_;

	//@ Data-Member: Enabled_
	// The Touch screen can be enabled or disabled. Which state it is in is
	// stored in this Boolean variable.
	static Boolean Enabled_;

	//@ Data-Member: TouchScreen_
	// Stores the current TouchStatus
	static Int16 TouchScreen_;

};

		
// Inlined methods
#include "Touch.in"

#endif // Touch_HH 
