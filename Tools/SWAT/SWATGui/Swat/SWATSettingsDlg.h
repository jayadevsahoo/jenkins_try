#pragma once

#include "resource.h"
#include "afxwin.h"
// CSWATSettingsDlg dialog

class CSWATSettingsDlg : public CDialog
{
	DECLARE_DYNAMIC(CSWATSettingsDlg)

public:
	CSWATSettingsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSWATSettingsDlg();

// Dialog Data
	enum { IDD = IDD_SWAT_SETTINGS_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	
	int m_nEditorType;

	virtual BOOL OnInitDialog();
	bool m_bRecordSWinReport;
	CButton m_Radio_SW;
	BOOL m_bAppendTimeInStatusWindow;
	BOOL m_bSendMail;
	BOOL m_bLockReports;
};
