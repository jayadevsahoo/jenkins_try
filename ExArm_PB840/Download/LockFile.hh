#ifndef  LockFile_HH
#define	LockFile_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@  Filename: LockFile.hh - Provides capability to create a lock
//    file during a download server execution.
//---------------------------------------------------------------------
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/LockFile.hhv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  12-June-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================

#include "DownloadTypes.hh"
#include "DownloadError.hh"
#include "DownloadCons.hh"
#include "DownloadFault.hh"
#include <stdio.h>


class LockFile 
{
  public:
    LockFile();
    ~LockFile(void);
	Int setLock(char *pTargetName);
	void unLock();
	Boolean anyTargetLocked(void);
    static void SoftFault(const FaultType softFaultID,
		const Uint32      lineNumber,
		const char*       pFileName  = NULL, 
		const char*       pPredicate = NULL);

  protected:

  private:
    LockFile(const LockFile&);			// not implemented...
    void   operator=(const LockFile&);	// not implemented...

    //@ Data-Member:  PMasterLockFile_;
    // a lock file name used when a download server runs in a Master
	// mode serving all the targets.
		static char	*PMasterLockFile_;

	//@Data-Member: PLockDir_
	// a directory name where all the lock files reside.
		static const char *PLockDir_;

	//@Data-Member: fileName_
	// a lock file name.
		char fileName_[100];


	//@Data-Member: pHandle_
	// a lock file handle pointer
	FILE *pHandle_;
};


#endif // LockFile_HH 
