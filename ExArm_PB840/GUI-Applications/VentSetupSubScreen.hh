#ifndef VentSetupSubScreen_HH
#define VentSetupSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: VentSetupSubScreen - The subscreen in the Lower screen
// selected by pressing the Vent Setup button.  Displays two screens,
// one for setting the Mode, Mandatory Type, Support Type and Trigger
// Type and the other for adjusting settings appropriate to the
// Vent mode selected on the first screen.  Also used in New Patient Setup.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/VentSetupSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 012   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 011   By: gdc   Date:  27-May-2005    SCR Number: 6170
//  Project:  NIV2
//  Description:
//       Removed Insp Too Long alarm for NIV. Added High Ti spont alert. 
//      
//  Revision: 010   By: gdc   Date:  15-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//       Added disconnect sensitivity to second screen in NIV.
//       Added NIV indicator in status bar of second setup sub-screen. 
//
//  Revision: 009  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added use of new 'DropDownSettingButton' class
//      *  added button for new volume support setting
//
//  Revision: 008  By: hhd    Date: 07-June-1999  DCS Number: 5423 
//  Project:  ATC
//  Description:
//      Removed iconBackgroundBox_.
//
//  Revision: 007  By: hhd    Date: 05-May-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//      Added background for flashing icons.
//
//  Revision: 006  By: sah    Date: 29-Apr-1999  DCS Number: 5365
//  Project:  ATC
//  Description:
//      Supporting new verification-needed mechanism, whereby certain
//      setting buttons that are deemed "ultra"-important will flash an
//      icon, and this subscreen will flash the same icon along with a
//      message.
//
//  Revision: 005  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 004  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 003  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/VentSetupSubScreen.hhv   1.9.1.0   07/30/98 10:22:38   gdc
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "BatchSettingsSubScreen.hh"
#include "Bitmap.hh"
#include "Box.hh"
#include "BreathTimingDiagram.hh"
#include "DiscreteSettingButton.hh"
#include "DropDownSettingButton.hh"
#include "IbwSettingButton.hh"
#include "IeRatioSettingButton.hh"
#include "Line.hh"
#include "NumericLimitSettingButton.hh"
#include "NumericSettingButton.hh"
#include "SubScreenTitleArea.hh"
#include "TimingSettingButton.hh"
//@ End-Usage

class VentSetupSubScreen : public BatchSettingsSubScreen
{
public:
	VentSetupSubScreen(SubScreenArea *pSubScreenArea);
	~VentSetupSubScreen(void);

	virtual void acceptHappened(void);

    // ButtonTarget virtual method
    virtual void buttonDownHappened(Button *pButton,
                                                Boolean byOperatorAction);
    virtual void buttonUpHappened(Button *pButton,
                                                Boolean byOperatorAction);
 
	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// BatchSettingsSubScreen virtual method
	virtual void adjustPanelRestoreFocusHappened(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									  );

private:
	// these methods are purposely declared, but not implemented...
	VentSetupSubScreen(void);							// not implemented...
	VentSetupSubScreen(const VentSetupSubScreen&);		// not implemented...
	void   operator=(const VentSetupSubScreen&);		// not implemented...

	void layoutScreen2_(void);

	void setVentSetupTabTitleText_( StringId stringId );

	// Constant: VENT_SETUP_MAX_BUTTONS_1, VENT_SETUP_MAX_BUTTONS_2 
	// Maximum number of buttons that can be displayed in screen 1 and 2 of
	// VentSetupSubScreen.  Must be modified when new buttons are added.
	enum
	{
		VENT_SETUP_MAX_BUTTONS_1 = 6,
		VENT_SETUP_MAX_BUTTONS_2 = 30
	};

	//@ Data-Member: onScreen1_
	// A flag which tells us whether we're in the first or second screen
	// of Vent Setup.
	Boolean onScreen1_;

	//@ Data-Member: isPreviousSetup_
	// TRUE if the currently displayed breath settings are a result of
	// pressing the Previous Setup button.
	Boolean isPreviousSetup_;

	//@ Data-Member: previousSetupButton_
	// The Previous Setup button, which allows the operator to restore
	// a previously stored settings context.
	TextButton previousSetupButton_;

	//@ Data-Member: continueButton_
	// The Continue button, which allows the operator to get from screen
	// 1 to screen 2 of Vent Setup.
	TextButton continueButton_;

	//@ Data-Member: arrScreen1BtnPtrs_
	// All buttons that could be displayed in Vent Setup, screen 1.
	// Last slot is reserved for end array marker.
	SettingButton*  arrScreen1BtnPtrs_[VENT_SETUP_MAX_BUTTONS_1 + 1];

	//@ Data-Member: arrScreen2BtnPtrs_
	// All buttons that could be displayed in Vent Setup, screen 2.
	// Last slot is reserved for end array marker.
	SettingButton*  arrScreen2BtnPtrs_[VENT_SETUP_MAX_BUTTONS_2 + 1];

	//-----------------------------------------------------------------
	// $[VC01000] -- when selected, the buttons for the main control
	//               settings shall display a drop-down menu...
	//-----------------------------------------------------------------

	//@ Data-Member: newIbwButton_
	// Setting button to change current patient IBW
	IbwSettingButton newIbwButton_;

	//@ Data-Member: ventTypeButton_
	// The Vent Type setting button (screen 1).
	DiscreteSettingButton ventTypeButton_;

	//@ Data-Member: modeButton_
	// The Mode setting button (screen 1).
	DropDownSettingButton modeButton_;

	//@ Data-Member: mandatoryTypeButton_
	// The Mandatory Type setting button (screen 1).
	//	MandatoryTypeButton mandatoryTypeButton_;
	DropDownSettingButton mandatoryTypeButton_;

	//@ Data-Member: supportTypeButton_
	// The Support Type setting button (screen 1).
	DropDownSettingButton supportTypeButton_;
  
	//@ Data-Member: triggerTypeButton_
	// The Trigger Type setting button (screen 1).
	DropDownSettingButton triggerTypeButton_;

	//@ Data-Member: cctTypeContainer_
	// Circuit type container - contains the circuit-type box, label and text
	Container   cctTypeContainer_;

	//@ Data-Member: cctTypeBox_
	// Box inside the cctTypeContainer_ 
	Box   cctTypeBox_;

	//@ Data-Member: cctTypeLabel_
	// The "CIRCUIT TYPE" label inside the cctTypeContainer_
	TextField   cctTypeLabel_;

	//@ Data-Member: cctTypeText_
	// Text field containing the circuit type (neonatal, ped, adult)
	TextField   cctTypeText_;

	//@ Data-Member: statusBar_
	// The status bar which displays the breath control settings (screen 2)
	Container statusBar_;

	//@ Data-Member: mainSettingsAreaContainer_
	// A container which holds all the buttons displayed in the "main settings
	// area" of screen 2 and whose fill color forms a background for the
	// buttons.
	Container mainSettingsAreaContainer_;

	//@ Data-Member: expiratorySensitivityButton_
	// The Expiratory Sensitivity setting button (screen 2).
	NumericSettingButton expiratorySensitivityButton_;

	//@ Data-Member: expiratoryTimeButton_
	// The Expiratory Time setting button (screen 2).
	TimingSettingButton expiratoryTimeButton_;

	//@ Data-Member: peepLowTimeButton_
	// The Peep Low Time setting button (screen 2).
	TimingSettingButton peepLowTimeButton_;

	//@ Data-Member: flowAccelerationButton_
	// The Flow Acceleration setting button (screen 2).
	NumericSettingButton flowAccelerationButton_;

	//@ Data-Member: flowPatternButton_
	// The Flow Pattern setting button (screen 2).
	DiscreteSettingButton flowPatternButton_;

	//@ Data-Member: flowSensitivityButton_
	// The Flow Sensitivity setting button (screen 2).
	NumericSettingButton flowSensitivityButton_;

	//@ Data-Member: highCircuitPressureButton_
	// The High Circuit Pressure setting button (screen 2).
	NumericSettingButton highCircuitPressureButton_;

	//@ Data-Member: inhaledSpontTidalVolumeButton_
	// The High Inhaled Spontaneous Tidal Volume Limit setting button
	// (screen 2).
	NumericSettingButton  inhaledSpontTidalVolumeButton_;

	//@ Data-Member: ieRatioButton_
	// The I:E Ratio setting button (screen 2).
	IeRatioSettingButton ieRatioButton_;

	//@ Data-Member: hlRatioButton_
	// The peep high:peep low Ratio setting button (screen 2).
	IeRatioSettingButton hlRatioButton_;

	//@ Data-Member: inspiratoryPressureButton_
	// The Inspiratory Pressure setting button (screen 2).
	NumericSettingButton inspiratoryPressureButton_;

	//@ Data-Member: inspiratoryTimeButton_
	// The Inspiratory Time setting button (screen 2).
	TimingSettingButton inspiratoryTimeButton_;

	//@ Data-Member: peepHighTimeButton_
	// The Peep High Time setting button (screen 2).
	TimingSettingButton peepHighTimeButton_;

	//@ Data-Member: oxygenPercentageButton_
	// The Oxygen Percentage setting button (screen 2).
	NumericSettingButton oxygenPercentageButton_;

	//@ Data-Member: peakFlowButton_
	// The Peak Flow setting button (screen 2).
	NumericSettingButton peakFlowButton_;

	//@ Data-Member: peepButton_
	// The Peep setting button (screen 2).
	NumericSettingButton peepButton_;

	//@ Data-Member: peepLowButton_
	// The Peep Low setting button (screen 2).
	NumericSettingButton peepLowButton_;

	//@ Data-Member: peepHighButton_
	// The Peep High setting button (screen 2).
	NumericSettingButton peepHighButton_;

	//@ Data-Member: plateauTimeButton_
	// The Plateau Time setting button (screen 2).
	TimingSettingButton plateauTimeButton_;

	//@ Data-Member: pressureSensitivityButton_
	// The Pressure Sensitivity setting button (screen 2).
	NumericSettingButton pressureSensitivityButton_;

	//@ Data-Member: pressureSupportButton_
	// The Pressure Support setting button (screen 2).
	NumericSettingButton pressureSupportButton_;

	//@ Data-Member: respiratoryRateButton_
	// The Respiratory Rate setting button (screen 2).
	NumericSettingButton respiratoryRateButton_;

	//@ Data-Member: tidalVolumeButton_
	// The Tidal Volume setting button (screen 2).
	NumericSettingButton tidalVolumeButton_;

	//@ Data-Member: percentSupportButton_ (screen 2).
	// The Percent Support Vent Setup setting button.
	NumericSettingButton percentSupportButton_;

	//@ Data-Member: tubeTypeButton_
	// The Patient Tube Type setting button (screen 2).
	DiscreteSettingButton tubeTypeButton_;

	//@ Data-Member: discSensButton_
	// The Disconnect Sens setting button (screen 2).
	NumericLimitSettingButton discSensButton_;

	//@ Data-Member: tubeIdButton_
	// The Tube Id setting button (screen 2).
	NumericSettingButton tubeIdButton_;

	//@ Data-Member: volumeSupportButton_
	// The Volume Support setting button (screen 2).
	NumericSettingButton volumeSupportButton_;

	//@ Data-Member: highSpontInspTimeButton_
	// The High Spontaneous Inspiratory Time Limit setting button (screen 2).
	TimingSettingButton highSpontInspTimeButton_;

	//@ Data-Member: apneaIntervalButton_
	// The apnea interval setting button shown in CPAP mode (screen 2)
	NumericLimitSettingButton apneaIntervalButton_;

	//@ Data-Member: apneaDisabledMsg_
	// Text field containing APNEA DISABLED warning message
	TextField   apneaDisabledMsg_;

	//@ Data-Member:  titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea  titleArea_;
	
	//@ Data-Member: ventTypeTitle_
	// The text for the Vent Type title displayed in the title area.
	TextField ventTypeTitle_;
	
	//@ Data-Member: modeTitle_
	// The text for the Mode title displayed in the title area.
	TextField modeTitle_;
	
	//@ Data-Member: mandatoryTypeTitle_
	// The text for the Mandatory Type title displayed in the title area.
	TextField mandatoryTypeTitle_;
	
	//@ Data-Member: supportTypeTitle_
	// The text for the Support Type title displayed in the title area.
	TextField supportTypeTitle_;
	
	//@ Data-Member: triggerTypeTitle_
	// The text for the Trigger Type title displayed in the title area.
	TextField triggerTypeTitle_;

	//@ Data-Member: ibwTitle_
	// The text for the Ideal Body Weight value displayed in the title
	// area and breath status area during New Patient Setup.
	TextField ibwTitle_;

	//@ Data-Member: spontMandTypeMsg_
	// The message which appears when the user chooses the Spont mode,
	// warning that the Mandatory Type value now applies to Manual
	// Inspirations only.
	TextField spontMandTypeMsg_;

	//@ Data-Member: verifySettingsMsg_
	// This message appears when any of the visible setting buttons
	// are in the verification-needed state.
	TextField verifySettingsMsg_;

	//@ Data-Member: verifySettingsIcon_
	// This icon appears when any of the visible setting buttons
	// are in the verification-needed state.
	Bitmap verifySettingsIcon_;

	//@ Data-Member: leftWingtip_
	// Left-hand "wingtip" line for the Mandatory Type on the status bar. 
	Line leftWingtip_;

	//@ Data-Member: leftWing_
	// Left-hand "wing" line for the Mandatory Type on the status bar.
	Line leftWing_;

	//@ Data-Member: rightWing_
	// Right-hand "wing" line for the Mandatory Type on the status bar.
	Line rightWing_;

	//@ Data-Member: rightWingtip_
	// Right-hand "wingtip" line for the Mandatory Type on the status bar.
	Line rightWingtip_;

	//@ Data-Member: breathTimingDiagram_
	// The Breath Timing diagram (graph) on screen 2.
	BreathTimingDiagram breathTimingDiagram_;

};


#endif // VentSetupSubScreen_HH 
