#include "StdAfx.h"
#include "CommandResponse.h"
#include <stdio.h>
#include <time.h>

bool CommandResponse::m_cancelReceive = false;

CommandResponse::CommandResponse(Socket* socket) :
	m_socket(socket)
{
}

CommandResponse::~CommandResponse(void)
{
}

INT CommandResponse::SendReceive(const CHAR* outBuf, CHAR* inBuf, INT inBufSize)
{
	if (!Send(outBuf))
		return -1;

    memset(inBuf, 0, inBufSize);
	INT bytes = Receive(inBuf, inBufSize);
	
	return bytes;
}

bool CommandResponse::Send(const CHAR* outBuf)
{
	if (!m_socket->Send(outBuf))
		return false;
	return true;
}

INT CommandResponse::Receive(CHAR* inBuf, INT inBufSize, LONG timeout)
{
#ifdef TCP_COMMUNICATION
	m_cancelReceive = false;

	clock_t waitTimeStamp = clock() + timeout;
	while (!m_socket->ReceiveReady())
	{
		if ((timeout > 0 && clock() > waitTimeStamp) || m_cancelReceive)
			return -1;
		Sleep(10);
	}

	INT bytesRead = m_socket->Receive(inBuf, inBufSize);
	return bytesRead;
#else
    return m_socket->Receive(inBuf, inBufSize, timeout);
#endif
}



