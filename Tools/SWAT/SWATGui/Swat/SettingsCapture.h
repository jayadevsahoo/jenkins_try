#pragma once

#include "stdafx.h"


#include <stdio.h>
#include "Windows.h"

#include "Swat.h"
#include "ISettingsCapture.h"
#include "ICConfiguration.h"

class CGUISettingsCapture
{
    private:

	HWND m_pWnd;
	
	int printData(char *pData, int iLength);

    IConfiguration   *m_pIConfiguration;
    ISettingsCapture *m_pISettingsCapture;

    HINSTANCE hinstLib;

    public:
    CGUISettingsCapture();

    ~CGUISettingsCapture(){
        m_pISettingsCapture->destroy();
        m_pIConfiguration  ->destroy();
     //   FreeLibrary(hinstLib); 
    }

    static int printData(void *pThis, char *pData, int iLength)
	{
        return ((CGUISettingsCapture *)pThis)->printData(pData,iLength);
    }

	unsigned long int CGUISettingsCapture::strIp2ulongIp(char *strIp);

};




