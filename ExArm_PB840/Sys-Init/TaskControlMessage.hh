#ifndef TaskControlMessage_HH
#define TaskControlMessage_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//====================================================================
 
//====================================================================
// Class:  TaskControlMessage - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskControlMessage.hhv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 05/16/95    DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//====================================================================


//@ Usage-Classes
#include "Sys_Init.hh"
//@ End-Usage
 
class AppContext;
class BufferPool;

class TaskControlMessage 
{
  public:
    TaskControlMessage( Sys_Init::InitClassId classId );

    virtual ~TaskControlMessage();

    virtual const char*  nameOf(void) const = 0;
    virtual void         dispatch(const AppContext& context) const = 0;

    static void Initialize(void);
    static const BufferPool& GetBufferPool(void);

    static void *        operator new(size_t nbytes);
    static void          operator delete(void* pObject);

    inline Sys_Init::InitClassId isA(void) const;

    static void          SoftFault(const SoftFaultID  softFaultID,
				   const Uint32       lineNumber,
				   const char*        pFileName = NULL,
				   const char*        pPredicate = NULL);
 
  protected:
    TaskControlMessage(const TaskControlMessage& rMessage);

  private:
    void   operator=(const TaskControlMessage&);  // declared only

    static BufferPool&  GetBufferPool_(void);

    //@ Data-Member:    classId_
    //  Contains the class identifier of the derived or concrete class.
    enum Sys_Init::InitClassId  classId_;
};
 
#include "TaskControlMessage.in"

#endif // TaskControlMessage_HH
