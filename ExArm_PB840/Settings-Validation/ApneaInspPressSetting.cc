#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaInspPressSetting - Apnea Inspiratory Pressure Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the pressure (in cmH2O)
//  to which gas is delivered to the patient during pressure-controlled apnea
//  inspirations (apnea PCV).  This class inherits from 'BatchBoundedSetting'
//  and provides the specific information needed for representation of apnea
//  inspiratory pressure.  This information includes the interval and range
//  of this setting's values, this setting's response to a Main Control
//  Setting's transition (see 'acceptTransition()'), this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the constraints of
//  this setting.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateConstraints_()' method for updating
//  the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaInspPressSetting.ccv   25.0.4.0   19 Nov 2013 14:27:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 009   By: sah   Date:  16-Aug-1999    DR Number: 5462
//  Project:  ATC
//  Description:
//	Modified 'correctSelf()', to only update value if currently applicable.
//
//  Revision: 008   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  added new 'correctSelf()' method to offload implementation
//	   detail from AdjustedContext to the specific setting
//	*  modified 'updateConstraints_()' to factor in PEEP-low,
//	   when applicable
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 007   By: sah   Date: 11-Nov-1998    DR Number:  5257
//  Project:  BILEVEL
//  Description:
//	Modified 'acceptTransition()' to use apnea Pi's 'updateConstraints()'
//	method for determining limits, rather than explicitly calculating
//	limits with redundant code.
//
//  Revision: 006   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 005   By: dosman    Date:  24-Dec-1997    DR Number: 
//  Project:  BILEVEL
//  Description:
//	initial bilevel version
//	added symbolic constants
//
//  Revision: 004   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//      --- Merging Rev "Color" (1.28.1.0) into Rev "BiLevel" (1.28.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 003   By: sah    Date:  11-Feb-1997    DR Number: 1748
//  Project: Sigma (R8027)
//  Description:
//	Added absolute bound, for when PEEP is zero.
//
//  Revision: 002   By: sah    Date:  08-Nov-1996    DR Number: 1562
//  Project: Sigma (R8027)
//  Description:
//	Fixed "warping" in 'updateConstraints_()' to be UP/DOWN, instead
//	of NEAREST; NEAREST causes problems due to incompatible resolutions.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaInspPressSetting.hh"
#include "MandTypeValue.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "ContextId.hh"
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02058] -- The setting's range ...
//  $[02060] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    5.0f,		// absolute minimum...
    0.0f,		// unused...
    ONES,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    90.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaInspPressSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BatchBoundedSetting.  The method passes to
//  BatchBoundedSetting it's default information.  Also, this method
//  initializes the setting's value interval range.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaInspPressSetting::ApneaInspPressSetting(void)
   : BatchBoundedSetting(SettingId::APNEA_INSP_PRESS,
			 Setting::NULL_DEPENDENT_ARRAY_,
			 &::INTERVAL_LIST_,
                         APNEA_INSP_PRESS_MAX_BASED_PEEP_ID,// upperBoundId...
                         APNEA_INSP_PRESS_MIN_ID)	    // lowerBoundId...
{
  CALL_TRACE("ApneaInspPressSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaInspPressSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaInspPressSetting::~ApneaInspPressSetting(void)
{
  CALL_TRACE("~ApneaInspPressSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01096] -- apnea breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaInspPressSetting::getApplicability(
			      const Notification::ChangeQualifier qualifierId
				   ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pApneaMandType =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_MAND_TYPE);

  DiscreteValue  apneaMandTypeValue;

  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    apneaMandTypeValue = pApneaMandType->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    apneaMandTypeValue = pApneaMandType->getAdjustedValue();
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }

  return((apneaMandTypeValue == MandTypeValue::PCV_MAND_TYPE)
	   ? Applicability::CHANGEABLE		// $[TI3]
	   : Applicability::INAPPLICABLE);	// $[TI4]
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  This setting is only interested in changes of apnea mandatory type
//  from "VCV" to "PCV".
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02023] -- transitioning from apnea PC...
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId == SettingId::APNEA_MAND_TYPE    &&
//   newValue  == MandTypeValue::PCV_MAND_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaInspPressSetting::acceptTransition(
				const SettingId::SettingIdType settingId,
				const DiscreteValue            newValue,
				const DiscreteValue
				       )
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  AUX_CLASS_PRE_CONDITION((settingId == SettingId::APNEA_MAND_TYPE),
  			  settingId);
  AUX_CLASS_PRE_CONDITION((newValue == MandTypeValue::PCV_MAND_TYPE),
  			  newValue);

  // ===================================================================
  // apnea mandatory type transition from VCV to PCV...
  // ===================================================================

  // use new-patient value...
  BoundedValue  apneaInspPress = getNewPatientValue();

  BoundStatus  dummyStatus(getId());

  // calculate bound constraints...
  updateConstraints_();

  // use the calculation of the maximum bound in 'updateConstraints_()'
  // to limit the current adjusted value to a valid range...
  getBoundedRange_().testValue(apneaInspPress, dummyStatus);

  // store the transition value into the adjusted context...
  setAdjustedValue(apneaInspPress);

  // activation of transition rule must be italicized...
  setForcedChangeFlag();
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Return a static const BoundedValue.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaInspPressSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;
  
  // $[02059] The setting's new-patient value ...
  newPatient.value     = SettingConstants::DEFAULT_MAND_PRESS_DELTA;
  newPatient.precision = ONES;

  return(newPatient);  // $[TI1]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  correctSelf()
//
//@ Interface-Description
//  Correct the value of this setting, with respect to its bound
//  contraints, and return a boolean as to whether a correction was
//  needed ('TRUE'), or not ('FALSE').
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02029]\a\ -- transition to a valid apnea value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
ApneaInspPressSetting::correctSelf(void)
{
  CALL_TRACE("correctSelf()");

  BoundStatus  dummyStatus(getId());

  if (getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE)
  {  // $[TI1] -- this setting is applicable...
    BoundedValue  apneaInspPress = getAdjustedValue();

    // calculate bound constraints...
    updateConstraints_();

    // use the calculation of the bounds in 'updateConstraints_()'
    // to limit the current adjusted value to a valid range...
    getBoundedRange_().testValue(apneaInspPress, dummyStatus);

    if (dummyStatus.isInViolation())
    {  // $[TI1.1] -- the current adjusted value required correction...
      // store the "clipped" value into the adjusted context...
      setAdjustedValue(apneaInspPress);

      // force the change flag due to this correction...
      setForcedChangeFlag();
    }  // $[TI1.2] -- no correction was needed...
  }  // $[TI2] -- this setting is NOT applicable...

  return(dummyStatus.isInViolation());
}


#if defined(SIGMA_DEVELOPMENT)

#include "ApneaIntervalSetting.hh"

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies? Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaInspPressSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = TRUE;

  if (getApplicability(Notification::ACCEPTED) !=
					  Applicability::INAPPLICABLE)
  {
    if (ApneaIntervalSetting::IsApneaPossible(BASED_ON_ACCEPTED_VALUES))
    {
      // is the accepted apnea inspiratory time value within the min and max
      // values?...
      isValid = BoundedSetting::isAcceptedValid(); // forward to base class...
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaInspPressSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = TRUE;

  if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
  {
    if (ApneaIntervalSetting::IsApneaPossible(BASED_ON_ADJUSTED_VALUES))
    {
      // is the adjusted apnea inspiratory time value within the min and max
      // values?...
      isValid = BoundedSetting::isAdjustedValid(); // forward to base class...
    }
  }

  return(isValid);
}

#endif // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaInspPressSetting::SoftFault(const SoftFaultID  softFaultID,
				 const Uint32       lineNumber,
				 const char*        pFileName,
				 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_INSP_PRESS_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02058] -- The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaInspPressSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

  BoundedValue  maxLimit;

  BoundedRange::ConstraintInfo  maxConstraintInfo;

  // set to absolute maximum bound...
  maxConstraintInfo.id    = APNEA_INSP_PRESS_MAX_ID;
  maxConstraintInfo.value = ABSOLUTE_MAX;

  Real32  peepValue;

  SettingBoundId  peepBasedMaxId;
  SettingBoundId  hichCctBasedMaxId;

  const Setting*  pPeep = SettingsMgr::GetSettingPtr(SettingId::PEEP);

  if (pPeep->getApplicability(Notification::ADJUSTED) != 
						Applicability::INAPPLICABLE)
  {  // $[TI5] -- PEEP is applicable...
    peepValue         = BoundedValue(pPeep->getAdjustedValue()).value;
    peepBasedMaxId    = APNEA_INSP_PRESS_MAX_BASED_PEEP_ID;
    hichCctBasedMaxId = APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEP_ID;
  }
  else
  {  // $[TI6] -- PEEP low must be applicable...
    const Setting*  pPeepLow = SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW);

    SAFE_CLASS_ASSERTION(
	  (pPeepLow->getApplicability(Notification::ADJUSTED) != 
						Applicability::INAPPLICABLE));

    peepValue         = BoundedValue(pPeepLow->getAdjustedValue()).value;
    peepBasedMaxId    = APNEA_INSP_PRESS_MAX_BASED_PEEPL_ID;
    hichCctBasedMaxId = APNEA_INSP_PRESS_MAX_BASED_PCIRC_PEEPL_ID;
  }

  //===================================================================
  // calculate apnea inspiratory pressure's maximum bound that is based
  // on the PEEP value...
  //===================================================================

  maxLimit.value = (SettingConstants::MAX_ALLOWED_PRESSURE_SETTING_VALUE -
  		    peepValue);

  if (maxLimit.value < maxConstraintInfo.value)
  {   // $[TI1]
    // reset this range's maximum constraint to allow for "warping" of this
    // new maximum value...
    getBoundedRange_().resetMaxConstraint();

    // warp the calculated maximum value to a lower "click" boundary...
    getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

    // the PEEP-based maximum is more restrictive than 'maxBoundValue',
    // therefore save its value and id as the maximum bound value and id...
    maxConstraintInfo.id    = peepBasedMaxId;
    maxConstraintInfo.value = maxLimit.value;
  }  // $[TI2] -- apnea insp. pressure's absolute max is more restrictive...

  //===================================================================
  // calculate apnea inspiratory pressure's maximum bound that is based
  // on high circuit pressure and the PEEP value...
  //===================================================================

  const Setting*  pHighCctPress =
			SettingsMgr::GetSettingPtr(SettingId::HIGH_CCT_PRESS);

  const Real32  HIGH_CCT_VALUE =
  			BoundedValue(pHighCctPress->getAdjustedValue()).value;

  maxLimit.value = (HIGH_CCT_VALUE - peepValue -
		    SettingConstants::MIN_MAND_PRESS_BUFF);

  if (maxLimit.value < maxConstraintInfo.value)
  {   // $[TI3]
    // reset this range's maximum constraint to allow for "warping" of this
    // new maximum value...
    getBoundedRange_().resetMaxConstraint();

    // warp the calculated maximum value to a lower "click" boundary...
    getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

    // the high circuit pressure-based maximum is more restrictive than
    // 'maxBoundValue', therefore save its value and id as the maximum
    // bound value and id...
    maxConstraintInfo.id    = hichCctBasedMaxId;
    maxConstraintInfo.value = maxLimit.value;
  }  // $[TI4] -- 'maxBoundValue' is more restrictive...

  // this setting has no maximum soft bounds...
  maxConstraintInfo.isSoft = FALSE;

  // update the maximum bound to the most restrictive bound...
  getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}
