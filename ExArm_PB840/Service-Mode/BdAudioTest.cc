#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdAudioTest - Test the BD audio alarm
//---------------------------------------------------------------------
//@ Interface-Description
//  This class implements the BD audio alarm test.  It tests the
//	alarm cable voltage and powerfail capacitor voltage in the
//	alarm-off state, then tests the ability of the powerfail cap to
//	maintain sufficient voltage during the alarm-on state.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the methods required to test the BD audio alarm.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method runTest() is responsible for testing the alarm.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/BdAudioTest.ccv   25.0.4.0   19 Nov 2013 14:23:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date: 07-Oct-1997    DR Number: DCS 2203
//       Project:  Sigma (840)
//       Description:
//			Incorporated new power fail cap discharge procedure.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BdAudioTest.hh"

//@ Usage-Classes

#include "Task.hh"
#include "LinearSensor.hh"
#include "RegisterRefs.hh"
#include "SmManager.hh"
#include "SmPromptId.hh"
#include "VentObjectRefs.hh"
#include "MiscSensorRefs.hh"
#include "VentStatus.hh"

#include "OsTimeStamp.hh"

#include <math.h>

#include "BitAccessGpio.hh"
#include "BinaryCommand.hh"
#include "IODevicesMemoryMap.hh"

#if defined(SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif  // defined(SIGMA_DEVELOPMENT)

#ifdef SIGMA_UNIT_TEST
# include "SmUnitTest.hh"
# include "FakeIo.hh"
# include "FakeControllers.hh"
# include "BdAudioTest_UT.hh"
#endif

//@ End-Usage

//=====================================================================
//
//  Mask for WriteIOPort
//
//====================================================================

//@ Constant: POWERFAIL_CAP_CHARGE_MASK
// mask for the powerfail cap charge bit
const Uint16 POWERFAIL_CAP_CHARGE_MASK = 0x0080 ;

//=====================================================================
//
//  Voltage levels for the alarm cable voltage and the powerfail Cap
//
//====================================================================

// minimum alarm cable voltage, alarm off
const Real32 MIN_CABLE_VOLTAGE = 3.5;	 // V

// maximum alarm cable voltage, alarm off
const Real32 MAX_CABLE_VOLTAGE = 5.05;	 // V

// minimum powerfail cap voltage, alarm off (cap charged)
const Real32 MIN_CAP_CHARGED_VOLTAGE = 4.5;  // V

// minimum powerfail cap voltage, alarm off (cap charged)
const Real32 MAX_CAP_CHARGED_VOLTAGE = 5.05; // V

// minimum RC time constant for alarm circuit
const Real32 MIN_TIME_CONSTANT = 60.0 ;  // sec

// minimum time for alarm-on test phase
const Uint32 MIN_DISCHARGE_TIME = 10000 ;  // msec

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdAudioTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdAudioTest::BdAudioTest(void)
{
	CALL_TRACE("BdAudioTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdAudioTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdAudioTest::~BdAudioTest(void)
{
	CALL_TRACE("~BdAudioTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest()
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.  It is used
//  to test the BD audio alarm.  No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assumption: BD alarm is off.
//
//	$[09156]  $[09181]
//	Read alarm cable voltage and check if in range.
//	Read powerfail capacitor initial voltage and check if in range.
//	Stop charging the capacitor and turn on alarm.
//	Prompt user to indicate if alarm is audible -- ACCEPT = yes,
//	CANCEL = no.
//	Turn off alarm as soon as user responds to prompt unless
//	minimum time did not elapse since alarm was turned on -- in this
//	case wait until minimum time elapses.
//  Read powerfail cap final voltage and check if in range.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BdAudioTest::runTest(void)
{
	CALL_TRACE("BdAudioTest::runTest(void)");

	extern struct AIIOMap *const PAIIOMap ;

	const Real32 MSECS_PER_SEC = 1000.0;

	// setup object allowing powerfail cap enable/disable
// E600 BDIO
#if 0
	BitAccessRegister powerfailCapRegister(
			Register::WORD,
			(Uint16 *)&PAIIOMap->serialReadWriteEeproms,
			INITIAL_SERIAL_WRITE_IMAGE) ;

	BinaryCommand powerfailCapChargerDisable(
			POWERFAIL_CAP_CHARGE_MASK, powerfailCapRegister) ;

	// Charge the powerfail cap
    powerfailCapChargerDisable.setBit( BitAccessGpio::OFF);
	powerfailCapRegister.writeNewCycle() ;
#endif

	// Check the alarm cable voltage
	Real32 alarmCableVoltage = RAlarmCableVoltage.getValue() ;
	RSmManager.sendTestPointToServiceData
		(SmDataId::BD_AUDIO_ALARM_CABLE_VOLTAGE, alarmCableVoltage);

	if ((alarmCableVoltage < MIN_CABLE_VOLTAGE) ||
		(alarmCableVoltage > MAX_CABLE_VOLTAGE))
	{
	// $[TI1]
		// Bad alarm cable voltage
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_1 );
	}
	// $[TI2]
	// implied else

	// Check the power fail cap charged voltage
	Real32 capChargedVoltage = RPowerFailCapVoltage.getValue() ;
	RSmManager.sendTestPointToServiceData
		(SmDataId::BD_AUDIO_POWER_FAIL_CAP_VOLTAGE1, capChargedVoltage);

	if ((capChargedVoltage < MIN_CAP_CHARGED_VOLTAGE) ||
		(capChargedVoltage > MAX_CAP_CHARGED_VOLTAGE))
	{
	// $[TI3]
		// Bad powerfail cap initial voltage
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_2 );
	}
	else	// continue test only if initial cap voltage meets spec
	{
	// $[TI4]
		// Stop charging the powerfail cap
// E600 BDIO        powerfailCapChargerDisable.setBit( BitAccessGpio::ON);
// E600 BDIO		powerfailCapRegister.writeNewCycle() ;

		// Turn on the sound and wait to partially discharge the Cap
	    RVentStatus.turnOnBdAudioAlarm_();

		// wait 100 msecs then read cap voltage
		Task::Delay( 0, 100);
		Real32 capDischargingVoltage0 = RPowerFailCapVoltage.getValue();

		// start counting time
		OsTimeStamp capDischarge;

		// Ask the operator if the sound is on
		RSmManager.promptOperator (SmPromptId::SOUND_PROMPT,
			SmPromptId::ACCEPT_PASS_AND_CANCEL_FAIL_ONLY);
		SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();

		if (operatorAction == SmPromptId::USER_EXIT)
		{
		// $[TI4.1]
			// Operator Exit
			RSmManager.sendOperatorExitToServiceData();
		}
		else if (operatorAction == SmPromptId::KEY_CLEAR)
		{
		// $[TI4.2]
			// audio alarm inaudible
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_4 );
		}
		else
		{
		// $[TI4.3]
			CLASS_ASSERTION ( operatorAction == SmPromptId::KEY_ACCEPT );

			// wait for a minimum discharge time before proceeding
			Uint32 dischargeTimeMsec = capDischarge.getPassedTime();
			while (dischargeTimeMsec < MIN_DISCHARGE_TIME)
			{
			// $[TI4.3.1]
				Task::Delay( 0, SM_CYCLE_TIME);

				dischargeTimeMsec = capDischarge.getPassedTime();

#ifdef SIGMA_UNIT_TEST
				// force a single loop to execute for unit test
				dischargeTimeMsec = MIN_DISCHARGE_TIME;
				SmUnitTest::WhileDoLoopFlag1 = TRUE;
#endif  // SIGMA_UNIT_TEST
			}
			// end while

			// Read the voltage after partial discharge
			Real32 capDischargingVoltage1 = RPowerFailCapVoltage.getValue() ;

			// make sure cap voltage decreased
			if (capDischargingVoltage1 >= capDischargingVoltage0)
			{
			// $[TI4.3.2]
				// cap voltage didn't decrease
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_3 );
			}
			else
			{
			// $[TI4.3.3]
				// cap voltage decreased, so proceed

				// calculate RC time constant
				Real32 timeConstant = (Real32) ((Real32)dischargeTimeMsec / MSECS_PER_SEC /
					log( capDischargingVoltage0 / capDischargingVoltage1));

				RSmManager.sendTestPointToServiceData
					(SmDataId::BD_AUDIO_TIME_CONSTANT, timeConstant);

				if (timeConstant < MIN_TIME_CONSTANT)
				{
				// $[TI4.3.3.1]
					// cap discharged too quickly
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_FAILURE,
						SmStatusId::CONDITION_3 );
				}
				// $[TI4.3.3.2]
				// implied else
			}
			// end else
		}
		// end else

		// Turn off the sound and charge the powerfail cap
		RVentStatus.turnOffBdAudioAlarm_();
// E600 BDIO        powerfailCapChargerDisable.setBit( BitAccessGpio::OFF);
// E600 BDIO		powerfailCapRegister.writeNewCycle() ;
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
BdAudioTest::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, BDAUDIOTEST,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
