#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendGraphsSubScreen - Trending Graphs sub-screen 
//---------------------------------------------------------------------
//@ Interface-Description 
//  The TrendGraphsSubScreen is a TrendSubScreen that displays a
//  graphical view of the trending data. It contains three plots of
//  trend data and the controls for selecting each trend parameter and
//  a time scale setting for all plots. It presents a graphical view
//  of trend data using a line plot to show trend sample data collected
//  over time.
// 
//  This class contains the setting buttons and scrollable menus to
//  select up to three different trend parameters and a timescale
//  selection button and menu. It also contains a trend cursor slider
//  that allows the user to select a single sample time along the time
//  axis for which the numeric data values is displayed to the right of
//  each plot.
// 
//  The UpperSubScreenArea instantiates this class. The trending tab
//  on the upper screen activates this sub-screen or the
//  TrendTableSubScreen when the tab button is pressed down. The
//  TrendSubScreen controls which trend sub-screen is activated by
//  setting the sub-screen pointer in the tab button to the last
//  TrendSubScreen selected by the operator. The trend graphs is
//  selected as the initial view of trend data.
// 
//  This class is a TrendSubScreen that provides definitions for the 
//  pure virtual methods isAnyButtonPressed_, requestTrendData_,
//  updateCursorPosition, disableScrolling and update to provide the
//  TrendSubScreen functionality. It also overloads many of the callback
//  virtual methods such as valueUpdate defined in TrendSubScreen to
//  provide specialized processing of these events. In most cases, the
//  overloaded method also calls the base class method to process these
//  events as well. In this way, the base class method provides the same
//  "common" processing of these events for all TrendSubScreens.
// 
//  This class is a TrendEventTarget that inherits the trendDataReady
//  method. This class registers with the TrendDataSetAgent class for
//  callbacks to this method when new trend data is available from the
//  agent.
// 
//---------------------------------------------------------------------
//@ Rationale 
//  Encapsulates the display objects and methods specific to the Trend
//  Graphs sub-screen.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The TrendGraphsSubScreen is a TrendSubScreen that is directly
//  instantiated in the UpperSubScreenArea. As a TrendSubScreen it
//  passes references to several of its contained Drawables to the base
//  class during construction. In this way, the TrendSubScreen base
//  class has access to these objects without having to instantiate
//  the objects as protected members in the base class and then require
//  the derived class to move and resize them according to its specific
//  requirements. In some cases, the object attributes can only be
//  defined in the constructor so this requires the object be
//  instantiated in the derived class or add "set" accessor methods to
//  change the object's constructed attributes. Instead of implementing
//  a new set of the "set" methods, we decided to instantiate in the
//  derived class and pass references to the base class.
// 
//  This class contains many objects that use the DirectDraw feature of
//  the graphics library. DirectDraw allows for drawing objects directly
//  in the frame buffer without going through the standard
//  GUI-Foundations repaint cycle. This provided the necessary
//  performance enhancements for the graphical elements of the trending
//  sub-screen, but placed much of the burden for managing the
//  overlapping graphics elements and exposure events back on the
//  application. This complicated the implementation of this class
//  considerably. This class uses numerous flags to assure that one
//  direct draw graphic doesn't write over another and when it does, the
//  underlying graphic is repainted when exposed. This implementation
//  could be simplified with an enhanced graphics library that managed
//  DirectDraw graphics more transparently.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  No special fault handling strategies apart from the usual assertions
//  and pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//	none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendGraphsSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6461
//  Project:  840S
//  Description:
//      Implemented #define workaround for compiler bug.
//
//  Revision: 002  By: gdc		Date:  11-Aug-2008   SCR Number: 6439
//  Project:  840S
//  Description:
//  	Removed touch screen "drill-down" work-around.
//
//  Revision: 001  By: gdc		Date:  30-Jun-2007   SCR Number: 6237
//  Project:  Trend
//  Description:
//  	Initial Version
//=====================================================================

#include "TrendGraphsSubScreen.hh"

//@ Usage-Classes

#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "MiscStrs.hh"
#include "SettingId.hh"
#include "SettingSubject.hh"
#include "TrendDataSetAgent.hh"
#include "TrendFormatData.hh"
#include "TrendSelectMenuItems.hh"
#include "TrendSelectValue.hh"
#include "TrendTimeScaleMenuItems.hh"
#include "UpperSubScreenArea.hh"

// TODO E600 removed
//#include "Ostream.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 BUTTON_Y1_ = 69;
static const Int32 BUTTON_Y2_ = 134;
static const Int32 BUTTON_Y3_ = 199;

static const Int32 BUTTON_WIDTH_ = 84;
static const Int32 BUTTON_HEIGHT_ = 34;
static const Int32 BUTTON_X1_ = 3;
static const Int32 BUTTON_BORDER_ = 2;

static const Int32 SELECT_MENU_X1_ = 90;
static const Int32 SELECT_MENU_Y1_ = 30;
static const Int32 SELECT_MENU_X2_ = 90;
static const Int32 SELECT_MENU_Y2_ = 75;
static const Int32 SELECT_MENU_X3_ = 90;
static const Int32 SELECT_MENU_Y3_ = 120;

static const Int32 TREND_TABLE_SCREEN_NAV_X_ = 1;
static const Int32 TREND_BUTTON_X_ = 1;
static const Int32 TREND_BUTTON_Y_ = 1;
static const Int32 TREND_BUTTON_WIDTH_ = 60;
static const Int32 TREND_BUTTON_HEIGHT_ = 30;


static const Int32 TIME_PLOT_X_ = 0;
static const Int32 PLOT1_Y_ = BUTTON_Y1_ - 14;
static const Int32 PLOT2_Y_ = BUTTON_Y2_ - 14;
static const Int32 PLOT3_Y_ = BUTTON_Y3_ - 14;

static const Int32 TIME_PLOT_WIDTH_ = 600;

static const Int32 TIME_PLOT_GRID_X_ = 136;
static const Int32 TIME_PLOT_GRID_Y_ = 5;
static const Int32 TIME_PLOT_GRID_WIDTH_ = TrendDataMgr::MAX_ROWS + 1;
static const Int32 SMALL_TIME_PLOT_GRID_HEIGHT_ = 60;
static const Int32 SMALL_TIME_PLOT_HEIGHT_ = SMALL_TIME_PLOT_GRID_HEIGHT_ + 27;

static const Int32 TIME_SCALE_BUTTON_X_ = 585;
static const Int32 TIME_SCALE_BUTTON_Y_ = 1;
static const Int32 TIME_SCALE_BUTTON_WIDTH_ = 50;
static const Int32 TIME_SCALE_BUTTON_HEIGHT_ = TREND_BUTTON_HEIGHT_;

static const Int32 TIME_SCALE_MENU_X_ = TIME_SCALE_BUTTON_X_;
static const Int32 TIME_SCALE_MENU_Y_ = 30;

static const Int32 EVENT_BOX_X_ = 0;
static const Int32 EVENT_BOX_Y_ = 0;
static const Int32 EVENT_BOX_WIDTH_ = 80;
static const Int32 EVENT_LABEL_HEIGHT_ = 11;
static const Int32 EVENT_BOX_LINE_WIDTH_ = 1;

static const Int32 CURSOR_LINE_LENGTH_ = SMALL_TIME_PLOT_GRID_HEIGHT_ * 3 + 9;

static const Int32 TREND_GRAPH_EVENT_SCROLL_X_ = 245;
static const Int32 TREND_GRAPH_EVENT_SCROLL_Y_ = 143;
static const Int32 TREND_EVENT_SCROLLBAR_WIDTH_ = 10;

static SubScreen* const NULL_FOCUS_SUBSCREEN_ = NULL;
static const Uint PRESET_DROP_DOWN_TEXT_SIZE_ = 8;

// NULL used instead of NULL_STRING_ID as workaround to compiler bug
static const StringId NULL_BUTTON_TITLE_ = NULL;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendGraphsSubScreen()  [Constructor]
//
//@ Interface-Description
//	Constructor.  The 'pSubScreenArea' parameter is the pointer to the
//	parent SubScreenArea which contains this subscreen (the Upper
//	subscreen area).
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes along the SubScreenArea pointer and references to several
//  contained objects to the base class so it can manage these objects
//  similarly for all TrendSubScreens. Initializes all members and
//  initializes the colors, sizes, and positions of all buttons and the
//  subscreen title. Adds all Drawables to this container for display.
//  Registers for button callbacks. Drawables are constructed here and
//  passed as references to the base class instead of constructing them
//  in the base class so their size and positon can be set once in their
//  own constructor.
// 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendGraphsSubScreen::TrendGraphsSubScreen(SubScreenArea* pSubScreenArea) 
:   TrendSubScreen(         pSubScreenArea, 
							tableScreenNavButton_, 
							printButton_, 
							printingText_,
							eventDetailButton_,
							presetsButton_),

	tableScreenNavButton_(	TREND_TABLE_SCREEN_NAV_X_,
							TREND_BUTTON_Y_,
							TREND_BUTTON_WIDTH_,                    
							TREND_BUTTON_HEIGHT_,
							Button::LIGHT,                          
							BUTTON_BORDER_, 
							Button::SQUARE,                         
							Button::NO_BORDER, 
							MiscStrs::TREND_TABLE_SCREEN_NAV_TITLE,
							NULL_STRING_ID, 
							GRAVITY_CENTER),
	presetsButton_(         TREND_BUTTON_X_ + TREND_BUTTON_WIDTH_,
							TREND_BUTTON_Y_, 
							TREND_BUTTON_WIDTH_,
							TREND_BUTTON_HEIGHT_,
							Button::LIGHT, 
							BUTTON_BORDER_, 
							Button::ROUND,
							Button::NO_BORDER,
							MiscStrs::TREND_PRESET_BUTTON_TITLE,
							SettingId::TREND_PRESET, 
							NULL_FOCUS_SUBSCREEN_,
							MiscStrs::TREND_PRESET_BUTTON_HELP_MSG, 
							PRESET_DROP_DOWN_TEXT_SIZE_),
	printButton_(           TREND_BUTTON_X_ + 2* TREND_BUTTON_WIDTH_, 
							TREND_BUTTON_Y_,
							TREND_BUTTON_WIDTH_,                    
							TREND_BUTTON_HEIGHT_,
							Button::LIGHT,                          
							BUTTON_BORDER_, 
							Button::SQUARE,                         
							Button::NO_BORDER, 
							MiscStrs::TREND_PRINT_BUTTON_TITLE),
	timeScaleButton_(       TIME_SCALE_BUTTON_X_,       
							TIME_SCALE_BUTTON_Y_,
							TIME_SCALE_BUTTON_WIDTH_,   
							TIME_SCALE_BUTTON_HEIGHT_,
							Button::LIGHT,              
							BUTTON_BORDER_,
							Button::ROUND,              
							Button::NO_BORDER,
							NULL_STRING_ID, 
							SettingId::TREND_TIME_SCALE, 
							ScrollableMenu::GetTrendTimeScaleMenu(),
							TrendTimeScaleMenuItems::GetMenuItems(),
							TIME_SCALE_MENU_X_,
							TIME_SCALE_MENU_Y_),
	printingText_(          MiscStrs::PRINTING_TITLE),
	trendSelectButton1_(    BUTTON_X1_,         
							BUTTON_Y1_,
							BUTTON_WIDTH_,      
							BUTTON_HEIGHT_, 
							Button::LIGHT,      
							BUTTON_BORDER_, 
							Button::ROUND,      
							Button::NO_BORDER,
							NULL_BUTTON_TITLE_,
							SettingId::TREND_SELECT_1, 
							ScrollableMenu::GetTrendSelectMenu(),
							TrendSelectMenuItems::GetMenuItems(),
							SELECT_MENU_X1_,
							SELECT_MENU_Y1_),
	trendSelectButton2_(    BUTTON_X1_,         
							BUTTON_Y2_,
							BUTTON_WIDTH_,      
							BUTTON_HEIGHT_, 
							Button::LIGHT,      
							BUTTON_BORDER_, 
							Button::ROUND,      
							Button::NO_BORDER, 
							NULL_BUTTON_TITLE_,
							SettingId::TREND_SELECT_2, 
							ScrollableMenu::GetTrendSelectMenu(),
							TrendSelectMenuItems::GetMenuItems(),
							SELECT_MENU_X2_,
							SELECT_MENU_Y2_),
	trendSelectButton3_(    BUTTON_X1_,         
							BUTTON_Y3_,
							BUTTON_WIDTH_,      
							BUTTON_HEIGHT_, 
							Button::LIGHT,      
							BUTTON_BORDER_, 
							Button::ROUND,      
							Button::NO_BORDER, 
							NULL_BUTTON_TITLE_,
							SettingId::TREND_SELECT_3,
							ScrollableMenu::GetTrendSelectMenu(),
							TrendSelectMenuItems::GetMenuItems(),
							SELECT_MENU_X3_,
							SELECT_MENU_Y3_),
	eventDetailButton_(     TIME_SCALE_BUTTON_X_ - TREND_BUTTON_WIDTH_, 
							TREND_BUTTON_Y_,
							TREND_BUTTON_WIDTH_,    
							TREND_BUTTON_HEIGHT_, 
							Button::LIGHT,          
							BUTTON_BORDER_, 
							Button::SQUARE,         
							Button::NO_BORDER, 
							MiscStrs::TREND_EVENT_DETAIL),
	trendPlot1_(            ),
	trendPlot2_(            ),
	trendPlot3_(            ),
	eventContainer_(        ),
	eventBox_(              EVENT_BOX_X_, 
							EVENT_BOX_Y_, 
							EVENT_BOX_WIDTH_, 
							TREND_BUTTON_HEIGHT_, 
							EVENT_BOX_LINE_WIDTH_),
	eventLabel_(            MiscStrs::TREND_EVENT_ID_LABEL),
	eventCell_(             ),
	trendCursorSlider_(     this),
	isDataRequestPending_(  FALSE),
	isCallbackActive_(      FALSE)
{

	// Size and position the area
	setX(0);
	setY(0);
	setWidth(UpperSubScreenArea::CONTAINER_WIDTH);
	setHeight(UpperSubScreenArea::CONTAINER_HEIGHT);

	// Set subscreen background color
	setFillColor(Colors::BLACK);

	printingText_.setColor(Colors::WHITE_ON_EXTRA_DARK_GREY_BLINK);
	printingText_.setShow(FALSE);
	addDrawable(&printingText_);

	// add the drawables, size and position the eventContainer_
	// the box is hidden but we use it to size the container 
	eventBox_.setColor(Colors::BLACK);
	eventLabel_.setColor(Colors::YELLOW);
	eventLabel_.setY(EVENT_BOX_LINE_WIDTH_ + 1);

	eventCell_.setX(EVENT_BOX_LINE_WIDTH_);
	eventCell_.setY(EVENT_BOX_LINE_WIDTH_ + EVENT_LABEL_HEIGHT_);
	eventCell_.setWidth(eventBox_.getWidth() - 2 * EVENT_BOX_LINE_WIDTH_);
	eventCell_.setHeight(eventBox_.getHeight() - 2 * EVENT_BOX_LINE_WIDTH_ - EVENT_LABEL_HEIGHT_ - 1);
	eventCell_.setContentsColor(Colors::YELLOW);

	eventContainer_.addDrawable(&eventBox_);
	eventContainer_.addDrawable(&eventCell_);

	eventContainer_.setX( eventDetailButton_.getX() - EVENT_BOX_WIDTH_ );
	eventContainer_.setY( TREND_BUTTON_Y_ );
	eventContainer_.sizeToFit();

	eventContainer_.addDrawable(&eventLabel_);
	eventLabel_.positionInContainer(::GRAVITY_CENTER);

	addDrawable(&eventContainer_);

	// position the cursor slider to overlap the plot slightly
	trendCursorSlider_.setShowAxis(FALSE);
	const Int32 CURSOR_CONTAINER_Y_ = PLOT1_Y_ - trendCursorSlider_.getHeight() + 26;
	const Int32 CURSOR_CONTAINER_X_ = 104;

	trendCursorSlider_.setX(CURSOR_CONTAINER_X_);
	trendCursorSlider_.setY(CURSOR_CONTAINER_Y_);
	trendCursorSlider_.setCursorLineLength(CURSOR_LINE_LENGTH_);

	// Add Average Trend Data Label.
	for (Uint32 index = 0; index < NUM_TREND_PLOTS_; index++)
	{
		pAvgTrendDataValue_[index].setColor(Colors::WHITE );
		pAvgTrendDataValue_[index].setX( TIME_PLOT_GRID_X_ + TIME_PLOT_GRID_WIDTH_ + 50);
		pAvgTrendDataValue_[index].setY( PLOT1_Y_ + index * (PLOT2_Y_ - PLOT1_Y_) + 30);   

		pAvgTrendDataValue_[index].setShow(FALSE );
		addDrawable(&pAvgTrendDataValue_[index] );
	}

	// point the presets menu to the preset strings set up in the base class
	presetsButton_.setValueInfo(TrendSubScreen::PresetsTypeInfo_);

	//
	// Add the buttons.
	//
	addDrawable(&tableScreenNavButton_);
	addDrawable(&printButton_);
	addDrawable(&trendCursorSlider_ );
	addDrawable(&eventDetailButton_);

	// set callbacks to this class.
	tableScreenNavButton_.setButtonCallback(this);
	printButton_.setButtonCallback(this);
	eventDetailButton_.setButtonCallback(this);

	// set persistent 
	eventDetailButton_.usePersistentFocus();

	// Position and size waveform plot objects
	trendPlot1_.setY(PLOT1_Y_);
	trendPlot2_.setY(PLOT2_Y_);
	trendPlot3_.setY(PLOT3_Y_);

	Int32 i = 0;
	trendPlots_[i++] = &trendPlot1_;
	trendPlots_[i++] = &trendPlot2_;
	trendPlots_[i++] = &trendPlot3_;
	CLASS_ASSERTION(i == NUM_TREND_PLOTS_)

	for (Int32 j=0; j<NUM_TREND_PLOTS_; j++)
	{
		trendPlots_[j]->setX(TIME_PLOT_X_);
		trendPlots_[j]->setGridArea(TIME_PLOT_GRID_X_, TIME_PLOT_GRID_Y_,
									TIME_PLOT_GRID_WIDTH_, SMALL_TIME_PLOT_GRID_HEIGHT_);
		trendPlots_[j]->setWidth(TIME_PLOT_WIDTH_);
		trendPlots_[j]->setHeight(SMALL_TIME_PLOT_HEIGHT_);
		trendPlots_[j]->setY1Limits(100, 0);
		trendPlots_[j]->showXAxisValues( FALSE);
		addDrawable(trendPlots_[j]);
		trendPlots_[j]->activate();

		// The graph sub-screen uses the first three dataset columns in the
		// TrendDataSet for its trend parameter selections corresponding to the
		// three plots
		datasetColumn_[j] = j;
	}

	// display the event markers
	trendPlot1_.setShowEvents(FALSE);
	trendPlot2_.setShowEvents(TRUE);
	trendPlot3_.setShowEvents(TRUE);

	// show the x-axis labels on the bottom graph
	trendPlot3_.showXAxisValues(TRUE);

	Int8 idx = 0;
	arrSettingButtonPtrs_[idx++] = &trendSelectButton1_;
	arrSettingButtonPtrs_[idx++] = &trendSelectButton2_;
	arrSettingButtonPtrs_[idx++] = &trendSelectButton3_;
	arrSettingButtonPtrs_[idx++] = &timeScaleButton_;
	arrSettingButtonPtrs_[idx++] = &presetsButton_;
	arrSettingButtonPtrs_[idx] = NULL;                                            

	AUX_CLASS_ASSERTION((idx <= NUM_TREND_SETTING_BUTTONS), idx);

	// Add the buttons to the subscreen
	for (idx = 0u; arrSettingButtonPtrs_[idx] != NULL; idx++)
	{
		arrSettingButtonPtrs_[idx]->setButtonCallback(this);
		arrSettingButtonPtrs_[idx]->usePersistentFocus();
		arrSettingButtonPtrs_[idx]->setSingleSettingAcceptEnabled(TRUE);
		addDrawable(arrSettingButtonPtrs_[idx]);
	}
	trendSelectButton1_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	trendSelectButton2_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	trendSelectButton3_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	timeScaleButton_.setValueTextPosition(::GRAVITY_VERTICAL_CENTER);
	presetsButton_.setValueTextPosition(::GRAVITY_BOTTOM);
	presetsButton_.setDropDownTextSize(8);

	// set up location of event detail window
	eventDetailLog_.setX(TREND_GRAPH_EVENT_SCROLL_X_);
	eventDetailLog_.setY(TREND_GRAPH_EVENT_SCROLL_Y_);

	// Register this class to the trend data set agent
	TrendDataSetAgent::RegisterTarget(this);  

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendGraphsSubScreen  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	none
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendGraphsSubScreen::~TrendGraphsSubScreen(void)   
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate() [virtual]
//
//@ Interface-Description
//	This SubScreen virtual method prepares this subscreen for display.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Calls the base class activate method first. Activates all buttons
//  and the cursor slider. Requests updated trend data from the
//  Trend-Database.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendGraphsSubScreen::activate(void)
{
	TrendSubScreen::activate();

	// Activate all buttons
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::activate);

	trendCursorSlider_.activate();

	for (Int32 plot = 0; plot < NUM_TREND_PLOTS_; plot++)
	{
		isPlotErased_[plot] = FALSE;
	}
	areAllPlotsErased_ = FALSE;

	// Update the plots with data.
	requestTrendData();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate() [virtual]
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Deactivates the cursor slider and all settings buttons. Calls the
//  base class deactivate.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendGraphsSubScreen::deactivate(void)
{
	trendCursorSlider_.deactivate();

	// deactivate all setting buttons...
	operateOnButtons_(arrSettingButtonPtrs_, &SettingButton::deactivate);

	TrendSubScreen::deactivate();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
//  This ButtonTarget virtual method is called when any button registed
//  with this sub-screen is pressed down by the operator
//  (byOperatorAction is TRUE) or when Button::setToDown() is called.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the TABLE navigation button is pressed then call displayTable_()
//  to switch to the tabular view of the trend data.
// 
//  When any of the trend parameter selection buttons are pressed down
//  then inhibit plot refresh (by setting isCallbackActive_ TRUE) since
//  the selection menus that are drawn directly in the frame buffer
//  overlay the graphs when these buttons are pressed.
// 
//  If the event detail button is pressed then call
//  updateEventDetailWindow() to build the contents of the event log and
//  activate the pop-up window.
// 
//  Passes the button down event on to the TrendSubScreen base class for
//  processing common to all TrendSubScreens.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendGraphsSubScreen::buttonDownHappened(Button *pButton, Boolean byOperatorAction)
{

	if (pButton == &tableScreenNavButton_)
	{
	    // There is no reason to set this button to up 
		// when we go into another subscreen.
		//pButton->setToUp();  
		displayTable_();
	}
	else if ((pButton == &timeScaleButton_   ) || 
			 (pButton == &trendSelectButton1_) ||
			 (pButton == &trendSelectButton2_) || 
			 (pButton == &trendSelectButton3_) ||
			 (pButton == &eventDetailButton_) ||
			 (pButton == &presetsButton_     ) 
			)
	{
		// menu buttons inhibit data refresh
		isCallbackActive_ = TRUE;

		if (pButton == &eventDetailButton_)
		{
			isEventButtonDown_ = TRUE;
			updateEventDetailWindow_();

			if (eventDetailLog_.isScrollbarVisible())
			{
				eventDetailLog_.setX(TREND_GRAPH_EVENT_SCROLL_X_);
			}
			else
			{
				eventDetailLog_.setX(TREND_GRAPH_EVENT_SCROLL_X_ + TREND_EVENT_SCROLLBAR_WIDTH_);
			}

			addDrawable(&eventDetailLog_);
			eventDetailLog_.activate();
		}

	}


	// let the base class process the event as well
	TrendSubScreen::buttonDownHappened(pButton, byOperatorAction);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
//  This ButtonTarget virtual method is called when any button registed
//  with this sub-screen is pressed up by the operator
//  (byOperatorAction is TRUE) or when Button::setToUp() is called.
//---------------------------------------------------------------------
//@ Implementation-Description
// 
//  When any of the trend parameter selection buttons are pressed up
//  this method reenables sub-screen refresh (by setting
//  isCallbackActive_ FALSE).
// 
//  If the event detail button is pressed up then deactive the window
//  and remove it from this sub-screen.
// 
//  Upon receiving the button up event, if a trend parameter setting has
//  changed, then attempt to make a request for new trend data. If
//  there's been no change in the trend parameters then refresh the
//  screen with the current data. The pop-up menus have exposed the
//  underlying plots that must now be refreshed.
// 
//  Passes the button up event on to the TrendSubScreen base class for
//  processing common to all TrendSubScreens.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TrendGraphsSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	if ((pButton == &timeScaleButton_) || 
		(pButton == &trendSelectButton1_) ||
		(pButton == &trendSelectButton2_) || 
		(pButton == &trendSelectButton3_) ||
		(pButton == &eventDetailButton_) ||
		(pButton == &presetsButton_)
	   )
	{
		isCallbackActive_ = FALSE;

		if ((pButton == &eventDetailButton_) && isEventButtonDown_)
		{
			isEventButtonDown_ = FALSE;
			eventDetailLog_.deactivate();
			removeDrawable( &eventDetailLog_ );
		}

		if (isSettingChanged_)
		{
			// request new trend data
			if (requestTrendData() == TrendSubScreen::REQUEST_ACCEPTED)
			{
				isSettingChanged_ = FALSE;
			}
		}
		else
		{
			// just refresh the screen without updating the dataset
			update();
		}
	}


	// let the base class process the event as well
	TrendSubScreen::buttonUpHappened(pButton, byOperatorAction);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isAnyButtonPressed_
//
//@ Interface-Description 
//  This TrendSubScreen pure virtual method is called by the
//  TrendSubScreen before starting a trend database request to assure
//  that the user interface is inactive.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method checks the button state from the Button class instead of
//  using some sort of semaphore in the buttonDownHappened callback
//  since the callback occurs after the button actually goes down
//  thereby opening a window between the time the first button goes up
//  and the second goes down when a data request could be started with
//  the user interface still active. Using the button state eliminates
//  this window.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean TrendGraphsSubScreen::isAnyButtonPressed_(void) const
{
	return( (timeScaleButton_.getButtonState()		== Button::DOWN) ||
			(trendSelectButton1_.getButtonState()	== Button::DOWN) ||
			(trendSelectButton2_.getButtonState()	== Button::DOWN) ||
			(trendSelectButton3_.getButtonState()	== Button::DOWN) ||
			(presetsButton_.getButtonState()     	== Button::DOWN) ||
			(eventDetailButton_.getButtonState() 	== Button::DOWN) ||
			(trendCursorSlider_.isCursorActive() )
		  );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: trendDataReady
//
//@ Interface-Description
//  This TrendEventRegistrar virtual method is called by
//  TrendDataSetAgent when new trend data is ready to be displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The buttons that were disabled during the dataset request are now
//  reactivated. Calls update() to refresh the graphs with new data.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendGraphsSubScreen::trendDataReady(TrendDataSet& rTrendDataSet)
{
	tableScreenNavButton_.setToUp();
	timeScaleButton_.setToUp();
	trendSelectButton1_.setToUp();
	trendSelectButton2_.setToUp();
	trendSelectButton3_.setToUp();
	presetsButton_.setToUp();
	eventDetailButton_.setToUp();
	trendCursorSlider_.enableCursor();

	isDataRequestPending_ = FALSE;

	update();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: update
//
//@ Interface-Description
//  Called to refresh the contents of the graphs with the contents of
//  the current TrendDataSet.
// 
//---------------------------------------------------------------------
//@ Implementation-Description 
// 
//  In general, this method is called to refresh the graphs any time a
//  button on the sub-screen is popped up. This manual refresh is
//  required because the trend cursor, pop-up menus and the event detail
//  window activated by this sub-screen's buttons are drawn directly
//  into the frame buffer. The background repaint mechanism contained in
//  GUI-Foundations does not work with objects drawn directly in the
//  frame buffer because these objects are never included in a list of
//  changed Drawables in the BaseContainer so the underlying objects
//  never receive a call to be refreshed. Additionally, the graphs
//  themselves are drawn directly in the frame buffer using DirectDraw
//  so they would never receive a exposure event since they're never
//  included in a list of Drawables to be refreshed when exposed.
// 
//  Several conditions inhibit refreshing the graphs. These conditions
//  include: when an active pop-up menu or event log overlays the
//  graph, when the user is scrolling through the graph with the trend
//  cusor, and while receiving new trend data.
// 
//  When refresh is inhibited, this method starts the display retry
//  timer so the refresh can be tried again later.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendGraphsSubScreen::update(void)
{
	if (!isVisible())
	{
		return;
	}

	if (isCallbackActive_)
	{
		// to prevent refresh when a menu overlays the graphs
		GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DISPLAY_RETRY_TIMER);
	}
	else if (trendCursorSlider_.isCursorLineShown())
	{
		// To prevent refresh while scrolling the graph with the trend cursor
		// This prevents multiple refresh requests caused by the event detail
		// button popping up/down while scrolling.
		GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DISPLAY_RETRY_TIMER);
	}
	else if (isDataRequestPending_)
	{
		// to prevent multiple refresh when buttons are set up during trendDataReady
		GuiTimerRegistrar::StartTimer(GuiTimerId::TREND_DISPLAY_RETRY_TIMER);
	}
	else
	{
		// cancel the retry timer in case we're able to refresh before the timer expires
		GuiTimerRegistrar::CancelTimer(GuiTimerId::TREND_DISPLAY_RETRY_TIMER);

		for (Int32 plot = 0; plot < NUM_TREND_PLOTS_; plot++)
		{
			trendPlots_[plot]->update(rTrendDataSetAgent_.getTrendDataSet(), datasetColumn_[plot]);
			pAvgTrendDataValue_[plot].setShow(TRUE);
			isPlotErased_[plot] = FALSE;
		}
		updateCursorPosition();
		areAllPlotsErased_ = FALSE;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//  Called when any registered setting has changed.
//
//  qualifierId		The context in which the change happened, either
//                  ACCEPTED or ADJUSTED.
// 
//  pSubject		The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Called when any of the observed trend setting parameters have
//  changed. Erases the plot of the changed trend parameter
//  If the time scale setting has changed, erases all plots. We erase
//  the plots before requesting new data from the database so the
//  setting displayed isn't mismatched with old plot data from the
//  previous setting.
// 
//  Forwards this event onto the TrendSubScreen base class for handling
//  setting changes common to all TrendSubScreens.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendGraphsSubScreen::valueUpdate(const Notification::ChangeQualifier qualifierId,
									   const SettingSubject* pSubject)
{
	if (!isVisible())
	{
		return;
	}

	const SettingId::SettingIdType  SETTING_ID = pSubject->getId();

	if (qualifierId == Notification::ACCEPTED)
	{
		if (SETTING_ID == SettingId::TREND_SELECT_1 && !isPlotErased_[0])
		{
			trendPlot1_.erase();
			pAvgTrendDataValue_[0].setShow(FALSE);
			isPlotErased_[0] = TRUE;
		}
		else if (SETTING_ID == SettingId::TREND_SELECT_2 && !isPlotErased_[1])
		{
			trendPlot2_.erase();
			pAvgTrendDataValue_[1].setShow(FALSE);
			isPlotErased_[1] = TRUE;
		}
		else if (SETTING_ID == SettingId::TREND_SELECT_3 && !isPlotErased_[2])
		{
			trendPlot3_.erase();
			pAvgTrendDataValue_[2].setShow(FALSE);
			isPlotErased_[2] = TRUE;
		}
		else if (SETTING_ID == SettingId::TREND_TIME_SCALE && !areAllPlotsErased_)
		{
			trendPlot1_.erase();
			trendPlot2_.erase();
			trendPlot3_.erase();
			pAvgTrendDataValue_[0].setShow(FALSE);
			pAvgTrendDataValue_[1].setShow(FALSE);
			pAvgTrendDataValue_[2].setShow(FALSE);
			areAllPlotsErased_ = TRUE;
		}
	}

	TrendSubScreen::valueUpdate(qualifierId,pSubject);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: requestTrendData_ [protected]
//
//@ Interface-Description
//	Requests new trend data via the TrendDataSetAgent.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method disables the user interface by making all sub-screen
//  buttons flat before signaling the TrendDataSetAgent to update its
//  dataset. It disables the user interface so the user isn't looking
//  at data as it's changing. Database retrieval time is perceptable but
//  acceptably short (~1 second). This could be enhanced by double
//  buffering the requests and dataset responses.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void TrendGraphsSubScreen::requestTrendData_()
{
	// make menus/cursor/data unavailable until database request completes
	tableScreenNavButton_.setToFlat();
	timeScaleButton_.setToFlat();
	trendSelectButton1_.setToFlat();
	trendSelectButton2_.setToFlat();
	trendSelectButton3_.setToFlat();
	presetsButton_.setToFlat();
	eventDetailButton_.setToFlat();
	eventCell_.setShow(FALSE);
	trendCursorSlider_.disableCursor();

	rTrendDataSetAgent_.requestTrendData(); 

	isDataRequestPending_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateCursorPosition
//
//@ Interface-Description 
//  This method is called to update the drawables that change with the
//  position of the trend cursor. This method updates the data value to
//  the right of each plot with the value in the dataset at the cursor's
//  current location. It also updates the event drawables for that
//  location.
// 
//  The TrendSubScreen calls this method when it receives a setting
//  change callback for the trend cursor position.
// 
//  The update method calls this method when refreshing the graphs.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  The TrendDataSetAgent provides the current cursor position on the
//  screen relative to newest data on the right-hand side of the graph.
//  It also provides the frame number in the database where the cursor
//  is located and access to the current TrendDataSet. Using this
//  information, this method retrieves the value at the cursor location
//  from the TrendDataSet and sets the numeric fields to this value.
// 
//  If the trend parameter selected by the operator is not available in
//  the current option set, this method displays "N/A" for its value.
//  For gapped data, it displays dashes.
// 
//  This method uses TrendFormatData to format the numeric data
//  according to its specifications and uses updateEventDrawables_ to
//  present the Trend Event box at the top of the display.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  Since this method retrieves the cursor location and frame number
//  from the TrendDataSet, the TrendDataSetAgent::valueUpdate setting
//  callback must be called before this method to update those variables
//  when the trend cursor setting changes. The order of instantiation
//  guarantees this sequencing since the TrendSubScreen instantiates the
//  TrendDataSet as part of its construction before it registers for the
// 	settings callback.
// 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void TrendGraphsSubScreen::updateCursorPosition(void)
{
	// Ignore event is we're invisible
	if (!isVisible() || isDataRequestPending_)
	{
		return;
	}

	// The cursor position is the displacement from the right hand side of
	// the graph. It also represents the row in the dataset that we're
	// trying to retrieve so we'll call the cursor position a "row" in this
	// context.

	Int32 row = rTrendDataSetAgent_.getCursorPosition();
	Int32 cursorFrame = rTrendDataSetAgent_.getCursorFrame();

	const TrendDataSet& rTrendDataSet = rTrendDataSetAgent_.getTrendDataSet();

	for (Uint32 plot = 0; plot < NUM_TREND_PLOTS_; plot++)
	{
		static wchar_t tmpBuffer1[40];
		static wchar_t tmpBuffer2[40];

		// each data column represents one of the trend parameter selections
		const TrendDataValues& rTrendDataValues = rTrendDataSet.getTrendDataValues(datasetColumn_[plot]);
		const TrendValue & rTrendValue = rTrendDataValues.getValue( row );
		TrendSelectValue::TrendSelectValueId colId = rTrendDataValues.getId();

		// The TrendDataSetAgent sets the column ID to TREND_DATA_NOT_AVAILABLE
		// if the data requested is not available in the current option set
		if (colId == TrendSelectValue::TREND_DATA_NOT_AVAILABLE)
		{
			// option not enabled therefore data not available
			swprintf( tmpBuffer1, L"N/A");
		}
		else if (colId == TrendSelectValue::TREND_EMPTY ||
				 !rTrendValue.isValid)
		{
			swprintf( tmpBuffer1, L"------" );
		}
		else
		{
			TrendFormatData::FormatData(tmpBuffer1, colId, rTrendValue.data.realValue);
		}

		swprintf( tmpBuffer2, L"{p=12,y=14:%s}", tmpBuffer1 );

		pAvgTrendDataValue_[plot].setText( tmpBuffer2 );
		pAvgTrendDataValue_[plot].setShow(TRUE );
	}

	updateEventDrawables_();

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: disableScrolling
//
//@ Interface-Description
//  This method is called to disable scrolling through the trend data
//  using the trend cursor. The TrendSubScreen base class calls this
//  method when any one trend parameter changes. This prevents the user
//  from looking through data that may not match the current set of
//  trend parameters shown on the display. It also forces the user to
//  accept any trend parameter changes before pressing the scrolling
//  button that will grab focus and postpone the dataset update required
//  to get the data in sync with the displayed settings.
//---------------------------------------------------------------------
//@ Implementation-Description 
//	Disables the trend cursor slider.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendGraphsSubScreen::disableScrolling(void)
{
	trendCursorSlider_.disableCursor();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateEventDrawables_
//
//@ Interface-Description 
//  This method is called to update the Trend Event related drawables on
//  the Trend Graph sub-screen. It displays the event numbers in the
//  event box at the top of the graphs sub-screen when one or more
//  events are stored in the data at the current cursor location.
//---------------------------------------------------------------------
//@ Implementation-Description 
//  This method gets the cursor location and Trend dataset from
//  the TrendDataSetAgent. It sets the event detail button up when it
//  detects one or more events stored in the trend dataset at the row
//  referenced by the cursor's position on the graph. Translates the
//  internally stored event numbers to a user representation using
//  TrendFormatData::FormatEvent(). It creates a text string containing
//  up to three trend event IDs and uses the LogCell to format and
//  display this text string.
//---------------------------------------------------------------------
//@ PreCondition
//  Since this method retrieves the cursor position from the
//  TrendDataSet, the TrendDataSetAgent::valueUpdate setting callback
//  must be called before this method to update those variables when the
//  trend cursor setting changes. The order of instantiation guarantees
//  this sequencing since the TrendSubScreen instantiates the
//  TrendDataSet as part of its construction before it registers for the
// 	settings callback.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void TrendGraphsSubScreen::updateEventDrawables_(void)
{
	const TrendDataSet& rTrendDataSet = rTrendDataSetAgent_.getTrendDataSet();
	Int32 row = rTrendDataSetAgent_.getCursorPosition();

	// update the event button and event numbers
	if (rTrendDataSet.getTrendEvents(row).isAnyEventSet())
	{
		if (eventDetailButton_.getButtonState() == Button::FLAT)
		{
			eventDetailButton_.setToUp();
		}

		static wchar_t tmpBuffer1[40];

		// Default the two string output parameters to point to the static
		// character buffers.
		StringId pString1 = &tmpBuffer1[0];
		StringId pString2 = NULL_STRING_ID;
		StringId pString3 = NULL_STRING_ID;
		StringId string1Message = NULL_STRING_ID;
		tmpBuffer1[0] = '\0';

		const TrendEvents & rEvents = rTrendDataSet.getTrendEvents( row );

		Int32 numEvents = 0;

		// extract the event ids 
		// this array should be sized to the maximum events to display in the popup
		// this should also be moved to the TrendSubScreen base class so we can use
		// it to display the event pop-up window.
		TrendEvents::EventId eventId[TrendEvents::TOTAL_EVENT_IDS]; 

		Int32 idx = 0;

		// extract the user events
		for (idx = TrendEvents::BEGIN_USER_EVENT_ID; 
			(idx <= TrendEvents::END_USER_EVENT_ID) && (numEvents < countof(eventId)); 
			idx++)
		{
			if (rEvents.isEventSet(TrendEvents::EventId(idx)))
			{
				eventId[numEvents++] = TrendEvents::EventId(idx);
			}
		}
		// extract the auto-events
		for (idx = TrendEvents::BEGIN_AUTO_EVENT_ID; 
			(idx <= TrendEvents::END_AUTO_EVENT_ID) && (numEvents < countof(eventId)); 
			idx++)
		{
			if (rEvents.isEventSet(TrendEvents::EventId(idx)))
			{
				eventId[numEvents++] = TrendEvents::EventId(idx);
			}
		}

		if (numEvents == 0)
		{
			swprintf(tmpBuffer1, L" ");
		}
		else if (numEvents == 1)
		{
			swprintf(tmpBuffer1, L" %d", 
					TrendFormatData::FormatEvent(eventId[0]));
		}
		else if (numEvents == 2)
		{
			swprintf(tmpBuffer1, L" %d %d",
					TrendFormatData::FormatEvent(eventId[0]),
					TrendFormatData::FormatEvent(eventId[1]));
		}
		else if (numEvents == 3)
		{
			swprintf(tmpBuffer1, L" %d %d %d", 
					TrendFormatData::FormatEvent(eventId[0]),
					TrendFormatData::FormatEvent(eventId[1]),
					TrendFormatData::FormatEvent(eventId[2]));
		}
		else if (numEvents > 3)
		{
			swprintf(tmpBuffer1, L" %d %d %d +", 
					TrendFormatData::FormatEvent(eventId[0]),
					TrendFormatData::FormatEvent(eventId[1]),
					TrendFormatData::FormatEvent(eventId[2]));
		}

		static const Boolean IS_CHEAP_TEXT = FALSE;
		static const Int32 MARGIN = 0;
		eventCell_.setText(IS_CHEAP_TEXT, pString1, pString2, pString3, ::CENTER,
						   TextFont::NORMAL, TextFont::TEN, MARGIN, string1Message);
		eventCell_.setShow(TRUE);
	}
	else
	{
		eventDetailButton_.setToFlat();
		eventCell_.setShow(FALSE);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition   
//  none
//@ End-Method
//=====================================================================

void TrendGraphsSubScreen::SoftFault(const SoftFaultID  softFaultID,
									 const Uint32       lineNumber,
									 const char*        pFileName,
									 const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDGRAPHSSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}

