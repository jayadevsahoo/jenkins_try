#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BdInop - BD State: Inop.
//---------------------------------------------------------------------
//@ Interface-Description
//  The BdInop class is a FsmState used by the BdFsm finite state
//  machine.  The class represents the wait state that BD Task Control
//  enters upon completing a BD transition to the Inop state.  It
//  contains methods for processing events while in this state. 
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the BD Inop state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  BdInop is derived from the FsmState class.  It overrides the
//  virtual method receiveGuiReady to provide state specific behavior
//  when it receives a GuiReadyMessage.  It overrides the virtual
//  method receiveCommUp to provide state specific behavior when it
//  receives a CommUpMessage.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/BdInop.ccv   25.0.4.0   19 Nov 2013 14:33:50   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "BdInop.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "TaskControlAgent.hh"
#include "GuiReadyMessage.hh"
#include "TaskAgentManager.hh"
//TODO E600 port #include "Reset.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdInop [default constructor]
//
//@ Interface-Description
//  BdInop class default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdInop::BdInop()
{
    CALL_TRACE("BdInop()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdInop [destructor]
//
//@ Interface-Description
//  BdInop class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
BdInop::~BdInop()
{
    CALL_TRACE("~BdInop()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
BdInop::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "BdInop";
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with 
//  this BdInop state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the INOP state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
SigmaState
BdInop::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_INOP;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveCommUp
//
//@ Interface-Description
//  The receiveCommUp method processes a CommUpMessage received by BD
//  task control in the BdInop state.  It sends a BdReady-Inop
//  message to GUI task control.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method invokes TaskControlAgent::ReportNodeReady to report
//  to the GUI that the BD is ready in the INOP state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdInop::receiveCommUp(const CommUpMessage& ) const
{
    CALL_TRACE("receiveCommUp(const CommUpMessage&)");
    TaskControlAgent::ReportNodeReady();
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveSyncClock
//
//@ Interface-Description
//  The receiveSyncClock method processes a SyncClockMessage received 
//  from GUI task control by BD task control in the BdInop state.
//  In this state, the message is ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdInop::receiveSyncClock(const SyncClockMessage& ) const
{
    CALL_TRACE("receiveSyncClock(const SyncClockMessage&)");
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveGuiReady
//
//@ Interface-Description
//  The receiveGuiReady method processes a GuiReadyMessage received 
//  from GUI task control by BD task control in the BdInop state.
//  It updates the local state of the GUI from the message.  
//  For GUI-Ready in the INOP or ONLINE states, it forwards the 
//  message to the application tasks.  For any other GUI state that 
//  is considered incompatible with the BdInop state, this method 
//  asserts causing the BD to reboot.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses FsmState::SetGuiState to update the local state of
//  the GUI to the state contained in the GuiReadyMessage.  
//  It invokes TaskAgentManager::Send to forward the GuiReadyMessage 
//  to each managed task. For incompatible GUI states, this method 
//  asserts to reboot the BD.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
BdInop::receiveGuiReady(const GuiReadyMessage& message) const
{
    CALL_TRACE("receiveGuiReady(const GuiReadyMessage&)");
 
    FsmState::SetGuiState(message);

    switch (message.getState())
    {
        case STATE_INOP:    // $[TI1]
        case STATE_ONLINE:
        case STATE_INIT:
        case STATE_START:
            TaskAgentManager::Send(message);
            break;
        case STATE_SERVICE:  // $[TI2]
        case STATE_SST:
        case STATE_TIMEOUT:
        default:
//TODO E600 define Reset  Reset::Initiate(INTENTIONAL, Reset::BDINOP);
            break;
    }
}
 

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
BdInop::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_BDINOP, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

