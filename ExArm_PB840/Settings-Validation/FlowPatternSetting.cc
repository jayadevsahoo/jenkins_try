#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  FlowPatternSetting - Flow Pattern Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates the gas-flow pattern
//  for mandatory volume-based breaths.  This class inherits from
//  'BatchDiscreteSetting' and provides the specific information needed for
//  the representation of flow pattern.  This information includes the set
//  of values that this setting can have (see 'FlowPatternValue.hh'), and
//  this batch setting's new-patient value.
//
//  This discrete setting has a dependent setting (inspiratory time), and
//  therefore must process updates to, and violations of, its dependent
//  setting.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This discrete setting overrides 'calcDependentValues_()' for processing
//  of its dependent settings (i.e., inspiratory time).
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/FlowPatternSetting.ccv   25.0.4.0   19 Nov 2013 14:27:24   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 011   By: gdc    Date: 05-Jan-2009    SCR Number: 5773
//  Project:  840S
//  Description:
//      Modified to always return RAMP for new patient value.
//
//  Revision: 010   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 009  By: sah     Date:  18-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added support for new 'VC+' mandatory type in 'getApplicability()'
//      *  added modified 'getNewPatientValue()', as per VC+ requirements
//      *  added support for flow pattern's new VC+-to-VC and VC-to-VC+
//         Transition Rules
//      *  added additional 'SIGMA_DEVELOPMENT' directives, to eliminate
//         compilation warnings
//
//  Revision: 008   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  incorporated circuit-based new-patient value
//	*  eliminated use of unneeded 'INVALID_DISCRETE_VALUE' bound state
//      *  added support for new 'IE_RATIO_SOFT_MAX_ID' soft bound.
//
//  Revision: 007   By: dosman   Date:  15-Jan-1999    DR Number: 5187
//  Project:  ATC
//  Description:
//    Changed processBoundViolation to determine the correct dependent
//    bound prompt to be displayed.
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 005   By: dosman    Date:  14-Jul-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	fixed a bug. symptoms: we changed main primary setting apnea tidal volume
//      to its max value and accepted it. Then we activated flow pattern setting.
//	When we tried to turn the knob to change flow pattern setting, system asserted.
//	Problem was that this setting's calcDependentValues_ was calling Setting's
//      calcDependentValues_ which assumes that dependents can be resolved,
//	whereas it should have called DiscreteSetting's calcDependentValues_
//	which simply records the setting and bound that limits this setting.
//
//  Revision: 004   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getNewPatientValue()
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  04-Jun-1997    DCS Number: 2188
//  Project: Sigma (R8027)
//  Description:
//	As part of the fixes for this DCS, I'm removing the special
//	handling done by this class so that a global solution can be
//	made in the base class ('DiscreteSetting').
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//  	Inital version.
//
//=====================================================================

#include "FlowPatternSetting.hh"
#include "FlowPatternValue.hh"
#include "ModeValue.hh"
#include "MandTypeValue.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
// RespRateSetting and BasedOnCategory are needed
// to determine Breath Period, 
// which is needed to compute Inspiratory Time and 
// Expiratory Time within processBoundViolation
#include "BasedOnCategory.hh"
#include "RespRateSetting.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// This array is set up dynamically via this class's 'calcDependentValues_()',
// because all dependents are not applicable in 'SPONT' mode...
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
{
	SettingId::INSP_TIME,
	SettingId::EXP_TIME,
	SettingId::IE_RATIO,

	SettingId::NULL_SETTING_ID
};


static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"SQ", FlowPatternValue::SQUARE_FLOW_PATTERN},
	{"RA", FlowPatternValue::RAMP_FLOW_PATTERN},
	{NULL , NULL}
};

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  FlowPatternSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[02125] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FlowPatternSetting::FlowPatternSetting(void)
: BatchDiscreteSetting(SettingId::FLOW_PATTERN,
					   ::ArrDependentSettingIds_,
					   FlowPatternValue::TOTAL_FLOW_PATTERNS)
{
	// $[TI1]
	CALL_TRACE("FlowPatternSetting()");
	setLookupTable(ParameterLookup_);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~FlowPatternSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Default destructor.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

FlowPatternSetting::~FlowPatternSetting(void)
{
	CALL_TRACE("~FlowPatternSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	FlowPatternSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	DiscreteValue  mandTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			mandTypeValue = pMandType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			mandTypeValue = pMandType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicabilityId;

	switch ( mandTypeValue )
	{
		case MandTypeValue::PCV_MAND_TYPE :	  // $[TI3]
			applicabilityId = Applicability::INAPPLICABLE;
			break;
		case MandTypeValue::VCV_MAND_TYPE :	  // $[TI4]
			applicabilityId = Applicability::CHANGEABLE;
			break;
		case MandTypeValue::VCP_MAND_TYPE :	  // $[TI5]
			applicabilityId = Applicability::VIEWABLE;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(mandTypeValue);
			break;
	}

	return(applicabilityId);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02126] -- the setting's new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	FlowPatternSetting::getNewPatientValue(void) const
{
	return(DiscreteValue(FlowPatternValue::RAMP_FLOW_PATTERN));
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, toValue, fromValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'fromValue' to 'toValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   (settingId == SettingId::MAND_TYPE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	FlowPatternSetting::acceptTransition(const SettingId::SettingIdType settingId,
										 const DiscreteValue            toValue,
										 const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, toValue, fromValue)");

	AUX_CLASS_ASSERTION((settingId == SettingId::MAND_TYPE), settingId);

	switch ( toValue )
	{
		//--------------------------------------------------------------
		// $[02022]\a\ -- transition to VC; retain current value...
		//--------------------------------------------------------------
		case MandTypeValue::VCV_MAND_TYPE :		  // $[TI1]
			// do nothing...
			break;

			//--------------------------------------------------------------
			// $[VC02006]\b\,\e\ -- transition to VC+; set to RAMP...
			//--------------------------------------------------------------
		case MandTypeValue::VCP_MAND_TYPE :		  // $[TI2]
			// force value to 'RAMP'...
			setAdjustedValue(DiscreteValue(FlowPatternValue::RAMP_FLOW_PATTERN));
			break;

		case MandTypeValue::PCV_MAND_TYPE :
		default:
			// unexpected mandatory type...
			AUX_CLASS_ASSERTION_FAILURE(toValue);
			break;
	}

	setForcedChangeFlag();
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	FlowPatternSetting::isAcceptedValid(void) const
{
	CALL_TRACE("isAcceptedValid()");

	Boolean isValid;

	// is the accepted apnea flow pattern value within its range?...
	isValid = DiscreteSetting::isAcceptedValid(); // forward to base class...

	if ( isValid )
	{
		if ( getApplicability(Notification::ACCEPTED) !=
			 Applicability::INAPPLICABLE )
		{
			// is the the accepted flow pattern value consistent with the
			// accepted inspiratory time?...
			const Setting*  pInspTime =
				SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

			isValid = pInspTime->isAcceptedValid();
		}
	}

	return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	FlowPatternSetting::isAdjustedValid(void)
{
	CALL_TRACE("isAdjustedValid()");

	Boolean  isValid = DiscreteSetting::isAdjustedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ADJUSTED) !=
			 Applicability::INAPPLICABLE )
		{
			// is the the adjusted flow pattern value consistent with the adjusted
			// inspiratory time?...
			Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

			isValid = pInspTime->isAdjustedValid();
		}
	}

	return(isValid);
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	FlowPatternSetting::SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							FLOW_PATTERN_SETTING, lineNumber, pFileName,
							pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02128] -- this setting's dependent settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	FlowPatternSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
	CALL_TRACE("calcDependentValues_()");

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

	const DiscreteValue  MODE_VALUE = pMode->getAdjustedValue();

	Uint  idx = 0u;

	switch ( MODE_VALUE )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :	  // $[TI1]
			// all dependents are active here...
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
			break;
		case ModeValue::SPONT_MODE_VALUE :		  // $[TI2]
		case ModeValue::CPAP_MODE_VALUE :
			// only inspiratory time in 'SPONT' mode...
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			break;
		case ModeValue::BILEVEL_MODE_VALUE :
		default :
			// unexpected mode value...
			AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
			break;
	}

	// terminate with the null id...
	ArrDependentSettingIds_[idx] = SettingId::NULL_SETTING_ID;

	// forward on to base class...
	return(DiscreteSetting::calcDependentValues_(isValueIncreasing));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  processBoundViolation_()  [virtual]
//
//@ Interface-Description
//  A dependent setting's bound has been violated due to a proposed
//  change to this setting.  This method is responsible for re-mapping
//  the dependent setting bound information, to bound information
//  specific to this setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	FlowPatternSetting::processBoundViolation_(void)
{
	CALL_TRACE("processBoundViolation_()");

	// get a reference to this setting's bound status...
	BoundStatus&  rBoundStatus = getBoundStatus_();

	SettingBoundId  flowPatternBoundId = ::NULL_SETTING_BOUND_ID;

	switch ( rBoundStatus.getBoundId() )
	{
		case ::IE_RATIO_MAX_ID :
		case ::INSP_TIME_MAX_ID : 
		case ::EXP_TIME_MIN_ID :		  // $[TI1]
			{
				// if the bound violation is one of these three,
				// then we need to determine which of the three bounds
				// is the most restrictive

				const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

				DiscreteValue MODE = pMode->getAdjustedValue();
				if ( (MODE == ModeValue::SPONT_MODE_VALUE) || (MODE == ModeValue::CPAP_MODE_VALUE) )
				{
					// $[TI1.1]
					// regardless of which bound was hit, I:E and Te are not
					// applicable in SPONT...
					flowPatternBoundId = ::FLOW_PATTERN_BASED_TI_MAX_ID;
				}
				else
				{
					// $[TI1.2]
					// We will access the saved values of (1)TI, (2)TE, (3)I:E
					// (i.e., the relevant dependent settings of this setting)
					// and then determine which setting (TI, TE, or I:E)
					// is the most restrictive cause of the bound violation
					// by comparing
					// (1) TI 
					// (2) TI based on TE, TI(TE), and 
					// (3) TI based on I:E, TI(I:E)
					// and finding the minimum of TI, TI(TE), TI(I:E).

					Real32  inspTimeValue;
					Real32  expTimeValue;
					Real32  ieRatioValue;

					const SettingId::SettingIdType*  pDependentId;
					const SettingValue*              pDependentValue;

					for ( pDependentId = ARR_DEPENDENT_SETTING_IDS_,
						  pDependentValue = arrDependentValues_;
						*pDependentId != SettingId::NULL_SETTING_ID;
						pDependentId++, pDependentValue++ )
					{
						// $[TI1.2.1] -- this branch is ALWAYS executed 
						// because we only call this method if the setting
						// has dependent settings
						// save each of the dependent values so that they are easily
						// retrievable...
						switch ( *pDependentId )
						{
							case SettingId::INSP_TIME :			  // $[TI1.2.1.1]
								inspTimeValue = BoundedValue(*pDependentValue).value;
								break;
							case SettingId::EXP_TIME :			  // $[TI1.2.1.2]
								expTimeValue = BoundedValue(*pDependentValue).value;
								break;
							case SettingId::IE_RATIO :			  // $[TI1.2.1.3]
								ieRatioValue = BoundedValue(*pDependentValue).value;
								break;
							default :
								// do nothing...
								break;
						} // end switch
					} // end for

					const Real32  BREATH_PERIOD_VALUE =
						(RespRateSetting::GetBreathPeriod(BASED_ON_ADJUSTED_VALUES));

					//-------------------------------------------------------------
					// compute inspiratory time from expiratory time...
					//-------------------------------------------------------------

					const Real32  TI_BASED_ON_TE_VALUE = (BREATH_PERIOD_VALUE -
														  expTimeValue);


					//-------------------------------------------------------------
					// compute inspiratory time from I:E ratio...
					//-------------------------------------------------------------

					Real32  eRatioValue;
					Real32  iRatioValue;

					if ( ieRatioValue < 0.0f )
					{	// $[TI1.2.2]
						// The I:E ratio value is negative, therefore we have 1:xx with
						// "xx" the expiratory portion of the ratio...
						iRatioValue = 1.0f;
						eRatioValue = -(ieRatioValue);
					}
					else
					{	// $[TI1.2.3]
						// The I:E ratio value is positive, therefore we have xx:1 with
						// "xx" the inspiratory portion of the ratio...
						iRatioValue = ieRatioValue;
						eRatioValue = 1.0f;
					}

					// calculate the insp time from the I:E ratio...
					const Real32  TI_BASED_ON_IE_VALUE =
						(iRatioValue * BREATH_PERIOD_VALUE) /
						(iRatioValue + eRatioValue);


					//-------------------------------------------------------------
					// determine most restrictive bound...
					//-------------------------------------------------------------

#if defined(SIGMA_DEVELOPMENT)
					// This bound ID is required to have been hit when the corresponding
					// dependent setting was accepting the change in the primary setting
					// Flow Pattern.  For example, if we choose the most restrictive bound 
					// on Flow Pattern to be based on Insp Time, then Insp Time better have
					// at least hit its bound when it was accepting the change in Flow
					// Pattern.
					SettingBoundId requiredDependentBoundId;
#endif // defined(SIGMA_DEVELOPMENT)

					if ( inspTimeValue <= TI_BASED_ON_TE_VALUE  &&
						 inspTimeValue <= TI_BASED_ON_IE_VALUE )
					{
						// $[TI1.2.4]
#if defined(SIGMA_DEVELOPMENT)
						requiredDependentBoundId = ::INSP_TIME_MAX_ID;
#endif // defined(SIGMA_DEVELOPMENT)
						flowPatternBoundId       = ::FLOW_PATTERN_BASED_TI_MAX_ID;

						rBoundStatus.setViolationId(SettingId::INSP_TIME);
					}
					else if ( TI_BASED_ON_TE_VALUE <= inspTimeValue  &&
							  TI_BASED_ON_TE_VALUE <= TI_BASED_ON_IE_VALUE )
					{
						// $[TI1.2.5]
#if defined(SIGMA_DEVELOPMENT)
						requiredDependentBoundId = ::EXP_TIME_MIN_ID;
#endif // defined(SIGMA_DEVELOPMENT)
						flowPatternBoundId       = ::FLOW_PATTERN_BASED_TE_MIN_ID;

						rBoundStatus.setViolationId(SettingId::EXP_TIME);
					}
					else if ( TI_BASED_ON_IE_VALUE <= inspTimeValue &&
							  TI_BASED_ON_IE_VALUE <= TI_BASED_ON_TE_VALUE )
					{
						// $[TI1.2.6]
#if defined(SIGMA_DEVELOPMENT)
						requiredDependentBoundId = (rBoundStatus.isSoftBound())
												   ? ::IE_RATIO_SOFT_MAX_ID
												   : ::IE_RATIO_MAX_ID;
#endif // defined(SIGMA_DEVELOPMENT)
						flowPatternBoundId       = ::FLOW_PATTERN_BASED_IE_MAX_ID;

						rBoundStatus.setViolationId(SettingId::IE_RATIO);
					}
					else
					{
						// this could should NEVER run!
						CLASS_ASSERTION_FAILURE();
					}

#if defined(SIGMA_DEVELOPMENT)
					// Perform a check that whichever of the dependent settings
					// we have decided is the most restrictive bound on Flow Pattern
					// should at least have hit a bound while it was accepting
					// the change in the primary setting Flow Pattern, and
					// the hitting of the bound is saved in arrDependentBoundIds_
					// This check is not required -- it is a confirmation that
					// our algorithm does not have any holes.
					const SettingBoundId*  pDependentBoundId;
					Boolean                foundAMatch = FALSE;

					for ( pDependentId = ARR_DEPENDENT_SETTING_IDS_,
						  pDependentBoundId = arrDependentBoundIds_;
						*pDependentId != SettingId::NULL_SETTING_ID;
						pDependentId++, pDependentBoundId++ )
					{
						if ( requiredDependentBoundId == *pDependentBoundId )
						{
							foundAMatch = TRUE;
						}
					}

					// the bound originally hit must be related to the bound
					// that we decide to display as a prompt.
					AUX_CLASS_ASSERTION((foundAMatch), requiredDependentBoundId);
#endif // defined(SIGMA_DEVELOPMENT)
				} // end of else ([TI1.2])
			} // end case
			break;

		case ::INSP_TIME_MIN_ID :
		case ::INSP_TIME_MIN_BASED_PLAT_TIME_ID : // $[TI2]
			// if the bound violation is one of these two,
			// then there is no ambiguity which prompt to display --
			// if TI is hitting a minimum, and I:E is hitting a minimum, 
			// then TE would also have to be at a maximum, and it is fine
			// to display INSP_TIME_MIN.
			flowPatternBoundId = ::FLOW_PATTERN_BASED_TI_MIN_ID;
			break;

		case ::IE_RATIO_SOFT_MAX_ID :
			// for soft-bounds, return the ID of the bound violated...
			flowPatternBoundId = ::IE_RATIO_SOFT_MAX_ID;
			break;

		default :
			// unexpected bound occurred...
			AUX_CLASS_ASSERTION_FAILURE(rBoundStatus.getBoundId());
			break;
	}; // end switch

	// set flow pattern's bound status up to indicate that flow pattern has
	// violated its bound...
	rBoundStatus.setBoundStatus(rBoundStatus.getBoundState(), flowPatternBoundId);

	// NOTE:  by returning a violated bound status, the base class's
	//        'calcNewValue()' method will take care of attempting the
	//        "next" flow pattern value...
}
