#include "stdafx.h"
#include "UDPCommunication.h"
#include <tchar.h>

UdpCommunication::UdpCommunication(void) :	recvSocket_(-1),
												sendSocket_(-1), 
												recvInitialized_(false),
												sendInitialized_(false)
{
	WORD w = MAKEWORD(2, 2);
    WSADATA wsadata;
    ::WSAStartup(w, &wsadata);
}

UdpCommunication::~UdpCommunication(void)
{
    stop();
}

bool UdpCommunication::setup(UDPDirection direction, unsigned short recvPort, 
											   const char* sendIpAddr, unsigned short sendPort)
{
	if( (direction & UDP_RECEIVE) != 0 && !recvInitialized_)
	{
		recvSocket_ = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if ( -1 == recvSocket_ )
		{
			RETAILMSG(true, (_T("ERROR in create recv socket.\r\n")));
			return false;
		}

		SOCKADDR_IN listen_addr;

		memset(&listen_addr, 0, sizeof(listen_addr));
		listen_addr.sin_family = AF_INET;
		listen_addr.sin_port = htons(recvPort);
		listen_addr.sin_addr.s_addr = INADDR_ANY;

		if(bind(recvSocket_, (SOCKADDR*)&listen_addr,sizeof(SOCKADDR_IN)) < 0) 
		{
            DWORD err = WSAGetLastError();
			RETAILMSG(true, (_T("ERROR binding in the server socket.\r\n")));
			return false;
		}
		recvInitialized_ = true;
	}

	if ( (direction & UDP_SEND) != 0 && !sendInitialized_ )
	{
		sendSocket_ = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if ( -1 == sendSocket_ )
		{
			RETAILMSG(true, (_T("ERROR in create recv socket.\r\n")));
			return false;
		}

		// enable transimission and receipt of broadcast messages on this socket
		char opt = -1;
		setsockopt( sendSocket_, SOL_SOCKET, SO_BROADCAST, (char*)&opt, sizeof(char) );
		
		// zero
		memset( &sendtoSockaddr_, 0, sizeof(sendtoSockaddr_) );

		// configure address
		sendtoSockaddr_.sin_family		= AF_INET;
		sendtoSockaddr_.sin_port		= htons( sendPort );
		sendtoSockaddr_.sin_addr.s_addr	= inet_addr( sendIpAddr );

		sendInitialized_ = true;
	}
	return true;
}

bool UdpCommunication::stop()
{
	if ( sendInitialized_ )
	{
		closesocket(sendSocket_);
		sendInitialized_ = false;
	}
	if ( recvInitialized_ )
	{
		closesocket(recvSocket_);
		recvInitialized_ = false;
	}

	return true;	
}

bool UdpCommunication::readData(Uint8 * DataBuffer, const DWORD NumberOfBytesToRead, 
								  DWORD* NumberOfBytesRead, const DWORD MaxMsToWait)
{
	if ( !recvInitialized_ )	
	{
		*NumberOfBytesRead = 0;
		return false;
	}

	fd_set fds;

    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = MaxMsToWait * 1000;

    FD_ZERO(&fds);
    FD_SET(recvSocket_, &fds);

    int selectResult = select(sizeof(fds)*8, &fds, NULL, NULL, &timeout);
    if(selectResult > 0)
    {
        SOCKADDR_IN clientaddr;
		int len = sizeof(clientaddr); 
		int recvResult = recvfrom(recvSocket_, (char*)DataBuffer, NumberOfBytesToRead, 0, 
			(sockaddr*)&clientaddr, &len );

        if( recvResult > 0)
        {
			*NumberOfBytesRead = recvResult;
			return true;
        }
		else if ( recvResult == 0 )
		{
			// recvResult = 0, if connection is gracefully closed
			RETAILMSG(true, (_T("ReadData: recvfrom connection is gracefully closed.\r\n")));
		}
		else
		{
			// recvResult = SOCKET_ERROR, use WSAGetLastError to get error code
			RETAILMSG(true, ( _T("ReadData: ERROR in recvfrom, %d\r\n"), WSAGetLastError()) );
		}
    }
	else if ( selectResult == 0 )
	{
		// selectResult = 0, if time out
		//RETAILMSG(true, ( _T("ReadData: select time out.\r\n") ) );
	}
	else
	{
		// selectResult = SOCKET_ERROR, use WSAGetLastError to get error code
		RETAILMSG(true, ( _T("ReadData: ERROR in select, %d\r\n"), WSAGetLastError()) );
	}

	*NumberOfBytesRead = 0;

	return false;
}

bool UdpCommunication::writeData(Uint8 * DataBuffer, const DWORD NumberOfBytesToWrite, DWORD* NumberOfBytesWritten)
{
	if ( !sendInitialized_ )
	{
		*NumberOfBytesWritten = 0;
		return false;
	}

	int sendResult = sendto( sendSocket_, (const char*)DataBuffer, NumberOfBytesToWrite, 0, 
		(sockaddr*)&sendtoSockaddr_, sizeof(sendtoSockaddr_) );

	// SOCKET_ERROR
	if ( sendResult == SOCKET_ERROR )
	{
		RETAILMSG(true, ( _T("WriteData: ERROR in sendto, %d\r\n"), WSAGetLastError()) );
		*NumberOfBytesWritten = 0;
		return false;
	}
	// send is partially completed
	else if ( (DWORD)sendResult < NumberOfBytesToWrite )
	{
		RETAILMSG(true, ( _T("ReadData: send is partially completed, %d of %d.\r\n"), sendResult, NumberOfBytesToWrite ) );
		*NumberOfBytesWritten = sendResult;
		return false;
	}

	*NumberOfBytesWritten = sendResult;

	return true;
}
