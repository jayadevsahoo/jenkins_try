#ifndef	BroadcastSocket_HH
#define	BroadcastSocket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: BroadcastSocket - Class definition that provides 
//  interface to a UDP socket that can broadcast data.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/BroadcastSocket.hhv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//@ Modification-Log
//
//  Revision: 004  By: gdc     Date: 30-Dec-2010  SCR Number: 6697
//  Project:  XENA2
//  Description:
//      Removed code to broadcast BD IP address as this logic is no 
//      longer used and consumes all mbufs causing a BD reset
//      during communication loss with the GUI.
//
//  Revision: 003  By: gdc     Date: 30-Dec-2010  SCR Number: 6717
//  Project:  XENA2
//  Description:
//      Implemented openUdpBroadcast to properly set up UDP broadcast 
//      socket.
//
//  Revision: 002  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  06-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//====================================================================



#include "Sigma.hh"
#include "NetworkSocket.hh"
#include "NetworkMsgId.hh"

class BroadcastSocket : public NetworkSocket 
{

public:
    BroadcastSocket(void);
    ~BroadcastSocket(void);
    Int openUdpBroadcast(void);
    //virtual Int recvMsg( XmitDataMsgId msgId, char *pData, Uint32 maxBytes); 
    Int broadcastMsg( XmitDataMsgId msgId, void* pData, Uint nbytes,
                      PortId udpport);

    static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL, 
                          const char*       pPredicate = NULL);
private:
    BroadcastSocket(const BroadcastSocket& );   // not implemented...
    void   operator=(const BroadcastSocket& );  // not implemented...

};

#endif // BroadcastSocket_HH 
