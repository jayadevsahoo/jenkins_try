#ifndef	BdSerialPort_HH
#define BdSerialPort_HH

//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2008, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Class:  BdSerialPort - Serial Port Driver
//---------------------------------------------------------------------
//@ Version-Information
//@(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BdSerialPort.hhv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $  
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  25-APR-2008    SCR Number: XXXX
//  Project:  PROX
//  Description:
//	Initial version.
//=====================================================================

#include "BD_IO_Devices.hh"
#include "SerialPort.hh"

class BdSerialPort : public SerialPort
{
public:

	BdSerialPort( const Uint8 deviceNumber );
	~BdSerialPort();

	virtual void idleFunction_(void);

private:

	BdSerialPort(void);		// not implemented

};

#endif	// ! BdSerialPort_HH
