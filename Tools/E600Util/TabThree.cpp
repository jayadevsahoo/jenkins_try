// TabThree.cpp : implementation file
//

#include "stdafx.h"
#include "E600Util.h"
#include "TabThree.h"
#include "GlobalObjects.h"
#include "UserMessages.h"
#include "BDSignals.h"

// TabThree dialog

IMPLEMENT_DYNAMIC(CTabThree, CDialog)

CTabThree::CTabThree(CWnd* pParent /*=NULL*/)
	: CDialog(CTabThree::IDD, pParent)
{
	for(int i=0; i<NUM_SIGNALS; i++)
	{
		MonLabelArr[i] = L"";
		MonSignals.push_back(L"");
		MonitorData[i] = 0.00;
	}
}

CTabThree::~CTabThree()
{
}

void CTabThree::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTabThree, CDialog)
ON_WM_LBUTTONDOWN()
ON_WM_PAINT()
ON_MESSAGE(WM_BD_DATA, OnBDData)
ON_MESSAGE(WM_HEADER_STRING, OnHeaderString)
END_MESSAGE_MAP()


// TabThree message handlers
BOOL CTabThree::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_rows = 7;
	m_columns = 6;
	m_activeRows = 3;
	m_activeColumns = 6;
	m_baseX = 2;
	m_baseY = 2;

	m_cellWidth = 219;
	m_cellHeight = 111;

	// color for background and grid
	COLORREF	crBackGnd = RGB(132, 130, 132);
	COLORREF	crGrid = RGB(0, 130, 189);

	// initialize brush and pens
	brush.CreateSolidBrush( crBackGnd );
	verticalPen.CreatePen(PS_SOLID, 4, crGrid);
	horizontalPen.CreatePen(PS_SOLID, 4, crGrid);

	/////////////////////////////////////// create memory dc and bmp buffers ////////////////////////////////////////////
	CRect rect;	//(l,t,r,b)
	
	GetClientRect(&rect);
	CClientDC *pDC = new CClientDC(this);	// screen DC

	//CDC* pDC = GetDC();

	nClientWidth = rect.Width();	// 1319
	nClientHeight = rect.Height();	// 869
	
	m_dcMemMonitorArea.CreateCompatibleDC( pDC );	// Mem DC
	m_MonitorsBufferBmp.CreateCompatibleBitmap( pDC, nClientWidth, m_cellHeight * m_rows);
	m_dcMemMonitorArea.SelectObject(&m_MonitorsBufferBmp);

	m_dcMonitorBkgnd.CreateCompatibleDC( pDC );
	m_bmpMonitorBkgnd.CreateCompatibleBitmap(pDC, nClientWidth, m_cellHeight * m_rows );
	m_dcMonitorBkgnd.SelectObject(&m_bmpMonitorBkgnd);

	//ReleaseDC(pDC);
	delete pDC;

	//////////////////////////////////////////// prepare monitor canvas //////////////////////////////////////////////////
	// with grid and title text
	PrepareMonitorCanvas();
	
	return FALSE;
}


void CTabThree::OnPaint()
{
#if 0	
	PAINTSTRUCT ps;
	CDC* pDC = BeginPaint(&ps);

	DrawMonitorData();
	pDC->BitBlt(m_baseX, m_baseY, nClientWidth, m_cellHeight*m_rows, &m_dcMemMonitorArea, 0, 0, SRCCOPY);

	EndPaint(&ps);
#endif
	CDialog::OnPaint();
}


LRESULT CTabThree::OnBDData(WPARAM x, LPARAM y)
{
	int nBytes = (int)x;
	float *pData = (float *)y;
	int nData = nBytes / sizeof(float);
	float SignalData[NUM_SIGNALS][NUM_SLOTS];

	memcpy(SignalData, pData, nBytes);

	for(int i=0; i<NUM_SIGNALS; i++)
	{
		MonitorData[i] = SignalData[i][NUM_SLOTS-1];
	}
	
	CDC* pDC = this->GetDC();
	DrawMonitorData();
	pDC->BitBlt(m_baseX, m_baseY, nClientWidth, m_cellHeight*m_rows, &m_dcMemMonitorArea, 0, 0, SRCCOPY);
	ReleaseDC(pDC);

	return 0;
}


LRESULT CTabThree::OnHeaderString(WPARAM x, LPARAM y)
{
	CString HeaderStr = (wchar_t *)y;
	MonSignals.clear();
	ParseCSVHeader(HeaderStr, MonSignals);
	
	PrepareMonitorCanvas();
	return 0;
}


void CTabThree::OnLButtonDown(UINT nFlags, CPoint point)
{

}


void CTabThree::PrepareMonitorCanvas()
{
	int savedDC = m_dcMonitorBkgnd.SaveDC();

	// draw background
	CRect	rcMonitorArea(0, 0, nClientWidth, nClientHeight);
	m_dcMonitorBkgnd.FillRect(&rcMonitorArea, &brush);

	// draw vertical grid
	m_dcMonitorBkgnd.SelectObject(&verticalPen);
	for (int i = 0; i < m_columns + 1; ++ i)
	{
		CPoint ptBegin, ptEnd;
		ptBegin.x = i * m_cellWidth;
		ptBegin.y = rcMonitorArea.top;
		ptEnd.x = ptBegin.x;
		ptEnd.y	= ptBegin.y + m_cellHeight * m_rows;

		m_dcMonitorBkgnd.MoveTo(ptBegin);
		m_dcMonitorBkgnd.LineTo(ptEnd);
	}

	// draw horizontal grid
	m_dcMonitorBkgnd.SelectObject(&horizontalPen);
	for (int j = 0; j < m_rows + 1; ++ j)
	{
		CPoint ptBegin, ptEnd;
		ptBegin.x = 0;
		ptBegin.y = rcMonitorArea.top + j * m_cellHeight;
		ptEnd.x = m_cellWidth * m_columns;
		ptEnd.y = ptBegin.y;

		m_dcMonitorBkgnd.MoveTo(ptBegin);
		m_dcMonitorBkgnd.LineTo(ptEnd);
	}

	
	m_dcMonitorBkgnd.SetTextColor(RGB(255,255,255));
	m_dcMonitorBkgnd.SelectObject( GlobalObjects::getInstance()->getArial_Bold(32) );

	// draw text title & data
	for (int cellIndex = 0; cellIndex < (int)MonSignals.size(); cellIndex ++)
	{
		int cellIndexX = cellIndex % m_columns;
		int cellIndexY = cellIndex / m_columns;

		CRect rcCell;
		rcCell.left = cellIndexX * m_cellWidth;
		rcCell.top = cellIndexY * m_cellHeight;
		rcCell.right = rcCell.left + m_cellWidth;
		rcCell.bottom = rcCell.top + m_cellHeight;

		// Label area
		CRect rcCellTitle;
		rcCellTitle.left = rcCell.left + 5;
		rcCellTitle.top = rcCell.top + 5;
		rcCellTitle.right = rcCellTitle.left + 210;
		rcCellTitle.bottom = rcCellTitle.top + 30;

		// Data area
		CRect rcData;
		rcData.left = rcCellTitle.left;
		rcData.top = rcCellTitle.bottom + 20;
		rcData.right = rcData.left + 200;
		rcData.bottom = rcData.top + 65;


		CString str = MonSignals[cellIndex];
		m_dcMonitorBkgnd.SetBkMode(TRANSPARENT);
		m_dcMonitorBkgnd.DrawText(str, rcCellTitle, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	}

	m_dcMonitorBkgnd.RestoreDC( savedDC );
}

void CTabThree::DrawMonitorData(  )
{
	m_dcMemMonitorArea.BitBlt(0, 0, nClientWidth, m_cellHeight * m_rows, &m_dcMonitorBkgnd, 0, 0, SRCCOPY);
	
	int savedDC = m_dcMemMonitorArea.SaveDC();

	m_dcMemMonitorArea.SetBkMode( TRANSPARENT );
	m_dcMemMonitorArea.SetTextColor( RGB(255, 255, 255));
	m_dcMemMonitorArea.SelectObject( GlobalObjects::getInstance()->getArial_Bold(40) );

	for (int cellIndex = 0; cellIndex < (int)MonSignals.size(); cellIndex++)
	{
		int cellIndexX = cellIndex % m_columns;
		int cellIndexY = cellIndex / m_columns;

		CRect rcCell;
		rcCell.left = cellIndexX * m_cellWidth;
		rcCell.top = cellIndexY * m_cellHeight;
		rcCell.right = rcCell.left + m_cellWidth;
		rcCell.bottom = rcCell.top + m_cellHeight;

		// Label area
		CRect rcCellTitle;
		rcCellTitle.left = rcCell.left + 5;
		rcCellTitle.top = rcCell.top + 5;
		rcCellTitle.right = rcCellTitle.left + 210;
		rcCellTitle.bottom = rcCellTitle.top + 30;

		// Data area
		CRect rcData;
		rcData.left = rcCellTitle.left;
		rcData.top = rcCellTitle.bottom + 20;
		rcData.right = rcData.left + 200;
		rcData.bottom = rcData.top + 65;


		CString str;
		str.Format(L"%2.2f", MonitorData[cellIndex]);
		m_dcMemMonitorArea.DrawText(str, rcData, DT_CENTER|DT_VCENTER|DT_WORDBREAK);
	}

	m_dcMemMonitorArea.RestoreDC( savedDC );
	
}
