#include "stdafx.h"
#if		defined(SIGMA_CLIENT)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Semaphore - Mutual exclusion mechanism to protect shared data, in
//  other words a semaphore.
//---------------------------------------------------------------------
//@ Interface-Description
// The Semaphore class can be used to protect the integrity of data that
// is shared between multiple tasks, or any code that must have
// multiple tasks access it in a Mutually Exclusive manner.  To use
// a Semaphore, information about the Semaphore must be entered in the 
// IpcTableDef.cc file including a unique I.D. and the type, Ipc::MUTEX. 
// This will cause the mutually exclusion mechanism to be created
// at initialization time. Multiple tasks (objects) 
// create their own handle by passing the IpcIds to the Semaphore 
// constructor.  After the Semaphore has been constructed, each task
// calls the methods request() and release() to use the Semaphore.
// A common use would be for a class that has methods that update
// static class data. These methods can be called from multiple
// tasks, so the update code must not be interrupted by another
// task calling the same function.  To solve this problem, a Semaphore
// is added as a private class member, and the update methods simply
// call request() and release() which surround the critical code.
// The Semaphore can be a static (shared) or non-static class member.
// It is recommended and simplifies the code if the Semaphore is
// a non-static member, so that each instance has its own Semaphore instance.
//---------------------------------------------------------------------
//@ Rationale
// The Semaphore class was created to provide a mutual exclusion binary
// semaphore-like mechanism for code that is not inherently task safe.
// The users of this class need to determine exactly why and when
// the use of a Semaphore is required. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Semaphore class uses the VRTX32 semaphore services to 
//  provide mutual exclusion.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions  and pre-conditions are used to
//  ensure correctness of software conditons.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/Semaphore.ccv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  jhv    Date:  24-Jan-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//=====================================================================
#include "Semaphore.hh"
#include "DownloadCons.hh"


//  Static Data definition


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Semaphore(void)
//
//@ Interface-Description
//	  Constructs a VRTX semaphore.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  Saves the VRTX semaphore id into the semaphoreId_ variable.
//---------------------------------------------------------------------
//@ PreCondition
//  (0 < id && id <= MAX_MUTEX_ID)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Semaphore::Semaphore(void) 
{ 
Int error;

    semaphoreId_ = ::sc_screate( SEMAPHORE_LENGTH, PRIORITY_ORDER, 
                                                  &error);
	if (error != RET_OK ) // $[TI1]
	{
		semaphoreId_ = -1;
	} // else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Semaphore(void)  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Semaphore::~Semaphore(void)
{
Int error;

	if ( semaphoreId_ != -1 ) // $[TI1]
	{
		::sc_sdelete(semaphoreId_, 0, &error);
	} // else $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: lock(const Uint32 timeout)
//
//@ Interface-Description
// Takes one argument which indicates how many milliseconds that the
// caller is willing to wait for the resource.  The default is
// NO_TIMEOUT(=0), which will cause the call to block until the resource
// is acquired.  If a wait time in milliseconds is given,
// then it is converted to VRTX ticks (about 10 milliseconds per tick),
// and rounded up to the nearest tick (e.g., 1 millisecond = 1 tick).
// If the time expires before the resource is available or other error
// cases, the FAILURE status returns. Otherwise, the SUCCESS status
// returns.
//
// Note: The timeout is not synchronized with the VRTX32 clock. Thus,
// a time-out value of one VRTX32 clock tick results in the task's time-out
// period ending on the next occurrence of any VRTX32 clock tick. The
// actual elapsed time the task times out could be less than one VRTX32
// clock tick in this example.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls VRTX32 ::sc_spend to gain access to the semaphore.
//  The arguments are the seamphore id, timeout value, and the address
//  of an error return location.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
SigmaStatus
Semaphore::lock(const Uint32 timeout)
{
Int error;

	CLASS_PRE_CONDITION( semaphoreId_ != -1 );

    Uint32 ticks = 
			( timeout + (MILLI_SEC_PER_TICK - 1 ) )/MILLI_SEC_PER_TICK;

    ::sc_spend(semaphoreId_, ticks, &error);
    if (error == RET_OK ) // $[TI1]
    {
		return(SUCCESS);
    }
    else // $[TI2]
    {
		return(FAILURE);
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: release(void)
//
//@ Interface-Description
// Takes no arguments, and simply releases the resource.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls VRTX32 ::sc_spost. With the semaphore id and address of error
//  return location as arguments.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
SigmaStatus
Semaphore::release(void)
{
Int error;

	CLASS_ASSERTION( semaphoreId_ != -1 );

    ::sc_spost(semaphoreId_, &error);
    if ( error != RET_OK ) // $[TI1]
    {
		return(FAILURE);
    }
    else // $[TI2]
    {
		return(SUCCESS);
    }
}


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSemaphoreId(void)
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Semaphore::setSemaphoreId(void)
{
	semaphoreId_ = 99;
}
#endif // SIGMA_UNIT_TEST

#endif //defined(SIGMA_CLIENT)
