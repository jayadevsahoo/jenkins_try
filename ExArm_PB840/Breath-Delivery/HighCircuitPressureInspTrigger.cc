#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: HighCircuitPressureInspTrigger - Triggers if circuit pressure >=
//        operator set value for the high circuit pressure limit.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class is derived from BreathTrigger. This trigger will trigger 
//    if the circuit pressure measured is greater than high circuit 
//    pressure limit setting.
//    The trigger can be enabled or disabled, depending upon the current 
//    breath phase and the applicability of the trigger.  When the pressure 
//    condition calculated by this trigger is true, the trigger is considered 
//    to have "fired".  This trigger is kept on a list contained in the 
//    BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    It is necessary to end expiratory pause, and proceed directly to inspiration 
//    when the circuit pressure equals or exceeds the valid pressure limit.  
//    This trigger provides a means of monitoring for that condition.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of whether 
//    this trigger is enabled or not.  If the trigger is not enabled, it will 
//    always return state of false.  If it is enabled and on the active list
//    in the BreathTriggerMediator, the condition monitored 
//    by the trigger will be evaluated every BD cycle. The condition detected 
//    by this trigger is circuit pressure measured during expiratory pause
//    at a value >= operator set value for the high circuit 
//    pressure limit.
// $[04220] $[01253]
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/HighCircuitPressureInspTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  1-Apr-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  sp    Date:  7-Jan-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "HighCircuitPressureInspTrigger.hh"

//@ Usage-Classes
#include "PhasedInContextHandle.hh"
#include "BreathRecord.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: HighCircuitPressureInspTrigger()  
//
//@ Interface-Description
//	Default Contstructor
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using BreathTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

HighCircuitPressureInspTrigger::HighCircuitPressureInspTrigger(
	void
) : BreathTrigger(Trigger::HIGH_CIRCUIT_PRESSURE_INSP)
{
  CALL_TRACE("HighCircuitPressureInspTrigger()");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~HighCircuitPressureInspTrigger()  
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

HighCircuitPressureInspTrigger::~HighCircuitPressureInspTrigger(void)
{
  CALL_TRACE("~HighCircuitPressureInspTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
HighCircuitPressureInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, HIGHCIRCUITPRESSUREINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns 
//    false.  Triggers if circuit pressure >= operator set
//    value for the high circuit pressure limit.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If the circuit pressure is greater than or equal to the high circuit 
//    pressure limit setting then this method returns true else returns false.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
HighCircuitPressureInspTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean rtnValue = FALSE;
  Real32 highCircuitPressureValue =
	PhasedInContextHandle::GetBoundedValue(SettingId::HIGH_CCT_PRESS).value;
 
  if ( BreathRecord::GetAirwayPressure() >= highCircuitPressureValue )
  {
	// $[TI2]
        rtnValue = TRUE;
  }
  // $[TI1]
 
  // TRUE: $[TI3]
  // FALSE: $[TI4]
  return (rtnValue);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
