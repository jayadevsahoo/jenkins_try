
#ifndef CommFaultId_HH
#define CommFaultId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename:  CommFaultId - Enumerators of the Communication Faults.
//---------------------------------------------------------------------
// Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/CommFaultId.hhv   25.0.4.0   19 Nov 2013 14:35:12   pvcs  $
//
// Modifications
//
//   Revision: 005   By: gdc   Date: 18-Aug-2009   SCR Number: 6147
//   Project: XB
//   Description:
//   	Added safety-Net diagnostic messages for BD monitor alerts.
//
//   Revision: 003   By: sah   Date: 12-Mar-1997   DCS Number: 1830
//   Revision: 004   By: hlg   Date: 06-Sep-2001   DCS Number: 5954
//   Project:  GuiComms
//   Description:
//	Added DCI_*_ERROR_PORT{1,2,3} for three
//	serial ports.
//
//   Revision: 003   By: sah   Date: 12-Mar-1997   DCS Number: 1830
//   Project:  Sigma (R8027)
//   Description:
//	Modified 'LOW_NETAPP_FAULT_VALUE' back to having a value of 0,
//	and added new placeholder enumerator ('NETWORKAPP_NULL_EVENT')
//	to represent the value 0.
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//
//   Revision: 001   By: jhv   Date: 30-Mar-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================


//@ Type:  CommFaultID
// Communication fault identifier.
enum CommFaultID
{
	UNDEFINED_COMM_FAULT          =  -1,

	LOW_NETAPP_FAULT_VALUE        =   0, 
	NETWORKAPP_NULL_EVENT        =   LOW_NETAPP_FAULT_VALUE,
	NETWORKAPP_EPERM         =   1,
	NETWORKAPP_ENOENT            =   2,
	NETWORKAPP_ESRCH         =   3,
	NETWORKAPP_EINTR         =   4,
	NETWORKAPP_EIO           =   5,
	NETWORKAPP_ENXIO         =   6,
	NETWORKAPP_E2BIG         =   7,
	NETWORKAPP_ENOEXEC           =   8,
	NETWORKAPP_EBADF         =   9,
	NETWORKAPP_ECHILD            =  10,
	NETWORKAPP_EAGAIN            =  11,
	NETWORKAPP_ENOMEM            =  12,
	NETWORKAPP_EACCES            =  13,
	NETWORKAPP_EFAULT            =  14,
	NETWORKAPP_ENOTBLK           =  15,
	NETWORKAPP_EBUSY         =  16,
	NETWORKAPP_EEXIST            =  17,
	NETWORKAPP_EXDEV         =  18,
	NETWORKAPP_ENODEV            =  19,
	NETWORKAPP_ENOTDIR           =  20,
	NETWORKAPP_EISDIR            =  21,
	NETWORKAPP_EINVAL            =  22,
	NETWORKAPP_ENFILE            =  23,
	NETWORKAPP_EMFILE            =  24,
	NETWORKAPP_ENOTTY            =  25,
	NETWORKAPP_ETXTBSY           =  26,
	NETWORKAPP_EFBIG         =  27,
	NETWORKAPP_ENOSPC            =  28,
	NETWORKAPP_ESPIPE            =  29,
	NETWORKAPP_EROFS         =  30,
	NETWORKAPP_EMLINK            =  31,
	NETWORKAPP_EPIPE         =  32,
	NETWORKAPP_EDOM          =  33,
	NETWORKAPP_ERANGE            =  34,
	NETWORKAPP_EWOULDBLOCK       =  35,
	NETWORKAPP_EINPROGRESS       =  36,
	NETWORKAPP_EALREADY          =  37,
	NETWORKAPP_ENOTSOCK          =  38,
	NETWORKAPP_EDESTADDRREQ      =  39,
	NETWORKAPP_EMSGSIZE          =  40,
	NETWORKAPP_EPROTOTYPE        =  41,
	NETWORKAPP_ENOPROTOOPT       =  42,
	NETWORKAPP_EPROTONOSUPPORT       =  43,
	NETWORKAPP_ESOCKTNOSUPPORT       =  44,
	NETWORKAPP_EOPNOTSUPP        =  45,
	NETWORKAPP_EPFNOSUPPORT      =  46,
	NETWORKAPP_EAFNOSUPPORT      =  47,
	NETWORKAPP_EADDRINUSE        =  48,
	NETWORKAPP_EADDRNOTAVAIL     =  49,
	NETWORKAPP_ENETDOWN          =  50,
	NETWORKAPP_ENETUNREACH       =  51,
	NETWORKAPP_ENETRESET         =  52,
	NETWORKAPP_ECONNABORTED      =  53,
	NETWORKAPP_ECONNRESET        =  54,
	NETWORKAPP_ENOBUFS           =  55,
	NETWORKAPP_EISCONN           =  56,
	NETWORKAPP_ENOTCONN          =  57,
	NETWORKAPP_ESHUTDOWN         =  58,
	NETWORKAPP_ETOOMANYREFS      =  59,
	NETWORKAPP_ETIMEDOUT         =  60,
	NETWORKAPP_ECONNREFUSED      =  61,
	NETWORKAPP_ELOOP         =  62,
	NETWORKAPP_ENAMETOOLONG      =  63,
	NETWORKAPP_EHOSTDOWN         =  64,
	NETWORKAPP_EHOSTUNREACH      =  65,
	NETWORKAPP_INVALID_COMM_DATA     =  66,
	NETWORKAPP_DATA_TOO_LARGE        =  67,
	NETWORKAPP_INVALID_COMM_MSGID    =  68,
	NETWORKAPP_NO_ACK_MESSAGE        =  69,
	NETWORKAPP_MAX_ACTIVE_MSG_DELAY  =  70,
	NETWORKAPP_BLOCKIO_NO_ACCEPT_CALLBACK=  71,
	NETWORKAPP_BLOCKIO_SOCKET_ALREADY_OPENED=72,
	NETWORKAPP_WRONG_INPUT_COUNT     =  73,
	NETWORKAPP_OUT_OF_ACKBUFFER      =  74,
	NETWORKAPP_MSG_TRANSMISSION_FAILED   =  75,
	NETWORKAPP_CONNECT_TIMEOUT       =  76,
	NETWORKAPP_RECV_TIMEOUT      =  77,
	HIGH_NETAPP_FAULT_VALUE       = NETWORKAPP_RECV_TIMEOUT,
	NUM_NETAPP_FAULTS = (HIGH_NETAPP_FAULT_VALUE-LOW_NETAPP_FAULT_VALUE+1),

	LOW_STWARE_FAULT_VALUE        = 1000,
	STWARE_LOG_ERROR         = LOW_STWARE_FAULT_VALUE,
	STWARE_LOG_DIAGNOSTIC        = 1001,
	STWARE_IN_CONTROL_UNLINK_INIFADDR_IFP= 1002,   // diagnostic error 
	STWARE_IN_CONTROL_UNLINK_INIFADDR_LIST=1003,   // diagnostic error
	STWARE_IN_CKSUM_C_OUT_OF_DATA    = 1004,   // diagnostic error
	STWARE_XSRN_ADDROUTE_MASK        = 1005,   // diagnostic error
	STWARE_XSRN_DELETE_INCONSISTENT  = 1006,   // diagnostic error
	STWARE_XSRN_DELETE_NO_ANNOTATION = 1007,   // diagnostic error
	STWARE_XSRN_DELETE_NO_US     = 1008,   // diagnostic error
	STWARE_XSRN_DELETE_ORPHANED_MASK = 1009,   // diagnostic error
	STWARE_UDP_USRREQ_UNEXPECTED_CONTROL_DATA=1010,// diagnostic error
	STWARE_XS_STARTUP_NO_UNIT        = 1011,   // diagnostic error
	STWARE_NO_MEMORY_FOR_IFADDR      = 1012,   // Serious Stackware Error
	STWARE_LOOUTPUT_CANNOT_HANDLE_IF     = 1013,   // Serious Stackware Error
	STWARE_XSRN_INSERT_COMING_OUT    = 1014,   // Serious Stackware Error
	STWARE_MCOPYDATA_NEGATIVE_OFFSET = 1015,   // Serious Stackware Error
	STWARE_MCOPYDATA_NULL_DATA_PTR   = 1016,   // Serious Stackware Error
	STWARE_SBDROP_LEN_TOO_LARGE1     = 1017,   // Serious Stackware Error
	STWARE_SBDROP_LEN_TOO_LARGE2     = 1018,   // Serious Stackware Error
	STWARE_SBCOMPRESS_NO_EOR     = 1019,   // Serious Stackware Error
	STWARE_ARPINPUT_DUPLICATE_IP_ADDR    = 1020,   // Serious Stackware Error
	STWARE_XS_REGISTERIF_INVALID_TYPE    = 1021,   // Serious Stackware Error
	STWARE_XS_REGISTERIF_INVALID_FAMILY  = 1022,   // Serious Stackware Error
	STWARE_XS_DRIVIOCTL_INIT_FAILURE = 1023,   // Serious Stackware Error
	STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT=1024,   // Serious Stackware Error
	STWARE_XS_GOODETHERRARP_WRONG_HWTYPE = 1025,   // Serious Stackware Error
	STWARE_XS_GOODETHERRARP_SHORT_RARP_PKT2=1026,  // Serious Stackware Error
	STWARE_XS_GOODETHERRARP_INVALID_PROTOCOL=1027, // Serious Stackware Error
	STWARE_M_COPYM_LEN_TOO_LARGE     = 1028,
	STWARE_M_COPYM_OFFSET_TOO_LARGE  = 1029,
	STWARE_M_COPYDATA_OUT_OF_MBUF    = 1030,
	STWARE_M_COPYDATA_LEN_TOO_LARGE  = 1031,
	STWARE_M_COPYDATA_OFFSET_NEGATIVE    = 1032,
	STWARE_M_COPYDATA_NULL_POINTER   = 1033,
	STWARE_MXS_MBGET_OUT_OF_MBUF     = 1034,   // Serious Stackware Error
	STWARE_M_CLALLOC_OUT_OF_CLICK    = 1035,   // Serious Stackware Error
	STWARE_MBUF_MULTI_MFREE      = 1036,   // Serious Stackware Error
	STWARE_MALLOC_FAILED         = 1037,
	STWARE_DUMMY_ERROR1          = 1038,
	STWARE_DUMMY_ERROR2          = 1039,
	STWARE_INFINITE_MBUF_LINK        = 1040,   // Serious Stackware Error
	HIGH_STWARE_FAULT_VALUE       = STWARE_INFINITE_MBUF_LINK,
	NUM_STWARE_FAULTS = (HIGH_STWARE_FAULT_VALUE-LOW_STWARE_FAULT_VALUE+1),

	LOW_DCI_FAULT_VALUE           = 2000,
	//default
	DCI_PARITY_ERROR         = LOW_DCI_FAULT_VALUE,
	DCI_INPUT_BUFFER_OVERFLOW_ERROR  = 2001,
	DCI_NON_SPECIFIC_ERROR       = 2002,
	DCI_UNKNOWN_ERROR            = 2003,
///////////////////////////////////////////////////////////////////////////////////
/////	Note that the following ennumerations (*_PORT1, *_PORT2, and *_PORT3) /////
/////   are dependent on method error in DciReport.cc in subsystem DCI.       /////    
///////////////////////////////////////////////////////////////////////////////////
	//Port 1
	DCI_PARITY_ERROR_PORT1       = DCI_PARITY_ERROR,
	DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT1= DCI_INPUT_BUFFER_OVERFLOW_ERROR,
	DCI_NON_SPECIFIC_ERROR_PORT1     = DCI_NON_SPECIFIC_ERROR,
	DCI_UNKNOWN_ERROR_PORT1      = DCI_UNKNOWN_ERROR,
	//Port 2
	DCI_PARITY_ERROR_PORT2       = 2010,
	DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT2= 2011,
	DCI_NON_SPECIFIC_ERROR_PORT2     = 2012,
	DCI_UNKNOWN_ERROR_PORT2      = 2013,
	//Port 3
	DCI_PARITY_ERROR_PORT3       = 2020,
	DCI_INPUT_BUFFER_OVERFLOW_ERROR_PORT3= 2021,
	DCI_NON_SPECIFIC_ERROR_PORT3     = 2022,
	DCI_UNKNOWN_ERROR_PORT3      = 2023,
	HIGH_DCI_FAULT_VALUE          = DCI_UNKNOWN_ERROR+8,  //8 additional unique error codes
	NUM_DCI_FAULTS = (HIGH_DCI_FAULT_VALUE-LOW_DCI_FAULT_VALUE+1),

	LOW_SAFETYNET_FAULT_VALUE		= 3000,
	SAFETYNET_CHECKSUM_ERROR		= 3000,
	SAFETYNET_CYCLE_TIME_ERROR		= 3001,
	SAFETYNET_INTERVAL_TIME_ERROR		= 3002,
	SAFETYNET_SYNC_ACQUIRED			= 3003,
	SAFETYNET_SYNC_LOST			= 3004,
	HIGH_SAFETYNET_FAULT_VALUE		= 3009,
	NUM_SAFETYNET_FAULTS = (HIGH_SAFETYNET_FAULT_VALUE-LOW_SAFETYNET_FAULT_VALUE+1),

	// this must be the last line!
	NUM_COMM_FAULT_IDS = (NUM_NETAPP_FAULTS+NUM_STWARE_FAULTS+NUM_DCI_FAULTS+NUM_SAFETYNET_FAULTS)
};


#endif //defined(CommFaultId_HH)
