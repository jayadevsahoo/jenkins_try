/************************************************************************/
/* THIS INFORMATION IS PROPRIETARY TO                                   */
/* MICROTEC - A Mentor Graphics Company                                 */
/*----------------------------------------------------------------------*/
/* Copyright (c) 1995-1997 Microtec - A Mentor Graphics Company         */
/* All rights reserved                                                  */
/************************************************************************/
/*
**The software source code contained in this file and in the related
**header files is the property of Microtec. and may only be
**used under the terms and conditions defined in the Microtec
**license agreement. You may modify this code as needed but you must 
**retain the Microtec copyright notices, including this statement.
**Some restrictions may apply when using this software with non-Microtec
**software.  In all cases, Microtec reserves all rights.
*/
/************************************************************************/

/*
 *
 *	Module Name:		%M%
 *
 *	Identification:		%Z% %I% %M%
 *
 *	Date:			%G%  %U%
 *
 *
 */

#ifndef _in82596_H
#define _in82596_H

#include <logio.h>		/* logio defines and typedefs */
#include <vbsp.h>		/* VBSP return codes and typedefs */
#include <ether_1.h>
#include <extio_1.h>

#define LAN_MIN_SIZE   64	/* minimum packet size */
#define LAN_MAX_SIZE   1514	/* maximum packet size */

#define LERR_BLK_COUNT  1	/*bad block count*/
#define LERR_SEND       3	/*error in send*/
#define LERR_INIT       4	/*driver not initialized */
#define NUM_RECV        256	/*number of receive buffers*/
#define NUM_SEND        64	/*number of send buffers*/
#define SIM_SIZE        1534	/*size of buffs in simple mode*/
#define MAXMIT          1518	/*max transmit size*/

#define MAX_FRAME   1514	/* Max. Ethernet Frame size in bytes */
#define MIN_FRAME   64		/* Min Ethernet Frame size in bytes */
#define MAX_CB      16		/* Initial CBs allowed. */
#define MAX_TFD     24		/* Initial TFDs allowed. */
#define MAX_RFD     64		/* Initial RFDs in RFA. */

#define  MPF 1			/* maximum logio buffers per frame */

#define MAX_RBD ((MAX_RFD * MPF)+1)

#define TRUE    1
#define FALSE   0
#define NULL    0

/* -------------------------------------------------------------
** Little Endian vs Big Endian
**
** Longword definitions for the 82596 often have swapped bit
** definitions.  The following section defines the bitfields for
** both BIG Endian and LITTLE Endian type of processors.
**
** Byte swap macros dependent on byte order.
** Port accesses, dependent on byte order and mask of 82596.
** INTELx86 : LITTLE Endian (INTEL type)
------------------------------------------------------------- */


/* Bzero from RTcore is quite efficient for x86. Hence we use bzero instead
of local_bzero. For Motorola targets however, it is not been verified.
*/

#if defined(INTELx86) || defined(I960) /* LITTLE ENDIAN CPU */
  	#define swap32(x)                   x
   	#define ISCP_BUSY                   (unsigned long)0x00000001
   	#define ISCP_SYSBUS_CSW             (unsigned long)0x00400000
   	#define ISCP_SYSBUS_ACT_LOW         (unsigned long)0x00200000
   	#define ISCP_SYSBUS_LOCK_ENABLED    (unsigned long)0x00100000
   	#define ISCP_SYSBUS_EXTERNAL_TRIG   (unsigned long)0x00080000
   	#define ISCP_SYSBUS_LINEAR          (unsigned long)0x00040000
	#define	in82596a_zero	bzero
# if defined(INTELx86)
	#define DELAY                       1 /* INTELx86 uses in/out inst. */
# else
        #define DELAY                       10000
# endif
#else /* BIG Enidnan, non-intel */
	#define swap32(x)      ((((unsigned long )(x))<<16) | ((((unsigned long )(x)))>>16))
	#define ISCP_BUSY                   (unsigned long)0x00010000
	#define ISCP_SYSBUS_CSW             (unsigned long)0x00000040
	#define ISCP_SYSBUS_ACT_LOW         (unsigned long)0x00000020
	#define ISCP_SYSBUS_LOCK_ENABLED    (unsigned long)0x00000010
	#define ISCP_SYSBUS_EXTERNAL_TRIG   (unsigned long)0x00000008
	#define ISCP_SYSBUS_LINEAR          (unsigned long)0x00000004
	#define DELAY                    10000	/* Software timing loop */
	#define	in82596a_zero		local_bzero
#endif
  
/* common macros */
#define ISCP_SYSBUS_ACT_HIGH         	(unsigned long)0x00000000
#define ISCP_SYSBUS_LOCK_DISABLED    	(unsigned long)0x00000000
#define ISCP_SYSBUS_INTERNAL_TRIG   	(unsigned long)0x00000000
#define IN82596A_PORT_RESET         (unsigned long)0xFFFFFFF0
#define PORT_ALTERNATE_SCP_ADDRESS     2

/* Some of the functions are defined to be macros, for speed */

#define in82596a_ca(dev)  \
	REG_WRITE_L((dev), ((in82596a_reg_map_t *)(dev)->map)->ca, 1)

#define  in82596a_wait_scb_cmd_zero(dev) \
{ unsigned short volatile *p = \
      &((in82596a_specific_t *)(dev)->config.specific)->lanws.scb.cmd; \
    while (*p); } 

/*
** Define board related constants here.
*/

/* Action Commands */

#define NOP                      0x0
#define IASETUP                  0x1
#define CONFIGURE                0x2
#define MCSETUP                  0x3
#define TRANSMIT                 0x4
#define TDR                      0x5
#define DUMP                     0x6
#define DIAGNOSE                 0x7


#define STATUS_COMMAND_COMPLETE  0x8000
#define STATUS_BUSY              0x4000
#define STATUS_OK                0x2000
#define STATUS_FAIL              0x0800

#define ISCP_DELAY               0x1000

#define SCB_CMD_EL               0x8000
#define SCB_RUS                  0x00F0	/* RX status mask		*/
#define	SCB_RU_IDLE		 0x0000	/* RX is idle			*/
#define	SCB_RU_SUSP		 0x0010	/* RX is suspended		*/
#define	SCB_RU_NO_RES		 0x0020	/* RX has no resources		*/
#define SCB_RU_READY             0x0040	/* RX is ready			*/
#define	SCB_RU_NO_RES_RBD	 0x00A0	/* RX has no resources, no RBDs	*/
#define	SCB_RU_NO_RBD		 0x00C0	/* RX has no RBDs		*/
#define SCB_CUS                  0x0700
#define SCB_CU_ACTIVE            0x0200
#define SCB_ST                   0xF000
#define SCB_ST_CX                0x8000
#define SCB_ST_FR                0x4000
#define SCB_ST_CNA               0x2000
#define SCB_ST_RNR               0x1000
#define SCB_ST_CUS               0x0700
#define SCB_ST_CUS_AC            0x0200
#define SCB_ST_RUS               0x00f0
#define SCB_ST_BUS_T             0x0008
#define SCB_CU_NOP               0x0000
#define SCB_CU_START             0x0100
#define SCB_CU_RESUME            0x0200
#define SCB_CU_SUSPEND           0x0300
#define SCB_CU_ABORT             0x0400
#define SCB_RU_NOP               0x0000
#define SCB_RU_START             0x0010
#define SCB_RU_RESUME            0x0020
#define SCB_RU_SUSPEND           0x0030
#define SCB_RU_ABORT             0x0040
#define SCB_CMD_ACK              0xF000

#define TFD_CMD_EOL                 (unsigned short)0x8000
#define TFD_CMD_INTERRUPT           (unsigned short)0x2000
#define TFD_CMD_TRANSMIT            (unsigned short)0x0004
#define TFD_CMD_FLEXIBLE            (unsigned short)0x0008

#define TFD_STATUS_COMPLETE         (unsigned short)0x8000
#define TBD_EOF                     (unsigned short)0x8000
#define TBD_SIZE_MASK               (unsigned short)0x3FFF

#define RFD_STATUS_COMPLETE         (unsigned short)0x8000
#define RFD_STATUS_BUSY             (unsigned short)0x4000
#define RFD_STATUS_OK               (unsigned short)0x2000
#define RFD_CMD_EL                  (unsigned short)0x8000
#define RFD_CMD_S                   (unsigned short)0x4000
#define RFD_CMD_FLEXIBLE            (unsigned short)0x0008
#define RFD_EOF                     (unsigned short)0x8000
#define RFD_F                       (unsigned short)0x4000

#define RBD_STATUS_EOF              (unsigned short)0x8000
#define RBD_STATUS_F                (unsigned short)0x4000
#define RBD_STATUS_COUNT_MASK       (unsigned short)0x3FFF
#define RBD_EL                      (unsigned short)0x8000
#define RBD_P                       (unsigned short)0x4000
#define RBD_SIZE_MASK               (unsigned short)0x3FFF

/*
** System Configuration Pointer. (Page 4-110)
*/
typedef struct SCP_struct {
    unsigned long sysbus;
    unsigned long sb_zeros;	/* zeros */
    unsigned long ISCP_Addr;	/* ISCP */
}SCP;

/* Intermediate System Configuration Pointer. (Page 4-113) */
typedef struct ISCP_struct {
    unsigned long busy;
    unsigned long SCB_Addr;	/* ptr to System Control Block */
}ISCP;

/*
 * System Configuration Block. Some of the elements have
 * different meanings if the 82596 is used in Monitor mode.
 * (Page 4-114)
 */

typedef struct SCB_struct {
    unsigned short status;
    unsigned short cmd;
    unsigned long *CBL_addr;		/* CBL Address */
    unsigned long *RFA_addr;		/* RFA Address */
    unsigned long CRCErrs;		/* CRC Errors */
    unsigned long AlinErrs;		/* Alignment Errors */
    unsigned long RescErrs;		/* Resource Errors */
    unsigned long OvernErrs;		/* Overrun Errors */
    unsigned long RcvcdtErrs;		/* RCVCDT Errors */
    unsigned long ShortFrameErrs;	/* Short Frame Errors */
    unsigned short TOffTimer;		/* T-ON TIMER */
    unsigned short TOnTimer;		/* T-OFF TIMER */
}SCB;

/*
 * Command block structure. (Page 4-121)
 */

typedef struct CB_struct {
    unsigned short status;
    unsigned short cmd;
    struct CB_struct *next;	/* addr of next command block */
}CB;

/* Transmit frame descriptor command structure. (Page 4-143) */

typedef struct TFD_struct {
    unsigned short status;
    unsigned short cmd;
    unsigned long *next;		/* addr of next command block */
    unsigned long *tbd_addr;		/* transmit buffer descriptor */
    unsigned short tcb_count;
    unsigned short zeros;
    unsigned char da[6];		/* destination address */
    unsigned short type;		/* user defined data field */
    unsigned long padding[2];		/* to pad to multiple of 4 longs */
}TFD;

/* Transmit buffer descriptor structure. (Page 4-147) */
typedef struct TBD_struct {
    unsigned short eof_size;
    unsigned short zeros;
    unsigned long *tbd_addr;		/* addr of next TBD for the frame */
    unsigned long *tb_addr;		/* addr of the transmission buffer */
    logio_buff_t *plogio;
}TBD;

/* Receive frame descriptor structure. (Page 4-170) */
typedef struct RFD_struct {
    unsigned short status;
    unsigned short cmd;
    unsigned long *next;		/* addr of next receive frame descriptor */
    unsigned long *rbd_addr;	/* addr of receive buffer descriptor */
    unsigned short eof_f_count;
    unsigned short size;
    unsigned char da[6];		/* destination address */
    unsigned char sa[6];		/* source address */
    unsigned short type;		/* user defined data field */
    unsigned short dcount;		/* used to pad to multiple of 4 longs */
}RFD;

/* Receive buffer descriptor structure. (Page 4-173) */

typedef struct RBD_struct {
    unsigned short status;
    unsigned short zeros;
    unsigned long *rbd_addr;		/* next receive buffer descriptor */
    unsigned long *rb_addr;		/* addr of receive buffer */
    unsigned short el_p_size;
    unsigned short zeros2;
    logio_buff_t *plogio;
    unsigned long padding[2];		/* pad to multiple of 4 longs */
    unsigned long dcount;
}RBD;

/* Configuration command structure. (Page 4-125) */
#define USE_CONFIG_BITFIELDS
typedef struct Conf_struct {
    unsigned short status;
    unsigned short cmd;
    unsigned long next;			/* addr of next command block */
    unsigned char p_bytecount;		/* byte 0  */
    unsigned char monitor_fifolimit;	/* byte 1  */
    unsigned char savbf;		/* byte 2  */
    unsigned char loop_preamble_addr;	/* byte 3  */
    unsigned char bof_priority;		/* byte 4  */
    unsigned char interframe_space;	/* byte 5  */
    unsigned char slot_time_low;	/* byte 6  */
    unsigned char retry_slot_high;	/* byte 7  */
    unsigned char byte8;		/* byte 8  */
    unsigned char byte9;		/* byte 9  */
    unsigned char min_frame_len;	/* byte 10 */
    unsigned char byte11;		/* byte 11 */
    unsigned char fdx;			/* byte 12 */
    unsigned char disable_bof_multi_1a;	/* byte 13 */
}CONF;

typedef struct IndAddr_struct {
    unsigned short status;
    unsigned short cmd;
    unsigned long next;			/* addr of next command block */
    unsigned char ind_addr[6];
    unsigned short padding;
}INDADDR;

/* Diagnose command structure. (Page 4-165) */

typedef struct Diag_struct {
    unsigned short status;
    unsigned short cmd;
    unsigned long next;			/* addr of next command block */
}DIAG;

struct comws {
    SCP *scp;				/* system configuration pointer */
    char SCP_Buffer[sizeof(SCP) + 0x30]; /* SCP buffer */
    ISCP iscp;				/* intermediate system configuration structure */
    SCB scb;
    RFD *begin_rfd,			/* begin and end recv frame pointers */
    *end_rfd;
    logio_buff_t
    	* plogio_full_b,
    	*plogio_full_e;
    RBD *begin_rbd, *end_rbd;
    unsigned long padding1[2];		/* align to 4 longword boundry */
    RFD rfa[MAX_RFD];			/* recv frame area */
    RBD rbda[MAX_RBD];			/* recv buffer desc area */
    TFD *begin_tfd,			/* start and end pointers for free
					   transmit */
    *end_tfd,				/* descriptors list */
    *begin_cbl,				/* start and end pointers for the
					   transmit desc. */
    *end_cbl;				/* on the 82596's list */
    TBD *begin_tbd, *end_tbd;
    RBD *prbd_end;
    unsigned long num_unlinked_rfds;
    unsigned long plogio_num;		/* align to 4 longword boundry */
    TFD tfda[MAX_TFD];			/* transmit frame desc area */
    TBD tbda[MAX_TFD * MPF];		/* transmit buffer desc area */
    unsigned short free_tbd_cnt;
};

typedef struct {
    int shadow_int_flags;
    struct comws lanws;
    unsigned short intr_stat;
}

in82596a_specific_t;

typedef struct {
    volatile unsigned short *ucw;
    volatile unsigned short *lcw;
    volatile unsigned long *ca;
}

in82596a_reg_map_t;

typedef struct {
    ether_1_config_t base_config;
    extern_fops_t externio;
    in82596a_reg_map_t reg;
    in82596a_specific_t specific;
} in82596a_config_t;

vbsp_return_t in82596a_install(_ANSIPROT2(ether_1_dev_desc_t *,
					  in82596a_config_t *));
vbsp_return_t in82596a_init(_ANSIPROT1(ether_1_dev_desc_t *));
vbsp_return_t in82596a_recv(_ANSIPROT2(ether_1_dev_desc_t *,
				       logio_buff_t ** /* out */ ));
vbsp_return_t in82596a_send(_ANSIPROT2(ether_1_dev_desc_t *,
				       logio_buff_t * /* in  */ ));

int in82596a_txtst(_ANSIPROT1(logio_interrupt_entry_t *));
int in82596a_rxtst(_ANSIPROT1(logio_interrupt_entry_t *));
int in82596a_errtst(_ANSIPROT1(logio_interrupt_entry_t *));
int in82596a_inittst(_ANSIPROT1(logio_interrupt_entry_t *));

vbsp_return_t in82596a_rx_buf_init(_ANSIPROT1(struct ether_1_dev_desc_t *));
logio_status_t in82596a_int_info_get(_ANSIPROT2(struct ether_1_dev_desc_t *,
						logio_int_entry_t *));
logio_status_t in82596a_int_info_set(_ANSIPROT2(struct ether_1_dev_desc_t *,
						logio_int_entry_t *));

/*
** This section declares all utility functions used by the in82596a driver.
** "Utility" functions are those functions that implement a logical chip
** operation and are not one of the high-level logio routines.
*/
int in82596a_init_scp_and_iscp(ether_1_dev_desc_t * dev);
int in82596a_init_2(ether_1_dev_desc_t * dev);
int in82596a_diagnose(ether_1_dev_desc_t * dev);
int in82596a_configure(ether_1_dev_desc_t * dev);
int in82596a_IA(ether_1_dev_desc_t * dev);
int in82596a_ca_no_wait(ether_1_dev_desc_t * dev);
int in82596a_write(ether_1_dev_desc_t * dev, unsigned long value);
int in82596a_soak_up(ether_1_dev_desc_t * dev);
int in82596a_ack_cmd(ether_1_dev_desc_t * dev);
int in82596a_wait_scb(ether_1_dev_desc_t * dev);
/*
int in82596a_wait_scb_cmd_zero(ether_1_dev_desc_t * dev);
*/
int in82596a_ru_start(ether_1_dev_desc_t * dev);
int in82596a_ru_restart(ether_1_dev_desc_t * dev, RFD * rfd);
int in82596a_eat_time(int count);
void in82596a_restart_cu(ether_1_dev_desc_t * dev, TFD * tfd);
int in82596a_ca_and_wait(ether_1_dev_desc_t * dev);
int in82596a_init_chip(ether_1_dev_desc_t * dev);
int in82596a_data_init(ether_1_dev_desc_t * dev);

int in82596a_spurious_int_test(logio_interrupt_entry_t * entry);
#endif

