
#ifndef DblBuff_BtatEventState_HH
#define DblBuff_BtatEventState_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  DblBuff_BtatEventState - Double-Buffered Data Class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/DoubleBufferData.H_v   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  sah    Date:  25-Jun-1997    DCS Number: 1908
//  Project:  Sigma (R8027)
//  Description:
//     Add mechanism to allow different semaphores to be used with different
//     instances of this class.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "PersistentObjsMacros.hh"

//@ Usage-Classes
#include "BtatEventState.hh"
#include "DoubleBuffer.hh"
#include "NovRamSemaphore.hh"
//@ End-Usage


class DblBuff_BtatEventState : public DoubleBuffer
{
  public:
    DblBuff_BtatEventState(const NovRamSemaphore::AccessIdType semaphoreId =
					NovRamSemaphore::DOUBLE_BUFF_ACCESS);
    ~DblBuff_BtatEventState(void);

    NovRamStatus::InitializationId  initialize(
    		const NovRamSemaphore::AccessIdType semaphoreId =
					NovRamSemaphore::DOUBLE_BUFF_ACCESS
					      );
    NovRamStatus::VerificationId    verifySelf(
    		const NovRamSemaphore::AccessIdType semaphoreId =
					NovRamSemaphore::DOUBLE_BUFF_ACCESS
					      );

    void  getData   (BtatEventState& rData) const;
    void  updateData(const BtatEventState& newData);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL,
			   const char*       pPredicate = NULL);

  private:
    DblBuff_BtatEventState(const DblBuff_BtatEventState&);  // not implemented...
    void  operator=(const DblBuff_BtatEventState&);	    // not implemented...

    inline NovRamSemaphore::AccessIdType  getSemaphoreId_(void) const;

    inline void  updateSemaphoreId_(
			  const NovRamSemaphore::AccessIdType semaphoreId
				   );

    inline Boolean  isSemaphoreIdValid_(void) const;

    //@ Data-Member:  arrDataItems_
    // The data items to be used as the two buffers.
    BtatEventState  arrDataItems_[2];

    //@ Type:  SemaphoreLayout_
    // This structure serves two purposes, first to ensure that no "padding"
    // is added by the compiler that includes memory not managed by this
    // class (that's why this 'struct' takes up a full long word), and,
    // secondly, provide a checksum mechanism for the semaphore id.
    struct SemaphoreLayout_
    {
      Uint16  checksum;
      Uint16  semaphoreId;
    };

    union  // anonymous union...
    {
      //@ Data-Member:  semaphoreLayout_
      // This contains the two semaphore fields:  the id, and the checksum.
      SemaphoreLayout_  semaphoreLayout_;

      //@ Data-Member:  semaphoreMemory_
      // This data item provides a clear means of updating the semaphore id
      // and checksum in one, non-interruptable long word update.
      Uint32  semaphoreMemory_;
    };
};


// Inlined Methods...
#include "DblBuff_BtatEventState.in"


#endif // DblBuff_BtatEventState_HH 
