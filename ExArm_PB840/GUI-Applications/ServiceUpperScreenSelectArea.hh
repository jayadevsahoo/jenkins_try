#ifndef ServiceUpperScreenSelectArea_HH
#define ServiceUpperScreenSelectArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceUpperScreenSelectArea - Area which contains screen select
// buttons for SST Results, Diagnostic Log Results, Alarm Log,
// Vent Configuration, Operation Time screen, Vent Test Summary.  Only
// in Development mode, Development options is available.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceUpperScreenSelectArea.hhv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  rhj    Date:  21-Oct-2008    SCR Number:  6435  
//    Project:  S840
//    Description:
//      Added access method: getDevelopmentOptionTabButton to allow the display
//		Development Options Subscreen in service mode.
// 
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  24-SEP-1997    DR Number:   1861
//    Project:  Sigma (R8027)
//    Description:
//      Added access method: getVentTestSummaryTabButton to allow the display
//		various test results to changes in service mode.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "Box.hh"
#include "TabButton.hh"
#include "AlarmLogTabButton.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ServiceUpperScreenSelectArea : public Container
{
public:
	ServiceUpperScreenSelectArea(void);
	~ServiceUpperScreenSelectArea(void);

	void initialize(void);
	void updateButtonDisplay(Boolean status);
	void setBlank(Boolean blank);

	inline TabButton *getDiagResultTabButton(void);
	inline TabButton *getVentTestSummaryTabButton(void);
    inline TabButton *getDevelopmentOptionTabButton(void);
	
	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	ServiceUpperScreenSelectArea(const ServiceUpperScreenSelectArea&);	// not implemented...
	void operator=(const ServiceUpperScreenSelectArea&);			// not implemented...

	//@ Data-Member: sstResultTabButton_
	// Screen select button for the SstResult subscreen.
	TabButton sstResultTabButton_;

	//@ Data-Member: diagResultTabButton_
	// Screen select button for the More Data subscreen.
	TabButton diagResultTabButton_;

	//@ Data-Member: serviceAlarmLogTabButton_
	// Screen select button for the Alarm Log subscreen.
	AlarmLogTabButton serviceAlarmLogTabButton_;

	//@ Data-Member: ventConfigTabButton_
	// Screen select button for the Configuration subscreen.
	TabButton ventConfigTabButton_;

	//@ Data-Member: operationalTimeTabButton_
	// Screen select button for the operational times of the major
	// system modules.
	TabButton  operationalTimeTabButton_;

	//@ Data-Member: ventTestSummaryTabButton_
	// Screen select button for the Ventilator Test Summary subscreen.
	TabButton  ventTestSummaryTabButton_;

	//@ Data-Member: developmentOptionTabButton_
	// Screen select button for the Ventilator Development Options subscreen.
	TabButton  developmentOptionTabButton_;

	//@ Data-Member: topLineBox_
	// Box used for displaying white border at the top of this area,
	// coincident with the bottom border for the UpperSubScreenArea.
	Box topLineBox_;

};

// Inlined methods
#include "ServiceUpperScreenSelectArea.in"

#endif // ServiceUpperScreenSelectArea_HH 
