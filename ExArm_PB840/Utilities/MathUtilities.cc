#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Filename:  MathUtilities - System-Wide Math Utilities.
//---------------------------------------------------------------------
//@ Interface-Description
//  This module provides a set of functions to operate on
//  floating-point numbers.  These functions provide a safe, standard
//  way for floating-point operations to occur.
//
//  These functions base their operation on a level of precision of the
//  value(s) being operated on.  These precision-based functions include
//  functions to test for equivalence of two floats, rounding of a float
//  value, extracting the whole-number part of a float, extracting the
//  fractional part of a float, determining the fractional remainder of
//  the division of two floating-point numbers, and calculating the
//  "magnitude" of a floating point value (moving its decimal point).
//---------------------------------------------------------------------
//@ Rationale
//  The use of these utilitis allows control over the accuracy of the
//  results of floating-point values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  One math function ('::modf()') is used by these functions for
//  breaking a floating-point number into a whole (integer) part and
//  a fractional part.
//
//  If the number of precisions change (see 'Precision' in 'Precision.hh')
//  then all of the pre-conditions of these functions will also need
//  changing.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard assertion macros are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/MathUtilities.ccv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//
//   Revision: 008   By: gdc   Date: 24-Aug-2010   SCR Number: 6663
//   Project:  API/MAT
//   Description:
//		Added Fletcher32 function.
//
//   Revision: 007   By: gdc   Date: 26-Sep-2006   DCS Number: 6236
//   Project:  RESPM
//   Description:
//		Added IsFpError function.
//
//   Revision: 006   By: gdc   Date: 16-Jun-1998   DCS Number: 5055
//   Project:  Color
//   Description:
//		Initial version.
//
//   Revision: 005   By: sah   Date: 18-Mar-1997   DCS Number: 1847
//   Project:  Sigma (R8027)
//   Description:
//	Correct testable item labelling, and remove unreachable paths.
//
//   Revision: 004   By: sah   Date: 04-Mar-1997   DCS Number: 1812
//   Project:  Sigma (R8027)
//   Description:
//	Need to improve the speed and accuracy of these functions.
//
//   Revision: 003   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//
//   Revision: 002   By: sah   Date: 14-Mar-1995   DR Number: 405
//   Project:  Sigma (R8027)
//   Description:
//	Fixed remainder problem with 'Modulus()' by checking if the
//	calculated remainder is equivalent to the divisor at the given
//	precision.  If so, a remainder of '0.0' is returned.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "MathUtilities.hh"

#include <math.h>
#include <string.h>
#include "UtilityClassId.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

// turn all optimizations on for this module...
#pragma option -Og
#pragma option -Ob
#pragma option -Oe
#pragma option -Ot

//=====================================================================
//
//  External Functions...
//
//=====================================================================

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsEquivalent(value1, value2, precision)
//
//@ Interface-Description
//  Is 'value1' equivalent to 'value2' based on the precision given
//  by 'precision'?
//---------------------------------------------------------------------
//@ Implementation-Description
//  Compare the "precise" values of each for equivalence.
//---------------------------------------------------------------------
//@ PreCondition
//  (precision <= THOUSANDS  &&  precision >= THOUSANDTHS)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean
IsEquivalent(const Real32    value1,
	     const Real32    value2,
	     const Precision precision)
{
	CALL_TRACE("::IsEquivalent(value1, value2, precision)");
	SAFE_FREE_PRE_CONDITION((precision <= THOUSANDS  &&
			   precision >= THOUSANDTHS),
			  UTILITIES, MATH_UTILITIES);

	static const Real32  ARR_ACCURACIES_[] =
	{
     	0.0005,		// THOUSANDTHS...
     	0.005,		// HUNDREDTHS...
     	0.05,		// TENTHS...
     	0.5,		// ONES...
     	5.0,		// TENS...
    	50.0,		// HUNDREDS...
   		500.0,		// THOUSANDS...
	};

	static const Real32* const  P_ACCURACIES_ = (ARR_ACCURACIES_ + THOUSANDS);

	const Real32  DIFF = ABS_VALUE((value1 - value2));

	return ( DIFF < P_ACCURACIES_[precision] );
}  // $[TI1] (TRUE)  $[TI2] (FALSE)


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  PreciseValue(value, precision)
//
//@ Interface-Description
//  Return the floating-point value of 'value' with its precision
//  based on 'precision'.  Rounding will be performed on the resulting
//  value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Round in the precision's decimal place by scaling the number
//  to where the ones place contains the significant digit to be
//  rounded. Add 0.5 and truncate. Then reverse scale and adjust
//  the sign.
//---------------------------------------------------------------------
//@ PreCondition
//  (precision >= THOUSANDTHS  &&  precision <= THOUSANDS)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Real32
PreciseValue(const Real32 value, const Precision precision)
{
  	CALL_TRACE("::PreciseValue(value, precision)");

  	SAFE_FREE_PRE_CONDITION(
		(precision <= THOUSANDS  && precision >= THOUSANDTHS),
		UTILITIES, MATH_UTILITIES);

	static const Real32  SCALE_FACTORS_[] =
	{
     	1000.0,	// THOUSANDTHS... -3
     	100.0,	// HUNDREDTHS...  -2
     	10.0,	// TENTHS...      -1
     	1.0,	// ONES...         0
     	0.1,	// TENS...         1
    	0.01,	// HUNDREDS...     2
   		0.001,	// THOUSANDS...    3
	};

	static const Real32* const  P_SCALE_FACTORS_ = SCALE_FACTORS_ + THOUSANDS;

  	Real32  scale = P_SCALE_FACTORS_[ precision ];

  	Real32  sign;
  	Real32  absvalue;

  	if ( value < 0.0 )
  	{														// $[TI1.1]
     	absvalue = -value;
     	sign  = -1.0;
  	}
  	else
  	{														// $[TI1.2]
     	absvalue = value;
     	sign  = 1.0;
  	}
  
  	return ( sign * ( int(absvalue * scale + 0.5) / scale ) );
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  WholePart(value)
//
//@ Interface-Description
//  Return the truncated (no rounding) whole-number part of the
//  floating-point value given by 'value'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use 'modf()' to break apart the whole part from the fractional
//  part.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Int32
WholePart(const Real32 value)
{
  CALL_TRACE("::WholePart(value)");

  double  wholePart;  // 'double' because of call to 'floor()/ceil()'...

  if (value >= 1.0f)
  {   // $[TI1]
    // 'value' contains a positive whole part in it, therefore use
    // '::floor()' to extract the whole number part...
    wholePart = ::floor(value);
  }
  else if (value <= -1.0f)
  {   // $[TI2]
    // 'value' contains a negative whole part in it, therefore use
    // '::ceil()' to extract the whole number part...
    wholePart = ::ceil(value);
  }
  else
  {  // $[TI3]
    // value does not contain a whole part, therefore the whole part
    // is '0.0'...
    wholePart = 0.0f;
  }

  return((Int32)wholePart);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  FractPart(value, precision)
//
//@ Interface-Description
//  Return the fractional part of the floating-point value given
//  by 'value'.  The fractional part will be rounded based on
//  'precision'.  If 'precision' is a whole-number precision (i.e.,
//  'precision >= ONES'), then zero is returned as the fractional part.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use 'modf()' to break apart the fractional part from the whole
//  part.
//---------------------------------------------------------------------
//@ PreCondition
//  (precision >= THOUSANDTHS   &&  precision <= THOUSANDS)
//---------------------------------------------------------------------
//@ PostCondition
//    ((precision > TENTHS  && ::FractPart(value, precision) == 0.0)  ||
//     (precision <= TENTHS && ABS_VALUE(::FractPart(value,precision)) < 1.0))
//@ End-Free-Function
//=====================================================================

Real32
FractPart(const Real32 value, const Precision precision)
{
  CALL_TRACE("::FractPart(value, precision)");
  SAFE_FREE_PRE_CONDITION((precision >= THOUSANDTHS  &&
			   precision <= THOUSANDS),
			  UTILITIES, MATH_UTILITIES);

  Real32  preciseFractPart;

  if (precision <= TENTHS)
  {   // $[TI1]
    Real32  fractPart;

    if (value >= 0.0f)
    {   // $[TI1.1]
      // extract the positive fractional part of 'value' by subtracting the
      // whole part from 'value'...
      fractPart = (Real32)((double)value - ::floor(value));
    }
    else
    {   // $[TI1.2]
      // extract the negative fractional part of 'value' by adding the
      // whole part from 'value'...
      fractPart = (Real32)((double)value - ::ceil(value));
    }

    // round to the precision given by 'precision'...
    preciseFractPart = ::PreciseValue(fractPart, precision);
  }
  else
  {   // $[TI2]
    // the requested precision requires a whole number, therefore there is
    // no fractional part...
    preciseFractPart = 0.0f;
  }

  return(preciseFractPart);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  Modulus(x, y, precision)
//
//@ Interface-Description
//  Return the floating-point remainder ('rem') of 'x / y'.  The remainder
//  has the same sign as 'x' such that 'rem == (x - (i * y))' for some
//  integer 'i'.  The final precision of the remainder is based on
//  'precision'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use 'fmod()' for calculating an "unrounded" remainder.
//---------------------------------------------------------------------
//@ PreCondition
//  (precision >= THOUSANDTHS  &&  precision <= THOUSANDS)
//---------------------------------------------------------------------
//@ PostCondition
//  (ABS_VALUE(::Modulus(x, y, precision)) <=
//  	ABS_VALUE(::PreciseValue(y, precision)))
//@ End-Free-Function
//=====================================================================

Real32
Modulus(const Real32 x, const Real32 y, const Precision precision)
{
  CALL_TRACE("::Modulus(x, y, precision)");
  SAFE_FREE_PRE_CONDITION((precision >= THOUSANDTHS  &&
			   precision <= THOUSANDS),
			   UTILITIES, MATH_UTILITIES);

  Real32  remainder;

  if (x != y)
  {   // $[TI1]
    // calculate the remainder of the division between 'x' and 'y'...
    remainder = (Real32)::fmod(x, y);

    if (remainder != 0.0f)
    {   // $[TI1.1]
      // the calculated remainder is non-zero, therefore "round" the
      // remainder based on 'precision'...
      remainder = ::PreciseValue(remainder, precision);

      if (remainder != 0.0f  &&
	  (remainder == y || ::PreciseValue((y-remainder),precision) == 0.0f))
      {   // $[TI1.1.1]
	// the remainder is still non-zero, but it is so close to the divisor
	// that the difference between the two is negligible at the given
	// precision, therefore return '0.0' as the remainder...
        remainder = 0.0f;
      }   // $[TI1.1.2] -- either 'remainder' is zero, or non-negligible...
    }   // $[TI1.2] -- 'remainder' is zero...
  }
  else
  {   // $[TI2] -- 'x' and 'y' are equivalent...
    // since 'x' and 'y' are equivalent, the remainder is zero...
    remainder = 0.0f;
  }

  return(remainder);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  Magnitude(base, magnitude)  [extern]
//
//@ Interface-Description
//  Return the floating-point value of 'base' multiplied by '10' to
//  the 'magnitude' power (e.g. Magnitude(base, magnitude) ==
//  (base * 10^magnitude)).
//---------------------------------------------------------------------
//@ Implementation-Description
//  This algorithm is set up such that there are no divisions needed
//  (divisions are more costly than multiplications), and the same loop
//  is used for both positive and negative magnitudes.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Real32
Magnitude(const Real32 base, const Int32 magnitude)
{
  CALL_TRACE("::Magnitude(base, magnitude)");

  // if 'magnitude' is zero, then return 'base', because 10^0 == 1...
  Real32  result = base;

  if (magnitude != 0)
  {   // $[TI1]
    const Real32  MULTIPLIER = (magnitude > 0) ? 10.0f   // $[TI1.1]
					       :  0.1f;  // $[TI1.2]

    Uint32  numIters = ABS_VALUE(magnitude);  // number of iterations...

    // start from the first magnitude and multiply the result by the
    // multiplier until there are no more magnitudes to apply...
    for (; numIters != 0; numIters--)
    {
      result *= MULTIPLIER;  // multiply running result by multiplier...
    }
  }   // $[TI2] -- 'magnitude' is zero...

  return(result);
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  DecimalPlaces(precision)
//
//@ Interface-Description
//  Returns the number of decimal places after the decimal point for
//  the specified precision.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (precision >= THOUSANDTHS  &&  precision <= THOUSANDS)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Int32
DecimalPlaces(const Precision precision)
{
  	CALL_TRACE("::DecimalPlaces(Precision)");

  	SAFE_FREE_PRE_CONDITION(
		(precision <= THOUSANDS  && precision >= THOUSANDTHS),
		UTILITIES, MATH_UTILITIES);

	static const Int32  DECIMAL_PLACES_[] =
	{
     	3,	// THOUSANDTHS... -3
     	2,	// HUNDREDTHS...  -2
     	1,	// TENTHS...      -1
     	0,	// ONES...         0
     	0,	// TENS...         1
    	0,	// HUNDREDS...     2
   		0,	// THOUSANDS...    3
	};

	static const Int32* const  P_DECIMAL_PLACES_ = DECIMAL_PLACES_ + THOUSANDS;

	// $[TI1]
  	return ( P_DECIMAL_PLACES_[ precision ] );
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  IsFpError
//
//@ Interface-Description
//  Returns TRUE if there is an error in the single precision floating
//  point number passed in as an argument. This function returns TRUE
//  for all negative/positive infinites and NAN (not-a-number). 
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the mantissa of the IEEE single precision floating point 
//  number contains the pattern representing any type of floating
//  point error, return TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Free-Function
//=====================================================================

Boolean IsFpError(Real32 value)
{
    static const Uint32 MANTISSA_ERROR = 0x7f800000;
    return((*(Uint32*)(&value) & MANTISSA_ERROR) == MANTISSA_ERROR);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  fletcher32_()
//
//@ Interface-Description
// This method accepts a pointer to a Uint16 data buffer and the length
// of the buffer as the number of 16-bit words over which to generate 
// a 32-bit Fletcher checksum. It returns the checksum as a Uint32.
//--------------------------------------------------------------------- 
//@ Implementation-Description
// Uses the Fletcher 32-bit checksum implementation as described in 
// the Wikipedia article http://en.wikipedia.org/wiki/Fletcher_checksum
//
// Source articles include:
// http://tools.ietf.org/html/rfc905
//
// Maxino, Theresa (2006). "Revisiting Fletcher and Adler Checksums" (PDF). 
// DSN 2006 Student Forum. http://www.zlib.net/maxino06_fletcher-adler.pdf.
//
// http://tools.ietf.org/html/rfc1146
// RFC 1146 - TCP Alternate Checksum Options describes the Fletcher 
// checksum algorithm. 
//--------------------------------------------------------------------- 
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition 
// none 
//@ End-Method 
//===================================================================== 

Uint32 
Fletcher32( const Uint16 *pData, Uint len ) 
{
    // frequency of partial sum reduction - every n summations do sum reduction
    static const Uint32 REDUCE_SUM_FREQ_ = 360;
    Uint32   sum1 = 0xffff; 
    Uint32   sum2 = 0xffff;

    while ( len )
    {
        Uint tlen = len > REDUCE_SUM_FREQ_ ? REDUCE_SUM_FREQ_ : len;
        len -= tlen;
        do 
        {
            sum1 += *pData++;
            sum2 += sum1;
        } while ( --tlen );
        sum1 = (sum1 & 0xffff) + (sum1 >> 16);
        sum2 = (sum2 & 0xffff) + (sum2 >> 16);
    }
    // Second reduction step to reduce sums to 16 bits
    sum1 = (sum1 & 0xffff) + (sum1 >> 16);
    sum2 = (sum2 & 0xffff) + (sum2 >> 16);

    return sum2 << 16 | sum1;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsFletcherChecksumValid()
//
//@ Interface-Description
//  Verifies the checksum of the specified buffer. Accepts a pointer
//  to a character buffer and a length in bytes. Returns TRUE if the
//  Fletcher32 checksum at the end of the buffer matches the calculated 
//  checksum otherwise returns FALSE.
//--------------------------------------------------------------------- 
//@ Implementation-Description
// Uses the Fletcher32 function to verify the integrity of the data 
// buffer.
//--------------------------------------------------------------------- 
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition 
// none 
//@ End-Method 
//===================================================================== 
Boolean IsFletcherChecksumValid(const unsigned char *buffer, Uint16 length)
{
    Uint32 rxChecksum;
    memcpy(&rxChecksum, buffer + length - sizeof(rxChecksum), sizeof(rxChecksum));
    Uint32 calculatedChecksum = Fletcher32((const Uint16*)(buffer), (length-sizeof(Uint32))/sizeof(Uint16));

    return ( rxChecksum != 0 && calculatedChecksum != 0 && rxChecksum == calculatedChecksum );
}

