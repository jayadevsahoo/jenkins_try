#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LeakGauge - Displays a leak slider for setting high and low
// leak limits, containing a pointer displaying the current value of the
// associated service datum.
//---------------------------------------------------------------------
//@ Interface-Description
// The display of the leak slider consists of the following components: a long
// vertical slider box along which data "slide", a numeric value at the top 
// of the slider indicating the maximum value (PASS) of the slider, a numeric
// value at the bottom indicating the minimum value, two numeric values in between
// the top and the buttom of the slider indicating the failure/alert values
// of the slider, and a service datum pointer which displays the current value
// of the service datum. 
//
// The serviceDataChangeHappened() receives changes from the Service Data
// subsystem to correctly position the service datum pointer.
//
// Once created an object of this class works mainly by itself.  It simply
// requires to be activated (via activate()) before being displayed  to
// hide the value pointer being displayed until it receives new leak data.
//---------------------------------------------------------------------
//@ Rationale
// Encapsulates all the functionality needed to display and use an
// Leak slider on the SstLeakSubScreen and the EstResultSubScreen classes.
//---------------------------------------------------------------------
//@ Implementation-Description
// Most of the code in this class goes into the setup of the leak slider's
// display which is performed in the constructor.  After constructing the
// display the main function of the slider is to react to the value of the
// service datum in order to "slide" the service data pointer along the slider.
// Changes to the service datum through the serviceDataChangeHappened() method.
//
// There is an overloaded Drawable::activate() method provided which should be
// called before the slider is displayed.  The activate() method hide the 
// value pointer until the new leak datum is available.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling strategies apart from the usual
// assertions and pre-conditions which verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// This class must be created with at least one alarm limit setting
// button.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LeakGauge.ccv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//
//  Revision: 008   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 007  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 006  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 005  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 004  By:  hhd    Date:  20-AUG-97    DR Number: 2321
//    Project:  Sigma (R8027)
//    Description:
//		Correct description in last modification log.
//
//  Revision: 003  By:  hhd    Date:  18-AUG-97    DR Number: 2321
//    Project:  Sigma (R8027)
//    Description:
//    Modified the value pointer's height in order to better display floating points with commas. 
//      (language dependent) 
//
//  Revision: 002  By:  yyy    Date:  23-JUL-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//		23-JUL-97 Modified MESSAGE_X_ value from SLIDER_X_ + 10 to
//		SLIDER_X_ so we can implement x= parameter in the cheap text.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//    Project:  Sigma (R8027)
//    Description:
//    Integration baseline.
//=====================================================================

#include <math.h>
#include "LeakGauge.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_WIDTH = 140;
static const Int32 CONTAINER_HEIGHT = 230;
static const Int32 VALUE_FIELD_X_ = 2;
static const Int32 VALUE_FIELD_WIDTH_ = 43;
static const Int32 VALUE_FIELD_HEIGHT_ = 15;
static const Int32 VALUE_TRIANGLE_WIDTH_ = 7;
static const Int32 SLIDER_WIDTH_ = 50;
static const Int32 SLIDER_HEIGHT_ = 180;
static const Int32 SLIDER_X_ 
					= VALUE_FIELD_X_ + VALUE_FIELD_WIDTH_ + VALUE_TRIANGLE_WIDTH_;
static const Int32 SLIDER_Y_ = (CONTAINER_HEIGHT - SLIDER_HEIGHT_) / 2;
static const Int32 RANGE_VALUE_X_ = SLIDER_X_ + SLIDER_WIDTH_ + 4;
static const Int32 MESSAGE_X_ = SLIDER_X_;
static const Int32 NUMERIC_LABEL_HEIGHT_ = 18;
static const Int32 MAX_LABEL_WIDTH_ = SLIDER_X_ + SLIDER_WIDTH_ - 3;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LeakGauge()  [Constructor]
//
//@ Interface-Description
//  Creates an leak slider for displaying service data pointer. The parameters
//  are as follows:
// >Von
//	minValue		The minimum lower bound limit for the leak slider.
//	maxValue		The maximum upper bound limit for the leak slider.
//	passMessageId	The StringId for the "PASS" message.
//	failMessageId	The StringId for the "FAIL" message.
//	upperValue		The upper bound limit for "PASS" to be valid.
//	lowerValue		The lower bound limit for "FAIL" to be valid.
//	alertMessageId	The StringId for the "ALERT" message.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  The constructor creates all the components of the leak slider's
//  display.  It also registers for receiving update notices for changes
//  to the service datum so that it knows when to slide the pointer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LeakGauge::LeakGauge(Real32 minValue, Real32 maxValue, 
				StringId passMessageId, StringId failMessageId,
				Real32 upperValue, Real32 lowerValue, 
				StringId alertMessageId) :
		sliderBox_(SLIDER_X_, SLIDER_Y_, SLIDER_WIDTH_, SLIDER_HEIGHT_),
		passBox_(SLIDER_X_, SLIDER_Y_, SLIDER_WIDTH_, SLIDER_HEIGHT_),
		failBox_(SLIDER_X_, SLIDER_Y_, SLIDER_WIDTH_, SLIDER_HEIGHT_),
		min_(minValue),
		max_(maxValue),
		minField_(TextFont::TEN, 4, LEFT),
		maxField_(TextFont::TEN, 4, LEFT),
		lower_(lowerValue),
		upper_(upperValue),
		lowerField_(TextFont::TEN, 4, LEFT),
		upperField_(TextFont::TEN, 4, LEFT),
		value_(maxValue - minValue),
		valueField_(TextFont::TEN, 4, RIGHT),
		valueBox_(0, 0, VALUE_FIELD_WIDTH_, VALUE_FIELD_HEIGHT_),
		valueTriangle_(VALUE_FIELD_WIDTH_ + VALUE_TRIANGLE_WIDTH_,
					VALUE_FIELD_HEIGHT_ / 2, VALUE_FIELD_WIDTH_, 0,
					VALUE_FIELD_WIDTH_, VALUE_FIELD_HEIGHT_ - 1)
{

	CALL_TRACE("LeakGauge::LeakGauge(Real32 minValue, Real32 maxValue, Real32 lowValue, Real32 highValue,"
				"StringId passMessageId, StringId failMessageId, StringId alertMessageId)");

	Int	upperYOffset;
	Int lowerYOffset;
	Int minYOffset;
	Int maxYOffset;

	// Calculate scaling factor for positioning graphicals in valueToPoint_().
	scaleFactor_ = SLIDER_HEIGHT_ / (max_ - min_);

	// Calculate the upper and lower bound pixel position.
	maxYOffset = 0;
	minYOffset = Int (scaleFactor_ * (max_ - min_));
	upperYOffset = Int (scaleFactor_ * (max_ - upper_));
	lowerYOffset = Int (scaleFactor_ * (max_ - lower_));

	// Dimension the whole slider container
	setWidth(CONTAINER_WIDTH);
	setHeight(CONTAINER_HEIGHT);

	// Default background color is Medium Grey
	setFillColor(Colors::BLACK);	

	// Set up the Slider Box
	sliderBox_.setColor(Colors::BLACK);
	sliderBox_.setFillColor(Colors::LIGHT_BLUE);

	// Set up the Pass Box
	passBox_.setColor(Colors::BLACK);
	passBox_.setFillColor(Colors::WHITE);

	// Set up the fail Box
	failBox_.setColor(Colors::BLACK);
	failBox_.setFillColor(Colors::MEDIUM_BLUE);

	// Set up the Minimum Value field color and dimensions
	minField_.setColor(Colors::WHITE);
	minField_.setX(RANGE_VALUE_X_);
	minField_.setY(SLIDER_Y_ + minYOffset - 10);
	minField_.setWidth(MAX_LABEL_WIDTH_);
	minField_.setHeight(NUMERIC_LABEL_HEIGHT_);

	// Set up the Maximum Value field color and dimensions
	maxField_.setColor(Colors::WHITE);
	maxField_.setX(RANGE_VALUE_X_);
	maxField_.setY(SLIDER_Y_ + maxYOffset - 10);
	maxField_.setWidth(MAX_LABEL_WIDTH_);
	maxField_.setHeight(NUMERIC_LABEL_HEIGHT_);

	// Set up the Lower Value field color and dimensions
	lowerField_.setColor(Colors::WHITE);
	lowerField_.setX(RANGE_VALUE_X_);
	lowerField_.setY(SLIDER_Y_ + lowerYOffset - 10);
	lowerField_.setWidth(MAX_LABEL_WIDTH_);
	lowerField_.setHeight(NUMERIC_LABEL_HEIGHT_);

	// Set up the Upper Value field color and dimensions
	upperField_.setColor(Colors::WHITE);
	upperField_.setX(RANGE_VALUE_X_);
	upperField_.setY(SLIDER_Y_ + upperYOffset - 10);
	upperField_.setWidth(MAX_LABEL_WIDTH_);
	upperField_.setHeight(NUMERIC_LABEL_HEIGHT_);

	// Set up the Value Pointer container and contents
	valueContainer_.setWidth(VALUE_FIELD_WIDTH_ + VALUE_TRIANGLE_WIDTH_);
	valueContainer_.setHeight(VALUE_FIELD_HEIGHT_);
	valueContainer_.setY(valueToPoint_(value_) - (VALUE_FIELD_HEIGHT_ / 2 + 1));
	valueContainer_.setX(VALUE_FIELD_X_);

	valueField_.setWidth(VALUE_FIELD_WIDTH_);
	valueField_.setHeight(VALUE_FIELD_HEIGHT_);
	valueField_.setPrecision(TENTHS);
	valueField_.setValue(value_);

	valueBox_.setColor(Colors::LIGHT_BLUE);
	valueBox_.setFillColor(Colors::LIGHT_BLUE);
	valueTriangle_.setColor(Colors::LIGHT_BLUE);
	valueTriangle_.setFillColor(Colors::LIGHT_BLUE);
	valueField_.setColor(Colors::BLACK);

	// Add value related drawables to container
	valueContainer_.addDrawable(&valueBox_);
	valueContainer_.addDrawable(&valueTriangle_);
	valueContainer_.addDrawable(&valueField_);

	// Set up the pass message field
	passMessageId_.setText(passMessageId);
	passMessageId_.setX(MESSAGE_X_);
	passMessageId_.setY(SLIDER_Y_);
	passBox_.setHeight(upperYOffset);

	if (alertMessageId != NULL_STRING_ID)
	{	// $[TI1]
		// Set up the alert message field
		alertMessageId_.setText(alertMessageId);
		alertMessageId_.setX(MESSAGE_X_);
		alertMessageId_.setY(SLIDER_Y_ + upperYOffset);
	}
	else
	{	// $[TI2]
		lower_ = upper_;
		lowerYOffset = Int (scaleFactor_ * (max_ - lower_));
	}

	// Set up the fail message field
	failMessageId_.setText(failMessageId);
	failMessageId_.setX(MESSAGE_X_);
	failMessageId_.setY(SLIDER_Y_ + lowerYOffset);

	failBox_.setY(SLIDER_Y_ + lowerYOffset);
	failBox_.setHeight(Int (scaleFactor_ * (lower_ - min_)));

	// Add Lower, Upper,Min, Max values and Slider Box to container
	addDrawable(&sliderBox_);
	addDrawable(&passBox_);
	addDrawable(&failBox_);
	addDrawable(&minField_);
	addDrawable(&maxField_);
	addDrawable(&lowerField_);
	addDrawable(&upperField_);
	addDrawable(&passMessageId_);
	addDrawable(&failMessageId_);
	addDrawable(&alertMessageId_);

	// Hide the following drawables
	valueContainer_.setShow(FALSE);
	addDrawable(&valueContainer_);

	// Set values of Min and Max fields
	minField_.setValue(min_);
	maxField_.setValue(max_);
	lowerField_.setValue(lower_);
	upperField_.setValue(upper_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LeakGauge  [Destructor]
//
//@ Interface-Description
// Destroys the leak slider.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LeakGauge::~LeakGauge(void)
{
	CALL_TRACE("LeakGauge::~LeakGauge(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Virtual method inherited from Drawable.  Must be called every time the
//  slider is about to be displayed.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply call setShow() to hide the value container's display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LeakGauge::activate(void)
{
	CALL_TRACE("LeakGauge::activate(void)");

	// Update the graphical service datum pointer if hidePointer_ is not turned
	// on.
	valueContainer_.setShow(FALSE);
			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setValue
//
//@ Interface-Description
//  Inform the leak gauge to update the new pointer "value".
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply save the "value_" and call updateValueField_() to update the
//  display.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LeakGauge::setValue(Real32 value)
{
	CALL_TRACE("LeakGauge::setValue(Real32 value)");

	value_ = value;
	updateValueField_();
			// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValueField_ [Private]
//
//@ Interface-Description
//  Called when the service datum value graphical needs updating.  We set
//  the value and position the pointer according to this value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply position and display the new value.  If the value is out of
//  the maximum or the minimum limits, then we set the clipped flag to
//  TRUE.  Whenever the data pointer is clipped, we should display it
//  using blink color.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LeakGauge::updateValueField_(void)
{
	Real32 absoluteValue;
	Boolean clipped;

	CALL_TRACE("LeakGauge::updateValueField_(void)");

#if defined FORNOW
printf("LeakGauge::updateValueField_() value = %f\n", value_);
#endif	// FORNOW

	// Display the new value
	valueContainer_.setShow(TRUE);				// Make sure it's displayed
	valueField_.setValue(value_);				// Set the value
	clipped = FALSE;

	// If the value is out of the min/max range of the slider warp it to
	// the min or max before positioning it.
	if (value_ < min_)
	{													// $[TI1]
		value_ = min_;
		clipped = TRUE;
	}
	else if (value_ > max_)
	{													// $[TI2]
		value_ = max_;
		clipped = TRUE;
	}											

	absoluteValue = value_ - min_;

#if defined FORNOW
printf("LeakGauge::updateValueField_() value = %f, absoluteValue = %f\n",
			value_, absoluteValue);
#endif	// FORNOW

	// Set the value flashing if it was out of range (clipped).
	valueField_.setColor(clipped ? Colors::WHITE_ON_BLACK_BLINK_FAST :
								   Colors::BLACK);

	// Position the value pointer in relation to its value!
	valueContainer_.setY(valueToPoint_(absoluteValue) - (VALUE_FIELD_HEIGHT_ / 2 + 1));
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueToPoint_
//
//@ Interface-Description
// Translates a floating point value into the equivalent Y co-ordinate
// on the slider which is returned as a Uint16.
//---------------------------------------------------------------------
//@ Implementation-Description
// Using the previously calculated scaleFactor_ value we use a simple
// calculation to return the correct Y coordinate.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int16
LeakGauge::valueToPoint_(Real32 sliderValue)
{
	CALL_TRACE("LeakGauge::valueToPoint_(Real32 sliderValue)");

	return ((Int16)(SLIDER_Y_ + SLIDER_HEIGHT_ - scaleFactor_ * sliderValue + 0.5));
														// $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LeakGauge::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LEAKGAUGE,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
