#ifndef	ModeSettingsCapture_HH
# define ModeSettingsCapture_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  ModeSettingsCapture - Control settings display area screen capture
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/ModeSettingsCapture.hhv   25.0.4.0   19 Nov 2013 14:12:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: quf         Date: 24-Sep-2001  DR Number: 5942
//  Project: GuiComms
//  Description:
//  Code/comment/header cleanup.
//
//  Revision: 001    By: gdc         Date: 23-Jan-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//  Initial version
//
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "ScreenCapture.hh"

//@ Usage-Classes
//@ End-Usage

class DisplayContext;

class ModeSettingsCapture : public ScreenCapture
{
  public:

	static ModeSettingsCapture & GetModeSettingsCapture(void);

	static void  SoftFault(const SoftFaultID softFaultID,
					       const Uint32	lineNumber,
					       const char *	pFileName = NULL,
					       const char *	pPredicate = NULL);

  protected:
	virtual Byte filterPixel_(Byte pixel) const;

  private:
	ModeSettingsCapture();
	~ModeSettingsCapture();

	ModeSettingsCapture( Uint32* pRasterData, 
				   Uint32  sizeofRasterData,
				   DisplayContext& rDisplayContext,
				   Int32 x0, Int32 y0,
	               Uint32 xWidth, Uint32 yHeight );

};

#endif	// ! ModeSettingsCapture_HH
