#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: VitalCapacityManeuver - Vital Capacity Maneuver handler
//---------------------------------------------------------------------
//@ Interface-Description
// Handles the Vital Capacity Maneuver events originated by the UI. 
// This class is a derived Maneuver. The Maneuver base class provides 
// data and methods common to all maneuvers. This class expands the 
// base class functionality for processing the Vital Capacity Maneuver.
// The Vital Capacity maneuver is a feature of the Respiratory Mechanics 
// Option.
//---------------------------------------------------------------------
//@ Rationale
// This class implements the Vital Capacity maneuver functionality on 
// the BD.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VitalCapacityManeuver.ccv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial version.
//
//=====================================================================

#include "VitalCapacityManeuver.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes
#include "IntervalTimer.hh"
#include "Trigger.hh"
#include "TriggersRefs.hh"
#include "TimerBreathTrigger.hh"
#include "MsgQueue.hh"
#include "BdQueuesMsg.hh"
#include "BreathRecord.hh"
#include "ApneaInterval.hh"
#include "ImmediateBreathTrigger.hh"
#include "AsapInspTrigger.hh"

//@ End-Usage

//@ Code...
//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================

//@ Constant MAX_TIME_PENDING
// $[RM12085] The maximum time waiting for patient effort into Vital Capacity inspiration
static const Int32 MAX_TIME_PENDING = 15000;

//@ Constant MAX_TIME_ACTIVE
// $[RM12185] The maximum Vital Capacity inspiratory+expiratory time
static const Int32 MAX_TIME_ACTIVE = 15000;

//@ Constant MAX_MANEUVER_TIME
// $[RM12083] The amount of time to extend the apnea interval
static const Int32 MAX_MANEUVER_TIME = MAX_TIME_PENDING + MAX_TIME_ACTIVE;

extern const Int32 PAUSE_INTERVAL_EXTENSION ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VitalCapacityManeuver
//
//@ Interface-Description
//  The method takes a ManeuverId and a reference to a IntervalTimer as
//  arguments. It calls the base class constructor and initializes its
//  data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VitalCapacityManeuver::VitalCapacityManeuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer) :
	Maneuver(maneuverId),
    TimerTarget(),
    rPauseTimer_(rPauseTimer),
    pauseTime_(0),
	isVcmExhComplete_(FALSE)
{
	CALL_TRACE("VitalCapacityManeuver::VitalCapacityManeuver(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VitalCapacityManeuver()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

VitalCapacityManeuver::~VitalCapacityManeuver(void)
{
  CALL_TRACE("~VitalCapacityManeuver()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
// This method takes a reference to an IntervalTimer and has no return
// value. The VC maneuver is a client of the timer server that 
// notifies its client (by invoking this method) of the end of the VC
// maneuver interval.
//---------------------------------------------------------------------
//@ Implementation-Description
// The IntervalTimer reference, rTimer, is that of the interval timer
// server for the VC maneuver.  
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// &rTimer == &rPauseTimer_
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
VitalCapacityManeuver::timeUpHappened(const IntervalTimer& rTimer)
{
    CALL_TRACE("timeUpHappened(const IntervalTimer& rTimer)");

    // make sure the caller is the proper server
    CLASS_PRE_CONDITION(&rTimer == &rPauseTimer_);

    rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);
	isTimedOut_ = TRUE;

	// maneuver timeout processed in next newCycle()
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: handleManeuver
//
//@ Interface-Description
// The method takes a Boolean (isStartRequest) as an argument and 
// returns an EventData::EventStatus. It is called to start the maneuver
// (isStartRequest is TRUE) or stop the maneuver (isStartRequest is
// FALSE). The return status indicates the state of the maneuver after
// processing the request. The client then communicates this state to
// the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
VitalCapacityManeuver::handleManeuver(const Boolean isStartRequest)
{
    CALL_TRACE("handleManeuver(const Boolean eventStatus)");

	EventData::EventStatus rtnStatus = EventData::IDLE;

	if (isStartRequest)
	{
		// user pressed the START button
		if (maneuverState_ == PauseTypes::PENDING || maneuverState_ == PauseTypes::ACTIVE)
		{
			// The maneuver was requested before.
			// Simply ignore this request - do nothing
			// an EventData::NO_ACTION_ACK is sent to GUI.
			rtnStatus = EventData::NO_ACTION_ACK;
		}
		else if (maneuverState_ == PauseTypes::IDLE)
		{
            // $[RM12085] An armed Vital Capacity maneuver shall be aborted if 15 seconds elapse without detecting patient inspiration effort to start the maneuver.]

			// maneuverState_ is IDLE so we allow the maneuver
		    pauseTime_ = MAX_TIME_PENDING;

		   	rPauseTimer_.setTargetTime(pauseTime_);

		    // start the timer to check the duration of the pending phase
		   	rPauseTimer_.restart();
			isTimedOut_ = FALSE;

			setActiveManeuver();

			// $[RM12083] During a VC maneuver, the apnea interval shall be extended by the maximum amount of time the maneuver can be armed and active (30 seconds).

			// since we hold off control inspiration while maneuver is pending, extend apnea here
			RApneaInterval.extendInterval(MAX_MANEUVER_TIME + PAUSE_INTERVAL_EXTENSION);

			// arm the maneuver by signaling the scheduler
			RArmManeuverTrigger.enable();

			maneuverState_ = PauseTypes::PENDING;
			clearRequested_ = FALSE;

			// report pending status to GUI
			rtnStatus = EventData::PENDING;
		}
		else
		{
			// Do nothing
			// Note: we do not perform any special operation for IDLE_PENDING
			// due to that this status will be reset by the current running
			// scheduler to IDLE in the same 5 ms cycle.
            AUX_CLASS_ASSERTION( maneuverState_ == PauseTypes::IDLE_PENDING,
                             	 maneuverState_ );
		}
	}
	else
	{
		// the STOP function is not defined for this maneuver
		CLASS_ASSERTION_FAILURE();
	}
	
	return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.
//	This method is invoked by the VcmPsvPhase breath phase when 
//  taking control in the current scheduler. Calling this method changes
//  the maneuver state to ACTIVE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
VitalCapacityManeuver::activate(void)
{
    CALL_TRACE("activate(void)");

	// $[RM12207] The maneuver shall transition to active at the beginning of the next patient inspiratory effort signaled by the backup pressure trigger. 
	// $[RM12086] /b/ An active Vital Capacity maneuver shall be considered to be successfully completed if 15 seconds elapse

	if (maneuverState_ == PauseTypes::PENDING)
	{
		// start the "active" and "idle pending" phase timer
		pauseTime_ = MAX_TIME_ACTIVE;
		rPauseTimer_.setTargetTime(pauseTime_);
		rPauseTimer_.restart();
		isTimedOut_ = FALSE;

		// Setup active maneuver state
		maneuverState_ = PauseTypes::ACTIVE;
		isVcmExhComplete_ = FALSE;

		// notify the user that maneuver is active:
		RVitalCapacityManeuverEvent.setEventStatus(EventData::ACTIVE);
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: complete
//
//@ Interface-Description
// This method takes no arguments and returns a Boolean. It is 
// invoked when the VC maneuver completes. It returns TRUE if there
// are no more pending VC maneuvers.
//---------------------------------------------------------------------
//@ Implementation-Description
// Invokes clear() to complete the VC maneuver and clean up.
// Always returns TRUE to indicate there are no more pending VC
// maneuvers since VC maneuvers are not queued or stacked.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
VitalCapacityManeuver::complete(void)    
{
    CALL_TRACE("complete(void)");

	isVcmExhComplete_ = TRUE;

	clear();

	return (TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: terminateManeuver
//
//@ Interface-Description
//  This method takes no arguments and returns nothing. Deactivates 
//  the maneuver on the next BD cycle.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the clearRequested_ flag TRUE to cancel the maneuver on the 
//  next BD cycle in newCycle().
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
VitalCapacityManeuver::terminateManeuver(void)
{
	clearRequested_ = TRUE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processUserEvent
//
//@ Interface-Description
// This method takes a reference to a UiEvent and returns a Boolean
// to indicate whether the UiEvent can be processed while this VC
// maneuver is active. Returns TRUE to continue processing of the 
// new UiEvent, otherwise returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
VitalCapacityManeuver::processUserEvent(UiEvent &rUserEvent)
{
	Boolean continueNewEvent = TRUE;

	if (rUserEvent.getId() == EventData::VITAL_CAPACITY_MANEUVER)
	{
		continueNewEvent = TRUE;
	}
	else if (rUserEvent.getId() == EventData::MANUAL_INSPIRATION)
	{
		if (maneuverState_ == PauseTypes::ACTIVE)
		{
			// $[RM12232] A manual inspiration request shall be rejected during the inspiratory phase of a VC Maneuver. 
			// manual insp not allowed during inspiratory phase
			rUserEvent.setEventStatus(EventData::REJECTED);
			continueNewEvent = FALSE;
		}
		else
		{
			// manual insp allowed while pending or idle_pending
			// $[RM12233] A manual inspiration request shall be accepted during the expiratory phase of a VC Maneuver after the Restricted phase of exhalation. 

			continueNewEvent = TRUE;

			if (maneuverState_ == PauseTypes::PENDING)
			{
				// $[RM12231] A pending VC Maneuver shall be canceled if the operator issues a Manual Inspiration.
				// a manual insp cancels a pending VC maneuver because the 
				// because the manual insp breath restarts the breath timers
				// and reenables the patient triggers that must be disabled 
				// while waiting for a patient inspiratory effort
				clearRequested_ = TRUE;
			}
		}
	}
	else
	{
		// reject any other maneuver request
		// $[RM12033] All RM maneuver requests shall be rejected if any other pause or RM maneuver has already taken place during the same breath.
		rUserEvent.setEventStatus(EventData::REJECTED);
		continueNewEvent = FALSE;
	}

	return(continueNewEvent);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
// This method takes no arguments and returns nothing. It is 
// invoked every BD cycle. It implements a state machine that defines
// the VC maneuver. It processes external events such as button
// press/release events and timeouts using internal variables that are    
// examined every BD cycle. Processing of these events are based on 
// the current state of the maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
// The actions of this method are dependent on the state of the 
// maneuver when this method is called. It uses triggers to signal 
// the scheduler. It processes external events such as button press/
// release events and timeouts using Boolean flags that are examined 
// every BD cycle.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
VitalCapacityManeuver::newCycle(void)    
{
	CALL_TRACE("newCycle(void)");

	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
	// determine the current phase type (e.g. EXHALATION, INSPIRATION)
	const  BreathPhaseType::PhaseType currentPhase = pBreathPhase->getPhaseType();

	if (maneuverState_ == PauseTypes::PENDING)
	{
		if (clearRequested_ || isTimedOut_)
		{
            // $[RM12085] armed VCM shall be aborted if 15 seconds elapse without detecting patient effort...

			// signal the scheduler to disarm the pending maneuver and 
			// resume normal breath delivery
			RArmManeuverTrigger.disable();
			RDisarmManeuverTrigger.enable();

			clear();
		}
	}
	if (maneuverState_ == PauseTypes::ACTIVE)
	{
        // $[RM12207] ...transition to active at the beginning of the next patient effort...
		// VsmPsvPhase inspiratory phase is active during ACTIVE state

		if (clearRequested_ || isTimedOut_)
		{		
            // $[RM12086] /b/ active VCM shall be considered to be successfully completed if 15 seconds elapse
			// trigger to stop the VcmPsvPhase
			RPauseCompletionTrigger.enable();
			// VsmPsvPhase will cancel the maneuver upon receiving the trigger
			// the scheduler restarts the breath timer when triggered out of VC insp
		}
		// if VC breath has completed then wait for exhalation data 
		// (exhaled tidal volume) in the IDLE_PENDING state
		else if (PreviousBreathPhaseType_ == BreathPhaseType::VITAL_CAPACITY_INSP &&
				 currentPhase == BreathPhaseType::EXHALATION)
		{
			// $[RM12208] The VC maneuver shall allow for a full exhalation effort after the inspiration.
			// transition to IDLE_PENDING -- MAX_TIME_ACTIVE timer continues
			maneuverState_ = PauseTypes::IDLE_PENDING;
		}
	}
	else if (maneuverState_ == PauseTypes::IDLE_PENDING)
	{
		// $[RM12208] The VC maneuver shall allow for a full exhalation effort after the inspiration.
		// The exhalation phase is active during IDLE_PENDING state

		// wait in IDLE_PENDING for data collection from
		// exhalation phase (exhaled tidal volume) - scheduler will call complete() to 
		// complete the maneuver and send data to GUI when patient insp occurs

		if (isTimedOut_)
		{
            // $[RM12086] /b/ active VCM shall be successfully completed if 15 seconds elapse

			// we're already in exhalation so trigger an immediate breath to exit
			// and restart control breath delivery

			// exit the exhalation phase and restart the breath timer
			RAsapInspTrigger.enable();

			// the next newCycle() will see the transition out of exhalation
			// report the VC to the GUI and complete the maneuver
		}
		else if (clearRequested_)
		{
			// $[RM12062] A Vital Capacity maneuver shall be canceled if...

			// Complete maneuver before newCycle sees transition out of exhalation
			// so no results are sent
			clear();
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
// This method takes no arguments and returns nothing.  It is 
// invoked every time the VC maneuver completes or is terminated.
// Its actions and processing are based on the state of the maneuver
// when called. It signals the scheduler, informs the GUI of the 
// maneuver's completion status and signals to send VC data to the 
// GUI. It resets its internal data in preparation for the next VC
// request.
//---------------------------------------------------------------------
//@ Implementation-Description
// The actions of this method are dependent on the state of the 
// maneuver when this method was called. 
// It uses triggers to signal maneuver completion to the scheduler.
// It calls the VitalCapacityManeuverEvent.setEventStatus() method 
// to send the maneuver completion status to the GUI.
// It uses MsgQueue::PutMsg to signal the BreathData task to send
// the Vital Capacity data to the GUI.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
VitalCapacityManeuver::clear(void)    
{
    CALL_TRACE("clear(void)");

	if (maneuverState_ == PauseTypes::PENDING)
	{
		// $[RM12062] A Vital Capacity maneuver shall be canceled if...
		// disable the arm maneuver trigger in case we got here due to a
		// mode change (eg. disconnect) and the trigger hasn't been serviced yet
		RArmManeuverTrigger.disable();
		RDisarmManeuverTrigger.enable();

		// send status to GUI
		RVitalCapacityManeuverEvent.setEventStatus(EventData::CANCEL);
	}
	else if (maneuverState_ == PauseTypes::ACTIVE)
	{
		// $[RM12062] A Vital Capacity maneuver shall be canceled if...
		// VcmPsvPhase::relinquishControl() canceled the maneuver due to an unexpected trigger
		RVitalCapacityManeuverEvent.setEventStatus(EventData::CANCEL);
	}
	else if (maneuverState_ == PauseTypes::IDLE_PENDING)
	{
		// isVcmExhComplete_ indicates the exhalation phase is complete
		// and we can report the exhaled tidal volume (VC) to the GUI
		if (isVcmExhComplete_)
		{
			RVitalCapacityManeuverEvent.setEventStatus(EventData::COMPLETE);
			// $[RM12065] When an active VC maneuver completes successfully, the total expiratory volume (or Vte) for the breath result shall be displayed on the Waveforms screen as the VC.
			// $[RM12211] When an active SVC maneuver completes successfully, the total expiratory volume (or Vte) for the breath result shall be displayed on the VC maneuver subscreen.
			MsgQueue::PutMsg(::BREATH_DATA_Q, BdQueuesMsg::SEND_VITAL_CAPACITY_DATA);
		}
		else
		{
            // $[RM12062] A Vital Capacity maneuver shall be canceled if...
			RVitalCapacityManeuverEvent.setEventStatus(EventData::CANCEL);
			// if VCM exhalation has not completed then enable disarm maneuver 
			// trigger to restart the breath timers as needed
			RDisarmManeuverTrigger.enable();
		}
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(maneuverState_);
	}

	maneuverState_ = PauseTypes::IDLE;
	clearRequested_ = FALSE;
	isVcmExhComplete_ = FALSE;
	
	// Stop the timer, but do not reset the currentTime_,
    rPauseTimer_.stop();
    rPauseTimer_.setCurrentTime(0);
	isTimedOut_ = FALSE;

   	resetActiveManeuver();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
VitalCapacityManeuver::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, VITALCAPACITYMANEUVER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


