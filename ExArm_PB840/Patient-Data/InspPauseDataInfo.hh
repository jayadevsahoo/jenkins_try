
#ifndef InspPauseDataInfo_HH
#define InspPauseDataInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: InspPauseDataInfo - Manages real time display of pressure
//                during inspiratory pause maneuver.
//---------------------------------------------------------------------
//
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/InspPauseDataInfo.hhv   10.7   08/17/07 10:27:40   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  iv     Date:  30-SEP-98    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial BiLevel version.  
//
//  Revision: 001  By:  jhv    Date:  06-Feb-95    DR Number: 
//    Project:  Sigma (R8027)
//    Description:
//        Initial version. 
//
//====================================================================

#include     "OsTimeStamp.hh"
#include     "WaveformData.hh"
#include     "PatientDataClassIds.hh"

class InspPauseDataInfo
{
  //@ Friend : PatientDataMgr.
  //  Allows PatientDataMgr class to access private methods.
  friend  class PatientDataMgr;

  public:
    InspPauseDataInfo();
    virtual ~InspPauseDataInfo(void);                 
    inline   Boolean isPauseKeyPressed();

    static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL);

  private:
    InspPauseDataInfo(const InspPauseDataInfo&);   // not implemented.
    void operator=(const InspPauseDataInfo&); // not implemented.

    void updateData_(WaveformData *pWaveData);
    void startPause_( const Real32 endInspPressure);
    inline   void stopPause_();

   //@ Data-Member: paused_
   // This flag indicates whether inspiratory pause maneuver has started.
    Boolean    paused_;

  //@ Data-Member: initialPiEnd_
  // The intial circuit pressure value at the start of the inspiratory pause
  // maneuver.
    Real32     initialPiEnd_;

  //@ Data-Member updateTime_ 
  // Time stamp of the last circuit pressure sample.
    OsTimeStamp  updateTime_;

 //@ Data-Member DELAY_TIME_
 //  Defines a time interval between 2 adjacent pressure samples
 	static const Uint DELAY_TIME_;
};


// Inlined methods...
#include "InspPauseDataInfo.in"

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
#endif // InspPauseDataInfo_HH 
