#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: SoftwareRevNum - Software Revision Number.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a template file used by 'genSwRevNumFile.x' to create a proper
//  revision file at build-time.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Build/vcssrc/SoftwareRevNum.C_v   10.0.5.0   06/22/06 11:13:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  07-Apr-2000    DR Number: 5700
//  Project:  NeoMode
//  Description:
//      Added 'SOFTWARE_REL_TYPE' field.
//
//  Revision: 001  By: sah    Date:  17-Dec-1997    DR Number: None
//  Project:  Sigma (R8027)
//  Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SoftwareRevNum.hh"
#include <string.h>

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//  Global Initialization...
//
//=====================================================================

static char SOFTWARE_FULL_REV_NUM[SoftwareRevNum::REV_NUM_SIZE] = "NOT Initialized";
static char SOFTWARE_SHORT_REV_NUM[SoftwareRevNum::REV_NUM_SIZE] = "NOT Initialized";

//Use to tag developer builds
static const char* DEVELOPER_PREFIX = "";

const SoftwareRelType::Id  SoftwareRelType::SOFTWARE_REL_TYPE = SoftwareRelType::DEVELOPMENT;

const Uint16 SW_MAJOR_REV_NUMBER = 1;
const Uint16 SW_MINOR_REV_NUMBER = 0;
const Uint16 SW_TEST_REV_NUMBER = 0;
const Uint16 SW_DEV_REV_NUMBER = 11;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftwareRevNum() (constructor)
//
//@ Interface-Description
// Creates the object. This is a protected constructor, so it invoked
// from within the GetInstance() API. In other words, it is invoked
// just once.
//---------------------------------------------------------------------
//@ Implementation-Description
// The version strings are "formed" just once. It is possible to do this
// from the GetAsFullString() or GetAsShortString() APIs every time. But
// given that those APIs are bound to be called a few times, we are doing
// it here once to save time.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
SoftwareRevNum::SoftwareRevNum()
{
	memset(SOFTWARE_FULL_REV_NUM, 0, SoftwareRevNum::REV_NUM_SIZE);
	memset(SOFTWARE_SHORT_REV_NUM, 0, SoftwareRevNum::REV_NUM_SIZE);
	_snprintf(SOFTWARE_FULL_REV_NUM, SoftwareRevNum::REV_NUM_SIZE-1, "%s%d.%d.%d.%d", 
		DEVELOPER_PREFIX, SW_MAJOR_REV_NUMBER, SW_MINOR_REV_NUMBER, SW_TEST_REV_NUMBER, SW_DEV_REV_NUMBER);
	_snprintf(SOFTWARE_SHORT_REV_NUM, SoftwareRevNum::REV_NUM_SIZE-1, "%s%d.%d", 
			DEVELOPER_PREFIX,SW_MAJOR_REV_NUMBER, SW_MINOR_REV_NUMBER);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetAsFullString()
//
//@ Interface-Description
// Returns the SW version number as a string "MAJOR.MINOR.TEST.DEV". The
// buffer returned is owned by this class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the pre-formed software revision string. The value returned
// would be the same through the entire run-time.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
const char* SoftwareRevNum::GetAsFullString() const
{
	return SOFTWARE_FULL_REV_NUM;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetAsShortString()
//
//@ Interface-Description
// Returns the SW version number in a short form - i.e. "MAJOR.MINOR". In
// other words, the TEST and DEV components of the revision are omitted. The
// buffer returned is owned by this class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the pre-formed software revision string. The value returned
// would be the same through the entire run-time.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
const char* SoftwareRevNum::GetAsShortString() const
{
	return SOFTWARE_SHORT_REV_NUM;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetMajorNumber()
//
//@ Interface-Description
// Returns the MAJOR component of SW version number.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
const Uint16 SoftwareRevNum::GetMajorNumber() const
{
	return SW_MAJOR_REV_NUMBER;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetMinorNumber()
//
//@ Interface-Description
// Returns the MINOR component of SW version number.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
const Uint16 SoftwareRevNum::GetMinorNumber() const
{
	return SW_MINOR_REV_NUMBER;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetInstance()
//
//@ Interface-Description
// Returns the only instance of SoftwareRevNum class.
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
const SoftwareRevNum& SoftwareRevNum::GetInstance()
{
	static SoftwareRevNum GlobalSoftwareRevNumInstance;
	return GlobalSoftwareRevNumInstance;
}

