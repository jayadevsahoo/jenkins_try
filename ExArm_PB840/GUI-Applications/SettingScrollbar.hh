
#ifndef SettingScrollbar_HH
#define SettingScrollbar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SettingScrollbar -  A vertical scrollbar for scrollable menus.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SettingScrollbar.hhv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  15-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Initial version.
//
//====================================================================

#include "GuiAppClassIds.hh"
#include "Scrollbar.hh"

//@ Usage-Classes
//@ End-Usage

class SettingScrollbar : public Scrollbar
{
public:
	SettingScrollbar(Uint16 height, Uint16 numDisplayRows);
	~SettingScrollbar(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:
	// these methods are purposely declared, but not implemented...
	SettingScrollbar(void);						 // not implemented...
	SettingScrollbar(const SettingScrollbar&);	 // not implemented...
	void   operator=(const SettingScrollbar&);	 // not implemented...

};


#endif // Scrollbar_HH
