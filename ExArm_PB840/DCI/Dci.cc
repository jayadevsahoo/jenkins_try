#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

// =========================== C L A S S   D E S C R I P T I O N ====
//@ Class: Dci - Sigma DCI communications class
//---------------------------------------------------------------------
//@ Interface-Description
//  This class encapsulates the functionality needed to support the Sigma
//  ventilator's portion of the PB 7200-series DCI protocol. The Dci class
//  contains static methods to generate the DCI and error reports for
//  output to the serial port.
//
//  The remainder of the class is private static functions.  This class is
//  not instantiated.
//---------------------------------------------------------------------
//@ Rationale
//  Maintain upward compatibility with the PB 7200-series ventilator in
//  communicating with external devices, specifically the Clinivision 
//  Handheld, HP Merlin, GE Marquette, and SpaceLabs monitors.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Dci class encapuslates the functionality to support the DCI protocol
//  on the Sigma ventilator.  Methods and data structures are grouped in
//  the following categories
//>Von
//	1. Report Production (in DciReport.cc)
//		* define report structure and data items
//		* construct proper report-specific header(s) and trailer(s)
//		* format report data items
//		* generate DCI error report
//	2. DCI-specific Data Types (in DciNumber.*, DciString.* and
//		  DciValueStatus.*)
//		* define circular buffer object
//		* define numeric/tag object
//		* define string/tag object
//	3. Language-specific DCI Output (in DciLanguage.cc)
//	4. Remaining DCI Functionality (in Dci.cc)
//		* DCI command handler
//		* accumulate report data from other Sigma subsystems/classes
//>Voff
//	
//  See "SIGMA Software Requirements Specification" (70000-75), section 4.3.1
//  for more information.
//---------------------------------------------------------------------
//@ Module-Decomposition
//  GetDci()
//  Dci()
//  ~Dci()
//	Initialize()
//
//	generateSNDA()
//	generateSNID()
//	generateSER()
//	generateSLMT()
//	output_()
//	getSndAData_()
//	getMiscData_()
//	getTime_()
//	getDate_()
//	getPatientId_()
//	getSoftwareRevNum_()
//	getApplicableBoundedValue_()
//	isDiscreteValueApplicable_()
//	getBatchSettings_()
//	encodeOnOff_()
//	computeIofIeRatio_()
//	computeEofIeRatio_()
//	correctedEofIeRatio_()
//	getPatientData_()
//	getAlarmStatus_()
//	encodeNormalAlarmReset_()
//  patientDataChangeHappened()
//
//  For the new SNDF protocal:
//
//  generateSNDF()
//  getAlarmStatusF_()
//  getApplicableBoundedValueF_()
//  getBatchSettingsF_()
//  getMiscDataF_()
//  getPatientDataF_()
//  getSndFData_()
//  encodeNormalAlarmResetF_()
//
//---------------------------------------------------------------------
//@ Fault-Handling
//  DCI error reports are produced on invalid input and input buffer overflow
//  as specified by the DCI protocol.  These error reports are written back
//  to the host and logged in the Sigma Communications diagnostic log.
//---------------------------------------------------------------------
//@ Restrictions
// Class initialization must be done once during system initialization time.
// The DCI task executes only on the GUI CPU platform and only during
// normal operation (i.e. not during Service Mode).
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/DCI/vcssrc/Dci.ccv   25.0.4.0   19 Nov 2013 14:01:56   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 030   By: mnr    Date: 24-May-2010    SCR Number: 6436
//  Project: PROX
//  Description: 
//      Put blank strings for PROX status and alarm if PROX is not installed.
// 
//  Revision: 029   By: mnr    Date: 15-Apr-2010    SCR Number: 6436  
//  Project: PROX
//  Description: 
//      encodeProxNormalFault_() added and using POnStr6_/POffStr6_
//		instead of PLeakEnabledStr6_/PLeakDisabledStr6_.
// 
//  Revision: 028   By: mnr    Date: 03-Mar-2010    SCR Number: 6436  
//  Project: PROX
//  Description:
//      Modified to support PROX state (ON/OFF) and PROX alarm (FAULT/NORMAL).
//  
//  Revision: 027   By: mnr    Date: 24-Feb-2010    SCR Number: 6436  
//  Project: PROX
//  Description:
//      Modified to support PROX state (ON/OFF/FAULT).
// 
//  Revision: 026    By: mnr   Date: 23-Feb-2010    SCR Number: 6556
//  Project: NEO
//  Description:
//  Special SNDF header for Philips IntelliBridge.
//
//  Revision: 025   By: rhj    Date: 14-Oct-2009    SCR Number: 6544  
//  Project:  XB2
//  Description:
//      Fixed high respiratory rate value bounded to 100,000 and 
//      modified the lenght of leakstate.
// 
//  Revision: 024   By: rpr    Date: 22-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support code review comments.
// 
//  Revision: 023   By: gdc    Date: 12-Dec-2008    SCR Number: 6360
//  Project:  840S
//  Description:
//      Blank RM data during BILEVEL or NIV. Blank VC and NIF data
// 		during neonatal ventilation.
// 
//  Revision: 022   By: gdc    Date: 02-Dec-2008    SCR Number: 6442
//  Project:  840S
//  Description:
//      Corrected byte count field in MISCF report.
// 
//  Revision: 021   By: rpr    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 020   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 019   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 018   By: rhj   Date:  20-Feb-2007    SCR Number: 6345
//  Project:  RESPM
//  Description:
//        Removed C20/C and changed it as a reserve.
//
// Revision: 017    By: rhj              Date: 01-Dec-2006  DR Number: 6324
//  Project: RESPM
//  Description:
//      Fixed Vital Capacity to report its value in Liters not mL.
//
// Revision: 016    By: rhj              Date: 29-Nov-2006  DR Number: 6301
//  Project: RESPM
//  Description:
//      Fixed Cstat and Rstat in the SNDA report and removed WOBtot,
//      WOBr, WOBe, and WOBpt.
//
// Revision: 015    By: rhj              Date: 10-Jul-2006  DR Number: 5928
//  Project: RESPM
//  Description:
//      Added support for the new command SNDF, which generates 
//      a MISCF Report.
//
// Revision: 014    By: erm           Date: 07-Aug-2003  DR Number: 6058
//  Project: AVER
//  Description:
//      Added guard to insure only serial port 1 is used for spaceLabs
//
// Revision: 013    By: erm           Date: 02-Jun-2003  DR Number: 6058
//  Project: AVER
//  Description:
//      Added support for SpaceLab monitors
//
//  Revision: 012    By: quf           Date: 26-JUN-2001  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//  Revision: 011   By: hct   Date:  19-OCT-2000   DCS Number: 5757
//  Project:  Baseline
//  Description:
//      Added functionality to support Marquette monitors.
//
//  Revision: 010   By: sah   Date:  05-Jun-2000   DCS Number: 5737
//  Project:  NeoMode
//  Description:
//      Added special handling of "OFF" values from low exh minute volume.
//
//  Revision: 009    By: hct           Date: 15-DEC-1998  DR Number: 5292, 5314
//  Project: ATC
//  Description:
//      Add functionality to zero settings which are inapplicable for the
//      current mandatory type.  Added TI numbers.
//
//  Revision: 008    By: hct              Date: 20-OCT-1998  DR Number: N/A
//  Project: BiLevel
//  Description:
//      Added TI numbers.
//
//  Revision: 007    By: hct              Date: 03-MAR-1998  DR Number: N/A
//  Project: BiLevel
//  Description:
//      Added Bilevel functionality.
//
//  Revision: 006    By: Gary Cederquist  Date: 26-FEB-1998  DR Number: 5030
//  Project: Sigma (R8027)
//  Description:
//      Corrected duplicate TI numbers.
//
//  Revision: 005    By: Gary Cederquist  Date: 04-FEB-1998  DR Number: 2735
//  Project: Sigma (R8027)
//  Description:
//      Mapped occlusion alarm to disconnect alarm.
//
//  Revision: 004    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2674
//  Project: Sigma (R8027)
//  Description:
//      (DCS 2674) Corrected MISCA field 41.
//
//  Revision: 003    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//                                                           DR Number: 2640
//  Project: Sigma (R8027)
//  Description:
//      (DCS 2605) Complete restructuring of GUI-Serial-Interface to fix
//      several problems. 
//      (DCS 2640) Changed SNDA fields to be compatible with VentNet 
//      implementation including change to patient ID field to contain
//      the ventilator serial number.
//
//  Revision: 002   By: sah   Date:  28-Aug-1997   DCS Number: 2429
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'BDGuiEvent' to 'BdGuiEvent'.
//
//  Revision: 1.0	By: jdm	Date: 7/26/95	DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "Sigma.hh"
#include "Dci.hh"

//@ Usage-Classes
#include "AcceptedContextHandle.hh"
#include "AlarmAnnunciator.hh"		// for alarm status...
#include "AlarmDci.hh"
#include "AlarmName.hh"
#include "BdGuiEvent.hh"		// for O2 suction status
#include "BreathDatumHandle.hh"
#include "ComPortConfigValue.hh"
#include "ConstantParmValue.hh"
#include "ContextId.hh"
#include "ContextObserver.hh"
// TODO E600 remove 
//#include "DciMisc.hh"			// for BASE_FLOW_MIN constant definition
#include "FlowPatternValue.hh"
#include "GuiApp.hh"
#include "LeakCompEnabledValue.hh"
#include "MathUtilities.hh"
#include "ModeValue.hh"
#include "PatientDataId.hh"		// for patient data...
#include "PauseTypes.hh"
#include "ProxEnabledValue.hh"
#include "SerialInterface.hh"
#include "SerialNumber.hh"
#include "SettingConstants.hh"
#include "SettingContextHandle.hh"
#include "SoftwareOptions.hh"
#include "SoftwareRevNum.hh"
#include "TaskControlAgent.hh"
#include "TimeStamp.hh"
#include "UpperScreen.hh"
#include "LowerSubScreenArea.hh"
#include "ProxSetupSubScreen.hh"
//@ End-Usage

#include <stdio.h>
#include <string.h>

//@ Code...
// allocate space for static class's data storage (declared in Dci.hh)

// special ASCII character constants
const	char		Dci::CR = 0x0D;			// control-M
const	char		Dci::FIELD_DELIMITER =	',';	// comma
static	const	char *	DciAHeader_ = "706,97";
static	const	char *	DciSNIDHeader_ = "102,14";
static	const	char *	DciSLMTHeader_ = "135,15";

static  const   char *  DciFHeader_ = "1225,169 ";
static  const   char *  DciFHeaderPhilips_ = "1229,169 ";

static  const   Real32  EMPTY_STR_ID = Real32(-10000.0); // -10000.0


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  GetDci
//
//@ Interface-Description
//  Static initializer for the DCI object. On first call, instantiates
//  and returns a reference to the Dci object. Subsequent calls only
//  return the reference.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  ((SerialInterface::SERIAL_PORT_NULL < portNum) && 
//  (portNum < SerialInterface::MAX_NUM_SERIAL_PORTS))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Dci &
Dci::GetDci(SerialInterface::PortNum portNum)
{
  // $[TI1]
  static Dci dci[SerialInterface::MAX_NUM_SERIAL_PORTS];

  SAFE_CLASS_PRE_CONDITION((SerialInterface::SERIAL_PORT_NULL < portNum) && 
      (portNum < SerialInterface::MAX_NUM_SERIAL_PORTS));

  return dci[portNum];
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Dci [constructor]
//
//@ Interface-Description
//  Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Dci::Dci()
	: isMarquette_( FALSE )
{
	// $[TI1]

    // Register for change in patient data
    PatientDataRegistrar::RegisterTarget(PatientDataId::IS_NIV_INSP_TOO_LONG_ITEM,
                                          this);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~Dci [destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Dci::~Dci()
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize
//
//@ Interface-Description
// Called during system-initialization to setup the DCI task execution
// environment prior to DCI task spawning.
//---------------------------------------------------------------------
//@ Implementation-Description
// During testing, we can initialize dummy values in the Batch Settings
// and Patient Data subsystems (if INIT_DUMMY_VALUES is defined at compile
// time).
//---------------------------------------------------------------------
//@ PreCondition
// Must be called DURING system initialization.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::Initialize(void)
{
  for (Int32 ii = SerialInterface::SERIAL_PORT_1; 
      ii < SerialInterface::MAX_NUM_SERIAL_PORTS; ii++)
  {
	Dci* pDci = &Dci::GetDci((SerialInterface::PortNum)ii);
        pDci->portNum_ = (SerialInterface::PortNum)ii;
  }
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  generateSNDA
//
//@ Interface-Description
//  Handle a DCI SNDA command by producing and outputting a DCI MISCA 
//  report.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Get all the pertinent ventilator data for a MISCA report.
//
//  After retrieving all the required data into the data buffers,
//  format each value as an ASCII string and put it into the output
//  string. reportATrailer_() terminates the report and sends it to
//  the serial port via the Serial-Interface subsystem.
//
//  $[00463] The host messages supported are: SNDA, SLMT, SER, SNID and RSET. 
//  Sigma shall respond to these host messages as long as the GUI can determine
//  that the BD is in the online state.
//  $[00464] In response to the SNDA<CR> host message the ventilator
//  shall reply with the MISCA report.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::generateSNDA(void)
{
    if ( TaskControlAgent::GetBdState() == STATE_ONLINE )
    {
        // $[TI1.1]
        getSndAData_();
 
        // produce a MISCA report in response to SNDA

	    // $[00464] $[00466] $[00472]
		reportHeader_(DciAId_, DciAHeader_);

		string6_( &dciMisc_.time );
		string18_( &dciMisc_.patientId );
		string6_( &dciMisc_.patientRoom );
		string12_( &dciMisc_.date );
		string6_( &dciSetting_.mode );
		float61_( &dciSetting_.respRate );
		float62_( &dciSetting_.tidalVolume );
		int6_( &dciSetting_.peakFlow );
		int6_( &dciSetting_.o2 );
		float61_( &dciSetting_.presSensitivity );
		float61_( &dciSetting_.peepCpap );
		float61_( &dciSetting_.plateauTime );
		int6_( &dciSetting_.sighRate );
		float62_( &dciSetting_.sighTidalVolume );
		int6_( &dciSetting_.multipleSighCount );
		int6_( &dciSetting_.sighHPL );
		int6_( &dciSetting_.apneaInterval );
		float62_( &dciSetting_.apneaTidalVolume );
		float61_( &dciSetting_.apneaRespRate );
		int6_( &dciSetting_.apneaPeakFlow );
		int6_( &dciSetting_.apneaO2 );
		int6_( &dciSetting_.supportPres );
		string6_( &dciSetting_.waveform );
		string6_( &dciSetting_.autoSighFeature );
		string6_( &dciSetting_.nebulizerFeature );
		string6_( &dciSetting_.suctionO2 );
		int6_( &dciSetting_.printInterval );
		int6_( &dciSetting_.dataAvgInterval );
		int6_( &dciSetting_.chartInterval );
		int6_( &dciPatient_.respRate );
		float62_( &dciPatient_.tidalVolume );
		float62_( &dciPatient_.minuteVolume );
		float61_( &dciPatient_.spontMinuteVolume );
		float61_( &dciPatient_.peakAirwayPres );
		float61_( &dciPatient_.meanAirwayPres );
		float61_( &dciPatient_.plateauPres );
		float62_( &dciPatient_.ePortion );
		int6_( &dciSetting_.highInspPres );
		int6_( &dciSetting_.lowInspPres );
		int6_( &dciSetting_.lowPeepCpap );
		float62_( &dciSetting_.lowTidalVolume );
		float61_( &dciSetting_.lowMinuteVolume );
		int6_( &dciSetting_.highRespRate );
		string6_( &dciAlarm_.highInspPres );
		string6_( &dciAlarm_.lowInspPres );
		string6_( &dciAlarm_.lowPeepCpapPres );
		string6_( &dciAlarm_.lowTidalVolume );
		string6_( &dciAlarm_.lowMinuteVolume );
		string6_( &dciAlarm_.highRespRate );
		string6_( &dciAlarm_.lowO2SupplyPres );
		string6_( &dciAlarm_.lowAirSupplyPres );
		string6_( &dciAlarm_.lowBattery );
		string6_( &dciAlarm_.apnea );
		string6_( &dciAlarm_.ieRatio );
		string6_( &dciAlarm_.exhValveLeak );
		string6_( &dciMisc_.time );
		int6_( &dciMisc_.practitionerId );
		string12_( &dciMisc_.date );
		float61_( &dciPatient_.Cstat );
		float61_( &dciPatient_.Rstat );
		float61_( &dciPatient_.Cdyn );
		float62_( &dciPatient_.Rdyn );
		int6_( &dciPatient_.NIF );
		float63_( &dciPatient_.VC );
		int6_( &dciPatient_.PSF);
		int6_( &dciSetting_.baseFlow );
		int6_( &dciSetting_.flowSensitivity );
		int6_( &dciPatient_.o2Saturation );
		int6_( &dciPatient_.pulseRate );
		int6_( &dciSetting_.lowO2Saturation );
		int6_( &dciSetting_.highO2Saturation );
		int6_( &dciSetting_.lowPulseRate );
		int6_( &dciSetting_.highPulseRate );
		int6_( &dciSetting_.oximeterAvgInterval );
		string6_( &dciAlarm_.lowO2Saturation );
		string6_( &dciAlarm_.highO2Saturation );
		string6_( &dciAlarm_.lowPulseRate );
		string6_( &dciAlarm_.highPulseRate );
		string6_( &dciAlarm_.oximeterStatus );
		float62_( &dciPatient_.plateauPres );
		int6_( &dciSetting_.pcvControlPres );
		float62_( &dciSetting_.pcvInspTime );
		int6_( &dciSetting_.apneaInterval );
		int6_( &dciSetting_.pcvApneaControlPres );
		float61_( &dciSetting_.apneaRespRate );
		float62_( &dciSetting_.pcvApneaInspTime );
		int6_( &dciSetting_.apneaO2 );
		int6_( &dciSetting_.highInspPres );
		string6_( &dciAlarm_.silence );
		string6_( &dciAlarm_.apneaVentilation );
		string6_( &dciAlarm_.disconnect );
		float62_( &dciSetting_.pcvIofIeRatio );
		float62_( &dciSetting_.pcvEofIeRatio );
		float62_( &dciSetting_.apneaPcvIofIeRatio );
		float62_( &dciSetting_.apneaPcvEofIeRatio );
		string9_( &dciSetting_.pcvControl );
		ieRatio62_( &dciPatient_.ieRatio );

		reportATrailer_();                 // terminate report
	}
    // $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isPhilips_
//
//@ Interface-Description
//  Check whether Philips device is the client
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
Boolean Dci::isPhilips_(void)
{
	Boolean isPhilips = FALSE;

	if( ( portNum_ == SerialInterface::SERIAL_PORT_1 ) &&
		( AcceptedContextHandle::GetDiscreteValue(SettingId::COM1_CONFIG) ==				  
		  ComPortConfigValue::PHILIPS_VALUE ) 
	   )
	{
		isPhilips = TRUE;
	}

	if( ( portNum_ == SerialInterface::SERIAL_PORT_2 ) &&
	   ( AcceptedContextHandle::GetDiscreteValue(SettingId::COM2_CONFIG) ==				  
		 ComPortConfigValue::PHILIPS_VALUE ) 
	  )
	{
	   isPhilips = TRUE;
	}
	
	if( ( portNum_ == SerialInterface::SERIAL_PORT_3 ) &&
		( AcceptedContextHandle::GetDiscreteValue(SettingId::COM3_CONFIG) ==				  
		  ComPortConfigValue::PHILIPS_VALUE ) 
	   )
	{
		isPhilips = TRUE;
	}

	return isPhilips;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  generateSNDF
//
//@ Interface-Description
//  Handle a DCI SNDF command by producing and outputting a DCI MISCF 
//  report.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Get all the pertinent ventilator data for a MISCF report.
//
//  After retrieving all the required data into the data buffers,
//  format each value as an ASCII string and put it into the output
//  string. reportATrailer_() terminates the report and sends it to
//  the serial port via the Serial-Interface subsystem.
//
//  $[00463] The host messages supported are: SNDA, SNDF, SLMT, SER, SNID and RSET. 
//  Sigma shall respond to these host messages as long as the GUI can determine
//  that the BD is in the online state.
//  $[RM00400] In response to the SNDF<CR> host message the ventilator
//  shall reply with the MISCF report.
//  $[RM00402]
//  $[RM00403] 
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::generateSNDF(void)
{
    if ( TaskControlAgent::GetBdState() == STATE_ONLINE )
    {
        getSndFData_();
 
        // produce a MISCF report in response to SNDF

        dciMiscF_.blank.put(PBlankStr6_);

		if( isPhilips_() )
		{
			// [NV00401] If Philips IntelliBridge monitor then respond with a 
			//			 special SNDF header.
			reportHeader_(DciFId_, DciFHeaderPhilips_);
		}
		else
		{
        reportHeader_(DciFId_, DciFHeader_);
		}

        string6_( &dciMiscF_.time );
        string18_( &dciMiscF_.patientId );
        string12_( &dciMiscF_.date );

        // Settings
        string9_( &dciSettingF_.ventType);
        string6_( &dciSettingF_.mode );      
        string6_( &dciSettingF_.mandatoryType);
        string6_( &dciSettingF_.spontType);
        string6_( &dciSettingF_.TriggerType);
        string6_( &dciSettingF_.respRate );
        string6_( &dciSettingF_.tidalVolume );
        string6_( &dciSettingF_.peakFlow );
        string6_( &dciSettingF_.o2 );
        string6_( &dciSettingF_.presSensitivity );
        string6_( &dciSettingF_.peepCpap );
        string6_( &dciSettingF_.plateauTime );

        // Apnea Settings
        string6_( &dciSettingF_.apneaInterval );
        string6_( &dciSettingF_.apneaTidalVolume );
        string6_( &dciSettingF_.apneaRespRate );
        string6_( &dciSettingF_.apneaPeakFlow );
        string6_( &dciSettingF_.apneaO2 );
        string6_( &dciSettingF_.pcvApneaControlPres );
        string6_( &dciSettingF_.pcvApneaInspTime );
        string6_(&dciSettingF_.apneaFlowPattern);
        string6_(&dciSettingF_.apneaMandatoryType);
        string6_( &dciSettingF_.apneaPcvIofIeRatio );
        string6_( &dciSettingF_.apneaPcvEofIeRatio );
        string6_( &dciSettingF_.supportPres );
        string6_( &dciSettingF_.waveform );
        string6_( &dciSettingF_.suctionO2 );

        // Alarm Settings.
        string6_( &dciSettingF_.highPpeak );
        string6_( &dciSettingF_.lowPpeak );
        string6_(&dciSettingF_.highVeTot);
        string6_(&dciSettingF_.lowVeTot);
        string6_(&dciSettingF_.highVteMand);
        string6_(&dciSettingF_.lowVteMand);
        string6_(&dciSettingF_.highVteSpont);
        string6_(&dciSettingF_.lowVteSpont);
        string6_( &dciSettingF_.highFtot );
        string6_(&dciSettingF_.highVti);
        string6_( &dciSettingF_.baseFlow );
        string6_( &dciSettingF_.flowSensitivity );
        string6_( &dciSettingF_.pcvControlPres );
        string6_( &dciSettingF_.pcvInspTime );
        string6_( &dciSettingF_.pcvIofIeRatio );
        string6_( &dciSettingF_.pcvEofIeRatio );
        string9_( &dciSettingF_.pcvControl );
        string6_( &dciSettingF_.tubeId);
        string6_( &dciSettingF_.tubeType);
        string18_( &dciSettingF_.humidificationType);
        string6_( &dciSettingF_.humidificationVol);
        string9_( &dciSettingF_.O2sensor);
        string6_( &dciSettingF_.DSens);
        string6_( &dciSettingF_.riseTimePercent);
        string6_( &dciSettingF_.percentSupport);
        string6_( &dciSettingF_.eSens);
        string6_(&dciSettingF_.ibw);
        string6_(&dciSettingF_.volumeSupport);
        string6_(&dciSettingF_.PEEPh);
        string6_(&dciSettingF_.PEEPl);
        string6_(&dciSettingF_.highPEEPtime);
        string6_(&dciSettingF_.highSpontInspTimeLimit);
        string9_(&dciSettingF_.circuitType);
        string6_(&dciSettingF_.lowPEEPtime);
        string6_(&dciSettingF_.Te);

        // Patient Data
        float61_( &dciPatientF_.plateauPres );
        float61_( &dciPatientF_.respRate );
        float63_( &dciPatientF_.Vte);
        float63_( &dciPatientF_.VeTot);
        float61_( &dciPatientF_.peakAirwayPres );
        float61_( &dciPatientF_.meanAirwayPres );
        float62_( &dciPatientF_.ePortion );
        ieRatio62_( &dciPatientF_.ieRatio );
        int6_( &dciPatientF_.deliveredO2 );     
        float63_( &dciPatientF_.Vti);
        float61_( &dciPatientF_.PEEPi);
        float61_( &dciPatientF_.Rtot);
        float61_( &dciPatientF_.Rpav);
        float61_( &dciPatientF_.Epav);
        float61_( &dciPatientF_.Cpav);
        float61_( &dciPatientF_.normRapShadowIndx);
        float62_( &dciPatientF_.rapShadowIndx);
        float62_( &dciPatientF_.spontInspPercent);
        float61_( &dciPatientF_.endExhPress);
        float62_( &dciPatientF_.TiSpont);
        float62_( &dciPatientF_.VeSpont);
        float61_( &dciPatientF_.PEEP_I);
        float61_( &dciPatientF_.PEEP_Tot);
        float61_( &dciPatientF_.Cstat);
        float61_( &dciPatientF_.Rstat);
        float61_( &dciPatientF_.Ppl);
        string6_( &dciPatientF_.highTiSpont);
        float61_( &dciPatientF_.Cdyn);
        float61_( &dciPatientF_.Rdyn);
        float61_( &dciPatientF_.PSF);
        float61_( &dciPatientF_.PEF);
        float61_( &dciPatientF_.EEF);
        string6_( &dciSettingF_.proxState);
        float61_( &dciPatientF_.NIF);
        float61_( &dciPatientF_.P100);
        float63_( &dciPatientF_.VC);


        // Alarms
        string6_( &dciAlarmF_.silence );
        string6_( &dciAlarmF_.apneaVentilation );
        string6_( &dciAlarmF_.highMinuteVolume);
        string6_( &dciAlarmF_.highExhaledVolume);
        string6_( &dciAlarmF_.highO2Percent);
        string6_( &dciAlarmF_.highInspPres );
        string6_( &dciAlarmF_.pVent );
        string6_( &dciAlarmF_.highRespRate);
        string6_( &dciAlarmF_.acPowerLoss);
        string6_( &dciAlarmF_.inoperativeBattery);
        string6_( &dciAlarmF_.lowBattery);
        string6_( &dciAlarmF_.lowACPower);
        string6_( &dciAlarmF_.lowExhaledMandVolume);
        string6_( &dciAlarmF_.lowMinuteVolume);
        string6_( &dciAlarmF_.lowExhaledSpontVolume);
        string6_( &dciAlarmF_.lowO2Percent);
        string6_( &dciAlarmF_.lowAirSupplyPres );
        string6_( &dciAlarmF_.lowO2SupplyPres );
        string6_( &dciAlarmF_.compressorInop );
        string6_( &dciAlarmF_.disconnect );
        string6_( &dciAlarmF_.occlusion );
        string6_( &dciAlarmF_.inspirationTooLong );
        string6_( &dciAlarmF_.procedureError );
        string6_( &dciAlarmF_.volumeLimit);
        string6_( &dciAlarmF_.highInspiredSpontVolume);
        string6_( &dciAlarmF_.highInspiredMandVolume);
        string6_( &dciAlarmF_.highCompensationLimit);
        string6_( &dciAlarmF_.pavStartupTooLong);
        string6_( &dciAlarmF_.pavRCnotAssessed);
        string6_( &dciAlarmF_.volumeNotDeliveredVcPlus);
        string6_( &dciAlarmF_.volumeNotDeliveredVS);
        string6_( &dciAlarmF_.lowInspPres);

        string6_( &dciAlarmF_.btat1);
        string6_( &dciAlarmF_.btat2);
        string6_( &dciAlarmF_.btat3);
        string6_( &dciAlarmF_.btat4);
        string6_( &dciAlarmF_.btat5);
        string6_( &dciAlarmF_.btat6);
        string6_( &dciAlarmF_.btat7);
        string6_( &dciAlarmF_.btat8);
        string6_( &dciAlarmF_.btat9);
        string6_( &dciAlarmF_.btat10);
        string6_( &dciAlarmF_.btat11);
        string6_( &dciAlarmF_.btat12);
        string6_( &dciAlarmF_.btat13);
        string6_( &dciAlarmF_.btat14);
        string6_( &dciAlarmF_.btat15);
        string6_( &dciAlarmF_.btat16);
        string6_( &dciAlarmF_.btat17);

		float63_( &dciPatientF_.VteSpont);        
		float61_( &dciPatientF_.WOBtot);

        string6_( &dciSettingF_.leakState );
        int6_( &dciPatientF_.percentLeak );
        float61_( &dciPatientF_.leak );
        int6_( &dciPatientF_.iLeak );
		string6_( &dciAlarmF_.proxAlarm);

        // Reserved for future updates
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );

        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );
        string6_( &dciMiscF_.blank );

        // terminate report
        reportATrailer_();                 
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSndFData_
//
//@ Interface-Description
// Accumulate the necessary batch setting, patient, alarm status
// and miscellaneous data for a  MISCF report.
//---------------------------------------------------------------------
//@ Implementation-Description
// MISCF report data is derived from 3 major subsystems/classes:  batch
// settings, patient data and alarm analysis.  Remaining report data,
// such as date and time, are considered to be miscellaneous.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getSndFData_(void)
{
    // Accumulate all SNDF data from Settings, 
    // Patient Data, Alarms and Misc.
    getMiscDataF_();
    getBatchSettingsF_();
    getPatientDataF_();
    getAlarmStatusF_();
    
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  generateSNID
//
//@ Interface-Description
//  Handle a DCI SNID command by producing and outputting a DCI IDENT 
//  report.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Get all the pertinent ventilator data for an IDENT report.
//
//  After retrieving all the required data into the data buffers,
//  format each value as an ASCII string and put it into the output
//  string. reportSNIDTrailer_() terminates the report and sends it to
//  the serial port via the Serial-Interface subsystem.
//
//  $[00463] The host messages supported are: SNDA, SLMT, SER, SNID and RSET. 
//  Sigma shall respond to these host messages as long as the GUI can determine
//  that the BD is in the online state.
//  $[MC00400] In response to the SNID<CR> host message the ventilator
//  shall reply with the IDENT report.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::generateSNID(void)
{
  if ( TaskControlAgent::GetBdState() == STATE_ONLINE )
  {
    // $[TI1]
    // produce a report in response to SNID

	dciMisc_.softwareRevNum.put(getSoftwareRevNum_());
    dciMisc_.blank.setNotInThisRelease();

    // $[MC00403]
    reportHeader_(DciSNIDId_, DciSNIDHeader_);

    string16_( &dciMisc_.blank );
    string12_( &dciMisc_.blank );
    string12_( &dciMisc_.softwareRevNum);
    string16_( &dciMisc_.blank );
    string5_( &dciMisc_.blank );
    string3_( &dciMisc_.blank );
    string3_( &dciMisc_.blank );
    string3_( &dciMisc_.blank );
    string3_( &dciMisc_.blank );
    string3_( &dciMisc_.blank );
    string3_( &dciMisc_.blank );
    string3_( &dciMisc_.blank );
    string3_( &dciMisc_.blank );
    string3_( &dciMisc_.blank );

    reportSNIDTrailer_();                 // terminate report
  }
  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  generateSER
//
//@ Interface-Description
//  Handle a DCI SER command by producing and outputting a DCI ER 
//  report.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Get all the pertinent ventilator data for a ER report.
//
//  After retrieving all the required data into the data buffers,
//  format each value as an ASCII string and put it into the output
//  string. reportSERTrailer_() terminates the report and sends it to
//  the serial port via the Serial-Interface subsystem.
//
//  $[00463] The host messages supported are: SNDA, SLMT, SER, SNID and RSET. 
//  Sigma shall respond to these host messages as long as the GUI can determine
//  that the BD is in the online state.
//  $[MC00401] In response to the SER <CR> host message the ventilator
//  shall reply with the ER report.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::generateSER(void)
{
  if ( TaskControlAgent::GetBdState() == STATE_ONLINE )
  {
    // $[TI1]
	// set flag indicating a Marquette monitor is connected
	isMarquette_ = TRUE;

    dciMisc_.time.put(getTime_());
    dciMisc_.blank.setNotInThisRelease();
    dciMisc_.estResult.put(PPassStr6_);

    reportHeader_(DciSERId_, NULL);

    // produce a report in response to SER
    // $[MC00404]

    string6_( &dciMisc_.time );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string6_( &dciMisc_.estResult );

    reportSERTrailer_();                 // terminate report
  }
  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  generateSLMT
//
//@ Interface-Description
//  Handle a DCI SLMT command by producing and outputting a DCI LMT 
//  report.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Get all the pertinent ventilator data for a LMT report.
//
//  After retrieving all the required data into the data buffers,
//  format each value as an ASCII string and put it into the output
//  string. reportSLMTTrailer_() terminates the report and sends it to
//  the serial port via the Serial-Interface subsystem.
//
//  $[00463] The host messages supported are: SNDA, SLMT, SER, SNID and RSET. 
//  Sigma shall respond to these host messages as long as the GUI can determine
//  that the BD is in the online state.
//  $[MC00402] In response to the SLMT<CR> host message the ventilator
//  shall reply with the LMT report.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::generateSLMT(void)
{
  if ( TaskControlAgent::GetBdState() == STATE_ONLINE )
  {
    // $[TI1]

    dciMisc_.blank.setNotInThisRelease();

    // produce a report in response to SLMT
    // $[MC00405]
    reportHeader_(DciSLMTId_, DciSLMTHeader_);

    string16_( &dciMisc_.blank );
    string5_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string16_( &dciMisc_.blank );
    string4_( &dciMisc_.blank );
    string16_( &dciMisc_.blank );
    string4_( &dciMisc_.blank );
    string16_( &dciMisc_.blank );
    string4_( &dciMisc_.blank );
    string6_( &dciMisc_.blank );
    string4_( &dciMisc_.blank );
    string2_( &dciMisc_.blank );
    string2_( &dciMisc_.blank );
    string16_( &dciMisc_.blank );
    string4_( &dciMisc_.blank );

    reportSLMTTrailer_();                 // terminate report
  }
  // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  output_
//
//@ Interface-Description
//  Write the data in the null terminated character string to the 
//  serial port. Always returns zero.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the SerialInterface::Write method which outputs the data
//  over the serial port and waits up to the specified timeout of
//  15 seconds for the data to be transmitted. Upon return, the data
//  has been transmitted or a timeout has occured and the output
//  discarded.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::output_(const char * pBuffer)
{
	// report is discarded after 15 second timeout
	SerialInterface::Write( pBuffer, strlen( pBuffer ), 15000, portNum_); 

	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSndAData_
//
//@ Interface-Description
// Accumulate the necessary batch setting, patient, alarm status
// and miscellaneous data for a  MISCA report.
//---------------------------------------------------------------------
//@ Implementation-Description
// MISCA report data is derived from 3 major subsystems/classes:  batch
// settings, patient data and alarm analysis.  Remaining report data,
// such as date and time, are considered to be miscellaneous.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getSndAData_(void)
{
	// use appropriate units or legal values			$[00474]
	getMiscData_();
	getBatchSettings_();
	getPatientData_();
	getAlarmStatus_();
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getMiscData_
//
//@ Interface-Description
// Accumulate that data that is deemed to be miscellaneous (not obtained
// from a given subsystem/class) in nature.  The data is written in the
// global data structure, dciMisc_.
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported are indicated as such so that they can be
// substituted with blanks during output.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getMiscData_(void)
{
		// fetch current settings...
	dciMisc_.time.put(getTime_());
	dciMisc_.patientId.put(getPatientId_());
	dciMisc_.patientRoom.setNotInThisRelease();
	dciMisc_.date.put(getDate_());
	dciMisc_.practitionerId.setNotInThisRelease();
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getMiscDataF_
//
//@ Interface-Description
// Accumulate that data that is deemed to be miscellaneous (not obtained
// from a given subsystem/class) in nature.  The data is written in the
// global data structure, dciMiscF_
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported are indicated as such so that they can be
// substituted with blanks during output.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getMiscDataF_(void)
{
        // fetch current settings...
    dciMiscF_.time.put(getTime_());
    dciMiscF_.patientId.put(getPatientId_());
    dciMiscF_.date.put(getDate_());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getTime_
//
//@ Interface-Description
//  Returns pointer to DCI-formatted time stamp.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

char *
Dci::getTime_(void)
{
	// $[TI1]
	TimeStamp	clock;

	Int32 count = sprintf(timeBuffer_, "%02d:%02d ", 
	                          clock.getHour(), 
				              clock.getMinutes());

	CLASS_ASSERTION( count <= sizeof( timeBuffer_ ) );

	return timeBuffer_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getDate_
//
//@ Interface-Description
//  Returns pointer to DCI-formatted date stamp.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

char *
Dci::getDate_(void)
{
	// $[TI1]
	TimeStamp	clock;

	Int32 count = sprintf(dateBuffer_, "%3.3s %02d %04d ",
		                        MonthNames_[clock.getMonth() - 1],
		                        clock.getDayOfMonth(),
		                        clock.getYear());

	CLASS_ASSERTION( count <= sizeof( dateBuffer_ ) );

	return dateBuffer_;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getPatientId_
//
//@ Interface-Description
//  Returns the formatted DCI patient ID field consisting of the 
//  string "840 " followed by the serial number of the BD unit.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method formats the patient ID field each time it is called
//  (instead of only the first time) since the BD serial number on the
//  GUI is blank until the BD starts communicating with the GUI.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

char *
Dci::getPatientId_(void)
{
	// $[TI1]

	//  field is 18 characters wide - format to not overflow
    Int32 count = sprintf(patientIdBuffer_, "840 %-.14s", 
	                          GuiApp::GetBdSN().getString() );
	
	CLASS_ASSERTION( count <= sizeof( patientIdBuffer_ ) );

	return patientIdBuffer_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getSoftwareRevNum_
//
//@ Interface-Description
//  Returns pointer to DCI-formatted software revision number string.
//	The "4-0" at the beginning of the "full" software revision number
//	string is removed to fit the 12-character DCI field.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

char *
Dci::getSoftwareRevNum_(void)
{
	//TODO BVP - E600 Need to check this logic. With the new Revision
	//number scheme (major.minor.test.dev, instead of the ECO), we have to
	//double check the impact here
	// $[TI1]
	Int32 count = sprintf( scratchBuffer_, "%-12.12s",
		SoftwareRevNum::GetInstance().GetAsShortString());

	CLASS_ASSERTION( count <= sizeof(scratchBuffer_) );

	return scratchBuffer_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicableBoundedValue_
//
//@ Interface-Description
//  $[TC0040] The 840's DCI feature shall zero out settings in the SNDA 
//  report which are not applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

Real32
Dci::getApplicableBoundedValue_(BatchSettingValues &settings, 
			        SettingId::SettingIdType settingId)
{
  const Applicability::Id  APPLICABILITY = 
      SettingContextHandle::GetSettingApplicability 
      (ContextId::ACCEPTED_CONTEXT_ID, settingId);

  Real32 applicableValue;

  switch (APPLICABILITY)
  {
  case Applicability::CHANGEABLE :   // $[TI1]
    applicableValue = settings.getBoundedValue(settingId).value;
    break;
  case Applicability::VIEWABLE :     // $[TI2]
  case Applicability::INAPPLICABLE : // $[TI3]
    applicableValue = 0.0f;
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE((APPLICABILITY << 16) | settingId);
    applicableValue = 0.0f;
    break;
  }

  if (settingId == SettingId::LOW_EXH_MINUTE_VOL  &&
	  SettingConstants::LOWER_ALARM_LIMIT_OFF == applicableValue)
  {  // $[TI4]
	// divide low minute volume's "OFF" value by 1000, to be consistent with
	// volume limit settings whose values are divided by 1000 to convert to
	// liters...
	applicableValue /= 1000.0f;
  }  // $[TI5]

  return (applicableValue);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isDiscreteValueApplicable_
//
//@ Interface-Description
//  $[TC0040] The 840's DCI feature shall zero or blank out settings 
//  in the SNDA report which are not applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

Boolean
Dci::isDiscreteValueApplicable_(BatchSettingValues &settings, 
			        SettingId::SettingIdType settingId)
{
  return (SettingContextHandle::GetSettingApplicability 
      (ContextId::ACCEPTED_CONTEXT_ID, settingId) == 
      Applicability::CHANGEABLE); // $[TI1] $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getBatchSettings_
//
//@ Interface-Description
// Accumulate BatchSettings data into global data structure, dciSetting_.
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported or not valid are indicated as such so that they
// can be substituted with blanks during output.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getBatchSettings_(void)
{

	// batch settings are initialized during system initialization
	// therefore they should be valid by the time this method executes
	CLASS_ASSERTION( NovRamManager::AreBatchSettingsInitialized() );

	BatchSettingValues settings;
	NovRamManager::GetAcceptedBatchSettings(settings);
	Real32	ieRatio;
	Real32	apneaInspTime;
	Real32	flowSensitivity;
	Int32	constP;
	Int32   flowP;

	// fetch current settings...
	ventMode_ = (ModeValue::ModeValueId)settings.getDiscreteValue(SettingId::MODE);
	switch(ventMode_)
	{
	case ModeValue::AC_MODE_VALUE:		// $[TI1.1]
		dciSetting_.mode.put(PCmvStr6_);
		dciSetting_.lowTidalVolume.put(
			(getApplicableBoundedValue_(settings, SettingId::LOW_EXH_MAND_TIDAL_VOL) + 0.5F)
			/ 1000.0F);		// convert ml to l
		dciSetting_.pcvControlPres.put(
			getApplicableBoundedValue_(settings, SettingId::INSP_PRESS));
		dciSetting_.peepCpap.put(
			getApplicableBoundedValue_(settings, SettingId::PEEP));
		break;
	case ModeValue::SIMV_MODE_VALUE:	// $[TI1.2]
		dciSetting_.mode.put(PSimvStr6_);
		dciSetting_.lowTidalVolume.put(
			(getApplicableBoundedValue_(settings, SettingId::LOW_EXH_SPONT_TIDAL_VOL) + 0.5F)
			/ 1000.0F);		// convert ml to l
		dciSetting_.pcvControlPres.put(
			getApplicableBoundedValue_(settings, SettingId::INSP_PRESS));
		dciSetting_.peepCpap.put(
			getApplicableBoundedValue_(settings, SettingId::PEEP));
		break;
	case ModeValue::SPONT_MODE_VALUE:	// $[TI1.3]
	case ModeValue::CPAP_MODE_VALUE:
		dciSetting_.mode.put(PCpapStr6_);
		dciSetting_.lowTidalVolume.put(
			(getApplicableBoundedValue_(settings, SettingId::LOW_EXH_SPONT_TIDAL_VOL) + 0.5F)
			/ 1000.0F);		// convert ml to l
		dciSetting_.pcvControlPres.put(
			getApplicableBoundedValue_(settings, SettingId::INSP_PRESS));
		dciSetting_.peepCpap.put(
			getApplicableBoundedValue_(settings, SettingId::PEEP));
		break;
	case ModeValue::BILEVEL_MODE_VALUE:	// $[TI1.4]
		dciSetting_.mode.put(PBilevlStr6_);
		dciSetting_.lowTidalVolume.put(
			(getApplicableBoundedValue_(settings, SettingId::LOW_EXH_SPONT_TIDAL_VOL) + 0.5F)
			/ 1000.0F);		// convert ml to l
		dciSetting_.pcvControlPres.put(
			getApplicableBoundedValue_(settings, SettingId::PEEP_HIGH));
		dciSetting_.peepCpap.put(
			getApplicableBoundedValue_(settings, SettingId::PEEP_LOW));
		break;
	default:	// invalid condition
		AUX_CLASS_ASSERTION_FAILURE( ventMode_ );
	}

	dciSetting_.respRate.put(
		getApplicableBoundedValue_(settings, SettingId::RESP_RATE));
	dciSetting_.tidalVolume.put(
		getApplicableBoundedValue_(settings, SettingId::TIDAL_VOLUME)
		/ 1000.0F);		// convert ml to l
	dciSetting_.peakFlow.put(
		getApplicableBoundedValue_(settings, SettingId::PEAK_INSP_FLOW));
	dciSetting_.o2.put(
		getApplicableBoundedValue_(settings, SettingId::OXYGEN_PERCENT));
	dciSetting_.presSensitivity.put(
		getApplicableBoundedValue_(settings, SettingId::PRESS_SENS));
	dciSetting_.plateauTime.put(
		getApplicableBoundedValue_(settings, SettingId::PLATEAU_TIME)
		/ 1000.0F);		// convert ms to s
	dciSetting_.sighRate.setNotSupported();
	dciSetting_.sighTidalVolume.setNotSupported();
	dciSetting_.multipleSighCount.setNotSupported();
	dciSetting_.sighHPL.setNotSupported();

	Real32 apneaInterval = getApplicableBoundedValue_(settings, SettingId::APNEA_INTERVAL);
	if (SettingConstants::UPPER_ALARM_LIMIT_OFF == apneaInterval)
	{
		apneaInterval = 0.0;
	}
	else
	{
		apneaInterval /= 1000.0;  // convert ms to s
	}
	dciSetting_.apneaInterval.put(apneaInterval);

	dciSetting_.apneaTidalVolume.put(
		getApplicableBoundedValue_(settings, SettingId::APNEA_TIDAL_VOLUME)
		/ 1000.0F);		// convert ml to l
	dciSetting_.apneaRespRate.put(
		getApplicableBoundedValue_(settings, SettingId::APNEA_RESP_RATE));
	dciSetting_.apneaPeakFlow.put(
		getApplicableBoundedValue_(settings, SettingId::APNEA_PEAK_INSP_FLOW));
	dciSetting_.apneaO2.put(
		getApplicableBoundedValue_(settings, SettingId::APNEA_O2_PERCENT));
	dciSetting_.supportPres.put(
		getApplicableBoundedValue_(settings, SettingId::PRESS_SUPP_LEVEL));
	if (isDiscreteValueApplicable_(settings, SettingId::FLOW_PATTERN)) // $[TI2.1]
	{
	  flowP = settings.getDiscreteValue(SettingId::FLOW_PATTERN);

	  switch ( flowP )
	  {
	    case FlowPatternValue::SQUARE_FLOW_PATTERN: // $[TI2.1.1]
	      dciSetting_.waveform.put(PSquareStr6_);
	      break;
	    case FlowPatternValue::RAMP_FLOW_PATTERN: // $[TI2.1.2]
	      dciSetting_.waveform.put(PRampStr6_);
	      break;
	    default:	// invalid condition
	      AUX_CLASS_ASSERTION_FAILURE( flowP );
	  }
	}
	else // $[TI2.2]
	{
	  dciSetting_.waveform.put(PBlankStr6_);
	}	

	dciSetting_.autoSighFeature.setNotSupported();
	dciSetting_.nebulizerFeature.setNotSupported();

     // mark Suction on if either On screen Calibration key is hit
    // or O2 key pad keys at hit
	const Boolean ON_OFF = ( ( BdGuiEvent::GetEventStatus(EventData::PERCENT_O2) 
							   == EventData::ACTIVE ) || 
							 ( BdGuiEvent::GetEventStatus(EventData::CALIBRATE_O2) 
							   == EventData::ACTIVE ) );

	// O2 Suction
	encodeOnOff_(  &dciSetting_.suctionO2, ON_OFF);

	dciSetting_.printInterval.setNotSupported();
	dciSetting_.dataAvgInterval.setNotSupported();
	dciSetting_.chartInterval.setNotSupported();
	dciSetting_.highInspPres.put(
		getApplicableBoundedValue_(settings, SettingId::HIGH_CCT_PRESS));
	dciSetting_.lowInspPres.setNotSupported();
	dciSetting_.lowPeepCpap.setNotSupported();

	Real32 lowTeVol = getApplicableBoundedValue_(settings, SettingId::LOW_EXH_MINUTE_VOL);
	if (SettingConstants::LOWER_ALARM_LIMIT_OFF == lowTeVol)
	{
	  // divide low minute volume's "OFF" value by 1000, to be consistent with
	  // volume limit settings whose values are divided by 1000 to convert to
	  // liters...
	  lowTeVol /= 1000.0f;
	}
	dciSetting_.lowMinuteVolume.put(lowTeVol);

	Real32 highRespRate = getApplicableBoundedValue_(settings, SettingId::HIGH_RESP_RATE);

	if (IsEquivalent(highRespRate, SettingConstants::UPPER_ALARM_LIMIT_OFF, TENS))
	{
		highRespRate = 100000;
	}
	dciSetting_.highRespRate.put(highRespRate);


	flowSensitivity = 
		getApplicableBoundedValue_(settings, SettingId::FLOW_SENS);
	if (IsEquivalent(flowSensitivity, 0.0f, TENTHS)) // $[TI4]
	{
	  dciSetting_.baseFlow.put(0.0f);
	}
	else // $[TI5]
	{
	  // TODO E600 port
	  //dciSetting_.baseFlow.put(flowSensitivity + ::BASE_FLOW_MIN);
	}
	dciSetting_.flowSensitivity.put(flowSensitivity);

	dciSetting_.lowO2Saturation.setNotSupported();
	dciSetting_.highO2Saturation.setNotSupported();
	dciSetting_.lowPulseRate.setNotSupported();
	dciSetting_.highPulseRate.setNotSupported();
	dciSetting_.oximeterAvgInterval.setNotSupported();
	dciSetting_.pcvApneaControlPres.put(
		getApplicableBoundedValue_(settings, SettingId::APNEA_INSP_PRESS));

	apneaInspTime = 
		getApplicableBoundedValue_(settings, SettingId::APNEA_INSP_TIME)
		/ 1000.0F;		// convert ms to s
	if (IsEquivalent(apneaInspTime, 0.0f, TENTHS)) // $[TI8]
	{
	  dciSetting_.pcvApneaInspTime.put(0.0f);
	  dciSetting_.apneaPcvIofIeRatio.put(0.0f);
	  dciSetting_.apneaPcvEofIeRatio.put(0.0f);
	}
	else // $[TI9]
	{
	  dciSetting_.pcvApneaInspTime.put(apneaInspTime);
	  ieRatio = settings.getBoundedValue(SettingId::APNEA_IE_RATIO).value;
	  dciSetting_.apneaPcvIofIeRatio.put(computeIofIeRatio_(ieRatio));
	  dciSetting_.apneaPcvEofIeRatio.put(computeEofIeRatio_(ieRatio));
	}

	if (isDiscreteValueApplicable_(settings, SettingId::CONSTANT_PARM)) // $[TI3.1]
	{
	  if (ventMode_ == ModeValue::BILEVEL_MODE_VALUE) // $[TI3.1.4]
	  {
	    dciSetting_.pcvInspTime.put(settings.getBoundedValue(SettingId::PEEP_HIGH_TIME)
		/ 1000.0F);		// convert ms to s
	    ieRatio = settings.getBoundedValue(SettingId::HL_RATIO).value;
	  }
	  else // $[TI3.1.5]
	  {
	    dciSetting_.pcvInspTime.put(settings.getBoundedValue(SettingId::INSP_TIME)
		/ 1000.0F);		// convert ms to s
	    ieRatio = settings.getBoundedValue(SettingId::IE_RATIO).value;
	  }
	  dciSetting_.pcvIofIeRatio.put(computeIofIeRatio_(ieRatio));
	  dciSetting_.pcvEofIeRatio.put(computeEofIeRatio_(ieRatio));

	  constP = settings.getDiscreteValue(SettingId::CONSTANT_PARM);

	  switch ( constP )
	  {
	  case ConstantParmValue::INSP_TIME_CONSTANT: // $[TI3.1.1]
		dciSetting_.pcvControl.put(PITimeStr6_);
		break;
	  case ConstantParmValue::IE_RATIO_CONSTANT: // $[TI3.1.2]
		dciSetting_.pcvControl.put(PIeRatioStr6_);
		break;
	  case ConstantParmValue::EXP_TIME_CONSTANT: // $[TI3.1.3]
		dciSetting_.pcvControl.setNotSupported();
		break;
	  default:	// invalid condition
		AUX_CLASS_ASSERTION_FAILURE( constP );
	  }
	}
	else // $[TI3.2]
	{
	  dciSetting_.pcvInspTime.put(0.0f);
	  dciSetting_.pcvIofIeRatio.put(0.0f);
	  dciSetting_.pcvEofIeRatio.put(0.0f);
	  dciSetting_.pcvControl.put(PBlankStr6_);
	}	
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getBatchSettingsF_
//
//@ Interface-Description
// Accumulate BatchSettings data into global data structure, dciSettingF_
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported or not valid are indicated as such so that they
// can be substituted with blanks during output.
// $[RM00401]
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getBatchSettingsF_(void)
{

    // batch settings are initialized during system initialization
    // therefore they should be valid by the time this method executes
    CLASS_ASSERTION( NovRamManager::AreBatchSettingsInitialized() );

    BatchSettingValues settings;
    NovRamManager::GetAcceptedBatchSettings(settings);
    Real32  ieRatio;
    Real32  apneaInspTime;
    Real32  flowSensitivity;
    Int32   constP, flowP;

    // Get Current Vent Type
    const DiscreteValue VENT_TYPE = 
        SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAcceptedValue();

    switch(VENT_TYPE)
    {
        case VentTypeValue::NIV_VENT_TYPE:
            dciSettingF_.ventType.put(PNIVStr9_);
            break;
        case VentTypeValue::INVASIVE_VENT_TYPE:
            dciSettingF_.ventType.put(PInvasiveStr9_);
            break;
        default:
            // Invalid VentType Setting
            AUX_CLASS_ASSERTION_FAILURE( VENT_TYPE );
            break;
    }    

    // Get Mode
    ventMode_ = (ModeValue::ModeValueId)settings.getDiscreteValue(SettingId::MODE);

    switch(ventMode_)
    {
        case ModeValue::AC_MODE_VALUE:  // $[TI1.1]
            dciSettingF_.mode.put(PACStr6_);
            break;
        case ModeValue::SIMV_MODE_VALUE:
            dciSettingF_.mode.put(PSimvStr6_);
            break;
        case ModeValue::SPONT_MODE_VALUE:
            dciSettingF_.mode.put(PSpontStr6_);
            break;
        case ModeValue::BILEVEL_MODE_VALUE:
            dciSettingF_.mode.put(PBilevlStr6_);
            break;
        case ModeValue::CPAP_MODE_VALUE:
            dciSettingF_.mode.put(PCpapStr6_);
            break;
        default:
            AUX_CLASS_ASSERTION_FAILURE( ventMode_ );
            break;
    }

    // Mandatory Type
    const DiscreteValue MANDATORY_TYPE =
        SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
                                              SettingId::MAND_TYPE);

    switch(MANDATORY_TYPE)
    {
        case MandTypeValue::PCV_MAND_TYPE:
            dciSettingF_.mandatoryType.put(PPCStr6_);
            break;
        case MandTypeValue::VCV_MAND_TYPE:
            dciSettingF_.mandatoryType.put(PVCStr6_);
            break;
        case MandTypeValue::VCP_MAND_TYPE:
            dciSettingF_.mandatoryType.put(PVCPlusStr6_);
            break;
        default:
            // Invalid Mandatory Type
            AUX_CLASS_ASSERTION_FAILURE( MANDATORY_TYPE );
            break;
    }

    // Get Support Type if applicable.
    const DiscreteValue SPONT_TYPE = 
        SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE)->getAcceptedValue();

    if(isDiscreteValueApplicable_(settings, SettingId::SUPPORT_TYPE))
    {
        switch(SPONT_TYPE)
        {

            case SupportTypeValue::OFF_SUPPORT_TYPE:
                dciSettingF_.spontType.put(PNoneStr6_);
                break;
            case SupportTypeValue::PSV_SUPPORT_TYPE:
                dciSettingF_.spontType.put(PPSStr6_);
                break;
            case SupportTypeValue::ATC_SUPPORT_TYPE:
                dciSettingF_.spontType.put(PTCStr6_);
                break;
            case SupportTypeValue::VSV_SUPPORT_TYPE:
                dciSettingF_.spontType.put(PVSStr6_);
                break;
            case SupportTypeValue::PAV_SUPPORT_TYPE:
                dciSettingF_.spontType.put(PPAStr6_);
                break;
            default:
                // Invalid SPONT_TYPE
                AUX_CLASS_ASSERTION_FAILURE( SPONT_TYPE );
                break;
        }

    }
    else
    {
        dciSettingF_.spontType.put(PBlankStr6_);

    }

    // Get Trigger Type
    const DiscreteValue TRIGGER_TYPE = 
        SettingsMgr::GetSettingPtr(SettingId::TRIGGER_TYPE)->getAcceptedValue();

    // Trigger Type
    switch(TRIGGER_TYPE)
    {
        case TriggerTypeValue::FLOW_TRIGGER:
            dciSettingF_.TriggerType.put(PVTrigerTypeStr6_);
            break;
        case TriggerTypeValue::PRESSURE_TRIGGER:
            dciSettingF_.TriggerType.put(PPTrigerTypeStr6_);
            break;
        default:
            // Invalid Trigger Type.
            AUX_CLASS_ASSERTION_FAILURE( TRIGGER_TYPE );
            break;
    }

    // Respiratory Rate Setting
    Real32 respRate = getApplicableBoundedValueF_(settings, SettingId::RESP_RATE);
    encodeSettingValues_( &dciSettingF_.respRate,  respRate,  TENTHS);

    // Tidal Volume Setting
    Real32 tidalVolume = getApplicableBoundedValueF_(settings, SettingId::TIDAL_VOLUME);
    encodeSettingValues_( &dciSettingF_.tidalVolume,  (tidalVolume/ 1000.0f),  THOUSANDTHS);

    // Peak Flow Setting
    Real32 peakFlow = getApplicableBoundedValueF_(settings, SettingId::PEAK_INSP_FLOW);
    encodeSettingValues_( &dciSettingF_.peakFlow,  peakFlow,  TENTHS);

    // Oxygen Percent Setting
    Real32 o2 = getApplicableBoundedValueF_(settings, SettingId::OXYGEN_PERCENT);
    encodeSettingValues_( &dciSettingF_.o2,  o2,  ONES);

    // Pressure Sensitivity Setting
    Real32 presSensitivity = getApplicableBoundedValueF_(settings, SettingId::PRESS_SENS);
    encodeSettingValues_( &dciSettingF_.presSensitivity,  presSensitivity,  TENTHS);

    // PEEP Setting
    Real32 peepCpap = getApplicableBoundedValueF_(settings, SettingId::PEEP);
    encodeSettingValues_( &dciSettingF_.peepCpap,  peepCpap,  TENTHS);

    // Plateau Time Setting
    Real32 plateauTime = getApplicableBoundedValueF_(settings, SettingId::PLATEAU_TIME);
    encodeSettingValues_( &dciSettingF_.plateauTime,  (plateauTime/ 1000.0F),  TENTHS);

    // Apnea Interval Setting
    Real32 apneaInterval = getApplicableBoundedValueF_(settings, SettingId::APNEA_INTERVAL);
    if( SettingConstants::UPPER_ALARM_LIMIT_OFF == apneaInterval )
	{
		apneaInterval = 0.0;
	}
    encodeSettingValues_( &dciSettingF_.apneaInterval,  (apneaInterval/ 1000.0F),  ONES);

    // Apnea Tidal Volume Setting
    Real32 apneaTidalVolume = getApplicableBoundedValueF_(settings, SettingId::APNEA_TIDAL_VOLUME);
    encodeSettingValues_( &dciSettingF_.apneaTidalVolume,  (apneaTidalVolume/ 1000.0F),  THOUSANDTHS);

    // Apnea Respiratory Setting
    Real32 apneaRespRate = getApplicableBoundedValueF_(settings, SettingId::APNEA_RESP_RATE);
    encodeSettingValues_( &dciSettingF_.apneaRespRate,  apneaRespRate,  TENTHS);

    // Apnea Respiratory Setting
    Real32 apneaPeakFlow = getApplicableBoundedValueF_(settings, SettingId::APNEA_PEAK_INSP_FLOW);
    encodeSettingValues_( &dciSettingF_.apneaPeakFlow,  apneaPeakFlow,  TENTHS);

    // Apnea Oxygen Percent Setting.
    Real32 apneaO2 = getApplicableBoundedValueF_(settings, SettingId::APNEA_O2_PERCENT);
    encodeSettingValues_( &dciSettingF_.apneaO2,  apneaO2,  ONES);

    // Apnea Inspiratory Pressure Setting
    Real32 pcvApneaControlPres = getApplicableBoundedValueF_(settings, SettingId::APNEA_INSP_PRESS);
    encodeSettingValues_( &dciSettingF_.pcvApneaControlPres,  pcvApneaControlPres,  ONES);

    // Apnea Inspiratory Time Setting
    apneaInspTime = 
        getApplicableBoundedValueF_(settings, SettingId::APNEA_INSP_TIME)
        / 1000.0F;      // convert ms to s

    // if Apnea Inspiratory Time Setting is not applicable,
    // use empty strings for Apnea Inspiratory Pressure,
    // Apnea Inspiratory Time, and Apnea IE Ratio.
    if (IsEquivalent(apneaInspTime, (EMPTY_STR_ID/ 1000), TENS)) // $[TI8]
    {
        encodeSettingValues_( &dciSettingF_.pcvApneaControlPres, EMPTY_STR_ID,  TENTHS);
        encodeSettingValues_( &dciSettingF_.pcvApneaInspTime, EMPTY_STR_ID,  HUNDREDTHS);
        encodeSettingValues_( &dciSettingF_.apneaPcvIofIeRatio, EMPTY_STR_ID,  HUNDREDTHS);
        encodeSettingValues_( &dciSettingF_.apneaPcvEofIeRatio, EMPTY_STR_ID,  HUNDREDTHS);
    }
    else 
    {

        ieRatio = settings.getBoundedValue(SettingId::APNEA_IE_RATIO).value;        
        encodeSettingValues_( &dciSettingF_.pcvApneaInspTime, apneaInspTime,  HUNDREDTHS);
        encodeSettingValues_( &dciSettingF_.apneaPcvIofIeRatio, computeIofIeRatio_(ieRatio),  HUNDREDTHS);
        encodeSettingValues_( &dciSettingF_.apneaPcvEofIeRatio, computeEofIeRatio_(ieRatio),  HUNDREDTHS);
    }

    // Apnea Flow Pattern 
    const DiscreteValue  APNEA_FLOW_PATTERN =
        SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
                                              SettingId::APNEA_FLOW_PATTERN);
    // Apnea Mandatory Type
    const DiscreteValue  APNEA_MANDATORY_TYPE =
        SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
                                              SettingId::APNEA_MAND_TYPE);
    switch(APNEA_MANDATORY_TYPE)
    {
        case MandTypeValue::PCV_MAND_TYPE:
            dciSettingF_.apneaMandatoryType.put(PPCStr6_);
            dciSettingF_.apneaFlowPattern.setInvalid();
            break;
        case MandTypeValue::VCV_MAND_TYPE:
            dciSettingF_.apneaMandatoryType.put(PVCStr6_);
            switch ( APNEA_FLOW_PATTERN )
            {
              case FlowPatternValue::SQUARE_FLOW_PATTERN: 
                dciSettingF_.apneaFlowPattern.put(PSquareStr6_);
                break;
              case FlowPatternValue::RAMP_FLOW_PATTERN: 
                dciSettingF_.apneaFlowPattern.put(PRampStr6_);
                break;
              default:    // invalid condition
                AUX_CLASS_ASSERTION_FAILURE( APNEA_FLOW_PATTERN );
                break;
            }
            break;
        default:
            // Invalid Mandatory Type
            AUX_CLASS_ASSERTION_FAILURE( APNEA_MANDATORY_TYPE );
            break;
    }

    // Pressure Support Level Setting
    Real32 supportPres = getApplicableBoundedValueF_(settings, SettingId::PRESS_SUPP_LEVEL);
    encodeSettingValues_( &dciSettingF_.supportPres,  supportPres,  ONES);


    // Flow Pattern 
    if (isDiscreteValueApplicable_(settings, SettingId::FLOW_PATTERN)) 
    {
        flowP = settings.getDiscreteValue(SettingId::FLOW_PATTERN);
        
        switch ( flowP )
        {
            case FlowPatternValue::SQUARE_FLOW_PATTERN: 
                dciSettingF_.waveform.put(PSquareStr6_);
                break;
            case FlowPatternValue::RAMP_FLOW_PATTERN: 
                dciSettingF_.waveform.put(PRampStr6_);
                break;
            default:    // invalid condition
                AUX_CLASS_ASSERTION_FAILURE( flowP );
                break;
        }
    }
    else 
    {
        dciSettingF_.waveform.put(PBlankStr6_);
    }   

	// mark Suction on if either on screen Calibration or off screen O2 key is depressed
	const Boolean ON_OFF = ( ( BdGuiEvent::GetEventStatus(EventData::PERCENT_O2) 
							   == EventData::ACTIVE ) || 
							 ( BdGuiEvent::GetEventStatus(EventData::CALIBRATE_O2) 
							   == EventData::ACTIVE ) );

	// O2 Suction
	encodeOnOff_(  &dciSettingF_.suctionO2, ON_OFF);

    // High Inspiratory Setting
    BoundedValue  highInspSetting = SettingsMgr::GetBoundedSettingPtr(
                              SettingId::HIGH_CCT_PRESS)->getAcceptedValue();
    encodeSettingValues_( &dciSettingF_.highPpeak, 
                         highInspSetting.value, ONES);

    // Low Inspiratory Setting
    Real32 lowInspSettingValue = getApplicableBoundedValueF_(settings, SettingId::LOW_CCT_PRESS);
    encodeSettingValues_(&dciSettingF_.lowPpeak, 
                         lowInspSettingValue, TENTHS);

    // High Exhaled Minute Volume Setting
    BoundedValue  highVeTotSetting = SettingsMgr::GetBoundedSettingPtr(
                              SettingId::HIGH_EXH_MINUTE_VOL)->getAcceptedValue();
    encodeSettingValues_( &dciSettingF_.highVeTot, 
                                    highVeTotSetting.value, THOUSANDTHS);

    // Low Exhaled Minute Volume Setting
    BoundedValue  lowVeTotSetting = SettingsMgr::GetBoundedSettingPtr(
                              SettingId::LOW_EXH_MINUTE_VOL)->getAcceptedValue();
    encodeSettingValues_( &dciSettingF_.lowVeTot, 
                                    lowVeTotSetting.value, THOUSANDTHS);

    // High Mand Exhaled Tidal Volume Setting
    BoundedValue  highVteMandSetting = SettingsMgr::GetBoundedSettingPtr(
                              SettingId::HIGH_EXH_TIDAL_VOL)->getAcceptedValue();
    encodeSettingValues_( &dciSettingF_.highVteMand, 
                                    highVteMandSetting.value, ONES);

    // Low Exhaled Mandatory Tidal Volume Setting
    BoundedValue  lowVteMandSetting = SettingsMgr::GetBoundedSettingPtr(
                              SettingId::LOW_EXH_MAND_TIDAL_VOL)->getAcceptedValue();
    encodeSettingValues_( &dciSettingF_.lowVteMand, 
                                    lowVteMandSetting.value, ONES);

    // High Spont Exhaled Tidal Volume Setting
    BoundedValue  highVteSpontSetting = SettingsMgr::GetBoundedSettingPtr(
                              SettingId::HIGH_EXH_TIDAL_VOL)->getAcceptedValue();
    encodeSettingValues_( &dciSettingF_.highVteSpont, 
                                    highVteSpontSetting.value, ONES);

    // Low Spont Exhaled Tidal Volume Setting
    BoundedValue  lowVteSpontSetting = SettingsMgr::GetBoundedSettingPtr(
                              SettingId::LOW_EXH_SPONT_TIDAL_VOL)->getAcceptedValue();
    encodeSettingValues_( &dciSettingF_.lowVteSpont, 
                                    lowVteSpontSetting.value, ONES);

    // High Respiratory Rate Setting
    BoundedValue  highRespRateSetting = SettingsMgr::GetBoundedSettingPtr(
                              SettingId::HIGH_RESP_RATE)->getAcceptedValue();
    encodeSettingValues_( &dciSettingF_.highFtot, 
                           highRespRateSetting.value, ONES);

    // Flow Sensitivity Setting
    flowSensitivity = 
        getApplicableBoundedValueF_(settings, SettingId::FLOW_SENS);
    if (IsEquivalent(flowSensitivity, EMPTY_STR_ID, TENS)) // $[TI4]
    {
        encodeSettingValues_( &dciSettingF_.baseFlow, 
                               EMPTY_STR_ID, TENTHS);

    }
    else 
    {
		// TODO E600 port
        //encodeSettingValues_( &dciSettingF_.baseFlow, 
        //                       (flowSensitivity + ::BASE_FLOW_MIN), TENTHS);

    }
    encodeSettingValues_( &dciSettingF_.flowSensitivity, 
                           (flowSensitivity ), TENTHS);

    // Inspiratory Pressure Setting.
    Real32 pcvControlPres = getApplicableBoundedValueF_(settings, SettingId::INSP_PRESS);
    encodeSettingValues_(&dciSettingF_.pcvControlPres, 
                         pcvControlPres, ONES);

    
    if (isDiscreteValueApplicable_(settings, SettingId::CONSTANT_PARM)) // $[TI3.1]
    {
        if (ventMode_ == ModeValue::BILEVEL_MODE_VALUE) // $[TI3.1.4]
        {
            // High PEEP Time
            Real32 highPEEPtime = (settings.getBoundedValue(SettingId::PEEP_HIGH_TIME)
                                   / 1000.0F);     // convert ms to s
            encodeSettingValues_(&dciSettingF_.highPEEPtime, 
                                 highPEEPtime, HUNDREDTHS);

            // Low PEEP Time
            Real32 lowPEEPtime = (settings.getBoundedValue(SettingId::PEEP_LOW_TIME)
                                   / 1000.0F);     // convert ms to s
            encodeSettingValues_(&dciSettingF_.lowPEEPtime, 
                                 lowPEEPtime, HUNDREDTHS);

            // PEEPh time to PEEPl time ratio
            ieRatio = settings.getBoundedValue(SettingId::HL_RATIO).value;


            // Send empty strings for Ti & Te.
            encodeSettingValues_( &dciSettingF_.pcvInspTime, 
                                   EMPTY_STR_ID, HUNDREDTHS);
            encodeSettingValues_( &dciSettingF_.Te, 
                                   EMPTY_STR_ID, HUNDREDTHS);


        }
        else 
        {
            // Inspiratory Time
            Real32 pcvInspTime = (settings.getBoundedValue(SettingId::INSP_TIME)
                                 / 1000.0F);     // convert ms to s
            encodeSettingValues_(&dciSettingF_.pcvInspTime, 
                                 pcvInspTime, HUNDREDTHS);
            // Expiratory Time
            Real32 Te = (settings.getBoundedValue(SettingId::EXP_TIME)
                                   / 1000.0F);     // convert ms to s
            encodeSettingValues_( &dciSettingF_.Te, Te, HUNDREDTHS);

            // Inspiratory to Expiratory Ratio
            ieRatio = settings.getBoundedValue(SettingId::IE_RATIO).value;

            // Send empty strings for PEEPh & PEEPl
            encodeSettingValues_( &dciSettingF_.highPEEPtime, 
                                   EMPTY_STR_ID, HUNDREDTHS);
            encodeSettingValues_( &dciSettingF_.lowPEEPtime, 
                                   EMPTY_STR_ID, HUNDREDTHS);

        }

        // IE ratio
        encodeSettingValues_( &dciSettingF_.pcvIofIeRatio, computeIofIeRatio_(ieRatio),  HUNDREDTHS);
        encodeSettingValues_( &dciSettingF_.pcvEofIeRatio, computeEofIeRatio_(ieRatio),  HUNDREDTHS);

        constP = settings.getDiscreteValue(SettingId::CONSTANT_PARM);

        switch ( constP )
        {
            case ConstantParmValue::INSP_TIME_CONSTANT: 
                dciSettingF_.pcvControl.put(PITimeStr6_);
                break;
            case ConstantParmValue::IE_RATIO_CONSTANT: 
                dciSettingF_.pcvControl.put(PIeRatioStr6_);
                break;
            case ConstantParmValue::EXP_TIME_CONSTANT: 
                dciSettingF_.pcvControl.put(PETimeStr6_);
                break;
            default:  // invalid condition
            AUX_CLASS_ASSERTION_FAILURE( constP );
            break;
        }
    }
    else 
    {

       
        encodeSettingValues_( &dciSettingF_.pcvInspTime, 
                               EMPTY_STR_ID, HUNDREDTHS);
        encodeSettingValues_( &dciSettingF_.pcvIofIeRatio, 
                               EMPTY_STR_ID, HUNDREDTHS);
        encodeSettingValues_( &dciSettingF_.pcvEofIeRatio, 
                               EMPTY_STR_ID, HUNDREDTHS);
        dciSettingF_.pcvControl.put(PBlankStr6_);

        encodeSettingValues_( &dciSettingF_.highPEEPtime, 
                               EMPTY_STR_ID, HUNDREDTHS);
        encodeSettingValues_( &dciSettingF_.lowPEEPtime, 
                               EMPTY_STR_ID, HUNDREDTHS);

        encodeSettingValues_( &dciSettingF_.Te, 
                               EMPTY_STR_ID, HUNDREDTHS);

    }   

    // Tube Id Setting
    Real32 tubeId = getApplicableBoundedValueF_(settings, SettingId::TUBE_ID);
    encodeSettingValues_(&dciSettingF_.tubeId,  tubeId, TENTHS);

    // Tube Type Setting
    const DiscreteValue TUBE_TYPE =
        SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,            
                      SettingId::TUBE_TYPE);

    if(isDiscreteValueApplicable_(settings, SettingId::TUBE_TYPE))
    {
        switch (TUBE_TYPE)
        {
            case TubeTypeValue::ET_TUBE_TYPE:
                dciSettingF_.tubeType.put(PETStr6_);
                break;
            case TubeTypeValue::TRACH_TUBE_TYPE:   
                dciSettingF_.tubeType.put(PTRACHStr6_);                       
                break;
            default:
                // Invalid TubeType
                AUX_CLASS_ASSERTION_FAILURE( TUBE_TYPE );
                break;
        }

    }
    else
    {
        dciSettingF_.tubeType.put(PBlankStr6_);

    }

    // Humidification Type Setting
    const DiscreteValue HUMID_TYPE =
    SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
                      SettingId::HUMID_TYPE);


    switch(HUMID_TYPE)
    {
        case HumidTypeValue::NON_HEATED_TUBING_HUMIDIFIER:
            dciSettingF_.humidificationType.put(PNonHeatedExpTubeStr18_);
            break;
        case HumidTypeValue::HEATED_TUBING_HUMIDIFIER:
            dciSettingF_.humidificationType.put(PHeatedExpTubeStr18_);
            break;
        case HumidTypeValue::HME_HUMIDIFIER:
            dciSettingF_.humidificationType.put(PHMEStr18_);
            break;
        default:
            // Invalid Humid Type
            AUX_CLASS_ASSERTION_FAILURE( HUMID_TYPE );
            break;
    }

    // Humidification Volume Setting
    Real32 humidificationVol = (getApplicableBoundedValueF_(settings, SettingId::HUMID_VOLUME)/ 1000);
    encodeSettingValues_(&dciSettingF_.humidificationVol,  humidificationVol, THOUSANDTHS);

    // FIO2 Option Setting
    const DiscreteValue  FIO2_IS_ENABLED =
        SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
                                              SettingId::FIO2_ENABLED);

	previousFIO2_IS_ENABLED = FIO2_IS_ENABLED;

    switch(FIO2_IS_ENABLED)
    {
        case Fio2EnabledValue::NO_FIO2_ENABLED:
            dciSettingF_.O2sensor.put(PO2DisabledStr9_);
            break;
        case Fio2EnabledValue::YES_FIO2_ENABLED:
            dciSettingF_.O2sensor.put(PO2EnabledStr9_);
            break;
		case Fio2EnabledValue::CALIBRATE_FIO2:
			if (Fio2EnabledValue::NO_FIO2_ENABLED == previousFIO2_IS_ENABLED)
			{
				dciSettingF_.O2sensor.put(PO2DisabledStr9_);
			}
			else
			{
				dciSettingF_.O2sensor.put(PO2EnabledStr9_);
			}
			break;
        default:
            // Invalid O2 Setting
            AUX_CLASS_ASSERTION_FAILURE( FIO2_IS_ENABLED );
            break;
    }
	// save the last enable/disable setting
	if (FIO2_IS_ENABLED != Fio2EnabledValue::CALIBRATE_FIO2)
	{
		previousFIO2_IS_ENABLED = FIO2_IS_ENABLED;
	}

    // D Sens Setting
    Real32 DSens = getApplicableBoundedValueF_(settings, SettingId::DISCO_SENS);
    encodeSettingValues_( &dciSettingF_.DSens, DSens, ONES);

    // Rise Time Percent Setting
    Real32 riseTimePercent = getApplicableBoundedValueF_(settings, SettingId::FLOW_ACCEL_PERCENT);
    encodeSettingValues_( &dciSettingF_.riseTimePercent, riseTimePercent, ONES);

    // Percent Support Setting
    Real32 percentSupport = getApplicableBoundedValueF_(settings, SettingId::PERCENT_SUPPORT);
    encodeSettingValues_( &dciSettingF_.percentSupport, percentSupport, ONES);

    // E Sens Setting
    Real32 eSens = getApplicableBoundedValueF_(settings, SettingId::EXP_SENS);
    encodeSettingValues_( &dciSettingF_.eSens, eSens, TENTHS);
 
    // IBW Setting
    Real32 IBW = getApplicableBoundedValueF_(settings, SettingId::IBW);
    encodeSettingValues_( &dciSettingF_.ibw, IBW, TENTHS);

    // High Inspiratory Tidal Volume Setting
    Real32 highVti = getApplicableBoundedValueF_(settings, SettingId::HIGH_INSP_TIDAL_VOL);
    encodeSettingValues_( &dciSettingF_.highVti, highVti, ONES);

    // Volume Support Setting
    Real32 volumeSupport = (getApplicableBoundedValueF_(settings, SettingId::VOLUME_SUPPORT)/1000);
    encodeSettingValues_( &dciSettingF_.volumeSupport, volumeSupport, THOUSANDTHS);

    // PEEP High Setting
    Real32 PEEPh = getApplicableBoundedValueF_(settings, SettingId::PEEP_HIGH);
    encodeSettingValues_( &dciSettingF_.PEEPh, PEEPh, TENTHS);

    // PEEP Low Setting
    Real32 PEEPl = getApplicableBoundedValueF_(settings, SettingId::PEEP_LOW);
    encodeSettingValues_( &dciSettingF_.PEEPl, PEEPl, TENTHS);

    // High Spont Inspiratory Time Limit
    Real32 highSpontInspTimeLimit = (getApplicableBoundedValueF_(settings, SettingId::HIGH_SPONT_INSP_TIME) /1000);
    encodeSettingValues_( &dciSettingF_.highSpontInspTimeLimit, highSpontInspTimeLimit, TENTHS);

    const DiscreteValue  CIRCUIT_TYPE = 
            SettingContextHandle::GetSettingValue(
                                    ContextId::ACCEPTED_CONTEXT_ID,
                                    SettingId::PATIENT_CCT_TYPE);
    // Circuit Type
    switch (CIRCUIT_TYPE)
    {
        case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
            dciSettingF_.circuitType.put(PpediatricCircuitStr9_);
            break;
        case PatientCctTypeValue::ADULT_CIRCUIT :
            dciSettingF_.circuitType.put(PadultCircuitStr9_);
            break;
        case PatientCctTypeValue::NEONATAL_CIRCUIT :
            dciSettingF_.circuitType.put(PneonatalCircuitStr9_);
            break;
        default:
            // Invalid CIRCUIT_TYPE Setting
            AUX_CLASS_ASSERTION_FAILURE( CIRCUIT_TYPE );
            break;

    }

	if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP))
	{
		// Leak Compensation Enabled/Disabled Setting
		const DiscreteValue  LEAK_COMP_IS_ENABLED =
			SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
												  SettingId::LEAK_COMP_ENABLED);
		switch (LEAK_COMP_IS_ENABLED)
		{
		case LeakCompEnabledValue::LEAK_COMP_DISABLED:
			dciSettingF_.leakState.put(POffStr6_);
			break;
		case LeakCompEnabledValue::LEAK_COMP_ENABLED:
			dciSettingF_.leakState.put(POnStr6_);
			break;
		default:
			// Invalid Leak Compensation Setting
			AUX_CLASS_ASSERTION_FAILURE( LEAK_COMP_IS_ENABLED );
			break;
		}
	}
	else
	{
		dciSettingF_.leakState.setInvalid();
	}


	if( LowerSubScreenArea::GetProxSetupSubScreen()->isProxInstalled() )
	{
	// [PX04001] Prox ON/OFF Setting
	const DiscreteValue  PROX_ENABLED_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::PROX_ENABLED);
	switch (PROX_ENABLED_VALUE)
	{
		case ProxEnabledValue::PROX_ENABLED:
            dciSettingF_.proxState.put(POnStr6_);
			break;
		case ProxEnabledValue::PROX_DISABLED:
			dciSettingF_.proxState.put(POffStr6_);
			break;
		default:
			// Invalid PROX Enable Setting
			AUX_CLASS_ASSERTION_FAILURE( PROX_ENABLED_VALUE );
			break;
	}
}
	else
	{
		dciSettingF_.proxState.put(PBlankStr6_);
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  encodeSettingValues_
//
//@ Interface-Description
// Pass a setting's value, presicion and return the appropriate
// null-terminated 6-character encoding of the value.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the value equals to SettingConstants::LOWER_ALARM_LIMIT_OFF
// or SettingConstants::UPPER_ALARM_LIMIT_OFF return "OFF   ".  if
// value equals to EMPTY_STR_ID then return 6 null-terminated empty spaces,
// else return the value in Thousands, hundreds, tens, ones, tenths,
// or hundreds given by the third parameter.
//---------------------------------------------------------------------
//@ PreCondition
//  pDciString is not equal to NULL
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

char *
Dci::encodeSettingValues_(DciString * pDciString, const Real32 value, const Precision precision)
{
    
    CLASS_PRE_CONDITION(pDciString != NULL);

    static  char    buffer[7];

    // Replace the Setting's value to "OFF" if it is an UPPER or LOWER ALARM LIMIT.
    if( IsEquivalent(value, SettingConstants::UPPER_ALARM_LIMIT_OFF, TENS)  ||
        IsEquivalent(value, SettingConstants::LOWER_ALARM_LIMIT_OFF, TENS))
    {
        pDciString->put(POffStr6_);
    }
    // Replace the Setting's Value to Blank Strings if it is an Empty String Id.
    else if( IsEquivalent(value, EMPTY_STR_ID, TENS) ||
             IsEquivalent(value, (EMPTY_STR_ID / 1000), TENS)   )
    {
        pDciString->put(PBlankStr6_);
    }
    else
    {

        // Set the precision
        switch ( precision )
        {

            case THOUSANDS:                
                sprintf(buffer,"%-6d", (Int32)PreciseValue(value, THOUSANDS));
                break;
            
            case HUNDREDS:                
                sprintf(buffer,"%-6d", (Int32)PreciseValue(value, HUNDREDS));
                break;

            case TENS:                
                sprintf(buffer,"%-6d", (Int32)PreciseValue(value, TENS));
                break;

            case ONES:        
                sprintf(buffer,"%-6d", (Int32)value);
                break;
    
            case TENTHS:    
                sprintf(buffer,"%-6.1f", value);
                break;
            case HUNDREDTHS:       
                sprintf(buffer,"%-6.2f", value);
                break;  
            case THOUSANDTHS:       
                sprintf(buffer,"%-6.3f", value);
                break;  
            default:    // invalid condition
                AUX_CLASS_ASSERTION_FAILURE( precision );
                break;
    
        }
        pDciString->put(buffer);    

    }
    pDciString->get(buffer);
    return(buffer);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  encodeOnOff_
//
//@ Interface-Description
// Accepts a boolean on/off value indicator and return the appropriate
// null-terminated 6-character encoding of it ("ON    " or "OFF   ").
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported or not valid are indicated as such so that they
// can be substituted with blanks during output.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

char *
Dci::encodeOnOff_( DciString * pDciString, const Boolean indicatorOn )
{
	static	char	buffer[7];

	if ( indicatorOn )
	{	
		// $[TI1.1]
		pDciString->put(POnStr6_);
	}
	else
	{
		// $[TI1.2]
		pDciString->put(POffStr6_);
	}

	pDciString->get(buffer);
	return(buffer);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  computeIofIeRatio_
//
//@ Interface-Description
// Pass a Sigma inhalation/exhalation ratio and return the inhalation
// portion of the ratio.
//---------------------------------------------------------------------
//@ Implementation-Description
// A Sigma I:E ratio is a signed floating-point value.  The sign indicates
// with part of the value is assumed to be 1 and follows these relationships:
//
//	if ieRatio  < 0 (ie: negative), then I = 1 and E = | ieRatio |
//	if ieRatio  > 0 (ie: positive), then E = 1 and I = ieRatio
//	if ieRatio == 0 it is invalid
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

Real32
Dci::computeIofIeRatio_(const Real32 ieRatio)
{
	Real32	iPortion = 1.0;

	if (ieRatio > 0.0)
	{						// $[TI1.1]
		iPortion = ieRatio;
	}
							// $[TI1.2]
	return(iPortion);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  computeEofIeRatio_
//
//@ Interface-Description
// Pass a Sigma inhalation/exhalation ratio and return the exhalation
// portion of the ratio.
//---------------------------------------------------------------------
//@ Implementation-Description
// A Sigma I:E ratio is a signed floating-point value.  The sign indicates
// with part of the value is assumed to be 1 and follows these relationships:
//
//	if ieRatio  < 0 (ie: negative), then I = 1 and E = | ieRatio |
//	if ieRatio  > 0 (ie: positive), then E = 1 and I = ieRatio
//	if ieRatio == 0 it is invalid
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

Real32
Dci::computeEofIeRatio_(const Real32 ieRatio)
{
	Real32	ePortion = 1.0;

	if (ieRatio < 0.0)
	{						// $[TI1.1]
		ePortion = (-ieRatio);
	}
							// $[TI1.2]
	return(ePortion);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  correctedEofIeRatio_
//
//@ Interface-Description
//  Returns the E part of an I:E ratio with I set to 1. For I<E, returns
//  E > 1.0. For I>E, returns E as the inverse of I.
//---------------------------------------------------------------------
//@ Implementation-Description
// A Sigma I:E ratio is a signed floating-point value.  The sign indicates
// with part of the value is assumed to be 1 and follows these relationships:
//
//	if ieRatio  < 0 (ie: negative), then I = 1 and E = | ieRatio |
//	if ieRatio  > 0 (ie: positive), then E = 1 and I = ieRatio
//	if ieRatio == 0 it is invalid
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

Real32
Dci::correctedEofIeRatio_(const Real32 ieRatio)
{
	Real32  ePortion = computeEofIeRatio_( ieRatio );

	if (ePortion == 1.0)
	{						
		// $[TI1.1]
		ePortion = 1.0 / computeIofIeRatio_( ieRatio );
	}
	// $[TI1.2]

	return(ePortion);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getPatientData_
//
//@ Interface-Description
// Accumulate PatientData into global data structure, dciPatient_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getPatientData_(void)
{
    // Get Circuit Type
    const DiscreteValue  PATIENT_CCT_TYPE = 
        SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
                                              SettingId::PATIENT_CCT_TYPE);

	BreathDatumHandle  respRate(PatientDataId::TOTAL_RESP_RATE_ITEM);
	BreathDatumHandle  tidalVolume(PatientDataId::EXH_TIDAL_VOL_ITEM);
	BreathDatumHandle  minuteVolume(PatientDataId::EXH_MINUTE_VOL_ITEM);
	BreathDatumHandle  spontMinuteVolume(PatientDataId::EXH_SPONT_MINUTE_VOL_ITEM);
	BreathDatumHandle  peakAirwayPres(PatientDataId::PEAK_CCT_PRESS_ITEM);
	BreathDatumHandle  meanAirwayPres(PatientDataId::MEAN_CCT_PRESS_ITEM);
	BreathDatumHandle  plateauPres(PatientDataId::END_INSP_PRESS_ITEM);
	BreathDatumHandle  ieRatio(PatientDataId::IE_RATIO_ITEM);

    // Insp Pause
    BreathDatumHandle  Cstat(PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM);
    BreathDatumHandle  Rstat(PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM);

    // RM
    BreathDatumHandle  Cdyn(PatientDataId::DYNAMIC_COMPLIANCE_ITEM);
    BreathDatumHandle  Rdyn(PatientDataId::DYNAMIC_RESISTANCE_ITEM);
    BreathDatumHandle  PSF(PatientDataId::PEAK_SPONT_INSP_FLOW_ITEM);
    BreathDatumHandle  NIF(PatientDataId::NIF_PRESSURE_ITEM);
    BreathDatumHandle  VC(PatientDataId::VITAL_CAPACITY_ITEM);

	BoundedBreathDatum datum;

	datum = respRate.getBoundedValue();
	dciPatient_.respRate.put(datum.data.value);

	datum = tidalVolume.getBoundedValue();
	dciPatient_.tidalVolume.put(Real32(datum.data.value / 1000.0)); // ml to l

	datum = minuteVolume.getBoundedValue();
	dciPatient_.minuteVolume.put(datum.data.value);

	datum = spontMinuteVolume.getBoundedValue();
	dciPatient_.spontMinuteVolume.put(datum.data.value);

	datum = peakAirwayPres.getBoundedValue();
	dciPatient_.peakAirwayPres.put(datum.data.value);

	datum = meanAirwayPres.getBoundedValue();
	dciPatient_.meanAirwayPres.put(datum.data.value);

	datum = plateauPres.getBoundedValue();
	dciPatient_.plateauPres.put(datum.data.value);

	dciPatient_.o2Saturation.setNotInThisRelease();
	dciPatient_.pulseRate.setNotInThisRelease();

	datum = ieRatio.getBoundedValue();
	dciPatient_.ePortion.put(correctedEofIeRatio_(datum.data.value));
	dciPatient_.ieRatio.put(datum.data.value);

    // Insp Pause

	// Cstat
    datum = Cstat.getBoundedValue();
	const Int32 complianceFlag = BreathDatumHandle(PatientDataId::
								   STATIC_LUNG_COMPLIANCE_VALID_ITEM).
								   getDiscreteValue().data;
	switch (complianceFlag)
	{
	    case PauseTypes::VALID:                        
	    case PauseTypes::OUT_OF_RANGE:
	    case PauseTypes::SUB_THRESHOLD_INPUT:
	    case PauseTypes::CORRUPTED_MEASUREMENT:
	    case PauseTypes::EXH_TOO_SHORT:
	    case PauseTypes::NOT_STABLE:   
			dciPatient_.Cstat.put(datum.data.value);
	        break;
		case PauseTypes::UNAVAILABLE:  // data can not be calculated - "***" and "()" is displayed    
		case PauseTypes::NOT_REQUIRED: // data is not required for this case - display dash            
            dciPatient_.Cstat.setInvalid();           
			break;

	}

	// Rstat
    datum = Rstat.getBoundedValue();

	const Int32 resistanceFlag = BreathDatumHandle(PatientDataId::
								   STATIC_AIRWAY_RESISTANCE_VALID_ITEM).
								   getDiscreteValue().data;
	switch (resistanceFlag)
	{
	    case PauseTypes::VALID:                        
	    case PauseTypes::OUT_OF_RANGE:
	    case PauseTypes::SUB_THRESHOLD_INPUT:
	    case PauseTypes::CORRUPTED_MEASUREMENT:
	    case PauseTypes::EXH_TOO_SHORT:
	    case PauseTypes::NOT_STABLE:   
			dciPatient_.Rstat.put(datum.data.value);
	        break;
		case PauseTypes::UNAVAILABLE:  // data can not be calculated - "***" and "()" is displayed    
		case PauseTypes::NOT_REQUIRED: // data is not required for this case - display dash            
            dciPatient_.Rstat.setInvalid();           
			break;
	}


    dciPatient_.Cdyn.setInvalid();
    dciPatient_.Rdyn.setInvalid();
    dciPatient_.PSF.setInvalid();
    dciPatient_.NIF.setInvalid();
    dciPatient_.VC.setInvalid();

    // if RM is enabled, update the RM data.
    if(SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH))
    {
		Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE);
		const DiscreteValue  VENT_TYPE = pSetting->getAcceptedValue();
		pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);
		const DiscreteValue  MODE = pSetting->getAcceptedValue();

		if (VENT_TYPE != VentTypeValue::NIV_VENT_TYPE && MODE != ModeValue::BILEVEL_MODE_VALUE)
		{
			datum = Cdyn.getBoundedValue();
			dciPatient_.Cdyn.put(Real32(datum.data.value));
	
			datum = Rdyn.getBoundedValue();
			dciPatient_.Rdyn.put(Real32(datum.data.value));
	
			datum = PSF.getBoundedValue();
			dciPatient_.PSF.put(Real32(datum.data.value));
	
			if (PATIENT_CCT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT)
			{
				datum = NIF.getBoundedValue();
				dciPatient_.NIF.put(Real32(datum.data.value));

				datum = VC.getBoundedValue();
				dciPatient_.VC.put(Real32(datum.data.value/ 1000.0 ) ); // ml to l

			}
		}
    }
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getPatientDataF_
//
//@ Interface-Description
// Accumulate PatientData into global data structure, dciPatientF_
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getPatientDataF_(void)
{

    BatchSettingValues settings;
    NovRamManager::GetAcceptedBatchSettings(settings);

    BreathDatumHandle  respRate(PatientDataId::TOTAL_RESP_RATE_ITEM);
    BreathDatumHandle  tidalVolume(PatientDataId::EXH_TIDAL_VOL_ITEM);
    BreathDatumHandle  minuteVolume(PatientDataId::EXH_MINUTE_VOL_ITEM);
    BreathDatumHandle  peakAirwayPres(PatientDataId::PEAK_CCT_PRESS_ITEM);
    BreathDatumHandle  meanAirwayPres(PatientDataId::MEAN_CCT_PRESS_ITEM);
    BreathDatumHandle  plateauPres(PatientDataId::END_INSP_PRESS_ITEM);
    BreathDatumHandle  ieRatio(PatientDataId::IE_RATIO_ITEM);
    BreathDatumHandle  deliveredO2(PatientDataId::DELIVERED_O2_PERCENTAGE_ITEM);
    BreathDatumHandle  Vti(PatientDataId::INSP_TIDAL_VOL_ITEM);
    BreathDatumHandle  PEEPi(PatientDataId::PAV_PEEP_INTRINSIC_ITEM);
    BreathDatumHandle  Rtot(PatientDataId::TOTAL_AIRWAY_RESISTANCE_ITEM);
    BreathDatumHandle  spontInspPercent(PatientDataId::SPONT_PERCENT_TI_ITEM);
    BreathDatumHandle  endExhPress(PatientDataId::END_EXP_PRESS_ITEM);
    BreathDatumHandle  Vte(PatientDataId::EXH_TIDAL_VOL_ITEM);
    BreathDatumHandle  VeTot(PatientDataId::EXH_MINUTE_VOL_ITEM);
    BreathDatumHandle  TiSpont(PatientDataId::SPONT_INSP_TIME_ITEM);
    BreathDatumHandle  VeSpont(PatientDataId::EXH_SPONT_MINUTE_VOL_ITEM);
	BreathDatumHandle  VteSpont(PatientDataId::EXH_SPONT_TIDAL_VOL_ITEM);
	BreathDatumHandle  WOBtot(PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM);

    BreathDatumHandle  Rpav(PatientDataId::PAV_PATIENT_RESISTANCE_ITEM);
    BreathDatumHandle  Epav(PatientDataId::PAV_LUNG_ELASTANCE_ITEM);
    BreathDatumHandle  Cpav(PatientDataId::PAV_LUNG_COMPLIANCE_ITEM);
    BreathDatumHandle  rapShadowIndx(PatientDataId::SPONT_RATE_TO_VOLUME_RATIO_ITEM);

    // Exp Pause
    BreathDatumHandle  PEEP_I(PatientDataId::PEEP_INTRINSIC_ITEM);
    BreathDatumHandle  PEEP_Tot(PatientDataId::PEEP_TOTAL_ITEM);

    // Insp Pause
    BreathDatumHandle  Cstat(PatientDataId::STATIC_LUNG_COMPLIANCE_ITEM);
    BreathDatumHandle  Rstat(PatientDataId::STATIC_AIRWAY_RESISTANCE_ITEM);
    BreathDatumHandle  Ppl(PatientDataId::PLATEAU_PRESSURE_ITEM);

    // RM Patient Data
    BreathDatumHandle  Cdyn(PatientDataId::DYNAMIC_COMPLIANCE_ITEM);
    BreathDatumHandle  Rdyn(PatientDataId::DYNAMIC_RESISTANCE_ITEM);
    BreathDatumHandle  PSF(PatientDataId::PEAK_SPONT_INSP_FLOW_ITEM);
    BreathDatumHandle  PEF(PatientDataId::PEAK_EXPIRATORY_FLOW_ITEM);
    BreathDatumHandle  EEF(PatientDataId::END_EXPIRATORY_FLOW_ITEM);
    BreathDatumHandle  NIF(PatientDataId::NIF_PRESSURE_ITEM);
    BreathDatumHandle  P100(PatientDataId::P100_PRESSURE_ITEM);
    BreathDatumHandle  VC(PatientDataId::VITAL_CAPACITY_ITEM);
    BreathDatumHandle  percentLeak(PatientDataId::PERCENT_LEAK_ITEM);
    BreathDatumHandle  leak(PatientDataId::EXH_LEAK_RATE_ITEM);
    BreathDatumHandle  iLeak(PatientDataId::INSP_LEAK_VOL_ITEM);


    BoundedBreathDatum datum;


    // Ppeak
    datum = peakAirwayPres.getBoundedValue();
    dciPatientF_.peakAirwayPres.put(datum.data.value);

    // Pmean
    datum = meanAirwayPres.getBoundedValue();
    dciPatientF_.meanAirwayPres.put(datum.data.value);

    // Ie Ratio
    datum = ieRatio.getBoundedValue();
    dciPatientF_.ePortion.put(correctedEofIeRatio_(datum.data.value));
    dciPatientF_.ieRatio.put(datum.data.value);

    // Ftot
    datum = respRate.getBoundedValue();
    dciPatientF_.respRate.put(datum.data.value);

    // Vte
    datum = Vte.getBoundedValue();
    dciPatientF_.Vte.put(Real32(datum.data.value/1000));

    // VeTot
    datum = VeTot.getBoundedValue();
    dciPatientF_.VeTot.put(datum.data.value);

    // O2
    datum = deliveredO2.getBoundedValue();
    const DiscreteValue  FIO2_IS_ENABLED =
        SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
                                              SettingId::FIO2_ENABLED);

    // FIO2 
    if(datum.data.value >= 0.0F && FIO2_IS_ENABLED == Fio2EnabledValue::YES_FIO2_ENABLED )
    {
        dciPatientF_.deliveredO2.put(datum.data.value);
    }
    else
    {
        dciPatientF_.deliveredO2.setInvalid();
    }

    // PEEP
    datum = endExhPress.getBoundedValue();
    dciPatientF_.endExhPress.put(Real32(datum.data.value));


    // Pi end
    datum = plateauPres.getBoundedValue();         
    dciPatientF_.plateauPres.put(datum.data.value);

    // Get Current Vent Type
    const DiscreteValue VENT_TYPE = 
        SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAcceptedValue();

    // Get Current Mode
    ventMode_ = (ModeValue::ModeValueId)settings.getDiscreteValue(SettingId::MODE);

    // Get Current Mandatory Type
    const DiscreteValue MANDATORY_TYPE =
        SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
                                              SettingId::MAND_TYPE);

    // Get Current Spontaneous Type
    const DiscreteValue SPONT_TYPE = 
        SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE)->getAcceptedValue();


        // Get Circuit Type
    const DiscreteValue  PATIENT_CCT_TYPE = 
        SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
                                              SettingId::PATIENT_CCT_TYPE);

    // Exp Pause
    datum = PEEP_I.getBoundedValue();
    dciPatientF_.PEEP_I.put(datum.data.value);

    datum = PEEP_Tot.getBoundedValue();
    dciPatientF_.PEEP_Tot.put(datum.data.value);

    // Insp Pause

	// Cstat
    datum = Cstat.getBoundedValue();
	const Int32 complianceFlag = BreathDatumHandle(PatientDataId::
								   STATIC_LUNG_COMPLIANCE_VALID_ITEM).
								   getDiscreteValue().data;
	switch (complianceFlag)
	{
	    case PauseTypes::VALID:                        
	    case PauseTypes::OUT_OF_RANGE:
	    case PauseTypes::SUB_THRESHOLD_INPUT:
	    case PauseTypes::CORRUPTED_MEASUREMENT:
	    case PauseTypes::EXH_TOO_SHORT:
	    case PauseTypes::NOT_STABLE:   
			dciPatientF_.Cstat.put(datum.data.value);
	        break;
		case PauseTypes::UNAVAILABLE:  // data can not be calculated - "***" and "()" is displayed    
		case PauseTypes::NOT_REQUIRED: // data is not required for this case - display dash            
            dciPatientF_.Cstat.setInvalid();           
			break;

	}

    // Rstat
    datum = Rstat.getBoundedValue();
	const Int32 resistanceFlag = BreathDatumHandle(PatientDataId::
								   STATIC_AIRWAY_RESISTANCE_VALID_ITEM).
								   getDiscreteValue().data;
	switch (resistanceFlag)
	{
	    case PauseTypes::VALID:                        
	    case PauseTypes::OUT_OF_RANGE:
	    case PauseTypes::SUB_THRESHOLD_INPUT:
	    case PauseTypes::CORRUPTED_MEASUREMENT:
	    case PauseTypes::EXH_TOO_SHORT:
	    case PauseTypes::NOT_STABLE:   
			dciPatientF_.Rstat.put(datum.data.value);
	        break;
		case PauseTypes::UNAVAILABLE:  // data can not be calculated - "***" and "()" is displayed    
		case PauseTypes::NOT_REQUIRED: // data is not required for this case - display dash            
            dciPatientF_.Rstat.setInvalid();           
			break;
	}

    datum = Ppl.getBoundedValue();
    dciPatientF_.Ppl.put(datum.data.value);


    // High Ti Spont
    dciPatientF_.highTiSpont.setInvalid();


    // PEEPi
    dciPatientF_.PEEPi.setInvalid();

	// WOBtot
    dciPatientF_.WOBtot.setInvalid();

    // Rtot
    dciPatientF_.Rtot.setInvalid();

    // Rpav
    dciPatientF_.Rpav.setInvalid();

    // Epav
    dciPatientF_.Epav.setInvalid();

    // Cpav
    dciPatientF_.Cpav.setInvalid();

    // F/Vt/Kg
    dciPatientF_.normRapShadowIndx.setInvalid();

    // f/Vt
    dciPatientF_.rapShadowIndx.setInvalid();

    // Ti/Ttot
    dciPatientF_.spontInspPercent.setInvalid();

    // Ti Spont
    dciPatientF_.TiSpont.setInvalid();

    // Ve Spont
    dciPatientF_.VeSpont.setInvalid();

	// Vte Spont
    dciPatientF_.VteSpont.setInvalid();

    // Cdyn
    dciPatientF_.Cdyn.setInvalid();

    // Rdyn
    dciPatientF_.Rdyn.setInvalid();

    // PSF
    dciPatientF_.PSF.setInvalid();

    // PEF
    dciPatientF_.PEF.setInvalid();

    // EEF
    dciPatientF_.EEF.setInvalid();

    // NIF
    dciPatientF_.NIF.setInvalid();

    // P100
    dciPatientF_.P100.setInvalid();

    // VC
    dciPatientF_.VC.setInvalid();

    // Vti
    datum = Vti.getBoundedValue();
    dciPatientF_.Vti.put(Real32(datum.data.value / 1000.0));   // ml to l

    switch(VENT_TYPE)
    {
        case VentTypeValue::NIV_VENT_TYPE:
            // NIV settings only

            switch(ventMode_)
            {
                case ModeValue::AC_MODE_VALUE:  // $[TI1.1]
                    switch(MANDATORY_TYPE)
                    {
                        case MandTypeValue::PCV_MAND_TYPE:
                            break;
                        case MandTypeValue::VCV_MAND_TYPE:
                            // Vti
                            dciPatientF_.Vti.setInvalid();           
                            break;

                        default:
                            // Invalid Mandatory Type
                            AUX_CLASS_ASSERTION_FAILURE( MANDATORY_TYPE );
                            break;

                    }
                    break;
                case ModeValue::SIMV_MODE_VALUE: // $[TI1.2]

                    // High Ti Spont
                    if(isNivInspTooLong_)
                    {
                        dciPatientF_.highTiSpont.put(PAlertStr6_);

                    }
                    else
                    {
                        dciPatientF_.highTiSpont.setInvalid();

                    }

                    // TiSpont
                    datum = TiSpont.getBoundedValue();
                    dciPatientF_.TiSpont.put(Real32(datum.data.value));

                    // VeSpont
                    datum = VeSpont.getBoundedValue();
                    dciPatientF_.VeSpont.put(Real32(datum.data.value));

					// VteSpont
					datum = VteSpont.getBoundedValue();
                    dciPatientF_.VteSpont.put(Real32(datum.data.value/1000.0f));

                    switch(MANDATORY_TYPE)
                    {


                        case MandTypeValue::PCV_MAND_TYPE:         
                            break;
                        case MandTypeValue::VCV_MAND_TYPE:
                            break;
                        default:
                            // Invalid Mandatory Type
                            AUX_CLASS_ASSERTION_FAILURE( MANDATORY_TYPE );
                            break;
                    }
                    

                    switch(SPONT_TYPE)
                    {
    
                        case SupportTypeValue::OFF_SUPPORT_TYPE:
                            break;    
                        case SupportTypeValue::PSV_SUPPORT_TYPE:
                            break;
                        default:
                            // Invalid spont Type for SIMV.
                            AUX_CLASS_ASSERTION_FAILURE( SPONT_TYPE );
                            break;
                    }
                    break;
                case ModeValue::SPONT_MODE_VALUE: // $[TI1.3]
                case ModeValue::CPAP_MODE_VALUE:

                    // Ti Spont
                    datum = TiSpont.getBoundedValue();
                    dciPatientF_.TiSpont.put(datum.data.value);

                    // Ve Spont
                    datum = VeSpont.getBoundedValue();
                    dciPatientF_.VeSpont.put(datum.data.value);

					// Vte Spont
                    datum = VteSpont.getBoundedValue();
                    dciPatientF_.VteSpont.put(Real32(datum.data.value/1000.0f));

                    // Ti/Ttot
                    datum = spontInspPercent.getBoundedValue();
                    dciPatientF_.spontInspPercent.put(datum.data.value);

                    // f/Vt
                    datum = rapShadowIndx.getBoundedValue();
                    dciPatientF_.rapShadowIndx.put(datum.data.value);

                    // High Ti Spont
                    if(isNivInspTooLong_)
                    {
                        dciPatientF_.highTiSpont.put(PAlertStr6_);

                    }
                    else
                    {
                        dciPatientF_.highTiSpont.setInvalid();

                    }

                    switch(MANDATORY_TYPE)
                    {
                        case MandTypeValue::PCV_MAND_TYPE: 
                            break;
                        case MandTypeValue::VCV_MAND_TYPE:
                            break;
                        default:
                            // Invalid Mandatory Type
                            AUX_CLASS_ASSERTION_FAILURE( MANDATORY_TYPE );
                            break;
                    }
                   
    
                    switch(SPONT_TYPE)
                    {
    
                        case SupportTypeValue::OFF_SUPPORT_TYPE:
                            break;    
                        case SupportTypeValue::PSV_SUPPORT_TYPE:
                            break;
                        default:
                            // Invalid spont Type for Spont Mode.
                            AUX_CLASS_ASSERTION_FAILURE( SPONT_TYPE );
                            break;
                    }
                    break;
                default: // invalid condition
                    AUX_CLASS_ASSERTION_FAILURE( ventMode_ );
                    break;
            }
            break;
    
        case VentTypeValue::INVASIVE_VENT_TYPE:
            // INVASIVE settings only

            switch(ventMode_)
            {
                case ModeValue::AC_MODE_VALUE:  // $[TI1.1]
                    // if RESPM is enabled, update the RM data.
                    if(SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH))
                    {
                        // Cdyn
                        datum = Cdyn.getBoundedValue();
                        dciPatientF_.Cdyn.put(Real32(datum.data.value));
            
                        // Rdyn
                        datum = Rdyn.getBoundedValue();
                        dciPatientF_.Rdyn.put(Real32(datum.data.value));
            
                        // PEF
                        datum = PEF.getBoundedValue();
                        dciPatientF_.PEF.put(Real32(datum.data.value));
            
                        // EEF
                        datum = EEF.getBoundedValue();
                        dciPatientF_.EEF.put(Real32(datum.data.value));
            
    
                        if(PATIENT_CCT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT)
                        {
    
                            // NIF
                            datum = NIF.getBoundedValue();
                            dciPatientF_.NIF.put(Real32(datum.data.value));
                
                            // P100
                            datum = P100.getBoundedValue();
                            dciPatientF_.P100.put(Real32(datum.data.value));
                
                            // VC
                            datum = VC.getBoundedValue(); 
                            dciPatientF_.VC.put(Real32(datum.data.value/ 1000.0)); // ml to l
                        }
                    }

                    switch(MANDATORY_TYPE)
                    {

                        case MandTypeValue::PCV_MAND_TYPE:
                            break;
                        case MandTypeValue::VCV_MAND_TYPE:
                            // Vti
                            dciPatientF_.Vti.setInvalid();           
                            break;
                        case MandTypeValue::VCP_MAND_TYPE:
                            break;
                        default:
                            // Invalid Mandatory Type
                            AUX_CLASS_ASSERTION_FAILURE( MANDATORY_TYPE );
                            break;

                    }               
                    break;
                case ModeValue::SIMV_MODE_VALUE: // $[TI1.2]
                    // if RESPM is enabled, update the RM data.
                    if(SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH))
                    {
        
                        // Cdyn
                        datum = Cdyn.getBoundedValue();
                        dciPatientF_.Cdyn.put(Real32(datum.data.value));
            
                        // Rdyn
                        datum = Rdyn.getBoundedValue();
                        dciPatientF_.Rdyn.put(Real32(datum.data.value));
            
                        // PSF
                        datum = PSF.getBoundedValue();
                        dciPatientF_.PSF.put(Real32(datum.data.value));
            
                        // PEF
                        datum = PEF.getBoundedValue();
                        dciPatientF_.PEF.put(Real32(datum.data.value));
            
                        // EEF
                        datum = EEF.getBoundedValue();
                        dciPatientF_.EEF.put(Real32(datum.data.value));
            
                        if(PATIENT_CCT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT)
                        {

                            // NIF
                            datum = NIF.getBoundedValue();
                            dciPatientF_.NIF.put(Real32(datum.data.value));
    
                            // P100
                            datum = P100.getBoundedValue();
                            dciPatientF_.P100.put(Real32(datum.data.value));
    
                            // VC
                            datum = VC.getBoundedValue();
                            dciPatientF_.VC.put(Real32(datum.data.value/ 1000.0)); // ml to l
                        }

                    }
                    // TiSpont
                    datum = TiSpont.getBoundedValue();
                    dciPatientF_.TiSpont.put(Real32(datum.data.value));

                    // VeSpont
                    datum = VeSpont.getBoundedValue();
                    dciPatientF_.VeSpont.put(Real32(datum.data.value));

					// VteSpont
                    datum = VteSpont.getBoundedValue();
                    dciPatientF_.VteSpont.put(Real32(datum.data.value/1000.0f));

                    switch(MANDATORY_TYPE)
                    {
                        case MandTypeValue::PCV_MAND_TYPE:

                            break;
                        case MandTypeValue::VCV_MAND_TYPE:

                            break;
                        case MandTypeValue::VCP_MAND_TYPE:
                            break;
                        default:
                            // Invalid Mandatory Type
                            AUX_CLASS_ASSERTION_FAILURE( MANDATORY_TYPE );
                            break;
                    }
    
    
                    switch(SPONT_TYPE)
                    {
    
                        case SupportTypeValue::OFF_SUPPORT_TYPE:
                            break;    
                        case SupportTypeValue::PSV_SUPPORT_TYPE:
                            break;
                        case SupportTypeValue::ATC_SUPPORT_TYPE:
                            break;
                        default:
                            // Invalid spont Type for SIMV.
                            AUX_CLASS_ASSERTION_FAILURE( SPONT_TYPE );
                            break;
                    }
                    break;
                case ModeValue::SPONT_MODE_VALUE: // $[TI1.3]
                case ModeValue::CPAP_MODE_VALUE:
                    // if RESPM is enabled, update the RM data.
                    if(SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH))
                    {
                        // Cdyn
                        datum = Cdyn.getBoundedValue();
                        dciPatientF_.Cdyn.put(Real32(datum.data.value));

                        // Rdyn
                        datum = Rdyn.getBoundedValue();
                        dciPatientF_.Rdyn.put(Real32(datum.data.value));

                        // PSF
                        datum = PSF.getBoundedValue();
                        dciPatientF_.PSF.put(Real32(datum.data.value));

                        // PEF
                        datum = PEF.getBoundedValue();
                        dciPatientF_.PEF.put(Real32(datum.data.value));

                        // EEF
                        datum = EEF.getBoundedValue();
                        dciPatientF_.EEF.put(Real32(datum.data.value));

                        if(PATIENT_CCT_TYPE != PatientCctTypeValue::NEONATAL_CIRCUIT)
                        {

                            // NIF
                            datum = NIF.getBoundedValue();
                            dciPatientF_.NIF.put(Real32(datum.data.value));
    
                            // P100
                            datum = P100.getBoundedValue();
                            dciPatientF_.P100.put(Real32(datum.data.value));
    
                            // VC
                            datum = VC.getBoundedValue();
                            dciPatientF_.VC.put(Real32(datum.data.value/1000.0) ); // ml to l
                        }
                    }


                    // Ti Spont
                    datum = TiSpont.getBoundedValue();
                    dciPatientF_.TiSpont.put(datum.data.value);

                    // Ve Spont
                    datum = VeSpont.getBoundedValue();
                    dciPatientF_.VeSpont.put(datum.data.value);

					// Vte Spont
					datum = VteSpont.getBoundedValue();
					dciPatientF_.VteSpont.put(Real32(datum.data.value/1000.0f));

                    // Ti/Ttot
                    datum = spontInspPercent.getBoundedValue();
                    dciPatientF_.spontInspPercent.put(datum.data.value);

                    // f/Vt
                    datum = rapShadowIndx.getBoundedValue();
                    dciPatientF_.rapShadowIndx.put(datum.data.value);



                    switch(MANDATORY_TYPE)
                    {
                        case MandTypeValue::PCV_MAND_TYPE:
                            break;
                        case MandTypeValue::VCV_MAND_TYPE:
                            break;
                        default:
                            // Invalid Mandatory Type
                            AUX_CLASS_ASSERTION_FAILURE( MANDATORY_TYPE );
                            break;

                    }
    
    
                    switch(SPONT_TYPE)
                    {
    
                        case SupportTypeValue::OFF_SUPPORT_TYPE:
                            break;    
                        case SupportTypeValue::PSV_SUPPORT_TYPE:
                            break;
                        case SupportTypeValue::ATC_SUPPORT_TYPE:
                        case SupportTypeValue::VSV_SUPPORT_TYPE:
                            break;
                        case SupportTypeValue::PAV_SUPPORT_TYPE:


                            // PEEPi
                            datum = PEEPi.getBoundedValue();
                            dciPatientF_.PEEPi.put(datum.data.value);

							// WOBtot
                            datum = WOBtot.getBoundedValue();
                            dciPatientF_.WOBtot.put(datum.data.value);

                            // Rtot
                            datum = Rtot.getBoundedValue();
                            dciPatientF_.Rtot.put(datum.data.value);

                            // Rpav
                            datum = Rpav.getBoundedValue();
                            dciPatientF_.Rpav.put(datum.data.value);

                            // Epav
                            datum = Epav.getBoundedValue();
                            dciPatientF_.Epav.put(datum.data.value);

                            // Cpav
                            datum = Cpav.getBoundedValue();
                            dciPatientF_.Cpav.put(datum.data.value);

                            // F/Vt/Kg
                            datum = rapShadowIndx.getBoundedValue();
                            dciPatientF_.normRapShadowIndx.put(datum.data.value / getApplicableBoundedValue_(settings,
                                                      SettingId::IBW));

                            break;
                        default:
                            // Invalid spont Type for Spont Mode.
                            AUX_CLASS_ASSERTION_FAILURE( SPONT_TYPE );
                            break;
                    }
    
                    break;
                case ModeValue::BILEVEL_MODE_VALUE: // $[TI1.4]


                    // Ti Spont
                    datum = TiSpont.getBoundedValue();
                    dciPatientF_.TiSpont.put(datum.data.value);

                    // Ve Spont
                    datum = VeSpont.getBoundedValue();
                    dciPatientF_.VeSpont.put(datum.data.value);

					// Vte Spont
                    datum = VteSpont.getBoundedValue();
                    dciPatientF_.VteSpont.put(Real32(datum.data.value/1000.0f));

                    switch(SPONT_TYPE)
                    {

                        case SupportTypeValue::OFF_SUPPORT_TYPE:
                            break;    
                        case SupportTypeValue::PSV_SUPPORT_TYPE:
                            break;
                        case SupportTypeValue::ATC_SUPPORT_TYPE:
                            break;
                        default:
                            // Invalid spont Type for SIMV.
                            AUX_CLASS_ASSERTION_FAILURE( SPONT_TYPE );
                            break;
                    }
    
                    break;
                default: // invalid condition
                    AUX_CLASS_ASSERTION_FAILURE( ventMode_ );
                    break;
            }
            break;            
        default:
            // Invalid Vent Type.
            AUX_CLASS_ASSERTION_FAILURE( VENT_TYPE );
            break;
    }

	if (SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP))
	{
		// Leak Compensation Enabled/Disabled Setting
		const DiscreteValue  LEAK_COMP_IS_ENABLED =
			SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
												  SettingId::LEAK_COMP_ENABLED);

		switch (LEAK_COMP_IS_ENABLED)
		{
		case LeakCompEnabledValue::LEAK_COMP_DISABLED:
			dciPatientF_.percentLeak.setInvalid();
			dciPatientF_.leak.setInvalid();
			dciPatientF_.iLeak.setInvalid();
			break;
		case LeakCompEnabledValue::LEAK_COMP_ENABLED:
			datum = percentLeak.getBoundedValue();
			dciPatientF_.percentLeak.put(datum.data.value);
			datum = leak.getBoundedValue();
			dciPatientF_.leak.put(datum.data.value);
			datum = iLeak.getBoundedValue();
			dciPatientF_.iLeak.put(datum.data.value);
			break;
		default:
			// Invalid Leak Compensation Setting
			AUX_CLASS_ASSERTION_FAILURE( LEAK_COMP_IS_ENABLED );
			break;
		}
	}
	else
	{
		dciPatientF_.percentLeak.setInvalid();
		dciPatientF_.leak.setInvalid();
		dciPatientF_.iLeak.setInvalid();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getAlarmStatus_
//
//@ Interface-Description
// Accumulate Alarm status data into global data structure, dciAlarm_.
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported or not valid are indicated as such so that they
// can be substituted with blanks during output.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getAlarmStatus_(void)
{
	AlarmName	alarm;

    encodeNormalAlarmReset_(&dciAlarm_.highInspPres,
			                 AlarmDci::GetStatus(A225));
	
	if (ventMode_ == ModeValue::AC_MODE_VALUE)
	{						// $[TI1.1]
		alarm = A255;		// mandatory
	}
	else
	{						// $[TI1.2]
		alarm = A265;		// spontaneous
	}
	encodeNormalAlarmReset_(&dciAlarm_.lowTidalVolume,
		AlarmDci::GetStatus(alarm));

	encodeNormalAlarmReset_(&dciAlarm_.lowMinuteVolume,
		AlarmDci::GetStatus(A260));
	encodeNormalAlarmReset_(&dciAlarm_.highRespRate,
		AlarmDci::GetStatus(A235));
	encodeNormalAlarmReset_(&dciAlarm_.lowO2SupplyPres,
		AlarmDci::GetStatus(A285));
	encodeNormalAlarmReset_(&dciAlarm_.lowAirSupplyPres,
		AlarmDci::GetStatus(A280));

	// force "NORMAL" status for compatibility with currrent VentNet 
	encodeNormalAlarmReset_( &dciAlarm_.lowBattery, AlarmDci::ALARM_DCI_NORMAL );

	encodeNormalAlarmReset_(&dciAlarm_.apnea,
		AlarmDci::GetStatus(A205));

	// if a Marquette monitor is connected force "NORMAL" status
	// for the following data fields, else blank these fields
	if (isMarquette_)
	{						// $[TI3.1]
		encodeNormalAlarmReset_( &dciAlarm_.lowInspPres,
									AlarmDci::ALARM_DCI_NORMAL );
		encodeNormalAlarmReset_( &dciAlarm_.lowPeepCpapPres,
									AlarmDci::ALARM_DCI_NORMAL );
		encodeNormalAlarmReset_( &dciAlarm_.ieRatio,
									AlarmDci::ALARM_DCI_NORMAL );
		encodeNormalAlarmReset_( &dciAlarm_.exhValveLeak,
									AlarmDci::ALARM_DCI_NORMAL );
		encodeNormalAlarmReset_( &dciAlarm_.lowO2Saturation,
									AlarmDci::ALARM_DCI_NORMAL );
		encodeNormalAlarmReset_( &dciAlarm_.highO2Saturation,
									AlarmDci::ALARM_DCI_NORMAL );
		encodeNormalAlarmReset_( &dciAlarm_.lowPulseRate,
									AlarmDci::ALARM_DCI_NORMAL );
		encodeNormalAlarmReset_( &dciAlarm_.highPulseRate,
									AlarmDci::ALARM_DCI_NORMAL );
	}
	else
	{						// $[TI3.2]
		dciAlarm_.lowInspPres.setNotSupported();
		dciAlarm_.lowPeepCpapPres.setNotSupported();
		dciAlarm_.ieRatio.setNotSupported();
		dciAlarm_.exhValveLeak.setNotSupported();
		dciAlarm_.lowO2Saturation.setNotSupported();
		dciAlarm_.highO2Saturation.setNotSupported();
		dciAlarm_.lowPulseRate.setNotSupported();
		dciAlarm_.highPulseRate.setNotSupported();
	}

	// force "NOOP C" for not installed oximeter
	dciAlarm_.oximeterStatus.put(PNoopCStr6_);

	encodeOnOff_( &dciAlarm_.silence, AlarmAnnunciator::GetAlarmsAreSilenced() );

	encodeNormalAlarmReset_(&dciAlarm_.apneaVentilation,
	                AlarmDci::GetStatus(A205));

	// map occlusion and disconnect alarms to disconnect report field
	AlarmDci::AlarmDciStatus disconnectAlarm = AlarmDci::GetStatus(A295);
	AlarmDci::AlarmDciStatus occlusionAlarm = AlarmDci::GetStatus(A305);

	if (   disconnectAlarm == AlarmDci::ALARM_DCI_ALARM
        || occlusionAlarm == AlarmDci::ALARM_DCI_ALARM )
	{
		// $[TI2.1]
		encodeNormalAlarmReset_(  &dciAlarm_.disconnect
		                        , AlarmDci::ALARM_DCI_ALARM );
		
	}
	else if (   disconnectAlarm == AlarmDci::ALARM_DCI_RESET
             || occlusionAlarm == AlarmDci::ALARM_DCI_RESET )
	{
		// $[TI2.2]
		encodeNormalAlarmReset_(  &dciAlarm_.disconnect
		                        , AlarmDci::ALARM_DCI_RESET );
	}
	else
	{
		// $[TI2.3]
		encodeNormalAlarmReset_(  &dciAlarm_.disconnect
		                        , AlarmDci::ALARM_DCI_NORMAL );
	} 
	
	//For SpaceLab monitors
	if ( (AcceptedContextHandle::GetDiscreteValue(SettingId::COM1_CONFIG) ==				  
		  ComPortConfigValue::SPACELAB_VALUE) && 
		 (portNum_ == SerialInterface::SERIAL_PORT_1))
     {
		 // Disconnect Alarm map to lowInspPres
		  if ( disconnectAlarm == AlarmDci::ALARM_DCI_ALARM )
		      {
		           encodeNormalAlarmReset_( &dciAlarm_.lowInspPres,
									AlarmDci::ALARM_DCI_ALARM );
		     
		      }
		   else if (disconnectAlarm == AlarmDci::ALARM_DCI_NORMAL)
		     {
		        encodeNormalAlarmReset_( &dciAlarm_.lowInspPres,
									AlarmDci::ALARM_DCI_NORMAL );
		     
		     }
		   
		
		
		     //Occlusion alarm map to hip
		     if ( occlusionAlarm == AlarmDci::ALARM_DCI_ALARM || 
		          AlarmDci::GetStatus(A225) == AlarmDci::ALARM_DCI_ALARM )
		      {
		         encodeNormalAlarmReset_( &dciAlarm_.highInspPres,
									AlarmDci::ALARM_DCI_ALARM );
			  }
			 else if (occlusionAlarm == AlarmDci::ALARM_DCI_RESET ||
			         AlarmDci::GetStatus(A225) == AlarmDci::ALARM_DCI_RESET)
		     {
		        encodeNormalAlarmReset_( &dciAlarm_.highInspPres,
									AlarmDci::ALARM_DCI_RESET );
		     
		     }
		    else
		      {
		          encodeNormalAlarmReset_( &dciAlarm_.highInspPres,
									AlarmDci::ALARM_DCI_NORMAL );
		      }
		
	 }
			 
	
	
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getAlarmStatusF_
//
//@ Interface-Description
// Accumulate Alarm status data into global data structure, dciAlarmF_
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported or not valid are indicated as such so that they
// can be substituted with blanks during output.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::getAlarmStatusF_(void)
{

    // APNEA
    encodeNormalAlarmResetF_(&dciAlarmF_.apneaVentilation,
                    AlarmDci::GetStatusF(A205));

    // High VeTot
    encodeNormalAlarmResetF_(&dciAlarmF_.highMinuteVolume,
                    AlarmDci::GetStatusF(A210));

    // High VteMand or VteSpont
    encodeNormalAlarmResetF_(&dciAlarmF_.highExhaledVolume,
                    AlarmDci::GetStatusF(A215));

    // High O2%
    encodeNormalAlarmResetF_(&dciAlarmF_.highO2Percent,
                    AlarmDci::GetStatusF(A220));

    // High Ppeak
    encodeNormalAlarmResetF_(&dciAlarmF_.highInspPres,
                             AlarmDci::GetStatusF(A225));

    // High Pvent
    encodeNormalAlarmResetF_(&dciAlarmF_.pVent,
                             AlarmDci::GetStatusF(A230));

    // High Ftot
    encodeNormalAlarmResetF_(&dciAlarmF_.highRespRate,
        AlarmDci::GetStatusF(A235));

    // AC Power Loss
    encodeNormalAlarmResetF_(&dciAlarmF_.acPowerLoss,
        AlarmDci::GetStatusF(A240));

    // Inoperative Battery
    encodeNormalAlarmResetF_(&dciAlarmF_.inoperativeBattery,
        AlarmDci::GetStatusF(A241));

    // Low Battery
    encodeNormalAlarmResetF_(&dciAlarmF_.lowBattery,
        AlarmDci::GetStatusF(A243));

    // Low AC Power
    encodeNormalAlarmResetF_(&dciAlarmF_.lowACPower,
        AlarmDci::GetStatusF(A250));

    // Low VteMand
    encodeNormalAlarmResetF_(&dciAlarmF_.lowExhaledMandVolume,
        AlarmDci::GetStatusF(A255));

    // Low VeTot
    encodeNormalAlarmResetF_(&dciAlarmF_.lowMinuteVolume,
        AlarmDci::GetStatusF(A260));

    // Low VteSpont
    encodeNormalAlarmResetF_(&dciAlarmF_.lowExhaledSpontVolume,
        AlarmDci::GetStatusF(A265));

    // Low O2 Percent
    encodeNormalAlarmResetF_(&dciAlarmF_.lowO2Percent,
        AlarmDci::GetStatusF(A270));

    // Low Air Supply
    encodeNormalAlarmResetF_(&dciAlarmF_.lowAirSupplyPres,
        AlarmDci::GetStatusF(A280));

    // Low O2 Supply
    encodeNormalAlarmResetF_(&dciAlarmF_.lowO2SupplyPres,
        AlarmDci::GetStatusF(A285));

    //  Compressor Inoperative
    encodeNormalAlarmResetF_(&dciAlarmF_.compressorInop,
        AlarmDci::GetStatusF(A290));

    //  Circuit Disconnect
    encodeNormalAlarmResetF_(&dciAlarmF_.disconnect,
        AlarmDci::GetStatusF(A295));

    //  Severe Occlusion
    encodeNormalAlarmResetF_(&dciAlarmF_.occlusion,
        AlarmDci::GetStatusF(A305));


    //  Inspiration too long
    encodeNormalAlarmResetF_(&dciAlarmF_.inspirationTooLong,
        AlarmDci::GetStatusF(A310));


    //  Procedure Error
    encodeNormalAlarmResetF_(&dciAlarmF_.procedureError,
        AlarmDci::GetStatusF(A315));


    //  Compliance Limited VT (Volume Limit)
    encodeNormalAlarmResetF_(&dciAlarmF_.volumeLimit,
        AlarmDci::GetStatusF(A335));

    //  High Vti Spont  
    encodeNormalAlarmResetF_(&dciAlarmF_.highInspiredSpontVolume,
        AlarmDci::GetStatusF(A380));

    //  High Vti Mand
    encodeNormalAlarmResetF_(&dciAlarmF_.highInspiredMandVolume,
        AlarmDci::GetStatusF(A390));

    //  High Pcomp
    encodeNormalAlarmResetF_(&dciAlarmF_.highCompensationLimit,
        AlarmDci::GetStatusF(A345));


    //  PAV Startup too long
    encodeNormalAlarmResetF_(&dciAlarmF_.pavStartupTooLong,
        AlarmDci::GetStatusF(A355));

    //  PAV R & C Not Assessed
    encodeNormalAlarmResetF_(&dciAlarmF_.pavRCnotAssessed,
        AlarmDci::GetStatusF(A360));


    //  Volume not delivered (VC+)
    encodeNormalAlarmResetF_(&dciAlarmF_.volumeNotDeliveredVcPlus,
        AlarmDci::GetStatusF(A366));


    //  Volume not delivered (VS)
    encodeNormalAlarmResetF_(&dciAlarmF_.volumeNotDeliveredVS,
        AlarmDci::GetStatusF(A365));


    //  Low Ppeak
    encodeNormalAlarmResetF_(&dciAlarmF_.lowInspPres,
        AlarmDci::GetStatusF(A400));


    //  BTAT1
    encodeNormalAlarmResetF_(&dciAlarmF_.btat1,
        AlarmDci::GetStatusF(A5));


    //  BTAT2
    encodeNormalAlarmResetF_(&dciAlarmF_.btat2,
        AlarmDci::GetStatusF(A10));


    //  BTAT3
    encodeNormalAlarmResetF_(&dciAlarmF_.btat3,
        AlarmDci::GetStatusF(A15));

    //  BTAT4
    encodeNormalAlarmResetF_(&dciAlarmF_.btat4,
        AlarmDci::GetStatusF(A20));

    //  BTAT5
    encodeNormalAlarmResetF_(&dciAlarmF_.btat5,
        AlarmDci::GetStatusF(A25));

    //  BTAT6
    encodeNormalAlarmResetF_(&dciAlarmF_.btat6,
        AlarmDci::GetStatusF(A30));

    //  BTAT7
    encodeNormalAlarmResetF_(&dciAlarmF_.btat7,
        AlarmDci::GetStatusF(A35));

    //  BTAT8
    encodeNormalAlarmResetF_(&dciAlarmF_.btat8,
        AlarmDci::GetStatusF(A40));

    //  BTAT9
    encodeNormalAlarmResetF_(&dciAlarmF_.btat9,
        AlarmDci::GetStatusF(A45));

    //  BTAT10
    encodeNormalAlarmResetF_(&dciAlarmF_.btat10,
        AlarmDci::GetStatusF(A50));

    //  BTAT11
    encodeNormalAlarmResetF_(&dciAlarmF_.btat11,
        AlarmDci::GetStatusF(A55));

    //  BTAT12
    encodeNormalAlarmResetF_(&dciAlarmF_.btat12,
        AlarmDci::GetStatusF(A60));

    //  BTAT13
    encodeNormalAlarmResetF_(&dciAlarmF_.btat13,
        AlarmDci::GetStatusF(A65));

    //  BTAT14
    encodeNormalAlarmResetF_(&dciAlarmF_.btat14,
        AlarmDci::GetStatusF(A70));

    //  BTAT15
    encodeNormalAlarmResetF_(&dciAlarmF_.btat15,
        AlarmDci::GetStatusF(A75));

    //  BTAT16
    encodeNormalAlarmResetF_(&dciAlarmF_.btat16,
        AlarmDci::GetStatusF(A80));

    //  BTAT17
    encodeNormalAlarmResetF_(&dciAlarmF_.btat17,
        AlarmDci::GetStatusF(A85));

	if( LowerSubScreenArea::GetProxSetupSubScreen()->isProxInstalled() )
	{
	// [PX04002] PROX FAULT/NORMAL
	encodeProxNormalFault_(&dciAlarmF_.proxAlarm,
		AlarmDci::GetStatusF(A1000));
	}
	else
	{
		dciAlarmF_.proxAlarm.put(PBlankStr6_);
	}
    
    // Check for Alarm Silence
    encodeOnOff_( &dciAlarmF_.silence, AlarmAnnunciator::GetAlarmsAreSilenced() );

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  encodeProxNormalFault_
//
//@ Interface-Description
// Pass a normal/alarm value indicator and return the appropriate
// null-terminated 6-character encoding of it ("NORMAL", "FAULT ").
// This method is only used for PROX alarm status currently.
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported or not valid are indicated as such so that they
// can be substituted with blanks during output.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

char *
Dci::encodeProxNormalFault_(DciString * pDciString, const AlarmDci::AlarmDciStatus status)
{
	static	char	buffer[7];

	switch ( status )
	{
    case AlarmDci::ALARM_DCI_RESET:		
	case AlarmDci::ALARM_DCI_NORMAL:		
		pDciString->put(PNormalStr6_);
		break;

	case AlarmDci::ALARM_DCI_LOW_URGENCY:
	case AlarmDci::ALARM_DCI_ALARM:			
		pDciString->put(PProxFaultStr6_);
		break;
	default:	// invalid condition
		AUX_CLASS_ASSERTION_FAILURE( status );
	}
	pDciString->get(buffer);
	return(buffer);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  encodeNormalAlarmReset_
//
//@ Interface-Description
// Pass a normal/alarm/reset value indicator and return the appropriate
// null-terminated 6-character encoding of it ("NORMAL", "ALARM " or
// "RESET ").
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported or not valid are indicated as such so that they
// can be substituted with blanks during output.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

char *
Dci::encodeNormalAlarmReset_(DciString * pDciString, const AlarmDci::AlarmDciStatus status)
{
	static	char	buffer[7];

	switch ( status )
	{
	case AlarmDci::ALARM_DCI_NORMAL:		// $[TI1.1]
		pDciString->put(PNormalStr6_);
		break;
	case AlarmDci::ALARM_DCI_ALARM:			// $[TI1.2]
		pDciString->put(PAlarmStr6_);
		break;
	case AlarmDci::ALARM_DCI_RESET:			// $[TI1.3]
		pDciString->put(PResetStr6_);
		break;
	default:	// invalid condition
		AUX_CLASS_ASSERTION_FAILURE( status );
	}
	pDciString->get(buffer);
	return(buffer);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  encodeNormalAlarmResetF_
//
//@ Interface-Description
// Pass a normal/low/medium/high/reset/alarm value indicator and return 
// the appropriate null-terminated 6-character encoding of it ("NORMAL",
// "LOW   ", "MEDIUM", "HIGH ","RESET ", "ALARM ").
//---------------------------------------------------------------------
//@ Implementation-Description
// Values not supported or not valid are indicated as such so that they
// can be substituted with blanks during output.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

char *
Dci::encodeNormalAlarmResetF_(DciString * pDciString, const AlarmDci::AlarmDciStatus status)
{
    static  char    buffer[7];

    switch ( status )
    {

        case AlarmDci::ALARM_DCI_ALARM:       
            pDciString->put(PAlarmStr6_);
            break;

        case AlarmDci::ALARM_DCI_NORMAL:        
            pDciString->put(PNormalStr6_);
            break;
        case AlarmDci::ALARM_DCI_LOW_URGENCY:        
            pDciString->put(PLowUrgencyAlarmStr6_);
            break;

        case AlarmDci::ALARM_DCI_MEDIUM_URGENCY:        
            pDciString->put(PMediumUrgencyAlarmStr6_);
            break;
        case AlarmDci::ALARM_DCI_HIGH_URGENCY:        
            pDciString->put(PHighUrgencyAlarmStr6_);
            break;
    
        case AlarmDci::ALARM_DCI_RESET:        
            pDciString->put(PResetStr6_);
            break;
        default:    // invalid condition
            AUX_CLASS_ASSERTION_FAILURE( status );
            break;

    }
    pDciString->get(buffer);
    return(buffer);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicableBoundedValueF_
//
//@ Interface-Description
//  The 840's DCI feature shall return a blank empty string in the SNDF 
//  report which are not applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

Real32
Dci::getApplicableBoundedValueF_(BatchSettingValues &settings, 
                    SettingId::SettingIdType settingId)
{
    const Applicability::Id  APPLICABILITY = 
      SettingContextHandle::GetSettingApplicability 
      (ContextId::ACCEPTED_CONTEXT_ID, settingId);
    
    Real32 applicableValue;
    
    switch (APPLICABILITY)
    {
        case Applicability::CHANGEABLE : 
            applicableValue = settings.getBoundedValue(settingId).value;
            break;
        case Applicability::VIEWABLE :     
        case Applicability::INAPPLICABLE : 
            applicableValue = EMPTY_STR_ID;
        break;
        default :
            AUX_CLASS_ASSERTION_FAILURE((APPLICABILITY << 16) | settingId);
            applicableValue = EMPTY_STR_ID;
        break;
    }

    
    return (applicableValue);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientDataChangeHappened
//
//@ Interface-Description
// Handles update event notification for patient data fields.  The
// 'patientDataId' identifies which patient data value changed so that
// we know which data value to update.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// The given 'patientDataId' value must be one of the id's associated
// with the patient data fields in this area.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void 
Dci::patientDataChangeHappened(PatientDataId::PatientItemId patientDataId)
{
    CALL_TRACE("patientDataChangeHappened(patientDataId)");

    if (patientDataId == PatientDataId::IS_NIV_INSP_TOO_LONG_ITEM)
    {
        BreathDatumHandle dataHandle(patientDataId);
        isNivInspTooLong_ = dataHandle.getDiscreteValue().data;
    }
    else
    {
        AUX_CLASS_ASSERTION_FAILURE(patientDataId);
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
// Implicitly gain execution control when a software assertion macro
// detects and error.
//---------------------------------------------------------------------
//@ Implementation-Description
// Automatically invoked when CLASS_ASSERTION and CLASS_PRE_CONDITON
// macros (among others) detect a fault.  We just call FaultHandler::SoftFault
// to display pertinent information to the user.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Dci::SoftFault(const SoftFaultID softFaultID,
		const Uint32	lineNumber,
		const char *	pFileName,
		const char *	pPredicate)
{
	FaultHandler::SoftFault(softFaultID, DCI_DEVICE, DCI_CLASS,
		lineNumber, pFileName, pPredicate);
}
