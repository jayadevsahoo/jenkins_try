#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  DateFormatSetting -  Date Format Setting
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that contains the date format.
//  This class inherits from 'BatchDiscreteSetting' and provides the 
//  specific information needed for representation of the date format.  
//  This information includes the valid values of this setting
//  (see 'DateFormatValue.hh'), and this batch setting's default value
//  (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members -- it just specifies behavior.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DateFormatSetting.ccv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: rhj    Date:  01-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Initial version.
//=====================================================================

#include "DateFormatSetting.hh"
#include "DateFormatValue.hh"
#include "AcceptedContext.hh"
#include "ContextMgr.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  DateFormatSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[00643] - setting range
//  $[00644] - setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DateFormatSetting::DateFormatSetting(void)
	: BatchDiscreteSetting(SettingId::DATE_FORMAT,
						   Setting::NULL_DEPENDENT_ARRAY_,
						   DateFormatValue::NUM_DATE_FORMAT_VALUES)
{
	CALL_TRACE("DateFormatSetting()");
}	 

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~DateFormatSetting()  [Virtual Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DateFormatSetting::~DateFormatSetting(void)	
{
	CALL_TRACE("~DateFormatSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//
//  date/time settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	DateFormatSetting::getApplicability(const Notification::ChangeQualifier) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	return(Applicability::CHANGEABLE);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	DateFormatSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const AcceptedContext&  rAcceptedContext = ContextMgr::GetAcceptedContext();

	DiscreteValue  newPatientValue;

	if (rAcceptedContext.areBatchValuesInitialized())
	{
		// when the Accepted Context has previosly-initialized values, use the
		// previous value of this setting...
		newPatientValue = rAcceptedContext.getBatchDiscreteValue(SettingId::DATE_FORMAT);
	}
	else
	{
		// The setting's new-patient value ...
		newPatientValue = DateFormatValue::DATE_FORMAT_DD_MMM_YY;
	}

	return(newPatientValue);
}  


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//          [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	DateFormatSetting::SoftFault(const SoftFaultID  softFaultID,
								 const Uint32       lineNumber,
								 const char*        pFileName,
								 const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							DATE_FORMAT_SETTING, lineNumber, pFileName,
							pPredicate);
} 

