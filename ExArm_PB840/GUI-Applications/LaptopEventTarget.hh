#ifndef LaptopEventTarget_HH
#define LaptopEventTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: LaptopEventTarget - A target to handle Laptop Request events.
// Classes can specify this class as an additional parent class and register
// to be informed of Laptop Request events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LaptopEventTarget.hhv   25.0.4.0   19 Nov 2013 14:08:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "LaptopEventId.hh"
#include "SerialInterface.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class LaptopEventTarget
{
public:
	virtual void laptopRequestHappened(LaptopEventId::LTEventId eventId,
											Uint8 *pMsgData);

	virtual void laptopFailureHappened(SerialInterface::SerialFailureCodes
														serialFailureCodes,
														Uint8 eventId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	LaptopEventTarget(void);
	virtual ~LaptopEventTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	LaptopEventTarget(const LaptopEventTarget&);	// not implemented...
	void operator=(const LaptopEventTarget&);		// not implemented...
};


#endif // LaptopEventTarget_HH 
