#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: CriticalSection - Critical Section.
//---------------------------------------------------------------------
//@ Interface-Description
//  The CriticalSection class implements an mutual exclusion lock for
//  access to shared data.  The CriticalSection obtains a low level
//  mutex in its constructor and releases the mutex in its destructor.
//  This way the mutex is owned for the life of the CriticalSection
//  object.  This implicit unlocking is safer than depending on an 
//  explicit call to unlock the mutex.
//---------------------------------------------------------------------
//@ Rationale
//  CriticalSection is a utility class providing a safe mutex.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the MutEx class to implement the low level semaphore/mutex.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/CriticalSection.ccv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "CriticalSection.hh"
#include "MutEx.hh"
#if 0
# include <vrtxil.h>
#endif

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CriticalSection [constructor]
//
//@ Interface-Description
//  The CriticalSection class constructor accepts a mutex identifier.
//  It requests this mutex from an operating system mutex/semaphore
//  service before returning.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A MutEx object is constructed from the mutexId parameter and the
//  mutex requested before returning from the constructor.  The calling
//  application will wait here until the mutex is granted.  The 
//  MutEx::request method is called to obtain the lock.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

CriticalSection::CriticalSection(const Int32 mutexId) 
    : mutex_(mutexId)
{
    CALL_TRACE("CriticalSection()");

    mutex_.request();
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CriticalSection [destructor]
//
//@ Interface-Description
//  The CriticalSection class destructor releases the mutex it obtained
//  during its construction.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The MutEx::release method is called to release the mutex/semaphore.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

CriticalSection::~CriticalSection()
{
    CALL_TRACE("~CriticalSection()");
    
    mutex_.release();
    // $[TI1]
}
 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
CriticalSection::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_CRITICALSECTION, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




