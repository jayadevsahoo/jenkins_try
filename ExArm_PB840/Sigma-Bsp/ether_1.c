/***************************************************************************
*
*		Copyright (c) 1995 MICROTEC RESEARCH INCORPORATED.
*
*	All rights reserved. MICROTEC RESEARCH's source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	MICROTEC RESEARCH. Any attempt or participation in deciphering,
*	decoding, reverse engineering or in any way altering the source code
*	is strictly prohibited, unless the prior written consent of
*	MICROTEC RESEARCH is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/

/*
**
** ether_1.c
**
**    This file contains device-independant, board-independant
**    functions that implement the logio functions common to
**    devices of type DEV_ETHER_1.
**
**    The functions here use the ether_1 device descriptors to
**    to access function pointers to perform the operations for
**    a specific physical ethernet chip.
**
**    The functions are:
**
**       logio_ether_1_init
**       logio_ether_1_read
**       logio_ether_1_write
**       logio_ether_1_control
**       logio_ether_1_poll
**       logio_ether_1_getmsg
**       logio_ether_1_putmsg
**
** History:
**
**    Date        Name              Description
**    ---------------------------------------------------------------------- 
**    05-05-92    Eugene Walden     First Version
**    11-11-92    Roger Cole        Made board and device independant
**
**/
#include "ansiprot.h"		/* ANSI prototypes */
#include "logio.h"		/* logio defines and typedefs */
#include "logio_charbuf.h"	/* circular character buffers */
#include "vbsp_cbid.h"		/* VBSP control block typedef */
#include "vbsp.h"		/* VBSP return codes and typedefs */
#include <ether_1.h>

/*
 ** definition of device id
 */

logio_status_t logio_ether_1_init(
   _ANSIPROT1(logio_device_id_t));

logio_status_t logio_ether_1_read(
   _ANSIPROT3(
      logio_device_id_t, 
      char *, 
      int *));

logio_status_t logio_ether_1_write(
   _ANSIPROT3(
      logio_device_id_t, 
      char *,   
      int *));

logio_status_t logio_ether_1_control(
   _ANSIPROT3(
      logio_device_id_t, 
      logio_control_t,
      void *));

logio_status_t logio_ether_1_getmsg(
   _ANSIPROT2(
      logio_device_id_t, 
      logio_buff_t **));

logio_status_t logio_ether_1_putmsg(
   _ANSIPROT2(
      logio_device_id_t, 
      logio_buff_t *));

logio_status_t logio_ether_1_poll(
   _ANSIPROT3(
      logio_device_id_t, 
      logio_control_t, 
      void *));

/* the following two structures define the logio ID for ETHER_1 */

const logio_fops_t logio_ether_1_fops = {
    logio_ether_1_init,
    logio_ether_1_read,
    logio_ether_1_write,
    logio_ether_1_control,
    logio_ether_1_getmsg,
    logio_ether_1_putmsg,
   logio_ether_1_poll
};

#if defined(PPC860) /* and for the other cpu which needs cache disable */
vbsp_return_t
logio_ether_dev_recv(ether_1_dev_desc_t *, logio_buff_t **);
vbsp_return_t
logio_ether_dev_send(ether_1_dev_desc_t *, logio_buff_t *);
#else
#define logio_ether_dev_recv(dev, pplogio) (*dev->fops.pkt_recv)(dev, pplogio)
#define logio_ether_dev_send(dev, plogio)  (*dev->fops.pkt_send)(dev, plogio)
#endif /* defined(PPC860) */


/*************************************************************************
**
** logio_ether_1_init
**
**     This function initializes the ETHER_1 device. This function 
**     sets up the logio interrupt handing functions, and then 
**     performs the device-specific initialization.
**
** Inputs:
**
**     id:   ID of the target device
**
** Outputs:
**
**     NONE
**
** Return codes:
**
**    LOGIO_STATUS_OK
**    LOGIO_STATUS_RESOURCES
**
** Side-effects:
** Global variables:
**
*/
logio_status_t logio_ether_1_init(id)
logio_device_id_t id;
{
    ether_1_dev_desc_t *dev = (ether_1_dev_desc_t *) id->data;
    
    logio_status_t ret;        /* return code */
    
    /* 
     * Set up the vectors and test routines for the ETHER_1 device 
     * 
     * The ETHER_1 device may have one vector associated with it, but
     * there can be four possible events that will cause this
     * interrupt to fire: 
     * 
     *    1. Receive descriptor update (packet received)
     *    2. Transmit descriptor update (packet transmitted)
     *    3. Error occurred
     *    4. Initialization sequence completed
     * 
     * The logio structures for ETHER_1 look like: 
     * 
     *     +-------------------------------------------+
     *  +--+-next                                      |
     *  |  +-------------------------------------------+
     *  |  | vector number = DEVICE_VECTOR             |
     *  |  +-------------------------------------------+
     *  |  | table_size = 4                            |
     *  |  +-------------------------------------------+
     *  |  | num_events = 4                            |
     *  |  +-------------------------------------------+
     *  |  | table     o                               |
     *  |  +-----------+-------------------------------+
     *  |              |
     *  |              V
     *  |   +----------+------------+---------+----+----+--------------+
     *  |   | ether_1  | PCKTRXPCKT | <flags> | <> | <> | eth1_rx_tst  |
     *  |   +----------+------------+---------+----+----+--------------+
     *  |   | ether_1  | PCKTTXRDY  | <flags> | <> | <> | eth1_tx_tst  |
     *  |   +----------+------------+---------+----+----+--------------+
     *  |   | ether_1  | PCKTERROR  | <flags> | <> | <> | eth1_err_tst |
     *  |   +----------+------------+---------+----+----+--------------+
     *  |   | ether_1  | PCKTINIT   | <flags> | <> | <> | eth1_init_tst|
     *  |   +----------+------------+---------+----+----+--------------+
     *  V
     *
     * In another instance, each event may have a separate vector.
     * For example, the logio structure for the PCKTTXRDY may
     * look like this:
     * 
     *    +-------------------------------------------+
     *  +-+-next                                      |
     *  | +-------------------------------------------+
     *  | | vector number = dev->config.tx_int_vector |
     *  | +-------------------------------------------+
     *  | | table_size = 1                            |
     *  | +-------------------------------------------+
     *  | | num_events = 1                            |
     *  | +-------------------------------------------+
     *  | | table     o                               |
     *  | +-----------+-------------------------------+
     *  |             |
     *  |             V
     *  | +-------+---------+-------+--+--+----------------------+
     *  | |ether_1|PCKTTXRDY|<flags>|<>|<>|dev->fops.ether_tx_tst|
     *  | +-------+---------+-------+--+--+----------------------+
     *  V
     *
     */

    if ( dev->fops.ether_rx_tst )
    {
	ret = logio_set_vector_and_test(
					id, 
					LOGIO_INT_EVENT_PCKTRXPCKT, 
					dev->config.rx_int_vector, 
					dev->fops.ether_rx_tst
					);
	
	if (ret != LOGIO_STATUS_OK)
	    return( ret );
    }
    
    if ( dev->fops.ether_tx_tst )
    {
	/* load ISR for DEV_ETHER_1 */
	ret = logio_set_vector_and_test(
					id, 
					LOGIO_INT_EVENT_PCKTTXRDY, 
					dev->config.tx_int_vector, 
					dev->fops.ether_tx_tst
					);
	
	if (ret != LOGIO_STATUS_OK)
	    return( ret );
    }
    
    if ( dev->fops.ether_err_tst )
    {
	ret = logio_set_vector_and_test(
					id, 
					LOGIO_INT_EVENT_PCKTERROR, 
					dev->config.err_int_vector, 
					dev->fops.ether_err_tst
					);
	
	if (ret != LOGIO_STATUS_OK)
	    return( ret );
    }
    
    if ( dev->fops.ether_init_tst )
    {
	ret = logio_set_vector_and_test(
					id, 
					LOGIO_INT_EVENT_PCKTINIT, 
					dev->config.init_int_vector, 
					dev->fops.ether_init_tst
					);
	
	if (ret != LOGIO_STATUS_OK)
	    return( ret );
    }
    
    (*dev->fops.init)(dev);
    
    return(LOGIO_STATUS_OK);
}


/*************************************************************************
**
** logio_ether_1_read
**
**      This operation is not supported for the network device. 
**      Thus, the function will always return 
**      LOGIO_STATUS_NOTSUPPORTED.
**
** Inputs:
**
**      id:      The ID of the target device
**      buflenp:   On input, this contains a pointer to an 
**                      integer that contains the number of bytes 
**                      in the input buffer
**
** Outputs:
**
**      buf:      The data that were read
**      buflenp:   The number of bytes read
**
** Return codes:
**
**      LOGIO_STATUS_NOTSUPPORTED
**
** Side-effects:
** Global variables:
**
*/
logio_status_t logio_ether_1_read(id, buf, buflenp)
logio_device_id_t   id;
char         *buf;
int         *buflenp;
{
    return(LOGIO_STATUS_NOTSUPPORTED);
}


/*************************************************************************
**
** logio_ether_1_write
**
**      This operation is not supported for the network device. 
**      Thus, the function will always return 
**      LOGIO_STATUS_NOTSUPPORTED.
**
** Inputs:
**
**      id:      The ID of the target device
**      buflenp:   On input, this contains a pointer to an 
**                      integer that contains the number of bytes 
**                      in the input buffer
**
** Outputs:
**
**      buf:      The data that were written
**      buflenp:   The number of bytes written
**
** Return codes:
**
**      LOGIO_STATUS_NOTSUPPORTED
**
** Side-effects:
** Global variables:
**
*/
logio_status_t logio_ether_1_write(id, buf, buflenp)
logio_device_id_t   id;
char         *buf;
int         *buflenp;
{
    return(LOGIO_STATUS_NOTSUPPORTED);
}


/*************************************************************************
** Function name:
**
**    logio_ether_1_control
**
** Description:
** logio_device_control() is used for device specific commands.
** The last parameter "arg" is a pointer to data to be used by the
** function or to be filled in by the function.  Other functions
** may ignore arg or may treat it directly as a data item; they
** may, for example, be passed an int value.
** 
** For each device, the following controls are required for the
** BSP developer to provide.
** 
** Control LOGIO_CONTROL_GETDEVNAME can be used for getting the
** name of a device. In this case "arg" is a buffer up to
** LOGIO_DEV_NAME_LEN, currently 16 bytes.
** 
** Control LOGIO_CONTROL_GETDEVTYPE can be used to get the type of
** the underlying physical device. In this case "arg" points to a
** variable of type logio_device_type_t. The only device type
** currently defined is LOGIO_DEV_TYPE_GENERIC.
** 
** Controls LOGIO_CONTROL_GETCOUNTTIME and
** LOGIO_CONTROL_SETCOUNTTIME get and set the number of
** nanoseconds per timer count increment.
** 
** For LOGIO_CONTROL_GETCOUNTTIME, logio_device_control returns
** the number of nanoseconds per timer count increment in "arg".
** 
** For LOGIO_CONTROL_SETCOUNTTYPE, logio_device_control sets the
** number of nanoseconds that per counter increment to "arg".
** 
** If logio_device_control cannot set the timer count increment
** time to "arg", then it makes the best attempt, writes into
** "arg" the actual value that was set, and returns
** LOGIO_STATUS_OK.
** 
** If this parameter cannot be modified at all, then
** logio_device_control returns LOGIO_STATUS_NOTSUPPORTED.
** 
** Controls LOGIO_CONTROL_GETTIMERPRELOAD and
** LOGIO_CONTROL_SETTIMERPRELOAD get and set the timer's preload
** value, in counts.
** 
** For LOGIO_CONTROL_GETTIMERPRELOAD, logio_device_control returns
** the number of counts per clock rollover in "arg".
** 
** For LOGIO_CONTROL_SETTIMERPRELOAD, logio_device_control sets
** the number of counts per clock rollover to "arg".
** 
** If logio_device_control cannot set the preload value to "arg",
** then it makes the best attempt, writes into "arg" the actual
** value that was set, and returns LOGIO_STATUS_OK.
** 
** If this parameter cannot be modified at all, then
** logio_device_control returns LOGIO_STATUS_NOTSUPPORTED.
** 
** Control LOGIO_CONTROL_GETTIMERROLLOVER gets the value at which
** the timer count will be reloaded with the preload value.
** logio_device_control returns the rollover value, in counts, in
** "arg".
** 
** Control LOGIO_CONTROL_READTIMER gets the current value of the
** timer's counter, in counts. logio_device_control returns the
** counter value in "arg".
** 
** Controls LOGIO_CONTROL_SETHWADDR and LOGIO_CONTROL_GETHWADDR
** can be used for devices that support the notion of a physical
** address. In this case "arg" is a structure.
** 
** #define LOGIO_HW_ADDR_LEN 6
** 
** typedef struct { 
**            int bufflen; 
**            char addr[LOGIO_HW_ADDR_LEN]; 
**            }
**    logio_hw_addr_t;
** 
** Control LOGIO_CONTROL_RESET can be used to reset the device
** when applicable.  The result is device dependent.
** 
** Control LOGIO_CONTROL_INTINFOGET is used to get information
** about a specific interrupt.  Currently the only information is
** "flags".  In this case "arg" points to a structure of type
** logio_int_entry_t defined in logio.h by
** 
** typedef struct { 
**            logio_int_event_t event; 
**            int flags; 
**            }
**         logio_int_entry_t;
** 
** "event" is the only input parameter. "flags" values currently
** supported are LOGIO_INT_ENABLED and LOGIO_INT_DISABLED.
** 
** Control LOGIO_CONTROL_INTINFOSET is used to set the values
** specified in the structure.  It returns the old values. The
** action specified by the control option is also performed at the
** device level. LOGIO_INT_DISABLED will disable delivery to the
** CPU of interrupts for the given <device,event> while
** LOGIO_INT_ENABLED will enable them.
** 
** Other device specific control command can be added to the list
** of available commands. Ready Systems reserves the use of
** constants in the range 0-0xffff.
** 
** This function is nonblocking. Certain operation may result the
** status LOGIO_STATUS_INPROGRESS. In this case, the function
** logio_device_poll() can be used to find out the status of this
** operation.
** 
**
**
** Inputs:
**
**      id:      The ID of the target device
**      control:   The control command
**      arg:       A pointer to data used by the function or 
**                      to be filled in by the function
**
** Outputs:
**
**      arg:       A pointer to data used by the function or 
**                      to be filled in by the function
**
** Return codes:
**
**    LOGIO_STATUS_OK
**    LOGIO_STATUS_NOTSUPPORTED
**    LOGIO_STATUS_INPROGRESS
**
** Side-effects:
** Global variables:
**
*/
int logio_ether_1_control(id, control, arg)
logio_device_id_t id;
logio_control_t control;
void *arg;
{
    logio_status_t       ret;
    logio_interrupt_t   s;
    ether_1_dev_desc_t *dev = (ether_1_dev_desc_t *) id->data;
    
    s = logio_interrupt_disable();
    
    ret = LOGIO_STATUS_OK;
    
    switch( control )
    {
    case LOGIO_CONTROL_GETDEVNAME:
    case LOGIO_CONTROL_GETDEVTYPE:
    case LOGIO_CONTROL_RESET:
	break;
	
    case LOGIO_CONTROL_INTINFOSET:
	if (dev->fops.int_info_set)
            ret = (*dev->fops.int_info_set)(dev,(logio_int_entry_t *) arg);
	else
	    ret = LOGIO_STATUS_NOTSUPPORTED;
	break;
	
    case LOGIO_CONTROL_INTINFOGET:
	if (dev->fops.int_info_get)
            ret = (*dev->fops.int_info_get)(dev,(logio_int_entry_t *) arg);
	else
	    ret = LOGIO_STATUS_NOTSUPPORTED;
	break;
	
    case LOGIO_CONTROL_SETHWADDR: 
	if (dev->config.board_set_ether_address)
	{
	    if (((logio_hw_addr_t *) arg)->bufflen >= 6)
		(*dev->config.board_set_ether_address)((unsigned char *)
					    ((logio_hw_addr_t *)arg)->addr);
	    else 
		ret = LOGIO_STATUS_EPARAM;
	}
	else
	    ret = LOGIO_STATUS_NOTSUPPORTED;
	break;
	
    case LOGIO_CONTROL_GETHWADDR:
	if (dev->config.board_get_ether_address)
	{
            if (((logio_hw_addr_t *) arg)->bufflen >= 6)
		(*dev->config.board_get_ether_address)((unsigned char *)
					      ((logio_hw_addr_t *)arg)->addr);
            else 
               ret = LOGIO_STATUS_EPARAM;
	 }
	else
	    ret = LOGIO_STATUS_NOTSUPPORTED;
	break;
	
    default:
	ret = LOGIO_STATUS_NOTSUPPORTED;
	break;
    }

    logio_interrupt_enable(s);
    
    return(ret);
}

/*************************************************************************
**
** logio_ether_1_poll
**
**    This function is used to poll the status of an earlier control
**    operation.  When the returning status is LOGIO_STATUS_OK, arg 
**    will contain the expected results.
**
** Inputs:
**
**      id:      The ID of the target device
**      control:   The control code
**
** Outputs:
**
**      arg:      Pointer to data that contain the result. 
**                      The data are valid when logio_device_poll 
**                      returns LOGIO_STATUS_OK.
**
** Return codes:
**
**    LOGIO_STATUS_OK
**    LOGIO_STATUS_INPROGRESS
**      LOGIO_STATUS_NOTSUPPORTED
**
** Side-effects:
** Global variables:
**
*/
logio_status_t logio_ether_1_poll(id, control, arg)
logio_device_id_t         id;
logio_control_t      control;
void          *arg;
{
    return(LOGIO_STATUS_NOTSUPPORTED);
}

/*************************************************************************
**
** logio_ether_1_getmsg
**
**    A pointer to the list of packets received by the corresponding 
**    device is returned if available, logio_buff_NULL is returned 
**    otherwise. A packet can actually be split across more than one 
**    buffer.
**
** Inputs:
**
**      id:      The ID of the target device
**
** Outputs:
**
**    **bufpp:          Received message
**
** Return codes:
**
**    LOGIO_STATUS_OK
**    LOGIO_STATUS_NOTSUPPORTED
**
** Method:
**
**      This function uses the device-specific board-
**      independent function fops.pkt_recv to get packets from the 
**      Ethernet device. The function expects a pointer to a 
**      structure containing information about the device. This 
**      structure is a global variable, a pointer to which is 
**      contained in logio_ether_1_id->data.
**
**      Each call to fops.pkt_recv returns a pointer to a single 
**      packet, if one is available.
**
** Side-effects:
** Global variables:
**
*/
logio_status_t logio_ether_1_getmsg(id, bufpp)
logio_device_id_t      id;
logio_buff_t         **bufpp;
{
    ether_1_dev_desc_t *dev = (ether_1_dev_desc_t *) id->data;
    logio_buff_t *lbptr;      /* pointer to a logio buffer     */
    logio_buff_t *start;      /* pointer to a logio buffer     */
    logio_interrupt_t   s;
    
    /* disable interrupt */
    s = logio_interrupt_disable();

    /* Get the first packet-- initialize start of packet list */
    if (logio_ether_dev_recv(dev, &lbptr) == VBSP_SUCCESS)
	start = lbptr;
    else
	start = logio_buff_NULL;
    
    /* 
     * Note: 
     * 
     * If you want ether_1_getmsg to return *all* avaiable packets, 
     * not just the first one, then uncomment the following code. 
     * 
     * Otherwise, only the first avaiable packet is returned. 
     * 
     */
    
    /* Get packets until no more are available */
    /*
       stat = VBSP_SUCCESS;

       while (stat == VBSP_SUCCESS)
       {
       stat = logio_ether_dev_pkt_(dev, &lbptr->next_pckt);
       lbptr = lbptr->next_pckt;
       }
       */

    /* restore interrupt status */
    logio_interrupt_enable(s);
    
    *bufpp = start;
    
    return(LOGIO_STATUS_OK);
}

/*************************************************************************
**
** logio_ether_1_putmsg
**
**    The packet pointed to by "bufp" is sent across the device. A 
**    packet can actually be split across more than one buffer.
**
** Inputs:
**
**      id:      The ID of the target device
**    *bufp:      buffer to send
**
** Outputs:
**
**      NONE
**
** Return codes:
**
**    LOGIO_STATUS_OK
**    LOGIO_STATUS_NOT_SUPPORTED
**    LOGIO_STATUS_WOULDBLOCK
**
** Method:
**
**      The buffer list pointed to by "bufp" can contain more than 
**      one packet. Go through the list of packets, sending each one
**      to the network device using the device-specific fops entry
**      pkt_send.
**
** Side-effects:
** Global variables:
**
*/
logio_status_t logio_ether_1_putmsg(id, bufp)
logio_device_id_t         id;
logio_buff_t      *bufp;
{
    ether_1_dev_desc_t *dev = (ether_1_dev_desc_t *) id->data;
    vbsp_return_t stat=VBSP_SUCCESS;  /* return code from pkt_send */
    logio_interrupt_t   s;
    
    /* disable interrupt */
    s = logio_interrupt_disable();
    
#if 0
    /* FOR RIGHT NOW, WE ONLY SUPPORT ONE SEND PER PUTMSG!!! */
    /* THIS COULD BE CHANGED LATER */
    /* Send each packet to Ethernet controller */
    while (bufp && (stat == VBSP_SUCCESS))
    {
	stat = (*dev->fops.pkt_send)(dev, bufp);
	
	if (stat == VBSP_SUCCESS)
	    bufp = bufp->next_pckt;
    }
#else
    if(bufp)
	stat = (*dev->fops.pkt_send)(dev, bufp);
#endif
    
    /* restore interrupt status */
    logio_interrupt_enable(s);
    
    if (stat != VBSP_SUCCESS)
	return(LOGIO_STATUS_WOULDBLOCK);
    
    return(LOGIO_STATUS_OK);
}

/*************************************************************************
**
** logio_get_free_list
**
**    This functions gets a list of logio buffers.
**
** Inputs:
**
**    tnxd:      Pointer to TNX structure: ignored in this 
**                      function, but maintained to keep the 
**                      interface.
**    n_buf:      Number of bufs requested
**
** Outputs:
**
**      listPP:    Pointer to pointer to buf list
**
** Return codes:
**
**      0:      Successful return
**      1:       No buffer available
**
** Method:
**
**      This function uses logio_buff_get in order to build a list 
**      of free logio buffers. logio_buff_get only returns a single 
**      buffer, but we need the ability to get a list of "n_buf" 
**      buffers.
**
** Side-effects:
** Global variables:
**
*/
int logio_get_free_list (tnxd, n_buf, listPP)
void *tnxd;			/* not used-- kept for intrfc compat      */
int n_buf;			/* number of buffers requested            */
logio_buff_t **listPP;		/* pointer to pointer to output mbuf list */
{
    int i;                           /* loop counter         */
    logio_buff_t *lbptr;             /* pointer to logio buffer      */
    logio_buff_t *start;             /* pointer to logio buffer      */
    logio_buff_t *tmp;               /* pointer to logio buffer      */
    
    extern logio_buff_t *logio_buff_get();  /* fct to get logio buff  */
    
    /* Get first buffer in the list and save a pointer to the start */
    lbptr = logio_buff_get(0, LOGIO_BUFF_TYPE_VT);
    start = lbptr;
    
    if (start == logio_buff_NULL)
    {
	*listPP = logio_buff_NULL;
	return(1);
    }
    
   /* Get the rest of the buffers, linking each into the list */
    for (i = 0; i < n_buf-1; i++) {
	lbptr->next_buff = logio_buff_get(0, LOGIO_BUFF_TYPE_VT);
	
	/* 
	 * If we couldn't get a buffer, then free all the buffers we 
	 * already allocated, and return an error code.
	 */
	
	if (lbptr->next_buff == logio_buff_NULL)
	{
	    while (start)
            {
		tmp = start->next_buff;
		
		/* Use the function pointer within the buffer to  free the
		 * buffer */
		(*start->free)(start);
		
		start = tmp;
            }
	    /* This indicates that there are not enough buffs available */
	    return(1);
	}
	lbptr = lbptr->next_buff;
    }
    
    /* Return the pointer to the start of the list in listPP */
    *listPP = start;
    
    return(0);
}

/*************************************************************************
**
** logio_release_list
**
** Inputs:
**
**    tnxd:      Pointer to TNX structure: ignored in this 
**               function, but maintained to keep interface.
**    listP:     Pointer to buf list
**
** Outputs:
**
**      NONE
**
** Return codes:
**
**      NONE
**
** Method:
**
**
** Side-effects:
** Global variables:
**
*/
void logio_release_list (tnxd, listP)
void *tnxd;			/* not used-- kept for intrfc compat */
logio_buff_t *listP;		/* pointer to list of mbufs          */
{
    logio_buff_t *tptr;
    
    /* Go through the list of buffers and free each one */
    while(listP)
    {
      tptr = listP->next_buff;
      
      /* Use the function pointer within the buffer to free the buffer */
      (*listP->free)(listP);
      listP = tptr;
  }
}

#if defined(PPC860)
vbsp_return_t
logio_ether_dev_recv(ether_1_dev_desc_t *dev, logio_buff_t **pplogio)
{
    unsigned int dcache_status;
    vbsp_return_t status;

    dcache_status = cpu_cm_data_cache_disable();
    status = (*dev->fops.pkt_recv)(dev, pplogio);
    cpu_cm_data_cache_restore(dcache_status);

    return(status);
}

vbsp_return_t
logio_ether_dev_send(ether_1_dev_desc_t *dev, logio_buff_t *plogio)
{
    unsigned int dcache_status;
    vbsp_return_t status;

    dcache_status = cpu_cm_data_cache_disable();
    status = (*dev->fops.pkt_send)(dev, plogio);
    cpu_cm_data_cache_restore(dcache_status);

    return(status);
}

#endif /* defined(PPC860) */
