#include "stdafx.h"


//#############################################################################
//
//  Eeprom24LCxx.cpp - Implementation file for Eeprom24LCxx_t class.
//
//
//#############################################################################

#include "Eeprom24LCxx.h"

static const int EEPROM_ADDRESS_INDEX = 1;
static const int EEPROM_WRITE_DELAY = 100;               // Write times vary, this gives lots to be sure

//#############################################################################
//
//  Eeprom24LCxx_t::Eeprom24LCxx_t
//
//#############################################################################

Eeprom24LCxx_t::Eeprom24LCxx_t(HANDLE FileHandle, uint8_t BusAddress, I2cSubAddressMode_t SubAddrMode, I2cBaudIndex_t BaudIndex, DWORD Timeout)
                                : I2C(FileHandle, BusAddress, SubAddrMode, BaudIndex, Timeout)
{
}


//#############################################################################
//
//  Eeprom24LCxx_t::Write - This performs 8 byte writes to the 24LCxx.
//      Returns true if successful.
//
//#############################################################################

bool Eeprom24LCxx_t::Write(uint16_t EepromDestAddress, uint8_t *SourceAddress, int NumberOfBytes)
{
    static const uint8_t PAGE_SIZE  = 8;

    BYTE            TxData[PAGE_SIZE];
    int             BytesLeftToWrite = NumberOfBytes;
    int             SourceDataIndex = 0;
	uint16_t        DestEndAddress = EepromDestAddress;
    bool            RetVal = true;

    while (BytesLeftToWrite > 0)
    {
    	int				TxArrayIndex = 0;										                // Put write data into array starting here
        do
        {
            TxData[TxArrayIndex++] = SourceAddress[SourceDataIndex++];
        }
        while (((DestEndAddress++ & 0x7) != 0x7) && (--BytesLeftToWrite > 0));                  // Stop at page boudary, or when no data left

        if (I2C::WriteDevice(EepromDestAddress, TxData, TxArrayIndex) != I2C_SUCCESSFUL)        // Do the write, if failure, try again
        {
            Sleep(200);
            if (I2C::WriteDevice(EepromDestAddress, TxData, TxArrayIndex) != I2C_SUCCESSFUL)    // try again
            {
                RetVal = false;
                break;                                              // report error and get out
            }
        }
        EepromDestAddress = DestEndAddress;                                                     // continue where we left off
        Sleep(EEPROM_WRITE_DELAY);
    }

    return RetVal;
}


//#############################################################################
//
//  Eeprom24LCxx_t::Read - This performs a sequential read of the 24LCxx.
//          First the read address is set by performing a write operation,
//          then the desired bytes are read.
//      Returns true if successful.
//
//#############################################################################

bool Eeprom24LCxx_t::Read (uint16_t EepromSourceAddress, uint8_t *DestinationAddress, int NumberOfBytes)
{
    bool RetVal = true;

    if (I2C::ReadDevice(EepromSourceAddress, DestinationAddress, NumberOfBytes) != I2C_SUCCESSFUL) // Now read the data
    {
        RetVal = false;
    }

    return RetVal;
}


