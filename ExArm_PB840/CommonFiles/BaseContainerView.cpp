#include "stdafx.h"
// LowerBaseContainerView.cpp : implementation file
//

#ifdef SIGMA_GUIPC_CPU
#include "ExArm.h"
#else
#include "main_e600GUI.h"
#endif

#include "BaseContainerView.h"

#include "Touch.hh"



// BaseContainerView
////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(BaseContainerView, CView)

BaseContainerView::BaseContainerView()
{

}

BaseContainerView::~BaseContainerView()
{
}

BEGIN_MESSAGE_MAP(BaseContainerView, CView)
ON_WM_LBUTTONDOWN()
ON_WM_LBUTTONUP()
END_MESSAGE_MAP()

void BaseContainerView::OnDraw(CDC* pDC)
{
	// TODO: add draw code here

}

void BaseContainerView::OnLButtonDown(UINT nFlags, CPoint point)
{
    int adjustedY = 0;
    int adjustedX = 0;

	//Adding GuiFoundation::SCREEN_HEIGHT_PIXELS since lower and upper view are considered as a single y axis co-ordinate
    adjustedY = point.y + GetOriginY();
	adjustedX = point.x;

	touchDriver_.handleCoordinateReport_(adjustedX, adjustedY, TouchDriver::TOUCH_PRESSED);

	CView::OnLButtonDown(nFlags, point);
}

void BaseContainerView::OnLButtonUp(UINT nFlags, CPoint point)
{
    int adjustedY = 0;
    int adjustedX = 0;

	//x, y cordinates are discarded on touch release since its already captured on touch pressed.
	touchDriver_.handleCoordinateReport_(adjustedX, adjustedY, TouchDriver::TOUCH_RELEASED);

	CView::OnLButtonUp(nFlags, point);
}

LONG BaseContainerView::GetOriginY()
{
	return 0;
}
