#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: ServiceModeNovramData - Service Mode data stored in novram
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is used for storage and retrieval of Service Mode novram
//	data.
//---------------------------------------------------------------------
//@ Rationale
//  The ServiceModeNovramData class encapsulates the data and access methods
//	for Service Mode data stored in novram.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Private data members with read and write access methods are defined.
//---------------------------------------------------------------------
//@ Fault-Handling
//  n/a
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ServiceModeNovramData.ccv   25.0.4.0   19 Nov 2013 14:23:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//   Revision: 001  By: quf    Date: 15-Sep-1997   DR Number: DCS 2385, 2410, 2525
//      Project:   Sigma (840)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "ServiceModeNovramData.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "ForceVentInopTest.hh"

#include "NovRamManager.hh"

#ifdef SIGMA_UNIT_TEST
#include "ServiceModeNovramData_UT.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceModeNovramData [constructor]
//
//@ Interface-Description
//  Constructor.  No arguments.  Initializes data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize data members.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ServiceModeNovramData::ServiceModeNovramData(void) :
	flowSensorCalStatus_(ServiceMode::NEVER_RUN)
    ,exhValveCalStatus_(ServiceMode::NEVER_RUN)
	,atmPresXducerCalStatus_(ServiceMode::NEVER_RUN)
	,ventInopTestPassed_(TRUE)
	,ventInopTestPhaseId_(ForceVentInopTest::START)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceModeNovramData [copy constructor]
//
//@ Interface-Description
//  Copy constructor.  Makes a copy of the argument ServiceModeNovramData object.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Create a copy of the argument ServiceModeNovramData object.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ServiceModeNovramData::ServiceModeNovramData(
				const ServiceModeNovramData& rServiceModeNovramData) :
	flowSensorCalStatus_(rServiceModeNovramData.flowSensorCalStatus_)
	,exhValveCalStatus_(rServiceModeNovramData.exhValveCalStatus_)
	,atmPresXducerCalStatus_(rServiceModeNovramData.atmPresXducerCalStatus_)
	,ventInopTestPassed_(rServiceModeNovramData.ventInopTestPassed_)
	,ventInopTestPhaseId_(rServiceModeNovramData.ventInopTestPhaseId_)
{
// $[TI2]
    CALL_TRACE("ServiceModeNovramData(const ServiceModeNovramData&)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceModeNovramData [destructor]
//
//@ Interface-Description
//  Destructor for the ServiceModeNovramData.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ServiceModeNovramData::~ServiceModeNovramData(void)
{
    CALL_TRACE("~ServiceModeNovramData(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=()
//
//@ Interface-Description
//	This method overloads the = operator, performing an equate of
// 	corresponding data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If lvalue object is not the same as the rvalue object, equate each
//	lvalue object member to the corresponding rvalue object member,
//	else leave the lvalue object alone.
//	Return the lvalue object.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ServiceModeNovramData&
ServiceModeNovramData::operator=(
				const ServiceModeNovramData& rServiceModeNovramData2)
{
	CALL_TRACE("ServiceModeNovramData::operator=( \
				const ServiceModeNovramData& rServiceModeNovramData2)");

	if (this != &rServiceModeNovramData2)
	{
	// $[TI1]
		flowSensorCalStatus_ =
						rServiceModeNovramData2.flowSensorCalStatus_;
		exhValveCalStatus_ =
						rServiceModeNovramData2.exhValveCalStatus_;
     	atmPresXducerCalStatus_ =
     					rServiceModeNovramData2.atmPresXducerCalStatus_;
		ventInopTestPassed_ =
						rServiceModeNovramData2.ventInopTestPassed_;
		ventInopTestPhaseId_ =
						rServiceModeNovramData2.ventInopTestPhaseId_;

#if defined( SIGMA_UNIT_TEST)
		ServiceModeNovramData_UT::ObjectsEquated = TRUE;
#endif  // defined(SIGMA_UNIT_TEST)
	}
	// $[TI2]
	// implied else: object is being equated to itself, so do nothing

	return( *this);
}


#ifdef SIGMA_BD_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetFlowSensorCalStatus [static]
//
//@ Interface-Description
//  This method is used to update the flow sensor cal status in novram.
//	Argument: new status.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update local object from novram, set new value of flowSensorCalStatus_,
//	write updated object to novram.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
ServiceModeNovramData::SetFlowSensorCalStatus( const ServiceMode::TestStatus status)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::SetFlowSensorCalStatus( \
		    								const ServiceMode::TestStatus status)");

	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	smNovramData.flowSensorCalStatus_ = status;
	NovRamManager::UpdateServiceModeState( smNovramData);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetFlowSensorCalStatus [static]
//
//@ Interface-Description
//  This method is used to read the flow sensor cal status from
//	novram.  No arguments, returns the flow sensor cal status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Load local object from novram then return value of flowSensorCalStatus_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
ServiceMode::TestStatus
ServiceModeNovramData::GetFlowSensorCalStatus( void)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::GetFlowSensorCalStatus( void)");

	// TODO E600 MS This is a hack. Remove when the whole flow sensor cal situation is resolved and
	// remove the following #if 0
	return ServiceMode::PASSED;

#if 0
	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	return( smNovramData.flowSensorCalStatus_);
#endif
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetExhValveCalStatus [static]
//
//@ Interface-Description
//  This method is used to update the exh valve cal status in novram.
//	Argument: new exh valve cal status.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update local object from novram, set new value of exhValveCalStatus_,
//	write updated object to novram.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
ServiceModeNovramData::SetExhValveCalStatus( const ServiceMode::TestStatus status)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::SetExhValveCalStatus( \
						    				const ServiceMode::TestStatus status)");

	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	smNovramData.exhValveCalStatus_ = status;
	NovRamManager::UpdateServiceModeState( smNovramData);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetExhValveCalStatus [static]
//
//@ Interface-Description
//  This method is used to read the exh valve cal status from
//	novram.  No arguments, returns the exh valve cal status flag.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Load local object from novram then return value of exhValveCalStatus_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
ServiceMode::TestStatus
ServiceModeNovramData::GetExhValveCalStatus( void)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::GetExhValveCalStatus( void)");

	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	return( smNovramData.exhValveCalStatus_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetAtmPresXducerCalStatus [static]
//
//@ Interface-Description
//  This method is used to update the atm pressure transducer cal status
//	in novram.  Argument: new status.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update local object from novram, set new value of atmPresXducerCalStatus_,
//	write updated object to novram.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
ServiceModeNovramData::SetAtmPresXducerCalStatus( const ServiceMode::TestStatus status)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::SetAtmPresXducerCalStatus( \
						    				const ServiceMode::TestStatus status)");

	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	smNovramData.atmPresXducerCalStatus_ = status;
	NovRamManager::UpdateServiceModeState( smNovramData);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAtmPresXducerCalStatus [static]
//
//@ Interface-Description
//  This method is used to read the atm pressure transducer cal status from
//	novram.  No arguments, returns the atm pressure cal status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Load local object from novram then return value of atmPresXducerCalStatus_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
ServiceMode::TestStatus
ServiceModeNovramData::GetAtmPresXducerCalStatus( void)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::GetAtmPresXducerCalStatus( void)");

	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	return( smNovramData.atmPresXducerCalStatus_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetVentInopTestPassed [static]
//
//@ Interface-Description
//  This method is used to update the vent inop test status
//	in novram.  Argument: new status.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update local object from novram, set new value of ventInopTestStatus_,
//	write updated object to novram.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
ServiceModeNovramData::SetVentInopTestPassed(const Boolean testPassed)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::SetVentInopTestStatus( \
								    		const Boolean testPassed)");

	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	smNovramData.ventInopTestPassed_ = testPassed;
	NovRamManager::UpdateServiceModeState( smNovramData);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsVentInopTestPassed [static]
//
//@ Interface-Description
//  This method is used to read the vent inop test status from
//	novram.  No arguments, returns the vent inop test status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Load local object from novram then return value of ventInopTestPassed_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Boolean
ServiceModeNovramData::IsVentInopTestPassed( void)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::IsVentInopTestPassed( void)");

	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	return( smNovramData.ventInopTestPassed_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetVentInopTestPhaseId [static]
//
//@ Interface-Description
//  This method is used to update the vent inop test phase ID
//	in novram.  Argument: new phase ID.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update local object from novram, set new value of ventInopTestPhaseId_,
//	write updated object to novram.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
ServiceModeNovramData::SetVentInopTestPhaseId(const Int16 phaseId)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::SetVentInopTestPhaseId( \
						    				const Inst16 phaseId)");

	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	smNovramData.ventInopTestPhaseId_ = phaseId;
	NovRamManager::UpdateServiceModeState( smNovramData);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetVentInopTestPhaseId [static]
//
//@ Interface-Description
//  This method is used to read the vent inop test phase ID from
//	novram.  No arguments, returns the vent inop test phase ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Load local object from novram then return value of ventInopTestPhaseId_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Int16
ServiceModeNovramData::GetVentInopTestPhaseId( void)
{
// $[TI1]
    CALL_TRACE("ServiceModeNovramData::GetVentInopTestPhaseId( void)");

	ServiceModeNovramData smNovramData;
	NovRamManager::GetServiceModeState( smNovramData);
	return( smNovramData.ventInopTestPhaseId_);
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
ServiceModeNovramData::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, SERVICEMODENOVRAMDATA, 
                          lineNumber, pFileName, pPredicate);
}

#endif  // SIGMA_BD_CPU


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================
