#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: Printf - Used to ensure null functionality in PRODUCTION mode.
//---------------------------------------------------------------------
//@ Interface-Description
//  The purpose of this file is to override and nullify the standard 
//  I/O functions fgetc() and fputc() when the system is compiled for
//  PRODUCTION. This is a precaution to assure that no task issues an
//  I/O request that would place the task in an interminable I/O wait 
//  state since the PRODUCTION kernel is configured without run-time
//  support for console input/output.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Build/vcssrc/Printf.ccv   25.0.4.0   19 Nov 2013 14:01:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: gdc    Date:  15-Sep-1998    DR Number: 5159
//  Project:  Color
//  Description:
//	Override the lower level I/O functions fgetc() and fputc() instead
//  of the higher level printf().
//
//  Revision: 001  By: sah    Date:  17-Dec-1997    DR Number: None
//  Project:  Sigma (R8027)
//  Description:
//	Initial version (Integration baseline).
//
//=====================================================================

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//  Global Function...
//
//=====================================================================

#if defined(SIGMA_PRODUCTION)

#include <stdio.h>

extern int
fputc(_ANSIPROT2(int, FILE *))
{
	return EOF;
}

extern int
fgetc(_ANSIPROT1(FILE *))
{
	return EOF;
}

#endif // defined(SIGMA_PRODUCTION)
