#ifndef SoftwareOptionsSubScreen_HH
#define SoftwareOptionsSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2008, Covidien
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SoftwareOptionsSubScreen - Subscreen which allows users to 
// configure ventilator's software options.  
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SoftwareOptionsSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  mnr    Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Implemented NeoMode Advance and Lockout options.
//
//  Revision: 003  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 002  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 001    By: rhj    Date: 20-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//		Initial version.
//
//====================================================================

//@ Usage-Classes
#include "SubScreen.hh"
#include "SubScreenTitleArea.hh"
#include "TextField.hh"
#include "GuiAppClassIds.hh"
#include "Bitmap.hh"
#include "Box.hh"
#include "Line.hh"
#include "TextButton.hh"
#include "AdjustPanelTarget.hh"
#include "ServicePromptMgr.hh"

//@ End-Usage

class SubScreenArea;

class SoftwareOptionsSubScreen : public SubScreen,
	public AdjustPanelTarget,
	public ServicePromptMgr

{
public:
	SoftwareOptionsSubScreen(SubScreenArea* pSubScreenArea);
	~SoftwareOptionsSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	virtual void buttonDownHappened(Button *pButton, Boolean byOperatorAction);
    virtual void buttonUpHappened(Button *, Boolean);
									

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelRestoreFocusHappened(void);
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void  adjustPanelClearPressHappened(void);


	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

	private:
	// these methods are purposely declared, but not implemented...
	SoftwareOptionsSubScreen(void);								// not implemented...
	SoftwareOptionsSubScreen(const SoftwareOptionsSubScreen&);	// not implemented...
	void operator=(const SoftwareOptionsSubScreen&);				// not implemented...
	void clearPrompts_(void);

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen
	SubScreenTitleArea titleArea_;

	//@ Data-Member: softwareOptionBiLevelLabel_;
	// The text field for displaying the label of the BiLevel Software Option
	TextField softwareOptionBiLevelLabel_;

	//@ Data-Member: softwareOptionNeoModeLabel_;
	// The text field for displaying the label of the NeoMode Software Option
	TextField softwareOptionNeoModeLabel_;

	//@ Data-Member: softwareOptionTCLabel_;
	// The text field for displaying the label of the TC Software Option
	TextField softwareOptionTCLabel_;

	//@ Data-Member: softwareOptionVtpcLabel_;
	// The text field for displaying the label of the VTPC Software Option
	TextField softwareOptionVtpcLabel_;

	//@ Data-Member: softwareOptionPavLabel_;
	// The text field for displaying the label of the PAV Software Option
	TextField softwareOptionPavLabel_;

	//@ Data-Member: softwareOptionRespMechLabel_;
	// The text field for displaying the label of the Resp Mech Software Option
	TextField softwareOptionRespMechLabel_;

	//@ Data-Member: softwareOptionTrendingLabel_;
	// The text field for displaying the label of the Trending Software Option
	TextField softwareOptionTrendingLabel_;

	//@ Data-Member: softwareOptionLeakCompLabel_;
	// The text field for displaying the label of the Leak Comp Software Option
	TextField softwareOptionLeakCompLabel_;

	//@ Data-Member: softwareOptionNeoModeUpdateLabel_;
	// The text field for displaying the label of the NeoMode Update Software Option
	TextField softwareOptionNeoModeUpdateLabel_;

	//@ Data-Member: softwareOptionNeoModeAdvancedLabel_;
	// The text field for displaying the label of the NeoMode Advanced Software Option
	TextField softwareOptionNeoModeAdvancedLabel_;

	//@ Data-Member: softwareOptionNeoModeLockoutLabel_;
	// The text field for displaying the label of the NeoMode Lockout Software Option
	TextField softwareOptionNeoModeLockoutLabel_;

	//@ Data-Member: optionBiLevelButton_
	// The button that is displayed whenever the BiLevel option is available. 
	// The user can press this button to enable or disable the option.
	Button optionBiLevelButton_;

	//@ Data-Member: optionNeoModeButton_
	// The button that is displayed whenever the NeoMode option is available. 
	// The user can press this button to enable or disable the option.
	Button optionNeoModeButton_;

	//@ Data-Member: optionTCButton_
	// The button that is displayed whenever the TC option is available. 
	// The user can press this button to enable or disable the option.
	Button optionTCButton_;

	//@ Data-Member: optionVtpcButton_
	// The button that is displayed whenever the VTPC option is available. 
	// The user can press this button to enable or disable the option.
	Button optionVtpcButton_;

	//@ Data-Member: optionPavButton_
	// The button that is displayed whenever the PAV option is available. 
	// The user can press this button to enable or disable the option.
	Button optionPavButton_;

	//@ Data-Member: optionRespMechButton_
	// The button that is displayed whenever the Resp Mech option is available. 
	// The user can press this button to enable or disable the option.
	Button optionRespMechButton_;

	//@ Data-Member: optionTrendingButton_
	// The button that is displayed whenever the Trending option is available. 
	// The user can press this button to enable or disable the option.
	Button optionTrendingButton_;


	//@ Data-Member: optionLeakCompButton_
	// The button that is displayed whenever the Leak Comp option is available. 
	// The user can press this button to enable or disable the option.
	Button optionLeakCompButton_;

	//@ Data-Member: optionNeoModeUpdateButton_
	// The button that is displayed whenever the NeoMode Update option is available. 
	// The user can press this button to enable or disable the option.
	Button optionNeoModeUpdateButton_;

	//@ Data-Member: optionNeoModeAdvancedButton_
	// The button that is displayed whenever the NeoMode Advanced option is available. 
	// The user can press this button to enable or disable the option.
	Button optionNeoModeAdvancedButton_;

	//@ Data-Member: optionNeoModeLockoutButton_
	// The button that is displayed whenever the NeoMode Lockout option is available. 
	// The user can press this button to enable or disable the option.
	Button optionNeoModeLockoutButton_;

	//@ Data-Member: optionBiLevelEnabledBitmap_
	// The bitmap that depicts that the BiLevel option is enabled.
	Bitmap optionBiLevelEnabledBitmap_;

	//@ Data-Member: optionNeoModeEnabledBitmap_
	// The bitmap that depicts that the NeoMode option is enabled.
	Bitmap optionNeoModeEnabledBitmap_;

	//@ Data-Member: optionTCEnabledBitmap_
	// The bitmap that depicts that the TC option is enabled.
	Bitmap optionTCEnabledBitmap_;

	//@ Data-Member: optionVtpcEnabledBitmap_
	// The bitmap that depicts that the VTPC option is enabled.
	Bitmap optionVtpcEnabledBitmap_;

	//@ Data-Member: optionPavEnabledBitmap_
	// The bitmap that depicts that the PAV option is enabled.
	Bitmap optionPavEnabledBitmap_;

	//@ Data-Member: optionRespMechEnabledBitmap_
	// The bitmap that depicts that the RespMech option is enabled.
	Bitmap optionRespMechEnabledBitmap_;

	//@ Data-Member: optionTrendingEnabledBitmap_
	// The bitmap that depicts that the Trending option is enabled.
	Bitmap optionTrendingEnabledBitmap_;

	//@ Data-Member: optionLeakCompEnabledBitmap_
	// The bitmap that depicts that the Leak Comp option is enabled.
	Bitmap optionLeakCompEnabledBitmap_;

	//@ Data-Member: optionNeoModeUpdateEnabledBitmap_
	// The bitmap that depicts that the NeoMode Update option is enabled.
	Bitmap optionNeoModeUpdateEnabledBitmap_;

	//@ Data-Member: optionNeoModeUpdateAdvancedBitmap_
	// The bitmap that depicts that the NeoMode Advanced option is enabled.
	Bitmap optionNeoModeAdvancedEnabledBitmap_;

	//@ Data-Member: optionNeoModeLockoutEnabledBitmap_
	// The bitmap that depicts that the NeoMode Lockout option is enabled.
	Bitmap optionNeoModeLockoutEnabledBitmap_;

	//@ Data-Member: backButton_
	// A Back button to allow transition from the Software Options Subscreen
	// back to the Development Options Subscreen.
	TextButton backButton_;

	//@ Data-Member: setAllButton_
	// A text button which sets all options to enabled.
	TextButton setAllButton_;

	//@ Data-Member: clearAllButton_
	// A text button which sets all options to disabled.
	TextButton clearAllButton_;

	//@ Data-Member: optionsResetButton_
	// A text button which reset all options to the datakey's values
	TextButton  optionsResetButton_;

	//@ Data-Member: pCurrentButton_
	// The pCurrentButton_ is used for remembering the previously button
	// which was pressed by the user.
	Button *pCurrentButton_;

};

#endif // SoftwareOptionsSubScreen_HH 
