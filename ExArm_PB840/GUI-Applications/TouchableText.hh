#ifndef TouchableText_HH
#define TouchableText_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: TouchableText - Touch-sensitive text objects which can display
// an associated message in the Message Area.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TouchableText.hhv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "TextField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class TouchableText : public TextField
{
public:
    TouchableText(StringId textId = NULL_STRING_ID, StringId msgId = NULL_STRING_ID);
    ~TouchableText(void);

	void setMessage(StringId msgId);

    virtual void touch(Int x, Int y);
    virtual void leave(Int x, Int y);
    virtual void release(void);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint        lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
protected:

private:
    // these methods are purposely declared, but not implemented...
    TouchableText(const TouchableText&);	// not implemented...
    void operator=(const TouchableText&);	// not implemented...

	//@ Data-Member: msgId_
	// The message, if any, to be displayed in the lower screen message
	// area as long as the operator has her finger on this text object.  If
	// the value is NULL_STRING_ID, then no message is displayed.
	StringId msgId_;
};


#endif // TouchableText_HH 
