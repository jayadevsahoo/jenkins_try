#ifndef ServiceLowerScreen_HH
#define ServiceLowerScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceLowerScreen - Contains and displays the
// four areas that constitute the Service Lower screen during 
// Service mode:  Service Lower Subscreen Area, Service Lower Screen Select
// Area, Message Area and Prompt Area.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceLowerScreen.hhv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 003   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//    
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"
#include "GuiEventTarget.hh"

//@ Usage-Classes
#include "MessageArea.hh"
#include "ServiceLowerScreenSelectArea.hh"
#include "LowerSubScreenArea.hh"
#include "PromptArea.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ServiceLowerScreen : public Container, public GuiEventTarget
{
public:
	ServiceLowerScreen(void);
	~ServiceLowerScreen(void);

	// Event handlers for the Same/New Patient Setup sequence
	void beginSst(void);
	void beginServiceInitialization(void);
	void beginServiceMode(void);

	inline ServiceLowerScreenSelectArea *getServiceLowerScreenSelectArea(void);
	inline LowerSubScreenArea    *getLowerSubScreenArea(void);
	inline PromptArea            *getPromptArea(void);
	inline MessageArea           *getMessageArea(void);

	// GuiEventTarget virtual method
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	// Initialize the Service Lower Screen memory.
	static inline void Initialize(void);

	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	// Reference to ServiceLowerScreen object.
	static ServiceLowerScreen &RServiceLowerScreen;
protected:

private:
	// these methods are purposely declared, but not implemented...
	ServiceLowerScreen(const ServiceLowerScreen&);		// not implemented...
	void   operator=(const ServiceLowerScreen&);	// not implemented...

	//@ Type: ServiceLowerScreenId
	// Lists the possible screen layout scenarios.
	enum ServiceLowerScreenId
	{
		SERVICE_NO_SCREEN,
		BEGIN_SST_MODE,
		BEGIN_FLOW_SENSOR_MODE,
		BEGIN_SERVICE_MODE,
		BEGIN_EST_MODE,
		BEGIN_SETUP_MODE,
		BEGIN_EXIT_SERVICE_MODE_MODE,
		BEGIN_LOWER_OTHER_SCREEN_MODE_MODE,
		SERVICE_MAX_NUM_SCREEN		
	};

	//@ Data-Member: currentScreen_
	// Flag which indicates which Screen is currently active
	ServiceLowerScreenId currentScreen_;

	//@ Data-Member: serviceLowerScreenSelectArea_
	// The Service Status Area
	ServiceLowerScreenSelectArea serviceLowerScreenSelectArea_;

	//@ Data-Member: serviceLowerSubScreenArea_
	// The Service Lower Sub-screen Area
	LowerSubScreenArea serviceLowerSubScreenArea_;

	//@ Data-Member: promptArea_
	// The Prompt Area
	PromptArea promptArea_;

	//@ Data-Member: messageArea_
	// The Message Area
	MessageArea messageArea_;

};

// Inlined methods
#include "ServiceLowerScreen.in"

#endif // ServiceLowerScreen_HH 
