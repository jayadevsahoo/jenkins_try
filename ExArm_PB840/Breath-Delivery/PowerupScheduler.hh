
#ifndef PowerupScheduler_HH
#define PowerupScheduler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PowerupScheduler - The scheduler that is active on power up
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PowerupScheduler.hhv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 003 By:  iv   Date:   15-Feb-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "BreathPhaseScheduler.hh"

//@ Usage-Classes
//@ End-Usage

class PowerupScheduler : public BreathPhaseScheduler
{
  public:
    PowerupScheduler(void);
    virtual ~PowerupScheduler(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
    virtual void determineBreathPhase(const BreathTrigger& breathTrigger);
    virtual void takeControl(const BreathPhaseScheduler& scheduler);
    virtual void relinquishControl(const ModeTrigger& modeTrigger);
	void initialize(void);
	
  protected:
	virtual EventData::EventStatus reportEventStatus_(const EventData::EventId id,
													 const Boolean eventStatus);
	virtual void enableTriggers_(void);

  private:
    PowerupScheduler(const PowerupScheduler&);		// not implemented...
    void   operator=(const PowerupScheduler&);	// not implemented...
};


#endif // PowerupScheduler_HH 
