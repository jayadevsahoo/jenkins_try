#ifndef EventFilter_IN
#define EventFilter_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: EventFilter - implements a filter for reporting events
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/EventFilter.inv   10.7   08/17/07 09:29:14   pvcs  
//
//@ Modification-Log
//
//  Revision: 001  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//			Initial revision
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerAlarmCallBack()  
//
//@ Interface-Description
//		This method has a pointer to a function that is to be called to post
//		an alarm as an argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		sets the argument passed in to pAlarmCallBack_
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
inline void
EventFilter::registerAlarmCallBack( PAlarmCallBack pAlarmCallBack )
{
	CALL_TRACE("EventFilter::registerAlarmCallBack( PAlarmCallBack pAlarmCallBack )") ;

	// $[TI1]
 	pAlarmCallBack_ = pAlarmCallBack ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerVentStatusCallBack()  
//
//@ Interface-Description
//		This method has a pointer to a function that is to be called to post
//		vent status messages as an argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		sets the argument passed in to pVentStatusCallBack__
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
inline void
EventFilter::registerVentStatusCallBack( PVentStatusCallBack pVentStatusCallBack )
{
	CALL_TRACE("EventFilter::registerVentStatusCallBack( PVentStatusCallBack pVentStatusCallBack )") ;
	
	// $[TI1]
    pVentStatusCallBack_ = pVentStatusCallBack;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#endif // EventFilter_IN 


