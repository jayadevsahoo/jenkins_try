//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientData.h
//----------------------------------------------------------------------------

#ifndef PATIENTDATADBHANDLER_H
#define PATIENTDATADBHANDLER_H


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "IEventLogger.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class PatientDataDBHandler
{
    public:

    /// @brief register_types registers event callbacks with pIEventLogger.
    /// 
    /// @param pIEventLogger A pointer to a IEventLogger interface.
    /// @param pUser A pointer to user defined data to be passed to callbacks 
    ///              at runntime. 
    /// 
    /// @return true.
    static bool register_types(IEventLogger *pIEventLogger, void *pUser);


    private:
    PatientDataDBHandler();  ///Not used
    ~PatientDataDBHandler(); ///Not used

};


#endif

