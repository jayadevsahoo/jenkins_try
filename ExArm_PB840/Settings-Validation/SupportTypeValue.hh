
#ifndef SupportTypeValue_HH
#define SupportTypeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  SupportTypeValue - Values of Support Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SupportTypeValue.hhv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//	PAV project-related changes:
//      *  added new 'PA' value
//
//  Revision: 004  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new 'VS' value
//
//  Revision: 003   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  re-ordered values to separate standard values from optional
//         values; now matches order in SRS
//
//  Revision: 002   By: sah   Date:  04-Feb-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new 'ATC' value
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage

struct SupportTypeValue
{
  //@ Type:  SupportTypeValueId
  // All of the possible values of the Support Type Setting.
  enum SupportTypeValueId
  {
    // $[02248] -- values of the range of Support Type Setting...

    // standard values...
    OFF_SUPPORT_TYPE,
    PSV_SUPPORT_TYPE,

    // optional values...
    ATC_SUPPORT_TYPE,
    VSV_SUPPORT_TYPE,
    PAV_SUPPORT_TYPE,

    TOTAL_SUPPORT_TYPES
  };
};


#endif // SupportTypeValue_HH 
