#ifndef AlarmSilenceResetHandler_HH
#define AlarmSilenceResetHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmSilenceResetHandler - Handles pressing of the Alarm Silence and
// Alarm Reset keys.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmSilenceResetHandler.hhv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//  Revision: 017 By: srp    Date: 28-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 005  By: erm     Date:  13-Dec-2001    DCS Number: 5976
//  Project: GuiComms
//  Description:
//  Modified to use OsTimeStamp instead of TimeStamp
//
//  Revision: 004  By:  hhd	   Date:  08-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//	Modified to provide additional off-screen key management, whereby Alarm Silence can be cancelled.
//
//  Revision: 003  By:  yyy	   Date:  22-Sept-1999    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//	Initial version.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//	Initial version.
//
//  Revision: 001  By:  mpm Date:  22-AUG-95	DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//  	Integration baseline.
//====================================================================

//@ Usage-Classes
#include "KeyPanelTarget.hh"
#include "GuiEventTarget.hh"
#include "GuiTimerTarget.hh"
#include "GuiAppClassIds.hh"
#include "OsTimeStamp.hh"

//@ End-Usage

class AlarmSilenceResetHandler :	public KeyPanelTarget,
									public GuiEventTarget,
									public GuiTimerTarget
{
public:
	AlarmSilenceResetHandler(void);
	~AlarmSilenceResetHandler(void);


	// Key Panel Target virtual methods
	virtual void keyPanelPressHappened(KeyPanel::KeyId key);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId key);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	// GuiEventTarget virtual method
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	static void Cancel(void);
	static void ClearDisplay(void);
	
	Boolean hardToSoftAlarmOccured(void);
	
	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	AlarmSilenceResetHandler(const AlarmSilenceResetHandler&);// not implemented...
	void   operator=(const AlarmSilenceResetHandler&);		// not implemented...

	//@ Data-Member: manualAlarmsResetRequested_
	// Flag indicates if the alarm silence is due to alarm reset key.
	static Boolean manualAlarmsResetRequested_;

	//@ Data-Member: startTime_
	// OsTime stamp for the time when alarm silence is requested.
	OsTimeStamp startTime_;

};

#endif // AlarmSilenceResetHandler_HH
