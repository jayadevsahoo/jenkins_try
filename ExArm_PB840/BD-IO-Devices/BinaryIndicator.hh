
#ifndef BinaryIndicator_HH
#define BinaryIndicator_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BinaryIndicator - digital port indicator class
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BinaryIndicator.hhv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

//@ Usage-Classes
#include "BitAccessGpio.hh"

//@ End-Usage


class BinaryIndicator
{
  public:

    enum IndicatorState { OFF, ON };

    BinaryIndicator( const ApplicationDigitalInputs_t bit, BitAccessGpio& bitAccess);
    virtual ~BinaryIndicator( void );

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL,
						   const char*       pPredicate = NULL );

    const IndicatorState getState( void );

  protected:
    ApplicationDigitalInputs_t  bit_;

    BitAccessGpio&  rBitAccessGpio_;

  private:
	  BinaryIndicator& operator=(const BinaryIndicator& rhs); 

};

#endif // BinaryIndicator_HH

