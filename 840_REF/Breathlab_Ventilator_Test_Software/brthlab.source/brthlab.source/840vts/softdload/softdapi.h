/*****************************************************************************/
/*   Module Name:    softdApi.h                                              */
/*   Purpose:        This module contains the code which interfaces          */
/*                   Cvi to DLL( BootP App)                                  */
/*                                                                           */
/*   Requirements:   This module implements requirements detailed in         */
/*                   75205.93                                                */
/*                                                                           */
/*   $Revision::   1.1      $                                                */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.

Copyright (c) 1998, Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/
#if !defined _SOFTD_API
#define _SOFTD_API

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

#define DllExport    __declspec( dllexport )

struct ERROR_XREF
{
    const short err_no;                          /* Error number      */
    char * const err_str;                        /* Error description */
};

 enum err{
           ERR_NO_ERROR,
           ERR_TIMEOUT,
           ERR_WINSOCK, 
           ERR_OUT_OF_MEMORY,
           ERR_SOCKET_LISTEN,
           ERR_SOCKET_TRANSMIT,
           ERR_SOCKET_CREATION,
           ERR_SOCKET_BIND,
           ERR_SOCKET_RECIEVE,
           ERR_VENT_STATE,
           ERR_VENT_ERROR,
           ERR_FILE_NOT_FOUND,
           ERR_SYSTEM_SEQ,
           ERR_VENT_REQUEST,
           ERR_END_OF_FILE,
 };

static struct ERROR_XREF const error_xref[] =    
  {
     ERR_NO_ERROR,          "STATUS OK",
     ERR_TIMEOUT,           "TIMEOUT ",
     ERR_WINSOCK,           "WINDOWS SOCKET LAYER ERROR",
     ERR_OUT_OF_MEMORY,     "OUT OF MEMORY",
     ERR_SOCKET_LISTEN,     "BAD LISTEN SOCKET",
     ERR_SOCKET_TRANSMIT,   "BAD TRANSMIT ON SOCKET",
     ERR_SOCKET_CREATION,   "BAD SOCKET CREATION",
     ERR_SOCKET_BIND,       "BAD SOCKET BIND",
     ERR_SOCKET_RECIEVE,    "BAD RECEIVING ON SOCKET",
     ERR_VENT_STATE,        "INCORRECT VENT STATE RECEIVED",
     ERR_VENT_ERROR,        "VENT REPORTS ERROR CONDITION",
     ERR_FILE_NOT_FOUND,    "TRANSMIT FILE NOT FOUND",
     ERR_SYSTEM_SEQ,        "BAD SYSTEM SEQUENCE",
     ERR_VENT_REQUEST,      "BAD VENT REQUEST OPTION",
     ERR_END_OF_FILE,       "END OF FILE ERROR",
  };


enum { 
        BOOTP_ERROR, 
        BOOTP_STOPPED,
        BOOTP_READY,
        GUI_REQUEST, 
        BD_REQUEST,
        BOOTP_BAD_CALLBACK
 };



DllExport typedef struct bootInfo{
    char *guiFileName;
    char *bdFileName;
    int  bdId;
    int  guiId;
    char *filePath;
    void (*pBootPCallback)(int status);   //callback for bootP
    void (*pTftpCallback )(int id,int cblock, int tblock); //callback for tftp
    int multiHomed;
    char *altAddress;
}BootInfo;


DllExport void StartSession( BootInfo *startUp, int *results);
DllExport int GetSessionLastError(void);
DllExport void EndSession(void);    



#endif

#ifdef __cplusplus
}
#endif
