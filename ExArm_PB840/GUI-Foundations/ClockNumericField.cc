#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ClockNumericField - This is a Drawable object which displays a numeric
// value.
//---------------------------------------------------------------------
//@ Interface-Description
// The ClockNumericField class manages the display of clock numbers on 
// the screen.  It is NumericField that formats the numeric data as a 
// digital clock would by filling with leading zeros for numbers less 
// than ten.
//---------------------------------------------------------------------
//@ Rationale
// The ClockNumericField class encapsulates the method required to properly
// format a digital clock number.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// This class uses the "contract programming" technique.  This technique
// utilizes the class assertion and class precondition macros provided by
// the Utilities subsystem to generate error messages in response to software
// faults.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/ClockNumericField.ccv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//=====================================================================

#include "ClockNumericField.hh"
#include <string.h>

//@ Code...


//=====================================================================
//
// Public methods
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ClockNumericField
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

ClockNumericField::ClockNumericField (TextFont::Size size, Uint numberOfDigits, 
								Alignment alignment) :
	NumericField(size, numberOfDigits, alignment)
{
	CALL_TRACE("ClockNumericField (TextFont::Size, Uint, Alignment)");

													// $[TI1]
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ClockNumericField
//
//@ Interface-Description
//      Destructor - Destroys object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This destructor does nothing.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

ClockNumericField::~ClockNumericField(void)
{
	CALL_TRACE("ClockNumericField::~ClockNumericField(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: draw	[virtual]
//
//@ Interface-Description
// Draws the numeric field on the screen.  The passed clipArea parameter is
// ignored.
//---------------------------------------------------------------------
//@ Implementation-Description
// The floating point number is converted to a string and then render()
// is called to paint the data on the screen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
ClockNumericField::draw(const Area &)
{
	CALL_TRACE("ClockNumericField::draw(const Area &)");
	
	AUX_CLASS_ASSERTION( 0 <= getValue() && getValue() <= 99, getValue() );

	wchar_t valueString[3] = {0};

	swprintf(valueString, L"%02.0f", getValue() );

	render(valueString );

	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ClockNumericField::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, GUI_FOUNDATIONS, CLOCK_NUMERIC_FIELD,
							lineNumber, pFileName, pPredicate);
}
