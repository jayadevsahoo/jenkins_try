#ifndef Button_HH
#define Button_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================
//====================================================================
// Class: Button - This is an all purpose general button.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Button.hhv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//  Revision: 010   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 009   By: gdc    Date:  21-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Made getButtonState() and getButtonType() const.
// 
//  Revision: 008   By: gdc    Date:  02-Oxt-2006    SCR Number: 6236
//  Project: RESPM
//  Description:
//	Added momentary push button type.
// 
//  Revision: 007   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
// 
//  Revision: 006   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 005  By:  hhd	   Date:  24-May-1999    DR Number: 5369 
//    Project:  Sigma (R8027)
//    Description:
//	Removed method setButtonType().  This was essentially an empty method since
//	all of its code was commented out.
//
//  Revision: 004   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//  
//  Revision: 003  By: gdc     Date: 16-Jun-1998     DCS Number:
//  Project:  Color
//  Description:
//      Initial release.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "Container.hh"

//@ Usage-Classes
#include "Colors.hh"

class ButtonTarget;
//@ End-Usage


class Button : public Container
{
public:
	//@ Type: ButtonState
	// This enum defines all valid states for a Button.
	enum ButtonState 
	{
		UP,
		DOWN,
		FLAT
	};
	
	//@ Type: ButtonType
	// This enum defines the style of button as seen in the UP position.
	enum ButtonType
	{
		LIGHT,
		LIGHT_NON_LIT,
		DARK
	};

	//@ Type: BorderType
	// Defines the Button border types.  That is, specifies whether or
	// not the Button features a surrounding box.
	enum BorderType
	{
		BORDER,
		NO_BORDER
	};

	//@ Type: CornerType
	// Defines choices for the graphical appearance of the Button
	// corners.
	enum CornerType
	{
		ROUND,
		SQUARE
	};

	enum PushButtonType
	{
		PB_TOGGLE,
		PB_MOMENTARY
	};

	Button(Int16 xOrg,  
		   Int16 yOrg, 
		   Int16 width, 
		   Int16 height, 
		   ButtonType buttonType,
		   Int16 bevelWidth, 
		   CornerType cornerType = SQUARE,
		   BorderType borderType = NO_BORDER, 
		   ButtonState buttonState = UP,
		   PushButtonType pushButtonType = PB_TOGGLE);
	virtual ~Button(void);

	// virtual from Drawable
	virtual void touch(Int x, Int y);
	virtual void leave(Int x, Int y);
	virtual void release(void);

	void setToUp(void);
	void setToDown(void);
	void setToFlat(void);
	void forceUp(void);
	void forceDown(void);

    Button::ButtonState getButtonState(void) const;

	void setButtonType(Button::ButtonType buttonType);
	Button::ButtonType getButtonType(void) const;

	void setBorderColor(Colors::ColorS borderColor);

	// virtual from Drawable
	virtual void draw(const Area& clipArea);

	inline void setButtonCallback(ButtonTarget* pButtonTarget);

	inline void addLabel(Drawable* pDrawable);
	inline void removeLabel(Drawable* pDrawable);

	static void SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName  = NULL,
						  const char*        pPredicate = NULL);

protected:
	virtual void upHappened_(Boolean isByOperatorAction);
	virtual void downHappened_(Boolean isByOperatorAction);

	inline ButtonState getState_(void) const;

private:
	Button(const Button&);			// Declared but not implemented
	void operator=(const Button&);	// Declared but not implemented

	void setState_(ButtonState newState);

	void drawButton_(void);
	void drawButtonUp_(void);
	void drawButtonDown_(void);
	void determineColors_(void);

	//@ Data-Member: buttonState_
	// Stores the current state of this button.  Its value should only
	// be altered by the private setState_() method, since a change in
	// this value must coincide with a change in the display graphics.
	ButtonState buttonState_;

	//@ Data-Member: labelContainer_
	// This container stores all of the labels (text, bitmaps, or
	// numbers) that appear inside the Button.  It is filled with
	// graphics by the object which is constructing the button.
	Container labelContainer_;

	//@ Data-Member: pButtonTarget_
	// Callback object which is notified when this button changes
	// state.
	ButtonTarget* pButtonTarget_;

	//@ Data-Member: xUpOffset_
	// This is the X part of the offset of the labelContainer_ from the 
	// top-left of the Button when the button is in the up state.
	Int16 xUpOffset_;

	//@ Data-Member: yUpOffset_
	// This is the Y part of the offset of the labelContainer_ from the 
	// top-left of the Button when the button is in the up state.
	Int16 yUpOffset_;

	//@ Data-Member: xDownOffset_
	// This is the X part of the offset of the labelContainer_ from the
	// top-left of the Button when the button is in the down state.
	Int16 xDownOffset_;

	//@ Data-Member: yDownOffset_
	// This is the Y part of the offset of the labelContainer_ from the
	// top-left of the Button when the button is in the down state.
	Int16 yDownOffset_;

	//@ Data-Member: bevelWidth_
	// Stores the width of the bevel edge.
	Int16 bevelWidth_;    

	//@ Data-Member: borderWidth_
	// Stores the width of the "surrounding box" border.
	Int16 borderWidth_;    

	//@ Data-Member: cornerType_
	// Stores the graphical appearance selection for the corner of the
	// button.
	CornerType cornerType_;  

	//@ Data-Member: buttonType_
	// Stores the type information for the button.
	ButtonType buttonType_;

	//@ Data-Member: INIT_BUTTON_TYPE_
	// Stores the initial type information for this button.
	const ButtonType INIT_BUTTON_TYPE_;

	//@ Data-Member: upperEdgeColor_ 
	// Stores the upper edge color for the button.
	Colors::ColorS upperEdgeColor_; 

	//@ Data-Member: lowerEdgeColor_; 
	// Stores the lower edge color for the button.
	Colors::ColorS lowerEdgeColor_; 

	//@ Data-Member: centerColor_
	// Stores the center color for the button.
	Colors::ColorS centerColor_; 

	//@ Data-Member: borderColor_
	// Stores the "surrounding box" border color for the button.
	Colors::ColorS borderColor_; 

	//@ Data-Member: pushButtonType_
	// Defines the type of push button as either a toggle button or momentary
	// push button.
	PushButtonType pushButtonType_;

};

// Inlined methods
#include "Button.in"

#endif // Button_HH 
