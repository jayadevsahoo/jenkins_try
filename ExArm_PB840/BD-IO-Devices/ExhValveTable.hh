
#ifndef ExhValveTable_HH
#define ExhValveTable_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ExhValveTable - Exhalation Valve Table contains calibration
//		information for the valve.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ExhValveTable.hhv   25.0.4.0   19 Nov 2013 13:54:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004 By: syw    Date: 23-Apr-1996   DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added gain and offset data members and access methods to meet
//			TUV design.  Added dumpTable() for debugging.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added operator=(), setNormalizedCommand(), and
//			setNormalizedCommand() methods.  TempCmdGainEntry has been
//			eliminated so that a 1 diminesional array of Uint16's is used
//			to store the normalized command	table.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"
//#include "CalibrationTable.h"

//@ Usage-Classes
//@ End-Usage

//@ Constant: PRESSURE_RANGE
// Maximum pressure for table
static const Int32 PRESSURE_RANGE = 105 ;

class ExhValveTable {
  public:

  	static const Uint32 EXH_VALVE_DATA_ARRAY_SIZE = 101;
	struct ExhValveCalStrcut
	{
		Real32 exhPressSamples		[EXH_VALVE_DATA_ARRAY_SIZE];
		Real32 exhPressDrvSamples   [EXH_VALVE_DATA_ARRAY_SIZE];
		Real32 dacSamples			[EXH_VALVE_DATA_ARRAY_SIZE];
	};

	struct ExhValveCalTable
	{
		ExhValveCalStrcut lowToHighData;
		ExhValveCalStrcut highToLowData;

	};

    ExhValveTable( void);
    ~ExhValveTable( void) ;

    void   operator=( const ExhValveTable& evTable) ;

    ExhValveTable( const ExhValveTable&);
	ExhValveTable( const ExhValveCalTable&);

	void initializeToDefault();

    static void SoftFault( const SoftFaultID softFaultID,
		  const Uint32     lineNumber,
		  const char*      pFileName  = NULL,
		  const char*      pPredicate = NULL) ;




    Real32* getPdrvTable()
    {
		return exhValveTable_.highToLowData.exhPressDrvSamples;
    }

    Real32* getP2Table()
    {
        return exhValveTable_.highToLowData.exhPressSamples;
    }

    Real32* getDacTable()
    {
		return exhValveTable_.highToLowData.dacSamples;
    }

	//get low-to-high EV cal table
	Real32* getPdrvTable_l2h()
    {
		return exhValveTable_.lowToHighData.exhPressDrvSamples;
    }

    Real32* getP2Table_l2h()
    {
		return exhValveTable_.lowToHighData.exhPressSamples;
    }

    Real32* getDacTable_l2h()
    {
		return exhValveTable_.lowToHighData.dacSamples;
    }

	void printHighToLowValues();
	void printLowToHighValues();
	void printTableContents();

	void printDataArray( Real32 *arrayPtr, Boolean printAsReal32 = TRUE );


#ifdef SIGMA_DEBUG
	void dumpTable( void) ;
#endif //SIGMA_DEBUG
  protected:

  private:

    // This is the calibration data that are actually used in the 880
	ExhValveCalTable exhValveTable_;

} ;

#endif // ExhValveTable_HH



