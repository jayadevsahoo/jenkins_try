 
#ifndef SIterC_AlarmUpdate_HH
#define SIterC_AlarmUpdate_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SIterC_AlarmUpdate - Iterator for all 'SListC_AlarmUpdate'
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/SIteratorC.H_v   25.0.4.0   19 Nov 2013 14:03:48   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
#include "SListC_AlarmUpdate.hh"
//@ End-Usage


class SIterC_AlarmUpdate
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    friend Ostream&  operator<<(Ostream& ostr,
                                const SIterC_AlarmUpdate& sIter);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  public:
    inline SIterC_AlarmUpdate(const SIterC_AlarmUpdate& sIter);
    inline SIterC_AlarmUpdate(const SListC_AlarmUpdate& sList);
    inline ~SIterC_AlarmUpdate(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);

    SigmaStatus  search(const AlarmUpdate&      item,
			const SearchFrom startFrom = FROM_FIRST);

    inline const AlarmUpdate&  currentItem(void) const;

    inline void  attachTo(const SIterC_AlarmUpdate& sIter);
    inline void  attachTo(const SListC_AlarmUpdate& sList);

    inline Boolean  operator==(const SIterC_AlarmUpdate& sIter) const;
    inline Boolean  operator!=(const SIterC_AlarmUpdate& sIter) const;

    inline Boolean  operator==(const SListC_AlarmUpdate& sList) const;
    inline Boolean  operator!=(const SListC_AlarmUpdate& sList) const;

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
    inline const SNodeC_AlarmUpdate*  getFirst_  (void) const;
    inline const SNodeC_AlarmUpdate*  getCurrent_(void) const;

    inline Boolean  isLegalNode_(const SNodeC_AlarmUpdate* pNode) const;

    inline void  setCurrent_(SNodeC_AlarmUpdate* pNewCurr);

  private:
    SIterC_AlarmUpdate(void);		// not implemented...

    //@ Data-Member:  pIterList_
    // A pointer to this iterator's constant list.
    const SListC_AlarmUpdate*  pIterList_;

    //@ Data-Member:  pIterCurrent_
    // A pointer to this iterator's current node.
    const SNodeC_AlarmUpdate*  pIterCurrent_;
};


// Inlined methods...
#include "SIterC_AlarmUpdate.in"


#endif  // SIterC_AlarmUpdate_HH
