; All rights reserved ADENEO EMBEDDED 2010
;==============================================================================
;             Texas Instruments OMAP(TM) Platform Software
; (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
;
; Use of this software is controlled by the terms and conditions found
; in the license agreement under which this software has been supplied.
;
;==============================================================================
;
;------------------------------------------------------------------------------
;
;  File:  memory_cfg.inc
;
;  This file is used to define g_oalAddressTable. This table is passed to
;  KernelStart to estabilish physical to virtual memory mapping. This table
;  is used also in memory OAL module to map between physical and virtual
;  memory addresses via OALPAtoVA/OALVAtoPA functions.
;
;  The config.bib file defines image memory layout ant it contains virtual
;  cached memory addresses which must be synchronized with g_oalAddressTable.
;  With each table change make sure that those constant are still valid.
;
;------------------------------------------------------------------------------
;  Export Definition

        EXPORT  g_oalAddressTable[DATA]
        
g_oalDeviceTable
	DCD     0xA5000000, 0x150000,   0x1000000,	0     ; CS5, LAN9115
    DCD     0xA6000000, 0x480000,   0x1000000,	0     ; L4 Core/Wakeup registers
    DCD     0xA7000000, 0x490000,   0x0100000,	0     ; L4 Peripheral
    DCD     0xA7100000, 0x680000,   0x1000000,	0     ; L3 registers
    DCD     0xA8100000, 0x6C0000,   0x1000000,	0     ; SMS registers
    DCD     0xA9100000, 0x6D0000,   0x1000000,	0     ; SDRC registers
    DCD     0xAa100000, 0x6E0000,   0x1000000,	0     ; GPMC registers
    DCD     0xAb100000, 0x402000,   0x0100000,	0     ; 64KB SRAM
    DCD     0xAb200000, 0x5C0000,   0x1000000,	0     ; IPSS interconnect
    DCD     0xAC200000, 0x000000,   0x0100000,	0     ; ROM
    DCD     0xAC300000, 0x080000,   0x0100000,	0     ; NAND Registers (FIFO)
    DCD     0x00000000, 0x000000,	0,			0     ; end of table

;------------------------------------------------------------------------------
;  Table format: cached address, physical address, size

g_oalAddressTable	
	IF BSP_SDRAM_BANK1_ENABLE
	IF BSP_512MB_RAM_ADDR_TABLE
	DCD     0x80000000, 0x80000000, 256     ; SDRAM for UI
	ELSE
	DCD     0x80000000, 0x80000000, 410     ; SDRAM for UI
	ENDIF
	ELSE
	DCD     0x80000000, 0x80000000, 128     ; SDRAM for UI
	ENDIF
	DCD     0x99A00000, 0x15000000,   2     ; CS5, LAN9115
    DCD     0x99C00000, 0x48000000,  16     ; L4 Core/Wakeup registers
    DCD     0x9AC00000, 0x49000000,   1     ; L4 Peripheral
    DCD     0x9AD00000, 0x68000000,  16     ; L3 registers
    DCD     0x9BD00000, 0x6C000000,  16     ; SMS registers
    DCD     0x9CD00000, 0x6D000000,  16     ; SDRC registers
    DCD     0x9DD00000, 0x6E000000,  16     ; GPMC registers
    DCD     0x9ED00000, 0x40200000,   1     ; 64KB SRAM
    DCD     0x9EE00000, 0x5C000000,  16     ; L3 Interconnect View of IVA2.2
    DCD     0x9FE00000, 0x00000000,   1     ; ROM
    DCD     0x9FF00000, 0x08000000,   1     ; NAND Registers (FIFO)
    DCD     0x00000000, 0x00000000,   0     ; end of table

;------------------------------------------------------------------------------

    END       
