// BPServer.cpp: implementation of the BootPServer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "softd.h"
#include "BPServer.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BootPServer::BootPServer()
{
	int x;

	/***** Zero out callbacks and state indicators *****/
	bootPCallback = NULL;
	tftpCallback = NULL;
	nextTftpClient = 0;
	active = 0;
	lastError = 0;
	socketReady = 0;
	multiHomed = 0;
	bdFileName[0] = 0;
	guiFileName[0] = 0;
	altAddress[0] = 0;

	/*********** Zero Clients ************/
	for(x = 0; x < MAX_TFTP_CLIENTS; x++){
		connectList[x] = NULL;
	}

	/******** do not accept msg's yet ************/
	readyForMsg = FALSE;
}

//////////////////////////////////////////////////////////////////////
BootPServer::~BootPServer()
{
	int x;

	/******* free all allocated memory  ********/

	if(incomingSocket != NULL){
		delete incomingSocket;
	}

	if(tftpSocket != NULL ){
		delete tftpSocket;
	}

	// zero  clients	
	for(x = 0; x <= nextTftpClient; x++){
		if(connectList[x] != NULL){
			delete connectList[x];
			connectList[x] = NULL;
		}
	}

	return;
}

//////////////////////////////////////////////////////////////////////
int BootPServer::GoLive()
{
	/***** Start fresh session close client and start *****/

	if(active == 1){
		EndSession();  // close and restart if active
	}

	lastError = 0;
	PrepWinSockEnviroment();
	PrepSockets();

	if(bootPCallback != NULL && tftpCallback != NULL){
		bootPCallback(BOOTP_READY);
		active = 1;
	}
	else{
		ErrorControl(BOOTP_BAD_CALLBACK);
	}

	return(lastError);
}

//////////////////////////////////////////////////////////////////////
void BootPServer::EndSession()
{
	int x;

	/**** zero clients *****/
	for(x = 0; x <= nextTftpClient; x++){
		if(connectList[x] != NULL){
			delete connectList[x];
			connectList[x] = NULL;
		}
	}

	if(incomingSocket != NULL){
		delete incomingSocket;
		incomingSocket = NULL;
	}

	if(tftpSocket != NULL ){
		delete tftpSocket;
		tftpSocket = NULL;
	}

	active = 0; 
	nextTftpClient = 0;		
	bootPCallback(BOOTP_STOPPED); //inform user of state
  
	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::ProcessMsg()
{
	int x,
	nread = 0;
	BOOTPMsg msg;
	BOOTPMsg outMsg;

	//if a proper bootp message  spawn a tftp process
	nread = incomingSocket->Receive((char *)&msg,sizeof(msg));

	if(nread == sizeof(msg)){ // proper size for message
		if(msg.opCode == BOOTP_REQUEST){  // conform message
			outMsg.opCode = BOOTP_REPLY;
			outMsg.serverIpAddr = inet_addr(serverAddress);
			for(x = 0; x < 8; x++){
				outMsg.etherAddr[x] = msg.etherAddr[x];
			}

			if(msg.targetType == GUI_BOARD){
				strcpy(outMsg.bootFileName,guiFileName);
				outMsg.targetType = GUI_BOARD;
				outMsg.clientIpAddr = inet_addr(guiAddress);
				bootPCallback(GUI_REQUEST);
			}
			else if(msg.targetType == BD_BOARD){
				strcpy(outMsg.bootFileName,bdFileName);
				outMsg.targetType = BD_BOARD;
				outMsg.clientIpAddr = inet_addr(bdAddress);
				bootPCallback(BD_REQUEST);
			}

			// SEND REPLY
			nread = incomingSocket->SendTo((char *)&outMsg, 
			                                 sizeof(outMsg), 
											 BOOTP_REPLY_PORT, 
											 serverBroadcastAddress);
			if(nread == sizeof(outMsg)){
				tftpSocket->Listen(); // ready stream for vent
			}
			else{
				ErrorControl(ERR_SOCKET_TRANSMIT);
			}
		} // end if(msg.opCode == BOOTP_REQUEST)
		// else do nothing not a valid msg
	} // end if(nread == sizeof(msg))

	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::PrepWinSockEnviroment(void)
{
	int nerror;
	int errflag = 0;
	char hname[255];
	LPHOSTENT hp;
	struct in_addr myaddr;

	// get address of system
	if(!errflag){
		nerror = gethostname(hname,255);
		if(nerror == SOCKET_ERROR){
			errflag = ERR_WINSOCK;
		}
	}

	if(!errflag){
		hp = gethostbyname(hname);
		if(multiHomed){
			myaddr.s_addr = inet_addr(altAddress);
		}
		else{
			myaddr = *(struct in_addr far *)(hp->h_addr);
		}
		serverAddress = inet_ntoa(myaddr);

		// set gui and bd address
		myaddr.s_impno = (unsigned char)(myaddr.s_impno + 0x01);
		bdAddress = inet_ntoa(myaddr); // server + 1

		myaddr.s_impno = (unsigned char)(myaddr.s_impno + 0x01);
		guiAddress = inet_ntoa(myaddr); // server + 2

		// set serverBroadcast address
		myaddr.s_impno = (unsigned char)255; //subnet @ 255
		serverBroadcastAddress = inet_ntoa(myaddr);
		readyForMsg = TRUE;	
	}

	if(errflag){
		ErrorControl(errflag);
	}

	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::PrepSockets(void)
{
	int errflag = 0,
	b_value = TRUE,
	nerror;

	/******* get memory Sockets udp and stream *******/

	// socket to hear a broadcast 
	incomingSocket = new BootPSocket(this);

	// socket to listen for tftp request
	tftpSocket = new TftpSocket(this);

	if(tftpSocket == NULL || incomingSocket == NULL){
		errflag = ERR_OUT_OF_MEMORY;
	}

	// create socket
	nerror = incomingSocket->Create(BOOTP_LISTEN_PORT,SOCK_DGRAM);
	if(nerror == SOCKET_ERROR){
		errflag = ERR_SOCKET_CREATION;  //bad create
	}

	// setOpt for broadcast 
	if(!errflag){
		incomingSocket->SetSockOpt(SO_BROADCAST,&b_value,sizeof(b_value));
	}

	// bind socket
	if(!errflag){
		nerror = incomingSocket->Bind(BOOTP_LISTEN_PORT);
		if(nerror == SOCKET_ERROR){
			errflag = ERR_SOCKET_BIND; //bad bind
		}
	}

	if(!errflag){
		nerror = tftpSocket->Create(TFTP_LISTEN_PORT);
		if(nerror == SOCKET_ERROR){
			errflag = ERR_SOCKET_CREATION; // bad create
		}
	}

	if(!errflag){
		nerror = tftpSocket->Bind(TFTP_REPLY_PORT); //MAYBE REPLY PORT
		if(nerror == SOCKET_ERROR){
			errflag = ERR_SOCKET_BIND; //bad bind
		}
	}

	if(errflag){
		ErrorControl(errflag);
	}

	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::ErrorControl(int errorNum)
{
	EndSession();
	lastError = errorNum;
	active = 0; 
	nextTftpClient = 0;			
	bootPCallback(BOOTP_ERROR);

	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::AcceptTftpConnection()
{
	int nerror;
	CAsyncSocket tempSocket;

	if(lastError == 0){
		if(nextTftpClient < MAX_TFTP_CLIENTS){
			connectList[nextTftpClient] = new TftpClientSocket(nextTftpClient, this);
			nerror = tftpSocket->Accept(*(connectList[nextTftpClient]));
			if(nerror == SOCKET_ERROR){
				ErrorControl(ERR_SOCKET_CREATION);
			}
			else{
				connectList[nextTftpClient]->SetClientCallback(tftpCallback);
				nextTftpClient++;
			} // end if(nerror == SOCKET_ERROR)
		} // end if(nextTftpClient < MAX_TFTP_CLIENTS)
	} // end if(lastError == 0)

	return;
}

//////////////////////////////////////////////////////////////////////
int BootPServer::ReadyToRecieve()
{
	return(readyForMsg);
}

//////////////////////////////////////////////////////////////////////
void BootPServer::SetFilePath(char *tmp)
{
	strcpy(filePath, tmp);

	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::SetGuiFileName(char *tmp, int imageId)
{
	strcpy(guiFileName, tmp);
	guiImageId = imageId;

	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::SetAltAddress(char *tmp)
{
	strcpy(altAddress, tmp);
	multiHomed = 1;

	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::SetBdFileName(char *tmp, int imageId)
{
	strcpy(bdFileName, tmp);
	bdImageId = imageId;

	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::SetBootPCallback(void (*callback)(int status))
{
	bootPCallback = callback;

	return;
}

//////////////////////////////////////////////////////////////////////
void BootPServer::SetTftpCallback(void (*callback)(int id, int cblock, int tblock))
{
	tftpCallback = callback;

	return;
}

//////////////////////////////////////////////////////////////////////
int BootPServer::GetLastError()
{
	return(lastError);
}

//////////////////////////////////////////////////////////////////////
char * BootPServer::FilePath()
{
	return(filePath);
}

//////////////////////////////////////////////////////////////////////
void BootPServer::setImageId(char *income, int *id)
{
	if(!(strcmp(income, guiFileName))){
		(*id) = guiImageId;
	}

	if(!(strcmp(income, bdFileName))){
		(*id) = bdImageId;
	}

	return;
}

