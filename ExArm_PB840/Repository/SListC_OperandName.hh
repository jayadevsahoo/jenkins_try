 
#ifndef SListC_OperandName_HH
#define SListC_OperandName_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SListC_OperandName - Singly-Linked List of 'OperandName' Items.
//--------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListC.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "SNodeC_OperandName.hh"
#include "Heap.hh"
//@ End-Usage


class SListC_OperandName
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                  ostr,
				       const SListC_OperandName& sList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  SIterC_OperandName
    // This list's corresponding iterator needs access to the protected
    // members of this list.
    friend class  SIterC_OperandName;

    //@ Friend:  SListR_OperandName
    // The singly-linked reference list uses a singly-linked list of
    // void*'s, and needs access to it's protected members.
    friend class  SListR_OperandName;

  public:
    virtual ~SListC_OperandName(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);

    SigmaStatus  search(const OperandName&      item,
			const SearchFrom startFrom = FROM_FIRST);

    void  insert (const OperandName&          newItem,
		  const InsertionPlace insertPlace = AFTER_CURRENT);
    void  append (const OperandName& newItem);
    void  prepend(const OperandName& newItem);

    SigmaStatus  removeCurrent(void);
    SigmaStatus  removeItem(const OperandName& item);
    void         clearList(void);

    inline const OperandName&  currentItem(void) const;
    inline OperandName&        currentItem(void);

    void  operator=(const SListC_OperandName& sList);

    inline Boolean  operator==(const SListC_OperandName& sList) const;
    inline Boolean  operator!=(const SListC_OperandName& sList) const;

    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32      lineNumber,
                           const char*       pFileName = NULL,
                           const char*       pBoolTest = NULL);

  protected:
    inline SListC_OperandName(Heap* pHeap);

    // return a pointer to the link that is equivalent to 'item', starting
    // the search from 'pStart'...
    const SNodeC_OperandName* findNode_(const OperandName&        item,
				 const SNodeC_OperandName* pStartNode) const;

    inline Boolean  isLegalNode_(const SNodeC_OperandName* pNode) const;

    inline const SNodeC_OperandName*  getFirst_  (void) const;
    inline const SNodeC_OperandName*  getCurrent_(void) const;

    inline SNodeC_OperandName*  getFirst_  (void);
    inline SNodeC_OperandName*  getCurrent_(void);

    inline void  setCurrent_(SNodeC_OperandName* pNewCurr);

    Boolean  isEquivTo_(const SListC_OperandName& sList) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    SListC_OperandName(const SListC_OperandName&);	// not implemented...
    SListC_OperandName(void);				// not implemented...

    inline void  deallocNode_(SNodeC_OperandName*& pOldNode);

    SNodeC_OperandName* getPrevNode_(const SNodeC_OperandName* pStart) const;

    //@ Data-Member:  rHeapOfNodes_
    // A reference to this list's heap of nodes.
    Heap&  rHeapOfNodes_;

    //@ Data-Member:  pFirst_
    // A pointer to the first node in this list.
    SNodeC_OperandName*  pFirst_;

    //@ Data-Member:  pCurrent_
    // A pointer to the current node of this list.
    SNodeC_OperandName*  pCurrent_;
};


// Inlined methods...
#include "SListC_OperandName.in"


#endif  // SListC_OperandName_HH
