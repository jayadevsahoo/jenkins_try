#ifndef EstResultSubScreen_HH
#define EstResultSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: EstResultSubScreen - The subscreen is automatically activated
// during EST Test SubScreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/EstResultSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc	   Date:  25-Jan-2007    DCS Number: 6237
//  Project:  TREND
//  Description:
//		Trend project related changes.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  10-Jun-97    DR Number: 1988
//    Project:  Sigma (R8027)
//    Description:
//      Added formatlabel_ method.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "LargeContainer.hh"
#include "GuiTestManager.hh"
#include "SmTestId.hh"
#include "SmDataId.hh"
#include "SubScreenTitleArea.hh"
#include "TestDataId.hh"
#include "TextField.hh"
#include "LeakGauge.hh"
#include "GuiAppClassIds.hh"
class TestResult;
//@ End-Usage

class EstResultSubScreen : public SubScreen,
							public GuiTestManagerTarget
{
public:
	EstResultSubScreen(SubScreenArea *pSubScreenArea);
	~EstResultSubScreen(void);

	// Overload SubScreen methods
	virtual void activate(void);
	virtual void deactivate(void);

	// GuiTestManagerTarget virtual method
	virtual void processTestData(Int dataIndex, TestResult *pResult);

	// Allow the outside world to set the title area on this subScreen.
	void setResultTitle(StringId title);
	void setDataFormat(SmTestId::ServiceModeTestId testId);
	void clearScreen(void);

	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	EstResultSubScreen(void);							// not implemented..
	EstResultSubScreen(const EstResultSubScreen&);	// not implemented..
	void operator=(const EstResultSubScreen&);			// not implemented..

	void setText_(TextField *testResultField, wchar_t * resultString);  
	void setupDisplayableTestResult_(SmDataId::ItemizedTestDataId testDataIdx,
									Int16 dataPrecision, Int16 *currentDisplayableCount,
									StringId labelString, StringId unitString);
	void formatLabel_(StringId labelString, StringId labelUnitId, wchar_t *labelBuffer);  

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member: valueAreaContainer_
	// A container which holds all the test value displayable texts.
	LargeContainer  valueAreaContainer_;

	//@ Data-Member: labelAreaContainer_
	// A container which holds all the test label displayable texts.
	LargeContainer  labelAreaContainer_;

	//@ Data-Member: unitAreaContainer_
	// A container which holds all the test unit displayable texts.
	LargeContainer  unitAreaContainer_;

	//@ Data-Member: testResultValueArray_
	// The text field for displaying the test result value.
	TextField testResultValueArray_[TestDataId::MAX_DATA_ENTRIES];

	//@ Data-Member: testResultLabelArray_
	// The text field for displaying the test result label.
	TextField testResultLabelArray_[TestDataId::MAX_DATA_ENTRIES];

	//@ Data-Member: testResultUnitArray_
	// The text field for displaying the test result unit.
	TextField testResultUnitArray_[TestDataId::MAX_DATA_ENTRIES];

	//@ Data-Member: isTestResultDisplayable_
	// The boolean array indicates if the corresponding test result shall
	// be displayed on GUI screen.
	Boolean isTestResultDisplayable_[TestDataId::MAX_DATA_ENTRIES];

	//@ Data-Member: testId_
	// The current service test Id in testing.
	SmTestId::ServiceModeTestId testId_;
	
	//@ Data-Member: leakGauge_
	// The graphical graph used to represent the Leak Test's result in the form of a gauge.
	LeakGauge leakGauge_;
	
	//@ Data-Member: testManager_
	// An object which look after rendering the service mode test.
	GuiTestManager testManager_;
	
	//@ Data-Member: maxDataCount_
	// The maximun number of data expected for each test in testing.
	Int maxDataCount_;
	
	//@ Data-Member: maxDisplayableDataCount_
	// The maximun number of data expected for each test in testing.
	Int maxDisplayableDataCount_;

};

#endif // EstResultSubScreen_HH 
