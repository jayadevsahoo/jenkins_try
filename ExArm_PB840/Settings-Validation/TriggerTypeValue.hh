
#ifndef TriggerTypeValue_HH
#define TriggerTypeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TriggerTypeValue - Values of the Trigger Type Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TriggerTypeValue.hhv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct TriggerTypeValue
{
  //@ Type:  TriggerTypeValueId
  // All of the possible values of the Trigger Type Setting.
  enum TriggerTypeValueId
  {
    // $[02256] -- values of the range of Trigger Type Setting...
    FLOW_TRIGGER,
    PRESSURE_TRIGGER,
    
    TOTAL_TRIGGER_TYPES
  };
};


#endif // TriggerTypeValue_HH 
