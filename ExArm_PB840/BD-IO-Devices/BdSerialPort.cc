#include "stdafx.h"
//====================================================================
// This is a proprietary work to which the Covidien corporation claims 
// exclusive right.  No part of this work may be used, disclosed, 
// reproduced, sorted in an information retrieval system, or 
// transmitted by any means, electronic, mechanical, photocopying, 
// recording, or otherwise without the prior written permission of 
// Covidien Corporation.
//
//              Copyright (c) 2008, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Class:  
//---------------------------------------------------------------------
//@ Version-Information
//@(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BdSerialPort.ccv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $  
// //
//@ Modification-Log
//
//  Revision: 001  By:  gdc    Date:  25-APR-2008    SCR Number: XXXX
//  Project:  PROX
//  Description:
//	Initial version.
//=====================================================================

#include "BdSerialPort.hh"
#include "Task.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BdSerialPort [constructor]
//
//@ Interface-Description
//  The device name is specified to the constructor to pass it through
//  to the SerialPort base class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

BdSerialPort::BdSerialPort( const Uint8 deviceNumber )
: SerialPort( deviceNumber)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BdSerialPort [destructor]
//
//@ Interface-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdSerialPort::~BdSerialPort( void )
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  idleFunction_ [virtual]
//
//@ Interface-Description
//  This function provides the idle activity for the task when waiting
//  for serial port activity. Since the BD serial task is of high 
//  priority, it needs to delay if it waiting for serial port activity.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BdSerialPort::idleFunction_(void)
{
	Task::Delay(0,10);    // delays 0-10 ms
}
