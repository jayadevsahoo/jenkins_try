#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PausePressTrigger - Triggers if conditions for pressure 
//	triggering during inspiratory/expiratory pause are met.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers exhalation based on the inspiratory or expiratory 
//    pressure reading and the operator set value of pressure sensitivity.
//    This trigger is one of the methods of used for terminating an
//    inspiratory pause when the pressure drops below the threshold level. 
//    It is enabled or disabled, depending upon the 
//    applicability of the trigger.  When the pressure condition calculated 
//    by this trigger is true, the trigger is considered to have "fired".  
//    This trigger is kept on a list contained in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements one of the algorithms for terminating an 
//    inspiratory/expiratory pause.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class is derived from BreathTrigger. The pressure triggering
//    conditions are implemented in the method triggerCondition.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this trigger can be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PausePressTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: iv      Date:  24-May-1999    DR Number: 5394
//  Project:  ATC
//  Description:
//      In triggerCondition_(), when scheduler is BiLevel, always use peep low
//      for peep value.
//      Class was renamed.  Use same pause trigger for both inspiration and
//		expiration pause handles.
//      Initial version for ATC.
//
//=====================================================================

#include "PausePressTrigger.hh"

//@ Usage-Classes
#include "PhasedInContextHandle.hh"
#include "Peep.hh"
#include "BreathMiscRefs.hh"
#include "PressureSensor.hh"
#include "MainSensorRefs.hh"
#include "TriggerTypeValue.hh"
#include "BreathPhaseScheduler.hh"
#include "BreathPhase.hh"
//@ End-Usage

//@ Constant: MIN_PAUSE_TIME
// min time for trigger the pause in msec
const Int32 MIN_PAUSE_TIME = 100 ;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PausePressTrigger
//
//@ Interface-Description
//      This constructor takes triggerId as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using BreathTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PausePressTrigger::PausePressTrigger(const Trigger::TriggerId triggerId) 
: BreathTrigger(triggerId)
{
    CALL_TRACE("PausePressTrigger(triggerId)");
    // $[TI1]
	readyToTrigger_ = FALSE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PausePressTrigger()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PausePressTrigger::~PausePressTrigger(void)
{
  CALL_TRACE("~PausePressTrigger()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PausePressTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PAUSEPRESSTRIGGER,
                          lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable()
//
//@ Interface-Description
//      This method enables the trigger. This method also initializes data
//      members that belong to patient trigger.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The data member readyToTrigger_ is initialized in addition to
//		the parent's data member.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
PausePressTrigger::enable(void)
{
  CALL_TRACE("enable(void)");
 
   // $[TI1]

   readyToTrigger_ = FALSE;
   isEnabled_ = TRUE;
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has occured, 
//    the method returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//     This method checks for all of the following conditions:
//    1.    After a 100 ms waiting period, the patient pressure measured
//		by the expiratory pressure sensor or inspiratory pressure sensor
//		drops below the baseline (PEEP) by an amount equal to or greater
//		than the Pressure Sensitivity. Pressure sensitivity for pressure
//		inspiration trigger is derived from setting.  Pressure sensitivity
//		for flow inspiration trigger is fixed at BACKUP_PRESSURE_SENS_VALUE cmH2O.	    
//
//---------------------------------------------------------------------
//@ PreCondition
//      (triggerId_ ==  TriggerTypeValue::FLOW_TRIGGER ||
//	 triggerId_ == PRESSURE_TRIGGER)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
PausePressTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean rtnValue = FALSE;

  Real32 peepValue = RPeep.getActualPeep();
  Real32 exhPressure = RExhPressureSensor.getValue(); 
  Real32 inspPressure = RInspPressureSensor.getValue(); 
  Real32 pressSensValue = BASE_FLOW_MIN;

  // A pointer to the current breath phase
  BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
  // determine the current phase type (e.g. EXHALATION, INSPIRATION)
  const  BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();

  DiscreteValue triggerType = PhasedInContextHandle::GetDiscreteValue( SettingId::TRIGGER_TYPE) ;

  if (triggerType == TriggerTypeValue::FLOW_TRIGGER)
  {
	   	// $[TI1.1]
    pressSensValue = BACKUP_PRESSURE_SENS_VALUE;
  }
  else if (triggerType == TriggerTypeValue::PRESSURE_TRIGGER)
  {
  	// $[TI1.2]
    pressSensValue = PhasedInContextHandle::GetBoundedValue(SettingId::PRESS_SENS).value;
  }
  else
  {
   	CLASS_PRE_CONDITION( triggerType == TriggerTypeValue::FLOW_TRIGGER
       			|| triggerType == TriggerTypeValue::PRESSURE_TRIGGER) ;
  }

  if (BreathPhaseScheduler::GetCurrentScheduler().getId() == SchedulerId::BILEVEL &&
  	  (phase == BreathPhaseType::INSPIRATORY_PAUSE || phase == BreathPhaseType::BILEVEL_PAUSE_PHASE))
  {
  // $[TI6]
      peepValue = PhasedInContextHandle::GetBoundedValue( SettingId::PEEP_LOW).value;
  }
  // $[TI7]

  if (!readyToTrigger_)
  {  // $[TI8]
	Int32 elapsedTime = BreathPhase::GetCurrentBreathPhase()->getElapsedTimeMs();
     
  	if (elapsedTime > MIN_PAUSE_TIME)
    {   // $[TI8.1]
        readyToTrigger_ = TRUE;
    }   // $[TI8.2]
  }  // $[TI9]
  
  if (readyToTrigger_ &&
      (exhPressure <= peepValue - pressSensValue ||
      inspPressure <= peepValue - pressSensValue))
  {
    // $[TI4]
    rtnValue = TRUE;
  }
  // $[TI5]

  return (rtnValue);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

