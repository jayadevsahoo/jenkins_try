//#include <userint.h>
/***************************************************************************
*   Source File Part Number:  4-075472-00
*   Source File Name:  softdlic.c
*   Purpose: Encodes objects and behaviors for the license agreement panel
*
*   Modification History:
*
*    Source Code Revision  Date of Revision  Author of Modification  Specific reasons and comments for the change
*    --------------------  ----------------  ----------------------  --------------------------------------------
*    Rev 1.0 beta 3        11 Feb 1998       Dave G. Richardson      Freeze code changes 
*
**************************************************************************/

//#include "ansi_c.h"
//#include <math.h>
#include <toolbox.h>
//#include <utility.h>

#include "globals.h"
#include "ldb.h"
#include "rt_bmps.h"
#include "softdlic.h"

#define DIALOG_BGND_COLOR 	0XC0C0C0 // 192, 192, 192 0XB0B0B0 // 176, 176, 176

//-------------------------------------------------------------------------
// Declare identifiers for Panel:  AGREE
// ------------------------------------------------------------------------

int AGREE_LICENSE_TEXT     = 0; // control identifier
int AGREE_CMD_AGREE_YES    = 0; // control identifier
int AGREE_CMD_AGREE_NO     = 0; // control identifier
int AGREE_TXT_LINE_1       = 0; // control identifier

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

static char* szEmptyString = "";

// text of the license agreement
static char *LicensePara_1[] = {
            {"1) You have legally obtained the 840 software CD-ROM through Puritan-Bennett."},
                               };

static char *LicensePara_2[] = {
            {"2) You received training in operating the 840 Ventilator and performing ventilator software updates."},
                               };

static char *LicensePara_3[] = {
            {"3) You will not modify, adapt, translate, reverse engineer, decompile, disassemble, \n"},
            {"   or otherwise attempt to discover the source code of the 840 Software."},
                               };

typedef struct tag_LICENSE {
	char **LicenseText;
	int    nNumStatements;
} LICENSE;

LICENSE LicenseAgreement[] = {
	{LicensePara_1, sizeof(LicensePara_1)/sizeof(char *)},
	{LicensePara_2, sizeof(LicensePara_2)/sizeof(char *)},
	{LicensePara_3, sizeof(LicensePara_3)/sizeof(char *)},
};
int numLicenseAgreementParas = sizeof(LicenseAgreement)/sizeof(LICENSE);

/***************************************************************************************
* Name: MakeAgreePanel                                                                 *
* Purpose: Creates a dialog for recording the license agreement                        *
* Example Call: HPNL hPanel = MakeAgreePanel(hParentPanel)                             *
* Input: HPNL hParentPanel = handle of parent                                          *
* Return: HPNL hPanel = handle of created panel                                        *
* Global: NONE                                                                         *
***************************************************************************************/

HPNL MakeAgreePanel(HPNL hParentPanel)
{
	HPNL hPanel;
	int error = 0;
	char cBuf[255];
	int i,j;
	LICENSE *LicenseParagraph;
	char szLicenseText[1024];

	// Create the panel
	// "Electronic End User License Agreement"
	errChk(hPanel = NewPanel(hParentPanel, LDBf(Ldb_SoftdLicPanelTitle), 81, 37, 300, 550));

	// Set the panel's attributes
	errChk(SetPanelAttribute(hPanel, ATTR_SIZABLE,      FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MINIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_CAN_MAXIMIZE, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_BACKCOLOR,    DIALOG_BGND_COLOR));
	if(hParentPanel){
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_COLOR,        VAL_GRAY));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_STYLE,        VAL_BEVELLED_FRAME));
		errChk(SetPanelAttribute(hPanel, ATTR_FRAME_THICKNESS,    2));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLEBAR_THICKNESS, 19));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_FONT,         VAL_DIALOG_FONT));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_POINT_SIZE,   14));
		errChk(SetPanelAttribute(hPanel, ATTR_TITLE_BOLD,         TRUE));
	}
	if(!hParentPanel){
		errChk(SetPanelAttribute(hPanel, ATTR_FLOATING,           FALSE));
	}

	// Build control: AGREE_TXT_LINE_1
	errChk(AGREE_TXT_LINE_1 = NewCtrl(hPanel, CTRL_TEXT_MSG, szEmptyString, 10, 10));
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1, ATTR_WIDTH,           500));
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1, ATTR_HEIGHT,          52));
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1, ATTR_CTRL_MODE,       VAL_INDICATOR));
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1, ATTR_TEXT_FONT,       VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1, ATTR_TEXT_POINT_SIZE, 14));
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1, ATTR_TEXT_BOLD,       TRUE));
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1, ATTR_TEXT_BGCOLOR,    VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1, ATTR_SIZE_TO_TEXT,    FALSE));
	// "Notice to user: By utilizing this CD-ROM and the software it contains, you accept all the terms and conditions of the BreathLab 840 VTS Software agreement."
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1, ATTR_DFLT_VALUE,      LDBf(Ldb_SoftdLicAgreeTxtLine01)));
	errChk(DefaultCtrl(hPanel, AGREE_TXT_LINE_1));

	// Build control: AGREE_LICENSE_TEXT
	errChk(AGREE_LICENSE_TEXT = NewCtrl (hPanel, CTRL_TEXT_BOX, szEmptyString, 89, 10));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_WIDTH,              520));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_HEIGHT,             82));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_SCROLL_BARS,        VAL_BOTH_SCROLL_BARS));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_FONT,         VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_POINT_SIZE,   12));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_UNDERLINE,    TRUE));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_BOLD,         TRUE));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_LEFT,         10));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_TOP,          68));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_WIDTH,        112));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_HEIGHT,       15));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_BGCOLOR,      VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
	// "By utilizing this software you agree that:"
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_LABEL_TEXT,         LDBf(Ldb_SoftdAgreeLicText)));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_TEXT_FONT,          VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_TEXT_POINT_SIZE,    12));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_TEXT_BOLD,          FALSE));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_WRAP_MODE,          VAL_WORD_WRAP));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_ENTER_IS_NEWLINE,   TRUE));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_SCROLL_BAR_SIZE,    VAL_SMALL_SCROLL_BARS));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_NO_EDIT_TEXT,       TRUE));

	LicensePara_1[0] = LDBf(Ldb_SoftdLicPara_1);
	LicensePara_2[0] = LDBf(Ldb_SoftdLicPara_2);
	LicensePara_3[0] = LDBf(Ldb_SoftdLicPara_3_1);
	LicensePara_3[1] = LDBf(Ldb_SoftdLicPara_3_2);

	strcpy(szLicenseText, "");
	for(i = 0; i < numLicenseAgreementParas; i++){
		LicenseParagraph = &(LicenseAgreement[i]);
		for(j = 0; j < LicenseAgreement[i].nNumStatements; j++){
			sprintf(cBuf, "%s", LicenseAgreement[i].LicenseText[j]);
			strcat(szLicenseText, cBuf);
		};
		sprintf(cBuf, "\n");
		strcat(szLicenseText, cBuf);
	};

	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT, ATTR_DFLT_VALUE, szLicenseText));
	errChk(DefaultCtrl(hPanel, AGREE_LICENSE_TEXT));

	// Build OK bitmap button
	errChk(AGREE_CMD_AGREE_YES = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, 200, 102));
	setButtonBMP(hPanel, AGREE_CMD_AGREE_YES, BMP_Okay192);	
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_FIT_MODE, VAL_SIZE_TO_IMAGE));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_LABEL_FONT,         VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_LABEL_POINT_SIZE,   14));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_LABEL_BOLD,         TRUE));
	// "I AGREE"
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_LABEL_TEXT,         LDBf(Ldb_SoftdLicAgreeYes)));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_LABEL_LEFT,         VAL_AUTO_CENTER));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_LABEL_TOP,          260));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_LABEL_BGCOLOR,      VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES, ATTR_LABEL_SIZE_TO_TEXT, TRUE));
	
	// Build Cancel bitmap button
	errChk(AGREE_CMD_AGREE_NO = NewCtrl(hPanel, CTRL_PICTURE_COMMAND_BUTTON, 0, 200, 377));
	setButtonBMP(hPanel, AGREE_CMD_AGREE_NO, BMP_Cancel192);	
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_CMD_BUTTON_COLOR, VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_FIT_MODE, VAL_SIZE_TO_IMAGE));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_LABEL_FONT,         VAL_DIALOG_FONT));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_LABEL_POINT_SIZE,   14));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_LABEL_BOLD,         TRUE));
	// "I DO NOT AGREE"
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_LABEL_TEXT,         LDBf(Ldb_SoftdLicAgreeNo)));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_LABEL_LEFT,         VAL_AUTO_CENTER));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_LABEL_TOP,          260));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_LABEL_BGCOLOR,      VAL_TRANSPARENT));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO, ATTR_LABEL_SIZE_TO_TEXT, TRUE));

   	errChk(SetPanelAttribute(hPanel, ATTR_CLOSE_CTRL, AGREE_CMD_AGREE_NO));

	// Set up ZPLANE order
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT,     ATTR_ZPLANE_POSITION,  0));
	errChk(SetCtrlAttribute(hPanel, AGREE_TXT_LINE_1,       ATTR_ZPLANE_POSITION,  1));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO,     ATTR_ZPLANE_POSITION,  2));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES,    ATTR_ZPLANE_POSITION,  3));

	// Set up TAB order
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_NO,     ATTR_CTRL_TAB_POSITION, 0));
	errChk(SetCtrlAttribute(hPanel, AGREE_LICENSE_TEXT,     ATTR_CTRL_TAB_POSITION, 1));
	errChk(SetCtrlAttribute(hPanel, AGREE_CMD_AGREE_YES,    ATTR_CTRL_TAB_POSITION, 2));

	// Finalize panel colors, positioning, and sizing
	errChk(SetPanelAttribute(hPanel, ATTR_CONFORM_TO_SYSTEM, FALSE));
	errChk(SetPanelAttribute(hPanel, ATTR_TOP,               VAL_AUTO_CENTER));
	errChk(SetPanelAttribute(hPanel, ATTR_LEFT,              VAL_AUTO_CENTER));

	//We're done!
	return hPanel;
Error:
	return error;
}

