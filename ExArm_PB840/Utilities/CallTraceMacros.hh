
#ifndef CallTraceMacros_HH
#define CallTraceMacros_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  CallTraceMacros - Call-Trace Macros.
//---------------------------------------------------------------------
// Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/CallTraceMacros.hhv   25.0.4.0   19 Nov 2013 14:35:12   pvcs  $
//
// Modifications
//
//  Revision: 001  By:  sah    Date:  07-15-93    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//=====================================================================

// Usage-Class
#if defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)
#include "CallTrace.hh"
#endif  // defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)
// End-Usage


#if defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)
// Macro:  CALL_TRACE(pFuncName)
// This macro processes function entry and exit for call-tracing.  The
// argument, 'pFuncName', is a string identification of the routine
// that is being entered.
#  define CALL_TRACE(pFuncName)	\
	CallTrace  callTrace(pFuncName, __FILE__, __LINE__);
#else   // defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)...
#  define CALL_TRACE(pFuncName)
#endif  // defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)...

#include <sstream>
#include <iomanip>
#include <iostream>

#if defined(_DEBUG)
#if defined(WIN32) && defined(RETAILMSG)
#define DEBUG_MSG(x) { std::wstringstream stream; stream << "[" << __FILE__ << ":" << __LINE__ << "] " << x << std::endl; RETAILMSG(TRUE, (_T("%s"), stream.str().c_str())); }
#else
#define DEBUG_MSG(x) { std::wcout << "[" << __FILE__ << ":" << __LINE__ << "] " << x << std::endl;  }
#endif
#else
#define DEBUG_MSG(x)
#endif

#if defined(WIN32) && defined(RETAILMSG)
#define RELEASE_MSG(x) { std::wstringstream stream; stream << x << std::endl; RETAILMSG(TRUE, (_T("%s"), stream.str().c_str())); }
#else
#define RELEASE_MSG(x) { std::wcout << x << std::endl;  }
#endif

#define DEBUG_VAR_DUMP(x) { DEBUG_MSG(#x << " = " << x); }

#endif  // CallTraceMacros_HH
