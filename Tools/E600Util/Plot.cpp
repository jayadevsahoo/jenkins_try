#include "StdAfx.h"
#include "Plot.h"
#include "GlobalObjects.h"

Plot::Plot(HWND hWnd, int width, int height, int timeLen, int maxScale, int midScale, int minScale)
{
	// save parameters to members
	m_hWnd = hWnd;
	m_width = width;
	m_height = height;
	m_timeLen = timeLen;

	// define margins for wave forms
	m_topMargin = 15;
	m_rightMargin = 30;
	m_markLength = 5;			// the length of ticks on the axis
	m_gridLines = 10;

	m_Origin.x = 80;			// from left
	m_Origin.y = m_height - 20;	// 20 from bottom

	setY( minScale, midScale, maxScale );

	m_waveWidth = (m_width - m_Origin.x - m_rightMargin);	// = the length of the x-axis
	m_waveWidth = m_waveWidth / timeLen * timeLen;			// adjust the width according to time length

	m_waveHeight = m_Origin.y - m_topMargin;				// = the length of the y-axis
	m_waveHeight = m_waveHeight / m_gridLines * m_gridLines;	// adjust the height according to grid lines

	// the wave area
	m_waveRect.left = m_Origin.x;
	m_waveRect.top = m_topMargin;
	m_waveRect.right = m_waveRect.left + m_waveWidth;
	m_waveRect.bottom = m_Origin.y;

	m_dwWiperResetTime = 0;
	m_bBkgndInvalid = true;
	m_bWiperReset = false;

	m_dataSpeed = PB880_DATA_SPEED;

	// m_timeLen = number of seconds on the x-axis. Compute # of pixels per datum
	m_pixelPerData = (double)m_waveWidth / ((double)m_timeLen * 1000 / m_dataSpeed);
	// ???
	m_slicePixels = (double) m_waveWidth / m_timeLen / 12 + 4;

	// build memory bitmap and memory dc
	HDC hDC = GetDC(m_hWnd);
	m_pWndDC = CDC::FromHandle(hDC);

	// create memory buffer for entire plot
	m_dcMem.CreateCompatibleDC(m_pWndDC);
	m_bmpMem.CreateCompatibleBitmap(m_pWndDC, m_width, m_height);
	m_pOldMemBmp = m_dcMem.SelectObject(&m_bmpMem);

	// create memory bitmap buffer for background
	m_bmpBkgnd.CreateCompatibleBitmap(m_pWndDC, m_width, m_height);
	m_dcBkgnd.CreateCompatibleDC(m_pWndDC);
	m_pOldBkgndBmp = m_dcBkgnd.SelectObject(&m_bmpBkgnd);

	ReleaseDC(m_hWnd, hDC);
}

Plot::~Plot(void)
{
	m_dcMem.SelectObject(m_pOldMemBmp);
	m_dcMem.DeleteDC();	
	m_bmpMem.DeleteObject();

	m_dcBkgnd.SelectObject(m_pOldBkgndBmp);
	m_dcBkgnd.DeleteDC();
	m_bmpBkgnd.DeleteObject();
}

void Plot::draw(CPaintDC* pPaintDC, int x, int y)
{
	ASSERT(m_pWndDC != NULL);

	if ( m_bBkgndInvalid )
	{
		// draw graphics to cache
		drawBackground(&m_dcBkgnd);
		drawCoordinate(&m_dcBkgnd);
	}
	

	if ( m_bWiperReset || m_bBkgndInvalid )
	{
		m_dcMem.BitBlt(0, 0, m_width, m_height, &m_dcBkgnd, 0, 0, SRCCOPY);
		m_bBkgndInvalid = false;
	}
	
	// draw wave forms
	drawWaveForm(&m_dcMem, m_data, m_oldData);
	// render memory dc to window dc	
	pPaintDC->BitBlt(x, y, m_width, m_height, &m_dcMem, 0, 0, SRCCOPY);
	

	if (m_bWiperReset)
		m_bWiperReset = false;
}

void Plot::setDataSpeed(int speed)
{ 
	m_dataSpeed = speed;
	m_pixelPerData = (double)m_waveWidth / ((double)m_timeLen * 1000 / m_dataSpeed);
}

void Plot::drawBackground( CDC* pMemDC )
{
	
	CRect rcCanvas(0, 0, m_width, m_height);

	TRIVERTEX triVertex[2];

	// left top
	triVertex[0].x = rcCanvas.left;
	triVertex[0].y	= rcCanvas.top;
	triVertex[0].Red = 0x0000;
	triVertex[0].Green = 0x0000;
	triVertex[0].Blue = 0x0000;
	triVertex[0].Alpha = 0;

	// right bottom
	triVertex[1].x = rcCanvas.right;
	triVertex[1].y = rcCanvas.bottom;
	triVertex[1].Red = 0x0000;
	triVertex[1].Green = 0x0000;
	triVertex[1].Blue = 0x0000;
	triVertex[1].Alpha = 0;


	GRADIENT_RECT gradientRect;
	gradientRect.UpperLeft = 0;
	gradientRect.LowerRight = 1;

	GradientFill(pMemDC->GetSafeHdc(), triVertex, 2, &gradientRect, 1, GRADIENT_FILL_RECT_V);
}

void Plot::drawCoordinate( CDC* pMemDC )
{

	CPoint xCordEnd, yCordEnd;
	xCordEnd.x = m_width - m_rightMargin;
	xCordEnd.y = m_Origin.y;
	yCordEnd.x = m_Origin.x;
	yCordEnd.y = m_topMargin;

	// prepare context
	CPen pen;
	CPen* pOldPen;
	pen.CreatePen(PS_SOLID, 2, RGB(255, 255, 255));
	pOldPen = pMemDC->SelectObject(&pen);

	// draw x,y coordinates
	pMemDC->MoveTo(xCordEnd);
	pMemDC->LineTo(m_Origin);
	pMemDC->LineTo(yCordEnd);

	// draw grids
	
	CPen pen2;
	pen2.CreatePen(PS_SOLID, 2, RGB(0, 0, 200));
	pMemDC->SelectObject(&pen2);

	int xScaling = m_waveWidth / m_timeLen;
	// horizontal grids
	for ( int i = 1; i <= m_gridLines; ++ i)
	{
		pMemDC->MoveTo(m_Origin.x + 1, m_Origin.y - m_waveHeight / 10 * i);
		pMemDC->LineTo(xCordEnd.x, m_Origin.y - m_waveHeight / 10 * i);
	}
	// vetical grids
	for ( int i = 1; i <= m_timeLen; ++ i)
	{
		pMemDC->MoveTo(m_Origin.x + i * xScaling, m_Origin.y - 2);
		pMemDC->LineTo(m_Origin.x + i * xScaling, yCordEnd.y);
	}
	pMemDC->SelectObject(&pen);

	// draw x coordinate mark
	
	for (int i = 0; i <= m_timeLen; ++ i)
	{
		pMemDC->MoveTo(m_Origin.x + i * xScaling, m_Origin.y);
		pMemDC->LineTo(m_Origin.x + i * xScaling, m_Origin.y + m_markLength);
	}

	// draw y coordinate mark
	
	CPoint ptBase;
	ptBase.x = m_Origin.x;
	
	ptBase.y = m_Origin.y - static_cast<long>(((double)(m_baseData - m_minData) / m_dataDelta ) * m_waveHeight);

	pMemDC->MoveTo(m_Origin);
	pMemDC->LineTo(m_Origin.x - m_markLength, m_Origin.y);

	pMemDC->MoveTo(ptBase);
	pMemDC->LineTo(ptBase.x - m_markLength, ptBase.y);

	pMemDC->MoveTo(m_Origin.x, m_Origin.y - m_waveHeight);
	pMemDC->LineTo(yCordEnd.x - m_markLength, m_Origin.y - m_waveHeight);

	// draw mark text
	CRect rcYMaxText, rcYMinText, rcYBaseText;
	getRectByPt(m_Origin.x - 10, m_Origin.y, 100, 16, ExtLeft, rcYMinText);
	getRectByPt(ptBase.x - 10, ptBase.y, 100, 16, ExtLeft, rcYBaseText);
	getRectByPt(yCordEnd.x - 10, yCordEnd.y, 100, 16, ExtLeft, rcYMaxText);

	CRect destRect;
	if ( m_maxData - m_baseData > m_baseData - m_minData)
	{
		if ( destRect.IntersectRect(&rcYBaseText, &rcYMinText) )
		{
			rcYBaseText.OffsetRect(0, - destRect.Height() - 30);
			pMemDC->MoveTo(rcYBaseText.BottomRight());
			pMemDC->LineTo(ptBase.x - m_markLength, ptBase.y);
		}
	}
	else
	{
		if (destRect.IntersectRect(&rcYBaseText, &rcYMaxText) )
		{
			rcYBaseText.OffsetRect(0, destRect.Height() + 30 );
			pMemDC->MoveTo(rcYBaseText.right, rcYBaseText.top);
			pMemDC->LineTo(ptBase.x - m_markLength, ptBase.y);
		}
	}

	pMemDC->SetBkMode(TRANSPARENT);
	pMemDC->SetTextColor(RGB(255,255,255));

	CString strText;
	strText.Format(_T("%.2f"), m_maxData);
	pMemDC->DrawText(strText, &rcYMaxText, DT_RIGHT|DT_VCENTER);

	strText.Format(_T("%.2f"), m_baseData);
	pMemDC->DrawText(strText, &rcYBaseText, DT_RIGHT|DT_VCENTER);

	strText.Format(_T("%.2f"), m_minData);
	pMemDC->DrawText(strText, &rcYMinText, DT_RIGHT|DT_VCENTER);

	pMemDC->SelectObject(pOldPen);
	pen.DeleteObject();
}


void Plot::setY( double min, double base, double max )
{
	m_minData = min;
	m_baseData = base;
	m_maxData = max;

	ASSERT(max - min > 0);
	ASSERT(max - base > 0);
	ASSERT(base - min > 0);

	m_dataDelta = static_cast<int>(m_maxData - m_minData);

	// after setY scale, we need to reset the 
	m_data.clear();
	m_oldData.clear();
	m_bBkgndInvalid = true;
	resetWiper();	
}

void Plot::getRectByPt( const CPoint& ptBase, int width, int height, RectExtMethod extMethod, CRect& rectRtn)
{
	CPoint ptLeftTop = ptBase;

	switch(extMethod)
	{
	case ExtLeft:
		ptLeftTop.x = ptBase.x - width;
		ptLeftTop.y = ptBase.y - height / 2;
		break;
	case ExtDown:
		ptLeftTop.x = ptBase.x - width / 2;
		ptLeftTop.y = ptBase.y;
		break;
	default:
		break;
	}

	rectRtn.left = ptLeftTop.x;
	rectRtn.top = ptLeftTop.y;
	rectRtn.right = rectRtn.left + width;
	rectRtn.bottom = rectRtn.top + height;
}

void Plot::getRectByPt( int x, int y, int width, int height, RectExtMethod extMethod, CRect& rectRtn )
{
	CPoint pt(x, y);
	getRectByPt(pt, width, height, extMethod, rectRtn);
}

void Plot::drawWaveForm( CDC* pMemDC, const deque<float>& data, const deque<float>& oldData )
{
	int savedDC = pMemDC->SaveDC();

	{
		pMemDC->SelectObject( GlobalObjects::getInstance()->getPen( Pen_OldCurve ));
		if ( m_bWiperReset && oldData.size() > 0)
		{
			// draw old data
			pMemDC->MoveTo(m_Origin.x, getYPos( *oldData.begin() ));

			for ( UINT iOldDataIndex = 0; iOldDataIndex < oldData.size(); ++ iOldDataIndex)
			{
				int y = getYPos( oldData[iOldDataIndex] );
				int x = getXPos( iOldDataIndex );

				pMemDC->LineTo(x, y);
			}
		}
	}

	pMemDC->SelectObject( GlobalObjects::getInstance()->getPen( Pen_Curve ) );

	UINT dataCount = static_cast<UINT>(m_slicePixels / m_pixelPerData ) + 1;
	UINT dataIndex = 0;
	if ( dataCount > data.size() )
		dataIndex = 0;
	else
		dataIndex = data.size() - dataCount;

	int repaintStartX = getXPos(dataIndex);
	pMemDC->BitBlt(repaintStartX + 1, 0, (int)m_slicePixels, m_height, &m_dcBkgnd, repaintStartX + 1, 0, SRCCOPY);

	if ( data.size() > 0 )
	{
		int firstY = getYPos( data[dataIndex] );
		pMemDC->MoveTo(repaintStartX, firstY);
		for ( UINT iDataIndex = dataIndex; iDataIndex < data.size(); ++ iDataIndex)
		{
			int y = getYPos( data[iDataIndex] );
			int x = getXPos( iDataIndex );

			pMemDC->LineTo(x, y);
		}
	}
	
	//////////////////////////////////////////////////// draw wiper //////////////////////////////////////////////////////
	int x = getXPos(data.size());
	pMemDC->SelectObject( GlobalObjects::getInstance()->getPen( Pen_Wiper ) );
	pMemDC->MoveTo(x, m_Origin.y - 1);
	pMemDC->LineTo(x, m_topMargin);

	// to calculate the real cycle time of the plot
	if (m_bWiperReset)
	{
		DWORD howLongPassed = ::GetTickCount() - m_dwWiperResetTime;

		m_strBuf.Format(_T("%d"), howLongPassed);
		
		m_dwWiperResetTime = GetTickCount();
	}

	pMemDC->RestoreDC( savedDC );
}

void Plot::getWaveRect( CRect& rectWaveForm)
{
	rectWaveForm = m_waveRect;
}

int Plot::getDataCount()
{
	return (m_timeLen * 1000 / m_dataSpeed);
}

void Plot::resetWiper()
{
	m_bWiperReset = true;
}

int Plot::getYPos( float data )
{
	double y = m_Origin.y - (double)(data - m_minData) / m_dataDelta * m_waveHeight;
	return static_cast<int>(y);
}

int Plot::getXPos( int index )
{
	double x = m_Origin.x + (index) * m_pixelPerData;
	return static_cast<int>(x + 1);
}


void Plot::appendData(float data)
{
	if ( (int)m_data.size() >= getDataCount() )
	{
		// copy current data queue to old data queue and then clear current data queue
		m_oldData.clear();
		copy(m_data.begin(), m_data.end(), std::back_inserter(m_oldData));
		m_data.clear();
		resetWiper();
	}

	m_data.push_back(data);
}