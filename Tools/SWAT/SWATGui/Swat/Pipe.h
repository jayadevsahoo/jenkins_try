//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Pipe.h
//----------------------------------------------------------------------------

#include "stdafx.h"


#include <windows.h> 
#include <tchar.h>
#include <stdio.h> 
#include <strsafe.h>



class CPipe
{
public:
	CPipe(bool bBD);
	int Run();
	void ErrorExit(PTSTR); 
	HANDLE CreateChildProcess(void); 
	void WriteToPipe(void); 
	void ReadFromPipe(void); 
	HANDLE g_hChildStd_IN_Rd ;
	HANDLE g_hChildStd_IN_Wr ;
	HANDLE g_hChildStd_OUT_Rd ;
	HANDLE g_hChildStd_OUT_Wr ;
	HANDLE g_hInputFile ;
	bool m_bBD;
	bool m_bError;



//	int argc; TCHAR *argv[];

};

