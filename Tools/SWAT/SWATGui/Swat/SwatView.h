// SwatView.h : interface of the CSwatView class
//


#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "mytabctrl.h"
#include "TabStatusDlg.h"
#include "TabLogDlg.h"
#include "TabPowerDlg.h"

#include "PictureEx.h"

class CSwatView : public CFormView
{
protected: // create from serialization only
	CSwatView();
	DECLARE_DYNCREATE(CSwatView)

public:
	enum{ IDD = IDD_VTREE5_FORM };

//	CSwatApp* m_pApp;

// Attributes
public:
	CSwatDoc* GetDocument() const;
	void UpdateTree(CString str);
	CImageList m_TreeImages;


	void InsertColumns();

	void Recurse(LPCTSTR pstr, HTREEITEM hCountry);
	void RecurseReadTree(HTREEITEM hCountry, CString str);
	void WriteReportLog(CString &str);

	static UINT Thread_Lua(LPVOID lp);
	static UINT Session_Starter(LPVOID lp);
	static UINT Script_Starter(LPVOID lp);
	static UINT RecordThread(LPVOID lp);
	static UINT ParseRecordLog(GuiIOUIOutputResp &resp);

	

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CSwatView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

	CWinThread* m_pLuaThread;
	CWinThread* m_pRecordThread;

public:
	CTreeCtrl m_TreeCtrl;
	CString m_strScript;
	afx_msg void OnVtreeRunselectedfiles();
	CProgressCtrl m_progressbar;
	CListCtrl m_List;
	afx_msg void OnBnClickedButtonMoveTolist();
	void AddListEntry(CString);
	afx_msg void OnBnClickedButtonUp();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CString m_strTestExecStatus; // test execution status
	//CString m_strSWATLogStatus; //SWAT logs moved to the app
	afx_msg void OnNMDblclkListControl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnVtreeStop();
	void RefreshStatusWindow(CString &str, int nMsgType);
	void RefreshSWATLogWindow(CString &str); //to send and update additional string for the SWAT status
	afx_msg void OnNMClickTree(NMHDR *pNMHDR, LRESULT *pResult);
	void CheckTreeChildren(HTREEITEM hTreeParent, BOOL bCheck);
	afx_msg void OnTvnKeydownTree(NMHDR *pNMHDR, LRESULT *pResult);
	CString m_strStatusString;
	BOOL m_bAutoScroll;
	afx_msg void OnBnClickedCheckAutoUpdate();
	void UpdateScriptResult(CString strResult);
	afx_msg void OnBnClickedButtonClear();


	void AddListResult(void);
	afx_msg void OnNMRClickListControl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRDblclkListControl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnUpdateVtreeRunselectedfiles(CCmdUI *pCmdUI);
	afx_msg void OnUpdateVtreeStop(CCmdUI *pCmdUI);
	afx_msg void OnClose();
	afx_msg void OnNMDblclkTree(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSwatRunmultipletimes();
	bool Run_scripts_multi_times(int nTimes);
	afx_msg void OnSwatsettingsInit();
	afx_msg void OnSwatsettingsSQL_lib();
	afx_msg void OnSwatViewstoredresults();
	afx_msg void OnNMRClickTree(NMHDR *pNMHDR, LRESULT *pResult);
	
	CMyTabCtrl m_Tab;
	CTabStatusDlg* m_pStatusDlg;
	CTabLogDlg* m_pLogDlg;
	CTabPowerDlg* m_pPowerDlg;

	//void TurnOnOffVent(bool bOnOff);
	//BOOL m_bStartLog;


	CString m_strSWATLog;
	
	afx_msg void OnPopUpExplore();
	
	afx_msg void OnPopUpRefreshTree();

	afx_msg void OnSwatsettingsPcapp();
	afx_msg void OnPopUpEditScript();

	void ShowBreathPhase(CString str);
	CPictureEx m_Picture;
	afx_msg void OnUpdateSwatRunmultipletimes(CCmdUI *pCmdUI);
	afx_msg void OnSwatRecord();
	afx_msg void OnUpdateSwatRecord(CCmdUI *pCmdUI);
	
	
	afx_msg void OnOpenLastResultLog();
	
private:
	void ClearListResult(void);


public:
	afx_msg void OnPopUpChangeScriptpath();
	afx_msg void OnSwatPause();
	afx_msg void OnUpdateSwatPause(CCmdUI *pCmdUI);
	void OpenScriptFile(CString strFileName);
	void SaveScriptFile(CString strFileName);
	void NewScriptFile();
	afx_msg void OnLvnKeydownListControl(NMHDR *pNMHDR, LRESULT *pResult);
	void AddSessionTestResultsToFile();
	void ReadSessionTestResultsFromFile(CStdioFile *file);


	CString GetScriptNameToRun();
	afx_msg void OnPopUpRunOnlyThisOne();
};

#ifndef _DEBUG  // debug version in SwatView.cpp
inline CSwatDoc* CSwatView::GetDocument() const
   { return reinterpret_cast<CSwatDoc*>(m_pDocument); }
#endif

