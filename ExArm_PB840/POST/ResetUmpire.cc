#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: ResetUmpire - Reset umpire 
//
//---------------------------------------------------------------------
//@ Interface-Description
//    This class contains the POST test to determine if three or more
//    unexpected resets have occurred in the past 24 operational hours.  
//    It is invoked by the Phase 2 POST Executive.
//---------------------------------------------------------------------
//@ Rationale
//    This class contains the reset umpire test required to halt
//    processing if too many resets occur.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//    Faults during POST cause the test in progress to fail.
//---------------------------------------------------------------------
//@ Restrictions
//    This is to be called only once during the power on initializaton
//    process.  It should be placed at the end of POST since it resets
//    the shutdown state to UNKNOWN which will cause an umpire strike
//    if POST resets.  This test is designed to capture application
//    faults.  The Rolling Thunder Test captures POST resets.
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/ResetUmpire.ccv   25.0.4.0   19 Nov 2013 14:17:06   pvcs  $
//
//@ Modification-Log
//
//
//  Revision: 007    By: gdc  Date: 11-Aug-2010  SCR Number: 6663
//    Project: MAT
//    Description:
//       Modified failure interval for API investigational device to 1 hour.
//
//  Revision: 006    By: erm  Date: 15-MAY-2003  DR Number: 6044
//    Project: AVER
//    Description:
//       Modified ResetUmpire to declare PASS for each strike under 3
//
//  Revision: 005    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 004    By: Gary Cederquist  Date: 23-JUL-1997  DR Number: 2323
//    Project: Sigma (R8027)
//    Description:
//       Always return PASSED for development version.
//
//  Revision: 003    By: Gary Cederquist  Date: 15-JUL-1997  DR Number: 2294
//    Project: Sigma (R8027)
//    Description:
//       Changed to issue a MAJOR POST failure when the third reset
//       occurs to prevent infinite resets.  After cycling power and
//       entering POST not via a reset, the umpire allows testing and
//       application initialization to continue, but prevents
//       ventilation due to the previous MAJOR POST failure.
//
//  Revision: 002    By: Gary Cederquist  Date: 07-MAY-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added missing requirement #06086.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "ResetUmpire.hh"
#include "PostNovram.hh"


//============== M E T H O D  D E S C R I P T I O N ===================
//@ Method: Test
//
//@ Interface-Description
//  >Von
//  Keeps track of the number of unexpected CPU resets
//
//  INPUTS:  NONE
//  OUTPUTS: PASSED, MINOR, MAJOR
//
//  This test uses the NOVRAM variables ShutdownState, WatchdogOccurred
//  and StrikeHour to determine if 3 unexpected resets have occurred 
//  in the past 24 operational hours.  If an unexpected reset (strike) 
//  occurred without striking out, this test returns a MINOR fault.
//  If the third unexpected strike occurs, it returns a MAJOR which
//  prevents further POST testing or application initialization.  The
//  MAJOR failure creates a service required condition which can only
//  be cleared by running EST.
//  $[11028] $[11087] $[06086]
//  >Voff  
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  DESCRIPTION 
//
//  If a watchdog reset occured (watchdogOccurred) and the shutdown
//  state is UNKNOWN then the watchdog timer was not strobed frequently
//  enough to keep it from firing.  The Task Monitor is responsible for
//  strobing the watchdog timer while the application is running.  Any
//  time this occurs, it is counted as a strike.
//
//  When the application reboots the processor intentionally (as is done
//  when exiting EST or SST) then the shutdown state is INTENTIONAL and
//  is not counted as a strike.
//
//  On the BD only, the shutdown state may be ACSWITCH or POWERFAIL as set
//  by the power fail NMI handler.  This is not counted as a strike.
//  
//  In the case of a processor exception or unexpected interrupt, the 
//  exception handler sets the state to EXCEPTION which is counted as
//  a strike.
//
//  Logging the third reset (strike) in 24 operational hours results 
//  in a MAJOR failure.  Logging a strike that is not the third strike
//  causes a MINOR fault.
//
//  The shutdown state and watchdog occurred state are reset to UNKNOWN
//  and FALSE respectively prior to returning.
//
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

Criticality
ResetUmpire::Test(void)
{
   Criticality    status;
   ShutdownState  state = Post::GetShutdownState_();

UNIT_TEST_POINT(POSTUT_23)

   if ( PKernelNovram->watchdogOccurred && state == UNKNOWN )
   {
       // $[TI1.1]
       state = WATCHDOG;
   }
       // $[TI1.2]

   switch ( state )
   {
#ifdef SIGMA_BD_CPU
      case UNKNOWN:
#endif
      case WATCHDOG: 
      case EXCEPTION:        // $[TI2.1]
      {
         ResetUmpire::LogStrike_();

         if ( ResetUmpire::IsStrikedOut_() )
         {
            // $[TI3.1]
            status = MAJOR;
         }
         else
         {
            // $[TI3.2]
            status = PASSED;
         }

         break;
      }

#ifdef SIGMA_GUI_CPU
      case UNKNOWN:
      case ACSWITCH:    // valid default state
#endif
#ifdef SIGMA_BD_CPU
      case ACSWITCH:
      case POWERFAIL:
#endif
      case INTENTIONAL:     // $[TI2.2]
      {
         status = PASSED;
         break;
      }

      default:              // $[TI2.3]
      {
         // state is invalid, but we still want to go online
         status = MINOR;
         break;
      }

   }

   //  Set the startup state to the previous shutdown state
   //  now that the reset umpire has executed
   Post::SetStartupState_( state );

   //  Always reset the shutdown state to its unknown reason.
   Post::SetShutdownState_( UNKNOWN );

   //  And reset the "watchdog occurred" state.
   PKernelNovram->watchdogOccurred = FALSE;

#ifdef SIGMA_DEVELOPMENT
   status = PASSED;
#endif
   return status;
}


//============== M E T H O D  D E S C R I P T I O N ===================
//@ Method: LogStrike_
//
//@ Interface-Description
//  This private method adds an entry to the strike buffer.  It logs the
//  operation hours in the circular buffer located in NOVRAM.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
ResetUmpire::LogStrike_(void)
{
   Uint32 & next = PPostNovram->strikeIndex;

   // Here taking the modulus automatically wraps the index
   next = ++next % UMPIRE_QUEUE_DEPTH;   

   PPostNovram->strikeHour[next] = Post::GetOperationalTime();

   // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsStrikedOut
//
//@ Interface-Description
//    This checks a queue of operational hours against the current 
//    operational hours.  If 3 errors exist within 24 hours then
//    the check fails (returns TRUE).
//---------------------------------------------------------------------
//@ Implementation-Description
//    Operational time must be read from Post's region of Novram, not
//    from the NovramManager.  A copy exists in both places, but the
//    NovramManager is not available during POST.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================


Boolean  
ResetUmpire::IsStrikedOut_(void)
{
   Uint32 currentOperationalHours = Post::GetOperationalTime();
   Uint8  strikes = 0;
#if defined(API_INVESTIGATIONAL_DEVICE)
   static const Uint32 FAILURE_INTERVAL_HOURS = 1;
#else
   static const Uint32 FAILURE_INTERVAL_HOURS = 24;
#endif

   for (Uint i = 0; i < UMPIRE_QUEUE_DEPTH; i++)
   {
      if (   PPostNovram->strikeHour[i] != 0xffffffff 
          && (  currentOperationalHours 
              < PPostNovram->strikeHour[i] + FAILURE_INTERVAL_HOURS ) )
      {
         // $[TI1.1]
         strikes++;
      }
         // $[TI1.2]
   }

UNIT_TEST_POINT(POSTUT_23_1)

   //  $[TI2.1]  $[TI2.2]
   return ( strikes >= 3 );
}
