#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TcvPhase - Implements tube compensation ventilation inspiration.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is derived from CompensationBasedPhase class.  Protected
//	virtual methods are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
// 	This class implements the algorithms for TC pressure
//	controlled inspirations.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	The data members values from the base class are defined	in the pure
//	virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/TcvPhase.ccv   10.7   08/17/07 09:44:06   pvcs  
//
//@ Modification-Log
//
//  Revision: 006   By: syw   Date:  17-Jul-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		Redesigned to use CompensationBasedPhase as base class.
//
//  Revision: 005  By: syw     Date:  27-Oct-1999    DR Number: 5559
//  Project:  Baseline
//  Description:
//		Trace TC05007.
//
//  Revision: 004  By: syw     Date:  08-Sep-1999    DR Number: 5515
//  Project:  Baseline
//  Description:
//		Initialize pid pressure controller's flow with desired flow instead
//		of lungFlow.
//
//  Revision: 003  By:  yyy    Date:  28-Jun-1999    DR Number: DCS 5452
//       Project:  ATC
//       Description:
//			Fixed calculation for MAX_PEEP_LOOP_COUNT using 5 instead
//			of CYCLE_TIME_MS which caused MAX_PEEP_LOOP_COUNT being
//			evaluated wrong (extern constant).
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  syw    Date:  14-Jan-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//             ATC initial version.
//
//=====================================================================

#include "TcvPhase.hh"

//@ Usage-Classes
#include "BreathMiscRefs.hh"
#include "Tube.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TcvPhase()  
//
//@ Interface-Description
//		Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Register callback with O2Mixture and initialize data members.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

TcvPhase::TcvPhase(void) : CompensationBasedPhase()
{
	CALL_TRACE("TcvPhase::TcvPhase(void)") ;

	// $[TI1]
	RO2Mixture.registerBreathPhaseCallBack( this) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TcvPhase()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

TcvPhase::~TcvPhase(void)
{
	CALL_TRACE("TcvPhase::~TcvPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TcvPhase::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, TCVPHASE,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineResistivePressure_()
//
//@ Interface-Description
//		This method has lung flow as an argument and returns nothing.
//		This method is called by the base class to obtain the resistive
//		component of the pressure trajectory.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The resistive pressure is the pressure drop across the tube.  This
//		pressure is scaled by the percent support level.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
TcvPhase::determineResistivePressure_( const Real32 lungFlow)
{
	CALL_TRACE("TcvPhase::determineResistivePressure_( const Real32 lungFlow)") ;

	// $[TI1]
	
	resistivePressure_ = RTube.getPressDrop(lungFlow) ;
	resistivePressure_ *= percentSupport_ ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineElasticPressure_()
//
//@ Interface-Description
//		This method has lung volume as an argument and returns nothing.
//		This method is called by the base class to obtain the elastic
//		component of the pressure trajectory.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The elastic pressure is zero.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
TcvPhase::determineElasticPressure_( const Real32 lungVolume)
{
	CALL_TRACE("TcvPhase::determineElasticPressure_( const Real32 lungVolume)") ;
	UNUSED_SYMBOL(lungVolume);	

	// $[TI1]

	elasticPressure_ = 0.0 ;
}
//=====================================================================
//
//  Private Methods...
//
//=====================================================================



