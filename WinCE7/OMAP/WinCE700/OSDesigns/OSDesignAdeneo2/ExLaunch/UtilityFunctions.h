
// ****************************************************************************
//
//  UtilityFunctions.h - This contains misc global functions which are not
//          associated with a class.
//
// ****************************************************************************

#ifndef UTILITY_FUNCTIONS_H

#define UTILITY_FUNCTIONS_H

#include <windows.h>
#include "Crc.h"

bool ValidateFile(LPCTSTR FileName, LPCTSTR CrcFileName);

#endif



