#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  TrendPresetTypeSetting - Trend Preset Type Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a non-batch, discrete setting which indicates the type of 
//  presets being displayed in the Trend graph or table subscreen. 
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TrendPresetTypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: mnr    Date:  07-Jan-2010    SCR Number: 6437
//  Project: Trend
//  Description:
//      Using MAX_PRESETS instead of TOTAL_TREND_PRESET_TYPES
//
//  Revision: 001   By: rhj    Date:  05-Jun-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//      Initial version
//=====================================================================

#include "TrendPresetTypeSetting.hh"
#include "TrendPresetTypeValue.hh"
#include "AcceptedContext.hh"
#include "ContextMgr.hh"
//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  TrendPresetTypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TR01148] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendPresetTypeSetting::TrendPresetTypeSetting(void)
	: NonBatchDiscreteSetting(SettingId::TREND_PRESET,
							  Setting::NULL_DEPENDENT_ARRAY_,
							  MAX_PRESETS)
{
	CALL_TRACE("TrendPresetTypeSetting()");
}	


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~TrendPresetTypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendPresetTypeSetting::~TrendPresetTypeSetting(void)	
{
	CALL_TRACE("~TrendPresetTypeSetting(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	TrendPresetTypeSetting::getApplicability(const Notification::ChangeQualifier) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	return(Applicability::CHANGEABLE);
}	

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	TrendPresetTypeSetting::getDefaultValue(void) const
{
	CALL_TRACE("getDefaultValue()");


	return(DiscreteValue(TrendPresetTypeValue::TREND_PRESET_NONE));	   
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//          [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	TrendPresetTypeSetting::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							TREND_PRESET_TYPE_SETTING, lineNumber, pFileName,
							pPredicate);
}


