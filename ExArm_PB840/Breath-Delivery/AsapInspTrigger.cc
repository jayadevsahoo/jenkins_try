#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AsapInspTrigger -  Triggers inspiration as soon as
//    possible after a mode change.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers the first breath following a mode transition.  It is
//    enabled and/or disabled by schedulers.  This trigger is kept on a list
//    contained in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    This trigger implements the algorithm for phasing in the first
//    breath of a new mode after the operator has changed the mode
//    setting, or reset apnea ventilation by pressing alarm reset.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of 
//    whether this trigger is enabled or not.  If the trigger is not 
//    enabled, it will always return a state of false.  If it is enabled,
//    and on the active trigger list in the BreathTriggerMediator, the 
//    condition monitored by the trigger is checked every BD cycle.
//    This trigger is enabled whenever a mode change is detected.  Since
//    a mode change does not interrupt any inspiration in progress, it
//    is possible to have this trigger enabled, and not on the active
//    BreathTrigger list.  When this is the case, it will become active
//    as soon as the inspiration is completed, and the active 
//    BreathTrigger list is changed.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this trigger may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/AsapInspTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 013  By: yab     Date:  18-Apr-2002    DR Number: 5899
//  Project:  VCP
//  Description:
//      Removed wrong tracing to IT requirement [04048] from Modification log.
//
//  Revision: 012  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 011  By:  yyy    Date:  10-Nov-98    DR Number: 5263, 5264
//       Project:  Sigma (R8027)
//       Description:
//			Added new parameter: isInspiratoryPhase in setBiLevelModeChangeCondition()
//			to identify when bilevel transitions to other modes, the breath phase
//			is in inspiration or not.
//			Added new method resetBiLevelModeChangeCondition() to reset the
//			biLevelNoticeWasReceived_ flag during relinquish control of Spont mode.
//			In triggerCondition_ fixed code for the bilevel spont breath mode change.
//
//  Revision: 010  By:  yyy    Date:  05-Nov-98    DR Number: 5252
//       Project:  Sigma (R8027)
//       Description:
//             Corrected mapping SRS to code.
//
//  Revision: 009  By:  yyy    Date:  08-Oct-1998    DR Number: 5260
//       Project:  BiLevel
//       Description:
//             Enable output to serial port for integration test
//			   for SRS 04049.
//
//  Revision: 008  By:  yyy    Date:  08-Oct-1998    DR Number: 
//       Project:  BiLevel
//       Description:
//             Initial bilevel version
//
//  Revision: 007  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 006  By:  iv    Date:  17-Jan-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review of Breath-Delivery schedulers.
//
//  Revision: 005  By:  iv    Date:  23-Dec-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 004  By:  sp    Date:  31-Oct-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 003  By:  iv    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date:  12-Feb-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Change BreathPhaseScheduler to SchedulerId (600).
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "AsapInspTrigger.hh"

#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "BreathType.hh"

#include "BreathRecord.hh"

#include "BreathSet.hh"

#include "ApneaInterval.hh"

#if defined(BL04049_IT) || defined(SIGMA_UNIT_TEST)
#include "PrintQueue.hh"
#include "Ostream.hh"
#endif

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AsapInspTrigger()  
//
//@ Interface-Description
//	Default Contstructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize TriggerId_ using BreathTrigger's constructor, 
//  peepHighToLowTransitionTime_ to 0, spontAndModeChangeOccuredAtPeepLow_
//  and biLevelModeChangeHappened_ to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AsapInspTrigger::AsapInspTrigger(
	void
) : BreathTrigger(Trigger::ASAP_INSP)
{
  CALL_TRACE("AsapInspTrigger()");
  // $[TI1]
  
  peepHighToLowTransitionTime_ = 0;
  spontAndModeChangeOccuredAtPeepLow_ = FALSE;
  biLevelModeChangeHappened_ = FALSE;
  biLevelNoticeWasReceived_ = FALSE;
  isInspiratoryPhase_ = FALSE;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AsapInspTrigger()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

AsapInspTrigger::~AsapInspTrigger(void)
{
  CALL_TRACE("~AsapInspTrigger()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBiLevelModeChangeCondition()
//
//@ Interface-Description
// This method takes three parameters:
//	peepHighToLowTransitionTime - the peep high to low transition time.
//	biLevelBreathType - most recent type of breath in bilevel.
//	spontAndModeChangeOccuredAtPeepLow - indicator for spont and
//	mode change both occured at peep low.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply sets the data members with parameters passed in.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
AsapInspTrigger::setBiLevelModeChangeCondition(Int32 peepHighToLowTransitionTime,
									   Int32 biLevelBreathType,
									   Boolean isInspiratoryPhase,
									   Boolean spontAndModeChangeOccuredAtPeepLow)
{
  CALL_TRACE("setBiLevelModeChangeCondition()");

  // $[TI1]
  peepHighToLowTransitionTime_ = peepHighToLowTransitionTime;
  biLevelBreathType_ = (::BreathType)biLevelBreathType;
  isInspiratoryPhase_ = isInspiratoryPhase;
  spontAndModeChangeOccuredAtPeepLow_ = spontAndModeChangeOccuredAtPeepLow;
  biLevelNoticeWasReceived_ = TRUE;
  biLevelModeChangeHappened_ = TRUE;

#if defined(BL04049_IT) || defined(SIGMA_UNIT_TEST)
	sprintf( string, "biLevelNoticeWasReceived_ = %d", biLevelNoticeWasReceived_);
	PrintQueue::SendString( string) ;
#endif // BL04049_IT

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetBiLevelModeChangeCondition()
//
//@ Interface-Description
// This method takes no parameters and returns no value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply sets the data members biLevelNoticeWasReceived_ to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
AsapInspTrigger::resetBiLevelModeChangeCondition(void)
{
  CALL_TRACE("resetBiLevelModeChangeCondition()");

  // $[TI1]
  biLevelNoticeWasReceived_ = FALSE;

#if defined BL04049_IT || SIGMA_UNIT_TEST
	sprintf( string, "biLevelNoticeWasReceived_ = %d", biLevelNoticeWasReceived_);
	PrintQueue::SendString( string) ;
#endif // BL04049_IT
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable()
//
//@ Interface-Description
//     This method takes no parameters and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//     The method calls the enable() method in the parent class and then
//     resets local flags to their initial values.  If the trigger is
//	   not enabled, then we shall enable the trigger.  Once the trigger is
//	   enabled then if the BiLevel scheduler didn't notified the trigger
//	   about a mode change, all data members are reset to their default
//	   values.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//     none
//@ End-Method
//=====================================================================
void
AsapInspTrigger::enable(void)
{
	CALL_TRACE("enable(void)");

	if (!isEnabled_)
	{	// $[TI3.1]
		Trigger::enable();
  
		if (!biLevelNoticeWasReceived_)
		{	// $[TI1]
			peepHighToLowTransitionTime_ = 0;
			spontAndModeChangeOccuredAtPeepLow_ = FALSE;
			biLevelBreathType_ = ::NON_MEASURED;
			biLevelModeChangeHappened_ = FALSE;
		}	// $[TI2]
		biLevelNoticeWasReceived_ = FALSE;
	}	// $[TI3.2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
AsapInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, ASAPINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_()
//
//@ Interface-Description
// This method takes no parameters.  If the trigger condition has occured,
// the method returns true, otherwise, the method returns false.
// This trigger is a breath trigger designed to determine the start of the
// first breath after a mode change. It makes sure that the first breath
// after a mode change does not start during the restricted part of the breath,
// and that apnea is not triggered because of the mode change, and if the breath
// rate in the new mode is greater than the previous rate, that there
// is enough time for exhalation based on Te >= 2.5 * Ti.
// The trigger is used when the user requests a mode change, or 
// when a change to or from apnea ventilation occurs. The breath period is 
// not used as part of the trigger condition when the transition
// involves spontaneous scheduler (breath period does not exist)
// To avoid race conditions with the apnea trigger, a modified apnea 
// interval is used for that trigger which is 100 milliseconds less than 
// the actual apnea interval.  
// $[04231] $[04248] $[04263] $[04265] $[04266] $[04267] $[04269] $[04270] $[04271]
// $[BL04049] :a On transition to BiLevel mode, the breath shall be
// delivered during the unrestricted phase of exhalation.
// $[BL04049] :d On transition to BiLevel mode, if the current breath
// is a spont breath, the time ...
//---------------------------------------------------------------------
//@ Implementation-Description
//    The following conditions must be met if an inspiration is to be
//    triggered:
//        1.  a) The phase is not restricted.
//            b) The mode is SPONT or breath type is SPONT or
//               biLevel breath type is SPONT.
//            c) The breath duration must be > additionalWaitTime + 2.5 * inspiratory time
//               or breath duration >= apnea interval.
//        2.  a) The phase is not restricted.
//            b) The mode is not SPONT.
//            c) The breath type is CONTROL, ASSIST or
//               biLevel breath type is CONTROL or ASSIST
//            d) The breath duration must be > additionWaitTime + 2.5 * inspiratory time 
//               or breath duration > breathPeriod
//               or breath duration >= apnea interval
//    If one of the above conditions ( 1 or 2 ) are met, then this method returns true.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

Boolean
AsapInspTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");
  Boolean returnValue = FALSE;
  BreathRecord* pCurrentRecord = RBreathSet.getCurrentBreathRecord();
  BreathType  breathType = pCurrentRecord->getBreathType();
  Real32 breathDuration =  pCurrentRecord->getBreathDuration();
  Real32 breathPeriod = BreathRecord::GetBreathPeriod();
  Real32 inspTime = BreathRecord::GetInspiratoryTime();
  // get the scheduler id (which is the previous schduler) from the
  // current breath record
  SchedulerId::SchedulerIdValue schedulerId =
			pCurrentRecord->getSchedulerId();
  // Make sure that this trigger fires before Apnea trigger,
  // because mode triggers are evaluated before breath triggers. 
  Real32 apneaIntervalForAsapTrigger = (Real32)RApneaInterval.getApneaInterval() - 100.0F;

  Real32 additionalWaitTime = inspTime;

  if (biLevelModeChangeHappened_ &&
  	 !spontAndModeChangeOccuredAtPeepLow_ &&
  	 !isInspiratoryPhase_)
  {
	  // $[TI6]
	  additionalWaitTime = (Real32) (peepHighToLowTransitionTime_);
  }// $[TI7] 

  if ( !BreathRecord::IsPhaseRestricted() &&
       ((schedulerId == SchedulerId::SPONT) ||
        (breathType == ::SPONT) ||
        (biLevelBreathType_ == ::SPONT)))
  {
    // $[04248]
    // $[TI2]
    if ((breathDuration > (additionalWaitTime + 2.5F * inspTime)) ||
        (breathDuration >= apneaIntervalForAsapTrigger))
    {
	    // $[TI2.1]
	    returnValue = TRUE;
	    
#if defined BL04049_IT || SIGMA_UNIT_TEST
	sprintf( string, "%s", "\n*************************" );
	PrintQueue::SendString( string) ;
	sprintf( string, "biLevelModeChangeHappened_ %d, %d, %d",
					biLevelModeChangeHappened_, spontAndModeChangeOccuredAtPeepLow_, isInspiratoryPhase_);
	PrintQueue::SendString( string) ;
	sprintf( string, "IsPhaseRestricted = %d, schedulerId = %d(), breathType = %d, %d",
					BreathRecord::IsPhaseRestricted(), schedulerId, breathType, biLevelBreathType_) ;
	PrintQueue::SendString( string) ;
	sprintf( string, "breathDuration  = %.2f, additionalWaitTime = %.2f(), inspTime = %.2f",
					breathDuration , additionalWaitTime, inspTime) ;
	PrintQueue::SendString( string) ;
	sprintf( string, "apneaInterval = %.2f", apneaIntervalForAsapTrigger) ;
	PrintQueue::SendString( string) ;
#endif // BL04049_IT

    }  	// $[TI2.2]
    
#if defined BL04049_IT || SIGMA_UNIT_TEST
	sprintf( string, ".") ;
	PrintQueue::SendString( string) ;
#endif // BL04049_IT

  } 
  else if ( !BreathRecord::IsPhaseRestricted() &&
            (schedulerId != SchedulerId::SPONT) &&
             ((breathType == ::CONTROL) || (breathType == ::ASSIST) ||
             ((biLevelBreathType_ == ::CONTROL) || (biLevelBreathType_ == ::ASSIST))) &&  
             ((breathDuration > (additionalWaitTime + 2.5F * inspTime)) ||
              (breathDuration > breathPeriod) ||
              (breathDuration >= apneaIntervalForAsapTrigger)) )
  {
    // $[TI3]
    returnValue = TRUE;

#if defined BL04049_IT
	sprintf( string, "%s", "\n*************************" );
	PrintQueue::SendString( string) ;
	sprintf( string, "IsPhaseRestricted = %d, schedulerId = %d(), breathType = %d, %d",
					BreathRecord::IsPhaseRestricted(), schedulerId, breathType, biLevelBreathType_) ;
	PrintQueue::SendString( string) ;
	sprintf( string, "breathDuration  = %.2f, additionalWaitTime = %.2f, inspTime = %.2f",
					breathDuration , additionalWaitTime, inspTime) ;
	PrintQueue::SendString( string) ;
	sprintf( string, "breathPeriod = %.2f, apneaInterval = %.2f",
					breathPeriod, apneaIntervalForAsapTrigger) ;
	PrintQueue::SendString( string) ;
#endif // BL04049_IT

  }
  // $[TI1]

  // TRUE: $[TI4]
  // FALSE: $[TI5]
  return (returnValue);
}



//=====================================================================
//
//  Private Methods...
//
//=====================================================================

