#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DEBUG - The DEBUG Subsystem's Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is the Utilities Subsytem's subsystem class.  This class
//  is reponsible for the initialization, via its initialization method
//  ('Utilities::Initialize()'), of this subsystem.
//---------------------------------------------------------------------
//@ Rationale
//  Each subsystem contains a class that is responsible for initializing
//  the subsytem.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Only the fault-handling system needs initialization.  Its
//  initialization method ('FaultHandler::Initialize()') is called
//  by this subsystem's initialization method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  The initialization is only to be called once during any
//  initialization sequence.
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/Utilities.ccv   25.0.4.0   19 Nov 2013 14:35:16   pvcs  $
//
//@ Modification-Log
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "Utilities.hh"

//@ Usage-Classes
#include "FaultHandler.hh"
#undef CALL_TRACE_OFF
#include "CallTraceMacros.hh"

#if defined(SIGMA_BD_CPU)
#include "GainsManager.hh"
using namespace GainsManagerNS;
#endif
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  Initialize this subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Utilities::Initialize(void)
{
#if defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)
  CallTrace::Initialize();     // must be called before 'CALL_TRACE()'...
  CALL_TRACE("Initialize()");
#endif  // defined(CALL_TRACE_ON)  &&  !defined(CALL_TRACE_OFF)

  FaultHandler::Initialize();

#if SIGMA_BD_CPU
#if CONTROLS_TUNING
   static Char* funcName = "Initialize";

   Boolean gmInitStatusSuccess = TRUE;

   // Initialization of the GainsManager
   GainsManager& gm = GainsManager::getInstance();
   gm.reset();

   for(int i=GAINS_NAMES_LOW; i<=GAINS_NAMES_HIGH; i++)
   {
	   if( gm.registerGain( (GainsManagerNS::GainsNames)i ) == TRUE )
	   {
	   }
	   else
	   {
		   gmInitStatusSuccess = FALSE;
		   break;
	   }
   }

   gm.registrationComplete(gmInitStatusSuccess);
#endif
#endif

}  // $[TI1]
