#ifndef  M_MISC_HH
#define  M_MISC_HH
#ifdef SIGMA_DEBUG
// This file contains the function prototypes and globals for pbmon - a test monitor.

// ****************************************************************************
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
// *****************************************************************************


#define  SERIAL_PORT_A   0
#define  SERIAL_PORT_B   1

typedef  enum
{
   BAUD_9600,
   BAUD_19200,
   BAUD_38400,
   BAUD_115200
} BAUD_RATE;
 
typedef  struct
{
   int   interrupt_driven:1;     //1 => port is interrupt driven
   int           xon_xoff:1;     //1 => port is using xon/xoff
} SERIAL_PORT_OPTIONS;

#define  SERIAL_BUFFER_SIZE   (1024)

typedef  struct
{
int                  receive_insert_index;
int                  receive_remove_index;
int                  transmit_insert_index;
int                  transmit_remove_index;
char                 tickle_transmit;  //1 => send next char immediately to 
                                       //     start/continue interrupts
                                       //2 => lock out interrupt routine
char                 interrupt_register;
SERIAL_PORT_OPTIONS  options;
char                 receive_buffer[SERIAL_BUFFER_SIZE];
char                 transmit_buffer[SERIAL_BUFFER_SIZE];
} SERIAL_PORT;


#define  NUMBER_OF_SERIAL_PORTS  (2)

#define  Z85230_VECTOR_BASE    (80)
#define  Z85230B_COMMAND       *((volatile char *)0xFFBE8021L)
#define  Z85230B_DATA          *((volatile char *)0xFFBE8023L)
#define  Z85230A_COMMAND       *((volatile char *)0xFFBE8025L)
#define  Z85230A_DATA          *((volatile char *)0xFFBE8027L)



// Stuff for Debug exception handler processing.


#define  NMI_VECTOR_NUMBER   	(31)
#define  NMI_SOURCE             (*((unsigned char *)0xffbe8008L))


typedef  struct
{
   char *effective_address;
} FORMAT2_3;

typedef  struct
{
   char                 *effective_address;
   unsigned short int   special_status_register;
   unsigned short int   write_back3_status;
   unsigned short int   write_back2_status;
   unsigned short int   write_back1_status;
   char                 *fault_address;
   char                 *write_back_3_address;
   unsigned long int    write_back_3_data;
   char                 *write_back_2_address;
   unsigned long int    write_back_2_data;
   char                 *write_back_1_address;
   unsigned long int    write_back_1_data;
   unsigned long int    push_data_lw1;
   unsigned long int    push_data_lw2;
   unsigned long int    push_data_lw3;
} FORMAT7;

typedef  struct
{
//   unsigned long int junk[18];

   unsigned long  int d[8];
   unsigned long  int a[7];
   unsigned short  int sr_pc_vect[4];

// Note: The above array implements the following equivalent structure
// The compiler is optimizing out the program counter incorrectly
//   unsigned short int status_register;
//   char	    *program_counter;
//   unsigned short int    format_and_vector_offset;

   union
   {
      FORMAT2_3  format2_3;
      FORMAT7    format7;

   } frame;

} EXCEPTION_STACK_FRAME; 

extern void    InitializeSerialPorts(void);
extern void    InitializeDiagnosticInterface(int port_number);
extern void    InitializeDiagnosticPort(int port);
extern int     dprintf(char *fmt, ...);
extern int     dprintf_b(char *fmt, ...);
extern void    send_string(char *);
extern void    send_string_b(char *);
extern char    send_char(char);
extern char    send_char_b(char);
extern char    SendString(int port, char *string);
extern int     SendIsReady(int port);
extern char    SendChar(int port, char ch);
extern char    GetChar(int port);
extern void    ResetSerialPort (int port_number);
extern unsigned long int VBR(void);
char   get_char_direct(void);


inline void    write_85230(int port_number, int destination, unsigned char value);
inline unsigned char  read_85230(int port_number, int source);


#include "misc.in"

#endif  // SIGMA_DEBUG
#endif  // M_MISC_HH
