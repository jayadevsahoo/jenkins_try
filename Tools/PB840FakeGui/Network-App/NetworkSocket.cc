
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: NetworkSocket -  Provides interface to the standard Network
//    Socket functionality, a parent class of all the socket classes.
//---------------------------------------------------------------------

//@ Interface-Description
//    The NetworkSocket class encapsulates a socket oriented network
//    communcation. This class provides most common functionalities
//    of sending and receiving network data on a socket. Most of the
//    methods are virtual so child classes can inherit or override
//    these methods to handle different types of socket communcation.
//
//    The class provides a sequence number for each network message
//      sent. A packet sequence number and the network message
//    id on a socket uniquely identifies a network message. These two
//    information can be used to track down a message on a specific
//    socket connection.
//
//    The NetworkSocket class provides the sendMsg and recvMsg virtual
//    methods to send and receive a complete network message. Each 
//    message is prepended with a network message header.
//    The class utilizes a send and receive mutex locking mechanism
//    to guarantee a complete network message is transmitted on a
//    given socket before a new network message transmission request.
//
//    A network message header is a fixed structure and contains
//    a network message id, data size and a packet sequence number. 
//
//    The class also provides the sendData and readData virtual methods
//    which send and receives a given number of bytes of data on the
//    current socket. An application code can interface with these
//    methods directly if it wants to process a network message header
//    information and its data. The lowest level data transmission
//    methods are the 'receive' and 'transmit' virtual methods which
//    directly moves data back and forth into the TCP/IP Stack-Ware
//    buffer.  All the repeated socket library errors are logged to
//    the Non-Volatile diagnostic memory area.
//    The reInit method reinitializes socket attributes after
//    a socket is opened.
//
//---------------------------------------------------------------------
//@ Rationale
//    
//    This class captures most of the common behaviour of all the
//    socket communication so to maximize the code sharing amongst
//    all different types of socket communication necessary for the
//    Sigma operation. All the communcation methods are modularized
//    finely so subclasses can override only the portion of communication
//    that it differs from.
//---------------------------------------------------------------------
//@ Implementation-Description
//    A network socket is created as a non-blocking IO mode as default.
//    An open socket id is passed to reInit method to be initialized
//    into the NetworkSocket object. When a socket call returns an error
//    status within the sendData and recvData methods, the 'send' and
//    'recv' operations are repeated up to four times just in the case
//    that the error condition is gone.
//    
//@ Fault-Handling
//  Some of the repeated socket library errors are recorded to
//  the non-volatile diagnostic memory. All the failed requests
//  return an error status to calling methods. Each method follows
//  the contract programming practice to generate a soft fault in
//  the case of software logic error. 
//---------------------------------------------------------------------
//@ Restrictions
//   One static MutEx object that is used for synchronizing a packet
//   serial number should have been instance variable,
//   so each socket instance has its own  MutEx object instead 
//   of sharing with all other NetworkSocket instances.
//   But mutex ids are predefined in Sigma and it is
//   hard to dynamically allocate a unique mutex for each instance.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/NetworkSocket.ccv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: gdc     Date: 30-Dec-2010  SCR Number: 6714
//  Project:  XENA2
//  Description:
//      Changed so an out of mbufs condition brings down communications
//      allowing stack to free all mbufs and restart communications.
//
//  Revision: 007  By: gdc     Date: 30-Dec-2010  SCR Number: 6722
//  Project:  XENA2
//  Description:
//      Generate message sequence numbers only for debug builds.
//
//  Revision: 006  By: gdc     Date: 30-Dec-2010  SCR Number: 6697
//  Project:  XENA2
//  Description:
//      Removed code to broadcast BD IP address as this logic is no 
//      longer used and consumes all mbufs causing a BD reset
//      during communication loss with the GUI.
//
//  Revision: 005  By: gdc     Date: 30-Dec-2010  SCR Number: 6717
//  Project:  XENA2
//  Description:
//      Corrected creation and initialization of all socket types.
//
//  Revision: 004  By: gdc     Date: 30-Dec-2010  SCR Number: 6716
//  Project:  XENA2
//  Description:
//      Changed to use thread-safe socket library.
//
//  Revision: 003  By: gdc     Date: 30-Dec-2010  SCR Number: 6712
//  Project:  XENA2
//  Description:
//      Use socket sendmsg() instead of send() to eliminate copy 
//      operation.
//
//  Revision: 002  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  <jhv>    Date:  13-Sep-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "StdAfx.h"
#include "NetworkSocket.hh"
#include "NetworkApp.hh"

extern "C" Uint32 get_my_ip_addr();

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NetworkSocket(void)  [Default Constructor]
//
//@ Interface-Description
//    The socket is initially set to a closed state.
//---------------------------------------------------------------------
//@ Implementation-Description
//    SocketId_ instance variable is set to -1 to indicate the
//    socket is closed.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

NetworkSocket::NetworkSocket(void)
{
    socketId_ = -1;    
    setLostMsgFlag_(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reInit(Int sockId)
//
//@ Interface-Description
//    Initializes the socket id with a passed in socket id. The passed
//    socket id should a non-zero which makes the socket open.
//    Reinitialize the packet sequence number to zero. 
//---------------------------------------------------------------------
//@ Implementation-Description
//   The socket option is set to maintain the TCP/IP level keep
//   alive protocol on the socket link.
//---------------------------------------------------------------------
//@ PreCondition
//	(sockId > 0)
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
NetworkSocket::reInit(Int sockId)
{
    CLASS_PRE_CONDITION(sockId > 0);
    socketId_ = sockId;
    sequenceNum_ = 0;
    maxMsgRecvTime_ = MAX_INPUT_WAIT_DURATION;
    setLostMsgFlag_(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~NetworkSocket()  [Destructor]
//
//@ Interface-Description
//   Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//     Close the socket if it is opened()
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

NetworkSocket::~NetworkSocket(void)
{
    CALL_TRACE("~NetworkSocket()");
    if ( opened() )
    {
        down(); 
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:: sendMsg( XmitDataMsgId msgId, char *buf, Uint nbytes)
//
//@ Interface-Description
//    This method sends a requested network message on this socket.
//    The message data is in the 'buf' memory area. The 'nbytes' is 
//    a message data size not including a message header.
//    If a given 'nbytes' value is zero, only the message header
//    portion is sent out to the network without any accompanying data.
//    The method returns a number of data bytes that are
//    successfully transmitted. The method returns -1 value when there
//    is an error. When the socket is not open, the method doesn't
//    transmit any data and returns -1.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//    The method copies a network message header and copies user
//    data into a local XmitBuf structure on a calling task's stack.
//    And it calls one socket library call to pass the data to the TCP/IP
//    code. The TCP/IP code ensures that the data given to
//    one call is transmitted as a contiguous data stream.
//--------------------------------------------------------------------- 
//@ PreCondition
//   (nbytes <= MAX_NETWORK_BUF_SIZE)
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

Int
NetworkSocket::sendMsg( XmitDataMsgId msgId, char *buf, Uint nbytes)
{

    CLASS_PRE_CONDITION( nbytes <= MAX_NETWORK_BUF_SIZE);
    if ( !opened() )
    {
        return -1; 
    }

	NetworkApp::XmitBuf sendBuf;
    sendBuf.header.msgId = msgId;
    sendBuf.header.pktSize = nbytes;
    sendBuf.header.seqNumber = getNextSequenceNum();
    if ( nbytes && buf)
    {
		memcpy((char *) sendBuf.data, (char *) buf,  (size_t) nbytes);
    }

    int count = 0;
	Uint bytesToSend = nbytes + sizeof(sendBuf.header);
	count = send(socketId_, (const char*)&sendBuf, bytesToSend, 0);

    if ( count == SOCKET_ERROR )
    {
		int error = WSAGetLastError();
		if( (error != WSAEWOULDBLOCK) && (error != WSAEINPROGRESS) )
		{
			NetworkApp::ReportCommError("NetworkSocket:sendMsg, SENDMSG()", error);
			setLostMsgFlag_(TRUE);
			return -1;
		}
    }
    else if ( count !=  nbytes + sizeof(sendBuf.header) )
    {
		NetworkApp::ReportCommError("NetworkSocket::broadcastMsg, SENDMSG() bytes", count);
        setLostMsgFlag_(TRUE);
        return -1;
    }

    return nbytes; 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:: recvMsg( XmitDataMsgId msgId, char *buf, Uint maxBytes)
//
//@ Interface-Description
//    The method receives a given network message to a user provided
//    data area. The message header information is not put into the
//    user data area. If the method doesn't receive a specified
//    network message, it generates an assertion. If a received
//    network message size is larger than a specified maximum
//    bytes, the method returns -1. All other type of communication
//    errors are detected by the 'readData' method and an error message
//    will be logged to the non-volatile diagnostic memory. The method
//    returns -1 when there is an communication error or the socket
//    is not open.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//   (maxBytes <= MAX_NETWORK_BUF_SIZE)
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int 
NetworkSocket::recvMsg( XmitDataMsgId msgId, char *pData, 
                        Uint32 maxBytes ) 
{
    CLASS_PRE_CONDITION(maxBytes <= MAX_NETWORK_BUF_SIZE);

    NetworkMsgHeader header;

    if (!opened() )
    {
        return -1; 
    }

    if ( readData((char *) &header, sizeof(NetworkMsgHeader)) 
         == sizeof( NetworkMsgHeader))
    {
        if ( header.pktSize <=  maxBytes)
        {
            if (header.pktSize && ( readData(pData, header.pktSize) 
                                    != header.pktSize ))
            {
                return -1;   
            }
            CLASS_ASSERTION(header.msgId == msgId);
            return header.pktSize; 
        }
    }
    return -1;   
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  readData( char *buf, int nbytes)
//
//@ Interface-Description
// The readData method receives 'nbytes' of network data
// from this socket into the 'buf' memory area. The method returns
// a byte count of the received data. If any communication error,
// the method returns -1. Otherwise, the method returns nbytes.
// All the socket library call errors
// are examined and logged internally.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
// All the sockets are implemented as a non-blocking IO socket
// to avoid a Task getting permanently suspended at the lower
// level TCP/IP code when the physical ethernet cable gets 
// disconnected.
// For a non-block  mode socket, the receive method reads only
// those data bytes that are already accumulated in the socket
// buffer. 
//
// If there is more data to read than what is on the socket during
// the recv() call, this method polls the socket for more data using
// ioctl() and delays 100 milliseconds between polling attempts to 
// allow the Stackware task time to read more network data into the 
// socket buffer.
//
// The polling ioctl() method is preferred to using "select for
// read" as ioctl can run outside the network critical section to read
// the socket. ioctl also uses fewer CPU cycles than select(). During
// development, we determined that select for read used so much of
// the CPU resources when the network was jammed that lower priority 
// tasks did not have a chance to run would cause more task monitor resets.
// This method is also preferred to calling recv() in a loop since recv()
// requires the same network critical section lock (elevating the calling
// task priority to network high) and consumes even more processing time 
// than the select() function.
//--------------------------------------------------------------------- 
//@ PreCondition
//         ( buf && nbytes )
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Int
NetworkSocket::readData(char *buf, Uint nbytes)
{
    CLASS_PRE_CONDITION( buf && nbytes );
    if (!opened())
    {
        return -1; 
    }

    OsTimeStamp waitTime;
    Int nextBytes = nbytes;
    int result;

    while ( nextBytes > 0 )
    {
        result = MT_recv(socketId_, buf, nextBytes, 0);

		// received some bytes
        if ( result > 0 )
        {
            nextBytes -= result;
            buf = (char *)buf + result;
        }
        else	// connection gracefully closed or receive failed
        {
			NetworkApp::ReportCommError( "NetworkSocket::readData RECV()", ::WSAGetLastError());
            return -1; 
        }

        if ( nextBytes > 0 )
        {
            // We still have more data to read. Poll for more data on the socket 
            // using ioctl(FIONREAD). See implementation description above.
            u_long bytesReady;
            Int taskReport = 0;
            do
            {
                if ( (errno = MT_ioctl(socketId_, FIONREAD, &bytesReady)) != 0 )
                {
                    NetworkApp::ReportCommError("NetworkSocket::readData IOCTL()", ::WSAGetLastError());
                    return -1;  
                }
                if ( bytesReady == 0 )
                {
                    if ( waitTime.getPassedTime() >= maxMsgRecvTime_ )
                    {
                        NetworkApp::ReportCommError("NetworkSocket:readData SELECT() timeout", 
                                                    NETWORKAPP_RECV_TIMEOUT);
                        return -1; 
                    }
                    Task::Delay(0,100);
                    if ( taskReport++ % 4 == 0 )
                    {
                        TaskMonitor::Report();
                    }
                }
            } while ( !bytesReady );
        }
    }

    return nbytes;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  down()
//
//@ Interface-Description
//      close the socket.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Call the 'close' socket library call to close the socket.
//      While a socket is closed, socketId_ is set to -1.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
NetworkSocket::down( )
{
    if (!opened())
    {
        return; 
    }
    MT_close(socketId_);
    socketId_ = -1;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifyConnection(XmitDataMsgId, XmitDataMsgId)
//
//@ Interface-Description
//    As long as this socket is opened, return a true value.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method is overriden by child classes if more verification
//    is necessary.
//---------------------------------------------------------------------
//@ PreCondition
//      ( opened() )
//   This socket should have been opened.
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//---------------------------------------------------------------------
SigmaStatus
NetworkSocket::verifyConnection(XmitDataMsgId, XmitDataMsgId)
{
    CLASS_PRE_CONDITION(opened());
    return SUCCESS; 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getNextSequenceNum()
//
//@ Interface-Description
//  Increment the packet sequence number and return the sequence
//  number for this network socket.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Increment sequence number under mutex to allow for multiple threads.
//  Only generate sequence numbers for debugging. Otherwise, return
//  zero.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

Uint32
NetworkSocket::getNextSequenceNum(void)
{
#ifdef SIGMA_DEBUG
    CriticalSection mutex(NETAPP_SENDMSG_ACCESS_MT);
    return ++sequenceNum_;
#else
    return 0;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: openTcpServer
//
//@ Interface-Description
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Int 
NetworkSocket::openTcpServer(PortId portId, Int numClients)
{
    int errno;

    PRINTF(("NetworkSocket::openTcpServer: portid = %d, numClients = %d, ", portId, numClients));

    if ( (socketId_ = MT_socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET )
    {
        NetworkApp::ReportCommError("NetworkSocket::openTcpServer socket()", ::WSAGetLastError());
        return -1;
    }
    PRINTF(("socketId = %d\n", socketId_));

    // Allow other sockets to bind() to this port, unless here is an active listening
    // socket bound to this port already. Avoids "address already in use" when restarting
    // the socket.
    int reuseon=1;
    if ( (errno = MT_setsockopt(socketId_, SOL_SOCKET, SO_REUSEADDR,(char *)&reuseon, sizeof(reuseon)) ) != 0 )
    {
		NetworkApp::ReportCommError("NetSocket::openTcpServer setsockopt(REUSEADDR)", ::WSAGetLastError());
        MT_close(socketId_);
        return -1;
    }

    struct sockaddr_in  servaddr;
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(portId);
    servaddr.sin_family      = AF_INET;
    if ( (errno = MT_bind(socketId_, (sockaddr *)&servaddr, sizeof(servaddr)) ) != 0 )
    {
		NetworkApp::ReportCommError("NetworkSocket::openTcpServer BIND()", ::WSAGetLastError());

        MT_close(socketId_);
        return -1;
    }

    if ( (errno = MT_listen(socketId_, numClients ) ) != 0 )
    {
        MT_close(socketId_);
        NetworkApp::ReportCommError( "NetworkSocket::openTcpServer LISTEN()", ::WSAGetLastError());
        return -1;
    }

    reInit(socketId_);

    return socketId_;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  acceptConnection( NetworkSocket &rSocket, Int32  waitDuration)
//
//@ Interface-Description
// Accepts a socket connection from the GUI board. 
// After verifying that the new socket connection can send
// and receive data, the new socket information is initialized 
// into the "rSocket" object. 
//--------------------------------------------------------------------- 
//@ Implementation-Description 
// This method is called only on the BD board to make a TCP/IP
// socket connection between the BD and GUI.
// The contactSocket in NetworkApp::OpenConnections is the socket 
// used by the GUI board to make a connection to the BD. When the 
// connection is accepted, a new socket is created for the connection 
// itself.
// This method uses the select() function to wait for readability on
// the contact socket. If the select succeeds, this method calls the
// accept() function to create a new socket that is set to non-blocking
// and assigned to the referenced NetworkSocket parameter.
//--------------------------------------------------------------------- 
//@ PreCondition
//  ( rSocket.opened() == FALSE )
//  (waittime % SELECT_TIMEOUT == 0)
//---------------------------------------------------------------------
//@ PostCondition 
//        none
//@ End-Method 
//===================================================================== 

Int 
NetworkSocket::acceptConnection( NetworkSocket &rSocket, Uint32 waittime )
{
    const Int SELECT_TIMEOUT = 500;
    CLASS_PRE_CONDITION( rSocket.opened() == FALSE );
    CLASS_PRE_CONDITION(waittime % SELECT_TIMEOUT == 0);

    int errno;
    Int newSocket;
    OsTimeStamp startTime;

    PRINTF(("NetworkSocket::acceptConnection socketId_ = %d\n", socketId_));

    fd_set readMask;
    FD_ZERO(&readMask);
    FD_SET(socketId_, &readMask);
    TaskMonitor::Report();

    timeval timeout;
	timeout.tv_sec = waittime / 1000;
	timeout.tv_usec = (waittime % 1000) * 1000;

	errno = MT_select(0, &readMask, NULL, NULL, &timeout);

    TaskMonitor::Report();
    if ( errno == SOCKET_ERROR )
    {
        NetworkApp::ReportCommError("NetworkSocket::acceptConnection SELECT()", ::WSAGetLastError());
        return -1;  
    }
	else if ( errno == 0 )		// time limit expired
	{
		// Don't report a communications error on a select timeout. Otherwise,
		// log would fill with connect timeouts if GUI isn't communicating.
		PRINTF(("NetworkSocket::acceptConnection SELECT() timeout"));
		return -1;  
	} 

    newSocket = accept(socketId_, (struct sockaddr *)0, 0);

	if ( newSocket == INVALID_SOCKET )
    {
        NetworkApp::ReportCommError("NetworkSocket::acceptConnection:ACCEPT()", ::WSAGetLastError());
        return -1;
    }

    u_long nonblockingon = 1;
    if ( (errno = MT_ioctl(newSocket, FIONBIO, &nonblockingon)) != 0 )
    {
        NetworkApp::ReportCommError("NetworkSocket::acceptConnection: IOCTL(FIONBIO)", ::WSAGetLastError());
        MT_close(newSocket);
        return -1; 
    }

    // assign the new socket to the reference socket and (re)initialize
    rSocket.reInit(newSocket);

    TaskMonitor::Report();
    if ( rSocket.verifyConnection(NetworkApp::ThisBoardAliveMsg,
                                  NetworkApp::OtherBoardAliveMsg ) == FAILURE )
    {
        PRINTF(("NetworkSocket::acceptConnection verifyConnection failed.\n"));
        rSocket.down();
        return -1;
    }

    return newSocket; 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: openTcpClient
//
//@ Interface-Description
//   This method is called from the GUI board to establish a socket
//   connection to the BD board as a client. The calling routine 
//   provides a BD board's internet address and port number.
//   After a socket connection is made, tests whether the socket can be
//   used to transmit and receive data.
//--------------------------------------------------------------------
//@ Implementation-Description
//  Need to REVISE - TBD
//    The call to the connect socket library routine is repeated
//    up to 12 times after pausing a given wait duration.
//    This is to provide enough time for the other board to come to
//    the socket connection phase. The socket mode is modified to be
//    a non-blocking while connecting to the other board.
//    The occassional long delay in the blocked mode socket connect
//    was noticed during the development.  In this way, the connect
//    call returns before a complete TCP/IP socket connection is
//    finished and the higher level code and check the statusi
//    periodically.  In this way, the code has more control over
//    when to stop the connection process if there is too long of 
//    delay in connecting to the other board.
//
//    After a socket connection is made, the socket mode is changed
//    back to the blocking mode.
//
//   
//---------------------------------------------------------------------
//@ PreCondition
//         (opened() == FALSE )
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Int
NetworkSocket::openTcpClient(Uint32 ipAddr, PortId portId)
{
    CLASS_PRE_CONDITION(opened() == FALSE);
    int errorCode;

    PRINTF(("openTcpClient: ipAddr = %08x, port = %d, ", ipAddr, portId));

    struct sockaddr_in  servaddr;  // server address

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;   
    servaddr.sin_addr.s_addr = htonl(ipAddr);
    servaddr.sin_port        = htons(portId);

	SOCKET newSocket = socket(AF_INET, SOCK_STREAM, 0);
	if ( newSocket == INVALID_SOCKET )
    {
        NetworkApp::ReportCommError("NetSocket::openTcpClient_() socket()", ::WSAGetLastError());
        return -1;
    }
    PRINTF(("socketId = %d\n", newSocket));

    u_long nonblockingon = 1;
    if ( MT_ioctl(newSocket, FIONBIO, &nonblockingon) != 0 )
    {
        MT_close(newSocket);
        NetworkApp::ReportCommError( "NetworkSocket::openTcpClient_(): IOCTL(FIONBIO)", ::WSAGetLastError());
        return -1;
    }

    /// perform non-blocking socket connect using connect and select
	errorCode = MT_connect(newSocket , (struct sockaddr *)&servaddr, sizeof(servaddr));
    if ( errorCode == SOCKET_ERROR )
	{
		errorCode = WSAGetLastError();

		if ( errorCode != WSAEWOULDBLOCK && errorCode != WSAEISCONN && errorCode != WSAEINPROGRESS )
		{
			closesocket(newSocket);
			PRINTF(("connect() errorCode = %d\n", errorCode));
			return -1;
		}
	}

	// call select to wait for writeability or timeout or error
	fd_set fdset;
	FD_ZERO(&fdset);
	FD_SET(newSocket, &fdset);

	timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	errorCode = select(0, NULL, &fdset, NULL, &timeout);

	if ( errorCode == 0 )
	{
		closesocket(newSocket);
		PRINTF(("select() timeout\n"));
		return -1;
	}
	else if ( errorCode == SOCKET_ERROR )
	{
		errorCode = WSAGetLastError();
		closesocket(newSocket);
		PRINTF(("select() failed, errorCode = %d\n", errorCode));
		return -1;
	}
	// else successful select

	// determine if connection succeeded or failed using getsockopt()
	int sockError;
	int len = sizeof(sockError);

	// get and clear the errno from the socket 
	if ( getsockopt(newSocket, SOL_SOCKET, SO_ERROR, (char*)&sockError, &len) != 0 )
	{
		errorCode = WSAGetLastError();
		PRINTF(("getsockopt() errno = %d", errorCode));
		closesocket(newSocket);
		return -1;
	}

	if ( sockError != 0 )
	{
		PRINTF(("connection failed, sockError = %d\n",sockError));
		closesocket(newSocket);		
		return -1;
	}

    // set up the timeout periods etc...
    reInit(newSocket);

    if ( verifyConnection(NetworkApp::ThisBoardAliveMsg,
                          NetworkApp::OtherBoardAliveMsg ) == FAILURE )
    {
        PRINTF(("verifyConnection failed.\n"));
        return -1;
    }

    return newSocket;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setLostMsgFlag_(Boolean flag)
//
//@ Interface-Description
//        Sets a lost message indicator. If a TRUE value is passed in
//      as the flag indicator value,  the lost message indicator is
//      set to indicate that a network message is lost due to either
//      receive or tranmit communication errors. If a FALSE value is
//      passed in, the lost message indicator is cleared.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      Sets the lostMsgFlag_ instance variable.
//      If the lost message indicator is set a true value, the method
//      sets the NetworkApp communication down state.
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
NetworkSocket::setLostMsgFlag_(Boolean value)
{
    if ( value == TRUE )
    {
        NetworkApp::SetCommDown_();    
    }
    lostMsgFlag_  = value; 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  anyLostMsg()
//
//@ Interface-Description
//      Determines if this socket has lost a network message. A network
//      message is considered lost when a network message cannot be
//      received or transmitted to the repeated communication errors.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
Boolean
NetworkSocket::anyLostMsg() const
{
    return lostMsgFlag_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
NetworkSocket::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, NETWORK_APPLICATION, NETWORK_SOCKET_CLASS,
                            lineNumber, pFileName, pPredicate);
}
