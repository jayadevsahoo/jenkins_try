
#ifndef AirFlowSensorTest_HH
#define AirFlowSensorTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AirFlowSensorTest - BD Safety Net Test for the Air Flow
//                            Sensor reading OOR test
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/AirFlowSensorTest.hhv   10.7   08/17/07 09:32:52   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  by    Date:  19-Sep-1996    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "SafetyNetTest.hh"

//@ Usage-Classes
#include "Background.hh"
//@ End-Usage


class AirFlowSensorTest : public SafetyNetTest
{
    public:

        AirFlowSensorTest( void );
        ~AirFlowSensorTest( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL,
			       const char*       pPredicate = NULL );

    virtual BkEventName checkCriteria( void );
  
    protected:

    private:

        // these methods are purposely declared, but not implemented...
        AirFlowSensorTest( const AirFlowSensorTest& );  // not implemented...
        void operator=( const AirFlowSensorTest& );     // not implemented...

        //@ Data-Member: numCyclesOorHigh_
        // The number of consecutive cycles that the test has
        // indicated the air flow sensor flow signal is out of
        // range on the high end.
        // This test must differentiate between OOR-High and
        // OOR-Low and therefore does not use the
        // numCyclesCriteriaMet_ data attribute of the base class.
        Int16 numCyclesOorHigh_;
 
        //@ Data-Member: numCyclesOorLow_
        // The number of consecutive cycles that the test has
        // indicated the air flow sensor flow signal is out of
        // range on the low end.
        // This test must differentiate between OOR-High and
        // OOR-Low and therefore does not use the
        // numCyclesCriteriaMet_ data attribute of the base class.
        Int16 numCyclesOorLow_;
};


#endif // AirFlowSensorTest_HH 
