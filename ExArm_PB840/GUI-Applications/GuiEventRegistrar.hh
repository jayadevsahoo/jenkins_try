#ifndef GuiEventRegistrar_HH
#define GuiEventRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: GuiEventRegistrar - The central registration, control, and
// dispatch point for all GUI state change events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiEventRegistrar.hhv   25.0.4.0   19 Nov 2013 14:07:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  29-AUG-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "GuiApp.hh"
#include "GuiAppClassIds.hh"
class GuiEventTarget;
//@ End-Usage


class GuiEventRegistrar
{
public:
	static void GuiEventHappened(GuiApp::GuiAppEvent eventId);
	static void RegisterTarget(GuiEventTarget* pTarget);
	static inline void Initialize(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	GuiEventRegistrar(void);						// not implemented...
	GuiEventRegistrar(const GuiEventRegistrar&);	// not implemented...
	~GuiEventRegistrar(void);						// not implemented...
	void operator=(const GuiEventRegistrar&);		// not implemented...

	enum
	{
		//@ Constant: MAX_TARGETS_
		// The maximum number of target pointers that can be stored by this
		// registrar.
		MAX_TARGETS_ = 50
	};

	//@ Data-Member: TargetArray_
	// A static array of GuiEventRecord_'s, indexed by the
	// GuiApp::GuiAppEvent enum.  Each array element contains a MsgTimer
	// object, a pointer to a registered callback object and a flag showing
	// whether the timer is currently active.
	static GuiEventTarget *TargetArray_[MAX_TARGETS_];

	//@ Data-Member: NumTargets_
	// The number of targets currently registered with this class.
	static Int32 NumTargets_;
};

// Inlined methods
#include "GuiEventRegistrar.in"

#endif // GuiEventRegistrar_HH 
