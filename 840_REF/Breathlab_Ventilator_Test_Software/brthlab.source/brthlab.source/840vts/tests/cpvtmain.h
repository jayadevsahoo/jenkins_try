/*****************************************************************************/
/*   Module Name:    cpvtmain.h                                            
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.2      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _CPVTMAIN_H_
#define _CPVTMAIN_H_

//#include <userint.h>

enum PANELSTATES {
	INITIAL,
	SPLASHINIT,
	APPSTART1,
	APPSTART2,
	APPRESTART
};

#define PANELS		 30
#define OPTIONPANELS 30

#define PANEL_HEIGHT 490
#define PANEL_WIDTH  760

extern int gPanelHandle[PANELS];
extern int gOPanelHandle[OPTIONPANELS];

extern char *gSeqDescription;
extern char *gRevision;   /* PVCS Revision string */
extern char gProgramRev[];
extern char gUUTPN[];
extern char gWorkOrderNumber[];
extern char gVentAssetNumber[];
extern char gCurrOperator[];
extern char gStationID[];
extern char gRunNumberFile[]; /* Name of .run file */
extern char gTestResFile[];   /* Name of .db1 file */
extern char gTestRunFile[];   /* Name of .db2 file */
extern char gSeqReportFile[];
extern char gSerialNum[];
extern int gTxState;
extern int gSequenceRunning;
extern int gNumTests;
extern int gPausedInd;
extern int gStoppedInd;
extern int gMustReset840;
extern int gOverallpass;
extern int gStationNum;
extern int gJetSeqID;
extern long int gRunNumber;
extern short gRptFileMode;
extern short gRptFileLock;
extern int sStatusDispVal;
extern int sStopOnFail;
extern int sAbortFlag;

extern void SetSplashVersionNum(void);
extern int ExitWrapper(void);

#ifdef OLD_SYS_STATUS
#define BLANK   0
#define LOGIN   1
#define READY   2
#define OPACT   3
#define SLFTST  4
#define STFAIL  5
#define STPASS  6
#define PASSED  7
#define PFAIL   8
#define CAUTION 9
#define STBY    10
#define LOOP    11
#define PABORT  12
#else
enum SYS_STATES{
	BLANK=0,
	LOGIN,
	READY,
	OPACT,
	SLFTST,
	STFAIL,
	STPASS,
	PASSED,
	PFAIL,
	CAUTION,
	STBY,
	LOOP,
	PABORT
};
#endif // #ifdef OLD_SYS_STATUS

void LoadPanels(int state);

#endif // #define _CPVTMAIN_H_
