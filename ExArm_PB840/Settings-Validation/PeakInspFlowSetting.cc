#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PeakInspFlowSetting - Peak Inspiratory Flow Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the maximum rate of
//  delivery (in liters-per-minute) of the specified "tidal volume" during
//  mandatory, volume-based breaths (VCV).  This class inherits from
//  'BoundedSetting' and provides the specific information needed for
//  representation of peak inspiratory flow.  This information includes the
//  interval and range of this setting's values, this setting's response to
//  a Main Control Setting's transition (see 'acceptTransition()'), this
//  batch setting's new-patient value (see 'getNewPatientValue()'), and the
//  contraints and dependent settings of this setting.
//
//  This class provides a static method for calculating peak inspiratory flow
//  based on the value of other settings.  This calculation method provides
//  a standard way for all settings (as needed) to calculate a peak
//  inspiratory flow value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines a 'calcDependentValues_()' method for updating
//  this setting's dependent settings, and an 'updateConstraints_()' method
//  for updating the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PeakInspFlowSetting.ccv   25.0.4.0   19 Nov 2013 14:27:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 020  By: mnr     Date: 07-Jan-2010     SCR Number: 6437
//  Project:  NEO
//  Description:
//       Added SCR number to the previous header.
//
//  Revision: 019   By: mnr    Date: 23-Dec-2009     SCR Number: 6437
//  Project:  NEO
//  Description:
//		SCR number added to previous header and code changes.		
//
//  Revision: 018   By: mnr    Date: 23-Nov-2009     SCR Number: 6549
//  Project:  NEO
//  Description:
//		Enforce lower bounds for PeakInspFlow, returning new min for NEONATAL
//
//  Revision: 017   By: mnr    Date: 28-July-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Scoping braces added to fix compilation error.
//
//  Revision: 016   By: mnr    Date: 28-July-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Declaration scope updated according to code review action item.
//
//  Revision: 015   By: mnr    Date: 24-July-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Enforce lower bound for SQUARE_PATTERN and in acceptPrimaryChange()
//
//  Revision: 014   By: mnr    Date: 20-July-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Adherence to coding standards in getNewPatientValue()
//
//  Revision: 013   By: mnr    Date: 14-July-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Different min value for Neomode in getNewPatientValue()
//
//  Revision: 012   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 011  By: sah    Date:  06-Nov-2000    DR Number: 5789
//  Project:  VTPC
//  Description:
//      Modified to use wider min/max limits in VC+.  This allows the
//      clinician more flexibility with their Vt/Ti combinations.
//
//  Revision: 010  By: sah    Date:  05-Oct-2000    DR Number: 5730
//  Project:  VTPC
//  Description:
//      Modified new-patient formula to use twice the IBW factor for
//      RAMP flow patterns, and modified transition limits based on
//      new NEONATAL constant.
//
//  Revision: 009  By: sah     Date:  18-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  removed min insp flow as a dependent setting, now
//         automatically tracks changes to this setting
//      *  added support for new VC+ mandatory type value
//      *  now this setting is a dependent setting (needs
//         'acceptPrimaryChange()') when mandatory type is VC+
//      *  added 'testTransition()' to support new condition transitions
//      *  modified DEVELOPMENT-only code for testing 'VC+'
//      *  modified 'calcDependentValue_()' to support VC+ value
//      *  added new VC+-specific min/max bounds
//      *  modified 'calcBasedOnSetting_()' to support this setting
//         being a dependent setting of resp rate, tidal volume, Ti,
//         Te and I:E, in VC+
//
//  Revision: 008   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//	*  incorporated circuit-specific new-patient values and ranges
//	*  re-wrote 'acceptTransition()' to handle NEONATAL circuit
//	*  added new 'getAbsolute{Min,Max}Value_()' methods to accomodate
//         new, dynamic limits
//
//  Revision: 007   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 006   By: dosman Date:  23-Sep-1998    DR Number: BiLevel 66 & 144
//  Project:  BILEVEL
//  Description:
//      The intermediate value of inspiratory time computed in
//      calcBasedOnSetting_ was changed to be warped in the same
//      direction as the direction the "setting being calculated".
//      Also, special processing was added in BoundedSetting::calcNewValue()
//
//  Revision: 005   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 004   By: syw    Date: 06-Jan-1998    DR Number: 1624
//  Project:  BILEVEL
//  Description:
//	Bilevel baseline.  Added BiLevel support.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged  Rev "Color" (1.32.1.0) into Rev "BiLevel" (1.32.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 002   By: sah    Date: 23 Dec 1996    DR Number: 1624
//  Project: Sigma (R8027)
//  Description:
//	Incorporated new PC-to-VC formulas.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PeakInspFlowSetting.hh"
#include "SettingConstants.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "FlowPatternValue.hh"
#include "PatientCctTypeValue.hh"
#include "ConstantParmValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
#include "ConstantParmSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// This array is set up dynamically via this class's 'calcDependentValues_()',
// because all dependents are not applicable in 'SPONT' mode...
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
{
	SettingId::IE_RATIO,
	SettingId::INSP_TIME,
	SettingId::EXP_TIME,

	SettingId::NULL_SETTING_ID
};


//  $[02218] -- The setting's range ...
//  $[02220] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	0.0f,		// absolute minimum...
	0.0f,		// unused...
	TENTHS,		// unused...
	NULL
};
static const BoundedInterval  NODE1_ =
{
	20.0f,		// change-point...
	0.1f,		// resolution...
	TENTHS,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	150.0f,		// absolute maximum...
	1.0f,		// resolution...
	ONES,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PeakInspFlowSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeakInspFlowSetting::PeakInspFlowSetting(void)
: BatchBoundedSetting(SettingId::PEAK_INSP_FLOW,
					  ::ArrDependentSettingIds_,
					  &::INTERVAL_LIST_,
					  PEAK_INSP_FLOW_MAX_BASED_CCT_TYPE_ID,// maxConstraintId
					  PEAK_INSP_FLOW_MIN_BASED_CCT_TYPE_ID)// minConstraintId
{
	CALL_TRACE("PeakInspFlowSetting()");
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PeakInspFlowSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeakInspFlowSetting::~PeakInspFlowSetting(void)
{
	CALL_TRACE("~PeakInspFlowSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	PeakInspFlowSetting::getApplicability(
										 const Notification::ChangeQualifier qualifierId
										 ) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	DiscreteValue  mandTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			mandTypeValue = pMandType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			mandTypeValue = pMandType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicabilityId;

	switch ( mandTypeValue )
	{
		case MandTypeValue::PCV_MAND_TYPE :	  // $[TI3]
			applicabilityId = Applicability::INAPPLICABLE;
			break;
		case MandTypeValue::VCV_MAND_TYPE :	  // $[TI4]
			applicabilityId = Applicability::CHANGEABLE;
			break;
		case MandTypeValue::VCP_MAND_TYPE :	  // $[TI5]
			applicabilityId = Applicability::VIEWABLE;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(mandTypeValue);
			break;
	}

	return(applicabilityId);
}

														   
//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02219] -- new-patient equations...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	PeakInspFlowSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);
	const Setting*  pFlowPattern =
		SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();
	const DiscreteValue  FLOW_PATTERN_VALUE =
		pFlowPattern->getNewPatientValue();

	Real32  ibwScalingFactor;
	Real32  absoluteMin = getAbsoluteMinValue_();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			ibwScalingFactor = SettingConstants::NP_PEAK_FLOW_NEO_IBW_SCALE;
			absoluteMin = SettingConstants::MIN_PEAK_FLOW_NEO_VALUE;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :	  // $[TI2]
			ibwScalingFactor = SettingConstants::NP_PEAK_FLOW_PED_IBW_SCALE;
			break;
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI3]
			ibwScalingFactor = SettingConstants::NP_PEAK_FLOW_ADULT_IBW_SCALE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	// The adjusted context contains the new-patient value for IBW.
	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	BoundedValue  newPatient;

	// calculate the new patient value...
	newPatient.value = (IBW_VALUE * ibwScalingFactor);

	if ( FLOW_PATTERN_VALUE == FlowPatternValue::RAMP_FLOW_PATTERN )
	{  // $[TI6]
		newPatient.value *= 2.0f;
	}  // $[TI7]

	// clip to absolute minimum...
	newPatient.value = MAX_VALUE(absoluteMin,		  // $[TI4]
								 newPatient.value);	  // $[TI5]

	// warp, and clip, the calculated value to a "click" boundary...
	getBoundedRange().warpValue(newPatient);

	return(newPatient);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned, and this setting's value is "clipped"
//  to that bound.  Otherwise, a value of 'NULL' is returned to indicate no
//  dependent bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
	PeakInspFlowSetting::acceptPrimaryChange(
											const SettingId::SettingIdType primaryId
											)
{
	CALL_TRACE("acceptPrimaryChange(primaryId)");

	// update dynamic constraints...
	updateConstraints_();

	BoundStatus&  rBoundStatus = getBoundStatus_();

	// initialize to holding this setting's id...
	rBoundStatus.setViolationId(getId());

	BoundedValue  newValue;

	newValue = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
								   BASED_ON_ADJUSTED_VALUES);
	
	// get the absolute minimum peak inspiratory flow value...
	const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
	// Enforce lower bound...[NE02000]
	newValue.value = MAX_VALUE( ABSOLUTE_MIN, newValue.value );

	// test 'newValue' against this setting's bounded range; the bound
	// violation state, if any, is returned in 'rBoundStatus', while
	// 'newValue' is "clipped" if a bound is violated...
	getBoundedRange_().testValue(newValue, rBoundStatus);

	// store the value...
	setAdjustedValue(newValue);

	const BoundStatus*  pBoundStatus;

	// if there is a bound violation return a pointer to this setting's
	// bound status, otherwise return 'NULL'...
	pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus // $[TI1]
				   : NULL;	   // $[TI2]

	return(pBoundStatus);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  testTransition(settingId, toValue, fromValue)  [virtual]
//
//@ Interface-Description
//  This virtual method is used by mandatory type to determine whether
//  the explicit, circuit-based transition rules of tidal volume and
//  peak insp flow are to be called.  If 'CONDITIONAL' is returned, the
//  circuit-based transition rules are needed.  If 'STAND_ALONE' is
//  returned, the current inspiratory time and tidal volume can be left
//  unchanged, and peak insp flow's transition value is already set.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (settingId == SettingId::MAND_TYPE  &&
//   (toValue == MandTypeValue::VCV_MAND_TYPE  ||
//    toValue == MandTypeValue::VCP_MAND_TYPE))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Setting::TransitionState
	PeakInspFlowSetting::testTransition(const SettingId::SettingIdType settingId,
										const DiscreteValue            toValue,
										const DiscreteValue            fromValue)
{
	CALL_TRACE("resetToStoredValue(settingId, toValue, fromValue)");
	AUX_CLASS_PRE_CONDITION((settingId == SettingId::MAND_TYPE),
							settingId);
	AUX_CLASS_PRE_CONDITION((toValue == MandTypeValue::VCV_MAND_TYPE  ||
							 toValue == MandTypeValue::VCP_MAND_TYPE),
							toValue);

	// ensure that this setting's min/max constraints are set appropriate to
	// the current mandatory type value (which is 'toValue')...
	updateConstraints_();

	BoundStatus  dummyStatus(getId());

	Setting::TransitionState  xitionState;

	switch ( fromValue )
	{
		case MandTypeValue::PCV_MAND_TYPE :		  // $[TI1]
			// $[02022]\d\   -- PC-to-VC:  force circuit-based transition rules...
			// $[VC02006]\h\ -- PC-to-VC+:  force circuit-based transition rules...
			xitionState = Setting::CONDITIONAL;
			break;

		case MandTypeValue::VCV_MAND_TYPE :		  // $[TI2]
			// $[VC02006]\d\ -- VC-to-VC+:  check peak flow's value...
			{
				// calculate the current peak flow value, based on the current plateau
				// time, flow pattern, tidal volume and inspiratory time...
				BoundedValue  peakFlow = calcBasedOnSetting_(SettingId::INSP_TIME,
															 BoundedRange::WARP_NEAREST,
															 BASED_ON_ADJUSTED_VALUES);
				
				// test 'peakFlow' against this setting's bounded range; the bound
				// violation state, if any, is returned in 'dummyStatus', while
				// 'peakFlow' is "clipped" if a bound is violated...
				getBoundedRange_().testValue(peakFlow, dummyStatus);

				if ( dummyStatus.isInViolation() )
				{  // $[TI2.1] -- a bound violation occurred...
					// this will cause the circuit-based transition rules for tidal volume
					// and peak insp flow to be executed...
					xitionState = Setting::CONDITIONAL;
				}
				else
				{  // $[TI2.2] -- no bound violation occurred...
					// leave tidal volume and inspiratory time unchanged...
					xitionState = Setting::STAND_ALONE;

					// store this value...
					setAdjustedValue(peakFlow);
				}
			}
			break;

		case MandTypeValue::VCP_MAND_TYPE :		  // $[TI3]
			// $[02022]\j\ -- VC+-to-VC:  make sure peak flow is within VC range...
			{
				BoundedValue  peakFlow = getAdjustedValue();
                BoundedValue  peakFlowLimitBasedOnInspTime =  calcBasedOnSetting_(SettingId::INSP_TIME,
															 BoundedRange::WARP_NEAREST,
															 BASED_ON_ADJUSTED_VALUES);

				// check if the peak flow is out of range due to inspiratory time.
				Boolean isValid = IsEquivalent(  peakFlow.value,  peakFlowLimitBasedOnInspTime.value,  TENTHS );
				
				// test 'peakFlow' against this setting's bounded range; the bound
				// violation state, if any, is returned in 'dummyStatus', while
				// 'peakFlow' is "clipped" if a bound is violated...
				getBoundedRange_().testValue(peakFlow, dummyStatus);

				if ( dummyStatus.isInViolation() || !isValid )
				{  // $[TI3.1] -- a bound violation occurred...
					// this will cause the circuit-based transition rules for tidal volume
					// and peak insp flow to be executed...
					xitionState = Setting::CONDITIONAL;
				}
				else
				{  // $[TI3.2] -- no bound violation occurred...
					// leave tidal volume, peak flow and inspiratory time unchanged...
					xitionState = Setting::STAND_ALONE;
				}
			}
			break;

		default :
			// unexpected 'fromValue'...
			AUX_CLASS_ASSERTION_FAILURE(fromValue);
			break;
	}

	return(xitionState);
} 


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The setting is only interested in the transition of mandatory type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The analysis that documents the definition of the "regions", and
//  their corresponding formulas, can be found in the Design History
//  File.  The document titled "Summary Analysis of Mandatory Type's
//  PC-to-VC Transition Rules" was submitted to the DHF in December 1996.
//
//  IMPORTANT:  due to peak inspiratory flow's dependency, in some cases,
//              on the value of tidal volume, tidal volume must be updated
//              via its transition rule BEFORE peak inspiratory flow.
//
//  $[02022]\d\   -- from-PC-to-VC transition rules
//  $[VC02006]\d\ -- from-VC-to-VC+ transition rules
//  $[VC02006]\h\ -- from-PC-to-VC+ transition rules
//  $[NE02000]\b\ -- to VC/VC+/non-SPONT (NEONATAL circuit)...
//  $[NE02000]\d\ -- to VC/SPONT (NEONATAL circuit)...
//  $[NE02001]\b\ -- to VC/VC+/non-SPONT (PEDIATRIC circuit)...
//  $[NE02001]\d\ -- to VC/SPONT (PEDIATRIC circuit)...
//  $[NE02002]\b\ -- to VC/VC+/non-SPONT (ADULT circuit)...
//  $[NE02002]\d\ -- to VC/SPONT (ADULT circuit)...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	PeakInspFlowSetting::acceptTransition(
										 const SettingId::SettingIdType settingId,
										 const DiscreteValue            newValue,
										 const DiscreteValue            currValue
										 )
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	//=====================================================================
	// mandatory type transition to VC, or VC+...
	//=====================================================================

	const Setting*  pFlowPattern =
		SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
	const Setting*  pRespRate = SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);

	const DiscreteValue  FLOW_PATTERN_VALUE =
		pFlowPattern->getAdjustedValue();
	const DiscreteValue  MODE_VALUE = pMode->getAdjustedValue();

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;
	const Real32  RESP_RATE_VALUE =
		BoundedValue(pRespRate->getAdjustedValue()).value;

    const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	BoundedValue  newPeakInspFlow;

	//-------------------------------------------------------------------
	// NOTE:  the formulas below only consider the affects of respiratory
	//        rate, tidal volume and IBW, following this giant switch statement
	//        the resulting value is clipped based on this settings bounds...
	//-------------------------------------------------------------------

	switch ( MODE_VALUE )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :		  // $[TI1]
			switch ( FLOW_PATTERN_VALUE )
			{
				//-------------------------------------------------------------------
				// flow pattern = 'DESCENDING RAMP'...
				//-------------------------------------------------------------------
				case FlowPatternValue::RAMP_FLOW_PATTERN :		// $[TI1.1]
					switch ( PATIENT_CCT_TYPE_VALUE )
					{
						case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI1.1.1]
							if ( RESP_RATE_VALUE >= 40.0f )
							{	// $[TI1.1.1.1] -- f >= 40...
								const Setting*  pTidalVolume =
									SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

								const Real32  TIDAL_VOLUME_VALUE =
									BoundedValue(pTidalVolume->getAdjustedValue()).value;

								if ( TIDAL_VOLUME_VALUE >= 15.0f )
								{  // $[TI1.1.1.1.1] -- min flow is 10% of peak flow...
									newPeakInspFlow.value = (0.53f * TIDAL_VOLUME_VALUE);
								}
								else
								{  // $[TI1.1.1.1.2] -- min flow is fixed at 0.8 L/min...
									newPeakInspFlow.value = ((0.60f * TIDAL_VOLUME_VALUE) - 0.8f);
								}
							}
							else
							{	// $[TI1.1.1.2] -- f < 40...
								newPeakInspFlow.value =
									(2.0f * SettingConstants::NP_PEAK_FLOW_NEO_IBW_SCALE * IBW_VALUE);
							}
							{
								// get the absolute minimum peak inspiratory flow value...
								const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
								// Enforce lower bound...[NE02000] \b\ i
								newPeakInspFlow.value = MAX_VALUE( ABSOLUTE_MIN, newPeakInspFlow.value );
							}
							break;
						case PatientCctTypeValue::PEDIATRIC_CIRCUIT : // $[TI1.1.2]
							if ( RESP_RATE_VALUE >= 34.0f )
							{	// $[TI1.1.2.1] -- f >= 34...
								const Setting*  pTidalVolume =
									SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

								const Real32  TIDAL_VOLUME_VALUE =
									BoundedValue(pTidalVolume->getAdjustedValue()).value;

								newPeakInspFlow.value = (0.39f * TIDAL_VOLUME_VALUE);
							}
							else
							{	// $[TI1.1.2.2] -- f < 34...
								newPeakInspFlow.value =
									(2.0f * SettingConstants::NP_PEAK_FLOW_PED_IBW_SCALE * IBW_VALUE);
							}
							break;
						case PatientCctTypeValue::ADULT_CIRCUIT : // $[TI1.1.3]
							if ( RESP_RATE_VALUE >= 32.0f )
							{	// $[TI1.1.3.1] -- f >= 32...
								const Setting*  pTidalVolume =
									SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

								const Real32  TIDAL_VOLUME_VALUE =
									BoundedValue(pTidalVolume->getAdjustedValue()).value;

								newPeakInspFlow.value = (0.39f * TIDAL_VOLUME_VALUE);
							}
							else
							{	// $[TI1.1.3.2] -- f < 32...
								newPeakInspFlow.value =
									(2.0f * SettingConstants::NP_PEAK_FLOW_ADULT_IBW_SCALE * IBW_VALUE);
							}
							break;
						default :
							// unexpected patient circuit type value...
							AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
							break;
					}
					break;

					//-------------------------------------------------------------------
					// flow pattern = 'SQUARE'...
					//-------------------------------------------------------------------
				case FlowPatternValue::SQUARE_FLOW_PATTERN :	// $[TI1.2]
					switch ( PATIENT_CCT_TYPE_VALUE )
					{
						case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI1.2.1]
							if ( RESP_RATE_VALUE >= 56.0f )
							{	// $[TI1.2.1.1] -- f >= 56...
								const Setting*  pTidalVolume =
									SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
								const Real32  TIDAL_VOLUME_VALUE =
									BoundedValue(pTidalVolume->getAdjustedValue()).value;

								newPeakInspFlow.value = (0.30f * TIDAL_VOLUME_VALUE);
							}
							else
							{	// $[TI1.2.1.2] -- f < 56...
								newPeakInspFlow.value =
									(SettingConstants::NP_PEAK_FLOW_NEO_IBW_SCALE * IBW_VALUE);
							}
							{
								// get the absolute minimum peak inspiratory flow value...
								const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
								// Enforce lower bound...[NE02000] \b\ i
								newPeakInspFlow.value = MAX_VALUE( ABSOLUTE_MIN, newPeakInspFlow.value );
							}
							break;
						case PatientCctTypeValue::PEDIATRIC_CIRCUIT : // $[TI1.2.2]
							if ( RESP_RATE_VALUE >= 38.0f )
							{	// $[TI1.2.2.1] -- f >= 38...
								const Setting*  pTidalVolume =
									SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
								const Real32  TIDAL_VOLUME_VALUE =
									BoundedValue(pTidalVolume->getAdjustedValue()).value;

								newPeakInspFlow.value = (0.21f * TIDAL_VOLUME_VALUE);
							}
							else
							{	// $[TI1.2.2.2] -- f < 38...
								newPeakInspFlow.value =
									(SettingConstants::NP_PEAK_FLOW_PED_IBW_SCALE * IBW_VALUE);
							}
							break;
						case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI1.2.3]
							if ( RESP_RATE_VALUE >= 35.0f )
							{	// $[TI1.2.3.1] -- f >= 35...
								const Setting*  pTidalVolume =
									SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);
								const Real32  TIDAL_VOLUME_VALUE =
									BoundedValue(pTidalVolume->getAdjustedValue()).value;

								newPeakInspFlow.value = (0.21f * TIDAL_VOLUME_VALUE);
							}
							else
							{	// $[TI1.2.3.2] -- f < 35...
								newPeakInspFlow.value =
									(SettingConstants::NP_PEAK_FLOW_ADULT_IBW_SCALE * IBW_VALUE);
							}
							break;
						default :
							// unexpected patient circuit type value...
							AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
							break;
					}
					break;

				default :
					// invalid flow pattern value...
					AUX_CLASS_ASSERTION_FAILURE(FLOW_PATTERN_VALUE);
					break;
			};
			break;

		case ModeValue::SPONT_MODE_VALUE :			  // $[TI2]
		case ModeValue::CPAP_MODE_VALUE :
			{
				Real32  ibwFactor;

				switch ( PATIENT_CCT_TYPE_VALUE )
				{
					case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI2.1]
						ibwFactor = SettingConstants::NP_PEAK_FLOW_NEO_IBW_SCALE;
						break;
					case PatientCctTypeValue::PEDIATRIC_CIRCUIT : // $[TI2.2]
						ibwFactor = SettingConstants::NP_PEAK_FLOW_PED_IBW_SCALE;
						break;
					case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2.3]
						ibwFactor = SettingConstants::NP_PEAK_FLOW_ADULT_IBW_SCALE;
						break;
					default :
						// unexpected patient circuit type value...
						AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
						break;
				}

				// set to new-patient value...
				newPeakInspFlow.value = (ibwFactor * IBW_VALUE);

				switch ( FLOW_PATTERN_VALUE )
				{
					case FlowPatternValue::RAMP_FLOW_PATTERN :		  // $[TI2.4]
						// multiply new-patient value by ramp factor...
						newPeakInspFlow.value *= 2.0f;
						break;
					case FlowPatternValue::SQUARE_FLOW_PATTERN :  // $[TI2.5]
						// do nothing...
						break;
					default :
						// invalid flow pattern value...
						AUX_CLASS_ASSERTION_FAILURE(FLOW_PATTERN_VALUE);
						break;
				};
			}
			break;

		case ModeValue::BILEVEL_MODE_VALUE :
		default :
			// invalid mode value...
			AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
			break;
	};

	// ensure limits are updated...
	updateConstraints_();

	// round/clip calculated value..
	getBoundedRange_().warpValue(newPeakInspFlow);

	// store the transition value into the adjusted context...
	setAdjustedValue(newPeakInspFlow);

	// activation of Transition Rule MUST be italicized...
	setForcedChangeFlag();
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?  Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	PeakInspFlowSetting::isAcceptedValid(void) const
{
	CALL_TRACE("isAcceptedValid()");

	Boolean  isValid = BoundedSetting::isAcceptedValid();

	if ( isValid )
	{
		const Applicability::Id  APPLIC_ID =
			getApplicability(Notification::ACCEPTED);

		Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

		switch ( APPLIC_ID )
		{
			case Applicability::CHANGEABLE :
				// ensure that inspiratory time is properly set based on this setting...
				isValid = pInspTime->isAcceptedValid();
				break;

			case Applicability::VIEWABLE :
				{
					const Real32  PEAK_FLOW_VALUE = BoundedValue(getAcceptedValue()).value;
					const Real32  CALC_PEAK_FLOW_VALUE = calcBasedOnSetting_(
																			SettingId::TIDAL_VOLUME,
																			BoundedRange::WARP_NEAREST,
																			BASED_ON_ACCEPTED_VALUES).value;

					isValid = (PEAK_FLOW_VALUE == CALC_PEAK_FLOW_VALUE);

					if ( !isValid )
					{
						Setting*  pTidalVolume =
							SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

						// during a VC+ New-Patient Setup, there's no way for me to base
						// VC+'s internal peak flow value on inspiratory time, while also
						// basing inspiratory time on peak flow's IBW-based value, therefore
						// test that inspiratory time is based on peak flow, via tidal
						// volume...
						isValid = pTidalVolume->isAcceptedValid();

						if ( !isValid )
						{
							const Setting*  pFlowPattern =
								SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
							const Setting*  pMinInspFlow =
								SettingsMgr::GetSettingPtr(SettingId::MIN_INSP_FLOW);

							cout << "\nINVALID Peak Insp Flow:\n";
							cout << "CALC = " << CALC_PEAK_FLOW_VALUE << endl;
							cout << *this << *pFlowPattern << *pMinInspFlow << *pInspTime
								<< *pTidalVolume << endl;
						}
					}
				}
				break;

			case Applicability::INAPPLICABLE :
			default :
				// do nothing...
				break;
		}
	}

	return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?  Return "True" if the setting is valid, returns "False"
//  if the setting is not valid.
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	PeakInspFlowSetting::isAdjustedValid(void)
{
	CALL_TRACE("isAdjustedValid()");

	Boolean  isValid = BoundedSetting::isAdjustedValid();

	if ( isValid )
	{
		const Applicability::Id  APPLIC_ID =
			getApplicability(Notification::ADJUSTED);

		Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

		switch ( APPLIC_ID )
		{
			case Applicability::CHANGEABLE :
				// ensure that inspiratory time is properly set based on this setting...
				isValid = pInspTime->isAdjustedValid();
				break;

			case Applicability::VIEWABLE :
				{
					const Real32  PEAK_FLOW_VALUE = BoundedValue(getAdjustedValue()).value;
					const Real32  CALC_PEAK_FLOW_VALUE = calcBasedOnSetting_(
																			SettingId::TIDAL_VOLUME,
																			BoundedRange::WARP_NEAREST,
																			BASED_ON_ADJUSTED_VALUES).value;

					isValid = (PEAK_FLOW_VALUE == CALC_PEAK_FLOW_VALUE);

					if ( !isValid )
					{
						Setting*  pTidalVolume =
							SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

						// during a VC+ New-Patient Setup, there's no way for me to base
						// VC+'s internal peak flow value on inspiratory time, while also
						// basing inspiratory time on peak flow's IBW-based value, therefore
						// test that inspiratory time is based on peak flow, via tidal
						// volume...
						isValid = pTidalVolume->isAdjustedValid();

						if ( !isValid )
						{
							const Setting*  pFlowPattern =
								SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
							const Setting*  pMinInspFlow =
								SettingsMgr::GetSettingPtr(SettingId::MIN_INSP_FLOW);

							cout << "\nINVALID Peak Insp Flow:\n";
							cout << "CALC = " << CALC_PEAK_FLOW_VALUE << endl;
							cout << *this << *pFlowPattern << *pMinInspFlow << *pInspTime
								<< *pTidalVolume << endl;
						}
					}
				}
				break;

			case Applicability::INAPPLICABLE :
			default :
				// do nothing...
				break;
		}
	}

	return(isValid);
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	PeakInspFlowSetting::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							PEAK_INSP_FLOW_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02222] -- this setting's dependent settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	PeakInspFlowSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
	CALL_TRACE("calcDependentValues_()");

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

	const DiscreteValue  MODE_VALUE = pMode->getAdjustedValue();

	Uint  idx = 0u;

	switch ( MODE_VALUE )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :
		case ModeValue::BILEVEL_MODE_VALUE :	  // $[TI1]
			// all dependents are active here...
			ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
			break;
		case ModeValue::SPONT_MODE_VALUE :		  // $[TI2]
		case ModeValue::CPAP_MODE_VALUE :
			// only inspiratory time in 'SPONT' mode...
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
			break;
	}

	// terminate with the null id...
	ArrDependentSettingIds_[idx] = SettingId::NULL_SETTING_ID;

	// forward on to base class...
	return(Setting::calcDependentValues_(isValueIncreasing));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	PeakInspFlowSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

	BoundedRange::ConstraintInfo  maxConstraintInfo;
	BoundedRange::ConstraintInfo  minConstraintInfo;

	if ( MAND_TYPE_VALUE == MandTypeValue::VCV_MAND_TYPE )
	{  // $[TI1] -- use VC bound ids...
		maxConstraintInfo.id = ::PEAK_INSP_FLOW_MAX_BASED_CCT_TYPE_ID;
		minConstraintInfo.id = ::PEAK_INSP_FLOW_MIN_BASED_CCT_TYPE_ID;
	}
	else
	{  // $[TI2] -- use VC+ bound ids...
		maxConstraintInfo.id = ::PEAK_INSP_FLOW_MAX_BASED_VCP_ID;
		minConstraintInfo.id = ::PEAK_INSP_FLOW_MIN_BASED_VCP_ID;
	}

	//-------------------------------------------------------------------
	// update maximum constraint...
	//-------------------------------------------------------------------

	maxConstraintInfo.value  = getAbsoluteMaxValue_();
	maxConstraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);

	//-------------------------------------------------------------------
	// update minimum constraint...
	//-------------------------------------------------------------------

	minConstraintInfo.value  = getAbsoluteMinValue_();
	minConstraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMinConstraint(minConstraintInfo);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All new-patient calculations are to be based on the VCV setting values.
//
// $[02019] -- dependent calculations
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnSettingId == SettingId::EXP_TIME  ||
//   basedOnSettingId == SettingId::IE_RATIO  ||
//   basedOnSettingId == SettingId::INSP_TIME)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
	PeakInspFlowSetting::calcBasedOnSetting_(
											const SettingId::SettingIdType basedOnSettingId,
											const BoundedRange::WarpDir    warpDirection,
											const BasedOnCategory          basedOnCategory
											) const
{
	CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

	SettingId::SettingIdType  finalBasedOnSettingId;

	// get the absolute minimum peak inspiratory flow value...
	const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

	if ( basedOnSettingId == SettingId::RESP_RATE  ||
		 basedOnSettingId == SettingId::TIDAL_VOLUME )
	{	// $[TI6] -- convert to timing constant id...
		const DiscreteValue  CONSTANT_PARM_VALUE =
			ConstantParmSetting::GetValue(basedOnCategory);

		switch ( CONSTANT_PARM_VALUE )
		{
			case ConstantParmValue::EXP_TIME_CONSTANT :	   // $[TI6.1]
				finalBasedOnSettingId = SettingId::EXP_TIME;
				break;
			case ConstantParmValue::IE_RATIO_CONSTANT :	   // $[TI6.2]
				finalBasedOnSettingId = SettingId::IE_RATIO;
				break;
			case ConstantParmValue::INSP_TIME_CONSTANT :   // $[TI6.3]
				finalBasedOnSettingId = SettingId::INSP_TIME;
				break;
			case ConstantParmValue::TOTAL_CONSTANT_PARMS :
			default :
				// invalid value for 'CONSTANT_PARM_VALUE'...
				AUX_CLASS_ASSERTION_FAILURE(CONSTANT_PARM_VALUE);
				break;
		};
	}
	else
	{	// $[TI7]
		// use passed in parameter...
		finalBasedOnSettingId = basedOnSettingId;
	}

	BoundedValue  inspTime;

	// determine a value for 'inspTime', based on which timing parameter
	// forcing this call...
	switch ( finalBasedOnSettingId )
	{
		case SettingId::EXP_TIME :
			{	// $[TI1] -- calculate an insp time from exp time...
				const Setting*  pExpTime =
					SettingsMgr::GetSettingPtr(SettingId::EXP_TIME);

				const Real32  EXP_TIME_VALUE =
					BoundedValue(pExpTime->getAdjustedValue()).value;
				const Real32  BREATH_PERIOD_VALUE =
					RespRateSetting::GetBreathPeriod(basedOnCategory);

				inspTime.value = (BREATH_PERIOD_VALUE - EXP_TIME_VALUE);
			}
			break;
		case SettingId::IE_RATIO :
			{	// $[TI2] -- calculate an insp time from I:E ratio...
				const Setting*  pIeRatio =
					SettingsMgr::GetSettingPtr(SettingId::IE_RATIO);

				const Real32  IE_RATIO_VALUE =
					BoundedValue(pIeRatio->getAdjustedValue()).value;
				const Real32  BREATH_PERIOD_VALUE =
					RespRateSetting::GetBreathPeriod(basedOnCategory);

				Real32  eRatioValue;
				Real32  iRatioValue;

				if ( IE_RATIO_VALUE < 0.0f )
				{	// $[TI2.1]
					// The I:E ratio value is negative, therefore we have 1:xx with
					// "xx" the expiratory portion of the ratio...
					iRatioValue = 1.0f;
					eRatioValue = -(IE_RATIO_VALUE);
				}
				else
				{	// $[TI2.2]
					// The I:E ratio value is positive, therefore we have xx:1 with
					// "xx" the inspiratory portion of the ratio...
					iRatioValue = IE_RATIO_VALUE;
					eRatioValue = 1.0f;
				}

				// calculate the insp time from the I:E ratio...
				inspTime.value = (iRatioValue * BREATH_PERIOD_VALUE) /
								 (iRatioValue + eRatioValue);

				BoundedSetting*  pInspTime =
					SettingsMgr::GetBoundedSettingPtr(SettingId::INSP_TIME);

				// because inspiratory time has a "coarse" resolution in VCV,
				// the "raw" insp time value must be rounded to a resolution boundary,
				// before using the value in the calculations, below...

				// the warping of inspiratory time must be in the opposite direction

				BoundedRange::WarpDir actualWarpDirection;

				switch ( warpDirection )
				{
					case BoundedRange::WARP_NEAREST :	  // $[TI2.3]
						actualWarpDirection = BoundedRange::WARP_NEAREST;
						break;
					case BoundedRange::WARP_UP :	  // $[TI2.4]
						actualWarpDirection = BoundedRange::WARP_DOWN;
						break;
					case BoundedRange::WARP_DOWN :		  // $[TI2.5]
						actualWarpDirection = BoundedRange::WARP_UP;
						break;
					default:
						AUX_CLASS_ASSERTION_FAILURE(warpDirection);
						break;
				}

				pInspTime->warpToRange(inspTime, actualWarpDirection, FALSE);
			}
			break;
		case SettingId::INSP_TIME :
			{	// $[TI3] -- get the "adjusted" insp time value...
				const Setting*  pInspTime =
					SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

				inspTime.value = BoundedValue(pInspTime->getAdjustedValue()).value;
			}
			break;
		default :
			// unexpected 'finalBasedOnSettingId' value...
			AUX_CLASS_ASSERTION_FAILURE(finalBasedOnSettingId);
			break;
	};

	// get pointers to each of the VCV parameters...
	const Setting*  pFlowPattern =
		SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
	const Setting*  pPlateauTime =
		SettingsMgr::GetSettingPtr(SettingId::PLATEAU_TIME);
	const Setting*  pTidalVolume =
		SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

	const DiscreteValue  FLOW_PATTERN_VALUE = pFlowPattern->getAdjustedValue();

	const Real32  PLATEAU_TIME_VALUE =
		BoundedValue(pPlateauTime->getAdjustedValue()).value;
	const Real32  TIDAL_VOLUME_VALUE =
		BoundedValue(pTidalVolume->getAdjustedValue()).value;

	BoundedValue  peakInspFlow;

	switch ( FLOW_PATTERN_VALUE )
	{
		case FlowPatternValue::SQUARE_FLOW_PATTERN :  // $[TI4] 
			{
				static const Real32  FACTOR_ = (60000.0f * 0.001f);

				peakInspFlow.value = ((FACTOR_ * TIDAL_VOLUME_VALUE) /
									  (inspTime.value - PLATEAU_TIME_VALUE));

				// Enforce lower bound...[NE02000] \b\ i
				peakInspFlow.value = MAX_VALUE( ABSOLUTE_MIN, peakInspFlow.value );
			}
			break;

		case FlowPatternValue::RAMP_FLOW_PATTERN :	  // $[TI5]
			{
				static const Real32  FACTOR_ = (60000.0f * 0.001f * 2.0f);

				const Real32  PRELIM_VALUE = ((FACTOR_ * TIDAL_VOLUME_VALUE) / 
											  (inspTime.value - PLATEAU_TIME_VALUE));

				// The formula above is incomplete, the full formula would include
				// the subtraction of minimum inspiratory flow from the entire
				// argument.  Minimum inspiratory flow is purely based on value of
				// peak inspiratory flow -- we have a "chicken and egg" problem.
				// Therefore, to solve this problem peak inspiratory flow is
				// calculated WITHOUT minimum inspiratory flow (result held in
				// 'PRELIM_VALUE'), and the minimum inspiratory flow is determined by
				// using the minimum of the two peak inspiratory flow values that
				// would result from the two possible minimum inspiratory flow
				// values.
				// (NOTE:  minInspFlow = MAX(0.8, (peakInspFlow * 0.10)))...
				peakInspFlow.value = MIN_VALUE((PRELIM_VALUE - 0.8f), // $[TI5.1]
											   (PRELIM_VALUE / 1.1f));	  // $[TI5.2]

                // Enforce lower bound...[NE02000]
				peakInspFlow.value = MAX_VALUE( ABSOLUTE_MIN, peakInspFlow.value );
			}
			break;

		default :
			AUX_CLASS_ASSERTION_FAILURE(FLOW_PATTERN_VALUE);
			break;
	};

	// place peak inspiratory flow on a "click" boundary...
	getBoundedRange().warpValue(peakInspFlow, warpDirection, FALSE);

	return(peakInspFlow);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic minimum constraint.
//
//  $[02218] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	PeakInspFlowSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

	switch ( MAND_TYPE_VALUE )
	{
		case MandTypeValue::PCV_MAND_TYPE :
		case MandTypeValue::VCV_MAND_TYPE :			  // $[TI1]
			{
				const Setting*  pPatientCctType =
					SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

				const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

				switch ( PATIENT_CCT_TYPE_VALUE )
				{
					case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI1.1]
						absoluteMinValue = SettingConstants::MIN_PEAK_FLOW_NEO_VALUE;
						break;
					case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
					case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI1.2]
						absoluteMinValue = SettingConstants::MIN_PEAK_FLOW_PED_ADULT_VALUE;
						break;
					default :
						// unexpected patient circuit type value...
						AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
						break;
				}
			}
			break;
		case MandTypeValue::VCP_MAND_TYPE :			  // $[TI2]
			{
				// SCR 6549: Wrong Vmax used in VC+ New Patient Settings
				const Setting*  pPatientCctType =
					SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

				const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

				switch ( PATIENT_CCT_TYPE_VALUE )
				{
					case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI1.1]
						absoluteMinValue = SettingConstants::MIN_PEAK_FLOW_NEO_VALUE;
						break;
					case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
					case PatientCctTypeValue::ADULT_CIRCUIT :
						// set to min allowable value...
						absoluteMinValue = ::LAST_NODE_.value;
						break;
					default :
						// unexpected patient circuit type value...
						AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
						break;
				}
			}
			break;
		default :
			// unexpected mandatory type value...
			AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
			break;
	};

	return(absoluteMinValue);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute maximum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic maximum constraint.
//
//  $[02218] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	PeakInspFlowSetting::getAbsoluteMaxValue_(void) const
{
	CALL_TRACE("getAbsoluteMaxValue_()");

	Real32  absoluteMaxValue;

	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

	switch ( MAND_TYPE_VALUE )
	{
		case MandTypeValue::PCV_MAND_TYPE :
		case MandTypeValue::VCV_MAND_TYPE :			  // $[TI1]
			{
				const Setting*  pPatientCctType =
					SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

				const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

				switch ( PATIENT_CCT_TYPE_VALUE )
				{
					case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI1.1]
						absoluteMaxValue = SettingConstants::MAX_PEAK_FLOW_NEO_VALUE;
						break;
					case PatientCctTypeValue::PEDIATRIC_CIRCUIT : // $[TI1.2]
						absoluteMaxValue = SettingConstants::MAX_PEAK_FLOW_PED_VALUE;
						break;
					case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI1.3]
						absoluteMaxValue = SettingConstants::MAX_PEAK_FLOW_ADULT_VALUE;
						break;
					default :
						// unexpected patient circuit type value...
						AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
						break;
				}
			}
			break;
		case MandTypeValue::VCP_MAND_TYPE :			  // $[TI2]
			// set to max allowable value...
			absoluteMaxValue = ::INTERVAL_LIST_.value;
			break;
		default :
			// unexpected mandatory type value...
			AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
			break;
	};

	return(absoluteMaxValue);
}
