#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BatchSettingsSubScreen - Base class for sub-screens that
//                                  require batch settings change behavior.
//---------------------------------------------------------------------
//@ Interface-Description
// Any object that receives AdjustPanel events must have this class as
// one of its parent classes.  These events include pressing the Accept 
// and Clear keys and any events (such as a button press) which result
// in the batch settings subscreen losing or regaining the focus.
//
// Although multiple inheritance is not normally allowed, adding this
// class as an additional parent class is acceptable because its only
// use is to implement a callback interface to another class.
//
// This class provides subscreen functionality via virtual methods
// derived from the GUI-Foundation class AdjustPanelTarget.
// 
// Upon deactivating a BatchSettingSubscreen, deactivate(), this class
// activates the timeoutSubscreen. This is a non-drawable subscreen
// that acts as a target for the SUBSCREEN_SETTING_CHANGE_TIMEOUT. In 
// this way, timeouts are handled correctly when no other
// BatchSettingsSubscreen is activated.
//---------------------------------------------------------------------
//@ Rationale
// This class contains methods which define the callback functionality 
// for the framework provided by the abstract parent class 
// AdjustPanelTarget to handle user events germaine to batch setting 
// subscreens.
//---------------------------------------------------------------------
//@ Implementation-Description
// This class implements methods which handle the user pressing the
// ACCEPT key OR the CLEAR key and any events which result in the
// subscreen losing or regaining the focus.
//---------------------------------------------------------------------
//@ Fault-Handling
// Some use of asserts to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BatchSettingsSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  ksg	   Date:  20-Jun-2007    SCR Number: 6237
//  Project:  TREND
//  Description: Added support to handle settings change timeouts while 
//  no BatchSettingsSubscreen derivative is active.
// 
//  Revision: 004  By: sah     Date:  23-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added new 'setTitleChangeState_()' method for standardized
//         handling of run-time formatting of discrete setting values
//
//  Revision: 003   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 002  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 001  By:  hct	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//=====================================================================

#include "BatchSettingsSubScreen.hh"

//@ Usage-Classes
#include "MiscStrs.hh"
#include "PromptArea.hh"
#include "Sound.hh"
#include "ContextSubject.hh"
#include "GuiTimerRegistrar.hh"
#include "GuiApp.hh"
#include "TimeoutSubScreen.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BatchSettingsSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructs a BatchSettingsSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
// The subscreen area pointer must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BatchSettingsSubScreen::BatchSettingsSubScreen(SubScreenArea *pSubScreenArea)
	: SubScreen(pSubScreenArea),
	currentSettingsFlag_(TRUE)
{
	CALL_TRACE("BatchSettingsSubScreen(pSubScreenArea)");
	CLASS_PRE_CONDITION(pSubScreenArea != NULL);


}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BatchSettingsSubScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BatchSettingsSubScreen::~BatchSettingsSubScreen(void)	
{
	CALL_TRACE("~BatchSettingsSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called by LowerSubScreenArea just before this subscreen is displayed
// allowing us to do any necessary display setup.  Takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void BatchSettingsSubScreen::activate(void)
{
	CALL_TRACE("activate()");

	// Attach to the Context Subject
	attachToSubject_(ContextId::ADJUSTED_CONTEXT_ID,
					 Notification::BATCH_CHANGED);

	// Start a timer to timeout subscreen setting changes.
	GuiTimerRegistrar::StartTimer(GuiTimerId::SUBSCREEN_SETTING_CHANGE_TIMEOUT, this);

	// Make sure this subscreen has the default adjust panel focus
	AdjustPanel::TakeNonPersistentFocus(this, TRUE, FALSE);

	setCurrentSettingsFlag_(TRUE);

	// forward on to derived classes...
	activateHappened_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by LowerSubScreenArea just after this subscreen is removed from the
// display allowing us to do any necessary cleanup.  Takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// We release the Adjust Panel focus, clear all prompts that we may have
// displayed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void BatchSettingsSubScreen::deactivate(void)
{
	CALL_TRACE("deactivate()");

	// get reference to timeoutSubscreen
	TimeoutSubScreen& rTimeoutSubScreen_ = TimeoutSubScreen::GetTimeoutSubScreen();

	// Detach itself from the Context Subject
	detachFromSubject_(ContextId::ADJUSTED_CONTEXT_ID,
					   Notification::BATCH_CHANGED);

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary, Advisory and Secondary prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
								   NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
								   NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_LOW,
								   NULL_STRING_ID);

	// Reactivate the timeout subscreen. This will handle screen timeouts when
	// no other BatchSettingsSubscreen is activated.
	rTimeoutSubScreen_.activate();
	// forward on to derived classes...
	deactivateHappened_();
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// Called when the Accept key is pressed down (and we're the current
// Adjust Panel focus).  We pass the event on to the focus subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply call the acceptHappened() method of 'pFocusSubScreen_' and
// let it do its stuff.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void BatchSettingsSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("adjustPanelAcceptPressHappened(void)");

	// Pass on accept event to "focus" sub-screen
	acceptHappened();                   
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
// Called when the Clear key is pressed (and we're the current
// Adjust Panel focus).  This is an invalid choice so make the Invalid
// Entry sound.
//---------------------------------------------------------------------
//@ Implementation-Description
// Tell the sound system to make the Invalid Entry sound.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void BatchSettingsSubScreen::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("adjustPanelClearPressHappened(void)");

    // Wrong choice
	Sound::Start(Sound::INVALID_ENTRY);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelLoseFocusHappened
//
//@ Interface-Description
// Called when this button is about to lose the focus of the Adjust
// Panel (e.g. when the operator presses down a setting button which
// grabs the Adjust Panel focus for itself).
//---------------------------------------------------------------------
//@ Implementation-Description
// When we lose the focus we simply remov the Primary "Press Accept if OK." 
// prompt.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void BatchSettingsSubScreen::adjustPanelLoseFocusHappened(void)
{
	CALL_TRACE("adjustPanelLoseFocusHappened(void)");

    // Clear prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
								   NULL_STRING_ID);    
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened()
//
//@ Interface-Description
// This method accepts no parameters and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// We simply grab the Adjust Panel focus and tell the operator to
// "Press Accept if OK." if any batch settings have changed.  If no
// batch settings have changed, clear the primary prompt area.
//---------------------------------------------------------------------
//@ PreCondition
// Can only be pressed down by operator.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void BatchSettingsSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("adjustPanelRestoreFocusHappened()");

	if (!areSettingsCurrent_())
	{													// $[TI1.1]
		// Set Primary "Press ACCEPT if OK." prompt.
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_HIGH, PromptStrs::PRESS_ACCEPT_IF_OK_P);
	}
	else
	{													// $[TI1.2]
		// Clear Primary and Advisory prompts.
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY,
									   PromptArea::PA_HIGH, NULL_STRING_ID);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  batchSettingUpdate
//
//@ Interface-Description
//	Redefined from ContextObserver base class.  This method processes
//  value changes in the identified context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Prepare for the value update which is about to happen in the derived
//  class.  Invoke valueUpdate() method in the derived class.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void BatchSettingsSubScreen::batchSettingUpdate(const Notification::ChangeQualifier qualifierId,
												const ContextSubject*               pSubject,
												const SettingId::SettingIdType      settingId)
{
	CALL_TRACE("batchSettingUpdate(qualifierId, pSubject)");

	// If we're invisible, do nothing
	if (!isVisible())
	{
		return;
	}

	TransitionId_  transitionId = NO_TRANSITION;

	if (currentSettingsFlag_)
	{  // $[TI3] -- now in the "current" state...
		if (pSubject->areAnyBatchSettingsChanged())
		{  // $[TI3.1] -- transitioning to the "proposed" state...
			// no longer in the "current" state...
			currentSettingsFlag_ = FALSE;
			transitionId         = CURRENT_TO_PROPOSED;
		}  // $[TI3.2] -- remaining in the "current" state...
	}
	else
	{  // $[TI4] -- now in the "proposed" state...
		if (!pSubject->areAnyBatchSettingsChanged())
		{  // $[TI4.1] -- transitioning back to the "current" state...
			// no longer in the "proposed" state...
			currentSettingsFlag_ = TRUE;
			transitionId         = PROPOSED_TO_CURRENT;
		}  // $[TI4.2] -- remaining in the "proposed" state...
	}

	// forward on to derived classes...
	valueUpdateHappened_(transitionId, qualifierId, pSubject);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void BatchSettingsSubScreen::SoftFault(const SoftFaultID  softFaultID,
									  const Uint32       lineNumber,
									  const char*        pFileName,
									  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, BATCHSETTINGSSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setTitleChangeState_
//
//@ Interface-Description
//	Set the state of the passed-in text field, based on the change boolean
//  and the title format string.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void BatchSettingsSubScreen::setTitleChangeState_(const StringId titleText,
												 const Boolean  isChanged,
												 TextField&     titleField)
{
	CALL_TRACE("setTitleChangeState_(isChanged, titleField, titleText)");

	 
	wchar_t  tmpString[TextField::MAX_STRING_LENGTH+1];

	swprintf(tmpString, titleText, 
			((isChanged) ? L'I'		//  -- set to italics...
			 : L'N'));	//  -- set to non-italics...

	titleField.setText(tmpString);
	titleField.setHighlighted(isChanged);
}
