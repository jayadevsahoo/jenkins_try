#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BreathTriggerMediator - Keeps applicable lists of breath triggers.
//---------------------------------------------------------------------
//@ Interface-Description
//    This mediator keeps track of which breath triggers are currently 
//    active, by manipulating predefined lists of triggers.  The list of 
//    active triggers is evaluated every cycle.  
//    Other objects may change the currently active breath list to any
//    list contained in the mediator, or disable all triggers that are 
//    on the currently active list. Manipulation of each trigger's enabled 
//    state, by other objects, may also be handled directly with each 
//    trigger, bypassing the mediator.  All triggers in the currently 
//    active list must be disabled before the list is replaced as the active list.  
//    If a trigger is an alarm trigger, then an alarm will be posted to 
//    the Alarm-Analysis subystem within BdAlarms.
//---------------------------------------------------------------------
//@ Rationale
//    It is necessary to keep lists of all triggers that could possibly be
//    applicable during each phase of a breath (inspiration, exhalation,
//    expiratory pause, inspiratory pause and non-breathing).  During these
//    phases, only some of the triggers are active, depending upon the type of
//    the phase currently active (eg. volume insp., spontaneous insp., flow
//    based exhalation ...).  The mediator provides a convenient place to keep
//    the lists of triggers, and also provides the methods required to
//    manipulate the lists.
//---------------------------------------------------------------------
//@ Implementation-Description
//    There are 4 arrays of pointers to BreathTriggers, one for each 
//    phase type.  Only one of these lists is ever active at any time.  
//    The triggers on the active list are interrogated every cycle, to 
//    see if any has "fired".  The order within the list is unimportant.
//    Each list is terminated with a NULL because of the variation in the
//    size of the list for each breath phase.
//    When one of the BreathTriggers on the active list fires, the active 
//    BreathPhase may be changed, also requiring the list of active triggers 
//    to be changed accordingly.  Since this change will be initiated while 
//    the list of triggers is in use, the list cannot be changed
//    immediately.  The private data member nextList_, must be set to 
//    the value of the parameter newList.  At the end of this cycle, 
//    when the list is no longer in use, the mediator will
//    update the active list pointer to the value of nextList_.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class is to be allowed.  When a new list 
//    is "activated", the states of the triggers on the list must not be 
//    changed, as they may have been set to the appropriate state before 
//    the list is activated.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathTriggerMediator.ccv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019   By: rhj   Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes:
//       Added RNetFlowBackupInspTrigger.
// 
//  Revision: 018   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		added RM phase triggers.
//
//  Revision: 017   By: syw   Date:  19-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added pav pause triggers handle.
//
//  Revision: 016  By: syw     Date:  15-Mar-1999    DR Number: 5367
//  Project:  ATC
//  Description:
//      Added new trigger RPausePressTrigger
//
//  Revision: 015  By: syw     Date:  15-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added new triggers RLungFlowExpTrigger, RLungVolumeExpTrigger,
//      RHighPressCompExpTrigger
//
//  Revision: 014  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//		ATC initial release
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 013  By:  iv    Date:  31-Jan-1998    DR Number: None
//      Project:  Sigma (R8027)
//      Description:
//          Bilevel initial version
//	        Added RPeepReductionExpTrigger, RBiLevelHighToLowExpTrigger,
//          RBiLevelLowToHighInspTrigger, RPausePressExpTrigger.
//
//  Revision: 012  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 011  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 010  By:  sp    Date: 1-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 009  By:  sp    Date:  9-Jan-1997    DR Number: DCS 1339
//       Project:  Sigma (R8027)
//       Description:
//             Add one more trigger to expiratory pause list.
//
//  Revision: 008  By:  sp    Date:  31-Oct-1996    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 007  By:  sp    Date:  26-Sep-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed PeepRecoveryTrigger to PeepRecoveryInspTrigger.
//             Removed TUV interface.
//
//  Revision: 006  By:  iv    Date:  07-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Added a PeepRecoveryTrigger to pExpList_.
//
//  Revision: 005  By:  iv    Date:  20-Jul-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added an immediate breath trigger to the pause list.
//
//  Revision: 004  By:  sp    Date:  26-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Add TUV interface (need to be uncommented later).
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem.
//             Notify rBdAlarm object when trigger occur in method checkActiveTrigger_.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "BreathTriggerMediator.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes
#include "BreathTrigger.hh"
#include "TriggersRefs.hh"
#include "BdAlarms.hh"
//@ End-Usage

//@ Code...


#ifdef SIGMA_UNIT_TEST
#include "Ostream.hh"
#endif //SIGMA_UNIT_TEST

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathTriggerMediator 
//
//@ Interface-Description
//		Constructor.
// All private data members are initialized here.
//---------------------------------------------------------------------
//@ Implementation-Description
// A change in the number of triggers in a list requires a change to the 
// constant that specifies the number of triggers in that list. 
// Note that the instance rOperatorInspTrigger is on all of the
// lists since this trigger implements a handshaking protocol with
// the manual inspiration user event (which is an asynchronous event) and
// has to be available at any given time that the manual inspiration key can
// be pressed.
// Each element in the array is initialized to point to a specific instance
// of a BreathTrigger (a derived class). The pointer to the active
// trigger list is initialized to NULL.
//---------------------------------------------------------------------
//@ PreCondition
// size of pExpList_ == NUM_EXP_TRIGGERS
// size of pInspList_ == NUM_INSP_TRIGGERS
// size of pExpPauseList_ == NUM_EXP_PAUSE_TRIGGERS
// size of pInspPauseList_ == NUM_INSP_PAUSE_TRIGGERS
// size of pPavInspPauseList_ == NUM_PAV_INSP_PAUSE_TRIGGERS
// size of pNonBreathingList_ == NUM_NON_BREATHING_TRIGGERS 
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
BreathTriggerMediator::BreathTriggerMediator()
{
	CALL_TRACE("BreathTriggerMediator()");

	// $[TI1]
	Int32 ii = 0;
	
	// initialization
	pExpList_[ii++] = (BreathTrigger*)&RPeepReductionExpTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RAsapInspTrigger;
	// To handle exhalation to exhalation when peep is high for bilevel.
	pExpList_[ii++] = (BreathTrigger*)&RBiLevelHighToLowExpTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RBiLevelLowToHighInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RPeepRecoveryInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RPeepRecoveryMandInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RNetFlowInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RPressureBackupInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RPressureInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RNetFlowBackupInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RSimvTimeInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RTimeInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&ROperatorInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RImmediateBreathTrigger;
	pExpList_[ii++] = (BreathTrigger*)&ROscTimeInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&ROscTimeBackupInspTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RArmManeuverTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RDisarmManeuverTrigger;
	pExpList_[ii++] = (BreathTrigger*)&RP100Trigger;
    // added to handle high pressures during bilevel exhalations
	pExpList_[ii++] = (BreathTrigger*)&RHighCircuitPressureExpTrigger;
	pExpList_[ii++] = NULL;

	CLASS_PRE_CONDITION(ii == NUM_EXP_TRIGGERS);

	ii = 0;
	pInspList_[ii++] = (BreathTrigger*)&RDeliveredFlowExpTrigger;
	pInspList_[ii++] = (BreathTrigger*)&RPressureExpTrigger;
	pInspList_[ii++] = (BreathTrigger*)&RLungFlowExpTrigger;
	pInspList_[ii++] = (BreathTrigger*)&RLungVolumeExpTrigger;
	pInspList_[ii++] = (BreathTrigger*)&RHighPressCompExpTrigger;
	pInspList_[ii++] = (BreathTrigger*)&RImmediateBreathTrigger;
	pInspList_[ii++] = (BreathTrigger*)&RImmediateExpTrigger;
	pInspList_[ii++] = (BreathTrigger*)&RBackupTimeExpTrigger;
	pInspList_[ii++] = (BreathTrigger*)&RHighCircuitPressureExpTrigger;
	pInspList_[ii++] = (BreathTrigger*)&RHighVentPressureExpTrigger;
	pInspList_[ii++] = (BreathTrigger*)&ROperatorInspTrigger;
	pInspList_[ii++] = NULL;

	CLASS_PRE_CONDITION(ii == NUM_INSP_TRIGGERS);

	// $[RM12062] The triggers used during a VCM inspiratory phase...
	ii = 0;
	pVcmInspList_[ii++] = (BreathTrigger*)&RDeliveredFlowExpTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RPressureExpTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RLungFlowExpTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RLungVolumeExpTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RHighPressCompExpTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RImmediateBreathTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RImmediateExpTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RBackupTimeExpTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RHighCircuitPressureExpTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RHighVentPressureExpTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&ROperatorInspTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RPauseTimeoutTrigger;
	pVcmInspList_[ii++] = (BreathTrigger*)&RPauseCompletionTrigger;
	pVcmInspList_[ii++] = NULL;

	CLASS_PRE_CONDITION(ii == NUM_VCM_INSP_TRIGGERS);

	// $[04220] 
	ii = 0;
	pExpPauseList_[ii++] = (BreathTrigger*)&RImmediateBreathTrigger;
	pExpPauseList_[ii++] = (BreathTrigger*)&RPauseTimeoutTrigger;
	pExpPauseList_[ii++] = (BreathTrigger*)&ROperatorInspTrigger;
	pExpPauseList_[ii++] = (BreathTrigger*)&RPauseCompletionTrigger; // user event
	pExpPauseList_[ii++] = (BreathTrigger*)&RPausePressTrigger;  // patient effort
	pExpPauseList_[ii++] = (BreathTrigger*)&RHighCircuitPressureInspTrigger;
	pExpPauseList_[ii++] = NULL;

	CLASS_PRE_CONDITION(ii == NUM_EXP_PAUSE_TRIGGERS);

	// $[BL04075]  
	ii = 0;
	pInspPauseList_[ii++] = (BreathTrigger*)&RImmediateBreathTrigger;
	pInspPauseList_[ii++] = (BreathTrigger*)&RPauseTimeoutTrigger;
	pInspPauseList_[ii++] = (BreathTrigger*)&ROperatorInspTrigger;
	pInspPauseList_[ii++] = (BreathTrigger*)&RPauseCompletionTrigger; // user event
	pInspPauseList_[ii++] = (BreathTrigger*)&RPausePressTrigger;
	pInspPauseList_[ii++] = (BreathTrigger*)&RHighCircuitPressureExpTrigger;
	pInspPauseList_[ii++] = NULL;

	CLASS_PRE_CONDITION(ii == NUM_INSP_PAUSE_TRIGGERS);

	ii = 0;
	pPavInspPauseList_[ii++] = (BreathTrigger*)&RImmediateBreathTrigger;
	pPavInspPauseList_[ii++] = (BreathTrigger*)&RPauseCompletionTrigger;
	pPavInspPauseList_[ii++] = (BreathTrigger*)&RHighCircuitPressureExpTrigger;
	pPavInspPauseList_[ii++] = NULL;

	CLASS_PRE_CONDITION(ii == NUM_PAV_INSP_PAUSE_TRIGGERS);

	// $[RM12077] The triggers used during a NIF...
	// $[RM12094] The triggers used during a P100...
	ii = 0;
	pNifPauseList_[ii++] = (BreathTrigger*)&RImmediateBreathTrigger;
	pNifPauseList_[ii++] = (BreathTrigger*)&RPauseTimeoutTrigger;
	pNifPauseList_[ii++] = (BreathTrigger*)&ROperatorInspTrigger;
	pNifPauseList_[ii++] = (BreathTrigger*)&RPauseCompletionTrigger; 
	pNifPauseList_[ii++] = (BreathTrigger*)&RP100CompletionTrigger;
	pNifPauseList_[ii++] = (BreathTrigger*)&RHighCircuitPressureInspTrigger;
	pNifPauseList_[ii++] = NULL;

	CLASS_PRE_CONDITION(ii == NUM_NIF_PAUSE_TRIGGERS);

	ii = 0;
	pNonBreathingList_[ii++] = (BreathTrigger*)&RPressureSvcTrigger;  //start SVC phase
	pNonBreathingList_[ii++] = (BreathTrigger*)&RSvcTimeTrigger;  //start SVC phase
	pNonBreathingList_[ii++] = (BreathTrigger*)&RSvcCompleteTrigger;  //Settling time trigger for SVO state
	pNonBreathingList_[ii++] = (BreathTrigger*)&RImmediateBreathTrigger;
	pNonBreathingList_[ii++] = (BreathTrigger*)&ROperatorInspTrigger;
	pNonBreathingList_[ii++] = (BreathTrigger*)&ROscTimeInspTrigger;
	pNonBreathingList_[ii++] = NULL;

	CLASS_PRE_CONDITION(ii == NUM_NON_BREATHING_TRIGGERS);

	//force initialization of active list pointer.
	pActiveBreathList_ = pNonBreathingList_;
	nextList_ = BreathTriggerMediator::NULL_LIST;  

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BreathTriggerMediator 
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
BreathTriggerMediator::~BreathTriggerMediator()
{
   CALL_TRACE("~BreathTriggerMediator()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//     This method takes no parameters, and returns no values.
//     To be called every BD cycle to evaluate all triggers on the list.  
//---------------------------------------------------------------------
//@ Implementation-Description
//    The method checkActiveTriggers() is invoked to determine if any of 
//    the triggers have fired.  If they have, it may be necessary to 
//    change the currently active list to the next list desired.  This 
//    method checks the state of nextList_, and if it is not NULL_LIST, the 
//    method changeListPtr() is called to change the pointer to the 
//    currently active list, to the new desired list.
// $[04013] $[04292]
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
BreathTriggerMediator::newCycle(void)
{
    CALL_TRACE("newCycle()");

    Boolean isTrigger = checkActiveTriggers_(); 


    // if one of the triggers has fired causing a new BreathPhase 
    // to be active, then change the active trigger list accordingly.
    if (isTrigger)
    {
        CLASS_PRE_CONDITION(nextList_ != BreathTriggerMediator::NULL_LIST);
	// $[TI1]
        changeListPtr_();
    }
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetTriggerList
//
//@ Interface-Description
//    This method contains no parameters, and has no return value.  
//    It may be called by any object desiring to reset all triggers on 
//    the active trigger list to disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method uses a while loop to traverse the currently active list 
//    of BreathTriggers. Each trigger on the active list is disabled.
//---------------------------------------------------------------------
//@ PreCondition
//    pActiveBreathList_!=NULL
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
BreathTriggerMediator::resetTriggerList(void)
{
    CALL_TRACE("resetTriggerList()");

    CLASS_PRE_CONDITION(pActiveBreathList_!=NULL);

    Int16 i = 0;
    while (pActiveBreathList_[i] != NULL)
    {
       // $[TI1]
       pActiveBreathList_[i]->disable();
       i++;
    }
    // $[TI2] this test item will never hit because of the class pre-condition.
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
BreathTriggerMediator::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BREATHTRIGGERMEDIATOR,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: checkActiveTriggers_
//
//@ Interface-Description
//    This method is invoked every cycle by the method newCycle().  
//    It takes no parameters and return true if any of the triggers 
//    has fired else return false.  If a trigger has fired, this
//    method checks if the trigger corresponds to an alarm condition; if
//    so BdAlarms is notifed, which in turn notifies the Alarm-Analysis
//    subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//    $[04292]
//    The condition monitored by each trigger on the active list is 
//    evaluated, until one of the conditions returns true.
//    The triggers that are not enabled will not be evaluated by the method
//    determineState.
//    If an active trigger fires, rBdAlarms.alarmTriggerFilter() is invoked
//    to determine if the trigger corresponds to an alarm condition.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
BreathTriggerMediator::checkActiveTriggers_(void)
{
    CALL_TRACE("checkActiveTriggers_()");
 
    CLASS_PRE_CONDITION(pActiveBreathList_!=NULL);

    Int16 i = 0;
    Boolean terminate = FALSE;
    while (pActiveBreathList_[i] != NULL && !terminate)
    {
       // $[TI1]
       terminate = pActiveBreathList_[i]->determineState();

       if (terminate)
       {
           // $[TI3]
           // Determine if the trigger is also an alarm trigger; if so
           // an alarm will be posted to the Alarm-Analysis subystem
           // within BdAlarms
           RBdAlarms.alarmTriggerFilter (pActiveBreathList_[i]->getId());
       }
       // $[TI4]
       i++;
    }
    // $[TI2] this test item will never get call because of the class pre-condition.

    return (terminate);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: changeListPtr_
//
//@ Interface-Description
//    This is a private method to be called only when it is time to change 
//    the currently active list of BreathTriggers.  It accepts no 
//    parameters, and has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This ensures proper resetting of the triggers.  The new active list, as
//    indicated by the private data member nextList_, is set via a switch 
//    statement.  The triggers on the new list must not be deactivated, 
//    as they may already have been set to the appropriate state before 
//    switching lists.  After changing the list pointer, the nextList_ 
//    data member must be set to NULL_LIST, indicating that it is not necessary
//    to change the active trigger list.
//---------------------------------------------------------------------
//@ PreCondition
//     none
//---------------------------------------------------------------------
//@ PostCondition
//     none
//@ End-Method
//=====================================================================
void
BreathTriggerMediator::changeListPtr_(void)
{
    CALL_TRACE("changeListPtr_(void)");

    switch (nextList_)
    {
	case BreathTriggerMediator::EXP_LIST:
		// $[TI1] 
		pActiveBreathList_ = pExpList_;
		break; 
	case BreathTriggerMediator::INSP_LIST:
		// $[TI2] 
		pActiveBreathList_ = pInspList_;
		break; 
	case BreathTriggerMediator::VCM_INSP_LIST:
		pActiveBreathList_ = pVcmInspList_;
		break; 
	case BreathTriggerMediator::EXP_PAUSE_LIST:
		// $[TI3] 
		pActiveBreathList_ = pExpPauseList_;
		break; 
	case BreathTriggerMediator::INSP_PAUSE_LIST:
		// $[TI6] 
		pActiveBreathList_ = pInspPauseList_;
		break; 
	case BreathTriggerMediator::PAV_INSP_PAUSE_LIST:
		// $[TI7] 
		pActiveBreathList_ = pPavInspPauseList_;
		break; 
	case BreathTriggerMediator::NIF_PAUSE_LIST:
		pActiveBreathList_ = pNifPauseList_;
		break; 
	case BreathTriggerMediator::NON_BREATHING_LIST:
		// $[TI4] 
		pActiveBreathList_ = pNonBreathingList_;
		break; 
	default:
		AUX_CLASS_ASSERTION_FAILURE(nextList_);
		break;
	}

    nextList_ = BreathTriggerMediator::NULL_LIST;
}


#ifdef SIGMA_UNIT_TEST
void
BreathTriggerMediator::write(void)
{
    CALL_TRACE("write(void)");

    cout << "--- class BreathTriggerMediator ---\n";
    cout << "nextList_:" << nextList_ << endl;

    Int16 i = 0;

    while (pActiveBreathList_[i] != NULL)
    {
       ((Trigger*)pActiveBreathList_[i])->write();
       i++;
    }

    cout << "--- end of class BreathTriggerMediator ---\n";
 
}

void
BreathTriggerMediator::setActiveTriggerList(BreathListName newList)
{
    CALL_TRACE("setActiveTriggerList(BreathListName newList)");
    switch (newList)
    {
        case BreathTriggerMediator::EXP_LIST:
            pActiveBreathList_ = pExpList_;
            break; 
        case BreathTriggerMediator::INSP_LIST:
            pActiveBreathList_ = pInspList_;
            break; 
        case BreathTriggerMediator::EXP_PAUSE_LIST:
            pActiveBreathList_ = pExpPauseList_;
            break; 
        case BreathTriggerMediator::INSP_PAUSE_LIST:
            pActiveBreathList_ = pInspPauseList_;
            break; 
        case BreathTriggerMediator::PAV_INSP_PAUSE_LIST:
            pActiveBreathList_ = pPavInspPauseList_;
            break; 
        case BreathTriggerMediator::NON_BREATHING_LIST:
            pActiveBreathList_ = pNonBreathingList_;
            break; 
        case BreathTriggerMediator::NULL_LIST:
            pActiveBreathList_ = NULL;
            break; 
        default:
            CLASS_PRE_CONDITION( 
                (nextList_== BreathTriggerMediator::EXP_LIST) ||
                (nextList_ == BreathTriggerMediator::INSP_LIST) ||
                (nextList_== BreathTriggerMediator::EXP_PAUSE_LIST) ||
                (nextList_ == BreathTriggerMediator::NON_BREATHING_LIST) || 
                (nextList_== BreathTriggerMediator::NULL_LIST) );
    }
}

#endif // SIGMA_UNIT_TEST
