/************************************************************************/
/* THIS INFORMATION IS PROPRIETARY TO                                   */
/* MICROTEC - A Mentor Graphics Company                                 */
/*----------------------------------------------------------------------*/
/* Copyright (c) 1995-1997 Microtec - A Mentor Graphics Company         */
/* All rights reserved                                                  */
/************************************************************************/
/*
**The software source code contained in this file and in the related
**header files is the property of Microtec. and may only be
**used under the terms and conditions defined in the Microtec
**license agreement. You may modify this code as needed but you must 
**retain the Microtec copyright notices, including this statement.
**Some restrictions may apply when using this software with non-Microtec
**software.  In all cases, Microtec reserves all rights.
*/
/************************************************************************/

/*
 *
 *	Module Name:		%M%
 *
 *	Identification:		%Z% %I% %M%
 *
 *	Date:			%G%  %U%
 *
 *
 */

/*
 * * in82596a.c *
 *
 * Ethernet Revision Level:   1		Device Driver:	in82596a
 *
 * This file contains board independant functions to support the in82596
 * ethernet chip.
 *
 * All register read/write operations are performed through READ/WRITE macros
 * that can call a board specific function.
 * Board specific interrupt controllers are supported through the use of the
 * INTERRUPT_START/END macros and the * BOARD_SPEC_INT_ENABLE/DISABLE macros.
 *
 * To ensure data cache coherency, add -DCACHE_DATA_COHERENCY switch for
 * compiler.  If cpu stores data into data cache from buffer area which the
 * ethernet device writes, you should keep data cache coherency between cpu
 * and ethernet device.  In usual case, this switch will be added in Imakefile
 * of bsp and makefile of bsp builder reference bsp.
 * The data cache cohereny macros: cache_data_coherency_safe() and
 * cache_data_coherency_unsafe() are defined in cpu.h.
 *
 * "#if defined(I960)" is used as the usual little endian cpu.  If the new
 * target "xxx" supports little endian same as i960, you should modify that
 * conditional compilation as "#if defined(I960) || defined(xxx)"
 *
 * "INTELx86" is used to deal with master/slave interrupt controler in PC/AT.
 * Those target dependencies should removed in the future.
 *
 * History: *
 *
 * Date        Name              Description *
 * ---------------------------------------------------------------------- *
 * 11-15-92    Roger Cole       First Version 
 * 11-17-92    Donn Rochette 	82596 first cut 
 * 11-24-92    Siprian Rodrigues First working version *
 * 02-15-93    Siprian Rodrigues Binary driver - clean up 
 * 08-26-93    Roger Cole        Upgraded to use logio buffers directly 
 * 02-02-94    Neven Haltmayer   Make it usable 
 * 03-24-94    Siprian Rodrigues Consolidated version for Intel & Motorola
 *                               boards
 * 10-06-95    Makoto Saito	     Added cache flush/invalidate; clean up;
 *                               Wrote supplemental comments 
 * 1997-09-30	jerryf		     Fixes for EIS 5231, ERR953.
 * 2009-8-11    Russell Rush     optimized DMA use, Reveresed what jerryf did 
 *                               above for TIME_OUT.  Change TIME_OUT from 0x03000 
 *                               back to 0x300 with our OS & interrupts disabled 
 *                               spin loop takes too long to catch errors in comms.
 *                               BD main loop can be lengthen so much that the 
 *                               5ms cycle can be missed 4 times.
 */

#ifdef _MCC386
	#define	outpd outpw
#endif

#include "in82596a.h"		/* 82596 defines and typedefs */


unsigned int    no_res_events;	/* Counter of "No Resources" events	*/


/*************************************************************************
* *
** in82596a_install
**
** Input/Output:
**
**    dev        A pointer to a caller-allocated  section  of
**               memory   of   size  sizeof(vbsp_dev_desc_t).
**               in82596a_init returns a device  descriptor  at
**               this location.
**
** Return codes:
**
**           VBSP_SUCCESS    The call succeeded
**           VBSP_FAILURE    The call failed
**
** Side-effects:
** Global variables:
**
*/
#ifdef __STDC__
vbsp_return_t
in82596a_install(ether_1_dev_desc_t * dev, in82596a_config_t * cfg)
#else
vbsp_return_t
in82596a_install(dev, cfg)
ether_1_dev_desc_t *dev;
in82596a_config_t *cfg;
#endif
{
#if defined(INTELx86)
	extern int spurious_int_master(logio_interrupt_entry_t *);
	extern int spurious_int_slave2(logio_interrupt_entry_t *);
#endif

	/* linked to the additional RAM space for this device */
	dev->config = cfg->base_config;

	/* install the register map */
	dev->map = &cfg->reg;

	/* initialize the external board function fops entry */
	dev->externio = cfg->externio;

	dev->config.interface_revision_level = ETHER_1_INTERFACE;

	/* initalize the device specific fops entries */
	dev->fops.init = in82596a_init;
	dev->fops.int_info_set = in82596a_int_info_set;
	dev->fops.int_info_get = in82596a_int_info_get;
	dev->fops.pkt_recv = in82596a_recv;
	dev->fops.pkt_send = in82596a_send;
	dev->fops.ether_rx_tst = in82596a_rxtst;
	dev->fops.ether_tx_tst = in82596a_txtst;
#if defined(INTELx86)
	dev->fops.ether_err_tst = spurious_int_master; /* err fn, mapped to unexpected int */
	dev->fops.ether_init_tst = spurious_int_slave2;/* Used only in 486 board */
#else
	dev->fops.ether_err_tst = 0; /* error function */
	dev->fops.ether_init_tst = 0;
#endif
	return(VBSP_SUCCESS);
}

/*************************************************************************
**
** in82596a_init
**
**    This function initializes an instance of an in82596a
**    Ethernet controller. The initialization sequence may
**    configure:
**
**           a.   The number of send/receive buffers
**           b.   The interrupt vector
**           c.   Ethernet address
**
**    and should initializes the chip.
**
**    This function requires a  device   descriptor.  Any  function
**    that needs to access the device must used this device
**    descriptor.
**
** Input/Output:
**
**    dev        an ethernet device descriptor
**
** Return codes:
**
**           VBSP_SUCCESS    The call succeeded
**           VBSP_FAILURE    The call failed
**
** Side-effects:
** Global variables:
**
*/
vbsp_return_t in82596a_init_rx(ether_1_dev_desc_t * dev);
vbsp_return_t
in82596a_init(ether_1_dev_desc_t * dev)
{
	/* Initialize the global BSS data area for the driver */
	if ( in82596a_data_init(dev) == FALSE )
		return(VBSP_FAILURE);

	/* Perform first pass initialization of the 82596 */
	if ( in82596a_init_scp_and_iscp(dev) == FALSE )
		return(VBSP_FAILURE);

#ifdef BOOTPROM
	/* Perform a self test of the 82596 */
	if ( in82596a_diagnose(dev) == FALSE )
		return(VBSP_FAILURE);

	/* Perform second pass initialization after the self test */
	if ( in82596a_init_scp_and_iscp(dev) == FALSE )
		return(VBSP_FAILURE);
#endif

	if ( in82596a_init_chip(dev) == FALSE )
		return(VBSP_FAILURE);

	in82596a_init_rx(dev);
	return(VBSP_SUCCESS);
}

/*************************************************************************
**  Name:   in82596a_data_init
**
**  Calling Sequence:
**
**   in82596a_data_init();
**
**  Interface Variables:
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This routine initializes all of the global BSS area for the driver.
**
*/
int in82596a_data_init(ether_1_dev_desc_t * dev)
{
	struct comws *lwsp = &((in82596a_specific_t *)dev->config.specific)->lanws;
	unsigned long tmp_scp;	/* Temp system configuration pointer */
	int i;			/* Loop counter */
	TFD *tfd;
	TBD *tbd;

	/* Perform global data initialization */
	tmp_scp = (unsigned long) &lwsp->SCP_Buffer[0];
	tmp_scp += 0x10;
	tmp_scp &= 0xFFFFFFF0;
	lwsp->scp = (SCP *) tmp_scp;

	lwsp->begin_tfd = lwsp->end_tfd = (TFD *) 0;
	lwsp->begin_tbd = lwsp->end_tbd = (TBD *) 0;
	lwsp->begin_cbl = lwsp->end_cbl = (TFD *) 0;

	lwsp->begin_rfd = lwsp->end_rfd = (RFD *) 0;
	lwsp->begin_rbd = lwsp->end_rbd = (RBD *) 0;

	lwsp->plogio_full_b = lwsp->plogio_full_e = 0;
	lwsp->plogio_num = MAX_RBD + 8;

	no_res_events = 0;	/* Clear the "No Resources" event counter	*/

	lwsp->free_tbd_cnt = 0;

	in82596a_zero((unsigned char *) &lwsp->SCP_Buffer[0],
				  sizeof(lwsp->SCP_Buffer));
	in82596a_zero((unsigned char *) &lwsp->iscp, sizeof(lwsp->iscp));
	in82596a_zero((unsigned char *) &lwsp->scb, sizeof(lwsp->scb));
	in82596a_zero((unsigned char *) &lwsp->tfda[0], sizeof(lwsp->tfda));
	in82596a_zero((unsigned char *) &lwsp->tbda[0], sizeof(lwsp->tbda));
	in82596a_zero((unsigned char *) &lwsp->rfa[0], sizeof(lwsp->rfa));
	in82596a_zero((unsigned char *) &lwsp->rbda[0], sizeof(lwsp->rbda));

	/* initialize the TFD and TBD lists  */
	tfd = lwsp->begin_tfd = &lwsp->tfda[0];
	for ( i = 0; i < (MAX_TFD - 1); i++ )
	{
		tfd->next = (unsigned long *) &lwsp->tfda[i + 1];
		tfd = &lwsp->tfda[i + 1];
	}
	tfd->next = (unsigned long *) 0;
	lwsp->end_tfd = tfd;

	tbd = lwsp->begin_tbd = &lwsp->tbda[0];
	for ( i = 0; i < ((MAX_TFD * MPF) - 1); i++ )
	{
		tbd->tbd_addr = (unsigned long *) &lwsp->tbda[i + 1];
		tbd = &lwsp->tbda[i + 1];
	}
	tbd->tbd_addr = (unsigned long *) 0;
	lwsp->end_tbd = tbd;
	lwsp->free_tbd_cnt = (MAX_TFD * MPF);	/* all tbds are available */

	return(TRUE);
}

/*************************************************************************
 *  Name:   in82596a_init_scp_and_iscp
 *
 *  Calling Sequence:
 *
 *   in82596a_init_scp_and_iscp();
 *
 *  Interface Variables:
 *
 *  Global Variables:
 *
 *  Calls:
 *
 *  Description:
 *
 *   This function performs the first pass of the 82596 chip initialization.
 *
 */

int in82596a_init_scp_and_iscp(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	int i;			/* Loop counter */

	/* Set up the System Configuration Pointer */

	/* cws must be set, erratum 5.0.  */
	lwsp->scp->sysbus = (ISCP_SYSBUS_CSW |
#ifdef I960
						 /* Board specific. Shouldn't be here.
						  * Configuration would do just fine */
						 ISCP_SYSBUS_ACT_LOW |
#else
						 ISCP_SYSBUS_ACT_HIGH |
#endif
						 ISCP_SYSBUS_LOCK_DISABLED |
						 ISCP_SYSBUS_INTERNAL_TRIG |
						 ISCP_SYSBUS_LINEAR);

	lwsp->scp->sb_zeros = 0;		/* Must be 0 */
	lwsp->scp->ISCP_Addr = (unsigned long) (swap32(&lwsp->iscp));

	/* Set up the Intermediate System Configuration Pointer */
	lwsp->iscp.busy = ISCP_BUSY;
	lwsp->iscp.SCB_Addr = (unsigned long) swap32((unsigned long) &lwsp->scb);

	/* Set up the System Control Block */
	/* Since the VRTX OS has interrupts disabled during frame xmit/received */
	/* no need to get off bus; make maximum use of time  */
	lwsp->scb.TOffTimer = 0;			/* no time off bus */
	lwsp->scb.TOnTimer = 0xffff;		/* maxtime */

	/* Reset the 82596 */
	in82596a_write(dev, IN82596A_PORT_RESET);
	in82596a_eat_time(100000);

	/* Set up the SCP and issue Channel Attention */
	in82596a_write(dev, (unsigned long)lwsp->scp | PORT_ALTERNATE_SCP_ADDRESS);
	in82596a_ca_and_wait(dev);

	/* Now wait for the 82596 to process the command */
	i = ISCP_DELAY;
	while ( (lwsp->iscp.busy & ISCP_BUSY) && i )
	{
		i--;
		in82596a_eat_time(DELAY);
	}

	/* And return */
	if ( i )
		return(TRUE);
	else
		return(FALSE);
}

/*************************************************************************
 *  Name:   in82596a_init_chip
 *
 *  Calling Sequence:
 *
 *   in82596a_init_chip();
 *
 *  Interface Variables:
 *
 *  Global Variables:
 *
 *  Calls:
 *
 *  Description:
 *
 *   This function performs the second pass of the 82596 chip initialization.
 *   It configures the chip using the configure command, and sets up the
 *   individual Ethernet address.
 *
 */
int in82596a_init_chip(ether_1_dev_desc_t * dev)
{
	unsigned long stat = TRUE;	/* Return status */
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;

	/* Configure the 82596 chip */
	stat &= in82596a_configure(dev);

	/* Set up the individual address */
	stat &= in82596a_IA(dev);

	/* Now soak up the CX & CNA interrupts generated by the commands */
	in82596a_soak_up(dev);

	/* Place the System Control Block in a known state */
	lwsp->scb.CBL_addr = 0;
	lwsp->scb.cmd = 0;

	return(stat);
}

/*************************************************************************
**
**  void in82596a_init_rx()
**
**  Description:
**
**   Obtain the required number of logio buffers  for our receiver logic,
**   and  finish up initializing the receive frame area. Start up the
**   receiver after initialization
**
*/
vbsp_return_t
in82596a_init_rx(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	logio_buff_t *plogio;
	RFD *rfd;		/* Pointer to current receive frame descriptor */
	RBD *rbd;		/* Pointer to current receive buffer descriptor */
	unsigned long temp;		/* Temp variable */
	int i;			/* Loop counter */
	int dummy;

	if ( logio_get_free_list(dummy, MAX_RBD, &plogio) != 0 )
		return(VBSP_INSUFFICIENT_MBUFS);

	/* Initialize the receive frame descriptor list of the RFA */
	lwsp->begin_rfd = (RFD *) & lwsp->rfa[0];
	for ( i = 0; i < (MAX_RFD - 1); i++ )
	{
		rfd = (RFD *) & lwsp->rfa[i];
		rfd->cmd = RFD_CMD_FLEXIBLE;

		/* next in linked list */
		rfd->next = (unsigned long *) (swap32(&lwsp->rfa[(i + 1)]));
		rfd->rbd_addr = (unsigned long *) 0xFFFFFFFF;
		rfd->dcount = i;	/* for DEBUG */
	}

	/* COMPLETE the last RFD, Set el == 1 */
	rfd = (RFD *) & lwsp->rfa[i];	/* get pointer to rfd in array */
	rfd->cmd = RFD_CMD_FLEXIBLE;
#if 0
	/* make list circular */
	rfd->next = (unsigned long *) (swap32(&lwsp->rfa[0]));
#endif

	/* no buffers are connected yet */
	rfd->rbd_addr = (unsigned long *) 0xFFFFFFFF;

	rfd->dcount = i;		/* for DEBUG */
	rfd->cmd |= RFD_CMD_EL;
	lwsp->end_rfd = rfd;	/* point to this last rfd */

	/* LINK THE First RFA to the First RBD!! */
	lwsp->begin_rfd->rbd_addr = (unsigned long *) (swap32(&lwsp->rbda[0]));

	/* Initialize the receive buffer descriptor list of the RFA */
	/* all of the RFD,RFDs are initially set to zero, so el = 0 */

	lwsp->begin_rbd = (RBD *) & lwsp->rbda[0];
	for ( i = 0; i < (MAX_RBD - 1); i++ )
	{
		rbd = (RBD *) & lwsp->rbda[i];
		rbd->rbd_addr = (unsigned long *) (swap32(&lwsp->rbda[(i + 1)]));
		rbd->plogio = plogio;
		temp = (unsigned long) plogio->data;
		rbd->rb_addr = (unsigned long *) (swap32(temp));
		rbd->dcount = i;	/* for DEBUG */
		rbd->el_p_size = plogio->dlen;
		plogio = plogio->next_buff;
		rbd->plogio->next_buff = logio_buff_NULL;
	}

	rbd = (RBD *) & lwsp->rbda[i];
#if 0
	rbd->rbd_addr = (unsigned long *) (swap32(&lwsp->rbda[0]));
#endif
	rbd->plogio = plogio;
	temp = (unsigned long) plogio->data;
	rbd->rb_addr = (unsigned long *) (swap32(temp));
	rbd->el_p_size = plogio->dlen;

	rbd->dcount = i;		/* for DEBUG */
	rbd->el_p_size |= RBD_EL;
	lwsp->end_rbd = rbd;

	/* Start up the receiver */
	in82596a_ru_start(dev);
	return(0);
}


/*************************************************************************
**
** in82596a_recv
**
**   This function gets the next available packet from an in82596a.
**
**    Only one packet (frame) will be returned at a time.
**
** Inputs:
**
**    dev           device descriptor
**
** Outputs:
**
**    buf             Pointer to a buffer containing the packet
**
** Return codes:
**
**    VBSP_SUCCESS         The call succeeded
**    VBSP_INVALID_DEV_ID  The ID is invalid
**    VBSP_FAILURE         The call failed
**
** for the first completed RFD in the list,
**  determine the number of logio buffers attached to the RFD
**  request the number of logio buffers
**  if we are unable to get these logio buffers, return an error
**  save the logio buffer chain with the valid frame
**  link the new buffers into the RFD
**  connect the RFD to the end of the list
**  restart the RU if necessary
**  return the list of logio buffers
** CHECK FOR STATUS "RAN OUT OF BUFFER SPACE" SOMEWHERE.....
**
*/


int check_ru_addr(dev, rfd, rbd)
ether_1_dev_desc_t *dev;	/* in  */
RFD *rfd;
RBD *rbd;
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	if ( (RFD *) & lwsp->rfa[0] <= (RFD *) rfd )
	{
		if ( (RFD *) rfd <= (RFD *) & lwsp->rfa[MAX_RFD] )
		{
			if ( (RBD *) & lwsp->rbda[0] <= (RBD *) rbd )
			{
				if ( rbd <= (RBD *) & lwsp->rbda[MAX_RFD * MPF] )
					return 1;
			}
		}
	}
	return 0;
}

void start_my_ru(dev)
ether_1_dev_desc_t *dev;	/* in  */
{
	struct comws volatile *lwsp = &((in82596a_specific_t *) dev->config.specific)->lanws;

	if ( (lwsp->scb.status & SCB_RUS) == SCB_RU_READY )
		return;
#if 0
	if ( lwsp->begin_rfd == lwsp->end_rfd )
	{
		/* here we are left with only one buffer */
		/* the easiest way is to wipe out everything */
		/* and start the process again */

		return;
	}
	if ( lwsp->begin_rfd->cmd & RFD_CMD_EL )
		return;
#endif
	if ( lwsp->begin_rfd->status & RFD_STATUS_COMPLETE )
		return;
	if ( check_ru_addr(dev, lwsp->begin_rfd, lwsp->begin_rbd) )
	{
		lwsp->scb.cmd = SCB_RU_START;
		lwsp->scb.RFA_addr = (unsigned long *) (swap32((unsigned long)
													   lwsp->begin_rfd));
		lwsp->begin_rfd->rbd_addr = (unsigned long *)(swap32((unsigned long)
															 lwsp->begin_rbd));
		in82596a_ca(dev);
	}
}

vbsp_return_t
in82596a_recv(dev, pplogio)
ether_1_dev_desc_t *dev;	/* in  */
logio_buff_t **pplogio;		/* out */
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	RFD *rfd;
	RBD *rbd;
	logio_buff_t *pliofree;
	RFD *b_recvq, *e_recvq;
	int     i;
	vbsp_return_t   stat;

#if defined(CACHE_DATA_COHERENCY)
	cpu_cm_cinfo_t cache_info;
#endif

	/*
	**	Check for the RX running out of resources (RFDs or RBDs).
	** If so, the RX is in "No Response" mode (dead to the world)
	** and the only thing to do is reinitialize all the RFDs and
	** RBDs and restart the RX from the beginning of the lists.
	** Unfortunately, this means dumping any frames that have
	** already been received.
	*/

	switch ( lwsp->scb.status & SCB_RUS )
	{
		case SCB_RU_NO_RES:
		case SCB_RU_NO_RES_RBD:
		case SCB_RU_NO_RBD:

			/*
			**	Increment the counter of these "No Resources" events.
			*/

			no_res_events++;

			in82596a_ru_suspend(dev);		/* Stop the RX		*/

			/*
			**	Free up the old logio buffers.
			*/

			for ( i = 0; i < MAX_RBD; i++ )
			{
				rbd = (RBD *) & lwsp->rbda[i];

				if ( rbd->plogio )
				{
					(rbd->plogio->free)(rbd->plogio);
				}
			}

			/*
			**	Clear the RFD and RBD structures and pointers.
			*/

			lwsp->begin_rfd = lwsp->end_rfd = (RFD *) 0;
			lwsp->begin_rbd = lwsp->end_rbd = (RBD *) 0;
			lwsp->plogio_full_b = lwsp->plogio_full_e = 0;
			lwsp->plogio_num = MAX_RBD + 8;
			in82596a_zero((unsigned char *) &lwsp->rfa[0], sizeof(lwsp->rfa));
			in82596a_zero((unsigned char *) &lwsp->rbda[0], sizeof(lwsp->rbda));

			/*
			**	Initialize RFDs and RBDs and start the RX again.
			*/

			if ( (stat = in82596a_init_rx(dev)) != VBSP_SUCCESS )
			{
				return(stat);
			}

			break;

		default:
			break;
	}

	*pplogio = logio_buff_NULL;	/* null this so we do not get any errors */
	b_recvq = e_recvq = (RFD *) 0;
	rfd = lwsp->begin_rfd;	/* point to the first possible valid rfd */

#if defined(CACHE_DATA_COHERENCY)
	cache_data_coherency_safe(cache_info);
#endif

	while ( rfd )
	{
		if ( !(rfd->status & RFD_STATUS_COMPLETE) )
		{
			start_my_ru(dev);
			break;
		}
		if ( rfd->rbd_addr == (unsigned long *) 0xffffffff )
		{
			break;
		}
		lwsp->begin_rfd = (RFD *) (swap32((unsigned long) rfd->next));

		/* point to first RBD linked to the first RFD */
		rbd = (RBD *) (swap32((unsigned long) rfd->rbd_addr));

		/* update begin_rbd */
		lwsp->begin_rbd = (RBD *) (swap32((unsigned long) rbd->rbd_addr));

		/* detach the rbd list from i82596's list */
		rbd->rbd_addr = (unsigned long *) 0;
		rfd->next = (unsigned long *) 0;

		/* put the rfd on the recvq for the recv routine to process */
		if ( b_recvq == (RFD *) 0 )
		{
			b_recvq = rfd;
			e_recvq = rfd;
		}
		else
		{
			e_recvq->next = (unsigned long *) rfd;
			e_recvq = rfd;
		}
		/* get next rfd */
		rfd = lwsp->begin_rfd;
	}
	if ( b_recvq != (RFD *) 0 )
	{
		rfd = b_recvq;
		while ( rfd )
		{
			b_recvq = (RFD *) b_recvq->next;

			if ( rfd->rbd_addr == (unsigned long *) 0 )
			{
#if defined(CACHE_DATA_COHERENCY)
				cache_data_coherency_unsafe(cache_info);
#endif
				return(VBSP_NO_PACKET);
			}
			rbd = (RBD *) (swap32((unsigned long) rfd->rbd_addr));

			if ( (lwsp->plogio_num) &&
				 ((pliofree = logio_buff_get(0, 0)) != logio_buff_NULL) )
			{
				/* there are enough buffers */
				pliofree->next_buff = logio_buff_NULL;
				if ( lwsp->plogio_full_b == logio_buff_NULL )
				{
					lwsp->plogio_full_b = lwsp->plogio_full_e = rbd->plogio;
				}
				else
				{
					lwsp->plogio_full_e->next_buff = rbd->plogio;
					lwsp->plogio_full_e = rbd->plogio;
				}
				lwsp->plogio_full_e->dlen = (rbd->status &
											 RBD_STATUS_COUNT_MASK);

				/* link in new logio buffer */
				rbd->plogio = pliofree;
				rbd->rb_addr = (unsigned long *) swap32((unsigned long)
														pliofree->data);
				rbd->el_p_size = pliofree->dlen;
				lwsp->plogio_num--;
			}
			rbd->el_p_size |= RBD_EL;
			rbd->el_p_size &= ~RBD_P;
			rbd->status = 0;
			rbd->rbd_addr = 0;
			if ( lwsp->begin_rbd == (RBD *) 0 )
			{
				lwsp->begin_rbd = rbd;
				lwsp->end_rbd = rbd;
			}
			else
			{
				lwsp->end_rbd->rbd_addr =
				(unsigned long *)(swap32((unsigned long) rbd));
				lwsp->end_rbd->el_p_size &= ~RBD_EL;
				lwsp->end_rbd = rbd;
			}
			in82596a_zero((unsigned char *) rfd, sizeof(RFD));
			rfd->cmd = RFD_CMD_FLEXIBLE | RFD_CMD_EL;
			rfd->rbd_addr = (unsigned long *) 0xffffffff;
			if ( lwsp->begin_rfd == (RFD *) 0 )
			{
				lwsp->begin_rfd = rfd;
				lwsp->end_rfd = rfd;
			}
			else
			{
				lwsp->end_rfd->next =
				(unsigned long *) (swap32((unsigned long) rfd));
				lwsp->end_rfd->cmd &= ~RFD_CMD_EL;
				lwsp->end_rfd = rfd;
			}
			rfd = b_recvq;
		}
	}
	if ( lwsp->plogio_full_b == logio_buff_NULL )
	{
#if defined(CACHE_DATA_COHERENCY)
		cache_data_coherency_unsafe(cache_info);
#endif
		return(VBSP_NO_PACKET);
	}
	else
	{
		*pplogio = lwsp->plogio_full_b;
		lwsp->plogio_full_b = lwsp->plogio_full_b->next_buff;
		(*pplogio)->next_buff = logio_buff_NULL;
		lwsp->plogio_num++;
	}
#if defined(CACHE_DATA_COHERENCY)
	cache_data_coherency_unsafe(cache_info);
#endif
	return(VBSP_SUCCESS);
}

/*************************************************************************
**
** in82596a_free_send_list()
**
**
**    This function will free TBDs, TFDs and logio_buffers after a
**    transmit command has been completed.
**
** Algorithm:
**    foreach command complete
**       foreach TBD of TFD
**          free logio buffer attached to the TBD
**          free TBD
**       free TFD
*/
vbsp_return_t
in82596a_free_send_list(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	TFD *tfd;
	TBD *tbd;
	int cnt;


	/* The int could be because of transmission of frame or CU went idle
	 * any way check if there are any transmitted frames not freed
	 */

/*
	if (!(lwsp->begin_cbl->status & TFD_STATUS_COMPLETE) &&
	((lwsp->scb.status & SCB_CUS) != SCB_CU_ACTIVE))
	{
	* we have frames not completed and the transmitter *
	* has been shut down.... *
	in82596a_restart_cu(dev, lwsp->begin_cbl);
	}
*/


	if ( !(lwsp->begin_cbl) )
		return(VBSP_SUCCESS);


	for ( tfd = lwsp->begin_cbl; tfd; tfd = lwsp->begin_cbl )
	{
		if ( tfd->status & TFD_STATUS_COMPLETE )
		{	/* if command complete */
			/* transmission completed without error or with error   */
			/*
			 * In any case free the tfd, we are dropping the frame
			 */
			/*
			 * if it was transmitted with error. TCP will take care
			 */
			/* update begin_cbl */
			lwsp->begin_cbl = (TFD *) swap32((unsigned long)
											 lwsp->begin_cbl->next);
			tbd = (TBD *) swap32((unsigned long) tfd->tbd_addr);
			lwsp->free_tbd_cnt++;
			for ( cnt = 0; tbd; tbd = (TBD *)swap32(tbd->tbd_addr),
				lwsp->free_tbd_cnt++, cnt++ )
			{
				/* free the logio buffer */
				(tbd->plogio->free) (tbd->plogio);
				tbd->plogio = logio_buff_NULL;

				/* attach freed tbd to the CPU's list */
				if ( lwsp->begin_tbd == (TBD *) 0 )
					lwsp->begin_tbd = lwsp->end_tbd = tbd;
				else
				{
					lwsp->end_tbd->tbd_addr = (unsigned long *) tbd;
					lwsp->end_tbd = tbd;
				}
				if ( tbd->eof_size & TBD_EOF )
					break;
			}
			/* attach the tfd to the CPU's list */
			tfd->next = (unsigned long *) 0;
			if ( lwsp->begin_tfd == (TFD *) 0 )
				lwsp->begin_tfd = lwsp->end_tfd = tfd;
			else
			{
				lwsp->end_tfd->next = (unsigned long *) tfd;
				lwsp->end_tfd = tfd;
			}
		}
		else
			break;		/* end of for loop */
	}
	if ( lwsp->begin_cbl == (TFD *)0 )
		lwsp->end_cbl = (TFD *)0;

	return(VBSP_SUCCESS);
}

/*************************************************************************
**
** in82596a_send
**
**    This function sends the packet to an in82596a.
**
** Inputs:
**
**      dev           An ID returned by a call to in82596a_init
**      list            A pointer to a buffer
**
** Outputs:
**
** Return codes:
**
**    VBSP_SUCCESS         The call succeeded
**    VBSP_INVALID_DEV_ID  The ID is invalid
**    VBSP_FAILURE         The call failed
**
** The send algorithm will vary considerably from device to
** device.  In general, however, if there exists a linked list
** or array of structures that in turn each point to a single
** data buffer, then the following algorithms may be appropiate.
**
** There are two possible cases for sending packets:
**
**    I. The entire packet is contained in a single logio buffer.
**    II. The packet is contained in a linked list of logio buffers.
**
** Sending a packet in the Case I scenario:
**
**    1. Make sure that there is a free transmit descriptor
**    2. Write the address of the data pointer in the logio
**       buffer structure into the buffer field of the
*        free transmit descriptor
**    3. Write the length from the len field of the logio
**       buffer structure into the count field of the free
**       transmit descriptor
**
**    4. Finally, indicate in the transmit descriptor:
**
**          a. The buffer contains data to be transmitted
**          b. The buffer is the start of a packet
**          c. The buffer is the end of a packet
**
**    NOTE: Usually, it is important to complete the count and
**          address fields of the buffer before the buffer has
**          been flaged as ready to transmit
**
**    5. Attempt to start the transmitter if necessary.
**
**
** Sending a packet in the Case II scenario:
**
**    1. Make sure that there are N free transmit descriptors
**    2. Write addresses of data pointers from buf structures
**       into the buffer fields of the free transmit descriptors
**    3. Write the lengths from len fields of the logio buffer
**       structures into data count fields of the free
**       transmit descriptors
**    4. Set any bits in the transmit descriptors that indicate
**       a continued buffer.
**    5. Set any bits in the end transmit descriptor that may
**       indicate the end of the packet.
**    6. Indicate that the packet is ready to transmit.
**    7. Attempt to start the transmitter if necessary.
**
*/
vbsp_return_t
in82596a_send(dev, plogio)
ether_1_dev_desc_t *dev;	/* in  */
logio_buff_t *plogio;		/* in  */
{
	struct comws volatile *lwsp = &((in82596a_specific_t *) dev->config.specific)->lanws;
	TFD *tfd;
	TBD *tbd;
	logio_buff_t *plio;
	int len;
	unsigned short lio_count = 0;
#if defined(CACHE_DATA_COHERENCY)
	cpu_cm_cinfo_t cache_info;
#endif
#ifdef DEBUG_BFR
	logio_buff_t *plio1;
#endif

#define	TIME_OUT  0x0300		
	/* This value is determined based on experiments using 
	   MVME167-02B (Motorola 68040), in order to reduce the 
	   chance to get into the race condition without hurting 
	   the reception performance. If this Ethernet driver is 
	   used on other boards with other processors, the value 
	   may need to be adjusted. 
	   Slower the CPU, lower the value shoule be.*/

	/*
	** 1997-09-30 jerryf
	**
	**	Changed from 0x0300 to 0x3000 because "spray"
	** performance was terrible in Spectra 4.x (i.e., since
	** this change was made). It's still not as good as
	** 3.Cda, but hopefully this is a workable comprimise.
	*/

	unsigned long retry_count = 0;

/* Needs to restart the CU if it's not working while there is some
   remaining commands.  */


	if ( plogio == (logio_buff_t *) 0 )
	{
		if ( (!(lwsp->scb.status & SCB_CUS) == SCB_CU_ACTIVE) 
			 && (lwsp->begin_cbl) )
		{
			/* The CU is not active!!
				Check one more time if the CU has completed the process, if
				so free the completed commands before restart_cu(). */
			if ( (lwsp->begin_cbl->status & TFD_STATUS_COMPLETE) )
				in82596a_free_send_list(dev);
			in82596a_restart_cu(dev, lwsp->begin_cbl);
		}
		return(0);
	}

#if defined(CACHE_DATA_COHERENCY)
	cache_data_coherency_safe(cache_info);
#endif

	/* non-blocking check for resources.... * We must have 1 TFD and 1
	TBD for each logio buffer in the list */

	for ( plio = plogio; plio; plio = plio->next_buff )
		lio_count++;

	if ( (lwsp->begin_tfd == (TFD *) 0) || (lwsp->free_tbd_cnt < lio_count) )
	{
#if defined(CACHE_DATA_COHERENCY)
		cache_data_coherency_unsafe(cache_info);
#endif
		if ( (!(lwsp->scb.status & SCB_CUS) == SCB_CU_ACTIVE)
			 && (lwsp->begin_cbl) )
		{
			/* The CU is not active!!
			Check one more time if the CU has completed the process, if
			so free the completed commands before restart_cu(). */
			if ( (lwsp->begin_cbl->status & TFD_STATUS_COMPLETE) )
				in82596a_free_send_list(dev);
			in82596a_restart_cu(dev, lwsp->begin_cbl);
		}
		return(VBSP_NO_DEVICE_BUFS);
	}

	/* we have a frame to transmit, get a transmit frame descriptor */
	/* prev_sr = logio_interrupt_disable(); */
	tfd = lwsp->begin_tfd;
	lwsp->begin_tfd = (TFD *) lwsp->begin_tfd->next;

	/* fill in the tfd */
	in82596a_zero((unsigned char *) tfd, sizeof(TFD));
	tfd->cmd = (TFD_CMD_EOL | TFD_CMD_INTERRUPT | TFD_CMD_TRANSMIT |
				TFD_CMD_FLEXIBLE);

	/* get a transmit buffer descriptor from beginning of the free list */
	/* LINK the TFD to this first TBD! */
	/* point the free TBD pointer to the next TBD */

	tbd = lwsp->begin_tbd;
	tfd->tbd_addr = (unsigned long *) swap32(tbd);
	lwsp->begin_tbd = (TBD *) lwsp->begin_tbd->tbd_addr;
	lwsp->free_tbd_cnt--;

	/* This assumes that there are TBDs available.  This routine is
	 * NON-Reentrant! * The TBDs may fall below the requried count if
	 * this routine is called again! * In this case, the begin_tbd must
	 * always be checked for NULL.  If it is NULL, * then the resources
	 * must be freed, and the routine should return a resources * error
	 * code.
	 */

	/* TBD->logio_buff_t *
	 *
	 * obtain initialize and attach logio buffers to transmit buffer
	 * descriptors.
	 */
	for ( plio = plogio, len = 0; plio; plio = plio->next_buff )
	{
		/* clear the TBD */
		in82596a_zero((unsigned char *) tbd, sizeof(TBD));
		tbd->eof_size = plio->dlen;
		len += plio->dlen;	/* counter for total length */

#ifdef DEBUG_BFR
		if ( (int) plio->data & 0x3 )
		{
			if ( (plio1 = logio_buff_get(0, 0)) == logio_buff_NULL )
			{
#if defined(CACHE_DATA_COHERENCY)
				cache_data_coherency_unsafe(cache_info);
#endif
				return(VBSP_NO_DEVICE_BUFS);
			}
			plio1->next_buff = plio->next_buff;
			plio1->dlen = plio->dlen;
			bcopy(plio->data,plio1->data, plio->dlen);
#if 0
			printf("%x  %x %x\n",plio,plio1,plio->data);
#endif
			(plio->free)(plio);	/* free the logio buffer */
			plio = plio1;
		}
#endif
		tbd->plogio = plio;	/* point to logio buffer */

		/* point to logio data */
		tbd->tb_addr = (unsigned long *) swap32(plio->data);

		if ( plio->next_buff == (logio_buff_t *) 0 )
			break;		/* stop if no more buffers */

		/* For reentrancy: Check for begin_tbd == 0, fail * if this
		 * condition is true.
		 */
		tbd->tbd_addr = (unsigned long *) swap32(lwsp->begin_tbd);
		tbd = lwsp->begin_tbd;
		lwsp->begin_tbd = (TBD *) lwsp->begin_tbd->tbd_addr;
		lwsp->free_tbd_cnt--;
	}

	/* All of the logio buffers have been linked to TBDs */

	/* make certain we transmit the minimum number of bytes per frame... */
	if ( len < MIN_FRAME )
		tbd->eof_size += (MIN_FRAME - len);

	/* The last tbd must have el=1 */
	tbd->eof_size |= TBD_EOF;


	if ( lwsp->begin_cbl )
	{
		if ( !(lwsp->end_cbl->status & TFD_STATUS_COMPLETE) )
		{
			if ( (lwsp->scb.status & SCB_CUS) == SCB_CU_ACTIVE )
			{
				/* The last cmd is not completed, and the CU is still active.
				   Give it a little time to complete the cmd before
				   calling free_send_list.			*/

				retry_count = 0;
				while ( !(lwsp->end_cbl->status & TFD_STATUS_COMPLETE)
						&& ((lwsp->scb.status & SCB_CUS) == SCB_CU_ACTIVE)
						&& (retry_count++ < TIME_OUT) );
			}
		}
		/* release all logio_buff_ts already sent by hardware */
		in82596a_free_send_list(dev);

	}

	/* Link the TFD into the command block list.  The interrupts are *
	 * disabled to ensure that a command completion does not occur *
	 * while the command list is being updated when begin_cbl is * not
	 * equal to zero.
	 */

	/* Is it the first TBD on list? */
	if ( lwsp->begin_cbl == (TFD *) 0 )
	{
		lwsp->begin_cbl = tfd;
		lwsp->end_cbl = tfd;
		in82596a_restart_cu(dev, tfd);

	}
	else
	{
		/* link this frame into the end of the list */
		lwsp->end_cbl->next = (unsigned long *) swap32((unsigned long) tfd);
		lwsp->end_cbl->cmd &= ~TFD_CMD_EOL; 
		lwsp->end_cbl = tfd;

		if ( (lwsp->scb.status & SCB_CUS) != SCB_CU_ACTIVE )
		/* The last chance to check the status, before give it up. */
		{
			/* we have frames not completed and the transmitter */
			/* has been shut down.... */
			/* The CU is stopped!! 
			   Check one more time if the CU has completed the process, if
			   so free the completed commands before restart_cu(). */
			in82596a_free_send_list(dev);
			in82596a_restart_cu(dev, lwsp->begin_cbl);
		}
	}
#if defined(CACHE_DATA_COHERENCY)
	cache_data_coherency_unsafe(cache_info);
#endif
	return(0);
}

/*************************************************************************
**
** in82596a_txtst
**
**    This function is called by the logio_default_int_handler in
**    order to determine whether or not a packet was transmitted.
**    Since there are four possible events that can occur on the
**    Lance vector, the logio default interrupt handler must, any
**    time the interrupt fires, determine which of the four events
**    caused the interrupt. This function will process the interrupt
**    and return 1 only if the cause of the interrupt
**    was a transmit packet event.
**
** Inputs:
**
**    entry: pointer to entry in interrupt table
**    num: maximum size of event_array
**
** Outputs:
**
**    indexp: pointer into event array where next entry is to be
**            inserted
**    event_array: array of <dev, event> pairs for this vector
**
** Return codes:
**
**    1: ETHER_1 transmitted a packet
**    0: ETHER_1 didn't transmit a packet
**
** Method:
**
**    get the chip status
**    if (status indicates packet transmission)
**       clear interrupt
**       enable interrupts
**       call system callout, if one has been installed
**       return 1
**    else
**       return 0
**
** Side-effects:
** Global variables:
**
*/
int in82596a_txtst(entry)
logio_interrupt_entry_t *entry;
{
	ether_1_dev_desc_t *dev = (ether_1_dev_desc_t *) entry->id->data;
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	unsigned short volatile *intr_stat = &((in82596a_specific_t *)
										   dev->config.specific)->intr_stat;
	int retcode = 0;
#if defined(CACHE_DATA_COHERENCY)
	cpu_cm_cinfo_t cache_info;
	cache_data_coherency_safe(cache_info);
#endif


	/* this is needed for shared memory driver, need to check */
	/* this else we would get a bus error when os is running */
	if ( !lwsp->begin_cbl )
	{
#if defined(CACHE_DATA_COHERENCY)
		cache_data_coherency_unsafe(cache_info);
#endif
		return(retcode);
	}
	if ( (lwsp->scb.status & SCB_CUS) != SCB_CU_ACTIVE )
	{

		if ( !(lwsp->begin_cbl->status & TFD_STATUS_COMPLETE) )
		{

			lwsp->scb.cmd = SCB_CU_START;
			lwsp->scb.CBL_addr 
			= (unsigned long *)swap32((unsigned long)lwsp->begin_cbl);
		}
		else
		{
			in82596a_free_send_list(dev);
			if ( lwsp->begin_cbl )
			{
				lwsp->scb.cmd = SCB_CU_START;
				lwsp->scb.CBL_addr 
				= (unsigned long *)swap32((unsigned long)lwsp->begin_cbl);
			}
			else
				lwsp->scb.cmd = 0;
		}
	}

	if ( *intr_stat & (SCB_ST_CX | SCB_ST_CNA) )
	{
		lwsp->scb.cmd |= (SCB_ST_CX | SCB_ST_CNA);

		/* Issue channel attention to indicate acknowledgement of the int */
		in82596a_ca(dev);
		in82596a_wait_scb_cmd_zero(dev);  
		/* If receive response is slow due to this wait, 
		   ca() should be called twice, first for ACK 
		   and wait for completion, and then the last for the
		   CU_START for transmission w/o wait. */
		retcode = 1;
	}
	else if ( lwsp->scb.cmd )
	{
		in82596a_ca(dev);
	}


#if defined(CACHE_DATA_COHERENCY)
	cache_data_coherency_unsafe(cache_info);
#endif

	/* If there was either RX or TX interrupt, send EOI */
	if ( *intr_stat )
		INTERRUPT_END((ether_1_dev_desc_t *) entry->id->data, INT_PKT_TX);

	return(retcode);
}

/*************************************************************************
**
**  in82596a_rxtst
**
**    This function is called by the logio_default_int_handler in
**    order to determine whether or not a packet was received.
**    Since there are four possible events that can occur on the
**    Lance vector, the logio default interrupt handler must, any
**    time the interrupt fires, determine which of the four events
**    caused the interrupt. This function will process the interrupt
**    and return 1 only if the cause of the interrupt was a receive
**    packet event.
**
** Inputs:
**
**    entry: pointer to entry in interrupt table
**    num: maximum size of event_array
**
** Outputs:
**
**    indexp: pointer into event array where next entry is to be
**            inserted
**    event_array: array of <dev, event> pairs for this vector
**
** Return codes:
**
**    1: ETHER_1 received a packet
**    0: ETHER_1 didn't receive a packet
**
** Method:
**
**    get the chip status
**    if (status indicates packet reception)
**       clear interrupt
**       enable interrupts
**       call system callout, if one has been installed
**       return 1
**    else
**       return 0
**
** Side-effects:
** Global variables:
**
*/


int in82596a_rxtst(entry)
logio_interrupt_entry_t *entry;
{
	ether_1_dev_desc_t *dev = (ether_1_dev_desc_t *) entry->id->data;
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	unsigned short volatile *intr_stat = &((in82596a_specific_t *)
										   dev->config.specific)->intr_stat;
	int retcode = 0;
#if defined(CACHE_DATA_COHERENCY)
	cpu_cm_cinfo_t cache_info;
#endif

	INTERRUPT_START((ether_1_dev_desc_t *) entry->id->data, INT_PKT_RX);

	/* Wait for the 82596 to clear the SCB command word */
#if defined(CACHE_DATA_COHERENCY)
	cache_data_coherency_safe(cache_info);
#endif
	in82596a_wait_scb_cmd_zero(dev);
	*intr_stat = lwsp->scb.status;

	if ( *intr_stat & (SCB_ST_FR | SCB_ST_RNR) )
	{
		lwsp->scb.cmd = (SCB_ST_FR | SCB_ST_RNR);

		/* Issue channel attention to indicate acknowledgement of the int */
		in82596a_ca(dev);
		in82596a_wait_scb_cmd_zero(dev);

#if !defined(INTELx86) /* ON intel processors, EOI should be executed only once
			  per interrupt. It is done by the last int source, the
			  TX interrupt */
		INTERRUPT_END((ether_1_dev_desc_t *) entry->id->data, INT_PKT_RX);
#endif
		retcode = 1;
	}
#if defined(CACHE_DATA_COHERENCY)
	cache_data_coherency_unsafe(cache_info);
#endif
	return(retcode);
}

/*************************************************************************
**
** in82596a_int_info_get
**
** This function returns interrupt information that tells the
** caller which interrupts are enabled for a particular device.
**
** Inputs:
**     dev          the ether_1 device descriptor
**     intrpt       which interrupt actually requested
** Outputs:
**     intrpt       flags field is updated
**
** Return codes:
**     LOGIO_STATUS_OK              The call succeeded
**     LOGIO_STATUS_NOTSUPPORTED
**     LOGIO_STATUS_EPARAM
**
*/
#ifdef __STDC__
logio_status_t
in82596a_int_info_get(ether_1_dev_desc_t * dev, logio_int_entry_t * intrpt)
#else
logio_status_t
in82596a_int_info_get(dev, intrpt)
ether_1_dev_desc_t *dev;
logio_int_entry_t *intrpt;
#endif
{
	in82596a_specific_t *in82596a =
	(in82596a_specific_t *)dev->config.specific;
	intrpt->flags = in82596a->shadow_int_flags;
	return(LOGIO_STATUS_OK);
}

/*************************************************************************
** in82596a_int_info_set
**
** This function controls a specific event/id pair by enabling or
** disabling the specific event requested.
**
** Inputs:
**     dev          the ether_1 device descriptor
**     intrpt       which event to set
** Outputs:
**     intrpt       flags field is updated to old value
**
** Return codes:
**     LOGIO_STATUS_OK              The call succeeded
**     LOGIO_STATUS_NOTSUPPORTED
**     LOGIO_STATUS_EPARAM
**
*/
#ifdef __STDC__
logio_status_t
in82596a_int_info_set(ether_1_dev_desc_t * dev, logio_int_entry_t * intrpt)
#else
logio_status_t
in82596a_int_info_set(dev, intrpt)
ether_1_dev_desc_t *dev;
logio_int_entry_t *intrpt;
#endif
{
	int new_flags = intrpt->flags;
	in82596a_specific_t *in82596a =
	(in82596a_specific_t *) dev->config.specific;
	intrpt->flags = in82596a->shadow_int_flags;

	if ( new_flags == LOGIO_INT_ENABLED )
		BOARD_SPEC_INT_ENABLE(dev, intrpt);
	else if ( new_flags == LOGIO_INT_DISABLED )
		BOARD_SPEC_INT_DISABLE(dev, intrpt);
	else
		return(LOGIO_STATUS_NOTSUPPORTED);

	in82596a->shadow_int_flags = new_flags;
	return(LOGIO_STATUS_OK);
}

/*************************************************************************
**  Name:   in82596a_diagnose
**
**  Calling Sequence:
**
**   in82596a_diagnose();
**
**  Interface Variables:
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function performs a simple diagnostic check on the 82596.
**
**/
int in82596a_diagnose(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	int stat;			/* Returned status */
	DIAG diagnose;

	/* clear the structure */
	in82596a_zero((unsigned char *) &diagnose, sizeof(diagnose));

	/* Initialize the diagnose command block */
	diagnose.cmd = DIAGNOSE | SCB_CMD_EL;
	diagnose.status = 0;
	diagnose.next = 0;

	/* Set up the System Configuration Block */
	lwsp->scb.cmd = SCB_CU_START;
	lwsp->scb.CBL_addr = (unsigned long *) swap32((unsigned long *) &diagnose);

	/* Execute the command */
	in82596a_ca_and_wait(dev);
	in82596a_eat_time(DELAY);
	in82596a_ack_cmd(dev);

	/* Wait for the command to complete */
	stat = TRUE;
	while ( !(diagnose.status & STATUS_COMMAND_COMPLETE) )
	{
		if ( diagnose.status & STATUS_FAIL )
		{
			stat = FALSE;
			break;
		}
	}

	/* And return */
	return(stat);
}

/*************************************************************************
**  Name:   in82596a_configure
**
**  Calling Sequence:
**
**   in82596a_configure();
**
**  Interface Variables:
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function configures the 82596 chip correctly for the mode we
**   need it to run under.
**
**/

int in82596a_configure(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	int stat;			/* Returned status */
	CONF config;

	/* clear the elements of the struct */
	in82596a_zero((unsigned char *) &config, sizeof(config));

#define CFG_MONITOR_DISABLE         0xC0
#define CFG_DO_NOT_SAVE_BAD_FRAMES  0

	config.status = 0;
	config.cmd = CONFIGURE | SCB_CMD_EL;
	config.next = 0;
	config.p_bytecount = 0xE;	/* prefetch = 0, byte count = 0x0E */
	config.monitor_fifolimit = CFG_MONITOR_DISABLE | 8;
	config.savbf = CFG_DO_NOT_SAVE_BAD_FRAMES;
	config.loop_preamble_addr = 0x2E;
	config.bof_priority = 0;
	config.interframe_space = 0x60;
	config.slot_time_low = 0;
	config.retry_slot_high = 0xf2;
	config.byte8 = 0;
	config.byte9 = 0;
	config.min_frame_len = 0x40;
	config.byte11 = 0xff;	/* byte11 = FF (See 82596 Errata) */
	config.fdx = 0;
	config.disable_bof_multi_1a = 0x3f;
	/* Execute the command */
	in82596a_wait_scb_cmd_zero(dev);

	lwsp->scb.cmd = SCB_CU_START;
	lwsp->scb.CBL_addr = (unsigned long *) swap32((unsigned long *) &config);
	in82596a_ca_and_wait(dev);
	in82596a_eat_time(DELAY);
	in82596a_ack_cmd(dev);

	/* Wait for the command to complete */
	stat = TRUE;
	while ( !(config.status & STATUS_COMMAND_COMPLETE) )
	{
		if ( !(config.status & STATUS_OK) )
		{
			stat = FALSE;
			break;
		}
	}

	/* And return */
	return(stat);
}

/*************************************************************************
**  Name:   in82596a_IA
**
**  Calling Sequence:
**
**   in82596a_IA();
**
**  Interface Variables:
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function executes the set Individual Address command.
**
**/

int in82596a_IA(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	int stat;			/* Returned status */
	INDADDR ia;

	/* zero out the struct elements */
	in82596a_zero((unsigned char *) &ia, sizeof(ia));

	/* Wait for the SCB status to settle */
	in82596a_wait_scb(dev);

	/* Initialize the Individual Address command */
	/* get the ethernet address */
	(*dev->config.board_get_ether_address) (&dev->config.ether_address[0]);
	(*dev->config.board_get_ether_address) (&ia.ind_addr[0]);

	ia.cmd = IASETUP | SCB_CMD_EL;
	ia.status = 0;
	ia.next = 0;

	/* Execute the Individual Address command */
	in82596a_wait_scb_cmd_zero(dev);
	lwsp->scb.cmd = SCB_CU_START;
	lwsp->scb.CBL_addr = (unsigned long *) swap32((unsigned long *) &ia);
	in82596a_ca_and_wait(dev);
	in82596a_eat_time(DELAY);
	in82596a_ack_cmd(dev);

	/* Wait for the command to complete */
	stat = TRUE;
	while ( !(ia.status & STATUS_COMMAND_COMPLETE) )
	{
		if ( !(ia.status & STATUS_OK) )
		{
			stat = FALSE;
			break;
		}
	}

	return(stat);
}

/*************************************************************************
**  Name:   in82596a_ca_and_wait
**
**  Calling Sequence:
**
**   in82596a_ca_and_wait();
**
**  Interface Variables:
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function issues a Channel Attention to the 82596.  Issued
**   after changing the SCB.
**
**   The function will wait for any pending command to be executed.
**
*/
int in82596a_ca_and_wait(ether_1_dev_desc_t * dev)
{
	in82596a_reg_map_t *i82596CA_Regs_ptr = (in82596a_reg_map_t *) dev->map;

	/* Write to the Channel Attention register */
	REG_WRITE_L(dev, i82596CA_Regs_ptr->ca, 1);

	/* wait for command to be executed */
	in82596a_wait_scb_cmd_zero(dev);	/* added later */

	/* And return */
	return(0);
}

/*************************************************************************
**  Name:   in82596a_ru_start
**
**  Calling Sequence:
**
**   in82596a_ru_start();
**
**  Interface Variables:
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function performs the initial start of the Receive Unit.
**
**   NOTE:  This function should only be called after both hwl_tnx_dev0_init()
**   and hwl_tnx_dev0_cdtinit() have been called.  This will insure that the
**   Receive Frame Area has been correctly set up and that the kernel has
**   given us our required mbufs.
**
**/
int in82596a_ru_start(ether_1_dev_desc_t * dev)
{
	/* Initialize and execute the RU start command */

	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	in82596a_wait_scb_cmd_zero(dev);
	lwsp->scb.cmd = SCB_RU_START;
	lwsp->scb.RFA_addr = (unsigned long *) (swap32(lwsp->begin_rfd));
	in82596a_ca_and_wait(dev);
	return(0);
}

int in82596a_ru_suspend(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	int prev_sr;
	prev_sr = logio_interrupt_disable();
	in82596a_wait_scb_cmd_zero(dev);
	lwsp->scb.cmd = SCB_RU_SUSPEND;
	in82596a_ca(dev);
	logio_interrupt_enable(prev_sr);
	return(0);
}

void in82596a_restart_cu(ether_1_dev_desc_t * dev, TFD * tfd)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;

/*	It's a caller's responsibility to make sure that this function
	is called when the CU is not active. 
	in82596a_wait_scb_cmd_zero(dev);
*/

	lwsp->scb.cmd = SCB_CU_START;
	lwsp->scb.CBL_addr = (unsigned long *) swap32((unsigned long) tfd);
	in82596a_ca(dev);		/* start the command unit */
}

/*************************************************************************
**  Name:   in82596a_soak_up
**
**  Calling Sequence:
**
**   in82596a_soak_up();
**
**  Interface Variables:
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function works as an interrupt handler before we are fully up
**   and running.
**
**/
int in82596a_soak_up(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;

	/* Wait for the previous command to be executed */
	in82596a_wait_scb_cmd_zero(dev);

	/* Acknowledge all reasons for interruptions */

	lwsp->scb.cmd = lwsp->scb.status & SCB_ST;
	in82596a_ca_and_wait(dev);
	in82596a_wait_scb_cmd_zero(dev);
	return(0);
}

/*************************************************************************
**  Name:   in82596a_ack_cmd
**
**  Calling Sequence:
**
**   in82596a_ack_cmd();
**
**  Interface Variables:
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function works as an interrupt handler and should be used until the
**   real ISR has been installed.
**
**   The following comment explains the mechanism of acknowledgment of a
**   command by the 82596 chip.
**
**   Once the CPU issues CA to the 82596 it executes the following sequence:
**
**    1. Accept the START_CU command by clearing the scb.cuc
**    2. Execute the scb.CBL_addr.
**    3. Update the scb.status.
**    4. Generate an interrupt.
**
**   The CPU should then:
**
**    1. Check that scb.cuc & scb.ruc are zero
**    2. Set scb.ack
**    3. Issue CA
**/
int in82596a_ack_cmd(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;

	/* Wait for the command to complete */
	in82596a_wait_scb_cmd_zero(dev);

	/* Check the status */
	if ( lwsp->scb.status & SCB_ST )
	{
		/* Set the ack */
		lwsp->scb.cmd = lwsp->scb.status & SCB_ST;

		/* Issue the Channel Attention */
		in82596a_ca_and_wait(dev);
		while ( lwsp->scb.cmd & SCB_CMD_ACK )
			;
	}
	return(0);
}

/*************************************************************************
**  Name:   in82596a_wait_scb
**
**  Calling Sequence:
**
**   in82596a_wait_scb();
**
**  Interface Variables:
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function waits until scb.status shows that the command has
**   been accepted.
**
**/
int in82596a_wait_scb(ether_1_dev_desc_t * dev)
{
	struct comws volatile *lwsp = &((in82596a_specific_t *)
									dev->config.specific)->lanws;
	unsigned long wait;		/* Timer/counter */

	/* Wait for the scb.status to show command acceptance */
	wait = 100000;
	while ( (!(lwsp->scb.status & SCB_ST)) && wait )
	{
		wait--;
	}
	return(0);
}

/*************************************************************************
**  Name:   in82596a_eat_time
**
**  Calling Sequence:
**
**   in82596a_eat_time(count);
**
**  Interface Variables:
**
**   count      Software timing loop counter.
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function performs a software timing loop.
**
**   count == # of times to perform the loop
**
**/
int in82596a_eat_time(int count)
{
	long in82596a_counterread;
	int i;			/* Loop counter */
	long temp;			/* Temporary storage variable */

	/* Perform the software timing loop */
	for ( i = 0; i < count; i++ )
		temp = in82596a_counterread;

	return(0);
}

/*************************************************************************
**  Name:   local_bzero (it is macro, in82596a_zero for motorola targets
**          This routine is not used for intel targets
**
**  Interface Variables:
**
**   p      Ptr to data area to be zeroed out
**   len      Number of bytes to be zeroed out
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function zeros out a block out memory.
**
**/
int local_bzero(p, len)
unsigned char *p;		/* Ptr to memory to be zeroed out */
int len;			/* Number of bytes to be zeroed out */
{
	register unsigned long *lp;	/* Long word memory pointer */

	/* Initialize this routine, set up for long word operations */
	if ( ((int) p & 1) && (len > 0) )
	{
		*p++ = (unsigned char) 0;
		len--;
	}
	if ( ((int) p & 2) && (len > 1) )
	{
		*p++ = (unsigned char) 0;
		*p++ = (unsigned char) 0;
		len -= 2;
	}
	/* Zero out the memory as requested using long word transfers */
	lp = (unsigned long *) p;
	while ( len > 3 )
	{
		*lp++ = (unsigned long) 0;
		len -= 4;
	}

	/* Use byte transfers for any remaining data */
	if ( len > 0 )
	{
		p = (unsigned char *) lp;
		for ( ; len > 0; len-- )
			*p++ = (unsigned char) 0;
	}
	return(0);
}

/*************************************************************************
**  Name:   in82596a_write
**
**  Calling Sequence:
**
**   in82596a_write(value);
**
**  Interface Variables:
**
**   value      Data to be written to the 82596 command word
**
**  Global Variables:
**
**  Calls:
**
**  Description:
**
**   This function writes a value out to the upper and lower command
**   words of the 82596 register.
**
**/
int in82596a_write(ether_1_dev_desc_t * dev, unsigned long value)
{
	in82596a_reg_map_t *i82596CA_Regs_ptr = (in82596a_reg_map_t *) dev->map;

#if !defined(I960)
	/* Write the upper command word */
	REG_WRITE_W(dev, i82596CA_Regs_ptr->ucw, (unsigned short)(value & 0xFFFF));
#else
	/* Write the lower command word */
	REG_WRITE_W(dev, i82596CA_Regs_ptr->lcw, (unsigned short)(value & 0xFFFF));
#endif

	/* Wait a little while */
	in82596a_eat_time(DELAY);

#if !defined(I960)
	/* Write the lower command word */
	REG_WRITE_W(dev, i82596CA_Regs_ptr->lcw, (unsigned short)(value >> 16));
#else
	/* Write the upper command word */
	REG_WRITE_W(dev, i82596CA_Regs_ptr->ucw, (unsigned short)(value >> 16));
#endif

	/* Wait a little while */
	in82596a_eat_time(DELAY);
	return(0);
}
