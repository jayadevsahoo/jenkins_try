
#ifndef ExhPressureSensorTest_HH
#define ExhPressureSensorTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: ExhPressureSensorTest - BD Safety Net Test for the detection
//                            of exhalation pressure sensor reading OOR
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/ExhPressureSensorTest.hhv   10.7   08/17/07 09:36:40   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  by    Date:  20-Sep-1996    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "SafetyNetTest.hh"

//@ Usage-Classes
#include "Background.hh"
//@ End-Usage

class ExhPressureSensorTest : public SafetyNetTest
{
    public:

        ExhPressureSensorTest( void );
        ~ExhPressureSensorTest( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL,
			       const char*       pPredicate = NULL );

        virtual BkEventName checkCriteria( void );
  
    protected:

    private:

        // these methods are purposely declared, but not implemented...
        ExhPressureSensorTest( const ExhPressureSensorTest& ); // not implemented...
        void operator=( const ExhPressureSensorTest& );        // not implemented...
};


#endif // ExhPressureSensorTest_HH 
