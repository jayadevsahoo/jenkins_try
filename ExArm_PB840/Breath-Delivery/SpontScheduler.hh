#ifndef SpontScheduler_HH
#define SpontScheduler_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SpontScheduler - the active BreathPhaseScheduler when the 
//  current mode is SPONT.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SpontScheduler.hhv   25.0.4.0   19 Nov 2013 14:00:10   pvcs  $
//@ Modification-Log
//
//  Revision: 009   By:   gdc    Date: 01-May-2009     SCR Number: 6488
//  Project:  840S
//  Description:
//		Fixed SIMV-SPONT-SIMV transition problem caused by using mode 
// 		setting value from phased-in context before the mode setting
// 		was phased in by an inspiration.
//
//  Revision: 008   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By:  yyy    Date: 06-Jan-1998     DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//          Initial release for Bilevel.
//
//  Revision: 005  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 004 By:  iv   Date:   11-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//		       Removed the Boolean noInspiration argument from newExhalation_().
//
//  Revision: 003 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002  By:  kam   Date:  27-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "BreathPhaseScheduler.hh"
#include "BreathType.hh"

//@ Usage-Classes
class Trigger;
class BreathPhase;
class BreathTrigger;
//@ End-Usage

class SpontScheduler : public BreathPhaseScheduler
{
	public:
		SpontScheduler(void);
		virtual ~SpontScheduler(void);

		static void SoftFault(const SoftFaultID softFaultID,
							  const Uint32      lineNumber,
							  const char*       pFileName  = NULL, 
							  const char*       pPredicate = NULL);

		virtual void determineBreathPhase(const BreathTrigger& breathTrigger);
		virtual void relinquishControl(const ModeTrigger& modeTrigger);
		virtual void takeControl(const BreathPhaseScheduler& scheduler);

	protected:
		virtual EventData::EventStatus reportEventStatus_(
														 const EventData::EventId id,
														 const Boolean eventStatus);
		virtual void settingChangeHappened_(const SettingId::SettingIdType id);
		virtual void enableTriggers_(void);

	private:
		SpontScheduler(const SpontScheduler&);	 // Declared but not implemented
		void operator=(const SpontScheduler&);	 // Declared but not implemented

		void startInspiration_(const BreathTrigger& breathTrigger,
							   BreathPhase* pBreathPhase, BreathType breathType);
		void startExhalation_(const BreathTrigger& breathTrigger,
							  BreathPhase* pBreathPhase,
							  const Boolean noInspiration = FALSE);

		BreathPhase*  determineSpontBreathPhase_(
												const Trigger::TriggerId triggerId,
												const Boolean            isPeepRecovery,
												const DiscreteValue      spontTypeValue,
												DiscreteValue&           rMandTypeValue,
												const Boolean            isReadyToActivate
												);

		BreathPhase*  pPrevSpontPhase_;
		BreathPhase*  pLostSpontPhase_;

		//@ Data-Member: spontModeSetting_
		// the Mode setting that caused this scheduler to run (CPAP or SPONT)
		DiscreteValue spontModeSetting_;
};


#endif // SpontScheduler_HH 
