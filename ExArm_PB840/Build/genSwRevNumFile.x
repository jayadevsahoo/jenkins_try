#!/usr/bin/csh -f
#
# vim:ts=8:sw=2:
###############################################################################
# NAME:  genSwRevNumFile
#
#  This takes the current directory name (e.g., 'PN_4_070000_85_A' or
#  'groupsrc'), and generates a software revision file.  The resulting file
#  is a result of this script applying a substitution within 'SoftwareRevNum.C'
#  and creating 'SoftwareRevNum.cc'.
#
#  ASSUMPTION:  it is a current requirement of this development environment
#               that the directory name is equivalent to the version of
#               the files that are contained within the directory.  This
#               script is implemented expecting this to be true.
#
###############################################################################
#
# Version
#    $Header:   /840/Baseline/Build/vcssrc/genSwRevNumFile.x_v   25.0.4.0   19 Nov 2013 14:01:42   pvcs  $
#
# Modification-Log
#
#  Revision: 004  By: sah    Date:  07-Apr-2000    DR Number: 5700
#  Project:  NeoMode
#  Description:
#       Added setting of new, "Software Release Type" field, based on
#       current directory.
#
#  Revision: 003  By: sah    Date:  03-Dec-1998    DR Number: <NONE>
#  Project:  Sigma (R8027)
#  Description:
#	Added support for 'Btree-Build' revisions.
#
#  Revision: 002  By: sah    Date:  05-May-1998    DR Number: 5081
#  Project:  Sigma (R8027)
#  Description:
#	Added project name to DEVELOPMENT revisions.
#
#  Revision: 001  By: sah    Date:  17-Dec-1997    DR Number: None
#  Project:  Sigma (R8027)
#  Description:
#	Initial version (Integration baseline).
#
###############################################################################

set USAGE="usage:  ${0}"

if ($#argv != 0) then
  echo "$USAGE"
  exit 1
endif


set CURR_DIR_NAME=${cwd:t}
set SUBSYS_DIR=${cwd:h}
set SUBSYS_NAME=${SUBSYS_DIR:t}

# filter out 'PN_' and replace all '_' with '-' (e.g., 'PN_4_070000_85_A'
# becomes '4-070000-85-A', 'NeoMode_000_r4_0' become 'NeoMode-000-r4-0')...
set VERSION=`echo ${CURR_DIR_NAME} | sed -e s/PN_4/4/g -e s/_/-/g`


echo ${CURR_DIR_NAME} | fgrep PN_4_0 >& /dev/null
if ($status != 0) then
  # current directory name does NOT have a part number ("PN_4_0") prefix...
  if (${CURR_DIR_NAME} == groupsrc) then
    set PROJ_DIR=${SUBSYS_DIR:h}
    set PROJ_NAME=${PROJ_DIR:t}

    set REVISION="${PROJ_NAME} @ `date '+%H:%M:%S %d-%h-%y'`"

    if ("${USER}" != "pvcs") then
      # prepend the user name...
      set REVISION="${USER}-${REVISION}"
    endif
  else
    # use filtered version for revision...
    set REVISION=$VERSION
  endif

  # since no part number prefix found, must be a "DEVELOPMENT" release...
  set RELEASE_TYPE=DEVELOPMENT
else
  # current directory name starts with a part number ("PN_4_0") prefix...
  echo ${CURR_DIR_NAME} | fgrep _85_X0 >& /dev/null
  if ($status != 0) then
    # current directory name does NOT have a DCO ("_85_X0") postfix; must
    # be official part number...
    set RELEASE_TYPE=PRODUCTION
  else
    # current directory name does have a DCO ("_85_X0") postfix; must
    # be DCO part number...
    set RELEASE_TYPE=PRE_PRODUCTION
  endif

  # use filtered version for revision...
  set REVISION=$VERSION
endif


if ("${SUBSYS_NAME}" == "Btree-Build") then
  set REVISION="${REVISION} (Btree)"
endif


if (! -f SoftwareRevNum.C) then
  get -v$VERSION SoftwareRevNum.C >& /dev/null
endif


cat SoftwareRevNum.C | sed -e "s/<REV_NUM>/${REVISION}/g" -e "s/<REL_TYPE>/${RELEASE_TYPE}/g" >! SoftwareRevNum.cc

exit 0
