#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: GuiStart - GUI State: Start.
//---------------------------------------------------------------------
//@ Interface-Description
//  The GuiStart class is a GuiStart used by the GuiFsm finite
//  state machine.  The class represents the entry state for the
//  GUI FSM in which the applications tasks are created and the GUI
//  waits for a BdReadyMessage from BD Task Control.  This class
//  contains methods for processing events while in this state.
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the GuiStart state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  GuiStart is derived from the FsmState class.  It overrides the
//  virtual method receiveBdReady and receiveStartupTimer to 
//  provide state specific behavior when it receives a 
//  BdReadyMessage or StartupTimer.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiStart.ccv   25.0.4.0   19 Nov 2013 14:33:54   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "GuiStart.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "BdReadyMessage.hh"
#include "GuiFsm.hh"
#include "GuiInop.hh"
#include "GuiOnline.hh"
#include "GuiService.hh"
#include "GuiOnlineInit.hh"
#include "GuiServiceInit.hh"
#include "GuiTimeout.hh"
#include "InitConditions.hh"
#include "PostAgent.hh"
#include "StartupTimer.hh"
#include "SyncClockTimer.hh"
#include "TaskAgentManager.hh"
#include "TaskControlAgent.hh"
#include "TransitionTimer.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiStart [default constructor]
//
//@ Interface-Description
//  GuiStart class default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiStart::GuiStart()
{
    CALL_TRACE("GuiStart()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiStart [destructor]
//
//@ Interface-Description
//  GuiStart class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiStart::~GuiStart()
{
    CALL_TRACE("~GuiStart()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

const char*
GuiStart::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "GuiStart";
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with
//  this GuiStart state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the ONLINE state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SigmaState
GuiStart::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_START;
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  The activate method is the entry method for the GuiStart class.  It
//  starts the GUI application tasks and sets the startup timer to 
//  force a transition to the TIMEOUT state if it doesn't receive
//  a BdReadyMessage.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method messages the TaskAgentManager to start the GUI 
//  application tasks.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
GuiStart::activate(void) const
{
    CALL_TRACE("activate(void)");
    TaskAgentManager::CreateTasks();
    StartupTimer::GetTimer().set();
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveBdReady
//
//@ Interface-Description
//  The receiveBdReady method processes a BdReadyMessage received
//  from BD task control by GUI task control in the GuiStart state.
//  It updates the local BD state from the message and forwards the
//  message to the application tasks.  For a BdReadyMessage with
//  service mode requested, this method transitions to the SERVICE-INIT
//  state.  If service mode has not been requested and yet service is 
//  required for either BD or GUI, or if TUV has latched the BD in 
//  the FAILED state, this method transitions to the INOP state.  
//  Otherwise, it transitions to the ONLINE-INIT state in preparation
//  for a transition to the ONLINE state.  For transitions to either
//  SERVICE-INIT or ONLINE-INIT, the system real-time clocks are 
//  synchronized by sending a SyncClockMessage to the BD.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses FsmState::SetBdState to set the local state of the BD to the
//  state information contained in the BdReadyMessage.
//  TaskAgentManager::ReportChangeState sends a ChangeStateMessage to
//  the application tasks.  TaskAgentManager::Send forwards the
//  BdReadyMessage to the applications.  The virtual method FsmState::
//  startTransition() initiates the state transitions.
//
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
GuiStart::receiveBdReady(const BdReadyMessage& message) const
{
    CALL_TRACE("receiveBdReady(const BdReadyMessage&)");

    StartupTimer::GetTimer().cancel();

    FsmState::SetBdState(message);

    if ( message.isServiceModeRequested() ) 
    {
        // $[TI1]
        TaskAgentManager::Send(message);
        GuiFsm::GetGuiServiceInitState().startTransition();
        TaskControlAgent::SyncClocks();
        SyncClockTimer::GetTimer().set();
    }
    else if (   FsmState::GetBdState() == STATE_FAILED )
    {
        // $[TI4]
        GuiFsm::GetGuiInopState().startTransition();
    }
    else if ( InitConditions::IsServiceRequired() )
    { 
        // $[TI2]
        TaskAgentManager::Send(message);
        GuiFsm::GetGuiInopState().startTransition();
    } 
    else
    {
        // $[TI3]
        TaskAgentManager::Send(message);
        GuiFsm::GetGuiOnlineInitState().startTransition();
        TaskControlAgent::SyncClocks();
        SyncClockTimer::GetTimer().set();
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveStartupTimeout
//
//@ Interface-Description
//  The receiveStarupTimeout method receives a StartupTimer
//  while in the GuiStart state.  This message indicates the BD has
//  failed to communicate within the required time period after 
//  entering the START state.  This method initiates a state transition
//  to the TIMEOUT state.  However, if TUV has declared the BD failed
//  then it transitions to the INOP state instead.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Starts the state transition using virtual FsmState::startTransition()
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
GuiStart::receiveStartupTimer(const StartupTimer& ) const
{
    CALL_TRACE("receiveStartupTimer(const StartupTimer&)");
    if ( FsmState::GetBdState() == STATE_FAILED )  
    {
        // $[TI1]
        GuiFsm::GetGuiInopState().startTransition();
    }
    else
    {
        // $[TI2]
        GuiFsm::GetGuiTimeoutState().startTransition();
    }
}
 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
GuiStart::SoftFault(const SoftFaultID  softFaultID,
                                  const Uint32       lineNumber,
                                  const char*        pFileName,
                                  const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_GUISTART,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================


