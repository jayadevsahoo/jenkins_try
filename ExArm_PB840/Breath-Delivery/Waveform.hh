
#ifndef Waveform_HH
#define Waveform_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: Waveform - Waveform data management.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Waveform.hhv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  sp    Date:  29-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection Rework.
//
//  Revision: 004  By:  sp    Date:  17-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date:  7-Feb-1994    DR Number: 704, 705 
//       Project:  Sigma (R8027)
//       Description:
//             Add task locking mechanism (704).
//             Add WAVEFORM_MSG for task monitoring (705).
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "WaveformSet.hh"

#include "BD_IO_Devices.hh"


//@ End-Usage

//@ Constant: WAVEFORM_TIME_CYCLE
// Waveform record is being inserted at 20 msec interval
static const Int32 WAVEFORM_TIME_CYCLE = 20;


class Waveform {
  public:
    Waveform(void);
    ~Waveform(void);

    static void SoftFault(const SoftFaultID softFaultID,
  const Uint32      lineNumber,
  const char*       pFileName  = NULL, 
  const char*       pPredicate = NULL);

    void newCycle(const WaveformRecord& waverec);
    void initialize(void);
    inline void synchronize(void);
    inline WaveformSet* getWaveformSetToSend(void);
    inline WaveformSet* getWaveformSetToWrite(void);
  
    //@ Data-Member:  WaveformTaskLockFlag;
    // If this flag is set to true when the waveform task is busy.
    static Boolean WaveformTaskLockFlag;

  protected:

  private:
    Waveform(const Waveform&);		// not implemented...
    void   operator=(const Waveform&);	// not implemented...


    //@ Data-Member:  count_
    // counter for 20 ms
    Int32 count_;

    //@ Data-Member:  waveformSet_
    // two waveform set to form double buffer
    WaveformSet  waveformSet_[2];

    //@ Data-Member:  writeIndex_
    // index to the waveform set to be filled
    Int32  writeIndex_;

    //@ Data-Member:  sendIndex_
    // index to the waveform set to be sent to GUI
    Int32  sendIndex_;


};


// Inlined methods...
#include "Waveform.in"


#endif // Waveform_HH 
