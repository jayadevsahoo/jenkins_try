#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: WaveformDataBuffer - A data buffer used for storing a stream
// of waveform data with enough data storage so as to display a full
// waveform on the Waveforms subscreen.
//---------------------------------------------------------------------
//@ Interface-Description
// WaveformDataBuffer is used as a repository for waveform data, typically
// either live waveform data (data that has just been measured from the
// patient) or frozen waveform data (old data that the operator wants to
// review).
//
// Access to the data is available only via an iterator.  See the
// WaveformIntervalIter class for more information.
//
// There are two methods supplied: addSample() adds a new sample to the
// buffer.  If the buffer is full the new sample is added and the oldest
// sampled removed from the buffer.  The assignment operator is also
// supplied for copying the contents of one data buffer to another.
//---------------------------------------------------------------------
//@ Rationale
// A repository for waveform data was needed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The data buffer itself is a fixed length array of WaveformData samples.
// As samples are added to the array an account is kept of the first
// and last samples in the array.  Once the array is full new samples begin
// to overwrite the oldest samples in the array.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformDataBuffer.ccv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  02-Jun-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  modified to use WaveformData's new assignment operator so that
//         future expansion of that struct's contents doesn't require a
//         change of this file
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//=====================================================================

#include "WaveformDataBuffer.hh"
#include <string.h>

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaveformDataBuffer()  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply initializes all data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformDataBuffer::WaveformDataBuffer(void) :
				lastSample_(WDB_INVALID_INDEX_),
				firstSample_(WDB_INVALID_INDEX_),
				wrappedAround_(FALSE)
{
	CALL_TRACE("WaveformDataBuffer::WaveformDataBuffer(void)");
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~WaveformDataBuffer  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformDataBuffer::~WaveformDataBuffer(void)
{
	CALL_TRACE("WaveformDataBuffer::WaveformDataBuffer(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addSample
//
//@ Interface-Description
// Adds a sample to the buffer.  Passed a pointer to the waveform data.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must first figure out where to put the new sample.  If it is the first
// sample then it goes in at index 0 otherwise it goes in at the next sample
// after lastSample_.  If the buffer has filled up then we must move
// firstSample_ to the sample beyond the new lastSample_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformDataBuffer::addSample(WaveformData *pWaveformData)
{
	CALL_TRACE("WaveformDataBuffer::addSample(WaveformData *pWaveformData)");

	// Move lastSample_ to next valid index
	if (lastSample_ == WDB_INVALID_INDEX_)
	{													// $[TI1]
		// This is the first point added
		firstSample_ = 0;
		lastSample_ = 0;
	}
	else
	{													// $[TI2]
		// Point to the next value
		lastSample_++;

		// Check if we've wrapped around and reset to 0 if so.
		if (lastSample_ >= NUM_WAVEFORM_SAMPLES)
		{												// $[TI3]
			lastSample_ = 0;
			wrappedAround_ = TRUE;
		}												// $[TI4]

		// If we've wrapped around move firstSample_ to the point beyond
		// lastSample_.
		if (wrappedAround_)
		{												// $[TI5]
			firstSample_ = lastSample_ + 1;
			if (firstSample_ >= NUM_WAVEFORM_SAMPLES)
			{											// $[TI6]
				firstSample_ = 0;
			}											// $[TI7]
		}												// $[TI8]
	}

	// Store the new data
	samples_[lastSample_] = *pWaveformData;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
// Clears the entire buffer of data.
//---------------------------------------------------------------------
//@ Implementation-Description
// Reset the firstSample_, lastSample_ and wrappedAround_ variables to
// indicate that the buffer is empty.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformDataBuffer::clear(void)
{
	CALL_TRACE("WaveformDataBuffer::clear(void)");

	// Reset indicators
	firstSample_   = WDB_INVALID_INDEX_;
	lastSample_    = WDB_INVALID_INDEX_;
	wrappedAround_ = FALSE;								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
// Assignment operator.
//---------------------------------------------------------------------
//@ Implementation-Description
// Copy the data members.  Use a memcpy() call to copy the waveform
// data samples as that's fastest.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
WaveformDataBuffer::operator=(const WaveformDataBuffer& waveformDataBuffer)
{
	CALL_TRACE("WaveformDataBuffer::operator=(waveformDataBuffer)");

	// Store the first and last points (the first and current points of
	// points_[]).
	firstSample_   = waveformDataBuffer.firstSample_;
	lastSample_	   = waveformDataBuffer.lastSample_;
	wrappedAround_ = waveformDataBuffer.wrappedAround_;

	// Block copy the points_[] values.
	memcpy((char *)samples_, (char *)waveformDataBuffer.samples_,
															sizeof(samples_));
														// $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
WaveformDataBuffer::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, WAVEFORMDATABUFFER,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
