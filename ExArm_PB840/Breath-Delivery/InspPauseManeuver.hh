 
#ifndef InspPauseManeuver_HH
#define InspPauseManeuver_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: InspPauseManeuver - implements the handling of inspiratory pause
//  maneuver.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/InspPauseManeuver.hhv   25.0.4.0   19 Nov 2013 13:59:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: gdc   Date:  29-Nov-2006   SCR Number: 6319
//  Project:  RESPM
//  Description:
//		Removed private data member clearRequested_ to use protected
//      data member with same name in Maneuver base class.
//		
//  Revision: 004   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Refactored code for base class modifications to accomodate RM 
//      maneuvers. Moved pressure stability buffer to its own class
//      that is now a member of this class. New method processUserRequest()
//      implements functionality of old Maneuver::DetermineManeuverStatus()
//      method in each maneuver class.
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  syw    Date:  20-Oct-1998    DR Number: 5223
//       Project:  BiLevel
//       Description:
//			Added updateState_() method.
//
//  Revision: 001  By:  iv    Date:  27-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version - per Breath Delivery formal code review 
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "EventData.hh"
#include "Maneuver.hh"
#include "TimerTarget.hh"
#include "LinearRegression.hh"
#include "PauseTypes.hh"
#include "PressureStabilityBuffer.hh"

//@ Usage-Classes

//@ End-Usage



class InspPauseManeuver :  public Maneuver, public TimerTarget
{
  public:

	//@ Type: MaxPlateauSample
	// This enum represents the sample counts needed for collecting
	// qPat, pEexh, and pEd data.
	enum MaxPlateauSample
	{
		MAX_PLATEAU_SAMPLE = 32
	};
	
	InspPauseManeuver(ManeuverId maneuverId, IntervalTimer& rPauseTimer);
    ~InspPauseManeuver(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

	// virtual from Maneuver
	virtual void activate(void);
	virtual Boolean complete(void);
	virtual void clear(void);
	virtual void terminateManeuver(void);
	virtual void newCycle(void);
	virtual Boolean processUserEvent(UiEvent& rUiEvent);

	EventData::EventStatus handleInspPause(const Boolean eventStatus);

	inline Boolean isManeuverAllowed(void);
	inline PauseTypes::ComplianceState getComplianceState(void) const;
	inline PauseTypes::ComplianceState getResistanceState(void) const;
	inline Real32 getComplianceValue(void) const;
	inline Real32 getResistanceValue(void) const;
	
	// virtual from TimerTarget
	virtual void timeUpHappened(const IntervalTimer& timer);

  protected:

  private:
    InspPauseManeuver(const InspPauseManeuver&);		// not implemented...
    void   operator=(const InspPauseManeuver&);	// not implemented...
	InspPauseManeuver(void);

	void calculateComplianceAndResistance_(void);
	PauseTypes::ComplianceState calculateCstat_(void);

	inline PauseTypes::ComplianceState updateState_( const PauseTypes::ComplianceState presentState,
													const PauseTypes::ComplianceState newState ) ;
	
#ifdef BTREE_UT
	void calculateComplianceAndResistanceUT_(const Boolean testTopSection, Real32& pedSlope);
	void calculateCstatUT_(const Boolean testTopSection);
#endif

	//@ Type: PauseTimerId
	// This enum represents the state of Inspiratory pause.
	// qPat, pEexh, and pEd.
  	enum PauseTimerId
  	{
		NULL_TIME,
  		MAX_MANUAL_TIME,
  		MAX_PENDING_TIME,
  		MAX_PLATEAU_TIME
  	};
  
	//@ Data-Member: &rPauseTimer_
	// a reference to the server IntervalTimer
	IntervalTimer& rPauseTimer_;

    //@ Data-Member: pauseTime_
    // pause timer count
    Int32 pauseTime_;
    
    //@ Data-Member: pauseTimeId_
    // pause timer Id.
    PauseTimerId pauseTimeId_;
    
	//@ Data-Member: timeOut_
	// Flag indicates if the timer has expired.
	Boolean timeOut_;
	
	//@ Data-Member: keyReleased_
	// Flag indicates if the user is pressing the key or not.
	Boolean keyReleased_;
	
	//@ Data-Member: audibleAnnunciatingNeeded_
	// Flag indicataes if an audio alarm is needed when plateau reaches
	// stability.
	Boolean audibleAnnunciatingNeeded_;
	
	//@ Data-Member: maneuverType_
	// Holds the current maneuver type.
	PauseTypes::ManeuverType maneuverType_;

	//@ Data-Member: pEdState_
	// The state of current end of inspiration pressure
	PauseTypes::PedState pEdState_;

	//@ Data-Member: pPlState_
	// The state of plateau pressure 
	PauseTypes::PpiState pPlState_;

	//@ Data-Member: pEd_
	// Averaged pressure at end of inspiration
	Real32 pEd_;

	//@ Data-Member: qPat_
	// Estimated patient flow
	Real32 qPat_;

	//@ Data-Member: pPl_
	// Plateau pressure
	Real32 pPl_;

	//@ Data-Member: cStat_
	// Static compliance
	Real32 cStat_;

	//@ Data-Member: complianceState_
	// Static compliance
	PauseTypes::ComplianceState complianceState_;

	//@ Data-Member: cT_
	// Breathing circuit compliance
	Real32 cT_;

	//@ Data-Member: rAw_
	// Airway resistance
	Real32 rAw_;

	//@ Data-Member: resistanceState_
	// State of airway resistance
	PauseTypes::ComplianceState resistanceState_;

	//@ Data-Member: pEdLR_
	// Array of end plateau pressures at end of inspiration
	LinearRegression pEdLR_;

	//@ Data-Member: inspFlowBuffer_
	// data buffer to collect the exhaled gas flow measuremnt
	SummationBuffer inspFlowBuffer_;

	//@ Data-Member: pEdTimer_
	// Sample time, at the rate which pressure and flow measurement
	// are sampled.
	Int32 pEdTimer_;

    //@ Data-Member: squareFlowPattenInVcMode_
    // Flag checking if we are in Vc Mode with square Flow Pattern
    Boolean squareFlowPattenInVcMode_;

	//@ Data-Member: pressureStabilityBuffer_
	// The buffer used to do stability checking.
	PressureStabilityBuffer pressureStabilityBuffer_;

};



// Inlined methods...
#include "InspPauseManeuver.in"


#endif // InspPauseManeuver_HH 
