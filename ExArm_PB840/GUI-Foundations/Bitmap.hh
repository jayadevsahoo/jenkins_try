#ifndef Bitmap_HH
#define Bitmap_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Bitmap - This is a graphical bitmap class
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/Bitmap.hhv   25.0.4.0   19 Nov 2013 14:11:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: gdc    Date:  21-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
//  
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 004   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 003  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 002  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "Drawable.hh"

//@ Usage-Classes
#include "Drawable.hh"
#include "DibInfo.hh"
//@ End-Usage

class Bitmap : public Drawable
{
public:
	Bitmap(DibInfo & rDibInfo);
	virtual ~Bitmap(void);

	void setImage(DibInfo & rDibInfo);

	// virtual from Drawable
	virtual void draw(const Area&);

	static void SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName  = NULL,
						  const char*        pPredicate = NULL);

protected:

private:
	Bitmap(void);              		// Declared, but not implemented.
	Bitmap(const Bitmap&);			// Declared, but not implemented.
	void operator=(const Bitmap&);	// Declared, but not implemented.

	//@ Data-Member: pBitmapImage_
	// Points to the bitmap image.
	DibInfo * pBitmapImage_;
};


#endif // Bitmap_HH 
