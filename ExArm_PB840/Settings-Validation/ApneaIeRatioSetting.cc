#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaIeRatioSetting - Ratio of Apnea Inspiratory Time to
//				  Apnea Expiratory Time Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the ratio between the
//  inspiratory and expiratory times during apnea ventilation.  This class
//  inherits from 'BatchBoundedSetting' and provides the specific information
//  needed for representation of apnea I:E ratio.  This information includes
//  the interval and range of this setting's values, this setting's response
//  to a change of one of its primary setting (see 'acceptPrimaryChange()'),
//  this batch setting's new-patient value (see 'getNewPatientValue()'), and
//  the dependent settings of this setting.
//
//  The value stored by this setting is based on one of the numbers of the
//  ratio, rather than the ratio itself.  That is, the number is stored
//  according to:
//>Von
//        1.00:1; when apnea Ti=apnea Te,
//        1:xxx;  where xxx = (apnea Te/apnea Ti) when apnea Ti < apnea Te,
//>Voff
//  and 1.00:1 > 1:xxx.
//
//  This class provides a static method for calculating apnea I:E ratio based
//  on the values of apnea inspiratory and apnea expiratory times.  This
//  calculation method provides a standard way for all settings (as needed)
//  to calculate an apnea I:E ratio value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateContraints_()' for updating its
//  dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaIeRatioSetting.ccv   25.0.4.0   19 Nov 2013 14:27:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  incorporated use of new 'warpToRange()' method
//      *  fixed improper implementation of the allowance of a 4.0% error
//         in the DEVELOPMENT-only 'is{Adjusted,Accepted}Value()' methods
//
//  Revision: 007   By: sah   Date:  12-Mar-1999    DR Number: 5310
//  Project:  ATC
//  Description:
//     Added missing '$' to requirement mapping.
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  added 'acceptTransition()' method due to new Transition Rule
//	   implementation
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 005   By: dosman    Date:  11-Nov-1998    DR Number: 5252
//  Project:  BILEVEL
//  Description:
//	Added SRS reqt #.
//
//  Revision: 004   By: dosman    Date:  24-Jun-1998    DR Number: BILEVEL 66
//  Project:  BILEVEL
//  Description:
//      The intermediate value of inspiratory time computed in
//      calcBasedOnSetting_ was changed to be warped in the same
//      direction as the direction the "setting being calculated".
//      Also, special processing was added in BoundedSetting::calcNewValue()
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	Intermediate calculations of NON-CONSTRAINT values of this setting
//	no longer need to be "warped" to a resolution value, therefore replace
//	'warpValue()' with the new 'getValuePrecision()' method, and
//	remove the 'warpDirection' parameter from the calculation
//	method(s).  Also, update the two DEVELOPMENT-only methods for
//	testing the validity of the adjusted/accepted values to use
//	'IsEquivalent()'.  NOTE:  the calculated breath period in
//	'updateConstraints_()' needs to be "warped" only for determining
//	if the breath period is "odd".
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaIeRatioSetting.hh"
#include "SettingConstants.hh"
#include "FlowPatternValue.hh"
#include "MandTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "ContextId.hh"
#include "ApneaRespRateSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02057] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
  {
    SettingId::APNEA_EXP_TIME,
    SettingId::APNEA_INSP_TIME,

    SettingId::NULL_SETTING_ID
  };


// $[02053] The setting's range ...
// $[02055] The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    -10000.0f,		// absolute minimum...
    0.0f,		// unused...
    ONES,		// unused...
    NULL
  };
static const BoundedInterval  NODE3_ =
  {
    -100.0f,		// change-point...
    1.0f,		// resolution...
    ONES,		// precision...
    &::LAST_NODE_	// next node...
  };
static const BoundedInterval  NODE2_ =
  {
    -10.0f,		// change-point...
    0.1f,		// resolution...
    TENTHS,		// precision...
    &::NODE3_		// next node...
  };
static const BoundedInterval  NODE1_ =
  {
    -1.01f,		// change-point...
    0.01f,		// resolution...
    HUNDREDTHS,		// precision...
    &::NODE2_		// next node...
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    1.00f,		// absolute maximum...
    (1.00f - (-1.01f)),// resolution...
    HUNDREDTHS,		// precision...
    &::NODE1_		// next node...
  };



//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaIeRatioSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the method initializes
//  it's value interval range.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaIeRatioSetting::ApneaIeRatioSetting(void)
        : BatchBoundedSetting(SettingId::APNEA_IE_RATIO,
			      ::ARR_DEPENDENT_SETTING_IDS_,
			      &::INTERVAL_LIST_,
			      APNEA_IE_RATIO_MAX_ID,	// maxConstraintId...
			      NULL_SETTING_BOUND_ID,	// minConstraintId...
			      FALSE)			// no epsilon...
{
  CALL_TRACE("ApneaIeRatioSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaIeRatioSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaIeRatioSetting::~ApneaIeRatioSetting(void)
{
  CALL_TRACE("~ApneaIeRatioSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaIeRatioSetting::getApplicability(
			      const Notification::ChangeQualifier qualifierId
				     ) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  // only viewable, because Apnea Constant Parm is fixed at
  // INSP_TIME_CONSTANT...
  return(Applicability::VIEWABLE);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02054] -- this setting's new patient value
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaIeRatioSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  newPatient = calcBasedOnSetting_(SettingId::APNEA_INSP_TIME,
				   BoundedRange::WARP_NEAREST,
				   BASED_ON_NEW_PATIENT_VALUES);

  return(newPatient);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: acceptPrimaryChange(primaryId)
//
//@ Interface-Description
//  One of this setting's primary settings have changed, therefore update
//  this setting's value based on the new value of the primary setting
//  -- indicated by 'primaryId'.  If this setting's bound is violated by
//  the primary setting's newly "adjusted" value, a pointer to this
//  setting's bound status is returned.  Otherwise, a value of 'NULL' is
//  returned to indicate no dependent bound was violated.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When apnea expiratory time is the primary setting, this implementation
//  depends on apnea inspiratory time being update BEFORE this method gets
//  called; this method expects the new apnea inspiratory time to be in
//  the Adjusted Context, during this type of update.
//
//  $[02002] -- new dependent settings based on proposed primary settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus*
ApneaIeRatioSetting::acceptPrimaryChange(
				  const SettingId::SettingIdType primaryId
				        )
{
  CALL_TRACE("acceptPrimaryChange(primaryId)");

  // update/reset dynamic constraints...
  updateConstraints_();

  BoundedValue  newApneaIeRatio;

  // calculate the new apnea I:E ratio from the current apnea inspiratory
  // time and apnea respiratory rate...
  newApneaIeRatio = calcBasedOnSetting_(primaryId, BoundedRange::WARP_NEAREST,
				        BASED_ON_ADJUSTED_VALUES);

  BoundStatus&  rBoundStatus = getBoundStatus_();

  // initialize to holding this setting's id...
  rBoundStatus.setViolationId(getId());

  getBoundedRange_().testValue(newApneaIeRatio, rBoundStatus);

  // store the new apnea I:E ratio value...
  setAdjustedValue(newApneaIeRatio);

  const BoundStatus*  pBoundStatus;

  // if there is a bound violation return a pointer to this setting's
  // bound status, otherwise return 'NULL'...
  pBoundStatus = (rBoundStatus.isInViolation()) ? &rBoundStatus	// $[TI1]
						: NULL;		// $[TI2]

  return(pBoundStatus);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02024]\f\ -- transitioning from apnea PC...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaIeRatioSetting::acceptTransition(const SettingId::SettingIdType,
				      const DiscreteValue,
				      const DiscreteValue)
{
  CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

  BoundedValue  newApneaIeRatio;

  newApneaIeRatio = calcBasedOnSetting_(SettingId::APNEA_INSP_TIME,
				        BoundedRange::WARP_NEAREST,
				        BASED_ON_ADJUSTED_VALUES);

  // store the transition value into the adjusted context...
  setAdjustedValue(newApneaIeRatio);

  setForcedChangeFlag();
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaIeRatioSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) !=
					    Applicability::INAPPLICABLE)
    {
      // the accepted apnea I:E ratio value is within its max and min range,
      // therefore see if the accepted apnea I:E ratio value consistent with
      // the accepted apnea inspiratory time and apnea expiratory time...
      const Real32        APNEA_IE_RATIO_VALUE =
				      BoundedValue(getAcceptedValue()).value;
      const BoundedValue  CALC_APNEA_IE_RATIO  =
			      calcBasedOnSetting_(SettingId::APNEA_INSP_TIME,
						  BoundedRange::WARP_NEAREST,
						  BASED_ON_ACCEPTED_VALUES);

      isValid = ::IsEquivalent(APNEA_IE_RATIO_VALUE,
			       CALC_APNEA_IE_RATIO.value,
			       CALC_APNEA_IE_RATIO.precision);

      if (!isValid)
      {
	// since the calculation of an apnea I:E ratio is inherently inaccurate,
	// a 4.0% error is allowed between the apnea inspiratory time-based
	// calculation and the current apnea I:E ratio values...
	const Real32  CALCULATION_ERROR =
			      ABS_VALUE((CALC_APNEA_IE_RATIO.value * 0.04));

	// calculate the difference between the actual and calculated values...
	const Real32  DIFFERENCE =
	  ABS_VALUE(ABS_VALUE(APNEA_IE_RATIO_VALUE) -
					  ABS_VALUE(CALC_APNEA_IE_RATIO.value));

	isValid = (DIFFERENCE <= CALCULATION_ERROR);
      }
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [static]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaIeRatioSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) !=
					    Applicability::INAPPLICABLE)
    {
      // the adjusted apnea I:E ratio value is within its max and min range,
      // therefore see if the adjusted apnea I:E ratio value consistent with
      // the adjusted apnea inspiratory time and apnea expiratory time...
      const Real32        APNEA_IE_RATIO_VALUE =
				      BoundedValue(getAdjustedValue()).value;
      const BoundedValue  CALC_APNEA_IE_RATIO  =
			      calcBasedOnSetting_(SettingId::APNEA_INSP_TIME,
						  BoundedRange::WARP_NEAREST,
						  BASED_ON_ADJUSTED_VALUES);

      isValid = ::IsEquivalent(APNEA_IE_RATIO_VALUE,
			       CALC_APNEA_IE_RATIO.value,
			       CALC_APNEA_IE_RATIO.precision);

      if (!isValid)
      {
	// since the calculation of an apnea I:E ratio is inherently inaccurate,
	// a 4.0% error is allowed between the apnea inspiratory time-based
	// calculation and the current apnea I:E ratio values...
	const Real32  CALCULATION_ERROR =
			      ABS_VALUE((CALC_APNEA_IE_RATIO.value * 0.04));

	// calculate the difference between the actual and calculated values...
	const Real32  DIFFERENCE =
	  ABS_VALUE(ABS_VALUE(APNEA_IE_RATIO_VALUE) -
					  ABS_VALUE(CALC_APNEA_IE_RATIO.value));

	isValid = (DIFFERENCE <= CALCULATION_ERROR);
      }

      if (!isValid)
      {
	const Setting*  pApneaInspTime = SettingsMgr::GetSettingPtr(
						    SettingId::APNEA_INSP_TIME
								   );
	const Setting*  pApneaRespRate = SettingsMgr::GetSettingPtr(
						    SettingId::APNEA_RESP_RATE
								   );
	cout << "INVALID Apnea I:E Ratio:\n";
	cout << "CALC = " << CALC_APNEA_IE_RATIO << endl;
	cout << *this << *pApneaInspTime << *pApneaRespRate << endl;
      }
    }
  }

  return(isValid);
}

#endif // defined (SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ApneaIeRatioSetting::SoftFault(const SoftFaultID  softFaultID,
			       const Uint32       lineNumber,
			       const char*        pFileName,
			       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_IE_RATIO_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaIeRatioSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  //===================================================================
  // apnea I:E ratio's minimum bound...
  //===================================================================

  BoundedRange::ConstraintInfo  maxConstraintInfo;

  if (isApneaBreathPeriodOdd_(BASED_ON_ADJUSTED_VALUES))
  {   // $[TI1] -- the breath period is an ODD value of resolution steps...
    // because of the fact that apnea I:E ratio's absolute maximum is
    // '1.00:1' ([02053]), and the breath period is ODD, the absolute maximum
    // is NOT allowed; use the "click" value just below the absolute max...
    maxConstraintInfo.value = ::INTERVAL_LIST_.pNext->value;
  }
  else
  {   // $[TI2] -- the breath period is an EVEN value of resolution steps...
    // use absolute maximum...
    maxConstraintInfo.value = getAbsoluteMaxValue_();
  }

  // initialize to apnea I:E ratio's absolute maximum constraint id...
  maxConstraintInfo.id = APNEA_IE_RATIO_MAX_ID;

  // this setting has no soft maximum bounds...
  maxConstraintInfo.isSoft = FALSE;

  // update apnea I:E ratio's maximum constraint...
  getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  ((basedOnSettingId == SettingId::APNEA_RESP_RATE  ||
//    basedOnSettingId == SettingId::APNEA_INSP_TIME)  &&
//   (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//    basedOnCategory == BASED_ON_NEW_PATIENT_VALUES))
//			||
//  ((basedOnSettingId == SettingId::APNEA_EXP_TIME  ||
//    basedOnSettingId == SettingId::APNEA_TIDAL_VOLUME  ||
//    basedOnSettingId == SettingId::APNEA_PEAK_INSP_FLOW  ||
//    basedOnSettingId == SettingId::APNEA_PLATEAU_TIME  ||
//    basedOnSettingId == SettingId::APNEA_FLOW_PATTERN)  &&
//    basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
ApneaIeRatioSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
					) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

#if defined(SIGMA_DEVELOPMENT)
  if (Settings_Validation::IsDebugOn(::APNEA_IE_RATIO_SETTING))
  {
    cout << "AIERS::cBOS bOSI==" << basedOnSettingId
       << " wD==" << warpDirection
       << " bOC==" << basedOnCategory
       << endl;
  }
#endif  // defined(SIGMA_DEVELOPMENT)

  // get the apnea breath period from the apnea respiratory rate...
  const Real32  APNEA_BREATH_PERIOD_VALUE =
		  ApneaRespRateSetting::GetApneaBreathPeriod(basedOnCategory);

  BoundedValue  apneaIeRatio;
  BoundedValue  apneaInspTime;

  Real32  apneaExpTimeValue;

  switch (basedOnSettingId)
  {
  //-------------------------------------------------------------------
  // $[02014](1) -- new apnea I:E ratio while changing apnea insp. time...
  // $[02018](1) -- new apnea I:E ratio while apnea insp. time constant...
  //-------------------------------------------------------------------
  case SettingId::APNEA_RESP_RATE :
  case SettingId::APNEA_INSP_TIME :
    {   // $[TI1] -- base apnea I:E ratio's calculation on apnea insp time...
      const Setting*  pApneaInspTime =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

      switch (basedOnCategory)
      {
      case BASED_ON_NEW_PATIENT_VALUES :	// $[TI1.1]
	// get the new-patient insp time value...
	apneaInspTime.value =
		    BoundedValue(pApneaInspTime->getNewPatientValue()).value;
	break;
      case BASED_ON_ADJUSTED_VALUES :		// $[TI1.2]
	// get the "adjusted" insp time value...
	apneaInspTime.value =
		      BoundedValue(pApneaInspTime->getAdjustedValue()).value;
	break;
      case BASED_ON_ACCEPTED_VALUES :
	// fall through to 'default:' when not in DEVELOPMENT mode...
#if defined(SIGMA_DEVELOPMENT)
	// get the "accepted" insp time value...
	apneaInspTime.value =
		      BoundedValue(pApneaInspTime->getAcceptedValue()).value;
	break;
#endif // defined(SIGMA_DEVELOPMENT)
      default :
	// unexpected 'basedOnCategory' value..
	AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
	break;
      };

      // calculate an apnea expiratory time, based on the "adjusted" apnea 
      // inspiratory time...
      apneaExpTimeValue = APNEA_BREATH_PERIOD_VALUE - apneaInspTime.value;
    }
    break;

  //-------------------------------------------------------------------
  // $[02012](1) -- new apnea I:E ratio while changing apnea exp. time...
  //-------------------------------------------------------------------
  case SettingId::APNEA_EXP_TIME :
    {   // $[TI2] -- base apnea I:E ratio's calculation on apnea exp time...
      // apnea I:E ratio is to base its value on the "adjusted" apnea
      // expiratory time value, only...
      AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
      			  basedOnCategory);

      const Setting*  pApneaExpTime =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME);

      // get the apnea expiratory time value from the Adjusted Context...
      apneaExpTimeValue = BoundedValue(pApneaExpTime->getAdjustedValue()).value;

      // calculate an apnea inspiratory time, based on the "adjusted" apnea
      // expiratory time...
      apneaInspTime.value = APNEA_BREATH_PERIOD_VALUE - apneaExpTimeValue;
    }
    break;

  //-------------------------------------------------------------------
  // $[02019](3) -- new apnea I:E ratio with new VCV parameter...
  //-------------------------------------------------------------------
  case SettingId::APNEA_TIDAL_VOLUME :
  case SettingId::APNEA_PEAK_INSP_FLOW :
  case SettingId::APNEA_PLATEAU_TIME :
  case SettingId::APNEA_FLOW_PATTERN :
    {   // $[TI3] -- base apnea I:E ratio's calculation on the apnea VCV
	//           settings...
      // apnea I:E ratio is to base its value on the "adjusted" VCV
      // settings, only...
      AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
			  basedOnCategory);

      // get pointers to each of the apnea VCV parameters...
      const Setting*  pApneaFlowPattern =
		 SettingsMgr::GetSettingPtr(SettingId::APNEA_FLOW_PATTERN);
      const Setting*  pApneaPeakInspFlow =
		SettingsMgr::GetSettingPtr(SettingId::APNEA_PEAK_INSP_FLOW);
      const Setting*  pApneaPlateauTime =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_PLATEAU_TIME);
      const Setting*  pApneaTidalVolume =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_TIDAL_VOLUME);

      const DiscreteValue  APNEA_FLOW_PATTERN_VALUE =
				       pApneaFlowPattern->getAdjustedValue();

      const Real32  APNEA_PEAK_INSP_FLOW_VALUE =
		   BoundedValue(pApneaPeakInspFlow->getAdjustedValue()).value;
      const Real32  APNEA_PLATEAU_TIME_VALUE =
		   BoundedValue(pApneaPlateauTime->getAdjustedValue()).value;
      const Real32  APNEA_TIDAL_VOLUME_VALUE =
		   BoundedValue(pApneaTidalVolume->getAdjustedValue()).value;

      switch (APNEA_FLOW_PATTERN_VALUE)
      {
      case FlowPatternValue::SQUARE_FLOW_PATTERN : 
	{   // $[TI3.1] 
	  static const Real32  SQUARE_FACTOR_ = (60000.0f * 0.001f);

	  // $[02019]b -- formula for calculating apnea inspiratory time
	  //              from the apnea VCV parameters...
	  apneaInspTime.value = (SQUARE_FACTOR_ * APNEA_TIDAL_VOLUME_VALUE) /
					  APNEA_PEAK_INSP_FLOW_VALUE;

#if defined(SIGMA_DEVELOPMENT)
          if (Settings_Validation::IsDebugOn(::APNEA_IE_RATIO_SETTING))
          {
            cout << "AIERS::cBOS "
                 << " SF==" << SQUARE_FACTOR_
                 << " ATVV=" << APNEA_TIDAL_VOLUME_VALUE
                 << " APIFV==" << APNEA_PEAK_INSP_FLOW_VALUE
                 << " aITv==" << apneaInspTime.value
                 << endl;
          }
#endif  // defined(SIGMA_DEVELOPMENT)


	}
	break;
      case FlowPatternValue::RAMP_FLOW_PATTERN :  
	{   // $[TI3.2] 
	  static const Real32  RAMP_FACTOR_ = (60000.0f * 0.001f * 2.0f);

	  const Setting*  pApneaMinInspFlow =
		  SettingsMgr::GetSettingPtr(SettingId::APNEA_MIN_INSP_FLOW);

	  const Real32  APNEA_MIN_INSP_FLOW_VALUE =
		    BoundedValue(pApneaMinInspFlow->getAdjustedValue()).value;

	  // $[02019]c -- formula for calculating apnea inspiratory time from
	  //              the apnea VCV parameters...
	  apneaInspTime.value = (RAMP_FACTOR_ * APNEA_TIDAL_VOLUME_VALUE) /
		    (APNEA_PEAK_INSP_FLOW_VALUE + APNEA_MIN_INSP_FLOW_VALUE);
	}
	break;
      default :
	// unexpected 'APNEA_FLOW_PATTERN_VALUE' value...
	AUX_CLASS_ASSERTION_FAILURE(APNEA_FLOW_PATTERN_VALUE);
	break;
      };

      // add the apnea plateau time to the calculated apnea inspiratory
      // time -- irregardless of the apnea flow pattern...$[02019]b...
      apneaInspTime.value += APNEA_PLATEAU_TIME_VALUE;

      BoundedSetting*  pApneaInspTime =
              SettingsMgr::GetBoundedSettingPtr(SettingId::APNEA_INSP_TIME);

      // because apnea inspiratory time has a "coarse" resolution in VCV,
      // the "raw" apnea insp time value must be rounded to a resolution
      // boundary, before calculating apnea expiratory time...
      pApneaInspTime->warpToRange(apneaInspTime, warpDirection, FALSE);

      // calculate apnea expiratory time...
      apneaExpTimeValue = APNEA_BREATH_PERIOD_VALUE - apneaInspTime.value;

#if defined(SIGMA_DEVELOPMENT)
      if (Settings_Validation::IsDebugOn(::APNEA_IE_RATIO_SETTING))
      {
            cout << "AIERS::cBOS "
                 << " aITv==" << apneaInspTime.value
                 << " ABPV==" << APNEA_BREATH_PERIOD_VALUE
                 << " aETV==" << apneaExpTimeValue
                 << endl;
      }
#endif  // defined(SIGMA_DEVELOPMENT)


    }
    break;

  default :
    // unexpected dependent setting id...
    AUX_CLASS_ASSERTION_FAILURE(basedOnSettingId);
    break;
  };

  //====================================================================
  // a LARGE knob delta can cause apnea Ti or apnea Te to "jump" passed a
  // bound constraint, causing a negative Ti or Te would result from the
  // knob delta, therefore calculate apnea I:E ratio based on the positive
  // values of the Ti and Te from above...
  //====================================================================
  
  BoundedRange::WarpDir    actualWarpDirection;

  apneaInspTime.value = ABS_VALUE(apneaInspTime.value);
  apneaExpTimeValue   = ABS_VALUE(apneaExpTimeValue);

  if (::IsEquivalent(apneaInspTime.value, apneaExpTimeValue, ::HUNDREDTHS))
  {   // $[TI4]
    // when the apnea inspiratory time is equivalent to the apnea expiratory
    // time (to be shown as "1.00:1"), we store a value of one...
    apneaIeRatio.value = 1.00f;

    // since no warping is actually needed -- just need precision -- warp
    // to nearest...
    actualWarpDirection = BoundedRange::WARP_NEAREST;
  }
  else if (apneaInspTime.value > apneaExpTimeValue)
  {   // $[TI5]
    // when the apnea inspiratory time is greater than the apnea expiratory
    // time (to be shown as "xx:1"), we store the apnea insp.-to-exp.
    // ratio...
    apneaIeRatio.value = (apneaInspTime.value / apneaExpTimeValue);

    // resulting value must be > 1.00:1, therefore force a warp up; this
    // special handling is needed because the resolution for this interval
    // is VERY large (2.01), therefore this ensures that a bound violation
    // will be detected...
    actualWarpDirection = BoundedRange::WARP_UP;
  }
  else
  {   // $[TI6]
    // when the apnea expiratory time is greater than the apneaInspiratory time
    // (to be shown as "1:xx"), we store the negative apnea exp.-to-insp.
    // ratio...
    apneaIeRatio.value = -(apneaExpTimeValue / apneaInspTime.value);

    if (apneaIeRatio.value > ::INTERVAL_LIST_.pNext->value)
    {   // $[TI6.1] -- apnea I:E greater than 1:1.01, yet less than 1.00:1...
      // this is a SPECIAL case for apnea I:E ratio only; due to apnea I:E
      // ratio's absolute maximum of 1.00:1, while having a LARGE resolution
      // between its "high change point" (1:1.01) and its maximum value
      // (1.00:1); cases can arise where, though apnea Te is greater than
      // apnea Ti, the resulting apnea I:E ratio is greater than 1:1.01; treat
      // treat this situation as a value of 1:1.01...
      apneaIeRatio.value = ::INTERVAL_LIST_.pNext->value;
    }   // $[TI6.2] -- things are right in the world...

    // use passed-in warp direction...
    actualWarpDirection = warpDirection;
  }

  // warp according to rules above...
  getBoundedRange().warpValue(apneaIeRatio, actualWarpDirection, FALSE);

  return(apneaIeRatio);
}


//====================================================================
//
//  Private Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isApneaBreathPeriodOdd_(basedOnCategory)  [const]
//
//@ Interface-Description
//  Returns 'TRUE' if the apnea breath period is "odd" (e.g., 3750ms, as
//  opposed to 3760ms), otherwise 'FALSE' is returned.  This information
//  is useful when calculating values for apnea I:E ratio, because if the
//  breath period is odd, a value of 1:1 is invalid (because apnea Ti and
//  Te can't be equal).  The 'basedOnCategory' parameter is used for
//  determining "where" to calculate the apnea breath period value from.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
ApneaIeRatioSetting::isApneaBreathPeriodOdd_(
				  const BasedOnCategory basedOnCategory
					    ) const
{
  CALL_TRACE("isApneaBreathPeriodOdd_(basedOnCategory)");

  BoundedSetting*  pApneaExpTime =
	      SettingsMgr::GetBoundedSettingPtr(SettingId::APNEA_EXP_TIME);

  BoundedValue  warpedApneaBreathPeriod;

  // get the "raw" apnea breath period value...
  warpedApneaBreathPeriod.value =
		ApneaRespRateSetting::GetApneaBreathPeriod(basedOnCategory);

  // use apnea expiratory time's interval to warp the breath period to a
  // value relative the the timing setting's resolutions...
  pApneaExpTime->warpToRange(warpedApneaBreathPeriod,
			     BoundedRange::WARP_NEAREST, FALSE);

  return((Int(warpedApneaBreathPeriod.value / 10.0f) % 2) != 0);
}   // $[TI1] (TRUE)  $[TI2] (FALSE)
