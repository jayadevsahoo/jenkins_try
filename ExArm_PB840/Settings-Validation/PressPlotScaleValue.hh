
#ifndef PressPlotScaleValue_HH
#define PressPlotScaleValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  PressPlotScaleValue - Y-Axis Scale Values for the Pressure Plot.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PressPlotScaleValue.hhv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: mnr    Date: 14-Jan-2009    SCR Number: 5987
//  Project:  NEO
//  Description:
//      Added new pressure plot scale ranges and remoed -20 to 10.
//
//  Revision: 002  By: rhj    Date: 04-Aug-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added a new pressure plot scale range from -60 to 20 for RESPM.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct PressPlotScaleValue
{
  //@ Type:  PressPlotScaleValueId
  // All of the possible values for PressPlotScaleSetting.
  enum PressPlotScaleValueId
  {
    // $[01160] -- values of the range of Pressure Plot Axis Setting...
	MINUS_2_TO_10_CMH2O,
	MINUS_2_TO_16_CMH2O,
	MINUS_2_TO_20_CMH2O,
	MINUS_5_TO_30_CMH2O,
    MINUS_20_TO_20_CMH2O,
    MINUS_20_TO_40_CMH2O,
    MINUS_20_TO_60_CMH2O,
    MINUS_20_TO_80_CMH2O,
    MINUS_20_TO_100_CMH2O,
    MINUS_20_TO_120_CMH2O,
    MINUS_60_TO_20_CMH2O,   

    TOTAL_PRESS_SCALE_VALUES
  };
};


#endif // PressPlotScaleValue_HH 
