
#ifndef FaultHandlerMacros_HH
#define FaultHandlerMacros_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  FaultHandlerMacros - Fault-Handler Macros.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/FaultHandlerMacros.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modifications
//
//   Revision: 004   By: sah   Date: 30-Aug-1996   DCS Number: 1280
//   Project:  Sigma (R8027)
//   Description:
//	Added "auxillary" assertion macros that take an error code
//	argument.
//
//   Revision: 003   By: sah   Date: 29-Feb-1996   DCS Number: 719
//   Project:  Sigma (R8027)
//   Description:
//	Added 'CLASS_ASSERTION_FAILURE()' and 'FREE_ASSERTION_FAILURE()'
//	to allow for elimination of warnings with the new compiler.
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

//@ Usage-Class
#include "FaultHandler.hh"
//@ End-Usage


//=====================================================================
//
// Reporting Macros...
//
//=====================================================================

//=====================================================================
// Reporting macro for CLASS pre-conditions, invariants and general
// assertions...
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)
// when in 'DEVELOPMENT' or 'DEBUG' mode, the filename and stringized
// boolean expression are passed to the fault-handling utility...
#  define  REPORT_CLASS_ASSERTION(softFaultId, boolExpr)	\
	     SoftFault(softFaultId, __LINE__, __FILE__, #boolExpr);
#else
// when in 'PRODUCTION' mode, the filename and stringized boolean
// expression are NOT passed to the fault-handling utility...
#  define  REPORT_CLASS_ASSERTION(softFaultId, boolExpr)	\
	     SoftFault(softFaultId, __LINE__);
#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
// Reporting Macro for class post-conditions...
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)
// when in 'DEVELOPMENT' or 'DEBUG' mode, the filename and stringized
// boolean expression are passed to the fault-handling utility...
#  define  REPORT_CLASS_POST_CONDITION(boolExpr, className)	\
	     className::SoftFault(POST_CONDITION_ID, __LINE__,	\
				  __FILE__, #boolExpr);
#else
// when in 'PRODUCTION' mode, the filename and stringized boolean
// expression are NOT passed to the fault-handling utility...
#  define  REPORT_CLASS_POST_CONDITION(boolExpr, className)	\
	     className::SoftFault(POST_CONDITION_ID, __LINE__);
#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
// Reporting macro for all FREE assertions...
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)
// when in 'DEVELOPMENT' or 'DEBUG' mode, the filename and stringized
// boolean expression are passed to the fault-handling utility...
#  define  REPORT_FREE_ASSERTION(softFaultId, subSystemId, freeId, boolExpr)\
	     FaultHandler::SoftFault(softFaultId, subSystemId, freeId,	\
				     __LINE__, __FILE__, #boolExpr);
#else
// when in 'PRODUCTION' mode, the filename and stringized boolean
// expression are NOT passed to the fault-handling utility...
#  define  REPORT_FREE_ASSERTION(softFaultId, subSystemId, freeId, boolExpr)\
	     FaultHandler::SoftFault(softFaultId, subSystemId, freeId,	\
				     __LINE__);
#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
// Testing Macros...
//
//=====================================================================

//=====================================================================
// Pre-Condition Testing Macros...
//=====================================================================

//@ Macro:  CLASS_PRE_CONDITION(boolExpr)
// This macro provides a test of the pre-condition identified by 'boolExpr'
// for a class.
#define CLASS_PRE_CONDITION(boolExpr)				\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_ASSERTION(PRE_CONDITION_ID, boolExpr);	\
	  }							\
	}

//@ Macro:  FREE_PRE_CONDITION(boolExpr, subSystemId, freeId)
// This macro provides a test of the pre-condition identified by 'boolExpr'
// for the free-function identified by 'subSystemId' and 'freeId'.
#define FREE_PRE_CONDITION(boolExpr, subSystemId, freeId)	\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_FREE_ASSERTION(PRE_CONDITION_ID,		\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
        }

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
//@ Macro:  SAFE_CLASS_PRE_CONDITION(boolExpr)
// This macro provides a DEVELOPMENT-ONLY test of the pre-condition identified
// by 'boolExpr' for a class.
#  define SAFE_CLASS_PRE_CONDITION(boolExpr)			\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_ASSERTION(PRE_CONDITION_ID, boolExpr);	\
	  }							\
	}

//@ Macro:  SAFE_FREE_PRE_CONDITION(boolExpr, subSystemId, freeId)
// This macro provides a DEVELOPMENT-ONLY test of the pre-condition identified
// by 'boolExpr' for the free-function identified by 'subSystemId' and
// 'freeId'.
#  define SAFE_FREE_PRE_CONDITION(boolExpr, subSystemId, freeId)\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_FREE_ASSERTION(PRE_CONDITION_ID,		\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
	}
#else  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
#  define SAFE_CLASS_PRE_CONDITION(boolExpr)
#  define SAFE_FREE_PRE_CONDITION(boolExpr, subSystemId, freeId)
#endif  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)


//=====================================================================
// Post-Condition Testing Macros...
//=====================================================================

//@ Macro:  CLASS_POST_CONDITION(boolExpr, className)
// This macro provides a test of the post-condition identified by 'boolExpr'
// for the class identified by 'className'.
#define CLASS_POST_CONDITION(boolExpr, className)		\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_POST_CONDITION(boolExpr, className);	\
	  }							\
        }

//@ Macro:  FREE_POST_CONDITION(boolExpr, subSystemId, freeId)
// This macro provides a test of the post-condition identified by 'boolExpr'
// for the free-function identified by 'subSystemId' and 'freeId'.
#define FREE_POST_CONDITION(boolExpr, subSystemId, freeId)	\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_FREE_ASSERTION(POST_CONDITION_ID,		\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
        }

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
//@ Macro:  SAFE_CLASS_POST_CONDITION(boolExpr, className)
// This macro provides a DEVELOPMENT-ONLY test of the post-condition identified
// by 'boolExpr' for the class identified by 'className'.
#  define SAFE_CLASS_POST_CONDITION(boolExpr, className)	\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_POST_CONDITION(boolExpr, className);	\
	  }							\
	}

//@ Macro:  SAFE_FREE_POST_CONDITION(boolExpr, subSystemId, freeId)
// This macro provides a DEVELOPMENT-ONLY test of the post-condition identified
// by 'boolExpr' for the free-function identified by 'subSystemId' and
// 'freeId'.
#  define SAFE_FREE_POST_CONDITION(boolExpr, subSystemId, freeId)\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_FREE_ASSERTION(POST_CONDITION_ID,		\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
	}
#else  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
#  define SAFE_CLASS_POST_CONDITION(boolExpr, className)
#  define SAFE_FREE_POST_CONDITION(boolExpr, subSystemId, freeId)
#endif  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)


//=====================================================================
// Invariant Testing Macros...
//=====================================================================

//@ Macro:  CLASS_INVARIANT(boolExpr)
// This macro provides a test of the invariant identified by 'boolExpr' for
// a class.
#define CLASS_INVARIANT(boolExpr)				\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_ASSERTION(INVARIANT_ID, boolExpr);	\
	  }							\
        }

//@ Macro:  FREE_INVARIANT(boolExpr, subSystemId, freeId)
// This macro provides a test of the invariant identified by 'boolExpr' for
// the free-function identified by 'subSystemId' and 'freeId'.
#define FREE_INVARIANT(boolExpr, subSystemId, freeId)		\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_FREE_ASSERTION(INVARIANT_ID,			\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
        }

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
//@ Macro:  SAFE_CLASS_INVARIANT(boolExpr)
// This macro provides a DEVELOPMENT-ONLY test of the invariant identified by
// 'boolExpr' for a class.
#  define SAFE_CLASS_INVARIANT(boolExpr)			\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_ASSERTION(INVARIANT_ID, boolExpr);	\
	  }							\
	}

//@ Macro:  SAFE_FREE_INVARIANT(boolExpr, subSystemId, freeId)
// This macro provides a DEVELOPMENT-ONLY test of the invariant identified
// by 'boolExpr' for the free-function identified by 'subSystemId' and
// 'freeId'.
#  define SAFE_FREE_INVARIANT(boolExpr, subSystemId, freeId)	\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_FREE_ASSERTION(INVARIANT_ID,			\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
	}
#else  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
#  define SAFE_CLASS_INVARIANT(boolExpr)
#  define SAFE_FREE_INVARIANT(boolExpr, subSystemId, freeId)
#endif  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)


//=====================================================================
// Assertion Testing Macros...
//=====================================================================

//@ Macro:  CLASS_ASSERTION(boolExpr)
// This macro provides a test of the assertion identified by 'boolExpr' for
// a class.
#define CLASS_ASSERTION(boolExpr)				\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_ASSERTION(ASSERTION_ID, boolExpr);	\
	  }							\
        }

//@ Macro:  FREE_ASSERTION(boolExpr, subSystemId, freeId)
// This macro provides a test of the assertion identified by 'boolExpr' for
// the free function identified by 'subSystemId' and 'freeId'.
#define FREE_ASSERTION(boolExpr, subSystemId, freeId)		\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_FREE_ASSERTION(ASSERTION_ID,			\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
        }

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
//@ Macro:  SAFE_CLASS_ASSERTION(boolExpr)
// This macro provides a DEVELOPMENT-ONLY test of the assertion identified by
// 'boolExpr' for a class.
#  define SAFE_CLASS_ASSERTION(boolExpr)			\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_CLASS_ASSERTION(ASSERTION_ID, boolExpr);	\
	  }							\
	}

//@ Macro:  SAFE_FREE_ASSERTION(boolExpr, subSystemId, freeId)
// This macro provides a DEVELOPMENT-ONLY test of the assertion identified by
// 'boolExpr' for the free function identified by 'subSystemId' and 'freeId'.
#  define SAFE_FREE_ASSERTION(boolExpr, subSystemId, freeId)	\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    REPORT_FREE_ASSERTION(ASSERTION_ID,			\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
	}
#else  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
#  define SAFE_CLASS_ASSERTION(boolExpr)
#  define SAFE_FREE_ASSERTION(boolExpr, subSystemId, freeId)
#endif  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)


//=====================================================================
// Forced Assertion Macros...
//=====================================================================

//@ Macro:  CLASS_ASSERTION_FAILURE()
// This macro provides a forced assertion identified for a class.
#define CLASS_ASSERTION_FAILURE()				\
	{							\
	  REPORT_CLASS_ASSERTION(ASSERTION_ID, (FALSE));	\
        }

//@ Macro:  FREE_ASSERTION_FAILURE(subSystemId, freeId)
// This macro provides a forced assertion for the free function identified
// by 'subSystemId' and 'freeId'.
#define FREE_ASSERTION_FAILURE(subSystemId, freeId)		\
	{							\
	  REPORT_FREE_ASSERTION(ASSERTION_ID,			\
				subSystemId, freeId,		\
				(FALSE));			\
        }

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
//@ Macro:  SAFE_CLASS_ASSERTION_FAILURE()
// This macro provides a DEVELOPMENT-ONLY forced assertion identified for
// a class.
#define SAFE_CLASS_ASSERTION_FAILURE()				\
	{							\
	  REPORT_CLASS_ASSERTION(ASSERTION_ID, (FALSE));	\
        }

//@ Macro:  SAFE_FREE_ASSERTION_FAILURE(subSystemId, freeId)
// This macro provides a DEVELOPMENT-ONLY forced assertion for the free
// function identified by 'subSystemId' and 'freeId'.
#define SAFE_FREE_ASSERTION_FAILURE(subSystemId, freeId)	\
	{							\
	  REPORT_FREE_ASSERTION(ASSERTION_ID,			\
				subSystemId, freeId,		\
				(FALSE));			\
        }
#else  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
#  define SAFE_CLASS_ASSERTION_FAILURE()
#  define SAFE_FREE_ASSERTION_FAILURE(subSystemId, freeId)
#endif  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)


//=====================================================================
//
// Auxillary Testing Macros...
//
//=====================================================================

//=====================================================================
// Auxillary Pre-Condition Testing Macros...
//=====================================================================

//@ Macro:  AUX_CLASS_PRE_CONDITION(boolExpr, auxErrorCode)
// This macro provides a test of the pre-condition identified by 'boolExpr'
// for a class.
#define AUX_CLASS_PRE_CONDITION(boolExpr, auxErrorCode)		\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_CLASS_ASSERTION(PRE_CONDITION_ID, boolExpr);	\
	  }							\
	}

//@ Macro:  AUX_FREE_PRE_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)
// This macro provides a test of the pre-condition identified by 'boolExpr'
// for the free-function identified by 'subSystemId' and 'freeId'.
#define AUX_FREE_PRE_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_FREE_ASSERTION(PRE_CONDITION_ID,		\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
        }

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
//@ Macro:  SAFE_AUX_CLASS_PRE_CONDITION(boolExpr, auxErrorCode)
// This macro provides a DEVELOPMENT-ONLY test of the pre-condition identified
// by 'boolExpr' for a class.
#  define SAFE_AUX_CLASS_PRE_CONDITION(boolExpr, auxErrorCode)	\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_CLASS_ASSERTION(PRE_CONDITION_ID, boolExpr);	\
	  }							\
	}

//@ Macro:  SAFE_AUX_FREE_PRE_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)
// This macro provides a DEVELOPMENT-ONLY test of the pre-condition identified
// by 'boolExpr' for the free-function identified by 'subSystemId' and
// 'freeId'.
#  define SAFE_AUX_FREE_PRE_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_FREE_ASSERTION(PRE_CONDITION_ID,		\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
	}
#else  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
#  define SAFE_AUX_CLASS_PRE_CONDITION(boolExpr, auxErrorCode)
#  define SAFE_AUX_FREE_PRE_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)
#endif  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)


//=====================================================================
// Auxillary Post-Condition Testing Macros...
//=====================================================================

//@ Macro:  AUX_CLASS_POST_CONDITION(boolExpr, auxErrorCode, className)
// This macro provides a test of the post-condition identified by 'boolExpr'
// for the class identified by 'className'.
#define AUX_CLASS_POST_CONDITION(boolExpr, auxErrorCode, className)\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_CLASS_POST_CONDITION(boolExpr, className);	\
	  }							\
        }

//@ Macro:  AUX_FREE_POST_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)
// This macro provides a test of the post-condition identified by 'boolExpr'
// for the free-function identified by 'subSystemId' and 'freeId'.
#define AUX_FREE_POST_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)	\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_FREE_ASSERTION(POST_CONDITION_ID,		\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
        }

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
//@ Macro:  SAFE_AUX_CLASS_POST_CONDITION(boolExpr, auxErrorCode, className)
// This macro provides a DEVELOPMENT-ONLY test of the post-condition identified
// by 'boolExpr' for the class identified by 'className'.
#  define SAFE_AUX_CLASS_POST_CONDITION(boolExpr, auxErrorCode, className)\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_CLASS_POST_CONDITION(boolExpr, className);	\
	  }							\
	}

//@ Macro:  SAFE_AUX_FREE_POST_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)
// This macro provides a DEVELOPMENT-ONLY test of the post-condition identified
// by 'boolExpr' for the free-function identified by 'subSystemId' and
// 'freeId'.
#  define SAFE_AUX_FREE_POST_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_FREE_ASSERTION(POST_CONDITION_ID,		\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
	}
#else  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
#  define SAFE_AUX_CLASS_POST_CONDITION(boolExpr, auxErrorCode, className)
#  define SAFE_AUX_FREE_POST_CONDITION(boolExpr, auxErrorCode, subSystemId, freeId)
#endif  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)


//=====================================================================
// Auxillary Assertion Testing Macros...
//=====================================================================

//@ Macro:  AUX_CLASS_ASSERTION(boolExpr, auxErrorCode)
// This macro provides a test of the assertion identified by 'boolExpr' for
// a class.
#define AUX_CLASS_ASSERTION(boolExpr, auxErrorCode)		\
	{							\
	 Boolean toComTempVariable = boolExpr;		\
	  if (!(toComTempVariable))				\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_CLASS_ASSERTION(ASSERTION_ID, boolExpr);	\
	  }							\
        }

//@ Macro:  AUX_FREE_ASSERTION(boolExpr, auxErrorCode, subSystemId, freeId)
// This macro provides a test of the assertion identified by 'boolExpr' for
// the free function identified by 'subSystemId' and 'freeId'.
#define AUX_FREE_ASSERTION(boolExpr, auxErrorCode, subSystemId, freeId)\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_FREE_ASSERTION(ASSERTION_ID,			\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
        }

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
//@ Macro:  SAFE_AUX_CLASS_ASSERTION(boolExpr, auxErrorCode)
// This macro provides a DEVELOPMENT-ONLY test of the assertion identified by
// 'boolExpr' for a class.
#  define SAFE_AUX_CLASS_ASSERTION(boolExpr, auxErrorCode)	\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_CLASS_ASSERTION(ASSERTION_ID, boolExpr);	\
	  }							\
	}

//@ Macro:  SAFE_AUX_FREE_ASSERTION(boolExpr, auxErrorCode, subSystemId, freeId)
// This macro provides a DEVELOPMENT-ONLY test of the assertion identified by
// 'boolExpr' for the free function identified by 'subSystemId' and 'freeId'.
#  define SAFE_AUX_FREE_ASSERTION(boolExpr, auxErrorCode, subSystemId, freeId)\
	{							\
	  if (!(boolExpr))					\
	  {							\
	    FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	    REPORT_FREE_ASSERTION(ASSERTION_ID,			\
				  subSystemId, freeId,		\
				  boolExpr);			\
	  }							\
	}
#else  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
#  define SAFE_AUX_CLASS_ASSERTION(boolExpr, auxErrorCode)
#  define SAFE_AUX_FREE_ASSERTION(boolExpr, auxErrorCode, subSystemId, freeId)
#endif  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)


//=====================================================================
// Auxillary Forced Assertion Macros...
//=====================================================================

//@ Macro:  AUX_CLASS_ASSERTION_FAILURE(auxErrorCode)
// This macro provides a forced assertion identified for a class.
#define AUX_CLASS_ASSERTION_FAILURE(auxErrorCode)		\
	{							\
	  FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	  REPORT_CLASS_ASSERTION(ASSERTION_ID, (FALSE));	\
        }

//@ Macro:  AUX_FREE_ASSERTION_FAILURE(auxErrorCode, subSystemId, freeId)
// This macro provides a forced assertion for the free function identified
// by 'subSystemId' and 'freeId'.
#define AUX_FREE_ASSERTION_FAILURE(auxErrorCode, subSystemId, freeId)\
	{							\
	  FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	  REPORT_FREE_ASSERTION(ASSERTION_ID,			\
				subSystemId, freeId,		\
				(FALSE));			\
        }

#if defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
//@ Macro:  SAFE_AUX_CLASS_ASSERTION_FAILURE(auxErrorCode)
// This macro provides a DEVELOPMENT-ONLY forced assertion identified for
// a class.
#define SAFE_AUX_CLASS_ASSERTION_FAILURE(auxErrorCode)		\
	{							\
	  FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	  REPORT_CLASS_ASSERTION(ASSERTION_ID, (FALSE));	\
        }

//@ Macro:  SAFE_AUX_FREE_ASSERTION_FAILURE(auxErrorCode, subSystemId, freeId)
// This macro provides a DEVELOPMENT-ONLY forced assertion for the free
// function identified by 'subSystemId' and 'freeId'.
#define SAFE_AUX_FREE_ASSERTION_FAILURE(auxErrorCode, subSystemId, freeId)\
	{							\
	  FaultHandler::StoreAuxErrorCode(auxErrorCode);	\
	  REPORT_FREE_ASSERTION(ASSERTION_ID,			\
				subSystemId, freeId,		\
				(FALSE));			\
        }
#else  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)
#  define SAFE_AUX_CLASS_ASSERTION_FAILURE(auxErrorCode)
#  define SAFE_AUX_FREE_ASSERTION_FAILURE(auxErrorCode, subSystemId, freeId)
#endif  // defined(SIGMA_DEVELOPMENT)  &&  !defined(ALL_SAFE_CONDITIONS_OFF)

//-----------------------------------------------------
// Macros used to define and implement generic
// soft fault handler methods in a class.
// Put the SOFT_FAULT_DEFN() in your class definition (.hh)
// Put the SOFT_FAULT_IMPL() in your class implementation (.cc)
//-----------------------------------------------------
#define SOFT_FAULT_DEFN() \
static void SoftFault(const SoftFaultID softFaultID, \
                      const Uint32      lineNumber, \
                      const char*       pFileName  = NULL, \
                      const char*       pPredicate = NULL);

//------------------------------------------------------------------------------
// SoftFault
//------------------------------------------------------------------------------
#define SOFT_FAULT_IMPL( CN, moduleId, classId ) \
void CN::SoftFault(const SoftFaultID  softFaultID, \
						  const Uint32       lineNumber, \
						  const char*        pFileName, \
						  const char*        pPredicate) \
{ \
	FaultHandler::SoftFault(softFaultID, moduleId, \
						  classId, lineNumber, \
						  pFileName, pPredicate); \
}

#endif  // FaultHandlerMacros_HH
