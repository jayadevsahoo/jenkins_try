#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ M O D U L E   D E S C R I P T I O N ====
//@ Filename: tod.cc  - Power on self test, Time of day test
//---------------------------------------------------------------------
//@ Interface-Description
//  >Von
//    This module contains functions to test and set the time of day 
//    clock of the GUI or BD CPU.
//
//    TimeOfDayClockTest - Performs the post test
//    InitializeTOD      - Sets default time/date
//    DisplayTOD         - For development, displays TOD to stdio
//  >Voff
//    
//---------------------------------------------------------------------
//@ Rationale
//    These tests satisfy the time of day diagnostic functions POST.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//    Both GUI and BD CPU's use a Dallas DS1286 with battery backup.
//  
//    TimeOfDayClockTest reads the current time from the DS1286
//    and validates the time & date settings.  InitilizeTOD is used to
//    set a default time/date if an invalid TOD is noted.
//
//---------------------------------------------------------------------
//@ Fault-Handling
//    A MINOR POST failure is recorded for an invalid time/date.
//---------------------------------------------------------------------
//@ Restrictions
//    None
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Kernel/vcssrc/tod.ccv   25.0.4.0   19 Nov 2013 14:13:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005   By: Cederquist    Date: 31-Jan-2011  SCR Number: 6744
//  Project: XENA2
//  Description:
//      Changed DS 1286 to "write enable" instead of disabling oscillator.
//
//  Revision: 004   By: Cederquist    Date: 27-Jan-2011  SCR Number: 6671
//  Project: XENA2
//  Description:
//      Changed to support selection of ST M48T58 time of day chip. Cleaned
//      up implementation of tests.
//
//  Revision: 003   By: Mitesh Raval  Date: 10-Nov-2010  SCR Number: 6671
//  Project: XENA2
//  Description:
//      Test added for FPGA_CTR to TimeOfDayClockTestXena2().
//      New methods added for Xena2 and backwards compatibility maintained.
//      Xena2 RTC related updates.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include <stdio.h>

#include "kernel.hh"
#include "tod.hh"
#include "DS1286.hh"
#include "STM48T58.hh"
#include "MemoryMap.hh"
#include "CpuDevice.hh"

// forward declarations
Criticality TimeOfDayClockTestDS1286(void);
void        InitializeTODDS1286(void);
Criticality TimeOfDayClockTestXena2(void);
void        InitializeTODXena2(void);


const Uint8 max_day[12] = {31,29,31,30,31,30,31,31,30,31,30,31};

// globals for verifying code using emulator
volatile DS1286RegMap *tptr;
volatile STM48T58RegMap *tptrXena2;
Uint year;
Uint month;
Uint day;
Uint hour;
Uint twelve_hour_format;
Uint minute;
Uint second;

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: TimeOfDayClockTest(void)
//
//@ Interface-Description
//
//  INPUTS: NONE
//
//  OUPUTS: PASSED, MAJOR, MINOR  
//---------------------------------------------------------------------
//@ Implementation-Description
//   Calls the appropriate TOD test based on board type.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
Criticality TimeOfDayClockTest  (void)
{
    if (CpuDevice::IsXena2Config())
    {
        return TimeOfDayClockTestXena2();
    }
    else
    {
        return TimeOfDayClockTestDS1286();
    }
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: TimeOfDayClockTest1286 (void)
//
//@ Interface-Description
//
//  INPUTS: NONE
//
//  OUPUTS: PASSED, MAJOR, MINOR  
//---------------------------------------------------------------------
//@ Implementation-Description
//  >Von
//     The timer is initialized by setting the EOSC bit to zero to enable
//     the clock oscillator. The TDM bit is zeroed to disable the time of
//     day alarm. The interrupt control bits (IPSW, IBH, PU, INP) are set
//     as dictated by the hardware design.  The TE bit will be set to one.
//     The command register will then be read to verify these bit settings.
//     Note that the INP bit is only meaningful on the DS1283; it is always
//     zero on the DS 1286.
//
//     Snapshot the time, turning off update to get a valid snapshot.
//     Read the time & date, validate the settings for the following:
//     
//     day of week = 1-7
//     month       = 1-12
//     day         = 1-max day for the month    (note: Feb = 29)
//     format      = 12,24 hr
//     hour        = 0-12 (0-23 for 24 hour format)
//     minute      = 0-59
//     second      = 0-59
//
//     If the any of the settings are invalid, initialize the clock to 
//     default time/date and return a MINOR failure.  $[11021]
//  >Voff
//---------------------------------------------------------------------
//@ PreCondition
//
//  On the GUI CPU, a valid time/date is assumed to exist
//  On the BD CPU, only the ability to set a time is assumed.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
#define  INTERRUPT_MODE_TEST_PATTERN1           (0)
#define  MASK_WATCHDOG_ALARM_TEST_PATTERN1      (0)
#define  MASK_TIME_OF_DAY_ALARM_TEST_PATTERN1   (0)

#define  INTERRUPT_MODE_TEST_PATTERN2           (7)
#define  MASK_WATCHDOG_ALARM_TEST_PATTERN2      (1)
#define  MASK_TIME_OF_DAY_ALARM_TEST_PATTERN2   (1)

Criticality TimeOfDayClockTestDS1286  (void)
{
	//TODO E600 port if needed

/*
    tptr = (DS1286RegMap*)DS1286_BASE;
    DS1286RegMap t;

    tptr->disable_real_time_oscillator = 0; //let it run
    tptr->enable_update                = 0; //but not update

    // **** set the controls bits to test values 
    tptr->interrupt_mode              = INTERRUPT_MODE_TEST_PATTERN1;
    tptr->mask_watchdog_alarm         = MASK_WATCHDOG_ALARM_TEST_PATTERN1;
    tptr->mask_time_of_day_alarm      = MASK_TIME_OF_DAY_ALARM_TEST_PATTERN1;

    UNIT_TEST_POINT(POSTUT_14_1)

    // **** allow update and check that the value are there
    tptr->enable_update                = 1;

    if (   (tptr->interrupt_mode          != INTERRUPT_MODE_TEST_PATTERN1)
           || (tptr->mask_watchdog_alarm     != MASK_WATCHDOG_ALARM_TEST_PATTERN1)
           || (tptr->mask_time_of_day_alarm  != MASK_TIME_OF_DAY_ALARM_TEST_PATTERN1)
       )
    {
        // $[TI1.1]
        return MAJOR;
    }

    // **** set the controls bits to test values 
    tptr->enable_update               = 0; //no update
    tptr->interrupt_mode              = INTERRUPT_MODE_TEST_PATTERN2;
    tptr->mask_watchdog_alarm         = MASK_WATCHDOG_ALARM_TEST_PATTERN2;
    tptr->mask_time_of_day_alarm      = MASK_TIME_OF_DAY_ALARM_TEST_PATTERN2;

    UNIT_TEST_POINT(POSTUT_14_2)

    // **** allow update and check that the value are there
    tptr->enable_update                   = 1; //allow update
    if (   (tptr->interrupt_mode          != INTERRUPT_MODE_TEST_PATTERN2)
           || (tptr->mask_watchdog_alarm     != MASK_WATCHDOG_ALARM_TEST_PATTERN2)
           || (tptr->mask_time_of_day_alarm  != MASK_TIME_OF_DAY_ALARM_TEST_PATTERN2)
       )
    {
        // $[TI2.1]
        return MAJOR;
    }
    // $[TI2.2]

    // **** stop update take a snapshot and check values in time registers
    tptr->enable_update                = 0;
    t = *tptr;

    year           = t.year_ones   +  10 * t.year_tens;
    month          = t.month_ones  +  10 * t.month_tens;
    day            = t.date_ones   +  10 * t.date_tens;
    twelve_hour_format = t.time_12_hour;

    if (twelve_hour_format)
    {
        // $[TI3.1]
        hour              = t.hour_ones + 10 * t.hour_tens_lsb;
    }
    else
    {
        // $[TI3.2]
        hour              = t.hour_ones + 10 * (t.hour_tens_lsb + t.hour_tens_msb);
    }

    minute         =  t.minute_ones + 10 * t.minute_tens;
    second         =  t.second_ones + 10 * t.second_tens;


    UNIT_TEST_POINT(POSTUT_14_3)

    // **** enable update from this point on
    tptr->enable_update                = 1;

    if (   (year                  > 99)
           || (month                 <  1) 
           || (month                 > 12)
           || (day                   <  1)
           || (day                   > max_day[month-1])
           || (twelve_hour_format        )
           || (hour                  > 23)
           || (minute                > 59)
           || (second                > 59) )
    {
        // $[TI4.1]
        // Time/Data is not valid so initialize the clock
        UNIT_TEST_POINT(POSTUT_14_4)
        InitializeTODDS1286();
        return MINOR;  // $[11021]
    }
    // $[TI4.2]
*/

    return PASSED;
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: TimeOfDayClockTestXena2 (void)
//
//@ Interface-Description
//
//  INPUTS: NONE
//
//  OUPUTS: PASSED, MAJOR, MINOR  
//---------------------------------------------------------------------
//@ Implementation-Description
//  >Von
//     The timer is initialized by setting the DOSC bit to zero to enable
//     the clock oscillator. Some test patterns are written and read 
//     back from the RTC registers to verify access. Once read/write 
//     access is confirmed, the actual time and date ranges are checked.
//     Ranges used are as following:
//     
//     day of week = 1-7
//     month       = 1-12
//     day         = 1-max day for the month    (note: Feb = 29)
//     format      = 12,24 hr
//     hour        = 0-12 (0-23 for 24 hour format)
//     minute      = 0-59
//     second      = 0-59
//
//     If the any of the settings are invalid, initialize the clock to 
//     default time/date and return a MINOR failure.  $[11021]
//  >Voff
//---------------------------------------------------------------------
//@ PreCondition
//
//  On the GUI CPU, a valid time/date is assumed to exist
//  On the BD CPU, only the ability to set a time is assumed.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

Criticality TimeOfDayClockTestXena2 (void)
{
	//TODO E600 port

    //write to the FPGA counter
/*
    *FPGA_CTR = 0;
    //refer to Delay::WaitForTicks() comments for how 10000 is derived
    const int TIME_OF_DAY_TICK_LOOP_COUNT = 10000; 

    //this is similar to how Delay::WaitForTicks() works
    int i;
    for (  i = TIME_OF_DAY_TICK_LOOP_COUNT
           ; i && (*FPGA_CTR < 2)
        ; i-- )
    {
        //do nothing
    }

    //report failure if i reached 0
    //since the FPGA_CTR gets used in the WaitForTicks() which
    //is used during the PbTimerTest, this is a pre-test to 
    //confirm that the counter is indeed functional
    if (!i)
    {
        return MAJOR;
    }

    tptrXena2 = (STM48T58RegMap*)STM48T58_BASE;
    STM48T58RegMap t;

    tptrXena2->disable_real_time_oscillator = 0; //let it run

    // **** stop update take a snapshot and check values in time registers
    tptrXena2->read_bit = 1;
    //TODO E600 replaced t = *tptrXena2; with memcpy
    memcpy(&t, *tptrXena2, sizeof(STM48T58RegMap));

    year           = t.year_ones   +  10 * t.year_tens;
    month          = t.month_ones  +  10 * t.month_tens;
    day            = t.date_ones   +  10 * t.date_tens;

    //always 24hr format
    hour           = t.hour_ones + 10 * (t.hour_tens_lsb + t.hour_tens_msb);

    minute         =  t.minute_ones + 10 * t.minute_tens;
    second         =  t.second_ones + 10 * t.second_tens;

    // **** enable update from this point on
    tptrXena2->read_bit = 0;

    if (   (year                  > 99)
           || (month                 <  1) 
           || (month                 > 12)
           || (day                   <  1)
           || (day                   > max_day[month-1])
           || (hour                  > 23)
           || (minute                > 59)
           || (second                > 59) )
    {
        // $[TI4.1]
        // Time/Data is not valid so initialize the clock
        InitializeTODXena2();
        return MINOR;  // $[11021]
    }
*/

    return PASSED;
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: InitializeTODDS1286 (void)
//
//@ Interface-Description
//  Sets the time of day to a default time.  $[11022]
//    
//  INPUTS: NONE
//
//  OUTPUTS: PASSED
//---------------------------------------------------------------------
//@ Implementation-Description
//
//     Initialize the DS1286 or DS1283 TOD chip to midnight 1995
//     00:00 1/1/1995
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

void InitializeTODDS1286 (void)
{
	//TODO E600 port
/*
    tptr = (DS1286RegMap*)DS1286_BASE;

    tptr->disable_real_time_oscillator = 0; // start the oscillator
    tptr->enable_update                = 0; // disable updates

    tptr->second_tenths                = 0;
    tptr->second_hundreths             = 0;

    tptr->second_tens                  = 0;
    tptr->second_ones                  = 0;

    tptr->minute_tens                  = 0;
    tptr->minute_ones                  = 0;

    tptr->time_12_hour                 = 0; // 24 hour mode
    tptr->hour_tens_msb                = 0;
    tptr->hour_tens_lsb                = 0;
    tptr->hour_ones                    = 0;

    tptr->day_of_week                  = 1; // 1/1/95 is Sunday

    tptr->date_tens                    = 0;
    tptr->date_ones                    = 1;

    tptr->month_tens                   = 0;
    tptr->month_ones                   = 1;

    tptr->year_tens                    = 9;
    tptr->year_ones                    = 5;

    UNIT_TEST_POINT(POSTUT_14_5)

    tptr->enable_update                = 1; // enable updates
*/

    // $[TI1]
}

//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: InitializeTODXena2 (void)
//
//@ Interface-Description
//  Sets the time of day to a default time.  $[11022]
//    
//  INPUTS: NONE
//
//  OUTPUTS: PASSED
//---------------------------------------------------------------------
//@ Implementation-Description
//
//     Initialize the STM48T58 TOD chip to midnight 1995
//     00:00 1/1/1995
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================

void InitializeTODXena2(void)
{
	//TODO E600 port
/*
    tptrXena2 = (STM48T58RegMap*)STM48T58_BASE;

    tptrXena2->disable_real_time_oscillator = 0; // start the oscillator
    tptrXena2->write_bit 					= 1; // disable updates to write

    tptrXena2->second_tens                  = 0;
    tptrXena2->second_ones                  = 0;

    tptrXena2->minute_tens                  = 0;
    tptrXena2->minute_ones                  = 0;

    tptrXena2->hour_tens_msb                = 0;
    tptrXena2->hour_tens_lsb                = 0;
    tptrXena2->hour_ones                    = 0;

    tptrXena2->day_of_week                  = 1; // 1/1/95 is Sunday

    tptrXena2->date_tens                    = 0;
    tptrXena2->date_ones                    = 1;

    tptrXena2->month_tens                   = 0;
    tptrXena2->month_ones                   = 1;

    tptrXena2->year_tens                    = 9;
    tptrXena2->year_ones                    = 5;

    tptrXena2->write_bit 					= 0; // write and enable updates
*/

    // $[TI1]
}


#ifdef SIGMA_DEBUG
//============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
//@ Free-Function: DisplayTOD (void)
//
//@ Interface-Description
//     Displays TOD to debug device
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  >Von
//  
//     This routine is intended for development purposes to display all
//     the registers of the TOD chip to a debug port.  This includes:
//
//     Day:              
//     Date:             
//     Time:             
//     Time AM/PM:       
//     24 Hour Format    
//     Alarm Day:        
//     Alarm Time:       
//     Alarm AM/PM:      
//     Alarm 24 Hour     
//     disable osc.:     
//     disable sq wave:  
//     Enable Update:    
//     Interrupt Mode:   
//     Mask watchdog:    
//     Mask TOD alarm:   
//     Watchdog seen:    
//     TOD Alarm seen:   
//     WatchDog Interval:
//
//  >Voff
// 
//    NOTE: This chopping up of the srting into individual lines is
//          needed because the Applied Microsystems print buffer
//          is not large enough to hold the entire string.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Free-Function
//=====================================================================
Uint16 errno;

void  DisplayTOD(void)
{
    if (!CpuDevice::IsXena2Config())
    {
        register volatile DS1286RegMap *m_time_of_day = (DS1286RegMap*)DS1286_BASE;
        DS1286RegMap tod;
        char tod_string[16][64];
        Uint8 save_update = m_time_of_day->enable_update;

        m_time_of_day->enable_update = 0;
        tod = *m_time_of_day;
        // **** set it back to it's entry value
        m_time_of_day->enable_update = save_update;

        sprintf(tod_string[0],"Day:                       %1.1d"
                , tod.day_of_week);

        sprintf(tod_string[1],"Date:                      %2.2d/%2.2d/%2.2d"
                , tod.month_ones + 10 * tod.month_tens
                , tod.date_ones  + 10 * tod.date_tens
                , tod.year_ones  + 10 * tod.year_tens);

        sprintf(tod_string[2],"Time:                      %2.2d:%2.2d:%2.2d.%1.1d%1.1d"
                , tod.time_12_hour ?
                tod.hour_ones + 10 *  tod.hour_tens_lsb
                : tod.hour_ones + 10 * (tod.hour_tens_lsb + 2 * tod.hour_tens_msb)
                , tod.minute_ones + 10 * tod.minute_tens
                , tod.second_ones + 10 * tod.second_tens
                , tod.second_tenths
                , tod.second_hundreths);

        sprintf(tod_string[3],"Time AM/PM:                %s"
                , tod.time_12_hour ? (tod.hour_tens_msb ? "PM" : "AM") : "24 Hr.");

        sprintf(tod_string[4],"Alarm Day:                 %s%d"
                , tod.alarm_day_of_week_mask ? "+" : "-"
                , tod.alarm_day_of_week);

        sprintf(tod_string[5],"Alarm Time:                %s%2.2d:%s%2.2d"
                , tod.alarm_hour_mask        ? "+" : "-"
                , tod.alarm_time_12_hour ?
                tod.alarm_hour_ones + 10 *  tod.alarm_hour_tens_lsb
                : tod.alarm_hour_ones + 10 * (tod.alarm_hour_tens_lsb + 2 * tod.alarm_hour_tens_msb)
                , tod.alarm_minute_mask      ? "+" : "-"
                , tod.alarm_minute_ones + 10 * tod.alarm_minute_tens);

        sprintf(tod_string[6],"Alarm AM/PM:               %s"
                , tod.alarm_time_12_hour ? (tod.alarm_hour_tens_msb ? "PM" : "AM") : "24 Hr.");

        sprintf(tod_string[7],"disable osc.:              %1.1d"
                , tod.disable_real_time_oscillator);

        sprintf(tod_string[8],"disable sq wave:           %1.1d"
                , tod.disable_squarewave_output);

        sprintf(tod_string[9],"Enable Update: (at Entry)  %1.1d"
                , save_update);

        sprintf(tod_string[10],"Interrupt Mode:            %1.1d"
                , tod.interrupt_mode);

        sprintf(tod_string[11],"Mask watchdog:             %1.1d"
                , tod.mask_watchdog_alarm);

        sprintf(tod_string[12],"Mask TOD alarm:            %1.1d"
                , tod.mask_time_of_day_alarm);

        sprintf(tod_string[13],"Watchdog seen:             %1.1d"
                , tod.watchdog_alarm);

        sprintf(tod_string[14],"TOD Alarm seen:            %1.1d"
                , tod.time_of_day_alarm);

        sprintf(tod_string[15],"WatchDog Interval:         %1.1d%1.1d.%1.1d%1.1d"
                , tod.watchdog_tens
                , tod.watchdog_ones
                , tod.watchdog_tenths
                , tod.watchdog_hundreths);
    }
}
#endif // SIGMA_DEBUG
