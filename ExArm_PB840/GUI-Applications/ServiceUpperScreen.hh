#ifndef ServiceUpperScreen_HH
#define ServiceUpperScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceUpperScreen - The base container for all upper screen containers.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceUpperScreen.hhv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//    
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"
#include "GuiEventTarget.hh"

//@ Usage-Classes
#include "ServiceTitleArea.hh"
#include "ServiceUpperScreenSelectArea.hh"
#include "UpperSubScreenArea.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ServiceUpperScreen : public Container, public GuiEventTarget
{
public:
	ServiceUpperScreen(void);
	~ServiceUpperScreen(void);

	// Event handlers for the SST or Service mode
	void beginSst(void);
	void beginServiceMode(void);
	
	inline ServiceTitleArea		*getServiceTitleArea(void);
	inline UpperSubScreenArea	*getUpperSubScreenArea(void);
	inline ServiceUpperScreenSelectArea	*getServiceUpperScreenSelectArea(void);

	// GuiEventTarget virtual method
	virtual void guiEventHappened(GuiApp::GuiAppEvent eventId);

	// Initialize the Service Upper Screen memory area.
	static inline void Initialize(void);

	static void SoftFault(const SoftFaultID softFaultID,
							     const Uint32      lineNumber,
							     const char*       pFileName  = NULL, 
							     const char*       pPredicate = NULL);
	// Reference to ServiceLowerScreen object.
	static ServiceUpperScreen &RServiceUpperScreen;

protected:

private:
	// these methods are purposely declared, but not implemented...
	ServiceUpperScreen(const ServiceUpperScreen&);		// not implemented...
	void operator=(const ServiceUpperScreen&);		// not implemented...

	//@ Type: ServiceUpperScreenId
	// Lists the possible screen layout scenarios.
	enum ServiceUpperScreenId
	{
		SERVICE_NO_SCREEN,
		BEGIN_SST_MODE,
		BEGIN_SERVICE_MODE,
		SST_RESULT_MODE,
		DIAG_RESULT_MODE,
		SERVICE_ALARM_LOG_MODE,
		CONFIG_MODE,
		OPERATIONAL_TIME_MODE,
		TEST_SUMMARY_MODE,
		SERVICE_MAX_NUM_SCREEN		
	};

	//@ Data-Member: currentScreen_
	// Flag which indicates which Screen is currently active
	ServiceUpperScreenId currentScreen_;

	//@ Data-Member: serviceUpperScreenSelectArea_
	// The container for ServiceUpperScreenSelectArea_.
	ServiceUpperScreenSelectArea serviceUpperScreenSelectArea_;

	//@ Data-Member: serviceUpperSubScreenArea_
	// The container for ServiceUpperSubScreenArea_.
	UpperSubScreenArea serviceUpperSubScreenArea_;

	//@ Data-Member: serviceTitleArea_
	// The container for ServiceTitleArea_.
	ServiceTitleArea serviceTitleArea_;
};

// Inlined methods
#include "ServiceUpperScreen.in"

#endif // ServiceUpperScreen_HH 
