#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TaskReadyMessage - TaskReady - Sys-Init subsystem class
//---------------------------------------------------------------------
//@ Interface-Description
//  The TaskReadyMessage class is a TaskControlMessage used to respond
//  to ChangeStateMessages.  Upon receiving and processing a 
//  ChangeStateMessage, the receiving task returns a TaskReadyMessage
//  to Task Control.  This informs Task Control that the application
//  task has completed its state transition.  The application normally
//  uses the convenience method TaskControlAgent::TaskReady to 
//  construct and send the TaskReadyMessage.  The Task Control Task
//  uses the TaskReadyMessage to drive its finite state machine to 
//  to proper state once all tasks respond to the state transition.
//  This class uses the AppContext callback mechanism to dispatch the 
//  message to the receiving task's callback function.
//---------------------------------------------------------------------
//@ Rationale
//  The TaskReadyMessage contains information informing of a completed
//  state transition.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is a derived TaskControlMessage.  The base class new and
//  delete operators override the library operators to allocate memory
//  for the message in the global BufferPool.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskReadyMessage.ccv   25.0.4.0   19 Nov 2013 14:33:58   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskReadyMessage.hh"
#include <string.h>

//@ Usage-Classes
#include "AppContext.hh"
#include "ChangeStateMessage.hh"
#include "SigmaState.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskReadyMessage [constructor]
//
//@ Interface-Description
//  The TaskReadyMessage constructor initializes the message to the
//  state, TaskAgent and sequence number contained in the 
//  ChangeStateMessage parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class TaskControlMessage constructor with the 
//  classId for this message.  Sets the internal state_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================


TaskReadyMessage::TaskReadyMessage(const ChangeStateMessage& rChangeState)

    :   TaskControlMessage(Sys_Init::ID_TASKREADYMESSAGE)
      , state_(rChangeState.getState()) 
      , pTaskAgent_(rChangeState.getPTaskAgent())
      , sequenceNumber_(rChangeState.getSequenceNumber())

{
    CALL_TRACE("TaskReadyMessage(ChangeStateMessage)");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TaskReadyMessage [destructor]
//
//@ Interface-Description
//  TaskReadyMessage destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TaskReadyMessage::~TaskReadyMessage()
{
    CALL_TRACE("~TaskReadyMessage()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
TaskReadyMessage::nameOf(void) const
{
    CALL_TRACE("nameOf()");
    switch ( state_ )
    {
        case (STATE_START):           // $[TI1]
            return "TaskReady-Start";
        case (STATE_ONLINE):          // $[TI2]
            return "TaskReady-Online";
        case (STATE_SERVICE):         // $[TI3]
            return "TaskReady-Service";
        case (STATE_SST):             // $[TI4]
            return "TaskReady-SST";
        case (STATE_INOP):            // $[TI5]
            return "TaskReady-Inop";
        case (STATE_TIMEOUT):         // $[TI6]
            return "TaskReady-Timeout";
        case (STATE_INIT):            // $[TI7]
            return "TaskReady-Init";
        default:                      // $[TI8]
            break;
    };
    return "TaskReady-Invalid";
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch
//
//@ Interface-Description
//  The method dispatches this TaskReadyMessage to the callback
//  function registered with the specified AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the AppContext dispatch method with a reference to this
//  TaskReadyMessage.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskReadyMessage::dispatch(const AppContext& context) const
{
    CALL_TRACE("dispatch(const AppContext&)");
    context.dispatch(*this);
    // $[TI1]
}

 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
TaskReadyMessage::SoftFault(const SoftFaultID  softFaultID,
                              const Uint32       lineNumber,
                              const char*        pFileName,
                              const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TASKREADYMESSAGE, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================



