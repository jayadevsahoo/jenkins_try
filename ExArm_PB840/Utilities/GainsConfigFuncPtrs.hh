//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file  GainsConfigFuncPtrs.hh
/// @brief Definition of the GainsConfigFuncPtrs class
//----------------------------------------------------------------------------
#ifndef GAINSCONFIGFUNCPTRS_HH_
#define GAINSCONFIGFUNCPTRS_HH_

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "GainsConfig.hh"
#include "SoftFaultId.hh"
#include "SigmaTypes.hh"

namespace GainsManagerNS
{
    //----------------------------------------------------------------------------
    // C L A S S   D E C L A R A T I O N
    // Declare this class as part of the GainsManagerNS namespace
    //----------------------------------------------------------------------------
    /// @class GainsConfigFuncPtrs
    /// @brief Uses function pointers to get and set gains to controllers.
    ///
    /// This class is useful for handling a miscellaneous group of controllers
    /// that do not have a common base class.
    //-------------------------------------------------------------------------
    class GainsConfigFuncPtrs;
};

//-------------------------------------
// C L A S S    D E F I N I T I O N
//-------------------------------------
class GainsManagerNS::GainsConfigFuncPtrs : public GainsManagerNS::GainsConfig
{
public:
	//--------------------------------------------------------------------
	/// Constructor
	//--------------------------------------------------------------------
    GainsConfigFuncPtrs(void);

    //--------------------------------------------------------------------
    /// Constructor
    ///
    /// param[in] name - the name of the gain to register
    //--------------------------------------------------------------------
	GainsConfigFuncPtrs( GainsManagerNS::GainsNames name );

    //--------------------------------------------------------------------
    /// Copy constructor
    ///
    /// @param[in] cb - GainsConfigFuncPtrs to copy
    //--------------------------------------------------------------------
	GainsConfigFuncPtrs(const GainsConfigFuncPtrs& cb );

    //--------------------------------------------------------------------
    /// Destructor
    //--------------------------------------------------------------------
    virtual ~GainsConfigFuncPtrs(void);

    //--------------------------------------------------------------------
    /// Assignment operator
    ///
    /// @param rhs - Right hand side.
    ///
    /// @return A reference to a GainsConfigFuncPtrs.
    //--------------------------------------------------------------------
    GainsConfigFuncPtrs& operator=(const GainsConfigFuncPtrs &rhs);

    //--------------------------------------------------------------------
    /// @see GainsConfig
    //--------------------------------------------------------------------
    virtual GainsConfigOpStatus distribute(void);

    //--------------------------------------------------------------------
    /// @see GainsConfig
    //--------------------------------------------------------------------
    virtual GainsConfigOpStatus load(void);

    //--------------------------------------------------------------------
    /// @see GainsConfig
    //--------------------------------------------------------------------
    virtual GainsConfigOpStatus setup(void);

private:
	void (*m_setptr)(Real32); /// function pointer to set the gain
	Real32 (*m_getptr)();     /// function pointer to get the gain
};

#endif // GAINSCONFIGFUNCPTRS_HH_
