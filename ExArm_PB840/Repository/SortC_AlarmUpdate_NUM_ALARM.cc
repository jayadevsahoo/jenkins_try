#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SortC_AlarmUpdate_NUM_ALARM - Fixed Sorted List of 'AlarmUpdate' Copies.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a derived class of an abstract, size-independent sorted
//  list of 'AlarmUpdate' items.  This provides an abstraction for a sorted
//  list of copies of data items each of type 'AlarmUpdate'.  The data items
//  of the list are copies of the data items that are added to the list,
//  and are sorted upon insertion.  This is a fixed list where, at most,
//  'NUM_ALARM' items can be on the list at one time.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a sorted collection of sortable objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Items are sorted as they're inserted into the list.  A
//  doubly-linked list of copies is used internally to keep the items.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions namely have to do with the data type that is
//  chosen for this list.  The data type must provide the following
//  mechanisms, to be allowed to be used in this list:
//>Von
//      AlarmUpdate::AlarmUpdate(const AlarmUpdate& aType);            // copy constructor...
//      AlarmUpdate::operator=(const AlarmUpdate& aType);       // assignment operator...
//      AlarmUpdate::operator==(const AlarmUpdate& aType) const;// equivalence operator...
//      AlarmUpdate::operator!=(const AlarmUpdate& aType) const;// inequivalence operator...
//>Voff
//  For primitive (built-in) types these are already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      The assignment operator of any class used as a type of this list
//      must be implemented such that upon completion of an assignment
//      between two instances, the instances are equivalent (i.e.
//      'a = b; POST_CONDITION(a == b);').
//---------------------------------------------------------------------
//@ Invariants
//  (isSorted())
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedSortC.C_v   25.0.4.0   19 Nov 2013 14:03:48   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SortC_AlarmUpdate_NUM_ALARM.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SortC_AlarmUpdate_NUM_ALARM(sortList)  [Copy Constructor]
//
//@ Interface-Description
//  Construct this sorted list using 'sortList'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A reference to this list's element list is passed to the base
//  class's constructor.  The copying of 'sortList' MUST be initiated
//  from this constructor, as opposed to the base class's constructor,
//  because the constructor for 'elemList_' can't run until the base
//  class's constructor is complete.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == sortList  &&  (isEmpty()  ||
//                           currentItem() == sortList.currentItem()))
//@ End-Method
//=====================================================================

SortC_AlarmUpdate_NUM_ALARM::SortC_AlarmUpdate_NUM_ALARM(
				   const SortC_AlarmUpdate_NUM_ALARM& sortList
					    )
				   : SortC_AlarmUpdate(&elemList_),
				     elemList_(sortList.elemList_)
{
  CALL_TRACE("SortC_AlarmUpdate_NUM_ALARM(sortList)");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SortC_AlarmUpdate_NUM_ALARM()  [Default Constructor]
//
//@ Interface-Description
//  Create an empty sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A reference to this list's element list is passed to the base
//  class's constructor.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

SortC_AlarmUpdate_NUM_ALARM::SortC_AlarmUpdate_NUM_ALARM(void)
			      : SortC_AlarmUpdate(&elemList_),
				elemList_()
{
  CALL_TRACE("SortC_AlarmUpdate_NUM_ALARM()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SortC_AlarmUpdate_NUM_ALARM()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SortC_AlarmUpdate_NUM_ALARM::~SortC_AlarmUpdate_NUM_ALARM(void)
{
  CALL_TRACE("~SortC_AlarmUpdate_NUM_ALARM()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSorted()  [const, virtual]
//
//@ Interface-Description
//  This method overrides the base class's pure virtual method, and
//  returns a boolean state indicating whether this list is currently
//  sorted.  Since the items are sorted upon insertion, and there are
//  no methods to access writable versions of the sorted items, this
//  method is always 'TRUE'.  (See 'FixedMutableSortC<AlarmUpdate,NUM_ALARM>' for
//  the ability to change items within a sorted list.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
SortC_AlarmUpdate_NUM_ALARM::isSorted(void) const
{
  CALL_TRACE("isSorted()");

#if defined(SIGMA_UNIT_TEST)
  if (!isEmpty())
  {
    // this sorted list is not empty, therefore check to see if there are
    // any nodes that are out-of-order...
    const DNodeC_AlarmUpdate* const  P_FIRST   = getFirst_();
    const DNodeC_AlarmUpdate*        pCurrNode = P_FIRST;
    const DNodeC_AlarmUpdate*        pNextNode;

    Boolean  isSortedFlag = TRUE;

    // while the nodes are sorted and we haven't looped around to the
    // first node, see if 'pCurrNode' is sorted in relation to the next
    // node ('pNextNode')...
    while (isSortedFlag  &&  (pNextNode = pCurrNode->getNextPtr()) != P_FIRST)
    {
      isSortedFlag = (pCurrNode->getItem()  < pNextNode->getItem()  ||
                      pCurrNode->getItem() == pNextNode->getItem());
      pCurrNode = pNextNode;    // move 'pCurrNode' to the next node...
    }

    // assert that it is sorted...
    CLASS_ASSERTION((isSortedFlag));
  }
#endif // defined(SIGMA_UNIT_TEST)

  return(TRUE);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                      [static]
//
//@ Interface-Description
//      Report the software fault that occured within the source code
//      of this class.  The fault, indicated by 'softFaultID', occured
//      at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//      none
//---------------------------------------------------------------------
//@ PostConditions
//      none
//@ End-Method
//=====================================================================

void
SortC_AlarmUpdate_NUM_ALARM::SoftFault(const SoftFaultID softFaultID,
				 const Uint32      lineNumber,
				 const char*       pFileName,
				 const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, FIXED_SORTC,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr)  [const, virtual]
//
// Interface-Description
//  This method overrides the base class's pure virtual method, and
//  dumps out an ASCII representation of the current state of this
//  list to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
SortC_AlarmUpdate_NUM_ALARM::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "SortC_AlarmUpdate_NUM_ALARM..." << elemList_;

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
