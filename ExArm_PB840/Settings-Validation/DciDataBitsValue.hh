
#ifndef DciDataBitsValue_HH
#define DciDataBitsValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  DciDataBitsValue - Values of DCI's Data Bits Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DciDataBitsValue.hhv   25.0.4.0   19 Nov 2013 14:27:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct DciDataBitsValue
{
  //@ Type:  DciDataBitsValueId
  // All of the possible values of the DCI Data Bits Setting.
  enum DciDataBitsValueId
  {
    // $[01191] -- values of the range of DCI Data Bits Setting...
    BITS_7_VALUE,
    BITS_8_VALUE,

    TOTAL_DATA_BITS_VALUES
  };
};


#endif // DciDataBitsValue_HH 
