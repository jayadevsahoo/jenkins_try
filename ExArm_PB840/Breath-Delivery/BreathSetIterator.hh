
#ifndef BreathSetIterator_HH
#define BreathSetIterator_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BreathSetIterator - Iterator class for BreathSet
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathSetIterator.hhv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003  By:  sp    Date:  9-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"

#include "Breath_Delivery.hh"

//@ Usage-Classes
#include "AveragedBreathData.hh"

class BreathRecord;
//@ End-Usage

class BreathSetIterator {
  public:
    //@ Friend: AveragedBreathData::updateAveragedBreathData(const Int32 msg)
    friend void AveragedBreathData::updateAveragedBreathData(const Int32 msg);

    BreathSetIterator(void);
    ~BreathSetIterator(void);
    void reset(const Int32 msg);
    BreathRecord* getPreviousBreathRecord(void);
    BreathRecord* getPrevRespRateBreathRecord(void);


    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
  protected:

  private:
    BreathSetIterator(const BreathSetIterator&);		// not emplemented...
    void   operator=(const BreathSetIterator&);	// not implemented...


    //@ Data-Member:  index_
    // Frozen copy of BreathSet::CurrentRecord_
    Int32 index_;

    //@ Data-Member:  numRecords_
    // Frozen copy of BreathSet::numRecords_
    Int32 numRecords_;

    //@ Data-Member:  numRespRateRecords_
    // Frozen copy of BreathSet::numRespRateRecords_
    Int32 numRespRateRecords_;
 
    //@ Data-Member:  numTidalVolumeRecords_
    // Frozen copy of BreathSet::numTidalVolumeRecords_
    Int32 numTidalVolumeRecords_;

};


#endif // BreathSetIterator_HH 
