#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: PostAgent - Power-On-Self-Test Agent.
//---------------------------------------------------------------------
//@ Interface-Description
//  Sys-Init instatiates this class in the processor ready messages
//  for the BD and the GUI.  It contains POST data required by the 
//  peer processor's application and provides accessor methods to this
//  encapsulated data.
//---------------------------------------------------------------------
//@ Rationale
//  The PostAgent class encapsulates all access to POST data in a 
//  network object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/POST/vcssrc/PostAgent.ccv   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 005    By: Gary Cederquist  Date: 04-JAN-2000  DR Number: 5588
//    Project: 840 NeoMode
//    Description:
//		Use TimeStamp::GetRNullTimeStamp() instead of global reference.
//
//  Revision: 004    By: sah   Date: 14-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 002    By: Gary Cederquist  Date: 21-APR-1997  DR Number: 1982
//    Project: Sigma (R8027)
//    Description:
//       Changed to accomodate new part number format (length).
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "PostAgent.hh"
#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

//@ Usage-Classes
#include "TimeStamp.hh"
//TODO E600 #include "OsUtil.hh"
#include <string.h>

//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PostAgent [constructor]
//
//@ Interface-Description
//  Default constructor.  Conditionally compiled for either the BD or
//  the GUI since some POST data on the BD is not available on the GUI.
//  It initializes its private data members by accessing static POST 
//  data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
#ifdef SIGMA_BD_CPU
PostAgent::PostAgent(void)
     :powerDownTime_        (Post::GetPowerDownTime())
     ,powerUpTime_          (Post::GetPowerUpTime())
     ,downtimeStatus_       (Post::GetDowntimeStatus())
     ,startupState_         (Post::GetStartupState())
     ,postFailed_           (Post::IsPostFailed())
     ,backgroundFailed_     (Post::GetBackgroundFailed())
     ,serviceModeRequested_ (Post::IsServiceModeRequested())
{
    CALL_TRACE("PostAgent(void)");

    strncpy(  kernelPartNumber_
            , Post::GetKernelPartNumber()
            , sizeof(kernelPartNumber_) );

    // $[TI1]
}
#endif // SIGMA_BD_CPU

#ifdef SIGMA_GUI_CPU
PostAgent::PostAgent(void)
     :powerDownTime_        (TimeStamp::GetRNullTimeStamp())
     ,powerUpTime_          (Post::GetPowerUpTime())
     ,downtimeStatus_       (DOWN_LONG)
     ,startupState_         (Post::GetStartupState())
     ,postFailed_           (Post::IsPostFailed())
     ,backgroundFailed_     (Post::GetBackgroundFailed())
     ,serviceModeRequested_ (FALSE)
{
    CALL_TRACE("PostAgent(void)");

		strncpy(  kernelPartNumber_
            , Post::GetKernelPartNumber()
            , sizeof(kernelPartNumber_) );

    // $[TI2]
}
#endif // SIGMA_GUI_CPU

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PostAgent [destructor]
//
//@ Interface-Description
//  Default destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PostAgent::~PostAgent(void)
{
    CALL_TRACE("~PostAgent(void)");
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PostAgent [copy constructor]
//
//@ Interface-Description
//  Copy constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PostAgent::PostAgent(const PostAgent& rPostAgent)
     :powerDownTime_        (rPostAgent.powerDownTime_)
     ,powerUpTime_          (rPostAgent.powerUpTime_)
     ,downtimeStatus_       (rPostAgent.downtimeStatus_)
     ,postFailed_           (rPostAgent.postFailed_)
     ,backgroundFailed_     (rPostAgent.backgroundFailed_)
     ,serviceModeRequested_ (rPostAgent.serviceModeRequested_)
     ,startupState_         (rPostAgent.startupState_)
{
    CALL_TRACE("PostAgent(const PostAgent&)");

    strncpy(  kernelPartNumber_
            , rPostAgent.kernelPartNumber_
            , sizeof(kernelPartNumber_) );

    // $[TI3]
}

 
#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
PostAgent::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, POST,
                          Post::ID_POSTAGENT,
                          lineNumber, pFileName, pPredicate);
}
 
#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================
