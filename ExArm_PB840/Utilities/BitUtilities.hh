
#ifndef BitUtilities_HH
#define BitUtilities_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  BitUtilities - Bit Operation Utilities.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/BitUtilities.hhv   25.0.4.0   19 Nov 2013 14:35:12   pvcs  $
//
//@ Modifications
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SigmaTypes.hh"

//@ Usage-Class
//@ End-Usage


//@ Begin-Free-Declarations
inline Uint  BitMask  (const Uint bitNum);
inline Uint  BitOffset(const Uint bitNum);

inline Uint  UsedBitsMask(const Uint maxBits);

inline Boolean  IsBitSet(const Uint arrUints[], const Uint bitNum);

inline void  SetBit  (Uint arrUints[], const Uint bitNum);
inline void  ClearBit(Uint arrUints[], const Uint bitNum);

//@ Constant:  BITS_PER_LWORD
// This is a global constant integer that contains the number of bits
// per long word.
static const Uint  BITS_PER_LWORD = (sizeof(Uint) * 8);
//@ End-Free-Declarations

///@detail Creates a bit mask. The mask starts at bit index 'startIndex'
///and will have 'numBits' consecutive 1's. All other bits will be 0.
///The index starts from least signficant bit as 1 (i.e. right most is bit
///is considered to be at index 1 - not at index 0).
///@param numBits - specifies the number of 1's required in the mask
///@param startIndex - specifies the starting index, counting from least
///significant bit as index 1
///@return the requested bit mask
Uint32 CreateBitMask(Uint8 numBits, Uint8 startIndex);

///@detail Extracts a set of bits starting from the specified index. This is
///useful when a long integer has multiple parameters packed into it.
///@param value specifies value from which the bit sequence is to be extracted.
///@param numTotalBits specifies the total number of usable bits in @param value
///@param startIndex specifies where the extraction should start. The indexing
///starts at the least significant bit as 1.
template <class T> 
T ExtractBits(T value, Uint8 numTotalBits, Uint8 startIndex)
{
	T ret = 0;

	Uint8 bitsToShift = 0;
	if(startIndex > numTotalBits)
		bitsToShift = startIndex - numTotalBits;
	//Get integer
	ret = (value & CreateBitMask(numTotalBits, startIndex)) >> bitsToShift;

	return ret;
}

// Inlined Functions...
#include "BitUtilities.in"


#endif  // BitUtilities_HH
