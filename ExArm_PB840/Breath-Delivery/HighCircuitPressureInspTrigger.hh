#ifndef HighCircuitPressureInspTrigger_HH
#define HighCircuitPressureInspTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: HighCircuitPressureInspTrigger - Triggers if circuit pressure >=
//        operator set value for the high circuit pressure limit.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/HighCircuitPressureInspTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  sp    Date:  7-Jan-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"

//@ End-Usage

class HighCircuitPressureInspTrigger : public BreathTrigger{
  public:
    HighCircuitPressureInspTrigger(void);
    virtual ~HighCircuitPressureInspTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
  protected:
    virtual Boolean triggerCondition_(void);

  private:
    HighCircuitPressureInspTrigger(const HighCircuitPressureInspTrigger&);  // not implemented...
    void   operator=(const HighCircuitPressureInspTrigger&);  // not implemented...

};


#endif // HighCircuitPressureInspTrigger_HH
