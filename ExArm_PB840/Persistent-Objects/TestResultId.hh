
#ifndef TestResultId_HH
#define TestResultId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Filename:  TestResultId - Test Result ID.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/TestResultId.hhv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
//@ End-Usage


//@ Type:  TestResultId
// Failure level of the tests.
enum TestResultId
{
  // these IDs are purposely ordered from least severe (i.e., none) failure
  // to most severe; please maintain this ordering philosophy...
  NOT_APPLICABLE_TEST_RESULT_ID,  // used for GUI tests on BD side...
  UNDEFINED_TEST_RESULT_ID,
  INCOMPLETE_TEST_RESULT_ID,

  NOT_INSTALLED_TEST_ID,
  PASSED_TEST_ID,
  OVERRIDDEN_TEST_ID,
  MINOR_TEST_FAILURE_ID,
  MAJOR_TEST_FAILURE_ID,

  NUM_TEST_RESULT_IDS
};


#endif // TestResultId_HH 
