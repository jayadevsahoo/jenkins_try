
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: NovRamSemaphore - NOVRAM Semaphore Class.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/NovRamSemaphore.inv   25.0.4.0   19 Nov 2013 14:18:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  IsBlockingEnabled()  [static]
//
//@ Interface-Description
//  Is blocking on the semaphores enabled?
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

inline Boolean
NovRamSemaphore::IsBlockingEnabled(void)
{
  CALL_TRACE("EnableBlocking()");
  return(NovRamSemaphore::BlockingEnabledFlag_);
}  // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  EnableBlocking()  [static]
//
//@ Interface-Description
//  This has the effect of enabling the semaphore checking.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  (NovRamSemaphore::IsBlockingEnabled())
//@ End-Method 
//===================================================================== 

inline void
NovRamSemaphore::EnableBlocking(void)
{
  CALL_TRACE("EnableBlocking()");
  NovRamSemaphore::BlockingEnabledFlag_ = TRUE;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DisableBlocking()  [static]
//
//@ Interface-Description
//  This has the effect of disabling the semaphore checking.  This is used
//  when a system error (e.g., assertion failure) occurs, and we don't
//  want to be blocked from getting to the System Diagnostic Log, etc.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  (!NovRamSemaphore::IsBlockingEnabled())
//@ End-Method 
//===================================================================== 

inline void
NovRamSemaphore::DisableBlocking(void)
{
  CALL_TRACE("DisableBlocking()");
  NovRamSemaphore::BlockingEnabledFlag_ = FALSE;
}  // $[TI1]
