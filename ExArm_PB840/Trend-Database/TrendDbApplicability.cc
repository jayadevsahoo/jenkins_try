#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================
// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbApplicability
//---------------------------------------------------------------------
//@ Interface-Description
//  This class interfaces with the TrendDbSettingRepository and 
//  TrendDbPatientRepository classes by calling their respective
//  IsApplicable() functions.
//  TrendDbPatientRepository also calls UpdateSettings() when the context
//  is modified.
//---------------------------------------------------------------------
//@ Rationale
// This class provides the logic to determine if a given trended parameter
// is valid within the current settings context.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
// There are 5 settings that affect the validity of the trended parameters:
// mode, ventilatory type (IV or NIV), trigger type, spont breath type, 
// and mand breath type.
// Each trended parameter gets a bit vector for each of these 5 settings.
// The bit vector contains the valid settings for the particular trended
// parameter.
// The current setting context is extracted from these 5 settings whenever
// any one of them changes. A local version of the current context is maintained
// in this class and is updated through a call to UpdateSettings().
// To determine whether a given trended parameter is valid, the parameter's
// bit-vectors are compared with the current setting context through a call
// to isApplicable(). There are 2 overloaded versions of isApplicable(), 1
// for settingIds and 1 for patientDataIds.
// 
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
// The isApplicable() function overloaded for patient data is only used for 
// bounded data. Non-bounded patient data is not currently trended, and is 
// not thought to ever need to be trended. Thus, the logic to test the validity
// of non-bounded patient data was removed.
// 
//---------------------------------------------------------------------
//@ Invariants
// 
//---------------------------------------------------------------------
//@ End-Preamble
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Trend-Database/vcssrc/TrendDbApplicability.ccv   10.7   08/17/07 11:01:44   pvcs  
//
// @ Modification-Log
//
//  Revision: 007   By: rhj   Date: 25-Oct-2010     SCR Number: 6603
//  Project:  PROX
//  Description:
//      Added the ability to log prox enable and disable events.
// 
//  Revision: 006   By: rpr    Date: 26-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support code review comments.
// 
//  Revision: 005   By: rpr    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 004   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 003   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 002  By:  rhj    Date:  20-Jul-2007    SCR Number: 6388
//       Project:  Trend
//       Description:
//             Enabled Vti in BILEVEL + TC
//
//  Revision: 001  By:  ksg    Date:  17-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//====================================================================
#include "TrendDbApplicability.hh"
#include "TrendEvents.hh"
#include "TrendDataMgr.hh"
#include "TrendDbEventReceiver.hh"

#include "ModeValue.hh"
#include "MandTypeValue.hh"
#include "SupportTypeValue.hh"
#include "TriggerTypeValue.hh"
#include "VentTypeValue.hh"
#include "Fio2EnabledValue.hh"
#include "LeakCompEnabledValue.hh"

#include "SettingContextHandle.hh"
#include "MathUtilities.hh"
#include "ProxEnabledValue.hh"



// Bit Vector definitions
// mode type
static const Uint8 SIMV_BIT = 1;
static const Uint8 SPONT_BIT = 2; 
static const Uint8 BI_LEVEL_BIT = 4;
static const Uint8 AC_BIT = 8;
static const Uint8 CPAP_BIT = 16;

// ventilation type
static const Uint8 NIV_BIT = 1;
static const Uint8 IV_BIT = 2;

// trigger type 
static const Uint8 PRESS_TRIG_BIT = 1;
static const Uint8 FLOW_TRIG_BIT = 2;

// mand breath type
static const Uint8 PC_BIT = 1;
static const Uint8 VC_PLUS_BIT = 2;
static const Uint8 VC_BIT = 4;

// spont breath type
static const Uint8 PA_BIT = 1;
static const Uint8 TC_BIT = 2;
static const Uint8 VS_BIT = 4;
static const Uint8 PSV_BIT = 8;
static const Uint8 OFF_BIT = 16;



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendFractionalDigits()  [Constructor]
//
//@ Interface-Description
// Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This function sets the bit vectors that determine context validity
//  for each trended parameter.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
TrendDbApplicability::TrendDbApplicability(void)    
{
	Uint8 i = 0;

	for (i = 0;i < SettingId::TOTAL_SETTING_IDS; i++)
	{
		applicabilitySettingArray_[i].mode = 0xFF;
		applicabilitySettingArray_[i].ventilationType =0xFF;
		applicabilitySettingArray_[i].triggerType = 0xFF;
		applicabilitySettingArray_[i].mandBreathType =0xFF;
		applicabilitySettingArray_[i].spontBreathType = 0xFF;
	}
	for (i = 0;i < PatientDataId::NUM_TOTAL_PATIENT_DATA; i++)
	{
		applicabilityPatientArray_[i].mode = 0xFF;
		applicabilityPatientArray_[i].ventilationType =0xFF;
		applicabilityPatientArray_[i].triggerType = 0xFF;
		applicabilityPatientArray_[i].mandBreathType =0xFF;
		applicabilityPatientArray_[i].spontBreathType = 0xFF;
	}


	// Patient-Data restrictions
	applicabilityPatientArray_[PatientDataId::DYNAMIC_COMPLIANCE_ITEM].mode = ~BI_LEVEL_BIT;
	applicabilityPatientArray_[PatientDataId::DYNAMIC_COMPLIANCE_ITEM].ventilationType = IV_BIT;

	applicabilityPatientArray_[PatientDataId::DYNAMIC_RESISTANCE_ITEM].mode = ~BI_LEVEL_BIT;
	applicabilityPatientArray_[PatientDataId::DYNAMIC_RESISTANCE_ITEM].ventilationType = IV_BIT;

	applicabilityPatientArray_[PatientDataId::END_EXPIRATORY_FLOW_ITEM].mode = ~BI_LEVEL_BIT;
	applicabilityPatientArray_[PatientDataId::END_EXPIRATORY_FLOW_ITEM].ventilationType = IV_BIT;

	applicabilityPatientArray_[PatientDataId::PEAK_EXPIRATORY_FLOW_ITEM].mode = ~BI_LEVEL_BIT;
	applicabilityPatientArray_[PatientDataId::PEAK_EXPIRATORY_FLOW_ITEM].ventilationType = IV_BIT;

	applicabilityPatientArray_[PatientDataId::PEAK_SPONT_INSP_FLOW_ITEM].ventilationType = IV_BIT;
	applicabilityPatientArray_[PatientDataId::PEAK_SPONT_INSP_FLOW_ITEM].mode = ~( BI_LEVEL_BIT | AC_BIT );

	applicabilityPatientArray_[PatientDataId::PAV_PEEP_INTRINSIC_ITEM].mode = ~BI_LEVEL_BIT;
	applicabilityPatientArray_[PatientDataId::PAV_PEEP_INTRINSIC_ITEM].ventilationType = IV_BIT;
	applicabilityPatientArray_[PatientDataId::PAV_PEEP_INTRINSIC_ITEM].spontBreathType = PA_BIT;
	applicabilityPatientArray_[PatientDataId::PAV_PEEP_INTRINSIC_ITEM].mandBreathType = 0;

	// WOBtot needs to be gapped when PA is not the active Spont type
	applicabilityPatientArray_[PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM].mode = SPONT_BIT;
	applicabilityPatientArray_[PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM].spontBreathType = PA_BIT;
	applicabilityPatientArray_[PatientDataId::TOTAL_WORK_OF_BREATHING_ITEM].mandBreathType = 0;

	applicabilityPatientArray_[PatientDataId::PEEP_INTRINSIC_ITEM].mode = ~(SPONT_BIT | CPAP_BIT);
    
	applicabilityPatientArray_[PatientDataId::PEEP_TOTAL_ITEM].mode = ~(SPONT_BIT | CPAP_BIT);
    
	applicabilityPatientArray_[PatientDataId::EXH_SPONT_MINUTE_VOL_ITEM].mode = ( SIMV_BIT | SPONT_BIT | BI_LEVEL_BIT);

	applicabilityPatientArray_[PatientDataId::EXH_SPONT_TIDAL_VOL_ITEM].mode = (SIMV_BIT | SPONT_BIT | BI_LEVEL_BIT );

	applicabilityPatientArray_[PatientDataId::INSP_TIDAL_VOL_ITEM].mode = ~AC_BIT;
	applicabilityPatientArray_[PatientDataId::INSP_TIDAL_VOL_ITEM].mandBreathType = ~VC_BIT;

	applicabilityPatientArray_[PatientDataId::PAV_LUNG_COMPLIANCE_ITEM].spontBreathType = PA_BIT;
	applicabilityPatientArray_[PatientDataId::PAV_LUNG_COMPLIANCE_ITEM].mandBreathType = 0;

	applicabilityPatientArray_[PatientDataId::PAV_PATIENT_RESISTANCE_ITEM].spontBreathType = PA_BIT;
	applicabilityPatientArray_[PatientDataId::PAV_PATIENT_RESISTANCE_ITEM].mandBreathType = 0;

	applicabilityPatientArray_[PatientDataId::PAV_LUNG_ELASTANCE_ITEM].spontBreathType = PA_BIT;
	applicabilityPatientArray_[PatientDataId::PAV_LUNG_ELASTANCE_ITEM].mandBreathType = 0;

	applicabilityPatientArray_[PatientDataId::SPONT_RATE_TO_VOLUME_RATIO_ITEM].mode = ( SPONT_BIT | CPAP_BIT );

	applicabilityPatientArray_[PatientDataId::SPONT_PERCENT_TI_ITEM].mode = ( SPONT_BIT | CPAP_BIT );

	applicabilityPatientArray_[PatientDataId::SPONT_INSP_TIME_ITEM].mode = ( SIMV_BIT | SPONT_BIT | CPAP_BIT | BI_LEVEL_BIT);

	applicabilityPatientArray_[PatientDataId::TOTAL_AIRWAY_RESISTANCE_ITEM].spontBreathType = PA_BIT;
	applicabilityPatientArray_[PatientDataId::TOTAL_AIRWAY_RESISTANCE_ITEM].mandBreathType = 0;



	// Setting Restrictions
	applicabilitySettingArray_[SettingId::EXP_SENS].mode = (SIMV_BIT | SPONT_BIT | CPAP_BIT | BI_LEVEL_BIT);

	applicabilitySettingArray_[SettingId::FLOW_ACCEL_PERCENT].mandBreathType = (PC_BIT | VC_PLUS_BIT);
	applicabilitySettingArray_[SettingId::FLOW_ACCEL_PERCENT].spontBreathType = (OFF_BIT | PSV_BIT | VS_BIT);

	applicabilitySettingArray_[SettingId::FLOW_SENS].triggerType = FLOW_TRIG_BIT;

	applicabilitySettingArray_[SettingId::HL_RATIO].mode = (BI_LEVEL_BIT);

	applicabilitySettingArray_[SettingId::IE_RATIO].mode = ~( BI_LEVEL_BIT | SPONT_BIT | CPAP_BIT);
	applicabilitySettingArray_[SettingId::IE_RATIO].mandBreathType = ( PC_BIT | VC_PLUS_BIT );
	applicabilitySettingArray_[SettingId::IE_RATIO].spontBreathType = 0;

	applicabilitySettingArray_[SettingId::INSP_PRESS].mode = ~BI_LEVEL_BIT;
	applicabilitySettingArray_[SettingId::INSP_PRESS].mandBreathType = PC_BIT;
	applicabilitySettingArray_[SettingId::INSP_PRESS].spontBreathType = 0;

	applicabilitySettingArray_[SettingId::INSP_TIME].mode = ~BI_LEVEL_BIT;
	applicabilitySettingArray_[SettingId::INSP_TIME].mandBreathType = (PC_BIT | VC_PLUS_BIT);
	applicabilitySettingArray_[SettingId::INSP_TIME].spontBreathType = 0;

	applicabilitySettingArray_[SettingId::PEAK_INSP_FLOW].mandBreathType = VC_BIT;
	applicabilitySettingArray_[SettingId::PEAK_INSP_FLOW].spontBreathType = 0;

	applicabilitySettingArray_[SettingId::PEEP].mode = ~BI_LEVEL_BIT;

	applicabilitySettingArray_[SettingId::PEEP_HIGH].mode = BI_LEVEL_BIT;

	applicabilitySettingArray_[SettingId::PEEP_HIGH_TIME].mode = BI_LEVEL_BIT;

	applicabilitySettingArray_[SettingId::PEEP_LOW].mode = BI_LEVEL_BIT;

	applicabilitySettingArray_[SettingId::PEEP_LOW_TIME].mode = BI_LEVEL_BIT;

	applicabilitySettingArray_[SettingId::PERCENT_SUPPORT].spontBreathType = ( TC_BIT | PA_BIT );
	applicabilitySettingArray_[SettingId::PERCENT_SUPPORT].mandBreathType = 0;

	applicabilitySettingArray_[SettingId::PLATEAU_TIME].mandBreathType = ( VC_BIT );
	applicabilitySettingArray_[SettingId::PLATEAU_TIME].spontBreathType = 0;

	applicabilitySettingArray_[SettingId::PRESS_SENS].triggerType = PRESS_TRIG_BIT;

	applicabilitySettingArray_[SettingId::PRESS_SUPP_LEVEL].spontBreathType = PSV_BIT;
	applicabilitySettingArray_[SettingId::PRESS_SUPP_LEVEL].mandBreathType = 0;

	applicabilitySettingArray_[SettingId::RESP_RATE].mode = ( AC_BIT | SIMV_BIT | BI_LEVEL_BIT);

	applicabilitySettingArray_[SettingId::TIDAL_VOLUME].mandBreathType = ( VC_BIT | VC_PLUS_BIT );
	applicabilitySettingArray_[SettingId::TIDAL_VOLUME].spontBreathType = 0;

	applicabilitySettingArray_[SettingId::VOLUME_SUPPORT].spontBreathType = VS_BIT;
	applicabilitySettingArray_[SettingId::VOLUME_SUPPORT].mandBreathType = 0;

    proxSetting_ = SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											             SettingId::PROX_ENABLED);

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendDbApplicability()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendDbApplicability::~TrendDbApplicability(void)   
{
	CALL_TRACE("TrendDbApplicability::~TrendDbApplicability(void)");

	// Do nothing
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpdateSettings
//
//@ Interface-Description
// Retrieves updated setting context for settings that affect
// applicability. Posts auto events indicating when settings have 
// changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Saves context upon entering function. Retrieves and saves new 
//  context from   Settings-Validation subsystem. The updated 
//  context is compared to the saved context and auto events are 
//  generated where there is a discrepancy.
//  In addition to a mode change auto event, a mode change also 
//  generates an auto-event indicating the current mand breath type
//  and, if applicable, the spontaneous breath type.
// 
// Satisfies requirement: [[TR011179] The system shall automatically 
// generate the following events....
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendDbApplicability::updateSettings(void)
{
	// previous settings used to determine auto events
	Uint8 oldMode = mode_;
	Uint8 oldVentType = ventType_;
	Uint8 oldSpontBreathType = spontBreathType_;
	Uint8 oldMandBreathType = mandBreathType_;

	// retrieve updated accepted setting information
	const DiscreteValue  MODE_VALUE = 
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::MODE);
	const DiscreteValue  VENT_TYPE_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::VENT_TYPE);
	const DiscreteValue  TRIGGER_TYPE_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::TRIGGER_TYPE);
	const DiscreteValue  SUPPORT_TYPE_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::SUPPORT_TYPE);
	const DiscreteValue  MAND_TYPE_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::MAND_TYPE);
	const DiscreteValue  FIO2_ENABLED_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::FIO2_ENABLED);
	const DiscreteValue  LEAK_COMP_ENABLED_VALUE =
	SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
										  SettingId::LEAK_COMP_ENABLED);

	switch (MODE_VALUE)
	{
	case ModeValue::AC_MODE_VALUE:                  
		mode_ = AC_BIT;
		if (oldMode != AC_BIT)
		{
			// force Mand breath type auto event
			oldMandBreathType = 0; 
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_MODE_AC);
		}
		break;
	case ModeValue::SIMV_MODE_VALUE:
		mode_ = SIMV_BIT;
		if (oldMode != SIMV_BIT)
		{
			// force Mand/Spont breath type auto event
			oldSpontBreathType = 0;
			oldMandBreathType = 0;
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_MODE_SIMV);
		}
		break;
	case ModeValue::SPONT_MODE_VALUE:
		mode_ = SPONT_BIT;
		if (oldMode != SPONT_BIT)
		{
			// force Mand/Spont breath type auto event
			oldSpontBreathType = 0;
			oldMandBreathType = 0;
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_MODE_SPONT);
		}
		break;
	case ModeValue::BILEVEL_MODE_VALUE:
		mode_ = BI_LEVEL_BIT;
		if (oldMode != BI_LEVEL_BIT)
		{
			// force Mand/Spont breath type auto event
			oldSpontBreathType = 0;
			oldMandBreathType = 0;
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_MODE_BILEVEL);
		}
		break;
	case ModeValue::CPAP_MODE_VALUE:
		mode_ = CPAP_BIT;
		if (oldMode != CPAP_BIT)
		{
			// force Mand/Spont breath type auto event
			oldSpontBreathType = 0;
			oldMandBreathType = 0;
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_MODE_CPAP);
		}
		break;
	default:
		// Invalid Mode Setting
		AUX_CLASS_ASSERTION_FAILURE( MODE_VALUE );                     
		break;
	}

	switch (VENT_TYPE_VALUE)
	{
	case VentTypeValue::NIV_VENT_TYPE:
		ventType_ = NIV_BIT;
		if (oldVentType != NIV_BIT)
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_VENTILATION_NIV);
		}
		break;
	case VentTypeValue::INVASIVE_VENT_TYPE:
		ventType_ = IV_BIT;
		if (oldVentType != IV_BIT)
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_VENTILATION_INVASIVE);
		}
		break;
	default:
		// Invalid VentType Setting
		AUX_CLASS_ASSERTION_FAILURE( VENT_TYPE_VALUE );
		break;
	}


	switch (FIO2_ENABLED_VALUE)
	{
	case Fio2EnabledValue::NO_FIO2_ENABLED:
	case Fio2EnabledValue::YES_FIO2_ENABLED:   
	case Fio2EnabledValue::CALIBRATE_FIO2:   
		fio2Setting_ = FIO2_ENABLED_VALUE;
		break;
	default:
		// Invalid FiO2 Setting
		AUX_CLASS_ASSERTION_FAILURE( FIO2_ENABLED_VALUE );    
		break;
	}

	switch (LEAK_COMP_ENABLED_VALUE)
	{
	case LeakCompEnabledValue::LEAK_COMP_DISABLED:
	case LeakCompEnabledValue::LEAK_COMP_ENABLED:
		leakCompEnabledSetting_ = LEAK_COMP_ENABLED_VALUE;
		break;
	default:
		// Invalid Leak Compensation Setting
		AUX_CLASS_ASSERTION_FAILURE( LEAK_COMP_ENABLED_VALUE );    
		break;
	}

	switch (TRIGGER_TYPE_VALUE)
	{
	case TriggerTypeValue::FLOW_TRIGGER:
		triggerType_ = FLOW_TRIG_BIT;
		break;
	case TriggerTypeValue::PRESSURE_TRIGGER:
		triggerType_ = PRESS_TRIG_BIT;
		break;
	default:
		// Invalid Trigger Setting
		AUX_CLASS_ASSERTION_FAILURE( TRIGGER_TYPE_VALUE );    
		break;  
	}

	switch (MAND_TYPE_VALUE)
	{
	case MandTypeValue::PCV_MAND_TYPE:
		mandBreathType_ = PC_BIT;
		if (oldMandBreathType != PC_BIT)
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_MANDATORY_PC);
		}
		break;
	case MandTypeValue::VCV_MAND_TYPE:
		mandBreathType_ = VC_BIT;
		if (oldMandBreathType != VC_BIT)
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_MANDATORY_VC);
		}
		break;
	case MandTypeValue::VCP_MAND_TYPE:
		mandBreathType_ = VC_PLUS_BIT;
		if (oldMandBreathType != VC_PLUS_BIT)
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_MANDATORY_VC_PLUS);
		}
		break;
	default:
		// Invalid Mandatory Type Setting
		AUX_CLASS_ASSERTION_FAILURE( MAND_TYPE_VALUE );    
		break;
	}

	switch (SUPPORT_TYPE_VALUE)
	{
	// spont breath type not applicable in A/C mode. Suppress auto event
	case SupportTypeValue::PSV_SUPPORT_TYPE:
		spontBreathType_ = PSV_BIT;

		if ((oldSpontBreathType != PSV_BIT) && (mode_ != AC_BIT))
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_SPONTANEOUS_PS);
		}
		break;
	case SupportTypeValue::OFF_SUPPORT_TYPE:
		spontBreathType_ = OFF_BIT;
		if ((oldSpontBreathType != OFF_BIT) && (mode_ != AC_BIT))
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_SPONTANEOUS_NONE);
		}
		break;
	case SupportTypeValue::ATC_SUPPORT_TYPE:
		spontBreathType_ = TC_BIT;
		if ((oldSpontBreathType != TC_BIT) && (mode_ != AC_BIT))
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_SPONTANEOUS_TC);
		}
		break;
	case SupportTypeValue::VSV_SUPPORT_TYPE:
		spontBreathType_ = VS_BIT;
		if ((oldSpontBreathType != VS_BIT) && (mode_ != AC_BIT))
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_SPONTANEOUS_VS);
		}
		break;
	case SupportTypeValue::PAV_SUPPORT_TYPE:
		spontBreathType_ = PA_BIT;
		if ((oldSpontBreathType != PA_BIT) && (mode_ != AC_BIT))
		{
			TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_SPONTANEOUS_PA);
		}
		break;
	default:
		// Invalid Support Type Setting
		AUX_CLASS_ASSERTION_FAILURE( SUPPORT_TYPE_VALUE );    
		break;
	}


    const DiscreteValue PROX_ENABLED_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,
											  SettingId::PROX_ENABLED);

    // [PX04003] Post related trending event 
    if (proxSetting_ != PROX_ENABLED_VALUE)
    {
        if( PROX_ENABLED_VALUE ==  ProxEnabledValue::PROX_DISABLED)
        {
           TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_PROX_DISABLED);
        }
        else
        {
           TrendDataMgr::PostEvent(TrendEvents::AUTO_EVENT_PROX_ENABLED);
        }

        // Store the current setting.
        proxSetting_ = PROX_ENABLED_VALUE;
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isApplicable
//
//@ Interface-Description
//  Determines if the settingId is applicable in the current context
//  returns TRUE if setting is applicable in current context
//  returns FALSE otherwise
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Compares the current context with the bit vectors (set above) that 
//  represent the valid context of a given setting.
//  
//  Satisfies Requirements:
//  [[TR01127] Trending shall display Gaps for Trend Parameters that are
//  not applicable...
//  [[TR01203] Trending shall display Gaps for samples acquired during
//  Apnea Ventilation
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean TrendDbApplicability::isApplicable(SettingId::SettingIdType settingId) const
{
	const TrendDbEventReceiver & eventReceiver = TrendDataMgr::GetEventReceiver(); 
	Boolean isApplicable = FALSE;
	if ( (eventReceiver.isPatientApnea() ) ||
		 (!eventReceiver.isPatientConnected() ) ||
		 (eventReceiver.isPatientCircuitOccluded() ))
	{
		// do not display settings while in Apnea, circuit disconnect, or occlusion
		return isApplicable;
	}
	// check mode context
	if (mode_ & applicabilitySettingArray_[settingId].mode)
	{
		// check trigger type context
		if (triggerType_ & applicabilitySettingArray_[settingId].triggerType)
		{
			// if spont breath type context is valid, must have a spont breath mode
			if ((spontBreathType_ & applicabilitySettingArray_[settingId].spontBreathType) &&
				(mode_ & ( SPONT_BIT | CPAP_BIT | SIMV_BIT | BI_LEVEL_BIT )))
			{
				isApplicable = TRUE;
			}
			// otherwise, check mandatory breath mode context
			else if (mandBreathType_ & applicabilitySettingArray_[settingId].mandBreathType)
			{
				isApplicable = TRUE;
			}
		}
	}

	return isApplicable;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isApplicable
//
//@ Interface-Description
//  Determines if the PatientDataId is applicable in the current context
//  returns TRUE if parameter is applicable in current context
//  returns FALSE otherwise
//---------------------------------------------------------------------
//@ Implementation-Description
//  Compares the current context with the bit vectors (set above) that 
//  represent the valid context of a given patientDataId.
// 
//  Satisfies Requirements:
//  [[TR01127] Trending shall display Gaps for Trend Parameters that are
//  not applicable...
//
// SRS2858 [[TR01203] Trending shall display Gaps for:
//      /a/ Settings during Apnea Ventilation
//      /b/ Settings and Patient Data Values during Occlusion
 //     /c/ Settings and Patient Data Values during Circuit Disconnect.
//
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean TrendDbApplicability::isApplicable(PatientDataId::PatientItemId patientId, 
										   BoundedBreathDatum boundedDatum) const
{
	Boolean isApplicable = FALSE;
	const TrendDbEventReceiver & eventReceiver = TrendDataMgr::GetEventReceiver(); 

	if ((!eventReceiver.isPatientConnected() ) ||
		eventReceiver.isPatientCircuitOccluded() ||  
		boundedDatum.timedOut)
	{
		// do not display patient data while in Occlussion, circuit disconnect, or 
		// patient data timeouts
		return isApplicable;
	}

	switch (patientId)
	{
	case PatientDataId::DELIVERED_O2_PERCENTAGE_ITEM:
		// check if FiO2 is enabled
		if (fio2Setting_ & Fio2EnabledValue::YES_FIO2_ENABLED)
		{
			if (!IsEquivalent(boundedDatum.data.value, -1.0F, ONES))
			{
				// -1 from Bd-Io-Devices means O2 sensor is not calibrated
				// Only display calibrated O2 sensor data
				isApplicable = TRUE;
			}
		}
		break;
	case PatientDataId::PERCENT_LEAK_ITEM:
	case PatientDataId::EXH_LEAK_RATE_ITEM:
	case PatientDataId::INSP_LEAK_VOL_ITEM:
		// Leak, %Leak and iLeak need to be gapped when Leak 
		// Compensation is disabled		
		if (leakCompEnabledSetting_ == LeakCompEnabledValue::LEAK_COMP_ENABLED)
		{
			isApplicable = TRUE;
		}
		break;
	case PatientDataId::INSP_TIDAL_VOL_ITEM:
		if ((mode_ & applicabilityPatientArray_[patientId].mode ) ||
			(mandBreathType_ & applicabilityPatientArray_[patientId].mandBreathType))
		{
			isApplicable = TRUE;
		}
		break;

	default:
		// check mode context
		if (mode_ & applicabilityPatientArray_[patientId].mode)
		{
			// check IV/NIV context
			if (ventType_ & applicabilityPatientArray_[patientId].ventilationType)
			{
				// check trigger type context
				if (triggerType_ & applicabilityPatientArray_[patientId].triggerType)
				{
					// if we are evaluating spontBreathType, must be in a spont breath mode
					if ((spontBreathType_ & applicabilityPatientArray_[patientId].spontBreathType) &&
						(mode_ & ( SPONT_BIT | CPAP_BIT | SIMV_BIT | BI_LEVEL_BIT )))
					{
						isApplicable = TRUE;
					}
					// otherwise just check mandatory breath type context
					else if (mandBreathType_ & applicabilityPatientArray_[patientId].mandBreathType)
					{
						isApplicable = TRUE;
					}

				}
			}
		}

		break;

	} // switch switch (patientId) ...


	return isApplicable;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbApplicability::SoftFault(const SoftFaultID  softFaultID,
									 const Uint32       lineNumber,
									 const char*        pFileName,
									 const char*        pPredicate)  
{
	CALL_TRACE("TrendDbSettingRepository::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_APPLICABILITY,
							lineNumber, pFileName, pPredicate);
}








