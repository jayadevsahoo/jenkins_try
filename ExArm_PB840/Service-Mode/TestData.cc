#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TestData - Compute processed sensor data
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is used to compute processed sensor data for Service
//	Mode tests, e.g. delivered volume.
//---------------------------------------------------------------------
//@ Rationale
//	This class is required to compute processed sensor data necessary
//	for the Service Mode tests.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method newCycle() executes every 10 msecs to recompute the
//	necessary data, e.g. flow integration for delivered volume.
//	The method getDeliveredVolume() returns the volume of delivered
//	gas (air and O2).
//	The method getNetDeliveredVolume() returns the volume of delivered
//	gas (air and O2) minus the volume which exited the exhalation valve.
//	The method resetDeliveredVolume() resets the delivered volume and
//	net delivered volume values to zero.
//	The method calculateFlowOffset() calculates the net offset of the
//	flow sensors.
//---------------------------------------------------------------------
//@ Fault-Handling
//	none
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/TestData.ccv   25.0.4.0   19 Nov 2013 14:23:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  syw    Date: 11-Aug-1997    DR Number: DCS 2356
//       Project:  Sigma (840)
//       Description:
//            Update peakPexh_ data member.
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  21-Aug-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "TestData.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmFlowController.hh"

#include "Task.hh"

#include "BD_IO_Devices.hh"
#include "FlowSensor.hh"
#include "PressureSensor.hh"
#include "ExhFlowSensor.h"
#include "MainSensorRefs.hh"

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_DEVELOPMENT
# include <stdio.h>
#endif  // SIGMA_DEVELOPMENT


//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestData()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.  No arguments.  Initializes private data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the private data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TestData::TestData(void) :
		deliveredVolume_(0.0),
		netDeliveredVolume_(0.0),
		flowOffset_(0.0),
		peakPexh_(0.0),
		aveExhDrvPress_(0.0),
		aveExhPress_(0.0)
{
// $[TI1]
	CALL_TRACE("TestData::TestData(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TestData()  [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

TestData::~TestData(void)
{
	CALL_TRACE("TestData::~TestData(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//    This method takes no arguments and return nothing.  It is used
//    to update the delivered volume and net delivered volume.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Read the air, O2, and exhalation flows, convert to incremental
//    delivered volume and net delivered volume, then add these to
//    to the stored deliveredVolume_ and netDeliveredVolume_ respectively.
//    Read the exhalation pressure and if it exceeds peakPexh_ then
//    store the exh pressure as the new peakPexh_.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
TestData::newCycle(void)
{
	CALL_TRACE("TestData::newCycle(void)");

	const Real32 SECS_PER_MIN = 60.0;
	const Real32 FLOW_TO_VOLUME = SM_CYCLE_TIME / SECS_PER_MIN;
	const Uint32 NUM_OF_PRESS_SAMPLES = 3;

	static Real32 exhDrvPressSum = 0;
	static Real32 exhPressSum = 0;

	// Calculate the average of the latest three samples of the exhalation vavle drv and exhalation
	// valve pressures
	exhDrvPressSum = exhDrvPressSum + RExhDrvPressureSensor.getFilteredValue() - aveExhDrvPress_;
	aveExhDrvPress_ = exhDrvPressSum / NUM_OF_PRESS_SAMPLES;			

	exhPressSum = exhPressSum + RExhPressureSensor.getFilteredValue() - aveExhPress_;
	aveExhPress_ = exhPressSum / NUM_OF_PRESS_SAMPLES;	


	Real32 sumInspFlow = RAirFlowSensor.getValue() +
							RO2FlowSensor.getValue();

	deliveredVolume_ += FLOW_TO_VOLUME * sumInspFlow;  // ml

	Real32 exhFlow;
	if (RSmFlowController.gasUsed() == AIR)
	{
	// $[TI1]
		exhFlow = RExhFlowSensor.getValue();
	}
	else
	{
	// $[TI2]
// E600 BDIO        exhFlow = RExhO2FlowSensor.getValue();
		exhFlow = RExhFlowSensor.getValue();
	}

	netDeliveredVolume_ += FLOW_TO_VOLUME *
		(sumInspFlow - exhFlow - flowOffset_);  // ml


	Real32 exhPressure = RExhPressureSensor.getValue();
	if (exhPressure > peakPexh_)
	{
	// $[TI3]
		peakPexh_ = exhPressure ;
	}
	// $[TI4]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateFlowOffset
//
//@ Interface-Description
//    This method takes no arguments and return nothing.  It is used
//    to calculate the flow offset, which is used to correct the gas
//    volume delivered to a circuit for the compliance calibration.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assume psols are closed.
//
//	Delay then read alpha-filtered air, O2, and exh flows
//	Calculate and store flow offset = (sum of insp flows) - exh flow
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
TestData::calculateFlowOffset(void)
{
// $[TI1]
	CALL_TRACE("TestData::calculateFlowOffset(void)");

	Task::Delay(2);
	Real32 airFlow =  RAirFlowSensor.getFilteredValue();
	Real32 o2Flow =  RO2FlowSensor.getFilteredValue();
	//Real32 exhFlow = RExhFlowSensor.getFilteredValue();
	
	// TODO E600 MS this is temporary...
	Real32 exhFlow = RExhFlowSensor.getValue();

	RETAILMSG ( TRUE, ( L"Air flow: %d\r\t", (Uint32)airFlow ) );
	RETAILMSG ( TRUE, ( L"O2 flow: %d\r\t", (Uint32)o2Flow ) );

	RETAILMSG ( TRUE, ( L"Exh flow: %d\r\t", (Uint32)exhFlow ) );


	Real32 inspFlowSum = airFlow + o2Flow;

	flowOffset_ = inspFlowSum - exhFlow;

#ifdef SIGMA_DEBUG
printf("airFlow = %.3f lpm\n", airFlow);
printf("o2Flow = %.3f lpm\n", o2Flow);
printf("exhFlow = %.3f lpm\n", exhFlow);
printf("flowOffset_ = %.3f lpm\n", flowOffset_);
#endif  // SIGMA_DEBUG

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
TestData::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_MODE, TESTDATA,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
