// FixHTML.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "StdioFileEx.h"


// CFixHTMLApp:
// See FixHTML.cpp for the implementation of this class
//

#define SWAT_REG_KEY				_T("SWAT\\User Settings")
#define SWAT_REG_PARAM_TREE_PATH	_T("Script_Tree_Path")

#define WM_FIXING			(WM_USER + 105)
#define WM_FILEDIRTY		(WM_USER + 106)
#define WM_LOG				(WM_USER + 107)

struct structData
{
	CString m_strToFind;
	CString m_strReplaceWith;
};

struct DirtyMsg
{
	CString strHTMLFileName;
	CString strStringFound;
};

class CFixHTMLApp : public CWinApp
{
public:
	CFixHTMLApp();
	CString m_strRootPath;

// Overrides
	public:
	virtual BOOL InitInstance();

	CString m_strFileName;
	bool m_bRunning; //status
	bool m_bRun; //command
	CList<structData,structData&> m_listReplaceString;
	CStdioFile m_fileIn;
	CStdioFileEx m_fileOut;

	bool LoadConfig(CString strHTMLPathName);
	void Log(CString str);


// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CFixHTMLApp theApp;