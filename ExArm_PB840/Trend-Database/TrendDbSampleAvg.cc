#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbSampleAvg
//---------------------------------------------------------------------
//@ Interface-Description
//  TrendDbSampleAvg is derived from the TrendDbSampleData type. It is
//  used for parameters for which the sampled data is averaged over the
//  8 timescales.
//  The class overloads the compressData() method to specify how the
//  sampled data is averaged/compressed for each timescale. 
//  The class overloads the clear_() method to reset the accumlators.
// 
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the logic for the averaged TrendDbSampleData type
//  over a timescale
//---------------------------------------------------------------------
//@ Implementation-Description
//  The compressData() method uses the the following two helper functions:
//  addToAcumm_(), to accumulate the required number of samples in each
//  timescale compression
//  and computeAvgAndClear_(), to compute the average and reset the
//  accumulators at the end of the timescale period.
//  
//  The compressed data is averaged according to the schema below: 
//  1_hr sample is not compressed
//  2_hr sample is the avg of 2 1_hr samples
//  4_hr sample is the avg of 2 2_hr samples
//  8_hr sample is the avg of 2 4_hr samples
//  12_hr sample is the avg of 3 4_hr samples
//  24_hr sample is the avg of 2 12_hr samples
//  48_hr sample is the avg of 2 24_hr samples
//  72_hr sample is the avg of 3 24_hr samples
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleAvg.ccv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
//  Revision: 001  By:  ksg    Date:  17-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//
//=====================================================================

#include <string.h>

#include "Trend_Database.hh"
#include "TrendDbSampleAvg.hh"


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleAvg [constructor]
//
//@ Interface-Description
//  Constructs the Patient Data TrendDbSampleAvg object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the provided PatientDataId and trendId to the base class. 
//  Clears the accumulators.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleAvg::TrendDbSampleAvg( PatientDataId::PatientItemId id,
									TrendSelectValue::TrendSelectValueId trendId )
	: TrendDbSampleData( id, trendId )
{
	clear_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleAvg [constructor]
//
//@ Interface-Description
//  Constructs the SettingId TrendDbSampleAvg object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the provided settingId and the trendId to the base class.
//  Clears the accumulator.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleAvg::TrendDbSampleAvg( SettingId::SettingIdType id,
									TrendSelectValue::TrendSelectValueId trendId )
	: TrendDbSampleData( id, trendId )
{
	clear_();    
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleAvg [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleAvg::~TrendDbSampleAvg()   
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear_()
//
//@ Interface-Description
//  Clears the accumulators
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleAvg::clear_( void )
{
	memset( acummArray_, 0, sizeof(acummArray_) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: computeAvgAndClear_
//
//@ Interface-Description
//  Computes the arithmetic mean for the given timescale and resets the
//  accumulator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns uncompressed sample value for 1_hr timescale.
//  If no samples ('num' is zero), returns 0.0
//  Otherwise, computes the arithmetic mean: 
//  (accumulated value)/(num samples)
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the provided timescale is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendValue TrendDbSampleAvg::computeAvgAndClear_( TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION( (timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES), timescale );
	TrendValue  rtn;

	if (timescale == TrendTimeScaleValue::TREND_TIME_SCALE_1HR)
	{
		// 1_hr timescale value is sampled, not averaged. Just return.
		rtn = valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_1HR];
	}
	else
	{
		// Number of samples included in the average
		if (acummArray_[timescale-1].num)
		{
			// compute mean
			rtn.data.realValue = ( acummArray_[timescale-1].value / 
								   ((Real32) acummArray_[timescale-1].num) );
			// set valid bit to TRUE
			rtn.isValid = TRUE;
		}
		else
		{
			// No values to average. Return 0.0 and set valid bit to FALSE.
			rtn.data.realValue = 0.0;
			rtn.isValid = FALSE;
		}

		// Reset acumm values
		acummArray_[timescale-1].value = 0.0;
		acummArray_[timescale-1].num = 0;
	}

	return( rtn );    
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addToAcumm_
//
//@ Interface-Description
//  Accumulates the sampled values in the provided timescale's
//  accumulator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the current sampled value is 'valid', adds it to the accumumulator
//  to be averaged. Update the timescale sample count 'num' for the average
//  divisor.
// 
//  Satisfies requirements:
//  [[TR01120] [PRD 1483, 1484, 1485] Gaps shall not be included in the
//  averaging process...
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the provided timescale is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleAvg::addToAcumm_( const TrendValue & value, TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION( (timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES), timescale );

	// The 1_hr timescale is not accumulated, just return
	// Only valid data is included in the average
	if (timescale > TrendTimeScaleValue::TREND_TIME_SCALE_1HR && value.isValid)
	{
		acummArray_[timescale-1].value += value.data.realValue;
		acummArray_[timescale-1].num++;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: compressData
//
//@ Interface-Description
//  Accumulates and averages samples from the 1_hr timescale to generate
//  samples at the higher timescales.
//---------------------------------------------------------------------
//@ Implementation-Description
//  1_hr sample is not compressed
//  2_hr sample is the avg of 2 1_hr samples
//  4_hr sample is the avg of 2 2_hr samples
//  8_hr sample is the avg of 2 4_hr samples
//  12_hr sample is the avg of 3 4_hr samples
//  24_hr sample is the avg of 2 12_hr samples
//  48_hr sample is the avg of 2 24_hr samples
//  72_hr sample is the avg of 3 24_hr samples
// 
//  Satisfies requirements:
//  [[TR01114] [PRD 1512] For the 2, 4, 8, 12, 24, 48, and 72 hour
//  timescales, the system shall determine...
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the provided timescale is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleAvg::compressData( TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION( (timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES), timescale );

	switch (timescale)
	{
		// 2_hr sample is the avg of 2 1_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_1HR :
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_1HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_2HR );
			break;

			// compute 2_hr sample
			// 4_hr sample is the avg of 2 2_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_2HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_2HR] =
				computeAvgAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_2HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_2HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_4HR );
			break;

			// compute 4_hr sample
			// 8_hr sample is the avg of 2 4_hr samples
			// 12_hr sample is the avg of 3 4_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_4HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_4HR] =
				computeAvgAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_4HR ); 
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_4HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_8HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_4HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_12HR );
			break;

			// compute 8_hr sample
		case TrendTimeScaleValue::TREND_TIME_SCALE_8HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_8HR] =
				computeAvgAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_8HR );
			break;

			// compute 12_hr sample
			// 24_hr sample is the avg of 2 12_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_12HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_12HR] =
				computeAvgAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_12HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_12HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_24HR );
			break;

			// compute 24_hr sample
			// 48_hr sample is the avg of 2 24_hr samples
			// 72_hr sample is the avg of 3 24_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_24HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_24HR] =
				computeAvgAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_24HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_24HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_48HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_24HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_72HR );
			break;

			// compute 48_hr sample
		case TrendTimeScaleValue::TREND_TIME_SCALE_48HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_48HR] =
				computeAvgAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_48HR );
			break;

			// compute 72_hr sample
		case TrendTimeScaleValue::TREND_TIME_SCALE_72HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_72HR] =
				computeAvgAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_72HR );
			break;

		default:
			TREND_MGR_DBG2( 1, "Sample Avg unknown timescale ", timescale ); 
			break;
	}
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbSampleAvg::SoftFault( const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_SAMPLE_AVG,
							lineNumber, pFileName, pPredicate);
}






