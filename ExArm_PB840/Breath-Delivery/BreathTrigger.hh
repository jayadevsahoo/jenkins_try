#ifndef BreathTrigger_HH
#define BreathTrigger_HH
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: BreathTrigger - This class is an abstract base class for all 
//      breath triggers the Breath Delivery system may need.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BreathTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
#  include "Sigma.hh"
 
#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "Trigger.hh"

//@ End-Usage

class BreathTrigger : public Trigger
{
  public:

    BreathTrigger(const Trigger::TriggerId id);
    virtual ~BreathTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL,
			  const char*       pPredicate = NULL);
	
  protected:
    virtual void triggerAction_(const Boolean trigCond);

  private:
	
    BreathTrigger(void);	// Declared but not implemented
    BreathTrigger(const BreathTrigger&);    // Declared but not implemented
    void operator=(const BreathTrigger&);   // Declared but not implemented

};


#endif // BreathTrigger_HH 
