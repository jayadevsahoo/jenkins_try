/***************************************************************************
*
*		Copyright (c) 1992 READY SYSTEMS CORPORATION.
*
*	All rights reserved. READY SYSTEMS' source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	READY SYSTEMS. Any attempt or participation in deciphering, decoding,
*	reverse engineering or in any way altering the source code is
*	strictly prohibited, unless the prior written consent of
*	READY SYSTEMS is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/

/*****************************************************************************
** serial.h
**
** Contents:
**
**    This header file contains type definitions used to configure a serial
**    device as a TTY type device or a PACKET type device. The device can
**    only be one or the other and the type describes how it can be used.
**    A PACKET device shall only support getmsg and putmsg for I/O where
**    a TTY device shall only support read and writ for I/O. This can be
**    configured at compile time by using the method tables data pointer.
**    If the data pointer is not NULL and the data->event is a serial_packet
**    device, then it shall be treated as a PACKET device, otherwise, it
**    shall be treated as a TTY device.
**
** Revision History:
**
** History:
**
**    Date        Name              Description
**    ---------------------------------------------------------------------- 
**    07-15-92    Mike Milne        First Version, serial.h
**    10-09-92    Roger Cole        Added externio interface, interrupt types
******************************************************************************/

#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <ansiprot.h>
#include <logio.h>
#include <serialpkt.h>
#include <compiler.h>

#include "logio_charbuf.h"
#include "vbsp.h"
#include "externio.h"      /* for register i/o and interrupt interfaces */

/* serial config types */

typedef enum {serial_packet_device, serial_tty_device} serial_flavor_t;
typedef enum {seven_bits, eight_bits} serial_bits_t;
typedef enum {one_stop_bit, two_stop_bits} serial_stop_bit_t;
typedef enum {odd_parity, even_parity, no_parity} serial_parity_t;

typedef struct {
   serial_flavor_t flavor;
   
   /* the counter value to write to the baud rate generator register */
   unsigned long baud_counter_val;
   serial_parity_t   parity;
   serial_bits_t     bits;
   serial_stop_bit_t stop;

   /* interrupt vectors must be known even if not programmable */
   unsigned char rx_int_vector;
   unsigned char tx_int_vector;
   unsigned char err_int_vector;
   
   /* the following pointer should be used to add extra device
   ** specific information needed that all devices may not need
   */
   void *specific;
} serial_config_t;

/* serial buffers can be logio_charbuf's or logio_buff's */

typedef struct {
   logio_charbuf_t inbuf;
   logio_charbuf_t outbuf;
} serial_tty_buff_t;

/* define the packet modes */
typedef enum {get_preamble_field, get_data_field} serial_packet_mode_t;

typedef struct {
   logio_buff_t         *curr;
   serial_packet_mode_t mode;
   unsigned short       byte_cnt;
   unsigned char        *next_ch;
} serial_pkt_tst_info_t;

typedef struct {
   logio_buff_t *nxt_pkt;

   /* buffers worked on by test routines (static info) */
   serial_pkt_tst_info_t rx;
   serial_pkt_tst_info_t tx;

   /* data needed by all test routines for preamble and logio_buffs */
   pkt_preamble_t preamble;
} serial_pkt_buff_t;

/*
 * a serial device descriptor has a flavor, buf pointers, device
 * descriptor pointers, device config pointers, and function table
 */

struct serial_dev_desc_t;

typedef struct {
   int (*tx_tst)(_ANSIPROT1(logio_interrupt_entry_t *));
   int (*rx_tst)(_ANSIPROT1(logio_interrupt_entry_t *));
   int (*err_tst)(_ANSIPROT1(logio_interrupt_entry_t *));
   vbsp_return_t (*init)(_ANSIPROT1(struct serial_dev_desc_t *));
   vbsp_return_t (*transmitter_off)(_ANSIPROT1(struct serial_dev_desc_t *));
   vbsp_return_t (*start_transmitter)(_ANSIPROT1(struct serial_dev_desc_t *));
   vbsp_return_t (*int_info_set)(_ANSIPROT2(struct serial_dev_desc_t *,
            logio_int_entry_t *));
   vbsp_return_t (*int_info_get)(_ANSIPROT2(struct serial_dev_desc_t *,
            logio_int_entry_t *));
   vbsp_return_t (*pputc)(_ANSIPROT2(struct serial_dev_desc_t *,
            unsigned char ));
   vbsp_return_t (*pgetc)(_ANSIPROT2(struct serial_dev_desc_t *,
            unsigned char *));

   /* the following pointer should be used if more device specific
    * functions must be called through the function table
    */
   void *specific;
} serial_fops_t;

typedef struct serial_dev_desc_t {
   void *map;
   serial_config_t config;
   serial_fops_t fops;
   extern_fops_t externio;       /* external register read/write and interrupt functions */
   union {
      serial_tty_buff_t tty;
      serial_pkt_buff_t pkt;
   } buf;
   void *xtra;
} serial_dev_desc_t;

/* these are the types for the externio interrupt start and interrupt end functions */
#define SERIAL_TTY_RX_INT   0
#define SERIAL_TTY_TX_INT   1
#define SERIAL_TTY_ER_INT   2
#define SERIAL_PKT_RX_INT   3
#define SERIAL_PKT_TX_INT   4
#define SERIAL_PKT_ER_INT   5

/* some function prototypes */
int logio_serial_packet_rx(_ANSIPROT2(serial_dev_desc_t *, unsigned char));
vbsp_return_t logio_serial_packet_tx(_ANSIPROT1(serial_dev_desc_t *));
vbsp_return_t logio_serial_prime_pump(_ANSIPROT1(serial_dev_desc_t *));


/****************************************************************************/
/* NOTE NOTE NOTE NOTE NOTE NOTE */
/* The following prototypes should be placed in the packet lib include file */
/* These functions are in serial.h temporarily ONLY */
/****************************************************************************/
/* extern declarations */
/* these will be included from the packet library */

extern get_serial_pkt_buf();
extern void resync_pkt();


#endif
