#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: VentStatus - implements the functionality required for
//                       interfacing with the vent head status LEDs
//                       and SVO, ten second timer, vent inop, and
//                       BD audio alarm registers
//---------------------------------------------------------------------
//@ Interface-Description
//  This class implements the interface to the ventilator status LEDs
//  Three vent head LEDs indicate the following ventilator status:
//  SVO, GUI inoperative, and ventilator inoperative.  Methods are
//  provided for turning each LED on or off.  It also provides
//  service to the BD audio alarm, power-up ten second timer, vent
//  inop, and safe state registers.
//---------------------------------------------------------------------
//@ Rationale
//  This class implements the algorithm for interfacing with the
//  ventilator status LEDs, activate/deactive SVO state, vent inop,
//  power-up ten second timer, BD audio alarm.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Three private BinaryCommand objects are used, for controlling the
//  LEDs.  Each LED is turned on and off using inlined methods.
//
//  Since the VentStatus class contains BinaryCommand objects, it
//  has to be constructed after the construction of the BinaryCommand
//  class.  This class does not initalize the LEDs to any particular
//  state.  That is the responsibility of other objects during system
//  initialization.  This class utilitize the Register class.  Hence,
//  the DirectVentStatusService method can only be called by the
//  highest priority task, Breath-Delivery.  All other tasks must use
//  VentStatusService method.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @ (#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/VentStatus.ccv   10.7   08/17/07 09:32:30   pvcs
//
//@ Modification-Log
//
//  Revision: 009 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 008  By:  syw	    Date:   14-Sep-1998    DR Number: none
//       Project:  BiLevel
//       Description:
//			BiLevel initial release.
//			Added second argument to pVentStatusCallback.
//
//  Revision: 007  By:  syw    Date: 23-Jun-1997    DR Number: DCS 2244
//       Project:  Sigma (R8027)
//       Description:
//			Added newCycle() method to update the status of the bdAudioAlarmOn_
//			based on the register contents.
//
//  Revision: 006  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 005 By: iv   Date: 06-May-1997   DR Number: DCS 2027
//      Project:  Sigma (840)
//      Description:
//          Removed alarmActivatedFlag_ and alarmActivationFlagUserId_
//          data members
//
//  Revision: 004 By: syw   Date: 14-Apr-1997   DR Number: DCS 1921
//      Project:  Sigma (840)
//      Description:
//			Added alarm off timer to determine amout of time that has
//			elapsed since the alarm is turned off.
//
//  Revision: 003 By: by   Date: 13-Feb-1997   DR Number: DCS 1718
//      Project:  Sigma (840)
//      Description:
//          Added registerForAlarmActivationFlag() for a client to
//          register for an alarm activation call back.  Also added
//          getAlarmActivationFlagStatus() the registered client to
//          retrieve the alarm activation status.  Added logic
//          in turnOnBdAudioAlarm_() and latchOnBdAudioAlarm_() to
//          set alarm activity flag to TRUE when executed.
//
//  Revision: 002 By: syw   Date: 07-Dec-1996   DR Number: DCS 1603
//      Project:  Sigma (R840)
//      Description:
//            Changed setBitOff() and setBitOn() to setBit().  Call writeNewCycle()
//            after calling setBit() to get immediate response.  Added
//            LatchVentStatusService() method.  Eliminated VentStatusService()
//            and newCycle() methods since BitAccessRegisterMediator will service
//            the request.  Eliminate ExternalServiceTable_.
//
//  Revision: 001  By:  by   Date:  11-Nov-1996    DR Number:
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "VentStatus.hh"

//@ Usage-Classes

#include "BDIORefs.hh"
#include "RegisterRefs.hh"
#include "Solenoid.hh"
#include "Psol.hh"
#include "VentObjectRefs.hh"
#include "ValveRefs.hh"
#include "ExhalationValve.hh"
#include "TaskControlAgent.hh"

#include "RegisterRefs.hh"
#include "BitAccessGpioMediator.hh"

//@ End-Usage

//@ Code...

// HW Specification see
// Sigma BDCPU Requirements Specification #70950-45
// Sigma Analog Interface PCBA Requirements Specification #70550-45

//@ Constant: SVO_MASK
// mask to for the SVO bit
static const Uint8 SVO_MASK = 0x08;

//@ Constant: LOSS_GUI_COMM_MASK1
// mask to for the loss of GUI
const Uint8 LOSS_GUI_COMM_MASK1 = 0x1;

//@ Constant: LOSS_GUI_COMM_MASK2
// mask to for the loss of GUI
const Uint8 LOSS_GUI_COMM_MASK2 = 0x2;

//@ Constant: VENT_IN_OP_MASK
// mask to for the vent inoperative bit
static const Uint8 VENT_IN_OP_MASK = 0x04;

//@ Constant: BD_AUDIO_ALARM_MASK
// mask to for the BD audio alarm bit
const Uint16  BD_AUDIO_ALARM_MASK = 0x2000;

//@ Constant: SAFE_STATE_MASK
// mask to for the safe state A and B bit
static const Uint8  SAFE_STATE_MASK = 0x08;

//@ Constant: FORCED_VENT_INOP_MASK
// mask to for the forced VENT INOP A and B bit
static const Uint8  FORCED_VENT_INOP_MASK = 0x04;

//@ Constant: TEN_SECOND_TIMER_MASK
// mask to for the 10 second timer bit
static const Uint8  TEN_SECOND_TIMER_MASK = 0x01;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VentStatus()  [Default Constructor]
//
//@ Interface-Description
//  This constructor takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The constructors for the private BinaryCommand objects are invoked.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

VentStatus::VentStatus( void ) :										// Original 840 bit IDs
    svoLed_( DOUT_DUMMY_840_OUT, RBitAccessGpio),                       // SVO_MASK
    guiLed1_( DOUT_DUMMY_840_OUT, RBitAccessGpio ),                     // LOSS_GUI_COMM_MASK1
    guiLed2_( DOUT_DUMMY_840_OUT, RBitAccessGpio ),                     // LOSS_GUI_COMM_MASK2
    ventInOpLed_( DOUT_DUMMY_840_OUT, RBitAccessGpio ),                 // VENT_IN_OP_MASK
    bdAudioAlarm_( DOUT_BUZZER, RBitAccessGpio ),                       //
    safetStateA_( DOUT_DUMMY_840_OUT, RBitAccessGpio ),                 // SAFE_STATE_MASK
    safetStateB_( DOUT_DUMMY_840_OUT, RBitAccessGpio ),                 // SAFE_STATE_MASK
    forcedVentInopA_( DOUT_DUMMY_840_OUT, RBitAccessGpio ),             // FORCED_VENT_INOP_MASK
    forcedVentInopB_( DOUT_DUMMY_840_OUT, RBitAccessGpio ),             // FORCED_VENT_INOP_MASK
    tenSecondTimerA_( DOUT_DUMMY_840_OUT, RBitAccessGpio ),             // TEN_SECOND_TIMER_MASK
    tenSecondTimerB_( DOUT_DUMMY_840_OUT, RBitAccessGpio )              // TEN_SECOND_TIMER_MASK
{
    CALL_TRACE("VentStatus::VentStatus(void)");

    // $[TI1]
    // initialize VENT INOP status flag
    ventInopStateActivated_ = FALSE;

    // initialize BD audio alarm status flag
    bdAudioAlarmOn_ = FALSE;

	// start timer
    alarmOffTimer_.now();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~VentStatus()  [Destructor]
//
//@ Interface-Description
//  This desconstructor takes no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Not implemented.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

VentStatus::~VentStatus(void)
{
     CALL_TRACE("VentStatus::~VentStatus(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DirectVentStatusService()
//
//@ Interface-Description
//  This method accepts two arguments, one is enumeration ObjectId
//  and one enumeration of CommandType.  No value is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is intended to service VentStatus requests by the
//  highest priority task (Breath-Delivery).  Only one request to
//  per object is serviced per call.  Once a request is made, it is
//  serviced immediately.  Hence, the calling task must not be
//  interrupted by another task.
//---------------------------------------------------------------------
//@ PreCondition
//  This method's intented client is strickly for the highest priority
//  task (Breath-Delivery).  All lower priority tasks must use
//  VentStatusService() to insure the data integrity of the Register
//  class image.
//
//  An attempt to de-activate a VENT INOP condition will cause
//  an assertion, except in service mode.  An attempt to activate
//  the ten second timer will cause an assertion.
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
VentStatus::DirectVentStatusService
(
    const ObjectId object, const CommandType command
)
{
    CALL_TRACE("VentStatus::DirectVentStatusService(ObjectId object, CommandType command))");

    switch( object )
    {
        case SVO_STATE:
        // $[TI1]

            if ( DEACTIVATE == command )
            {
            // $[TI1.1]
                RVentStatus.deactivateSvoState_();
            }
            else if ( ACTIVATE == command )
            {
            // $[TI1.2]
                RVentStatus.activateSvoState_();
            }
            // $[TI1.3]
            break;

        case LOSS_OF_GUI_LED:
        // $[TI2]

            if ( DEACTIVATE == command )
            {
            // $[TI2.1]
                RVentStatus.turnOffGuiLed_();
            }
            else if ( ACTIVATE == command )
            {
            // $[TI2.2]
                RVentStatus.turnOnGuiLed_();
            }
            // $[TI2.3]
            break;

        case VENT_INOP_STATE:
        // $[TI3]
            SAFE_CLASS_ASSERTION( command != DEACTIVATE );

            RVentStatus.activateVentInopState_( object );

#if 0 //E600 #ifndef SIGMA_UNIT_TEST
            // issue write to bit for immediate response to request
            RSafetyNetControlAPort.writeNewCycle() ;
            RSafetyNetControlBPort.writeNewCycle() ;
#endif // SIGMA_UNIT_TEST
            break ;

        case VENT_INOP_REG_A:
        case VENT_INOP_REG_B:

            if ( ACTIVATE == command )
            {
            // $[TI3.1]
                RVentStatus.activateVentInopState_( object );
            }
            else if ( DEACTIVATE == command )
            {
            // $[TI3.2]
                RVentStatus.deactivateVentInopState_( object );
            }
            // $[TI3.3]
            break;

        case BD_AUDIO_ALARM:
        // $[TI4]

            if ( DEACTIVATE == command )
            {
            // $[TI4.1]
                RVentStatus.turnOffBdAudioAlarm_();
            }
            else if ( ACTIVATE == command )
            {
            // $[TI4.2]
                RVentStatus.turnOnBdAudioAlarm_();
            }
            // $[TI4.3]
            break;

        case TEN_SECOND_TIMER:
        // $[TI5]
            SAFE_CLASS_ASSERTION( command != ACTIVATE );

            // TEN_SECOND_TIMER is intended to fall throught

        case TEN_SECOND_TIMER_REG_A:
        case TEN_SECOND_TIMER_REG_B:

            if ( DEACTIVATE == command )
            {
            // $[TI5.1]
                RVentStatus.deactivateTenSecondTimer_( object );
            }
            else if ( ACTIVATE == command )
            {
            // $[TI5.2]
                RVentStatus.activateTenSecondTimer_( object );
            }
            // $[TI5.3]
            break;

        default:
            CLASS_ASSERTION( (object == SVO_STATE) ||
                                  (object == LOSS_OF_GUI_LED) ||
                                  (object == VENT_INOP_STATE) ||
                                  (object == BD_AUDIO_ALARM) ||
                                  (object == TEN_SECOND_TIMER) ||
                                  (object == VENT_INOP_REG_A) ||
                                  (object == VENT_INOP_REG_B) ||
                                  (object == TEN_SECOND_TIMER_REG_A) ||
                                  (object == TEN_SECOND_TIMER_REG_B) );
            break;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LatchVentStatusService()
//
//@ Interface-Description
//  This method accepts two arguments, one is enumeration ObjectId
//  and one enumeration of CommandType.  No value is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is intended to service VentStatus latch requests.
//    Once a request is made, it is serviced at the end of the BD cycle.
//---------------------------------------------------------------------
//@ PreCondition
//    object == LOSS_OF_GUI_LED || object == BD_AUDIO_ALARM)
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
VentStatus::LatchVentStatusService
(
    const ObjectId object, const CommandType command
)
{
    CALL_TRACE("VentStatus::LatchVentStatusService(ObjectId object, CommandType command))");

    switch( object )
    {
        case LOSS_OF_GUI_LED:
        // $[TI1]

            if ( DEACTIVATE == command )
            {
            // $[TI1.1]
                RVentStatus.latchOffGuiLed_();
            }
            else if ( ACTIVATE == command )
            {
            // $[TI1.2]
                RVentStatus.latchOnGuiLed_();
            }
            // $[TI1.3]
            break;

        case BD_AUDIO_ALARM:
        // $[TI2]

            if ( DEACTIVATE == command )
            {
            // $[TI2.1]
                RVentStatus.latchOffBdAudioAlarm_();
            }
            else if ( ACTIVATE == command )
            {
            // $[TI2.2]
                RVentStatus.latchOnBdAudioAlarm_();
            }
            // $[TI2.3]
            break;

        default:
            CLASS_ASSERTION( (object == LOSS_OF_GUI_LED) ||
                             (object == BD_AUDIO_ALARM) );
            break;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle() ;
//
//@ Interface-Description
//  	This method accepts no arguments and returns nothing.  This method
//		updates the bd alarm status each BD cycle when called.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Update the bdAudioAlarmOn_ flag if the bit was turned on.  If the
//		alarm is on, reset the timer.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//  	none
//@ End-Method
//=====================================================================
void
VentStatus::newCycle( void)
{
	CALL_TRACE("VentStatus::newCycle( void)") ;
	
	bool status = FALSE;	
	if(status)//TODO E600 (RWriteIOPort.getBitState( BD_AUDIO_ALARM_MASK) == BitAccessRegister::ON)
	{
        // $[TI1]
		// alarm is off
		bdAudioAlarmOn_ = FALSE ;
	}
	else
	{
        // $[TI2]
		bdAudioAlarmOn_ = TRUE ;
	    alarmOffTimer_.now() ;		// reset timer
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//  [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
VentStatus::SoftFault( const SoftFaultID  softFaultID,
                       const Uint32       lineNumber,
                       const char*        pFileName,
                       const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, VENTSTATUS,
                           lineNumber, pFileName, pPredicate );
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateSvoState_()
//
//@ Interface-Description
//  This method accepts no argument and returns no value.
//  This method activates BD Safe State.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method initiates a ventilator safe state.  First, it neutralizes
//  pneumatics.  Second, the SVO activated, SVO LED is turned on, and
//  the electronics is set to safe state mode.  Lastly, the GUI is
//  notified of the safe state status.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
VentStatus::activateSvoState_( void )
{
    CALL_TRACE("VentStatus::activateSvoState_( void )");

    // $[TI1]
    // neutralize pneumatics
    RAirPsol.updatePsol( 0 );
    RO2Psol.updatePsol( 0 );
    RSafetyValve.open();
    RExhalationValve.deenergize();

    // activate safe state
    turnOnSvoLed_();
    safetStateA_.setBit( BitAccessGpio::OFF);
    safetStateB_.setBit( BitAccessGpio::OFF);

    // notify the VentAndUserEventStatus class of the ventilator status
    // change; the Gui is in turn informed of the status
    pVentStatusCallBack_( EventData::SVO, EventData::ACTIVE, EventData::NULL_EVENT_PROMPT );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateSvoState_()
//
//@ Interface-Description
//  This method accepts no argument and returns no value.
//  This method deactivates BD Safe State.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method de-activates a safe state.  First, it closes the
//  safety valve.  Second, the SVO LED is turned off and the electronics
//  taken out of safe state.  Lastly, the GUI is notified of the change
//  in safe state status.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
VentStatus::deactivateSvoState_( void )
{
    CALL_TRACE("VentStatus::deactivateSvoState_( void )");

    // $[TI1]
    RSafetyValve.close();

    turnOffSvoLed_();
    safetStateA_.setBit( BitAccessGpio::ON);
    safetStateB_.setBit( BitAccessGpio::ON);

    // notify the VentAndUserEventStatus class of the ventilator status
    // change; the Gui is in turn informed of the status
    pVentStatusCallBack_(EventData::SVO, EventData::CANCEL, EventData::NULL_EVENT_PROMPT);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateVentInopState_
//
//@ Interface-Description
//  This method accepts an ObjectID enumeration and returns no value.
//  This method deactivates the VENT INOP states.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is intended to be used by Service-Mode only.
//  First, the electronics to control the pnuematics is enabled.
//  Second, the safety valve is closed.  The exhalation heater
//  is turned-on.  The Vent Inop LED is turned-off.  Lastly, GUI is
//  notified of the change in vent status.
//---------------------------------------------------------------------
//@ PreCondition
//  Must be in Service-Mode.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
VentStatus::deactivateVentInopState_( const ObjectId object )
{
    CALL_TRACE("VentStatus::deactivateVentInopState_( ObjectId object )");

    CLASS_PRE_CONDITION( TaskControlAgent::GetMyState() == STATE_SERVICE );

    if ( TRUE == ventInopStateActivated_ )
    {
    // $[TI1]

        // activate VENT INOP state
        switch( object )
        {
            case VENT_INOP_REG_A:
            // $[TI1.1]
                forcedVentInopA_.setBit( BitAccessGpio::OFF);
                break;

            case VENT_INOP_REG_B:
            // $[TI1.2]
                forcedVentInopB_.setBit( BitAccessGpio::OFF);
                break;

            default:
                CLASS_ASSERTION( ( VENT_INOP_REG_A == object ) ||
                                      ( VENT_INOP_REG_B == object ) );
                break;
        }

        // close safety valve
        RSafetyValve.close();

        // turn-off exhalation heater
// E600 BDIO        RExhHeater.setBit( BitAccessGpio::OFF);

        // de-activate BD audio alarm
        turnOffBdAudioAlarm_();

        // turn-off VENT INOP LED
        turnOffVentInOpLed_();

        // notify the VentAndUserEventStatus class of the ventilator status
        // change; the Gui is in turn informed of the status
        pVentStatusCallBack_(EventData::VENT_INOP, EventData::CANCEL, EventData::NULL_EVENT_PROMPT);

        // update VENT INOP status flag
        ventInopStateActivated_ = FALSE;
    }
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateVentInopState_
//
//@ Interface-Description
//  This method accepts an ObjectID enumeration and returns no value.
//  This method activates the VENT INOP states.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method initiates a Vent Inop condition.
//  First, the pneumatics are neutalized.  Second, the Vent Inop LED is
//  turned on, the electronics is set to Vent Inop state.  Lastly,
//  GUI is notified of the Vent Inop.
//---------------------------------------------------------------------
//@ PreCondition
//  Must be in Service-Mode to active the separate Vent Inop registers.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
VentStatus::activateVentInopState_( const ObjectId object )
{
    CALL_TRACE("VentStatus::activateVentInopState_( const ObjectId object )");

    if ( FALSE == ventInopStateActivated_ )
    {
    // $[TI1]

        switch ( object )
        {
            case VENT_INOP_REG_A:
            // $[TI1.1]

                CLASS_PRE_CONDITION( TaskControlAgent::GetMyState() == STATE_SERVICE );
                forcedVentInopA_.setBit( BitAccessGpio::ON);
                break;

            case VENT_INOP_REG_B:
            // $[TI1.2]

                CLASS_PRE_CONDITION( TaskControlAgent::GetMyState() == STATE_SERVICE );
                forcedVentInopB_.setBit( BitAccessGpio::ON);
                break;

            case VENT_INOP_STATE:
            // $[TI1.3]

                forcedVentInopA_.setBit( BitAccessGpio::ON);
                forcedVentInopB_.setBit( BitAccessGpio::ON);

                // neutralize pneumatics
                RAirPsol.updatePsol( 0 );
                RO2Psol.updatePsol( 0 );
                RSafetyValve.open();
                RExhalationValve.deenergize();

                // activate BD audio alarm
                turnOnBdAudioAlarm_();

                // turn-off exhalation heater
// E600 BDIO                RExhHeater.setBit( BitAccessGpio::OFF);

                // activate VENT INOP LED
                turnOnVentInOpLed_();

                // notify the VentAndUserEventStatus class of the ventilator
                // status change; the Gui is in turn informed of the status
                pVentStatusCallBack_(EventData::VENT_INOP, EventData::ACTIVE, EventData::NULL_EVENT_PROMPT);
                break;

            default:
                CLASS_ASSERTION( ( VENT_INOP_STATE == object ) ||
                                      ( VENT_INOP_REG_A == object ) ||
                                      ( VENT_INOP_REG_B == object ) );
                break;
        }

        // update VENT INOP status flag
        ventInopStateActivated_ = TRUE;
    }
    // $[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivateTenSecondTimer_()
//
//@ Interface-Description
//  This method accepts an ObjectID enumeration and returns no value.
//  This method deactivates the 10 second timer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The both BinaryCommand object for the 10 second timer indicator are
//  commanded to ON.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
VentStatus::deactivateTenSecondTimer_( const ObjectId object )
{
    CALL_TRACE("VentStatus::deactivateTenSecondTimer_( const ObjectId object )");

    switch( object )
    {
        case TEN_SECOND_TIMER:
        // $[TI1]

            tenSecondTimerA_.setBit( BitAccessGpio::ON);
            tenSecondTimerB_.setBit( BitAccessGpio::ON);
            break;

        case TEN_SECOND_TIMER_REG_A:
        // $[TI2]

            CLASS_PRE_CONDITION( TaskControlAgent::GetMyState() == STATE_SERVICE );
            tenSecondTimerA_.setBit( BitAccessGpio::ON);
            break;

        case TEN_SECOND_TIMER_REG_B:
        // $[TI3]

            CLASS_PRE_CONDITION( TaskControlAgent::GetMyState() == STATE_SERVICE );
            tenSecondTimerB_.setBit( BitAccessGpio::ON);
            break;

        default:
            CLASS_ASSERTION( ( TEN_SECOND_TIMER == object ) ||
                                  ( TEN_SECOND_TIMER_REG_A == object ) ||
                                  ( TEN_SECOND_TIMER_REG_B == object ) );
            break;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activateTenSecondTimer_
//
//@ Interface-Description
//  This method accepts an ObjectID enumeration and returns no value.
//  This method activates the 10 second timer.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is intended to be used by Service-Mode only.
//  The BinaryCommand object for the 10 second timer indicator is
//  commanded to OFF.
//---------------------------------------------------------------------
//@ PreCondition
//  Must be in Service-Mode.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
VentStatus::activateTenSecondTimer_( const ObjectId object )
{
    CALL_TRACE("VentStatus::activateTenSecondTimer_( const ObjectId object )");

    CLASS_PRE_CONDITION( TaskControlAgent::GetMyState() == STATE_SERVICE );

    switch( object )
    {
        case TEN_SECOND_TIMER_REG_A:
        // $[TI1]

            tenSecondTimerA_.setBit( BitAccessGpio::OFF);
            break;

        case TEN_SECOND_TIMER_REG_B:
        // $[TI2]

            tenSecondTimerB_.setBit( BitAccessGpio::OFF);
            break;

        default:
            CLASS_ASSERTION( ( TEN_SECOND_TIMER_REG_A == object ) ||
                                  ( TEN_SECOND_TIMER_REG_B == object ) );
            break;
    }

}



