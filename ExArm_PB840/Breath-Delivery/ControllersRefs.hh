
#ifndef ControllersRefs_HH
#define ControllersRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: ControllersRefs - All the external references of various controllers 
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ControllersRefs.hhv   25.0.4.0   19 Nov 2013 13:59:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: syw     Date:  09-Oct-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added VolumeTargetedController.
//
//  Revision: 004  By:  syw    Date:  14-Jan-1999    DR Number: none
//       Project:  ATC
//       Description:
//		 	ATC initial version.
//			Eliminate ExhValveEController class.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By: syw    Date:  27-Feb-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//       	Added rExhHeaterController.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

// Volume Controller
class VolumeController;
extern VolumeController& RO2VolumeController;
extern VolumeController& RAirVolumeController;

// Flow Controller
class FlowController;
extern FlowController& RO2FlowController;
extern FlowController& RAirFlowController;

//Pressure Controller
class PressureController;
extern PressureController& RPressureController;

class PidPressureController ;
extern PidPressureController& RPidPressureController ;

// Exhalation Valve Controller
class ExhValveIController;
extern ExhValveIController& RExhValveIController;

// Exhalation Heater Controller
class ExhHeaterController ;
extern ExhHeaterController& RExhHeaterController ;

// Peep Controller
class PeepController ;
extern PeepController& RPeepController ;

// Volume Targeted Controller
class VolumeTargetedController ;
extern VolumeTargetedController& RVolumeTargetedController ;

#endif // ControllersRefs_HH 


