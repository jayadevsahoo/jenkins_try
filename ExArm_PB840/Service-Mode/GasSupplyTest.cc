#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:	GasSupplyTest -- Test gas supply pressure switches,
//          SV cracking pressure, psol leakage, flow sensor offsets
//---------------------------------------------------------------------
//@ Interface-Description
//	This class is responsible for testing the air and O2 pressure
//	switches, psol leakage, safety valve cracking pressure, and flow
//	sensor offsets.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the methods required for the gas supply
//	test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() implements the gas supply test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/GasSupplyTest.ccv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  iv     Date: 19-Dec-1997    DR Number: DCS 2702
//       Project:  Sigma (840)
//       Description:
//            Change the leak test time to 3.125 seconds. Change was made
//            in method psolLeakTest_. 
//
//  Revision: 003  By:  quf    Date: 02-Jul-1997    DR Number: DCS 2134
//       Project:  Sigma (840)
//       Description:
//            At end of test, make the "connect air" prompt appear
//            regardless of whether the O2 was connected previously.
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "GasSupplyTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmManager.hh"
#include "SmGasSupply.hh"
#include "SmUtilities.hh"
#include "SmFlowController.hh"
#include "SmDataId.hh"
#include "SmConstants.hh"

#include "Task.hh"
#include "OsTimeStamp.hh"

#include "VentObjectRefs.hh"
#include "MainSensorRefs.hh"
#include "ValveRefs.hh"
#include "PressureSensor.hh"
#include "ExhFlowSensor.h"
#include "Psol.hh"
#include "BD_IO_Devices.hh"
#include "Compressor.hh"

#if defined(SIGMA_DEVELOPMENT)
#include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
# include "GasSupplyTest_UT.hh"
#endif

//@ End-Usage

//@ Constant: PSOL_LEAK_FAILURE_THRESHOLD
const Real32 PSOL_LEAK_FAILURE_THRESHOLD = 100.0;  // cmH2O

//@ Constant: PSOL_LEAK_ALERT_THRESHOLD
const Real32 PSOL_LEAK_ALERT_THRESHOLD = 50.0;  // cmH2O

//@ Constant: MIN_RELIEF_PRESSURE
// safety valve cracking pressure min value
const Real32 MIN_RELIEF_PRESSURE = 112.0 ;  // cmH2O

//@ Constant: MAX_RELIEF_PRESSURE
// safety valve cracking pressure max value
const Real32 MAX_RELIEF_PRESSURE = 127.5 ; // cmH2O

//@ Constant: PEAK_PRESSURE_TEST_MIN_FLOW
// flow required for safety valve cracking pressure test
const Real32 PEAK_PRESSURE_TEST_MIN_FLOW = 100.0 ;  // lpm

//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: GasSupplyTest()  [Default Constructor]
//
//@ Interface - Description
//	Constructor.
//-----------------------------------------------------------------------
//@ Implementation - Description
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
GasSupplyTest::GasSupplyTest( void)
{
	CALL_TRACE("GasSupplyTest::GasSupplyTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~GasSupplyTest()  [Destructor]
//
//@ Interface - Description
//	Destructor.
//-----------------------------------------------------------------------
//@ Implementation - Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
GasSupplyTest::~GasSupplyTest( void)
{
	CALL_TRACE("GasSupplyTest::~GasSupplyTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTest()
//
//@ Interface - Description
//	This method performs the gas supply test.  No arguments, no return
//	value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	$[09014]
//	Autozero the pressure transducers.
//	Verify that wall air and O2 are connected, and prompt user to
//	connect the unconnected gas.  If compressor is installed, allow
//	user to skip connecting wall air and use compressor for testing.
//	Prompt user to block to-patient port.
//	Check safety valve cracking pressure and peak pressure.
//	Prompt user to disconnect O2.
//	Check for air psol leak.
//	Disable compressor, and if wall air is the selected air source,
//	prompt user to disconnect it.
//	Check flow sensors zero offsets.
//	Prompt user to connect O2.
//	Check for O2 psol leak.
//	Enable compressor, and if wall air is the selected air source,
//	prompt user to connect wall air.
//	Prompt user to unblock to-patient port and connect test circuit.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
void
GasSupplyTest::runTest( void)
{
	CALL_TRACE("GasSupplyTest::void runTest( void)") ;

	// Autozero the pressure transducers
	RSmUtilities.autozeroPressureXducers();

	Boolean operatorExit = FALSE;
	Boolean wallAirAvailable = TRUE;

#ifdef SIGMA_DEBUG
	printf("wall air IS %spresent\n",
		RSmGasSupply.isWallAirPresent() ? "" : "NOT ");
	printf("O2 IS %spresent\n",
		RSmGasSupply.isO2Present() ? "" : "NOT ");
#endif  // SIGMA_DEBUG

	// Check if wall air is connected
	if (!RSmGasSupply.isWallAirPresent())
	{
	// $[TI1]
		SmPromptId::ActionId operatorAction;

		if (RCompressor.checkCompressorInstalled())
		{
		// $[TI1.1]
			// compressor is installed:
			// ACCEPT key = user connected wall air,
			// CLEAR key = user wants to use compressor air
			RSmManager.promptOperator(
				SmPromptId::CONNECT_AIR_IF_AVAIL_PROMPT,
				SmPromptId::ACCEPT_AND_CANCEL_ONLY);

			operatorAction = RSmManager.getOperatorAction();

			CLASS_ASSERTION(
				operatorAction == SmPromptId::USER_EXIT ||
				operatorAction == SmPromptId::KEY_ACCEPT ||
				operatorAction == SmPromptId::KEY_CLEAR );
		}
		else
		{		
		// $[TI1.2]
			// compressor not installed:
			// ACCEPT key = user connected wall air,
			// CLEAR key is disabled for this case
			RSmManager.promptOperator(
				SmPromptId::CONNECT_AIR_PROMPT,
				SmPromptId::ACCEPT_KEY_ONLY);

			operatorAction = RSmManager.getOperatorAction();

			CLASS_ASSERTION(
				operatorAction == SmPromptId::USER_EXIT ||
				operatorAction == SmPromptId::KEY_ACCEPT );
		}
		// end else

		if (operatorAction == SmPromptId::USER_EXIT)
		{
		// $[TI1.3]
			// EXIT button was pressed

			operatorExit = TRUE ;
		}
		else if (operatorAction == SmPromptId::KEY_CLEAR)
		{
		// $[TI1.4]
			// CLEAR key was pressed

			// Wall air not available, use compressor air
			wallAirAvailable = FALSE ;
		}
		else
		{
		// $[TI1.5]
			// ACCEPT key was pressed

			// Retest if wall air is connected
			if (!RSmGasSupply.isWallAirPresent())
	    	{
			// $[TI1.5.1]
				// wall air not detected
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_4);
			}
			// $[TI1.5.2]
			// implied else: wall air was connected
    	}
		// end else
	}
	// $[TI2]
	// implied else: wall air already connected

#ifdef SIGMA_UNIT_TEST
	GasSupplyTest_UT::WallAirAvailable = wallAirAvailable;
#endif  // SIGMA_UNIT_TEST

	// Check if O2 is connected
	if (!operatorExit)
	{
	// $[TI3]
		// Test if O2 is connected
		if (!RSmGasSupply.isO2Present())
		{
		// $[TI3.1]
			if (!RSmUtilities.messagePrompt(SmPromptId::CONNECT_O2_PROMPT))
			{
			// $[TI3.1.1]
				operatorExit = TRUE;
			}
			else
			{
			// $[TI3.1.2]
				// Retest if O2 is connected
				if (!RSmGasSupply.isO2Present())
				{
				// $[TI3.1.2.1]
					// O2 not detected
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_FAILURE,
						SmStatusId::CONDITION_5);
				}
				// $[TI3.1.2.2]
				// implied else: O2 was connected
			}
			// end else
		}
		// $[TI3.2]
		// implied else: O2 already connected
	}
	// $[TI4]
	// implied else: EXIT button was pressed

	// Block inlet port then perform cracking pressure test
	if (!operatorExit)
	{
	// $[TI5]
		// Prompt to block the inlet port to allow checking for
		// a PSOL leak.
		if (!RSmUtilities.messagePrompt(SmPromptId::BLOCK_INLET_PORT_PROMPT))
		{
		// $[TI5.1]
			operatorExit = TRUE;
		}
		else
		{
		// $[TI5.2]
#ifdef SIGMA_UNIT_TEST
			Boolean svTestPassed = GasSupplyTest_UT::SvTestPassed;
#else
			Boolean svTestPassed = isCrackingPressureOk_();
#endif  // SIGMA_UNIT_TEST

			if (!svTestPassed)
			{
			// $[TI5.2.1]
				// failed SV cracking pressure test:
				// port not blocked or
				// test really failed
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_10);
			}
			// $[TI5.2.2]
			// implied else: SV cracking pressure test passed
		}
		// end else
	}
	// $[TI6]
	// implied else: EXIT button was pressed

	// Perform O2 switch and air psol leak checks
	if (!operatorExit)
	{
	// $[TI7]
		// Prompt to disconnect the O2 supply
		if (!RSmUtilities.messagePrompt(SmPromptId::DISCONNECT_O2_PROMPT))
		{
		// $[TI7.1]
			operatorExit = TRUE;
		}
		else
		{
		// $[TI7.2]
			// Bleed off any stored O2
			RSmUtilities.drainO2();

			// Verify O2 is disconnected
			if (RSmGasSupply.isO2Present())
			{
			// $[TI7.2.1]
				// O2 detected
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_6);
			}
			else
			{
			// $[TI7.2.2]
				// make sure data item pointer is in sync
				RSmManager.advanceTestPoint
					(SmDataId::GAS_SUPPLY_AIR_PSOL_LEAK_PRESSURE);

				// With the O2 disconnected, check for air PSOL leak
				Real32 inspPressureRise = psolLeakTest_();
				if (inspPressureRise > PSOL_LEAK_FAILURE_THRESHOLD)
				{
				// $[TI7.2.2.1]
					// air psol leak FAILURE
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_FAILURE,
						SmStatusId::CONDITION_7);
				}
				else if (inspPressureRise > PSOL_LEAK_ALERT_THRESHOLD)
				{
				// $[TI7.2.2.2]
					// air psol leak ALERT
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_ALERT,
						SmStatusId::CONDITION_13);
				}
				// $[TI7.2.2.3]
				// implied else: no air psol leak
			}
			// end else
		}
		// end else
	}
	// $[TI8]
	// implied else: EXIT button was pressed

	// Perform wall air switch and 0 flow checks
	if (!operatorExit)
	{
	// $[TI9]
		// Disable the compressor
		RSmGasSupply.disableCompressorAndControl();

		// Prompt to disconnect the wall air if it is present
		if (wallAirAvailable)
		{
		// $[TI9.1]
			if (!RSmUtilities.messagePrompt(
							SmPromptId::DISCONNECT_AIR_PROMPT))
			{
			// $[TI9.1.1]
				operatorExit = TRUE;
			}
			// $[TI9.1.2]
			// implied else
		}
		// $[TI9.2]
		// implied else: wall air not available

		if (!operatorExit)
		{
		// $[TI9.3]
			// Bleed off all stored air
			RSmUtilities.drainAir();

			if (RSmGasSupply.isWallAirPresent())
			{
			// $[TI9.3.1]
				// wall air detected
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_8);
			}
			else if (RSmGasSupply.isCompressorAirPresent())
			{
			// $[TI9.3.2]
				// compressor pressure detected
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_12);
			}
			// $[TI9.3.3]
			// implied else: air was disconnected and drained

			// check flow sensor 0 lpm readings
			Real32 airFlow = RAirFlowSensor.getValue();
			Real32 o2Flow = RO2FlowSensor.getValue();
			Real32 exhFlow = RExhFlowSensor.getValue();

			// make sure data item pointer is in sync
			RSmManager.advanceTestPoint(
				SmDataId::GAS_SUPPLY_AIR_FLOW_SENSOR_0_OFFSET );

			RSmManager.sendTestPointToServiceData( airFlow);
			RSmManager.sendTestPointToServiceData( o2Flow);
			RSmManager.sendTestPointToServiceData( exhFlow);

			if (airFlow > MAX_INSP_FLOW_SENSOR_0_OFFSET)
			{
			// $[TI9.3.4]
				// air flow sensor 0 offset is high
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_15);
			}
			// $[TI9.3.5]
			// implied else

			if (o2Flow > MAX_INSP_FLOW_SENSOR_0_OFFSET)
			{
			// $[TI9.3.6]
				// O2 flow sensor 0 offset is high
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_16);
			}
			// $[TI9.3.7]
			// implied else

			if (exhFlow > MAX_EXH_FLOW_SENSOR_0_OFFSET)
			{
			// $[TI9.3.8]
				// exh flow sensor 0 offset is high
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_17);
			}
			// $[TI9.3.9]
			// implied else
		}
		// $[TI9.4]
		// implied else: EXIT button was pressed
	}
	// $[TI10]
	// implied else: EXIT button was pressed

	// Perform O2 psol leak checks
	if (!operatorExit && !RSmGasSupply.isAnyAirPresent())
	{
	// $[TI11]
		// prompt to reconnect the O2
		if (!RSmUtilities.messagePrompt(SmPromptId::CONNECT_O2_PROMPT))
		{
		// $[TI11.1]
			operatorExit = TRUE;
		}
		else
		{
		// $[TI11.2]
			if (!RSmGasSupply.isO2Present())
			{
			// $[TI11.2.1]
				// no o2 present
				// Major Failure, Condition 11
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_11);
			}				
			else
			{
			// $[TI11.2.2]
				// make sure data item pointer is in sync
				RSmManager.advanceTestPoint
					(SmDataId::GAS_SUPPLY_O2_PSOL_LEAK_PRESSURE);

				Real32 inspPressureRise = psolLeakTest_();
				if (inspPressureRise > PSOL_LEAK_FAILURE_THRESHOLD)
				{
				// $[TI11.2.2.1]
					// O2 psol leak FAILURE
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_FAILURE,
						SmStatusId::CONDITION_9);
				}
				else if (inspPressureRise > PSOL_LEAK_ALERT_THRESHOLD)
				{
				// $[TI11.2.2.2]
					// O2 psol leak ALERT
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_ALERT,
						SmStatusId::CONDITION_14);
				}
				// $[TI11.2.2.3]
				// implied else: no O2 psol leak
			}
			// end else

			if (wallAirAvailable)
			{
			// $[TI11.2.3]
				// prompt to reconnect wall air
				if (!RSmUtilities.messagePrompt(
									SmPromptId::CONNECT_AIR_PROMPT))
				{
				// $[TI11.2.3.1]
					operatorExit = TRUE;
				}
				// $[TI11.2.3.2]
				// implied else
			}
			// $[TI11.2.4]
			// implied else
		}
		// end else
	}
	// $[TI12]
	// implied else: EXIT button was pressed or air was not
	// disconnected
		
	if (!operatorExit)
	{
	// $[TI13]
		// prompt to unblock port and connect test circuit
		if (!RSmUtilities.messagePrompt(
						SmPromptId::UNBLOCK_INLET_PORT_PROMPT))
		{
		// $[TI13.1]
			RSmManager.sendOperatorExitToServiceData();
		}
		// $[TI13.2]
		// implied else
	}
	else
	{
	// $[TI14]
		RSmManager.sendOperatorExitToServiceData();
	}
	// end else

	// Re-enable compressor control
	RSmGasSupply.enableCompressorAndControl();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
GasSupplyTest::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("GasSupplyTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, GASSUPPLYTEST,
  							 lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: psolLeakTest_()
//
//@ Interface - Description
//	This method tests for psol leaks.  It has no arguments and returns
//	a pressure value which represents the psol leak.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Assumption: only one gas supply is connected -- the appropriate
//	PSOL is leak tested for that gas supply.
//
//	Open SV, wait to for gas to drain from insp module, close SV.  Delay
//	for the prescribed test time then read and return the insp pressure.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
Real32
GasSupplyTest::psolLeakTest_( void)
{
// $[TI1]
	CALL_TRACE("GasSupplyTest::psolLeakTest_( void)") ;

	// open the SV to bleed any built-up insp pressure
	RSmUtilities.openSafetyValve();

// For faster unit testing, don't compile the delays
#ifndef SIGMA_UNIT_TEST
    Task::Delay(1);
#endif  // SIGMA_UNIT_TEST

	// close the safety valve, wait the prescribed test time
	// then measure the built-up insp pressure
	OsTimeStamp timer;
   	RSmUtilities.closeSafetyValve();

// For faster unit testing, don't compile the delays
#ifndef SIGMA_UNIT_TEST
    Task::Delay(0, 3125 - timer.getPassedTime());
#endif  // SIGMA_UNIT_TEST

	Real32 inspPressure = RInspPressureSensor.getValue() ;

#ifdef SIGMA_DEBUG
	Uint32 testTime = timer.getPassedTime();
    printf("psol leak test time = %u msec\n", testTime);
#endif  // SIGMA_DEBUG

	RSmManager.sendTestPointToServiceData(inspPressure);

#ifdef SIGMA_DEBUG
	printf("psol leak = %f cm H20\n", inspPressure);
#endif

   	return( inspPressure);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isCrackingPressureOk_()
//
//@ Interface-Description
//  This method takes no arguments and returns a Boolean indicating TRUE
//	if the test passed, FALSE if not.  It is used to test the safety
//	valve cracking pressure.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Measure the safety valve cracking pressure at a low flow, then 
//	ramp the pressure to the high flow level and determine the
//	safety valve peak pressure.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
GasSupplyTest::isCrackingPressureOk_(void)
{
	CALL_TRACE("SafetySystemTest::isCrackingPressureOk(void)");

	const Real32 INITIAL_FLOW = 1.0f;  // lpm
	const Int32 STABILITY_TIME = 5;  // secs

	Real32 inspiratoryPressure;
	Int16 airPsolCommand = 0;

	Boolean testPassed = TRUE;

	// adjust max and min relief pressures by the pressure error
	Real32 minReliefPressure = MIN_RELIEF_PRESSURE *
			(1 - REL_PRESSURE_ERROR) - ABS_PRESSURE_ERROR;
	Real32 maxReliefPressure = MAX_RELIEF_PRESSURE *
			(1 + REL_PRESSURE_ERROR) + ABS_PRESSURE_ERROR;

	// Establish low flow
	RSmFlowController.establishFlow( INITIAL_FLOW, DELIVERY, AIR);

	// Wait for flow to stabilize
	if ( !RSmFlowController.waitForStableFlow() )
	{
	// $[TI1]
		testPassed = FALSE;

		// Stop the flow controller
		RSmFlowController.stopFlowController();
	}
	// $[TI2]
	// implied else: reached stable flow

	if (testPassed)
	{
	// $[TI3]
		// Stop the flow controller, but keep the PSOL at its last
		// command to keep the flow going
		RSmFlowController.stopFlowController(TRUE);

		// get current psol command so air psol can be commanded directly
		airPsolCommand = RAirPsol.getPsolCommand();

		// Delay to get stable alpha-filtered insp pressure
		Task::Delay(STABILITY_TIME);
		inspiratoryPressure = RInspPressureSensor.getFilteredValue();

		RSmManager.sendTestPointToServiceData( inspiratoryPressure);

		// Verify the safety valve cracking pressure is in range at low flow
		if (inspiratoryPressure < minReliefPressure ||
			inspiratoryPressure > maxReliefPressure)
		{
		// $[TI3.1]
			// Bad Safety Valve
			// or inlet port not blocked
			testPassed = FALSE;

			// close psol
			RAirPsol.updatePsol(0);
		}
		// $[TI3.2]
		// implied else: cracking pressure test passed
	}
	// $[TI4]
	// implied else

	if (testPassed)
	{
	// $[TI5]
		// Now check for peak cracking pressure as flow is ramped

		// clear peak pressure
		Real32 peakPressure = 0.0;

		// set o2 psol to initial count
		Int16 o2PsolCommand = LIFTOFF_STARTING_COUNT;
		RO2Psol.updatePsol(o2PsolCommand);
		Task::Delay( 0, 100);

		// read initial insp flow
		Real32 inspFlow = RAirFlowSensor.getValue() + RO2FlowSensor.getValue();

		// Ramp air psol only to start, and if air alone can't reach
		// PEAK_PRESSURE_TEST_MIN_FLOW, start ramping o2
		while ( (inspFlow < PEAK_PRESSURE_TEST_MIN_FLOW) &&
				( (airPsolCommand < MAX_COUNT_VALUE) ||
				  (o2PsolCommand < MAX_COUNT_VALUE) ) )
		{
		// $[TI5.1]
			inspiratoryPressure = RInspPressureSensor.getValue();

			if (inspiratoryPressure > peakPressure)
			{
			// $[TI5.1.1]
				peakPressure = inspiratoryPressure;
			}
			// $[TI5.1.2]
			// implied else

			// increment air psol count until it maxes out, then increment
			// o2 psol count if max pressure is still not reached
			if (airPsolCommand < MAX_COUNT_VALUE)
			{
			// $[TI5.1.3]
				RAirPsol.updatePsol(++airPsolCommand);
			}
			else
			{
			// $[TI5.1.4]
				RO2Psol.updatePsol(++o2PsolCommand);
			}
			// end else

// For faster unit testing, don't compile the delays
#ifndef SIGMA_UNIT_TEST
			Task::Delay (0, SM_CYCLE_TIME);
#endif  // SIGMA_UNIT_TEST

			inspFlow = RAirFlowSensor.getValue() + RO2FlowSensor.getValue();
		}
		// end while

		// Close the PSOLs
		RAirPsol.updatePsol(0);
		RO2Psol.updatePsol(0);

		RSmManager.sendTestPointToServiceData( peakPressure);

		// Verify the safety valve does not exceed the maximum pressure
		// and that the final flow reached high enough
		if (peakPressure > maxReliefPressure ||
			inspFlow < PEAK_PRESSURE_TEST_MIN_FLOW)
		{
		// $[TI5.2]
			testPassed = FALSE;
		}				
		// $[TI5.3]
		// implied else
	}
	// $[TI6]
	// implied else

	return( testPassed);
}
