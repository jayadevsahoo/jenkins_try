#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  ExhValveVelocityXducerTest
//---------------------------------------------------------------------
//@ Interface-Description
//---------------------------------------------------------------------
//@ Rationale
//	This class tests the ability of the exhalation valve to establish
//  an exhalation pressure given a constant inspiratory flow.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs the test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ExhValveVelocityXducerTest.ccv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 001  By:  fpq    Date:  11-Feb-1997    DR Number: 
//	Project:  Sigma (840)
//	Description:
//		Initial version (Integration baseline).
//
//=====================================================================

#include "ExhValveVelocityXducerTest.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmFlowController.hh"
#include "SmUtilities.hh"
#include "SmManager.hh"
#include "SmConstants.hh"

#include "Task.hh"

#include "ValveRefs.hh"
#include "MainSensorRefs.hh"
#include "ExhalationValve.hh"
#include "PressureSensor.hh"
#include "Psol.hh"

#ifdef SIGMA_DEBUG
# include "RegisterRefs.hh"
# include "FlowSensor.hh"
# include "ExhFlowSensor.hh"
# include "OsTimeStamp.hh"
#endif  // SIGMA_DEBUG

#if defined(SIGMA_DEVELOPMENT)
#include <stdio.h>
#endif  // defined(SIGMA_DEVELOPMENT)

#ifdef SIGMA_UNIT_TEST
# include "FakeIo.hh"
# include "FakeControllers.hh"
# include "FakeTask.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

static const Uint16 NUM_SAMPLES = 11;  // # of pressure samples to integrate
static Boolean StartTest = FALSE;
static Boolean TestCompleted = FALSE;
static Uint16 DampingGainCount = 0;
static Real32 SumExhPressure = 0.0;

#ifdef SIGMA_DEBUG
static Real32 ExhPressure0[NUM_SAMPLES];
static Real32 ExhPressure1[NUM_SAMPLES];
static Real32 EvLoopbackCurrent0[NUM_SAMPLES];
static Real32 EvLoopbackCurrent1[NUM_SAMPLES];
static Uint32 Time0[NUM_SAMPLES];
static Uint32 Time1[NUM_SAMPLES];
static Uint32 SmBkgCycleCount0[NUM_SAMPLES];
static Uint32 SmBkgCycleCount1[NUM_SAMPLES];
static Real32 InspFlow0[NUM_SAMPLES];
static Real32 InspFlow1[NUM_SAMPLES];
static Real32 ExpFlow0[NUM_SAMPLES];
static Real32 ExpFlow1[NUM_SAMPLES];
static OsTimeStamp* PTimer;
static Uint16 PhaseNum = 0;

extern Uint32 SmBkgCycleCounter;
#endif  // SIGMA_DEBUG

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ExhValveVelocityXducerTest
//
//@ Interface - Description
//  	Constructor.
//-----------------------------------------------------------------------
//@ Implementation - Description
//-----------------------------------------------------------------------
//@ PreCondition
//-----------------------------------------------------------------------
//@ PostCondition
//@ End - Method
//=======================================================================
ExhValveVelocityXducerTest::ExhValveVelocityXducerTest( void)
{
	CALL_TRACE("ExhValveVelocityXducerTest::ExhValveVelocityXducerTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~ExhValveVelocityXducerTest
//
//@ Interface - Description
//-----------------------------------------------------------------------
//@ Implementation - Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
ExhValveVelocityXducerTest::~ExhValveVelocityXducerTest( void)
{
	CALL_TRACE("ExhValveVelocityXducerTest::~ExhValveVelocityXducerTest( void)") ;
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTest
//
//@ Interface - Description
//	This method performs the exh valve velocity transducer test.  No
//	arguments, no return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	$[09190]
//	At the specified test flow, command the exh valve with a high damping
//	gain and sample the expiratory pressure at 10 msec intervals for 100
//	msecs.  Sum these pressure samples.
//	Repeat this with a zero damping gain instead.
//	Compare the pressure sums, and if the difference of the pressure sums
//	is below spec, generate a FAILURE.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
void
ExhValveVelocityXducerTest::runTest( void)
{
	CALL_TRACE("ExhValveVelocityXducerTest::runTest( void)") ;

	const Uint16 INITIAL_DAMPING_GAIN_COUNT = 410;  // = 1 V
	const Uint16 FINAL_DAMPING_GAIN_COUNT = 0;
	const Real32 MIN_DELTA_SUM_P = 20.0;  // cmH2O   TBD
#ifdef SIGMA_DEBUG
	Real32 evRestingPressure;
	Real32 testFlow;

	printf("input EV resting pressure: ");
	scanf("%f", &evRestingPressure);
	if (evRestingPressure < 0.0) evRestingPressure = 0.3;
	else if (evRestingPressure > 1.0) evRestingPressure = 0.7;
	printf("EV resting pressure = %4.2f cmH2O\n", evRestingPressure);
	
	printf("input test flow: ");
	scanf("%f", &testFlow);
	if (testFlow < 80.0)  testFlow = 100.0;
	else if (testFlow > 200.0) testFlow = 180.0;
	printf("test flow = %6.2f lpm\n", testFlow);

	const Real32 EV_RESTING_PRESSURE = evRestingPressure;  // cmH2O
	const Real32 TEST_FLOW = testFlow;  // lpm
#else
	const Real32 EV_RESTING_PRESSURE = 0.3;  // cmH2O
	const Real32 TEST_FLOW = 180.0;  // lpm
#endif  // SIGMA_DEBUG

	// autozero the pressure transducers
	RSmUtilities.autozeroPressureXducers();
	
	// set minimum EV bias current
	RExhalationValve.updateValve( EV_RESTING_PRESSURE,
								ExhalationValve::NORMAL );

	// wait for valve to settle
	Task::Delay( 0, 200);

	// ***** Perform test at high damping gain *****
#ifdef SIGMA_DEBUG
	OsTimeStamp timer;
#endif  // SIGMA_DEBUG
	RSmFlowController.establishFlow( TEST_FLOW, DELIVERY, AIR);

	// wait up to 500 msec for controller to stabilize flow
	RSmFlowController.waitForStableFlow(500);

	// stop flow controller but keep PSOL at last command
	RSmFlowController.stopFlowController(TRUE);
	// delay for exp pressure stability
	Task::Delay( 0, 200);

#ifdef SIGMA_DEBUG
	Uint32 stabilityTime0 = timer.getPassedTime();
	timer.now();
	PTimer = &timer;
	PhaseNum = 0;
#endif  // SIGMA_DEBUG

	// check pressure response at non-zero damping gain
	Real32 sumExhPressure0 = testVelocityXducer_(INITIAL_DAMPING_GAIN_COUNT);

#ifdef SIGMA_DEBUG
	Uint32 elapsedTime0 = timer.getPassedTime();
	printf("elapsed time phase 0 = %u msec\n", elapsedTime0);
	printf("stability time phase 0 = %u msec\n", stabilityTime0);
#endif  // SIGMA_DEBUG

	// shut off flow and fully open the exh valve
	RAirPsol.updatePsol(0);
	RSmUtilities.openExhValve();

	// delay for compressor to recharge
	Task::Delay(3);

	// set minimum EV bias current
	RExhalationValve.updateValve( EV_RESTING_PRESSURE,
								ExhalationValve::NORMAL );

	// wait for valve to settle
	Task::Delay( 0, 200);

	// ***** Perform test at low damping gain *****
#ifdef SIGMA_DEBUG
	timer.now();
#endif  // SIGMA_DEBUG
	RSmFlowController.establishFlow( TEST_FLOW, DELIVERY, AIR);
	
	// wait up to 500 msec for controller to stabilize flow
	RSmFlowController.waitForStableFlow(500);
	
	// stop flow controller but keep PSOL at last command
	RSmFlowController.stopFlowController(TRUE);
	// delay for exp pressure stability
	Task::Delay( 0, 200);

#ifdef SIGMA_DEBUG
	Uint32 stabilityTime1 = timer.getPassedTime();
	timer.now();
	PhaseNum = 1;
#endif  // SIGMA_DEBUG

	// check pressure response at zero damping gain
	Real32 sumExhPressure1 =  testVelocityXducer_(FINAL_DAMPING_GAIN_COUNT);

#ifdef SIGMA_DEBUG
	Uint32 elapsedTime1 = timer.getPassedTime();
	printf("elapsed time phase 1 = %u msec\n", elapsedTime1);
	printf("stability time phase 1 = %u msec\n", stabilityTime1);
#endif  // SIGMA_DEBUG

	// take difference of pressure sums
	const Real32 DELTA_SUM_P = sumExhPressure1 - sumExhPressure0;
	RSmManager.sendTestPointToServiceData(DELTA_SUM_P);

	// shut off flow and open exh valve
	RAirPsol.updatePsol(0);
	RSmUtilities.openExhValve();

	// Compare results
	if (DELTA_SUM_P < MIN_DELTA_SUM_P)
	{
	// $[TI1]
		RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_1);
	}
	// $[TI2]
	// implied else: test passed

#ifdef SIGMA_DEBUG
	printf("DELTA_SUM_P     = %.4f\n", DELTA_SUM_P );
	printf("MIN_DELTA_SUM_P = %.4f\n", MIN_DELTA_SUM_P );

	printf("***** DATA *****\n");
	for (Int32 idx = 0; idx < NUM_SAMPLES; idx++)
	{
		printf("%3u %3u %7.4f %7.2f %7.2f %7.2f    %3u %3u %7.4f %7.2f %7.2f %7.2f\n",
			SmBkgCycleCount0[idx], Time0[idx], ExhPressure0[idx], EvLoopbackCurrent0[idx],
															InspFlow0[idx], ExpFlow0[idx],
			SmBkgCycleCount1[idx], Time1[idx], ExhPressure1[idx], EvLoopbackCurrent1[idx],
															InspFlow1[idx], ExpFlow1[idx] );
	}
#endif  // SIGMA_DEBUG

}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: testVelocityXducer_()
//
//@ Interface - Description
//	This method initiates one phase of the exh valve velocity transducer
//	test.  Argument: the desired damping gain count.  Returns the
//	pressure sum.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	Set StartTest TRUE and TestCompleted FALSE to initiate the test phase
//	(which runs in another task), then wait until it completes.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
Real32
ExhValveVelocityXducerTest::testVelocityXducer_(Uint16 dampingGainCount)
{
	CALL_TRACE("ExhValveVelocityXducerTest::testVelocityXducer_(\
											Uint16 dampingGainCount)") ;

	// sync to next VRTX tick
	Task::Delay( 0, 10);

	// setup to command the exh valve via the sm bkg cycle
	DampingGainCount = dampingGainCount;
	TestCompleted = FALSE;
	StartTest = TRUE;  // do this AFTER setting all other variables

	// wait for test to complete (running in sm background cycle)
	while (!TestCompleted)
	{
	// $[TI1]
		Task::Delay( 0, 10);

#ifdef SIGMA_UNIT_TEST
		// since SM background cycle is not running for unit test,
		// need to call runTestInBkg() here or else we're stuck in
		// the loop!
		runTestInBkg();
#endif  // SIGMA_UNIT_TEST
	}
	// end while

	return(SumExhPressure);
}


// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: runTestInBkg()
//
//@ Interface - Description
//	This method performs one phase of the exh valve velocity transducer
//	test.  It is implemented as a state machine, executing every 10 msecs
//	within the BD executive task.  No arguments, no return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//	The static Boolean StartTest is set TRUE by testVelocityXducer_() to
//	start the state machine.
//	State 0: test idle.
//	State 1: StartTest is TRUE => close exh valve with the appropriate
//			 damping gain, read pressure sample 1, set StartTest FALSE,
//			 set testEnabled TRUE.
//	States 2-10: Read pressure samples 2 through 10, update pressure sum.
//	State 11: Read pressure sample 11, update pressure sum, set TestCompleted
//			  TRUE to signal the end of the test to the initiating task,
//			  set testEnabled FALSE to reset the state machine to state 0.
//-----------------------------------------------------------------------
//@ PreCondition
//	none
//-----------------------------------------------------------------------
//@ PostCondition
//	none
//@ End - Method
//=======================================================================
void
ExhValveVelocityXducerTest::runTestInBkg(void)
{
	CALL_TRACE("ExhValveVelocityXducerTest::setExhValve(void)") ;

	static Uint32 idx = 0;
	static Boolean testEnabled = FALSE;

	if (StartTest)
	{
	// $[TI1]
		// 
		const Real32 TEST_PRESSURE = 14.1;  // cmH2O

		// command the EV and EV damping gain DACs
		RExhalationValve.updateValve( TEST_PRESSURE, ExhalationValve::NORMAL );
// E600 BDIO		REVDampingGainDacPort.writeRegister(DampingGainCount);
	
		testEnabled = TRUE;
		StartTest = FALSE;
		SumExhPressure = 0.0;
		idx = 0;

#ifdef SIGMA_DEBUG
		SmBkgCycleCounter = 0;
#endif  // SIGMA_DEBUG

	}
	// $[TI2]
	// implied else

	if (testEnabled)
	{
	// $[TI3]
#ifdef SIGMA_DEBUG
		if (PhaseNum == 0)
		{
			ExhPressure0[idx] = RExhPressureSensor.getValue();
			EvLoopbackCurrent0[idx] = RExhalationValve.getExhValveCurrent();
			Time0[idx] = PTimer->getPassedTime();
			SmBkgCycleCount0[idx] = SmBkgCycleCounter;
			InspFlow0[idx] = RAirFlowSensor.getValue();
			ExpFlow0[idx] = RExhFlowSensor.getValue();
		}
		else
		{
			ExhPressure1[idx] = RExhPressureSensor.getValue();
			EvLoopbackCurrent1[idx] = RExhalationValve.getExhValveCurrent();
			Time1[idx] = PTimer->getPassedTime();
			SmBkgCycleCount1[idx] = SmBkgCycleCounter;
			InspFlow1[idx] = RAirFlowSensor.getValue();
			ExpFlow1[idx] = RExhFlowSensor.getValue();
		}
#endif  // SIGMA_DEBUG

		SumExhPressure += RExhPressureSensor.getValue();
		idx++;
		if (idx >= NUM_SAMPLES)
		{
		// $[TI3.1]
			// set flags to end the test
			testEnabled = FALSE;
			TestCompleted = TRUE;
		}
		// $[TI3.2]
		// implied else
	}
	// $[TI4]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
ExhValveVelocityXducerTest::SoftFault( const SoftFaultID  softFaultID,
									   const Uint32       lineNumber,
									   const char*        pFileName,
									   const char*        pPredicate)
{
	CALL_TRACE("ExhValveVelocityXducerTest::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, EXHVALVEVELOCITYXDUCERTEST,
  							 lineNumber, pFileName, pPredicate) ;
}
