
#ifndef Alarm_Analysis_HH
#define Alarm_Analysis_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Alarm_Analysis - The  Alarm_Analysis Subsystem.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/Alarm_Analysis.hhv   25.0.4.0   19 Nov 2013 13:51:18   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 09/02/97         DR Number:  2355
//      Project:   Sigma   (R8027)
//      Description:
//         Hardcoded module name ID numbers.
//  
//   Revision 003   By:   sah      Date: 15-Jan-1998      DR Number:  5004
//      Project:   Sigma   (R8027)
//      Description:
//         Obsoleted class ID for the 'AlarmMessages' class, which has
//         been moved to the GUI-Apps subsystem, and renamed 'AlarmStrs'.
//  
//   Revision 005   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "AlarmConstants.hh"
#include "TemplateMacros.hh"

//@ Usage-Classes
//@ End-Usage

class Alarm_Analysis 
{
  public:
    
    //@ Type:  Alarm_AnalysisClassId
    // Ids of all of the classes/files of this subsystem.
    enum Alarm_AnalysisClassId {
      ALARM                      = 0,
      ALARMANALYSISINFORMATION   = 1,
      ALARMANNUNCIATOR           = 2,
      ALARMAUGMENTATION          = 3,
      ALARMAUGMENTATIONLIST      = 4,
      ALARMBROADCAST             = 5,
      ALARMBROADCASTARRAY        = 6,
      ALARMCOMM                  = 7,
      ALARMCOMMLIST              = 8,
      ALARMDCI                   = 9,
      ALARMEVENT                 = 10,
      ALARMINFORMATION           = 11,
      ALARMLIST                  = 12,
      ALARMMANAGER               = 13,
      /* ALARMMESSAGES           = 14, OBSOLETED */
      ALARMOPERAND               = 15,
      ALARMOPERANDLIST           = 16,
      ALARMRESPONSES             = 17,
      ALARMUPDATE                = 18,
      ALARMUPDATEBUFFER          = 19,
      ALARMUPDATEBUFFERLIST      = 20,
      ALARMUPDATELIST            = 21,
      ALARMUPDATESORTEDLIST      = 22,
      CALCFUNCTIONS              = 23,
      CHECKALARMLIST             = 24,
      CHILDLIST                  = 25,
      CONDITIONAUGMENTATION      = 26,
      CONDITIONEVALUATIONS       = 27,
      CONSTRAINT                 = 28,
      COUNTCONSTRAINT            = 29,
      DEPENDENTAUGMENTATION      = 30,
      DEPENDENTINFORMATION       = 31,
      MESSAGENAMEARRAY           = 32,
      OPERANDNAMELIST            = 33,
      OPERATINGCOMPONENT         = 34,
      OPERATINGCONDITION         = 35,
      OPERATINGGROUP             = 36,
      OPERATINGGROUPLIST         = 37,
      TEMPORALCONSTRAINT         = 38,
      TIMESTAMP                  = 39,
      VALIDVHLIST                = 40,
      VHMCURRBREATHCOUNTCARDINAL = 41,
      VHMPREVBREATHCOUNTCARDINAL = 42,
      VHMCURRBREATHCOUNTDUR      = 43,
      VHMEVENTCOUNTDUR           = 44,
      VHMTEMPORAL                = 45,
      VIOLATIONHISTORY           = 46,
      VIOLATIONHISTORYLIST       = 47,
      VIOLATIONHISTORYMANAGER    = 48,
      VIOLATIONTIMER             = 49,
      VIOLATIONTIMERLIST         = 50,
      ALARM_ANALYSIS_CLASS       = 51,
      ALARMREPOSITORY            = 52,
      NUM_ALARM_ANALYSIS_CLASSES
    };

    static void Initialize(void);

    static void     SoftFault(const SoftFaultID softFaultID,
							  const Uint32      lineNumber,
							  const char*       pFileName  = NULL, 
							  const char*       pPredicate = NULL);

  private:
    Alarm_Analysis();                           // defined but not implemented
    Alarm_Analysis(const Alarm_Analysis&);      // defined but not implemented
    ~Alarm_Analysis();                          // defined but not implemented

    void operator = (const Alarm_Analysis&);

};


#endif // Alarm_Analysis_HH 
