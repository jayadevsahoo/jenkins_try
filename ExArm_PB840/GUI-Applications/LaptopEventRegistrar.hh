#ifndef LaptopEventRegistrar_HH
#define LaptopEventRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

//====================================================================
// Class: LaptopEventRegistrar - The central registration, control, and
// dispatch point for External Control events received by GUI-Apps.  The
// update events are sent from the GUI-Serial-Interface subsystem via the GUI
// task's User Annunciation queue.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LaptopEventRegistrar.hhv   25.0.4.0   19 Nov 2013 14:08:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: gdc    Date: 22-Jan-2001   DR Number: 5493
//  Project:  GuiComms
//  Description:
//      Implemented SMCP on additional serial ports.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By: gdc      Date: 24-Nov-1997  DR Number: 2605
//    Project:  Sigma (R8027)
//    Description:
//     Changed interface to GUI-Serial-Interface as part of its 
//     restructuring.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "UserAnnunciationMsg.hh"
#include "LaptopEventId.hh"
#include "SerialInterface.hh"
#include "SmcpPacket.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
class LaptopEventTarget;
class LaptopEventCascade;
//@ End-Usage


class LaptopEventRegistrar
{
public:

	static void LaptopRequestHappened(Int32 portNum);
	static void LaptopFailureHappened(
					SerialInterface::SerialFailureCodes serialFailureCodes,
					Int32 portNum);
	static void RegisterTarget(LaptopEventId::LTEventId eventId,
							   LaptopEventTarget* pTarget);
	static void Initialize(void);

	static SerialInterface::PortNum GetControlPort(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	LaptopEventRegistrar(void);							// not implemented...
	LaptopEventRegistrar(const LaptopEventRegistrar&);	// not implemented...
	~LaptopEventRegistrar(void);						// not implemented...
	void operator=(const LaptopEventRegistrar&);		// not implemented...

	
	//@ Data-Member: CascadeArray_
	// A static array of LaptopEventCascade objects, implemented as
	// a simple pointer to an object.  The index to the array corresponds
	// to the LaptopGuiEventId::LaptopGuiEventIdType and provides fast lookup.
	static LaptopEventCascade *CascadeArray_;

	//@ Data-Member: PMsg_
	// A static string array to holds the incoming/outgoing laptop
	// messges.
	static Uint8 PMsg[ sizeof(SmcpMessage) ];

	//@ Data-Member: ControlPortNum_
	// Contains the port number of the initiating serial control port
	static Int32 ControlPortNum_;
};


#endif // LaptopEventRegistrar_HH 
