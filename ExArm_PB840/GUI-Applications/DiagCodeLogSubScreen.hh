#ifndef DiagCodeLogSubScreen_HH
#define DiagCodeLogSubScreen_HH

//    =====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: DiagCodeLogSubScreen - The screen selected by pressing the
// Diagnostic Code Log button on the Upper Other Screens Subscreen and
// the Service Upper Screen.
// It displays system, network communication and EST/SST diagnostic codes.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/DiagCodeLogSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: rhj    Date: 16-Jan-2008   SCR Number: 6423
//  Project:  Baseline
//	Description:
//		Added enableDateLocalization_ which is used to toggle
//      between the date localization format or the original date 
//      format.
// 
//  Revision: 005   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//	Optimized graphics library. Implemented Direct Draw mode for drawing
//  outside of BaseContainer refresh cycle.
//
//  Revision: 004  By:  hhd	   Date:  11-Mar-1999    DCS Number: 5340 
//  Project:  ATC
//  Description:
//		 Added new inline method clearLogEntries().
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date:  10-Sep-97    DR Number: 1923
//    Project:  Sigma (R8027)
//    Description:
//      Changed the name of communications Diagnostic log to the system
//		information log.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"
#include "LogTarget.hh"
#include "NovRamEventTarget.hh"
#include "GuiTimerTarget.hh"
#include "LaptopEventTarget.hh"

//@ Usage-Classes
#include "Bitmap.hh"
#include "CodeLogEntry.hh"
#include "Array_CodeLogEntry_MAX_CODE_LOG_ENTRIES.hh"
#include "NovRamUpdateManager.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class SubScreenArea;
class ScrollableLog;

class DiagCodeLogSubScreen : public SubScreen, 
							 public LogTarget, 
							 public NovRamEventTarget,
							 public GuiTimerTarget,
							 public LaptopEventTarget 
				
{
public:
	DiagCodeLogSubScreen(SubScreenArea* pSubScreenArea);
	~DiagCodeLogSubScreen(void);

	enum LogType
	{
		//@ Constant: DL_GUI_SYSTEM
		// Indicates a System Diagnostic log.
		DL_SYSTEM = 0,

        //@ Constant: DL_SYS_INFO
        // Indicates a Comm Diagnostic log.
        DL_SYS_INFO,

        //@ Constant: DL_EST_SST
        // Indicates a EST/SST Diagnostic log.
        DL_EST_SST,

        //@ Constant: DL_NUMBER_OF_TYPES
        // Evaluates to the number of defined log types.  Must be
        // the last definition in the enumerated type.
        DL_NUMBER_OF_TYPES
    };

	// Inherited from SubScreen
	virtual void activate(void);
	virtual void deactivate(void);

	// Inherited from LogTarget
	virtual void getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
						Boolean &rIsCheapText,
						StringId &rString1, StringId &rString2,
						StringId &, StringId &);

	// Inherited from NovRamEventTarget
	virtual void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
																	updateId);

	// LaptopEventTarget virtual methods
	virtual void laptopRequestHappened(LaptopEventId::LTEventId eventId,
											Uint8 *pMsgData);

	// Convenient methods for setting and retrieving logType_ value
	inline void setLogType(int logType);
	inline int getLogType(void);

    // ButtonTarget virtual method
	void buttonDownHappened(Button* pButton, Boolean isByOperatorAction);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	DiagCodeLogSubScreen(void);							// not implemented...
	DiagCodeLogSubScreen(const DiagCodeLogSubScreen&);	// not implemented...
	void operator=(const DiagCodeLogSubScreen&);		// not implemented...

	// Method to fill message buffer with data being transmitted to the serial
	// port
	void fillMessageBuffer_(LaptopEventId::LTEventId);

	// Method to update diagnostic log's display
	void updateDiagnosticLogDisplay_(NovRamUpdateManager::UpdateTypeId updateTypeId);

	NovRamUpdateManager::UpdateTypeId getNovRamUpdateId(int);

	inline void clearLogEntries();

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen
	SubScreenTitleArea titleArea_;

	//@ Data-Member: pLog_
	// Pointer to scrollable log
	ScrollableLog *pLog_;

	//@ Data-Member: logType_
	// The type of diagnostic log on display
	int logType_;

	//@ Data-Member: goBackButton_
	// Button to go back to previous screen
	TextButton	goBackButton_;

	//@ Data-Member: leftArrowBitmap_
	// Button bitmap for goBackButton_
	Bitmap	leftArrowBitmap_;

	//@ Data-Member: logEntries_
	// Array of NovRam's diagnostic code entries.
	FixedArray(CodeLogEntry,MAX_CODE_LOG_ENTRIES) logEntries_;

	//@ Data-Member: TestResultString_
	// Array of test result types
   	StringId TestResultString_[::NUM_TEST_RESULT_IDS]; 

	//@ Data-Member: enableDateLocalization_
    // A flag which toogles between the date 
	// international format or the original
	// date format.
	Boolean enableDateLocalization_;
};

// Inlined methods
#include "DiagCodeLogSubScreen.in"

#endif // DiagCodeLogSubScreen_HH 
