#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: DiagnosticCode - Diagnostic Code Layout Class.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class defines the layout of a diagnostic code, and stores fault
//  information in that format.  This class is used to store fault
//  information in a standard diagnostic code format so as to be stored
//  in the various diagnostic logs.
//
//  There are a number of methods that provide for storing the fault
//  information of the different types of diagnostic events.  These methods
//  accept the appropriate information parameters, and store that
//  information, along with the fault type, in this diagnostic code.
//
//  Each of the diagnostic code layouts are defined as public, bit-field
//  stuctures of this class.
//---------------------------------------------------------------------
//@ Rationale
//  This class is the only class that needs to "know" the format of
//  the different diagnostic codes, thereby minimizing the "change-ripple"
//  effect in the future.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains an anonymous union of the layouts for the
//  the diagnostic types, along with an instance of a 96-bit structure.
//  This allows for direct access to named bit-fields, along with direct
//  access to the entire code's value.
//
//  The first four bits of each code are exactly the same for each type
//  of diagnostic code.  A private bit-field structure (see
//  'CommonLayoutBits_') takes advantage of the common layout.  If the
//  field size or location of this common field is altered, all of the bit
//  fields must recieve the corresponding change -- including
//  'CommonLayoutBits_'.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Standard pre-conditions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/DiagnosticCode.ccv   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  rhj    Date:  16-Apr-2009    SCR Number: 6495
//  Project:  840S2
//  Description:
//		Added a single test parameter to setExtendedSelfTestCode 
//      function.
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 003  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 002  By:  sah    Date:  13-May-1997    DCS Number: 2085
//  Project:  Sigma (R8027)
//  Description:
//      Added the storing of auxillary information for POST diagnostics
//      (see 'setStartupSelfTestCode()').
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//  
//=====================================================================

#include "DiagnosticCode.hh"

#if defined(SIGMA_GUI_CPU)
#include <stdio.h>
#endif // defined(SIGMA_GUI_CPU)

#include "Utilities.hh"

//@ Usage-Classes...
#include "kpost.hh"
#include "Task.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

const DiagnosticCode::CpuEventId  DiagnosticCode::CPU_EVENT_ID_
#if defined(SIGMA_BD_CPU)
					    = DiagnosticCode::BD_CPU_DIAG;
#elif defined(SIGMA_GUI_CPU)
					    = DiagnosticCode::GUI_CPU_DIAG;
#endif


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DiagnosticCode(diagCode)  [Copy Constructor]
//
//@ Interface-Description
//  Construct a default diagnostic code, using 'diagCode' for initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiagnosticCode::DiagnosticCode(const DiagnosticCode& diagCode)
{
  CALL_TRACE("DiagnosticCode(diagCode)");
  operator=(diagCode);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DiagnosticCode()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default diagnostic code, with a code of '0'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (sizeof(DiagnosticCode::SoftwareTestBits) ==
//   sizeof(DiagnosticCode::ExceptionBits)
//  (sizeof(DiagnosticCode::ExceptionBits) ==
//   sizeof(DiagnosticCode::NmiBits)
//  (sizeof(DiagnosticCode::NmiBits) ==
//   sizeof(DiagnosticCode::BackgroundTestBits)
//  (sizeof(DiagnosticCode::BackgroundTestBits) ==
//   sizeof(DiagnosticCode::CommunicationBits)
//  (sizeof(DiagnosticCode::CommunicationBits) ==
//   sizeof(DiagnosticCode::StartupSelfTestBits)
//  (sizeof(DiagnosticCode::StartupSelfTestBits) ==
//   sizeof(DiagnosticCode::ShortSelfTestBits)
//  (sizeof(DiagnosticCode::ShortSelfTestBits) ==
//   sizeof(DiagnosticCode::ExtendedSelfTestBits)
//  (sizeof(DiagnosticCode::ExtendedSelfTestBits) ==
//   sizeof(DiagnosticCode::CommonLayoutBits_)
//  (sizeof(DiagnosticCode::CommonLayoutBits_) ==
//   sizeof(Uint96))
//---------------------------------------------------------------------
//@ PostCondition
//  (isClear())
//@ End-Method
//=====================================================================

DiagnosticCode::DiagnosticCode(void)
{
  CALL_TRACE("DiagnosticCode()");

  // make sure all of the bit-layout structures are all the same size...
  SAFE_CLASS_ASSERTION((sizeof(DiagnosticCode::SoftwareTestBits) ==
			sizeof(DiagnosticCode::ExceptionBits)));
  SAFE_CLASS_ASSERTION((sizeof(DiagnosticCode::ExceptionBits) ==
			sizeof(DiagnosticCode::NmiBits)));
  SAFE_CLASS_ASSERTION((sizeof(DiagnosticCode::NmiBits) ==
			sizeof(DiagnosticCode::BackgroundTestBits)));
  SAFE_CLASS_ASSERTION((sizeof(DiagnosticCode::BackgroundTestBits) ==
			sizeof(DiagnosticCode::CommunicationBits)));
  SAFE_CLASS_ASSERTION((sizeof(DiagnosticCode::CommunicationBits) ==
			sizeof(DiagnosticCode::StartupSelfTestBits)));
  SAFE_CLASS_ASSERTION((sizeof(DiagnosticCode::StartupSelfTestBits) ==
			sizeof(DiagnosticCode::ShortSelfTestBits)));
  SAFE_CLASS_ASSERTION((sizeof(DiagnosticCode::ShortSelfTestBits) ==
			sizeof(DiagnosticCode::ExtendedSelfTestBits)));
  SAFE_CLASS_ASSERTION((sizeof(DiagnosticCode::ExtendedSelfTestBits) ==
			sizeof(DiagnosticCode::CommonLayoutBits_)));
  SAFE_CLASS_ASSERTION((sizeof(DiagnosticCode::CommonLayoutBits_) ==
			sizeof(Uint96)));

  clearCode();
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DiagnosticCode()  [Destructor]
//
//@ Interface-Description
//  Destroy this diagnostic code.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiagnosticCode::~DiagnosticCode(void)
{
  CALL_TRACE("~DiagnosticCode()");
}   // $[TI1]


#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getDiagCodeString()  [const]
//
//@ Interface-Description
//  Return the corresponding diagnostic code representation (e.g., "AS0411")
//  for the diagnostic code that is represented in this instance.  The
//  characters for this string are stored into 'pDiagCodeStr' for return.
//
//  NOTE:  it is left as the responsibility of the caller to ensure that
//	   the passed array is large enough for a 6-character code and
//	   terminating character.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
DiagnosticCode::getDiagCodeString(wchar_t* pDiagCodeString) const
{
  CALL_TRACE("getDiagCodeString(pDiagCodeString)");

  switch (commonBits_.diagId)
  {
  case DiagnosticCode::SYSTEM_EVENT_DIAG :		// $[TI1]
    {
      const wchar_t  CATEGORY_CHAR =
	      (systemEventCode_.cpuFlagId == DiagnosticCode::BD_CPU_DIAG)
	       ? L'E' : L'S';	// $[TI1.1] $[TI1.2]

      swprintf(pDiagCodeString, L"%c%5d", CATEGORY_CHAR,
	      systemEventCode_.systemEventId);
    }
    break;
  case DiagnosticCode::SOFTWARE_TEST_DIAG :		// $[TI2]
    {
      const wchar_t  CATEGORY_CHAR =
	      (softwareTestCode_.cpuFlagId == DiagnosticCode::BD_CPU_DIAG)
	       ? L'D' : L'U';	// $[TI2.1] $[TI2.2]

      swprintf(pDiagCodeString, L"%c%2d%3d", CATEGORY_CHAR,
	      softwareTestCode_.subsystemId, softwareTestCode_.moduleId);
    }
    break;
  case DiagnosticCode::EXCEPTION_DIAG :			// $[TI3]
    {
      // TBD:  need to map to 'D', 'G', or 'H', based on trap ID...
      const wchar_t  CATEGORY_CHAR =
	      (exceptionCode_.cpuFlagId == DiagnosticCode::BD_CPU_DIAG)
	       ? L'D' : L'U';	// $[TI3.1] $[TI3.2]

      swprintf(pDiagCodeString, L"%cT0%3d", CATEGORY_CHAR,
	      exceptionCode_.vectorNumber);
    }
    break;
  case DiagnosticCode::NMI_DIAG :			// $[TI4]
    {
      // TBD:  do these all of these cause a reset?...
      const wchar_t  CATEGORY_CHAR =
	      (nmiCode_.cpuFlagId == DiagnosticCode::BD_CPU_DIAG)
	       ? L'H' : L'V';	// $[TI4.1] $[TI4.2]

      swprintf(pDiagCodeString, L"%cN0%3d", CATEGORY_CHAR,
	      nmiCode_.nmiSourceId);
    }
    break;
  case DiagnosticCode::COMMUNICATION_DIAG :		// $[TI5]
    {
      const wchar_t  CATEGORY_CHAR =
	      (communicationCode_.cpuFlagId == DiagnosticCode::BD_CPU_DIAG)
	       ? L'L' : L'Z';	// $[TI5.1] $[TI5.2]

      swprintf(pDiagCodeString, L"%cC%4d", CATEGORY_CHAR,
	      communicationCode_.commFaultId);
    }
    break;
  case DiagnosticCode::STARTUP_SELF_TEST_DIAG :		// $[TI6]
    {
      wchar_t  categoryChar;

      if (startupSelfTestCode_.cpuFlagId == DiagnosticCode::BD_CPU_DIAG)
      {   // $[TI6.1]
         categoryChar = (startupSelfTestCode_.failureLevel ==
						  ::MAJOR_TEST_FAILURE_ID)
	                 ? L'K' : L'L';	// $[TI6.1.1]  $[TI6.1.2]
      }
      else
      {   // $[TI6.2]
         categoryChar = (startupSelfTestCode_.failureLevel ==
						  ::MAJOR_TEST_FAILURE_ID)
	                 ? L'X' : L'Z';	// $[TI6.2.1]  $[TI6.2.2]
      }

      swprintf(pDiagCodeString, L"%cP0%3d", categoryChar,
	      startupSelfTestCode_.startupFaultId);
    }
    break;
  case DiagnosticCode::BACKGROUND_TEST_DIAG :		// $[TI7]
    {
      wchar_t  categoryChar;

      if (backgroundTestCode_.cpuFlagId == DiagnosticCode::BD_CPU_DIAG)
      {   // $[TI7.1]
         categoryChar = (backgroundTestCode_.failureLevel ==
						    ::MAJOR_TEST_FAILURE_ID)
	                 ? L'K' : L'L';	// $[TI7.1.1]  $[TI7.1.2]
      }
      else
      {   // $[TI7.2]
         categoryChar = (backgroundTestCode_.failureLevel ==
						    ::MAJOR_TEST_FAILURE_ID)
	                 ? L'X' : L'Z';	// $[TI7.2.1]  $[TI7.2.2]
      }

      swprintf(pDiagCodeString, L"%cB%4d", categoryChar,
	      backgroundTestCode_.testNumber);
    }
    break;
  case DiagnosticCode::SHORT_SELF_TEST_DIAG :		// $[TI8]
    {
      const wchar_t  FAILURE_LEVEL_CHAR =
	(shortSelfTestCode_.failureLevel == ::MAJOR_TEST_FAILURE_ID)
	 ? L'F' : L'A';	// $[TI8.1] $[TI8.2]

      swprintf(pDiagCodeString, L"%cS%2d%2d", FAILURE_LEVEL_CHAR,
	      shortSelfTestCode_.testNumber, shortSelfTestCode_.stepNumber);
    }
    break;
  case DiagnosticCode::EXTENDED_SELF_TEST_DIAG :	// $[TI9]
    {
      const wchar_t  FAILURE_LEVEL_CHAR =
      (extendedSelfTestCode_.failureLevel == ::MAJOR_TEST_FAILURE_ID)
       ? L'F' : L'A';	// $[TI9.1] $[TI9.2]

      swprintf(pDiagCodeString, L"%cE%2d%2d", FAILURE_LEVEL_CHAR,
	      extendedSelfTestCode_.testNumber,
	      extendedSelfTestCode_.stepNumber);
    }
    break;
  case DiagnosticCode::UNDEFINED_DIAG_CODE :		// $[TI10]
  case DiagnosticCode::NUM_FAULT_TYPE_IDS :
  default :
    pDiagCodeString[0] = L'\0';
    break;
  };

  // guarantee a terminated string...
  pDiagCodeString[6] = L'\0';

  for (Uint idx = 0; pDiagCodeString[idx] != L'\0'; idx++)
  {
    if (pDiagCodeString[idx] == L' ')
    {
      pDiagCodeString[idx] = L'0';
    }
  }
}

#endif // defined(SIGMA_GUI_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setSystemEventCode(systemEventId, auxEventCode)
//
//@ Interface-Description
//  Set up this diagnostic code to contain the given system event.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getDiagCodeTypeId() == DiagnosticCode::SYSTEM_EVENT_DIAG)
//@ End-Method
//=====================================================================

void
DiagnosticCode::setSystemEventCode(
			  const DiagnosticCode::SystemEventId systemEventId,
			  const Uint32                        auxEventCode
			  	  )
{
  CALL_TRACE("setSystemEventCode(systemEventId, auxEventCode)");

  clearCode(); // clear out all of the bits of this code...

  systemEventCode_.diagId        = DiagnosticCode::SYSTEM_EVENT_DIAG;
  systemEventCode_.cpuFlagId     = DiagnosticCode::CPU_EVENT_ID_;
  systemEventCode_.systemEventId = systemEventId;
  systemEventCode_.auxEventCode  = auxEventCode;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setSoftwareTestCode(softFaultId, subsystemId, moduleId,
//  		                 lineNumber, auxErrorCode)
//
//@ Interface-Description
//  Set up this diagnostic code to contain the given software test 
//  diagnostic.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getDiagCodeTypeId() == DiagnosticCode::SOFTWARE_TEST_DIAG)
//@ End-Method
//=====================================================================

void
DiagnosticCode::setSoftwareTestCode(const SoftFaultID softFaultId,
				    const SubSystemID subsystemId,
				    const Uint16      moduleId,
				    const Uint16      lineNumber,
				    const Uint32      auxErrorCode)
{
  CALL_TRACE("setSoftwareTestCode(softFaultId, subsystemId, moduleId, lineNumber, auxErrorCode)");
  CLASS_PRE_CONDITION((softFaultId > UNDEFINED_SOFT_FAULT  &&
		       softFaultId < NUM_SOFT_FAULT_IDS));


  clearCode(); // clear out all of the bits of this code...

  softwareTestCode_.diagId       = DiagnosticCode::SOFTWARE_TEST_DIAG;
  softwareTestCode_.cpuFlagId    = DiagnosticCode::CPU_EVENT_ID_;
  softwareTestCode_.assertionId  = softFaultId;
  //this is the Task number, not necessarely the actual thread ID
  softwareTestCode_.activeTaskId = Task::GetTaskBaseInfo().GetIndex();
  softwareTestCode_.subsystemId  = (Uint8)subsystemId;
  softwareTestCode_.moduleId     = moduleId;
  softwareTestCode_.lineNumber   = lineNumber;
  softwareTestCode_.auxErrorCode = auxErrorCode;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setExceptionCode(vectorNumber, instructionAddr, faultAddr,
//			      taskId)
//
//@ Interface-Description
//  Set up this diagnostic code to contain the given exception diagnostic.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getDiagCodeTypeId() == DiagnosticCode::EXCEPTION_DIAG)
//@ End-Method
//=====================================================================

void
DiagnosticCode::setExceptionCode(const Uint16 vectorNumber,
				 const Uint32 instructionAddr,
				 const Uint32 faultAddr,
				 const Uint16 taskId)
{
  CALL_TRACE("setExceptionCode(vectorNumber, instructionAddr, faultAddr, taskId)");

  clearCode(); // clear out all of the bits of this code...

  exceptionCode_.diagId          = DiagnosticCode::EXCEPTION_DIAG;
  exceptionCode_.cpuFlagId       = DiagnosticCode::CPU_EVENT_ID_;
  exceptionCode_.vectorNumber    = vectorNumber;
  exceptionCode_.activeTaskId    = taskId;
  exceptionCode_.instructionAddr = instructionAddr;
  exceptionCode_.faultAddr       = faultAddr;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setNmiCode(nmiSourceId, errorCode)
//
//@ Interface-Description
//  Set up this diagnostic code to contain the given non-maskable
//  interrupt diagnostic.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getDiagCodeTypeId() == DiagnosticCode::NMI_DIAG)
//@ End-Method
//=====================================================================

void
DiagnosticCode::setNmiCode(const Uint16 nmiSourceId,
			   const Uint32 errorCode)
{
  CALL_TRACE("setExceptionCode(nmiSourceId, errorCode)");

  clearCode(); // clear out all of the bits of this code...

  nmiCode_.diagId      = DiagnosticCode::NMI_DIAG;
  nmiCode_.cpuFlagId   = DiagnosticCode::CPU_EVENT_ID_;
  nmiCode_.nmiSourceId = nmiSourceId;
  nmiCode_.errorCode   = errorCode;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setCommunicationCode(commFaultId)
//
//@ Interface-Description
//  Set up this diagnostic code to contain the communication diagnostic
//  identified by the passed parameter.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getDiagCodeTypeId() == DiagnosticCode::COMMUNICATION_DIAG)
//@ End-Method
//=====================================================================

void
DiagnosticCode::setCommunicationCode(const Uint32 commFaultId)
{
  CALL_TRACE("setCommunicationCode(commFaultId)");

  clearCode(); // clear out all of the bits of this code...

  communicationCode_.diagId      = DiagnosticCode::COMMUNICATION_DIAG;
  communicationCode_.cpuFlagId   = DiagnosticCode::CPU_EVENT_ID_;
  communicationCode_.commFaultId = commFaultId;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setStartupSelfTestCode(failureCriticality, startupFaultId)
//
//@ Interface-Description
//  Set up this diagnostic code to contain the startup self test diagnostic
//  identified by the passed parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getDiagCodeTypeId() == DiagnosticCode::STARTUP_SELF_TEST_DIAG)
//@ End-Method
//=====================================================================

void
DiagnosticCode::setStartupSelfTestCode(const TestResultId failureCriticality,
				       const Uint16       startupFaultId,
				       const Uint32       programCounter,
				       const Uint8        vectorNum,
				       const Uint8        nmiSourceReg,
				       const Uint8        auxErrorCode)
{
  CALL_TRACE("setStartupSelfTestCode()");

  clearCode(); // clear out all of the bits of this code...

  startupSelfTestCode_.diagId        = DiagnosticCode::STARTUP_SELF_TEST_DIAG;
  startupSelfTestCode_.cpuFlagId     = DiagnosticCode::CPU_EVENT_ID_;
  startupSelfTestCode_.failureLevel  = failureCriticality;
  startupSelfTestCode_.startupFaultId= startupFaultId;
  startupSelfTestCode_.programCounter= programCounter;
  startupSelfTestCode_.vectorNum     = vectorNum;
  startupSelfTestCode_.nmiSourceReg  = nmiSourceReg;
  startupSelfTestCode_.auxErrorCode  = auxErrorCode;
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setBackgroundTestCode(testFailureLevel,
//				   backgroundTestNumber, errorCode)
//
//@ Interface-Description
//  Set up this diagnostic code to contain the background test diagnostic
//  identified by the passed parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getDiagCodeTypeId() == DiagnosticCode::BACKGROUND_TEST_DIAG)
//@ End-Method
//=====================================================================

void
DiagnosticCode::setBackgroundTestCode(
				  const TestResultId testFailureLevel,
				  const Uint16       backgroundTestNumber,
				  const Uint32       errorCode
				     )
{
  CALL_TRACE("setBackgroundTestCode(testFailureLevel, backgroundTestNumber, errorCode)");

  clearCode(); // clear out all of the bits of this code...

  backgroundTestCode_.diagId       = DiagnosticCode::BACKGROUND_TEST_DIAG;
  backgroundTestCode_.cpuFlagId    = DiagnosticCode::CPU_EVENT_ID_;
  backgroundTestCode_.failureLevel = testFailureLevel;
  backgroundTestCode_.testNumber   = backgroundTestNumber;
  //this is the Task number, not necessarely the actual thread ID
  backgroundTestCode_.activeTaskId = Task::GetTaskBaseInfo().GetIndex();
  backgroundTestCode_.errorCode    = errorCode;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setShortSelfTestCode(testResultId,
//				  sstTestNumber, testStepNumber)
//
//@ Interface-Description
//  Set up this diagnostic code to contain the SST diagnostic identified
//  by the passed parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getDiagCodeTypeId() == DiagnosticCode::SHORT_SELF_TEST_DIAG)
//@ End-Method
//=====================================================================

void
DiagnosticCode::setShortSelfTestCode(const TestResultId testResultId,
				     const Uint16       sstTestNumber,
				     const Uint16       testStepNumber)
{
  CALL_TRACE("setShortSelfTestCode(testResultId, sstTestNumber, testStepNumber)");

  clearCode(); // clear out all of the bits of this code...

  shortSelfTestCode_.diagId       = DiagnosticCode::SHORT_SELF_TEST_DIAG;
  shortSelfTestCode_.failureLevel = testResultId;
  shortSelfTestCode_.testNumber   = sstTestNumber;
  shortSelfTestCode_.stepNumber   = testStepNumber;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setExtendedSelfTestCode(testResultId,
//				     estTestNumber, testStepNumber,
//                   isSingleTest)
//
//@ Interface-Description
//  Set up this diagnostic code to contain the EST diagnostic identified
//  by the passed parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (getDiagCodeTypeId() == DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
//@ End-Method
//=====================================================================

void
	DiagnosticCode::setExtendedSelfTestCode(const TestResultId testResultId,
											const Uint16       sstTestNumber,
											const Uint16       testStepNumber,
											const Boolean      isSingleTest)
{
	CALL_TRACE("setExtendedSelfTestCode(testResultId, sstTestNumber, testStepNumber, isSingleTest)");

	clearCode(); // clear out all of the bits of this code...

	extendedSelfTestCode_.diagId      = DiagnosticCode::EXTENDED_SELF_TEST_DIAG;
	extendedSelfTestCode_.failureLevel= testResultId;
	extendedSelfTestCode_.testNumber  = sstTestNumber;
	extendedSelfTestCode_.stepNumber  = testStepNumber;
	extendedSelfTestCode_.isSingleTest = isSingleTest;
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                  [static]
//
//@ Interface-Description
//  Report the software fault that occurred within the source code
//  of this class.  The fault, indicated by 'softFaultID', occurred
//  at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================
 
void
DiagnosticCode::SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName,
			  const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, DIAGNOSTIC_CODE,
                          lineNumber, pFileName, pBoolTest);
}
