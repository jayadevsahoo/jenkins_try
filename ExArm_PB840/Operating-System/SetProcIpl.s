; vi:ts=8:
;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, stored in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================
;
; =========================== M O D U L E   D E S C R I P T I O N ====
;@ Filename: SetIplIsr.s - Set 68K IPL Interrupt Service Routine (ISR)
;---------------------------------------------------------------------
;@ Interface-Description
;  This module contains the assembly function to set the interrupt
;  processing level (IPL) of the 680X0 to the level specified.  
;---------------------------------------------------------------------
;@ Rationale
; n/a
;---------------------------------------------------------------------
;@ Implementation-Description
; n/a
;---------------------------------------------------------------------
;@ Fault-Handling
; n/a
;---------------------------------------------------------------------
;@ Restrictions
; n/a
;---------------------------------------------------------------------
;@ Invariants
; n/a
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version-Information
; @(#) $Header:   /840/Baseline/Operating-System/vcssrc/SetProcIpl.s_v   25.0.4.0   19 Nov 2013 14:16:20   pvcs  $
;
;@ Modification-Log
;
;    Revision 001   By: Gary Cederquist Date: 28-MAY-1997 DR Number: 2176
;       Project:   Sigma   (R8027)
;       Description:
;          Initial version.
; 
;=====================================================================


;====================== P R O C E D U R E   D E S C R I P T I O N ====
;@ Procedure:  _SetProcIpl - Set 68K Interrupt Priority Level
;
;@ Interface-Description
;  This function is called by sigma_trap_handler when a Sigma TRAP 
;  (TRAP #1) occurs with a function code of 2.  It sets the 680X0
;  interrupt processing level to the level specified and returns the 
;  previous level to the caller.  The trap to invoke this function
;  is defined in SetProcessorIPL.  Register D1 contains the desired
;  interrupt processing level.  The new interrupt level is contained on
;  the stack as a result of the call to SetProcessorIPL.  The old
;  interrupt level contained on in the SR on the stack is returned in
;  D0.  The new interrupt level is masked into the stack's SR.  When an
;  RTE occurs in the trap handler, the new interrupt level takes
;  effect.
;
;  At entry, the stack looks like this:
;
;		15			0
;		+-----------------------+
;	    +0	| trap handler ret hi   | <- sp at entry
;		+-----------------------+
;	    +2	| trap handler ret lo   | 
;		+-----------------------+
;	    +4	|          sr           |
;		+-----------------------+
;	    +6  |         pc hi         |
;		+-----------------------+
;	    +8	|         pc lo         |
;		+.......................+
;	   +10	| 0000 | vector  offset |
;		+-----------------------+
;
;  At exit (RTS), the stack looks like this:
;
;		15			0
;		+-----------------------+
;	    +0	| trap handler ret hi   | <- SP before RTS
;		+-----------------------+
;	    +2	| trap handler ret lo   | 
;		+-----------------------+
;	    +4	|      modified sr      | <- SP after RTS
;		+-----------------------+
;	    +6  |         pc hi         |
;		+-----------------------+
;	    +8	|         pc lo         |
;		+.......................+
;	   +10	| 0000 | vector  offset |
;		+-----------------------+
;
;---------------------------------------------------------------------
;@ Implementation-Description
;  See Interface-Description
;---------------------------------------------------------------------
;@ PreCondition
;  none
;---------------------------------------------------------------------
;@ PostCondition
;  none
;@ End-Procedure
;=====================================================================

; stack offsets after register save (D2)
	OFFSET	0
	DS.L	1			; D2 save area
        DS.L    1			; trap handler return address
STACKSR:
	DS.W	1			; SR at TRAP
	DS.L	1			; PC at TRAP
	DS.W	1			; FORMAT at TRAP
	DS.L	1			; SetProcessorIPL return address
STACKIPL:
	DS.L	1			; SetProcessorIPL argument


; void SetProcIpl()
; {
	SECTION	code,4,C
	XDEF	_SetProcIpl

_SetProcIpl:
	movem.l	d2,-(sp)		; save registers

	and.l	#7,d1			; make sure IPL is between 0 and 7

	move.w	STACKSR(sp),d0		; get trap status register
	and.l	#$0700,d0		; isolate the bits of interest...
	ror.l	#8,d0			; ...and normalize them into LSB
					; for the return value

	move.w	STACKSR(sp),d2		; get trap status register
	and.w	#$F8FF,d2		; get rid of current IPL bits
	rol.l	#8,d1			; get new IPL bits into proper position
	or.l	d1,d2			; insert them
	move.w	d2,STACKSR(sp)		; and restore it (it will become
					; effective when we do the rte)

	movem.l	(sp)+,d2		; restore registers

	rts			        ; our modified SR becomes effective
					; when trap handler returns

;                                       // $[TI1]
; }
	END
