
// ****************************************************************************
//
//  DebugAssert.h - This contains a macro for handling asserts.
//      We want to trap failed assertions rather than use the
//      standard WinCE assertion handling. For debugging,
//      set a breakpoint in TrapDebugAssert.
//
// ****************************************************************************

#ifndef DEBUG_ASSERT_H

#define DEBUG_ASSERT_H

#include "FileUtilities.hh"


// This macro allows the various threads to access a common handler
// which the application traps.
#define DEBUG_ASSERT(Condition){                                    \
    if (!(Condition))                                               \
    {                                                               \
        TrapDebugAssert(_T(__FILE__), __LINE__);                    \
    } }


// This macro allows the various threads to access a common handler
// which the application traps. This version also writes a message
// to the DebugDataThread.
#define DEBUG_ASSERT_MESSAGE(Condition, Message)                    \
{                                                                   \
    if (!(Condition))                                               \
    {                                                               \
        TrapDebugAssert(Message, _T(__FILE__), __LINE__);           \
    }                                                               \
}



#endif


