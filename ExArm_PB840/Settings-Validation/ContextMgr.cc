#include "stdafx.h"
  
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//	          Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ContextMgr - Manager of the Context Instances.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is a static class that provides access, via references, to
//  each, and every, context instance.  The responsibilities of this class
//  include initializing ALL of the context instances, and providing access
//  to each of those instances.  This class merely provides a convenient
//  means -- via static methods -- of getting access to the various context
//  instances.
//
//  This class will be incorporated on both the GUI and BD CPUs.  Access
//  methods for the various contexts are available based on the CPU that
//  is being generated:  the Accepted, Adjusted and Recoverable Contexts
//  are available on the GUI CPU only, the Pending Context has versions
//  for both the GUI and BD CPU, and the Phased-In Context is only available
//  on the BD CPU.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a central repository for all of the context
//  instances.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class contains static references to each of the context instances.
//  Like the access methods, these references are conditionally compiled in,
//  or out, based on the CPU that the system is being built for.  If
//  'SIGMA_GUI_CPU' is defined, only the GUI-applicable context instances are
//  available. If 'SIGMA_BD_CPU' is defined, only the BD-applicable context
//  instances are available.  To facilitate easier development, the
//  'SIGMA_COMMON' pre-processor variable, if defined, will make ALL of the
//  contexts available.  (NOTE:  the "build" utility ensures that only ONE
//  of the three aforementioned pre-processor variables are defined at a
//  time.)
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ContextMgr.ccv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: sah  Date:  11-Jan-1999    DR Number:  5321
//  Project:  ATC
//  Description:
//	As part of making all 'SoftFault()' methods non-inlined, I'm
//	also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//	
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ContextMgr.hh"

//@ Usage-Classes
#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
#include "AcceptedContext.hh"
#include "AdjustedContext.hh"
#include "RecoverableContext.hh"
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
#include "PendingContext.hh"
#include "PhasedInContext.hh"
#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Data Definitions...
//
//=====================================================================
 
#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

static Uint  PAcceptedContext_[
		(sizeof(AcceptedContext) + sizeof(Uint) - 1) / sizeof(Uint)
			      ];

static Uint  PAdjustedContext_[
		(sizeof(AdjustedContext) + sizeof(Uint) - 1) / sizeof(Uint)
			      ];

static Uint  PRecoverableContext_[
	     (sizeof(RecoverableContext) + sizeof(Uint) - 1) / sizeof(Uint)
				 ];
 
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

static Uint  PPendingContext_[
		(sizeof(PendingContext) + sizeof(Uint) - 1) / sizeof(Uint)
			     ];
 

static Uint  PPhasedInContext_[
	       (sizeof(PhasedInContext) + sizeof(Uint) - 1) / sizeof(Uint)
			      ];
 
#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
 

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

AcceptedContext&     ContextMgr::RAcceptedContext_ =
				  *((AcceptedContext*)::PAcceptedContext_);
AdjustedContext&     ContextMgr::RAdjustedContext_ =
				  *((AdjustedContext*)::PAdjustedContext_);
RecoverableContext&  ContextMgr::RRecoverableContext_ =
			    *((RecoverableContext*)::PRecoverableContext_);

#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)


#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

PendingContext&  ContextMgr::RPendingContext_ =
				  *((PendingContext*)::PPendingContext_);

PhasedInContext&   ContextMgr::RPhasedInContext_ =
				  *((PhasedInContext*)::PPhasedInContext_);

#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize(void)  [static]
//
//@ Interface-Description
//  Initialize each, and every, applicable context instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The order of the initialization MATTERS!  The Accepted Context must
//  be initialized BEFORE the Adjusted, Recoverable and Phased-In Contexts.
//  Another dependency is that the Pending Context must be initialized
//  BEFORE the Phased-In Context.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextMgr::Initialize(void)
{
  CALL_TRACE("ContextMgr::Initialize()");

#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

  // initialize the Accepted Context...
  SAFE_CLASS_ASSERTION((sizeof(::PAcceptedContext_) >=
			sizeof(AcceptedContext)));
  new (::PAcceptedContext_) AcceptedContext(); 

  // initialize the Adjusted Context...
  SAFE_CLASS_ASSERTION((sizeof(::PAdjustedContext_) >=
			sizeof(AdjustedContext)));
  new (::PAdjustedContext_) AdjustedContext(); 

  // initialize the Recoverable Context...
  SAFE_CLASS_ASSERTION((sizeof(::PRecoverableContext_) >=
			sizeof(RecoverableContext)));
  new (::PRecoverableContext_) RecoverableContext(); 

#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)


#if defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)

  // initialize the Phased-In Context...
  SAFE_CLASS_ASSERTION((sizeof(::PPhasedInContext_) >=
			sizeof(PhasedInContext)));
  new (::PPhasedInContext_) PhasedInContext(); 

  // initialize the Pending Context (AFTER the Phased-In Context)...
  SAFE_CLASS_ASSERTION((sizeof(::PPendingContext_) >=
			sizeof(PendingContext)));
  new (::PPendingContext_) PendingContext(); 

#endif // defined(SIGMA_BD_CPU)  ||  defined(SIGMA_COMMON)
}  // $[TI1]


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ContextMgr::SoftFault(const SoftFaultID  softFaultID,
		      const Uint32       lineNumber,
		      const char*        pFileName,
		      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION, CONTEXT_MGR,
  			  lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
