;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================
;
;
;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: integer.s 
;
;---------------------------------------------------------------------
;@ Interface-Description
;  >Von
;
;  This module contains a test that exercises the integer unit of the 68040.
;  During POST, after the processor is set up, one of the first tests is       
;  to verify the microprocessor.  Basic CPU functions are 
;  tested in the Integer Unit test of the Kernel.  The addressing modes, 
;  memory management, data cache, and floating point units are tested in 
;  later in POST.
;
;  Most instructions have the ability to execute using a number of 
;  addressing modes.  The instruction set test validates a single addressing 
;  mode per instruction, preferably using registers if applicable.  If an 
;  effective address <ea> is required, a register will be used.   
;
;
;               Instructions Used In Integer Unit Test
;
;
;  Instruction    Category                Description
;  =================================================================
;
;  ADD.L          Integer Arithmetic      Arithmetic Add
;  ADDA.W         Integer Arithmetic      Address Add
;  ADDQ.L         Integer Arithmetic      Add quick
;  ANDI.L         Logical operation       And immediate
;  BEQ.L          Program Control         Branch equals
;  BGE.S          Program Control         Branch greater or equal
;  BGT.S          Program Control         Branch greater than
;  BNE.S          Program Control         Branch not equal
;  BRA.S          Program Control         Branch unconditionally
;  CLR.L          Integer Arithemetic     Clear  and operand
;  CMP.L          Integer Arithemetic     Compare
;  JSR            Program control         Jump to subroutine
;  LEA.L          Data Movement           Load effective address
;  LINK           Data Movement           Link and allocate
;  MOVE.L         Data Movement           Move Long
;  MOVE.W         Data Movement           Move Word
;  MOVEA.L        Data Movement           Move address long
;  MOVEM.L        Data Movement           Move multiple, long
;  MOVEQ          Data Movement           Move quick
;  PEA            Data Movement           Push effective address
;  RTS            Data Movement           Return from subroutine
;  TST.B          Program control         Test an operand
;  UNLK           Data Movement           Unlink
;  ADD            Integer Arithmetic      Integer Add
;  MULT           Integer Arithmetic      Integer Multiply
;  SHL            Integer Arithmetic      Shift Left
;  SHR            Integer Arithmetic      Shift Right
;
;  >Voff
;
;@ Implementation-Description
;  See description above
;
;@ Restrictions
;  The static RAM part of NOVRAM is used so no DRAM is required, but this
;  assumes the NOVRAM static RAM is functional.
;  
;  This test also assumes that the microprocessor is in a reasonable 
;   state of readiness - Vcc to the processor is stable, and the hardware 
;   has reset the CPU in a proper manner.
;  
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
;
; @(#) $Header:   /840/Baseline/Kernel/vcssrc/integer.s_v   25.0.4.0   19 Nov 2013 14:13:54   pvcs  $
;
;@ Modification-Log
;
;
;  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;    Project: Sigma (R8027)
;    Description:
;       Added testable item annotations.
;
;  Revision: 002    By: Gary Cederquist  Date: 16-MAY-1997  DR Number: 2121
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options from source, using command line instead.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================
;
;
;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: _IntegerUnitTest
;
;@ Interface-Description
;  >Von
;    This routine implements the 68040 processor integer unit test of the  
;    POST Kernel.  This verifies the microprocessor's operation. $[11010]
;    
;
;    Inputs:  NONE
;    Outputs: NONE  
;
;  The integer unit of the 68040 consists of six stages of pipelined
;  operations.  To test this unit a register test of the 16 general
;  purpose registers (D0-D7, A0-A7), and the 8 bit condition code
;  register (CCR) is performed.  The patterns 0, 1s, 0101.., 1010..,
;  are used.  The NZVC bits of the CCR are tested using logical and
;  arithmetic instructions.  Once the registers are verified, the
;  instructions in the table below are exercised.  This test is written
;  in assembly language.
;
;  The remaining portion of the integer unit test is dedicated to
;  exercise the instructions as noted in the following table.  The 6
;  stages (instruction fetch, decode, calculate, address fetch,
;  execute, and write back) portions of this unit is exercised when
;  these instructions are used.
;
;  A small amount of EPROM SRAM as addressed by the stack pointer is
;  used for this test.  This memory will be implicitly tested, if this
;  memory is nonfunctional, the instruction test will fail.  If an
;  instruction uses different fields in operation encoding, the
;  instruction will use a single combination using  reasonable
;  selections to validate the instruction.   The approach here is that
;  the same microcode is exercised, regardless of the addressing mode
;  or field selections or which register is used, and to test all
;  permutations of instruction variations is prohibitive and
;  unnecessary.
;
;  A CPU test may not be able to test every operation code of the
;  microprocessor.  For example, testing the HALT command is
;  meaningless, and multiprocessor instructions are not applicable in
;  the Sigma architecture.  Likewise testing an obscure operation of
;  the microprocessor (such as BFFFO, SBCD, ROXL) is time prohibitive.
;  Generally only a small number of instructions  are used to form the
;  bulk of the application software, a profile analysis was performed
;  on a representative sample of C++ code from the Sigma application to
;  indicate which CPU instructions to test during  POST.
;
;  This test is broken into the following sections:
;
;     Program control - Verify that comparisions, branches and jumps
;     work.  This is done first to verify that we can branch to an
;     error state if one is detected.
;
;     Register test.  Patterns are rippled through D0-D7, A0-A7.
;
;     Condition code register.  The XNZVC bits of this register are
;     tested.
;
;     Individual instructions.  See the module header for the list of
;     instructions tested.
;
;  The instruction cache and data cache are off during this test.
;
;  If this test fails, kernelError is called directly.
;
;  REGISTER USAGE:
;
;  The registers D0-D7, A0-A7 are used during this test.
;  These registers are NOT preserved.
;
;  >Voff
;---------------------------------------------------------------------
;@ Implementation-Description
;  See description above
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================
      TTL   'IntegerUnitTest.s - POST CPU Integer Unit Test'

      NAME   integerUnit

      INCLUDE  kernel.inc

      SECTION code,4,C

      XREF     IntegerUnitTestReturnLocation
      XREF     _SetTestResult__FUc
      XREF     _KernelError__Fv

      XDEF     _IntegerUnitTest__Fv
; void IntegerUnitTest() {
_IntegerUnitTest__Fv:
;

;  In the first steps, make sure you can jump & branch properly.
      jmp      next0                   ; test jump

;  At this point jump did not work, if jump does not work jsr probably won't work
;  either.  Try a branch first, if that does not work then reset the processor.
      bra      integerError         
      reset

next0:
      bra      next1                   ; test branch
      jmp      integerError            ; error - branch did not work
      reset

next1:
;  Now check that you can compare properly
      moveq    #1,d0                   ; test comparison and branch      
      cmp      #1,d0               
      bne      integerError            ; error if not equal

;  Flags test - part 1
      move     #0,ccr                  ; clears NZVC flags
      bmi      integerError            ; error if negative           (N)
      beq      integerError            ; error if zero set           (Z)
      bvs      integerError            ; error if overflow is set    (V)
      bcs      integerError            ; error if carry is set       (C)

;  Flags test - part 2, the converse of part 1
      move.l   #7fffffffh,d0           ; start with the largest positive number
      addi.l   #100,d0                 ; cause a overflow
      bvc      integerError            ; error if overflow is clear  (V)
      neg      d0                      ; make it negative
      bpl      integerError            ; error if positive           (N)
      beq      integerError            ; error if zero flag is set   (Z)
      bcc      integerError            ; error if carry is not set   (C)

;  Test the Extend bit (X)
      move     #0,ccr                  ; clear the extend bit (X)
      move.l   #1,d0                   ; rotate a 1 around d0   
      roxr.l   #3,d0                   ; the extend bit must work or compare will fail
      cmp.l    #40000000h,d0
      bne      integerError            

      move     #0,ccr                  ; clear the flags register when flag sectionis done




;   Register test - Use a macro here to simplify code
TESTPATTERN   MACRO   P1
      move.l   #P1,d0                  ; Ripple pattern through registers
      move.l   d0,d1
      move.l   d1,d2
      move.l   d2,d3
      move.l   d3,d4   
      move.l   d4,d5
      move.l   d5,d6 
      move.l   d6,d7
      move.l   d7,a0
      move.l   a0,a1
      move.l   a1,a2
      move.l   a2,a3
      move.l   a3,a4   
      move.l   a4,a5
      move.l   a5,a6 
      move.l   a6,a7                 ; unable to test stack pointer
      cmp.l    #P1,a7                ; Compare final register with orig pattern
      bne      integerError
      ENDM


;   Now use this macro to test the following patterns
      TESTPATTERN 0                    ; all zeros
      TESTPATTERN 0ffffffffh           ; all ones
      TESTPATTERN 0aaaaaaaah           ; alternating ones 101010
      TESTPATTERN 055555555h           ; alternating ones 010101
      TESTPATTERN 031415926h           ; another known pattern

;   Done testing address registers so reset stack pointer

      movea.l  #P_INITIAL_STACK,sp


;   Cache control should be off (0)
      movec    cacr,d0              
      cmp      #CACR_INITIAL_VALUE,d0
      bne      integerError


;   Test miscellaneous instructions - refer to design document for list
      move.l   #1234567,d1             ; Compare that 1234567+72359+7-43=1306890
      add.l    #72359,d1               ; using long arithmetic
      addq.l   #7,d1
      subi.l   #43,d1
      cmpi.l   #1306890,d1             ; note - 1306890 = 13F10Ah
      beq.l    next2            
      jmp      integerError      

;  some arithemetic using word lengths
next2:
      move.w   d1,a1                   ; truncated result of above should be F10A=61706
      adda.w   #1708,a1                ; 61706+1708=63414 (f7b6h)
      cmpa.w   #63414,a1                 
      bne.w    integerError

      move.l   #0aaaaaaaah,d2          ; test and immediate
      andi.l   #000f00f00h,d2
      cmpi.l   #000a00a00h,d2
      bne.l    integerError
      clr.l    d2                      ; test clear
      bne.l    integerError

      move.b   #2,d3                   ; test short branches
      cmp.b    #2,d3
      bne.s    next3                   ; Error - branch to a short displacement
      bgt.s    next4                   
      cmp.b    #5,d3                   ; 5 should be greater than 2
      bge.s    next3                   
      bra.s    next4
next3:
      jmp      integerError            ; any branch errors go here

next4:
      move.l   #0ff000000h,a0          ; Grab a location in EPROM
      movem.l  (a0),d1/d2/d3           ; test multiple moves
      movem.l  (a0),a1/a2/a3           ; test multiple moves

      cmp.l    a1,d1             
      bne.l    integerError
      cmp.l    a2,d2
      bne.l    integerError
      cmp.l    a3,d3
      bne.l    integerError

;   The following exercises a common pass by argument routine of the form
;
;   int a;
;   foo(&a);
;   if (a != 10) integerError;
;
;   where:
;      foo(int *param)
;      {
;         *param = 10;
;      }
;
;   Note: the stack is inherently used here.   If this fails either the
;   instructions are not working properly or stack memory has failed.
;
      link     a0,#-4      ;   associate an address register with a stack location
      pea      -4(a0)
      jsr      _jsrTest__Fv
      moveq    #10,d0
      cmp.l    -4(a0),d0
      bne.l    integerError
      unlk     a0

;    test multiple move instruction
      move.l   #0,a1
      link     a1,#-8
      move.l   #11111111h,d1                 ; init registers to known values
      move.l   #22222222h,d2
      move.l   #33333333h,d3
      move.l   #44444444h,d4
      move.l   #55555555h,d5

      movem.l  d1/d2/d3/d4/d5,-(sp)          ; move registers to stack
      move.l   #0,d0                         ; corrupt the registers
      move.l   d0,d1
      move.l   d0,d2
      move.l   d0,d3
      move.l   d0,d4
      move.l   d0,d5
      movem.l  -28(a1),d1/d2/d3/d4/d5        ; move back the values from stack

      cmp.l    #11111111h,d1                 ; perform the comparisions
      bne.l    integerError
      cmp.l    #22222222h,d2
      bne.l    integerError
      cmp.l    #33333333h,d3
      bne.l    integerError
      cmp.l    #44444444h,d4
      bne.l    integerError
      cmp.l    #55555555h,d5
      bne.l    integerError
      unlk    a1

;  test load effective address using stack locations 12 & 16
      link     a1,#-16                 ; create a working area of stack
                                       ; note: A1=SP, SP = SP-16, 
      lea.l    -12(a1),a0              ; load address of stack-12 to a0   
      move.l   a0,-16(a1)              ; copy address of stack-12 to stack(0)
      lea.l    (sp),a2                 ; load address of stack to a2
      move.l   (a2),a3                 ; copy contents of stack(0)
      cmp.l    a0,a3                   ; a0 should match a3
      bne.l    integerError
      unlk     a1

;  test integer multiply, divide
      move.l   #52933,d0
      mulu.l   #70625,d0
      cmp.l    #3738393125,d0          ; test 52933*70625=3738393125
      bne.l    integerError
      move.l   #52933,d1
      divul.l  d1,d0
      cmp.l    #70625,d0               ; test 36676812/52933=70625
      bne.l    integerError   

;  test shift right & left
      move.l   #0ffffffffh,d0
      lsr.l    #7,d0
      cmp.l    #01ffffffh,d0      
      bne.l    integerError
      lsl.l    #7,d0
      cmp.l    #0ffffff80h,d0
      bne.l    integerError

      jmp      IntegerUnitTestReturnLocation     ; Jump to next test in sequence

;   end of IntegerUnitTest
;
;==============================================================================
; // $[TI1.1]

;
;
integerError:                  
; // $[TI1.2]
      movea.l  #P_INITIAL_STACK,sp
      pea      MAJOR
      jsr      _SetTestResult__FUc         ; log the error if possible
      jmp      _KernelError__Fv            ; end of the road


      XDEF     _jsrTest__Fv
_jsrTest__Fv
      movea.l  4(sp),a1
      moveq    #10,d0
      move.l   d0,(a1)
      rts
      jmp      integerError         ; if code reaches here rts did not work

; }
      END
