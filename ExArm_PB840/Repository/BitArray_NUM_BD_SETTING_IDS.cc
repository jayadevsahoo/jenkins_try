#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  BitArray_NUM_BD_SETTING_IDS - Fixed Array of 'NUM_BD_SETTING_IDS' Bits.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a derived class of an array of 'NUM_BD_SETTING_IDS' bits
//  ('BitArray').  Array operations are provided, and each
//  accessible element in the array is a bit.  Safety is ensured by
//  checking all bit accesses for out-of-range behavior.  There are
//  operations to get the maximum number of bits, along with the number
//  that are either set (turned "on") or clear (turned "off").  There
//  are also operations to test, and alter single bits, or all of the
//  bits.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a safe and consistent way of using a collection of
//  boolean values.  It is very compact -- each bit represents a single
//  boolean as opposed to an entire byte -- and there are both
//  single-bit and multiple-bit operations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An array of integers is used to store all of the bits.  This array
//  is big enough to hold 'NUM_BD_SETTING_IDS' bits.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedBitArray.C_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 08-Feb-1999   DR Number: 5339
//   Project:  ATC
//   Description:
//	Removed 'SIGMA_DEVELOPMENT' flags from around 'SoftFault()'
//	method, to allow for mixed-mode builds.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//	Also, as part of making all 'SoftFault()' methods non-inlined,
//	I'm also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "BitArray_NUM_BD_SETTING_IDS.hh"

//@ Usage-Classes...
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BitArray_NUM_BD_SETTING_IDS(bitArray)  [Copy Constructor]
//
//@ Interface-Description
//  Construct this bit array of 'NUM_BD_SETTING_IDS' bits, and initialize all
//  of the bits to the bits contained in 'bitArray'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The address to the first integer of the integer array, along with the
//  total number of bits, is passed to the base class's constructor.
//  After the base class finishes constructing, each of the integers
//  from 'bitArray' is copied into this bit array.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == bitArray  &&  getMaxBits() == NUM_BD_SETTING_IDS)
//@ End-Method
//=====================================================================

BitArray_NUM_BD_SETTING_IDS::BitArray_NUM_BD_SETTING_IDS(const BitArray_NUM_BD_SETTING_IDS& bitArray)
					 : BitArray(pElems_, NUM_BD_SETTING_IDS)
{
  CALL_TRACE("BitArray_NUM_BD_SETTING_IDS(bitArray)");

  copyAllElems_(bitArray.pElems_);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BitArray_NUM_BD_SETTING_IDS()  [Default Constructor]
//
//@ Interface-Description
//  Construct this bit array of 'NUM_BD_SETTING_IDS' bits, and initialize all
//  of the bits to '0'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The address to the first integer of the integer array, along with the
//  total number of bits, is passed to the base class's constructor.
//  After the base class finishes constructing, each of the integers
//  in this bit array are initialized to '0'.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (areAllClear()  &&  getMaxBits() == NUM_BD_SETTING_IDS)
//@ End-Method
//=====================================================================

BitArray_NUM_BD_SETTING_IDS::BitArray_NUM_BD_SETTING_IDS(void)
					 : BitArray(pElems_, NUM_BD_SETTING_IDS)
{
  CALL_TRACE("BitArray_NUM_BD_SETTING_IDS()");

  setAllElems_(0u);         // initialize all integers to '0'...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~BitArray_NUM_BD_SETTING_IDS()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this bit array.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

BitArray_NUM_BD_SETTING_IDS::~BitArray_NUM_BD_SETTING_IDS(void)
{
  CALL_TRACE("~BitArray_NUM_BD_SETTING_IDS()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                     [static]
//
//@ Interface-Description
//  Report the software fault that occured within the source code
//  of this class.  The fault, indicated by 'softFaultID', occurred
//  at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
BitArray_NUM_BD_SETTING_IDS::SoftFault(const SoftFaultID softFaultID,
                               const Uint32      lineNumber,
                               const char*       pFileName,
                               const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, FIXED_BIT_ARRAY,
			  lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr)  [const]
//
// Interface-Description
//  This overrides the base class's pure virtual method, and dumps
//  out the contents of this bit array to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
BitArray_NUM_BD_SETTING_IDS::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "BitArray_NUM_BD_SETTING_IDS {" << endl;
  ostr << "  getMaxElems_() = " << (Int)getMaxElems_() << endl;
  ostr << "  getMaxBits()   = " << (Int)getMaxBits() << endl;
  ostr << "  pElems_[] {";

  for (Uint currBit = 0; currBit < getMaxBits(); currBit++)
  {
    if ((currBit % ::BITS_PER_LWORD) == 0)
    {
      // new line for every integer...
      ostr << endl;
      ostr << "    ";
    }
    else if ((currBit % 8) == 0)
    {
      // seperator between every 8 bits...
      ostr << " ";
    }

    ostr << (char)('0' + isBitSet(currBit));
  }

  ostr << endl;
  ostr << "  }" << endl;
  ostr << "}" << endl;

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
