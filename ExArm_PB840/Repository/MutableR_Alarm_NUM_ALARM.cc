#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  MutableR_Alarm_NUM_ALARM - Mutable Fixed Sorted List of 'Alarm' References.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a derived class of an abstract, size-independent sorted
//  list of 'Alarm' references ('AbstractSortR').  This class provides
//  an abstraction for a sorted list of references to data items each
//  of type 'Alarm'.  The data items of the list are references to the
//  data items that are added to the list, and are sorted -- based on
//  the value of the items, themselves -- upon insertion.  This is a
//  fixed list where, at most, 'NUM_ALARM' items can be on the list at one
//  time.  The items of this sorted list can be changed via direct
//  manipulation of the referenced item -- bypassing this list.
//  Therefore, this class provides writable access to its items along
//  with a re-sorting method to re-sort this list if an item has changed
//  by either of the two aforementioned update techniques.
//
//  Unlike the mutable sorted list of COPIES, this list derives directly
//  from the abstract sorted list.  Because the data items are managed
//  external to this list -- this list just has references to them --
//  the value of these data can be changed by by-passing the list.
//  Therefore, since it cannot be guaranteed that this list is always
//  sorted -- as can the 'FixedSortC<Alarm,NUM_ALARM>' class -- only "mutable"
//  sorted list are available for references.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a sorted collection of sortable objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Items are sorted as they're inserted into the list.  A doubly-
//  linked list of copies is used internally to keep the items.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions namely have to do with the data type that is
//  chosen for this list.  The data type must provide the following
//  mechanisms, to be allowed to be used in this list:
//>Von
//      Alarm::operator==(const Alarm& aType) const;// equivalence operator...
//      Alarm::operator!=(const Alarm& aType) const;// inequivalence operator...
//      Alarm::operator>(const Alarm& aType) const; // greater-than operator...
//      Alarm::operator<(const Alarm& aType) const; // less-than operator...
//>Voff
//  For primitive (built-in) types these are already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      Since these lists contain pointers to the inserted items, those
//      items that are inserted into these lists must have a lifetime that
//      is longer than the lifetime of these lists' reference to them.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedMutableSortR.C_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 08-Feb-1999   DR Number: 5339
//   Project:  ATC
//   Description:
//	Removed 'SIGMA_DEVELOPMENT' flags from around 'SoftFault()'
//	method, to allow for mixed-mode builds.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//	Also, as part of making all 'SoftFault()' methods non-inlined,
//	I'm also making those 'SoftFault()' methods that are NOT called
//	in PRODUCTION mode, DEVELOPMENT-only.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "MutableR_Alarm_NUM_ALARM.hh"
#include "Alarm.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  MutableR_Alarm_NUM_ALARM(sortList)  [Copy Constructor]
//
//@ Interface-Description
//  Construct this sorted list using 'sortList'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A pointer to this list's reference list is passed to the base
//  class's constructor.  The copying of 'sortList' MUST be initiated
//  from this constructor, as opposed to the base class's constructor,
//  because the constructor for 'refList_' can't run until the base
//  class's constructor is complete.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == sortList  &&  (isEmpty()  ||
//                           currentItem() == sortList.currentItem()))
//@ End-Method
//=====================================================================

MutableR_Alarm_NUM_ALARM::MutableR_Alarm_NUM_ALARM(
			      const MutableR_Alarm_NUM_ALARM& sortList
							  )
			      : SortR_Alarm(&refList_),
				refList_(sortList.refList_)
{
  CALL_TRACE("MutableR_Alarm_NUM_ALARM(sortList)");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  MutableR_Alarm_NUM_ALARM()  [Default Constructor]
//
//@ Interface-Description
//  Create an empty sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The base class handles the initialization.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

MutableR_Alarm_NUM_ALARM::MutableR_Alarm_NUM_ALARM(void)
				: SortR_Alarm(&refList_),
				  refList_()
{
  CALL_TRACE("MutableR_Alarm_NUM_ALARM()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~MutableR_Alarm_NUM_ALARM()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

MutableR_Alarm_NUM_ALARM::~MutableR_Alarm_NUM_ALARM(void)
{
  CALL_TRACE("~MutableR_Alarm_NUM_ALARM()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSorted()  [const, virtual]
//
//@ Interface-Description
//  This method overrides the base class's pure virtual method, and
//  returns a boolean indicator as to whether this list is currently
//  sorted.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Check the order of each item in the list against the items around
//  them.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
MutableR_Alarm_NUM_ALARM::isSorted(void) const
{
  CALL_TRACE("isSorted()");

  Boolean  sortFlag = TRUE;

  if (!isEmpty())
  {   // $[TI1]
    // this sorted list is not empty, therefore iterate through the nodes
    // and check to see if each is sorted with respect to the next node...
    const DNodeC_MemPtr* const  P_FIRST   = getFirst_();
    const DNodeC_MemPtr*        pCurrNode = P_FIRST;
    const DNodeC_MemPtr*        pNextNode;

    // while the list is sorted and we haven't looped to the first node,
    // check each node against the next node for sorting...
    while (sortFlag  &&  (pNextNode = pCurrNode->getNextPtr()) != P_FIRST)
    {   // $[TI1.1]
      // $[TI1.1.1] (TRUE) $[TI1.1.2] (FALSE)
      sortFlag = (getItem_(pCurrNode) <  getItem_(pNextNode)  ||
		  getItem_(pCurrNode) == getItem_(pNextNode));
      pCurrNode = pNextNode;    // move current to the next node...
    }   // $[TI1.2] -- no iterations (there is only one node in this list)...
  }   // $[TI2] -- this list is currently empty...

  return(sortFlag);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  reSort()
//
//@ Interface-Description
//  Re-sort this list of sortable items.  The current item is not
//  necessarily the same after re-sorting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use a local sorted list to sort all of the items from this list.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isSorted())
//@ End-Method
//=====================================================================

void
MutableR_Alarm_NUM_ALARM::reSort(void)
{
  CALL_TRACE("reSort()");

  if (!isEmpty())
  {   // $[TI1]
    // this list is NOT empty, therefore use a local list ('tmpList') to
    // sort each of the items in this list...
    MutableR_Alarm_NUM_ALARM  tmpList;

    // start from this list's first node...
    refList_.goFirst();

    // while there are more nodes in this list, add each of this list's
    // items to 'tmpList' -- thereby sorting them...
    do
    {
      tmpList.addItem(refList_.currentItem());
    } while (refList_.goNext() == SUCCESS);

    *this = tmpList;  // copy over the newly sorted list...
  }   // $[TI2] -- this list is currently empty...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//                      [static]
//
//@ Interface-Description
//      Report the software fault that occured within the source code
//      of this class.  The fault, indicated by 'softFaultID', occured
//      at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//      none
//---------------------------------------------------------------------
//@ PostConditions
//      none
//@ End-Method
//=====================================================================

void
MutableR_Alarm_NUM_ALARM::SoftFault(const SoftFaultID softFaultID,
				        const Uint32      lineNumber,
				        const char*       pFileName,
				        const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, FIXED_MUTABLE_SORTR,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  print_(ostr)  [const, virtual]
//
// Interface-Description
//  This method overrides the base class's pure virtual method, and
//  dumps out an ASCII representation of the current state of this
//  list to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreConditions
//  none
//---------------------------------------------------------------------
// PostConditions
//  none
// End-Method
//=====================================================================

Ostream&
MutableR_Alarm_NUM_ALARM::print_(Ostream& ostr) const
{
  CALL_TRACE("print_(ostr)");

  ostr << "MutableR_Alarm_NUM_ALARM..." << refList_;

  return(ostr);
}

#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
