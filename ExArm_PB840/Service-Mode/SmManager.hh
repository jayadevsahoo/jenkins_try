#ifndef SmManager_HH
#define SmManager_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: SmManager - Service Mode Manager
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmManager.hhv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007  By: rhj    Date: 04-Apr-2009     SCR Number: 6495
//  Project:  840S2
//  Description:
//      Implemented single test mode.
//
//  Revision: 006  By: dosman    Date: 26-Feb-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release.
//	Added new method to reset the test point count.
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  gdc    Date: 23-Sep-1997    DR Number: 2513
//       Project:  Sigma (R8027)
//       Description:
//           Modified DoTest interface to support test parameters.
//
//  Revision: 003  By:  quf    Date: 05-Aug-1997    DR Number: DCS 2130
//       Project:  Sigma (840)
//       Description:
//            Added inline method isTestRepeat().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad  Date:  08-Nov-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmTestId.hh"
#include "SmDataId.hh"
#include "SmStatusId.hh"
#include "SmPromptId.hh"
#include "MsgQueue.hh"
#include "NetworkMsgId.hh"
#include "BinaryCommand.hh"
#include "SmPackets.hh"

//@ End-Usage

//@ Constant: FUNCTION_REQUEST_MAX
// size of function request buffer
const Int32 FUNCTION_REQUEST_MAX = 5;

extern const Real32 SERVICE_MODE_ALPHA;
extern const Uint16 TEST_SIGNAL_MASK;


class SmManager
{
  public:
    SmManager(void);
    ~SmManager(void);	// not implemented...


	void sendStatusToServiceData(
			SmStatusId::TestStatusId testStatusId,
			SmStatusId::TestConditionId testConditionId);
	void sendTestStartToServiceData( SmTestId::ServiceModeTestId testId);
	void sendTestEndToServiceData( SmTestId::ServiceModeTestId testId);
	void sendOperatorExitToServiceData( void);
	void sendOptionNotInstalledToServiceData( void);

	inline void advanceTestPoint( SmDataId::ItemizedTestDataId nextTestPoint);
	inline void resetTestPoint(void);
	void sendTestPointToServiceData( Real32 testPoint);
	void sendTestPointToServiceData(
		SmDataId::ItemizedTestDataId nextTestPoint, Real32 testPoint);
	void sendMonitorPointToServiceData( Real32 monitorPoint);

	void promptOperator( SmPromptId::PromptId promptId,
							SmPromptId::KeysAllowedId keysAllowedId);

	SmPromptId::ActionId getOperatorAction( void);

	void processRequests( void);

	inline Boolean isTestRepeat(void);
#if defined(SIGMA_BD_CPU)
	//TODO E600_LL: hack for calibration; to be removed
	Boolean isCalibrationInProgress() { return calInProgress; }
#endif

	static void CommsDown( void);
	static inline Boolean CommDied( void);

	static Boolean IsSingleTestMode(void);

	inline SmTestId::ServiceModeTestTypeId getCurrentTestType(void) const;

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL,
		  const char*       pPredicate = NULL);

#if defined(SIGMA_GUI_CPU) || defined(SM_TEST) || defined(SIGMA_UNIT_TEST)
    //=================================================================
    //  GUI-only interface...
    //=================================================================

	static inline void DoTest(SmTestId::ServiceModeTestId testId,
				Boolean testRepeat = FALSE, const char * const pParameters = NULL);

	void doTest(SmTestId::ServiceModeTestId testId,
				Boolean testRepeat = FALSE, const char * const pParameters = NULL);

	static inline void OperatorAction( SmTestId::ServiceModeTestId testId,
						  SmPromptId::ActionId actionId);

	void operatorAction( SmTestId::ServiceModeTestId testId,
						  SmPromptId::ActionId actionId);

	static inline void DoFunction( SmTestId::FunctionId functionId);
	void doFunction( SmTestId::FunctionId functionId);

#endif  // defined(SIGMA_GUI_CPU) || defined(SM_TEST) || defined(SIGMA_UNIT_TEST)

#if defined(SIGMA_BD_CPU)
    //=================================================================
    //  BD-only interface...
    //=================================================================

	inline void signalTestPoint(void);
	void processTestSignal(void);

	static void RecvMsg( XmitDataMsgId msgId, void *pData, Uint32 size);
	static void RecvAction( XmitDataMsgId msgId, void *pData, Uint32 size);

#endif  // defined(SIGMA_BD_CPU)

  protected:

  private:
    SmManager(const SmManager&);		// not implemented...
    void operator=(const SmManager&);	// not implemented...


	void localTestRequest_( SmTestId::ServiceModeTestId testId,
							Boolean testRepeat, 
                            const char * const pParameters);

	void runTest_(SmTestId::ServiceModeTestId testId);


	void localFunctionRequest_( SmTestId::FunctionId functionId);
	void dequeueFunctionRequest_( void);
	void localFunction_( SmTestId::FunctionId functionId);

	void localActionNotification_( SmPromptId::ActionId actionId);

	void shutDownServiceMode_( void);

	void estPassed_( void);

	void setupServiceMode_( void);

	void delayForRemoteTests_( void);

#if defined (SIGMA_GUI_CPU) || defined (SIGMA_UNIT_TEST) || defined (SM_TEST)
    //=================================================================
    //  GUI-only private methods...
    //=================================================================

	void remoteTestRequest_( SmTestId::ServiceModeTestId testId,
								Boolean testRepeat,
                                const char * const pParameters);

	void remoteFunctionRequest_( SmTestId::FunctionId functionId);

	void remoteActionNotification_( SmPromptId::ActionId actionId);

#endif  // defined(SIGMA_GUI_CPU) || defined (SIGMA_UNIT_TEST) || defined (SM_TEST)

    //=================================================================
    //  Common BD/GUI private data...
    //=================================================================

	//@ Data-Member: testPointCount_
	// Stores data ID as an integer
	Int32 testPointCount_;

	// @ Data-Member: setup_
	// flag indicating that setupServiceMode_() was executed
	Boolean setup_;
	
	// @ Data-Member: currentTestId_
	// test ID of currently-executing test
	SmTestId::ServiceModeTestId currentTestId_;

	// @ Data-Member: pendingTestId_
	// test ID of the pending test
	SmTestId::ServiceModeTestId pendingTestId_;

	// @ Data-Member: testRepeat_
	// indicates that the current test is repeat of last test
	Boolean testRepeat_;

	// @ Data-Member: functionRequest_[]
	// buffer for function request IDs
	SmTestId::FunctionId functionRequest_[FUNCTION_REQUEST_MAX];

	// @ Data-Member: functionRequestStart_
	// start-of-buffer pointer for function request buffer
	Int32 functionRequestStart_;

	// @ Data-Member: functionRequestEnd_
	// end-of-buffer pointer for function request buffer
	Int32 functionRequestEnd_;

	// @ Data-Member: TestInProgress_
	// test-in-progress flag
	static Boolean TestInProgress_;

	// @ Data-Member: CommDiedDuringTest_
	// flag indicating that comm went down during a test
	static Boolean CommDiedDuringTest_;

	// @ Data-Member: testType_
	// type of test (EST, SST, External, or Misc)
	SmTestId::ServiceModeTestTypeId testType_;

	// @ Data-Member: pTestParameters_
	// test parameter string - null terminated
	char pTestParameters_[SmMsgPacket::PARAMETER_LENGTH];

	//@ Data-Member: isSingleTestMode_
	// To determine whether we are in single test mode or not
	static Boolean isSingleTestMode_;

#if defined(SIGMA_BD_CPU)
    //=================================================================
    //  BD-only private data...
    //=================================================================

    //@ Data-Member: testSignal_
    // object used to access the test signal bit
// E600 BDIO	BinaryCommand testSignal_;

    //@ Data-Member: testSignalOn_
	// test signal is on flag
	Boolean testSignalOn_;

    //@ Data-Member: testSignalRequested_
	// test signal is requested flag
	Boolean testSignalRequested_;

    //@ Data-Member: signalOnTime_
	// amount of time test signal was on
	Int32 signalOnTime_;

	//TODO E600_LL: hack for calibration; to be removed
	Boolean calInProgress;

#endif  // defined(SIGMA_BD_CPU)

};


// Inlined methods...
#include "SmManager.in"

#endif // SmManager_HH
