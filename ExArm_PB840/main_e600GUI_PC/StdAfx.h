// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once
#ifdef _DEBUG
#define _CRT_SECURE_NO_WARNINGS 1
#endif
#define _CRT_NON_CONFORMING_SWPRINTFS 1
#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0601		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0601	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
//#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#define _WIN32_WINDOWS 0x0601 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

void WINAPIV NKDbgPrintfW(LPCWSTR lpszFmt, ...);


#define RETAILMSG(cond,printf_exp)   \
   ((cond)?(NKDbgPrintfW printf_exp),1:0)

#define DEBUGMSG(cond,printf_exp)   \
   ((void)((cond)?(NKDbgPrintfW printf_exp),1:0))

typedef struct _DBGPARAM {
	WCHAR	lpszName[32];           // @field Name of module
	WCHAR   rglpszZones[16][32];    // @field names of zones for first 16 bits
	ULONG   ulZoneMask;             // @field Current zone Mask
} DBGPARAM, *LPDBGPARAM;

#define DEBUGZONE(n)  (dpCurSettings.ulZoneMask&(0x00000001<<(n)))


BOOL TransparentImage(HDC hdcDest, 
LONG DstX, 
LONG DstY, 
LONG DstCx, 
LONG DstCy,
HDC hSrc, 
LONG SrcX, 
LONG SrcY, 
LONG SrcCx, 
LONG SrcCy, 
COLORREF TransparentColor
);


#define MSGQUEUE_NOPRECOMMIT            0x00000001
#define MSGQUEUE_ALLOW_BROKEN           0x00000002

#define MSGQUEUE_MSGALERT               0x00000001



typedef struct MSGQUEUEOPTIONS_OS {
    DWORD dwSize;                           // size of the structure
    DWORD dwFlags;                          // behavior of message queue
    DWORD dwMaxMessages;                    // max # of msgs in queue
    DWORD cbMaxMessage;                     // max size of msg
    BOOL  bReadAccess;                      // read access requested
} MSGQUEUEOPTIONS, FAR *LPMSGQUEUEOPTIONS, *PMSGQUEUEOPTIONS;

typedef struct MSGQUEUEINFO {
    DWORD dwSize;                           // size of structure
    DWORD dwFlags;                          // behavior of message queue
    DWORD dwMaxMessages;                    // max # of msgs in queue
    DWORD cbMaxMessage;                     // max size of msg
    DWORD dwCurrentMessages;                // # of message in queue currently
    DWORD dwMaxQueueMessages;               // high water mark of queue
    WORD  wNumReaders;                      // # of readers
    WORD  wNumWriters;                      // # of writes
} MSGQUEUEINFO, *PMSGQUEUEINFO, FAR *LPMSGQUEUEINFO;

#ifndef __MSGQUEUE_H__
#define __MSGQUEUE_H__

//#ifdef __cplusplus
//extern "C" {
//#endif /* __cplusplus */
#ifdef SIGMA_GUIPC_CPU
typedef unsigned char   uint8_t;
typedef signed char     int8_t;
typedef unsigned short  uint16_t;
typedef short           int16_t;
typedef unsigned long   uint32_t;
typedef long            int32_t;
typedef unsigned __int64   uint64_t;
typedef __int64            int64_t;
//extern CRITICAL_SECTION			cs;

#endif


HANDLE WINAPI CreateMsgQueue(LPCWSTR lpName, LPMSGQUEUEOPTIONS lpOptions);
HANDLE WINAPI OpenMsgQueue(HANDLE hSrcProc, HANDLE hMsgQ, LPMSGQUEUEOPTIONS lpOptions);
BOOL WINAPI ReadMsgQueue(HANDLE hMsgQ, __out_bcount(cbBufferSize) LPVOID lpBuffer, DWORD cbBufferSize,
                LPDWORD lpNumberOfBytesRead, DWORD dwTimeout, DWORD *pdwFlags);
//BOOL WINAPI ReadMsgQueueEx(HANDLE hMsgQ, __out_bcount(cbBufferSize) LPVOID lpBuffer, DWORD cbBufferSize,
//                LPDWORD lpNumberOfBytesRead, DWORD dwTimeout, DWORD *pdwFlags, PHANDLE phTok);
BOOL WINAPI WriteMsgQueue(HANDLE hMsgQ, LPVOID lpBuffer, DWORD cbDataSize,
                DWORD dwTimeout, DWORD dwFlags);
BOOL WINAPI GetMsgQueueInfo(HANDLE hMsgQ, LPMSGQUEUEINFO lpInfo);

BOOL WINAPI CloseMsgQueue(HANDLE hMsgQ);

//#ifdef __cplusplus
//}
//#endif

#endif // __MSGQUEUE_H__

#ifdef USE_PCH
#include "SigmaTypes.hh"
#include "Sigma.hh"
#include "CallTrace.hh"
#include "TextButton.hh"
#include "Alarm.hh"
#include "TextField.hh"
#include "LargeContainer.hh"
#include "UpperScreen.hh"
#include "SettingValue.hh"
#include "DiscreteSettingButton.hh"
#include "PatientDataMgr.hh"
#include "LowerScreen.hh"
#include "AlarmAnnunciator.hh"
#include "MsgQueue.hh"
#include "FaultHandlerMacros.hh"
#include "NetworkAppClassIds.hh"
#include "GuiAppClassIds.hh"
#include "Box.hh"
#include "OperatingGroup.hh"
#include "NetworkApp.hh"
#include "SettingSubject.hh"
#include "TrendDataMgr.hh"
#include "ViolationHistoryManager.hh"
#include "AlarmConstants.hh"
#include "GuiFoundation.hh"
#include "TrendSubScreen.hh"
#include "BatchSettingValues.hh"
#include "GuiSize.hh"
#include "LowerScreenSelectArea.hh"
#include "AlarmOffKeyPanel.hh"
#include "GuiTestManager.hh"
#include "TemplateMacros.hh"
#include "Setting.hh"
#include "UpperScreenSelectArea.hh"
#include "BatchSettingsSubScreen.hh"
#include "NumericField.hh"
#include "DisplayContext.hh"
#include "OsTimeStamp.hh"
#endif

#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

