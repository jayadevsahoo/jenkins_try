#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SvoTrigger - Monitors the system for lost of gas supply.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers a change to Svo mode based on the
//    availability of air and O2 gas supply. 
//    The trigger can be enabled or disabled, depending upon
//    the current mode of breathing and the applicability of the
//    trigger.  When the conditions monitored by this trigger
//    are true, the trigger is considered to have "fired".  Any scheduler
//    needing this trigger must have it on its ModeTrigger list.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for detecting a 
//    Svo, and triggers the transition to the Svo mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class contains a boolean state variable to keep track of 
//    whether this trigger is enabled or not.  If the trigger is not 
//    enabled, it will always return state of false. The trigger condition 
//    depends on the status of the air and o2 supply.  If the trigger 
//    condition has been met, the trigger fires by notifying the currently 
//    active  scheduler of its state. 
//    This trigger resides on the lists of any BreathPhaseScheduler that
//    requires detection of Svo to be active.  When any of those
//    schedulers are active, the trigger is monitored every BD cycle.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SvoTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 004  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 003  By:  iv    Date:  27-Aug-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added a check for O2 and Air background check results.
//             Air background fault is equivalent to no air supply.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "SvoTrigger.hh"

//@ Usage-Classes
#include "VentObjectRefs.hh"
   
#include "GasSupply.hh"
    
//@ End-Usage
	 
//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SvoTrigger 
//
//@ Interface-Description
//    Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//    The triggerId is set by the base class constructor.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
SvoTrigger::SvoTrigger(void) 
: ModeTrigger(Trigger::SVO)
{
  CALL_TRACE("SvoTrigger( Trigger::TriggerId triggerId )");
  // $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SvoTrigger 
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SvoTrigger::~SvoTrigger(void)
{
  CALL_TRACE("SvoTrigger::~SvoTrigger(void)");
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
SvoTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, SVOTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has 
//    occured, the method returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If both air or o2 gas supply are lost or inoperative then the trigger
//    condition is true else return false.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Boolean
SvoTrigger::triggerCondition_(void)
{

	CALL_TRACE("SvoTrigger::triggerCondition_()");

	Boolean	triggerStatus = FALSE;
#if 0 //LL_E600
	if ( (!RGasSupply.isTotalAirPresent() || RGasSupply.hasAirBkgndFailed())  &&
	     (!RGasSupply.isO2Present() || RGasSupply.hasO2BkgndFailed()) )
	{
		// $[TI1]
		triggerStatus = TRUE;
	}
	// $[TI2]
#endif
	//FALSE: $[TI3]
	//TRUE: $[TI4]
	return (triggerStatus);
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

