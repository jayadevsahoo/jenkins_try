#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BdSettingValues - Manager of the Bd Setting Values.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class manages the values of the Breath-Delivery Settings.  Instances
//  of this class can be used to store all of the BD setting values.  This
//  class provides methods to query and set any of the BD setting
//  values.  It also has four copy methods to do copies between instances
//  of this class:  two 'operator=()' methods which copy ALL values from
//  either an instance of this class or of the 'BatchSettingValues' class,
//  and does NOT generate any callbacks; and two 'copyFrom()' methods which
//  copy only those values that are different between the two, aforementioned,
//  instances and generates a callback for each value copied.
//
//  This class inherits from 'SettingValueMgr' which provides the callback
//  functionality.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a general-purpose class for managing, as a set,
//  BD setting values.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Internally, instances of this class contain arrays of type 'BatchValue'.
//  'BatchValue' is a union of the three different types of BD setting
//  values:  bounded, discrete and sequential values.  This array is big
//  enough to store all of the BD setting values, where the setting IDs are
//  used as indices into this array.
//
//  This class is implemented under the assumption that there are no BD,
//  sequential settings (see 'SettingId.hh').  In development mode, this
//  assumption is tested during construction, and everywhere that this
//  assumption affects the code a comment with "ASSUMPTION #1:" is present.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BdSettingValues.ccv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 003   By: sah   Date:  03-Jan-2000    DR Number: 5327
//  Project:  ATC
//  Description:
//	Updated for NeoMode 
//
//  Revision: 002   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added "AUX" assertions for better debugging
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "BdSettingValues.hh"

//@ Usage-Classes
#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
#  include "BatchSettingValues.hh"
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdSettingValues(bdValues)  [Copy Constructor]
//
//@ Interface-Description
//  Construct a BD setting value's instance, using 'bdValues' to initialize
//  this new instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdSettingValues::BdSettingValues(const BdSettingValues& bdValues)
				 : SettingValueMgr(NULL)
{
  CALL_TRACE("BdSettingValues(bdValues)");

  // copy all of the values from 'bdValues' into this instance...
  copyFrom(bdValues);
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdSettingValues(pInitCallBack)  [Constructor]
//
//@ Interface-Description
//  Construct a BD setting value's instance, using 'pInitCallback'
//  as the pointer to the value-change callback function.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdSettingValues::BdSettingValues(
			    SettingValueMgr::ChangeCallbackPtr pInitCallback
			        ) : SettingValueMgr(pInitCallback)
{
  CALL_TRACE("BdSettingValues(pInitCallback)");

#if defined(SIGMA_DEVELOPMENT)
  // ASSUMPTION #1:  there are no BD, batch sequential settings...
  CLASS_ASSERTION((isAssumption1Valid_()));
#endif // defined(SIGMA_DEVELOPMENT)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdSettingValues()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default BD setting value's instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdSettingValues::BdSettingValues(void)
				 : SettingValueMgr(NULL)
{
  CALL_TRACE("BdSettingValues()");

#if defined(SIGMA_DEVELOPMENT)
  // ASSUMPTION #1:  there are no BD, batch sequential settings...
  CLASS_ASSERTION((isAssumption1Valid_()));
#endif // defined(SIGMA_DEVELOPMENT)
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdSettingValues(void)  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this BD setting values instance.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BdSettingValues::~BdSettingValues(void)
{
  CALL_TRACE("~BdSettingValues()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getBoundedValue(boundedId)  [const]
//
//@ Interface-Description
//  Return the bounded setting value and precision, that is given by
//  'boundedId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundedValue&
BdSettingValues::getBoundedValue(const SettingId::SettingIdType boundedId) const
{
  CALL_TRACE("getBoundedValue(boundedId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBdBoundedId(boundedId)), boundedId);

  return((arrBdValues_[boundedId]).getBoundedValue()); // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getDiscreteValue(discreteId)  [const]
//
//@ Interface-Description
//  Return the discrete setting value that is given by 'discreteId'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiscreteValue
BdSettingValues::getDiscreteValue(const SettingId::SettingIdType discreteId) const
{
  CALL_TRACE("getDiscreteValue(discreteId)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBdDiscreteId(discreteId)), discreteId);

  return((arrBdValues_[discreteId]).getDiscreteValue());   // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setBoundedValue(boundedId, newValue)
//
//@ Interface-Description
//  Set the value of the bounded setting represented by 'boundedId',
//  to 'newValue'.  This will activate this instance's callback, if
//  any, to indicate that the bounded setting identified by 'boundedId'
//  has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdBoundedId(boundedId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BdSettingValues::setBoundedValue(const SettingId::SettingIdType boundedId,
				 const BoundedValue & newValue)
{
  CALL_TRACE("setBoundedValue(boundedId, newValue)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBdBoundedId(boundedId)), boundedId);

  // copy 'newValue' as the new bounded value of the bounded setting
  // identified by 'boundedId'...
  (arrBdValues_[boundedId]).setBoundedValue(newValue);

  // activate this instance's callback, if any, to indicate that a setting's
  // value has changed...
  activateCallback_(boundedId);
} // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  setDiscreteValue(discreteId, newValue)
//
//@ Interface-Description
//  Set the value of the discrete setting represented by 'discreteId',
//  to 'newValue'.  This will activate this instance's callback, if
//  any, to indicate that the discrete setting identified by 'discreteId'
//  has changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (SettingId::IsBdDiscreteId(discreteId))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BdSettingValues::setDiscreteValue(const SettingId::SettingIdType discreteId,
                  const DiscreteValue newValue)
{
  CALL_TRACE("setDiscreteValue(discreteId, newValue)");
  AUX_CLASS_PRE_CONDITION((SettingId::IsBdDiscreteId(discreteId)), discreteId);

  // copy 'newValue' as the new discrete value of the discrete setting
  // identified by 'discreteId'...
  (arrBdValues_[discreteId]).setDiscreteValue(newValue);

  // activate this instance's callback, if any, to indicate that a setting's
  // value has changed...
  activateCallback_(discreteId);
} // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(bdValues)
//
//@ Interface-Description
//  Copy ALL of the BD setting values from 'bdValues' into here.  No
//  callbacks are generated.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BdSettingValues::operator=(const BdSettingValues& bdValues)
{
  CALL_TRACE("operator=(bdValues)");

  if (this != &bdValues)
  {   // $[TI1]
    Uint  idx;

    // for each bounded BD setting...
    for (idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
         idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
    {
      const SettingId::SettingIdType ID = (SettingId::SettingIdType)idx;

      // copy into this instance...
      (arrBdValues_[ID]).setBoundedValue(bdValues.getBoundedValue(ID));
    }

    // for each discrete BD setting...
    for (idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
         idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
    {
      const SettingId::SettingIdType ID = (SettingId::SettingIdType)idx;

      // copy into this instance...
      (arrBdValues_[ID]).setDiscreteValue(bdValues.getDiscreteValue(ID));
    }

    //=================================================================
    // ASSUMPTION #1:  there are no BD, batch sequential settings...
    //=================================================================
  }  // $[TI2] -- assignment to oneself...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  copyFrom(bdValues)
//
//@ Interface-Description
//  Copy all of the CHANGED BD setting values from 'bdValues'
//  into here.  Each value copied into this instance will generate
//  a callback with the registered callback, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BdSettingValues::copyFrom(const BdSettingValues& bdValues)
{
  CALL_TRACE("copyFrom(bdValues)");

  if (this != &bdValues)
  {   // $[TI1]
    Uint  idx;

    // for each bounded BD setting...
    for (idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
    	 idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
    {   // $[TI1.1] -- this path ALWAYS executes...
      const SettingId::SettingIdType  ID = (SettingId::SettingIdType)idx;

      if (getBoundedValue(ID) != bdValues.getBoundedValue(ID))
      {   // $[TI1.1.1]
	// the bounded value from this instance identified by 'ID' is
	// different than the corresponding bounded value from
	// 'bdValues', therefore copy it into this instance...
	setBoundedValue(ID, bdValues.getBoundedValue(ID));
      }   // $[TI1.1.2] -- the two bounded values are equivalent...
    }   // end of for...

    // for each discrete BD setting...
    for (idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
    	 idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
    {   // $[TI1.2] -- this path ALWAYS executes...
      const SettingId::SettingIdType  ID = (SettingId::SettingIdType)idx;

      if (getDiscreteValue(ID) != bdValues.getDiscreteValue(ID))
      {   // $[TI1.2.1]
	// the discrete value from this instance identified by 'ID' is
	// different than the corresponding discrete value from
	// 'bdValues', therefore copy it into this instance...
	setDiscreteValue(ID, bdValues.getDiscreteValue(ID));
      }   // $[TI1.2.2] -- the two discrete values are equivalent...
    }   // end of for...

    //=================================================================
    // ASSUMPTION #1:  there are no BD, batch sequential settings...
    //=================================================================
  }  // $[TI2] -- 'bdValue' IS this instance...
}


#if defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(batchValues)
//
//@ Interface-Description
//  Copy ALL of the BD setting values from 'batchValues' into here.  No
//  callbacks are generated.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BdSettingValues::operator=(const BatchSettingValues& batchValues)
{
  CALL_TRACE("operator=(batchValues)");

  Uint  idx;

  // for each bounded BD setting...
  for (idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
  {
    const SettingId::SettingIdType ID = (SettingId::SettingIdType)idx;

    // copy into this instance...
    (arrBdValues_[ID]).setBoundedValue(batchValues.getBoundedValue(ID));
  }

  // for each discrete BD setting...
  for (idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
  {
    const SettingId::SettingIdType ID = (SettingId::SettingIdType)idx;

    // copy into this instance...
    (arrBdValues_[ID]).setDiscreteValue(batchValues.getDiscreteValue(ID));
  }

  //=================================================================
  // ASSUMPTION #1:  there are no BD, batch sequential settings...
  //=================================================================
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  copyFrom(batchValues)
//
//@ Interface-Description
//  Copy all of the CHANGED BD setting values from 'batchValues'
//  into here.  Each value copied into this instance will generate
//  a callback with the registered callback, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BdSettingValues::copyFrom(const BatchSettingValues& batchValues)
{
  CALL_TRACE("copyFrom(batchValues)");

  Uint  idx;

  // for each bounded batch setting...
  for (idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
  {   // $[TI1.1] -- this path ALWAYS executes...
    const SettingId::SettingIdType  ID = (SettingId::SettingIdType)idx;

    if (getBoundedValue(ID) != batchValues.getBoundedValue(ID))
    {   // $[TI1.1.1]
      // the bounded value from this instance identified by 'ID' is
      // different than the corresponding bounded value from
      // 'batchValues', therefore copy it into this instance...
      setBoundedValue(ID, batchValues.getBoundedValue(ID));
    }   // $[TI1.1.2] -- the two bounded values are equivalent...
  }   // end of for...

  // for each discrete BATCH setting...
  for (idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
       idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
  {   // $[TI1.2] -- this path ALWAYS executes...
    const SettingId::SettingIdType  ID = (SettingId::SettingIdType)idx;

    if (getDiscreteValue(ID) != batchValues.getDiscreteValue(ID))
    {   // $[TI1.2.1]
      // the discrete value from this instance identified by 'ID' is
      // different than the corresponding discrete value from
      // 'batchValues', therefore copy it into this instance...
      setDiscreteValue(ID, batchValues.getDiscreteValue(ID));
    }   // $[TI1.2.2] -- the two discrete values are equivalent...
  }   // end of for...

  //=================================================================
  // ASSUMPTION #1:  there are no BD, batch sequential settings...
  //=================================================================
}

#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator==(bdValues)   [const]
//
//@ Interface-Description
//  Are all of the BD values of this instance equivalent to the values of
//  'bdValues'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
BdSettingValues::operator==(const BdSettingValues& bdValues) const
{
  CALL_TRACE("operator==(bdValues)");

  Boolean  isEquiv = TRUE;

  Uint  idx;

  // for each bounded BD setting...
  for (idx  = SettingId::LOW_BATCH_BD_BOUNDED_ID_VALUE;
       isEquiv  &&  idx <= SettingId::HIGH_BATCH_BD_BOUNDED_ID_VALUE; idx++)
  {
    isEquiv = (arrBdValues_[idx].getBoundedValue() ==
			      bdValues.arrBdValues_[idx].getBoundedValue());
  }

  // for each discrete BD setting...
  for (idx  = SettingId::LOW_BATCH_BD_DISCRETE_ID_VALUE;
       isEquiv  &&  idx <= SettingId::HIGH_BATCH_BD_DISCRETE_ID_VALUE; idx++)
  {
    isEquiv = (arrBdValues_[idx].getDiscreteValue() ==
			    bdValues.arrBdValues_[idx].getDiscreteValue());
  }

  //=================================================================
  // ASSUMPTION #1:  there are no BD, batch sequential settings...
  //=================================================================

  return(isEquiv);
}   // $[TI1] (TRUE)  $[TI2] (FALSE)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	    [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
BdSettingValues::SoftFault(const SoftFaultID  softFaultID,
			      const Uint32       lineNumber,
			      const char*        pFileName,
			      const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  BD_SETTING_VALUES, lineNumber, pFileName,
			  pPredicate);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  isAssumption1Valid_()   [const]
//
// Interface-Description
//  Is the basis for ASSUMPTION #1 valid?
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
BdSettingValues::isAssumption1Valid_(void) const
{
  CALL_TRACE("isAssumption1Valid_()");

  Boolean  isValid = TRUE;

  Uint  idx;

  // for each bounded BD setting...
  for (idx  = SettingId::LOW_BATCH_BD_ID_VALUE;
       isValid  &&  idx <= SettingId::HIGH_BATCH_BD_ID_VALUE; idx++)
  {
    //=================================================================
    // ASSUMPTION #1:  there are no BD, batch sequential settings...
    //=================================================================
    isValid = !SettingId::IsBdSequentialId((SettingId::SettingIdType)idx);
  }

  return(isValid);
}

#endif // defined(SIGMA_DEVELOPMENT)
