#include "stdafx.h"
#if	defined(SIGMA_CLIENT)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Nellcor/Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BufQueue - Manages a pool of network data buffer so to maximize
//      utilization of the buffer space among multiple tasks. The class
//		provides critical section  protection for multiple task 
//		access.  When the class runs out of buffer space, it will delay
//		the calling task up to 500 milli-seconds to give enough time
//	    other tasks to free up some of the network buffers.
//---------------------------------------------------------------------
//@ Interface-Description
//		The class provides several static methods to allocate a network
//    	buffer and free a network buffer.
//      The BufQueue class internally manages a list of free buffers and
//      a list of buffers that contains download data.
//---------------------------------------------------------------------
//@ Rationale
//		Encapsulate network buffer management code and data within a class.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Maintains two singly linked list, one for free buffer list and 
//      the other for download data block list. There are two hundred
//      4K bytes size network buffer shared among these two buffer list.
//      When the flash memory burning rate is slower than the network
//      data transmission rate, there will be more data block queued
//      than the free buffers can hold. 
//---------------------------------------------------------------------
//@ Fault-Handling
//  A standard pre-condition test is used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/Download/vcssrc/BufQueue.ccv   25.0.4.0   19 Nov 2013 14:02:46   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <jhv>    Date:  08-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BufQueue.hh"
#include "Download.hh"

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BufQueue(void)  [Default Constructor]
//
//@ Interface-Description
//   Initializes the all the network buffers as a singly linked
//	 list and assign the list to the free list.
//---------------------------------------------------------------------
//@ Implementation-Description
//		call the reInit method.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
BufQueue::BufQueue(void)
{
	// $[TI1]
	reInit();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reInit(void)
//
//@ Interface-Description
//   Initializes the all the network buffers as a singly linked
//	 list and assign the list to the free list.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 	The data block list is initially empty by assigning NULL to
//		the tail and head pointer of the data block list.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
BufQueue::reInit(void)
{
Int j;

	// $[TI1]
	for(j=0; j < ( NETWORK_BUFFER_ENTRIES -1); j++)
	{
		buffers_[j].pNext = &buffers_[j+1]; 
	} // no [TI] needed
	buffers_[j].pNext = NULL;
	freeList_ = &buffers_[0];
	pTail_ = pHead_ = NULL;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFreeBuf(Uint waitTime) 
//
//@ Interface-Description
//   Gets a free network buffer from the free buffer list and
//	 returns a pointer to the network buffer.
//	 If there is no free buffer, wait for up to 500 milli seconds
//	 until a network buffer is available. If a free buffer cannot
//   be allocated even after the delay, a NULL pointer is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Locks the semaphore before it accesses the buffer free list.
//   If a network buffer is not right away available, it goes on a loop
//   checking the free list every BUFFER_WAIT_INTERVAL milliseconds up to 
//   total waitTime millseconds.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
NetworkBuf *
BufQueue::getFreeBuf(Uint waitTime)
{
NetworkBuf *pBuf;
Int leftTime = waitTime;

	CLASS_ASSERTION(freeLock_.lock()==SUCCESS);
	while( !freeList_ ) // $[TI1]
	{
		freeLock_.release();
		if ( leftTime <=  0  ) // $[TI1.1]
		{
			return(NULL);
		} // else $[TI1.2]
		leftTime -= BUFFER_WAIT_INTERVAL ;
		Download::DelayTask( BUFFER_WAIT_INTERVAL);
		freeLock_.lock();
	} // $[TI2]
	pBuf = freeList_;
	freeList_ = pBuf->pNext;
	pBuf->pNext = NULL;
	freeLock_.release();
	return(pBuf);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: freeBuf(NetworkBuf *pBuf) 
//
//@ Interface-Description
//   Frees a given network buffer back to the buffer free list.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 Locks the semaphore before it accesses the buffer free list.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BufQueue::freeBuf(NetworkBuf *pBuf)
{
	// $[TI1]
	CLASS_PRE_CONDITION(pBuf);

	CLASS_ASSERTION(freeLock_.lock() == SUCCESS);

	pBuf->pNext = freeList_;
	freeList_ = pBuf;
	freeLock_.release();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getDataBlock(void) 
//
//@ Interface-Description
//   Get next data Block from the data block list.
//	 If there is no data block ready on the data block list, return
//	 a NULL pointer.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The data block list is accessed by multiple tasks, therefore
//   needs a semaphore protection before it is accessed.
//   A data block is accessed in a FIFO order, pHead_ points to the
//   first in data block and pTail_ points to the last in the data block.
//	 Get the data block always from the head of the list.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
NetworkBuf *
BufQueue::getDataBlock(void)
{
NetworkBuf *pBuf;

	dataLock_.lock();
	if (!pHead_ )  // $[TI1]
	{
		dataLock_.release();
		return(NULL);
	} // else $[TI2]
	pBuf = pHead_;
	if ( pHead_ == pTail_ ) // $[TI3]
	{
		pTail_ = NULL;
		pHead_ = NULL;
	}
	else // $[TI4]
	{
		pHead_ = pHead_->pNext;
		pHead_->pPrev = NULL;
	}
	dataLock_.release();
	return(pBuf);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addDataBlock(NetworkBuf *pBuf) 
//
//@ Interface-Description
//   Add a given data block to the data block list maintaining
//	 the order that a data block is added.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  The data block list is accessed by the  FlashBurner task and
//	  the TftpSocket task. The TftpSocket task adds data blocks to the
//    list. The FlashBurner task releases each data block after the
//    contents of the block is burnt to the Flash Memory.
//	  The dataLock_ semaphore is used for a critical section.
//	  The new data block is added to the tail end of the data block list.
//---------------------------------------------------------------------
//@ PreCondition
//		CLASS_PRE_CONDITION(pBuf)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BufQueue::addDataBlock(NetworkBuf *pBuf)
{
CLASS_PRE_CONDITION(pBuf);

	dataLock_.lock();
	pBuf->pPrev = pTail_;
	pBuf->pNext = NULL;
	if ( pTail_) // $[TI1]
        {
		pTail_->pNext = pBuf;
        }
	// else $[TI2]
	pTail_ = pBuf;

	if ( pHead_ == NULL  ) // $[TI3]
        {
		pHead_ = pTail_;
        }
	// else $[TI4]
	dataLock_.release();
}

#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getFreeList(void)
//
//@ Interface-Description
//   Return the current freeList_ pointer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
NetworkBuf *
BufQueue::getFreeList(void)
{
	return(freeList_);
}
#endif // SIGMA_UNIT_TEST


#ifdef SIGMA_UNIT_TEST
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setFreeList(NetworkBuf *pBuf)
//
//@ Interface-Description
//   Set the freeList_ pointer to value specified.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BufQueue::setFreeList(NetworkBuf *pBuf)
{
	freeList_ = pBuf;
}
#endif // SIGMA_UNIT_TEST


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BufQueue::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
           const char*        pFileName,
           const char*        pPredicate)
{
      DownloadFault::SoftFault(softFaultID, 
          DOWNLOAD, BUF_QUEUE_CLASS, lineNumber, pFileName, pPredicate);
}

#endif //defined(SIGMA_CLIENT)
