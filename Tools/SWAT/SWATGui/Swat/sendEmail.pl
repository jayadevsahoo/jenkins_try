# perl SendEmail.pl "Some small string for mail-body" "rohit.vij" "My Subject Line"
# perl SendEmail.pl "FILE:./tmp_email.html" "rohit.vij" "My Subject Line2"

# If you need to send an HTML email, then save your contents in some file e.g. ./tmp_email.html
# Then just add these 3 lines & a newline at the top of that mail-body-file
# Mime-Version: 1.0;
# Content-Type: text/html; charset="ISO-8859-1";
# Content-Transfer-Encoding: 7bit;
#



use strict;
use warnings;

use Getopt::Long;

use Data::Dumper;
use Cwd;

#####################

# C O N S T A N T S

my $EMAIL_FROM = "SWAT\@covidien.com";
my $EMAIL_SUBJECTPREFIX = '[SWAT] ';
my $MAIL_SERVER = "mailmkg.thcg.net";
my $EMAIL_SUFFIX = "\@covidien.com";

#####################


my $CYGWIN = ($^O =~ /cygwin/i) ? (1) : (0);
my $WIN64 = 0;

# Must be set as early as possible !!!
if($CYGWIN) {
	$ENV{'PATH'} = "/cygdrive/c/cygwin/bin:" . $ENV{'PATH'} ;
} else {
	$ENV{'PATH'} = "C:\\cygwin\\bin;" . $ENV{'PATH'} ;
}

if(`uname -a` =~ /WOW64/i) {
	$WIN64 = 1;
}

my $ThisProgDir = $0; $ThisProgDir =~ s#\\#/#g; $ThisProgDir =~ s#^(.*)/.*?$#$1#;

my $RedirectStdoutToNull = ($CYGWIN)?("1>/dev/null"):("1>nul");
my $RedirectStdoutStdErrToNull = ($CYGWIN)?("1>/dev/null 2>&1"):("1>nul 2>&1");


my $username = $ENV{USERNAME};

my $CurrDir = cwd();

#####################


main(@ARGV);

exit 0;

######################

sub main {

	my ($Mailbody_FileOrStr) = shift;
	die "\nFirst parameter: Mailbody_FileOrStr missing !!!\n" unless defined($Mailbody_FileOrStr);
	my ($CommaSepList_To) = shift;
	die "\nSecond parameter: CommaSepList_To missing !!!\n" unless defined($CommaSepList_To);
	my ($Subject) = shift;
	die "\nThird parameter: Subject missing !!!\n" unless defined($Subject);
	
	my @arr = grep(s#\s*##g, split(/\s*,\s*/,$CommaSepList_To));
	SendEmail($Mailbody_FileOrStr, \@arr, $Subject);
}


######################

sub SendEmail # Will not work if McAfee Virus Access Scan is enabled
{
	my ( $mailBody, $ArrRef_ToList, $subject ) = @_;
	
	use Net::SMTP;
	
	my $MAIL_DEBUG = 0;
	
	my $str_ToList = "";
	
	if($MAIL_DEBUG) {
		$str_ToList = "<$username\@covidien.com>";
		#$subject .= " [".Timestamp()."]";
	}
	else {
		foreach my $email (@$ArrRef_ToList) {
			next if(!defined($email));
			next if($email =~ m#\s#);
			if($email !~ m#\@#) {
				$email .= "\@covidien.com";
			}
			$str_ToList .= ", <$email>" unless($str_ToList =~ /<$email>/);
		}
		$str_ToList =~ s#^\s*,\s*##;
	}
	
	return if($str_ToList =~ m#^\s*$#);
	print "Sending email to recipients: [$str_ToList]\n";
	
	DisableVirusChecks();
	
	# Set up the mail container
	my $smtp = Net::SMTP->new( Host => $MAIL_SERVER , Port => 25, Debug => 1 , Hello => 'covidien.com' );
	#my $smtp = Net::SMTP->new( $MAIL_SERVER );
	
	unless ($smtp) {
		print "Cannot create connection to mail server " . $MAIL_SERVER . " !!!\n<$@>\n";
		return;
	}
	
    # Email is from who?
    $smtp->mail( $EMAIL_FROM );
	my $str_ToList2 = $str_ToList; $str_ToList2 =~ s#[<> ]##g;
	my @arr2 = split(/,/,$str_ToList2);
	$smtp->to( @arr2 );
    # create the body of the email
    $smtp->data();
	$smtp->datasend( "To: $str_ToList2\n" );
	$smtp->datasend( "Subject: $EMAIL_SUBJECTPREFIX" . $subject . "\n" );
	
	if( ($mailBody=~/^FILE:(.+)$/) && (-f "$1") ) { # in case we have saved the mail-body in an actual file
		$mailBody = $1;
		open my $MAIL_BODY, "<", $mailBody or die "Cannot open file $mailBody: $!\n";
		foreach my $line ( <$MAIL_BODY> )
		{
		  $smtp->datasend( $line );
		}
		close $MAIL_BODY;
	}
	else {
		$smtp->datasend( $mailBody );
	}
	
    # complete the data portion
    $smtp->dataend();

    # send the email
    $smtp->quit();

}


###############################################################################

sub DisableVirusChecks {

	if($CYGWIN) {
		if($WIN64) {
			system(q{"/cygdrive/c/Program Files (x86)/McAfee/VirusScan Enterprise/mcadmin.exe" /disableoas});
		} else {
			system(q{"/cygdrive/c/Program Files/McAfee/VirusScan Enterprise/mcadmin.exe" /disableoas});
		}
	}
	else {
		system(q{"C:\\Program Files (x86)\\McAfee\\VirusScan Enterprise\\mcadmin.exe" /disableoas});
	}
	
	system "net stop McShield";
}

