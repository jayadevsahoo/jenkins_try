#ifndef DeliveredFlowExpTrigger_HH
#define DeliveredFlowExpTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: DeliveredFlowExpTrigger - Triggers exhalation based on  
//	End-Inspiratory flow method
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/DeliveredFlowExpTrigger.hhv   25.0.4.0   19 Nov 2013 13:59:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 013  By: rhj     Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 012  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 011  By:  iv    Date:  23-Oct-1998    DR Number: 5229
//       Project:  BiLevel
//       Description:
//          Added a pressureTrajectory_ data member.
//
//  Revision: 010  By:  iv    Date:  18-Jul-1997    DR Number: 2299
//       Project:  Sigma (R8027)
//       Description:
//             Added a data member peakNetFlow_
//
//  Revision: 009  By:  sp    Date:  2-Jan-1997    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection Rework.
//
//  Revision: 008  By:  sp    Date:  25-Sept-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 007  By:  sp    Date:  31-May-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test methods getDesiredFlow, getReliefFlowTarget,
//	       getExhalationSensitivity and getEffectivePressure.
//
//  Revision: 006  By:  sp    Date:  26-Apr-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Add new method setExhalationSensitivity and new data member
//             exhalationSensitivity_.
//
//  Revision: 005  By:  sp    Date:  26-Apr-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Change method GetPeakDesiredFlow to non-static.
//
//  Revision: 004  By:  iv    Date:  25-Apr-1996    DR Number:DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added access method for peakDesiredFlow_
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number:DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  sp    Date: 1-Feb-1996    DR Number:DCS 669
//       Project:  Sigma (R8027)
//       Description:
//             add method setReliefFlowTarget and data member reliefFlowTarget_.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"
#  include "TriggersRefs.hh"

//@ Usage-Classes
#  include "BreathTrigger.hh"
#  include "MathUtilities.hh"
//@ End-Usage

class DeliveredFlowExpTrigger : public BreathTrigger{
  public:
    DeliveredFlowExpTrigger(void);
    virtual ~DeliveredFlowExpTrigger(void);

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
  
    inline void setEffectivePressure(const Real32 effectivePressure);
    void setDesiredFlow(const Real32 desiredFlow);
    inline void setReliefFlowTarget(const Real32 reliefFlowTarget);
    inline void setPressureTrajectory(const Real32 pressureTrajectory);
    virtual void enable(void);

  protected:
    virtual Boolean triggerCondition_(void);

  private:
    DeliveredFlowExpTrigger(const DeliveredFlowExpTrigger&);		// not implemented...
    void   operator=(const DeliveredFlowExpTrigger&);	// not implemented...

    //@ Data member: peakDesiredFlow_
    // peak desired flow
    Real32 peakDesiredFlow_;

    //@ Data member: peakNetFlow_
    // peak net flow
    Real32 peakNetFlow_;

    //@ Data member: desiredFlow_
    // set by flow controller
    Real32 desiredFlow_;

    //@ Data member: reliefFlowTarget_
    // relief flow target 
    Real32 reliefFlowTarget_;

    //@ Data member: effectivePressure_
    // set by pressure based phase
    // target pressure not including peep
    Real32 effectivePressure_;

    //@ Data member: pressureTrajectory_
    // set by pressure based phase
    // pressure trajectory to be followed by the controller
    Real32 pressureTrajectory_;

};


// Inlined methods...
#include "DeliveredFlowExpTrigger.in"


#endif // DeliveredFlowExpTrigger_HH 
