/*****************************************************************************/
/*   Module Name:    75667.c                                                 */
/*   Purpose:        This module contains the code which performs the        */
/*                   software download for the 840 Vent.                     */
/*                                                                           */
/*   Requirements:   This module implements requirements detailed in         */
/*                   75677.93                                                */
/*                                                                           */
/*   $Revision::   1.1      $                                                */
/*                                                                           */
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.

Copyright (c) 1998-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/
 
/****************/
/*** Includes ***/
/****************/
#include <windows.h>
#include <ansi_c.h>
#ifdef DEMOMODE
#include <formatio.h>
#endif
#include <stdio.h>
#include <tcpsupp.h> // GetTCPHostAddr (IP, sizeof(IP))
#include <toolbox.h> // SetBOLE(), MAX_FILE_EXTENSION_LENGTH, FindFileExtension(FileName)
#include <userint.h>
#include <utility.h>
#include <winsock2.h>

#include "globals.h"
#include "72361.h" // AppErr
#include "840.h" // Establish840(int noAutoDetect), Is840Connected(void)
#include "840Cmds.h" // VENTDATA Vent
//#include "840Msgs.h"
#include "cpvtmain.h" // gCurrOperator, gPanelHandle[PANELS]
#include "detfun.h"
//#include "dload.h"
#include "ErrCodes.h"
#include "helpinit.h" // ShowHelp
#include "ldb.h"
#include "mainpnls.h"
#include "MsgPopup.h"
#include "Pb_util.h"
#include "registry.h" // GetCSEEnabled()
#include "RT_Bmps.h"
#include "softdapi.h"
#include "softdlic.h"
#include "softdload.h"
#include "sysmenu.h" // SetSysPanelMessage
#include "testing.h"
       
/***************/
/*** Defines ***/
/***************/
/* Watch dogs */
#define TIMEOUT                       60                    
#define START_TIME_OUT                60                    
#define TIMEOUT_ERROR                 -6
//#define GUI_TIMEOUT                   -5
//#define BD_TIMEOUT                    -4
#define USER_BREAK                    -3

/* Config */
#define MAX_CHAR_STRING               20   
#define AbsConfigPath                 "\\softd\\abs\\"
#define AbsConfigName                 "Abs.cfg"
#define CONFIG_ERROR                  -8  
#define MAX_IMAGES                    10
#define MAX_IMAGE_BUFFER              20
#define MAX_ADAPTERS                   4

/*Bootp Server */
#define BOOTP_PROBLEM                -2  
#define BOOTP_COMPLETE                1
#define BOOTP_PROCESSING              0

#define LINEFEED        0x0A
#define CARRIAGE_RETURN 0x0D

/************************/
/*** Global Variables ***/
/************************/
static short SelfTestDone = FALSE;       /* TRUE when self test completed  */
char GuiFileName[MAX_FILENAME_LEN];      /* GUI Image File Name */
char BdFileName[MAX_FILENAME_LEN];       /* BDU Image File Name */
char IP[20];                             /* Address of Ethernet Card */
char szIPaddr[4][256];
int nNumAdapters = 0;                    /* qty adapters found */
int MultiHomed;                          /* 2 or more Ethernet Cards */

char AbsFilePath[MAX_PATHNAME_LEN];      /* Image Files Path */
char AbsConfigFile[MAX_PATHNAME_LEN];    /* path and file name of abs file */

int IMAG_PANEL = _840PANEL +1;
int imagePanel;
int imagePanelDone;
int imagePanelStatus = FAILURE;
static int LookingFor840 = 0;

int BootPPanel;                          /* BootP status panel Handle */

int TftpPanel;                           /* Tftp status panel Handle */
int GuiDone;                             /* Gui Completion indicator */
int BdDone;                              /* Bd Completion indicator */
int BdWatchDog;                          /* Bd Timeout monitor between tftp blocks*/
int GuiWatchDog;                         /* Gui Timeout monitor between tftp blocks*/ 
int BdFound;                             /* When a Bd request service */
int GuiFound;                            /* When a Gui request service */
static int Ready = 0;                    /* BOOTP & Cvi ready */
int nRunMode = 0;
int ServerStarted;                       /* Server active */
static int ServerStat;                   /* server status */
static int BduStat;                      /* server status */
static int GuiStat;                      /* server status */
#ifdef DEMOMODE
int DownloadTimer;
#endif

/******************************/
/*** Structures Definitions ***/
/******************************/

/* To parse ABS config file */
typedef struct imageList{
    char partNumber[MAX_IMAGE_BUFFER];
    char rev[MAX_IMAGE_BUFFER];
    char bdFileName[MAX_IMAGE_BUFFER];
    char guiFileName[MAX_IMAGE_BUFFER];
    char desc[MAX_IMAGE_BUFFER];
}ImageList;

static ImageList tempImage[MAX_IMAGES];

/*****************************/
/*** Function Declarations ***/
/*****************************/ 

extern int CVIFUNC_C GetUserBitmapFromText (char *imageText, int version, int *bitmapId); // needed if there are bitmaps

/* Setup/Cleanup Functions */
int StartDload(void);
int StopDload(void);

/* Support Functions */

/* Test Executive Support Functions */
int SelfTest(void);  

/* BootP Support Functions */
int  StartServer(void);
void bootPCall(int incomingMsg);
void tftpCall(int id, int currentblock, int totalblock);

/* Image File Support Functions */
int GetImageFile(void);
int SetImageFile(char *bd, char *gui);

/* Vent Serial Communcations Support Functions */
int SelectState(void);   

int BuildBOOT_PANEL_DemoTimer(int hParentPanel);
int CVICALLBACK BootpDemoTimeout(int panel, int control, int event, 
                                 void *callbackData, int eventData1, int eventData2);

char *UDPErrorString(int error);


/*****************************************************************************/
/* Name:    ShowSetupInstructions                                            */
/*                                                                           */
/* Purpose: Display Instructions for Cabling and Ventilator Startup          */
/*                                                                           */
/* Example Call:    status = ShowSetupInstructions()                         */
/*                                                                           */
/* Input:  <void>                                                            */
/*                                                                           */
/* Output: Modal dialog with instructional text, buttons for OK and Cancel   */
/*                                                                           */
/* Globals Used:    <None>                                                   */
/*                                                                           */
/* Globals Changed: <None>                                                   */
/*                                                                           */
/* Return: int status SUCCESS = Instructions acknowledged, continue          */
/*                    FAILURE = Cancel                                       */
/*                                                                           */
/*****************************************************************************/
static int ShowSetupInstructions(void)
{
	int insPanel;
	int handle;
	int control;
	int panelDone;
	int status;

	/* Display Instructions for Cabling and Ventilator Startup */
	if( (insPanel = BuildINS_PANEL(0)) < 0 ){
		CPVTMessagePopup(LDBf(Ldb_Error), GetUILErrorString(insPanel), 
		                 OK_ONLY_DLG, NO_HELP);
		status = FAILURE;
	}
	else{
		panelDone = FALSE;
		InstallPopup(insPanel);
		while(panelDone == FALSE){
			GetUserEvent(0, &handle, &control);
			if(handle != insPanel){
				continue;
			}
			if(control == INS_PANEL_INS_DONE){
				status = SUCCESS;
				panelDone = TRUE;
			}
			if(control == INS_PANEL_INS_CANCEL){
				status = FAILURE;
				panelDone = TRUE;
			}
		}
		DiscardPanel(insPanel);
	}

	return(status);
}

/*****************************************************************************/
/* Name:    StartDload                                                       */
/*                                                                           */
/* Purpose: This function performs the sequence startup functions            */
/*                                                                           */
/* Example Call:   This function is called by the Test Executive only.       */
/*                                                                           */
/* Input:   Refer to Prototype for all Setup/Cleanup Test Functions          */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  FALSE if NO error(s)/failure(s) detected                         */
/*****************************************************************************/
int StartDload(void)
{
	#ifdef SHOW_INSPANEL
	int insPanel;
	int handle;
	int control;
	int panelDone;
	#endif
	int errFlag = 0;
	int connect;

	/******* initialize globals ********/
	while(TRUE){
		GuiDone       = 0;
		BdDone        = 0;
		BdWatchDog    = 0;
		GuiWatchDog   = 0;
		BdFound       = 0;
		GuiFound      = 0;
		Ready         = 0;
		ServerStarted = 0;

		if(SelfTestDone != TRUE){
			errFlag = SelfTest();
			if(errFlag){
				return(-1);
			}
		}  

		if(GetImageFile() != SUCCESS){
			SelfTestDone = FALSE;
			return(-1); 
		} 

		/* 1 = Software Replacement, 0 = Initial Download */
		nRunMode = SelectState();
		if(nRunMode >= 0){

			if(nRunMode){ // > 0
				// Software Replacement, first erase current firmware
				#ifdef SHOW_INSPANEL
				/* Display Instructions to place vent into External Test Control*/
				if( (insPanel = BuildINS_PANEL(0)) < 0 ){
					CPVTMessagePopup(LDBf(Ldb_Error), GetUILErrorString(insPanel), 
					                 OK_ONLY_DLG, NO_HELP);
					return(-1);
				}

				panelDone = FALSE;
				InstallPopup(insPanel);
				while(panelDone == FALSE){
					GetUserEvent(0, &handle, &control);
					if(handle != insPanel){
						continue;
					}

					if(control == INS_PANEL_INS_DONE){
						panelDone = TRUE;
					}
					if(control == INS_PANEL_INS_CANCEL){
						panelDone = TRUE;
						DiscardPanel(insPanel);
						return(-1);
					}
				}
				DiscardPanel(insPanel);
				#endif //SHOW_INSPANEL
     
				Register_Sys_CB();
				connect = Establish840(AUTO_DETECT);
				if(connect != SUCCESS){
					gErrorFlag = TRUE;
					ErrorMessage(COM_840_ERROR);
					return(-1); 
				}
				else{
					if(nRunMode == 1){
						if(VentDoFunction(BD_DOWNLOAD_ID) != SUCCESS){
							// "Can Not Erase BD Image File in Ventilator"
							CPVTMessagePopup(LDBf(Ldb_Com_Error), LDBf(Ldb_Dload_CannotEraseBDU), 
							                 OK_ONLY_DLG, NO_HELP);
							return(-1);
						}
					}
					if(nRunMode == 1){
						if(VentDoFunction(GUI_DOWNLOAD_ID) != SUCCESS){
							// "Can Not Erase GUI Image File in Ventilator"
							CPVTMessagePopup(LDBf(Ldb_Com_Error), LDBf(Ldb_Dload_CannotEraseGUI), 
							                 OK_ONLY_DLG, NO_HELP);
							return(-1);
						}
					}
				} /* end else */
			} // end if(nRunMode)
		} /* end Select State */
		else{ // SelectState error
			return(-1);
		}

		CPVTMessagePopup(LDBf(Ldb_InsPanelTitle), LDBf(Ldb_840_cycle_power), 
		                 OK_ONLY_DLG, NO_HELP);

		StartServer();
	} // end while(TRUE)
}

/*****************************************************************************/
/* Name:    SelectState                                                      */
/*                                                                           */
/* Purpose: This function allow the user set if Vent Software is Initial or  */
/*          replacement(serial connection required)                          */
/*                                                                           */
/* Example Call:   TypeOfConnection = SelectState(data)                      */
/*                                                                           */
/* Input: tTestData *data                                                    */
/* Output:  <int>                                                            */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  /* 1 = Software Replacement, 0 = Initial Download                */
/* error(s)/failure(s) detected process through AppError                     */
/*****************************************************************************/
int SelectState(void)
{
	int done;
	int control;
	int handle;
	int statePanel;
	int nWasDloadType;

	/* Retrieve the state panel */
	if( (statePanel = BuildSTATEPANEL(0)) < SUCCESS ){
		CPVTMessagePopup(LDBf(Ldb_Error), GetUILErrorString(statePanel), OK_ONLY_DLG, NO_HELP);
		return(statePanel);
	}

	done = 0;
	nWasDloadType = DloadType;
	if(DloadType == 0){
		SetCtrlVal(statePanel, STATEPANEL_INITIAL_DLOAD,      TRUE);
		SetCtrlVal(statePanel, STATEPANEL_REPLACE_BOTH_DLOAD, FALSE);
	}
	else if(DloadType == 1){
		SetCtrlVal(statePanel, STATEPANEL_INITIAL_DLOAD,      FALSE);
		SetCtrlVal(statePanel, STATEPANEL_REPLACE_BOTH_DLOAD, TRUE);
	}
	else{
		return(-1);
	}
	InstallPopup(statePanel);
	// DloadType set by DnloadStateCB in softdload.c
	while(!done){
		GetUserEvent(0, &handle, &control);
		if(handle != statePanel){
			continue;
		}

		if(control == STATEPANEL_STATE_DONE){
			done = 1;
			continue;
		}
		if(control == STATEPANEL_STATE_CANCEL){
			DloadType = nWasDloadType;
			done = 1;
			DiscardPanel(statePanel);
			return(-1);
		}
	}
	DiscardPanel(statePanel);

	return(DloadType);
}
    
/*****************************************************************************/
/* Name:    StopSeq                                                          */
/*                                                                           */
/* Purpose: This function performs the sequence stop functions               */
/*                                                                           */
/* Example Call:   This function is called by the Test Executive only.       */
/*                                                                           */
/* Input:   Refer to Prototype for all Setup/Cleanup Test Functions          */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  FALSE if NO error(s)/failure(s) detected                         */
/*          TRUE  if any error(s) / failure(s) are detected                  */
/*****************************************************************************/
int StopDload(void) 
{
	if(ServerStarted){
		EndSession(); // stop the dll from processing
	}

	ServerStarted = 0;

	return TRUE;
}

/*****************************************************************************/
/* Name:    StartServer                                                      */
/*                                                                           */
/* Purpose: This function starts the server.This function call by Test Exec  */
/*          also called with secret login                                    */
/*                                                                           */
/* Input:   See Test step definition above                                   */
/*                                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*         ListBuff                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           ServerStat,BootPPanel,TftpPanel,sAbortFlag                      */
/*                                                                           */
/* Return:  <None>                                                           */
/*****************************************************************************/
int StartServer(void)
{
	BootInfo request;
	int results = 0;
	int err;

	char altaddr[20];
	int serFlag = 1;
	int userAbort = FALSE;

	int FrameThickness;
	int TitleThickness;
	int CtrlTop, CtrlHeight;

	char cMsgBuf[256];

	/************** init test flags ****************/
	ServerStat = 0;

	request.guiFileName = GuiFileName;
	request.guiId = GUI_DOWNLOAD_ID;

	request.bdFileName = BdFileName;
	request.bdId = BD_DOWNLOAD_ID;

	request.filePath = AbsFilePath;

	request.pBootPCallback = bootPCall;
	request.pTftpCallback = tftpCall;
	request.multiHomed = MultiHomed;
	request.altAddress = IP;

	/* Retrieve the bootP status panel */
	if( (BootPPanel = BuildBOOT_PANEL(gPanelHandle[_840PANEL])) < SUCCESS ){
		CPVTMessagePopup(LDBf(Ldb_Error), GetUILErrorString(BootPPanel), OK_ONLY_DLG, NO_HELP);
		return(BootPPanel);
	}

	#ifdef DEMOMODE
	DownloadTimer = BuildBOOT_PANEL_DemoTimer(BootPPanel);
	#endif

	InstallPopup(BootPPanel);

	/* Retrieve the tftp panel */
	if( (TftpPanel = BuildTFTP_PANEL(gPanelHandle[_840PANEL])) < SUCCESS ){
		CPVTMessagePopup(LDBf(Ldb_Error), GetUILErrorString(TftpPanel), OK_ONLY_DLG, NO_HELP);
		DiscardPanel(BootPPanel);
		return(TftpPanel);
	}

	GetCtrlAttribute(gPanelHandle[_840PANEL],  _840PANEL_DECORATION, ATTR_TOP,    &CtrlTop);
	GetCtrlAttribute(gPanelHandle[_840PANEL],  _840PANEL_DECORATION, ATTR_HEIGHT, &CtrlHeight);
	GetPanelAttribute(TftpPanel, ATTR_FRAME_THICKNESS,    &FrameThickness);
	GetPanelAttribute(TftpPanel, ATTR_TITLEBAR_THICKNESS, &TitleThickness);
	SetPanelAttribute(TftpPanel, ATTR_TOP, CtrlTop + FrameThickness + TitleThickness + BEVEL_THICKNESS);
	SetPanelAttribute(TftpPanel, ATTR_HEIGHT, CtrlHeight - ((2*BEVEL_THICKNESS) + (2*FrameThickness) + TitleThickness) );

	SetCtrlVal(TftpPanel, TFTP_PANEL_BD_FILE,  BdFileName);
	SetCtrlVal(TftpPanel, TFTP_PANEL_GUI_FILE, GuiFileName);

	SetCtrlAttribute(TftpPanel, TFTP_PANEL_BD_MSG,      ATTR_VISIBLE, FALSE);
	SetCtrlAttribute(TftpPanel, TFTP_PANEL_BD_PROGRAM,  ATTR_VISIBLE, FALSE); 
	SetCtrlAttribute(TftpPanel, TFTP_PANEL_BD_COMPLETE, ATTR_VISIBLE, FALSE);

	SetCtrlAttribute(TftpPanel, TFTP_PANEL_GUI_MSG,      ATTR_VISIBLE, FALSE); 
	SetCtrlAttribute(TftpPanel, TFTP_PANEL_GUI_PROGRAM,  ATTR_VISIBLE, FALSE);
	SetCtrlAttribute(TftpPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_VISIBLE, FALSE);

	DisplayPanel(TftpPanel);

	ServerStat = BOOTP_PROCESSING;
	BduStat    = BOOTP_PROCESSING;
	GuiStat    = BOOTP_PROCESSING;

	sAbortFlag = FALSE; // may be set TRUE by hitting CANCEL on BOOTP panel

	#ifdef DEMOMODE
	bootPCall(BOOTP_READY);
	#else
	StartSession(&request, &results);
	ServerStarted = 1;
	#endif

	while(serFlag != 0){
		if(ServerStat != BOOTP_PROCESSING){
			serFlag = 0;
			Ready = 0;
		}
          
		if(sAbortFlag == TRUE){ // may be set TRUE by hitting CANCEL on BOOTP panel
			serFlag = 0;
			Ready = 0;
			userAbort = TRUE;
		}    

		ProcessSystemEvents();   
	}

	#ifndef DEMOMODE
	EndSession();
	SetCtrlAttribute(BootPPanel, BOOT_PANEL_BOOTP_TIMEOUT, ATTR_ENABLED, 0);
	#else
	SetCtrlAttribute(BootPPanel, DownloadTimer, ATTR_ENABLED, 0);
	#endif

	if(userAbort == TRUE){
		// "User Abort" "Cancel"
		CPVTMessagePopup(LDBf(Ldb_Dload_UserAbortTitle), LDBf(Ldb_Dload_UserAbortMsg), OK_ONLY_DLG, NO_HELP);
	}

	if(ServerStat == BOOTP_PROBLEM){
		#ifndef DEMOMODE
		err = GetSessionLastError();
		AppErr(err, 0.0);
		#endif
	}    

	if(BduStat == TIMEOUT_ERROR){
		// "Timeout Abort" "BDU Transmittal Timeout"
		CPVTMessagePopup(LDBf(Ldb_Dload_TimeoutAbortTitle), LDBf(Ldb_Dload_BDUtimeoutMsg), OK_ONLY_DLG, NO_HELP);
	}
	if(GuiStat == TIMEOUT_ERROR){
		// "Timeout Abort" "GUI Transmittal Timeout"
		CPVTMessagePopup(LDBf(Ldb_Dload_TimeoutAbortTitle), LDBf(Ldb_Dload_GUItimeoutMsg), OK_ONLY_DLG, NO_HELP);
	}

	if(ServerStat == BOOTP_COMPLETE){
		cMsgBuf[0] = '\0';
		if(BdDone == TRUE){
			// "BDU COMPLETE"
			strcat(cMsgBuf, LDBf(Ldb_Dload_BDUcomplete));
		}
		if(GuiDone == TRUE){
			if(BdDone == TRUE){
				strcat(cMsgBuf, "\n");
			}
			// "GUI COMPLETE"
			strcat(cMsgBuf, LDBf(Ldb_Dload_GUIcomplete));
		}
		// "Software Installation/Replacement Done"
		CPVTMessagePopup(LDBf(Ldb_Dload_CompleteTitle), cMsgBuf, OK_ONLY_DLG, NO_HELP);
	} 

	DiscardPanel(TftpPanel);
	DiscardPanel(BootPPanel);

	return(SUCCESS);
}

/*****************************************************************************/
/* Name:     Set840Labels                                                     
/*                                                                           
/* Purpose:  sets the state of the 840 information displayed on the login panel
/*                                                                           
/* Example Call:   void Set840Labels(int ventdetected);                                    
/*                                                                           
/* Input:    int ventdetected	1 if detected, 0 if not       
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void Set840Labels(int ventdetected)
{
	char *BD;
	char *GUI;

	SetCtrlVal(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_AUTODET_STATUS, LDBf(Ldb_840_NoDetect));

	if(!ventdetected){
		SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_AUTODET_STATUS, ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_VENT_CAL_BUTTON,    ATTR_VISIBLE, TRUE);
		Register_ImageSetup_CB();
	}
	else{
		GUI = strchr(Vent.Gui.version, ':');
		if(GUI){
			GUI++;
			SetCtrlVal(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_GUISOFTWARE, GUI);
        }
		else{
			SetCtrlVal(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_GUISOFTWARE, LDBf(Ldb_Not_Found));
		}

		BD = strchr(Vent.Bd.version, ':');
		if(BD){
			BD++;
  			SetCtrlVal(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_BDSOFTWARE, BD);
        }
        else{
   			SetCtrlVal(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_BDSOFTWARE, LDBf(Ldb_Not_Found));
   		}

		SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_GUISOFTWARE_LBL,  ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_GUISOFTWARE,      ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_BDSOFTWARE_LBL,   ATTR_VISIBLE, TRUE);
		SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_BDSOFTWARE,       ATTR_VISIBLE, TRUE);

		SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_AUTODET_STATUS, ATTR_VISIBLE, FALSE);
	}

	return;
}


/*****************************************************************************/
/* Name:     Setup840Wrapper		                                                   
/*                                                                           
/* Purpose:  calls the autodetect function for the vent
/*                                                                           
/* Example Call:   void Setup840Wrapper(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
static void Setup840Wrapper(void)
{
    int bitmapId = 0; // needed if there are bitmaps 
    char *dataPtr = NULL; 

	LookingFor840 = 1;
	setButtonBMP(gPanelHandle[IMAG_PANEL], IMAG_PANEL_VENT_CAL_BUTTON, BMP_Cancel192);
	ResetDetect();
	Register_ImageSetup_CB();
	Set840Labels(!Establish840(AUTO_DETECT));
	ProcessSystemEvents();
	LookingFor840 = 0;
    dataPtr = 
        "!!!!!$!!-3\?C!<!!!!>!!!<9!!!<!!!!!!!!!Q!Q!!9!!!!!0!``Q`!!8/!!_!!_O`!YO_!QO\\\"FNZ"
        "#/+(;X[%FZ[#FZ[#Q\\[\"Y_4/@!`!!!LY_!LFZ#L;X%L+S&<`P'2/+ZNP)`OP'+OS&;OX%QO\\\"_E`"
        "/;`!!![!Y_[!FZ[#+SW&ZPU)/1<YN(<`P'L;X%LQ\\\"J_`/07!!_!!_O`!FOZ#+NS&/'%XMW'P\?W#C*"
        "FZ7XFR-HFJ%<BE_3B@\\.B>_3F@%<FD-HFJ7XGRC*GZQ\?U#/$<XM'<WK'<`P'L3O\"LQ\\\"4/5!_!!Q"
        "O\\\"3NO\"XNM'WNK'VNK'UNF'BJ)Y09ML`93A]90\?]2/\"/B\?]0B\?\\/'#/\?']0\?%]/\"9/>]2/\"/B"
        "\?]0B\?]/'\"/>%]/\"9/>`:4A0:MMCJ)YPNA\"VNK'WNJ'VNK'`OP';OX%_E`/2`!!![!FZW#^HW#TIW&"
        "TIW&QDW#@&FX+FBI^1B\?^1B@^1B\?^1B\?^0B\?^1B@]1B@^1E\?/$'^1@'^1\?%]/%91\?^:0\?+:FJ@J&XPNA"
        "\"TNI&UNG%VOI&3OO\"YE_/0`!!![!FZW#^HW#TGW$TGW%H4F^2PBM_3BA_3BA_3B@_23/&@'_3@'_2@"
        "'_3@'_2A'_3A'_3@'_3@'_2@'_3@'_3@'_2@'_3@'_3@72PN;H4^<TG%<TG%<TG$L3O\"JY_/0.!!_!!"
        "FNZ#^NH#QND#QND#AJ&X):AG!:5A!95B`95A`95B`:5A!:5A363;3633363;+63D+63D+63\\+63\\#9"
        "3%`:5A!:4A!:4A!95A`2/$5FA!5BA`5FA)@GGA'EY/$<QD#L3O\"JY_/0,!!_!!MJU^^NH#PNA\"PNA\""
        "<:`U#::C\":7C!2/%7FB\"7FB+36\\3313))!)11!199!9BB!BJJ!JRR&R##6###6#33633363336;+3"
        "6D+36D+36\\3LF1\"7FB\"7FC$:FC<`EU/$<PA\"L3O\"J_`/0*!!_!!YN_!^NH#MJ=`ON\?!<:`U$2/$"
        "9FC#:FC$:FC$9FC#9FC#9FD331;RR%R/+!!!!!999!999!RRR6;;;;\\%55$/$:9C#::C<:`U/'$O\?[!"
        "FZU#/*`!!!L3O\";M=`;M=`;\?%X7'\?E7%<D7%;E7%<E7%<D7%<E7%<D7%<E6+3\\6333!)))1/-!\"!!"
        "#6##T6TT$:<D$:<D%:<D%:;E%:<E':\?F\?J%XMJ=`MJ=`YKA_QN\\\"/0(!![!MUG^YAG_L:G_E-F\\-G"
        "FI'>FF'\?FF'>FF&\?FF'>FF'>FF'\?FE&>FF331;RR%R/-!!!!!9996;;;;\\%57'>F7'>F7&>F7&>E7'>"
        "F7'>F7-HI;D-[;L:_;L:_L3O\"4/'!_!!_O`!3NO\"JJ7]GJ3[5:TP(:AG(:AG(:AF):AG):AG(:AG):"
        "AF(:AF+6;\\3533)\"))/!-!!&!##6#TT6T=ZP)PP0)47&\?)BFG(AFG)AFG)AFG(AFG5UGPG3G[J7G]R"
        "9K^MUE^/&`!!!KMU^;R9^;I4\\;>#V7*DH7*DH6+C/*$H*:DI*:DH*:DH+:DH353;R\"RR/!-!!!!99&"
        "9;;7;\\%F5;RP)PP0)`X0!\?C%5/%7*DH7+CH;>\"V;H4];H4]L(F'J_`/0$!!_!!_O`!3NO\"FJ1[CJ,"
        "X3:RN,:GI,:FJ,:GJ,:FJ,:GJ/&$,GFI+;6\\3313))%)/-!!!!6###6TTT7;LF7,GI73L=]47\?]`X!]"
        "47\?7,FI7-GI7,GI7,FI73RN;C-Y;F1[;R9^LQ\\\"4/$!_!![J\\`ZN<\"EJ/Z>J#W.5J/$FK.IEJ/$7"
        ".JK7.IK7-JK633;!RRR1/-!!!!1\"11;6;;\\J%5.:JK.:JK.:IK+<D-PTP)`RX!T6TT.:JK.:IK.:JK"
        ".:JK9:^RDJ/YFK1ZBJIZ/0$!![!MUG^F/GXB+FX7WFQ0MFM0LEL/\"7/LM70ML60M/*$L0:LM=:Z)363"
        "33633#5##R!RRJ!JJ9!999\"99/!(!!!!RR&RTT6TD\\FF0MFL0MFM0MFL#3K-BO@%`X0!++&9#;F5#D"
        "F=0MFL0LFL7WGQC,GXB+KX!=K[_``!!!`!!![!7DGZA)GWA(FX2QFO1OFM2OFN2PFM2PFM2PFN2OFM;R"
        "F)##6###6#;;6;TT6TLL6LTT;T55F5TT6TLL6L33633363##1#RR!RJJ!J99!999&93373_'E-/$73PN"
        "72PN7#;-6#3L]`X!]`X!63DP7#;572PM71PN73PO;A(Y;@)W;S2WLQ\\\"`!!!`!!!K!=[;>&U;=\"V7"
        "4RO74SO74SO7D\\NK%5N6#++!9991/%!!!!1!11!!!!1!11!!!!R!RRR\"RR;6;;L6LL_J'-;6;;_J'-"
        "T6TTD6DDL6LLL:\\F5:SO5:SO4:RO4:RO#9D5Z83\\+T+9`SX!BEO%Z:7)3:RN4:SO3:RO<J!V>J&UGK"
        "*WBLIZ!`!!!^!!\\J2\\=J#T::^T5;UP%K5N^`'/!W)TVKVV=J==T6TT;5;;R\"RR/!%!!!!11!1!!!!"
        "99!911%1/%!!!!!)))63336DDD67CP2/$6:VP6:VQ6:UP5:UP#;D5BHO%`SX!^A^9Z6+\\+:H>5:UQ7:"
        "XQ;:]T<J$TCK'V7LDZ!`!!!^!!SJ2W;J!S9:\\S8;XR5L55!X)T`TX!!W)TNKNNNJNN_I'-J\"JJ/!$!"
        "!!!99&9DD6DDD1DRR!R11!111!1))!)JJ&J##6#336333633;6P3LF17XFQ8XFQ7XFR7XFQ7XFQ7XFR+"
        "LG=V+@A`X0!77\"1R+6L+HF;3RFI8YFR9\\GS;!GS=\"KT7DPZ!!`!!!W!P*FS9^FR:\\FS:[PS',0Z`"
        "X0!`X0!PH%1/&K555;_'-6TTTKNNNKNNNK^^^K^^^K===6TTT6333633363AA73D%2/+9:[S=<Z)`TX!"
        "`QX!R5#D_:8#0:OB9:[S::\\T9:^R:;_R7LDZ!`!!!^!!PJ*S8:\\Q;<]U'R,Z/0%`X'!^16X3TEF/$7"
        ";^TK-5=K-5=3/$5J55_J'-363336;;/&\"/H6`:^FS;^ES/%7;]T2/%;:^T;:]T;:]T;:]U3<TF`TX!`"
        "QX!R5#D^67\\1:QA::]R::^T8:\\Q8;\\Q7LDZ!`!!!^!!PJ*S6:YO<K!V5GNLBFO%/0$`X0!47&\?+LF"
        "=3TGF=!GV=!FV<`FU=`EV/$7=`U7<`U6=`/*%U=E!/&FU=`EV/$7<`U7=`V7=`U7=`T73TF]`X!]`X!&"
        "R#D&Z3L7.N;7=`U7<`U76YO78[PK/:U`!!!`!!!;P*S76XO;>#V;\?#W73\\N]`X!]`X!&Z3\\6#;L7#;"
        "%7+L=;@%X;\?$W:\?$/*&W\?E#/$GW\?$GW\?#GW\?#GW>#GW>#GV>#GW\?#GW>\"FV;^EP/$;>#W;\?\"U;\?\"V"
        "7;\\N]`X!]`X!&R+D&R+L7'G17<`R;>#W74VN76XOK/:U`!!!`!!!;P*S74VN;\?#W;@%X73TF]`X!]`X"
        "!&Z+3\"JZ3&R+D7#D-;@%X;@&X;@&X7;\\N3/*=K==5K55=K===J==_I'-9\"990:O;>J#U@J&XAJ'Y@"
        "J&X@J%WEL+;`TX!PQP)R5+DR6+L$:E(=J!R=J\"V2:SM4;VN/L:U!`!!!^!!PJ*S1:RL>J$WBJ)Y3<TF"
        "4T7\?`RX!L1TRB%R+R6#;#:;%AJ(YAJ(YBJ(Y;;\\N^L^^`^``/0(PH`X````PH`XPH`X``Q`99&9+NG/"
        "=\"FPA/+$(Y;B(YJ5NL]`X!]47\?&R+D&R+L7#D%;>#R;\?$W71RL77UMK7DZ`!!!`!!!K$1R2/\"/:PJ="
        "J!VDJ+[3<\\F'T,Z`SX!EAPNJ%Z+R6#D#:D%DJ+ZDJ+[DJ+[3;\\FNLNN`^``>:(5+1/$BE%3BE%+BF%"
        ">(P551U;#9E%#9K%-!AK11\"9`=6\\;`GKD*GWF,GU;%GVD+PZ',0Z`X+!BO2%R+2LZ36L&FG(\?%FT<`"
        "EU/\"7/PJ77UMKBIZ`!!!`!!!K/:U7.MI76XO;E.\\;;%NJ5FD]`X!]PP)&Z+3&Z3T7+N/;F.\\;E.\\"
        ";E.\\73\\FKNNN````3+B%\"R)\\#AT,#AT,#BN-\"R)\\3;R1`51;\"J!L\"J!TK-!K!111&Z3T7;^H"
        ";H,Q;Z8<;Z7;;F.\\]PP)]`X!6TTT&Z3L&Z3L7+D-;@&T7:]S7.MI7AWPKMU^`!!!`!!!KBIZ2/\"/:M"
        "J0:PKFJ/\\GJ1^;<\\F\?TC5`SX!EBPN#6;\\3:T=HJ1]GJ0]GJ0]3;\\=NLNN`^``3)B%R)9%!9LV!9L"
        "V)9;NR*9%;,R15]1;J%!TJ'!T-I!K1!11Z63L/&\"/LG2J+KD$;P\"`X0!470\?`X0!47&\?#DB%Z32L_;"
        "6V+NF/<`FO4UFO,JGHP*KS_``!!!`!!![!TVF]KZFJ*FGGA'GXI3G_;%KN5F@D`X0!PP&);Q6T3\\G=J"
        "4G^J3G^J4F_3\\K=NNPN``U`3BA%R9B%!LBV!LBV);ANR9E%;JP-PH[X-!KK9)KRNFA^11\"1R+6D,GG"
        ".K,KG*>5]/$]`X!J5FD6#D\\&Z3L6%@Z;;!H;\?%V2/\"/:LL,;JH/J:U/0$!![!_`[!$1FR'BFE9\\GS"
        "K6G_D-FN3TP=++09`X0!77&1;\\G3G*GKK7G`I4F\\;`K=NNPN``U`3JA-Z)5\\/$#BN-\"Z)\\33J-`"
        "````XXX`HHHKVVV!111&R+D7,H.;M/IK;B'4/%`SX!BFO%*6CO#6D\\9:]G::[S):EF4;PHMJU^/0%!!"
        "[!BIEZ/\"7/FF2/\"/:LKEJ.\\NN9!;J%F;8QTPTP)`TX!'R,ZFJ,\?JJ5[KJ6^HK)DNLNN`^``3)J-Z&"
        ")\\/!$NFA)Z)5\\3JP-XX[X^^F^LL6L##1#))\")Z36D-PG0N2KM8@P,`X0!`X0!47+\?BO6%3A6A#=7R"
        "<!GF>$FS*FFG'BFDQ^KK_`4/%!`!!!K[\\`7Q^K7&\?C76XO<O<\";D-N2/\"/:T5L8LDPSP)EBPN3:\\"
        "=HJ3WMJ9^HK-JNLNN`^``3)B%R)9%!9LV!9DN)9;NR*9%3,J-P^PPT5TT)!))1!9B9!BJZ6;L0:V6LJ4"
        "S^K<;5FNL=:Z)#5;TR5+DR5#;Z63L::^E::\\S&:\?C,;EEBJIZ/0'!![!BIEZ/\"7/FF7&\?C;D+Z<P\?$"
        ";;%F6+F_&Z3L64OI73L%;G1R;L8[;H-JKNNN````33B%#R9%'!T^'!LV'!DN#R9%33J-`PPP6TTT!199"
        "\"JZ+&R+D6$E]73\\=;L8Y<Q\?#;\?-J7+N/&Z3D&J#+&J#36#;L7<`N7&\?C7$<B7Q^KJ_`/0'!!_!![J\\"
        "`[J&M\"::A.:LELN8!DJ-O;:\\3*9K#`6=T4:X0KJ7WNJ:];K%FNLNN`^``3)B%N%5`)93F)93F19+FN"
        "&5`3,B%P^PP_I'-1!9BR6#3#6;T.:Q0MJ:\\PJ\?`NJ=^@J,J19P)Z53DJ5#3R6#3#6;T2:QN\"::A-;C"
        "CBJIZ/0)!![!MUF^>MBE`7F\?8YGRRAW%M;E\\/\"7/T52/\"/:S*=J&@QN@!QJ\?_IK.KNLNN`^``;*R1"
        "#%9/$E%#BE%#9E%;RP1TP[X55A59B\"JZ36D+F6_;`G=O<G\\TCW$P>G_F0FO9]F6#D2TZ;6L#;2TZ32"
        "\\`7B\?`7K\?$1ER/+`!!!K7DZ7,>@'^5>7<_U;N=`;\?-J;K9W;N<[<SC#;T@^;S5NKNNN`XXXKF=N3/&9"
        "K)RFL=NH_HHNINN1!19Z6+3#6;L::^:PJ>]VNG(TNE%NJ;ZAJ.K;:^76:W2#9D%^94>^:4>Q;^K[J\\`"
        "/0,!![!/:FU$8B<\\2F<<_GTVGW'XIW(WIW(WFW&TBG`R5KMVVPV51V;LL5L/&6LLD6DDD6TTTKF=N;_"
        "'-;_'-!11177Y3;Q\?]<XI)<XI(<TD$<SC#;M:]7<_T']2='\\2=7DPEKTV]4/-!_!!_O`!$J1R$98<[:"
        ".<7:YQQN\?$ZNL*WNH(SJC`GJ-J;6;;T6TTT6TT/+(==L=^'`/XX`XHHQH99&95TG*Q\?G\\ZLW)YLW*YL"
        "W*M9V!7YBQ[0B<[0F<>MKEMUE^//`!!!L_`!K/:U7,>@'Y-:2/\"/:LKFJ1\\[NO+QJ@`EI2K`55EN\""
        "X`D6DD/+*55G5_'A-11&1-G7]O=GZXKW)[NW+G0F].LBKY-B:Y-F:KZKJTVE]/2`!!!K7DZ7>ME'X+:'"
        "[0<77YR;I4^<RB\";D,H&R#3!\?EH1/,)\"))+6F_IJ4VMN:!79XR[90<V:*9\":3<[K&M[J\\`/04!!["
        "!TVG][&FM$8B<V*B9^4F>7WGPC*GWF0FP#D2TZ32DR+2/(3&R#3&R+;6#;L6#;T72QJ'^4>'V*9'[,97"
        ";HCK/:UJ_`/06!!_!!_O`!BJIZQ:^K$98<X9)8U:'7)9BCZ53\\Z53TZ53LZ2/$;6L#;2LZ32LZ32DR+"
        "2DJ#6D(@BFU'B7U'B7[,F95DKA$1KRTVE]/;`!!!KBIZ;[&M7;HC7\"3<'[,91/*U9'7X9)8_:0:,:>@"
        "Q;^K/K:UTJV]/0\?!![![\\K`BIKZ$1FRQ^EK/(7DPE7KZJ;[&MK7DZKTV]8/\"!]!!";
    GetUserBitmapFromText (dataPtr, 102, &bitmapId);
    if (bitmapId){
        SetCtrlBitmap (gPanelHandle[IMAG_PANEL], IMAG_PANEL_VENT_CAL_BUTTON, 0, bitmapId);
        DiscardBitmap (bitmapId);
        bitmapId = 0;
    }

	return;
}


/*****************************************************************************/
/* Name:    GetImageFile                                                     */
/*                                                                           */
/* Purpose: This function will display current images listed within abs.cfg  */
/*                                                                           */
/*                                                                           */
/* Input:   See Test step definition above                                   */
/*                                                                           */
/* Output:  <int>                                                            */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Globals Changed:                                                          */
/*                                                                           */
/*                                                                           */
/* Return:  <int>                                                            */
/*                                                                           */
/*****************************************************************************/
int GetImageFile(void)
{
	int fileDone;
	int x = 0;
	char inBuff[100];
	char errMsg[256];
	char ListBuff[256];       // buffer one Image file set to load into listbox
	int nBreakOnLibErrsFlag;  // flag to break/notbreak on library errors

	FILE *stream;

	nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break on error
	stream = fopen(AbsConfigFile, "r");
	SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
	if(stream == NULL){
		// "Can Not Locate Image File"
		strcpy(errMsg, LDBf(Ldb_ImagFileErrNofind));
		strcat(errMsg, "\n");
		strcat(errMsg, AbsConfigFile);
		// "Software File Error"
		CPVTMessagePopup(LDBf(Ldb_ImagFileErrTitle), errMsg, OK_ONLY_DLG, NO_HELP);
		return(-1);
	}

	/* Retrieve the image list panel */
	if ( (imagePanel = BuildIMAG_PANEL(0)) < SUCCESS ){
		CPVTMessagePopup(LDBf(Ldb_Error), GetUILErrorString(imagePanel), OK_ONLY_DLG, NO_HELP);
		fclose(stream);  
		return(imagePanel);
	}
	gPanelHandle[IMAG_PANEL] = imagePanel;

	Set840Labels(Is840Connected());

	if(MultiHomed){
	x = 0;
	while(x < MAX_ADAPTERS){
		InsertListItem(imagePanel, IMAG_PANEL_ADAPTER_LIST, x, szIPaddr[x], x);
		x++;
	}    
    SetCtrlAttribute(imagePanel, IMAG_PANEL_ADAPTER_LIST, ATTR_DFLT_INDEX, 0);
    }

	x = 0;
	while(x < MAX_IMAGES){
		fileDone = fscanf(stream, "%s", inBuff);

		if(fileDone == EOF){
			break;
		}

		strcpy(tempImage[x].partNumber,  strtok(inBuff, ","));
		strcpy(tempImage[x].rev,         strtok(NULL,   ",")); 
		strcpy(tempImage[x].bdFileName,  strtok(NULL,   ","));
		strcpy(tempImage[x].guiFileName, strtok(NULL,   ","));
		strcpy(tempImage[x].desc,        strtok(NULL,   ","));

		/* only show 3 elements (s/w pn), rev, desc */
		sprintf(ListBuff, "PN: %s    Rev: %s    Desc: %s", 
		        tempImage[x].partNumber, 
		        tempImage[x].rev, 
		        tempImage[x].desc);

		InsertListItem(imagePanel, IMAG_PANEL_IMAG_LIST, x, ListBuff, x);

		x++;
	}    
	fclose(stream);  
    SetCtrlAttribute(imagePanel, IMAG_PANEL_IMAG_LIST, ATTR_DFLT_INDEX, 0);

	/* RETRIEVE IMAGE FILE NAME */

	imagePanelDone = FALSE;
	imagePanelStatus = FAILURE;

	// Display the image list as a modal dialog
	InstallPopup(imagePanel);
	// Refresh the display of the ventilator's current software revisions
	ImagePanelCallback(imagePanel, IMAG_PANEL_VENT_CAL_BUTTON, EVENT_COMMIT, 0, 0, 0);
	// Wait for operator to select dnload file set 
	// and commit OK (IMAG_PANEL_LANG_DONE)
	// via Callback ImagePanelCallback
	while(imagePanelDone == FALSE){
		ProcessSystemEvents();
	} /* end while(imagePanelDone == FALSE) */

	DiscardPanel(imagePanel);

	return(imagePanelStatus); // set during ImagePanelCallback
}

/*****************************************************************************/
/* Name:    SetImageFile                                                     */
/*                                                                           */
/* Purpose: This function will checks and set imagefile to be download       */
/*                                                                           */
/*                                                                           */
/* Input:   See Test step definition above                                   */
/*                                                                           */
/* Output:  <int>                                                            */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Globals Changed:                                                          */
/*                                                                           */
/*                                                                           */
/* Return:  <int>  if connected                                              */
/*                                                                           */
/*****************************************************************************/
int SetImageFile(char *bd, char *gui)
{
	int err = 0;
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors
	char bdTemp[256];
	char guiTemp[256];  
	FILE *bdStream;
	FILE *guiStream;

	strcpy(BdFileName,  bd); // 8.3 format
	strcpy(GuiFileName, gui);

	strcpy(bdTemp, AbsFilePath);
	strcat(bdTemp, bd);

	strcpy(guiTemp, AbsFilePath);
	strcat(guiTemp, gui);

	nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break on error
	bdStream = fopen(bdTemp, "r");
	SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
	if(bdStream == NULL){
		err = 1;
	}
	else{
		fclose(bdStream);
	}

	nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break on error
	guiStream = fopen(guiTemp, "r");
	SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
	if(guiStream == NULL){
		err = 1;
	}
	else{
		fclose(guiStream);
	}

	return(err);
}

/*****************************************************************************/
/* Name:    SelfTest                                                         */
/* Purpose: Check comport status                                             */
/* Example Call:   SelfTest(&data);                                          */
/*                                                                           */
/* Input:  data  pointer to test data structure                              */
/* Output: <None>                                                            */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return: <None>                                                            */
/*           Note: if error is detected program is aborted through AppError()*/
/*****************************************************************************/
int SelfTest(void) 
{
	char ProjectDir[         MAX_PATHNAME_LEN];
	char ImageConfigFile[    MAX_PATHNAME_LEN];
	char IPConfigFile[       MAX_PATHNAME_LEN];
	char defaultFileSpec[    MAX_FILE_EXTENSION_LENGTH +1];

	char ConfigDriveName[    MAX_DRIVENAME_LEN];
	char ConfigDirectoryName[MAX_DIRNAME_LEN];
	char ConfigFileName[     MAX_FILENAME_LEN];

	int hAgreePnl;
	int insPanel;
	int handle;
	int control;
	int panelDone;

	char ipTemp[MAX_CHAR_STRING];
	FILE *stream;
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors
	int status;
	int i;
	WORD wVersionRequested;
	WSADATA wsaData;
	char szHostName[256];
	HOSTENT *pHostEnt;
	int nAdapter = 0;
	struct in_addr addr;
	char *szIPtemp;
	

	status = FALSE;
	while(status == FALSE){
		SetMouseCursor(VAL_HOUR_GLASS_CURSOR);
		for(i=2; i<25; i++){ // start looking for CD in drive 2 ('C')
			sprintf(AbsFilePath, "%c%s%s", 'A'+i, ":", AbsConfigPath);
			strcpy(AbsConfigFile, AbsFilePath);
			strcat(AbsConfigFile, AbsConfigName);
			nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break on error
			stream = fopen(AbsConfigFile, "r");
			SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
			if(stream != NULL){
				fclose(stream);
				strcpy(ImageConfigFile, AbsFilePath);
				strcat(ImageConfigFile, "mediachk.chk");
				nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break on error
				stream = fopen(ImageConfigFile, "w");
				SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag
				if(stream != NULL){
					// error, can write to media.
					// should not be possible if AbsFilePath points to CD (non-writable) media
					fclose(stream);
					DeleteFile(ImageConfigFile);
#ifdef DEMOMODE
//#define USE_HD_ABS
#endif
					#ifdef USE_HD_ABS
					status = TRUE;
					break;
					#endif
				}
				else{
					status = TRUE;
					break;
				}
			}
		}
		SetMouseCursor(VAL_DEFAULT_CURSOR);
		if(i >= 25){
			if(CPVTConfirmPopup(LDBf(Ldb_NoDloadCDerrTitle), LDBf(Ldb_NoDloadCDerrMsg), OK_CANCEL_DLG, NO_HELP)){
				SetMouseCursor(VAL_HOUR_GLASS_CURSOR);
				Delay(5.0);
				SetMouseCursor(VAL_DEFAULT_CURSOR);
			}
			else{
				return(CONFIG_ERROR);
			}
		}
	}

	if(!GetCSEEnabled()){
		if( (hAgreePnl = MakeAgreePanel(0)) < SUCCESS ){
			CPVTMessagePopup(LDBf(Ldb_Error), GetUILErrorString(hAgreePnl), OK_ONLY_DLG, NO_HELP);
			return(-1);
		}

		panelDone = FALSE;
		InstallPopup(hAgreePnl);
		while(panelDone == FALSE){
			GetUserEvent(0, &handle, &control);
			if(handle != hAgreePnl){
				continue;
			}

			if(control == AGREE_CMD_AGREE_YES){
				panelDone = TRUE;
			}
			if(control == AGREE_CMD_AGREE_NO){
				panelDone = TRUE;
				DiscardPanel(hAgreePnl);
				return(-1);
			}
		}
		DiscardPanel(hAgreePnl);
	}

	wVersionRequested = MAKEWORD(1, 1);
	if( (status = WSAStartup(wVersionRequested, &wsaData)) != SUCCESS){
		CPVTMessagePopup(LDBf(Ldb_Error), UDPErrorString(status), OK_ONLY_DLG, NO_HELP);
		WSACleanup();
		return status;
	}
	if( (status = gethostname(szHostName, sizeof(szHostName)) ) != SUCCESS){
		status = WSAGetLastError();
		CPVTMessagePopup(LDBf(Ldb_Error), UDPErrorString(status), OK_ONLY_DLG, NO_HELP);
		WSACleanup();
		return status;
	}
	if( (pHostEnt = gethostbyname(szHostName)) == NULL){
		Delay(0.1);
		status = WSAGetLastError();
		CPVTMessagePopup(LDBf(Ldb_Error), UDPErrorString(status), OK_ONLY_DLG, NO_HELP);
		WSACleanup();
		return status;
	}
	MultiHomed = FALSE;
	nAdapter = 0;
	while(pHostEnt->h_addr_list[nAdapter] && nAdapter < MAX_ADAPTERS){
		addr = *(struct in_addr far *)(pHostEnt->h_addr_list[nAdapter]);
		szIPtemp = inet_ntoa(addr);
		if(strcmp(szIPtemp, "127.0.0.1") == 0){
			/* idle dial-up network */
			;
			/* Display Instructions for Cabling and Ventilator Startup */
			if( (insPanel = BuildINS_PANEL(0)) < SUCCESS ){
				CPVTMessagePopup(LDBf(Ldb_Error), GetUILErrorString(insPanel), OK_ONLY_DLG, NO_HELP);
				return(-1);
			}

			panelDone = FALSE;
			InstallPopup(insPanel);
			while(panelDone == FALSE){
				GetUserEvent(0, &handle, &control);
				if(handle != insPanel){
					continue;
				}

				if(control == INS_PANEL_INS_DONE){
					panelDone = TRUE;
				}
				if(control == INS_PANEL_INS_CANCEL){
					panelDone = TRUE;
					DiscardPanel(insPanel);
					return(-1);
				}
			}
			DiscardPanel(insPanel);
			WSACleanup();
			wVersionRequested = MAKEWORD(1, 1);
			if( (status = WSAStartup(wVersionRequested, &wsaData)) != SUCCESS){
				CPVTMessagePopup(LDBf(Ldb_Error), UDPErrorString(status), OK_ONLY_DLG, NO_HELP);
				WSACleanup();
				return status;
			}
			if( (status = gethostname(szHostName, sizeof(szHostName)) ) != SUCCESS){
				status = WSAGetLastError();
				CPVTMessagePopup(LDBf(Ldb_Error), UDPErrorString(status), OK_ONLY_DLG, NO_HELP);
				WSACleanup();
				return status;
			}
			if( (pHostEnt = gethostbyname(szHostName)) == NULL){
				Delay(0.1);
				status = WSAGetLastError();
				CPVTMessagePopup(LDBf(Ldb_Error), UDPErrorString(status), OK_ONLY_DLG, NO_HELP);
				WSACleanup();
				return status;
			}
			continue;
		}
		strcpy(szIPaddr[nAdapter], szIPtemp);
		strcpy(IP, szIPtemp);
		nAdapter++;
	}
	nNumAdapters = nAdapter;
	if(nNumAdapters > 1){
		MultiHomed = TRUE;
	}
	WSACleanup();

	SelfTestDone = TRUE;

	return(SUCCESS);   
}

/*****************************************************************************/
/* Name:    bootPCall                                                        */
/*                                                                           */
/* Purpose: This function is used for recieving Bootp Messages               */
/*                                                                           */
/*                                                                           */
/* Input:   <int>                                                            */
/*                                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*                                                                           */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  <None>                                                           */
/*****************************************************************************/
void bootPCall(int incomingMsg)
{
	double elapsedTime;

	elapsedTime = Timer();

	switch(incomingMsg){
		case BOOTP_ERROR:    // error in dll server
                             ResetTextBox(BootPPanel, BOOT_PANEL_BOOTP_TEXT, LDBf(Ldb_BootP_Error));
                             SetCtrlAttribute(BootPPanel, BOOT_PANEL_BOOTP_TIMEOUT, ATTR_ENABLED, 0);
                             #ifdef DEMOMODE
                             SetCtrlAttribute(BootPPanel, DownloadTimer, ATTR_ENABLED, 0);
                             #endif
                             ServerStat = BOOTP_PROBLEM;
                             break;
                                
		case BOOTP_STOPPED:  // we stop the server
                             if(Ready){ // this is for dll restarts
                             	ResetTextBox(BootPPanel, BOOT_PANEL_BOOTP_TEXT, LDBf(Ldb_BootP_Stopped));
                             	SetCtrlAttribute(BootPPanel, BOOT_PANEL_BOOTP_TIMEOUT, ATTR_ENABLED, 0);
                             	#ifdef DEMOMODE
                             	SetCtrlAttribute(BootPPanel, DownloadTimer, ATTR_ENABLED, 0);
                             	#endif
                             }
                             break;
                                
		case BOOTP_READY:    // ready and waiting
                             Ready = 1;
                             ResetTextBox(BootPPanel, BOOT_PANEL_BOOTP_TEXT, LDBf(Ldb_BootP_Ready));
                             // start calcTimeout
                             SetCtrlAttribute(BootPPanel, BOOT_PANEL_BOOTP_TIMEOUT, ATTR_ENABLED, 1);
                             #ifdef DEMOMODE
                             SetCtrlAttribute(BootPPanel, DownloadTimer, ATTR_ENABLED, 1);
                             #endif
                             GuiWatchDog = elapsedTime + (double)TIMEOUT; 
                             BdWatchDog = elapsedTime + (double)TIMEOUT; 
                             break;
                                
		case GUI_REQUEST:    // found gui
                             GuiFound = 1;
                             ResetTextBox(BootPPanel, BOOT_PANEL_BOOTP_TEXT, LDBf(Ldb_BootP_Processing));
                             GuiWatchDog = elapsedTime + (double)TIMEOUT;
                             DisplayPanel(TftpPanel);
                             break;
                                
		case BD_REQUEST:     // found bd
                             BdFound = 1;
                             ResetTextBox(BootPPanel, BOOT_PANEL_BOOTP_TEXT, LDBf(Ldb_BootP_Processing));
                             BdWatchDog = elapsedTime + (double)TIMEOUT; 
                             DisplayPanel(TftpPanel);    
                             break;
	} // end switch incomingMsg

}

/*****************************************************************************/
/* Name:    tftpCall                                                         */
/*                                                                           */
/* Purpose: This function is used for recieving Tftp Messages                */
/*                                                                           */
/*                                                                           */
/* Input:   <int,int,int>                                                    */
/*                                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           Revision                                                        */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  <None>                                                           */
/*****************************************************************************/
void tftpCall(int id, int currentblock, int totalblock)
{
	double value; // a pair one each gui and bd
	double elapsedTime;

	elapsedTime = Timer();

	// update panel  

	if(id == BD_DOWNLOAD_ID){
		BdWatchDog = elapsedTime + (double)TIMEOUT;
		if(totalblock > 0){
			SetCtrlVal(TftpPanel, TFTP_PANEL_BD_TOTAL, totalblock);
			SetCtrlVal(TftpPanel, TFTP_PANEL_BD_BLOCK_SENT, currentblock);
			value = ((double)currentblock / (double)totalblock) * 100.0;
			if(value > 100.0){
				value = 100.0;
			}
			SetCtrlVal(TftpPanel, TFTP_PANEL_BD_PERCENT, value);
		}

		if(currentblock >= totalblock){
			SetCtrlAttribute(TftpPanel, TFTP_PANEL_BD_MSG,     ATTR_VISIBLE, 1);
			SetCtrlAttribute(TftpPanel, TFTP_PANEL_BD_PROGRAM, ATTR_VISIBLE, 1);
		}

		if(currentblock == -1 && totalblock == -1){
			SetCtrlAttribute(TftpPanel, TFTP_PANEL_BD_COMPLETE, ATTR_VISIBLE, 1); 
			BdDone = 1;
		}
	}
	else if(id == GUI_DOWNLOAD_ID){
		GuiWatchDog = elapsedTime + (double)TIMEOUT;
		if(totalblock > 0){
			SetCtrlVal(TftpPanel, TFTP_PANEL_GUI_TOTAL, totalblock);
			SetCtrlVal(TftpPanel, TFTP_PANEL_GUI_BLOCK_SENT, currentblock);
			value = ((double)currentblock / (double)totalblock) * 100.0;
			if(value > 100.0){
				value = 100.0;
			}
			SetCtrlVal(TftpPanel, TFTP_PANEL_GUI_PERCENT, value);
		}

		if(currentblock >= totalblock){
			SetCtrlAttribute(TftpPanel, TFTP_PANEL_GUI_MSG,     ATTR_VISIBLE, 1); 
			SetCtrlAttribute(TftpPanel, TFTP_PANEL_GUI_PROGRAM, ATTR_VISIBLE, 1);
		}

		if(currentblock == -1 && totalblock == -1){
			SetCtrlAttribute(TftpPanel, TFTP_PANEL_GUI_COMPLETE, ATTR_VISIBLE, 1); 
			GuiDone = 1;
		}
	}

	/****** CONDITIONS IF ONLY GUI START AND COMPLETE.***********/
	/***************** IF ONLY BD  START AND COMPLETE.***********/
	/***************** IF BOTH GUI AND BD START AND COMPLETE.****/

	if(GuiDone == 1 && BdFound == 0){
		ServerStat = BOOTP_COMPLETE;
	}

	if(BdDone == 1 && GuiFound == 0){
		ServerStat = BOOTP_COMPLETE;
	}

	if(GuiDone == 1 && BdDone == 1){
		ServerStat = BOOTP_COMPLETE;
	}

	ProcessSystemEvents();
}

/*****************************************************************************/
/* Name:    calcTimeout                                                      */
/*                                                                           */
/* Purpose: This function is a CVI callback to increment timers              */
/*                                                                           */
/*                                                                           */
/* Input:                                                                    */
/*                                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           Revision                                                        */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  <None>                                                           */
/*****************************************************************************/

int  CVICALLBACK calcTimeout(int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	// timeout occurs when a response is not recieved from vent
	// in a timely manner

	double elapsedTime;

	elapsedTime = Timer();

	/* request from bd or gui */
	if(( (BdWatchDog + (double)START_TIME_OUT) < elapsedTime ) && (BdFound == 0) ){
		BduStat =  TIMEOUT_ERROR;
	}

	if(( (GuiWatchDog + (double)START_TIME_OUT) < elapsedTime )&& (GuiFound == 0) ){
		GuiStat =  TIMEOUT_ERROR;
	}

	/* Sequence block time out WatchDogs updated by tftpCall */
	if( (BdWatchDog < elapsedTime )  && (BdDone != 1) && (BdFound == 1) ){
		BduStat =  TIMEOUT_ERROR;
	}

	if( (GuiWatchDog < elapsedTime ) && (GuiDone != 1) && (GuiFound == 1) ){
		GuiStat =  TIMEOUT_ERROR;
	}

	if(BduStat == TIMEOUT_ERROR && GuiStat == TIMEOUT_ERROR){
		ServerStat =  TIMEOUT_ERROR;  
	}

	return(0);
}    

/*****************************************************************************/
/* Name:     ImagePanelCallback                                              */
/*                                                                           */
/* Purpose:  Handles commands on Image Panel                                 */
/*                                                                           */
/* Example Call:                                                             */
/*           int CVICALLBACK ImagePanelCallback(int panel, int event,        */
/*                                        void *callbackData,                */
/*                                        int eventData1, int eventData2);   */
/*                                                                           */
/* Input:    Standard PANEL callback arguments                               */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           LookingFor840, imagePanelStatus, imagePanelDone                 */
/*                                                                           */
/* Globals Used:                                                             */
/*           MultiHomed, IP                                                  */
/*                                                                           */
/* Return:   always 0                                                        */
/*                                                                           */
/*****************************************************************************/

int CVICALLBACK ImagePanelCallback(int panel, int control, int event, 
                                   void *callbackData, 
                                   int eventData1, int eventData2)
{
	int status;

	switch(event){
		case EVENT_COMMIT:
			if(control == IMAG_PANEL_VENT_CAL_BUTTON){
				if(LookingFor840){
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_VENT_CAL_BUTTON, ATTR_DIMMED, TRUE);
					LookingFor840 = 0;
					Stop840Detect();
				}
				else{
					if(MultiHomed){
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_ADAPTER_LIST, ATTR_DIMMED, TRUE);
					}
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_IMAG_LIST,    ATTR_DIMMED, TRUE);
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_LANG_DONE,    ATTR_DIMMED, TRUE);
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_LANG_CANCEL,  ATTR_DIMMED, TRUE);

				    Setup840Wrapper();

					if(MultiHomed){
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_ADAPTER_LIST,    ATTR_DIMMED, FALSE);
					}
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_IMAG_LIST,       ATTR_DIMMED, FALSE);
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_LANG_DONE,       ATTR_DIMMED, FALSE);
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_LANG_CANCEL,     ATTR_DIMMED, FALSE);
					SetCtrlAttribute(gPanelHandle[IMAG_PANEL], IMAG_PANEL_VENT_CAL_BUTTON, ATTR_DIMMED, FALSE);
				}
			}
			else if(control == IMAG_PANEL_LANG_DONE){
				int selection;
				int answer;

				if(MultiHomed){
				GetCtrlIndex(imagePanel, IMAG_PANEL_ADAPTER_LIST, &selection);
				GetLabelFromIndex(imagePanel, IMAG_PANEL_ADAPTER_LIST, selection, IP);
				}

				GetCtrlIndex(imagePanel, IMAG_PANEL_IMAG_LIST, &selection);

				if(SetImageFile(tempImage[selection].bdFileName,
				                tempImage[selection].guiFileName)){
					answer = CPVTConfirmPopup("ABS FILE NOT FOUND", LDBf(Ldb_Retry_prompt), OK_CANCEL_DLG, NO_HELP);
					if(!answer){
						imagePanelStatus = FAILURE;
						imagePanelDone = TRUE;
					}
				}
				else{
					imagePanelStatus = SUCCESS;
					imagePanelDone = TRUE;
				}
			}
			else if(control == IMAG_PANEL_LANG_CANCEL){
				imagePanelStatus = FAILURE;
				imagePanelDone = TRUE;
			}
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return(0);
}

/*****************************************************************************/
/* Name:     BOOTPCancelCallback                                             */
/*                                                                           */
/* Purpose:  Handles Cancel command on BOOTP (tftp status) panel             */
/*                                                                           */
/* Example Call:                                                             */
/*           int CVICALLBACK BOOTPCancelCallback(int panel, int event,       */
/*                                        void *callbackData,                */
/*                                        int eventData1, int eventData2);   */
/*                                                                           */
/* Input:    Standard PANEL callback arguments                               */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           sAbortFlag                                                      */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Return:   always 0                                                        */
/*                                                                           */
/*****************************************************************************/

int CVICALLBACK BOOTPCancelCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	int status;

	switch(event){
		case EVENT_COMMIT:
			SetSysPanelMessage("");
			sAbortFlag = TRUE;
			break;
		case EVENT_KEYPRESS:
			switch(eventData1){
				case VAL_F1_VKEY:
				case VAL_F1_VKEY | VAL_SHIFT_MODIFIER:
					ShowHelp(panel, GetActiveCtrl(panel), eventData1);
					break;
				default:
					break;
			} // end switch(eventData1)
			break;
		// end case EVENT_KEYPRESS
		default:
			break;
	} // end switch(event)

	return(0);
}

#ifdef DEMOMODE
/*****************************************************************************/
/* Name:    BuildBOOT_PANEL_DemoTimer                                        */
/*                                                                           */
/* Purpose: For Demo version of program,                                     */
/*          create a timer to manage simulated tftp software download        */
/*                                                                           */
/* Input:   Handle of parent panel where timer is created                    */
/*                                                                           */
/* Output:  <None>                                                           */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Return:  Handle of timer or error code                                    */
/*****************************************************************************/

int BuildBOOT_PANEL_DemoTimer(int hParentPanel)
{
    int error = 0;
    int BOOT_PANEL_DemoTimer;

    // Build control: BOOT_PANEL_BOOTP_TIMEOUT
    errChk(BOOT_PANEL_DemoTimer = NewCtrl (hParentPanel, CTRL_TIMER, "Untitled Control", 33, 25));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_CONSTANT_NAME, "BOOTP_DEMOTIMEOUT"));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_CALLBACK_FUNCTION_POINTER, BootpDemoTimeout));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_SHORTCUT_KEY, 0));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_CTRL_MODE, VAL_INDICATOR));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_LABEL_WIDTH, 75));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_LABEL_HEIGHT, 15));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_LABEL_BGCOLOR, VAL_LT_GRAY));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_INTERVAL, 1.0));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_ENABLED, FALSE));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_HEIGHT, 38));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_WIDTH, 38));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_LABEL_TOP, 12));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_LABEL_LEFT, 7));
    errChk(SetCtrlAttribute(hParentPanel, BOOT_PANEL_DemoTimer, ATTR_LABEL_SIZE_TO_TEXT, TRUE));

    //We're done!
    return BOOT_PANEL_DemoTimer;
Error:
    return error;
}

#define DATA_BLOCK_SIZE 4096

/*****************************************************************************/
/* Name:     BootpDemoTimeout                                                */
/*                                                                           */
/* Purpose:  For Demo version of program,                                    */
/*           simulates tftp software download                                */
/*                                                                           */
/* Example Call:                                                             */
/*           int CVICALLBACK BootpDemoTimeout(int panel, int event,          */
/*                                        void *callbackData,                */
/*                                        int eventData1, int eventData2);   */
/*                                                                           */
/* Input:    Standard PANEL callback arguments                               */
/*                                                                           */
/* Output:   <None>                                                          */
/*                                                                           */
/* Globals Changed:                                                          */
/*           <None>                                                          */
/*                                                                           */
/* Globals Used:                                                             */
/*           <None>                                                          */
/*                                                                           */
/* Return:   always 0                                                        */
/*                                                                           */
/*****************************************************************************/

int CVICALLBACK BootpDemoTimeout(int panel, int control, int event, void *callbackData, int eventData1, int eventData2)
{
	double elapsedTime;
	int err = 0;

	char bdTemp[256];
	long bdFileSize = 0L;
	static HFILE bdStream = 0;
	static unsigned int bdCurrentBlock = 0;
	static unsigned int bdTotalBlocks = 0;
	static double bdTimeOut = 0.0;

	char guiTemp[256];  
	long guiFileSize = 0L;
	static HFILE guiStream = 0;
	static unsigned int guiCurrentBlock = 0;
	static unsigned int guiTotalBlocks = 0;
	static double guiTimeOut = 0.0;

	char cFileBuff[DATA_BLOCK_SIZE +1];
	int nBytesRead = 0;;

	elapsedTime = Timer();

	if(BdDone && GuiDone){
		return(0);
	}

//  For demo testing with BDU not detected, uncomment the following line
//	goto nobdu;
	if(Ready == 1 && BdFound == 0/* && nRunMode != 2*/){
		strcpy(bdTemp, AbsFilePath);
		strcat(bdTemp, BdFileName);
		GetFileInfo(bdTemp, &bdFileSize);
		bdTotalBlocks = bdFileSize / DATA_BLOCK_SIZE;
		bdCurrentBlock = 0;
		bdStream = OpenFile(bdTemp, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_BINARY);
		if(bdStream == -1){
			err = 1;
		}
		else{
			bootPCall(BD_REQUEST);
			SetCtrlAttribute(panel, control, ATTR_INTERVAL, 0.1);
		}
		return(0);
	}
	nobdu:

//  For demo testing with GUI not detected, uncomment the following line
//	goto nogui;
	if(Ready == 1 && GuiFound == 0/* && nRunMode != 1*/){
		strcpy(guiTemp, AbsFilePath);
		strcat(guiTemp, GuiFileName);
		GetFileInfo(guiTemp, &guiFileSize);
		guiTotalBlocks = guiFileSize / DATA_BLOCK_SIZE;
		guiCurrentBlock = 0;
		guiStream = OpenFile(guiTemp, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_BINARY);
		if(guiStream == -1){
			err = 1;
		}
		else{
			bootPCall(GUI_REQUEST);
			SetCtrlAttribute(panel, control, ATTR_INTERVAL, 0.1);
		}
		return(0);
	}
	nogui:

	if(Ready == 1 && BdFound == 1){
		if(BdDone == 0){
			if((nBytesRead = ReadFile(bdStream, cFileBuff, DATA_BLOCK_SIZE)) != DATA_BLOCK_SIZE){
				if(nBytesRead == 0){ // EOF
					if(elapsedTime > bdTimeOut){
						err = CloseFile(bdStream);
						tftpCall(BD_DOWNLOAD_ID, -1, -1);
					}
				}
			}
			else{
				bdCurrentBlock += 1;
				tftpCall(BD_DOWNLOAD_ID, bdCurrentBlock, bdTotalBlocks);
				if(bdCurrentBlock >= bdTotalBlocks){
					bdTimeOut = elapsedTime + 5.0;
				}
			}
		}
	}

	if(Ready == 1 && GuiFound == 1){
		if(GuiDone == 0){
			if((nBytesRead = ReadFile(guiStream, cFileBuff, DATA_BLOCK_SIZE)) != DATA_BLOCK_SIZE){
				if(nBytesRead == 0){ // EOF
					if(elapsedTime > guiTimeOut){
						err = CloseFile(guiStream);
						tftpCall(GUI_DOWNLOAD_ID, -1, -1);
					}
				}
			}
			else{
				guiCurrentBlock += 1;
				tftpCall(GUI_DOWNLOAD_ID, guiCurrentBlock, guiTotalBlocks);
				if(guiCurrentBlock >= guiTotalBlocks){
					guiTimeOut = elapsedTime + 5.0;
				}
			}
		}
	}
	return(0);
}    
#endif

char *UDPErrorString(int error)
{
	switch(error)
	{
		case 0:
			return "No Error.";
			//break;
		case WSAEACCES: // 13
			return "Permission denied.";
			//break;
		case WSAEADDRINUSE: // 48
			return "Socket address is already in use.";
			//break;
		case WSAEADDRNOTAVAIL: // 49
			return "Cannot assign requested address.";
			//break;
		case WSAEAFNOSUPPORT: // 47
			return "Address family not supported by protocol family.";
			//break;
		case WSAEALREADY: // 37
			return "Operation already in progress.";
			//break;
		case WSAECONNABORTED: // 53
			return "The connection has been aborted.";
			//break;
		case WSAECONNREFUSED: // 61
			return "Connection refused.";
			//break;
		case WSAECONNRESET: // 54
			return "The connection was reset by the remote site.";
			//break;
		case WSAEDESTADDRREQ: // 39
			return "Destination address required.";
			//break;
		case WSAEDISCON: // 94
			return "Graceful shutdown in progress.";
			//break;
		case WSAEFAULT: // 14
			return "Invalid pointer value, or buffer is too small.";
			//break;
		case WSAEHOSTDOWN: // 64
			return "Host is down.";
			//break;
		case WSAEHOSTUNREACH: // 65
			return "No route to host.";
			//break;
		case WSAEINPROGRESS: // 36
			return "Another Windows socket operation is in progress. A blocking operation is currently executing.";
			// break;
		case WSAEINTR: // 04
			return "The (blocking) call was canceled through WSACancelBlockingCall.";
			//break;
		case WSAEINVAL: // 22
			return "An invalid argument was supplied.";
			//break;
		case WSAEISCONN: // 56
			return "Socket is already connected.";
			//break;
		case WSAEMFILE: // 24
			return "Too many open sockets.";
			//break;
		case WSAEMSGSIZE: // 40
			return "The data was to large to fit in the specified buffer and has been truncated.";
			//break;
		case WSAENETDOWN: // 50
			return "The network subsystem has failed.";
			//break;
		case WSAENETRESET: // 52
			return "Network dropped connection on reset.";
			//break;
		case WSAENETUNREACH: // 51
			return "Network is unreachable.";
			//break;
		case WSAENOBUFS: // 55
			return "No buffer space available, queue was full.";
			//break;
		case WSAENOPROTOOPT: // 42
			return "Invalid or unsupported protocol option.";
			//break;
		case WSAENOTCONN: // 57
			return "Socket not connected.";
			//break;
		case WSAENOTSOCK: // 38
			return "Not a socket.";
			//break;
		case WSAEOPNOTSUPP: // 45
			return "Feature not supported.";
			//break;
		case WSAEPFNOSUPPORT: // 46
			return "Protocol family not supported.";
			//break;
		case WSAEPROCLIM: // 67
			return "Too many processes.";
			//break;
		case WSAEPROTONOSUPPORT: // 43
			return "Protocol not supported.";
			//break;
		case WSAEPROTOTYPE: // 41
			return "Protocol wrong type for socket.";
			//break;
		case WSAESHUTDOWN: // 58
			return "Cannot send after socket shutdown.";
			//break;
		case WSAESOCKTNOSUPPORT: // 44
			return "Socket type not supported.";
			//break;
		case WSAETIMEDOUT: // 60
			return "Connection timed out.";
			//break;
		case WSAEWOULDBLOCK: // 35
			return "A non-blocking socket operation could not be completed immediately.";
			//break;
		case WSAHOST_NOT_FOUND: // 1001
			return "Host not found.";
			//break;
		case WSANO_DATA: // 1004
			return "Valid name, no data record of requested type.";
			//break;
		case WSANO_RECOVERY: // 1003
			return "Non-recoverable error.";
			//break;
		case WSANOTINITIALISED: // 93
			return "A successful WSAStartup call must occur before using this function.";
			//break;
		case WSASYSNOTREADY: // 91
			return "Network subsystem is unavailable.";
			//break;
		case WSATRY_AGAIN: // 1002
			return "Non-authoritative host not found.";
			//break;
		case WSATYPE_NOT_FOUND: // 109
			return "Class type not found.";
			//break;
		case WSAVERNOTSUPPORTED: // 92
			return "WINSOCK.DLL version out of range.";
			//break;
		default:
			return "Unknown System Error.";
			//break;
	}
//	return "Unknown System Error.";
}
