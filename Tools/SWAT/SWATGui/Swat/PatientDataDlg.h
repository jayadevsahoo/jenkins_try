#pragma once
#include "afxcmn.h"
#include "PD_ComboListCtrl.h"

// CPatientDataDlg dialog

class CPatientDataDlg : public CDialog
{
	DECLARE_DYNAMIC(CPatientDataDlg)

public:
	CPatientDataDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPatientDataDlg();

// Dialog Data
	enum { IDD = IDD_PATIENT_DATA_DLG };

protected:
	HICON m_hIcon;
	afx_msg LRESULT OnEndLabelEditVariableCriteria(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT PopulateComboList(WPARAM wParam, LPARAM lParam);
	afx_msg void OnRclickList(NMHDR* pNMHDR, LRESULT* pResult);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnPaint();


	DECLARE_MESSAGE_MAP()
public:
	CComboListCtrl m_List;
	afx_msg void OnLvnItemchangedListControl(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL OnInitDialog();

	void InsertColumns();
	afx_msg void OnLvnBeginlabeleditListControl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLstInsert();

	void RefreshBDValues();

private:
	int m_iItemCount;
public:
	
	void PopulateListBox();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonWaveform();
	void OverWrite();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonClear();
	afx_msg void OnBnClickedButtonCopyID();
};
