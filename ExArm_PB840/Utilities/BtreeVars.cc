#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BtreeVars -- Used for BTree-specific variables/triggers.
//---------------------------------------------------------------------
//@ Interface-Description
//  To locate data items in the non-cached, B-Tree data area, static
//  data members need be declared within the struct header, and defined
//  within the source file.
//---------------------------------------------------------------------
//@ Rationale
//  Provides a standard way of defining variables for B-Tree monitoring.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/BtreeVars.ccv   25.0.4.0   19 Nov 2013 14:35:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  sah    Date:  14-Aug-97    DCS Number: 2320
//  Project:  Sigma (R8027)
//  Description:
//       Integration baseline.
//
//=====================================================================


#include "BtreeVars.hh"

//@ Usage-Classes
//@ End-Usage

// "btreevars" is relocated and aligned to 16-byte boundary by linker
#pragma  option -NZbtreevars
#pragma  option -Xp


//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

Byte  BtreeVars::CurrAlarmUrgency;
