//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
/// @file BatchExecuter.h
///
/// @brief
/// Class deffinition for BatchExecuter.
#ifndef BATCHEXECUTER_H
#define BATCHEXECUTER_H


#include "EventManager.h"
#include "BatchCommand.h"
#include "Context.h"
#include "Actions.h"
#include "TimeStamp.h"
#include "BatchCommandInfo.h"
#include "SwatBatchUser.h"

#ifndef MAX_BATCH_DEPTH
#define MAX_BATCH_DEPTH 10
#endif

namespace swat{



    /// @class BatchExecuter
    ///
    /// @brief A context manager and executer of coroutines.
    class BatchExecuter{
        private:
            BatchCommandInfo *m_pclBatchCommandInfo;
            BatchCommand     *m_pclBatchCommand;
            unsigned int      m_iBatchLength;
            unsigned int      m_iBatchCurrentIndex;

            // Context related
            unsigned int  m_iCurrentContextIndex;
            int     (* m_contextHandlar[MAX_BATCH_DEPTH] )(BatchExecuter *pCMgr);
            Context    m_Context[MAX_BATCH_DEPTH];

            // Time
            static TimeStamp m_iTime;

            // Null context
            static int nullContext    (BatchExecuter *pCMgr);

            // Context management
            bool pushContext(int (* pContextHandlar )(BatchExecuter *pCMgr));
            bool popContext();

            friend class SkipBufferIndex;
            friend class SetBufferIndex;
            friend class SetSensorBufferValue;
            friend class Mark;
            friend class MarkEvent;
            friend class RepeatN;
            friend class RepeatTime;
            friend class RepeatEvent;
            friend class WaitTime;
            friend class WaitEvent;
            friend class UnsetSensor;
            friend class SetSensor;
            friend class AddSensor;
            friend class DispatchContext;
            friend class SignalEvent;

            friend class SetRawSensor;
            friend class AddRawSensor;
            friend class UnsetRawSensor;

			friend class SimulateGuiTouch;
			friend class SimulateGuiBezelKey;

        public:

            static void updateTime();

            BatchExecuter();


            void init(BatchCommandInfo *pclBatchCommand);

            void reset();

            int run();

            enum{
                STATE_END,
                STATE_CONTINUE,
                STATE_WAIT,
                STATE_CORETURN_TIMEOUT,
                STATE_CORETURN_CONTINUE,
                STATE_RESETALL,
                STATE_INVALID_BATCH_COMMAND,
                STATE_ERROR
            };

    };

}
#endif
