#ifndef ModeTriggerRefs_HH
#define ModeTriggerRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// File: ModeTriggerRefs - extern definitions of mode triggers
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ModeTriggerRefs.hhv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================


// ModeTrigger references

class TimerModeTrigger;
extern TimerModeTrigger& RApneaTrigger;

class ApneaAutoResetTrigger;
extern ApneaAutoResetTrigger& RApneaAutoResetTrigger;

class DisconnectTrigger;
extern DisconnectTrigger& RDisconnectTrigger;

class DiscAutoResetTrigger;
extern DiscAutoResetTrigger& RDiscAutoResetTrigger;

class ImmediateModeTrigger;
extern ImmediateModeTrigger& RSettingChangeModeTrigger;
extern ImmediateModeTrigger& RManualResetTrigger;
extern ImmediateModeTrigger& RStartupTrigger;
extern ImmediateModeTrigger& ROcclusionAutoResetTrigger;

class OcclusionTrigger;
extern OcclusionTrigger& ROcclusionTrigger;

class SvoTrigger;
extern SvoTrigger& RSvoTrigger;

class SvoResetTrigger;
extern SvoResetTrigger& RSvoResetTrigger;

class VentSetupCompleteTrigger;
extern VentSetupCompleteTrigger& RVentSetupCompleteTrigger;

//Trigger Mediator
class ModeTriggerMediator;
extern ModeTriggerMediator& RModeTriggerMediator;

#endif // ModeTriggerRefs_HH 
