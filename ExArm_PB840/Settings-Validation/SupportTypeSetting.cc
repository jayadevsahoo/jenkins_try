#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  SupportTypeSetting - Support Type Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, discrete setting that indicates whether spontaneous
//  breaths shall be pressure-assisted using pressure support ventilation
//  ("PSV"), or if no spontaneous breath assistance is given ("off").
//  This setting is also a Main Control Setting, and, therefore, activates
//  the Transition Rules of the settings that are dependent on this setting's
//  transitions.  This class inherits from 'DiscreteSetting' and provides
//  the specific information needed for the representation of support type.
//  This information includes the set of values that this setting can have
//  (see 'SupportTypeValue.hh'), this setting's activation of Transition
//  Rules (see 'calcTransition()'), and this batch setting's new-patient
//  value.
//
//  Because this is a Main Control Setting, the changing of this setting
//  is done differently than the other settings.  The direct changing of a
//  Main Control Setting does not update the settings that are dependent on
//  its value -- unlike those settings that have dependent settings and
//  override 'calcDependentValues_()'.  The updating of the "dependent"
//  settings of Main Control Settings is done by the Adjusted Context AFTER
//  all of the Main Control Settings have been changed and approved by the
//  operator.  This update is done via the Transition Rules defined for this
//  setting (see 'calcTransition()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SupportTypeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 022   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 021   By: gdc    Date: 24-Jun-2009    SCR Number: 6518
//  Project:  840S
//  Description:
//      Modifications to provide the correct "enabled" values for AC. 
//      Even though spontaneous type is not applicable (not visible)
//      during AC, the control algorithms do not allow for a 
//      setting of PAV or VS when in AC and specifically VC+. 
// 		VS and PAV are now "disabled" and the default PS setting 
// 		applied when mode is set to AC.
//
//  Revision: 020   By: gdc    Date: 27-Apr-2009    SCR Number: 6489
//  Project:  840S
//  Description:
//      Modifications to provide for transitioning of tube 
//		I.D. to new patient value when spontaneous type is changed to 
//		PAV with an incompatible tube I.D.. This includes changes to 
//		the user interface to verify transitioned value with flashing
//		verify arrow icon. Change better supports verification icon
//		used for tube type and I.D. as well as humidification type and
//		volume.
//
//  Revision: 019   By: gdc    Date: 11-Feb-2009    SCR Number: 6469
//  Project:  840S
//  Description:
//      getNewPatientValue to return PS when VC+ option not enabled.
//
//  Revision: 018   By: gdc    Date: 31-Dec-2008    SCR Number: 6177
//  Project:  840S
//  Description:
//      Restructured class to correctly revert to the user-selected value 
// 		when an "observed" setting has forced a change to the value of
// 		this setting. Changed getNewPatient method to provide default value 
// 		for	this setting based on the settings it depends upon. Implemented
// 		derived method setAdjustedValue to save the user-selected value for
// 		so it can be used by valueUpdate in handling changes to higher level
// 		settings such as vent-type.
//
//  Revision: 017   By: rhj    Date: 10-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation related changes.
//
//  Revision: 016   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 015   By: gdc    Date: 11-Aug-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support changing IBW while ventilating.
//
//  Revision: 014  By: gdc    Date: 24-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Reworked per code review.  
//   
//  Revision: 013  By: gdc    Date: 20-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      NIV mods. Added SRS requirement numbers.  
//
//  Revision: 012  By: gdc    Date: 18-Feb-2005    SCR Number: 6144
//  Project:  NIV1
//  Description:
//      Changes to support NIV.
//   
//  Revision: 011  By: emccoy Date: 27-Jul-2004    SCR Number: 6137
//  Project:  PAV3
//  Description:
//      Added support to remove lowSpontTidalVolume alarm during PAV
//   
//  Revision: 010   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added new member, 'isLastSpontValuePav_', for supporting
//         the automatic changing of this setting, based on changes to
//         mode (see 'valueUpdate()')
//      *  changed 'calcTransition()' to support 'PA' value
//      *  added override of 'resetState()' to clear new
//         'isLastSpontValuePav_' flag
//      *  added override of 'calcNewValue()' to clear new
//         'isLastSpontValuePav_' flag, when the user changes the setting
//         directly
//
//  Revision: 009  By: sah    Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  changed 'isEnabledValue()' to a public method, to provide
//         support for the new drop-down menus
//      *  added new member, 'isLastSpontValueVsv_', for supporting the
//         automatic changing of this setting, based on changes to mode
//         (see 'valueUpdate()')
//      *  changed 'calcTransition()' to support 'VS' value
//      *  added override of 'resetState()' to clear new
//         'isLastSpontValueVsv_' flag
//      *  added override of 'calcNewValue()' to clear new
//         'isLastSpontValueVsv_' flag, when the user changes the setting
//         directly
//
//  Revision: 008   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 007   By: sah    Date: 19-Jan-2000    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  now 'TC' is not allowed when circuit type is 'NEONATAL'
//
//  Revision: 006   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  modified 'calcTransition()' to support the new 'ATC' value
//	*  now an observer of mode setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden from
//	   observer base class
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  added new 'isEnabledValue_()' method to determine whether new
//	   'ATC' value is allowed, or not
//
//  Revision: 004   By: dosman    Date:  07-Apr-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Fixed incorrect default value caused by getNewPatientValue()
//	returning SettingValue: casted it to DiscreteValue
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  28-May-1998    DR Number: 5079
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.13.1.0) into Rev "BiLevel" (1.13.2.0)
//	Modified this setting's new-patient value from 'OFF' to 'PSV'.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//=====================================================================

#include "SupportTypeSetting.hh"
#include "ModeValue.hh"
#include "SupportTypeValue.hh"
#include "PatientCctTypeValue.hh"
#include "VentTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
//@ End-Usage

//@ Code...

static const DiscreteParameterItem ParameterLookup_[] = 
{
	{"NONE", SupportTypeValue::OFF_SUPPORT_TYPE},
	{"PS", SupportTypeValue::PSV_SUPPORT_TYPE},
	{"PA", SupportTypeValue::PAV_SUPPORT_TYPE},
	{"TC", SupportTypeValue::ATC_SUPPORT_TYPE},
	{"VS", SupportTypeValue::VSV_SUPPORT_TYPE},
	{NULL, NULL}
};


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  SupportTypeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02248] -- setting range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SupportTypeSetting::SupportTypeSetting(void)
: BatchDiscreteSetting(SettingId::SUPPORT_TYPE,
					   Setting::NULL_DEPENDENT_ARRAY_,
					   SupportTypeValue::TOTAL_SUPPORT_TYPES),
isChangeForced_(FALSE),
operatorSelectedValue_(NO_SELECTION_)
{
	setLookupTable(ParameterLookup_);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~SupportTypeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SupportTypeSetting::~SupportTypeSetting(void)
{
	CALL_TRACE("~SupportTypeSetting(void)");
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  setAdjustedValue(newAdjustedValue)  [virtual]
//
//@ Interface-Description
//  This virtual method is called to set the adjusted value of this 
//  setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the Setting base class method to actually store the new value
//  in the adjusted context. 
//---------------------------------------------------------------------
//@ PreCondition
//  isEnabledValue(DiscreteValue(newValue))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SupportTypeSetting::setAdjustedValue(const SettingValue& newValue)
{
	AUX_CLASS_PRE_CONDITION(isEnabledValue(DiscreteValue(newValue)), DiscreteValue(newValue));

	// call the base class to set the value into the adjusted context
	Setting::setAdjustedValue(newValue);

	if ( !isChangeForced_ )
	{
		operatorSelectedValue_ = newValue;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01080] -- main control setting applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id SupportTypeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id applicability;
	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

	DiscreteValue  modeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			modeValue = pMode->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			modeValue = pMode->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	if ( modeValue == ModeValue::SIMV_MODE_VALUE    ||
		 modeValue == ModeValue::BILEVEL_MODE_VALUE  ||
		 modeValue == ModeValue::SPONT_MODE_VALUE )
	{
		applicability = Applicability::CHANGEABLE;

		if ( qualifierId == Notification::ADJUSTED && 
			 ContextMgr::GetAdjustedContext().getAllSettingValuesKnown() &&
			 SettingsMgr::GetSettingPtr(SettingId::IBW)->isChanged() )
		{
			applicability = Applicability::VIEWABLE;
		}
	}
	else if ( modeValue == ModeValue::CPAP_MODE_VALUE )
	{
		applicability = Applicability::VIEWABLE;
	}
	else
	{
		applicability = Applicability::INAPPLICABLE;
	}

	return applicability;
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isEnabledValue()  [const, virtual]
//
//@ Interface-Description
//  Is this setting's value given by 'value', a currently-enabled value?
//---------------------------------------------------------------------
//@ Implementation-Description
// $[02248]
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean SupportTypeSetting::isEnabledValue(const DiscreteValue value) const
{
	Boolean  isEnabled;

	const DiscreteValue CCT_TYPE =  
	SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE)->getAdjustedValue();

	const DiscreteValue VENT_TYPE = 
	SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();

	const DiscreteValue MODE_VALUE = 
	SettingsMgr::GetSettingPtr(SettingId::MODE)->getAdjustedValue();

	switch ( value )
	{
		case SupportTypeValue::OFF_SUPPORT_TYPE :
			// OFF is ALWAYS enabled...
			isEnabled = TRUE;
			break;

		case SupportTypeValue::PSV_SUPPORT_TYPE :
			isEnabled = (MODE_VALUE != ModeValue::CPAP_MODE_VALUE);
			break;

		case SupportTypeValue::ATC_SUPPORT_TYPE :	  // $[TI2]
			// this is an optional value...
			if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::ATC) )
			{  // $[TI2.1] -- 'ATC' option is active...
				switch ( CCT_TYPE )
				{
					case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
					case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2.1.1]
						{
							const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

							// $[TI2.1.1.1] (TRUE)  $[TI2.1.1.2] (FALSE)...
							isEnabled = (BoundedValue(pIbw->getAdjustedValue()).value >= 7.0f)
										&& (VENT_TYPE == VentTypeValue::INVASIVE_VENT_TYPE);
						}
						break;
					case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI2.1.2]
						// 'ATC' not allowed for neonatal circuits...
						isEnabled = FALSE;
						break;
					default :
						// unexpected circuit type...
						AUX_CLASS_ASSERTION_FAILURE(CCT_TYPE);
						break;
				};
			}
			else
			{  // $[TI2.2] -- 'ATC' option is NOT active...
				isEnabled = FALSE;
			}
			break;

		case SupportTypeValue::VSV_SUPPORT_TYPE :	  // $[TI3]
			// this is an optional value...
			if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::VTPC) )
			{  // $[TI3.1] -- 'VTPC' option is active...
				switch ( MODE_VALUE )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SIMV_MODE_VALUE :
					case ModeValue::BILEVEL_MODE_VALUE :	  // $[TI3.1.1]
						// 'VS' not allowed for these mode values...
						isEnabled = FALSE;
						break;
					case ModeValue::SPONT_MODE_VALUE :		  // $[TI3.1.2]
					case ModeValue::CPAP_MODE_VALUE :
						// 'VS' is allowed for SPONT mode non-NIV...
						isEnabled = (VENT_TYPE == VentTypeValue::INVASIVE_VENT_TYPE);
						break;
					default :
						// unexpected mode...
						AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
						break;
				};
			}
			else
			{  // $[TI3.2] -- 'VTPC' option is NOT active...
				isEnabled = FALSE;
			}
			break;

		case SupportTypeValue::PAV_SUPPORT_TYPE :	  // $[TI4]
			// $[LC02017] \a\ PAV is selectable when...
			// this is an optional value...
			if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::PAV) )
			{  // $[TI4.1] -- 'PAV' option is active...
				switch ( MODE_VALUE )
				{
					case ModeValue::AC_MODE_VALUE :
					case ModeValue::SIMV_MODE_VALUE :
					case ModeValue::BILEVEL_MODE_VALUE :	  // $[TI4.1.1]
						// 'PAV' not allowed for these mode values...
						isEnabled = FALSE;
						break;
					case ModeValue::SPONT_MODE_VALUE :		  // $[TI4.1.2]
					case ModeValue::CPAP_MODE_VALUE :
						{
							switch ( CCT_TYPE )
							{
								case PatientCctTypeValue::ADULT_CIRCUIT : // $[TI4.1.2.1]
									{
										const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

										// $[TI4.1.2.1.1] (TRUE)  $[TI4.1.2.1.2] (FALSE)...
										isEnabled =
										(BoundedValue(pIbw->getAdjustedValue()).value >= 25.0f)
										&& (VENT_TYPE == VentTypeValue::INVASIVE_VENT_TYPE);
									}
									break;
								case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
								case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI4.1.2.2]
									// 'PAV' not allowed for either pediatric or neonatal circuits...
									isEnabled = FALSE;
									break;
								default :
									// unexpected circuit type...
									AUX_CLASS_ASSERTION_FAILURE(CCT_TYPE);
									break;
							};
						}
						break;
					default :
						// unexpected mode...
						AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
						break;
				};
			}
			else
			{  // $[TI4.2] -- 'PAV' option is NOT active...
				isEnabled = FALSE;
			}
			break;

		case SupportTypeValue::TOTAL_SUPPORT_TYPES :
		default :
			// unexpected support type value...
			AUX_CLASS_ASSERTION_FAILURE(value);
			break;
	}

	return(isEnabled);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  Calculate, and initialize to, the new discrete value.  The value of
//  'knobDelta' is only used in so far as the "direction" of change; the
//  value, itself, is ignored a delta of '1' is applied in the direction
//  of change.
//
//  This does some specific work for this class, then forwards on to base
//  class's implementation.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus& SupportTypeSetting::calcNewValue(const Int16 knobDelta)
{
	CALL_TRACE("calcNewValue(knobDelta)");

	// forward to base class...
	return(DiscreteSetting::calcNewValue(knobDelta));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcTransition(toValue, fromValue)  [virtual]
//
//@ Interface-Description
//  The purpose of this method is to inform the settings effected
//  by a state transition of this setting. So they can adjust
//  their value(s) appropriately.
//
//  This method deals with state transitions where-as dependent settings
//  calculations typically deal with bounded settings and incremental
//  changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Inform the settings effected by a state transition of this setting.
//  So they can adjust their value(s) appropriately.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SupportTypeSetting::calcTransition(const DiscreteValue toValue,
										const DiscreteValue fromValue)
{
	CALL_TRACE("calcTransition(toValue, fromValue)");

	//-------------------------------------------------------------------
	//  Handle all of the "to" transitions...
	//-------------------------------------------------------------------

	// $[LC02009]
	SettingsMgr::GetSettingPtr(SettingId::LEAK_COMP_ENABLED)->
	acceptTransition(getId(), toValue, fromValue);
	SettingsMgr::GetSettingPtr(SettingId::DISCO_SENS)->
	acceptTransition(getId(), toValue, fromValue);

	switch ( toValue )
	{
		case SupportTypeValue::OFF_SUPPORT_TYPE : // $[TI1]
			// do nothing...
			break;
		case SupportTypeValue::PSV_SUPPORT_TYPE : // $[TI2]
			// $[02027] -- transition to PS...
			SettingsMgr::GetSettingPtr(SettingId::PRESS_SUPP_LEVEL)->
			acceptTransition(getId(), toValue, fromValue);
			break;
		case SupportTypeValue::ATC_SUPPORT_TYPE : // $[TI3]
			// $[PA02000] -- transition to TC or PA...
			SettingsMgr::GetSettingPtr(SettingId::PERCENT_SUPPORT)->
			acceptTransition(getId(), toValue, fromValue);
			break;
		case SupportTypeValue::VSV_SUPPORT_TYPE : // $[TI4]
			// $[VC02007] -- transition to VS...
			SettingsMgr::GetSettingPtr(SettingId::VOLUME_SUPPORT)->
			acceptTransition(getId(), toValue, fromValue);
			break;
		case SupportTypeValue::PAV_SUPPORT_TYPE : // $[TI5]
			// $[PA02000] -- transition to TC or PA...
			// $[PA02001] -- transition to PA...
			SettingsMgr::GetSettingPtr(SettingId::PERCENT_SUPPORT)->
			acceptTransition(getId(), toValue, fromValue);
			SettingsMgr::GetSettingPtr(SettingId::EXP_SENS)->
			acceptTransition(getId(), toValue, fromValue);
			SettingsMgr::GetSettingPtr(SettingId::LOW_EXH_SPONT_TIDAL_VOL)->
			acceptTransition(getId(), toValue, fromValue);  
			SettingsMgr::GetSettingPtr(SettingId::TUBE_ID)->
			acceptTransition(getId(), toValue, fromValue);  
			break;
		default :
			// unexpected "to" support type value...
			AUX_CLASS_ASSERTION_FAILURE(toValue);
			break;
	};

	//-------------------------------------------------------------------
	//  Handle all of the "from" transitions...
	//-------------------------------------------------------------------

	switch ( fromValue )
	{
		case SupportTypeValue::OFF_SUPPORT_TYPE :
		case SupportTypeValue::PSV_SUPPORT_TYPE :
		case SupportTypeValue::ATC_SUPPORT_TYPE :
		case SupportTypeValue::VSV_SUPPORT_TYPE : // $[TI6]
			// do nothing -- no special action needed for these "from" values...
			break;
		case SupportTypeValue::PAV_SUPPORT_TYPE : // $[TI7]
			// $[PA02001] -- transition from PA...
			SettingsMgr::GetSettingPtr(SettingId::EXP_SENS)->
			acceptTransition(getId(), toValue, fromValue);
			break;
		default :
			// unexpected "from" support type value...
			AUX_CLASS_ASSERTION_FAILURE(fromValue);
			break;
	};
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Returns this setting's new-patient (default) value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a default setting value for support type based on the 
//  current vent-type and mode.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue SupportTypeSetting::getNewPatientValue(void) const
{
	// $[02249] The setting's new-patient value ...
	DiscreteValue newPatientValue = SupportTypeValue::PSV_SUPPORT_TYPE;

	const DiscreteValue VENT_TYPE = 
	SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE)->getAdjustedValue();
	const DiscreteValue MODE = 
	SettingsMgr::GetSettingPtr(SettingId::MODE)->getAdjustedValue();

	if ( VENT_TYPE == VentTypeValue::NIV_VENT_TYPE )
	{
		if ( MODE == ModeValue::CPAP_MODE_VALUE )
		{
			newPatientValue = SupportTypeValue::OFF_SUPPORT_TYPE;
		}
	}

	return newPatientValue;
}

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  resetState()  [virtual]
//
//@ Interface-Description
//  Reset the state of this setting, preparing it for changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SupportTypeSetting::resetState(void)
{
	CALL_TRACE("resetState()");

	// forward up to base class...
	Setting::resetState();

	operatorSelectedValue_ = getAcceptedValue();
}	// $[TI1]


//==================== M E T H O D   D E S C R I P T I O N =============
//@ Method:  valueUpdate(qualifierId, pSubject)  [virtual]
//
//@ Interface-Description
//  Respond to vent type, mode and IBW value changes by updating the 
//  support type setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02025]/a/ -- from A/C, spontaneous type shall retain its current value...
//  $[02026]/h/ -- from SPONT while 'PA' or 'VS', change to 'PS'...
//  $[NI02010]\c\ -- from Invasive to NIV, if not NONE or PS, change to PS
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SupportTypeSetting::valueUpdate(const Notification::ChangeQualifier qualifierId,
									 const SettingSubject*               pSubject)
{
	if ( qualifierId == Notification::ADJUSTED )
	{
		const SettingId::SettingIdType SETTING_ID = pSubject->getId();
		if ( SETTING_ID == SettingId::MODE || SETTING_ID == SettingId::VENT_TYPE )
		{
			const DiscreteValue CURRENT_VALUE = getAdjustedValue();

			isChangeForced_ = TRUE;
			// attempt to revert to the operator selection first if enabled
			if ( operatorSelectedValue_ != NO_SELECTION_ && isEnabledValue(operatorSelectedValue_) )
			{
				if ( CURRENT_VALUE != operatorSelectedValue_ )
				{
					setAdjustedValue(operatorSelectedValue_);
				}
			}
			else
			{
				setAdjustedValue(getNewPatientValue());
			}
			// else - no change required
			isChangeForced_ = FALSE;

			updateApplicability(qualifierId);
		}
		else if ( SETTING_ID == SettingId::IBW )
		{
			updateApplicability(qualifierId);
		}
		else
		{
			AUX_CLASS_ASSERTION_FAILURE(SETTING_ID);
		}
	}  // -- don't care about accepted value changes...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doRetainAttachment(pSubject)  [virtual, const]
//
//@ Interface-Description
//  When task-level, state changes occur, all subjects are notified
//  to detach from all observers.  This is done because the GUI-Apps
//  observers may be overwritten by new class instances, without any
//  explicit destructor/detachment calls.
//
//  This method is defined here to indicate to any of this instance's
//  subjects, not to detach this observer.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean SupportTypeSetting::doRetainAttachment(const SettingSubject*) const
{
	CALL_TRACE("doRetainAttachment(pSubject)");

	return(TRUE);
}  // $[TI1]


//==================== M E T H O D   D E S C R I P T I O N ============
//@ Method:  settingObserverInit()  [virtual]
//
//@ Interface-Description
//  This setting monitors the value of Mode and IBW, therefore
//  this virtual method is overridden to provide a point during
//  initialization to attach to that (subject) setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void SupportTypeSetting::settingObserverInit(void)
{
	CALL_TRACE("settingObserverInit()");

	// monitor changes in mode's value...
	attachToSubject_(SettingId::MODE, Notification::VALUE_CHANGED);
	// monitor changes in vent type...
	attachToSubject_(SettingId::VENT_TYPE, Notification::VALUE_CHANGED);
	attachToSubject_(SettingId::IBW, Notification::VALUE_CHANGED);
}  // $[TI1]


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SupportTypeSetting::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							SUPPORT_TYPE_SETTING, lineNumber, pFileName,
							pPredicate);
}
