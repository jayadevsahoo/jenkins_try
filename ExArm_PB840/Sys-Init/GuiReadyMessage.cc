#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: GuiReadyMessage - GUI Node Ready Message.
//---------------------------------------------------------------------
//@ Interface-Description
//  The GuiReadyMessage is a TaskControlMessage created by GUI Task-
//  Control and sent to BD Task-Control indicating the GUI is ready
//  in the specified SigmaState.  The message contains the 
//  processor's current SigmaState and other state information.  It
//  contains a PostAgent object that encapsulates data from
//  GUI POST NOVRAM.  This message class uses the AppContext 
//  callback mechanism to dispatch the message to the receiving 
//  task's callback function.
//---------------------------------------------------------------------
//@ Rationale
//  The GuiReadyMessage encapsulates GUI state information sent by
//  the GUI to the BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is a derived TaskControlMessage.  The base class new and
//  delete operators override the library operators to allocate memory
//  for the message in the global BufferPool.  The copy constructor is
//  defined so the receiving task can reconstruct the message in its
//  own address space with valid virtual pointers.  The PostAgent is
//  instantiated in the message at construction.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiReadyMessage.ccv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 29-SEP-1997 DR Number: 2513
//      Project:   Sigma   (R8027)
//      Description:
//         Added GUI FLASH serial number for DataKey restore operation.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "GuiReadyMessage.hh"
#include <string.h>

//@ Usage-Classes
#include "AppContext.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


#if defined(SIGMA_GUI_CPU) || defined(SIGMA_UNIT_TEST) || defined(LOCAL_SIM)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiReadyMessage [constructor]
//
//@ Interface-Description
//  The GuiReadyMessage constructor uses the state and service-required
//  arguments to intialize the contents of the message.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class TaskControlMessage constructor with the
//  classId for this message.  Initializes its private data members.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiReadyMessage::GuiReadyMessage(
    enum SigmaState state,
    Boolean service_required)

    :  TaskControlMessage(Sys_Init::ID_GUIREADYMESSAGE)
      ,state_(state)
      ,serviceRequired_(service_required)
      ,postAgent_()
	  // TODO E600 determine how to read the serial nubmer
      //,serialNumber_( (char*)FLASH_SERIAL_NO_ADDR )
	  ,serialNumber_( (char*)"ABCDE600")
{
    CALL_TRACE("GuiReadyMessage(Int32, enum SigmaState, Boolean)");
    // $[TI1]
}
#endif // SIGMA_GUI_CPU || SIGMA_UNIT_TEST || LOCAL_SIM


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiReadyMessage [copy constructor]
//
//@ Interface-Description
//  Copy constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the base class TaskControlMessage constructor from the 
//  referenced GuiReadyMessage.  Initializes internal
//  data members directly from contents of referenced message.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

 
GuiReadyMessage::GuiReadyMessage(const GuiReadyMessage& rMessage) :
     TaskControlMessage(rMessage)
    ,state_(rMessage.state_)
    ,serviceRequired_(rMessage.serviceRequired_)
    ,postAgent_(rMessage.postAgent_)
    ,serialNumber_( rMessage.serialNumber_ )
{
    CALL_TRACE("GuiReadyMessage(const GuiReadyMessage&)");
    // $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiReadyMessage [destructor]
//
//@ Interface-Description
//  GuiReadyMessage destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiReadyMessage::~GuiReadyMessage()
{
    CALL_TRACE("~GuiReadyMessage()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class with its state qualifier.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name
//  and state qualifier.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================
 
const char*
GuiReadyMessage::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    switch ( state_ )
    {
        case (STATE_INOP):          // $[TI1]
            return "GuiReady-Inop";
        case (STATE_ONLINE):        // $[TI2]
            return "GuiReady-Online";
        case (STATE_SERVICE):       // $[TI3]
            return "GuiReady-Service";
        case (STATE_SST):           // $[TI4]
            return "GuiReady-SST";
        case (STATE_START):         // $[TI5]
            return "GuiReady-Start";
        case (STATE_TIMEOUT):       // $[TI6]
            return "GuiReady-Timeout";
        case (STATE_INIT):          // $[TI7]
            return "GuiReady-Init";
        default:                    // $[TI8]
            break;
    };
    return "GuiReady-Invalid";
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dispatch
//
//@ Interface-Description
//  The method dispatches this GuiReadyMessage to the callback
//  function registered with the specified AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the AppContext dispatch method with a reference to this
//  GuiReadyMessage.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
GuiReadyMessage::dispatch(const AppContext& context) const
{
    CALL_TRACE("dispatch(void)");
    context.dispatch(*this);
    // $[TI1]
}


 

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
GuiReadyMessage::SoftFault(const SoftFaultID  softFaultID,
                            const Uint32       lineNumber,
                            const char*        pFileName,
                            const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_GUIREADYMESSAGE, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

