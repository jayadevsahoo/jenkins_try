#ifndef ISETTINGSCATPURE
#define ISETTINGSCATPURE

class ISettingsCapture{
    public:
    virtual bool addCallback(int (*func)(void *,char *, int), void *obj    ) = 0;
    virtual bool removeCallback(int (*func)(void *,char *, int), void *obj ) = 0;

    virtual ~ISettingsCapture() = 0;

    //static ISettingsCapture *getInstance(char *dev, int (*func)(void *,char *, int)=0, void *obj=0 );
    virtual void destroy() = 0;
};


typedef ISettingsCapture * (ISettingsCapture_F)(char *dev, int (*func)(void *, char *, int), void *obj);

extern "C" __declspec(dllexport) ISettingsCapture * __cdecl 
    getSettingsCaptureInstance(char *dev, int (*func)(void *, char *, int), void *obj );

#endif