#ifndef WaveformIntervalIter_HH
#define WaveformIntervalIter_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: WaveformIntervalIter - An iterator class designed not to
// iterate through a full WaveformDataBuffer, but through a specified
// interval of data in WaveformDataBuffer.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformIntervalIter.hhv   25.0.4.0   19 Nov 2013 14:08:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "PatientDataId.hh"
#include "WaveformDataBuffer.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class WaveformIntervalIter
{
public:
	WaveformIntervalIter(void);
	~WaveformIntervalIter(void);

	void attachToBuffer(WaveformDataBuffer *pWaveformDataBuffer);

	inline void reset(void);
	inline Boolean hasData( void );

	void beginInterval(void);
	inline void endInterval(void);
	SigmaStatus setToNextInterval(void);
	void beginNextInterval(void);
	SigmaStatus beginIntervalAtOffset(Int32 offsetMSecs);
	SigmaStatus endIntervalAtOffset(Int32 offsetMSecs);

	SigmaStatus goFirst(void);
	SigmaStatus goLast(void);
	SigmaStatus goNext(void);
	SigmaStatus goPrev(void);

	inline Uint32 getLength(void);

	Real32 getBoundedValue(PatientDataId::PatientItemId patientDataId);
	Int32 getDiscreteValue(PatientDataId::PatientItemId patientDataId);

	inline void sync(const WaveformIntervalIter &otherIter);
	inline void syncFirstWithCurrent(const WaveformIntervalIter &otherIter);
	inline void syncLast(const WaveformIntervalIter &otherIter);
	inline void syncLastWithCurrent(const WaveformIntervalIter &otherIter);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	WaveformIntervalIter(const WaveformIntervalIter&);	// not implemented...
	void operator=(const WaveformIntervalIter&);		// not implemented...

	//@ Data-Member: pWaveformDataBuffer_
	// The data buffer which this class iterates through.
	WaveformDataBuffer *pWaveformDataBuffer_;

	//@ Data-Member: firstSample_
	// The start of the iteration interval.
	Int32 firstSample_;

	//@ Data-Member: lastSample_
	// The end of the iteration interval.
	Int32 lastSample_;

	//@ Data-Member: currSample_
	// The current interation point in the specified interval.
	Int32 currSample_;
};

// Inlined methods
#include "WaveformIntervalIter.in"

#endif // WaveformIntervalIter_HH 
