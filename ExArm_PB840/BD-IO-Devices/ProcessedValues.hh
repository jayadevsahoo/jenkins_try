
#ifndef ProcessedValues_HH
#define ProcessedValues_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ProcessedValues - Filtering and slope calculation.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ProcessedValues.hhv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added setAlpha() method.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

//@ End-Usage

class ProcessedValues {
  public:
    ProcessedValues( const Real32 alpha) ;
	ProcessedValues( void) {}
    ~ProcessedValues( void) ;

    void updateValues( const Real32 currValue) ;
    inline void initValues( const Real32 filtered, const Real32 slope,
                            const Real32 filteredSlope, const Real32 prevValue) ;

    inline Real32 getFilteredValue( void) const ;
    inline Real32 getSlopeValue( void) const ;
    inline Real32 getFilteredSlopeValue( void) const ;
    inline void setAlpha( const Real32 alpha) ;
	inline Real32 updateAlphaFilterValue( const Real32 newValue) ;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL) ;
 
  protected:

  private:
    ProcessedValues( const ProcessedValues&) ; // not implemented...
    void   operator=( const ProcessedValues&) ;// not implemented...

    //@ Data-Member:  prevValue_
    // The previous value is saved for slope calculations and
    // for initializing the filtered value.
    Real32  prevValue_ ;

    //@ Data-Member:  slopeValue_
    // The calculated slope value.
    Real32 slopeValue_ ;

    //@ Data-Member:  filteredValue_
    // The calculated filtered value.
    Real32  filteredValue_ ;

    //@ Data-Member:  filteredSlopeValue_
    // The calculated filtered slope value.
    Real32  filteredSlopeValue_ ;

    //@ Data-Member:  alpha_
    // The alpha applied for filtering.
    Real32  alpha_ ;
};


// Inlined methods...
#include "ProcessedValues.in"


#endif // ProcessedValues_HH 


