
#ifndef ModeValue_HH
#define ModeValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ModeValue - Values of the Mode Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ModeValue.hhv   25.0.4.0   19 Nov 2013 14:27:30   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 003   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  re-ordered values to separate standard values from optional
//         values; now matches order in SRS
//
//  Revision: 002   By: dosman    Date:  04-Dec-1996    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Adding new mode to the list of possible modes settable.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct ModeValue
{
  //@ Type:  ModeValueId 
  // All of the possible values of the Mode Setting.
  enum ModeValueId
  {
    // $[02200] -- values of the range of Mode Setting...

    // standard values...
    AC_MODE_VALUE,
    SIMV_MODE_VALUE,
    SPONT_MODE_VALUE,

    // optional values...
    BILEVEL_MODE_VALUE,
	CPAP_MODE_VALUE,

    TOTAL_MODE_VALUES
  };
};


#endif // ModeValue_HH 
