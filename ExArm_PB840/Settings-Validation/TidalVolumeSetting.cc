#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  TidalVolumeSetting - Tidal Volume Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the volume (in
//  milliliters) of gas that is to be delivered to the patient during a
//  mandatory, volume-based breath.  This setting only applies to mandatory
//  breaths.  This class inherits from 'BoundedSetting' and provides the
//  specific information needed for representation of tidal volume.
//  This information includes the interval and range of this setting's
//  values, this setting's response to a Main Control Setting's transition
//  (see 'acceptTransition()'), this batch setting's new-patient value
//  (see 'getNewPatientValue()'), and the contraints and dependent settings
//  of this setting.
//
//  This class provides a static method for calculating tidal volume based
//  on the values of other settings.  This calculation method provides a
//  standard way for all settings (as needed) to calculate a tidal volume
//  value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines a 'calcDependentValues_()' method for updating
//  this setting's dependent settings, and an 'updateConstraints_()' method
//  for updating the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TidalVolumeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 021   By: mnr    Date: 28-Dec-2009     SCR Number: 6437
//  Project:  NEO
//  Description:
//		Resolution transition fixed, lower limit based on Advanced now. 
//	
//  Revision: 020   By: mnr    Date: 23-Nov-2009     SCR Number: 6522
//  Project:  NEO
//  Description:
//		Separate Tidal vol min for SQUARE FLOW PATTERN, resolution 
//		to tenths from 2mL to 5mL.
//
//  Revision: 019   By: mnr    Date: 27-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Enforce lower bound in calcBasedOnSetting_().
//
//  Revision: 018   By: mnr    Date: 21-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Lower min values based on NeoMode Option status.
//
//  Revision: 017   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Updates for lower tidal volume min value based on MAND TYPE.
//
//  Revision: 016   By: gdc    Date: 02-Mar-2009    SCR Number: 6478
//  Project:  840S
//  Description:
//		Initialized highVtiBasedMax resolution using BoundedValue
//		assignment operator. The decrement operation that followed 
//		detected an uninitialized resolution value when development 
//		debugging was turned on.
//
//  Revision: 015   By: gdc    Date: 26-Jan-2009    SCR Number: 6461
//  Project:  840S
//  Description:
//      Implemented #define workaround for compiler bug.
//
//  Revision: 014   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 013 By: scottp  Date:  28-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//	Added Vt/Vti/Vsupp string settings 
//
//  Revision: 012 By: jja     Date:  21-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//	Added Vt/Vti/Vsupp setting interaction/limitations for VC+ and VS
//
//  Revision: 011  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  removed minute volume as a dependent setting, now it
//         automatically tracks changes to this setting
//      *  added support for new 'VC+' mandatory type
//      *  updated SRS mappings
//      *  modified 'calcDependentValues_()' to support 'VC+', and
//         use peak insp flow as a dependent when 'VC+'
//
//  Revision: 010   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//	*  re-wrote 'acceptTransition()' to handle NEONATAL circuit
//	*  modified 'updateConstraints_()' to handle new soft limits
//	*  added 'findSoftMinMaxValue_()' for handling new soft limits
//	*  added new 'getAbsoluteMinValue_()' method to accomodate
//         new, dynamic lower limits
//
//  Revision: 009   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new minute volume setting as a dependent setting
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 008   By: dosman Date:  05-Oct-1998    DR Number: 
//  Project:  BILEVEL
//  Description:
//	Because of the coarser resolution of TidalVolume now, 
//      Tidal Volume was reaching below its lower constraint
//      during acceptTransition when Resp Rate was 98 
//      (whereas before the change in resolution, Tidal Volume
//      only reached below its lower constraint when Resp Rate 
//      was 99). So, acceptTransition() was changed to clip
//      Tidal Volume when Resp Rate is greater than or equal to
//      98.
//
//  Revision: 007   By: dosman Date:  16-Sep-1998    DR Number: BiLevel 144
//  Project:  BILEVEL
//  Description:
//      The intermediate value of inspiratory time computed in
//      calcBasedOnSetting_ was changed to be warped in the same
//      direction as the direction the "setting being calculated".
//      Also, special processing was added in BoundedSetting::calcNewValue()
//
//  Revision: 006   By: dosman Date: 02 Jun 1998    DR Number: 66
//  Project:  BILEVEL
//  Description:
//	Same change as in ApneaTidalVolume.cc See Revision: 004 in header.
//
//  Revision: 005   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 004   By: syw    Date: 06 Jan 1997    DR Number:
//  Project:  BILEVEL
//  Description:
//	BiLevel Baseline.  Added BiLevel handle.
//
//  Revision: 003   By: sah    Date:  28-May-1998    DR Number: 5058
//  Project:  COLOR
//  Description:
//	--- Merged Rev "Color" (1.33.1.0) into Rev "BiLevel" (1.33.2.0)
//	Modified range/resolution for improving the user's ability to
//	accurately change this setting's value.
//
//  Revision: 002   By: sah    Date: 23 Dec 1996    DR Number: 1624
//  Project: Sigma (R8027)
//  Description:
//	Incorporated new PC-to-VC formulas.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "TidalVolumeSetting.hh"
#include "SettingConstants.hh"
#include "FlowPatternValue.hh"
#include "ModeValue.hh"
#include "MandTypeValue.hh"
#include "SupportTypeValue.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "RespRateSetting.hh"
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
#include "SettingContextHandle.hh"
#include "SoftwareOptions.hh"
#include "InspTimeSetting.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// This array is set up dynamically via this class's 'calcDependentValues_()',
// because all dependents are not applicable in 'SPONT' mode...
static SettingId::SettingIdType  ArrDependentSettingIds_[] =
{
	SettingId::IE_RATIO,
	SettingId::INSP_TIME,
	SettingId::EXP_TIME,

	SettingId::NULL_SETTING_ID
};


//  $[02251] The setting's range ...
//  $[02253] The setting's resolution ...

//  use #define values to initialize static const struct to workaround compiler bug
static const BoundedInterval  LAST_NODE_ =
{
	DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS,
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  NODE3_ =
{
	DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC, // change-point...5.0f
	0.10f,		// resolution...
	TENTHS,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE2_ =
{
	100.0f,		// change-point...
	1.0f,		// resolution...
	ONES,		// precision...
	&::NODE3_
};
static const BoundedInterval  NODE1_ =
{
	400.0f,		// change-point...
	5.0f,		// resolution...
	ONES,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	2500.0f,		// absolute maximum...
	10.0f,		// resolution...
	TENS,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: TidalVolumeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, the value interval
//  is initialized by this method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TidalVolumeSetting::TidalVolumeSetting(void)
: BatchBoundedSetting(SettingId::TIDAL_VOLUME,
					  ::ArrDependentSettingIds_,
					  &::INTERVAL_LIST_,
					  TIDAL_VOL_MAX_ID,		// maxConstraintId...
					  TIDAL_VOL_MIN_ID)		// minConstraintId...
{
	CALL_TRACE("TidalVolumeSetting()");

	// register this setting's soft bounds...
	registerSoftBound_(::TIDAL_VOL_SOFT_MIN_BASED_IBW_ID, Setting::LOWER);
	registerSoftBound_(::TIDAL_VOL_SOFT_MAX_BASED_IBW_ID, Setting::UPPER);
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~TidalVolumeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TidalVolumeSetting::~TidalVolumeSetting(void)
{
	CALL_TRACE("~TidalVolumeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	TidalVolumeSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	DiscreteValue  mandTypeValue;

	switch ( qualifierId )
	{
		case Notification::ACCEPTED :	  // $[TI1]
			mandTypeValue = pMandType->getAcceptedValue();
			break;
		case Notification::ADJUSTED :	  // $[TI2]
			mandTypeValue = pMandType->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifierId);
			break;
	}

	Applicability::Id  applicability;

	switch ( mandTypeValue )
	{
		case MandTypeValue::PCV_MAND_TYPE :	  // $[TI4]
			applicability = Applicability::INAPPLICABLE;
			break;
		case MandTypeValue::VCV_MAND_TYPE :
		case MandTypeValue::VCP_MAND_TYPE :	  // $[TI3]
			applicability = Applicability::CHANGEABLE;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(mandTypeValue);
			break;
	}

	return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this bounded setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02252] -- new-patient value of tidal volume...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	TidalVolumeSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	// Adjusted Context contains the new-patient value of IBW)...
	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

	BoundedValue  newPatient;

	// calculate the IBW-based tidal volume new-patient value...
	newPatient.value = MAX_VALUE(ABSOLUTE_MIN,					// $[TI1]
								 (IBW_VALUE * SettingConstants::NP_TIDAL_VOL_IBW_SCALE)); // $[TI2]

	// warp the calculated value to a "click" boundary...
	getBoundedRange().warpValue(newPatient);

	return(newPatient);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The setting is only interested in the transition of mandatory type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The analysis that documents the definition of the "regions", and
//  their corresponding formulas, can be found in the Design History
//  File.  The document titled "Summary Analysis of Mandatory Type's
//  PC-to-VC Transition Rules" was submitted to the DHF in December 1996.
//
//  $[02022]\c\   -- from-PC-to-VC transition rules
//  $[VC02006]\d\ -- from-VC-to-VC+ transition rules
//  $[VC02006]\g\ -- from-PC-to-VC+ transition rules
//  $[NE02000]\a\ -- to VC/VC+ (non-SPONT / NEONATAL circuit)...
//  $[NE02000]\c\ -- to VC/VC+ (SPONT / NEONATAL circuit)...
//  $[NE02001]\a\ -- to VC/VC+ (non-SPONT / PEDIATRIC circuit)...
//  $[NE02001]\c\ -- to VC/VC+ (SPONT / PEDIATRIC circuit)...
//  $[NE02002]\a\ -- to VC/VC+ (non-SPONT / ADULT circuit)...
//  $[NE02002]\c\ -- to VC/VC+ (SPONT / ADULT circuit)...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	TidalVolumeSetting::acceptTransition(const SettingId::SettingIdType settingId,
										 const DiscreteValue  newValue,
										 const DiscreteValue  currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	//===================================================================
	// mandatory type transition to VC or VC+...
	//===================================================================

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const Setting*  pIbw  = SettingsMgr::GetSettingPtr(SettingId::IBW);
	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	const DiscreteValue  MODE_VALUE = pMode->getAdjustedValue();
	const Real32         IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;
	const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

	BoundedValue  tidalVolume;

	if ( newValue == MandTypeValue::VCP_MAND_TYPE && currValue == MandTypeValue::VCV_MAND_TYPE )
	{
		// set to current value...
		tidalVolume.value = BoundedValue(getAdjustedValue()).value;
	}
	else
	{
		// set to new-patient based value...
		tidalVolume.value = (SettingConstants::NP_TIDAL_VOL_IBW_SCALE * IBW_VALUE);
	}

	switch ( MODE_VALUE )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE : // $[TI1]
			{
				const Setting*  pRespRate =
					SettingsMgr::GetSettingPtr(SettingId::RESP_RATE);
				const Setting*  pPatientCctType =
					SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

				const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();
				const Real32  RESP_RATE_VALUE =
					BoundedValue(pRespRate->getAdjustedValue()).value;

				Real32  minuteVolumeFactorLimit;

				// the minute volume factor limit is the respiratory rate value, above
				// which a scaled factor is used against tidal volume's new-patient value;
				// the purpose of the minute volume factor is to keep a consistent minute
				// volume with respect to the new-patient values of tidal volume and
				// respiratory rate...
				switch ( PATIENT_CCT_TYPE_VALUE )
				{
					case PatientCctTypeValue::NEONATAL_CIRCUIT :  // $[TI1.1]
						minuteVolumeFactorLimit = SettingConstants::NP_RESP_RATE_NEO_VALUE;
						break;
					case PatientCctTypeValue::PEDIATRIC_CIRCUIT : // $[TI1.2]
						minuteVolumeFactorLimit = SettingConstants::NP_RESP_RATE_PED_VALUE;
						break;
					case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI1.3]
						minuteVolumeFactorLimit = SettingConstants::NP_RESP_RATE_ADULT_VALUE;
						break;
					default :
						// unexpected 'PATIENT_CCT_TYPE_VALUE' value...
						AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
						break;
				};

				if ( RESP_RATE_VALUE > minuteVolumeFactorLimit )
				{	// $[TI1.4]
					// multiply new-patient based value by a respiratory rate factor;
					// this factor ensures a constant minute volume...
					tidalVolume.value *= (minuteVolumeFactorLimit / RESP_RATE_VALUE);
				}	// $[TI1.5]

				if ( MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE )
				{
					const Setting*  pVti = SettingsMgr::GetSettingPtr(SettingId::HIGH_INSP_TIDAL_VOL);
					const Real32  VTI_VALUE = BoundedValue(pVti->getAdjustedValue()).value;
					// Now test for the high inspired volume based limitation
					if ( tidalVolume.value >= VTI_VALUE )
					{
						// the Volume-based minimum is more restrictive, therefore make it the
						// upper bound value...
						tidalVolume.value = VTI_VALUE;
					}
				}
			}
			break;

		case ModeValue::SPONT_MODE_VALUE :	  // $[TI2]
		case ModeValue::CPAP_MODE_VALUE : 
			// do nothing...
			break;

		case ModeValue::BILEVEL_MODE_VALUE :  
		default :
			// unexpected mode value...
			AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
			break;
	};

	// temporarily disable all soft bounds so they don't limit the calculated
	// value (this also updates the constraints)...
	overrideAllSoftBoundStates_();

	//-------------------------------------------------------------------
	// NOTE:  up to now only the respiratory rate and IBW formulas have been
	//        explicitly applied, the 'warpValue()' below clips based on this
	//        setting's bounds...
	//-------------------------------------------------------------------

	// check the value to not fall below the lower bound
	const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
	tidalVolume.value = MAX_VALUE( ABSOLUTE_MIN, tidalVolume.value );	

	// using this setting's constraints (sans the soft bounds), round/clip
	// calculated value...
	getBoundedRange_().warpValue(tidalVolume);

	if( tidalVolume.value == DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC )
	{
		tidalVolume.precision = ONES;
	}

	// store the transition value into the adjusted context...
	setAdjustedValue(tidalVolume);

	// now that the calculated value has been stored, re-enable all applicable
	// soft bounds...
	updateAllSoftBoundStates_(Setting::NON_CHANGING);

	// activation of Transition Rule MUST be italicized...
	setForcedChangeFlag();
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  correctForMinimumTi()
//
//@ Interface-Description
//  The method is called during certain setting transitions to correct the
//	adjusted tidal volume value to ensure its within range based upon the
// 	inspiratory time setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the tidal volume is outside of its allowed range based upon the
//  inspriatory time dependent setting then bring the value to within
//  range.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TidalVolumeSetting::correctForMinimumTi()
{
	InspTimeSetting* inspTimeSetting = (InspTimeSetting*)SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

	// If the inspiratory time setting is at its lowest limit, then correct the tidal volume
	// based upon inspiratory time. Under certain transition rule situations, the formula computing
	// the inspiratory time would compute below the lower limit of 0.2 seconds, then brought back
	// to its minimum value of 0.2. When this occurs, the tidal volume must be re-adjusted for
	// the newly adjusted inspiratory time.
	if (IsEquivalent(BoundedValue(inspTimeSetting->getAdjustedValue()).value, inspTimeSetting->getMinLimit(), HUNDREDTHS))
	{
		// Get the adjusted tidal volume taking into account the inspiratory time setting
		BoundedValue correctedTidalVolume = calcBasedOnSetting_(SettingId::INSP_TIME,
				BoundedRange::WARP_UP, BASED_ON_ADJUSTED_VALUES);

		// Set a new adjusted tidal volume that is within range
		setAdjustedValue(correctedTidalVolume);
	}
}

#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	TidalVolumeSetting::isAcceptedValid(void) const
{
	CALL_TRACE("isAcceptedValid()");

	Boolean  isValid = BoundedSetting::isAcceptedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ACCEPTED) !=
			 Applicability::INAPPLICABLE )
		{
			// is the the accepted tidal volume value consistent with the accepted
			// inspiratory time?...
			const Setting*  pInspTime =
				SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

			isValid = pInspTime->isAcceptedValid();
		}
	}

	return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
	TidalVolumeSetting::isAdjustedValid(void)
{
	CALL_TRACE("isAdjustedValid()");

	Boolean  isValid = BoundedSetting::isAdjustedValid();

	if ( isValid )
	{
		if ( getApplicability(Notification::ADJUSTED) !=
			 Applicability::INAPPLICABLE )
		{
			// is the the adjusted tidal volume value consistent with the adjusted
			// inspiratory time?...
			Setting*  pInspTime = SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

			isValid = pInspTime->isAdjustedValid();
		}
	}

	return(isValid);
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
	TidalVolumeSetting::SoftFault(const SoftFaultID  softFaultID,
								  const Uint32       lineNumber,
								  const char*        pFileName,
								  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							TIDAL_VOLUME_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcDependentValues_(isValueIncreasing)
//
//@ Interface-Description
//  Notify this setting's dependent settings of an update to this
//  primary setting.  If a dependent bound is violated, this setting's
//  bound status is updated with the dependent bound information.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02255] -- this setting's dependent settings...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean
	TidalVolumeSetting::calcDependentValues_(const Boolean isValueIncreasing)
{
	CALL_TRACE("calcDependentValues_()");

	const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);
	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	const DiscreteValue  MODE_VALUE = pMode->getAdjustedValue();
	const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAdjustedValue();

	Uint  idx = 0u;

	switch ( MODE_VALUE )
	{
		case ModeValue::AC_MODE_VALUE :
		case ModeValue::SIMV_MODE_VALUE :	  // $[TI1]
			switch ( MAND_TYPE_VALUE )
			{
				case MandTypeValue::VCV_MAND_TYPE :		// $[TI1.1]
					// all three timing dependents are active here...
					ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
					ArrDependentSettingIds_[idx++] = SettingId::EXP_TIME;
					ArrDependentSettingIds_[idx++] = SettingId::IE_RATIO;
					break;
				case MandTypeValue::VCP_MAND_TYPE :		// $[TI1.2]
					// only peak insp flow for VC+...
					ArrDependentSettingIds_[idx++] = SettingId::PEAK_INSP_FLOW;
					break;
				case MandTypeValue::PCV_MAND_TYPE :
				default :
					// unexpected mand type value...
					AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
					break;
			};
			break;

		case ModeValue::SPONT_MODE_VALUE :		  // $[TI2]
		case ModeValue::CPAP_MODE_VALUE :
			// only inspiratory time in 'SPONT' mode...
			ArrDependentSettingIds_[idx++] = SettingId::INSP_TIME;
			break;

		case ModeValue::BILEVEL_MODE_VALUE :
		default :
			// unexpected mode value...
			AUX_CLASS_ASSERTION_FAILURE(MODE_VALUE);
			break;
	}

	// terminate with the null id...
	ArrDependentSettingIds_[idx] = SettingId::NULL_SETTING_ID;

	// forward on to base class...
	return(Setting::calcDependentValues_(isValueIncreasing));
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02251]   -- tidal volume's range...
//  $[NE02006] -- tidal volume's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	TidalVolumeSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	const Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pSetting->getAdjustedValue()).value;

	pSetting = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	const DiscreteValue  MAND_TYPE_VALUE = pSetting->getAdjustedValue();

	pSetting = SettingsMgr::GetSettingPtr(SettingId::MODE);

	const DiscreteValue  MODE_VALUE = pSetting->getAdjustedValue();

	pSetting = SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

	const DiscreteValue  SPONT_TYPE_VALUE = pSetting->getAdjustedValue();

	Real32  softMinValue, softMaxValue;

	findSoftMinMaxValues_(softMinValue, softMaxValue);

	//===================================================================
	// calculate tidal volume's most restrictive MAXIMUM bound...
	//===================================================================
	{
		BoundedRange::ConstraintInfo  maxConstraintInfo;

		//-----------------------------------------------------------------
		// determine which upper limit to apply...
		//-----------------------------------------------------------------

		BoundedValue  ibwBasedMax;
		const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

		BoundedValue  highVtiBasedMax;
		highVtiBasedMax.value = ABSOLUTE_MAX + 1.0f;  // Ensure a high value to start
		maxConstraintInfo.value = ABSOLUTE_MAX;
		BoundStatus dummyStatus(getId());
		Boolean useAbsoluteMax = TRUE;	   // assume absolue max will be most restrictive

		if ( isSoftBoundActive_(TIDAL_VOL_SOFT_MAX_BASED_IBW_ID) )
		{	// $[TI1] -- tidal volume's soft maximum bound is currently active...
			// initialize to soft bound maximum bound id...
			ibwBasedMax.value = softMaxValue;    

			maxConstraintInfo.id = TIDAL_VOL_SOFT_MAX_BASED_IBW_ID;
		}
		else
		{	// $[TI2] -- tidal volume's soft bound is NOT active...
			// initialize ibw based values & id...
			ibwBasedMax.value = (IBW_VALUE *
								 SettingConstants::MAX_TIDAL_VOL_IBW_SCALE);

			maxConstraintInfo.id = TIDAL_VOL_MAX_BASED_IBW_ID;
		}

		if ( MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE )
		{
			pSetting = SettingsMgr::GetSettingPtr(SettingId::HIGH_INSP_TIDAL_VOL);
			highVtiBasedMax = pSetting->getAdjustedValue();  // assign value and precision
		}

		//-----------------------------------------------------------------
		// determine which upper limit to apply 
		//-----------------------------------------------------------------

		// Start with the IBW based max.....
		// NOTE:  When soft bound is active, this branch is forced by 
		//        ibwBasedMax.value setting above; constraintId must not be 
		//        changed in this path!
		if ( ibwBasedMax.value < ABSOLUTE_MAX )
		{	// $[TI3]
			// reset this range's maximum constraint to allow for "warping" of a
			// new maximum value...
			getBoundedRange_().resetMaxConstraint();

			// warp the maximum value to a lower "click" boundary...
			getBoundedRange_().warpValue(ibwBasedMax, BoundedRange::WARP_DOWN);            
			// the IBW-based maximum is more restrictive, therefore make it the
			// upper bound value...
			maxConstraintInfo.value = ibwBasedMax.value;
			useAbsoluteMax = FALSE;
		}


		// Now test for the high inspired volume based limitation
		// (but only after all settings have been initialized)
		AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();
		if ( (highVtiBasedMax.value <= maxConstraintInfo.value) && rAdjContext.getAllSettingValuesKnown() )
		{	// $[TI1]
			// the Volume-based minimum is more restrictive, therefore make it the
			// lower bound value...

			// reset this range's maximum constraint to allow for "warping" of a
			// new maximum value...
			getBoundedRange_().resetMaxConstraint();

			if ( MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE )
			{
				maxConstraintInfo.id = TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL_MAND_ID;
			}

			if ( (SPONT_TYPE_VALUE == SupportTypeValue::ATC_SUPPORT_TYPE) && 
				 (MODE_VALUE != ModeValue::AC_MODE_VALUE) )	 // Both mand and spont
			{
				maxConstraintInfo.id = TIDAL_VOL_MAX_BASED_HIGH_INSP_TIDAL_VOL_ID;

			}

			getBoundedRange_().decrement( 1, highVtiBasedMax, dummyStatus);
			maxConstraintInfo.value = highVtiBasedMax.value;
			useAbsoluteMax = FALSE;
		}

		if ( useAbsoluteMax )
		{
			// the absolute maximum is more restrictive, therefore make it the
			// upper bound value...
			maxConstraintInfo.id     = TIDAL_VOL_MAX_ID;
			maxConstraintInfo.value  = ABSOLUTE_MAX;
		}


		// is set to soft bound?...
		maxConstraintInfo.isSoft =
		// $[TI5] (TRUE)  $[TI6] (FALSE)...
			(maxConstraintInfo.id == TIDAL_VOL_SOFT_MAX_BASED_IBW_ID);

		getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
	}


	//===================================================================
	// calculate tidal volume's most restrictive MINIMUM bound...
	//===================================================================
	{
		BoundedRange::ConstraintInfo  minConstraintInfo;

		//-----------------------------------------------------------------
		// determine which IBW-based lower limit to apply...
		//-----------------------------------------------------------------

		BoundedValue  ibwBasedMin;

		if ( isSoftBoundActive_(TIDAL_VOL_SOFT_MIN_BASED_IBW_ID) )
		{	// $[TI7] -- tidal volume's soft minimum bound is currently active...
			// initialize to soft bound minimum bound id...
			ibwBasedMin.value    = softMinValue;
			minConstraintInfo.id = TIDAL_VOL_SOFT_MIN_BASED_IBW_ID;
		}
		else
		{	// $[TI8] -- tidal volume's soft bound is NOT active...
			// initialize to absolute minimum bound id...
			ibwBasedMin.value = (IBW_VALUE *
								 SettingConstants::MIN_TIDAL_VOL_IBW_SCALE);
			minConstraintInfo.id = TIDAL_VOL_MIN_BASED_IBW_ID;
		}


		//-----------------------------------------------------------------
		// determine which lower limit to apply (absolute or IBW-based?)...
		//-----------------------------------------------------------------

		const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

		if ( ibwBasedMin.value > ABSOLUTE_MIN )
		{	// $[TI9]
			// reset this range's minimum constraint to allow for "warping" of a
			// new minimum value...
			getBoundedRange_().resetMinConstraint();
			
			// warp the minimum value to a upper "click" boundary...
			getBoundedRange_().warpValue(ibwBasedMin, BoundedRange::WARP_UP);

			// the IBW-based minimum is more restrictive, therefore make it the
			// upper bound value...
			minConstraintInfo.value = ibwBasedMin.value;
		}
		else
		{	// $[TI10]
			// the absolute minimum is more restrictive, therefore make it the
			// upper bound value...
			minConstraintInfo.id    = TIDAL_VOL_MIN_ID;
			minConstraintInfo.value = ABSOLUTE_MIN;
		}

		// is set to soft bound?...
		minConstraintInfo.isSoft =
		// $[TI11] (TRUE)  $[TI12] (FALSE)...
			(minConstraintInfo.id == TIDAL_VOL_SOFT_MIN_BASED_IBW_ID);

		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NE02006] -- tidal volume's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	TidalVolumeSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
											  Real32& rSoftMaxValue) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	BoundedValue  boundedValue;

	//-------------------------------------------------------------------
	// determine lower soft bound limit value...
	//-------------------------------------------------------------------
	// calculate lower soft bound limit...
	boundedValue.value =
		(IBW_VALUE * SettingConstants::MIN_TIDAL_VOL_SOFT_IBW_SCALE);

	// warp the calculated value to the upper "click", with no clipping...
	getBoundedRange().warpValue(boundedValue, BoundedRange::WARP_UP, FALSE);

	rSoftMinValue = boundedValue.value;

	//-------------------------------------------------------------------
	// determine upper soft bound limit value...
	//-------------------------------------------------------------------

	// calculate upper soft bound limit...
	boundedValue.value =
		(IBW_VALUE * SettingConstants::MAX_TIDAL_VOL_SOFT_IBW_SCALE);

	// warp the calculated value to the lower "click", with no clipping...
	getBoundedRange().warpValue(boundedValue, BoundedRange::WARP_DOWN, FALSE);

	rSoftMaxValue = boundedValue.value;
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//  All new-patient calculations are to be based on the VCV setting values.
//
//  $[02019] -- dependent calculations
//---------------------------------------------------------------------
//@ PreCondition
//  (basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//  (basedOnSettingId == SettingId::EXP_TIME  ||
//   basedOnSettingId == SettingId::IE_RATIO  ||
//   basedOnSettingId == SettingId::INSP_TIME)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
	TidalVolumeSetting::calcBasedOnSetting_(
										   const SettingId::SettingIdType basedOnSettingId,
										   const BoundedRange::WarpDir    warpDirection,
										   const BasedOnCategory          basedOnCategory
										   ) const
{
	CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

	AUX_CLASS_PRE_CONDITION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
							basedOnCategory);

	BoundedValue  inspTime;

	// determine a value for 'inspTime', based on which timing parameter
	// forcing this call...
	switch ( basedOnSettingId )
	{
		case SettingId::EXP_TIME :
			{	// $[TI1] -- calculate an insp time from exp time...
				const Setting*  pExpTime =
					SettingsMgr::GetSettingPtr(SettingId::EXP_TIME);

				const Real32  EXP_TIME_VALUE =
					BoundedValue(pExpTime->getAdjustedValue()).value;
				const Real32  BREATH_PERIOD_VALUE =
					RespRateSetting::GetBreathPeriod(basedOnCategory);

				inspTime.value = (BREATH_PERIOD_VALUE - EXP_TIME_VALUE);
			}
			break;
		case SettingId::IE_RATIO :
			{	// $[TI2] -- calculate an insp time from I:E ratio...
				const Setting*  pIeRatio =
					SettingsMgr::GetSettingPtr(SettingId::IE_RATIO);

				const Real32  IE_RATIO_VALUE =
					BoundedValue(pIeRatio->getAdjustedValue()).value;
				const Real32  BREATH_PERIOD_VALUE =
					RespRateSetting::GetBreathPeriod(basedOnCategory);

				Real32  eRatioValue;
				Real32  iRatioValue;

				if ( IE_RATIO_VALUE < 0.0f )
				{	// $[TI2.1]
					// The I:E ratio value is negative, therefore we have 1:xx with
					// "xx" the expiratory portion of the ratio...
					iRatioValue = 1.0f;
					eRatioValue = -(IE_RATIO_VALUE);
				}
				else
				{	// $[TI2.2]
					// The I:E ratio value is positive, therefore we have xx:1 with
					// "xx" the inspiratory portion of the ratio...
					iRatioValue = IE_RATIO_VALUE;
					eRatioValue = 1.0f;
				}

				// calculate the insp time from the I:E ratio...
				inspTime.value = (iRatioValue * BREATH_PERIOD_VALUE) /
								 (iRatioValue + eRatioValue);

				BoundedSetting*  pInspTime =
					SettingsMgr::GetBoundedSettingPtr(SettingId::INSP_TIME);

				// the "raw" insp time value must be rounded to a resolution boundary,
				// before using the value in the calculations, below...
				pInspTime->warpToRange(inspTime, warpDirection, FALSE);
			}
			break;
		case SettingId::INSP_TIME :
		case SettingId::PEAK_INSP_FLOW :
			{	// $[TI3] -- get the "adjusted" insp time value...
				const Setting*  pInspTime =
					SettingsMgr::GetSettingPtr(SettingId::INSP_TIME);

				inspTime.value = BoundedValue(pInspTime->getAdjustedValue()).value;
			}
			break;
		default :
			// unexpected 'basedOnSettingId' value...
			AUX_CLASS_ASSERTION_FAILURE(basedOnSettingId);
			break;
	};

	// get pointers to each of the VCV parameters...
	const Setting*  pFlowPattern =
		SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);
	const Setting*  pPeakInspFlow =
		SettingsMgr::GetSettingPtr(SettingId::PEAK_INSP_FLOW);
	const Setting*  pPlateauTime =
		SettingsMgr::GetSettingPtr(SettingId::PLATEAU_TIME);

	const DiscreteValue  FLOW_PATTERN_VALUE = pFlowPattern->getAdjustedValue();

	const Real32  PEAK_INSP_FLOW_VALUE =
		BoundedValue(pPeakInspFlow->getAdjustedValue()).value;
	const Real32  PLATEAU_TIME_VALUE   =
		BoundedValue(pPlateauTime->getAdjustedValue()).value;

	BoundedValue  tidalVolume;

	switch ( FLOW_PATTERN_VALUE )
	{
		case FlowPatternValue::SQUARE_FLOW_PATTERN :  // $[TI4] 
			{
				static const Real32  FACTOR_ = 1.0f / (60000.0f * 0.001f);

				// calculate the tidal volume value...
				tidalVolume.value = (PEAK_INSP_FLOW_VALUE *
									 (inspTime.value - PLATEAU_TIME_VALUE)) * FACTOR_;
			}
			break;

		case FlowPatternValue::RAMP_FLOW_PATTERN :	  // $[TI5]
			{
				static const Real32  FACTOR_ = 1.0f / (60000.0f * 0.001f * 2.0f);

				const Setting*  pMinInspFlow =
					SettingsMgr::GetSettingPtr(SettingId::MIN_INSP_FLOW);

				const Real32  MIN_INSP_FLOW_VALUE =
					BoundedValue(pMinInspFlow->getAdjustedValue()).value;

				// calculate the tidal volume value...
				tidalVolume.value = ((PEAK_INSP_FLOW_VALUE + MIN_INSP_FLOW_VALUE) *
									 (inspTime.value - PLATEAU_TIME_VALUE)) * FACTOR_;
			}
			break;

		default :
			AUX_CLASS_ASSERTION_FAILURE(FLOW_PATTERN_VALUE);
			break;
	};


	const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();
	// $[02251] Enforce lower bound
	tidalVolume.value = MAX_VALUE(ABSOLUTE_MIN, tidalVolume.value);	

	// place tidal volume on a "click" boundary...
	getBoundedRange().warpValue(tidalVolume, warpDirection, FALSE);

    if( tidalVolume.value == DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC )
	{
		tidalVolume.precision = ONES;
	}

	return(tidalVolume);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Return tidal volume's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic minimum constraint.
//
//  $[02251] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	TidalVolumeSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	const DiscreteValue  MAND_TYPE_VALUE =
		SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
											  SettingId::MAND_TYPE);

	const Setting*  P_FLOW_PATTERN =
		SettingsMgr::GetSettingPtr(SettingId::FLOW_PATTERN);

	const DiscreteValue  FLOW_PATTERN_VALUE = P_FLOW_PATTERN->getAdjustedValue();

	AdjustedContext&  rAdjustedContext = ContextMgr::GetAdjustedContext();

	Boolean isNeoModeAdvancedEnabled = 
		SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_ADVANCED);

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			if( isNeoModeAdvancedEnabled )
			{
				// return the lower bound based on MAND TYPE
				if( MAND_TYPE_VALUE == MandTypeValue::VCP_MAND_TYPE )
				{
					// SCR 6522 : Different min for SQUARE flow pattern
					if( !rAdjustedContext.isInNewPatientSetupMode() 
					   && ( FLOW_PATTERN_VALUE == FlowPatternValue::SQUARE_FLOW_PATTERN )
					  )
					{
						absoluteMinValue = 
							SettingConstants::TIDAL_VOL_NEONATAL_MIN_SQUARE_FLOW_PATTERN_VALUE;
                    }
					else { // [NE02000]
						absoluteMinValue = SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS;
					}
				}
				else // [NE02000]
				{
                    absoluteMinValue = SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_NON_VC_PLUS;
				}
			}
			else // [NV02000] Tidal vol shall be 5mL if Advanced NeoMode is NOT present
			{
				absoluteMinValue = SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_MISC;
			}
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2]
			absoluteMinValue = SettingConstants::TIDAL_VOL_STANDARD_MIN_VALUE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMinValue);
}
