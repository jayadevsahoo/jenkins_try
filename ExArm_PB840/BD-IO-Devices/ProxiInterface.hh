#ifndef	ProxiInterface_HH
#define ProxiInterface_HH

//====================================================================
// This is a proprietary work to which the Covidien corporation claims
// exclusive right.  No part of this work may be used, disclosed,
// reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Covidien Corporation.
//
//              Copyright (c) 2008, Covidien Corporation
//=====================================================================

//=====================================================================
//@ Class:
//---------------------------------------------------------------------
//@ Version-Information
//@ Version
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ProxiInterface.hhv   25.0.4.0   19 Nov 2013 13:54:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 019   By: gdc   Date: 17-Jan-2011    SCR Number: 6733
//  Project:  PROX
//  Description:
//      Included VCO2 study code for development reference.
//
//  Revision: 018   By: rhj   Date: 14-Dec-2010    SCR Number: 6656
//  Project:  PROX
//  Description:
//      Added a flag to prevent barametric pressures to be updated
//      during Demo mode.
//
//  Revision: 017   By: rhj   Date: 16-Nov-2010    SCR Number: 6622
//  Project:  PROX
//  Description:
//      Added stopProx();
//
//  Revision: 016   By: rpr   Date: 29-Sept-2010    SCR Number: 6687
//  Project:  PROX
//  Description:
//      Fixed getting continuous serial data without any commands from PROX.
//
//  Revision: 015   By: rhj   Date: 21-Sept-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added some debug information for prox.
//
//  Revision: 014   By: erm   Date: 9-Sep-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added isProxReady() and isProxSuspended()
//
//  Revision: 013   By: erm   Date: 15-June-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added flow and airway pressure out-of-range checks
//
//  Revision: 012   By: mnr   Date: 10-Jun-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      updatePurgeInterval() added.
//
//  Revision: 011   By: erm   Date: 25-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Remove flow zero valuve time from purge params
//
//  Revision: 011   By: mnr   Date: 13-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Increased PROX_MAX_REV_STR_SIZE to 36 (35 max + 1 null).
//      $Header added in Version section at the top.
//
//  Revision: 010   By: erm   Date: 06-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Remove togleMotor
//
//  Revision: 009   By: mnr   Date: 03-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Purge suspend/resume and status APIs added.
//
//  Revision: 008  By:  mnr    Date:  22-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      isSerialNumReady_ and isSerialNumReady() added.
//
//  Revision: 007  By:  mnr    Date: 13-Apr-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      isRevStrReady_ and isRevStrReady() added.
//
//  Revision: 006  By:  rpr    Date: 31-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added isProxBoardInstalled function
//
//  Revision: 005  By:  rhj    Date: 31-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added isOemIdReady flag
//
//  Revision: 004  By:  mnr    Date: 30-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      PROX rev string size reduced to 25.
//
//  Revision: 003  By:  mnr    Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      PROX rev string and serial number related updates.
//
//  Revision: 002  By:  erm    Date:  1-Feb-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added Purge, modified/ add waveforms, motor control,etc
//
//  Revision: 001  By:  gdc    Date:  25-APR-2008    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Initial version.
//=====================================================================

#include "BD_IO_Devices.hh"
#include "BdSerialInterface.hh"
#include "SettingId.hh"
#include "OsTimeStamp.hh"
#include "Mercury.hh"
#include "ServiceMode.hh"
//@ Usage-Classes
//@ End-Usage
class BigSerialNumber;
class SerialPort;

class ProxiInterface : public BdSerialInterface
{

public:


	enum
	{
		PROX_MAX_ID_STRING = 20
	};
	enum
	{
		PROX_MAX_REV_STR_SIZE = 36 //Max 35 + 1 NULL
	};

	// constants used to parse through prox packets
	enum
	{
		// must be a power of 2
		NUMBUFFS = 32,
		NUMBUFFSMASK = NUMBUFFS-1,

		CMD_INDEX = 0,
		NBF_INDEX = 1,
		MIN_PKT_SIZE = 3,	 // CMD + NBF + CKS
		MAX_PKT_SIZE = 0x7f + 2,  // CMD + NBF + MSG
		PROX_CMD_MASK = 0x80
	};

	enum ZeroStatus
	{
		  ZERO_IDLE               = 1,
		  ZERO_REQUESTED          = 2,
		  ZERO_IN_PROGRESS        = 3,
		  ZERO_REQUIRED           = 4

	};

	enum PurgeError
	{
		OVER_PRESSSURE            = 0x01,
		OVER_PRERESURE_CHARGING   = 0x02,
		PUMP_OFF                  = 0x04,
		PUMP_OFF_PURGE			  = 0x08,
		WAIT_EXCEEDED             = 0x10,
		PUMP_DISABLED             = 0x20
	};

	enum PurgeStatusByte
	{
		CUSTOM_PURGE_DISABLED         = 1,
		CUSTOM_PURGE_ERROR            = 2,
		CUSTOM_PURGE_IDLE             = 3,
		CUSTOM_PURGE_CHARGING         = 4,
		CUSTOM_PURGE_SUSPENDED        = 5,
		CUSTOM_PURGE_WAITING          = 6,
		CUSTOM_PURGE_DISCHARGING      = 7,
		CUSTOM_PURGE_SETTLING         = 8,
		CUSTOM_PURGE_WAIT_ZERO_LINE   = 20,
		CUSTOM_PURGE_ZERO_INPROGRESS  = 21,
		CUSTOM_PURGE_CAL_INPROGRESS   = 64,
		CUSTOM_PURGE_SQ_WAVE_DEMO     = 65

	};

	enum PurgeMode
	{
		      PROX_AUTO_PURGE_MODE  = 1,
			  PROX_MANUAL_MODE      = 2,
			  INSTANT_MODE          = 3
			};

	enum PurgeParam
	{
		    TARGET_PRESSURE,
		    PRESSURE_TOLORANCE,
		    THRESHOLD_PRESSURE,
		    PURGE_DURATION,
		    SETTING_TIME,
		    AUTOZERO_DURATION,
		    MAX_PURGE_PARAM
	 } ;

	 enum ReadState
	 {
	     COMMAND,
	     NUMBER_OF_BYTES,
	     DATA
	 };

	 enum PurgeMask
	 {
        PURGE_ENABLE_HIGH_MASK = 0x1,
		ZERO_REQUIRED_MASK     = 0x2,
		RESERVOIR_PRESSURE_INCREASE_ERROR_MASK = 0x4,
		RESERVOIR_PRESSURE_DECREASE_ERROR_MASK = 0x8,
        RESERVOIR_INHALATION_DISCHARGE_ERROR_MASK = 0x10
	 };

	 enum FlowStatusByte
	{
		 UNDEFINED               = 0,
         FLOW_STATUS_NO_ERROR    = 1,
		 DEMO_MODE_ACTIVE        = 2,
		 EEPROM_ERROR            = 3,
		 BAR_PRESSURE_ERROR      = 4,
		 HARDWARE_ERROR          = 5,
		 FLOW_PURGE_IN_PROGRESS  = 6,
		 FLOW_ZERO_IN_PROGRESS   = 7,
         FLOW_ZERO_ERROR         = 8
    };

	 struct PurgeParams
	{
	   PurgeMode mode;
       Real32 maxWaitTime;
	   Real32 resTargetPressure;
	   Real32 resMaxPressure;
	   Int8 cycleRepCount;
	   Real32 releaseValveOpenTime;
	   Real32 releaseValveCloseTime;
	   Uint16 intervalTime;
       Real32 resPressureTransOffset;
	   Real32 maxChargeTime;
      };

	  struct GasConditions
	  {
          Uint8 fio2;                    //O2 percent
		  Real32 anesthetic;             // anesthetic 0 -> 20%
		  Uint8  gasBalance;             // 0 = N2 , 1 = N20, 2 = Helium
		  Uint8  inspHumidity;           // Insp Humidity 0 -> 100%
		  Uint8  inspGasTemperature;     // temp in C  0 -> 40
	      Uint8 expGasParam;             // bit0 = 0 -> exp temp = insp temp,
		                                 //bit0 = 1 -> exp temp = patient Temp (35C)
										 // bit1 = 0 -> exp humidity = insp humidity
										 // bit1 = 1 -> exp humidity = patient humidity (100%)

 	  };

    enum MonitoredDataState
    {
        DATA_UNINITIALIZED,
        DATA_INVALID,
        DATA_QUESTIONABLE,
        DATA_VALID
    };
	struct MonitoredData
	{
		union
		{
            Uint32  data;
			Real32  value;
			char *  pStr;
		};
		OsTimeStamp updateTime;
		OsTimeStamp reportTime;
		Uint32      updateFrequency;
        MonitoredDataState state;
	};

	struct PurgeStatus
	{
		Uint8 byte1;
		Uint8 byte2;
		Uint8 byte3;
		OsTimeStamp updateTime;
		OsTimeStamp reportTime;
	};

    ProxiInterface( BdSerialPort & rSerialPort,
					BdSerialInterface::PortNum portNum );
	~ProxiInterface();
	ProxiInterface& operator=(const ProxiInterface& rhs);	//not implemented
	virtual void newCycle(void);
	Real32 getFlow(void) const;
	Real32 getPressure(void) const;
	Real32 getCO2(void) const;
	Real32 getVolume(void) const;
	Real32 getInspVolume(void) const;
	Real32 getExhVolume(void) const;
	Real32 getBarometricPressure(void) const;
	Real32 getAccumatorPressure(void) const;
	Real32 getResevoirPressure(void) const;
	void performZero(void);
	Boolean isDataFresh(void) const;
	Boolean getFromProxIE(void);

#if defined(VCO2_STUDY)
    Uint32 getCapnostatStatus() const;
    Real32 getEndTidalCO2() const;
    Real32 getInspiredCO2() const;
    Real32 getVolumeCO2ExpiredPerMinute() const;
    Real32 getMixedExpiredCO2() const;
    Real32 getStartOfInspirationMark() const;
    Real32 getStartOfExpirationMark() const;
    Real32 getInspiredTime() const;
    Real32 getExpiredTime() const;
    Boolean getDataValid() const;
    void setETCO2TimePeriod(const Mercury::ETCO2TimePeriod period);
    void setVCO2AveragingTime(const Mercury::VCO2AveragingPeriod period);
    Uint32 getMercurySoftwareOptions() const;
#endif // defined(VCO2_STUDY)

    Boolean updatePurgeParams  (PurgeParams &params, Boolean forceAll = FALSE);
    Boolean updateGasConditions( GasConditions &conditions);

	void dumpAccumalator(Boolean pumbOn = FALSE);
	void requestManualPurge(Real32 targetPressure, Real32 maxPressure);
	Uint32 verifyPurgeParams(PurgeParams &params);
	ProxiInterface::ZeroStatus getZeroStatus(void);
    Uint32 getFlowStatus(void);
	Mercury::SensorType getSensorType(void);
    Boolean isSensorReadingCycleError(Uint32 delta);

	Boolean isPurgeActive(void);
	Boolean isPurgeError(void);
	Boolean isWaitingForPurgeEnable(void);
	Boolean isIdleModeActive(void);

    Boolean isZeroWaiting(void);
	Boolean isZeroActive(void);

	Uint8 getPurgeErrorByte(void);
	Uint8 getReservoirError(void);
	Uint32 getFlowOOR(void);
	Uint32 getPressureOOR(void);
	void suspend(void);
	void resume(void);
	Uint32 getBoardSerialNumber(void) const;
	void copyMainRevStr(BigSerialNumber& mainRevStr);
	Boolean isRevStrReady(void);
	Boolean isOemIdReady(void);
	Boolean isSerialNumReady(void);
    static Boolean isProxBoardInstalled(void);
	Boolean isCommunicationUp(void);
	Boolean isProxSuspended(void);

	void setSquareWaveformDemoMode(Boolean enable);

	void suspendPurge(void);
	void resumePurge(void);

	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32 lineNumber,
						   const char * pFileName = NULL,
						   const char * pPredicate = NULL);
    void disableSquareWaveformDemoMode(void);
    void enableSquareWaveformDemoMode(void);
    void enablePurgeTest(Real32 targetPressure);
    void setManualPurge(void);
    void setGasParams( GasType testGas );

	void updatePurgeInterval(Uint16 targetTimeInSecs);
	void enableContinuousMode(void);
	void disableContinuousMode(void);
	Boolean isProxReady(void);
    void setInspiredVolumeFactor(Real32 inpspiredVolumeFactor);
    void setExpiredVolumeFactor(Real32 expiredVolumeFactor);
    void setAtmPressure(Real32 atmPressure);
    void setHmeFactorTest(Real32 hmeFactorTest);
    void setHmeFactor(Real32 hmeFactor);
    void setProxCorrectionFactor(Real32 proxCorrectionFactor);

protected:
	virtual void processRequests_(void);

private:
	void addChecksum_(short value, unsigned char buffer[]);
	int addWord_(unsigned char* pBuf, const short value);
    void enablePurge_(void);
    void disablePurge_(void);
	void resetModule_(void);
	void startContinuousMode_(void);
	void stopContinuousMode_(void);
	void getSoftwareRevision_(const Mercury::RevisionFormat revisionFormat);
	void setAirwayGasConditions_(void);
	void processWaveformPkt_(unsigned char * msg, int pktSize);
	void processGetAndSetPkt_(unsigned char * msg, int pktSize);
	void sendPacket_(void* buffer, const int numbytes);
	void transmitPurgeParams_(void);


	Boolean isChecksumValid_(unsigned char *pBuffer, short value);
    Boolean readCmd_( Byte * buffer);
    void processCmd_(Byte * msg);
	void fixUpbuffer_(void);

	void setManualPurge_(PurgeMode mode);
	void setMaxInputLineWait_(Real32 target);
	void setResTargetPressure_(Real32 target);
	void setResMaxPressure_(Real32 target);
	void setCycleRepCount_(Uint8 count);
	void setValveOpenTime_(Real32 target);
	void setValveCloseTime_(Real32 target);
	void setIntervalTime_(Uint16 target);
	void setFlowZeroValveOpenTime_(Real32 target);
	void setMaxChargeTime_ (Real32 target);
	void getBoardSerialNumber_(void);
	void getOEMId_(void);
	void transmitZeroRequest_(void);
	void transmitDumpAccumalator_(Boolean pumpOn);
	void enableManualPurge_(void);
	void executeManualPurge_(void);
    void purgeTest_(void);
	void stopProx_(Boolean runOnce);

    //@ Data-Member: isDataFresh_
	//flag to show  new data has arrived
	Boolean isDataFresh_ ;

    //@ Data-Member: isUpdateNeeded_
	//flag to show  purge Params are changed
	Boolean isUpdateNeeded_;

    //@ Data-Member: isGasConditionUpdateNeeded_
	//flag to show  gas condition are changed
	Boolean isGasConditionUpdateNeeded_;

	//@ Data-Member: isGasConditionUpdateNeeded_
	//flag to show  request to start prox again
	Boolean resumeRequest_;

	//@ Data-Member: suspendRequest_
	//flag to show  request to suspend prox comm
	Boolean suspendRequest_;

	//@ Data-Member: resumePurgeRequest_
	//flag to show  request to start purge again
	Boolean resumePurgeRequest_;

	//@ Data-Member: suspendPurgeRequest_
	//flag to show  request to suspend purge
	Boolean suspendPurgeRequest_;

	//@ Data-Member: isProxSuspended_
	//flag to show prox is suspended
	Boolean isProxSuspended_;

	//@ Data-Member: isFlowRequested_
	//flag to show flow waveform requested
	Boolean isFlowRequested_;

	//@ Data-Member: isPressureRequested_
	//flag to show pressure waveform requested
	Boolean isPressureRequested_;

	//@ Data-Member: isCO2Requested_
	//flag to show CO2 waveform requested
	Boolean isCO2Requested_;

    //@ Data-Member: isVolumeRequested_
	//flag to show Volume waveform requested
	Boolean isVolumeRequested_;

    //@ Data-Member: isAccumPressureRequested_
	//flag to show Accumalator waveform requested
	Boolean isAccumPressureRequested_;

    //@ Data-Member: isDumpAccumalatorRequested_
	//flag to show dump accumlator is requested
	Boolean isDumpAccumalatorRequested_;

    //@ Data-Member: isHostZeroRequested_
	//flag to show Autozero is requested
	Boolean isHostZeroRequested_;

	//@ Data-Member: isZeroBusy_
	//flag to show Autozero is requested
	Boolean isZeroBusy_;

	//@ Data-Member: isModuleResetRequested_
	//flag to show Autozero is requested
	Boolean isModuleResetRequested_;

	//@ Data-Member: isManualPurgeRequested_
	//flag to show manual purge is requested
	Boolean isManualPurgeRequested_;


	//@ Data-Member: sPurgeParamsTransmitting_
	//flag to show transmiting purge params
	Boolean isPurgeParamsTransmitting_;

	//@ Data-Member: sPurgeParamsTransmitting_
	//flag to show transmiting  gas condition
	Boolean isGasConditionsTransmitting_;

	//@ Data-Member: pumpOn_
	//flag to show pump is on
	Boolean pumpOn_;

	//@ Data-Member: volumeResolution_
	//incoming volume resoultion
    Real32  volumeResolution_;

	//@ Data-Member: manualTargetPressure_
	//target pressure to use during manaual purge
	Real32 manualTargetPressure_;

	//@ Data-Member:  manualMaxPressure_
	//max pressure to use during manaual purge
	Real32 manualMaxPressure_;

	//@ Data-Member:  totalWaveformPkts_
	// used to count the number of wave form packets
	Int32   totalWaveformPkts_;

	//@ Data-Member:  missedWaveformPkts_
	// used to count the number of missed wave form packets
	Int32	missedWaveformPkts_;

	//@ Data-Member:  badHdrs_
	// used to count the number unknown headers from interface
	Int32	badHdrs_;

	//@ Data-Member:  expectedPacketCount_
	// used to count the number of expected packets
	Int32   expectedPacketCount_;

	//@ Data-Member:  invalidChecksums_
	// used to count the number of invalid checksum from interface
	Int32	invalidChecksums_;

	//@ Data-Member:  shortPkts_
	// used to count the number of packets that are not complete
	Int32	shortPkts_;

	//@ Data-Member:  currentCycle_
	// used to control when to process next control cycle
	// increments each cycle
	Int32   currentCycle_;

	//@ Data-Member:  nextReadCycle_
	// used to control when to process next control cycle
	Int32   nextReadCycle_;

	//@ Data-Member:  totalBytesRead_
	// keeps track of bytes read from interface
	Int32	totalBytesRead_;

	//@ Data-Member:  readState_
	// processing state of incoming packets
	ReadState readState_;

	//@ Data-Member:  maxRead_
	// tracking the number of maximum bytes
	Int32 maxRead_;

	//@ Data-Member: serialNumber_
	//serial number ov Mercury module
	Uint32   boardSerialNumber_;

	//@ Data-Member: zeroStatus_
	//current autozero status
	ZeroStatus zeroStatus_;

    //@ Data-Member: flow_
	//flow data from prox
	MonitoredData   flow_;

    //@ Data-Member: pressure_
	//pressure data from prox
	MonitoredData   pressure_;

	//@ Data-Member: co2_
	//CO2 data from prox
	MonitoredData	co2_;

	//@ Data-Member: volume_
	//volume_ data from prox
	MonitoredData	volume_;

    //@ Data-Member:  flowStatus_
	//flow status data from prox
	MonitoredData   flowStatus_;

    //@ Data-Member: barometricPressure_
	//barometric  data from prox
	MonitoredData	barometricPressure_;

	//@ Data-Member: reportedZeroTime_
	//time reported for next autozero from prox
	MonitoredData   reportedZeroTime_;

    //@ Data-Member: currentResevoirPressure_
	//Resevoir Pressure from prox
	MonitoredData   currentResevoirPressure_;

	//@ Data-Member: mainRevision_
	//main revision data from prox
	MonitoredData	mainRevision_;

    //@ Data-Member: mainRevisionStr_
	//main revision data storage
	char			mainRevisionStr_[PROX_MAX_REV_STR_SIZE];

    //@ Data-Member: pressure_
	//OEM Id string data storage
    char            idString_[PROX_MAX_ID_STRING];

	//@ Data-Member: pressure_
	//Resevoir Pressure on waveform from prox
	MonitoredData   accumlatorPressure_;

    //@ Data-Member: sensorType_
	//current connect prox sensor
	Mercury::SensorType sensorType_;

	//@ Data-Member: currentUpdateTime_
	//last waveform update
	OsTimeStamp     currentUpdateTime_;

	//@ Data-Member: purgeParams_
	//incoming purge params
	PurgeParams purgeParams_;

    //@ Data-Member: purgeParams_
	//outgoing purge params
	PurgeParams transferPurgeBuffer_;

    //@ Data-Member: purgeStatus_
	//current purge status
	PurgeStatus purgeStatus_;

	//@ Data-Member: transferGasConditionBuffer_
	//gas condition buffer
	GasConditions transferGasConditionBuffer_;

	//@ Data-Member: flowStatusByte1_
	//overall flow status
	Uint8 flowStatusByte1_;

	//@ Data-Member: isRevStrReady_
	//Boolean status flag
	Boolean isRevStrReady_;

	//@ Data-Member: isOemIdReady_
    //Boolean status flag
	Boolean isOemIdReady_;

	//@ Data-Member: isSerialNumReady_
	//Boolean status flag
	Boolean isSerialNumReady_;

	//@ Data-Member: enableSquareWaveformDemoMode_
	// enable square waveform demo mode.
    Boolean enableSquareWaveformDemoMode_;

	//@ Data-Member: disableSquareWaveformDemoMode_
	// disable square waveform demo mode.
	Boolean disableSquareWaveformDemoMode_;

	//@ Data-Member: enablePurgeTest_
	// enable purge test
	Boolean enablePurgeTest_;

	//@ Data-Member: sstTargetPressure_
	// Set SST target pressure.
	Real32 sstTargetPressure_;

    //@ Data-Member: forceDownAllPurgeParams_
	// Force all param update;
	Boolean forceDownAllPurgeParams_;

	// number of max size commands plus some extra for padding for messages
	// that flow past end of buffer
	static const Uint32 LOGIO_CHARBUF_SIZE = 200;

	enum
	{
		PORTBUFFERSIZE = MAX_PKT_SIZE*NUMBUFFS //TODO E600 change + (LOGIO_CHARBUF_SIZE)
	};
    //@ Data-Member: portBuffer_
	// local prox serial buffer
	Byte portBuffer_[PORTBUFFERSIZE];

    //@ Data-Member: cmdDex_
	// cmdDex index where next command can be found in buffer
	Uint32 cmdDex_;

    //@ Data-Member: nextDex_
	// nextDex_ index where next command can be found in buffer
	Uint32 nextDex_;

    //@ Data-Member: bytesRead_
	// bytesRead_ number bytes avail from prox
	Uint32 bytesRead_;

    //@ Data-Member: commandFound_
	// commandFound_ indicates that a command bytes was parsed and found
	Boolean commandFound_;

    //@ Data-Member: nbfFound_
	// nbfFound_ indicates that a nbf bytes was parsed and found
	Boolean nbfFound_;

    //@ Data-Member: pktSize_
	// pktSize_ size of packet from prox interface
	Uint32 pktSize_;

    //@ Data-Member: numCmds_
	// numCmds_ number of commands from prox interface
	Uint32 numCmds_;

    //@ Data-Member: setProxManualPurge_
	// Sets the manual purge
	Boolean setProxManualPurge_;

    //@ Data-Member: isGetOEMIdRequested_
	// flag to show OEM id is requested
	Boolean isGetOEMIdRequested_;

    //@ Data-Member: isPurgeIntervalUpdateRequested_
	// flag to track whether Sst Purge interval needs to be updated
	Boolean isPurgeIntervalUpdateRequested_;

	//@ Data-Member: numCmds_
	// numCmds_ number of commands from prox interface
	Uint16 targetPurgeInterval_;

    //@ Data-Member: flowOOR_
	// counter for out-of-range pressure readings
	Uint32 airwayPressureOOR_;

	//@ Data-Member: flowOOR_
	// counter for out-of-range flow readings
	Uint32 flowOOR_;

	//@ Data-Member: enableContinuousMode_
	// enable continuous mode
	Boolean enableContinuousMode_;

	//@ Data-Member: disableContinuousMode_
	// disable continuous mode
	Boolean disableContinuousMode_;

	//@ Data-Member: isProxReady_
	// flag to indicate the process is ready
	Boolean isProxReady_;

    //@ Data-Member: inspiredVolumeFactor_
    // stores the inspired volume factor.
    Real32 inspiredVolumeFactor_;

    //@ Data-Member: expiredVolumeFactor_
    // stores the expired volume factor.
    Real32 expiredVolumeFactor_;

    //@ Data-Member: atmPressure_
    // stores the atm pressure.
    Real32 atmPressure_;

    //@ Data-Member: hmeFactorTest_
    // stores the HME factor test.
    Real32 hmeFactorTest_;

    //@ Data-Member: hmeFactor_
    // stores the HME factor.
    Real32 hmeFactor_;

    //@ Data-Member: proxCorrectionFactor_
    // stores the current prox correction factor.
    Real32 proxCorrectionFactor_;

    //@ Data-Member: isProxInDemoMode_
	// flag which indicates demo mode enabled or not
	Boolean isProxInDemoMode_;

    //@ Data-Member: isProxBoardInstalled_
	// TRUE when prox board is detected on the BD serial port (CTS is asserted)
	static Boolean isProxBoardInstalled_;


#if defined(VCO2_STUDY)
    void getMercurySoftwareOptions_(void);
    void zeroCapnostat_(void);

	MonitoredData	inspiratoryVolume_;
	MonitoredData	expiratoryVolume_;
    MonitoredData   capnostatStatus_;
    MonitoredData   endTidalCO2_;
    MonitoredData   inspiredCO2_;
    MonitoredData   volumeCO2ExpiredPerMinute_;
    MonitoredData   mixedExpiredCO2_;
    MonitoredData   startOfInspirationMark_;
    MonitoredData   startOfExpirationMark_;
    MonitoredData   inspiredTime_;
    MonitoredData   expiredTime_;
    MonitoredData   capnostatZsb_;
    Uint32          mercurySoftwareOptions_;
    Boolean         isCapnostatZeroed_;
    Boolean         isCapnostatZeroRequested_;
    Uint8           previousCapnostatStatus_;
#endif // defined(VCO2_STUDY)

};

#endif	// ! ProxiInterface_HH
