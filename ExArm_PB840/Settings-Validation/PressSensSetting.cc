#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PressSensSetting - Pressure Sensitivity Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the pressure sensitivity
//  level, which is the pressure drop below baseline that is required to
//  begin a patient-initiated breath (either mandatory or spontaneous) when
//  trigger type is pressure trigger, as measured in the exhalation limb.
//  This class inherits from 'BoundedSetting' and provides the specific
//  information needed for representation of pressure sensitivity.  This
//  information includes the interval and range of this setting's values,
//  and this batch setting's new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PressSensSetting.ccv   25.0.4.0   19 Nov 2013 14:27:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 003   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "PressSensSetting.hh"
#include "TriggerTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[02233] -- The setting's range ...
//  $[02236] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    0.1f,		// absolute minimum...
    0.0f,		// unused...
    TENTHS,		// unused...
    NULL
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    20.0f,		// absolute maximum...
    0.1f,		// resolution...
    TENTHS,		// precision...
    &::LAST_NODE_
  };


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PressSensSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, the value interval
//  is initialized.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PressSensSetting::PressSensSetting(void)
      : BatchBoundedSetting(SettingId::PRESS_SENS,
			    Setting::NULL_DEPENDENT_ARRAY_,
			    &::INTERVAL_LIST_,
			    PRESS_SENS_MAX_ID,	// maxConstraintId...
			    PRESS_SENS_MIN_ID)	// minConstraintId...
{
  CALL_TRACE("PressSensSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PressSensSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PressSensSetting::~PressSensSetting(void)
{
  CALL_TRACE("~PressSensSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
PressSensSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  const Setting*  pTriggerType =
			SettingsMgr::GetSettingPtr(SettingId::TRIGGER_TYPE);

  DiscreteValue  triggerTypeValue;
  
  switch (qualifierId)
  {
  case Notification::ACCEPTED :		// $[TI1]
    triggerTypeValue = pTriggerType->getAcceptedValue();
    break;
  case Notification::ADJUSTED :		// $[TI2]
    triggerTypeValue = pTriggerType->getAdjustedValue();
    break;
  default :
    AUX_CLASS_ASSERTION_FAILURE(qualifierId);
    break;
  }

  return((triggerTypeValue == TriggerTypeValue::PRESSURE_TRIGGER)
	   ? Applicability::CHANGEABLE		// $[TI3]
	   : Applicability::INAPPLICABLE);	// $[TI4]
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a constant for the new patient value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
PressSensSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;
  
  // $[02234] The setting's new-patient value ...
  newPatient.value     = 2.0f;
  newPatient.precision = TENTHS;

  return(newPatient);   // $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
PressSensSetting::SoftFault(const SoftFaultID  softFaultID,
			    const Uint32       lineNumber,
			    const char*        pFileName,
			    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  PRESSURE_SENS_SETTING, lineNumber, pFileName,
			  pPredicate);
}
