#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ExhFlowSensor - Implements the expiratory flow sensors.
//---------------------------------------------------------------------
//@ Interface-Description
//      The class contains methods for converting the raw ADC count
//      to flow (LPM) and updating the processed value (filtered,
//      slope, filtered slope), for correcting the exhalation flow
//      to dry flow, for reading the processed values, and for
//      initializing the filtered values.
//
//      The flow sensors are used by breath delivery and service mode
//      to measure the actual flow.  Methods are implemented to update the
//		raw counts to flow (LPM) and update the filtered, slope, and filtered
//		slope values; to get the filtered value; to get the slope value; to
//      get the filtered slope value; to initialize the filtered,
//		slope, filteredSlope, and previous values; to set the conversion
//		factor used to convert the exhalation flow to dry; and to set the
//		mix factor used to compensate exhalation flow for o2 concentration.
//
//		This class provides support for the BD_CPU and the GUI_CPU.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of flow sensors.
//---------------------------------------------------------------------
//@ Implementation-Description
//      All the calculation for the conversion from ADC counts to
//      flow reading is done in the method rawToEngValue_().
//		A temperature sensor is also associated
//      with the flow sensor for temperature compensation.  The class
//      ProcessedValue is used to handle further processing of the flow
//      reading as required.  A correction to dry gas and O2 mix is needed
//      for the exhalation flow measurements.
//
// $[00401] $[04054]
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/ExhFlowSensor.ccv   25.0.4.0   19 Nov 2013 13:54:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004  By: syw     Date:  16-Sep-1997    DR Number: 2427
//  	Project:  Sigma (840)
//		Description:
//			Added requirement traceability.
//
//	Revision: 003  By:  syw    Date:  15-Aug-1997    DR Number: DCS 2386
//      Project:  Sigma (R8027)
//      Description:
//			Define unoffsettedFlow_ and unoffsettedProcessedValues_ value.
//
//	Revision: 002  By:  syw    Date:  03-Jul-1997    DR Number: DCS 2282
//      Project:  Sigma (R8027)
//      Description:
//			Eliminate calTemp since it is now fixed.  Removed all spiro references.
//			Eliminate references to flowTemperature_.
//
//	Revision: 001  By:  syw    Date:  08-Apr-1997    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "ExhFlowSensor.hh"

//@ Usage-Classes

#include "TemperatureSensor.hh"

#if defined (SIGMA_BD_CPU)

#include "CalInfoRefs.hh"
#include "FlowSensorOffset.hh"

#endif // defined (SIGMA_BD_CPU)

//@ End-Usage

#ifdef SIGMA_DEBUG
#include <stdio.h>
#endif //SIGMA_DEBUG

static const Real32 INITIAL_MIX = 21.0F ;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ExhFlowSensor()
//
//@ Interface-Description
//      Constructor.  This method takes six arguments, SafetyNetSensorData *,
//		adcId, 2 FlowSensorCalInfo *, TemperatureSensor&, alpha for
//		the BD_CPU.  The GUI_CPU has 2 FlowSensorCalInfo *, and
//		TemperatureSensor& as arguments.  This method has no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The data members are initialized to values passed in.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

ExhFlowSensor::ExhFlowSensor(
// E600 BDIO  SafetyNetSensorData *pData,
  const AdcChannels::AdcChannelId adcId,
  TemperatureSensor& temperatureSensor,
  const Real32 alpha,
  FlowSensorCalInfo *pFlowSensorCalInfo,
  FlowSensorCalInfo *pSecondaryFlowSensorCalInfo
) : FlowSensor( pData, adcId, temperatureSensor, alpha, pFlowSensorCalInfo),	// $[TI1]
    pSecondaryFlowSensorCalInfo_(pSecondaryFlowSensorCalInfo),
    dryProcessedValues_( alpha),
    unoffsettedProcessedValues_( alpha)
{
	CALL_TRACE("ExhFlowSensor::ExhFlowSensor( SafetyNetSensorData *pData \
				const AdcChannels::AdcChannelId adcId, \
				TemperatureSensor& temperatureSensor, \
				const Real32 alpha, \
				FlowSensorCalInfo *pFlowSensorCalInfo, \
				FlowSensorCalInfo *pSecondaryFlowSensorCalInfo)") ;

	// $[TI2]

	dryFactor_ = 1.0 ;
	o2Percent_ = INITIAL_MIX ;
}

#else

ExhFlowSensor::ExhFlowSensor(
  TemperatureSensor& temperatureSensor,
  FlowSensorCalInfo *pFlowSensorCalInfo,
  FlowSensorCalInfo *pSecondaryFlowSensorCalInfo
) : FlowSensor( temperatureSensor, pFlowSensorCalInfo),		// $[TI3]
    pSecondaryFlowSensorCalInfo_(pSecondaryFlowSensorCalInfo)
{
	CALL_TRACE("ExhFlowSensor::ExhFlowSensor( \
				TemperatureSensor& temperatureSensor, \
				FlowSensorCalInfo *pFlowSensorCalInfo, \
				FlowSensorCalInfo *pSecondaryFlowSensorCalInfo)") ;

	o2Percent_ = INITIAL_MIX ;
}

#endif // defined (SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ExhFlowSensor()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
ExhFlowSensor::~ExhFlowSensor( void)
{
	CALL_TRACE("ExhFlowSensor::~ExhFlowSensor( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValue()
//
//@ Interface-Description
//      This method take no arguments and converts the raw sample to
//		engineering units and update the processed values (slope, filter,
//		etc.).
//---------------------------------------------------------------------
//@ Implementation-Description
//      Convert the raw data into flow (LPM).  Correct for dry
//      gas (exhalation dry flow).  Update the filtered and slope values.
//		$[04326]
//---------------------------------------------------------------------
//@ PreCondition
//		None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================

#if defined (SIGMA_BD_CPU)

void
ExhFlowSensor::updateValue( void)
{
	CALL_TRACE("ExhFlowSensor::updateValue( void)") ;

	// $[TI1]

	Sensor::updateValue() ;
	Real32 airSide = engValue_ ;

	// use temp pointer to store original pointer while we modify it to
	// obtain the o2 cal table reading
	FlowSensorCalInfo *pTemp = pFlowSensorCalInfo_ ;
	pFlowSensorCalInfo_ = pSecondaryFlowSensorCalInfo_ ;

	Real32 o2Side = rawToEngValue_( rawCount_) ;

	// restore pointer
	pFlowSensorCalInfo_ = pTemp ;

	Real32 mixFlow = (airSide * (100.0F - o2Percent_) + o2Side * (o2Percent_ - 21.0F))
					/ 79.0F ;

	Uint32 airFlow = (Uint32)((100.0F - o2Percent_) * mixFlow / 79.0) ;
	Uint32 o2Flow = (Uint32)((o2Percent_ - 21.0) * mixFlow / 79.0) ;

	unoffsettedFlow_ = mixFlow ;

// TODO E600 do we need this?
// TODO E600 BDIO     mixFlow = mixFlow - RAirSideFlowSensorOffset.getOffset( airFlow)
// TODO E600 BDIO                 - RO2SideFlowSensorOffset.getOffset(o2Flow) ;

	engValue_ = mixFlow ;
	dryFlow_ = mixFlow * dryFactor_ ;

    processedValues_.updateValues( engValue_) ;
    dryProcessedValues_.updateValues( dryFlow_) ;
    unoffsettedProcessedValues_.updateValues( unoffsettedFlow_) ;
}

#endif // defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValue
//
//@ Interface-Description
//		This method has the flow and temperature values in counts as
//		arguments and returns the corresponding flow value in lpm.
//		This method differs from the updateValue( void) in that the rawCount
//		is passed in instead of being obtained by the AdcChannels class.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Update the flowTemperature_ value before rawToEngValue_ uses the
//		value.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

#if defined (SIGMA_GUI_CPU)

Real32
ExhFlowSensor::updateValue( const AdcCounts flowCounts, const AdcCounts tempCounts)
{
	CALL_TRACE("ExhFlowSensor::updateValue( const AdcCounts flowCounts, \
				const AdcCounts tempCounts)") ;

    // $[TI2]

	rTemperatureSensor_.updateValue( tempCounts) ;

	Real32 airSide = rawToEngValue_( flowCounts) ;

	FlowSensorCalInfo *pTemp = pFlowSensorCalInfo_ ;
	pFlowSensorCalInfo_ = pSecondaryFlowSensorCalInfo_ ;

	// obtain reading for o2 side
	Real32 o2Side = rawToEngValue_( flowCounts) ;

	// restore pointer
	pFlowSensorCalInfo_ = pTemp ;

	Real32 mixFlow = (airSide * (100.0F - o2Percent_) + o2Side * (o2Percent_ - 21.0F))
					/ 79.0F ;

	return( mixFlow) ;
}

#endif // defined (SIGMA_GUI_CPU)



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
ExhFlowSensor::SoftFault( const SoftFaultID  softFaultID,
                       const Uint32       lineNumber,
                       const char*        pFileName,
                       const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;

    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, EXHFLOWSENSOR, lineNumber,
                             pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================




