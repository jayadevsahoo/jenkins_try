####################################################################
#
#  $Header:   /840/Baseline/GUI-Applications/vcssrc/makefile.__v   25.0.4.0   19 Nov 2013 14:08:48   pvcs  $
#
####################################################################

SHELL = /bin/sh
CURRENT_SUBSYSTEM=GUI-Applications

ifeq "$(PLATFORM)" "SUN"
REQUIRED_SUBSYSTEMS=\
                    Sys-Init \
                    GUI-Foundations \
                    GUI-Applications \
                    Patient-Data \
                    Service-Data \
                    Network-Application \
                    Settings-Validation \
                    Trend-Database \
                    Persistent-Objects \
                    Alarm-Analysis \
                    Utilities \
                    Repository \
                    Foundation \
                    Kernel \
                    POST \
                    POST-Library \
                    Safety-Net \
                    OS-Foundation \
                    Service-Mode  \
                    GUI-Serial-Interface
endif

CPU_TYPE	= GUI_CPU
OPTIMIZE	= ON
ARM_COMPLIANCE	= ON

include ../../makedefs.gmake   # up two directories so can be used locally

#CCFLAGS =	-DCT_65545 -DXTFUNCPROTO -DSIGMA_GUI -DSIGMA_PAV


ifeq "$(IS_LOCAL_BUILD)" "TRUE"
INCL    +=  -I$(LOCAL_DIR)/.. \
			-I$(LOCAL_DIR)/Alarm-Analysis/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/Patient-Data/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/Settings-Validation/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/GUI-Foundations/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/Download/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/VGA-Graphics-Driver/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/GUI-IO-Devices/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/BD-IO-Devices/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/Operating-System/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/Breath-Delivery/$(CURR_DIR_NAME) \
			-I$(LOCAL_DIR)/Build/$(CURR_DIR_NAME)
endif

# only resides on GUI side of the house,
# so build "nothing" for the BD side
ifeq "$(CPU_TYPE)" "GUI_CPU"
	SRCS=$(wildcard *.cc)
	OBJS=$(patsubst %.cc,$(OBJ_DIR_NAME)/%.o,$(SRCS))
	PROG = $(LIB_TARGET)
else
	SRCS=
	OBJS=
nobuild :
endif

STRING_FILE_PREFIX?=English

ifeq "$(LANGUAGE)" "ENGLISH"
  STRING_FILE_PREFIX=English
endif
ifeq "$(LANGUAGE)" "ENGLISH_US"
  STRING_FILE_PREFIX=English
endif
ifeq "$(LANGUAGE)" "ENGLISH_NON_US"
  STRING_FILE_PREFIX=English
endif
ifeq "$(LANGUAGE)" "FRENCH"
  STRING_FILE_PREFIX=French
endif
ifeq "$(LANGUAGE)" "GERMAN"
  STRING_FILE_PREFIX=German
endif
ifeq "$(LANGUAGE)" "ITALIAN"
  STRING_FILE_PREFIX=Italian
endif
ifeq "$(LANGUAGE)" "SPANISH"
  STRING_FILE_PREFIX=Spanish
endif
ifeq "$(LANGUAGE)" "PORTUGUESE"
  STRING_FILE_PREFIX=Portuguese
endif
ifeq "$(LANGUAGE)" "JAPANESE"
  STRING_FILE_PREFIX=Japanese
endif
ifeq "$(LANGUAGE)" "POLISH"
  STRING_FILE_PREFIX=Polish
endif
ifeq "$(LANGUAGE)" "RUSSIAN"
  STRING_FILE_PREFIX=Russian
endif

RAW_STRING_FILES=$(wildcard $(STRING_FILE_PREFIX)*Strs.LA)
HEATED_STRING_FILES=$(patsubst %.LA,%.IN,$(RAW_STRING_FILES))
COOKED_STRING_FILES=$(patsubst %.IN,%.in,$(HEATED_STRING_FILES))

ifeq "$(LANGUAGE)" "JAPANESE"
  FINAL_STRING_FILES=EnglishHelpStrs.in $(COOKED_STRING_FILES)
else
  FINAL_STRING_FILES=$(COOKED_STRING_FILES)
endif

$(info FINAL_STRING_FILES = $(FINAL_STRING_FILES))
.PHONY: stringfiles

# here is your specified library's dependencies...
$(LIB_TARGET) : stringfiles $(OBJS)

all:
	$(MAKE) --jobs 20 -k MODE=$(MODE) PLATFORM=TARGET CPU_TYPE=GUI_CPU LANGUAGE=$(LANGUAGE)

stringfiles : $(FINAL_STRING_FILES)

all_depend:
	$(MAKE) MODE=$(MODE) PLATFORM=TARGET CPU_TYPE=GUI_CPU depend

update : # get the latest source...
	@get -r"$(VERSION)" -n \
	    $(wildcard $(PROJECT_DIR)/$(CURRENT_SUBSYSTEM)/vcssrc/*.??v)

clean : # clear your object files...
	@echo "Cleaning the intermediate language files... 
	rm -f *.IN $(STRING_FILE_PREFIX)*Strs.in StringProcessFile*.sed
	@echo "Cleaning the object file directory..."
	rm -f $(OBJ_DIR_NAME)/*.$(OBJEXT)

depend :
	@$(MAKEDEPEND)

# rule to make a '.IN' file from a '.LA' file...
%.IN : %.LA
	@echo "[$(CPU_TYPE),$(PLATFORM)]:  Supplementing $<..."
	@rm -f $@
	@chmod +x supplementLanguage.x
	@supplementLanguage.x $(LANGUAGE) $< -y > /dev/null

# rule to make a '.in' file from a '.IN' file...
%.in : %.IN
	@echo "[$(CPU_TYPE),$(PLATFORM)]:  Filtering $<..."
	@rm -f $@
	@chmod +x processStrings.x
	@processStrings.x $(LANGUAGE) $< -y > /dev/null


$(OBJ_DIR_NAME)/AlarmStrs.o : $(STRING_FILE_PREFIX)AlarmStrs.in
$(OBJ_DIR_NAME)/MiscStrs.o : $(STRING_FILE_PREFIX)MiscStrs.in
$(OBJ_DIR_NAME)/PromptStrs.o : $(STRING_FILE_PREFIX)PromptStrs.in
ifeq "$(LANGUAGE)" "JAPANESE"
EnglishHelpStrs.IN : EnglishHelpStrs.LA
EnglishHelpStrs.in : EnglishHelpStrs.IN
$(OBJ_DIR_NAME)/HelpStrs.o : JapaneseHelpStrs.in EnglishHelpStrs.in
else
$(OBJ_DIR_NAME)/HelpStrs.o : $(STRING_FILE_PREFIX)HelpStrs.in
endif


$(STRING_FILE_PREFIX)AlarmStrs.in  : $(STRING_FILE_PREFIX)AlarmStrs.IN
$(STRING_FILE_PREFIX)HelpStrs.in   : $(STRING_FILE_PREFIX)HelpStrs.IN
$(STRING_FILE_PREFIX)MiscStrs.in   : $(STRING_FILE_PREFIX)MiscStrs.IN
$(STRING_FILE_PREFIX)PromptStrs.in : $(STRING_FILE_PREFIX)PromptStrs.IN

$(STRING_FILE_PREFIX)AlarmStrs.IN  : $(STRING_FILE_PREFIX)AlarmStrs.LA
$(STRING_FILE_PREFIX)HelpStrs.IN   : $(STRING_FILE_PREFIX)HelpStrs.LA
$(STRING_FILE_PREFIX)PromptStrs.IN : $(STRING_FILE_PREFIX)PromptStrs.LA
$(STRING_FILE_PREFIX)MiscStrs.IN   : $(STRING_FILE_PREFIX)MiscStrs.LA

ifneq "" "$(wildcard .makedepend.$(OBJ_DIR))"
  include .makedepend.$(OBJ_DIR)
endif
