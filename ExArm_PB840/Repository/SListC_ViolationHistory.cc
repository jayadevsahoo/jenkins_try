#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SListC_ViolationHistory - Singly-Linked List of 'ViolationHistory' Items.
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides an abstraction for a singly-linked list of generic
//  data items each of type 'ViolationHistory'.  The data items of the list are
//  copies of the data items that are added to the list.  There are
//  non-constant, public methods to iterate through the contents of
//  the list, and there's also an iterator class (see
//  'SIterC_ViolationHistory') to allow iteration of a constant list.  This
//  is an abstract base class and is used with 'FixedSListC<ViolationHistory,SIZE>'.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a standard abstraction for storing data items
//  in a dynamic collection, where initial placement within the
//  collection is quick and easy.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each list has an internal heap -- which is provided by the derived
//  class during construction -- containing singly-linked nodes
//  ('SNodeC_ViolationHistory').  The nodes are allocated from the heap to
//  contain copies of the inserted data.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertion macros are used to ensure
//  correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions have to do with the data type that is chosen for
//  this list.  The data type must provide the following mechanisms,
//  to be allowed to be used in this list:
//>Von
//              ViolationHistory::ViolationHistory(const ViolationHistory& aType);             // copy...
//              ViolationHistory::operator=(const ViolationHistory& aType);        // assignment...
//              ViolationHistory::operator==(const ViolationHistory& aType) const; // equivalence...
//              ViolationHistory::operator!=(const ViolationHistory& aType) const; // inequivalence...
//>Voff
//  For primitive (built-in) types this is already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      The assignment operator of any class used as a type of this list
//      must be implemented such that upon completion of an assignment
//      between two instances, the instances are equivalent (i.e.
//      'a = b; POST_CONDITION(a == b);').
//---------------------------------------------------------------------
//@ Invariants
//  ((pFirst_ == NULL  &&  pCurrent_ == NULL)  ||
//    (isLegalNode_(pFirst_)  &&  isLegalNode_(pCurrent_)))
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListC.C_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SListC_ViolationHistory.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SListC_ViolationHistory()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this list and each of its nodes.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The 'rHeapOfNodes_' alias can NOT be used from within this
//  destructor, because by the time this destructor is called
//  the derived class's destructor has already destroyed the heap.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SListC_ViolationHistory::~SListC_ViolationHistory(void)
{
  CALL_TRACE("~SListC_ViolationHistory()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  search(item, startFrom)
//
//@ Interface-Description
//  Search the current list for a node that is equivalent to 'item'.
//  'startFrom' indicates whether to start the search from the
//  first node ('FROM_FIRST'), or the current node ('FROM_CURRENT').
//  If an equivalence is found, then 'SUCCESS' is returned, and the
//  current node is left pointing to the matching node.  If no
//  equivalence is found, then 'FAILURE' is returned, and the list
//  is left unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Near the end of this method a pointer to a constant node,
//  'pFoundNode', has its const-ness cast away.  This is due to the
//  fact that 'findNode_()' is a constant method (to allow its use
//  in constant public methods) that returns a pointer.  To ensure
//  consistent const-ness of 'findNode_()', the pointer that is
//  returned is a pointer to a CONSTANT node.  The const-ness of
//  the returned node is not necessary in the design of 'findNode_()',
//  but rather a consistency of design of that method.  Therefore,
//  when this method casts the const-ness of 'pFoundNode' away, it
//  is an acceptable case of working properly with a proper design.
//---------------------------------------------------------------------
//@ PreConditions
//  (startFrom == FROM_FIRST  ||  startFrom == FROM_CURRENT)
//---------------------------------------------------------------------
//@ PostConditions
//  (status == SUCCESS) ==> (currentItem() == item)
//  (status == FAILURE) ==> (isEmpty()  ||
//               currentItem() == OLD{currentItem()})
//@ End-Method
//=====================================================================

SigmaStatus
SListC_ViolationHistory::search(const ViolationHistory&      item,
			     const SearchFrom startFrom)
{
  CALL_TRACE("search(item, startFrom)");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore search for a node that contains
    // an item that is equivalent to 'item'...
    const SNodeC_ViolationHistory*  pFoundNode = NULL;

    // set 'pFoundNode' to the node that this search is to begin the search
    // from...
    switch (startFrom)
    {
    case FROM_FIRST:		// $[TI1.1]
      pFoundNode = pFirst_;
      break;
    case FROM_CURRENT:		// $[TI1.2]
      pFoundNode = pCurrent_;
      break;
    case FROM_LAST:
    default:
      AUX_CLASS_ASSERTION_FAILURE(((startFrom     << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(ViolationHistory)));	    // bottom 12 bits...
      break;
    }

    // 'pFoundNode' doesn't contain an item that is equivalent to 'item',
    // therefore use 'findNode_()' to search, starting from
    // 'pFoundNode'...
    pFoundNode = findNode_(item, pFoundNode);

    if (pFoundNode != NULL)
    {   // $[TI1.3]
      // the matching node is found, therefore set the current node to the
      // found node...
      pCurrent_ = (SNodeC_ViolationHistory*)pFoundNode; // "const cast-away:"...
      status    = SUCCESS;
    }   // $[TI1.4] -- no match was found...
  }   // $[TI2] -- this list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  insert(newItem, insertPlace)
//
//@ Interface-Description
//  Insert 'newItem' before/after the current list node.  If
//  'insertPlace' is 'AFTER_CURRENT' then insert after the current
//  node, otherwise if 'insertPlace' is 'BEFORE_CURRENT', insert BEFORE
//  the current node. (If the list is empty, then 'insertPlace' is ignored.)
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//  (insertPlace == AFTER_CURRENT  ||  insertPlace == BEFORE_CURRENT)
//---------------------------------------------------------------------
//@ PostConditions
//  (currentItem() == newItem)
//@ End-Method
//=====================================================================

void
SListC_ViolationHistory::insert(const ViolationHistory&          newItem,
			     const InsertionPlace insertPlace)
{
  CALL_TRACE("insert(newItem, insertPlace)");
  SAFE_CLASS_PRE_CONDITION((!isFull())); // checked by 'new (heap) ...'

  SNodeC_ViolationHistory*  pNewNode;

  // allocate a new node from this list's heap that is initialized with
  // 'newItem'...
  pNewNode = new (rHeapOfNodes_) SNodeC_ViolationHistory(newItem);

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore add 'pNewNode' to a location based
    // on 'insertPlace'...
    switch (insertPlace)
    {
    case AFTER_CURRENT:		// $[TI1.1]
      // insert 'pNewNode' AFTER 'pCurrent_'...
      pNewNode->pNext_ = pCurrent_->pNext_;
      pCurrent_->pNext_ = pNewNode;
      break;
    case BEFORE_CURRENT:	// $[TI1.2]
      // insert 'pNewNode' BEFORE 'pCurrent_'...
      if (pCurrent_ == pFirst_)
      {   // $[TI1.2.1]
	// 'pCurrent_' is at the front of the list...
	pFirst_ = pNewNode;
      }
      else
      {   // $[TI1.2.2]
	// insert 'pNewNode' between 'pCurrent_' and the node just
	// before 'pCurrent_'...
	(getPrevNode_(pCurrent_))->pNext_ = pNewNode;
      }

      pNewNode->pNext_ = pCurrent_;
      break;
    default:
      AUX_CLASS_ASSERTION_FAILURE(((insertPlace   << 22) |  // top 10 bits...
				   (getMaxItems() << 12) |  // middle 10 bits...
				   sizeof(ViolationHistory)));	    // bottom 12 bits...
      break;
    }
  }
  else
  {   // $[TI2]
    // this list is empty, therefore add 'pNewNode' as the first--and
    // only--node in this list...
    pFirst_ = pNewNode;
  }

  // set the current node to the newly inserted node...
  pCurrent_ = pNewNode;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  append(newItem)
//
//@ Interface-Description
//  Append 'newItem' to the end of this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the current node to the last node in this list, then use
//  'insert()' to add a new node after the current node.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//---------------------------------------------------------------------
//@ PostConditions
//  (isAtLast()  &&  currentItem() == newItem)
//@ End-Method
//=====================================================================

void
SListC_ViolationHistory::append(const ViolationHistory& newItem)
{
  CALL_TRACE("append(newItem)");
  SAFE_CLASS_PRE_CONDITION((!isFull()));    // checked by 'insert()'...

  if (!isEmpty())
  {   // $[TI1]
    // set 'pCurrent_' to the last node...
    pCurrent_ = getPrevNode_(NULL);
  }   // $[TI2]

  // insert a node containing 'newItem' after the current node...
  insert(newItem, AFTER_CURRENT);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  prepend(newItem)
//
//@ Interface-Description
//  Prepend 'newItem' to the front of this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Set the current node to the first node in this list, then use
//  'insert()' to add a new node before the current node.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//---------------------------------------------------------------------
//@ PostConditions
//  (isAtFirst()  &&  currentItem() == newItem)
//@ End-Method
//=====================================================================

void
SListC_ViolationHistory::prepend(const ViolationHistory& newItem)
{
  CALL_TRACE("prepend(newItem)");
  SAFE_CLASS_PRE_CONDITION((!isFull()));    // checked by 'insert()'...

  if (!isEmpty())
  {   // $[TI1]
    // set 'pCurrent_' to the first node...
    pCurrent_ = pFirst_;
  }   // $[TI2]

  // insert a node containing 'newItem' before the current node...
  insert(newItem, BEFORE_CURRENT);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeCurrent()
//
//@ Interface-Description
//  Remove the current node from the list, and return 'SUCCESS'.  If
//  the list is empty, then 'FAILURE' is returned, and this list is
//  left unchanged.  The new current is set to the node immediately
//  after the current node.  If the current node is at the end of the
//  list, then the new current is the previous node.  If the current
//  node is the only node in the list, then the list is left empty.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SigmaStatus
SListC_ViolationHistory::removeCurrent(void)
{
  CALL_TRACE("removeCurrent()");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore remove the current node...
    SNodeC_ViolationHistory*  pOldCurr = pCurrent_;  // store the current node...

    if (pFirst_ == pCurrent_)
    {   // $[TI1.1]
      // the current node is the first node in this list, therefore remove
      // the first node in this list, and  move the first and current nodes
      // to the next node, if any...
      pFirst_   = pCurrent_->pNext_;
      pCurrent_ = pFirst_;
    }
    else
    {   // $[TI1.2]
      // the current node is NOT the first node in this list, therefore
      // get the node that is immediately previous to the current node...
      SNodeC_ViolationHistory*  pPrevNode = getPrevNode_(pCurrent_);

      // remove the current node from the linked list by "by-passing" it...
      pPrevNode->pNext_ = pCurrent_->pNext_;

      // the new current pointer is either the next node, or, if there is
      // no next node, the previous node...
      pCurrent_ = (pCurrent_->pNext_ != NULL)
		   ? pCurrent_->pNext_		// $[TI1.2.1]
		   : pPrevNode;			// $[TI1.2.2]
    }

    // deallocate the stored current node...
    deallocNode_(pOldCurr);
    status = SUCCESS;
  }   // $[TI2] -- this list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeItem(item)
//
//@ Interface-Description
//  Remove the first node in this list that is equivalent to 'item',
//  and return 'SUCCESS'.  If there is no items that are equivalent
//  to 'item' in this list, then return 'FAILURE', and leave this
//  list unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method uses 'search()' to find 'item' and set the current
//  node to it, then uses 'removeCurrent()' to remove the current
//  node.  This method returns 'SUCCESS' when 'removeCurrent()' is
//  called.  The new current is set to the node immediately
//  after the current node.  If the removed node is at the end of the
//  list, then the new current is the previous node.  If the removed
//  node is the only node in the list, then the list is left empty.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SigmaStatus
SListC_ViolationHistory::removeItem(const ViolationHistory& item)
{
  CALL_TRACE("removeItem(item)");

  // if this list is not empty and a search for an equivalent node is
  // successful, then remove the node that was found in the search,
  // otherwise return 'FAILURE'...
  return((!isEmpty()  &&  search(item) == SUCCESS)
	  ? removeCurrent()		// $[TI1]
	  : FAILURE);			// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  clearList()
//
//@ Interface-Description
//  Clear out this list, and leave this list as an empty list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Step through each node in this list and deallocate them.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

void
SListC_ViolationHistory::clearList(void)
{
  CALL_TRACE("clearList()");

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore, starting from the first node,
    // deallocate each node in this list...
    SNodeC_ViolationHistory*  pNextNode = pFirst_;
    SNodeC_ViolationHistory*  pCurrNode = pFirst_;

    do
    { // while there are more nodes to deallocate...
      pNextNode = pNextNode->pNext_;	// store the next node...
      deallocNode_(pCurrNode);		// deallocate this node...
      pCurrNode = pNextNode;		// set this node to next node...
    } while (pCurrNode != NULL);

    // set this list to be empty...
    pFirst_   = NULL;
    pCurrent_ = NULL;
  }   // $[TI2] -- this list is already empty...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator=(sList)  [Assignment Operator]
//
//@ Interface-Description
//  Copy the items from 'sList' to this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This list is cleared out, then each item from 'sList' is copied
//  into newly allocated nodes for this list.
//---------------------------------------------------------------------
//@ PreConditions
//  (getMaxItems() >= sList.getMaxItems())
//---------------------------------------------------------------------
//@ PostConditions
//  (*this == sList  &&  (isEmpty()  ||
//                        currentItem() == sList.currentItem()))
//@ End-Method
//=====================================================================

void
SListC_ViolationHistory::operator=(const SListC_ViolationHistory& sList)
{
  CALL_TRACE("operator=(sList)");

  if (this != &sList)
  {   // $[TI1]
    // this list is not the same list as 'sList', therefore clear out
    // this list...
    clearList();

    if (!sList.isEmpty())
    {   // $[TI1.1]
      // 'sList' is not empty, therefore make sure that this list can
      // contain all of the items from 'sList', then do the copying...
      AUX_CLASS_PRE_CONDITION((getMaxItems() >= sList.getMaxItems()),
			    ((getMaxItems()       << 22) |  // top 10 bits...
			     (sList.getMaxItems() << 12) |  // middle 10 bits...
			     sizeof(ViolationHistory)));		    // bottom 12 bits...

      const SNodeC_ViolationHistory*  pOtherCurr = sList.pFirst_;
      SNodeC_ViolationHistory*        pNewCurr   = NULL;

      // step through each of the nodes from 'sList', and insert them into
      // this list...
      do
      {   // $[TI1.1.1] -- this ALWAYS executes...
	// insert the "current" item from 'sList' at the end of this list...
	insert(pOtherCurr->getItem(), AFTER_CURRENT);

	if (pOtherCurr == sList.getCurrent_())
	{   // $[TI1.1.1.1]
	  // save the corresponding 'pCurrent_', to later set up
	  // this list's 'pCurrent_' to be the same value as the
	  // current pointer of 'sList'...
	  pNewCurr = pCurrent_;
	}   // $[TI1.1.1.2]

	pOtherCurr = pOtherCurr->pNext_;
      } while (pOtherCurr != NULL);

      // set 'pCurrent_' to the corresponding current of 'sList'...
      pCurrent_ = pNewCurr;
    }   // $[TI1.2] -- 'sList' is empty...
  }   // $[TI2] -- assigning to itself...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//	                [static]
//
//@ Interface-Description
//	Report the software fault that occured within the source code
//	of this class.  The fault, indicated by 'softFaultID', occured
//	at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
SListC_ViolationHistory::SoftFault(const SoftFaultID softFaultID,
				const Uint32      lineNumber,
				const char*       pFileName,
				const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, ABSTRACT_SLISTC,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  findNode_(item, pStartNode)  [const]
//
//@ Interface-Description
//  Starting from 'pStartNode' search for the node that is equivalent
//  to 'item', then return the pointer to that node.  If no match
//  is found before the end of the list, then return 'NULL'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  (isLegalNode(pStartNode))
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

const SNodeC_ViolationHistory*
SListC_ViolationHistory::findNode_(const ViolationHistory&               item,
				const SNodeC_ViolationHistory* pStartNode) const
{
  CALL_TRACE("findNode_(item, pStartNode)");
  SAFE_CLASS_PRE_CONDITION((isLegalNode_(pStartNode)));

  // from 'pStartNode' compare each node's item with 'item' until an
  // equivalence is found or the end of this list is reached...
  while (pStartNode != NULL  &&  item != pStartNode->nodeItem_)
  {   // $[TI1] -- at least one iteration...
    pStartNode = pStartNode->pNext_;  // go to the next node...
  }   // $[TI2] -- no iterations...

  return(pStartNode);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isEquivTo_(sList)  [const, virtual]
//
//@ Interface-Description
//  Is this list equivalent to 'sList'?
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
SListC_ViolationHistory::isEquivTo_(const SListC_ViolationHistory& sList) const
{
  CALL_TRACE("isEquivTo_(sList)");

  Boolean isEquiv;

  // the two lists are equivalent if they are at the same address
  // location, OR they're both empty...$[TI1] (TRUE) $[TI2] (FALSE)
  isEquiv = (this == &sList  ||  (isEmpty()  &&  sList.isEmpty()));

  if (!isEquiv  &&  !isEmpty()  &&  !sList.isEmpty())
  {   // $[TI3]
    // both lists are NOT empty, therefore continue checking for
    // equivalence...
    const SNodeC_ViolationHistory*  pCurrNode1 = pFirst_;
    const SNodeC_ViolationHistory*  pCurrNode2 = sList.pFirst_;

    do
    {   // $[TI3.1]
      // while the items of the two nodes are equivalent, and the end of
      // either list has not been reached, check for equivalence...
      isEquiv = (pCurrNode1->nodeItem_ == pCurrNode2->nodeItem_);

      if (isEquiv)
      {   // $[TI3.1.1]
	// the items of these two nodes are equivalent, therefore move
	// both of them to the next node in their respective lists...
	pCurrNode1 = pCurrNode1->pNext_;
	pCurrNode2 = pCurrNode2->pNext_;
      }   // $[TI3.1.2] -- an item in-equivalence is found...
    } while (isEquiv  &&  pCurrNode1 != NULL  &&  pCurrNode2 != NULL);

    if (isEquiv)
    {   // $[TI3.2]
      // each item in this list is equivalent the corresponding item of
      // 'sList', make sure that they BOTH are now 'NULL'; the terminated
      // iteration ensures that at least one is 'NULL', therefore test
      // that they are both equal...$[TI3.2.1] (TRUE)  $[TI3.2.2] (FALSE)
      isEquiv = (pCurrNode1 == pCurrNode2);
    }   // $[TI3.3] -- an item in-equivalence is found...
  }   // $[TI4] -- both same list, OR at least one of the lists is empty...

  return(isEquiv);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  print_(ostr)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual that is to be overidden by any derived
//  classes, and defined to dump the contents of an instance to 'ostr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getPrevNode_(pStopNode)
//
//@ Interface-Description
//  Return the pointer of the node that is directly previous to
//  the pointer given by 'pStopNode'.  If 'pStopNode' is 'NULL',
//  then return a pointer to the last node in this list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  When finding the previous node of this list's LAST node, the
//  search will start from 'pCurrent' instead of from 'pFirst_'.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isEmpty()  &&  pFirst_ != pStopNode)
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SNodeC_ViolationHistory*
SListC_ViolationHistory::getPrevNode_(const SNodeC_ViolationHistory* pStopNode) const
{
  CALL_TRACE("getPrevNode_(pStopNode)");
  SAFE_CLASS_PRE_CONDITION((!isEmpty()  &&  pFirst_ != pStopNode));

  SNodeC_ViolationHistory*  pPrevNode;

  pPrevNode = (pStopNode != NULL) ? pFirst_	// $[TI1]
				  : pCurrent_;	// $[TI2]

  // when 'pStopNode' is 'NULL', then find the last node in this list...
  for (; pPrevNode->pNext_ != pStopNode; pPrevNode = pPrevNode->pNext_)
  {   // $[TI3] -- at least one iteration...
    // do nothing...
  }   // $[TI4] -- no iterations occur...

  return(pPrevNode);
}
