
#ifndef OscPcvPhase_HH
#define OscPcvPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OscPcvPhase - Implements OSC PCV phase.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OscPcvPhase.hhv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: syw     Date:  14-Feb-2000    DR Number: 5639
//  Project:  NeoMode
//  Description:
//		Added determineKi_() method.
//
//  Revision: 004  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 003 By: syw    Date: 26-Feb-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added determineTargetPressure_() method since Settings will not
//			update the setting to the proper value as initially assumed.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "PressureBasedPhase.hh"

//@ End-Usage

class OscPcvPhase : public PressureBasedPhase
{
  public:
    OscPcvPhase( void) ;
    virtual ~OscPcvPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
							   const Uint32      lineNumber,
							   const char*       pFileName  = NULL, 
							   const char*       pPredicate = NULL) ;
  
  protected:
	virtual void determineEffectivePressureAndBiasOffset_( void) ;
	virtual void determineTimeToTarget_( void) ;
	virtual void determineFlowAccelerationPercent_( void) ;
	virtual void enableExhalationTriggers_( void) ;
	virtual void determineKp_( void) ;
	virtual void determineKi_( void) ;
	virtual void updatePressureControllerWf_( void) ;
	virtual void determineTargetPressure_( void) ;

  private:
    OscPcvPhase( const OscPcvPhase&) ;		// not implemented...
    void   operator=( const OscPcvPhase&) ;	// not implemented...

} ;


#endif // OscPcvPhase_HH 
