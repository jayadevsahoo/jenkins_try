#pragma once


// ViewStoredResultsDlg dialog

class ViewStoredResultsDlg : public CDialog
{
	DECLARE_DYNAMIC(ViewStoredResultsDlg)

public:
	ViewStoredResultsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~ViewStoredResultsDlg();

// Dialog Data
	enum { IDD = IDD_VIEW_RESULTS_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CMonthCalCtrl m_DateTimeCtrl;
	afx_msg void OnMcnSelectMonthcalendar1(NMHDR *pNMHDR, LRESULT *pResult);
	
	int m_day;
	CString m_strSession;
	CString m_strFileName;
	afx_msg void OnBnClickedButtonShowResults();
};
