#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PavFilters - Filters used for PAV processing.
//---------------------------------------------------------------------
//@ Interface-Description
//	Three filters are provided: 1) statistical filter, 2) directional
//	filter, and 3) a slope estimator filter.  Methods are provided to
//	initialize and update the filters.
//---------------------------------------------------------------------
//@ Rationale
//	This class implements the algorithms required for filtering data.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The directional and statistical filters manage the past data..
//	The slope estimator is implemented as a free floating function (static)
//	that provides signal processing without any static memory.   The caller
//	is required to provide the static memory locations.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/PavFilters.ccv   10.7   08/17/07 09:40:24   pvcs  
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 001   By: syw   Date:  19-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV initial version.
//
//=====================================================================

#include "PavFilters.hh"

//@ Usage-Classes

#include <math.h>
#include "BD_IO_Devices.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PavFilters()  
//
//@ Interface-Description
//		Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	The statistical and directional filters are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PavFilters::PavFilters( void)
{
	CALL_TRACE("PavFilters::PavFilters( void)") ;

    // $[TI1]
	resetStatisticalFilter() ;
	resetDirectionalFilter( 0.0) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PavFilters()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PavFilters::~PavFilters(void)
{
	CALL_TRACE("PavFilters::~PavFilters(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: statisticalFilter()  
//
//@ Interface-Description
//		This method has the patient data value and the input data to filter
//		as arguments and returns either the input data or the filtered input
//		data depending on the error between the patient data and the input data.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	A circular buffer is used to store the output data for the standard
//		deviation computation.  A running sum and sum squared totals are kept
//		to minimize computation times for the standard deviation calculation.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
PavFilters::statisticalFilter( const Real32 patientData, const Real32 input)
{
	CALL_TRACE("PavFilters::statisticalFilter( const Real32 patientData, const Real32 input)") ;

	Real32 output ;
	
	// $[PA24023] 
	if (index_ == 0 && !bufferFull_)
	{
	    // $[TI1]
		// reset occurred, initialize output equal to input
		output = input ;
		filteredInput_ = input ;
		outputBuffer_[index_] = output ;
		index_++ ;
		sum_ = output ;
		sumSquared_ = output * output ;
	}
	else
	{
	    // $[TI2]
		const Real32 ALPHA = 0.75 ;
		filteredInput_ = ALPHA * filteredInput_ + (1.0F - ALPHA) * input ;

		if (index_ == 1 && !bufferFull_)
		{
		    // $[TI3]
			output = input ;
		}
		else
		{
		    // $[TI4]
			Real32 stdDev ;
			
			if (!bufferFull_)
			{
			    // $[TI5]
				stdDev = (sumSquared_ - sum_ * sum_ / index_) / (index_ - 1) ;
			}
			else
			{
			    // $[TI6]
				stdDev = (sumSquared_ - sum_ * sum_ / BUFF_SIZE) / (BUFF_SIZE - 1) ;
			}

			stdDev = (Real32) pow( stdDev, 0.5f) ;				

			if (ABS_VALUE( patientData - input) < 2 * stdDev + MAX_VALUE( 1.0, 0.1 * filteredInput_))
			{
			    // $[TI7]
				output = input ;
			}
			else
			{
			    // $[TI8]
				output = filteredInput_ ;
			}
		}

		// keep running totals fo sum_ and sumSquared_
		sum_ = sum_ - outputBuffer_[index_] + output ;
		sumSquared_ = sumSquared_ - outputBuffer_[index_] * outputBuffer_[index_] +
						output * output ;
		outputBuffer_[index_] = output ;

		if (++index_ >= BUFF_SIZE)
		{
		    // $[TI9]
			index_ = 0 ;
			bufferFull_ = TRUE ;
		}
	    // $[TI10]
	}
	return (output) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: directionalFilter()  
//
//@ Interface-Description
//		This method has the input value, alpha up and alpha down value of
//		the alpha filter as inputs and returns the alpha filtered value.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If the input is greater or equal than the previous output, alpha
//		up is used else alpha down is used in the alpha filter.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
PavFilters::directionalFilter( const Real32 input, const Real32 alphaUp,
								const Real32 alphaDown)
{
	CALL_TRACE("PavFilters::directionalFilter_( const Real32 input, \
					const Real32 alphaUp, const Real32 alphaDown)") ;
	
	// $[PA24024] 
	Real32 alpha ;
	
	if (input >= directionalFilterOutput_)
	{
	    // $[TI1]
		alpha = alphaUp ;
	}
	else
	{
	    // $[TI2]
		alpha = alphaDown ;
	}
	

	directionalFilterOutput_ = alpha * directionalFilterOutput_ + (1.0F - alpha) * input ;
	
	return( directionalFilterOutput_) ;
}
	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SlopeEstimator()  
//
//@ Interface-Description
//		This method has the input, a reference to the previous input, a
//		reference to the past output, a reference to the past 2 outputs,
//		and a gain as arguments and returns the estimated slope of the
//		input.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The estimated slope is computed and is returned.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
PavFilters::SlopeEstimator( const Real32 input, Real32 &input_1,
						   Real32 &output_1, Real32 &output_2,
						   const Real32 gain)
{
	CALL_TRACE("PavFilters::SlopeEstimator( const Real32 input, \
					Real32 &input_1, Real32 &output_1, \
					Real32 &output_2, const Real32 gain)") ;

	// $[PA24037]
    // $[TI1]

	const Real32 A = 31.416F * gain * gain ;
	const Real32 B = 62.832F * gain + 2.0F ;
	const Real32 C = 1.0F / (1.0F + (5.0F * gain * gain + 2.0F * gain) * 31.416F) ;

	Real32 output = (A * (input - input_1) + B * output_1 - output_2) * C ;
	input_1 = input ;
	output_2 = output_1 ;
	output_1 = output ;

	return( output) ;
}						   
	

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PavFilters::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PAVFILTERS,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================

