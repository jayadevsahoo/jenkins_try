/*****************************************************************************/
/*   Module Name:    COM.h                                                
/*   Purpose:      Header for Serial Communications Module                                 
/*                                                                          
/*                                                                           
/*   Requirements:                                                  
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/
/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef COM_H
#define COM_H

#include "ErrCodes.h"

// each device that uses this module must have a unique ID, ID must not be 0
#define UNIQUE_ID_PTS  1
#define UNIQUE_ID_840  2

// how many times to retransmit a message trying to get the remote to accept
#define MAX_COM_TRIES   5

#define TEST_STR_SIZE   64
#define MAX_COM_STR     256

#define ACK           0x06
#define NAK           0x15

#define MAX_SUPPORTED_COM_PORT  32  // support com port from 1 to 32

enum { ESTABLISH_COM_STARTING, ESTABLISH_COM_IN_PROGRESS, ESTABLISH_COM_SUCCESS, 
       ESTABLISH_COM_FAILURE, ESTABLISH_COM_NO_PORT };

// this function pointer is called to determine if the remote device's response 
// to a test string was correct.
//  msg - the message returned from the remote device
//  size - the size of the message
typedef int (*TestStrResponseValidFuncPtr)(char* msg, int size);

typedef struct ComStruct {
	char name[32];          // device name displayed to user
	int uniqueID;           // each port user must have unique ID

	// how long to wait for instrument to respond to a query
	double timeout;

	// how long to wait between trying different com speed/parity settings
	double delayBetweenComTries;

	//size, in bytes, of the input and output RS-232 message queue's
	int inQueueSize;
	int outQueueSize;

	// min and max values used in attempting a connection
	int portMax;            // 32 max 
	int portMin;            // 1 min 
	int baudMax;            // 256000 max
	int baudMin;            // 110 min
	int parityMax;          // 4 max (0=none, 1=odd, 2=even, 3=mark, 4=space)
	int parityMin;          // 0 min
	int databitsMax;        // 8 max
	int databitsMin;        // 5 min
	int stopbitsMax;        // 2 max
	int stopbitsMin;        // 1 min

	// if connection is made, this is what we connected at
	int port;
	int baud;
	int parity;
	int databits;
	int stopbits;
	int connected;     // 1 if we are connected, 0 otherwise

	// test string to try on instrument 
	char testStr[TEST_STR_SIZE];          // send this to instrument
	int testStrLen;
	int testStrResponseLen;

	// function to call to see if test string response was correct
	TestStrResponseValidFuncPtr pResponseValidFunc;

	// function to let UI know about com port detect status
	int (*pComDetectStatusFunc)(struct ComStruct *com, int status);

	char* registryKey;

	int autoDetectDisabled;  // if 0 autodetect remote device, otherwise don't
} COM_STRUCT;

// function to let UI know about com port detect status.
// Give caller the com structure defining the port currently trying to connect with,
//             and the connect status. 
//  com    - the com structure defining the port currently under detection
//  status - ESTABLISH_COM_IN_PROGRESS if the port is actively being detected, 
//           ESTABLISH_COM_SUCCESS if the port has connected successfully and 
//           ESTABLISH_COM_FAILURE if the detection process failed to find remote device
//  return - the called function should return:
//            0 if detection should continue,
//            1 terminates the detection process.
typedef int (*ComDetectStatusFuncPtr)(COM_STRUCT *, int status);

extern char *(szComParity[]);

// public functions
int  ComEstablish(COM_STRUCT*, int noAutoDetect);
void ComResetEstablish(void);
int  ComSend(char*, int, COM_STRUCT*);
int  ComRecv(char*, int, int*, COM_STRUCT*);
int  ComTerminate(COM_STRUCT*);
int  ComFlushInQ( COM_STRUCT*);
int  CharsInQ(COM_STRUCT* com);
int  ComFlushOutQ(COM_STRUCT*);
unsigned long ComFindAllValidPorts(void);
int  ReadComConfig( COM_STRUCT* com);
int  WriteComConfig(COM_STRUCT* com);

// private functions
static int Connect(COM_STRUCT* com);
static int ConnectAllPortParams(COM_STRUCT* com);
static int TryTestStr(COM_STRUCT* com);
static void UpdateConnectPanel(COM_STRUCT* com);

#endif //COM_H
