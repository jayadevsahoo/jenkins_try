#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LaptopEventCascade - A LaptopEventTarget which stores a list
// of LaptopEventTargets.  Allows multiple LaptopEventTargets to
// register for laptop request.
//---------------------------------------------------------------------
//@ Interface-Description
// LaptopEventCascade inherits directly from LaptopEventTarget so it can be a
// target for laptop requests.  The idea is that multiple objects may want to
// be informed when one laptop request happens so this class accomplishes
// that by storing an array of LaptopEventTargets.  Targets can be registered
// via the addTarget() method.
//
// The cascade can then be registered with the particular request and, when it
// receives notification of an laptop request via its laptopRequestHappened()
// and laptopFailureHappened() methods, it then calls the laptopRequestHappened()
// or laptopFailureHappened() method of each object that has registered with it.
// Hence, the change is cascaded to all interested objects.
//
// LaptopEventCascade is intended for use by the LaptopEventRegistrar class.
//---------------------------------------------------------------------
//@ Rationale
// Needed for multiple objects to be notified when a laptop request happens
//---------------------------------------------------------------------
//@ Implementation-Description
// The cascade stores a list of LaptopEventTarget pointers.  As objects register
// with the cascade (via addTarget()) pointers to the objects are stored in the
// array (called targets_[]).  If the cascade is registered as a target of a
// laptop request then it will receive request notice via the laptopRequestHappened()
// or laptopFailureHappened() method.  This method just loops through the pointers
// in the targets_[] array and calls their laptopRequestHappened() or
// laptopFailureHappened() methods.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// There is a limit to the number of targets stored in a cascade.  This number
// can be increased if the need arises.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
//@(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LaptopEventCascade.ccv   25.0.4.0   19 Nov 2013 14:08:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd     Date:  18-MAY-95    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "LaptopEventCascade.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LaptopEventCascade()  [Default Constructor]
//
//@ Interface-Description
// Constructs the LaptopEventCascade.  Needs no arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply initializes the 'targets_[]' array.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LaptopEventCascade::LaptopEventCascade(void) :
						numTargets_(0)
{
	CALL_TRACE("LaptopEventCascade::LaptopEventCascade(void)");

	// Initialize array
	for (Uint16 i = 0; i < LPT_MAX_TARGETS_; i++)
	{
		targets_[i] = NULL;
	}
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LaptopEventCascade  [Destructor]
//
//@ Interface-Description
// Destroys the LaptopEventCascade.
//---------------------------------------------------------------------
//@ Implementation-Description
// Needs to do nothing
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

LaptopEventCascade::~LaptopEventCascade(void)
{
	CALL_TRACE("LaptopEventCascade::~LaptopEventCascade(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addTarget [public]
//
//@ Interface-Description
// Adds a Laptop event target to this cascade.  Passed 'pTarget', a
// pointer to the target object.
//---------------------------------------------------------------------
//@ Implementation-Description
// Adds another target pointer to the 'targets_[]' array.
//---------------------------------------------------------------------
//@ PreCondition
// The target pointer must be non-null and the 'targets_[]' array must
// not be full.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopEventCascade::addTarget(LaptopEventTarget* pTarget)
{
	CALL_TRACE("LaptopEventCascade::addTarget(LaptopEventTarget* pTarget)");

	CLASS_PRE_CONDITION(pTarget != NULL);
	CLASS_PRE_CONDITION(numTargets_ < LPT_MAX_TARGETS_);

	// Add target to the next empty element of the array.
	targets_[numTargets_] = pTarget;

	numTargets_++;										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: laptopRequestHappened
//
//@ Interface-Description
// Called when the request event of which this cascade is a target happens.
// We simply call the corresponding method of all registered targets
// with the same parameters, described below:
// >Von
//	eventId			The enum id of the event which happens.
//	pMsgData		The message coming from the laptop.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Walks the list of targets and calls their laptopRequestHappened()
// methods in turn.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopEventCascade::laptopRequestHappened(LaptopEventId::LTEventId eventId, Uint8 *pMsgData)
	{
	CALL_TRACE("LaptopEventCascade::laptopRequestHappened("
					"LaptopEventId::LTEventId eventId, Uint8 *pMsgData)");

	// Walk the 'targets_[]' array and call the eventChangeHappened()
	// method on each target.
	for (Uint16 i = 0; i < numTargets_; i++)
	{													// $[TI1]
		SAFE_CLASS_ASSERTION(NULL != targets_[i]);
		targets_[i]->laptopRequestHappened(eventId, pMsgData);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: laptopFailureHappened
//
//@ Interface-Description
// Called when the laptop event of which this cascade is a target happens.
// We simply call the corresponding method of all registered targets
// with the same parameters, described below:
// >Von
//	serialFailureCodes		The enum id of the failure code.
//	eventId					The enum id of the LaptopEventID.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Walks the list of targets and calls their laptopFailureHappened()
// methods in turn.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopEventCascade::laptopFailureHappened(SerialInterface::SerialFailureCodes
														serialFailureCodes,
														Uint8 eventId)
{
	CALL_TRACE("LaptopEventCascade::laptopFailureHappened("
					"SerialInterface::SerialFailureCodes serialFailureCodes)");

	// Walk the 'targets_[]' array and call the eventChangeHappened()
	// method on each target.
	for (Uint16 i = 0; i < numTargets_; i++)
	{													// $[TI1]
		SAFE_CLASS_ASSERTION(NULL != targets_[i]);
		targets_[i]->laptopFailureHappened(serialFailureCodes, eventId);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LaptopEventCascade::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LAPTOPEVENTCASCADE,
									lineNumber, pFileName, pPredicate);
}
