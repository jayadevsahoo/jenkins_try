#ifndef SafetyNetTestMediator_HH
#define SafetyNetTestMediator_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: SafetyNetTestMediator - Runs Safety net tests and checks
//                                 status every new cycle.
//---------------------------------------------------------------------
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SafetyNetTestMediator.hhv   25.0.4.0   19 Nov 2013 14:00:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: syw   Date:  21-Mar-2000    DR Number: 5611
//  Project:  NeoMode
//	Description:
//		Eliminate SvSwitchSideRangeTest class.
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002 By: iv     Date: 10-Mar-1998   DR Number: DCS 5041
//  	Project:  Sigma (R8027)
//		Description:
//			Added an argument Uint16 errorCode to logBdSafetyNetEvent().
//
//  Revision: 001  By:  by    Date:  05-Sep-1995    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//=====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
#include "Background.hh"
#include "SafetyNetSensorData.hh"

class SafetyNetTest;
class SafetyNetRangeTest;
//@ End-Usage

//@ Constant: NUM_BD_SAFETY_NET_TESTS
// the number of BD run time safety net tests
static const Int16 NUM_BD_SAFETY_NET_TESTS = 12;

//@ Constant: NUM_BD_SAFETY_NET_RANGE_TESTS
// the number of BD run time safety net range tests
static const Int16 NUM_BD_SAFETY_NET_RANGE_TESTS = 1;


class SafetyNetTestMediator 
{
    public:

		//////////////////////////////////////////////////////////////////////
        // This enumeration contains identifiers for Background events which
        // are detected in other parts of the BD system and reported to
        // the SafetyNetTestMediator.  The SafetyNetTestMediator provides the
        // only interface between BD and the Background subsystem with regard
        // to reporting background events.
		//////////////////////////////////////////////////////////////////////
        enum BdBackgndId { PI_AZ_OOR, PE_AZ_OOR };
    
        SafetyNetTestMediator( void );
        ~SafetyNetTestMediator( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL,
			       const char*       pPredicate = NULL) ;
        void newCycle( void ) const;

        void logBdSafetyNetEvent( const SafetyNetTestMediator::BdBackgndId bdBackgndId, const Uint16 errorCode );

    protected:

    private:

        void takeAction_(const BkEventName backgndEventId) const;
    
        // these methods are purposely declared, but not implemented...
        SafetyNetTestMediator( const SafetyNetTestMediator& );  // not implemented...
        void operator=( const SafetyNetTestMediator& );         // not implemented...
   
        //@ Data-Member: pBdSafetyNetTests_
        // A set of pointers to BD runtime safety net tests.
        SafetyNetTest* pBdSafetyNetTests_[ NUM_BD_SAFETY_NET_TESTS ];

        //@ Data-Member: pBdSafetyNetRangeTests_
        // A set of pointers to BD runtime safety net range tests.
        SafetyNetRangeTest* pBdSafetyNetRangeTests_[ NUM_BD_SAFETY_NET_RANGE_TESTS ];
}; 


#endif // SafetyNetTestMediator_HH 
