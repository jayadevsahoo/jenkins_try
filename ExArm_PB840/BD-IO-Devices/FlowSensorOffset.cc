#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//============================== C L A S S   D E S C R I P T I O N ====
// Class: FlowSensorOffset - This class is the interface between the
//		FlowSensor class and the NovRamManager for retrieval and updates
//		of the flow offsets.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class contains methods to set and get the flow sensor offset
//		values.   
//---------------------------------------------------------------------
//@ Rationale
//		Abstraction of flow sensor offsets
//---------------------------------------------------------------------
//@ Implementation-Description
//		An array is used to store the offsets for each object.  The offsets
//		are defaulted to zero.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
//
//@ Modification-Log
//
//  Revision: 002 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date:  27-Aug-97    DR Number: 2279
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "FlowSensorOffset.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowSensorOffset()
//
//@ Interface-Description
//  	This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize the offset array to 0.0
//---------------------------------------------------------------------
//@ PreCondition
//  	None.
//---------------------------------------------------------------------
//@ PostCondition
//  	None.
//@ End-Method
//=====================================================================

FlowSensorOffset::FlowSensorOffset( void )
{
    CALL_TRACE("FlowSensorOffset::FlowSensorOffset( void )");

	// $[TI1]
	
	for (Uint32 ii=0; ii < MAX_FLOW_CALIBRATION; ii++)
	{
		offset_[ii] = 0.0 ;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FlowSensorOffset()  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Not implemented.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

FlowSensorOffset::~FlowSensorOffset(void)
{
     CALL_TRACE("FlowSensorOffset::~FlowSensorOffset(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowSensorOffset
//
//@ Interface-Description
//  	Copy constuctor.  Takes a reference to a FlowSensorOffset as an
//  	argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	This copy constructor sets this object equal to the input objects.
//---------------------------------------------------------------------
//@ PreCondition
//  	None.
//---------------------------------------------------------------------
//@ PostCondition
//  	None.
//@ End-Method
//=====================================================================

FlowSensorOffset::FlowSensorOffset( const FlowSensorOffset& rFlowSensorOffset )
{
    CALL_TRACE("FlowSensorOffset::FlowSensorOffset( const FlowSensorOffset& rFlowSensorOffset )") ;
     
	// $[TI2]
	
	for (Uint32 ii=0; ii < MAX_FLOW_CALIBRATION; ii++)
	{
		offset_[ii] = rFlowSensorOffset.offset_[ii] ;
	}
}
                           
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: =operator
//
//@ Interface-Description
//  	= operator.  Takes a reference to a FlowSensorOffset as an
//  	argument and returns no value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Set every element in the FlowSensorOffset object equal to the object
//  	passed in.
//---------------------------------------------------------------------
//@ PreCondition
//  	None.
//---------------------------------------------------------------------
//@ PostCondition
//  	None.
//@ End-Method
//=====================================================================

void
FlowSensorOffset::operator=( const FlowSensorOffset& rFlowSensorOffset )
{
    CALL_TRACE("FlowSensorOffset::operator=( const FlowSensorOffset& rFlowSensorOffset )");

    if ( this != &rFlowSensorOffset )
    {
		// $[TI1]
		for (Uint32 ii=0; ii < MAX_FLOW_CALIBRATION; ii++)
		{
			offset_[ii] = rFlowSensorOffset.offset_[ii] ;
		}
    }
    else
    {
    	CLASS_PRE_CONDITION( FALSE) ;
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getOffset
//
//@ Interface-Description
//  	This method has an index as an argument and returns the offset
//		corresponding to the index.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Returns offset value corresponding to the index.
//---------------------------------------------------------------------
//@ PreCondition
//  	None.
//---------------------------------------------------------------------
//@ PostCondition
//  	None.
//@ End-Method
//=====================================================================

Real32
FlowSensorOffset::getOffset( const Uint32 index)
{
	CALL_TRACE("FlowSensorOffset::getOffset( const Uint32 index)") ;

	Real32 offset = 0.0 ;
	
	if (index < MAX_FLOW_CALIBRATION)
	{
		// $[TI1.1]
		offset = offset_[index] ;
	}
	else
	{
		// $[TI1.2]
		offset = offset_[MAX_FLOW_CALIBRATION-1] ;
	}
	return( offset) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//  [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
FlowSensorOffset::SoftFault( const SoftFaultID  softFaultID,
                        const Uint32       lineNumber,
                        const char*        pFileName,
                        const char*        pPredicate )
{
      CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

      FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, FLOWSENSOROFFSET,
                               lineNumber, pFileName, pPredicate );
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
  
//=====================================================================
//
//  Private Methods...
//
//=====================================================================















