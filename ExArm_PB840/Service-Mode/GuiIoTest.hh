#ifndef GuiIoTest_HH
#define GuiIoTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: GuiIoTest - Test GUI IO
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/GuiIoTest.hhv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//
//@ Modification-Log
//  Revision: 007  By: erm       Date: 16-Jan-2002     DR Number: 5984
//  Project: GuiComms
//  Description:
//  Added External Serial LoopBack test
//
//  Revision: 006  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 005  By: gdc      Date: 24-Nov-1997  DR Number: 2605
//       Project:  Sigma (R8027)
//       Description:
//            Changed interface to GUI-Serial-Interface as part of its 
//            restructuring.
//
//  Revision: 004  By:  quf    Date: 31-Jul-1997    DR Number: DCS 2013
//       Project:  Sigma (840)
//       Description:
//            Removed nurse call test from GUI audio test and made it
//            a separate test.
//
//  Revision: 003  By:  quf    Date: 27-May-1997    DR Number: DCS 2042
//       Project:  Sigma (840)
//       Description:
//            Implemented GUI Audio Test.
//            Implemented GUI Serial Port Test.  Deleted testSerialIoTest_(),
//            added clearString_().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  sp    Date: 26-Feb-1996    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"
//TODO E600 added GUI only
#if defined(SIGMA_GUI_CPU)
#include "SmPromptId.hh"
#include "SmStatusId.hh"
#include "MsgQueue.hh"
#include "Keys.hh"
#include "Led.hh"

//@ Usage-Classes

//@ End-Usage

extern const Int16 MIN_KNOB_DELTA;

extern const char TEST_MSG[];
extern const Uint TEST_MSG_SIZE;


class GuiIoTest
{
	public:
		enum KnobTestResult
		{
			OPERATOR_EXIT,
			PASSED,
			FAILED
		};

		enum KnobDirection
		{
			LEFT = -1,	// unit knob delta, counterclockwise
			RIGHT = 1	// unit knob delta, clockwise
		};

		GuiIoTest(void);
		~GuiIoTest(void);

		static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);

		void runKeyboardTest(void);
		void runKnobTest(void);
		void runTouchScreenTest(void);
		void runGuiLedTest(void);
		void runGuiAudioTest(void);
		void runSerialIoTest(void);
		void runNurseCallTest(void);
		void runExternalSerialIoTest(void);

	protected:

	private:
		GuiIoTest(const GuiIoTest&);		// not implemented...
		void operator=(const GuiIoTest&);	// not implemented...

		Boolean testKeyPress_(SmPromptId::PromptId testId, GuiKeys guiKeyCode);
		KnobTestResult testKnob_(KnobDirection direction);
		Int16 limitKnobSum_(Int16 knobSum, KnobDirection direction);
		Boolean testGuiAudio_(void);
		void testNurseCall_(void);
		void clearString_(char* string, Int32 length);

		//@ Data-Member: guiIoTestQ_
		// MsgQueue to receive GUI IO messages on
		MsgQueue guiIoTestQ_;
		
};

#endif //defined(SIGMA_GUI_CPU)

#endif // GuiIoTest_HH 
