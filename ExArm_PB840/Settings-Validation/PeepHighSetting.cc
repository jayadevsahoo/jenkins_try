#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  PeepHighSetting - PEEP High Level for BiLevel Breaths
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the 
//  high-level positive pressure
//  applied to the patient's circuit during exhalation.  This class
//  inherits from 'BoundedSetting' and provides the specific information
//  needed for representation of PEEP_HIGH.  This information includes the
//  interval and range of this setting's values, and this batch setting's
//  new-patient value (see 'getNewPatientValue()').
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, or dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/PeepHighSetting.ccv   25.0.4.0   19 Nov 2013 14:27:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 007  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  corrected comment
//      *  modified to support new VC+ value
//
//  Revision: 006   By: sah    Date: 22-Sep-1999    DR Number: 5424
//  Project:  NeoMode
//  Description:
//	Obsoleted 'SettingOptions' class, to use new global
//      'SoftwareOptions' class.
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  modified various methods to factor in new PEEP-low setting
//	*  removed unnecessary 'isAcceptedValid()' method
//
//  Revision: 003   By: dosman Date:  11-Nov-1998    DR Number: 5247
//  Project:  BILEVEL
//  Description:
//	Changed resolution to be 1 in all cases
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: dosman    Date: 18-Dec-1997    DR Number:
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//      Added requirement numbers for tracing to SRS. 
//      Made new patient value for PEEP_HIGH = 
//              new patient value for PEEP + DEFAULT_MAND_PRESS_DELTA
//      Transition into BILEVEL from 'VC' results in updating 
//      PEEP_HIGH = new patient value for PEEP + DEFAULT_MAND_PRESS_DELTA
//
//=====================================================================

#include "PeepHighSetting.hh"
#include "MandTypeValue.hh"
#include "ModeValue.hh"
#include "SettingConstants.hh"

#ifdef SIGMA_DEVELOPMENT
	#include "Ostream.hh"
#endif // SIGMA_DEVELOPMENT

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[BL02007] -- The setting's range ...
//  $[BL02010] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	5.0f,		// absolute minimum...
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	90.0f,		// absolute maximum...
	1.0f,		// resolution...
	ONES,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: PeepHighSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//
// The minimum value of PeepHighSetting is equal to 
// the minimum value of PeepSetting plus
// the minimum mandatory type pressure above peep.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information. Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepHighSetting::PeepHighSetting(void)
: BatchBoundedSetting(SettingId::PEEP_HIGH,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  PEEP_HIGH_MAX_ID,	// maxConstraintId...
					  PEEP_HIGH_MIN_ID)	// minConstraintId...
{
	CALL_TRACE("PeepHighSetting()");
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~PeepHighSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

PeepHighSetting::~PeepHighSetting(void)
{
	CALL_TRACE("~PeepHighSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01071] -- main settings applicability
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	PeepHighSetting::getApplicability(const Notification::ChangeQualifier qualifierId) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id  applicability;

	if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL) )
	{  // $[TI1] -- BILEVEL is an active option...
		const Setting*  pMode = SettingsMgr::GetSettingPtr(SettingId::MODE);

		DiscreteValue  modeValue;

		switch ( qualifierId )
		{
			case Notification::ACCEPTED :		// $[TI1.1]
				modeValue = pMode->getAcceptedValue();
				break;
			case Notification::ADJUSTED :		// $[TI1.2]
				modeValue = pMode->getAdjustedValue();
				break;
			default :
				AUX_CLASS_ASSERTION_FAILURE(qualifierId);
				break;
		}

		applicability = (modeValue == ModeValue::BILEVEL_MODE_VALUE)
						? Applicability::CHANGEABLE	   // $[TI1.3]
						: Applicability::INAPPLICABLE; // $[TI1.4]
	}
	else
	{  // $[TI2] -- BILEVEL is not an active option...
		applicability = Applicability::INAPPLICABLE;
	}

	return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[BL02008] -- new patient value for PeepHigh
//  Returns a constant for the new patient value.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	PeepHighSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	BoundedValue  newPatient;

	const Setting*  pPeepLow = SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW);

	const Real32  PEEP_LOW_VALUE =
		BoundedValue(pPeepLow->getNewPatientValue()).value;

	// $[BL02008] The setting's new-patient value ...
	newPatient.value = PEEP_LOW_VALUE +
					   SettingConstants::DEFAULT_MAND_PRESS_DELTA;

	// warp the calculated value to a "click"...
	getBoundedRange().warpValue(newPatient);

	return(newPatient);	 // $[TI1]
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  This method is only called during transition of mode
//	to BiLevel
//
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02025]\d\   -- transitioning from A/C to BiLevel
//  $[TC02000]\a\ -- transitioning from SIMV to BiLevel
//  $[02026]\c\   -- transitioning from SPONT to BiLevel
//---------------------------------------------------------------------
//@ PreCondition
//  ((settingId == SettingId::MODE)           &&
//   ((currValue == ModeValue::SIMV_MODE_VALUE)  ||
//    (currValue == ModeValue::AC_MODE_VALUE)  ||
//    (currValue == ModeValue::SPONT_MODE_VALUE) ||
//    (currValue == ModeValue::CPAP_MODE_VALUE))  &&
//   (newValue == ModeValue::BILEVEL_MODE_VALUE))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
	PeepHighSetting::acceptTransition(const SettingId::SettingIdType settingId,
									  const DiscreteValue            newValue,
									  const DiscreteValue        currValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	AUX_CLASS_PRE_CONDITION((settingId == SettingId::MODE), settingId);
	AUX_CLASS_PRE_CONDITION(((currValue == ModeValue::SIMV_MODE_VALUE)  ||
							 (currValue == ModeValue::SPONT_MODE_VALUE) ||
							 (currValue == ModeValue::CPAP_MODE_VALUE) ||
							 (currValue == ModeValue::AC_MODE_VALUE) ),
							currValue);
	AUX_CLASS_PRE_CONDITION((newValue == ModeValue::BILEVEL_MODE_VALUE),
							newValue);

	const Setting*  pMandType = SettingsMgr::GetSettingPtr(SettingId::MAND_TYPE);

	// need to use the accepted value of mandatory type...
	const DiscreteValue  MAND_TYPE_VALUE = pMandType->getAcceptedValue();

	const Setting*  pPeep = SettingsMgr::GetSettingPtr(SettingId::PEEP);
	const Real32    PEEP_VALUE = BoundedValue(pPeep->getAdjustedValue()).value;

	BoundedValue  peepHigh;

	switch ( MAND_TYPE_VALUE )
	{
		case MandTypeValue::VCV_MAND_TYPE :
		case MandTypeValue::VCP_MAND_TYPE :
			{	// $[TI3]
				//===================================================================
				// mandatory type transition from VC/VC+ to PC...
				//===================================================================

				peepHigh.value = PEEP_VALUE + SettingConstants::DEFAULT_MAND_PRESS_DELTA;
			}
			break;

		case MandTypeValue::PCV_MAND_TYPE :
			{	// $[TI4]
				//===================================================================
				// mandatory type transition from PCV to VCV...
				//===================================================================

				const Setting*  pInspPress =
					SettingsMgr::GetSettingPtr(SettingId::INSP_PRESS);

				const Real32  INSP_PRESS_VALUE =
					BoundedValue(pInspPress->getAdjustedValue()).value;

				peepHigh.value = PEEP_VALUE + INSP_PRESS_VALUE;
			}
			break;

		default:
			AUX_CLASS_ASSERTION_FAILURE(MAND_TYPE_VALUE);
			break;
	}

	BoundStatus  dummyStatus(getId());

	// calculate bound constraints...
	updateConstraints_();

	// use the calculation of the maximum bound in 'updateConstraints_()'
	// to limit the current adjusted value to a valid range...
	getBoundedRange_().testValue(peepHigh, dummyStatus);

	// reset both constraints for the warping below...
	getBoundedRange_().resetMinConstraint();
	getBoundedRange_().resetMaxConstraint();

	// warp the calculated value to a "click"...
	getBoundedRange_().warpValue(peepHigh);

	// store the transition value into the adjusted context...
	setAdjustedValue(peepHigh);

	// activation of Transition Rule MUST be italicized...
	setForcedChangeFlag();
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	PeepHighSetting::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							PEEP_HIGH_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the minimum upper bound, and set the upper limit to it.
//
//  $[BL02007] -- PEEP_HIGH's range
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	PeepHighSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	//===================================================================
	// determine maximum bound constraint...
	//===================================================================

	BoundedRange::ConstraintInfo  maxConstraintInfo;

	// start off with the absolute maximum bound value and ID...
	maxConstraintInfo.id    = PEEP_HIGH_MAX_ID;
	maxConstraintInfo.value = getAbsoluteMaxValue_();

	BoundedValue  maxLimit;

	// reset this range's maximum constraint to allow for "warping" of a
	// new maximum value...
	getBoundedRange_().resetMaxConstraint();

	const Setting*  pHighCctPress =
		SettingsMgr::GetSettingPtr(SettingId::HIGH_CCT_PRESS);

	const Real32  HIGH_CCT_PRESS_VALUE =
		BoundedValue(pHighCctPress->getAdjustedValue()).value;

	maxLimit.value = (HIGH_CCT_PRESS_VALUE - 
					  SettingConstants::MIN_MAND_PRESS_BUFF);

	if ( maxLimit.value < maxConstraintInfo.value )
	{	// $[TI1]
		// warp the calculated maximum value to a lower "click" boundary...
		getBoundedRange_().warpValue(maxLimit, BoundedRange::WARP_DOWN);

		// the high circuit pressure-based maximum is more restrictive than
		// 'maxConstraintInfo.value', therefore save its value and id as the
		// maximum bound value and id...
		maxConstraintInfo.value = maxLimit.value;
		maxConstraintInfo.id    = PEEP_HIGH_MAX_BASED_PCIRC_ID;
	}  // $[TI2] -- high cct. pressure's absolute max is more restrictive...

	// this setting has no maximum soft bounds...
	maxConstraintInfo.isSoft = FALSE;

	// update the maximum bound to the most restrictive bound...
	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);


	//===================================================================
	// determine minimum bound constraint...
	//===================================================================

	BoundedRange::ConstraintInfo  minConstraintInfo;

	// start off with the absolute minimum bound value and ID...
	minConstraintInfo.value = getAbsoluteMinValue_();
	minConstraintInfo.id    = PEEP_HIGH_MIN_ID;

	BoundedValue  minLimit;

	// reset this range's minimum constraint to allow for "warping" of a
	// new minimum value...
	getBoundedRange_().resetMinConstraint();

	const Setting*  pPeepLow = SettingsMgr::GetSettingPtr(SettingId::PEEP_LOW);

	const Real32 PEEP_LOW_VALUE =
		BoundedValue(pPeepLow->getAdjustedValue()).value;

	minLimit.value = (PEEP_LOW_VALUE + SettingConstants::MIN_MAND_PRESS_DELTA);

	if ( minLimit.value > minConstraintInfo.value )
	{	// $[TI3]
		// warp the calculated minimum value to an upper "click" boundary...
		getBoundedRange_().warpValue(minLimit, BoundedRange::WARP_UP);

		// the PEEP LOW - based minimum is more restrictive than
		// 'minConstraintInfo.value', therefore save its value and id as the minimum
		// bound value and id...
		minConstraintInfo.id    = PEEP_HIGH_MIN_BASED_PEEPL_ID;
		minConstraintInfo.value = minLimit.value;
	}  // $[TI4] -- PEEP LOW based minimum is less restrictive than current minimum

	// this setting has no minimum soft bounds...
	minConstraintInfo.isSoft = FALSE;

	// update the minimum bound to the most restrictive bound...
	getBoundedRange_().updateMinConstraint(minConstraintInfo);
}
