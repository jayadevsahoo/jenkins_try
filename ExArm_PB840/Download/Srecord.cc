#include "stdafx.h"
#if	defined(SIGMA_SUN)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//---------------------------------------------------------------------
//@ Interface-Description
//@ Srecord - this class defines all the activities necessary to parse
//  a Motorola S record object format to an actual memory image.
//---------------------------------------------------------------------
//@ Rationale
//	 This class encapsulates all the knowledge and data that are necessary
//   to parse a continuous bytes of S record data to an actual memory
//   image.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The class handles only a subset of S record formats, 32 bit address
//   format and an end of 32 bit address record. Since this class is
//   is used to build a 32bit Sigma application executable images.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions  and pre-conditions are used to
//  ensure correctness of software conditons.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/Srecord.ccv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <jhv>    Date:  22-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "Srecord.hh"
#include "BuildFlash.hh"
#include "DownloadTypes.hh"
#include "stdio.h"

Byte Srecord::AsciiConvTbl_[16] = {'0','1','2','3',
								  '4','5','6','7',
								  '8','9','A','B',
								  'C','D','E','F'};
//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================

//@ Code...
//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Srecord()  [Default Constructor]
//
//@ Interface-Description
//    Initialize the instance variables to its initial value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Srecord::Srecord()
{
	state_ = S_SYNC;
	type_ = 0;		
	count_ = 0 ;		
	cksum_ = 0 ;	
	addlen_ = 8 ;		
	address_ = 0 ;	
	data_ = 0 ;		
	flop_ = 0 ;		
	exitSrecord_ = FALSE;
	isNumber_ = FALSE ;		
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initStates()  
//
//@ Interface-Description
//    Initialize all the states of a S record parsing machine 
//    to its initial values.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
Srecord::initStates()
{
	state_ = S_SYNC;
	type_ = 0;		
	count_ = 0 ;		
	cksum_ = 0 ;	
	addlen_ = 8 ;		
	address_ = 0 ;	
	data_ = 0 ;		
	flop_ = 0 ;		
	exitSrecord_ = FALSE;
	isNumber_ = FALSE ;		
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Srecord()  [Destructor]
//
//@ Interface-Description
//   Default Destructor. Do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 none.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Srecord::~Srecord(void)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: parse(Byte *c)
//
//@ Interface-Description
//	  Parse a given S record data char to a correct S record value
//    based on the current S record parsing states.
//--------------------------------------------------------------------
//@ Implementation-Description
//    A S record file is consists of individual S records.
//    Each S record starts with a letter 'S' to indicate that it is
//    a S record type. Immediately after the 'S' record, a numeric
//    character follows to indicate what type of S record.
//    The following two numeric characters indicate total length
//    of the current record bytes that includes the number of data,
//    address and checksum bytes in the record.
//
//    Depending on the S record type, a different number
//    of numeric numbers follow to indicate the absolute address
//    where the data to be placed. This method only allows
//    S record types (S3) that has  an 8bytes of hexa decimal address.
//    Following the address, a specified number of data bytes are
//    followed in a hexa-decimal character. The last two characters, 
//    one's complement of the additive checksum of all the preceding
//    characters including the byte count, address and data bytes.
//   
//    The end of the S record file should end with a termination S
//	  record (S7). Except the S record type indicator, all the following
//    characters in a S record should be a hexa-decimal character.
//
//   An additive checksum is computed as the srecord address, data
//   and byte count got converted to a binary byte of data.
//
//   Do not raise an error condition when an extra new line character
//   is encountered while parsing the S7, termination S record.
//
//   Since Sigma project doesn't use the S7 record to determine
//   a start jump address, don't write out contents of the S7 record
//   to the flash memory image.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
Srecord::parse( Byte nibble )
{
Uint nib_acc = 0;

	if ( ( nibble <= '9' ) && ( nibble >= '0' ))
	{
		nib_acc = nibble& 0x0f;
		isNumber_ = TRUE;
	}else if ((( nibble <= 'F' ) && ( nibble >= 'A' ))
		|| ( (nibble <= 'f') && (nibble >= 'a')))
	{
		nib_acc = (nibble & 0x0f) + 9;
		isNumber_ = TRUE;
	}
	else
		isNumber_ = FALSE;

	if ( (state_ != S_SYNC) && (!isNumber_) || (nibble == '\033'))
	{
		if (! (( state_ == S_EXIT) && ( nibble == 0xA ))) 
		{
			state_ = S_ERROR;
		}
	}


	switch(state_)
	{
	case S_SYNC:
		{
			if (nibble == 'S')
			{
				state_ = S_GET_TYPE;
			}
			break;
		}
	case S_GET_TYPE:
		{
			type_ = nib_acc;
			count_ = 0;
			cksum_ = 0;
			state_ = S_GET_COUNT_HI;
			break;
		}
	case S_GET_COUNT_HI:
		{
			count_ += nib_acc << 4;
			state_ = S_GET_COUNT_LO;
			break;
		}
	case S_GET_COUNT_LO:
		{
			count_ += nib_acc;
			cksum_ += count_;
			state_ = S_GET_ADDR;
			address_ = 0;
			flop_ = 0;

			//
			// Take action base on record type
			//
			switch(type_)
			{
			case 0:
			case 5:
			//
			// Simply look for the next record
			//
				{
					state_ = S_SYNC;
					break;
				}
			case 7:
			//
			// Normal exit record
			//
				{
					state_ = S_EXIT;
					exitSrecord_ = TRUE;
					break;
				}
			//
			// Set the address length appropriately
			//
			case 3: // only 32 bit address is supported.
				count_ = count_ - SRECORD_ADDR_LEN - 1;
				addrlen_ = 2*SRECORD_ADDR_LEN;
				break;
		    default:
				BuildFlash::PrintErr("Only 32 bit address length records are supported\n");
				return(FAILURE);
			}
			break;
		}
	case S_GET_ADDR:
		{
			address_ = (address_ << 4) + nib_acc;

			if ( flop_++ & 1 )
			{
				cksum_ += address_ & 0xff;
			}

			if ( ! (--addrlen_) )
			{
				state_ = S_GET_DATA;
				data_ = 0;
			}

			break;
		}
	case S_GET_DATA:
		{
			data_ = (data_ << 4) + nib_acc;

			if ( flop_++ & 1 )
			{
				cksum_ += data_;

				if (pImageBuilder_->writeByte((Byte *)address_, data_) == FAILURE)
				{
					return(FAILURE);
				}
				address_++;
				data_ = 0;

				if ( !(--count_) )
				{
					state_ = S_GET_CKSUM;
				}
			}
			break;
		}
	case S_GET_CKSUM:
		{
			data_ = (data_ << 4) + nib_acc;

			if ( flop_++ & 1 )
			{
				if ( ( cksum_ + data_ + 1 ) & 0xff )
				{
					BuildFlash::PrintErr("Srecord Input Data checksum Error\n");
					state_ = S_ERROR;
				} else
				{
					state_ = S_SYNC;
				}
			}
			break;
		}
	case S_EXIT:
		{
			// Throw away termination Srecord data, we don't use them.
			break;
		}
	case S_ERROR:
	default:
		{
			BuildFlash::PrintErr("incorrect Srecord format.\n");
			return(FAILURE);
		}
	}
	return(SUCCESS);		// return results
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buildDataRecord(Byte *pStart, Byte *pData,
//			Byte *pSrecord, Int dataSize)
//
//@ Interface-Description
//		Build a Srecord type that allows a 8 byte long physical 
//      address from a given number of data bytes in the pData
//      memory.
//      All the data is considered word aligned and the starting 
//      address is specified by the 'pStart' argument.
//      The output S record is put into the 'pSrecord' memory.
//      The method returns a length of the srecord created.
//--------------------------------------------------------------------
//@ Implementation-Description
//     A S record cannot exceed more than 250 bytes long, therefore
//     the method doesn't allow data size bigger than 112 bytes.
//     Each data byte values gets translated to two byte long
//     hexa-decimal character representation.
//     Add a new line at the end of each S record in case someone
//     wants to look at the output file with a editor.
//     
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
Srecord::buildDataRecord(Byte *targetAddr, Byte *pData, Byte *pSrecord,
	Int dataSize)
{
Byte nBytes = 0;
Int i;
Byte chkSum = 0;
Byte *pStart = (Byte *) &targetAddr;
Int rec_len;

	CLASS_PRE_CONDITION( dataSize <= MAX_SRECORD_DATA_LENGTH );

	rec_len = dataSize + SRECORD_ADDR_LEN + SRECORD_CHECKSUM_LEN;

    pSrecord[nBytes++] = 'S';
	pSrecord[nBytes++] = '3';
    pSrecord[nBytes++] = AsciiConvTbl_[(rec_len & 0xF0)>>4];
    pSrecord[nBytes++] = AsciiConvTbl_[rec_len & 0xF];
	chkSum += (Uint8) rec_len;
	for ( i = 0; i<SRECORD_ADDR_LEN; i++)
	{
    	pSrecord[nBytes++] = AsciiConvTbl_[(pStart[i] & 0xF0)>>4];
    	pSrecord[nBytes++] = AsciiConvTbl_[pStart[i] & 0xF];
		chkSum += pStart[i];
	}
	for(i=0; i < dataSize; i++)
	{
		pSrecord[nBytes++] = AsciiConvTbl_[( pData[i]  & 0xF0 ) >> 4];
		pSrecord[nBytes++] = AsciiConvTbl_[ pData[i]  & 0xF];
		chkSum += pData[i];
	}
	chkSum = ~chkSum;

	pSrecord[nBytes++] = AsciiConvTbl_[( chkSum  & 0xF0 ) >> 4];
	pSrecord[nBytes++] = AsciiConvTbl_[ chkSum  & 0xF];
	pSrecord[nBytes++] = '\n';
	return(nBytes);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buildEofRecord( Byte *pSrecord )
//
//@ Interface-Description
//		Build a Srecord type that indicates an end of Srecord file.
//      The termination S record is placed in the pSrecord memory.
//      Returns total number of bytes in the S record.
//--------------------------------------------------------------------
//@ Implementation-Description
//   The 'S7' Srecord type is an end of Srecord file record.
//   Its format is simple. It doesn't contain any data and it usually
//   specifies a target start address. A zero value address is put into
//   the output Srecord to since the downloader doesn't have to 
//   an execution address.
//
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
Srecord::buildEofRecord ( Byte *pSrecord )
{
Byte cByte, nBytes = 0;
Int i;
Byte chkSum = 0;
Int rec_len;


	rec_len =  SRECORD_ADDR_LEN + SRECORD_CHECKSUM_LEN;

    pSrecord[nBytes++] = 'S';
	pSrecord[nBytes++] = '7';
    pSrecord[nBytes++] = AsciiConvTbl_[(rec_len & 0xF0)>>4];
    pSrecord[nBytes++] = AsciiConvTbl_[rec_len & 0xF];
	chkSum += rec_len;
	for ( i = 0; i < SRECORD_ADDR_LEN; i += 1)
	{
		cByte = 0;
		pSrecord[nBytes++] = '0';
		pSrecord[nBytes++] = '0';
		chkSum += cByte;
	}
	chkSum = ~chkSum;
	pSrecord[nBytes++] = AsciiConvTbl_[( chkSum  & 0xF0 ) >> 4];
	pSrecord[nBytes++] = AsciiConvTbl_[ chkSum  & 0xF];
    pSrecord[nBytes++] = '\n';
	return(nBytes);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Srecord::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  DownloadFault::SoftFault(softFaultID, DOWNLOAD,
  	SRECORD_CLASS, lineNumber, pFileName, pPredicate);
}

#endif //defined(SIGMA_SUN)
