#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendingLog - A GuiTable to display trending data
//---------------------------------------------------------------------
//@ Interface-Description
// TrendingLog is a GuiTable used to display trending data on the
// TrendTableSubScreen. It contains a scrollbar used to scroll
// through the table of data.
// 
// The GuiTable base class provides most of the methods for formatting
// the table and scrolling through the rows of data. The TrendingLog
// class defines the virtual method refreshCell_() to set up and format
// the individual LogCells according to the specific display
// requirements of the trending log.
// 
// On construction the TrendingLog class is told how many rows and
// columns will be in the table that it displays. It is also passed a
// pointer to a LogTarget object. TrendingLog retrieves the textual
// content of any cell in the table via this LogTarget. The textual
// content can be specified in a number of different ways, either as
// specific cheap text or generic text that is auto-formatted within
// TrendingLog, thus relieving the application of some of the burden of
// text formatting.
//
// To begin displaying a scrollable log the minimum that must be done
// is: construct the TrendingLog, call setColumnInfo() once for each
// column of the TrendingLog, call setEntryInfo() to specify the number
// of entries in the log and call activate() before it will be
// displayed.
//
// If the log is scrolled by the user, the TrendingLog automatically
// retrieves the contents of any newly displayed rows by calling
// LogTarget's getLogEntryColumn() method. 
//---------------------------------------------------------------------
//@ Rationale
// Encapsulates the cell formatting functions for the trending log.
//---------------------------------------------------------------------
//@ Implementation-Description 
// The client constructs a TrendingLog by specifying a LogTarget
// pointer, the number of rows, columns and row height of the table.
// TrendingLog passes these formatting options along to the base class
// constructor that controls the size and shape of the table.
// 
// TrendingLog is passed a LogTarget pointer on construction. When
// refreshCell_ is called, it calls this LogTarget via the
// getLogEntryColumn() method to retrieve the contents of each cell.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// None of the construction parameters can subsequently be changed after
// construction e.g. you cannot change the number of rows displayed by
// TrendingLog after construction.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendingLog.ccv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  gdc	   Date:  06-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Initial version.
//
//=====================================================================

#include "TrendingLog.hh"
#include "LogTarget.hh"
#include "GuiTimerRegistrar.hh"
// TODO E600 removed
//#include "Ostream.hh"

//@ Usage-Classes
//@ End-Usage

#pragma instantiate GuiTable<NUM_LOG_ROWS, NUM_LOG_COLS>

//=====================================================================
//
//		Static Data...
//
//=====================================================================

// Initialize static constants.
static const Boolean DISPLAY_SCROLLBAR = TRUE;

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendingLog()  [Constructor]
//
//@ Interface-Description
// Constructs a TrendingLog.  Passed the following parameters:
// >Von
//	pTarget				A LogTarget through which cell contents are
//                      retrieved.
// 
//	numDisplayRows		Number of rows displayed by the log.
// 
//	numDisplayColumns	Number of columns displayed by the log.
// 
//  rowHeight			The height in pixels of a row (includes the
//                      one pixel of the horizontal dividing line
//                      between rows).
// 
//	displayScrollbar	Pass FALSE if you do not want TrendingLog to
//                      have a scrollbar.  Defaults to TRUE.
// 
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize all private data members. Pass along the table size
// parameters and scrollbar to the GuiTable base class.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendingLog::TrendingLog(LogTarget &logTarget, 
						 Uint16 numDisplayRows, 
						 Uint16 numDisplayColumns, 
						 Uint16 rowHeight)
:   GuiTable<NUM_LOG_ROWS, NUM_LOG_COLS>(numDisplayRows, 
										 numDisplayColumns, 
										 rowHeight, 
										 scrollbar_, 
										 DISPLAY_SCROLLBAR),
	scrollbar_(             rowHeight * numDisplayRows - 1, numDisplayRows),
	rLogTarget_(			logTarget)
{
	CALL_TRACE("TrendingLog::TrendingLog(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TrendingLog()  [Destructor]
//
//@ Interface-Description
// TrendingLog destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TrendingLog::~TrendingLog(void)
{
	CALL_TRACE("TrendingLog::~TrendingLog(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: refreshCell_ [virtual, private]
//
//@ Interface-Description
// Refreshes the contents of a single cell.  Passed the row and column
// number of the cell.
//---------------------------------------------------------------------
//@ Implementation-Description 
// Calculates the entry number passed to the LogTarget from the
// specified row number and the current firstDisplayedEntry_.
// 
// Applies formatting options to each cell based on several factors. If
// the cell is in a "selected row" then display the cell with yellow
// text on a black background. If fast-scrolling is active then formats
// a non-selected row with dark-blue on a black background to making the
// text barely visible. For all other non-selected rows without
// fast-scroll active, formats the cell with white text on a black
// background.
// 
// Uses the LogTarget::getLogEntryColumn to retrieve the data contents
// of the cell and LogCell::setText to format the text and present it in
// the table cell.
//---------------------------------------------------------------------
//@ PreCondition
// The specified row and column must be in range of the displayed table.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendingLog::refreshCell_(Uint16 row, Uint16 col)
{
	AUX_CLASS_PRE_CONDITION(row < NUM_DISPLAY_ROWS_, row);
	AUX_CLASS_PRE_CONDITION(col < NUM_DISPLAY_COLUMNS_, col);

	Boolean isCheapText = FALSE;
	StringId string1 = NULL_STRING_ID;
	StringId string2 = NULL_STRING_ID;
	StringId string3 = NULL_STRING_ID;
	StringId string1Message = NULL_STRING_ID;
	Uint16 entryNumber = firstDisplayedEntry_ + row;

	// First, check if cell is on a row that corresponds to a log entry
	if (entryNumber >= numEntries_)
	{
		// we should never get here in TrendingLog
		AUX_CLASS_ASSERTION_FAILURE(entryNumber);
	}
	else
	{
		// Check if the cell is on a row that's selected.
		if (selectedEntry_ == entryNumber)
		{
			// It is so highlight it.
			cells_[row][col].setContentsColor(Colors::YELLOW);
		}
		else if (isFastScrolling_)
		{
			// if we're fast scrolling then dim the outlying data
			cells_[row][col].setContentsColor(Colors::DARK_BLUE);
		}
		else
		{
			// Not selected and not fast scrolling so render in normal color
			cells_[row][col].setContentsColor(Colors::WHITE);
		}

		// Ask the log target for the contents of the entry's column
		rLogTarget_.getLogEntryColumn(firstDisplayedEntry_ + row, col,
									  isCheapText, string1, string2, string3,
									  string1Message);

		// Set the contents of the cell
		if (NULL_STRING_ID == string2)
		{										
			// string2 is not used so pass oneLineFontSize as the required
			// font size.
			cells_[row][col].setText(isCheapText,
									 string1, string2, string3,
									 columnInfos_[col].alignment,
									 columnInfos_[col].fontStyle,
									 columnInfos_[col].oneLineFontSize,
									 columnInfos_[col].margin,
									 string1Message);
		}
		else
		{										 
			// string2 is used so pass twoLineFontSize as the required
			// font size.
			cells_[row][col].setText(isCheapText,
									 string1, string2, string3,
									 columnInfos_[col].alignment,
									 columnInfos_[col].fontStyle,
									 columnInfos_[col].twoLineFontSize,
									 columnInfos_[col].margin,
									 string1Message);
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendingLog::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDINGLOG,
							lineNumber, pFileName, pPredicate);
}

