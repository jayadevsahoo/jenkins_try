#ifndef PDTimerTarget_HH
#define PDTimerTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PDTimerTarget - A target to handle Patient Data  Timer events.
// Classes can specify this class as an additional parent class register to be
// informed of timer events.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/PDTimerTarget.hhv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: rhj   Date:  07-Nov-2008  SCR Number: 6435
//  Project:  840S
//  Description:
//      Added INSP_TIDAL_VOL_TIMEOUT_INTERVAL enum.
//
//  Revision: 003  By: rhj   Date:  03-Oct-2006  SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added Dynamics mechanics timeout
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  26-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "Sigma.hh"
#include "PDTimerId.hh"
#include "PatientDataClassIds.hh"
//@ End-Usage

enum TimerInterval
{
	TIDAL_VOLUME_TIMEOUT_INTERVAL = 120000,
	DYNAMIC_MECHANICS_TIMEOUT_INTERVAL = 120000,
	INSP_TIDAL_VOL_TIMEOUT_INTERVAL = 120000
};

class PDTimerTarget
{
public:
	virtual void timerEventHappened(PDTimerId::PDTimerIdType timerId)= 0;

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	PDTimerTarget(void);
	virtual ~PDTimerTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	PDTimerTarget(const PDTimerTarget&);	// not implemented...
	void operator=(const PDTimerTarget&);	// not implemented...
};


#endif // PDTimerTarget_HH 
