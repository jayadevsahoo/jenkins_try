#include "stdafx.h"
//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#include "BatchJobManager.h"
#include "SwatState.h"

namespace swat{

    /////////////////////////////////////////////////////////////
    // BatchJobManager::freeNode
    /////////////////////////////////////////////////////////////
    ///TODO What if at begining, delete element, do we correctly treat case.
    inline BatchJobManager::ListNode *BatchJobManager::freeNode(ListNode *pLast, ListNode *pCur)
    {
        //Get pointer to next list element.
        ListNode *pNextNode=pCur->pNext;

        // If we are in the middle of the list
        if(pLast)
        {
            //Point last node to next node
            pLast->pNext=pNextNode;
        }
        else
        {
            //There is no last node, must be first node in activeList.
            activeList=pNextNode;
        }

        // Put on top of freeList
        pCur->pNext=freeList;
        freeList=pCur;

        return pNextNode;
    }

    /////////////////////////////////////////////////////////////
    // BatchJobManager::hasFreeNode
    /////////////////////////////////////////////////////////////
    inline bool BatchJobManager::hasFreeNode()
    {
        return freeList!=0;
    }

    /////////////////////////////////////////////////////////////
    // BatchJobManager::getFreeNode
    /////////////////////////////////////////////////////////////
    inline BatchJobManager::ListNode *BatchJobManager::getFreeNode()
    {
        ListNode *pTemp=freeList;

        if(freeList)
        {
            freeList = pTemp->pNext;
        }

        return pTemp;
    }

    /////////////////////////////////////////////////////////////
    // BatchJobManager::appendToActiveList
    /////////////////////////////////////////////////////////////
    inline void BatchJobManager::appendToActiveList(ListNode *p)
    {
        p->pNext=activeList;
        activeList=p;
    }

    /////////////////////////////////////////////////////////////
    // BatchJobManager::getNextBatch
    /////////////////////////////////////////////////////////////
    inline BatchCommandInfo *BatchJobManager::getNextBatch()
    {
        return m_pfnGetNextBatch();
    }

    /////////////////////////////////////////////////////////////
    // BatchJobManager::BatchJobManager
    /////////////////////////////////////////////////////////////
    BatchJobManager::BatchJobManager(int cnt,BatchCommandInfo * (*getNextBatch)()):
        freeList(0),
        activeList(0),
        m_pfnGetNextBatch(getNextBatch)
    {

        ListNode *pCur= freeList = new ListNode();

        for(int i=1;i<cnt;i++,pCur=pCur->pNext)
        {
            pCur->pNext = new ListNode();
        }

        pCur->pNext=0;
    }

    /////////////////////////////////////////////////////////////
    // BatchJobManager::~BatchJobManager
    /////////////////////////////////////////////////////////////
    BatchJobManager::~BatchJobManager()
    {
        ListNode *pNext, *pCur;


        for(pCur= freeList;pCur;pCur=pNext)
        {
            pNext=pCur->pNext;
            delete pCur;
        }

        freeList = 0;

        for(pCur= activeList;pCur;pCur=pNext)
        {
            pNext=pCur->pNext;
            delete pCur;
        }

        activeList = 0;
    }

    /////////////////////////////////////////////////////////////
    // BatchJobManager::RunCycle
    /////////////////////////////////////////////////////////////
    ///The logic here is high risk, I must rethink it.
    void BatchJobManager::RunCycle()
    {
        if(SwatState::bEnabled)
        {
            BatchCommandInfo *pBatchCommandInfo;
            ListNode *cur=activeList;
            ListNode *last = 0;

            //Cycle all the batch scrupts and execute.
            for(;cur!=0;last=cur,cur=cur->pNext)
            {
again_0:

                int iTemp = cur->clBatchExecuter.run();

                switch(iTemp)
                {
                    case BatchExecuter::STATE_END:

                        cur->clBatchExecuter.reset();

                        cur = freeNode(last,cur);

                        if(cur)
                        {
                            goto again_0;
                        }


                        goto stop_0;

                    case BatchExecuter::STATE_RESETALL:

                        cur = activeList;

                        for(ListNode *tmp;cur!=0;)
                        {
                            cur->clBatchExecuter.reset();
                            tmp = cur->pNext;

                            cur->pNext = freeList;
                            freeList=cur;

                            cur=tmp;
                        }

                        activeList = 0;

                        goto stop_0;


                    case BatchExecuter::STATE_WAIT:
                        break;
                }


            }


stop_0:

            while(this->hasFreeNode() && (pBatchCommandInfo = this->getNextBatch()) )
            {
                ListNode *pListNode = getFreeNode();

                pListNode->clBatchExecuter.init(pBatchCommandInfo);

                this->appendToActiveList(pListNode);

            }

            //Increment the time
            BatchExecuter::updateTime();
        }
    }

}
