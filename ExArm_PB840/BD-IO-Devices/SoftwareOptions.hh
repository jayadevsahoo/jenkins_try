#ifndef SoftwareOptions_HH
#define SoftwareOptions_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SoftwareOptions - storage for the ventilator software options.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/SoftwareOptions.hhv   25.0.4.0   19 Nov 2013 13:54:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 021  By:  mnr    Date:  07-Jan-2010    SCR Number: 6437
//  Project:  NEO
//  Description:
//		SRS tracing added as per code review feedback.
//
//  Revision: 020  By:  mnr    Date:  28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Added ADVANCED and LOCKOUT options.
//
//  Revision: 019  By:  gdc    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 018   By: gdc    Date: 17-Feb-2009    SCR Number: 6019
//  Project:  840S
//  Description:
//      Modified to increase data key reliability. Corrected reverse
//      logic problem in isUpdateHours and renamed method to
//      isDataKeyHoursUpdateEnabled.
//
//  Revision: 017   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 016  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 015   By: rhj   Date:  20-Oct-2008    SCR Number: 6435
//  Project:  S840
//  Description:
//      Added LEAK_COMP enum.
// 
//  Revision: 014   By: gdc   Date:  21-Feb-2007    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      RESPM Project related changes.    
//
//  Revision: 013   By: erm   Date: 19-Dec-2002   DR Number: 6028 
//  Project:  EMI
//  Description:
//      Added ability to copy configuration code.
//      Made generateValidationCode() public.
//      Allowed BD CPU to use getDataKeyType().
//      
//
//  Revision: 012   By: syw   Date: 23-Aug-2000   DR Number:  5753
//  Project:  Delta
//  Description:
//      Added SINGLE_SCREEN enum.
//
//  Revision: 011   By: sah   Date:  17-Jul-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added VTPC option id
//
//  Revision: 010   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      VTPC project-related changes:
//      *  added PAV option id
//
//  Revision: 009   By: sah   Date: 30-Mar-2000   DR Number:  5671
//  Project:  NeoMode
//  Description:
//      Added new method, 'getDataKeyType()', for allowing the distinguishing
//      between data key types.
//
//  Revision: 008   By: hct   Date: 20-DEC-1999   DR Number: 5571
//  Project:  NeoMode
//  Description:
//      Added comment referring to document 4-070136-45 "Datakey Requirements
//      Specification" which should be updated when options are added.
//
//  Revision: 007   By: sah   Date: 24-Sep-1999   DR Number: 5424
//  Project:  NeoMode
//  Description:
//      Added capability for user-controlled option states.  Also,
//      redesigned with a static, global interface, with enums for
//      the testing of an option state, rather than unique methods.
//
//  Revision: 006  By: syw    Date: 28-Oct-1999   DR Number: 5474
//  Project:  ATC
//  Description:
//		Removed PAV_MASK.  Use bit 1 for ATC.
//
//  Revision: 005  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 004  By:  healey    Date:  08-Jan-99    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//          ATC initial release
//          Added new mask for ATC support
//
//  Revision: 003  By:  syw    Date: 19-Nov-1998    DR Number: DCS 5218
//       Project:  BiLevel
//       Description:
//          Added isOptionsFieldOkay() method.
//
//  Revision: 002  By:  syw    Date: 24-Aug-1998    DR Number: DCS 5144
//       Project:  Sigma (840)
//       Description:
//          Implement new data key layout with configuration code and
//			validation code layout.
//
//  Revision: 001  By:  syw    Date: 04-Dec-1996    DR Number: none
//       Project:  Sigma (840)
//       Description:
//             BiLevel initial version.
//
//====================================================================

#include "Sigma.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes

#include "SerialNumber.hh"

//@ End-Usage

struct ConfigurationCodeMapping
{
	Uint32 customerOptions ;
	Uint32 developmentOptions : 4 ;
	Uint32 keyType : 4 ;
	Uint32 ventType : 4 ;
	Uint32 reserved : 20 ;
} ;

typedef struct ConfigurationCodeMapping ConfigurationCode ;

class SoftwareOptions
{
  public:

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! NOTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
	// When key types are added to this enumerator, document 4-070136-45
    // "Datakey Requirements Specification" should be updated accordingly
	// so that manufacturing can produce datakeys which are correct.
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! NOTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
  	enum KeyTypes { ORIGINAL = 0,
  					OPTION_FORMATTED,
  					STANDARD,
  					ENGINEERING,
  					SALES,
  					MANUFACTURING,
  					LAST_VALID_KEY_TYPE = MANUFACTURING} ;

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! NOTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
	// When options are added to this enumerator, document 4-070136-45
    // "Datakey Requirements Specification" should be updated accordingly
	// so that manufacturing can produce datakeys which are correct.
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! NOTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
  	enum OptionId
	{
		//  $[BL04079] -- BiLevel is an installable option
		BILEVEL		= 0,

		//  $[TC04042] -- TC is an installable option
		ATC			= 1,

		//  $[NE04000] -- NeoMode is an installable option
		NEO_MODE	= 2,

		//  single screen option removed  - now reserved
		RESERVED1	= 3,

		//  $[VC04000] -- VTPC is an installable option
		VTPC		= 4,

		//  $[PA04000] -- PAV is an installable option
		PAV			= 5,

		// $[RM12067] -- RESPIRATORY MECHANICS is an installable option
		RESP_MECH   = 6,

		// $[TR01180] -- TRENDING is an installable option
		TRENDING    = 7,

		// $[LC04000] -- LEAK COMPENSATION is an installable option
		LEAK_COMP   = 8,

		// $[LC04002] -- NeoMode Update including CPAP and +20% O2
		NEO_MODE_UPDATE   = 9,

		// $[NV04002] -- NeoMode Advanced including 2mL/0.3kg/presets/events
		NEO_MODE_ADVANCED = 10,

		// $[NV04000] -- NeoMode Lockout shall be an installable option
		NEO_MODE_LOCKOUT = 11,

		NUM_OPTIONS
	};
	
	//length of Optionkey
	enum{SERIAL_KEY_LENGTH =  24};

    SoftwareOptions( void) ;
    ~SoftwareOptions( void) ;
    SoftwareOptions( const SoftwareOptions&) ;
    void operator=( const SoftwareOptions&) ;
    
#if defined(SIGMA_GUI_CPU)
	// provide only r-value access on GUI CPU...
	static inline const SoftwareOptions&  GetSoftwareOptions(void);
#else
	// allow l-value access on BD CPU...
	static inline SoftwareOptions&        GetSoftwareOptions(void);
#endif // defined(SIGMA_GUI_CPU)

	static Boolean  IsOptionEnabled(const OptionId optionId);

	static void  EnableOption (const OptionId optionId);
	static void  DisableOption(const OptionId optionId);

    static void SoftFault( const SoftFaultID	softFaultID,
						   const Uint32		lineNumber,
						   const char		*pFileName  = NULL, 
						   const char		*pPredicate = NULL) ;

	Boolean isSetFlashSerialNumber( void) const;
	void validateData( void) ;
	void copyConfigurationCode(ConfigurationCode & configCode);
	Uint32 generateValidationCode( void) const;

	Boolean isStandardDevelopmentFunctions( void) const;
	KeyTypes getDataKeyType( void) const ;

#if defined (SIGMA_GUI_CPU) || defined (SIGMA_DEVELOPMENT)
	Boolean isOptionsFieldOkay( void) const ;
	Boolean isVerifySerialNumber( void) const;
	Boolean isKeyTypeSales( void) const;
#endif // defined (SIGMA_GUI_CPU) || defined (SIGMA_DEVELOPMENT)

#if defined (SIGMA_BD_CPU) || defined (SIGMA_DEVELOPMENT)
	inline void setCustomerOptions( const Uint32 options) ;
	inline void setDevelopmentOptions( const Uint32 options) ;
	inline void setKeyType( const Uint32 keyType) ;
	inline void setVentType( const Uint32 ventType) ;
	inline void setReserved( const Uint32 reserved) ;
	inline void setValidationCode( const Uint32 code) ;
	inline void setSerialNumber( const SerialNumber sn) ;
	inline void setDataKeyType( Uint32 type) ;

	Boolean isDataKeyHoursUpdateEnabled(void) const;
#endif // defined (SIGMA_BD_CPU) || defined (SIGMA_DEVELOPMENT)
	
  protected:

  private:

	
	
	//@ Data-Member: configCode_
	//	code to indicate software configuration (i.e. what options are available)
	ConfigurationCode configCode_ ;

	//@ Data-Member: validationCode_
	// code to verify that configCode_ is valid
	Uint32 validationCode_ ;

	//@ Data-Member: serialNumber_
	// bd serial number to verify validation code
	SerialNumber serialNumber_ ;

	//@ Data-Member: optionsFieldOkay_ ;
	// set if the validation code is correct
	Boolean optionsFieldOkay_ ;	

	//@ Data-Member: dataKeyType_
	// original layout of data key type (PRODUCTION or DEMO)
	Uint32 dataKeyType_ ;

	//@ Data-Member: GlobalSoftwareOptions_
	// Statically allocated instance of this class, that is a global reference
	// to the current software options.
	static SoftwareOptions  GlobalSoftwareOptions_;
} ;

#include "SoftwareOptions.in"

#endif // SoftwareOptions_HH 
