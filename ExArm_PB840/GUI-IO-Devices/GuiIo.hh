#ifndef GuiIo_HH
# define GuiIo_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
//@ File: GuiIo.hh - GUI-CPU I/O device addresses and hooks
//---------------------------------------------------------------------
//  Version-Information
// @(#) $Header:   /840/Baseline/GUI-IO-Devices/vcssrc/GuiIo.hhv   25.0.4.0   19 Nov 2013 14:12:20   pvcs  $
//
//  Modification-Log
//
//  Revision: 004  By:  mjf    Date:  17-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             New CompactFlash class.
//
//  Revision: 003  By:  gdc    Date:  04-Jan-2001    DR Number: 5493
//       Project:  GuiComms
//       Description:
//             New TouchDriver.
//
//  Revision: 002  By:  gdc    Date:  28-MAY-1997    DR Number: 2016
//       Project:  Sigma (R8027)
//       Description:
//             Removed unncessary reference to MFP901.hh and associated
//             unused interrupt vector references.
//
//  Revision: 001  By:  clw    Date:  28-Mar-1997    DR Number: <none>
//       Project:  Sigma (R8027)
//       Description:
//             Initial version to coding standards (Integration baseline).
//=====================================================================

# include "Sigma.hh"
# include "MemoryMap.hh"
# include "IpcIds.hh"

//@ Type GuiIoDevicesClassId
// Ids of all the classes of this subsystem.  Note that some of
// the code is implemented as free functions.
enum GuiIoDevicesClassId
{
   TOUCH_DRIVER,
   COMPACT_FLASH,
   NUM_GUI_IO_DEVICES_CLASSES
};

	// all non-static functions contained in GuiIo.cc
//@ Begin-Free-Function
extern	void	GuiIoDevicesInitialize(void);
extern	Boolean	IsSVOLedOn(void);
extern	Boolean	IsVentInopLedOn(void);

	// diagnostic functions not part of production code
# ifdef	SIGMA_DEVELOPMENT
extern	void	TestFrontPanelLeds(void);
extern	void	TestKeyboardLeds(void);
extern	void	HeartBeatTask(void);
extern	void	HeartBeatTaskInit(void);
# endif	// SIGMA_DEVELOPMENT

	// initialization functions contained in *Task.cc
extern	void	KeysDrvTaskInit(void);		// in KeysTask.cc
extern	void	KnobDrvTaskInit(void);		// in KnobTask.cc
extern	void	TouchDrvTaskInit(void);		// in TouchTask.cc

	// task entry points contained in *Task.cc
extern	void	KeysDrvTask(void);		// in KeysTask.cc
extern	void	KnobDrvTask(void);		// in KnobTask.cc
extern	void	TouchDrvTask(void);		// in TouchTask.cc

	// entry points to read rotary encoder accumulated value
extern	Int16	GetKnobDelta(void);		// in KnobTask.cc
extern	void 	AckKnobEvent(void);		// in KnobTask.cc

	// entry points to redirect event notification.
	// NB all 3 default to USER_ANNUNCIATION_Q
extern	void	RouteKeyEvents(const IpcId newQId = NULL_ID);
extern	void	RouteKnobEvents(const IpcId newQId = NULL_ID);
extern	void	RouteTouchEvents(const IpcId newQId = NULL_ID);
	
//@ End-Free-Function

	// USER_ANNUNCIATION_Q flow control mechanisms:
	//	set FALSE when a complete input (touch & release)
	//	is put into the Q by Keys/Touch task;
	//	set TRUE when a complete input (touch & release)
	//	is taken out of the Q by GUI-App task.
	// When flag is FALSE, no further input of that type is
	// permitted to enter the Q.
	// CAUTION:	better be initialized to TRUE, else disaster.
extern		Boolean	KeyInputAllowed;	// for keyboard
extern		Boolean	TouchInputAllowed;	// for touch screen

	// GUI I/O Interrupt assignments
extern	"C"	Uint32	KEY_DOWN_INTR;
extern	"C"	Uint32	KEY_UP_INTR;
extern	"C"	Uint32	TOUCH_INTR;


	// not-so-elegant debugging tools
# ifdef	GUI_IO_DEBUG		// enable debug output

#  include <stdio.h>

#  define DBG(string)			printf(string)
#  define DBG1(string, a)		printf(string, a)
#  define DBG2(string, a, b)		printf(string, a, b)
#  define DBG3(string, a, b, c)		printf(string, a, b, c)
#  define DBG4(string, a, b, c, d)	printf(string, a, b, c, d)

# else				// disable debug output

#  define DBG(string)			;
#  define DBG1(string, a)		;
#  define DBG2(string, a, b)		;
#  define DBG3(string, a, b, c)		;
#  define DBG4(string, a, b, c, d)	;

# endif	// GUI_IO_DEBUG

#endif	// GuiIo_HH
