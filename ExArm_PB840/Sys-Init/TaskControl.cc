#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: TaskControl - Task-Control Task Main Processing.
//---------------------------------------------------------------------
//@ Interface-Description
//  The TaskControl class contains the primary operating components for
//  the Task Control task.  These components include the finite state
//  machine, AppContext, network message callback function, and all 
//  TaskControlMessage callback functions.  It also contains the Task
//  Control task event loop which receives messages and dispatches 
//  them to the callback functions.  The callback function then sends 
//  the message to the current state object for processing.  
//  The Initialize method instantiates
//  all function static AppContext by calling its accessor method.  
//  Once the MainLoop method is entered from Sys_Init::Initialize,
//  the task waits for a queue event, dequeues the message, dispatches
//  the message and then waits for another queue event.  This loop
//  repeats indefinitely.
//---------------------------------------------------------------------
//@ Rationale
//  The TaskControl class encapsulates the primary operating components 
//  for the Task Control task including its event processing loop.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Initialize method instantiates the function static by calling 
//  the accessor routine GetAppContext.  These routines
//  initialize the function static and return a reference the object.
//  Subsequently, the accessor routines simply return a reference to
//  the previously initialized static object.  The Initialize method
//  also registers the callback functions contained in TaskControl.
//  It registers the NetworkMessageCallback routine with the
//  Network-Application subsystem to receive messages from the network.
//  It also registers the callback functions to process 
//  TaskControlMessages.  The SendEvent static method queues a 
//  TaskControlMessage from the task control task to a specified queue.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/TaskControl.ccv   25.0.4.0   19 Nov 2013 14:33:56   pvcs  $
//
//@ Modification-Log
//
//   Revision 003   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 002   By:   gdc      Date: 11-JUN-1997 DR Number: 1902
//      Project:   Sigma   (R8027)
//      Description:
//         Modified for processing TaskReadyMessages and TransitionTimers
//         in a different context than other message so one transition
//         completes before starting another.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "TaskControl.hh"
#include "IpcIds.hh"
#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#endif

//@ Usage-Classes
#include "AppContext.hh"
#include "FsmState.hh"
#include "MsgQueue.hh"
#include "NetworkApp.hh"
#include "BdReadyMessage.hh"
#include "GuiReadyMessage.hh"
#include "StartupTimer.hh"
#include "SyncClockMessage.hh"
#include "SyncClockTimer.hh"
#include "TransitionTimer.hh"
#include "TaskControlAgent.hh"
#include "TaskControlMessage.hh"
#include "TaskControlQueueMsg.hh"
#include "TaskReadyMessage.hh"
#include "TransitionTimer.hh"
#include "TaskAgentManager.hh"
#include "Task.hh"
#include "MemoryHandle.hh"

//TODO E600 this include is temp for testing, remove later
//#include "BdQueuesMsg.hh"


//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SendEvent
//
//@ Interface-Description
//  Constructs a TaskControlQueueMsg from the TaskControlMessage
//  pointer and queues it to the specified queue.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskControl::SendEvent(Int32 queueId, TaskControlMessage* pMessage)
{
    CALL_TRACE("SendEvent(TaskControlMessage*,Int32)");

    TaskControlQueueMsg   msg( TaskControlQueueMsg::TASK_CONTROL_MSG,
                               MemoryHandle::CreateHandle(pMessage));
#ifdef SIGMA_DEVELOPMENT
    if (Sys_Init::GetDebugLevel(Sys_Init::ID_TASKCONTROL) >= 1)
        cout << "--- " << pMessage->nameOf() << " ---> QUEUE #" << queueId << endl;
#endif

    Int32 status = MsgQueue::PutMsg(queueId, msg.getQueueData());
    AUX_CLASS_ASSERTION(status == Ipc::OK, status);
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//  The TaskControl Initialize method initializes the function static
//  for the class.  It registers the NetworkMessageCallback function
//  with Network-Application subsystem to receive network messages.
//  Each TaskControlMessage callback routine is registered with the
//  TaskControl AppContext.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the static function GetAppContext to initialize 
//  the function static.  Calls the AppContext::setCallback method for
//  each TaskControlMessage callback routine.  Calls NetworkApp::
//  RegisterRecvMesg to receive the BdReadyMessage/GuiReadyMessage and
//  the SyncClockMessage (BD only).
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskControl::Initialize(void)
{
    CALL_TRACE("Initialize(void)");

    // controlled static initialization
    AppContext&   normalContext = GetNormalContext();
    AppContext&   transitionContext = GetTransitionContext();

    normalContext.setCallback(TaskControl::BdReadyCallback);
    normalContext.setCallback(TaskControl::GuiReadyCallback);
    normalContext.setCallback(TaskControl::CommDownCallback);
    normalContext.setCallback(TaskControl::CommUpCallback);
    normalContext.setCallback(TaskControl::ChangeStateCallback);

    transitionContext.setCallback(TaskAgentManager::Receive);
    transitionContext.setCallback(TaskControl::TransitionTimerCallback);
    (void) TransitionTimer::GetTimer();

#ifdef SIGMA_BD_CPU
    normalContext.setCallback(TaskControl::SyncClockCallback);

    NetworkApp::RegisterRecvMsg( GUI_NODE_READY_MSG_ID,
                                 TaskControl::NetworkMessageCallback);
    NetworkApp::RegisterRecvMsg( SYNC_CLOCK_MSG_ID,
                                 TaskControl::NetworkMessageCallback);
#endif
 
#ifdef SIGMA_GUI_CPU
    normalContext.setCallback(TaskControl::StartupTimerCallback);
    normalContext.setCallback(TaskControl::SyncClockTimerCallback);
    normalContext.setCallback(TaskControl::BdFailedCallback);

    (void) StartupTimer::GetTimer();
    (void) SyncClockTimer::GetTimer();

    NetworkApp::RegisterRecvMsg( BD_NODE_READY_MSG_ID,
                                 TaskControl::NetworkMessageCallback);
#endif
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MainLoop
//
//@ Interface-Description
//  The TaskControl MainLoop uses the AppContext to wait for a queue
//  event, dequeue the event and dispatch it to the message processing
//  callback functions.  Once called this method does not return.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the AppContext::mainLoop method.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
TaskControl::MainLoop(void)
{
#ifndef SIGMA_UNIT_TEST
    CALL_TRACE("MainLoop(void)");

    //  main loop -- never returns
    for (;;) 
    {
		AppContext & currentContext = TaskControl::GetAppContext();
        currentContext.dispatchEvent(currentContext.nextEvent());
    }
    // $[TI1]

#endif // !SIGMA_UNIT_TEST
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAppContext
//
//@ Interface-Description
//  The TaskControl GetAppContext method initializes the static 
//  AppContext on the first pass and returns a reference to the static
//  AppContext.  On subsequent calls, it simply returns a reference to
//  the previously initialized AppContext.  It constructs the AppContext
//  with task control's queue identifier so that AppContext::mainLoop
//  can be used.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Not inline since these contain function static.  The AppContext
//  is constructed to wait on the TASK_CONTROL_Q.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AppContext&
TaskControl::GetAppContext(void)
{
    CALL_TRACE("GetAppContext(void)");

    if ( FsmState::IsTransitioning() )
    {                                                      // $[TI1.1]
        return TaskControl::GetTransitionContext();
    }
    else
    {                                                      // $[TI1.2]
        return TaskControl::GetNormalContext();
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetNormalContext
//
//@ Interface-Description
//  The TaskControl GetAppContext method initializes the static
//  AppContext on the first pass and returns a reference to the static
//  AppContext.  On subsequent calls, it simply returns a reference to
//  the previously initialized AppContext.  It constructs the AppContext
//  with task control's queue identifier so that AppContext::mainLoop
//  can be used.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Not inline since these contain function static.  The AppContext
//  is constructed to wait on the TASK_CONTROL_Q.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
AppContext&
TaskControl::GetNormalContext(void)
{
    CALL_TRACE("GetNormalContext(void)");
    static MsgQueue   msgQueue(TASK_CONTROL_Q);
    static AppContext appContext(&msgQueue);
 
    // $[TI1]
    return appContext;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTransitionContext
//
//@ Interface-Description
//  The TaskControl GetAppContext method initializes the static
//  AppContext on the first pass and returns a reference to the static
//  AppContext.  On subsequent calls, it simply returns a reference to
//  the previously initialized AppContext.  It constructs the AppContext
//  with task control's queue identifier so that AppContext::mainLoop
//  can be used.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Not inline since these contain function static.  The AppContext
//  is constructed to wait on the TASK_CONTROL_Q.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
AppContext&
TaskControl::GetTransitionContext(void)
{
    CALL_TRACE("GetNormalContext(void)");
    static MsgQueue   msgQueue(TASK_CONTROL_TRANS_Q);
    static AppContext appContext(&msgQueue);
 
    // $[TI1]
    return appContext;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommDownCallback
//
//@ Interface-Description
//  The TaskControl CommDownCallback static method receives a 
//  CommDownMessage and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::CommDownCallback(const CommDownMessage& rMessage)
{
    CALL_TRACE("CommDownCallback(CommDownMessage&)");
    FsmState::GetCurrentState().receiveCommDown(rMessage);
    // $[TI1]
}
 
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CommUpCallback
//
//@ Interface-Description
//  The TaskControl CommUpCallback static method receives a 
//  CommUpMessage and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::CommUpCallback(const CommUpMessage& rMessage)
{
    CALL_TRACE("CommUpCallback(CommUpMessage&)");
    FsmState::GetCurrentState().receiveCommUp(rMessage);
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdReadyCallback
//
//@ Interface-Description
//  The TaskControl BdReadyCallback static method receives a 
//  BdReadyMessage and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::BdReadyCallback(const BdReadyMessage& rMessage)
{
    CALL_TRACE("BdReadyCallback(const BdReadyMessage&)");
    FsmState::GetCurrentState().receiveBdReady(rMessage);
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiReadyCallback
//
//@ Interface-Description
//  The TaskControl GuiReadyCallback static method receives a 
//  GuiReadyMessage and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::GuiReadyCallback(const GuiReadyMessage& rMessage)
{
    CALL_TRACE("GuiReadyCallback(const GuiReadyMessage&)");
    FsmState::GetCurrentState().receiveGuiReady(rMessage);
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeStateCallback
//
//@ Interface-Description
//  The TaskControl ChangeStateCallback static method receives a 
//  ChangeStateMessage and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::ChangeStateCallback(const ChangeStateMessage& rMessage)
{
    CALL_TRACE("ChangeStateCallback(const ChangeStateMessage&)");
    FsmState::GetCurrentState().receiveChangeState(rMessage);
    // $[TI1]
}

 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SyncClockCallback
//
//@ Interface-Description
//  The TaskControl SyncClockCallback static method receives a 
//  SyncClockCallback and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::SyncClockCallback(const SyncClockMessage& rMessage)
{
    CALL_TRACE("SyncClockCallback(const SyncClockMessage&)");
    FsmState::GetCurrentState().receiveSyncClock(rMessage);
    // $[TI1]
}

 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: StartupTimerCallback
//
//@ Interface-Description
//  The TaskControl StartupTimerCallback static method receives a 
//  StartupTimer and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::StartupTimerCallback(const StartupTimer& rMessage)
{
    CALL_TRACE("StartupTimerCallback(const StartupTimerCallback&)");
    FsmState::GetCurrentState().receiveStartupTimer(rMessage);
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SyncClockTimerCallback
//
//@ Interface-Description
//  The TaskControl SyncClockTimerCallback static method receives a 
//  SyncClockMessage and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::SyncClockTimerCallback(const SyncClockTimer& rMessage)
{
    CALL_TRACE("SyncClockTimerCallback(const SyncClockTimer&)");
    FsmState::GetCurrentState().receiveSyncClockTimer(rMessage);
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TransitionTimerCallback
//
//@ Interface-Description
//  The TaskControl TransitionTimerCallback static method receives a 
//  TransitionTimer and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::TransitionTimerCallback(const TransitionTimer& rMessage)
{
    CALL_TRACE("TransitionTimerCallback(const TransitionTimer&)");
    FsmState::GetCurrentState().receiveTransitionTimer(rMessage);
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdFailedCallback
//
//@ Interface-Description
//  The TaskControl BdFailedCallback static method receives a 
//  BdFailedMessage and forwards it to the current FsmState for 
//  processing.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::BdFailedCallback(const BdFailedMessage& rMessage)
{
    CALL_TRACE("BdFailedCallback(const BdFailedMessage&)");
    FsmState::GetCurrentState().receiveBdFailed(rMessage);
    // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NetworkMessageCallback
//
//@ Interface-Description
//  The TaskControl NetworkMessageCallback static function provides 
//  callback processing for messages received from the Network-
//  Application subsystem via the interprocessor network.  TaskControl
//  initialization registers this callback function with Network-
//  Application.  Upon receiving one of the registered message ids,
//  Network-Application calls this function within its own task thread.
//  Since this callback function executes at the same priority as the 
//  Network-Application task, this function processes the message only
//  minimally.  This function parses the data contained in a 
//  Network-Application buffer to determine its class identifier.  
//  Upon determining the message class, a new message is (copy) 
//  constructed in task control's BufferPool.  This reconstructs the
//  message's virtual pointers which are not valid since the address
//  space has changed.  This function then queues the constructed 
//  message to task control's MsgQueue so it can be
//  processed at task control's normal priority outside 
//  Network-Application's task thread.  This method will assert upon
//  receiving a Network-Application data buffer containing an invalid 
//  class identifier. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the TaskControlMessage::isA base class method to determine the
//  class identifier.  Once identified, the copy constructor for the
//  identified class constructs a new object in a BufferPool buffer
//  allocated with the TaskControlMessage new operator.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
TaskControl::NetworkMessageCallback( XmitDataMsgId,
                                     void * pData,
                                     size_t)
{
    CALL_TRACE("NetworkMessageCallback(XmitDataMsgId, void *, size_t)");
 
    const TaskControlMessage&  msg = *(const TaskControlMessage*)pData;
 
    switch ( msg.isA() )
    {
      case Sys_Init::ID_BDREADYMESSAGE:  // $[TI1]
        {
          // cast network object to BdReadyMessage
          const BdReadyMessage& rBdReady = *(const BdReadyMessage*)pData;
 
          // allocate free store and copy construct new object
          BdReadyMessage* pEvent = new BdReadyMessage(rBdReady);
 
          //  add contructed event to Task Control queue and process at
          //  Task Control's normal priority level
          TaskControlAgent::AddEvent(pEvent);
 
          break;
        }
 
      case Sys_Init::ID_GUIREADYMESSAGE:  // $[TI2]
        {
          // cast network object to GuiReadyMessage
          const GuiReadyMessage& rGuiReady = *(const GuiReadyMessage*)pData;
 
          // allocate free store and copy construct new object
          GuiReadyMessage* pEvent = new GuiReadyMessage(rGuiReady);
 
          //  add contructed event to Task Control queue and process at
          //  Task Control's normal priority level
          TaskControlAgent::AddEvent(pEvent);
 
          break;
        }
 
      case Sys_Init::ID_SYNCCLOCKMESSAGE:  // $[TI3]
        {
          // cast network object to SyncClockMessage
          const SyncClockMessage& rSyncClock = *(const SyncClockMessage*)pData;
 
          // allocate free store and copy construct new object
          SyncClockMessage* pEvent = new SyncClockMessage(rSyncClock);
 
          //  add contructed event to Task Control queue and process at
          //  Task Control's normal priority level
          TaskControlAgent::AddEvent(pEvent);
 
          break;
        }

      default:  // $[TI4]
          AUX_CLASS_ASSERTION_FAILURE(msg.isA());
          break;
    }
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
 
void
TaskControl::SoftFault(const SoftFaultID  softFaultID,
                         const Uint32       lineNumber,
                         const char*        pFileName,
                         const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_TASKCONTROL, 
                          lineNumber, pFileName, pPredicate);
}
 
 
//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================

