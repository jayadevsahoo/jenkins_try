///***************************************************************************
//*
//*		Copyright (c) 1992 READY SYSTEMS CORPORATION.
//*
//*	All rights reserved. READY SYSTEMS' source code is an unpublished
//*	work and the use of a copyright notice does not imply otherwise.
//*	This source code contains confidential, trade secret material of
//*	READY SYSTEMS. Any attempt or participation in deciphering, decoding,
//*	reverse engineering or in any way altering the source code is
//*	strictly prohibited, unless the prior written consent of
//*	READY SYSTEMS is obtained.
//*
//*
//*	Module Name:		%M%
//*
//*	Identification:		%Z% %I% %M%
//*
//*	Date:			%G%  %U%
//*
//****************************************************************************
//*/
//
///*
// *
// * File:
// *
// *    serialpkt.h
// *
// * Author:
// *
// *    Mike Milne
// *
// * Date:
// *
// *    8-06-92
// *
// * Contents:
// *
// *    This header file contains prototypes, type definitions, and constants
// *    to support serial packet encapsulation and decapsulation. Capsulation
// *    is the method used to send datagrams (PACKETS) across an unreliable
// *    medium (No Hardware Error Correction) while maintaining data integrity.
// *
// * Revision History:
// *
// *    Name |   Date   | Description of modification
// *    -----+----------+-----------------------------------------------------
// *    MWM  | 08-06-92 | First version
// *    -----+----------+-----------------------------------------------------
// *
// *****************************************************************************
// *                                                                           *
// *		Copyright (c) 1990 READY SYSTEMS CORPORATION.                *
// *                                                                           *
// *	All rights reserved. READY SYSTEMS' source code is an unpublished    *
// *	work and the use of a copyright notice does not imply otherwise.     *
// *	This source code contains confidential, trade secret material of     *
// *	READY SYSTEMS. Any attempt or participation in deciphering, decoding,*
// *	reverse engineering or in any way altering the source code is        *
// *	strictly prohibited, unless the prior written consent of             *
// *	READY SYSTEMS is obtained.                                           *
// *                                                                           *
// *                                                                           *
// *****************************************************************************/
//
//#ifndef _SERIALPKT_H_
//#define _SERIALPKT_H_
//
////#include <compiler.h>
////#include <ansiprot.h>
////#include <logio.h>
//
///*
// * Serial Packets have a preamble and a data section. The preamble
// * section can be divided into 2 parts. The first part of the preamble
// * is the sync chars which may or may not be used but can include more
// * than one character. The second part of the preamble is the length
// * characters which is the data section size. To read a packet, the preamble
// * is read, which gives information about the message to allow the rest of
// * the message to be read. The message should then be decapsulated before
// * forwarded to the client.
// */
//
///* define the preamble type */
//#define PKT_SYNC_CNT_MAX	8	/* maximum sync characters allowed */
//
////typedef struct {
////	BYTE sync_cnt;			/* the number of sync chars sent */
////	BYTE sync_chars[PKT_SYNC_CNT_MAX];	/* the sync chars in order */
////	BYTE len_cnt;			/* the number of length chars */
////	WORD max_pkt_len;		/* the max size of the data field */
////}pkt_preamble_t;
//
///* define return codes for encaps and decaps */
//
//typedef int pkt_serial_status_t;	/* pkt status return type */
//
//#define PKT_SERIAL_DECAPS_SUCCESS	(pkt_serial_status_t)0
//#define PKT_SERIAL_ENCAPS_SUCCESS	(pkt_serial_status_t)0
//#define PKT_SERIAL_DECAPS_FAILURE	(pkt_serial_status_t)1
//#define PKT_SERIAL_DECAPS_CRCERR	(pkt_serial_status_t)2
//#define PKT_SERIAL_RESOURCES		(pkt_serial_status_t)3
//
///* version of connection server */
//#define VER4 1        /* byte_stuffying version */
//#define VER3 0        /* old version */
//
///* function prototypes */
//void pkt_get_serial_preamble(ANSIPROT1(pkt_preamble_t *preamblep));
//pkt_serial_status_t pkt_serial_encaps(ANSIPROT4(logio_buff_t **lbuffp,
//	DWORD ip_addr, WORD port, WORD *msg_sizep));
//pkt_serial_status_t pkt_serial_encaps_v2(ANSIPROT6(logio_buff_t **lbuffp,
//	DWORD ip_addr, WORD port, WORD *msg_sizep, WORD *pad_len, WORD ver));
//
//pkt_serial_status_t pkt_serial_decaps(ANSIPROT4(logio_buff_t **lbuffpp,
//		DWORD *ip_addrp, WORD *portp, WORD *msg_sizep));
//
//
//#endif /* !_SERIALPKT_H_ */
