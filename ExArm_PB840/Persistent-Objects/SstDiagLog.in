
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class:  SstDiagLog - SST Diagnostic Code Log.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/SstDiagLog.inv   25.0.4.0   19 Nov 2013 14:18:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' to non-inlined method.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifySelf()  [const]
//
//@ Interface-Description
//  Verify the integrity of this SST Diagnostic Log.  If this log is
//  determined to be valid, 'NovRamStatus::ITEM_VALID' is returned.
//  If not, 'NovRamStatus::ITEM_INVALID' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID  ||
//   verifySelf() == NovRamStatus::ITEM_INVALID)
//@ End-Method
//=====================================================================

inline NovRamStatus::VerificationId
SstDiagLog::verifySelf(void) const
{
  CALL_TRACE("verifySelf()");
  return(verifyLog_(::MAX_SST_LOG_ENTRIES));
}   // $[TI1]
