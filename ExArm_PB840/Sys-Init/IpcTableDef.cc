#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
//@ Filename: IpcTableDef.cc - IpcTable::Table_ definition.
//---------------------------------------------------------------------
//@ Interface-Description
//  This file contains the definition of the IpcTable::Table_ private
//  data member.  It is maintained separately from other IpcTable class
//  members to allow development configurations the ability to 
//  override the table's definition at link time.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  Defines the IpcTable::Table_.  Each entry defines an inter-process
//  communication object that OS-Foundation uses to initialize its
//  classes from.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/IpcTableDef.ccv   25.0.4.1   20 Nov 2013 17:37:46   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 014  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software. 
// 
//  Revision: 013   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 012   By: gdc    Date:  07-Jun-2007    DCS Number: 6237
//  Project: Trend
//  Description:
//		Added TREND_DATA_MGR_Q, COMPACT_FLASH_MT, TREND_EVENT_MT
//
//  Revision: 011   By: gdc    Date:  23-Jan-2001    DCS Number: 5493
//  Project: GuiComms
//  Description:
//		Added SMCP interface queues for aux serial ports.
//
//  Revision: 010   By: gdc    Date:  29-Jun-1999    DCS Number: 5327
//  Project: 840 Neonatal
//  Description:
//		Optimized VGA library for Neonatal project. Used an additional
//  	mutex so lower and upper screens can have separate contexts.
//
//  Revision: 009  By:  gdc    Date:  24-Nov-1997    DCS Number: 2605
//  Project:  Sigma (R8027)
//  Description:
//      Changed SERIAL_INTERFACE_Q to a MailBox (from MsgQueue) as part
//      of GUI-Serial-Interface restructuring. Removed DCI queues.
//
//  Revision: 008  By:  sah    Date:  30-Sep-1997    DCS Number: 2530
//  Project:  Sigma (R8027)
//  Description:
//      Added a 'NOVRAM_SEQ_NUM_BUFFER_MT' so that the 'NovRamSequenceNum'
//	class can use the 'DoubleBuffer(TYPE)' template, internally.
//
//  Revision: 007  By:  gdc    Date:  14-JUL-1997    DCS Number: 2177
//  Project:  Sigma (R8027)
//  Description:
//      Increased breath bar queue entries to 10 (from 4).
//
//  Revision: 006  By:  sah    Date:  25-Jun-1997    DCS Number: 2021
//  Project:  Sigma (R8027)
//  Description:
//      Added a 'TOUCH_TASK_Q' so that the touch task can register into
//      the task-control's state-change mechanism.
//
//  Revision: 005  By:  sah    Date:  25-Jun-1997    DCS Number: 1908
//  Project:  Sigma (R8027)
//  Description:
//      Added three new semaphores for use with double-buffered data
//      items.
//
//  Revision: 004  By:  gdc    Date:  18-Jun-1997    DCS Number: 1902
//  Project:  Sigma (R8027)
//  Description:
//      Added TASK_CONTROL_TRANS_Q as Task Control's transition queue.
//      Reduced elements in TASK_CONTROL_Q and reallocated them to 
//      TASK_CONTROL_TRANS_Q to accomodate transitional states.
//
//  Revision: 003  By:  yyy    Date:  10-May-1997    DCS Number: 2037
//  Project:  Sigma (R8027)
//  Description:
//      Added one new NOVRAM mutex.
//
//  Revision: 002  By:  sah    Date:  09-Apr-1997    DCS Number: 1894
//  Project:  Sigma (R8027)
//  Description:
//      Added two new NOVRAM mutexes.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "IpcIds.hh"
#include "IpcTable.hh"
#include "Ipc.hh"

//@ Usage-Classes
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

const IpcTableEntry IpcTable::Table_[] =
{
// Unique ID                    Type             Len   Name
// ---------------------        --------------   ----  -------------
   {USER_ANNUNCIATION_Q,         Ipc::MSG_QUEUE, 100,  "USER_ANNUN_Q"}
  ,{DEBUG_IO_Q,                  Ipc::MSG_QUEUE,  50,  "DEBUG_INPUT_Q"}
  ,{NOVRAM_SEQ_NUM_BUFFER_MT,    Ipc::MUTEX,       1,  "NOVRAM_SEQ_NUM_BUFFER_MT"}
  ,{NOVRAM_READ_COUNTER_MT,      Ipc::MUTEX,       1,  "NOVRAM_READ_COUNTER_MT"}
  ,{NOVRAM_DIAG_LOG_MT,          Ipc::MUTEX,       1,  "NOVRAM_DIAG_LOG_MT"}
  ,{NOVRAM_DIAG_LOG_ARRAY_MT,    Ipc::MUTEX,       1,  "NOVRAM_DIAG_LOG_ARRAY_MT"}
  ,{NOVRAM_DOUBLE_BUFF_MT,       Ipc::MUTEX,       1,  "NOVRAM_DOUBLE_BUFF_MT"}
  ,{NOVRAM_RESULT_TABLE_MT,      Ipc::MUTEX,       1,  "NOVRAM_RESULT_TABLE_MT"}
  ,{NOVRAM_RESULT_TABLE_ARRAY_MT,Ipc::MUTEX,       1,  "NOVRAM_RESULT_TABLE_ARRAY_MT"}
  ,{NOVRAM_ALARM_LOG_MT,         Ipc::MUTEX,       1,  "NOVRAM_ALARM_LOG_MT"}
  ,{NOVRAM_SEQUENCE_NUM_MT,      Ipc::MUTEX,       1,  "NOVRAM_SEQUENCE_NUM_MT"}
  ,{NOVRAM_DIAG_LOG_BUFFER_MT,   Ipc::MUTEX,       1,  "NOVRAM_DIAG_LOG_BUFFER_MT"}
  ,{NOVRAM_RESULT_TABLE_BUFFER_MT,Ipc::MUTEX,      1,  "NOVRAM_RESULT_TABLE_BUFFER_MT"}
  ,{NOVRAM_MISC_DATA_BUFFER_MT,  Ipc::MUTEX,       1,  "NOVRAM_MISC_DATA_BUFFER_MT"}
  ,{NOVRAM_DATA_CORR_INFO_MT,    Ipc::MUTEX,       1,  "NOVRAM_DATA_CORR_INFO_MT"}
  ,{NOVRAM_FIO2_CAL_INFO_MT,     Ipc::MUTEX,       1,  "NOVRAM_FIO2_CAL_INFO_MT"}
  ,{NOVRAM_BD_SYSTEM_STATE_MT,   Ipc::MUTEX,       1,  "NOVRAM_BD_SYSTEM_STATE_MT"}
  ,{RA_PUTMSG_MT,                Ipc::MUTEX,       1,  "RA_PUTMSG_MUTEX"}
  ,{KEYBOARD_POLL_Q,             Ipc::MSG_QUEUE,  50,  "KEYBOARD_POLL_Q"}
  ,{TASK_CONTROL_TRANS_Q,        Ipc::MSG_QUEUE,  20,  "TASK_CONTROL_TRANS_Q"}
  ,{TIMER_TBL_MT,                Ipc::MUTEX,       1,  "TIMER_TBL_MT"}
  ,{UPPER_VGA_ACCESS_MT,         Ipc::MUTEX,       1,  "UPPER_VGA_ACCESS_MT"}
  ,{OPERATING_PARAMETER_EVENT_Q, Ipc::MSG_QUEUE,  50,  "OP_EVENT_Q"}
  ,{TASK_CONTROL_Q,              Ipc::MSG_QUEUE,  10,  "TASK_CONTROL_Q"}
  ,{TASK_CONTROL_SIM_Q,          Ipc::MSG_QUEUE,  10,  "TASK_CONTROL_SIM_Q"}
  ,{TIMER_5MS_Q,                 Ipc::MSG_QUEUE,   4,  "TIMER_5MS_Q"}
  ,{SPIROMETRY_TASK_Q,           Ipc::MSG_QUEUE,   5,  "SPIROMETRY_Q"}
  ,{WAVEFORM_TASK_Q,             Ipc::MSG_QUEUE,  50,  "WAVEFORM_Q"}
  ,{BUFFER_LIST_MT,              Ipc::MUTEX,       1,  "BUFFER_LIST_MT"}
  ,{NETAPP_QUEUE_ACCESS_MT,      Ipc::MUTEX,       1,  "NETAPP_QUEUE_MT"}
  ,{NETAPP_ACKMSG_ACCESS_MT,     Ipc::MUTEX,       1,  "NETAPP_ACKMSG_MT"}
  ,{NETAPP_SENDMSG_ACCESS_MT,    Ipc::MUTEX,       1,  "NETAPP_SENDMSG_MT"}
  ,{SETTINGS_XACTION_Q,          Ipc::MSG_QUEUE,  20,  "SETTINGS_XACTION_Q"}
  ,{LOWER_VGA_ACCESS_MT,         Ipc::MUTEX,       1,  "LOWER_VGA_ACCESS_MT"}
  ,{TC_BUFFER_POOL_MT,           Ipc::MUTEX,       1,  "TC_BUFFER_POOL_MT"}
  ,{NOVRAM_XACTION_Q,            Ipc::MSG_QUEUE,  40,  "NOVRAM_XACTION_Q"}
  ,{SERVICE_MODE_TASK_Q,         Ipc::MSG_QUEUE,   4,  "SERVICE_MODE_TASK_Q"}
  ,{KNOB_DELTA_MT,               Ipc::MUTEX,       1,  "KNOB_DELTA_MT"}
  ,{BD_STATUS_TASK_Q,            Ipc::MSG_QUEUE,  50,  "BD_STATUS_TASK_Q"}
  ,{TASK_MONITOR_Q,              Ipc::MSG_QUEUE, 200,  "TASK_MONITOR_Q"}
  ,{BACKGROUND_CYCLE_Q,          Ipc::MSG_QUEUE,  20,  "BACKGROUND_CYCLE_Q"}
  ,{BREATH_DATA_Q,               Ipc::MSG_QUEUE,   6,  "BREATH_DATA_Q"}
  ,{SERVICE_MODE_TEST_Q,         Ipc::MSG_QUEUE,   4,  "SERVICE_MODE_TEST_Q"}
  ,{SERVICE_MODE_ACTION_Q,       Ipc::MSG_QUEUE,   4,  "SERVICE_MODE_ACTION_Q"}
  ,{SERIAL_INTERFACE_Q,          Ipc::MAILBOX,     1,  "SERIAL_INTERFACE_Q"}
  ,{SERIAL_INPUT_MT,             Ipc::MUTEX,       1,  "SERIAL_INPUT_MT"}
  ,{SERIAL_OUTPUT_MT,            Ipc::MUTEX,       1,  "SERIAL_OUTPUT_MT"}
  ,{WAVEFORM_DATA_POOL_MT,       Ipc::MUTEX,       1,  "WAVEFORM_DATA_POOL_MT"}
  ,{SERVICE_MODE_GUIIO_Q,        Ipc::MSG_QUEUE,  10,  "SERVICE_MODE_GUIIO_Q"}
  ,{XMIT_EV_TABLE_TASK_Q,        Ipc::MSG_QUEUE,   2,  "XMIT_EV_TABLE_TASK_Q"}
  ,{SM_GUI_TASK_CTRL_MSG_Q,      Ipc::MSG_QUEUE,   4,  "SM_GUI_TASK_CTRL_MSG_Q"}
  ,{TIMER_20MS_Q,                Ipc::MSG_QUEUE,  20,  "TIMER_20MS_Q"}
  ,{PATIENT_DATA_TIMEOUT_MT,     Ipc::MUTEX,       1,  "PATIENT_DATA_TIMEOUT_MT"}
  ,{BACKGROUND_FAULT_HANDLER_Q,  Ipc::MSG_QUEUE,  20,  "BACKGROUND_FAULT_HANDLER_Q"}
  ,{SERVICE_DATA_TASK_Q,         Ipc::MSG_QUEUE,  20,  "SERVICE_DATA_TASK_Q"}
  ,{GUIAPP_SCHEDULER_Q,          Ipc::MSG_QUEUE,  10,  "GUIAPP_SCHEDULER_Q"}
  ,{GUIAPP_SCHEDULER_MT,         Ipc::MUTEX,       1,  "GUIAPP_SCHEDULER_MT"}
  ,{GUIAPP_BREATH_BAR_Q,         Ipc::MSG_QUEUE,  10,  "GUIAPP_BREATH_BAR_Q"}
  ,{SERVICE_DATA_MT,             Ipc::MUTEX,       1,  "SERVICE_DATA_MT"}
  ,{TOUCH_TASK_Q,                Ipc::MSG_QUEUE,  10,  "TOUCH_TASK_Q"}
  ,{SMCP_INTERFACE1_Q,           Ipc::MAILBOX,     1,  "SMCP1_Q"}
  ,{SMCP_INTERFACE2_Q,           Ipc::MAILBOX,     1,  "SMCP2_Q"}
  ,{SMCP_INTERFACE3_Q,           Ipc::MAILBOX,     1,  "SMCP3_Q"}
  ,{COMPACT_FLASH_MT,            Ipc::MUTEX,       1,  "COMPACT_FLASH_MT"}
  ,{TREND_DATA_MGR_Q,            Ipc::MSG_QUEUE,   5,  "TREND_DATA_MGR_Q"} 
  ,{TREND_EVENT_MT,				 Ipc::MUTEX,	   1,  "TREND_EVENT_MT"}
  ,{VSET_SERVER_MT,				 Ipc::MUTEX,	   1,  "VSET_SERVER_MT"}
  ,{GAINS_MANAGER_MT,            Ipc::MUTEX,       1,  "GAINS_MANAGER_MT"}
  ,{CONTROLS_TUNING_MT,			 Ipc::MUTEX,	   1,  "CONTROLS_TUNING_MT"}
  ,{ADC_CHANNELS_MT,			 Ipc::MUTEX,       1,  "ADC_CHANNELS_MT"}
,{MEMORY_HANDLE_MT,			 Ipc::MUTEX,       1,  "MEMORY_HANDLE_MT"}
,{BD_BATCH_COMMAND_LIST_MT,	 Ipc::MUTEX,       1,  "BD_BATCH_COMMAND_LIST_MT"}
};
 
const Uint IpcTable::NumEntries_ 
           = sizeof(IpcTable::Table_) / sizeof(IpcTableEntry);
