/***************************************************************************
*
*		Copyright (c) 1992 READY SYSTEMS CORPORATION.
*
*	All rights reserved. READY SYSTEMS' source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	READY SYSTEMS. Any attempt or participation in deciphering, decoding,
*	reverse engineering or in any way altering the source code is
*	strictly prohibited, unless the prior written consent of
*	READY SYSTEMS is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/

#ifndef _EXTERNIO_H_
#define _EXTERNIO_H_

struct serial_dev_desc_t;

typedef struct {
   void (*reg_write_b)(_ANSIPROT2(BYTE *, BYTE )); /* and initialized PER DEVICE*/
   void (*reg_write_w)(_ANSIPROT2(WORD *, WORD));  /* these function pointers */
   void (*reg_write_l)(_ANSIPROT2(LONG *, LONG));  /* are used to call functions*/
   BYTE (*reg_read_b)( _ANSIPROT1(BYTE *)); /* that may contain board */
   WORD (*reg_read_w)( _ANSIPROT1(WORD *));  /* specific code */
   LONG (*reg_read_l)( _ANSIPROT1(LONG *));
   vbsp_return_t (*interrupt_start)(_ANSIPROT2(struct serial_dev_desc_t *,
					unsigned char));
   vbsp_return_t (*interrupt_end)(_ANSIPROT2(struct serial_dev_desc_t *,
					unsigned char));
   vbsp_return_t (*brd_spec_int_disable)(_ANSIPROT2(struct serial_dev_desc_t *,
					logio_int_entry_t *));
   vbsp_return_t (*brd_spec_int_enable)(_ANSIPROT2(struct serial_dev_desc_t *,
					logio_int_entry_t *));
	/* the following pointer should be used to add extra device
	 * specific information needed that all devices may not need
	 */
	void *specific;
} extern_fops_t;



#define REG_WRITE_B(devid,address,data)\
   if(devid->externio.reg_write_b)\
      (*devid->externio.reg_write_b)(address,data);\
   else\
      (BYTE)*address=(BYTE)data;
#define REG_WRITE_W(devid,address,data)\
   if(devid->externio.reg_write_w)\
      (*devid->externio.reg_write_w)(address,data);\
   else\
      (WORD)*address=(WORD)data;
#define REG_WRITE_L(devid,address,data)\
   if(devid->externio.reg_write_l)\
      (*devid->externio.reg_write_l)(address,data);\
   else\
      (LONG)*address=(LONG)data;

#define REG_READ_B(devid,address,value)\
      *value = (BYTE)*address;

#define REG_READ_W(devid,address,value)\
      *value = (WORD)*address;

#define REG_READ_L(devid,address,value)\
      *value = (LONG)*address;

#if 0
#define REG_READ_B(devid,address,value)\
   if(devid->externio.reg_read_b)\
      *value = (*devid->externio.reg_read_b)(address);\
   else\
      *value = (BYTE)*address;
#define REG_READ_W(devid,address)\
   if(devid->externio.reg_read_w)\
      (*devid->externio.reg_read_w)(address);\
   else\
      (WORD)*address;
#define REG_READ_L(devid,address)\
   if(devid->externio.reg_read_l)\
      (*devid->externio.reg_read_l)(address);\
   else\
      (LONG)*address;
#endif

#define INTERRUPT_START(dev, type) \
   if ((dev)->externio.interrupt_start)\
      (*(dev)->externio.interrupt_start)((dev), (type))

#define INTERRUPT_END(dev, type) \
   if ((dev)->externio.interrupt_end)\
      (*(dev)->externio.interrupt_end)((dev), (type))

#define BOARD_SPEC_INT_DISABLE(dev, intrpt) \
   if ((dev)->externio.brd_spec_int_disable) \
      (*(dev)->externio.brd_spec_int_disable)((dev), (intrpt))

#define BOARD_SPEC_INT_ENABLE(dev, intrpt) \
   if ((dev)->externio.brd_spec_int_enable) \
	   (*(dev)->externio.brd_spec_int_enable)((dev), (intrpt))

#endif
