#include "stdafx.h"
// This module is compiled only for BD CPU
#ifdef SIGMA_BD_CPU  

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: BdPost  - Breath Delivery CPU Power On Self Tests
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains the Phase 2 POST test definitions and accessor
//  method for the BD CPU.  The GetTestItems method returns the list of
//  tests appropriate for short or long POST depending on the shutdown 
//  and downtime status of the BD.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//    Faults during POST cause the test in progress to fail.
//---------------------------------------------------------------------
//@ Restrictions
//    None
//---------------------------------------------------------------------
//@ Invariants
//    None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST/vcssrc/BdPost.ccv   25.0.4.0   19 Nov 2013 14:17:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007    By: Gary Cederquist  Date: 10-AUG-1999  DR Number: 5502
//    Project: 806 Compressor
//    Description:
//       Removed AC Voltage Test which was redundant to background test.
//
//  Revision: 006    By: Gary Cederquist  Date: 26-JUL-1999  DR Number: 5488
//    Project: 840 ATC
//    Description:
//       Changed requirement id 11097 to TC11000.
//
//  Revision: 005    By: Gary Cederquist  Date: 05-MAY-1999  DR Number: 5373
//    Project: 840 ATC
//    Description:
//       Deconflicted LONG enumerator by defining a "classed" enumerator
//		 for PostType.
//
//  Revision: 004    By: Gary Cederquist  Date: 05-MAY-1999  DR Number: 5203
//    Project: 840 ATC
//    Description:
//       Implemented the PrevPostFailureTests to allow entry into 
//		 service mode after a previous POST failure. The PrevPostFailureTests
//		 skips all POST tests except for the service switch test that
//		 provides entry into service mode. POST bypasses the normal test
//		 sequence if a previous POST failure exists since running the 
//		 tests again will result in another POST failure which would not
//		 allow entry into service mode where a better diagnosis of the 
//		 problem can occur.
//
//  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
//    Project: Sigma (R8027)
//    Description:
//       Added testable item annotations.
//
//  Revision: 002    By: Gary Cederquist  Date: 07-MAY-1997  DR Number: 1996
//    Project: Sigma (R8027)
//    Description:
//       Added missing requirement #00306.
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "BdPost.hh"

#include <stdio.h>
#include <string.h>
#include "Cpu.hh"
#include "Analog.hh"
#include "PostNovram.hh"
#include "SafeState.hh"
#include "PostErrors.hh"
#include "Bto.hh"
#include "NmiRegister.hh"
#include "Ether.hh"
#include "Dram.hh"
#include "ServiceSwitch.hh"

// -------------------------------------------------------------------------
// This is the POST executive test structure for the BD CPU.  Each test
// Each test is executed in the order they appear in this structure.
//
//
// Structure Members:
//
// testName:        Text of test in progress, used to send status information
//                  to a debug port
//
// testNumber:      Value displayed on diagnostic LED indicator.
//
// pTestProcedure:  Pointer to test procedure to execute
//
// -----------------------------------------------------------------------------

// This set of tests is used when a previous MAJOR post failure has been
// logged. Ventilation is not allowed with a previous MAJOR so we can skip 
// POST tests that may be causing the MAJOR POST failure. This allows
// the operator a way to enter service mode and view the diagnostic log
// to further diagnose the cause of the MAJOR POST fault. The DRAM is
// cleared several times to give the operator enough time to press the
// service button.

const PostTestItem  BdPost::PrevPostFailureTests_[] =
{                                                 
   {"Service Switch Test",     POST_ERROR_SERVICE_SWITCH,          ServiceSwitch::TestAndArm      }
  ,{"DRAM Memory Clear",       POST_ERROR_DRAM_MEMORY,             Dram::Clear                    }
  ,{"DRAM Memory Clear",       POST_ERROR_DRAM_MEMORY,             Dram::Clear                    }
  ,{"DRAM Memory Clear",       POST_ERROR_DRAM_MEMORY,             Dram::Clear                    }
  ,{"DRAM Memory Clear",       POST_ERROR_DRAM_MEMORY,             Dram::Clear                    }
  ,{"Service Switch Test",     POST_ERROR_SERVICE_SWITCH,          ServiceSwitch::Disarm          }
  ,{"END OF TESTS",            POST_ERROR_UNUSED,                  NULL                           }
};

const PostTestItem  BdPost::ShortPostTests_[] =
{                                                 
   {"Service Switch Test",     POST_ERROR_SERVICE_SWITCH,          ServiceSwitch::TestAndArm      }
  ,{"Bus Timer Test",          POST_ERROR_BTO,                     Bto::Test                      }
  ,{"DRAM Parity Error Test",  POST_ERROR_DRAM_PARITY,             Dram::ParityTest               }
  ,{"NMI Register Test",       POST_ERROR_NMI_REGISTER,            NmiRegister::Test              }
  ,{"DRAM Memory Clear",       POST_ERROR_DRAM_MEMORY,             Dram::Clear                    }
  ,{"Service Switch Test",     POST_ERROR_SERVICE_SWITCH,          ServiceSwitch::Disarm          }
  ,{"END OF TESTS",            POST_ERROR_UNUSED,                  NULL                           }
};

//  Note: TestVentInop is tested prior to the NMI register since
//  VENT-INOP is reflected in the NMI source register as well.  Doing
//  the vent-inop test first will give the operator a better sense of
//  what's wrong with the ventilator other than a generalized register
//  failure.

const PostTestItem  BdPost::LongPostTests_[] =
{                                                 
/* Test                      Test    Test                   
   Name                      Num     procedure                         */

   {"Service Switch Test",     POST_ERROR_SERVICE_SWITCH,          ServiceSwitch::TestAndArm      }
  ,{"Bus Timer Test",          POST_ERROR_BTO,                     Bto::Test                      }
  ,{"DRAM Parity Error Test",  POST_ERROR_DRAM_PARITY,             Dram::ParityTest               }
  ,{"Test Vent Inop",          POST_ERROR_VENTINOP,                TestVentInop                   }
  ,{"NMI Register Test",       POST_ERROR_NMI_REGISTER,            NmiRegister::Test              }
  ,{"Ethernet Start",          POST_ERROR_LAN_SELFTEST_START,      Ether::TestStart               }
  ,{"Test Analog Interface",   POST_ERROR_ANALOG_INTERFACE_PCB,    Analog::TestAnalogInterfacePCB }
  ,{"DRAM Memory Test",        POST_ERROR_DRAM_MEMORY,             Dram::Test                     }
  ,{"Test ADC",                POST_ERROR_ADC,                     Analog::TestADC                }
  ,{"Test Analog Devices",     POST_ERROR_ANALOG_DEVICES,          Analog::TestAnalogDevices      }
  ,{"Test Safe State",         POST_ERROR_SAFESTATE_SYSTEM,        TestSafeStateSystem            }
  ,{"Test DAC",                POST_ERROR_DAC,                     Analog::TestDAC                }
  ,{"Test Serial Device",      POST_ERROR_SERIAL_DEVICE,           Analog::TestSerialDevice       }
  ,{"Ethernet End",            POST_ERROR_LAN_SELFTEST_END,        Ether::TestEnd                 }
  ,{"Service Switch Test",     POST_ERROR_SERVICE_SWITCH,          ServiceSwitch::Disarm          }
  ,{"END OF TESTS",            POST_ERROR_UNUSED,                  NULL                           }
};


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTestItems
//
//@ Interface-Description
//  Returns the ShortPostTests or LongPostTests based on the the short
//  POST criteria.
//
//  This routine reads the time from power down, and the current time
//  from the real time clock.  A validation of time is performed, if a
//  valid time is present the two times are compared to each other.
//
//  Short POST is returned if POWERFAIL is in progress and the time
//  since power was lost is less than 5 minutes and the ventilator
//  was not powered off by the AC power switch.  $[11029]
//
//  Since the reset umpire is run after all POST tests, a reset during
//  POST will not cause a long POST to occur (since the shutdown state
//  is not reset until the reset umpire runs).  However, a POST failure 
//  resets the shutdown state to ACSWITCH so long post (including the 
//  service switch test) is run after a POST failure.
//
//  $[00306]
//  $[TC11000] ...bypass Phase 2 POST if in "POST failed" state
//---------------------------------------------------------------------
//@ Implementation-Description
//    See Interface-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

const PostTestItem *
BdPost::GetTestItems(void)
{
UNIT_TEST_POINT(POSTUT_38)

   if (   (Post::GetShutdownState_() == POWERFAIL) 
       && (Post::GetDowntimeStatus() == DOWN_SHORT) ) // $[11029]
   {
       // $[TI1.1]
       Post::SetPostType_( Post::POST_SHORT );
       return BdPost::ShortPostTests_;
   }
   else
   {
	 // $[TI1.2]
     Post::SetPostType_( Post::POST_LONG );

	 if ( Post::IsPostFailed() )  // $[TC11000]
	 {
       // $[TI2.1]
       return BdPost::PrevPostFailureTests_;
	 }
	 else
	 {
       // $[TI2.2]
       return BdPost::LongPostTests_;
	 }
   }
}


#endif   // ifdef SIGMA_BD_CPU
