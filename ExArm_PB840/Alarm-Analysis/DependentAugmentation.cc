#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: DependentAugmentation - Info for dependent augmentation matrix row.
//---------------------------------------------------------------------
//@ Interface-Description
// DependentAugmentation is derived from pure virtual base class
// AlarmAugmentation.  It defines the pure virtual functions operator== and 
// operator!= and contains a reference to the dependent Alarm object which is
// specified in the Dependent Augmentation column of the Technical and 
// Non-Technical alarm matrices.  It also provides a method for accessing the 
// dependent Alarm.  The virtual base class AlarmAugmentation provides a method 
// for accessing the superior alarm.
//---------------------------------------------------------------------
//@ Rationale
// DependentAugmentation collects the annunciated alarm information for a
// dependent augmentation matrix row and defines a relationship between a 
// superior Alarm object and an Alarm object (defining the dependent 
// augmentation criteria specified in an alarm matrix row) which is dependent
// to it.
//---------------------------------------------------------------------
//@ Implementation-Description
// In addition to the functionality provided by pure virtual base class
// AlarmAugmentation, DependentAugmentation provides a method to return the
// reference to the dependent Alarm which must be active for the dependent
// augmentation information to be included in the alarm annunciation.
// It also defines operator == and operator!= to determine if two 
// DependentAugmentation objects are equal or not.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/DependentAugmentation.ccv   25.0.4.0   19 Nov 2013 13:51:22   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "DependentAugmentation.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DependentAugmentation
//
//@ Interface-Description
//    Constructor.
// >Von
//    rInfo            Reference to AlarmAnalysisInformation object which 
//                     contains analysis and remedy message enumerators, 
//                     urgency/priority information and pointers to the System 
//                     Response and Reset System Response associated with the 
//                     augmentation in the Alarm Matrices.
//    rAlarm           Reference to the superior Alarm in the superior Alarm / 
//                     Dependent Augmentation / dependent Alarm relationship.
//    rDependentAlarm  Reference to the dependent Alarm in the superior Alarm /
//                     DependentAugmentation / dependent Alarm relationship.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

DependentAugmentation::DependentAugmentation(
                      const AlarmAnalysisInformation& rInfo,
                      Alarm& rAlarm,
                      Alarm& rDependentAlarm):
                      AlarmAugmentation(DEPENDENT_AUGMENTATION, rInfo, rAlarm),
                      rDependentAlarm_  (rDependentAlarm)
{
  CALL_TRACE("DependentAugmentation(rInfo, rAlarm, rDependentAlarm)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DependentAugmentation
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

DependentAugmentation::~DependentAugmentation(void)
{
  CALL_TRACE("~DependentAugmentation(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator==
//
//@ Interface-Description
//   Equality operator.
//
// >Von
//   rAug  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   Two DependentAugmentation objects are equal if the addresses of their 
//   dependent alarms are equal and their base classes are equal.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
Boolean
DependentAugmentation::operator==(const AlarmAugmentation& rAug) const
{
  CALL_TRACE("operator==(rAug)");

  const DependentAugmentation& rDependentAug = 
      (const DependentAugmentation&)rAug;

  return((&rDependentAlarm_ == &rDependentAug.rDependentAlarm_) && 
          this->AlarmAugmentation::operator==(rAug)); // $[TI1] $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator!=
//
//@ Interface-Description
//   Inequality operator.
//
// >Von
//   rAug  Reference to the object to be used in the comparison.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//   Two DependentAugmentation objects are not equal if the addresses of their 
//   dependent alarms are not equal or their base classes are not equal.
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
Boolean
DependentAugmentation::operator!=(const AlarmAugmentation& rAug) const
{
  CALL_TRACE("operator!=(rAug)");

  return (!(*this == rAug)); // $[TI1] $[TI2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
DependentAugmentation::SoftFault(const SoftFaultID  softFaultID,
                                 const Uint32       lineNumber,
                                 const char*        pFileName,
                                 const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::DEPENDENTAUGMENTATION, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
