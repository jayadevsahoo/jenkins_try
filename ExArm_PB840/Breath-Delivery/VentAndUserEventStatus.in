
#ifndef VentAndUserEventStatus_IN
#define VentAndUserEventStatus_IN

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: VentAndUserEventStatus - handles sending of ventilator status to GUI
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/VentAndUserEventStatus.inv   25.0.4.0   19 Nov 2013 14:00:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002 By: sp   Date:   06-Mar-1996    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  kam    Date:  30-Oct-1995    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial release.
//
//=====================================================================

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SetEventStatusCommState
//
//@ Interface-Description
//      This method takes a BdGuiCommSync::CommState as input and
//      returns nothing.
//      This method is called when the BdStatusTask is notified that
//      the status of communications between the BD and the GUI has
//      changed.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method sets EventStatusCommState_ to the passed value.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

inline void
VentAndUserEventStatus::SetEventStatusCommState (const BdGuiCommSync::CommState state)
{
    // $[TI1]
    CALL_TRACE ("VentAndUserEventStatus::SetEventStatusCommState (const BdGuiCommSync::CommState state)");
    EventStatusCommState_ = state;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#endif // VentAndUserEventStatus_IN 
