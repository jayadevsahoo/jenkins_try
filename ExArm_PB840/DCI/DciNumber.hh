#ifndef	DciNumber_HH
#define	DciNumber_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ====================================================================
//@ Class: DciNumber - Store a Real32 value with an associated validity tag
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/DCI/vcssrc/DciNumber.hhv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 17-DEC-1997  DR Number: 2605
//  Project: Sigma (R8027)
//  Description:
//      Restructuring of DCI to fix several problems. 
//
//  Revision  1.0	By: jdm	Date: 8/15/95	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

//@ Usage-Classes
#include "DciClassIds.hh"
#include "DciDebug.hh"
#include "DciValueStatus.hh"
//@ End-Usage


class DciNumber : public DciValueStatus
{
    public:
				DciNumber(void);
				~DciNumber(void);
	static	void	SoftFault(const SoftFaultID	softFaultID,
					const	Uint32		lineNumber,
					const	char *		pFileName = NULL,
					const	char *		pPredicate = NULL);

		DciValueStatus::DciValueStatusId get(Real32 * pNumber = NULL);

		inline	Real32			put(Real32 number);
		inline	Real32			put(Int32 number);

    protected:

    private:
				// this is purposesly not implemented...
				// TODO E600 commented out to compile
				//operator=(const DciNumber &);	// n. i.

				// the value being stored
			Real32	number_;
};


// inlined methods...
#include "DciNumber.in"

#endif	// DciNumber_HH

