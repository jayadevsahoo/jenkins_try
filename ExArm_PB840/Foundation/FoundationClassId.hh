
#ifndef FoundationClassId_HH
#define FoundationClassId_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  FoundationClassId - Ids for classes/files of this subsystem..
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FoundationClassId.hhv   25.0.4.0   19 Nov 2013 14:03:48   pvcs  $
//
//@ Modification-Log
//  
//   Revision: 002   By: sah   Date: 15-Jan-1998   DR Number: 5004
//   Project:  Sigma (R8027)
//   Description:
//	Added ID for new 'HashTableC' class.  Also, added hard-coded
//      values for each of the class IDs.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

//@ Usage-Classes
//@ End-Usage


//@ Type:  FoundationClassId
// Identifications of the modules within the Foundation Subsystem.
enum FoundationClassId
{
  ABSTRACT_ARRAY	= 0,
  ABSTRACT_BIT_ARRAY	= 1,
  ABSTRACT_DLISTC	= 2,
  ABSTRACT_DLISTR	= 3,
  ABSTRACT_HEAP		= 4,
  ABSTRACT_SLISTC	= 5,
  ABSTRACT_SLISTR	= 6,
  ABSTRACT_SORTC	= 7,
  ABSTRACT_SORTR	= 8,
  D_ITERATOR_C		= 9,
  D_ITERATOR_R		= 10,
  D_LINKED_NODE_C	= 11,
  FIXED_ARRAY		= 12,
  FIXED_BIT_ARRAY	= 13,
  FIXED_HEAP		= 14,
  FIXED_DLISTC		= 15,
  FIXED_DLISTR		= 16,
  FIXED_MUTABLE_SORTC	= 17,
  FIXED_MUTABLE_SORTR	= 18,
  FIXED_SLISTC		= 19,
  FIXED_SLISTR		= 20,
  FIXED_SORTC		= 21,
  FIXED_SORTR		= 22,
  S_ITERATOR_C		= 23,
  S_ITERATOR_R		= 24,
  S_LINKED_NODE_C	= 25,
  SORT_ITER_C		= 26,
  SORT_ITER_R		= 27,
  FOUNDATION_CLASS	= 28,
  HASH_TABLE_C		= 29,

  NUM_FOUNDATION_CLASSES
};


#endif // FoundationClassId_HH 
