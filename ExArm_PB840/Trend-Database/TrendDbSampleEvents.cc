#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDbSampleEvents
//---------------------------------------------------------------------
//@ Interface-Description
//  This type is used to collect and compress the sampled user or auto
//  events. This class is derived from the TrendDbSampleData class type.
// 
//---------------------------------------------------------------------
//@ Rationale
//  Encapsluates the logic to compress the trended event bit-vectrs for
//  all the trended timescales.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  The compressData() method handles the averaging scheme for each of 
//  the trended timescales. When the samples are accumulated to the 
//  bit-vectors representing each of the timescales, addToAccum is called
//  which ORs the event bits that were set in the current time frame.
//  When sufficient frames have been accumulated for the various timescales,
//  the accumulated events are returned to the client and the accumulators 
//  (bit-vectors) are cleared.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbSampleEvents.ccv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
//  Revision: 001  By:  ksg    Date:  16-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//
//=====================================================================

#include <string.h>

#include "Trend_Database.hh"
#include "TrendDbSampleEvents.hh"


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleEvents [constructor]
//
//@ Interface-Description
//  Constructs the class with the user or auto event unique Id
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the base class a NULL patient_data_item id (an event is not
//  a patient data item) and the provided trendId (refers to either the
//  user or auto trend event ids).
// 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleEvents::TrendDbSampleEvents( TrendDataMgr::TrendId trendId )
	: TrendDbSampleData(PatientDataId::NULL_PATIENT_DATA_ITEM, 
						(TrendSelectValue::TrendSelectValueId) trendId )
{
	clear_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDbSampleEvents [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSampleEvents::~TrendDbSampleEvents() 
{
	// do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear_
//
//@ Interface-Description
//  Clear the user and auto bit-vectors for all timescales
//---------------------------------------------------------------------
//@ Implementation-Description
//  None
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleEvents::clear_( void )
{
	memset( acummArray_, 0x00, sizeof( acummArray_ ) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: returnAndClear_
//
//@ Interface-Description
//  Return the accumulated events for this timescale and clear the 
//  timescale's bit-vector
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  For the 1_hr timescale, just return the sampled 1_hr value. For the
//  other timescales, return the accumulated bits and clear the
//  accumulator for the given timescale.
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the provided timescale is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendValue TrendDbSampleEvents::returnAndClear_( TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION((timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES), timescale);
	TrendValue  rtn;

	if (timescale == TrendTimeScaleValue::TREND_TIME_SCALE_1HR)
	{
		// 1_hr is not accumulated; just return the sampled value
		rtn = valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_1HR];
	}
	else
	{
		// return and clear the accumulated value
		rtn.data.discreteValue = acummArray_[ timescale - 1 ];
		acummArray_[ timescale - 1 ] = 0;
	}
	return( rtn );    
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: addToAcumm_
//
//@ Interface-Description
//  Accumulates the events in the provided timescale's bit-vector.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses bit-wise OR to accummulate the events.
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the provided timescale is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleEvents::addToAcumm_( const TrendValue & events, 
									   TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION( (timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES), timescale );

	// The 1_hr timescale is not accumulated, just return
	if (timescale > TrendTimeScaleValue::TREND_TIME_SCALE_1HR)
	{
		// OR new events to accumulated events
		acummArray_[ timescale - 1 ] |= events.data.discreteValue; 
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: compressData
//
//@ Interface-Description
//  Accumulates and combines samples from the 1_hr timescale to generate
//  samples at the higher timescales.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  1_hr sample is not compressed
//  2_hr sample is the OR of 2 1_hr samples
//  4_hr sample is the OR of 2 2_hr samples
//  8_hr sample is the OR of 2 4_hr samples
//  12_hr sample is the OR of 3 4_hr samples
//  24_hr sample is the OR of 2 12_hr samples
//  48_hr sample is the OR of 2 24_hr samples
//  72_hr sample is the OR of 3 24_hr samples
// 
//  Satisfies requirements:
//  [[TR01193] [PRD 1531] Multiple occurrences of the same Event
//  (manual or automatic)...
//  [[TR01114] [PRD 1512] For the 2, 4, 8, 12, 24, 48, and 72 hour
//  timescales, the system shall determine...
// 
//---------------------------------------------------------------------
//@ PreCondition
//	Ensures the provided timescale is valid
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDbSampleEvents::compressData( TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	AUX_CLASS_PRE_CONDITION( (timescale < TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES),
							 timescale );

	switch (timescale)
	{
		// 2_hr sample is the OR of 2 1_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_1HR :
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_1HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_2HR );
			break;

			// return 2_hr sample
			// 4_hr sample is the OR of 2 2_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_2HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_2HR] = 
				returnAndClear_(TrendTimeScaleValue::TREND_TIME_SCALE_2HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_2HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_4HR );
			break;

			// return 4_hr sample
			// 8_hr sample is the OR of 2 4_hr samples
			// 12_hr sample is the OR of 3 4_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_4HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_4HR] =
				returnAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_4HR ); 
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_4HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_8HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_4HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_12HR );
			break;

			// return 8_hr sample
		case TrendTimeScaleValue::TREND_TIME_SCALE_8HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_8HR] =
				returnAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_8HR );
			break;

			// return 12_hr sample
			// 24_hr sample is the OR of 2 12_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_12HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_12HR] =
				returnAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_12HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_12HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_24HR );
			break;

			// return 24_hr sample
			// 48_hr sample is the OR of 2 24_hr samples
			// 72_hr sample is the OR of 3 24_hr samples
		case TrendTimeScaleValue::TREND_TIME_SCALE_24HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_24HR] =
				returnAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_24HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_24HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_48HR );
			addToAcumm_( valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_24HR],
						 TrendTimeScaleValue::TREND_TIME_SCALE_72HR );
			break;

			// return 48_hr sample
		case TrendTimeScaleValue::TREND_TIME_SCALE_48HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_48HR] =
				returnAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_48HR );
			break;

			// return 72_hr sample
		case TrendTimeScaleValue::TREND_TIME_SCALE_72HR : 
			valueArray_[TrendTimeScaleValue::TREND_TIME_SCALE_72HR] =
				returnAndClear_( TrendTimeScaleValue::TREND_TIME_SCALE_72HR );
			break;

		default:
			TREND_MGR_DBG2( 1, "Sample Event unknown timescale ", timescale ); 
			break;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendDbSampleEvents::SoftFault( const SoftFaultID  softFaultID,
									 const Uint32       lineNumber,
									 const char*        pFileName,
									 const char*        pPredicate)  
{
	CALL_TRACE("TrendDbSampleAvg::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_DB_SAMPLE_EVENTS,
							lineNumber, pFileName, pPredicate);
}
