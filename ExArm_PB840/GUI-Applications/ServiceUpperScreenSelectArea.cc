#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ServiceUpperScreenSelectArea - Area which contains screen select
// buttons for SST Results, Diagnostic Log Results, Alarm Log,
// Vent Configuration, Operation Time screen, Vent Test Summary.  Only
// in Development mode, Development options is available.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the GUI-Foundation Container
// class.  This class contains TabButton objects of various types, as
// well as a Box object used for the "tab" graphics.
//
// There are four main interface methods: 1) the initialize() method
// performs one-time post-construction initialization, 2) the
// updateButtonDisplay() method turns on or off the select buttons
// in the Service Upper Screen, 3) the setBlank() method blanks/unblanks
// all drawables in the ServiceUpperScreenSelect area and 4) the 
// getDiagResultTabButton() returns the address of the DiagResultTabButton.
//---------------------------------------------------------------------
//@ Rationale
// A convenient organizer for the screen select buttons for the upper
// LCD screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Normally, a screen select area contains TabButton button objects.
// When selected, a TabButton normally draws a thick white border around
// itself which appears to merge with the border of the associated
// SubScreenArea.  This gives the illusion that the button is "attached
// to" the SubScreenArea.  To do this, the screen select area must be
// positioned such that it overlaps the subscreen area.  However, the
// screen select area has a dark gray background and the subscreen area
// has a white background.  To compensate for this, we add a fake white
// border along the top of the screen select area ('topLineBox_').
//
// Note that this area does not implement the mutual-exclusivity
// property of screen select buttons.  Instead, this is enforced by the
// SubScreenArea.  The initialize() method properly associates each
// TabButton with its SubScreenArea.
// $[BL00400]
//---------------------------------------------------------------------
//@ Fault-Handling
// Usual assertions to verify code correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceUpperScreenSelectArea.ccv   25.0.4.0   19 Nov 2013 14:08:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 021  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 020   By: gdc  Date:  26-May-2007    SCR Number:  6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 019   By: gdc  Date:  31-Aug-2000    DR Number:  5751
//  Project:  GuiComms
//  Description:
//      setBlank() caused development option tab button to show
//		even if a development datakey was not present after a download.
//		The tab button is now added to the container only if the 
//		development options datakey is present. The visibility attribute
//		can then be changed for all the tab buttons without affecting
//		the hidden development option tab button.
//
//  Revision: 018   By: sah  Date:  07-Mar-2000    DR Number:  5677
//  Project:  NeoMode
//  Description:
//      Eliminated functionality tied to 'SIGMA_PSUEDO_OFFICIAL' build
//      flag.
//
//  Revision: 007  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 006  By:  syw   Date:  16-Nov-1998    DR Number: 5252
//       Project:  BiLevel
//       Description:
//			Added requirement tracing.
//
//  Revision: 005  By:  syw	   Date:  15-Oct-1998    DCS Number: 5213
//  Project:  BiLevel
//  Description:
//		Eliminate referece to IsDemoDataKey... Use IsStandardDevelopmentOptions instead.
//
//  Revision: 004  By:  syw	   Date:  20-Aug-1998    DCS Number: 
//  Project:  BiLevel
//  Description:
//		BiLevel Initial version.  Development option button is now controlled
//		by the data key type.  Eliminate referece to IsDemoDataKey... Use
//		IsStandardDevelopmentOptions instead.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  yyy    Date: 16-Oct-1997    DR Number: DCS 2561
//  Project:  Sigma (840)
//  Description:
//		Disabled the development screen option when in final official release
//		unless a demo key found.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "ServiceUpperScreenSelectArea.hh"

//@ Usage-Classes
#include "ServiceUpperScreen.hh"
#include "SIterR_Drawable.hh"
#include "MiscStrs.hh"
#include "SoftwareOptions.hh"
#include "SstResultsSubScreen.hh"
#include "DiagLogMenuSubScreen.hh"
#include "AlarmLogSubScreen.hh"
#include "VentConfigSubScreen.hh"
#include "OperationalTimeSubScreen.hh"
#include "VentTestSummarySubScreen.hh"
#include "DevelopmentOptionsSubScreen.hh"
 
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 431;
static const Int32 CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 49;
static const Int32 BUTTON_WIDTH_ = 91;
static const Int32 BUTTON_HEIGHT_ = 49;
static const Int32 BUTTON_BORDER_ = 3;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceUpperScreenSelectArea()  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members and initializes the colors, sizes, and
// positions of all graphical objects.  Adds all graphical objects to
// the parent container for display.
//
// $[01066] The Upper Screen Select Area displays the following ...
// $[07010] The Upper screen select area shall continuously display ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceUpperScreenSelectArea::ServiceUpperScreenSelectArea(void) :
	sstResultTabButton_(0, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_,	Button::SQUARE,
						MiscStrs::SST_RESULT_TAB_BUTTON_LABEL, NULL),
	diagResultTabButton_(BUTTON_WIDTH_, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE,
						MiscStrs::DIAG_LOG_TAB_BUTTON_LABEL, NULL),
	serviceAlarmLogTabButton_(BUTTON_WIDTH_ * 2, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE,
						NULL, 			NULL),
	ventConfigTabButton_(BUTTON_WIDTH_ * 3, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE,
						MiscStrs::VENT_CONFIG_TAB_BUTTON_LABEL, NULL),
	operationalTimeTabButton_(BUTTON_WIDTH_ * 4, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE,
						MiscStrs::OP_TIME_LOG_TAB_BUTTON_LABEL, NULL),
	ventTestSummaryTabButton_(BUTTON_WIDTH_ * 5, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE,
						MiscStrs::TEST_SUMMARY_TAB_BUTTON_LABEL, NULL),
	developmentOptionTabButton_(BUTTON_WIDTH_ * 6, 0, BUTTON_WIDTH_,
						BUTTON_HEIGHT_, Button::LIGHT_NON_LIT,
						BUTTON_BORDER_, Button::SQUARE,
						L"{p=10,x=0,y=15:DEVELOP{x=0,Y=15:OPTIONS}}", NULL),  
	topLineBox_(0, 0, CONTAINER_WIDTH_, BUTTON_BORDER_)
{
	CALL_TRACE("ServiceUpperScreenSelectArea::ServiceUpperScreenSelectArea(void)");

	// Size and position the area
	setX(CONTAINER_X_);
	setY(CONTAINER_Y_);
	setWidth(CONTAINER_WIDTH_);
	setHeight(CONTAINER_HEIGHT_);

	// Default background color is extra dark grey
	setFillColor(Colors::EXTRA_DARK_BLUE);

	// Add white line at top
	topLineBox_.setColor(Colors::FRAME_COLOR);
	topLineBox_.setFillColor(Colors::FRAME_COLOR);
	addDrawable(&topLineBox_);

	//
	// Add screen select buttons to container.
	//
	addDrawable(&sstResultTabButton_);
	addDrawable(&diagResultTabButton_);
	addDrawable(&serviceAlarmLogTabButton_);
	addDrawable(&ventConfigTabButton_);
	addDrawable(&operationalTimeTabButton_);
	addDrawable(&ventTestSummaryTabButton_);
	if (SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions())
	{
	// $[TI1]
	  addDrawable(&developmentOptionTabButton_);
	}
	// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ServiceUpperScreenSelectArea  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ServiceUpperScreenSelectArea::~ServiceUpperScreenSelectArea(void)
{
	CALL_TRACE("ServiceUpperScreenSelectArea::ServiceUpperScreenSelectArea(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
// One-time post-construction initialization which associates each
// screen select button with its subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Called once at application initialization time to register the
// subscreens owned by the Service upper subscreen area with their screen 
// select buttons.
//
// $[01067] The buttons shall form a mutually exclusive group ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceUpperScreenSelectArea::initialize(void)
{
// $[TI1]
	CALL_TRACE("ServiceUpperScreenSelectArea::initialize(void)");

	sstResultTabButton_.setSubScreen(
			UpperSubScreenArea::GetSstResultsSubScreen());
	diagResultTabButton_.setSubScreen(
			UpperSubScreenArea::GetDiagLogMenuSubScreen());
	serviceAlarmLogTabButton_.setSubScreen(
			UpperSubScreenArea::GetAlarmLogSubScreen());
	ventConfigTabButton_.setSubScreen(
			UpperSubScreenArea::GetVentConfigSubScreen());
	operationalTimeTabButton_.setSubScreen(
			UpperSubScreenArea::GetOperationalTimeSubScreen());
	ventTestSummaryTabButton_.setSubScreen(
			UpperSubScreenArea::GetVentTestSummarySubScreen());
	developmentOptionTabButton_.setSubScreen(
			UpperSubScreenArea::GetDevelopmentOptionsSubScreen());
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateButtonDisplay
//
//@ Interface-Description
// Passed a boolean, 'status', which tells SV Upper Screen Select Area to either
// show or hide the tabButton objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method is called to update the appearance of the upper screen select 
// buttons.  Calls the setShow() methods of all tab buttons in the
// ServiceUpperScreenSelectArea.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceUpperScreenSelectArea::updateButtonDisplay(Boolean status)
{
//$[TI1]
	CALL_TRACE("ServiceUpperScreenSelectArea::updateButtonDisplay(Boolean status)");

	//
	// Pass the message along to the contained TabButton objects.
	//
	sstResultTabButton_.setShow(status);
	diagResultTabButton_.setShow(status);
	serviceAlarmLogTabButton_.setShow(status);
	ventConfigTabButton_.setShow(status);
	operationalTimeTabButton_.setShow(status);
	ventTestSummaryTabButton_.setShow(status);
	developmentOptionTabButton_.setShow(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setBlank
//
//@ Interface-Description
// Passed a boolean, 'blank', which tells SV Upper Screen Select Area to either
// blank its area (make all drawable contents invisible) or not.
//---------------------------------------------------------------------
//@ Implementation-Description
// Go through the members of this area's container and set each
// drawable's show flag to be the inverse of 'blank'.  However,
// topLineBox_ must always be shown.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
ServiceUpperScreenSelectArea::setBlank(Boolean blank)
{
	CALL_TRACE("ServiceUpperScreenSelectArea::setBlank(Boolean blank)");

	// Set the show flag on all drawable contents appropriately
	SIterR_Drawable children(*getChildren_());
	Drawable *pDraw;
  	for (SigmaStatus ok = children.goFirst();
			(SUCCESS == ok) && (pDraw= (Drawable *)&(children.currentItem()));
			ok = children.goNext())
	{
		pDraw->setShow(!blank);
	}

	// The Top Line box must be shown at all times
	topLineBox_.setShow(TRUE); 							// $[TI1]
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
ServiceUpperScreenSelectArea::SoftFault(const SoftFaultID  softFaultID,
								 const Uint32       lineNumber,
								 const char*        pFileName,
								 const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							SERVICEUPPERSCREENSELECTAREA,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
