#ifndef PavManager_HH
#define PavManager_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PavManager - Central location to manage pav related data required 
//					for PAV.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/PavManager.hhv   10.7   08/17/07 09:40:32   pvcs  
//
//@ Modification-Log
//
//  Revision: 006   By:   rpr    Date: 08-April-2009      DR Number: 6490
//  Project:  S840BUILD2
//  Description:
//        Pav Optimization.  Added local copy of ibw
//
//  Revision: 005   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 004   By: gdc   Date:  28-Nov-2006    DR Number: 6316
//  Project:  RESPM
//  Description:
//  Removed isPavInspiration_ boolean and references to it since it
//  did not reflect the current state of the PAV breath once newCycle()
//  was changed to run only during PA/SPONT (see SCR 6116 fix below).
//
//  Revision: 003   By: gfu   Date:  17-July-2003    DR Number: 6080
//  Project:  PAV
//  Description:
//      Modified per SDCR #6080 -- added data items for computing pressure 
//      differnece between wye pressure at start of insp and when instrinsic
//      peep is computed.
//
//  Revision: 002   By: syw   Date:  27-Nov-2000    DR Number: 5792
//  Project:  PAV
//  Description:
//		"Averaged" rRaw calculations.
//
//  Revision: 001   By: syw   Date:  19-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "PavState.hh"

//@ Usage-Classes

#include "Resistance.hh"
#include "Trigger.hh"
#include "PavFilters.hh"
#include "BreathPhase.hh"
#include "TemperatureSensor.hh"
#include "MainSensorRefs.hh"

//@ End-Usage

// $[PA24036]
const Uint32 R_FILTERED_BUFFER_SIZE = 20 ;
const Uint32 VTI_TI_BUFFER_SIZE = 20 ;

// $[PA24043]
const Uint8  TOTAL_BREATHS = 8 ;

const Uint32 INTRINSIC_PEEP_BUFFER_SIZE = 20;  

struct ResistanceData {
	Real32 maxLung ;
	Real32 minLung ;
    Real32 startupLung ;
	Real32 rRaw ;
    Real32 rTotal;
	PavFilters filter ;
	Real32 rPatient ;
	Uint32 nR ;
} ;

struct ComplianceData {
	Real32 maxLung ;
	Real32 minLung ;
    Real32 startupLung ;
	Real32 cRaw ;
	PavFilters filter ;
	Real32 cPatient ;
	Uint32 nE ;
} ;

struct Statistics
{
	Real32 buffer[VTI_TI_BUFFER_SIZE] ;
	Real32 sum ;
	Real32 sumSquared ;
	Real32 average ;
	Real32 stdDev ;
	Uint32 index ;
	Boolean bufferFull ;
} ;

struct IntrinsicPeepData {
	Real32 lungPressure [INTRINSIC_PEEP_BUFFER_SIZE];
	Real32 wyePressure [INTRINSIC_PEEP_BUFFER_SIZE];
};
 

class PavManager
{
  public:
    PavManager( void) ;
    ~PavManager( void) ;

	Boolean isPerformPavPause( const BreathPhase* pBreathPhase,
							   const Trigger::TriggerId id) ;
	inline void setPauseComplete( const Trigger::TriggerId id) ;
	inline void setExhPostPavPause( const Trigger::TriggerId id) ;
	inline PavState::Id getPavState( void) const ;
	inline Real32 getTotalWob( void ) ;
	inline Real32 getPatientWob( void) ;
	inline Real32 getPatientElastanceWob( void) ;
	inline Real32 getPatientResistiveWob( void) ;
	inline Real32 getCraw( void) ;
	inline Real32 getRraw( void) ;
	inline Real32 getRtotal( void) ;
	inline Real32 getIntrinsicPeep( void) ;

	inline Boolean  isPlateauDataReady(void) const;
	inline void     resetPlateauDataFlag(void);

	inline Boolean  isFirstPavBreath(void) const;
	inline Real32 getTriggerPressureDrop( void) ;

		
    static void SoftFault(  const SoftFaultID softFaultID,
							const Uint32      lineNumber,
							const char*       pFileName  = NULL, 
			 				const char*       pPredicate = NULL) ;

	void newCycle( void) ;
	void newInspiration( void) ;
	Real32 getEPatient( const PavState::Id state) ;
	Real32 getRPatient( const PavState::Id state) ;
	void recoveryFromDisconnect( void) ;
	void reset( void) ;
	void resetAlarms( void) ;
	void paBreathTruncated( const Boolean status) ;
	void resyncAlarms( void) ;
	
  protected:

  private:
    PavManager( const PavManager&) ;  		// not implemented...
    void   operator=( const PavManager&) ; 	// not implemented...

	Real32 computeCRaw_( void) ;
	void computeRRaw_( void) ;
	void completePreviousBreathCalculation_( void) ;
	void updateStatistics_( const Uint32 inspTime,
							const Real32 currentInspVolume) ;
	Boolean pauseScheduler_( void) ;
	Boolean validateCRaw_( void) ;
	Boolean validateRRaw_( const Boolean isCRawAccepted) ;
					
	//@ Data-Member: exhResistance_
	// exh tubing resistance
	Resistance exhResistance_ ;

	//@ Data-Member: inspResistance_
	// insp tubing resistance
	Resistance inspResistance_ ;

	//@ Data-Member: pavState_
	// state of PAV assessment
	PavState::Id pavState_ ;

	//@ Data-Member: wyePressure_
	// wye pressure estimate based on insp side pressure sensor
	Real32 wyePressure_ ;

	//@ Data-Member: wyePressure_1_
	// previous wye pressure estimate based on insp side pressure sensor
	Real32 wyePressure_1_ ;

	//@ Data-Member: wyePressureSlope_
	// wye pressure slope based on output of slope estimator
	Real32 wyePressureSlope_ ;

	//@ Data-Member: wyePressureSlope_1_
	// previous wye pressure slope based on output of slope estimator
	Real32 wyePressureSlope_1_ ;

	//@ Data-Member: wyePressureSlope_2_
	// previous 2 wye pressure slope based on output of slope estimator
	Real32 wyePressureSlope_2_ ;

	//@ Data-Member: intrinsicPeep_ ;
	// intrinsic peep
	Real32 intrinsicPeep_ ;

	//@ Data-Member: endExhLungPressure_ ;
	// end exhalation pressure
	Real32 endExhLungPressure_ ;

	//@ Data-Member: prevEndExhLungPressure_
	// previous breaths end exhalation pressure
	Real32 prevEndExhLungPressure_ ;

	//@ Data-Member: endPlateauPressure_
	// end plateau pressure
	Real32 endPlateauPressure_ ;

	//@ Data-Member: pauseComplete_
	// set when pause is complete
	Boolean pauseComplete_ ;

	//@ Data-Member: isPlateauDataReady_
	// set when plateau data is ready for transimission; cleared when
	// sent
	Boolean isPlateauDataReady_ ;

	//@ Data-Member: endPlateauPressureSlope_
	// pressure slope when pause is completed
	Real32 endPlateauPressureSlope_ ;

	//@ Data-Member: inspLungVolume_
	// inspired lung volume (btps)
	Real32 inspLungVolume_ ;

	//@ Data-Member: autozeroOccurred_ ;
	// set if an autozero occurred during this breath
	Boolean autozeroOccurred_ ;	

	//@ Data-Member: resistanceData_
	// storage for resistance data
	ResistanceData resistanceData_ ;

	//@ Data-Member: complianceData_
	// storage for compliance data
	ComplianceData complianceData_ ;

	//@ Data-Member: tiStats_
	// storage for ti statistics
	Statistics tiStats_ ;

	//@ Data-Member: vTiStats_
	// storage for Vti statistics
	Statistics vTiStats_ ;

	//@ Data-Member: breathsSinceLastPause_ ;
	// number of breaths since last pav pause event
	Uint32 breathsSinceLastPause_ ;	

	//@ Data-Member: nextPauseTarget_
	// breath target for next pause
	Uint32 nextPauseTarget_ ;

	//@ Data-Member: performPavPause_
	// flag to indicate whether to perform a pav pause
	Boolean performPavPause_ ;

	//@ Data-Member: timeElapsedSinceFirstReconnect_
    // [[PA24034]When recovering from a disconnection following 
    // a startup routine, ....  To prevent sequential 
    // reduction of the resistance value, reduction shall be allowed 
    // only once during a time window of 10 minutes starting after 
    // the first reconnection has been acknowledged.  ...]
    //-----------------------------------------------------------
	// Time elapsed since 1st reconnection event from a disconnect.
    // This data member is first used as a boolean flag by initialied
    // to TEN_MINIUTES	so it can go into the newCycle() loop
    // should a reconnection occured.  From then on it is used
    // as a counter-timer flag to count up 10 minutes.
    Uint32 timeElapsedSinceFirstReconnect_ ;

	//@ Data-Member: isPavPause_
	// set true if pav pause lasted the full duration
	Boolean isPavPause_ ;

	//@ Data-Member: firstPaBreathOccurred_
	// set TRUE if first PA breath occurred
	Boolean firstPaBreathOccurred_ ;

	//@ Data-Member: patientElastanceWob_
	// patient elastance work of breathing
	Real32 patientElastanceWob_ ;

	//@ Data-Member: patientResistiveWob_
	// patient resistive work of breathing
	Real32 patientResistiveWob_ ;

	//@ Data-Member: patientWob_
	// patient work of breathing
	Real32 patientWob_ ;

	//@ Data-Member: totalWob_
	// total work of breathing
	Real32 totalWob_ ;

	//@ Data-Member: previousPercentSupport_
	// percent support on previous breath
	Real32 previousPercentSupport_ ;

	//@ Data-Member: resistiveTargetQLungSum_
	// sum of resistive target pressure times lung flow 
	Real32 resistiveTargetQLungSum_ ;

	//@ Data-Member: elasticTargetQLungSum_
	// sum of elastic target pressure times lung flow 
	Real32 elasticTargetQLungSum_ ;

	//@ Data-Member: numTruncatedBreaths_
	// number of PA breaths truncated prematurely
	Uint32 numTruncatedBreaths_ ;

	//@ Data-Member: numPaBreaths_
	// number of PA breaths
	Uint32 numPaBreaths_ ;

	//@ Data-Member: isBreathTruncated_[TOTAL_BREATHS]
	// array of last TOTAL_BREATHS to indicate if breath was truncated
	Boolean isBreathTruncated_[TOTAL_BREATHS] ;

	//@ Data-Member: truncationIndex_
	// index into isBreathTruncated_ array
	Uint32 breathTruncatedIndex_ ;

	//@ Data-Member: peakLungFlow_
	// peak lung flow during exhalation
	Real32 peakLungFlow_ ;

	//@ Data-Member: numRRawValues_
	// number of rRaw values obtained since peak occurred
	Uint32 numRRawValues_ ;

	//@ Data-Member: forcePavPauseFlag_
	// Flag used to indicate that a PAV pause is needed with the next
	// inspiration.
	Boolean  forcePavPauseFlag_;
        
     //@ Data-Member: intrinsicPeepBuffer_
     // structure which contains lung and wye pressure buffers
     // which are used to calculate intrinsic Peep
    IntrinsicPeepData intrinsicPeepBuffer_;
     
     //@ Data-Member: nextPeepPosition_ 
     // indicator for next entry into intrinsicPeepBuffer_  
     Uint32 nextPeepPosition_;
     
     //@ Data-Member: peepHundredMsTick_
     // counter for each 100 ms cycle
     Uint32 peepHundredMsTick_;

	//@ Data-Member: intrinsicPeepWyePressure_
	// wyePressure used in the intrinsic Peep calculation
	Real32 intrinsicPeepWyePressure_;

	//@ Data-Member: triggerPressureDrop_
	// difference between wyePressure used in the intrinsic Peep calculation
	// and the wyePressure at the start of inspiration.
	Real32 triggerPressureDrop_;

	//@ Data-Member: ibw_
	//Local copy of IBW
	Real32 ibw_;
} ;

#include "PavManager.in"

#endif // PavManager_HH 

