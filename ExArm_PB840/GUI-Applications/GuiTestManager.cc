#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: GuiTestManager - Generic service test class which handles the
// housekeeping procedure needed by the service test.
//---------------------------------------------------------------------
//@ Interface-Description
// A GuiTestManager is used as a central control object to interface between
// GUI subscreen and Service tests.
// 
// On construction the GuiTestManager class, the corresponding
// GuiTestManagerTarget object will be set to handle the test results.
//
// Each GuiTestManager object shall have its own local copy of TestResult
// objects which include both the commands and data types of test result.
// This class also handle the registration of the TestResult objects to the
// Service Data Subsystem.
//
// When the Service Data Manager detected a new test result, updateChangedData()
// a virtual method is inherited from being an TestResutlTarget to process
// the test result.
//---------------------------------------------------------------------
//@ Rationale
// This object is necessary to register, set, and dispatch the test result
// which are received from the Service GuiApp.
//---------------------------------------------------------------------
//@ Implementation-Description
// The basic building block of GuiTestManager is the TestResultTarget class.
// A TestResultTarget object is used to process the Service Data callback
// functionality for new test result commands or data.  
//
// GuiTestManager is passed a GuiTestManagerTarget pointer on construction.
// It is from this pointer that it dispatch the test result commands/data to
// the activated subscreen to perform special handling.
// 
// The capability of this object to instantiate the TestResultTarget objects
// for both command and data shall free the individual subscreen of allocating
// memory and instiatiating the objects.
//
// The capability of this object to transmit the command, data, and user
// response to/from the Service Data subsystem
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiTestManager.ccv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By: yyy      Date: 02-Oct-1997  DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//      Filter out (do not send) condition Id when status is Start, End,
//		Not installed, or operator exit.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "GuiTestManager.hh"

#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "SmStatusId.hh"
#include "SmDataId.hh"
#include "ServiceDataRegistrar.hh"
#include "TestResultId.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiTestManager   [Default Constructor]
//
//@ Interface-Description
// Constructs a GuiTestManager.  Passed the following parameters:
// >Von
//	pTarget					A GuiTestManagerTarget pointer through which the
//							test results (ie prompt, status, and data) will
//							be handled.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize all data members.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiTestManager::GuiTestManager(GuiTestManagerTarget *pTarget) :
		pTarget_(pTarget),
		pCommand_(NULL),
		pKeyAllowed_(NULL),
		pStatus_(NULL),
		pCondition_(NULL)
{
	CALL_TRACE("GuiTestManager::GuiTestManager(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiTestManager  [Destructor]
//
//@ Interface-Description
// GuiTestManager destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiTestManager::~GuiTestManager(void)
{
	CALL_TRACE("GuiTestManager::~GuiTestManager(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupTestResultDataArray
//
//@ Interface-Description
// This method must be called once for each GuiTestManagerTarget object
// to instantiate the memory area allocated for the test result data objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// Use replace new to instantiate test result data objects.  And setup
// the default display precision for each test result data.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::setupTestResultDataArray(void)
{
	CALL_TRACE("GuiTestManager::setupTestResultDataArray(void)");


#ifdef FORNOW
printf("GuiTestManager::setupTestResultDataArray, at line = %d\n", __LINE__);
#endif	// FORNOW

	// Associate the testResults object with the pointer to the test result
	// array.
	// All the access to the testResult object will be done through the pointer
	// to testResults.
	CLASS_ASSERTION(sizeof(testResultDataMemory_) >=
					TestDataId::MAX_DATA_ENTRIES * sizeof(TestResult));

	pTestResultDataArray_ = (TestResult *)testResultDataMemory_;

	for (Int idx = 0; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		new (&pTestResultDataArray_[idx]) TestResult(this);

		// Set default percision.
		pTestResultDataArray_[idx].setPrecision(0);
	}
																	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupTestCommandArray
//
//@ Interface-Description
// This method must be called once for each GuiTestManagerTarget object
// to instantiate the memory area allocated for the test result command
// objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// Use replace new to instantiate test result data objects.  And setup
// the default display precision for each test result data.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::setupTestCommandArray(void)
{
	CALL_TRACE("GuiTestManager::setupTestCommandArray(void)");

	// Associate the testResults object with the pointer to the test result
	// commands array.
	// All the access to the testResult object will be done through the pointer
	// to testResults.
	CLASS_ASSERTION(sizeof(testResultCommandsMemory_) >=
					TestDataId::MAX_PROMPT_ENTRIES * sizeof(TestResult));
	CLASS_ASSERTION(sizeof(testResultStatusMemory_) >=
					TestDataId::MAX_STATUS_ENTRIES * sizeof(TestResult));

	Int idx = 0;

	pTestResultCommandArray_ = (TestResult *)testResultCommandsMemory_;
	pCommand_ 	= pTestResultCommandArray_;
	pKeyAllowed_= pCommand_ + 1;

#ifdef FORNOW
printf("GuiTestManager::setupTestCommands, at line = %d, pCommand=%d, pKeyAllowed_=%d\n",
					__LINE__, pCommand_, pKeyAllowed_);
#endif	// FORNOW

	for (idx = 0; idx < TestDataId::MAX_PROMPT_ENTRIES; idx++)
	{
		// Run placement new for each TestResult object.
		new(&pTestResultCommandArray_[idx])
										TestResult(this);
	}

	pTestResultStatusArray_ = (TestResult *)testResultStatusMemory_;

	for (idx = 0; idx < TestDataId::MAX_STATUS_ENTRIES; idx++)
	{
		new(&pTestResultStatusArray_[idx])
			TestResult(this);
	
	}
																	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerTestResultData
//
//@ Interface-Description
// This method must be called once for each GuiTestManagerTarget object
// to register to the Service Data Registrar for callback process.
//---------------------------------------------------------------------
//@ Implementation-Description
// Registers the given target with the associated 'eventId' sequentially
// through the test result data array.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::registerTestResultData(void)
{
	CALL_TRACE("GuiTestManager::registerTestResultData(void)");

	Int serviceDataRegisterTargetIdx = 0;
	Int idx;

#ifdef FORNOW
printf("GuiTestManager::registerTestResultData, this=%d, at line = %d\n",
					 this, __LINE__);
#endif	// FORNOW

	for (idx = 0; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		serviceDataRegisterTargetIdx = idx + TestDataId::FIRST_DATA_ITEM;

		// Register each test result into the DATA area of the Service Data
		// Register.
		ServiceDataRegistrar::RegisterTarget(&pTestResultDataArray_[idx],
					(TestDataId::TestDataItemId) serviceDataRegisterTargetIdx);
	}
																	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerTestCommands
//
//@ Interface-Description
// This method must be called once for each GuiTestManagerTarget object
// to register to the Service Data Registrar for callback process.
//---------------------------------------------------------------------
//@ Implementation-Description
// Registers the given target with the associated 'eventId' sequentially
// through the test result command array.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::registerTestCommands(void)
{
	CALL_TRACE("GuiTestManager::registerTestCommands(void)");

#ifdef FORNOW
printf("GuiTestManager:::registerTestCommands, this=%d, at line = %d\n",
						this, __LINE__);
#endif	// FORNOW

	int serviceDataRegisterTargetIdx = 0;
	Int idx = 0;

	for (idx = 0; idx < TestDataId::MAX_PROMPT_ENTRIES; idx++)
	{
		// Register each test result into the DATA area of the Service Data
		// Register.
		ServiceDataRegistrar::RegisterTarget(&pTestResultCommandArray_[idx],
					(TestDataId::TestDataItemId) idx);
	}

	for (idx = 0; idx < TestDataId::MAX_STATUS_ENTRIES; idx++)
	{
		serviceDataRegisterTargetIdx = idx + TestDataId::FIRST_STATUS_DATA_ITEM;

		// Register each test result into the DATA area of the Service Data
		// Register.
		ServiceDataRegistrar::RegisterTarget(&pTestResultStatusArray_[idx],
					(TestDataId::TestDataItemId) serviceDataRegisterTargetIdx);
	}
																	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateChangedData
//
//@ Interface-Description
// This is a virtual method inherited from being an TestResutlTarget.  It is
// called when the Service Data Manager detected a new test result.
//---------------------------------------------------------------------
//@ Implementation-Description
// Depending on the type of test result, different methods will be called to
// process the various test results.
// If the test result is a command, then processTestPrompt_() method will be
// called to handle the new command.
// If the test result is a test status, then processTestResultStatus()
// method will be called to handle the new status.
// If the test result is a test condition, then processTestResultCondition()
// method will be called to handle the new condition code.
// If the test result is a test data, then processGuiTestData() method
// will be called to handle the new test data.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::updateChangedData(TestResult *pResult)
{
	CALL_TRACE("EstSetupSubScreen::updateChangedData(TestResult *pResult)");

#if defined FORNOW	
printf("GuiTestManager::updateChangedData at line  %d, this=%d, pResult=%d\n",
					 __LINE__, this, pResult);
#endif	// FORNOW

	if (pResult == pCommand_)
	{															// $[TI1]
		processGuiTestPrompt_(pResult->getCommandValue());
	}
	else if (pResult == pKeyAllowed_)
	{                                                           // $[TI2]
		processGuiTestKeyAllowed_(pResult->getCommandValue());
	}
	else if (pResult >= pTestResultStatusArray_ &&
			 pResult < pTestResultStatusArray_ +
								TestDataId::MAX_STATUS_ENTRIES)
	{                                                           // $[TI3]
		processGuiTestResultCondition_(pResult->getConditionValue());
		processGuiTestResultStatus_(pResult->getStatusValue());
	}
	else
	{                                                           // $[TI4]
		processGuiTestData_((Int) (pResult - pTestResultDataArray_), pResult);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processGuiTestPrompt_
//
//@ Interface-Description
// Handle the new prompt command.
//---------------------------------------------------------------------
//@ Implementation-Description
// Call the GuiTestManagerTarget's virtual method to process it.
//---------------------------------------------------------------------
//@ PreCondition
// None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::processGuiTestPrompt_(Int command)
{
	CALL_TRACE("GuiTestManager::processGuiTestPrompt_(Int command)");

#if defined FORNOW
printf("GuiTestManager::processGuiTestPrompt_ at line  %d, command = %d, pTarget_=%d\n",
				__LINE__, command, pTarget_);
#endif	// FORNOW
	pTarget_->processTestPrompt(command);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processGuiTestKeyAllowed_
//
//@ Interface-Description
// 	Handle the new key allowed command.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Call the GuiTestManagerTarget's virtual method to process it.
//---------------------------------------------------------------------
//@ PreCondition
// 	None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::processGuiTestKeyAllowed_(Int keyAllowed)
{
	CALL_TRACE("GuiTestManager::processGuiTestKeyAllowed_(Int keyAllowed)");

#if defined FORNOW
printf("GuiTestManager::processGuiTestKeyAllowed_ at line  %d, keyAllowed = %d, pTarget_=%d\n",
					__LINE__, keyAllowed, pTarget_);
#endif	// FORNOW
	pTarget_->processTestKeyAllowed(keyAllowed);
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processGuiTestResultStatus_
//
//@ Interface-Description
// Handle the new status command.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Translate the test result status into NovRam test result Id format,
// 	then call the GuiTestManagerTarget's virtual method to process it.
//---------------------------------------------------------------------
//@ PreCondition
// 	None.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::processGuiTestResultStatus_(Int resultStatus)
{
	CALL_TRACE("GuiTestManager::processGuiTestResultStatus_(Int resultStatus)");

	CLASS_ASSERTION((resultStatus >= (Int) SmStatusId::TEST_END) &&
						 (resultStatus < (Int) SmStatusId::NUM_TEST_DATA_STATUS));
#if defined FORNOW
printf("GuiTestManager::processGuiTestResultStatus_ at line  %d, resultStatus= %d, pTarget_=%d\n",
			__LINE__, resultStatus, pTarget_);
#endif	// FORNOW

	Int testResultId = UNDEFINED_TEST_RESULT_ID;

	switch (resultStatus)
	{
		case SmStatusId::TEST_END:			// End of test status received	
												// $[TI1]
			// Remember the current test result status Id.
			testResultId = PASSED_TEST_ID;
			break;

		case SmStatusId::TEST_ALERT:			// $[TI2]
			// Remember the current test result status Id.
			testResultId = MINOR_TEST_FAILURE_ID;
			break;

		case SmStatusId::TEST_FAILURE:			// $[TI3]
			// Remember the current test result status Id.
			testResultId = MAJOR_TEST_FAILURE_ID;
			break;

		case SmStatusId::NOT_INSTALLED:			// $[TI4]
			// Remember the current test result status Id.
			testResultId = NOT_INSTALLED_TEST_ID;
			break;
			
		case SmStatusId::TEST_START:			// $[TI5]
		case SmStatusId::TEST_OPERATOR_EXIT:	// $[TI6]
			testResultId = UNDEFINED_TEST_RESULT_ID;
			break;

		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}

#if defined FORNOW
printf("GuiTestManager::processGuiTestResultStatus_, resultStatus = %d, testResultId = %d\n", resultStatus, testResultId);
#endif	// FORNOW
		pTarget_->processTestResultStatus(resultStatus, testResultId);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition_
//
//@ Interface-Description
// Handle the new condition command.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Call the GuiTestManagerTarget's virtual method to process it.
//---------------------------------------------------------------------
//@ PreCondition
// 	The given `newScreenId' must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::processGuiTestResultCondition_(Int resultCondition)
{
	CALL_TRACE("GuiTestManager::processGuiTestResultCondition_(Int resultCondition)");

#if defined FORNOW
printf("GuiTestManager::processGuiTestResultCondition_ at line  %d, resultCondition=%d, pTarget_=%d\n",
					 __LINE__, resultCondition, pTarget_);
#endif	// FORNOW

	if (resultCondition == SmStatusId::NULL_CONDITION_ITEM ||
		 resultCondition >= SmStatusId::NUM_CONDITION_ITEM)
	{										// $[TI1]
		return;
	}										// $[TI2]

	pTarget_->processTestResultCondition(resultCondition);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processGuiTestData_
//
//@ Interface-Description
// Handle the new data command.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Call the GuiTestManagerTarget's virtual method to process it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::processGuiTestData_(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("GuiTestManager::processGuiTestData_(Int dataIndex, TestResult *pResult)");

#if defined FORNOW
printf("GuiTestManager::processGuiTestData_ at line  %d, dataIndex = %d\n",
					__LINE__, dataIndex);
#endif	// FORNOW

	CLASS_ASSERTION(dataIndex < TestDataId::MAX_DATA_ENTRIES &&
						dataIndex >= 0);

	pTarget_->processTestData(dataIndex, pResult);
											// $[TI1]
}




//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
GuiTestManager::SoftFault(const SoftFaultID  softFaultID,
							 const Uint32       lineNumber,
							 const char*        pFileName,
							 const char*        pPredicate)  
{
	CALL_TRACE("GuiTestManager::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							GUITESTMANAGER,
							lineNumber, pFileName, pPredicate);
}
