#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SortC_AlarmUpdate - Abstract Sorted List of 'AlarmUpdate' Copies.
//---------------------------------------------------------------------
//@ Interface-Description
//  This provides an abstraction for a sorted list of copies of
//  data items each of type 'AlarmUpdate'.  The data items of the list are
//  copies of the data items that are added to the list.  The items
//  are inserted into the list based on the sorted order of the new
//  item against the current list items.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a sorted collection of sortable objects.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Items are sorted as they're inserted into the list.  A
//  doubly-linked list of copies is used internally to keep the items.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  The restrictions namely have to do with the data type that is
//  chosen for this list.  The data type must provide the following
//  mechanisms, to be allowed to be used in this list:
//>Von
//      AlarmUpdate::AlarmUpdate(const AlarmUpdate& aType);            // copy constructor...
//      AlarmUpdate::operator=(const AlarmUpdate& aType);       // assignment operator...
//      AlarmUpdate::operator==(const AlarmUpdate& aType) const;// equivalence operator...
//      AlarmUpdate::operator!=(const AlarmUpdate& aType) const;// inequivalence operator...
//      AlarmUpdate::operator>(const AlarmUpdate& aType) const; // greater-than operator...
//      AlarmUpdate::operator<(const AlarmUpdate& aType) const; // less-than operator...
//>Voff
//  For primitive (built-in) types these are already supplied, but for
//  complex (class) types, these must be explicitly supplied.
//
//      The assignment operator of any class used as a type of this list
//      must be implemented such that upon completion of an assignment
//      between two instances, the instances are equivalent (i.e.
//      'a = b; POST_CONDITION(a == b);').
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSortC.C_v   25.0.4.0   19 Nov 2013 14:03:44   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SortC_AlarmUpdate.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~SortC_AlarmUpdate()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this sorted list and the items it contains.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The 'rElemList_' alias can NOT be used from within this
//  destructor, because the element list will be destructed by the
//  derived class's destructor BEFORE this destructor is run.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SortC_AlarmUpdate::~SortC_AlarmUpdate(void)
{
  CALL_TRACE("~SortC_AlarmUpdate()");
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isSorted()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by all
//  derived classes, and defined to return a boolean indicator as to
//  whether this instance is currently sorted.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  search(item)
//
//@ Interface-Description
//  Search the current list for a node that is a match of 'item'.
//  If a match is found, then 'SUCCESS' is returned, and the current
//  node is left pointing to the matching node.  If NO match is found,
//  then 'FAILURE' is returned, and the list is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Near the end of this method a pointer to a constant node,
//  'pFoundNode', has its const-ness cast away.  This is due to the
//  fact that 'findNode_()' is a constant method (to allow its use
//  in constant public methods) that returns a pointer.  To ensure
//  consistent const-ness of 'findNode_()', the pointer that is
//  returned is a pointer to a CONSTANT node.  The const-ness of
//  the returned node is not necessary in the design of 'findNode_()',
//  but rather a consistency of design of that method.  Therefore,
//  when this method casts the const-ness of 'pFoundNode' away, it
//  is an acceptable case of working properly with a proper design.
//---------------------------------------------------------------------
//@ PreConditions
//  (isSorted())
//---------------------------------------------------------------------
//@ PostConditions
//  (status == SUCCESS) ==> (currentItem() == item)
//  (status == FAILURE) ==> (pCurrent_ == OLD{pCurrent_})
//@ End-Method
//=====================================================================

SigmaStatus
SortC_AlarmUpdate::search(const AlarmUpdate& item)
{
  CALL_TRACE("search(item)");

  SigmaStatus  status = FAILURE;

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore make sure that it is sorted,
    // and then search for a node that contains a item equivalent to
    // 'item'...
    SAFE_CLASS_PRE_CONDITION((isSorted()));

    // get a pointer to a node that is either equivalent to 'item', or
    // is an immediate neighbor of where an equivalent node would be...
    const DNodeC_AlarmUpdate*  pFoundNode = findNode_(item);

    if (item == pFoundNode->getItem())
    {   // $[TI1.1]
      // the matching node is found, now make sure that 'pFoundNode'
      // points to the first node that is equivalent to 'item', therefore,
      // while not at the beginning of this list, check each previous
      // node for an item that is equivalent to 'item'...
      for (; pFoundNode != getFirst_()  &&
			     item == pFoundNode->getPrevPtr()->getItem();
	   pFoundNode = pFoundNode->getPrevPtr())
      {   // $[TI1.1.1] -- at least one iteration...
	// do nothing...
      }   // $[TI1.1.2] -- no iterations...

      setCurrent_((DNodeC_AlarmUpdate*)pFoundNode); // "const cast away: ..."
      status = SUCCESS;
    }   // $[TI1.2] -- a matching item is NOT found...
  }  // $[TI2] -- this list is currently empty...

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  addItem(newItem)
//
//@ Interface-Description
//  Add 'newItem' into its proper location within this sorted list.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method must first determine where it is the new node that
//  is to contain 'newItem' needs to be located, then it needs to
//  insert a new node at that location.  The 'insert()' method of
//  this list's sorted list is used for the insertion of the new
//  node.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//  (isSorted())
//---------------------------------------------------------------------
//@ PostConditions
//  (currentItem() == newItem)
//  (isSorted())
//@ End-Method
//=====================================================================

void
SortC_AlarmUpdate::addItem(const AlarmUpdate& newItem)
{
  CALL_TRACE("addItem(newItem)");

  InsertionPlace  insertPlace;

  if (!isEmpty())
  {   // $[TI1]
    // this list is not empty, therefore determine the "sorted" location
    // for 'newItem' then add a new node there with an item equivalent
    // to 'newItem'...
    SAFE_CLASS_PRE_CONDITION((!isFull()));  // checked by 'rElemList_'...
    SAFE_CLASS_PRE_CONDITION((isSorted())); // checked by 'findNode_()'...

    // find a node whose item is equivalent to 'newItem', or a neighbor
    // node that is located where 'newItem' should be added...
    const DNodeC_AlarmUpdate*  pFoundNode = findNode_(newItem);

    if (newItem == pFoundNode->getItem())
    {   // $[TI1.1]
      // 'findNode_()' found a node with an item that is equivalent to
      // 'newItem', therefore add 'newItem' after the last node that is
      // equivalent to 'newItem'...
      for (; pFoundNode->getNextPtr() != getFirst_()  &&
			     newItem == pFoundNode->getNextPtr()->getItem();
	   pFoundNode = pFoundNode->getNextPtr())
      {   // $[TI1.1.1] -- at least one iteration...
	// do nothing...
      }   // $[TI1.1.2] -- no iterations...

      // insert the new node after the last node that is equivalent to
      // 'newItem'...
      insertPlace = AFTER_CURRENT;
    }
    else
    {   // $[TI1.2]
      // 'pFoundNode' does not contain an item that is equivalent to
      // 'newItem', but it does contain a node that is to be the neighbor
      // of this new item's node, therefore if 'newItem' is less than
      // the item of 'pFoundNode', then insert it before the current
      // node, otherwise insert it after the current node...
      insertPlace = (newItem < pFoundNode->getItem())
		     ? BEFORE_CURRENT		// $[TI1.2.1]
		     : AFTER_CURRENT;		// $[TI1.2.2]
    }

    // set the current node to be the node that 'findNode_()' found...
    setCurrent_((DNodeC_AlarmUpdate*)pFoundNode);  // "const cast away: ..."
  }
  else
  {   // $[TI2] -- this list is currently empty...
    insertPlace = AFTER_CURRENT;  // either value is fine...
  }

  // insert a new node into this list relative to the current location...
  rElemList_.insert(newItem, insertPlace);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  removeItem(oldItem)
//
//@ Interface-Description
//  Remove the first node in this list that is a match of 'oldItem',
//  and return 'SUCCESS'.  If there is NO match, then 'FAILURE' is
//  returned, and the list is unchanged.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method relies on the fact that search will set the current
//  node to the item found, when an equivalence is found.
//---------------------------------------------------------------------
//@ PreConditions
//  (isSorted())
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

SigmaStatus
SortC_AlarmUpdate::removeItem(const AlarmUpdate& oldItem)
{
  CALL_TRACE("removeItem(oldItem)");
  SAFE_CLASS_PRE_CONDITION((isSorted()));

  SigmaStatus  status;

  // if this list is not empty and the search for 'oldItem' is successful,
  // then remove the node that the search found 'oldItem' in, otherwise
  // return 'FAILURE'...
  status = (!isEmpty()  &&  search(oldItem) == SUCCESS)
	    ? rElemList_.removeCurrent()	// $[TI1]
	    : FAILURE;				// $[TI2]

  return(status);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//	                [static]
//
//@ Interface-Description
//	Report the software fault that occured within the source code
//	of this class.  The fault, indicated by 'softFaultID', occured
//	at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
SortC_AlarmUpdate::SoftFault(const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName,
			       const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, ABSTRACT_SORTC,
                          lineNumber, pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  findNode_(item)  [const]
//
//@ Interface-Description
//  Find the node that contains an element equivalent to 'item',
//  and return a pointer to it.  If no match is found then return
//  the node where the search terminated; if the list is empty, then
//  return 'NULL'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If this list is not empty, then the search will terminate right
//  where a node containing 'item' should be located.
//---------------------------------------------------------------------
//@ PreConditions
//  (isSorted())
//---------------------------------------------------------------------
//@ PostConditions
//  ((!isEmpty()  &&  findNode_() != NULL)  ||
//   (isEmpty()   &&  findNode_() == NULL))
//@ End-Method
//=====================================================================

const DNodeC_AlarmUpdate*
SortC_AlarmUpdate::findNode_(const AlarmUpdate& item) const
{
  CALL_TRACE("findNode_(item)");
  AUX_CLASS_PRE_CONDITION((isSorted()),
			  ((getMaxItems() << 16) | sizeof(AlarmUpdate)));

  const DNodeC_AlarmUpdate*  pFoundNode = getCurrent_();

  if (!isEmpty()  &&  pFoundNode->getItem() != item)
  {   // $[TI1]
    // this list is not empty and 'pFoundNode' is not already pointing
    // at a node containing an item equivalent to 'item'.  If 'pFoundNode'
    // is not at the front of this list and it's item is greater than
    // 'item', then search backwards from 'pFoundNode'.  If 'pFoundNode'
    // is not at the end of this list and it's item is less than
    // 'item', then search forwards from 'pFoundNode'...
    if (pFoundNode != getFirst_()  &&  item < pFoundNode->getItem())
    {   // $[TI1.1]
      // while 'pFoundNode' is not at the front of this list and
      // 'item' is less than the item contained in 'pFoundNode', search
      // backward for a match...
      do
      {
	pFoundNode = pFoundNode->getPrevPtr();
      } while (pFoundNode != getFirst_()  &&
	       item < pFoundNode->getItem());
    }
    else if (pFoundNode->getNextPtr() != getFirst_()  &&
	     item > pFoundNode->getItem())
    {   // $[TI1.2]
      // while 'pFoundNode' is not at the end of this list and
      // 'item' is greater than the item contained in 'pFoundNode', search
      // forward for a match...
      do
      {
	pFoundNode = pFoundNode->getNextPtr();
      } while (pFoundNode->getNextPtr() != getFirst_()  &&
	       item > pFoundNode->getItem());
    }
  }   // $[TI2]

  return(pFoundNode);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  print_(ostr)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by all
//  derived classes, and defined to dump the contents of this instance
//  to 'ostr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented
