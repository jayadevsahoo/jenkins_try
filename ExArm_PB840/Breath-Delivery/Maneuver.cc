#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Maneuver - Maneuver base class
//---------------------------------------------------------------------
//@ Interface-Description
// This class is the base class for all maneuver classes. 
// Maneuvers include expiratory pause, inspiratory pause and 
// respiratory mechanics maneuvers including the NIF, P100 and Vital
// Capacity maneuvers. This base class defines the methods that all 
// derived Maneuver classes must contain. It contains data and accessors 
// methods common to all Maneuver classes. This class also provides a
// public static interface to a single active maneuver. 
//---------------------------------------------------------------------
//@ Rationale
// This class provides the base class for all Maneuver classes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Maneuver.ccv   25.0.4.0   19 Nov 2013 13:59:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc   Date:  29-Nov-2006   SCR Number: 6319
//  Project:  RESPM
//  Description:
//		Removed cancelManeuverRequested() method so derived classes
//      will use clearRequested_ protected data member more consistently.
//		
//  Revision: 006   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//      To accommodate RM maneuvers: Moved pressure stability buffer to 
//      its own class. Moved maneuver specific processing to derived classes.
//
//  Revision: 005  By: syw     Date:  18-Nov-1999    DR Number: 5584
//  Project:  ATC
//  Description:
//		Replaced CancelManeuver() with TerminateManeuver() method.
//		Restore original CancelManeuver().
//
//  Revision: 004  By: yyy     Date:  03-Nov-1999    DR Number: 5458
//  Project:  ATC
//  Description:
//      Clear all the pending or active maneuval request when communicaiton
//		goes down in CancelManeuver().
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By:  yyy    Date:  19-Oct-1998    DR Number: 5217
//       Project:  BiLevel
//       Description:
//      	Removed FORNOW statement.
//
//  Revision: 001  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//=====================================================================

#include "Maneuver.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes
#include "ManeuverRefs.hh"
#include "Trigger.hh"
#include "TriggersRefs.hh"
#include "BreathRecord.hh"

//@ End-Usage

//@ Code...
// initialize static data members
Maneuver* Maneuver::PActiveManeuver_ = NULL;
BreathPhaseType::PhaseType Maneuver::PreviousBreathPhaseType_=BreathPhaseType::NON_BREATHING;

//=====================================================================
//
//  //@ Data-Member: Methods..
//
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Maneuver
//
//@ Interface-Description
// This base class constructor takes a maneuverId as an argument and
// returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// The private member data is is initialized to the value of the 
// maneuverId argument.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Maneuver::Maneuver(ManeuverId maneuverId) :
	maneuverId_(maneuverId),
	maneuverState_(PauseTypes::IDLE),
	clearRequested_(FALSE)

{
	CALL_TRACE("Maneuver::Maneuver(ManeuverId maneuverId)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Maneuver()  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Maneuver::~Maneuver(void)
{
  CALL_TRACE("~Maneuver()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setActiveManeuver
//
//@ Interface-Description
// The method takes no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method sets the active maneuver to "this" maneuver object.
// Sets the private static PActiveManeuver_ to the "this" pointer.
//---------------------------------------------------------------------
//@ PreCondition
//  (PActiveManeuver_ == NULL || PActiveManeuver_ == this)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Maneuver::setActiveManeuver(void)
{
  CALL_TRACE("setActiveManeuver()");

  CLASS_PRE_CONDITION(PActiveManeuver_ == NULL || PActiveManeuver_ == this);

  PActiveManeuver_ = this;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetActiveManeuver
//
//@ Interface-Description
// The method takes no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method resets the active maneuver pointer and clearRequested_
// flag. Called when a maneuver has completed and is going back to 
// the IDLE state.
//---------------------------------------------------------------------
//@ PreCondition
//	PActiveManeuver_ == this
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Maneuver::resetActiveManeuver(void)
{
								// $[TI1]
	CALL_TRACE("resetActiveManeuver(void)");

	CLASS_PRE_CONDITION(PActiveManeuver_ == this);

	clearRequested_ = FALSE;
	PActiveManeuver_ = NULL;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CancelManeuver [static]
//
//@ Interface-Description
//    This method takes no arguments and returns nothing.  Invoke the
//	  active maneuver's clear() method to cancel this maneuver
//    immediately.
//---------------------------------------------------------------------
//@ Implementation-Description
//    If there is an active maneuver, then call clear() to
//    terminate this active maneuver immediately.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
Maneuver::CancelManeuver(void)    
{
    CALL_TRACE("Maneuver::CancelManeuver(void)");

    if (PActiveManeuver_)
    {											// $[TI1.1]
		PActiveManeuver_->clear();
    }											// $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TerminateManeuver [static]
//
//@ Interface-Description
// This static method takes no arguments and returns nothing. It
// issues commands to deactivate the active maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls the active maneuver's virtual method terminateManeuver to 
// terminate the maneuver. Each maneuver has its own termination 
// algorithm that is defined in this virtual method.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
Maneuver::TerminateManeuver(void)    
{
	CALL_TRACE("Maneuver::TerminateManeuver(void)") ;
	
    if (PActiveManeuver_)
    {											// $[TI1.1]
		PActiveManeuver_->terminateManeuver();
    }											// $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: NewCycle [static]
//
//@ Interface-Description
// This static method takes no arguments and returns nothing. It
// is invoked every BD cycle. If there is an active maneuver, this 
// method invokes the maneuver's new cycle method. 
//---------------------------------------------------------------------
//@ Implementation-Description
// If there is an active maneuver, invoke the pure virtual newCycle().
// Also maintain previous breath phase information as a protected
// member for use by the Maneuver classes.
//---------------------------------------------------------------------
//@ PreCondition
//    none.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
Maneuver::NewCycle(void)
{
    CALL_TRACE("NewCycle(void)");

	if (PActiveManeuver_)
	{
		PActiveManeuver_->newCycle();
	}

	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
	PreviousBreathPhaseType_ = pBreathPhase->getPhaseType();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ProcessUserEvent [static]
//
//@ Interface-Description
// This method accepts a UiEvent& and an EventData::RequestedState
// as arguments and returns a Boolean value. This method is invoked 
// whenever the new UiEvent is received. It returns TRUE if the new
// GUI event is allowed. It returns FALSE if the GUI event is rejected.
//---------------------------------------------------------------------
//@ Implementation-Description
// This method is invoked when the GUI sends user request to the BD 
// through the UiEvent class interface. The new user request is
// processed by the current active maneuver. 
// Each maneuver has his own rules for handling new maneuver START 
// requests. These rules are implemented in the virtual method 
// processUserEvent() that returns TRUE if the new maneuver request 
// is allowed or FALSE if it's not allowed.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
Maneuver::ProcessUserEvent(UiEvent& rUiEvent, EventData::RequestedState request)    
{
    CALL_TRACE("Maneuver::DetermineManeuverStatus(EventData::EventId newGuiToBdUserEvent)");

	Boolean continueEvent = TRUE;

	if (PActiveManeuver_ && request == EventData::START)
	{
		// The user requested to start another maneuver while
		// a current maneuver is active
		// the active maneuver decides how to process the new request
		continueEvent = PActiveManeuver_->processUserEvent(rUiEvent);
	}
	else if (!PActiveManeuver_ && request == EventData::STOP)
	{
		// handles case when maneuver completed before user releases button
		// so if there's no active maneuver then there's no need to 
		// continue processing the STOP request
		continueEvent = FALSE;
	}

	return(continueEvent);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Complete [static]
//
//@ Interface-Description
// The method takes no arguments and returns nothing.
// This method is called when the maneuver is completed.  
//---------------------------------------------------------------------
//@ Implementation-Description
// If there is an active maneuver, call the virtual method complete() 
// method to finish up the maneuver. If there are no more pending
// maneuvers of this type, then reset the active maneuver pointer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
Maneuver::Complete(void)
{
	CALL_TRACE("Complete()");

	if (PActiveManeuver_)
	{
		if (PActiveManeuver_->complete())
		{
			// if there are no more pending maneuvers then clear the active
			// maneuver pointer.
			PActiveManeuver_ = NULL;
		}
		// another maneuver is pending so retain active maneuver pointer
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Maneuver::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, MANEUVER,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

