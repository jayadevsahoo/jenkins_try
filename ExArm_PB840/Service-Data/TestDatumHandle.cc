#include "stdafx.h"

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//              Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================== C L A S S   D E S C R I P T I O N ================
//@ Class:  TestDatumHandle -  Provides a dedicated data access handle
//          for each specific service data item.
//---------------------------------------------------------------------
//@ Interface-Description
//     This class provies a dedicated data access handle for a specified
//     service item from the Service Data Manager subsystem.
//---------------------------------------------------------------------
//@ Rationale
//   This class encapsulates data accessing interface to one specific
//   service item ID interface within one object.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Each handle maintains a pointer to the data processing agent
//    which actually manages the service item data type that this
//    handle represents.
//    Since this class is mainly an interface to an actual data
//    processing agent object, most of the access methods are inline
//    methods to avoid extra performance overhead.
//---------------------------------------------------------------------
//@ Fault-Handling
//   Standard CLASS_PRE_CONDITION and CLASS_ASSERTION macros are used
//   to detect some of the obvious error conditions. These macros
//   end up calling TestDatumHandle::SoftFault method.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/TestDatumHandle.ccv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================


#include "TestDatumHandle.hh"
#include "ServiceDataMgr.hh"

//@ Code...
//====================================================================
//
//  Public Methods...
//
//====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestDatumHandle  [ Constructor]
//
//@ Interface-Description
//    Establishes exact service data type this handle is going to
//    provide an access to.
//---------------------------------------------------------------------
//@ Implementation-Description
//    Initializes the 'pAgent_' member to the service data processing
//    agent that handles this service item data type.
//---------------------------------------------------------------------
//@ PreCondition
//  ( ServiceDataMgr::IsTestItemId(id) )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TestDatumHandle::TestDatumHandle(const TestDataId::TestDataItemId itemId)
{
	CALL_TRACE("TestDatumHandle(const TestDataId::TestDataItemId itemId)");
	CLASS_PRE_CONDITION( ServiceDataMgr::IsTestItemId(itemId) );

	pAgent_ = ServiceDataMgr::GetDataAgent(itemId);
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TestDatumHandle()  [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TestDatumHandle::~TestDatumHandle(void)
{
  CALL_TRACE("~TestDatumHandle()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
TestDatumHandle::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_DATA,
	TEST_DATUM_HANDLE, lineNumber, pFileName, pPredicate);
}


#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
