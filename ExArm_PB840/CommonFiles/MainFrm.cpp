#include "stdafx.h"
// MainFrm.cpp : implementation of the CMainFrame class
//

#ifdef SIGMA_GUIPC_CPU
#include "ExArm.h"
#else
#include "main_e600GUI.h"
#endif
#include "GuiFoundation.hh"
#include "MainFrm.h"
#include "LowerBaseContainerView.h"
#include "UpperBaseContainerView.h"




#ifdef SIGMA_GUIPC_CPU
extern CExArmApp theApp;
#else
extern Cmain_e600GUIApp theApp;
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()

// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.cx = GuiFoundation::SCREEN_WIDTH_PIXELS +22;
	cs.cy = (GuiFoundation::SCREEN_HEIGHT_PIXELS*2)+56;
	cs.x =0;
	cs.y =0;
	cs.style = WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE|WS_SYSMENU|WS_THICKFRAME;
	
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.lpszClass = AfxRegisterWndClass(0);

	return TRUE;
}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

#ifndef _WIN32_WCE
void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif

#endif //_DEBUG


// CMainFrame message handlers

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{

	// define the 2 views sizes
	CSize pane1(GuiFoundation::SCREEN_WIDTH_PIXELS,GuiFoundation::SCREEN_HEIGHT_PIXELS);

	// create the 2 splitters
	m_bSplitterCreated = m_ObjSplitterUper_Lower.CreateStatic(this,2, 1,WS_CHILD|WS_VISIBLE|WS_BORDER,AFX_IDW_PANE_FIRST); 

	// associate the classes to the views
	m_ObjSplitterUper_Lower.CreateView(0, 0, RUNTIME_CLASS(CUpperBaseContainerView), pane1,pContext);
	m_ObjSplitterUper_Lower.CreateView(1, 0, RUNTIME_CLASS(CLowerBaseContainerView),pane1, pContext);

	return m_bSplitterCreated;
	//return CFrameWndEx::OnCreateClient(lpcs, pContext);
}


