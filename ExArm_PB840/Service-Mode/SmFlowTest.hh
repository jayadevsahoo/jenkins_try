#ifndef SmFlowTest_HH
#define SmFlowTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SmFlowTest - Service Mode Flow Test
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmFlowTest.hhv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: quf    Date: 24-Apr-2000     DR Number: 5709
//  Project:  NeoMode
//  Description:
//	Added rampFlow_() to ramp the flow to the desired level rather
//	than setting the flow immediately to the desired level.
//
//  Revision: 002  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 001  By:  gdc    Date: 23-Sep-1997    DR Number: 2513
//       Project:  Sigma (R8027)
//       Description:
//           Initial version to support service/manufacturing test.
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

#ifdef SIGMA_UNIT_TEST
# define FlowSensor FakeSensor
# define ExhFlowSensor FakeSensor
# define Psol FakePsol

# include "FakeSensor.hh"
# include "FakePsol.hh"

#else
# include "FlowSensor.hh"
# include "ExhFlowSensor.h"
# include "Psol.hh"

#endif  // SIGMA_UNIT_TEST

class SmFlowTest
{
  public:
	SmFlowTest(  const GasType gasType
               , FlowSensor & flowSensor
               , ExhFlowSensor_t & exhFlowSensor
               , Psol & psol );

	~SmFlowTest(void);

	static void SoftFault(const SoftFaultID softFaultID,
	  const Uint32      lineNumber,
	  const char*       pFileName  = NULL, 
	  const char*       pPredicate = NULL);
	
    static const SmFlowTest & GetOxygenFlowTest(void);
    static const SmFlowTest & GetAirFlowTest(void);

	void purgeCircuit(void) const;
	void establishFlow( const char * const pParamters ) const;

  protected:

  private:
	SmFlowTest(void);  // not implemented...
	SmFlowTest(const SmFlowTest&);	// not implemented...
	void operator=(const SmFlowTest&);	// not implemented...

    void recharge_(void) const;
	void rampFlow_( const Real32 targetFlow) const;

    //@ Data-Member: rFlowSensor_
	// reference to insp flow sensor object to be used
    FlowSensor &    rFlowSensor_;

    //@ Data-Member: rExhFlowSensor_
	// reference to exh flow sensor object to be used
    ExhFlowSensor_t & rExhFlowSensor_;

    //@ Data-Member: rPsol_
	// reference to psol object to be used
    Psol &          rPsol_;

    //@ Data-Member: gasType_
	// gas type to be used
    GasType         gasType_;
};


#ifdef SIGMA_UNIT_TEST
# undef FlowSensor
# undef ExhFlowSensor
# undef Psol
#endif  // SIGMA_UNIT_TEST

#endif // SmFlowTest_HH 
