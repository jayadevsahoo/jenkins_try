#ifndef TrendSelectMenuItems_HH
#define TrendSelectMenuItems_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TrendSelectMenuItems -  Trend parameter selection menu items
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendSelectMenuItems.hhv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc    Date: 10-Jan-2007    SCR Number: 6237
//  Project:  TREND
//  Description:
//      Initial version.
//
//====================================================================

//@ Usage-Classes
#include "GuiAppClassIds.hh"
#include "SettingMenuItems.hh"
//@ End-Usage

class TrendSelectMenuItems : public SettingMenuItems
{
public:
	static TrendSelectMenuItems& GetMenuItems(void);

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
private:
	TrendSelectMenuItems(void);
	virtual ~TrendSelectMenuItems(void);

    // these methods are purposely declared, but not implemented...
    TrendSelectMenuItems(const TrendSelectMenuItems&);				// not implemented...
    void   operator=(const TrendSelectMenuItems&);			// not implemented...
};


#endif // TrendSelectMenuItems_HH
