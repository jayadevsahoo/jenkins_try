#ifndef ClockSettingButton_HH
#define ClockSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ClockSettingButton - A setting button which displays a
// title and the value of a numeric clock setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ClockSettingButton.hhv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SettingButton.hh"

//@ Usage-Classes
#include "ClockNumericField.hh"
//@ End-Usage

class ClockSettingButton : public SettingButton
{
public:
	ClockSettingButton(Int16 xOrg, Int16 yOrg, Int16 width,
				Int16 height, ButtonType buttonType, Int16 bevelSize,
				CornerType cornerType, BorderType borderType,
				TextFont::Size numberSize,
				Int16 valueCenterX, Int16 valueCenterY,
				StringId titleText, SettingId::SettingIdType settingId,
				SubScreen *pFocusSubScreen, StringId messageId);
	~ClockSettingButton(void);

	inline void setItalic(Boolean isItalic);

	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
                                const SettingSubject*         pSubject);


private:
	// these methods are purposely declared, but not implemented...
	ClockSettingButton(const ClockSettingButton&);		// not implemented...
	void   operator=(const ClockSettingButton&);	// not implemented...

	//@ Data-Member: buttonValue_
	// The Clock Numeric Field which displays this button's setting value.
	ClockNumericField buttonValue_;
};

// Inlined methods
#include "ClockSettingButton.in"

#endif // ClockSettingButton_HH 
