// TpSocket.h : header file
//
#if !defined(AFX_TPSOCKET_H__607BB36B_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)
#define AFX_TPSOCKET_H__607BB36B_026F_11D1_9F84_00805FCAC1CC__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "vtUtils.h"
#include "BPServer.h"

// forward declaration

class BootPServer;

// end forward declaration

// define's used by 840 vent conversation on network
enum TftpOpcode{
	Tftp_MIN_OP = 8001,
	OP_RRQ = Tftp_MIN_OP,
	OP_DATA,
	OP_DATA_ACK,
	OP_END_OF_FILE,
	OP_DOWNLOAD_SUCCESS,
	OP_DATA_REQUEST,
	OP_ERROR,
	Tftp_MAX_OP = OP_ERROR
};

typedef	enum TftpState{
	STATES_MIN = 1,
	RRQ_SENT = STATES_MIN,
	RRQ_RECEIVED,
	INITIAL_STATE,
	DATA_SENT,
	DATA_RECEIVED,
	DATA_WAITING,
	ACK_WAITING,
	ACK_RECEIVED,
	END_OF_FILE_REACHED,
	STATES_MAX = ACK_RECEIVED,
	DATA_PROCESSING,
	DATA_DONE,
	CLIENT_ERROR,
}StateControl;

enum DataBlockIndex{
	CURRENT_BLOCK,
	NEXT_BLOCK
};

typedef struct tftpMsgHeader{
	unsigned short int opCode;
	unsigned short int pktSize;
	unsigned short int blockNum;
}TftpMsgHeader;

typedef struct RRQMsg{
	TargetType type;
	char fileName[MAX_FILE_NAME_LENGTH ];
	char padding[188];
}TftpRRQMsg;

#define DATA_BLOCK_SIZE 4096

/////////////////////////////////////////////////////////////////////////////
// TftpClientSocket command target

class TftpClientSocket : public CSocket
{
// Attributes
public:

// Operations
public:
	TftpClientSocket();
	TftpClientSocket(int id, BootPServer *server);
	virtual ~TftpClientSocket();
	void SetClientCallback(void(*callback)(int, int, int));
	void (*clientCallback)(int id, int curblock,int toblock);
	int ClientId();
	int imageId;

private:
	// Attributes
    long clientFilePostion;
	long clientFileSize;
	CStdioFile clientFile;
	int tftpClientId;
	TftpMsgHeader incomingPacket;
	BootPServer* bpServer;
	int tblock;
	int CURRENT_STATE;

	// Operations
	void SendData(int block);
	void CompleteRequest();
	void DoRRQ();
	void ProcessPacket();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TftpClientSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(TftpClientSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TPSOCKET_H__607BB36B_026F_11D1_9F84_00805FCAC1CC__INCLUDED_)
