//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================

#ifndef __Battery_Info_Broadcaster__
#define __Battery_Info_Broadcaster__

// TODO E600 EBM
// Use NetworkApp class to broadcast message when the IsCommunicationUp check
// is removed from NetworkApp::BroadcastMsg method, UdpCommunication class is
// used to workaround it currently.
#include "UdpCommunication.h"

struct ExternalBatteryInfo;
struct SystemBatteryInfo;

/// @class BatteryInfoBroadcaster
/// @detail Implements a task to broadcast the battery information to Ethernet
class BatteryInfoBroadcaster
{
public:
    /// @detail The entry point of broadcast task
    static void BroadcastTask();

private:
   
    BatteryInfoBroadcaster(void);
    ~BatteryInfoBroadcaster(void);

    // Read EBM data from hardware
    static void ReadEBMData_( ExternalBatteryInfo& externalBatteryInfo );
    
    // Read System battery info from hardware
    static void ReadSystemBatteryData_( SystemBatteryInfo& systemBatteryInfo );

    // Initialize the UDP broadcast
    static void InitializeBroadcast_( );

    static  UdpCommunication    UdpComm_;
};

#endif //__Battery_Info_Broadcaster__
