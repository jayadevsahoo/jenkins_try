
#ifndef EventData_HH
#define EventData_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2010, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Struct: bdToGuiEvent and guiToBdUserEvent - Defines
//      structure of data passed between processors for user events
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/EventData.hhv   25.0.4.0   19 Nov 2013 13:54:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: rhj    Date: 01-Sept-2010   SCR Number: 6631  
//  Project:  PROX
//  Description:
//      Added PROX_READY event id.
//  
//  Revision: 008   By: rhj    Date: 23-Jan-2009    SCR Number: 6436  
//  Project:  PROX
//  Description:
//      Added PROX_INSTALLED event id.
//
//  Revision: 007   By: mnr    Date: 24-Feb-2010    SCR Number: 6555  
//  Project:  NEO
//  Description:
//      New SCREEN_LOCK event added.
//  
//  Revision: 006   By: rpr    Date: 23-Jan-2009    SCR Number: 6435  
//  Project:  840S
//  Description:
//      Modified for code review comments.
// 
//  Revision: 005   By: gdc    Date: 14-Jan-2009    SCR Number: 6361
//  Project:  840S
//  Description:
//      Removed unused enum values.
// 
//  Revision: 004   By: rpr    Date: 10-Oct-2008    SCR Number: 
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 003   By: gdc   Date:  21-Feb-2007    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      RESPM Project related changes.    
//
//  Revision: 002  By:  iv     Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//		Project: Sigma (R8027)
//		Description:
//				Initial version.
//  
//====================================================================


# include "Sigma.hh"

//@ Usage-Classes
//@ End-Usage

union EventData
{
  public:

    //@Type: EventId
    // This enum identifies the EventId 
    enum EventId {
        START_OF_EVENT_IDS = 0,
        MANUAL_INSPIRATION = START_OF_EVENT_IDS,
        PERCENT_O2,
		CALIBRATE_O2,
        GUI_FAULT,
        ALARM_RESET,
        EXPIRATORY_PAUSE,
        INSPIRATORY_PAUSE,
        NIF_MANEUVER,
        P100_MANEUVER,
        VITAL_CAPACITY_MANEUVER,
        SST_CONFIRMATION,
		SST_EXIT_TO_ONLINE,
		MANUAL_PURGE,
        CLEAR_KEY,
		SCREEN_LOCK,

        APNEA_VENT,
        BATTERY_BACKUP_READY,
        COMPRESSOR_READY,
        COMPRESSOR_OPERATING,
        OCCLUSION,
        ON_BATTERY_POWER,
        PATIENT_CONNECT,
        SAFETY_VENT,
        SVO,
        VENT_INOP,
		PROX_FAULT,
		PROX_INSTALLED,
		PROX_READY,
		END_OF_EVENT_IDS = PROX_READY,
		NUM_EVENT_IDS
    };

    //@ Type: EventStatus
    // This enum identifies the status of events
    enum EventStatus {
        IDLE,      		// no activity
        PENDING,   		// waiting for event
        REJECTED,  		// request is rejected
        ACTIVE,    		// event is active
        COMPLETE,  		// event has fully completed
        CANCEL,    		// event has prematurely terminated
        AUDIO_ACK, 		// event for audio sound
        NO_ACTION_ACK,	// acknowledge the user event w/o any activity
        ACTIVE_PENDING,	// back to back breath pauses requested
        PLATEAU_ACTIVE	// event when plateau is active
    }; 

    //@ Type: EventPrompt
    // This enum identifies the prompt of events
    enum EventPrompt {
        NULL_EVENT_PROMPT=-1,		// no prompt (default) 
        PENDING_PROMPT,   			// pending mode maneuver prompt
        AUTO_PROMPT,  				// auto mode maneuver prompt
        MANUAL_PROMPT    			// manual mode maneuver prompt
    }; 

    //@ Type: RequestedState
    // This enum identifies the state of a user event requested by the GUI
    enum RequestedState {
        NO_REQUEST, // no activity
        START,      // start an event
        STOP        // stop an event
    }; 

    //@ Type: MoreData
    // This enum identifies the more data used to request percent O2
    enum MoreData {
        NO_O2_DATA = -1,
		HUNDREDPERCENT = 100
    }; 

    // Bd to GUI event status message structure; used for both user and
    // ventilator status events
    struct 
    {
        EventId id;
        EventStatus status;
        EventPrompt prompt;
    } bdToGuiEvent;

    // GUI to BD user event request message structure
    struct 
    {
        EventId id;
        RequestedState request;
		MoreData moredata;		// used to force calibration of O2 sensor
								// when neonatal circuits are in use
								// NO_DATA not valid adult circuit in use else 
								// the target percent is entered 0 - 100 percent
    } guiToBdUserEvent;

};

//=========================================================================
//
//    Global Constants....
//
//=========================================================================

//@ Constant:  NUM_EVENT_IDS
// Number of Events that can be requested from the UI to the BD or whose
// status can be sent from BD to UI
enum { NUM_EVENT_IDS = EventData::NUM_EVENT_IDS };


#endif // EventData_HH 




