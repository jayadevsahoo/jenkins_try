#ifndef BaseContainer_HH
#define BaseContainer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BaseContainer - A specialized Container which is at the top 
// of the container tree.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Foundations/vcssrc/BaseContainer.hhv   25.0.4.0   19 Nov 2013 14:11:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: gdc    Date:  21-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
//	Removed SUN prototype code.
// 
//  Revision: 009   By: gdc    Date:  29-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Added Single Screen Compatibility.
//
//  Revision: 008   By: gdc    Date:  29-Jun-1999    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 007   By: sah    Date: 11-Jan-1999   DR Number: 5321
//  Project:  Sigma (R8027)
//  Description:
//      Changed 'SoftFault()' method to non-inline method.
//
//  Revision: 006  By: sah       Date: 12-Jun-1997    DCS Number: 2208
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated new VGA interface methods, files and types.
//
//  Revision: 005  By: yyy      Date: 17-Jan-1997  DCR Number: 1526
//    Project:  Sigma (R8027)
//    Description:
//      Added isRepaintNeeded()
//
//  Revision: 004  By: gdc      Date: 15-Feb-1996  DR Number: 674
//    Project:  Sigma (R8027)
//    Description:
//	Removed ifndef / endif around include directives
//
//  Revision: 003  By: mpm      Date: 02-Aug-1995  DCR Number: 486
//    Project:  Sigma (R8027)
//    Description:
//      Made destructor virtual as per coding standard requirements.
//
//  Revision: 002  By: yyy      Date: 15-May-1995  DCR Number: 1948
//    Project:  Sigma (R8027)
//    Description:
//      Changed the constant used by changedList to MAX_CHANGE_LIST_NUM.
//  
//  Revision: 001  By: yyy      Date: 01-Jan-1995  DR Number: NONE
//    Project:  Sigma (R8027)
//    Description:
//	Integration baseline.
//====================================================================

#include "Sigma.hh"
#include "GuiFoundation.hh"
#include "VgaDisplayId.hh"

//@ Usage-Classes
#include "Container.hh"
#include "DisplayContext.hh"
#include "GraphicsContext.hh"
#include "TemplateMacros.hh"
#include "SListR_Drawable_MAX_CHANGE_LIST_NUM.hh"

class FixedSListC(Area, MAX_AREA_LIST_NUM);
//@ End-Usage

class BaseContainer : public Container
{
	//@ Friend: GuiFoundation::Initialize()
	// Allows the private constructor for this class to be called during
	// initialization of the GUI-Foundations subsystem ONLY.
	// We do not wish to allow anyone else to instantiate a BaseContainer.
	friend class GuiFoundation;

public:
	// redefine Container::setFillColor() to make NO_COLOR illegal:
	virtual void setFillColor(Colors::ColorS fillColor);

	virtual void setX(Int);   // redefined to make illegal
	virtual void setY(Int);   // redefined to make illegal
	virtual void appendTo(Container *);   // redefined to make illegal
	virtual void removeFromParent(void);   // redefined to make illegal
	virtual void setShow(Boolean);   // redefined to make illegal

	virtual void evaluateBase(void);   // redefined to do nothing

	// redefine Container::evaluateVisible() to always return visible = TRUE:
	virtual void evaluateVisible(void);

	virtual void setDirectDraw(Boolean directDrawEnabled);
	virtual Boolean isDirectDrawEnabled(void) const;
	
	void repaint(void);

#if defined( SIGMA_GUIPC_CPU )
	void invalidate(void);
#endif // SIGMA_GUIPC_CPU

	Boolean isRepaintNeeded(void);
	void addToChangedList(Drawable *pDrawable);

	inline DisplayContext & getDisplayContext(void);
	inline GraphicsContext & getGraphicsContext(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
protected:

private:
	// The constructor is made private to prevent the application from
	// instantiating any BaseContainers.  This is done for the application
	// by the GuiFoundation::Initialize() method; to make this possible,
	// this class grants friendship to GuiFoundation::Initialize() above.
	BaseContainer(DisplayContext & rDisplayContext);
	virtual ~BaseContainer(void);

	BaseContainer(const BaseContainer&);	// declared, not implemented.
	void operator=(const BaseContainer&);	// declared, not implemented.

	void mergeAreaToList_(FixedSListC(Area,MAX_AREA_LIST_NUM)& areaList, Area newArea);

	//@ Data-Member: changedDrawables_
	FixedSListR(Drawable,MAX_CHANGE_LIST_NUM) changedDrawables_;

	//@ Data-Member: displayContext_
	//  holds the DisplayContext for this BaseContainer
	DisplayContext  & displayContext_;

	//@ Data-Member: graphicsContext_
	//  holds the GraphicsContext for this BaseContainer
	GraphicsContext  graphicsContext_;

	//@ Data-Member: isDirectDrawEnabled_
	//  TRUE if the DirectDraw mode is enabled
	Boolean  isDirectDrawEnabled_;
};

#include "BaseContainer.in"

#endif // BaseContainer_HH 
