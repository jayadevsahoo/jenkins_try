// TestResultSet.h : Declaration of the CTestResultSet

#pragma once

// code generated on Tuesday, September 22, 2009, 12:00 PM

class CTestResultSet : public CRecordset
{
public:
	CTestResultSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestResultSet)

// Field/Param Data

// The string types below (if present) reflect the actual data type of the
// database field - CStringA for ANSI datatypes and CStringW for Unicode
// datatypes. This is to prevent the ODBC driver from performing potentially
// unnecessary conversions.  If you wish, you may change these members to
// CString types and the ODBC driver will perform all necessary conversions.
// (Note: You must use an ODBC driver version that is version 3.5 or greater
// to support both Unicode and these conversions).

	LONGLONG	m_test_result_id;
	CStringA	m_test_session_id;
	CStringA	m_test_data;
	CTime		m_test_start_time;
	CTime		m_test_stop_time;

	CStringA	m_test_data_type;
	CStringA	m_test_data_units;
	CStringA	m_test_data_stroage_type;
	CStringA	m_test_status;
	CStringA	m_filename;
	float		m_test_data_min;
	float		m_test_data_max;
	CStringA	m_test_data_derived_from;

	CTime		m_script_start_time;
	CTime		m_script_stop_time;

	CStringA	m_test_comment;
	

// Overrides
	// Wizard generated virtual function overrides
	public:
	virtual CString GetDefaultConnect();	// Default connection string

	virtual CString GetDefaultSQL(); 	// default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX support

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};


