#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SmManager - Service Mode Manager
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides methods for communication between Service-Mode
//	the other subsystems.
//	Since the class is used on both the BD and the GUI CPUs, preprocessor
//	directives are used to partition the code accordingly.
//---------------------------------------------------------------------
//@ Rationale
//	This class is required for communication between Service-Mode and
//	the other subsystems.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The static methods DoTest() and DoFunction() are called by an external
//	subsystem to request a Service-Mode test or function, respectively.
//	The method processRequests() invokes the requested test or function.
//	The method promptOperator() is used by a test to prompt the operator
//	to perform an action, and getOperatorAction() is used to receive
//	the operator keypress after responding to a prompt.
//	The static method OperatorAction() is used by the GUI to send an
//	operator keypress to the Service-Mode subsystem, in response to an
//	operator prompt.
//	Methods are provided to transmit test status and test data messages
//	to the Service-Data subsystem, e.g. sendStatusToServiceData().
//	The methods signalTestPoint() and processTestSignal() are provided
//	to allow tests to trigger the test signal bit during sensor
//	measurements to allow external equipment to synchronize its
//	measurements.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmManager.ccv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 039   By: mnr   Date: 25-May-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//      Delay moved to DevelopmentOptionsSubScreen.cc.
//
//  Revision: 038   By: mnr   Date: 10-May-2010     SCR Number: 39
//  Project:  PROX
//  Description:
//      Only save the PROX config info if PROX SST test passed.
//
//  Revision: 037  By: mnr    Date: 24-Mar-2010     DR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX SST and config info related updates.
//
//  Revision: 036  By: mnr    Date: 28-Dec-2009     SCR Number: 6437
//  Project:  NEO
//  Description:
//      Added support for Advanced and Lockout options.
//
//  Revision: 035  By: rhj    Date: 15-Apr-2009     SCR Number: 6495
//  Project:  840S2
//  Description:
//      Implement a way to log a single test.
//
//  Revision: 034  By: rhj    Date: 04-Apr-2009     SCR Number: 6495
//  Project:  840S2
//  Description:
//      Implemented single test mode.
//
//  Revision: 033  By:  gdc    Date:  18-Feb-2009    SCR Number: 6480
//  Project:  840S
//  Description:
//		Implemented "set vent head operational hours" test.
//
//  Revision: 032   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 031  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 030  By: rhj    Date: 20-Oct-2008     SCR Number: 6435
//  Project: 840S
//  Description:
//  Added ENABLE_LEAK_COMP_OPTION and DISABLE_LEAK_COMP_OPTION to
//  localFunction_.
//
//  Revision: 029  By: gdc    Date: 26-Mar-2007     SCR Number: 6237
//  Project: Trend
//  Description:
//  Added compact flash test.
//
//  Revision: 028  By: rhj    Date: 26-Mar-2007     SCR Number: 6176
//  Project: RESPM
//  Description:
//  Allow a successful EST resets the three-strike unexpected reset counter.
//
//  Revision: 027  By: erm    Date: 18-Dec-2002     DR Number: 6028
//  Project: EMI
//  Description:
//  Add New id MFG_PROD_CONVERT_ID
//
//  Revision: 026  By: cep    Date: 17-Apr-2002     DR Number: 5903
//  Project:  VCP
//  Description:
//  Added default to Switch, and fixed indentation.
//
//  Revision: 025  By: erm    Date: 16-Jan-2002     DR Number: 5984
//  Project: GuiComms
//  Description:
//  Added External Serial LoopBack test
//
//  Revision: 024  By: gdc    Date: 28-Aug-2000     DR Number: 5753
//  Project:  Delta
//  Description:
//      Implemented Single Screen option.
//
//  Revision: 023  By: sah    Date: 17-Jul-2000     DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//      *  added handling of ENABLE/DISABLE directives for VTPC option
//
//  Revision: 022   By: sah   Date:  23-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//      PAV project-related changes:
//      *  added handling of ENABLE/DISABLE directives for PAV option
//
//  Revision: 021  By: quf    Date: 18-May-2000     DR Number: 5728
//  Project:  NeoMode
//  Description:
//      In setupServiceMode_(), added additional setup for RExhO2FlowSensor.
//
//  Revision: 020  By: sah    Date: 06-Jul-1999     DR Number: 5424
//  Project:  NeoMode
//  Description:
//      Added capability for circuit type-specific override of SST, including
//      appropriate resistance and compliance values.  Also, added ability
//		for user to override set options from data key.
//
//  Revision: 019  By: dosman    Date: 26-Feb-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//      ATC initial release.
//      Added #ifdef SIGMA_PRODUCTION around some code
//      that should only be compiled for production, so it was
//      causing a pesky warning message.
//
//  Revision: 018  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 017  By:  gdc    Date: 14-Oct-1998    DR Number: DCS 5105
//       Project:  BiLevel
//       Description:
//          Added call to Post::ClearStrikesLog().
//
//  Revision: 016  By:  syw    Date: 27-Aug-1998    DR Number:
//       Project:  BiLevel
//       Description:
//          BiLevel Initial Release.  Added DK_COPY_HOURS_ID test.
//
//  Revision: 015  By:  quf    Date: 11-Nov-1997    DR Number: DCS 2621
//       Project:  Sigma (840)
//       Description:
//			"Development Option" functions FORCE_EST_OVERIDE_ID,
//			FORCE_SST_OVERIDE_ID, and DEFAULT_FLASH_ID are no longer
//			conditionally compiled with the SIGMA_DEVELOPMENT and
//			SIGMA_PSUEDO_OFFICIAL compile flags, i.e. they are always
//			compiled now.  Fixed FORCE_SST_OVERIDE_ID function.
//
//  Revision: 014  By:  gdc    Date: 10-Oct-1997   DR Number: DCS 2172
//       Project:  Sigma (840)
//       Description:
//			Added missing requirement annotations.
//
//  Revision: 013  By:  quf    Date: 29-Sep-1997   DR Number: DCS 1861
//       Project:  Sigma (840)
//       Description:
//			Added resetting of vent inop test state via
//			localFunction_(SmTestId::DEFAULT_FLASH_ID).
//
//  Revision: 012  By:  quf    Date: 29-Sep-1997   DR Number: DCS 2385
//       Project:  Sigma (840)
//       Description:
//			Added resetting of flow sensor cal status to PASSED and
//			resetting of exh flow offsets to 0 via
//			localFunction_(SmTestId::DEFAULT_FLASH_ID).
//
//  Revision: 011  By:  quf    Date: 25-Sep-1997   DR Number: DCS 2410
//       Project:  Sigma (840)
//       Description:
//			Added atmospheric pressure xducer calibration, including
//			resetting of status to PASSED and resetting of offset to 0 via
//			localFunction_(SmTestId::DEFAULT_FLASH_ID).
//
//	Revision: 010  By:  gdc    Date:  23-Sep-1997    DR Number: DCS 2513
//      Project:  Sigma (R8027)
//      Description:
//			Added several tests to support service/manufacturing test.
//			Removed all references to the remote flow sensor high flow
//			and low flow tests, which are replaced with the remote air
//			and O2 tests.
//
//	Revision: 009  By:  syw    Date:  15-Aug-1997    DR Number: DCS 2386
//      Project:  Sigma (R8027)
//      Description:
//			Call ExhFlowSensor::setUnoffsettedProcessedValuesAlpha().
//
//  Revision: 008  By:  quf    Date: 31-Jul-1997    DR Number: DCS 2013
//       Project:  Sigma (840)
//       Description:
//			Added code for GUI nurse call test.
//
//  Revision: 007  By:  quf    Date: 30-Jul-1997    DR Number: DCS 2205
//       Project:  Sigma (840)
//       Description:
//			Removed conditional compile of Exh Valve Velocity Transducer test code,
//			added case statements for this test in appropriate places.
//
//  Revision: 006  By:  syw    Date: 23-Jul-1997    DR Number: DCS 2279
//       Project:  Sigma (840)
//       Description:
//			Add FlowSensorCalibration.
//
//  Revision: 005  By:  quf    Date: 16-Jul-1997    DR Number: DCS 2066
//       Project:  Sigma (840)
//       Description:
//            During "SST completed" function execution, add an update
//            of the patient circuit type novram variable (previously
//            done by gui-app).
//
//  Revision: 004  By:  quf    Date: 20-Jun-1997    DR Number: DCS 2153
//       Project:  Sigma (840)
//       Description:
//            Add 30 msecs data xmit delay to sendTestPointToServiceData(Real32)
//            to avoid loss of comm due to too much data being sent
//            over the network too quickly, particularly during the
//            GeneralElectronicsTest.
//
//  Revision: 003  By:  quf    Date: 28-May-1997    DR Number: DCS 2042
//       Project:  Sigma (840)
//       Description:
//            Implemented GUI Serial Port Test.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed Modification-Log and PreCondition entry for
//            promptOperator().
//            Fixed global reference names (i.e. "r" changed to "R").
//            Removed SM_TEST-compiled code pertaining to GSCircuitAttachedTest,
//            ExhValveCommandTest, and MyTest.
//
//  Revision: 001  By:  mad    Date:  08-Nov-1995    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "SmManager.hh"

//@ Usage-Classes

#include "Task.hh"
#include "TaskControlAgent.hh"
#include "ForceVentInopTest.hh"
#include "SmPackets.hh"
#include "TestDataPacket.hh"
#include "ServiceDataMgr.hh"
#include "Post.hh"
#include "InitiateReboot.hh"
#include "Background.hh"
#include "SerialNumberInitialize.hh"
#include "SmRefs.hh"
#include "CalInfoDuplication.hh"
#include "PatientCctTypeValue.hh"
#include "HumidTypeValue.hh"
#include "SoftwareOptions.hh"
#include "UserOptions.hh"
#include "NovRamManager.hh"
#include "EstResultTable.hh"

#if defined (SIGMA_BD_CPU)

# include "BdSettingValues.hh"

# include "ExhValveCalibration.hh"
# include "ComplianceCalibration.hh"
# include "InspResistanceTest.hh"
# include "SstFilterTest.hh"
# include "GasSupplyTest.hh"
# include "CircuitPressureTest.hh"
# include "ExhValveSealTest.hh"
# include "ExhValvePerformanceTest.hh"
# include "SafetySystemTest.hh"
# include "GeneralElectronicsTest.hh"
# include "ExhHeaterTest.hh"
# include "LeakTest.hh"
# include "FsCrossCheckTest.hh"
# include "BdLampTest.hh"
# include "BdAudioTest.hh"
# include "SmUtilities.hh"
# include "SmPressureController.hh"
# include "SmFlowController.hh"
# include "SmExhValveController.hh"
# include "BatteryTest.hh"
# include "FlowSensorCalibration.hh"
# include "AtmPresXducerCalibration.hh"
# include "ProxTest.hh"
# include "ProxManager.hh"

# include "MainSensorRefs.hh"
# include "VentObjectRefs.hh"
# include "ExhFlowSensor.hh"
# include "PressureSensor.hh"
# include "VentStatus.hh"
# include "CalInfoRefs.hh"
# include "CalInfoFlashBlock.hh"
# include "ValveRefs.hh"
# include "ExhalationValve.hh"
# include "TemperatureSensor.hh"
#include "Barometer.hh"
// E600 BDIO # include "SmCompressorEeprom.hh"
# include "SmDatakey.hh"
# include "SmFlowTest.hh"
# include "CommTaskAgent.hh"
# include "BreathMiscRefs.hh"
# include "CircuitCompliance.hh"
# include "Resistance.hh"
# include "AtmPressOffset.hh"
# include "ServiceModeNovramData.hh"
# include "FlowSensorOffset.hh"
# include "MiscSensorRefs.hh"
# include "LinearSensor.hh"
# include "PhasedInContextHandle.hh"
#include <string.h>

#endif  // defined(SIGMA_BD_CPU)

#if defined (SIGMA_GUI_CPU)

# include "CompactFlashTest.hh"
# include "GuiIoTest.hh"
# include "SettingContextHandle.hh"
# include "SettingsMgr.hh"
# include "SettingSubject.hh"
# include "BoundStatus.hh"

#endif // defined (SIGMA_GUI_CPU)

#if defined (SIGMA_DEVELOPMENT)
# include <stdio.h>
#endif // defined (SIGMA_DEVELOPMENT)

#if defined (SM_TEST)
# include "TestDriver.hh"
#endif // defined (SM_TEST)

#if defined(SIGMA_UNIT_TEST)
# include "SmUnitTest.hh"
# include "SmManager_UT.hh"
# include "FakeCommTaskAgent.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage


//@ Code...

Boolean SmManager::CommDiedDuringTest_ = FALSE;
Boolean SmManager::TestInProgress_ = FALSE;

#if defined (SIGMA_BD_CPU)

//@ Constant: SERVICE_MODE_ALPHA
// alpha filter constant for Service Mode, used for
// filtered flow and pressure
const Real32 SERVICE_MODE_ALPHA = 0.905;

//@ Constant: TEST_SIGNAL_MASK
// mask for test signal bit
const Uint16 TEST_SIGNAL_MASK = 0x4000 ;


struct ResistanceInfo_
{
	Real32  slope;
	Real32  offset;
};

static const ResistanceInfo_  ADULT_CCT_INSP_RESISTANCE_ =
  {
	  0.00016f,  // slope...
	  0.02240f   // offset...
  };

static const ResistanceInfo_  ADULT_CCT_EXH_RESISTANCE_ =
  {
	  0.00046f,  // slope...
	  0.01328f   // offset...
  };


static const ResistanceInfo_  PED_CCT_INSP_RESISTANCE_ =
  {
	  0.00016f,  // slope...
	  0.04215f   // offset...
  };

static const ResistanceInfo_  PED_CCT_EXH_RESISTANCE_ =
  {
	  0.00053f,  // slope...
	  0.01817f   // offset...
  };


static const ResistanceInfo_  NEO_CCT_INSP_RESISTANCE_ =
  {
	  0.00374f,  // slope...
	  0.04345f   // offset...
  };

static const ResistanceInfo_  NEO_CCT_EXH_RESISTANCE_ =
  {
	  0.00420f,  // slope...
	  0.04561f   // offset...
  };


struct ComplianceInfo_
{
	Real32 lowCompliance;
	Real32 highCompliance;
	Uint32 lowFlowTimeMs;
	Uint32 highFlowTimeMs;
	Real32 lowFlowAdiabaticFactor;
	Real32 highFlowAdiabaticFactor;
};

static const ComplianceInfo_  ADULT_CCT_COMPLIANCE_ =
  {
	  2.36f,  // lowCompliance...
	  2.48f,  // highCompliance...
	  1540u,  // lowFlowTimeMs...
	   760u,  // highFlowTimeMs...
	  0.97f,  // lowFlowAdiabaticFactor...
	  0.95f   // highFlowAdiabaticFactor...
  };

static const ComplianceInfo_  PED_CCT_COMPLIANCE_ =
  {
	  1.46f,  // lowCompliance...
	  1.52f,  // highCompliance...
	   960u,  // lowFlowTimeMs...
	   480u,  // highFlowTimeMs...
	  0.97f,  // lowFlowAdiabaticFactor...
	  0.94f   // highFlowAdiabaticFactor...
  };

static const ComplianceInfo_  NEO_CCT_COMPLIANCE_ =
  {
	  1.35f,  // lowCompliance...
	  0.66f,  // highCompliance...
	   440u,  // lowFlowTimeMs...
	   230u,  // highFlowTimeMs...
	  0.94f,  // lowFlowAdiabaticFactor...
	  0.89f   // highFlowAdiabaticFactor...
  };


#endif // defined (SIGMA_BD_CPU)

#ifdef SIGMA_NEO_UNIT_TEST
extern SmStatusId::TestConditionId Id ;
#endif // SIGMA_NEO_UNIT_TEST



Boolean SmManager::isSingleTestMode_ = FALSE;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================


#if defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmManager()  [Default Constructor]
//
//@ Interface-Description
//  Contructor.  No arguments.  Initializes private data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize private data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmManager::SmManager(void) :
			testPointCount_(0),
			currentTestId_(SmTestId::TEST_NOT_START_ID),
			testRepeat_(FALSE),
			setup_(FALSE),
			functionRequestStart_(0),
			functionRequestEnd_(0),
// E600 BDIO			testSignal_(TEST_SIGNAL_MASK, RWriteIOPort),
			testSignalOn_(FALSE),
			testSignalRequested_(FALSE),
			signalOnTime_(0),
			testType_(SmTestId::SM_MISC_TYPE)
{
	CALL_TRACE("SmManager::SmManager()");

    for (Uint i=0; i<sizeof(pTestParameters_); i++)
    {
	// $[TI1]
        pTestParameters_[i] = 0;
    }

	//TODO E600_LL: hack for calibration; to be removed
	calInProgress = FALSE;
}

#endif // defined (SIGMA_BD_CPU)


#if defined (SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SmManager()  [Default Constructor]
//
//@ Interface-Description
//  Contructor.  No arguments.  Initializes private data members.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize private data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmManager::SmManager(void) :
			testPointCount_(0),
			currentTestId_(SmTestId::TEST_NOT_START_ID),
			testRepeat_(FALSE),
			setup_(FALSE),
			functionRequestStart_(0),
			functionRequestEnd_(0),
			testType_(SmTestId::SM_MISC_TYPE)
{
// $[TI2]
	CALL_TRACE("SmManager::SmManager()");
}

#endif // defined (SIGMA_GUI_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SmManager()  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SmManager::~SmManager(void)
{
	CALL_TRACE("SmManager::~SmManager()");
}



#if defined(SIGMA_GUI_CPU) || defined(SM_TEST) || defined(SIGMA_UNIT_TEST)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doTest()
//
//@ Interface-Description
//	This method is used to request a test to be performed.  Takes
//	a ServiceModeTestId and a Boolean testRepeat as arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Forward GUI test requests to the local test handler, and
//	forward BD test requests to the remote test handler.
//	NOTE: all test requests initiate on the GUI.
//---------------------------------------------------------------------
//@ PreCondition
//	BD state == service, SST, or unknown.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::doTest(SmTestId::ServiceModeTestId testId,
		Boolean testRepeat, const char * const pParameters )
{
	CALL_TRACE("SmManager::doTest(SmTestId::ServiceModeTestId testId)");

#if !defined(SM_TEST)
	CLASS_PRE_CONDITION(
		(TaskControlAgent::GetBdState() == STATE_SERVICE) ||
		(TaskControlAgent::GetBdState() == STATE_SST) ||
		(TaskControlAgent::GetBdState() == STATE_UNKNOWN) );
#endif  // !defined(SM_TEST)

	switch (testId)
	{
		case SmTestId::INITIALIZE_FLOW_SENSORS_ID:
		case SmTestId::GAS_SUPPLY_TEST_ID:
		case SmTestId::CIRCUIT_RESISTANCE_TEST_ID:
		case SmTestId::SST_FILTER_TEST_ID:
		case SmTestId::BD_LAMP_TEST_ID:
		case SmTestId::BD_AUDIO_TEST_ID:
		case SmTestId::FS_CROSS_CHECK_TEST_ID:
		case SmTestId::SST_FS_CC_TEST_ID:
		case SmTestId::CIRCUIT_PRESSURE_TEST_ID:
		case SmTestId::SAFETY_SYSTEM_TEST_ID:
		case SmTestId::EXH_VALVE_SEAL_TEST_ID:
		case SmTestId::EXH_VALVE_TEST_ID:
		case SmTestId::EXH_VALVE_CALIB_TEST_ID:
		case SmTestId::FS_CALIBRATION_ID:
		case SmTestId::SM_LEAK_TEST_ID:
		case SmTestId::SST_LEAK_TEST_ID:
		case SmTestId::COMPLIANCE_CALIB_TEST_ID:
		case SmTestId::EXH_HEATER_TEST_ID:
		case SmTestId::GENERAL_ELECTRONICS_TEST_ID:
		case SmTestId::BATTERY_TEST_ID:
		case SmTestId::BD_INIT_SERIAL_NUMBER_ID:
		case SmTestId::BD_VENT_INOP_ID:

#ifdef SIGMA_DEVELOPMENT
		case SmTestId::BD_INIT_DEFAULT_SERIAL_NUMBER_ID:
#endif // SIGMA_DEVELOPMENT

		case SmTestId::RESTART_BD_ID:
		case SmTestId::ATMOS_PRESSURE_READ_ID:
		case SmTestId::SET_COMPRESSOR_SN_ID:
		case SmTestId::SET_COMPRESSOR_TIME_ID:
		case SmTestId::SET_DATAKEY_TO_VENT_DATA_ID:
		case SmTestId::SET_AIR_FLOW_ID:
		case SmTestId::SET_O2_FLOW_ID:
		case SmTestId::PURGE_WITH_AIR_ID:
		case SmTestId::PURGE_WITH_O2_ID:
		case SmTestId::ATMOS_PRESSURE_CAL_ID:
		case SmTestId::SET_VENT_HEAD_TIME_ID:
		case SmTestId::SM_SST_PROX_TEST_ID:
#ifdef SIGMA_GUI_CPU
		/*TODO E600 TT: in the original 840 code, DK_COPY_HOURS_ID was not 
			handled by any case of SmManager::doTest,
			and fell into default resulting in CLASS_ASSERTION_FAILURE. 
			For now just to prevent CLASS_ASSERTION_FAILURE, need verify on vent in future*/
		case SmTestId::DK_COPY_HOURS_ID:
#endif
		// $[TI1]
			remoteTestRequest_(testId, testRepeat, pParameters);
			break;

		case SmTestId::GUI_KEYBOARD_TEST_ID:
		case SmTestId::GUI_KNOB_TEST_ID:
		case SmTestId::GUI_LAMP_TEST_ID:
		case SmTestId::GUI_TOUCH_TEST_ID:
		case SmTestId::GUI_AUDIO_TEST_ID:
		case SmTestId::GUI_SERIAL_PORT_TEST_ID:
		case SmTestId::SERIAL_LOOPBACK_TEST_ID:
		case SmTestId::GUI_INIT_SERIAL_NUMBER_ID:
		case SmTestId::GUI_VENT_INOP_ID:
		case SmTestId::GUI_NURSE_CALL_TEST_ID:

#ifdef SIGMA_DEVELOPMENT
		case SmTestId::GUI_INIT_DEFAULT_SERIAL_NUMBER_ID:
#endif // SIGMA_DEVELOPMENT

		case SmTestId::CAL_INFO_DUPLICATION_ID:
		case SmTestId::RESTART_GUI_ID:
		case SmTestId::COMPACT_FLASH_TEST_ID:
		// $[TI2]
			localTestRequest_(testId, testRepeat, pParameters);
			break;

		case SmTestId::TEST_NOT_START_ID:
		case SmTestId::NUM_SERVICE_TEST_ID:
		default:
		// $[TI3]
		    CLASS_ASSERTION_FAILURE();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operatorAction()
//
//@ Interface-Description
//	This method is used to send an operator keypress action to a test
//	which had previously sent an operator prompt.  Takes ServiceModeTestId
//	and ActionId as arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Forward operator action for a GUI test to the local action handler, and
//	forward operator action for a BD test to the remote action handler.
//	NOTE: all operator actions initiate on the GUI.
//---------------------------------------------------------------------
//@ PreCondition
//	BD state == service, SST, or unknown.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::operatorAction(SmTestId::ServiceModeTestId testId,
						  SmPromptId::ActionId actionId)
{
	CALL_TRACE("SmManager::operatorAction(\
		SmTestId::ServiceModeTestId testId,\
		SmPromptId::ActionId actionId)");

#if !defined(SM_TEST)
	CLASS_PRE_CONDITION(
		(TaskControlAgent::GetBdState() == STATE_SERVICE) ||
		(TaskControlAgent::GetBdState() == STATE_SST) ||
		(TaskControlAgent::GetBdState() == STATE_UNKNOWN) );
#endif  // !defined(SM_TEST)

	// Use the test id to determine which CPU to send the action
	switch (testId)
	{
		case SmTestId::INITIALIZE_FLOW_SENSORS_ID:
		case SmTestId::GAS_SUPPLY_TEST_ID:
		case SmTestId::CIRCUIT_RESISTANCE_TEST_ID:
		case SmTestId::SST_FILTER_TEST_ID:
		case SmTestId::BD_LAMP_TEST_ID:
		case SmTestId::BD_AUDIO_TEST_ID:
		case SmTestId::FS_CROSS_CHECK_TEST_ID:
		case SmTestId::SST_FS_CC_TEST_ID:
		case SmTestId::CIRCUIT_PRESSURE_TEST_ID:
		case SmTestId::SAFETY_SYSTEM_TEST_ID:
		case SmTestId::EXH_VALVE_SEAL_TEST_ID:
		case SmTestId::EXH_VALVE_TEST_ID:
		case SmTestId::EXH_VALVE_CALIB_TEST_ID:
		case SmTestId::FS_CALIBRATION_ID:
		case SmTestId::SM_LEAK_TEST_ID:
		case SmTestId::SST_LEAK_TEST_ID:
		case SmTestId::COMPLIANCE_CALIB_TEST_ID:
		case SmTestId::EXH_HEATER_TEST_ID:
		case SmTestId::GENERAL_ELECTRONICS_TEST_ID:
		case SmTestId::BATTERY_TEST_ID:
		case SmTestId::BD_INIT_SERIAL_NUMBER_ID:
		case SmTestId::BD_VENT_INOP_ID:
		case SmTestId::SET_AIR_FLOW_ID:
		case SmTestId::SET_O2_FLOW_ID:
		case SmTestId::SM_SST_PROX_TEST_ID:

#ifdef SIGMA_DEVELOPMENT
		case SmTestId::BD_INIT_DEFAULT_SERIAL_NUMBER_ID:
#endif // SIGMA_DEVELOPMENT

		case SmTestId::RESTART_BD_ID:
		// $[TI1]
			remoteActionNotification_(actionId);
			break;

		case SmTestId::GUI_KEYBOARD_TEST_ID:
		case SmTestId::GUI_KNOB_TEST_ID:
		case SmTestId::GUI_LAMP_TEST_ID:
		case SmTestId::GUI_AUDIO_TEST_ID:
		case SmTestId::GUI_TOUCH_TEST_ID:
		case SmTestId::GUI_SERIAL_PORT_TEST_ID:
		case SmTestId::SERIAL_LOOPBACK_TEST_ID:
		case SmTestId::GUI_INIT_SERIAL_NUMBER_ID:
		case SmTestId::GUI_VENT_INOP_ID:
		case SmTestId::GUI_NURSE_CALL_TEST_ID:

#ifdef SIGMA_DEVELOPMENT
		case SmTestId::GUI_INIT_DEFAULT_SERIAL_NUMBER_ID:
#endif // SIGMA_DEVELOPMENT

		case SmTestId::CAL_INFO_DUPLICATION_ID:
		case SmTestId::RESTART_GUI_ID:
		case SmTestId::COMPACT_FLASH_TEST_ID:
		// $[TI2]
			localActionNotification_(actionId);
			break;

		case SmTestId::TEST_NOT_START_ID:
		case SmTestId::NUM_SERVICE_TEST_ID:
		default:
		// $[TI3]
		    CLASS_ASSERTION_FAILURE();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: doFunction()
//
//@ Interface-Description
//	This method is used to request a function to be performed.  Takes
//	a FunctionId argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Forward GUI test requests to the local test handler, and
//	forward BD test requests to the remote test handler.
//	NOTE: all test requests initiate on the GUI.
//---------------------------------------------------------------------
//@ PreCondition
//	BD state == service, SST, or unknown.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::doFunction(SmTestId::FunctionId functionId)
{
// $[TI1]
	CALL_TRACE("SmManager::doFunction(SmTestId::FunctionId functionId)");

#if !defined(SM_TEST)
	CLASS_PRE_CONDITION(
		(TaskControlAgent::GetBdState() == STATE_SERVICE) ||
		(TaskControlAgent::GetBdState() == STATE_SST) ||
		(TaskControlAgent::GetBdState() == STATE_UNKNOWN) );
#endif  // !defined(SM_TEST)

// For BD or GUI stand-alone testing, don't send a remote request
#if !defined(SM_TEST)
	// Tell the remote CPU about the request
	RSmManager.remoteFunctionRequest_(functionId);
#endif // !defined(SM_TEST)

// For unit testing, don't send a local request here, since
// the remote request will indirectly perform a local request
// as long as the LOCAL_COM flag is defined.
#if !defined (SIGMA_UNIT_TEST)
	// Tell the local CPU about the request
	RSmManager.localFunctionRequest_(functionId);
#endif  // !defined(SIGMA_UNIT_TEST)

}

#endif  // defined(SIGMA_GUI_CPU) || defined(SM_TEST) || defined(SIGMA_UNIT_TEST)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendStatusToServiceData()
//
//@ Interface-Description
//	This method is used to send status messages to the Service-Data
//	subsystem.  Two arguments: a TestStatusId and a TestConditionId,
//	no return value.  Types of status messages are test start,
//	test end, operator exit, option not installed, test failure, and
//	test alert.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Build the status packet, then send it to service data if communication
//	is currently up and has never gone down during the current test.
//	Delay to allow the packet to be processed.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::sendStatusToServiceData (
			SmStatusId::TestStatusId testStatusId,
			SmStatusId::TestConditionId testConditionId)
{
	CALL_TRACE("SmManager::sendStatusToServiceData( \
	            SmStatusId::TestStatusId testStatusId, \
	            SmStatusId::TestConditionId testConditionId)");

#ifdef SIGMA_NEO_UNIT_TEST
	Id = testConditionId ;
#endif // SIGMA_NEO_UNIT_TEST

	// Build the status packet
	TestStatusPacket statusPacket;

	statusPacket.testId = currentTestId_;
	statusPacket.status = testStatusId;
	statusPacket.repeatTestRequested = testRepeat_;
	statusPacket.condition = testConditionId;

	// only send message if comm never went down during test
	if ( !CommDiedDuringTest_  &&
			NetworkApp::IsCommunicationUp() )
	{
	// $[TI1]
#ifdef SIGMA_UNIT_TEST
		// for unit testing, send status msg to buffer
		SmUnitTest::StatusBuffer[SmUnitTest::StatusIndex++] = statusPacket;
#else
		ServiceDataMgr::NewStatusData(&statusPacket, sizeof(statusPacket));

		// delay if necessary
		delayForRemoteTests_();
#endif  // SIGMA_UNIT_TEST
	}
	else
	{
	// $[TI2]
		CommsDown();
	}
	// end else


#ifdef SIGMA_DEBUG
	printf("Test Status: %d, Condition: %d\n", testStatusId, testConditionId);
#endif

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendTestStartToServiceData()
//
//@ Interface-Description
//	This method is used to send a test start message to the Service-Data
//	subsystem.  Takes a ServiceModeTestId as an argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Store the input test ID in data member currentTestId_.
//	Set testPointCount_ to 0.
//	Call sendStatusToServiceData() with TEST_START as the status ID
//	and with the default condition ID for a test start.
//---------------------------------------------------------------------
//@ PreCondition
//	currentTestId_ == SmTestId::TEST_NOT_START_ID  (i.e. no test should be
//	in progress yet)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::sendTestStartToServiceData (SmTestId::ServiceModeTestId testId)
{
// $[TI1]
	CALL_TRACE("SmManager::sendTestStartToServiceData (\
		SmTestId::ServiceModeTestId testId)");

	CLASS_PRE_CONDITION(currentTestId_ == SmTestId::TEST_NOT_START_ID);

	currentTestId_ = testId;
	testPointCount_ = 0;

#ifdef SIGMA_DEBUG
	printf("Starting test #%d...", currentTestId_);
#endif

	SmManager::sendStatusToServiceData(
		SmStatusId::TEST_START, SmStatusId::NULL_CONDITION_ITEM);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendTestEndToServiceData()
//
//@ Interface-Description
//	This method is used to send a test end message to the Service-Data
//	subsystem.  Takes a ServiceModeTestId as an argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Call sendStatusToServiceData() with TEST_END as the status ID
//	and with the default condition ID for a test end.
//	Set data member to TEST_NOT_START_ID to indicate test not in progress.
//---------------------------------------------------------------------
//@ PreCondition
//	currentTestId_ == argument test ID.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::sendTestEndToServiceData (SmTestId::ServiceModeTestId testId)
{
// $[TI1]
	CALL_TRACE("SmManager::sendTestEndToServiceData (\
		SmTestId::ServiceModeTestId testId)");

	CLASS_PRE_CONDITION (currentTestId_ == testId);

#ifdef SIGMA_DEBUG
	printf("End of test #%d...", currentTestId_);
#endif

	SmManager::sendStatusToServiceData(
		SmStatusId::TEST_END, SmStatusId::NUM_CONDITION_ITEM);

	currentTestId_ = SmTestId::TEST_NOT_START_ID;

#ifdef SIGMA_UNIT_TEST
	SmUnitTest::TestDone = TRUE;
#endif  // SIGMA_UNIT_TEST

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendOperatorExitToServiceData()
//
//@ Interface-Description
//	This method is used to send an operator exit message to the Service-Data
//	subsystem.  No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Call sendStatusToServiceData() with TEST_OPERATOR_EXIT as the status ID
//	and with the default condition ID for an operator exit.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::sendOperatorExitToServiceData (void)
{
// $[TI1]
	CALL_TRACE("SmManager::sendOperatorExitToServiceData(void)");

#ifdef SIGMA_DEBUG
	printf("Operator Exit...");
#endif

	SmManager::sendStatusToServiceData(
		SmStatusId::TEST_OPERATOR_EXIT, SmStatusId::NULL_CONDITION_ITEM);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendOptionNotInstalledToServiceData()
//
//@ Interface-Description
//	This method is used to send an "option not installed" method to the
//	Service-Data subsystem.  No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Call sendStatusToServiceData() with NOT_INSTALLED as the status ID
//	and with the default condition ID for a NOT_INSTALLED status.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::sendOptionNotInstalledToServiceData (void)
{
// $[TI1]
	CALL_TRACE("SmManager::sendOptionNotInstalledToServiceData(void)");

#ifdef SIGMA_DEBUG
	printf("Option Not Installed...");
#endif

	SmManager::sendStatusToServiceData(
		SmStatusId::NOT_INSTALLED, SmStatusId::NULL_CONDITION_ITEM);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendTestPointToServiceData()
//
//@ Interface-Description
//	This method is used to send a single test data point to the Service-Data
//	subsystem and automatically increment a counter which represents the
//	current data ID.  Takes a Real32 data point value as an argument, no
//	return value.
//	This version of the method is used when the current value of
//	testPointCounter_ corresponds to the correct data ID for the
//	data point argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Build the data packet, then send it to Service-Data if communication
//	is currently up and has never gone down during the current test.
//	Delay to allow the packet to be processed.
//	Increment testPointCounter_.
//
//  $[08047] For any EST test invoked through the GUI serial interface,
//  Sigma shall return each flow and pressure tested data point.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::sendTestPointToServiceData(Real32 testPoint)
{
	CALL_TRACE("SmManager::sendTestPointToServiceData(Real32 testPoint)");

	// Build the test data packet
	TestDataPacket dataPacket;

	dataPacket.testId = currentTestId_;
	dataPacket.dataCount = 1;
	dataPacket.startDataIndex = (SmDataId::ItemizedTestDataId) testPointCount_;
	dataPacket.pointData[0] = testPoint;

	// only send message if comm never went down during test
	if ( !CommDiedDuringTest_ &&
		 NetworkApp::IsCommunicationUp() )
	{
	// $[TI1]
#ifdef SIGMA_UNIT_TEST
		// for unit testing, send data msg to buffer
		SmUnitTest::DataBuffer[SmUnitTest::DataIndex++] = dataPacket;
#else
		ServiceDataMgr::NewTestData(&dataPacket, sizeof(dataPacket));

		// delay to avoid inundating the network with test data --
		// 1 VRTX tick works experimentally, use 3 for engineering margin
		Task::Delay(0, 30);

		// add additional delay for remote tests only
		delayForRemoteTests_();
#endif  // SIGMA_UNIT_TEST
	}
	else
	{
	// $[TI2]
		CommsDown();
	}
	// end else

#ifdef SIGMA_DEBUG
	printf("Test Point #%d: %8.3f\n", testPointCount_, testPoint);
#endif

	// increment test point index
	testPointCount_++;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendTestPointToServiceData()
//
//@ Interface-Description
//	This method is used to send a single test data point to the Service-Data
//	subsystem.  Takes an ItemizedTestDataId and a Real32 data point value
//	as arguments, no return value.
//	This version of the method is used when the calling function needs
//	to associate a specific data ID with the data, rather than using
//	the currently-stored testPointCount_ as the data ID.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set testPointCount_ equal to the ItemizedTestDataId argument, then
//	call sendTestPointToServiceData(testPoint).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void SmManager::sendTestPointToServiceData
	(SmDataId::ItemizedTestDataId nextTestPoint,
	Real32 testPoint)

{
// $[TI3]
	CALL_TRACE("void SmManager::sendTestPointToServiceData\
	(SmDataId::ItemizedTestDataId nextTestPoint, Real32 testPoint)");

	testPointCount_ = nextTestPoint;
	sendTestPointToServiceData(testPoint);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sendMonitorPointToServiceData()
//
//@ Interface-Description
//	This method is used to send a single test data point to the Service-Data
//	subsystem without incrementing the data point counter, e.g.
//	for updating data to be used to update the pressure bargraph pointer
//	during the Leak Test, where the changing data is sent with the
//	same test ID.
//	Takes a Real32 data point value	as an argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Build the data packet, then send it to Service-Data if communication
//	is currently up and has never gone down during the current test.
//	NOTE: since the pointer needs to be updated as quickly as possible
//	during the pressurization phase of the Leak Test, don't put a
//	delay after the data send.  The calling function is responsible for
//	inserting delays between calls.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::sendMonitorPointToServiceData(Real32 monitorPoint)
{
	CALL_TRACE("SmManager::sendMonitorPointToServiceData(Real32 monitorPoint)");

	// Build the test data packet
	TestDataPacket dataPacket;

	dataPacket.testId = currentTestId_;
	dataPacket.dataCount = 1;
	dataPacket.startDataIndex = (SmDataId::ItemizedTestDataId) testPointCount_;
	dataPacket.pointData[0] = monitorPoint;

	// only send message if comm never went down during test
	if ( !CommDiedDuringTest_ &&
		 NetworkApp::IsCommunicationUp() )
	{
	// $[TI1]
#ifdef SIGMA_UNIT_TEST
		// for unit testing, send data msg to buffer
		SmUnitTest::DataBuffer[SmUnitTest::DataIndex] = dataPacket;
		SmUnitTest::MonitorPointBuffer[SmUnitTest::MonitorPointIndex++] = monitorPoint;
#else
		ServiceDataMgr::NewTestData(&dataPacket, sizeof(dataPacket));
#endif  // SIGMA_UNIT_TEST
	}
	else
	{
	// $[TI2]
		CommsDown();
	}
	// end else

#ifdef SIGMA_DEBUG
	printf("Monitor Point #%d: %8.3f\n", testPointCount_, monitorPoint);
#endif
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: promptOperator()
//
//@ Interface-Description
//	This method sends an operator prompt to the GUI.  Two arguments:
//	a PromptId indicating which prompt message to display and a
//	KeysAllowedId to indicate which GUI keys are allowed as a response
//	to the prompt.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Build the operator prompt packet, then send it to Service-Data
//	if communication is currently up and has never gone down during the
//	current test.
//---------------------------------------------------------------------
//@ PreCondition
//	promptId argument must be in the acceptable range.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::promptOperator (SmPromptId::PromptId promptId,
							SmPromptId::KeysAllowedId keysAllowedId)
{
	CALL_TRACE("SmManager::promptOperator(\
				SmPromptId::PromptId promptId,\
				SmPromptId::KeysAllowedId keysAllowedId)");

	CLASS_PRE_CONDITION(
				promptId > SmPromptId::NULL_PROMPT_ID &&
				promptId < SmPromptId::NUM_PROMPT_ID );

	// Build the operator prompt packet
	TestPromptPacket promptPacket;

	promptPacket.testId = currentTestId_;
	promptPacket.prompt = promptId;
	promptPacket.keysAllowed = keysAllowedId;

	// only send message if comm never went down during test
	if ( !CommDiedDuringTest_ && NetworkApp::IsCommunicationUp() )
	{
	// $[TI1]
#ifdef SIGMA_UNIT_TEST
		// for unit testing, send prompt msg to buffer
		SmUnitTest::PromptBuffer[SmUnitTest::PromptIndex++] = promptPacket;
#else
		ServiceDataMgr::NewPromptData(&promptPacket, sizeof(promptPacket));
#endif  // SIGMA_UNIT_TEST
	}
	else
	{
	// $[TI2]
		CommsDown();
	}
	// end else

#ifdef SM_TEST
	// for bd or gui stand-alone testing
	RTestDriver.displayPrompt(promptId);
#endif  // SM_TEST

#ifdef SIGMA_DEBUG
	printf("Prompt Id= %d  Keys Allowed= %d\n", promptId, keysAllowedId);
#endif  // SIGMA_DEBUG

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getOperatorAction()
//
//@ Interface-Description
//	This method receives an operator action from the GUI via the
//	SERVICE_MODE_ACTION_Q.  No arguments, returns an ActionId.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Wait until a  message is received on the SERVICE_MODE_ACTION_Q
//	or until communication goes down.
//	If a message was received, extract the action ID and return it.
//	If comm went down, return an "operator exit" action ID so that
//	test will quit immediately.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SmPromptId::ActionId
SmManager::getOperatorAction (void)
{
	CALL_TRACE("SmManager::getOperatorAction (void)");

	static MsgQueue smActionQueue(::SERVICE_MODE_ACTION_Q);
	Int32 msgPend;
	Int32 status ;

#ifndef SM_TEST
	// If comm goes down before or during a wait for operator keypress,
	// force an Exit button press to exit the current test.
	for(;;)
	{
	// $[TI1]
		Task::Delay(0, 10);

		if ( !CommDiedDuringTest_ &&
			 NetworkApp::IsCommunicationUp() )
		{
		// $[TI1.1]
			status = smActionQueue.acceptMsg(msgPend);

			if (status == Ipc::OK)
			{
			// $[TI1.1.1]
				break;
			}
			else
			{
			// $[TI1.1.2]
				CLASS_ASSERTION( status == Ipc::NO_MESSAGE);

# if defined(SIGMA_UNIT_TEST)
				// For unit testing, simulate "no message" on 1st iteration
				// then get simulated keypress from buffer on 2nd iteration
				SmManager::OperatorAction (SmUnitTest::TestId,
					SmUnitTest::ActionBuffer[SmUnitTest::ActionIndex++]);
# endif  // defined(SIGMA_UNIT_TEST)
			}
			// end else
		}
		else
		{
		// $[TI1.2]
			CommsDown();
			msgPend = SmPromptId::USER_EXIT;
			break;
		}
		// end else
	}
	// end for

#else

	// for BD or GUI stand-alone testing, get simulated keypress from
	// vconsole user input
	RTestDriver.queueAction();
	smActionQueue.pendForMsg(msgPend);

#endif  // SM_TEST

	SmPromptId::ActionId actionId = (SmPromptId::ActionId) msgPend;

	return (actionId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequests()
//
//@ Interface-Description
//	This method processes the test and function requests.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	While Service Mode is not complete:
//		Pend for a test or function request message on the SERVICE_MODE_TEST_Q.
//		When a request is received:
//			Call setupServiceMode_() if this is the 1st request.
//			Determine the type of request (test or function) then
//			execute the request via the appropriate method.
//	When Service Mode is complete, exit loop and reboot system.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
//TODO E600_LL: for calibration hacking; remove later
#include "BDIORefs.hh"
#include "AioDioDriver.h"
#include "BDIOTest.h"

void
SmManager::processRequests (void)
{
	CALL_TRACE("SmManager::processRequests (void)");

	Int32 msgPend = -1;	

	MsgQueue serviceModeTestQueue(::SERVICE_MODE_TEST_Q);
	MsgQueue serviceModeActionQueue(::SERVICE_MODE_ACTION_Q);

#ifdef SIGMA_GUIPC_CPU
	// TODO E600 - Revist when the task initialization order issue
	// solved.

	// Delay some time for Network-App system ready, or the service
	// mode stay in "Cal Info Duplication" subscreen forever.
	Task::Delay(5);
#endif

#ifdef SIGMA_PRODUCTION
	while (!ServiceMode::IsServiceComplete())
#else
	for(;;)		// never exit loop for development build
#endif  // SIGMA_PRODUCTION
	{
	// $[TI1]

#if defined (SM_TEST)
		// display menu for service mode stand-alone test driver
		RTestDriver.testMenu();
#endif // SM_TEST

		//TODO E600_LL: comment the following lines out for calibration hacking; change back later LL_E600_CAL
#if !defined(SIGMA_BD_CPU)
		Int32 status = serviceModeTestQueue.pendForMsg(msgPend);
	    CLASS_ASSERTION (status == Ipc::OK);
#endif

#if !defined(SM_TEST)
# if defined(SIGMA_GUI_CPU)
		CLASS_ASSERTION(
		  (TaskControlAgent::GetBdState() == STATE_SERVICE) ||
		  (TaskControlAgent::GetBdState() == STATE_SST) ||
		  (TaskControlAgent::GetBdState() == STATE_UNKNOWN) );
# else
		CLASS_ASSERTION(
		  (TaskControlAgent::GetBdState() == STATE_SERVICE) ||
		  (TaskControlAgent::GetBdState() == STATE_SST) );
# endif // SIGMA_GUI_CPU
#endif  // !defined(SM_TEST)

		if (!setup_)
		{
		// $[TI1.1]
			setupServiceMode_();
			setup_ = TRUE;
		}
		// $[TI1.2]
		// implied else: skip setup if already performed

		////////////////////////////////////////////////////////////////////////
		//TODO E600_LL: for calibration hacking; remove later - LL_E600_CAL
#if defined (SIGMA_BD_CPU)
		calInProgress = FALSE;
		for(;;)
		{
			Boolean bInitialWait = FALSE;

			// TODO E600 MS remove this whole loop when it's decided that
			// we will no longer need this
			if( FALSE )
			//if( AioDioDriver.IsServiceRequest() )
			{
				if(bInitialWait == FALSE)
				{
					// wait here to give BDIOTest a chance to get to a new cal state
					Task::Delay( 10, 0) ;
					bInitialWait = TRUE;
				}
				BdioCalType_t CalType;
				CalType = BdioTest.GetCalType();

				if(CalType == CAL_FLOW_SENSOR_OFFSET)
				{
					msgPend = TEST_REQUEST;
					pendingTestId_ = SmTestId::FS_CALIBRATION_ID;
					break;
				}
				else if(CalType == CAL_PSOL_LIFTOFF)
				{
					msgPend = TEST_REQUEST;
					pendingTestId_ = SmTestId::FS_CROSS_CHECK_TEST_ID;
					break;
				}
				Task::Delay( 1, 0) ;
			}
			else
			{
				Int32 status = serviceModeTestQueue.pendForMsg(msgPend);
				CLASS_ASSERTION (status == Ipc::OK);
				break;
			}
		}
		//TODO E600 LL
		calInProgress = TRUE;
#endif
		////////////////////////////////////////////////////////////////////////

		// Check if this is a test or a function request
		switch (msgPend)
		{
			case TEST_REQUEST:
			// $[TI1.3]
				// set test-in-progess flag TRUE for the
				// comm-down handlers
				TestInProgress_ = TRUE;

				// perform the pending test
				runTest_(pendingTestId_);

				// reset test-in-progress and comm-died flags
				TestInProgress_ = FALSE;
				CommDiedDuringTest_ = FALSE;

				break;

			case FUNCTION_REQUEST:
			// $[TI1.4]
				// perform next function on the queue
				dequeueFunctionRequest_();
				break;

			default:
			// $[TI1.5]
				CLASS_ASSERTION_FAILURE();
		}
	}
	// end while

#ifdef SIGMA_PRODUCTION
	// Service is over, restart the system.
	Task::Delay (2);
	InitiateReboot(INTENTIONAL);

	// Assert because the reboot failed
	CLASS_ASSERTION_FAILURE();
#endif//SIGMA_PRODUCTION
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  CommsDown()
//
//@ Interface-Description
//	This static method sets a flag indicating that communication
//	went down in the middle of a test.  No argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If a test is in progress, set CommDownDuringTest_ = TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SmManager::CommsDown( void)
{
	CALL_TRACE("SmManager::CommsDown(void)");

#ifndef SM_TEST
	if ( TestInProgress_ )
	{
	// $[TI1]
		CommDiedDuringTest_ = TRUE;
	}
	// $[TI2]
	// implied else
#endif  // SM_TEST

}


#if defined(SIGMA_BD_CPU)

//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  RecvMsg()
//
//@ Interface-Description
//  This static method is used by the BD to receive a BD test or function
//	request from the GUI.  It is a callback routine registered during
//	Service-Mode initialization.  Three arguments -- message ID,
//	message packet pointer, packet size.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Determine the request type, test or function, then decode the
//	packet and invoke the local test or function handler as appropriate.
//---------------------------------------------------------------------
//@ PreCondition
//	message ID == SERVICE_MODE_MSG and data pointer is non-null and
//	packet is correct size.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SmManager::RecvMsg(XmitDataMsgId msgId, void *pData, Uint32 size)
{
	CALL_TRACE("SmManager::RecvMsg(XmitDataMsgId msgId, \
				void *pData, Uint32 size)");

	CLASS_PRE_CONDITION ((msgId == SERVICE_MODE_MSG) &&
				(pData) && (sizeof(SmMsgPacket) == size));

	SmMsgPacket *pMsgPtr = (SmMsgPacket *) pData;

	if(pMsgPtr->packetType == TEST_REQUEST)
	{
	// $[TI1]
		RSmManager.localTestRequest_(pMsgPtr->packetData.testRequested.testId,
			pMsgPtr->packetData.testRequested.testRepeat,
			pMsgPtr->packetParameters );
	}
	else
	{
	// $[TI2]
		CLASS_ASSERTION(pMsgPtr->packetType == FUNCTION_REQUEST);
		RSmManager.localFunctionRequest_(pMsgPtr->packetData.functionId);
	}
	// end else
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  RecvAction()
//
//@ Interface-Description
//  This static method is used by the BD to receive an operator action
//	for a BD test from the GUI.  It is a callback routine registered
//	during Service-Mode initialization.  Three arguments -- message ID,
//	message packet pointer, packet size.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Extract the action ID from the packet then pass the ID to the
//	local action handler.
//---------------------------------------------------------------------
//@ PreCondition
//	message ID == SERVICE_MODE_ACTION and data pointer is non-null and
//	packet is correct size.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SmManager::RecvAction(XmitDataMsgId msgId, void *pData, Uint32 size)
{
// $[TI1]
	CALL_TRACE("SmManager::RecvAction(XmitDataMsgId msgId, void *pData, Uint size)");


	CLASS_PRE_CONDITION((msgId == SERVICE_MODE_ACTION) && (pData)
						&& ( sizeof(SmActionPacket) == size ))

	SmActionPacket *pActionPtr = (SmActionPacket *) pData;

	RSmManager.localActionNotification_(pActionPtr->actionId);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestSignal
//
//@ Interface-Description
//  This method implements a state machine for test signal pulse generation.
//	It is designed to run in within the ServiceModeBackgroundCycle().
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Implement a software one-shot: once a test signal request is made,
//	turn on the test signal for a fixed time length then turn it off.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::processTestSignal(void)
{
	CALL_TRACE("SmManager::processTestSignal(void)");

	const Int32 SIGNAL_LENGTH = 50;  // msec

	// Check if it's time to turn the test signal off
	if (testSignalOn_)
	{
	// $[TI1]
		// Advance the signal on timer and turn off
		// turn off the signal if it is time to do so
		signalOnTime_ += SM_CYCLE_TIME;
		if (signalOnTime_ >= SIGNAL_LENGTH)
		{
		// $[TI1.1]
			// Turn off the signal
// E600 BDIO            testSignal_.setBit( BitAccessGpio::OFF);

#ifdef SIGMA_DEBUG
//	printf ("-");
#endif // SIGMA_DEBUG

			testSignalOn_ = FALSE;
		}
		// $[TI1.2]
		// implied else: signal not on long enough, so
		// don't turn it off yet
	}
	// $[TI2]
	// implied else: signal is not on, so no action

	// Check if a new signal has been requested
	if (testSignalRequested_)
	{
	// $[TI3]
		testSignalRequested_ = FALSE;

		if (testSignalOn_)
		{
		// $[TI3.1]
			// if signal is already on, reset the
			// signal on timer to 0
			signalOnTime_ = 0;

#ifdef SIGMA_DEBUG
//	printf ("o");
#endif // SIGMA_DEBUG

		}
		else
		{
		// $[TI3.2]
			// if signal is off, turn it on
// E600 BDIO            testSignal_.setBit( BitAccessGpio::ON);
			signalOnTime_ = 0;
			testSignalOn_ = TRUE;

#ifdef SIGMA_DEBUG
//	printf ("+");
#endif // SIGMA_DEBUG

		}
		// end else
	}
	// $[TI4]
	// implied else: no test signal requested, so no action
}

#endif  // defined(SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SmManager::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;

	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, SMMANAGER,
    	                      lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest_()
//
//@ Interface-Description
//	This method takes a ServiceModeTestId argument and performs the
//	associated test.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Send a test start message to Service-Data.
//	Perform the test associated with the ServiceModeTestId argument.
//	Send a test end message to Service-Data.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::runTest_(SmTestId::ServiceModeTestId testId)
{

	CALL_TRACE("SmManager::runTest_(SmTestId::ServiceModeTestId testId)");
	// Notify Service Data the test has started.
	sendTestStartToServiceData (testId);

	switch (testId)
	{

#if defined (SIGMA_BD_CPU)

		case SmTestId::INITIALIZE_FLOW_SENSORS_ID:
		// $[TI1]
            RCalInfoFlashBlock.buildFlowSensorData();
			break;

		case SmTestId::GAS_SUPPLY_TEST_ID:
		// $[TI2]
			RGasSupplyTest.runTest();
			break;

// ***** NOTE: TI 3 was deleted *****

		case SmTestId::CIRCUIT_RESISTANCE_TEST_ID:
		// $[TI4]
			RInspResistanceTest.runTest();
			break;

		case SmTestId::SST_FILTER_TEST_ID:
		// $[TI5]
			RSstFilterTest.runTest();
			break;

		case SmTestId::BD_LAMP_TEST_ID:
		// $[TI6]
			RBdLampTest.runTest();
			break;

		case SmTestId::BD_AUDIO_TEST_ID:
		// $[TI7]
			RBdAudioTest.runTest();
			break;

		case SmTestId::FS_CROSS_CHECK_TEST_ID:
		// $[TI9]
			RFsCrossCheckTest.estTest();
			break;

		case SmTestId::SST_FS_CC_TEST_ID:
		// $[TI10]
			RFsCrossCheckTest.sstTest();
			break;

		case SmTestId::CIRCUIT_PRESSURE_TEST_ID:
		// $[TI13]
			RCircuitPressureTest.runTest();
			break;

		case SmTestId::SAFETY_SYSTEM_TEST_ID:
		// $[TI14]
			RSafetySystemTest.runTest();
			break;

		case SmTestId::EXH_VALVE_SEAL_TEST_ID:
		// $[TI16]
			RExhValveSealTest.runTest();
			break;

		case SmTestId::EXH_VALVE_TEST_ID:
		// $[TI17]
			RExhValvePerformanceTest.runTest();
			break;

		case SmTestId::EXH_VALVE_CALIB_TEST_ID:
		// $[TI18]
			RExhValveCalibration.runTest();
			break;

		case SmTestId::FS_CALIBRATION_ID:
		// $[TI44]
			RFlowSensorCalibration.runTest() ;
			break ;

		case SmTestId::SM_LEAK_TEST_ID:
		// $[TI19]
			RLeakTest.estTest();
			break;

		case SmTestId::SST_LEAK_TEST_ID:
		// $[TI20]
			RLeakTest.sstTest();
			break;

		case SmTestId::COMPLIANCE_CALIB_TEST_ID:
		// $[TI21]
			RComplianceCalibration.runTest();
			break;

		case SmTestId::EXH_HEATER_TEST_ID:
		// $[TI22]
			RExhHeaterTest.runTest();
			break;

// ***** NOTE: TI 25 was deleted *****

		case SmTestId::GENERAL_ELECTRONICS_TEST_ID:
		// $[TI26]
			RGeneralElectronicsTest.runTest();
			break;

		case SmTestId::BATTERY_TEST_ID:
		// $[TI27]
			RBatteryTest.runTest();
			break;

		case SmTestId::SM_SST_PROX_TEST_ID:
		// $[TI27]
			RProxTest.sstTest();
			break;

		case SmTestId::BD_INIT_SERIAL_NUMBER_ID:
		// $[TI28]
			RSerialNumberInitialize.copySerialNumber();
			break;

		case SmTestId::BD_VENT_INOP_ID:
		// $[TI29]
			RForceVentInopTest.runTest();
			break;

// ***** NOTE: TI 30 was deleted *****
#ifdef SIGMA_DEVELOPMENT
		case SmTestId::BD_INIT_DEFAULT_SERIAL_NUMBER_ID:
			RSerialNumberInitialize.storeXs();
			break ;
#endif  // SIGMA_DEVELOPMENT

		case SmTestId::RESTART_BD_ID:
		// $[TI31]
			shutDownServiceMode_();
			break;

		case SmTestId::ATMOS_PRESSURE_READ_ID:
		// $[TI48]
			RAtmPresXducerCalibration.readXducer( pTestParameters_ );
			break;

		case SmTestId::SET_COMPRESSOR_SN_ID:
		// $[TI49]
// E600 BDIO             SmCompressorEeprom::GetSmCompressorEeprom().setSerialNumber( pTestParameters_ );
			break;

		case SmTestId::SET_COMPRESSOR_TIME_ID:
		// $[TI50]
// E600 BDIO             SmCompressorEeprom::GetSmCompressorEeprom().setOperationalHours( pTestParameters_ );
			break;

		case SmTestId::SET_DATAKEY_TO_VENT_DATA_ID:
		// $[TI51]
			SmDatakey::GetSmDatakey().setDatakeyData();
			break;

		case SmTestId::SET_AIR_FLOW_ID:
		// $[TI52]

			SmFlowTest::GetAirFlowTest().establishFlow( pTestParameters_ );
			break;

		case SmTestId::SET_O2_FLOW_ID:
		// $[TI53]

			SmFlowTest::GetOxygenFlowTest().establishFlow( pTestParameters_ );
			break;

		case SmTestId::PURGE_WITH_AIR_ID:
		// $[TI54]
			SmFlowTest::GetAirFlowTest().purgeCircuit();
			break;

		case SmTestId::PURGE_WITH_O2_ID:
		// $[TI55]
			SmFlowTest::GetOxygenFlowTest().purgeCircuit();
			break;

		case SmTestId::ATMOS_PRESSURE_CAL_ID:
		// $[TI56]
			RAtmPresXducerCalibration.runCalibration( pTestParameters_ );
			break;

		case SmTestId::SET_VENT_HEAD_TIME_ID:
			SmDatakey::GetSmDatakey().setVentHeadOperationalHours( pTestParameters_ );
			break;

		// All the GUI tests are not allowed.
		case SmTestId::GUI_KEYBOARD_TEST_ID:
		case SmTestId::GUI_KNOB_TEST_ID:
		case SmTestId::GUI_LAMP_TEST_ID:
		case SmTestId::GUI_AUDIO_TEST_ID:
		case SmTestId::GUI_TOUCH_TEST_ID:
		case SmTestId::GUI_SERIAL_PORT_TEST_ID:
		case SmTestId::SERIAL_LOOPBACK_TEST_ID:
		case SmTestId::GUI_INIT_SERIAL_NUMBER_ID:
		case SmTestId::GUI_VENT_INOP_ID:
		case SmTestId::GUI_NURSE_CALL_TEST_ID:

#ifdef SIGMA_DEVELOPMENT
		case SmTestId::GUI_INIT_DEFAULT_SERIAL_NUMBER_ID:
#endif  // SIGMA_DEVELOPMENT

		case SmTestId::CAL_INFO_DUPLICATION_ID:
		case SmTestId::RESTART_GUI_ID:
		case SmTestId::COMPACT_FLASH_TEST_ID:

#elif defined (SIGMA_GUI_CPU)

		case SmTestId::GUI_KEYBOARD_TEST_ID:
		// $[TI32]
			RGuiIoTest.runKeyboardTest();
			break;

		case SmTestId::GUI_KNOB_TEST_ID:
		// $[TI33]
			RGuiIoTest.runKnobTest();
			break;

		case SmTestId::GUI_LAMP_TEST_ID:
		// $[TI34]
			RGuiIoTest.runGuiLedTest();
			break;

		case SmTestId::GUI_AUDIO_TEST_ID:
		// $[TI35]
			RGuiIoTest.runGuiAudioTest();
			break;

		case SmTestId::GUI_SERIAL_PORT_TEST_ID:
		// $[TI37]
			RGuiIoTest.runSerialIoTest();
			break;

		case SmTestId::SERIAL_LOOPBACK_TEST_ID:
		// $[TI60]
			RGuiIoTest.runExternalSerialIoTest();
			break;

		case SmTestId::GUI_INIT_SERIAL_NUMBER_ID:
		// $[TI38]
			RSerialNumberInitialize.copySerialNumber();
			break;

		case SmTestId::GUI_VENT_INOP_ID:
		// $[TI39]
			RForceVentInopTest.runTest();
			break;

// ***** NOTE: TI 40 was deleted *****
#ifdef SIGMA_DEVELOPMENT
		case SmTestId::GUI_INIT_DEFAULT_SERIAL_NUMBER_ID:
			RSerialNumberInitialize.storeXs();
			break;
#endif  // SIGMA_DEVELOPMENT

		case SmTestId::CAL_INFO_DUPLICATION_ID:
		// $[TI41]
			RCalInfoDuplication.execute() ;
			break ;

		case SmTestId::RESTART_GUI_ID:
		// $[TI42]
			shutDownServiceMode_();
			break;

		case SmTestId::COMPACT_FLASH_TEST_ID:
			CompactFlashTest::GetCompactFlashTest().runTest();
			break;

		case SmTestId::GUI_NURSE_CALL_TEST_ID:
		// $[TI47]
			RGuiIoTest.runNurseCallTest();
			break;

		// All the BD tests are not allowed
		case SmTestId::INITIALIZE_FLOW_SENSORS_ID:
		case SmTestId::GAS_SUPPLY_TEST_ID:
		case SmTestId::CIRCUIT_RESISTANCE_TEST_ID:
		case SmTestId::SST_FILTER_TEST_ID:
		case SmTestId::BD_LAMP_TEST_ID:
		case SmTestId::BD_AUDIO_TEST_ID:
		case SmTestId::FS_CROSS_CHECK_TEST_ID:
		case SmTestId::SST_FS_CC_TEST_ID:
		case SmTestId::CIRCUIT_PRESSURE_TEST_ID:
		case SmTestId::SAFETY_SYSTEM_TEST_ID:
		case SmTestId::EXH_VALVE_SEAL_TEST_ID:
		case SmTestId::EXH_VALVE_TEST_ID:
		case SmTestId::EXH_VALVE_CALIB_TEST_ID:
		case SmTestId::FS_CALIBRATION_ID:
		case SmTestId::SM_LEAK_TEST_ID:
		case SmTestId::SST_LEAK_TEST_ID:
		case SmTestId::COMPLIANCE_CALIB_TEST_ID:
		case SmTestId::EXH_HEATER_TEST_ID:
		case SmTestId::GENERAL_ELECTRONICS_TEST_ID:
		case SmTestId::BATTERY_TEST_ID:
		case SmTestId::BD_INIT_SERIAL_NUMBER_ID:
		case SmTestId::BD_VENT_INOP_ID:

#ifdef SIGMA_DEVELOPMENT
		case SmTestId::BD_INIT_DEFAULT_SERIAL_NUMBER_ID:
#endif // SIGMA_DEVELOPMENT

		case SmTestId::RESTART_BD_ID:
		case SmTestId::ATMOS_PRESSURE_READ_ID:
		case SmTestId::SET_COMPRESSOR_SN_ID:
		case SmTestId::SET_COMPRESSOR_TIME_ID:
		case SmTestId::SET_DATAKEY_TO_VENT_DATA_ID:
		case SmTestId::SET_AIR_FLOW_ID:
		case SmTestId::SET_O2_FLOW_ID:
		case SmTestId::PURGE_WITH_AIR_ID:
		case SmTestId::PURGE_WITH_O2_ID:
		case SmTestId::ATMOS_PRESSURE_CAL_ID:
		case SmTestId::SET_VENT_HEAD_TIME_ID:
		case SmTestId::SM_SST_PROX_TEST_ID:

#endif  // defined(SIGMA_BD_CPU)

		case SmTestId::TEST_NOT_START_ID:
		case SmTestId::NUM_SERVICE_TEST_ID:
		default:
		// $[TI43]
		    CLASS_ASSERTION_FAILURE();
	}

	// Notify Service Data the test has ended.
	sendTestEndToServiceData(testId);

#ifdef SIGMA_BD_CPU
    switch ( testId )
    {
		case SmTestId::SET_COMPRESSOR_SN_ID:
		// $[TI57]
            //  update the GUI data by sending a BdReadyMessage
            //  that contains the compressor serial number
            CommTaskAgent::NetworkUp();
            break;
        default:
		// $[TI58]
            break;
    }
#endif // SIGMA_BD_CPU
}


#if defined (SIGMA_GUI_CPU) || defined (SIGMA_UNIT_TEST) || defined (SM_TEST)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: remoteTestRequest_()
//
//@ Interface-Description
//	This method sends a BD test request from the GUI to the BD via
//	the network link.  Two arguments: a ServiceModeTestId and a
//	Boolean testRepeat flag.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Build the test request packet then send it to the BD via the network.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::remoteTestRequest_(SmTestId::ServiceModeTestId testId,
	Boolean testRepeat, const char * const pParameters )
{
	CALL_TRACE("SmManager::remoteTestRequest_(\
	SmTestId::ServiceModeTestId testId,\
	Boolean testRepeat, const char * const)");
	SmMsgPacket testPacket;

	testPacket.packetType = TEST_REQUEST;
	testPacket.packetData.testRequested.testId = testId;
	testPacket.packetData.testRequested.testRepeat = testRepeat;
    testPacket.packetParameters[0] = '\0';
    if ( pParameters )
    {
	// $[TI1]
	    strncpy(  testPacket.packetParameters
                , pParameters
                , sizeof(testPacket.packetParameters) );
        testPacket.packetParameters[ sizeof(testPacket.packetParameters)-1 ] = '\0';
    }
	// $[TI2]
    // implied else
	NetworkApp::XmitMsg(SERVICE_MODE_MSG, (void *) &testPacket, sizeof(testPacket));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: remoteFunctionRequest_()
//
//@ Interface-Description
//	This method sends a BD function request from the GUI to the BD via
//	the network link.  Takes a FunctionId argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Build the function request packet then send it to the BD via the network.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

void
SmManager::remoteFunctionRequest_(SmTestId::FunctionId functionId)
{
// $[TI1]
	CALL_TRACE("SmManager::remoteFunctionRequest_(SmTestId::FunctionId functionId)");

	SmMsgPacket functionPacket;

	functionPacket.packetType = FUNCTION_REQUEST;
	functionPacket.packetData.functionId = functionId;

	NetworkApp::XmitMsg(SERVICE_MODE_MSG, (void *) &functionPacket, sizeof(functionPacket));
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: remoteActionNotification_()
//
//@ Interface-Description
//	This method sends a operator action from the GUI to the BD via
//	the network link.  Takes an ActionId argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

void
SmManager::remoteActionNotification_(SmPromptId::ActionId actionId)
{
// $[TI1]
	CALL_TRACE("remoteActionNotification_(SmPromptId::\
				ActionId actionId)");

	SmActionPacket actionPacket;

	actionPacket.actionId = actionId;

	NetworkApp::XmitMsg(SERVICE_MODE_ACTION, (void *) &actionPacket, sizeof(actionPacket));
}

#endif  // defined(SIGMA_GUI_CPU) || defined (SIGMA_UNIT_TEST) || defined (SM_TEST)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: localActionNotification_()
//
//@ Interface-Description
//	This method puts an operator action message on the SERVICE_MODE_ACTION_Q,
//	which is received and processed in a separate task.  Takes an
//	ActionId argument, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Put an operator action message on the SERVICE_MODE_ACTION_Q.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::localActionNotification_(SmPromptId::ActionId actionId)
{
// $[TI1]
	CALL_TRACE("localActionNotification_(SmPromptId::ActionId \
													actionId)");

	CLASS_ASSERTION(MsgQueue::PutMsg(::SERVICE_MODE_ACTION_Q,
									(Int32) actionId) == Ipc::OK);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: dequeueFunctionRequest_()
//
//@ Interface-Description
//	This method performs the next function stored in the function buffer.
//	A buffer is needed because a new function may be received while
//	a function is still being performed.  No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If buffer is not empty, get the next function ID and perform the
//	appropriate function via localFunction_().
//---------------------------------------------------------------------
//@ PreCondition
//	function buffer is not empty.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::dequeueFunctionRequest_(void)
{
	CALL_TRACE("SmManager::dequeueFunctionRequest_(void)");

	// Check for an empty queue
	CLASS_ASSERTION (functionRequestStart_ != functionRequestEnd_);

	Int32 nextRequest = functionRequestStart_ + 1;

	if (nextRequest >= FUNCTION_REQUEST_MAX)
	{
	// $[TI1]
		nextRequest = 0;
	}
	// $[TI2]
	// implied else: don't roll over index yet

	SmTestId::FunctionId functionId =
		functionRequest_ [functionRequestStart_];

	functionRequestStart_ = nextRequest;

	RSmManager.localFunction_ (functionId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: localFunction_()
//
//@ Interface-Description
//	This method performs the function associated with the FunctionId
//	argument.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Peform the function associated with the FunctionId argument.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::localFunction_(SmTestId::FunctionId functionId)
{
	CALL_TRACE("SmManager::localFunction_(SmTestId::FunctionId functionId)");

#ifdef SIGMA_BD_CPU
	BdSettingValues acceptedSettingValues;
	FlowSensorOffset defaultFsOffset;
	AtmPressOffset atmPressOffset;
	Resistance defaultRes;
#endif  // SIGMA_BD_CPU

	switch (functionId)
	{
// ***** NOTE: TI 1 was deleted *****

		case SmTestId::SET_TEST_TYPE_EST_ID:
		// $[TI2]
			ServiceDataMgr::SetTestType(SmTestId::SM_EST_TYPE);
			testType_ = SmTestId::SM_EST_TYPE;
			break;

		case SmTestId::SET_TEST_TYPE_SST_ID:
		// $[TI3]
			ServiceDataMgr::SetTestType(SmTestId::SM_SST_TYPE);
			testType_ = SmTestId::SM_SST_TYPE;
			break;

		case SmTestId::SET_TEST_TYPE_REMOTE_ID:
		// $[TI4]
			ServiceDataMgr::SetTestType(SmTestId::SM_EXTERNAL_TYPE);
			testType_ = SmTestId::SM_EXTERNAL_TYPE;
			break;

		case SmTestId::SET_TEST_TYPE_MISC_ID:
		// $[TI5]
			ServiceDataMgr::SetTestType(SmTestId::SM_MISC_TYPE);
			testType_ = SmTestId::SM_MISC_TYPE;
			break;

		case SmTestId::EST_OVERRIDE_ID:
		// $[TI6]
			NovRamManager::EstOverridden();
#ifdef SIGMA_UNIT_TEST
			SmManager_UT::EstOverridden = TRUE;
#endif
            Post::ClearStrikesLog();
			break;

		case SmTestId::SST_OVERRIDE_ID:
		// $[TI7]
			NovRamManager::SstOverridden();
#ifdef SIGMA_UNIT_TEST
			SmManager_UT::SstOverridden = TRUE;
#endif
			break;

		case SmTestId::EST_COMPLETE_ID:
		// $[TI8]
			NovRamManager::CompletedEst();
#if defined(SIGMA_GUI_CPU)
		    EstResultTable::EnableSingleTestLog();
#endif
#ifdef SIGMA_UNIT_TEST
			SmManager_UT::CompletedEst = TRUE;
#endif
            Post::ClearStrikesLog();
			break;

		case SmTestId::SST_COMPLETE_ID:
		// $[TI9]
			NovRamManager::CompletedSst();
#ifdef SIGMA_UNIT_TEST
			SmManager_UT::CompletedSst = TRUE;
#endif

#ifdef SIGMA_BD_CPU
			NovRamManager::GetAcceptedBatchSettings( acceptedSettingValues);

			// Update the last circuit type
			NovRamManager::UpdateLastSstPatientCctType(
				acceptedSettingValues.getDiscreteValue(SettingId::PATIENT_CCT_TYPE));

#endif  // SIGMA_BD_CPU

			break;

		case SmTestId::BD_DOWNLOAD_ID:
		// $[TI10]
#if defined(SIGMA_BD_CPU)
			Post::SetDownloadScheduled(TRUE);
#endif  // defined (SIGMA_BD_CPU)
			break;

		case SmTestId::GUI_DOWNLOAD_ID:
		// $[TI11]
#if defined(SIGMA_GUI_CPU)
			Post::SetDownloadScheduled(TRUE);
#endif  // defined(SIGMA_GUI_CPU)
			break;

		case SmTestId::FORCE_EST_OVERIDE_ID:
		// $[TI12]
			NovRamManager::OverrideAllEstTests();
			Post::ClearStrikesLog();
#if defined(SIGMA_GUI_CPU)
		    EstResultTable::EnableSingleTestLog();
#endif
#  ifdef SIGMA_UNIT_TEST
			SmManager_UT::ForceEstOverride = TRUE;
#  endif
			break;

		case SmTestId::RESET_SOFTWARE_OPTIONS:
		// $[TIxx]
			{
				UserOptions  userOptions;

				NovRamManager::GetUserOptions(userOptions);
				userOptions.resetOptionBits();
				NovRamManager::StoreUserOptions(userOptions);
#  ifdef SIGMA_UNIT_TEST
				SmManager_UT::ResetSoftwareOptions_ = TRUE;
#  endif
			}
			break;

		case SmTestId::FORCE_SST_PED_OVERIDE_ID:
		case SmTestId::FORCE_SST_ADULT_OVERIDE_ID:
		case SmTestId::FORCE_SST_NEO_OVERIDE_ID:
		// $[TI13]
			{
				NovRamManager::OverrideAllSstTests();

				DiscreteValue  requestedCctType = PatientCctTypeValue::TOTAL_CIRCUIT_TYPES;

				switch (functionId)
				{
					case SmTestId::FORCE_SST_ADULT_OVERIDE_ID:
				        // $[TI18]
						requestedCctType = PatientCctTypeValue::ADULT_CIRCUIT;
						break;

					case SmTestId::FORCE_SST_PED_OVERIDE_ID:
				        // $[TI19]
						requestedCctType = PatientCctTypeValue::PEDIATRIC_CIRCUIT;
						break;

					case SmTestId::FORCE_SST_NEO_OVERIDE_ID:
				        // $[TI20]
						requestedCctType = PatientCctTypeValue::NEONATAL_CIRCUIT;
						break;

					default:
						// unexpected functionId...
						AUX_CLASS_ASSERTION_FAILURE(functionId);
						break;
				}

#ifdef SIGMA_GUI_CPU
				NovRamManager::UpdateLastSstHumidifierType(HumidTypeValue::HME_HUMIDIFIER);

				SettingContextHandle::AdjustSstBatch();

				SettingSubject*  pSettingSubject;

				pSettingSubject =
					SettingsMgr::GetSettingSubjectPtr(SettingId::PATIENT_CCT_TYPE);

				while (DiscreteValue(pSettingSubject->getAdjustedValue()) !=
															   requestedCctType)
				{
			        // $[TI22]
					// go to the next circuit type value...
					pSettingSubject->calcNewValue(1);
				}  // $[TI23]

				SettingContextHandle::AcceptSstBatch();
#endif // SIGMA_GUI_CPU

#ifdef SIGMA_BD_CPU

				Task::Delay(12);
                                NovRamManager::UpdateLastSstProxFirmwareRev( RProxManager.getFirmwareRev() );
				NovRamManager::UpdateLastSstProxSerialNum( RProxManager.getBoardSerialNumber() );

				NovRamManager::UpdateLastSstPatientCctType(requestedCctType);

				CircuitCompliance  circuitCompliance;
				Resistance         inspResistance;
				Resistance         exhResistance;

				// set resistance characterization flow limit
				switch (requestedCctType)
				{
					case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
					// $[TI13.3]
						circuitCompliance.setLowCompliance(::PED_CCT_COMPLIANCE_.lowCompliance);
						circuitCompliance.setHighCompliance(::PED_CCT_COMPLIANCE_.highCompliance);
						circuitCompliance.setLowFlowTime(::PED_CCT_COMPLIANCE_.lowFlowTimeMs);
						circuitCompliance.setHighFlowTime(::PED_CCT_COMPLIANCE_.highFlowTimeMs);
						circuitCompliance.setLowFlowAdiabaticFactor(::PED_CCT_COMPLIANCE_.lowFlowAdiabaticFactor);
						circuitCompliance.setHighFlowAdiabaticFactor(::PED_CCT_COMPLIANCE_.highFlowAdiabaticFactor);
						inspResistance.setSlope(::PED_CCT_INSP_RESISTANCE_.slope);
						inspResistance.setOffset(::PED_CCT_INSP_RESISTANCE_.offset);
						exhResistance.setSlope(::PED_CCT_EXH_RESISTANCE_.slope);
						exhResistance.setOffset(::PED_CCT_EXH_RESISTANCE_.offset);
						break;

					case PatientCctTypeValue::ADULT_CIRCUIT :
					// $[TI13.4]
						circuitCompliance.setLowCompliance(::ADULT_CCT_COMPLIANCE_.lowCompliance);
						circuitCompliance.setHighCompliance(::ADULT_CCT_COMPLIANCE_.highCompliance);
						circuitCompliance.setLowFlowTime(::ADULT_CCT_COMPLIANCE_.lowFlowTimeMs);
						circuitCompliance.setHighFlowTime(::ADULT_CCT_COMPLIANCE_.highFlowTimeMs);
						circuitCompliance.setLowFlowAdiabaticFactor(::ADULT_CCT_COMPLIANCE_.lowFlowAdiabaticFactor);
						circuitCompliance.setHighFlowAdiabaticFactor(::ADULT_CCT_COMPLIANCE_.highFlowAdiabaticFactor);
						inspResistance.setSlope(::ADULT_CCT_INSP_RESISTANCE_.slope);
						inspResistance.setOffset(::ADULT_CCT_INSP_RESISTANCE_.offset);
						exhResistance.setSlope(::ADULT_CCT_EXH_RESISTANCE_.slope);
						exhResistance.setOffset(::ADULT_CCT_EXH_RESISTANCE_.offset);
						break;

					case PatientCctTypeValue::NEONATAL_CIRCUIT :
					// $[TI13.5]
						circuitCompliance.setLowCompliance(::NEO_CCT_COMPLIANCE_.lowCompliance);
						circuitCompliance.setHighCompliance(::NEO_CCT_COMPLIANCE_.highCompliance);
						circuitCompliance.setLowFlowTime(::NEO_CCT_COMPLIANCE_.lowFlowTimeMs);
						circuitCompliance.setHighFlowTime(::NEO_CCT_COMPLIANCE_.highFlowTimeMs);
						circuitCompliance.setLowFlowAdiabaticFactor(::NEO_CCT_COMPLIANCE_.lowFlowAdiabaticFactor);
						circuitCompliance.setHighFlowAdiabaticFactor(::NEO_CCT_COMPLIANCE_.highFlowAdiabaticFactor);
						inspResistance.setSlope(::NEO_CCT_INSP_RESISTANCE_.slope);
						inspResistance.setOffset(::NEO_CCT_INSP_RESISTANCE_.offset);
						exhResistance.setSlope(::NEO_CCT_EXH_RESISTANCE_.slope);
						exhResistance.setOffset(::NEO_CCT_EXH_RESISTANCE_.offset);
						break;

					default :
						// unexpected circuit type value...
						AUX_CLASS_ASSERTION_FAILURE(requestedCctType);
						break;
				}

				inspResistance.setResistanceValid(TRUE);
				NovRamManager::UpdateInspResistance(inspResistance);

				exhResistance.setResistanceValid(TRUE);
				NovRamManager::UpdateExhResistance(exhResistance);

				NovRamManager::UpdateCircuitComplianceTable(circuitCompliance);
#endif  // SIGMA_BD_CPU

#  ifdef SIGMA_UNIT_TEST
				SmManager_UT::ForceSstOverride = TRUE;
#  endif
			}
			break;

		case SmTestId::ENABLE_BILEVEL_OPTION :
		// $[TI101]
			SoftwareOptions::EnableOption(SoftwareOptions::BILEVEL);
			break;
		case SmTestId::ENABLE_ATC_OPTION :
		// $[TI102]
			SoftwareOptions::EnableOption(SoftwareOptions::ATC);
			break;
		case SmTestId::ENABLE_NEO_MODE_OPTION :
		// $[TI103]
			SoftwareOptions::EnableOption(SoftwareOptions::NEO_MODE);
			break;
		case SmTestId::ENABLE_RESERVED1_OPTION :
		// $[TI107]
			SoftwareOptions::EnableOption(SoftwareOptions::RESERVED1);
			break;
		case SmTestId::ENABLE_VTPC_OPTION :
		// $[TI107]
			SoftwareOptions::EnableOption(SoftwareOptions::VTPC);
			break;
		case SmTestId::ENABLE_PAV_OPTION :
		// $[TI108]
			SoftwareOptions::EnableOption(SoftwareOptions::PAV);
			break;
		case SmTestId::ENABLE_RESP_MECH_OPTION :
			SoftwareOptions::EnableOption(SoftwareOptions::RESP_MECH);
			break;
		case SmTestId::ENABLE_TRENDING_OPTION :
			SoftwareOptions::EnableOption(SoftwareOptions::TRENDING);
			break;
		case SmTestId::ENABLE_LEAK_COMP_OPTION :
			SoftwareOptions::EnableOption(SoftwareOptions::LEAK_COMP);
			break;
		case SmTestId::ENABLE_NEO_MODE_UPDATE_OPTION :
			SoftwareOptions::EnableOption(SoftwareOptions::NEO_MODE_UPDATE);
			break;
		case SmTestId::ENABLE_NEO_MODE_ADVANCED_OPTION :
			SoftwareOptions::EnableOption(SoftwareOptions::NEO_MODE);
			SoftwareOptions::EnableOption(SoftwareOptions::NEO_MODE_UPDATE);
			SoftwareOptions::EnableOption(SoftwareOptions::NEO_MODE_ADVANCED);
			break;
        case SmTestId::ENABLE_NEO_MODE_LOCKOUT_OPTION :
			SoftwareOptions::EnableOption(SoftwareOptions::NEO_MODE);
			SoftwareOptions::EnableOption(SoftwareOptions::NEO_MODE_UPDATE);
			SoftwareOptions::EnableOption(SoftwareOptions::NEO_MODE_ADVANCED);
			SoftwareOptions::EnableOption(SoftwareOptions::NEO_MODE_LOCKOUT);
			break;

		case SmTestId::DISABLE_BILEVEL_OPTION :
		// $[TI104]
			SoftwareOptions::DisableOption(SoftwareOptions::BILEVEL);
			break;
		case SmTestId::DISABLE_ATC_OPTION :
		// $[TI105]
			SoftwareOptions::DisableOption(SoftwareOptions::ATC);
			break;
		case SmTestId::DISABLE_NEO_MODE_OPTION :
		// $[TI106]
			SoftwareOptions::DisableOption(SoftwareOptions::NEO_MODE);
            SoftwareOptions::DisableOption(SoftwareOptions::NEO_MODE_ADVANCED);
			SoftwareOptions::DisableOption(SoftwareOptions::NEO_MODE_LOCKOUT);
			break;
		case SmTestId::DISABLE_RESERVED1_OPTION :
		// $[TI108]
			SoftwareOptions::DisableOption(SoftwareOptions::RESERVED1);
			break;
		case SmTestId::DISABLE_VTPC_OPTION :
		// $[TI109]
			SoftwareOptions::DisableOption(SoftwareOptions::VTPC);
			break;
		case SmTestId::DISABLE_PAV_OPTION :
		// $[TI110]
			SoftwareOptions::DisableOption(SoftwareOptions::PAV);
			break;
		case SmTestId::DISABLE_RESP_MECH_OPTION :
			SoftwareOptions::DisableOption(SoftwareOptions::RESP_MECH);
			break;
		case SmTestId::DISABLE_TRENDING_OPTION :
			SoftwareOptions::DisableOption(SoftwareOptions::TRENDING);
			break;
		case SmTestId::DISABLE_LEAK_COMP_OPTION :
			SoftwareOptions::DisableOption(SoftwareOptions::LEAK_COMP);
			break;
		case SmTestId::DISABLE_NEO_MODE_UPDATE_OPTION :
			SoftwareOptions::DisableOption(SoftwareOptions::NEO_MODE_UPDATE);
			break;
		case SmTestId::DISABLE_NEO_MODE_ADVANCED_OPTION :
			SoftwareOptions::DisableOption(SoftwareOptions::NEO_MODE_ADVANCED);
			break;
		case SmTestId::DISABLE_NEO_MODE_LOCKOUT_OPTION :
			SoftwareOptions::DisableOption(SoftwareOptions::NEO_MODE_LOCKOUT);
			break;

		case SmTestId::DEFAULT_FLASH_ID:
		// $[TI100]
			// set default EV cal table
			RCalInfoDuplication.useDefaultEvTable() ;
#ifdef SIGMA_BD_CPU
			// set default flow sensor cal tables
	    	NovRamManager::UpdateAirFlowSensorOffset( defaultFsOffset) ;
	    	NovRamManager::UpdateO2FlowSensorOffset( defaultFsOffset) ;
			// set flow sensor cal status to "passed"
			ServiceModeNovramData::SetFlowSensorCalStatus(
												ServiceMode::PASSED);

			// set atm pressure xducer offset to 0 in novram and RAM
			atmPressOffset.setAtmPressOffset(0);
			NovRamManager::UpdateAtmPressOffset(atmPressOffset);
			RAtmosphericPressureSensor.setOffset(0);
			// set atm pressure xducer cal status to "passed"
			ServiceModeNovramData::SetAtmPresXducerCalStatus(
												ServiceMode::PASSED);

			// reset the vent inop test state
			RForceVentInopTest.reset();
#endif  // SIGMA_BD_CPU

			break;

		case SmTestId::DELETE_SYSTEM_DIAGNOSTIC_LOG_ID:
        // $[TI15]
            NovRamManager::ClearSystemDiagnostic();
            break;

		case SmTestId::DELETE_SYSTEM_INFORMATION_LOG_ID:
        // $[TI16]
            NovRamManager::ClearSystemInformation();
            break;

		case SmTestId::DELETE_SELFTEST_LOG_ID:
        // $[TI17]
            NovRamManager::ClearEstSstDiagnostic();
            break;

        case SmTestId::MFG_PROD_KEY_CONVERT_ID:
        //$[TI18]
#if defined(SIGMA_BD_CPU)
		    SmDatakey::GetSmDatakey().setMfgDatakeyToProdDatakey();
#endif  // defined (SIGMA_BD_CPU)
            break;
		case SmTestId::FORCE_EST_UNDEFINED_ID:
			NovRamManager::SetAllTestsUndefined();
			break;
		case SmTestId::ENABLE_SINGLE_TEST_ID:
			isSingleTestMode_ = TRUE;
			break;
		case SmTestId::DISABLE_SINGLE_TEST_ID:
			isSingleTestMode_ = FALSE;
			break;

		case SmTestId::FUNCTION_ID_START:
		case SmTestId::FUNCTION_ID_MAX:
		case SmTestId::EST_PASSED_ID:  // deleted function
		default:
			AUX_CLASS_ASSERTION_FAILURE(functionId);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: localTestRequest_()
//
//@ Interface-Description
//	This method places a test request message on the SERVICE_MODE_TEST_Q,
//	which is received and processed in a separate task.  Takes a
//	ServiceModeTestId and Boolean testRepeat arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set data members pendingTestId_ and testRepeat_ from the respective
//	arguments, then put the test request message on the SERVICE_MODE_TEST_Q.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	test request was successfully placed on the SERVICE_MODE_TEST_Q
//@ End-Method
//=====================================================================

void
SmManager::localTestRequest_(SmTestId::ServiceModeTestId testId,
			Boolean testRepeat, const char * const pParameters )
{
	CALL_TRACE("SmManager::localTestRequest_(\
			SmTestId::ServiceModeTestId testId,\
			Boolean testRepeat, const char * const)");

	pendingTestId_ = testId;
	testRepeat_ = testRepeat;

    pTestParameters_[0] = '\0';
    if ( pParameters )
    {
	// $[TI1]
        strncpy( pTestParameters_, pParameters, sizeof(pTestParameters_) );
        pTestParameters_[ sizeof(pTestParameters_)-1 ] = '\0';
    }
	// $[TI2]
    // implied else

	CLASS_ASSERTION(MsgQueue::PutMsg(::SERVICE_MODE_TEST_Q,
				(Int32) TEST_REQUEST) == Ipc::OK);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: localFunctionRequest_()
//
//@ Interface-Description
//	This method places a function request message on the SERVICE_MODE_TEST_Q,
//	which is received and processed in a separate task.  Takes a
//	ServiceModeTestId and Boolean testRepeat arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Put function ID argument into the next function ID buffer position,
//	update the buffer pointers.
//	Put the function request message on the SERVICE_MODE_TEST_Q.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	function request was successfully placed on the SERVICE_MODE_TEST_Q
//@ End-Method
//=====================================================================

void
SmManager::localFunctionRequest_(SmTestId::FunctionId functionId)
{
	CALL_TRACE("SmManager::localFunctionRequest_(SmTestId::FunctionId functionId)");


	// calculate the next end-of-buffer pointer value, roll it
	// to zero if it exceeds the max value
	Int32 nextRequest = functionRequestEnd_ + 1;
	if (nextRequest >= FUNCTION_REQUEST_MAX)
	{
	// $[TI1]
		nextRequest = 0;
	}
	// $[TI2]
	// implied else

	// Check for buffer overflow
	CLASS_ASSERTION(nextRequest != functionRequestStart_);

	// put functionId into the next open slot
	functionRequest_[functionRequestEnd_] = functionId;

	// move the end-of-buffer pointer
	functionRequestEnd_ = nextRequest;

	// send the function request for the stored functionId
	CLASS_ASSERTION(MsgQueue::PutMsg(::SERVICE_MODE_TEST_Q,
				(Int32) FUNCTION_REQUEST) == Ipc::OK);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupServiceMode_ ()
//
//@ Interface-Description
//	This method initializes system parameters that can't be initialized
//	until it is known that the system is in the Service or SST state.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set flow and pressure sensor alpha filter constants to the
//	Service Mode value.
//	Close the safety valve.
//	Setup the exh flow sensor O2% and dry factor to the Service Mode
//	required default values.
//	Point the flow sensor and exh valve objects to the FLASH image
//	for calibration data.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmManager::setupServiceMode_(void)
{
// $[TI1]
	CALL_TRACE("SmManager::setupServiceMode_(void)");

#if defined(SIGMA_BD_CPU)
	// Slow down the filters to remove noise
	RAirFlowSensor.setAlpha(SERVICE_MODE_ALPHA);
	RO2FlowSensor.setAlpha(SERVICE_MODE_ALPHA);
	RExhFlowSensor.setAlpha(SERVICE_MODE_ALPHA);
// E600 BDIO    RExhO2FlowSensor.setAlpha(SERVICE_MODE_ALPHA);
// E600 BDIO	RExhFlowSensor.setUnoffsettedProcessedValuesAlpha(SERVICE_MODE_ALPHA);
// E600 BDIO    RExhO2FlowSensor.setUnoffsettedProcessedValuesAlpha(SERVICE_MODE_ALPHA);
	RInspPressureSensor.setAlpha(SERVICE_MODE_ALPHA);
	RExhPressureSensor.setAlpha(SERVICE_MODE_ALPHA);

	RSmUtilities.closeSafetyValve();

	// Setup the exhalation flow sensor
    RExhFlowSensor.setO2Percent(21.0f);
// E600 BDIO    RExhFlowSensor.setDryFactor(1.0f);

	// Setup the exhalation O2 flow sensor
// E600 BDIO    RExhO2FlowSensor.setO2Percent(100.0f);
// E600 BDIO    RExhO2FlowSensor.setDryFactor(1.0f);

	// ALWAYS point to flash image in RAM while in Service Mode or SST!!
	RAirFlowSensor.setPFlowSensorCalInfo (
		&RCalInfoFlashBlock.FlashImage.airCalInfo);
	RO2FlowSensor.setPFlowSensorCalInfo (
		&RCalInfoFlashBlock.FlashImage.o2CalInfo);

// E600 BDIO	RExhFlowSensor.setPFlowSensorCalInfo (
// E600 BDIO		&RCalInfoFlashBlock.FlashImage.exhCalInfo);
// E600 BDIO	RExhFlowSensor.setPSecondaryFlowSensorCalInfo (
// E600 BDIO		&RCalInfoFlashBlock.FlashImage.exhO2CalInfo);

// E600 BDIO    RExhO2FlowSensor.setPFlowSensorCalInfo (
// E600 BDIO        &RCalInfoFlashBlock.FlashImage.exhO2CalInfo);
// E600 BDIO    RExhO2FlowSensor.setPSecondaryFlowSensorCalInfo (
// E600 BDIO        &RCalInfoFlashBlock.FlashImage.exhO2CalInfo);

	RExhalationValve.setPExhValveTable (
		&RCalInfoFlashBlock.FlashImage.exhValveCalInfo);

#endif  // defined(SIGMA_BD_CPU)

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: shutDownServiceMode_ ()
//
//@ Interface-Description
//	This method is used to signal service mode complete so that system
//	can reboot itself.  No arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the service complete flag TRUE.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
SmManager::shutDownServiceMode_(void)
{
// $[TI1]
	CALL_TRACE("SmManager::shutDownServiceMode_(void)");

	ServiceMode::SetServiceComplete(TRUE);

#ifdef SIGMA_DEBUG
	printf ("Stopping Service Mode.\n");
#endif // SIGMA_DEBUG

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: delayForRemoteTests_()
//
//@ Interface-Description
//	This method is used to insert a delay when sending status or
//	data messages during remote (External) tests. No arguments, no
//	return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If test type is External then delay, else don't delay.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
SmManager::delayForRemoteTests_(void)
{
	CALL_TRACE("SmManager::delayForRemoteTests_(void)");

	if (testType_ == SmTestId::SM_EXTERNAL_TYPE)
	{
	// $[TI1]
		Task::Delay( 0, 300);
	}
	// $[TI2]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsSingleTestMode
//
//@ Interface-Description
//   This method takes no parameters and returns a static data member
//   isSingleTestMode_.
//---------------------------------------------------------------------
//@ Implementation-Description
//   The value of the static data member isSingleTestMode_ is returned.
//---------------------------------------------------------------------
//@ PreCondition
//   none.
//---------------------------------------------------------------------
//@ PostCondition
//   none.
//@ End-Method
//=====================================================================
Boolean
	SmManager::IsSingleTestMode(void)
{   CALL_TRACE("SmManager::IsSingleTestMode()");

	return(isSingleTestMode_);
}

