#ifndef TextArea_HH
#define TextArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TextArea - Implements an enclosed scrollable area that allows
// viewing of a body of text.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TextArea.hhv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  14-JUL-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Sigma.hh"

//@ Usage-Classes
#include "Box.hh"
#include "Container.hh"
#include "TextField.hh"

//@ End-Usage

//@ Type: TextAreaLine
// A structure used to store a line of text within a TextArea as well as the
// Y position at which it will be displayed.
struct TextAreaLine
{
	Uint16 yPosition;
	const wchar_t* P_TEXT;    
};


//@ Type: pageInfo_
// A structure used to store information on a page of text including first
// line's index into the TextAreaLine array and the number of lines in the
// page.
struct pageInfo_
{
	Uint16 firstLineIndex;
	Uint16 numberOfLines;
};


class TextArea : public Container
{
public:
	TextArea(Uint16 width, Uint16 height);
	~TextArea(void);

	// Method to set and process the TextAreaLine array.  Necessary before
	// text display and paging can begin.
	void setLines(TextAreaLine *pTextLines);

	// Called to scroll (page) text.  
	void scrollHappened(Uint16 direction);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	// Inline functions to query if there is more text to page to.
	inline Boolean nextPage();
	inline Boolean prevPage();


	enum SCROLLING_
	{
		//@ Constant: SCROLL_FORWARD
		// Value indicating the forward scrolling
		SCROLL_FORWARD,

		//@ Constant: SCROLL_BACKWARD
		// Value indicating the forward scrolling.
		SCROLL_BACKWARD
	};


protected:

private:
	// these methods are purposely declared, but not implemented...
	TextArea(void);						// not implemented...
	TextArea(const TextArea&);			// not implemented...
	void operator=(const TextArea&);	// not implemented...

	// This method process text lines before they are displayed.
	void processLines_(void);

	enum
	{
		//@ Constant: MAX_TEXT_LINE_FIELDS_
		// The maximum number of lines that can be displayed onscreen by a
		// TextArea object.
		MAX_TEXT_LINE_FIELDS_ = 30,

		//@ Constant: TOP_OF_HELP_PAGE_Y_
		// The Y pixel coordinate of the first line on a help page, unless it
		// has not been positioned yet.
		TOP_OF_HELP_PAGE_Y_ = 1
	};

	enum
	{
		//@ Constant: MAX_PAGES_
		// The maximum number of pages per menu topics. 
		MAX_PAGES_ = 20
	};

	//@ Data-Member: pTextLines_
	// Pointer to the array of text lines that is to be displayed in this
	// text area.
	TextAreaLine* pTextLines_;
	
	//@ Data-Member: textLineFields_
	// An array of TextField objects used to display lines of text on screen.
	TextField textLineFields_[MAX_TEXT_LINE_FIELDS_];

	//@ Data-Member: numTextLines_
	// The number of text lines pointed to by pTextLines_.
	Uint16 numTextLines_;

	//@ Data-Member: numPages_
	// Number of pages in the current help topics
	Uint16 numPages_;

	//@ Data-Member: currentPage_
	// Current page
	int currentPage_;

	//@ Data-member: textheight_
	// the text area height.
	Uint16 textHeight_;

	//@ Data-Member: pageInfoArray_
	// Array of pageInfo_ structs storing info for each page of a help topic
	pageInfo_ pageInfoArray_[MAX_PAGES_];

	//@ Constant: LINE_SPACING_
	// The number of pixels that should be between each line of the area.
	static const Int32 LINE_SPACING_;
};

// Inlined methods
#include "TextArea.in"

#endif // TextArea_HH 
