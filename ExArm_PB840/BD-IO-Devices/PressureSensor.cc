#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureSensor - Class for pressure measuring sensors that
//      incorporate a calibrated zero offset.  NOTE: The atmospheric pressure
//      sensor is a linear sensor because the zero point is fixed.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class, derived from the Sensor class, is responsible for
//      providing pressure measurements, using raw ADC count.  The
//		PressureXducerAutozero class is a friend of PressureSensor class.
//		Methods are implemented to convert the raw counts to pressure and
//		update the filtered, slope, and filtered slope values; to return the
//		filtered value; to return the slope value; to return the filtered
//		slope value; to initialize the filtered, slope, filteredSlope, and
//		previous values; to redefine the alpha of the filter; and to set the
//		zero point offset value.
//---------------------------------------------------------------------
//@ Rationale
//      Pressure sensors are used by the BD system to monitor
//      patient data and to provide feedback measurements for
//      pressure and exhalation controllers.
//---------------------------------------------------------------------
//@ Implementation-Description
//      All the calculations for the conversion from ADC count to
//      pressure reading are done using the method rawToEngValue_().
//      The class ProcessedValues handles further processing of
//      the pressure reading as required.  The zero offset is stored
//      as a data member to provide compensation for zero drift.
//
// $[00401]
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/PressureSensor.ccv   25.0.4.0   19 Nov 2013 13:54:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003 By: syw    Date: 20-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added SafetyNetSensorData * to constructor arguments.  Modified
//			updateValue to call the base class updateValue() so the
//			Safety Net Sensor Data can be checked.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "PressureSensor.hh"
#include "ServosGlobal.h"

//@ Usage-Classes

//@ End-Usage

//@ Code...

// The following constant is used to convert  counts to pressure (cmH2O).
// The calibration points for the pressure sensors are:
// -100 cmH2O = 1 volt, 140 cmH2O = 8 volts
static const Real32 MIN_PRESS = -100.0 ;
static const Real32 MIN_VOLT = 1.0 ;
static const Real32 MAX_PRESS = 140.0 ;
static const Real32 MAX_VOLT = 8.0 ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureSensor()
//
//@ Interface-Description
//      Constructor.  Three arguments are used to construct a pressure
//      sensor object: SafetyNetSensorData *, the sensor id and the filter's
//		alpha value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The sensor id and SafetyNetSensorData * are used for sensor
//		construction, the alpha is used to initialize the processed values
//		construction.  The zero point offset is set to a nominal value, awaiting
//      an auto zero maneuver.
//		$[00401]
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
PressureSensor::PressureSensor( SafetyNetSensorData *pData,
								const AdcChannels::AdcChannelId adcId,
								const Real32 alpha)
: Sensor(adcId, pData), processedValues_(alpha)        // $[TI1]
{
	CALL_TRACE("PressureSensor::PressureSensor( \
				SafetyNetSensorData *pData, \
				const AdcChannels::AdcChannelId adcId, const Real32 alpha)") ;
#if E600_TO_CHECK
    // Set a nominal zero offset for the pressure sensors until
    // the auto zero maneuver is run.  Round up to the nearest integer
    zeroOffset_ = (AdcCounts)( ( (0.0F - MIN_PRESS) / (MAX_PRESS - MIN_PRESS)
    				  * (MAX_VOLT - MIN_VOLT) + MIN_VOLT
    			    ) / COUNT_TO_VOLT_FACTOR + 0.5F) ;
#endif
	
	// temporary default values
	switch(adcId)
	{
	case ADC_EXP_PRESSURE_TRANSDUCER:
		zeroOffset_ = pP2Transducer->GetZeroCounts();
		spanCounts_ = pP2Transducer->GetSpanCounts();
		break;
	case ADC_INSP_PRESSURE_TRANSDUCER:
		zeroOffset_ = pP1Transducer->GetZeroCounts();
		spanCounts_ = pP1Transducer->GetSpanCounts();
		break;
	case ADC_EXH_DRIVE_PRESSURE_TRANSDUCER:
		zeroOffset_ = pPdrvTransducer->GetZeroCounts();
		spanCounts_ = pPdrvTransducer->GetSpanCounts();
		break;
	case ADC_EXH_SUPPLY_PRESSURE_TRANSDUCER:
		zeroOffset_ = 0x10;
		spanCounts_ = 0xa04;
		break;
	default:
		break;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureSensor()
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//        None
//---------------------------------------------------------------------
//@ PreCondition
//        None
//---------------------------------------------------------------------
//@ PostCondition
//        None
//@ End-Method
//=====================================================================
PressureSensor::~PressureSensor( void)
{
	CALL_TRACE("PressureSensor::~PressureSensor( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateValue()
//
//@ Interface-Description
//      This method takes no arguments and returns nothing.  It updates
//      the sensor data based upon the last ADC counts read.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Obtain the raw count.  Convert the raw count value into a pressure
//      value, then update processValues_.
//---------------------------------------------------------------------
//@ PreCondition
//        None
//---------------------------------------------------------------------
//@ PostCondition
//        None
//@ End-Method
//=====================================================================
void
PressureSensor::updateValue( void)
{
	CALL_TRACE("PressureSensor::updateValue( void)") ;

    // $[TI1]
	Sensor::updateValue() ;

#ifdef INTEGRATION_TEST_ENABLE
    Sensor::updateVirtualValue();
#endif // INTEGRATION_TEST_ENABLE

    processedValues_.updateValues(engValue_);
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
PressureSensor::SoftFault( const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, PRESSURESENSOR, lineNumber,
                             pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: rawToEngValue_()
//
//@ Interface-Description
//      This method has one argument, count, and returns the pressure (cmH20)
//      that is equivalent to the value passed in.
//---------------------------------------------------------------------
//@ Implementation-Description
//        Perform a linear transformation from ADC counts to pressure
//        reading after adjusting for the zero offset.
//---------------------------------------------------------------------
//@ PreCondition
//        None
//---------------------------------------------------------------------
//@ PostCondition
//        None
//@ End-Method
//=====================================================================
Real32
PressureSensor::rawToEngValue_( const AdcCounts count) const
{
	CALL_TRACE("PressureSensor::rawToEngValue_( const AdcCounts count)") ;

    // $[TI1]
#if E600_TO_CHECK
    return ( (count - zeroOffset_) * COUNT_TO_VOLT_FACTOR
				 * (MAX_PRESS - MIN_PRESS) / (MAX_VOLT - MIN_VOLT)
		   ) ;
#endif
 
	static const Real32 SPAN_CAL_PRESSURE = 60.0f;
	static const Real32 ZERO_CAL_PRESSURE = 0.0f;

	Real32 engValue =  0;

	engValue = ((SPAN_CAL_PRESSURE - ZERO_CAL_PRESSURE) / (spanCounts_ - zeroOffset_))
							* (count - zeroOffset_);
		
	
	return engValue;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

