#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendDataMgr
//---------------------------------------------------------------------
//@ Interface-Description
//  The TrendDataMgr is responsible for the overall coordination of the other
//  agents to perform:
//  - Collection of the Patient Data Results from Patient-Data
//  - Collection of the setting values from Settings-Validation
//  - Collection of Auto and User events
//  - Compression of the Trend Data into higher timescales
//  - Storage of the trend values on the compact flash
//  - Provides trend data to clients such as Gui App
// 
//  The TrendDataMgr handles the initialization of the Trend-Database
//  - Sys-Init calls the Subsystems initialization which starts the initialization
//  - The Gui-Online event starts the initialization of the database from the compact flash
//  - Patient Setup causes the Patient-Data and Setting's registration to be done
//    A "new" patient will reset the database
//  - Patient Connected event will cause the starting of the data collection
//  - Data Errors (Compact Flash) will cause the manager to stop and delay spin
// 
// 	The Gui Application must inform the manager once patient setup has been completed
//  and if the patient setup is "new" or "same" patient. The static method PatientSetupComplete()
//  needs to be called, if this is the same patient then call it with the TRUE parameter, if
//  the patient setup is "new" then call it with the FALSE parameter, this will cause the
//  database to be reset.
// 
//  Clients request trend Data by configured a TrendDataSet object and calling the 
//  static RequestData() method passing in the TrendDataSet object.  The RequestData()
//  method will accept the request and return REQUEST_ACCEPTED and then process the request
//  and signal that the data is available via the TrendEventTarget::trendDataReady().
//  Or it will reject the request and return either MANAGER_BUSY (processing some other request)
//  or MANAGER_NOT_ENABLED (manager is not finished with initialization or in error).
// 
//  Clients can request that an event be stored in the trend database via the PostEvent() method.
// 
//  Clients register to get events from the Trend-Data base via the TrendEventTarget
//  - trendDataReady() will be sent when the requested trend data is available for the client
//  - trendManagerStatus() will be sent when there is a change in the manager status
//    like finishing initialization or detection of errors
// 
//  Clients and request other information such as
//  - GetTableCapacity() will return the capacity in samples for a table
//  - GetTableFrequency() will return the number of frames per sample for a given table
//  - GetEndOfDataPosition() will return the current frame sequence number in the database
// 
//---------------------------------------------------------------------
//@ Rationale
//  Encapsulates the public interface for the clients and the interface
//  for the other trend agents that the manager is responsible for.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	The TrendDataMgr coordinates the activities of the other trend agents.
//  The manager provides static instance of the other managers via "get" methods,
//  this is done so that clients of TrendDataMgr don't have to know about the 
//  details of the other agents.  The only symbols to exists in the clients space is
//  the name of the agents and now details, as would be the case if TrendDataMgr contained
//  the agents.
// 
//  The TrendDataMgr uses the state member "initState_" to control/manage the activity
//  of the other agents, not all of the activities are valid in all of the states.
//  The manager will perform/coordinate the following activities in each state:
//  - NOT_INITIALIZED: Initial state following the construction of the object, the static
//    method Initialize() will update the state.  This needs to be performed before the
//    Task() method starts to run, NOT_INITIALIZED is an illegal state in the Task() method
//  - INITIALIZATION_PENDING: The managers initialization is complete and the Task() method
//    is waiting for the Gui online event before completing the database initialization, no
//    other activities are performed in this state
//  - INITIALIZED_IDLE: The manager has received the Gui online event and completed the 
//    database initialization, and is waiting for the patient setup complete event.  In this
//    state the only other valid activity is a database reset.
//  - SAMPLE_IDLE: The manager has received the patient setup complete method and is waiting
//    for the patient connected event.  In this state the only other valid activity is a
//    trend data request.
//  - SAMPLE_RUNNING: The manager has received the patient connected event and is now
//    collecting the trend data from Patient-Data and Settings-Validation.  In this state
//    the only other valid activity is a trend data request.
//  - NO_TREND_OPTION: The manager has found that the Trend software option is not installed.
//    There are no valid activities in this state.
//  - ERROR: The manager has detected an error (Data corruption or Compact Flash).  There are
//    no valid activities in this state.
// 
//  The TrendDataMgr performs all of its activities on its own "task" which is running the static
//  Task() method.  As stated above the activities very depending on the state of the manager.
//  The task runs forever spending most of its time in the "pendForMsq" with a timeout.  It
//  uses this timeout to perform other activities such collecting trend data.  When a valid
//  message is returned and the manager is in a valid state to process a data request, the
//  request is processed and a signal is sent back to the Gui Application.  
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//  Deferred to the Agents
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDataMgr.ccv   25.0.4.0   19 Nov 2013 14:34:36   pvcs  $
//
//@ Modification-Log         
//
//  Revision: 003  By:  rhj    Date:  15-Aug-2007    SCR Number: 6390
//       Project:  Trend
//       Description:
//             Fixed SCR 6390 by adding code to initialize the 
//             repositories in the ResetDatabase method.
//
//  Revision: 002  By:  rhj    Date:  20-Jul-2007    SCR Number: 6390
//       Project:  Trend
//       Description:
//             Added a ResetDatabase method, which resets the database.
//
//  Revision: 001  By:  mjf    Date:  22-Aug-2006    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version.
//=====================================================================

#include "GuiApp.hh"				// GUI-Applications
#include "Task.hh"                  // OS-Foundation
#include "TimeStamp.hh"             // OS-Foundation
#include "OsTimeStamp.hh"           // OS-Foundation
#include "IpcIds.hh"                // Sys-Init
#include "UserAnnunciationMsg.hh"   // GUI-Applications
#include "Background.hh"            // Saftey-Net
#include "SoftwareOptions.hh"		// BD-IO-Devices
#include "OsUtil.hh"    			// Operating-System
#include "PatientDataMgr.hh"		// Patient-Data


#include "Trend_Database.hh"
#include "TrendDataMgr.hh"
#include "TrendDataSet.hh"
#include "TrendDataValues.hh"
#include "TrendDbPatientRepository.hh"
#include "TrendDbSettingRepository.hh"
#include "TrendDbApplicability.hh"	
#include "TrendDbEventReceiver.hh"
#include "TrendDbTestDataAgent.hh"
#include "TrendDbControlBlock.hh"


//@ Code...

// Control the amount of debug output, the level can be changed
// run time up updating the static DebugMask variable.
#ifdef TREND_MGR_DEBUG
//Uint8 TrendDataMgr::DebugMask = 255;   // everything
//Uint8 TrendDataMgr::DebugMask = 63;    // almost everything 
//Uint8 TrendDataMgr::DebugMask = 31;    // Med level debug
//Uint8 TrendDataMgr::DebugMask = 7;	   // light level debug
Uint8 TrendDataMgr::DebugMask = 3;     // Startup, Performance, Error
#else
Uint8 TrendDataMgr::DebugMask = 1;		// Error's Only
#endif

// Used in development to lock the database from any reset operation
// Default to not locked
Boolean TrendDataMgr::IsLockDatabase = FALSE; 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDataMgr [constructor]
//
//@ Interface-Description
//  Constructs the object.  Initializes the members and sets the state
//  to NON_INITIALIZED.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataMgr::TrendDataMgr( void )
	:   initState_( NON_INITIALIZED ),
		readRequestQ_( TREND_DATA_MGR_Q ),
		frameSeqNumber_( 0 ),
		isInitRequestDatabase_( FALSE ),
		isResetRequestDatabase_( FALSE ),
		isSystemContextChange_( TRUE ),
		isDataRequestInProcess_( FALSE )
{            
	TREND_MGR_DBG2( 2, "Trend Data Manager constructor, debug ",
					DebugMask );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDataMgr [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataMgr::~TrendDataMgr( void )
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [static]
//
//@ Interface-Description
//  Constructs and Initialize the TrendDataMgr, must be called before the Task()
//  begins to run.  Also constructs each of the agents and registers the Event
//  Receiver to get Bd, Novram, and User/Auto events.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the state to INITIALIZED_PENDING
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::Initialize( void )
{
	// First call to the static constructor of TrendDataMgr and its agents
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	TrendDbEventReceiver & trendEventReceiver = trendDataMgr.getEventReceiver_();
	TrendDbPatientRepository & trendPatientRep = trendDataMgr.getPatientRepository_();
	TrendDbSettingRepository & trendSettingRep = trendDataMgr.getSettingRepository_();
	TrendDbApplicability & trendApplicability = trendDataMgr.getApplicability_();
	TrendDbControlBlock & trendDbControlBlock = trendDataMgr.getControlBlock_();

	// Check to see if Xena hard is installed
	if( IsXenaConfig() )
	{
		TREND_MGR_DBG1( 2, "TrendDataMgr Xena hardware found" );

		// Register Event Receiver now, the registration of the repositories is 
		// hold off until Patient Setup complete, so that the Gui Application 
		// doesn't deregister the registration.
		trendEventReceiver.registerTargets();

		// Update the state
		trendDataMgr.setState_( INITIALIZATION_PENDING );
	}
	else
	{
		// Not Xena hardware, set state to NO_TREND_OPTION
		TREND_MGR_DBG1( 2, "TrendDataMgr Xena hardware not found" );

		trendDataMgr.setState_( NO_TREND_OPTION );
	}

	TREND_MGR_DBG2( 2, "TrendDataMgr Initialize, state ", trendDataMgr.initState_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initRepositories_
//
//@ Interface-Description
//  Initialize the trend database
//---------------------------------------------------------------------
//@ Implementation-Description
//  Opens the compact flash interface, initialize the Control Block,
//  Patient Repository and Setting Repository agents.  Checks the 
//  software options for the Trend option and development key.
//  Sets the state to 
//  INITIALIZED_IDLE - if all of the initialization completed successfully
//  NO_TREND_OPTION - if the Trend option is not installed, overrides ERROR
//  ERROR - An error has been detected in the initialization
// 
//  Satisfies requirement:
//  [TR01133] During system initialization the compact flash shall identify
//  compact flash failures ...
//  [TR01134] The Trend option shall not be available until the initialization
//  of the compact flash has completed
//  [TR01137] The initialization process ... shall not prevent or delay therapy
// 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::initRepositories_( void )
{
	TREND_MGR_DBG1( 2, "Trend Data Manager initialize repositories" );
	Boolean isError = FALSE;

	// Open the compact flash interface
	TREND_MGR_DBG1( 2, "Open Compact Flash driver" );
	CompactFlash & cfInterface = CompactFlash::GetCompactFlash();
	if ((cfInterface.open()) != SUCCESS)
	{
		TREND_MGR_DBG1( 1, "Compact Flash failed to open" );
		isError = TRUE;
	}

	TrendDbControlBlock & trendControlBlock = getControlBlock_();

	if (!isError)
	{
		// Setup the control block, this must be performed before
		// the repositories can be initialized
		// The Control block will either initialized from the compact flash or
		// will default to an empty database
		TREND_MGR_DBG1( 2, "Initialize control blocks" );
		trendControlBlock.initialize();

		// Initialize the patient repository
		// The Patient Repository is not checked of constancy here, that is done
		// when data is read from the patient repository, therefor initialize() doesn't
		// return a status
		TrendDbPatientRepository &  trendPatient = getPatientRepository_();
		trendPatient.initialize();

		// Initialize the Setting repository, including the creation of the Index Table
		// Return the status from the creation of the Index Table, if the table can't be 
		// created then there is a data error in the settings repository
		TrendDbSettingRepository &  trendSetting = getSettingRepository_();
		if (trendSetting.initialize() != SUCCESS)
		{
			TREND_MGR_DBG1( 1, "Setting Repository failed first initialization try" );

			// Wait a second and try again, reopen the compact flash to reset the interface
			Task::Delay( 1, 0 );
			if (cfInterface.open() != SUCCESS || trendSetting.initialize() != SUCCESS)
			{
				TREND_MGR_DBG1( 1, "Setting Repository failed to initialize, second try" );
				isError = TRUE;
			}
		}
	}

	if (!isError)
	{
		// Initialize the frame/sequence number
		initializeSequenceCount_();
		TREND_MGR_DBG2( 2, "Initialize frame/seq number ", getCurrentSequenceCount_() ); 

		// Flush the control block, if it fails try again
		if (trendControlBlock.flush() != SUCCESS)
		{
			TREND_MGR_DBG1( 1, "Control Block failed to flush, first try" );

			// Wait a second and try again, reopen the compact flash to reset the interface
			Task::Delay( 1, 0 );
			if (cfInterface.open() != SUCCESS || trendControlBlock.flush() != SUCCESS)
			{
				TREND_MGR_DBG1( 1, "Control Block failed to flush, second try" );
				isError = TRUE;
			}
		}
	}

	// Check to see if the developer key is present, if so lock the database
	const SoftwareOptions& swOptions = SoftwareOptions::GetSoftwareOptions();
	TREND_MGR_DBG2( 4, "Data Key type is ", swOptions.getDataKeyType() );
	if (swOptions.getDataKeyType() == SoftwareOptions::ENGINEERING)
	{
		// Lock the database
		TrendDataMgr::IsLockDatabase = TRUE;
		TREND_MGR_DBG1( 2, "Developer Key found, lock trend database" );
	}

	// Check to see if the Trend option is installed
	if (!swOptions.IsOptionEnabled( SoftwareOptions::TRENDING ))
	{
		TREND_MGR_DBG1( 1, "Trend Option not installed" );
		setState_( NO_TREND_OPTION );
	}
	else
	{
		TREND_MGR_DBG2( 2, "Trend Option installed, error flag ", isError );

		// If error call Disable() to log the message signal Gui Application 
		// and update state to ERROR, else set state to INITIALIZED_IDLE
		if (isError)
		{
			Disable();
		}
		else
		{
			setState_( INITIALIZED_IDLE );
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsEnabled [static]
//
//@ Interface-Description
//  Return TRUE if the patient setup is complete and waiting for patient connect 
//  or the task is running collecting data with no errors
//---------------------------------------------------------------------
//@ Implementation-Description
//  Checks the state for "SAMPLE_IDLE" or "SAMPLE_RUNNING"
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDataMgr::IsEnabled( void )
{
	Boolean isEnable = FALSE;
	TrendDataMgr &  trendDataMgr = GetTrendDataMgr();

	if (trendDataMgr.isState_(SAMPLE_IDLE) || trendDataMgr.isState_(SAMPLE_RUNNING))
	{
		isEnable = TRUE;
	}

	TREND_MGR_DBG4( 32, "TrendDataMgr state ", trendDataMgr.initState_,
					", Enabled ", isEnable );
	return( isEnable );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: IsInitialzed [static]
//
//@ Interface-Description
//  Return TRUE if the task is initialized
//---------------------------------------------------------------------
//@ Implementation-Description
//  Check for INITIALIZED_IDLE or IsEnabled()
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Boolean TrendDataMgr::IsInitialized( void )
{
	Boolean isInitialized = FALSE;
	TrendDataMgr &  trendDataMgr = GetTrendDataMgr();

	if (trendDataMgr.isState_(INITIALIZED_IDLE) || IsEnabled())
	{
		isInitialized = TRUE;
	}

	TREND_MGR_DBG4( 32, "TrendDataMgr state ", trendDataMgr.initState_,
					", Initialized ", isInitialized );

	return( isInitialized );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Disable [static]
//
//@ Interface-Description
//  Disables Trending, Logs a message in the system information log
//---------------------------------------------------------------------
//@ Implementation-Description
//  Checks the Compact Flash interface status, if it is not OK then set
//  the minor code in the log message to 1, else the minor code will be 0
// 
//  Satisfies requirement:
//  [TR01145] Upon detection of compact flash failures ... shall log an
//  error condition in the System Information Log
//  [TR01136] ... Trending feature shall be disabled ...
// 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::Disable( void )
{
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	Uint16 minorCode = 0;

	// If not already in ERROR, set the state, signal Gui and log message
	if (!trendDataMgr.isState_( INIT_STATE_ERROR ))
	{
		trendDataMgr.setState_(INIT_STATE_ERROR);
		trendDataMgr.signalGuiManagerStatus_();

		CompactFlash & cfInterface = CompactFlash::GetCompactFlash();
		if (cfInterface.getStatus() != CompactFlash::STATUS_OK)
		{
			minorCode = 1;
		}

		Background::LogDiagnosticCodeUtil( ::BK_COMPACT_FLASH_ERROR_INFO, minorCode );
	}

	TREND_MGR_DBG2( 1, "Trend Data Manager - disabled, code ", minorCode ); 
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PostEvent [static]
//
//@ Interface-Description
//  Request that an event be stored in the database
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forwards the event to TrendDbEventReceiver
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::PostEvent( TrendEvents::EventId event )
{
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	TrendDbEventReceiver & TrendDbEventReceiver_ = trendDataMgr.getEventReceiver_();
	TrendDbEventReceiver_.postEvent( event );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PostEventValue [static]
//
//@ Interface-Description
//  Request that an event and its value be stored in the database
//---------------------------------------------------------------------
//@ Implementation-Description
//  Forwards the event and value to TrendDbEventReceiver
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::PostEventValue( TrendEvents::EventId event, const TrendValue & value )
{
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	TrendDbEventReceiver & TrendDbEventReceiver_ = trendDataMgr.getEventReceiver_();
	TrendDbEventReceiver_.postEventValue( event, value );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  PatientSetupCompleted [static]
//
//@ Interface-Description
//  Update the patient setup complete status to TRUE
//  Takes the isSamePatient parameter and if TRUE  marks the SAME patient
//  event, if FALSE the database reset request is set TRUE
//---------------------------------------------------------------------
//@ Implementation-Description
//  Passes the patient setup to TrendDbEventReceiver
//  Set the database reset first, so that a "New" patient will always
//  clear the database before the patient setup complete is processed
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::PatientSetupCompleted( Boolean isSamePatient )
{
	TREND_MGR_DBG2( 4, "Received Patient Complete, same patient ", isSamePatient );
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	TrendDbEventReceiver & trendEvent = trendDataMgr.getEventReceiver_();

	// If this is same patient then mark the "Same Patient" auto event
	// otherwise reset the database (ie "New Patient" )
	if (isSamePatient)
	{
		trendEvent.postEvent( TrendEvents::AUTO_EVENT_SAME_PATIENT );
	}
	else
	{
		TREND_MGR_DBG1( 1, "TrendDataMgr Request Reset/clear Database" );
		trendDataMgr.isResetRequestDatabase_ = TRUE;
	}

	// Set the patient setup compelete flag
	trendEvent.setPatientSetupComplete( TRUE );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SystemContextChange [static]
//
//@ Interface-Description
//  Update system context change  status to TRUE
//---------------------------------------------------------------------
//@ Implementation-Description
//  This will force a refresh of all of the trend values
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::SystemContextChange( void )
{
	TREND_MGR_DBG1( 4, "Received System Context change" );
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	trendDataMgr.isSystemContextChange_ = TRUE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RequestData [static]
//
//@ Interface-Description
//  Request the manager to fill in Trend data into the provided TrendDataSet
//  Puts a pointer to the TrendDataSet on the input queue
//---------------------------------------------------------------------
//@ Implementation-Description
//  Verifies that a request is not already in progress
//  and that the manager is enabled to handle data request
//  Returns the status of the request either
//      REQUEST_ACCEPTED    -   this request is in the queue and will be processed
//                              and sent back via the trendDataReady
//      MANAGER_BUSY        -   manager is already processing a request and that
//                              requested data will be sent via the trendDataReady
//      MANAGER_NOT_ENABLED -   manager is not enabled and there will be no call
//                              to the trendDataReady
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataMgr::RequestStatus TrendDataMgr::RequestData( TrendDataSet & trendDataSet )
{  
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	RequestStatus rtnStatus;

	// First check to see if the manager is enabled
	if (trendDataMgr.IsEnabled())
	{
		// Check to see if a request is already in process
		if (trendDataMgr.isDataRequestInProcess_)
		{
			TREND_MGR_DBG1( 1, "Trend Manager busy with other data request" );
			rtnStatus = MANAGER_BUSY;
		}
		else
		{
			// not busy, so put this request on the queue
			TREND_MGR_DBG4( 32, "Request Data Set, ", 
							hex, (Uint32) &trendDataSet, dec );

			// The data request in process semaphore "isDataRequestInProcess" will 
			// be cleared by the Task() method once the request is completed and 
			// before the signal is sent back to the Gui Application
			// Also mark the DataSet in use "PROCESSING" so that Gui Application knows
			// not to modify it until the request is completed and the signal is received
			trendDataMgr.isDataRequestInProcess_ = TRUE;
			trendDataSet.setStatus_( TrendDataSet::PROCESSING );
			MsgQueue::PutMsg( TREND_DATA_MGR_Q, (Uint32) &trendDataSet );
			rtnStatus = REQUEST_ACCEPTED;
		}
	}
	else
	{
		// manager not enabled
		TREND_MGR_DBG1( 1, "Trend Manager not enabled, can't request data" );
		rtnStatus = MANAGER_NOT_ENABLED;                   
	}

	TREND_MGR_DBG2( 4, "Request Data return status ", rtnStatus );
	return rtnStatus;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Task [static]
//
//@ Interface-Description
//  All of the activity for the TrendDataMgr happens on its "task" which
//  is running Task() method.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	The Task() method runs forever, processing all of the managers activities
//  based on the state.
// 
//  The Task() method runs all the time even if Trending is not installed/supported
//  therefore the start of the method has no activity other then setup of local
//  variables, before switching on the state of the manager:
//  - NON_INITIALIZED: is invalid (means the static Initialize() didn't get called
//    from Sys-Init before the task started, this will disable trending and but the
//    manager into an ERROR state
//  - INITIALIZATION_PENDING: Wait for the Gui to go online before initializing the 
//    compact flash and the repositories (also checks the isInitRequestDatabase_ flag
//    used by the TestDataAgent to initialize the repositories in service mode).  Updates
//    the state to INITIALIZED_IDLE if successful, NO_TREND_OPTION if trending is not 
//    installed and ERROR if an error is detected.
//  - INITIALIZED_IDLE: Waits for a database reset request and a patient setup complete
//    The database reset will always be done first, before processing the patient setup
//    and moving the state to SAMPLE_IDLE.
//  - SAMPLE_IDLE: Waits for the patient to be connected then signals Gui that Trending
//    is collecting data and updates the state to SAMPLE_RUNNING
//  - SAMPLE_RUNNING: Will collect the trend data every ten seconds on 10 second
//    boundary of the clock (10, 20, 30, etc ...).  It will compute the delay timeout 
//    required to get to the next ten second boundary (the overshoot is computed to
//    predict the difference in time between ready to run and actual getting to
//    run, this prediction is subtracted from the delay timeout)
//  - NO_TREND_OPTION/ERROR: In both of these states the manager will loop forever
//    and will process/do nothing else.  A power cycle is required to get out of this
//    state
// 
//  On each loop the Task() will wait on the message queue with the timeout to the next
//  ten second boundary (if collecting trend data, else it will timeout at 1 second).
//  If a trend data request is received and the manager is enabled to handle request
//  then process the request and signal Gui once the request is done.  Go back through
//  the loop which will calculate the timeout/delay to the next ten second boundary from
//  the current time.
// 
//  Satisfies requirment: [TR0119] ... the system shall sample trend data every 10 seconds
//  
// 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::Task( void )
{
	//TODO E600 this is temp hack only, remove when implemented
	/*while(1)
	{
		printf("TrendDataMgr::Task\r\n");
		Task::Delay(1);
	}*/

	/*//TODO E600 port, enable when ready

	static const Uint16 ONE_SEC = 1000;		// in ms									   
	static const Uint16 HALF_SEC = 500;		// in ms
	static const Uint16 TEN_SEC = 10000;	// in ms
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();

	TrendDataSet * pTrendDataSet;
	OsTimeStamp runningTime;
	TimeStamp ventDateTime;

	// Default isFirstSample to TRUE and reqTimeout to one second, and no overshoot
	Boolean isFirstSample = FALSE;
	Int32 reqTimeout = ONE_SEC;   
	Int32 overshoot = 0;

	// Update the clocks
	ventDateTime.now();
	runningTime.now();

	// Loop forever
	while (TRUE)
	{
		// Process per current state
		switch (trendDataMgr.initState_)
		{
		    case NON_INITIALIZED :
			// Error at this point initialization should be complete
			{
				TREND_MGR_DBG1( 1, "Illegal initialization state in main task loop" );
				Disable();
				break;
			}

		    case INITIALIZATION_PENDING :
			// Wait for Gui to go online or an initialization request
			{
				TREND_MGR_DBG2( 32, "Gui State is ", GuiApp::GetGuiState() );

				if (GuiApp::GetGuiState() == STATE_ONLINE || trendDataMgr.isInitRequestDatabase_)
				{
					// Initialize the repositories, if successful the state will be updated
					// to INITIALIZED_IDLE.  If Trending is not enabled then update the state
					// to NO_TREND_OPTION and if an error is detected update the state to ERROR
					trendDataMgr.initRepositories_();

					// Clear any request to initialize
					trendDataMgr.isInitRequestDatabase_ = FALSE;

					TREND_MGR_DBG2( 2, "TrendDataMgr Initialized Database - state ", 
									trendDataMgr.initState_ );
				}
				break;
			}

		    case INITIALIZED_IDLE :
			// Waiting for the patient to connect or request to reset the database
			{
				// Check to see if the database should be reset before patient setup
				if (trendDataMgr.isResetRequestDatabase_)
				{
					if (!trendDataMgr.IsLockDatabase)
					{
						TREND_MGR_DBG1( 2, "TrendDataMgr Reset database" );

						// Reset the patient and setting repository
						TrendDbPatientRepository & patientRep = trendDataMgr.getPatientRepository_();
						TrendDbSettingRepository & settingRep = trendDataMgr.getSettingRepository_();
						patientRep.resetDatabase();
						settingRep.resetDatabase();

						// Reinitialize the current frame number, this will force it
						// to a even boundary for all of the patient data tables so that
						// the first sample in the new data will have the correct number
						// of frames in its average
						trendDataMgr.initializeSequenceCount_();

						// Flush the control block, try again if not successful 
						TrendDbControlBlock & controlBlock = trendDataMgr.getControlBlock_();
						if (!controlBlock.flush())
						{
							TREND_MGR_DBG1( 2, "Reset, failed to flush control block first try" );

							// Wait and reset the Compact Flash interface
							Task::Delay( 1, 0 );
							CompactFlash & cfInterface = CompactFlash::GetCompactFlash();
							if (cfInterface.open() != SUCCESS || controlBlock.flush() != SUCCESS)
							{
								TREND_MGR_DBG1( 1, "Reset, failed to flush control block" );
								Disable();
							}
						}
					}
					else
					{
						TREND_MGR_DBG1( 1, "Database can't be reset when locked" );
					}

					// Clear the request
					trendDataMgr.isResetRequestDatabase_ = FALSE;
				}

				// Check to see if patient setup is complete, request for callbacks and update
				// state to "SAMPLE_IDLE" waiting for patient connected 
				// Make sure that a database reset request is not active before processing
				// the patient complete
				const TrendDbEventReceiver & trendEventReceiver = GetEventReceiver(); 
				if (trendEventReceiver.isPatientSetupComplete() && !trendDataMgr.isResetRequestDatabase_ )
				{
					// Register for Setting-Validation callbacks via the Setting Repository
					TrendDbSettingRepository &  settingRep = trendDataMgr.getSettingRepository_();
					settingRep.registerTarget();

					// Register for Patient-Data callbacks via the Trend Data Manager which will forward
					// the call back to Patient Repository, this is done becuase the callback is static
					PatientDataMgr::RegisterBDItemCallback( TrendDataMgr::ChangeHappened );

					// force an update for all of the trend data
					TrendDataMgr::SystemContextChange(); 

					trendDataMgr.setState_( SAMPLE_IDLE );
					TREND_MGR_DBG1( 2, "Trend task ready for data request" );

					// Send event back to Gui that initialization is complete
					// and that the manager is waiting for patient connect
					trendDataMgr.signalGuiManagerStatus_();
				}

				break;
			}

		    case SAMPLE_IDLE :
			// Wait for the patient connect event, before collecting trend data
			{
				const TrendDbEventReceiver & trendEventReceiver = GetEventReceiver();
				if (trendEventReceiver.isPatientConnected())
				{
					TREND_MGR_DBG1( 2, "Patient connected, start sample delay" );
					trendDataMgr.setState_( SAMPLE_RUNNING );

					// Send event to the Gui that data collection has started
					trendDataMgr.signalGuiManagerStatus_();

					// Set that this is the first sample and current time
					// The first sample will be held off for a short time
					runningTime.now();
					isFirstSample = TRUE;
				}

				break;
			}

		    case SAMPLE_RUNNING :
			// Collection trend samples
			{
				// If this is the first sample then wait for 10 seconds
				// before taking the first sample, this lets Patient-Data
				// become stable before getting data (while not waiting to long)
				if (isFirstSample && runningTime.getPassedTime() > TEN_SEC)
				{
					TREND_MGR_DBG1( 1, "Start Data Collection" );
					isFirstSample = FALSE;
					runningTime.now();
				}

				// Is it time to collect trend data, use the vent time in seconds
				// Try to get the time at even 10 second boundaries (like 10, 20, 30 etc)
				// but if it has been more then ten seconds from the last update then
				// go ahead and collect the data now
				ventDateTime.now();
				if (!isFirstSample && 
					(!( ventDateTime.getSeconds() % 10 ) || (runningTime.getPassedTime() > TEN_SEC ) ))
				{
					// Compute the current over shoot if any
					// Don't let the overshoot get to big, as this will offset the dealy timeout
					// to much and will result in waking up to early and then having to just wait
					// a short time (less then a second).
					Int32 currentOvershoot = runningTime.getPassedTime() - TEN_SEC;
					if (currentOvershoot < 0)
					{
						currentOvershoot = 0;
					}
					else if (currentOvershoot > HALF_SEC)
					{
						// Log an error message if the delay is so big that a ten second
						// period was missed
						if( currentOvershoot > TEN_SEC )
						{
							TREND_MGR_DBG2( 1, "TrendDataMgr missed sample, delayed (ms) ",
											    currentOvershoot );
						}
						currentOvershoot = HALF_SEC;
					}

					// Compute the averaged over shoot which will be used to offset the delay
					if( overshoot == 0 )
					{
						overshoot = currentOvershoot;
					}
					else
					{
						overshoot = (overshoot + currentOvershoot) / 2;
					}

					TREND_MGR_DBG4( 2, "TrendDataMgr current overshoot, ", currentOvershoot,
									   ", avg overshoot, ", overshoot );

					// Set the timer for the next sample, this is done before the sampleTrend_
					// so that its time will be included in the 10 second count
					runningTime.now();

					// Sample the trend data
					Uint32 seqPosNumber = trendDataMgr.getCurrentSequenceCount_();
					trendDataMgr.sampleTrend_( seqPosNumber, ventDateTime );
				}

				// Set timeout for next request based on vent time to the next 10 second boundary
				// and subtract the previous overshoot
				reqTimeout = TEN_SEC - ventDateTime.getHundredths() -
							 (ONE_SEC * (ventDateTime.getSeconds() % 10) );
				reqTimeout -= overshoot;
				if (reqTimeout < HALF_SEC)
				{
					// Don't like the timeout get to small
					reqTimeout = HALF_SEC;
					TREND_MGR_DBG2( 2, "TrendDataMgr request timeout to small ", reqTimeout );
				}

				break;
			}

		    case NO_TREND_OPTION:
		    case INIT_STATE_ERROR:
			// No Trend installed or Error happened, spin here forever and ever
			{
				TREND_MGR_DBG2( 1, "TrendDataMgr Task spin delay forever, state ",
								trendDataMgr.initState_ );
				while (TRUE)
					Task::Delay( 0, TEN_SEC );

				// Should never get here, break cause compiler error about unreachable code
				//break; 
			}

		    default :
			// Illegal state
			{
				TREND_MGR_DBG2( 1, "TrendDataMgr illegal state ", trendDataMgr.initState_ );
				Disable();

				break;
			}

			// end of the switch
		}   

		// Wait for a data request to come in or timeout
		TREND_MGR_DBG4( 32, "Read request timeout ", reqTimeout,
						", overshoot ", overshoot );

		// Need to read the request even if not going to use it
		// so that the queue will not overfull.  The request will only be
		// processed if the manager is "Enabled".  The readRequest_() also
		// provides the pacing of the Task() loop
		Int32 rtn = trendDataMgr.readRequest_( pTrendDataSet, reqTimeout );
		if (rtn == Ipc::OK && IsEnabled())
		{
			Uint32 seqPosNumber = trendDataMgr.getCurrentSequenceCount_();
			trendDataMgr.processRequest_( pTrendDataSet, seqPosNumber );

			// Clear the data in process flag before signal Gui
			// just in case it is ready for another request right now
			trendDataMgr.isDataRequestInProcess_ = FALSE;
			trendDataMgr.signalGuiDataReady_( (Uint32) pTrendDataSet );
		}

		// end of the while forever loop
	}
*/
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ChangeHappened [static]
//
//@ Interface-Description
//  This is the callback function registered with the Patient Data Manager
//  This function is called when a PatientItemId has been updated in the
//  Patient Data manager
//---------------------------------------------------------------------
//@ Implementation-Description
//  Fowards the call to TrendDbPatientRepository
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::ChangeHappened( const PatientDataId::PatientItemId dataId )
{
	TrendDataMgr & trendMgr = GetTrendDataMgr();
	TrendDbPatientRepository & patientRep = trendMgr.getPatientRepository_();
	patientRep.changeHappened( dataId );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTableCapacity [static]
//
//@ Interface-Description
//  Return the capacity for the given patient data table
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the private getTableCapacity_ method
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDataMgr::GetTableCapacity( TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	Uint32 cap = 0;

	if (timescale <= TrendTimeScaleValue::TREND_TIME_SCALE_72HR)
	{
		TrendDataMgr & dataMgr = GetTrendDataMgr();
		cap = dataMgr.getTableCapacity_( (TrendDataMgr::TableId) timescale );
	}
	else
	{
		TREND_MGR_DBG2( 1, "Can't find table capacity for timescale ", timescale );
	}

	return( cap );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTableCapacity_
//
//@ Interface-Description
//  Return the capacity for the given the table
//---------------------------------------------------------------------
//@ Implementation-Description
//  The patient data tables capacities are based on the 1 Hour size in frames
//  divided by the number of frames per sample of the given table
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDataMgr::getTableCapacity_( TrendDataMgr::TableId tableId )
{
	Uint32 cap = 0;

	if (tableId == TrendDataMgr::SETTING_TABLE_ID)
	{
		cap = TrendDataMgr::TREND_SETTING_DB_CAPACITY;
	}
	else if (tableId <= TrendDataMgr::PATIENT_HIGH_TABLE_ID)
	{
		Uint16 tableFrequency = getTableFrequency_( tableId );
		cap = TrendDataMgr::TREND_PATIENT_DB_CAPACITY / tableFrequency;
	}
	else
	{
		TREND_MGR_DBG2( 1, "Bad table - no capacity info ", tableId );
	}

	return( cap );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTableFrequency [static]
//
//@ Interface-Description
//  Return the number of frames per sample for the given table this factor
//  is used in the update/refresh frequency factor for the given patient table
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the private getTableFrequency_() method
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint16 TrendDataMgr::GetTableFrequency( TrendTimeScaleValue::TrendTimeScaleValueId timescale )
{
	Uint16 factor = 0;

	if (timescale <= TrendTimeScaleValue::TREND_TIME_SCALE_72HR)
	{
		TrendDataMgr & dataMgr = GetTrendDataMgr();
		factor = dataMgr.getTableFrequency_( (TrendDataMgr::TableId) timescale );
	}
	else
	{
		TREND_MGR_DBG2( 1, "Can't find table capacity for timescale ", timescale );
	}

	return( factor );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getTableFrequency_
//
//@ Interface-Description
//  Return the number of samples per frame for the given table
//---------------------------------------------------------------------
//@ Implementation-Description
//  Gets the frames per sample from the Patient Repository agent.
//  The settings are only updated on changes and so have no fixed frequency 
//  so just return 1.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint16 TrendDataMgr::getTableFrequency_( TrendDataMgr::TableId tableId )
{
	Uint16 factor = 0;

	if (tableId == SETTING_TABLE_ID)
	{
		// Use one for the setting's database
		factor = 1;
	}
	else if (tableId <= PATIENT_HIGH_TABLE_ID)
	{
		// Ask the patient repository for the factor
		TrendDbPatientRepository & patientRep = getPatientRepository_();
		factor = patientRep.getTableFrequency( (TrendTimeScaleValue::TrendTimeScaleValueId) tableId );
	}
	else
	{
		TREND_MGR_DBG2( 1, "Bad table - no frequency factor info ", tableId );
	}

	return( factor );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEndOfDataPosition [static]
//
//@ Interface-Description
//  Return the current frame sequence number
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the private getCurrentSequenceCount_() which returns the next frame
//  sequence number, therefore subtract 1 to get current frame sequence number
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Uint32 TrendDataMgr::GetEndOfDataPosition( void )
{
	TrendDataMgr & trendMgr = GetTrendDataMgr();
	return( trendMgr.getCurrentSequenceCount_() - 1 );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initializeSequenceCount_
//
//@ Interface-Description
//  Set the current frame sequence number
//---------------------------------------------------------------------
//@ Implementation-Description
//  The frame sequence number is initialized from the control block
//  If there is no data in the Trend Database (1Hr table) then
//  in this case the frame number is forced to a 24 minute boundary so that
//  trending compression will start off in a known state for all timescales
//  and will correctly compress the first sample in each of the trend tables.
//  If there is no frame number in the control block (ie new install) then
//  default to a fixed value.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::initializeSequenceCount_( void )
{
	TrendDbControlBlock & trendControlBlock = getControlBlock_();

	// get the frame sequence number from the control block agent
	Int32 storeFrame = trendControlBlock.getCurrentFrame();
	if (storeFrame)
	{
		Uint32 dbSize = trendControlBlock.getSizeBlocks(PATIENT_1HR_TABLE_ID);
		if (!dbSize)
		{
			// There is no data in the 1Hr database, align the frame sequence number
			Uint16 commonFactor = getTableFrequency_(PATIENT_72HR_TABLE_ID) * 2;
			Int32 periods = (storeFrame / commonFactor) + 1;
			frameSeqNumber_ = (periods * commonFactor) + 1;
			TREND_MGR_DBG6( 2, "Align Frame/seq from stored value  ", storeFrame,
							", periods ", periods,
							", new frame value ", frameSeqNumber_ );
		}
		else
		{
			TREND_MGR_DBG2( 2, "Initialize Frame/Seq from stored value ", storeFrame );
			frameSeqNumber_ = storeFrame;
		}
	}
	else
	{
		// Use default
		frameSeqNumber_ = TrendDataMgr::TREND_PATIENT_DB_CAPACITY;
		TREND_MGR_DBG2( 2, "Initialize Frame/seq from default value  ",
						frameSeqNumber_ );
	}

	// Update the value in the control block
	trendControlBlock.setCurrentFrame( frameSeqNumber_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setNextSequenceCount_
//
//@ Interface-Description
//  Set the frame sequence number to the next value
//---------------------------------------------------------------------
//@ Implementation-Description
//  Use the control block to store the frame sequence number
//  The frame sequence number should never roll.  Its max value is 2 billion
//  which would be over 680 years of continuous use or over 14 million new patients
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::setNextSequenceCount_( void )
{
	// Update the frame number
	frameSeqNumber_++;

	// Validate the frame/seq number has not rolled, this would be bad so Disable
	if (frameSeqNumber_ <= TrendDataMgr::TREND_PATIENT_DB_CAPACITY)
	{
		TREND_MGR_DBG1( 1, "Trend Position number rolled" );
		Disable();
	}

	// Update the control block
	TrendDbControlBlock & trendControlBlock = getControlBlock_();
	trendControlBlock.setCurrentFrame( frameSeqNumber_ );

	TREND_MGR_DBG2( 4, "Next Frame/seq number ", frameSeqNumber_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendDataMgr
//
//@ Interface-Description
//  Return a static instance of the Trend Data Manager
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDataMgr & TrendDataMgr::GetTrendDataMgr( void )
{
	static TrendDataMgr TrendDataMgr_Manager_;
	return( TrendDataMgr_Manager_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPatientRepository_ 
//
//@ Interface-Description
//  Return a static instance of the trend patient repository agent
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbPatientRepository & TrendDataMgr::getPatientRepository_( void )
{
	static TrendDbPatientRepository   TrendDbPatient_Repository_;
	return( TrendDbPatient_Repository_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPatientRepository [Static] 
//
//@ Interface-Description
//  Return a static const instance of the Trend patient Repository
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the private getPatientRepository_() method
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TrendDbPatientRepository & TrendDataMgr::GetPatientRepository( void )
{
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	return( trendDataMgr.getPatientRepository_() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSettingRepository_
//
//@ Interface-Description
//  Return a static instance of the Trend Setting Repository
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbSettingRepository & TrendDataMgr::getSettingRepository_( void )
{
	static TrendDbSettingRepository   TrendDbSetting_Repository_;
	return( TrendDbSetting_Repository_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetSettingRepository [Static]
//
//@ Interface-Description
//  Return a static const instance of the Trend Setting Repository
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the private getSettingRepository_() method
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TrendDbSettingRepository & TrendDataMgr::GetSettingRepository( void )
{
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	return( trendDataMgr.getSettingRepository_() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getApplicability_
//
//@ Interface-Description
//  Return a static instance of the Trend Applicability
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbApplicability &  TrendDataMgr::getApplicability_( void )
{
	static TrendDbApplicability   TrendDbApplicability_;
	return( TrendDbApplicability_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetTrendDbApplicability [static]
//
//@ Interface-Description
//  Return a static const instance of the Trend Applicability
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the private getApplicability_() method
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TrendDbApplicability &  TrendDataMgr::GetApplicability( void )
{
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	return( trendDataMgr.getApplicability_() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getEventReceiver_
//
//@ Interface-Description
//  Return a static instance of the Trend Event Receiver
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbEventReceiver & TrendDataMgr::getEventReceiver_( void )
{
	static TrendDbEventReceiver TrendDbEventReceiver_;
	return( TrendDbEventReceiver_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetEventReceiver
//
//@ Interface-Description
//  Return a static const instance of the Trend Event Receiver
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the private getEventReceiver_() method
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TrendDbEventReceiver & TrendDataMgr::GetEventReceiver( void )
{
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	return( trendDataMgr.getEventReceiver_() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetControlBlock [static]
//
//@ Interface-Description
//  Return a static instance of the Control Block
//  TBD should be const
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the private getControlBlock_ method
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
const TrendDbControlBlock & TrendDataMgr::GetControlBlock( void )
{
	TrendDataMgr & trendDataMgr = GetTrendDataMgr();
	return( trendDataMgr.getControlBlock_() );
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getControlBlock_
//
//@ Interface-Description
//  Return a static instance of the Control Block
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendDbControlBlock & TrendDataMgr::getControlBlock_( void )
{
	static TrendDbControlBlock TrendDbControlBlock_;

	return( TrendDbControlBlock_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetReadBuffer [static]
//
//@ Interface-Description
//  Return the static memory used to read data from the compact flash
//---------------------------------------------------------------------
//@ Implementation-Description
//  All of the tables use the same read buffer and the manager will process
//  the trend request serially. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
MemPtr TrendDataMgr::GetReadBuffer( void )
{
	static CompactFlash::Block  ReadBuffer_Memory_[ TREND_READ_BUFFER_SIZE ];

	return( ReadBuffer_Memory_ );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: readRequest_
//
//@ Interface-Description
//  Wait on the message Queue for a request, pass in a reference to a
//  trendDataSet pointer, this value will be updated with the value of the
//  message and returned to the caller.  Also pass in a timeout to wait.
//  Returns the Ipc status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the Os-Foundation MsgQueue
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
Int32 TrendDataMgr::readRequest_( TrendDataSet* & pTrendDataSet, Uint16 timeout )
{
	// If no timeout is supplied, use min 10ms
	if (timeout < 10)
	{
		timeout = 10;
	}

	Int32 msg = 0;
	Int32 rtn = readRequestQ_.pendForMsg( msg, timeout );

	if (rtn == Ipc::OK)
	{
		pTrendDataSet = (TrendDataSet*) msg;
	}

	return( rtn );
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processRequest_
//
//@ Interface-Description
//  Process the Trend Data request given a TrendDataSet pointer, which has the
//  request information and will hold the trend data returned.  The Current
//  frame sequence count is given to calculate the offset of the request from
//  the current data in the trend database.
//  
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method will:
//  1. Validate/configure the client request
//  2. Pass request to TrendDbPatientRepository to process
//  3. Pass the request to TrendDbSettingRepository to process (if needed)
//  4. Signal the Gui that the data is ready
//---------------------------------------------------------------------
//@ PreCondition
//	if pTrendDataSet doesn't exists then just return
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::processRequest_( TrendDataSet * pTrendDataSet, Uint32 currentSeq )
{
	if (!pTrendDataSet)
	{
		TREND_MGR_DBG1( 1, "Invalid Trend Data Set" );
		return;
	}

	// Time the request performance
	OsTimeStamp requestPerformanceTime;
	requestPerformanceTime.now();

	// Load the request parameters/configuration from the data set
	// Default the position and request size
	TrendRequestInfo & reqInfo = pTrendDataSet->getRequestInfo_();
	reqInfo.position_ = 0;
	reqInfo.requestSize_ = MAX_ROWS; 

	TREND_MGR_DBG4( 4, "Manager process request, Start Frame ", reqInfo.seqNumber_,
					", start Index ", reqInfo.position_ ); 
	TREND_MGR_DBG4( 4, "  Timescale ", reqInfo.timescale_,
					", Req Cnt ", reqInfo.requestSize_ );
	TREND_MGR_DBG4( 4, "  Data Set Addr ", hex, (Uint32) pTrendDataSet, dec );  

	TrendDbPatientRepository & trendPatient = getPatientRepository_();

	// If the startSeq is provided then let's use this as the starting point
	Uint16 tableFactor = GetTableFrequency( reqInfo.timescale_ );
	if (reqInfo.seqNumber_ && reqInfo.seqNumber_ < currentSeq)
	{
		Uint16 startOffset = (currentSeq - reqInfo.seqNumber_ - 1);
		reqInfo.position_ = startOffset / tableFactor;
		TREND_MGR_DBG6( 4, "Converted seq/frame ", reqInfo.seqNumber_,
						", current frame ", currentSeq,
						", -> position ", reqInfo.position_ );
	}
	else
	{
		// Round the current seq/frame number for the given table
		Uint32 ndx = (currentSeq - 1) / tableFactor;
		reqInfo.seqNumber_ = ndx * tableFactor;

		TREND_MGR_DBG4( 4, "Use most current frame/seq ", reqInfo.seqNumber_,
						", position ", reqInfo.position_ );
	}

	// Set the most current flag in the dataset, based on position zero which
	// will always loaded most current data out of the repositories
	if (reqInfo.position_ == 0)
	{
		pTrendDataSet->setMostCurrent_( TRUE );
	}
	else
	{
		// Not most current
		pTrendDataSet->setMostCurrent_( FALSE );
	}
	TREND_MGR_DBG2( 4, "Set most current data flag ", pTrendDataSet->isMostCurrent() );

	// First load the patient data
	// This will load the frame sequence numbers for each of the rows
	// and the Auto/User events for each row.
	TrendDataSet::Status status = trendPatient.processRequest( pTrendDataSet );

	// If the request failed try again, if it fails again disable trending
	if (status == TrendDataSet::PROCESS_ERROR)
	{
		TREND_MGR_DBG1( 1, " Patient Repository load failed first try" );
		Task::Delay( 1, 0 );

		// Reinitialize the compact flash interface
		CompactFlash & cfInterface = CompactFlash::GetCompactFlash();
		cfInterface.open();
		status = trendPatient.processRequest( pTrendDataSet );
		if (status == TrendDataSet::PROCESS_ERROR)
		{
			TREND_MGR_DBG1( 1, "Patient Repository load failed second try" );
			Disable();
		}
	}
	TREND_MGR_DBG2( 4, "Patient Repository load status ", status );

	// Get the setting's trend data if needed (request has Setting parameters)
	TrendDbSettingRepository & trendSetting = getSettingRepository_();
	if (status == TrendDataSet::PROCESS_OK &&
		trendSetting.hasSettingIds( pTrendDataSet ))
	{
		status = trendSetting.processRequest( pTrendDataSet );

		// If there was an error try again before disabling trending
		if (status == TrendDataSet::PROCESS_ERROR)
		{
			TREND_MGR_DBG1( 1, "Setting Repository load failed first try" );
			Task::Delay( 1, 0 );

			// Reinitialize the compact flash interface
			CompactFlash & cfInterface = CompactFlash::GetCompactFlash();
			cfInterface.open();
			status = trendSetting.processRequest( pTrendDataSet );
			if (status == TrendDataSet::PROCESS_ERROR)
			{
				TREND_MGR_DBG1( 1, "Setting Repository load failed second try" );
				Disable();
			}
			TREND_MGR_DBG2( 4, "Setting Repository load status ", status );
		}
	}
	else
	{
		TREND_MGR_DBG1( 4, "Nothing to load from Settings" );
	}

	// Set the data set status and timescale
	// The data count is set by the TrendDbPatientRepository
	pTrendDataSet->setStatus_( status );
	pTrendDataSet->setTimescale_( reqInfo.timescale_ );

	// Post process the trend data set to support processing in the trend data agent
	// For any of the trend data values "columns" that have not been filled in by 
	// either repository copy over the trend select Id and set all of the data to gaps
	// Depends on the fact that the initialization has already gapped all of the data
	if( status != TrendDataSet::PROCESS_ERROR )
	{
		for( Int16 i = 0; i < TrendDataMgr::MAX_COLUMNS; i++ )
		{
			TrendDataValues & trendValues = pTrendDataSet->getTrendDataValues_( i );
			if( trendValues.getId() == TrendSelectValue::TREND_EMPTY )
			{
				trendValues.setId_( reqInfo.requestIds_[i] );
				TREND_MGR_DBG2( 8, "No data for Id ",  reqInfo.requestIds_[i] );	 							  
			}
		}
	}

	// Output the time in ms
	TREND_MGR_DBG2( 2, "Request performance time, ",
					requestPerformanceTime.getPassedTime() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: sampleTrend_
//
//---------------------------------------------------------------------
//@ Interface-Description
//  Sample the Patient and Setting values and update the database repositories
//---------------------------------------------------------------------
//@ Implementation-Description
//  First check to see if a System Context Change happened, if so force a 
//  refresh of all the trend data.  The sample the settings and patient data
//  repositories and events.  Then update the frame sequence number.  
//  The last step is to flush the control block if this fails try again.
// 	Because all of the table updates are flush at the end in the control block
//  the failure will only lose the last update.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::sampleTrend_( Uint32 seqNumber, const TimeStamp & trendSampleTime )
{
	// Time the Sample processing time
	OsTimeStamp samplePerformanceTime;
	samplePerformanceTime.now();

	TREND_MGR_DBG2( 4, "Manager Collect Sample Seq ", seqNumber );
	TREND_MGR_DBG6( 4, "Date/Time: ", trendSampleTime.getHour(),
					":", trendSampleTime.getMinutes(),
					":", trendSampleTime.getSeconds() );

	// Get read/write access to the agents
	TrendDbSettingRepository & trendSetting = getSettingRepository_();
	TrendDbPatientRepository & trendPatient = getPatientRepository_();
	TrendDbApplicability & trendApplicability = getApplicability_();

	// If the system context has changed then refresh the agents
	if (isSystemContextChange_)
	{
		TREND_MGR_DBG1( 4, "System Context Change - refresh data" );
		trendSetting.resyncAll();
		trendPatient.resyncAll();
		trendApplicability.updateSettings();

		isSystemContextChange_ = FALSE;
	}

	// Sample settings
	trendSetting.sample( seqNumber, trendSampleTime );

	// Frezze the current set of events and values, any other updates to events
	// after this will go into the next frame
	TrendDbEventReceiver & trendEventReceiver = getEventReceiver_();
	trendEventReceiver.freezeEvents();

	// Sample patient values and also the User and Auto Events
	trendPatient.sample( seqNumber, trendSampleTime );

	// Clear out the "frozen" user and auto event's
	trendEventReceiver.clearEvents();

	// Increment seqNumber for next sample
	setNextSequenceCount_();

	// Flush the control block
	TrendDbControlBlock & trendControlBlock = getControlBlock_();
	if (!trendControlBlock.flush())
	{
		// Try one more time before Disable trending
		TREND_MGR_DBG1( 1, "Control Block failed to flush after sample, first try" );
		Task::Delay( 1, 0 );

		// Reinitialize the compact flash interface
		CompactFlash & cfInterface = CompactFlash::GetCompactFlash();
		cfInterface.open();
		if (!trendControlBlock.flush())
		{
			TREND_MGR_DBG1( 1, "Control Block failed to flush after sample, second try" );
			TrendDataMgr::Disable();
		}
	}

	// Output performance in ms
	TREND_MGR_DBG2( 2, "Sample performance time, ",
					samplePerformanceTime.getPassedTime() );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: signalGuiDataReady_
//
//---------------------------------------------------------------------
//@ Interface-Description
//  Send the data ready Event to the GUI on the UserAnnunciation Queue
//---------------------------------------------------------------------
//@ Implementation-Description
//  Address of the TrendDataSet is passed in the lower 24 bits of the message
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::signalGuiDataReady_( Uint32 dataSetAddr )
{

	TREND_MGR_DBG4( 4, "Signal GUI that data is ready Addr ",
					hex, dataSetAddr, dec );

	UserAnnunciationMsg msg;
	msg.trendDataReadyParts.eventType = UserAnnunciationMsg::TREND_DATA_READY_EVENT; 
	msg.trendDataReadyParts.dataSetAddr = dataSetAddr & 0xFFFFFF;
	MsgQueue::PutMsg( USER_ANNUNCIATION_Q, msg.qWord );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: signalGuiManagerStatus_
//
//---------------------------------------------------------------------
//@ Interface-Description
//  Send the manager status Event to the GUI on the UserAnnunciation Queue
//---------------------------------------------------------------------
//@ Implementation-Description
//  isEnabled   -   Manager is enabled and processing request
//  isRunning   -   Manager is running and collection trend data
// 
// Satisfies requirement: 
//  [TR01194] ... Trending option is not possible or is not available ...
// 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendDataMgr::signalGuiManagerStatus_( void )
{
	Boolean isEnabled = IsEnabled();
	Boolean isRunning = isState_( SAMPLE_RUNNING );

	TREND_MGR_DBG4( 4, "Signal GUI Manager status - enabled ", isEnabled,
					", running ", isRunning );

	UserAnnunciationMsg msg;
	msg.trendMgrStatusParts.eventType = UserAnnunciationMsg::TREND_MANAGER_STATUS_EVENT;
	msg.trendMgrStatusParts.isEnabled = isEnabled;
	msg.trendMgrStatusParts.isRunning = isRunning;
	MsgQueue::PutMsg( USER_ANNUNCIATION_Q, msg.qWord );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ResetDatabase [static]
//
//@ Interface-Description
//  Empty the database
//---------------------------------------------------------------------
//@ Implementation-Description
//  Resets the Patient and Setting repositories back to empty
//  Only valid if the data collection is suppended
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================

void TrendDataMgr::ResetDatabase(void)
{

	TREND_MGR_DBG1( 2, "TrendDataMgr Reset database" );

	TrendDataMgr & trendDataMgr = GetTrendDataMgr();

	if (!TrendDataMgr::IsInitialized())
	{
		// Initialize the repositories, if successful the state will be updated
		// to INITIALIZED_IDLE.  If Trending is not enabled then update the state
		// to NO_TREND_OPTION and if an error is detected update the state to ERROR
		trendDataMgr.initRepositories_();

	}


	// Reset the patient and setting repository
	TrendDbPatientRepository & patientRep = trendDataMgr.getPatientRepository_();
	TrendDbSettingRepository & settingRep = trendDataMgr.getSettingRepository_();
	patientRep.resetDatabase();
	settingRep.resetDatabase();

	// Reinitialize the current frame number, this will force it
	// to a even boundary for all of the patient data tables so that
	// the first sample in the new data will have the correct number
	// of frames in its average
	trendDataMgr.initializeSequenceCount_();

	// Flush the control block, try again if not successful 
	TrendDbControlBlock & controlBlock = trendDataMgr.getControlBlock_();
	if (!controlBlock.flush())
	{
		TREND_MGR_DBG1( 2, "Reset, failed to flush control block first try" );

		// Wait and reset the Compact Flash interface
		Task::Delay( 1, 0 );
		CompactFlash & cfInterface = CompactFlash::GetCompactFlash();
		if (cfInterface.open() != SUCCESS || controlBlock.flush() != SUCCESS)
		{
			TREND_MGR_DBG1( 1, "Reset, failed to flush control block" );
			Disable();
		}
	}


}


