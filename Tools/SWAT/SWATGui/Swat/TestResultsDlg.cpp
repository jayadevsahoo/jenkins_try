// TestResultsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Swat.h"
#include "TestResultsDlg.h"

#include <math.h>
#include <time.h>


// CTestResultsDlg dialog

IMPLEMENT_DYNAMIC(CTestResultsDlg, CDialog)

CTestResultsDlg::CTestResultsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTestResultsDlg::IDD, pParent)
	, m_strResults(_T(""))
	, m_strComboVar(_T(""))
{
	m_Plots=new Plots[MAX_X];
	m_nElement = 0;
	m_nAnnotation = 0;
}



CTestResultsDlg::~CTestResultsDlg()
{
	delete m_Plots;
}

void CTestResultsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NTGRAPHCTRL, m_Graph);
	DDX_Text(pDX, IDC_EDIT_RESULTS, m_strResults);
	DDX_CBString(pDX, IDC_COMBO, m_strComboVar);
	DDX_Control(pDX, IDC_COMBO, m_comboCtrl);
	DDX_Control(pDX, IDC_LIST_RESULTS, m_List);
}


BEGIN_MESSAGE_MAP(CTestResultsDlg, CDialog)
	ON_BN_CLICKED(IDC_RESET_GRAPH, &CTestResultsDlg::OnBnClickedResetGraph)
	ON_BN_CLICKED(IDC_BUTTON_PLOT_GRAPH, &CTestResultsDlg::OnBnClickedButtonPlotGraph)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_RESULTS, &CTestResultsDlg::OnCustomDrawList1)
	ON_BN_CLICKED(IDC_BUTTON_OPEN_REPORT, &CTestResultsDlg::OnBnClickedButtonOpenReport)
	ON_BN_CLICKED(IDC_BUTTON_REFRESH, &CTestResultsDlg::OnBnClickedButtonRefresh)
END_MESSAGE_MAP()


// CTestResultsDlg message handlers

BOOL CTestResultsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_List.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);


	// Customize Graph Properties 
	m_Graph.SetAxisColor (RGB(0,255,0));
	m_Graph.SetLabelColor (RGB(0,0,0));

	// Control's Frame and Plot area options
	m_Graph.SetPlotAreaColor(RGB(212,222,200));
	m_Graph.SetFrameStyle(1); // (1) - Flat
							 // (2) - Scope (raised frame and sunken plot area borders)
							 // (3) - 3DFrame (a bitmap frame picture)
	    
	m_Graph.SetGridColor(RGB(192,192,192));
	m_Graph.SetShowGrid (TRUE);

	m_Graph.SetCursorColor (RGB(255,0,0));
	m_Graph.SetTrackMode (3);

	m_Graph.SetXLabel ("Time");
	
	

	

	m_Graph.AddAnnotation();

	//SetTimeLimit(CTime(2009,12,15,   14,51,00), 
	//			 CTime(2009,12,15,   14,51,30));

	m_strComboVar = "circuitPressure";
	UpdateData(0);

	CWaitCursor wait;
	PlotGraph();

	

	// TODO:  Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTestResultsDlg::SetTimeLimit(CTime startTime, CTime stopTime)
{

}

void CTestResultsDlg::SetResultParams(CString strSessionID, CString strScriptName)
{
	m_strResultsSessionID = strSessionID;
	m_strResultsScriptName = strScriptName;

}


void CTestResultsDlg::PlotGraph()
{
	InsertColumns();
	PopulateList();

	GetElementReady(0);
	FillGraphTable("WAVEFORM_BREATH_DATA", "circuitPressure");
	DrawGraphTable(true);

	
}

void CTestResultsDlg::PopulateList()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();

	CString strFilter;
	
	strFilter.Format("test_session_id = '%s' AND filename = '%s'", 
		m_strResultsSessionID, 
		m_strResultsScriptName);

	CString strCaption;
	strCaption = m_strResultsScriptName;
	strCaption.Replace("\\\\", "\\");
	strCaption = "Test Results for " + strCaption;
	m_Graph.SetCaption(strCaption);

	
	pApp->m_TestResultSet.m_strFilter= strFilter;
	pApp->m_TestResultSet.m_strSort = "test_result_id DESC";

	if(!pApp->m_TestResultSet.Requery())
	{
		AfxMessageBox("ERROR : Can not requery test result set.");
		return;
	}

	int nIndex = 0; CString strLine;
	m_start_time = 0;
	m_stop_time = 0;

	while(!pApp->m_TestResultSet.IsEOF())
	{
			CString strTestData = pApp->m_TestResultSet.m_test_data;
			if(strTestData.Find(".")!= -1) // truncating to 2 decimal places
			{
				strTestData = strTestData.Left(strTestData.Find(".")+3);
			}

			if(!pApp->m_TestResultSet.m_test_status.CompareNoCase("Pass") || 
				!pApp->m_TestResultSet.m_test_status.CompareNoCase("Fail") )
			{
				strLine.Format("%-20s %-17s %-15s %-17.2f %-17.2f %-15s\r\n\r\n",
				pApp->m_TestResultSet.m_test_data_type,
				strTestData,
				pApp->m_TestResultSet.m_test_data_units,
				pApp->m_TestResultSet.m_test_data_min,
				pApp->m_TestResultSet.m_test_data_max,
				pApp->m_TestResultSet.m_test_status);


				m_List.InsertItem(nIndex, pApp->m_TestResultSet.m_test_data_type);
				m_List.SetItemText(nIndex, 1, strTestData);
				m_List.SetItemText(nIndex, 2, pApp->m_TestResultSet.m_test_data_units);
				strLine.Format("%5.2f",pApp->m_TestResultSet.m_test_data_min);
				m_List.SetItemText(nIndex, 3, strLine);
				strLine.Format("%5.2f",pApp->m_TestResultSet.m_test_data_max);
				m_List.SetItemText(nIndex, 4, strLine);
				m_List.SetItemText(nIndex, 5, pApp->m_TestResultSet.m_test_status);
				m_List.SetItemText(nIndex, 6, pApp->m_TestResultSet.m_test_comment);


			}
			else //for "info" or TODO type result, don't show min and max
			{
				strLine.Format("%-20s %-17s %-15s %-17s %-17s %-15s\r\n\r\n",
				pApp->m_TestResultSet.m_test_data_type,
				strTestData,
				pApp->m_TestResultSet.m_test_data_units,
				" " ,
				" ",
				pApp->m_TestResultSet.m_test_status);


				//Populate the list ctrl
				m_List.InsertItem(nIndex, pApp->m_TestResultSet.m_test_data_type);
				m_List.SetItemText(nIndex, 1, strTestData);
				m_List.SetItemText(nIndex, 2, pApp->m_TestResultSet.m_test_data_units);
			//	strLine.Format("%5.2f",pApp->m_TestResultSet.m_test_data_min);
			//	m_List.SetItemText(nIndex, 3, strLine);
			//	strLine.Format("%5.2f",pApp->m_TestResultSet.m_test_data_max);
			//	m_List.SetItemText(nIndex, 4, strLine);
				m_List.SetItemText(nIndex, 5, pApp->m_TestResultSet.m_test_status);
				m_List.SetItemText(nIndex, 6, pApp->m_TestResultSet.m_test_comment);

			}
		m_start_time = pApp->m_TestResultSet.m_test_start_time;
		m_stop_time = pApp->m_TestResultSet.m_test_stop_time;
		pApp->m_TestResultSet.MoveNext();	
	}
	


}

void CTestResultsDlg::GetElementReady(int nElement)
{
	// Customize Graph Elements 
   // The Graph elements are dynamically allocated! 
   // Element 0 is allocated by default
   // Even after a call to the ClearGraph method,
   // the Element-0  is automaticaly allocated.
  
	if(nElement == 0)
	{
		m_Graph.SetElementLineColor(RGB(255,0,0));
		m_Graph.SetElementLinetype(0);
		m_Graph.SetElementWidth(1);

		m_Graph.SetElementPointColor(RGB(0,0,0));
		m_Graph.SetElementPointSymbol(3);
		m_Graph.SetElementSolidPoint(TRUE);


		//Allocate new annotation
//		

		m_Graph.SetAnnotation(0);
		m_Graph.SetAnnoLabelX(-1);
		m_Graph.SetAnnoLabelY(0);
		m_Graph.SetAnnoLabelBkColor(RGB(255, 0, 0));
	
		m_nElement = 0;

	}
	else
	{
		// Allocate a new element: Element-1
		m_Graph.AddElement();
		
		m_Graph.SetElement(nElement);

		m_Graph.SetElementLinetype(2);
		 
		m_Graph.SetElementLinetype(0);
		m_Graph.SetElementLineColor(RGB(255,0,0));
		m_Graph.SetElementPointColor(RGB(0, 0, 0));
		m_Graph.SetElementPointSymbol(1);
		m_Graph.SetElementWidth(1);

		m_nElement = nElement;
	}

	

}


void CTestResultsDlg::FillGraphTable(CString strTable, CString strParam)
{
	CWaitCursor wait;
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString strSQL;

	m_Graph.SetAnnoLabelCaption(strParam);
	m_Graph.SetElementName(strParam);

	m_Graph.SetYLabel(strParam);



/*
DELETE FROM "TestResults".test_graph;
INSERT INTO "TestResults".test_graph(
            "time", "value")
    (  SELECT foo.event_time,  pd."exhaledMinuteVol"
        from (SELECT event_sequence_id, event_descriptor, event_time, save_data
  FROM "PatientData".event_data  where event_time BETWEEN '2009-12-10 16:01:59' AND '2009-12-30 16:01:59') 
  as foo, "PatientData"."AVERAGE_BREATH_DATA" as pd where
  foo.event_sequence_id = pd.event_sequence_id )
;
*/
	
/*	strSQL = "INSERT INTO \"TestResults\".test_graph(\"time\", \"value\") "
		"(  SELECT foo.event_time,  pd.\"exhaledMinuteVol\" from "
		"(SELECT event_sequence_id, event_descriptor, event_time, save_data FROM \"PatientData\".event_data "
		" where event_time BETWEEN '2009-12-10 16:01:59' AND '2009-12-30 16:01:59') "
		"as foo, \"PatientData\".\"AVERAGE_BREATH_DATA\" as pd where "
		" foo.event_sequence_id = pd.event_sequence_id )";
*/
		
		 

/*for waveforms table

INSERT INTO "TestResults".test_graph(
            "time", "value")

(SELECT foo.event_time,  pd."circuitPressure"
        from (SELECT event_sequence_id, event_descriptor, event_time, save_data
  FROM "PatientData".event_data  where event_time BETWEEN '2009-12-10 16:01:59' AND '2009-12-30 16:01:59'  ) 
  as foo, "PatientData"."WAVEFORM_BREATH_DATA" as pd where
  foo.event_sequence_id = pd.event_sequence_id ORDER BY pd.event_sequence_id ASC, pd.event_index ASC)
*/


	if(strTable.CompareNoCase("WAVEFORM_BREATH_DATA") == 0)
	{//for waveform table data comes 200 times per second, need to order by the event_index
		strSQL.Format("INSERT INTO \"TestResults\".test_graph(\"time\", \"value\")"	
		"(SELECT foo.event_time, pd.\"%s\" from "
		"(SELECT event_sequence_id, event_descriptor, event_time, save_data FROM "
		"\"PatientData\".event_data  where event_time BETWEEN '%s' AND '%s' "
		" ) as foo, \"PatientData\".\"%s\" as pd where "
		"foo.event_sequence_id = pd.event_sequence_id "
		"ORDER BY pd.event_sequence_id ASC, pd.event_index ASC)", 

		strParam,
		m_start_time.Format("%Y-%m-%d %H:%M:%S"), 
		m_stop_time.Format("%Y-%m-%d %H:%M:%S"), 
		strTable);

	}
	else
	{
		strSQL.Format( "INSERT INTO \"TestResults\".test_graph(\"time\", \"value\") "
		"(  SELECT foo.event_time,  pd.\"%s\" from "
		"(SELECT event_sequence_id, event_descriptor, event_time, save_data FROM \"PatientData\".event_data "
		" where event_time BETWEEN '%s' AND '%s' order by event_sequence_id) "
		"as foo, \"PatientData\".\"%s\" as pd where "
		" foo.event_sequence_id = pd.event_sequence_id)", 
		
		strParam, 
		m_start_time.Format("%Y-%m-%d %H:%M:%S"), 
		m_stop_time.Format("%Y-%m-%d %H:%M:%S"), 
		strTable) ;
	}

	CWaitCursor wait1;

	try
	{
		pApp->m_TestGraphset.m_pDatabase->ExecuteSQL("DELETE FROM \"TestResults\".test_graph");
		pApp->m_TestGraphset.m_pDatabase->ExecuteSQL(strSQL);
		pApp->m_TestGraphset.Requery();

	}
	catch(CDBException* pe)
	{
	   // The error code is in pe->m_nRetCode
	   pe->ReportError();
	   pe->Delete();
	}

	
//	AfxMessageBox("SQL query executed.");


}
	

void CTestResultsDlg::DrawGraphTable(BOOL bWaveForm)
{
	
	CWaitCursor wait;
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	
	if(bWaveForm)
	{
		long lIndex = 0;
		CString strTemp, str;
		if(!pApp->m_TestGraphset.IsBOF())
			pApp->m_TestGraphset.MoveFirst();
		double yMin(0), yMax(0);

		m_graphStart = (double)pApp->m_TestGraphset.m_time.GetTime();
		double timeX(0), n(0);
		while(!pApp->m_TestGraphset.IsEOF())	
		{
			timeX = timeX + 0.005;
			m_Graph.PlotXY(timeX, pApp->m_TestGraphset.m_value, m_nElement);

			//get the min and max Y to scale the graph accordingly
			if(pApp->m_TestGraphset.m_value < yMin)
			{
				yMin = pApp->m_TestGraphset.m_value;
			}
			else if( pApp->m_TestGraphset.m_value > yMax)
			{
				yMax = pApp->m_TestGraphset.m_value; 
			}


			pApp->m_TestGraphset.MoveNext();
			n++;
	
		}

		m_strResults = str;
		m_Graph.SetRange(timeX-0.5, timeX, yMin-0.5, yMax+0.5);
	}
	else
	{
	}

	//	AutoRange();
	

}




void CTestResultsDlg::OnBnClickedResetGraph()
{
	m_Graph.AutoRange();
	
}



void CTestResultsDlg::OnBnClickedButtonPlotGraph()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	UpdateData(1);
	GetDlgItem(IDC_COMBO)->GetWindowText(m_strComboVar);
	
	m_Graph.ClearGraph();
	pApp->m_TestGraphset.Requery();
	GetElementReady(0);
	FillGraphTable("WAVEFORM_BREATH_DATA", m_strComboVar);
	//FillGraphTable("BDSIGNAL", m_strComboVar);
	DrawGraphTable(true);
	
}


void CTestResultsDlg::InsertColumns()
{
	LV_COLUMN Column;
	int nCol;

	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	CString szText;
	
	szText = "Test Parameter";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	Column.cx = 250;//120;
	nCol = m_List.InsertColumn( 0, &Column );


	Column.mask = LVCF_TEXT|LVCF_WIDTH ;
	
	szText = "Actual Value";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	Column.cx = 100;
	nCol = m_List.InsertColumn( 1, &Column );

	szText = "Units";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	Column.cx = 0;//80;
	nCol = m_List.InsertColumn( 2, &Column );

	szText = "Expected Min. Value";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	Column.cx = 0;//100;
	nCol = m_List.InsertColumn( 3, &Column );

	szText = "Expected Max. Value";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	Column.cx = 0;//100;
	nCol = m_List.InsertColumn( 4, &Column );

	szText = "Test Result";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	Column.cx = 100;
	nCol = m_List.InsertColumn( 5, &Column );


	szText = "Comments";
	Column.pszText = (TCHAR*)(LPCTSTR)szText;
	Column.cchTextMax = szText.GetLength();
	Column.cx = 200;
	nCol = m_List.InsertColumn( 6, &Column );

}

void CTestResultsDlg::OnCustomDrawList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;
	if (lplvcd->nmcd.dwDrawStage == CDDS_PREPAINT)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	
	else if (lplvcd->nmcd.dwDrawStage == CDDS_ITEMPREPAINT)

	{
		if(m_List.GetItemText(lplvcd->nmcd.dwItemSpec, 5).CompareNoCase("PASS") == 0 )
		{
            lplvcd->clrText = RGB(0, 120, 0);
            *pResult = CDRF_NEWFONT;
		}
		else if(m_List.GetItemText(lplvcd->nmcd.dwItemSpec, 5).CompareNoCase("FAIL") == 0 )
		{
            lplvcd->clrText = RGB(255, 0, 0);
            *pResult = CDRF_NEWFONT;
		}
		else
		{
			// no change; tell the control to do the default
			*pResult = CDRF_DODEFAULT;
        }
	}
	else
	{
		// it wasn't a notification that was interesting to us.
		// just return zero.
		*pResult = 0;
	}
}

void CTestResultsDlg::OnBnClickedButtonOpenReport()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	CString strFileName = m_strResultsScriptName;
	strFileName.Replace("\\\\", "\\");
	if(strFileName.Right(4).CompareNoCase(".lua") == 0)
	{
		CString strLogFile = pApp->GetReportFileName(strFileName);
		FILE * pFile;
		pFile = fopen (strLogFile,"r");
		if (pFile!=NULL)
		{
			// file exists.  close and open in explorer
			fclose (pFile);
			ShellExecute(0, "open", "iexplore.exe", strLogFile, 0, SW_SHOW);
		}
		else 
		{
			AfxMessageBox("Report not found: \n" + strLogFile);
		}
	}
	
}

void CTestResultsDlg::OnBnClickedButtonRefresh()
{


	m_List.DeleteAllItems();
	PopulateList();
}
