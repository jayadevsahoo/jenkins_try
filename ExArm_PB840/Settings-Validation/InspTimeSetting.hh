
#ifndef InspTimeSetting_HH
#define InspTimeSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  InspTimeSetting - Inspiratory Time Setting.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/InspTimeSetting.hhv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: sah   Date:  22-Jul-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//
//  Revision: 005   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new 'getApplicability()' method
//	*  now an observer of constant parm setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden from
//	   observer base class
//
//  Revision: 004   By: dosman Date:  29-Apr-1998    DR Number: 4
//  Project:  BILEVEL
//  Description:
//	change back to a SingleIntervalRange
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: dosman Date:  23-Feb-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Added updateAbsValues_() (see Method Description for details)
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class InspTimeSetting : public BatchBoundedSetting, public SettingObserver
{
  public:
    InspTimeSetting(void); 
    virtual ~InspTimeSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    virtual const BoundStatus*  acceptPrimaryChange(
				    const SettingId::SettingIdType primaryId
						   );

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
				   const DiscreteValue            newValue,
				   const DiscreteValue            currValue);

    // SettingObserver methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

    virtual void  settingObserverInit(void);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif // defined (SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual Boolean calcDependentValues_(const Boolean isValueIncreasing);
    virtual void  updateConstraints_  (void);

    virtual BoundedValue  calcBasedOnSetting_(
			    const SettingId::SettingIdType basedOnSettingId,
			    const BoundedRange::WarpDir    warpDirection,
			    const BasedOnCategory          basedOnCategory
					     ) const;

  private:
    InspTimeSetting(const InspTimeSetting&);	// not implemented...
    void  operator=(const InspTimeSetting&);	// not implemented...
};


#endif // InspTimeSetting_HH 
