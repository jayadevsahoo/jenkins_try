/*****************************************************************************/
/*   Module Name:    Common.h                                          
/*   Purpose:      Purpose                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _COMMON_H
#define _COMMON_H

typedef int Bool;

#define OK_840        0
#define FAIL_840      1  

#define YES_840       1
#define NO_840        0

#define COM_840_ACTIVE   1
#define COM_840_INACTIVE 0

#define PTS_ERROR 1
#define PTS_OK    0

#define FILE_ERROR_840   1

// Common Color Schemes
#define VAL_OUR_ORANGE	0x00FFCE63 
#define VAL_OUR_YELLOW	VAL_YELLOW
#define VAL_OUR_RED		VAL_RED
#define VAL_OUR_GREEN	VAL_GREEN

#endif // #ifndef _COMMON_H
