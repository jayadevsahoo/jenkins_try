#ifndef SummationBuffer_HH
#define SummationBuffer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SummationBuffer - class to sum data in a buffer.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/SummationBuffer.hhv   10.7   08/17/07 09:43:42   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By: rhj     Date:  27-Oct-2010    SCR Number: 6694
//  Project:  PROX
//  Description:
//       Added getCurrentAverage() and getCurrentSum()
// 
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  05-Mar-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

//@ End-Usage

class SummationBuffer {
  public:
  	SummationBuffer( Real32 *pBuffer, Uint32 bufferSize) ;
    ~SummationBuffer( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
				 const Uint32      lineNumber,
				 const char*       pFileName  = NULL, 
				 const char*       pPredicate = NULL) ;

	inline void reset( void) ;
	inline Real32 getSum( void) const ;
	inline Uint32 getNumSamples( void) const ;
	inline Uint32 getBufferSize( void) const ;
	inline Boolean isBufferFull( void) const ;

	virtual void updateBuffer( const Real32 data) ;
	Real32 getAverage( void) const ;
	Real32 getCurrentAverage( void) ;
	Real32 getCurrentSum( void) ;
		
  protected:

  private:
    SummationBuffer( void) ;						// not implemented...
    SummationBuffer( const SummationBuffer&) ;  	// not implemented...
    void   operator=( const SummationBuffer&) ; 	// not implemented...

	//@ Data-Member: *pBuffer_
	// pointer to the buffer to be summed
	Real32 *pBuffer_ ;

	//@ Data-Member: bufferSize_
	// buffer size
	Uint32 bufferSize_ ;

	//@ Data-Member: index_
	// index to buffer
	Uint32 index_ ;

	//@ Data-Member: sum_ ;
	// sum of the data in buffer
	Real32 sum_ ;

	//@ Data-Member: isBufferFull_
	// set if buffer is full
	Boolean isBufferFull_ ;

} ;

#include "SummationBuffer.in"

#endif // SummationBuffer_HH 

