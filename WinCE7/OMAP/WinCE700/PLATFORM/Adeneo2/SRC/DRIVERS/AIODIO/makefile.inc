_TGTPROJSDKROOT=$(_WINCEROOT)\OSDesigns\$(_TGTPROJ)\SDKs
_SOCCOMMON=$(_PLATFORMROOT)\common\src\soc\common_ti_V1

CopySDK:
	if not EXIST $(_TGTPROJSDKROOT)\ExSdkInc mkdir $(_TGTPROJSDKROOT)\ExSdkInc
	xcopy /I /D /Q    AioDio.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_PLATFORMROOT)\common\src\soc\common_ti_V1\common_ti\INC\omap_mcspi_regs.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_TARGETPLATROOT)\src\app\common\i2cproxy.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_WINCEROOT)\PUBLIC\COMMON\SDK\INC\winnetwk.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_TARGETPLATROOT)\src\inc\bsp_version.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q /S $(_WINCEROOT)\3rdParty\GL\*.* $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_PUBLICROOT)\common\oak\inc\pwinuser.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_SOCCOMMON)\common_ti\INC\sdk_spi.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_SOCCOMMON)\common_ti\INC\gpio_ioctls.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_WINCEROOT)\3rdParty\FTDI\ftd2xx.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_WINCEROOT)\3rdParty\FTDI\libMPSSE_i2c.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_WINCEROOT)\3rdParty\FTDI\libMPSSE_spi.h $(_TGTPROJSDKROOT)\ExSdkInc > nul
	xcopy /I /D /Q    $(_WINCEROOT)\3rdParty\FTDI\libMPSSE.lib $(_TGTPROJSDKROOT)\ExSdkInc\lib > nul
