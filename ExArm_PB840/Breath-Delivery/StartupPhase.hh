
#ifndef StartupPhase_HH
#define StartupPhase_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: StartupPhase - A placeholder for a breath, used during the powerup
//								 sequence.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/StartupPhase.hhv   25.0.4.0   19 Nov 2013 14:00:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  30-Nov-1994    DR Number: 
//		Project:  Sigma (R8027)
//		Description:
//			Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

#include "BreathPhase.hh"

//@ End-Usage

class StartupPhase : public BreathPhase
{
  public:
    StartupPhase( void) ;
    virtual ~StartupPhase( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
				   const Uint32      lineNumber,
				   const char*       pFileName  = NULL, 
				   const char*       pPredicate = NULL);

    virtual void relinquishControl( const BreathTrigger& trigger) ;
    virtual void newCycle( void) ;
    virtual void newBreath( void) ;

  protected:

  private:
    StartupPhase(const StartupPhase&) ;			// not implemented...
    void   operator=( const StartupPhase&) ;	// not implemented...

};


#endif // StartupPhase_HH 
