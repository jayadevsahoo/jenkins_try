#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an processing retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//              Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================== C L A S S   D E S C R I P T I O N ================
//@ Class:  SstTestResultData -  Manage the SST service test data to be
//                            saved into or loaded from NovRam.
//---------------------------------------------------------------------
//@ Interface-Description
//>Von
// 
//  Description:
//  ------------
//    This class supports the following services for NovRam through the parent
//    class.
//    1. Loads the previous test result data from NovRam for upper level
//       application (GuiApp) usage.
//    2. Informs the test set completion to NovRam and updates the information.
//
//  Class Key Methods:
//  -------------------
//     1. loadDataFromNovRam -- Request NovRam to fill in the test data result.
//	   2. testCompleted -- Called by upper level applications to inform the
//        NovRam the completion of the test set.
//>Voff
//---------------------------------------------------------------------
//@ Rationale
//    The purpose of this derived class is to provide test specific functions
//    that have to be handle for the particular test type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  To support the information accessibility, SstTestResultData has two
//  important data members: testInfo_ and testDataArray_.
//  The 'testInfo_' is an array of items which knows about the 'count' of each
//  test ID and the starting address of the first data element within the
//  'testDataArray_'.
//  The 'testDataArray_' is an array of test data for SST tests.
// 
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/SstTestResultData.ccv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: mnr   Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX SST related updates.
// 
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "limits.h"
#include "SstTestResultData.hh"
#include "SmTestId.hh"
#include "NovRamManager.hh"
#include "FaultHandler.hh"

//@ Code...
//====================================================================
//
//  Public Methods...
//

//====================================================================
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SstTestResultData()  [Default Constructor]
//
//@ Interface-Description
//  Initializes all the private and inherited member variables.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize testDataArray_ and testInfo_ array elements values. 
//---------------------------------------------------------------------
//@ PreCondition
//	 none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================

SstTestResultData::SstTestResultData(void) :
		TestResultData(testInfo_, testDataArray_, NUM_SUMMARIZED_TEST_DATA_ITEM)
{
	CALL_TRACE("SstTestResultData::SstTestResultData(void)");

	initializeDataArray_();

// Initialize internal pointers into the array of test result data 
	testInfo_[SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID].pData =
			&testDataArray_[0] + CIRCUIT_RESISTANCE_TEST_START_INDEX;
	testInfo_[SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID].count =
			CIRCUIT_RESISTANCE_TEST_END_INDEX - CIRCUIT_RESISTANCE_TEST_START_INDEX +1;

	testInfo_[SmTestId::SST_TEST_EXPIRATORY_FILTER_ID].pData = 
			&testDataArray_[0] + SST_FILTER_TEST_START_INDEX; 
	testInfo_[SmTestId::SST_TEST_EXPIRATORY_FILTER_ID].count = 
			SST_FILTER_TEST_END_INDEX - SST_FILTER_TEST_START_INDEX +1;

	testInfo_[SmTestId::SST_TEST_CIRCUIT_LEAK_ID].pData = 
			&testDataArray_[0] + SST_LEAK_TEST_START_INDEX;
	testInfo_[SmTestId::SST_TEST_CIRCUIT_LEAK_ID].count = 
			SST_LEAK_TEST_END_INDEX - SST_LEAK_TEST_START_INDEX +1;


	testInfo_[SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID].pData		= 
						&testDataArray_[0] + CIRCUIT_PRESSURE_TEST_START_INDEX;
	testInfo_[SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID].count = 
						CIRCUIT_PRESSURE_TEST_END_INDEX - 
						CIRCUIT_PRESSURE_TEST_START_INDEX +1;

	testInfo_[SmTestId::SST_TEST_FLOW_SENSORS_ID].pData = 
			&testDataArray_[0] + SST_FS_CC_TEST_START_INDEX; 
	testInfo_[SmTestId::SST_TEST_FLOW_SENSORS_ID].count = 
			SST_FS_CC_TEST_END_INDEX - SST_FS_CC_TEST_START_INDEX +1;

	testInfo_[SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID].pData = 
			&testDataArray_[0] + COMPLIANCE_CALIB_TEST_START_INDEX;
	testInfo_[SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID].count = 
					COMPLIANCE_CALIB_TEST_END_INDEX - COMPLIANCE_CALIB_TEST_START_INDEX +1;

	testInfo_[SmTestId::SST_TEST_PROX_ID].pData = 
			&testDataArray_[0] + PROX_FLOW_TEST_START_INDEX;
	testInfo_[SmTestId::SST_TEST_PROX_ID].count = 
					PROX_FLOW_TEST_END_INDEX - PROX_FLOW_TEST_START_INDEX +1;

													// $[TI]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	initializeDataArray_ [private]
//
//@ Interface-Description
//  Called to set the initial values of elements of testDataArray_ 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Sets the initial values of testDataArray_ elements to INT_MIN. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
SstTestResultData::initializeDataArray_(void)
{
	CALL_TRACE("SstTestResultData::initializeDataArray_(void)");

	// Initialize test data array
	for (int i=0; i<NUM_SUMMARIZED_TEST_DATA_ITEM; i++)
	{
    	testDataArray_[i] = INT_MIN;	// Minimum limit on integers, which is
										// the same size as floats on mc6800
	}												// $[TI1]
													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SstTestResultData  [Destructor]
//
//@ Interface-Description
//  Empty
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SstTestResultData::~SstTestResultData(void)
{
	CALL_TRACE("SstTestResultData::~SstTestResultData(void)");
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	operator=
//
//@ Interface-Description
//  Assignment operator.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First, we copy the content of the testDataArray_ from the source object
//  to this object's data storage.  Then we copy
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================
SstTestResultData& 
SstTestResultData::operator=(const SstTestResultData &sstResultData)
{
	CALL_TRACE("SstTestResultData::operator=(const SstTestResultData &sstResultData");

	if (this != &sstResultData)
	{													// $[TI1]
		// Copying testDataArray_ values into private storage. 
		// testInfo_ values is fixed so there is no need for transferal.
		for (int i = 0; i < NUM_SUMMARIZED_TEST_DATA_ITEM; i++)
		{												// $[TI1.1]
			testDataArray_[i] = sstResultData.testDataArray_[i];
		}												// $[TI1.2]

		// Copying parent's value into private storage.
		TestResultData::operator=(sstResultData);
	}													// $[TI2]
	return(*this);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	Copy constructor 
//
//@ Interface-Description
//  Copy member values from the given object to the current object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Invoke the assignment operator with the given object.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
SstTestResultData::SstTestResultData(const SstTestResultData& sstTestResultData) :
		TestResultData((TestDataStruct *)sstTestResultData.testInfo_, 
					   (Real32 *)sstTestResultData.testDataArray_, 
						(Int32)	NUM_SUMMARIZED_TEST_DATA_ITEM)
{
	CALL_TRACE("SstTestResultData::SstTestResultData(const SstTestResultData"
					"&sstTestResultData");
	operator=(sstTestResultData);
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: loadDataFromNovRam 
//
//@ Interface-Description
//	 Called by GUI-Applications to load the test result data from NovRam
//	 into the current TestResultData object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call NovRam method to update local stack with latest test data. 
//---------------------------------------------------------------------
//@ PreCondition
//	 none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================
void 
SstTestResultData::loadDataFromNovRam(void)
{
	CALL_TRACE("SstTestResultData::loadDataFromNovRam(void)");
	
#if defined (SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
	// Call method from NovRam to get test data
	NovRamManager::GetSstTestResultData(*this);
														// $[TI1]
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:	testCompleted 
//
//@ Interface-Description
//  Called by Service-Data when an SST test is complete.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call update() method and write data for this test to NovRam. 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void 
SstTestResultData::testCompleted()
{
	CALL_TRACE("SstTestResultData::testCompleted()");
	
#if defined (SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
	// Call update method from NovRam to update its test data
	NovRamManager::UpdateSstTestResultData(*this);
														// $[TI1]
#endif // defined(SIGMA_GUI_CPU)  ||  defined(SIGMA_COMMON)
}



//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                      [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SstTestResultData::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("SstTestResultData::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, SERVICE_DATA, SST_TEST_RESULT_DATA, 
							lineNumber, pFileName, pPredicate);
}
