#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================
 
 
//============================== C L A S S   D E S C R I P T I O N ====
//@ Filename: Stdio - Sigma Printf and Sprintf Functions
//---------------------------------------------------------------------
//@ Interface-Description
//  This function provides a universal interface (wrapper) to the Sun 
//  and MRI sprintf and printf library functions.  Since the standard 
//  library sprintf function has a different prototype for the different
//  compilers, this function provides a common interface to this
//  library function.  
//---------------------------------------------------------------------
//@ Rationale
//  This function provides a common interface to the sprintf system
//  function.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  Uses the standard Sigma applications fault handling mechanisms.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/Stdio.ccv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 001    By: gdc   Date: 15-APR-1997   DCS Number: 5055
//    Project: Color
//    Description:
//       Initial version
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "Sigma.hh"
#include "UtilityClassId.hh"
#include "Stdio.hh"
#undef printf
#undef sprintf
#include <stdarg.h>
#include <stdio.h>

//#include "fpopt.h"
//extern int fpopt(_ANSIPROT2(int, ...));

//@ Usage-Classes
//@ End-Usage

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free-Function: Sprintf 
//
//@ Interface-Description
//  This method provides a common interface to the Sun and MRI sprintf
//  library functions.  It takes the standard sprintf arguments and 
//  returns the number of characters that were transferred to the output
//  string. Prior to calling sprintf, this method sets the floating 
//  point rounding mode to "round to nearest" as required by sprintf
//  to properly round floating point numbers. It restores the rounding
//  mode prior to returning to the caller.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Sun prototype for sprintf() is different, unfortunately
//  does not return the number of characters copied in the
//  sprintf operation.  So we define a function that always returns the
//  number of characters copied whether on the Sun or the target.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
int 
Sprintf(char *s, const char* pFormat, ...)
{
	// $[TI1]
	// initialize the va_list for vsprintf
	//va_list args;
	//va_start(args, pFormat);
 //
	//Int prevRoundingMode = fpopt(FPOPT_SET_ROUND, FPOPT_ROUND_NEAREST);
	//int count = vsprintf(s, pFormat, args);
	//fpopt(FPOPT_SET_ROUND, prevRoundingMode);
	//return count;

	return 0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free-Function: Printf 
//
//@ Interface-Description
//  This method provides a common interface to the Sun and MRI printf
//  library functions.  It takes the standard printf arguments and 
//  returns the number of characters that were transferred to the output
//  string. Prior to calling printf, this method sets the floating 
//  point rounding mode to "round to nearest" as required by printf
//  to properly round floating point numbers. It restores the rounding
//  mode prior to returning to the caller.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The Sun prototype for printf() is different, unfortunately
//  does not return the number of characters copied in the
//  sprintf operation.  So we define a function that always returns the
//  number of characters copied whether on the Sun or the target.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
int 
Printf(const char* pFormat, ...)
{
	// $[TI1]
	// initialize the va_list for vprintf
	va_list args;
	/*va_start(args, pFormat);
 
	Int prevRoundingMode = fpopt(FPOPT_SET_ROUND, FPOPT_ROUND_NEAREST);
	int ret = vfprintf(stdout, pFormat, args);
	fpopt(FPOPT_SET_ROUND, prevRoundingMode);
	return ret;*/

	return 0;
}
