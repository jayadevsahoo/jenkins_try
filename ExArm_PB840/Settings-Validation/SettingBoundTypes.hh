
#ifndef SettingBoundTypes_HH
#define SettingBoundTypes_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  SettingBoundTypes - Enumerators used for the setting bounds.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/SettingBoundTypes.hhv   25.0.4.0   19 Nov 2013 14:27:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	Incorporated initial specifications for NeoMode Project. Also,
//      eliminated use of unneeded 'INVALID_DISCRETE_VALUE' bound state.
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//      Initial version.
//  
//====================================================================
 
//@ Usage-Classes
//@ End-Usage


//@ Type:  BoundState
// This is the list of the different kinds of bound states a setting bound
// may be in.
enum BoundState
{
  NO_BOUND_VIOLATION,
  LOWER_HARD_BOUND,
  LOWER_SOFT_BOUND,
  UPPER_SOFT_BOUND,
  UPPER_HARD_BOUND
};


#endif // SettingBoundTypes_HH 
