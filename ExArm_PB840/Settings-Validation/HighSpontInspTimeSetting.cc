#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2005, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  HighSpontInspTimeSetting - High Circuit Pressure Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the upper limit of
//  inspiratory time when the ventilator is providing non-invasive 
//  ventilation. During invasive ventilation, the "inspiration too
//  long" or high spontaneous inspiratory time is determined 
//  algorithmically in the Breath-Delivery subsystem. During non-
//  invasive ventilation (NIV), this value can be set using the 
//  High Ti spont limit setting button. This setting is passed as part
//  of the batch settings which are phased in on the nexty breath.
//  This class inherits from 'BoundedSetting' and provides the specific
//  information needed for representation of high spontaneous inspiratory
//  time. This information includes the interval and range of this 
//  setting's values, and this batch setting's new-patient value.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has NO dependent settings, but 'updateConstraints_()' is
//  overridden to update this setting's dynamic bounds.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/HighSpontInspTimeSetting.ccv   25.0.4.0   19 Nov 2013 14:27:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 16-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added support for VentSet API.
// 
//  Revision: 003   By: gdc    Date: 01-Dec-2008    SCR Number: 6438
//  Project:  840S
//  Description:
//      Lower bound changed to 0.2 sec for Neonatal patients.
//
//  Revision: 002   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 001  By: gdc    Date: 27-May-2005    SCR Number: 6170
//  Project:  NIV2
//  Description:
//      Initial version.
//=====================================================================

#include "HighSpontInspTimeSetting.hh"
#include "SettingConstants.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "PatientCctTypeValue.hh"
#include "ModeValue.hh"
#include "VentTypeValue.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// all values are in milliseconds

//  $[NI02013] -- The setting's range ...
//  $[NI02015] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
{
	200.0f,		// absolute minimum...
	0.0f,			// unused...
	HUNDREDS,		// unused...
	NULL
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	10000.0f,		// absolute maximum...
	100.0f,		// resolution...
	HUNDREDS,		// precision...
	&::LAST_NODE_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: HighSpontInspTimeSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, this method initializes
//  the value interval range.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighSpontInspTimeSetting::HighSpontInspTimeSetting(void)
: BatchBoundedSetting(SettingId::HIGH_SPONT_INSP_TIME,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  HIGH_SPONT_INSP_TIME_MAX_BASED_IBW_ID,  // maxConstraintId..
					  HIGH_SPONT_INSP_TIME_MIN_ID)	  // minConstraintId...
{
	CALL_TRACE("HighSpontInspTimeSetting()");
	isATimingSetting_ = TRUE;
}	// $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~HighSpontInspTimeSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

HighSpontInspTimeSetting::~HighSpontInspTimeSetting(void)
{
	CALL_TRACE("~HighSpontInspTimeSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is applicable only when NIV vent type is selected.
//
//  $[01081] $[04029]
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	HighSpontInspTimeSetting::getApplicability(
											  const Notification::ChangeQualifier qualifier) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id applicability = Applicability::INAPPLICABLE;

	const Setting* P_VENT_TYPE = SettingsMgr::GetSettingPtr(SettingId::VENT_TYPE);
	const Setting* P_MODE = SettingsMgr::GetSettingPtr(SettingId::MODE);

	DiscreteValue ventTypeValue;
	DiscreteValue modeValue;

	switch ( qualifier )
	{
		case Notification::ACCEPTED :
			ventTypeValue = P_VENT_TYPE->getAcceptedValue();
			modeValue = P_MODE->getAcceptedValue();
			break;
		case Notification::ADJUSTED :
			ventTypeValue = P_VENT_TYPE->getAdjustedValue();
			modeValue = P_MODE->getAdjustedValue();
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(qualifier);
			break;
	}

	if ( ventTypeValue == VentTypeValue::NIV_VENT_TYPE
		 && (modeValue == ModeValue::SIMV_MODE_VALUE ||
			 modeValue == ModeValue::SPONT_MODE_VALUE ||
			 modeValue == ModeValue::CPAP_MODE_VALUE) )
	{
		applicability = Applicability::CHANGEABLE;
	}

	return(applicability);
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NI02014] -- new-patient equations...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	HighSpontInspTimeSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);
	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	BoundedValue  newPatient;
	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			newPatient.value = 1000.0 + IBW_VALUE * 100.0;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :
			newPatient.value = 1990.0 + IBW_VALUE * 20.0;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	// warp, and clip, the calculated value to a "click" boundary...
	getBoundedRange().warpValue(newPatient);

	return(newPatient);
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	HighSpontInspTimeSetting::SoftFault(const SoftFaultID  softFaultID,
										const Uint32       lineNumber,
										const char*        pFileName,
										const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							HIGH_SPONT_INSP_TIME_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[NI02013] - the setting's range...
//  $[LC02006] the setting shall be constrained by the new IBW...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	HighSpontInspTimeSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	BoundedRange::ConstraintInfo  maxConstraintInfo;

	//-------------------------------------------------------------------
	// update maximum constraint...
	//-------------------------------------------------------------------

	maxConstraintInfo.id = HIGH_SPONT_INSP_TIME_MAX_BASED_IBW_ID;
	maxConstraintInfo.value  = getAbsoluteMaxValue_();
	maxConstraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMaxConstraint(maxConstraintInfo);

	BoundedRange::ConstraintInfo  minConstraintInfo;

	//-------------------------------------------------------------------
	// update minimum constraint...
	//-------------------------------------------------------------------

	minConstraintInfo.id = HIGH_SPONT_INSP_TIME_MIN_ID;
	minConstraintInfo.value  = getAbsoluteMinValue_();
	minConstraintInfo.isSoft = FALSE;

	getBoundedRange_().updateMinConstraint(minConstraintInfo);
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic minimum constraint.
//
//  $[NI02013] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32 HighSpontInspTimeSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const Setting*  pPatientCctType = SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);
	const DiscreteValue  PATIENT_CCT_TYPE_VALUE = pPatientCctType->getAdjustedValue();

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			absoluteMinValue = 200.0f;
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :
			absoluteMinValue = 400.0f;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMinValue);
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMaxValue_()  [const, virtual]
//
//@ Interface-Description
//  Return this setting's absolute maximum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic maximum constraint.
//
//  $[NI02013] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	HighSpontInspTimeSetting::getAbsoluteMaxValue_(void) const
{
	CALL_TRACE("getAbsoluteMaxValue_()");

	return( BoundedValue(getNewPatientValue()) );
}
