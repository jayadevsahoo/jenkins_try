
#ifndef BinaryCommand_HH
#define BinaryCommand_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: BinaryCommand - digital output interface class
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/BinaryCommand.hhv   25.0.4.0   19 Nov 2013 13:53:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002  By: syw    Date:  19-Dec-1996    DR Number: DCS 1603
//  	Project:  Sigma (R8027)
//		Description:
//			Changed Register to BitAccessGpio.  Added setBit(), removed
//			setBitOn() and SetBitOff() methods.	Changed bitMask_ to bit_.
//			Added latchBit() method.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"

//@ Usage-Classes
#include "BitAccessGpio.hh"
#include "AioDioDriver.h"
//@ End-Usage

class BinaryCommand
{
  public:

    BinaryCommand( const ApplicationDigitalOutputs_t bit, BitAccessGpio& bitAccess);
    virtual ~BinaryCommand( void );

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL,
						   const char*       pPredicate = NULL );

	void setBit( BitAccessGpio::BitState bitState) ;
    void latchBit( BitAccessGpio::BitState bitState) ;


  protected:

  private:
    BinaryCommand();                // Don't allow this to be instantiated
	BinaryCommand& operator=(const BinaryCommand& rhs);   //Not implemented
    //@ Data-Member:  bit_
    //  bit position of command within the byte/word data
    ApplicationDigitalOutputs_t  bit_;

    //@ Data-Member:  rBitAccessGpio_
	//  a reference the register this object interfaces
    BitAccessGpio&  rBitAccessGpio_;
};


#endif // BinaryCommand_HH
