#ifndef ScrollableMenuSettingButton_HH
#define ScrollableMenuSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ScrollableMenuSettingButton - A specialized
// DiscreteSettingButton that displays a pop-up ScrollableMenu
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ScrollableMenuSettingButton.hhv   25.0.4.0   19 Nov 2013 14:08:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By: gdc     Date:  06-Dec-2006   SCR Number: 6237
//  Project:  TREND
//  Description:
//      Initial version
//
//====================================================================

#include "DiscreteSettingButton.hh"
#include "ScrollableMenu.hh"
#include "SettingMenuItems.hh"

//@ Usage-Classes
#include "Box.hh"

//@ End-Usage

class ScrollableMenuSettingButton : public DiscreteSettingButton
{
public:
	ScrollableMenuSettingButton(Uint16 xOrg, 
								Uint16 yOrg, 
								Uint16 width,
								Uint16 height, 
								ButtonType buttonType, 
								Uint8 bevelSize,
								CornerType cornerType, 
								BorderType borderType,
								StringId titleText,
								SettingId::SettingIdType settingId,
								ScrollableMenu& rScrollableMenu,
								SettingMenuItems& rMenuItems,
								Uint16 menuX,
								Uint16 menuY,
								Boolean isMainSetting = FALSE);

	~ScrollableMenuSettingButton(void);


	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);


	void updateScrollDisplay(Uint16 firstDisplayedEntry);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
								const SettingSubject*         pSubject);

	// Button virtual methods
	virtual void upHappened_(Boolean byOperatorAction);
	virtual void downHappened_(Boolean byOperatorAction);

private:
	// these methods are purposely declared, but not implemented...
	ScrollableMenuSettingButton(const ScrollableMenuSettingButton&);	// not implemented...
	void   operator=(const ScrollableMenuSettingButton&);	 // not implemented...

	//@ Data-Member: rScrollableMenu_
	// Reference to the pop-up ScrollableMenu
	ScrollableMenu& rScrollableMenu_; 

	//@ Data-Member: rSettingMenuItems_
	// Reference to the source of the menu items strings displayed in the
	// ScrollableMenu. Also the source for the DiscreteSettingButton display
	// "value" text.
	SettingMenuItems& rSettingMenuItems_; 

	//@ Data-Member: SCROLLABLE_MENU_X
	// X origin for pop-up ScrollableMenu
	const Uint16 SCROLLABLE_MENU_X;

	//@ Data-Member: SCROLLABLE_MENU_Y
	// Y origin for pop-up ScrollableMenu
	const Uint16 SCROLLABLE_MENU_Y;
};


#endif // ScrollableMenuSettingButton_HH 
