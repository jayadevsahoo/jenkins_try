/*****************************************************************************/
/*   Module Name:    parms.h                                            
/*   Purpose:      Exports function names for modifying parameter data and attributes                              
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/
#include <userint.h>
#include "globals.h"

#ifndef _PARMS
#define _PARMS

// The following are 'named' offsets into the parameter tables for storage and attributes
#define PARM_HIFLOW 0
#define PARM_LOFLOW 1
#define PARM_HIPSR  2
#define PARM_LOPSR  3
#define PARM_VOL    4
#define PARM_PCT_O2 5
#define PARM_BAROM  6

#define PARM_HIFLOW_TEMPR 0
#define PARM_LOFLOW_TEMPR 1

// Parameter colors defined
#define VOL_COLOR    (0xFFCC66) // orange
#define HIFLOW_COLOR (0x66CCFF) // pale blue
#define LOFLOW_COLOR (0x66CCFF) // pale blue
#define HIPSR_COLOR  (0x00FF99) // green|blue
#define LOPSR_COLOR  (0x00FF99) // green|blue
#define PCTO2_COLOR  VAL_OFFWHITE
#define BAROM_COLOR  VAL_OFFWHITE

// The following are 'named' offsets into the Attributes tables
// Volume
#define PARM_UNITS_LITERS       0
#define PARM_UNITS_ML           1
#define PARM_UNITS_CF           2

// Flow
#define PARM_UNITS_SLPM         0
#define PARM_UNITS_LPM          1
#define PARM_UNITS_CFM          2

// Pressure
#define PARM_UNITS_CMH2O        0 // low
#define PARM_UNITS_PSIG         0 // high
#define PARM_UNITS_KPA          1 // high, low
#define PARM_UNITS_INHG         2 // high, low
#define PARM_UNITS_INH2O        3 // low
#define PARM_UNITS_MMHG         4 // low

// Pressure (barometric)
#define PARM_UNITS_PSIA         0
#define PARM_UNITS_ATM          1
#define PARM_UNITS_MBAR         2
#define PARM_UNITS_BAR          3

// Gas concentration
#define PARM_UNITS_PCTO2        0

// Temperature/Pressure flow correction
#define PARM_MODE_STP           0
#define PARM_MODE_ATP           1
#define PARM_MODE_BTPS          2

// Display updates
#define PARM_MODE_CONTINUOUS    0
#define PARM_MODE_PEAK          1

// Temperature units
#define PARM_TEMPR_DEGREES_F    0
#define PARM_TEMPR_DEGREES_C    1

// Gas type lists
#define GAS_TYPE_AIR    0 // index 0 GASTYPE list control HIGH/LOW FLOW
#define GAS_TYPE_O2     1 // index 1 GASTYPE list control HIGH/LOW FLOW
#define GAS_TYPE_N2O    2 // index 2 GASTYPE list control LOW FLOW only
#define GAS_TYPE_O2_MIX 3 // index 2 GASTYPE list control HIGH FLOW only

// Threshold items
#define THRESHOLD_1 0
#define THRESHOLD_2 1

// List of gas types
typedef struct tag_GASTYPESTRUCT {
	int nGasTypeID;
	char *szGasTypeText;
} GASTYPESTRUCT;

typedef struct tag_GASTYPELIST {
	GASTYPESTRUCT *GasType;
} GASTYPELIST;

// Used to create tables for formatting numeric displays
typedef struct tag_FMTDIGITSSTRUCT {
	int nIntDigits;
	int nFracDigits;
} FMTDIGITSSTRUCT;

// Used for keeping matched pairs of time/data
typedef struct tag_DATA {
	BOOL bValidData;
	double dTime;
	double dData;
} DATASTRUCT;

// Derived values (Volume)
typedef struct tag_BREATHVALUES {
	double dBreathRate;
	double dIEratio;
	double dItime;
	double dEtime;
	double dMinuteVol;
} BREATHVALUES;

//
typedef struct tag_THRESHOLD {
	int nParmID;
	double dThresholdValue;
	int nSlope;
} THRESHOLDSTRUCT;
//
typedef struct tag_THRESHOLDSET {
	THRESHOLDSTRUCT *Threshold1;
	THRESHOLDSTRUCT *Threshold2;
} THRESHOLDSET;

// Used to make tables of parameter attributes
typedef struct tag_PARMS {
	int nParmID;              // named offset into parameter tables
	char *szIconSymbol;       // letter symbol for parameter
	CVI_COLOR nParmColor;     // color associated with the parameter for numerics and chart lines
	int (*fnGetMeasure)(double *); // function to acquire data
	double dCurrentData;      // last (current) datum measured
	double dMaxData;          // largest data value in record set
	double dMinData;          // smallest data value in record set
	double dTtlData;          // sum of datums in data set
	unsigned long nDataCount; // qty of datums in data set
	double dAvgData;          // current value for average of measured data
	double dTempPeakData;     // max datum measured in current breath
	double dCurrentPeakData;  // last peak datum measured
	double dMaxPeakData;      // largest data value in peak record set
	double dMinPeakData;      // smallest data value in peak record set
	double dTtlPeakData;      // sum of datums in peak data set
	unsigned long nPeakDataCount; // qty of datums in peak data set
	double dAvgPeakData;      // current value for average of peak measured data
	FMTDIGITSSTRUCT *FmtDigits; // tables of digits before/after decimal point for parameter's Units of Measure
	DATASTRUCT *dDataset;     // matched datum pairs of time & measured data
	int nDataStart;           // location of first datum in data set
	int nDataEnd;             // location of last datum in data set
	char **szUnits;           // table of Units of Measure for parameter
	int nNumUnits;            // qty of entries in Units of Measure table
	int nUnitOfMeasure;       // offset to current Unit of Measure in Units of Measure table
	int nHasFlowCorrectionMode;   // parameter can/cannot display a Flow Correction Mode
	char **szFlowCorrectionModes; // table of Flow Correction Modes
	int nNumFlowCorrectionModes;  // qty of entries in Flow Correction Modes table
	int nFlowCorrectionMode;  // offset to current Flow Correction Mode in Flow Correction Modes table
	int nHasGasType;          // parameter can/cannot select a gas type
	GASTYPELIST *GasTypeList; //
	int nNumGasTypes;         // qty of entries in gas type list
	int nGasTypeListItem;     // current item in gas type list
	int nHasThreshold;        // parameter can/cannot display a Threshold
	double Threshold1;        // 'Start' threshold
	double Threshold2;        // 'End' threshold
	THRESHOLDSET *Thresholds; // ptr to set of thresholds
	int nHasTemperature;      // parameter can/cannot display a temperature
	char **szTemprUnits;      // table of temperature Units of Measure
	int nNumTemprs;           // qty of entries in temperature Units of Measure table
	int nTempr;               // offset to current temperature Unit of Measure in temperature Units of Measure table
	int nTemprParm;           // media flow whose temperature is associated with the parameter
	int nHasBTBpeakMode;      // parameter can/cannot display Breath-to-Breath peak data
	char **szBTBpeakModes;    // table of BTBpeak Modes
	int nNumBTBpeakModes;     // qty of entries in BTBpeak Modes table
	int nBTBpeakMode;         // offset to current BTBpeak Mode in BTBpeak Modes table
	int nHasTare;
	double *dTare;
	UNITSCHARTYSCALE *Yscales; // table of chart Y-Scales
	int nDefaultYScale;       // default Y-Scale when making an initial (default) chart
} PARMSTRUCT;

extern char *(szParmName[]);
extern char *(szPselectCmdText[]);
extern char *(szPselectMenuText[]);

// Used to make table of working limits for parameters
typedef struct tag_PARMLIMITS {
	int nParmID;
	double dMinDataLimit;
	double dMaxDataLimit;
} PARMLIMITSTRUCT;

// Used to make tables of temperature attributes
// Elements are similar to parameter tables but with fewer elements
typedef struct tag_TEMPR {
	int nParmID;
	double dCurrentData;
	int nIntDigits;
	int nFracDigits;
	DATASTRUCT *dDataset;
	int nDataStart;
	int nDataEnd;
	char **szUnits;
	int nNumUnits;
} TEMPRSTRUCT;

extern char *(szTemprName[]);

// Maximum depth of parameter storage to allocate
// 60 seconds (max chart depth) / .010 seconds (shortest sampling interval)
#define DATADEPTH 6000

#define MAX_ITIME       60.0 // seconds
#define MAX_ETIME       60.0 // seconds
#define MAX_IERATIO    999
#define MAX_BREATHRATE 150.0 // bpm
#define MAX_MINUTEVOL   99.0 // STP

extern double draPlotXData[];
extern double draPlotYData[];

// If _USEMACROS is not defined, commonly used values are accessed via function calls
#ifndef _USEMACROS
void SetBreathRate(double dNewBreathRate);
double GetBreathRate(void);
void SetIEratio(double dNewIEratio);
double GetIEratio(void);
void SetItime(double dNewItime);
double GetItime(void);
void SetEtime(double dNewEtime);
double GetEtime(void);
void SetMinuteVol(double dNewMinuteVol);
double GetMinuteVol(void);
void SetHFtare(double dNewHFtare);
double GetHFtare(void);
void SetBarPsrComp(int nNewBarPsrComp);
int GetBarPsrComp(void);
void SetVolUnits(int nNewVolUnits);
int GetVolUnits(void);
void SetVolMode(int nNewDisplayMode);
int GetVolMode(void);
#else
// If _USEMACROS is defined, commonly used values are accessed via inline code
extern double dBreathRate;
#define SetBreathRate(dNewBreathRate) (dBreathRate = dNewBreathRate)
#define GetBreathRate() dBreathRate
extern double dIEratio;
#define SetIEratio(dNewIEratio) (dIEratio = dNewIEratio)
#define GetIEratio() dIEratio
extern double dItime;
#define SetItime(dNewItime) (dItime = dNewItime)
#define GetItime() dItime
extern double dEtime;
#define SetEtime(dNewEtime) (dEtime = dNewEtime)
#define GetEtime() dEtime
extern double dMinuteVol;
#define SetMinuteVol(dNewMinuteVol) (dMinuteVol = dNewMinuteVol)
#define GetMinuteVol() dMinuteVol
extern double dHFtare;
#define SetHFtare(dNewHFtare) (dHFtare = dNewHFtare)
#define GetHFtare() dHFtare
extern int nBarPsrComp;
#define SetBarPsrComp(nNewBarPsrComp) (nBarPsrComp = nNewBarPsrComp)
#define GetBarPsrComp() nBarPsrComp
extern int VolUnits;
#define SetVolUnits(nNewVolUnits) (VolUnits = nNewVolUnits)
#define GetVolUnits() VolUnits
extern int VolMode;
#define SetVolMode(nNewVolMode) (VolMode = nNewVolMode)
#define GetVolMode() VolMode
#endif // #ifndef _USEMACROS

// Number of defined chartable parameters
// (qty of rows in parms table)
extern int num_params;

// Number of defined temperature parameters
// (qty of rows in tempr table)
extern int num_tempr;

void SetParmThreshold(int nParmType, int ThresholdType, double nNewThreshold);
double GetParmThreshold(int nParmType, int ThresholdType);
int ThresholdPassed(double dReading,int nParmType, int ThresholdType);

// function calls to obtain a parameter measurement
int fnGetVol(double *dData);
int fnGetHiFlow(double *dData);
int fnGetLoFlow(double *dData);
int fnGetHiPsr(double *dData);
int fnGetLoPsr(double *dData);
int fnGetPctO2(double *dData);
int fnGetBarom(double *dData);
int fnGetHiFlowTempr(double *dData);
int fnGetLoFlowTempr(double *dData);

// If _USEMACROS is not defined, attributes are accessed via function calls
#ifndef _USEMACROS
int    GetParmID(int nParmType);
int   *GetParmIDaddress(int nParmType);
char  *GetParmIcon(int nParmType);
int    GetParmColor(int nParmType);
char  *GetParmName(int nParmType);
char  *GetParmCmdText(int nParmType);
char  *GetParmMenuText(int nParmType);
double GetParmCurrentData(int nParmType);
double GetParmMaxData(int nParmType);
double GetParmMinData(int nParmType);
double GetParmAvgData(int nParmType);
DATASTRUCT *GetParmDataset(int nParmType, int nDatum);
double GetParmTime(int nParmType, int nDatum);
double GetParmData(int nParmType, int nDatum);
int    GetParmDataStart(int nParmType);
int    GetParmDataEnd(int nParmType);
char **GetParmUnitsList(int nParmType);
int    GetParmUnitsCount(int nParmType);
char  *GetParmUnitsText(int nParmType, int nUnits);
void   SetParmUnitOfMeasure(int nParmType, int nUnits);
int    GetParmUnitOfMeasure(int nParmType);
int    GetParmHasFlowCorrectionMode(int nParmType);
char **GetParmFlowCorrectionModeList(int nParmType);
int    GetParmFlowCorrectionModeCount(int nParmType);
char  *GetParmFlowCorrectionModeText(int nParmType, int nMode);
void   SetParmFlowCorrectionMode(int nParmType, int nMode);
int    GetParmFlowCorrectionMode(int nParmType);
int    GetParmHasGasType(int nParmType);
GASTYPELIST *GetParmGasTypeList(int nParmType);
int    GetParmGasTypeCount(int nParmType);
int    GetParmGasTypeID(int nParmType, int nGasTypeListItem);
char  *GetParmGasTypeTxt(int nParmType, int nGasTypeListItem);
void   SetParmGasTypeListItem(int nParmType, int nNewGasTypeListItem);
int    GetParmGasTypeListItem(int nParmType);
int    GetParmHasThreshold(int nParmType);
int    GetParmHasTemperature(int nParmType);
char **GetParmTemprList(int nParmType);
int    GetParmTemprCount(int nParmType);
char  *GetParmTemprTxt(int nParmType, int nTempr);
void   SetParmTempr(int nParmType, int nNewTempr);
int    GetParmTempr(int nParmType);
int    GetParmTemprParm(int nParmType);
int    GetParmHasBTBpeakMode(int nParmType);
char **GetParmBTBpeakModeList(int nParmType);
int    GetParmBTBpeakModeCount(int nParmType);
char  *GetParmBTBpeakModeText(int nParmType, int nMode);
void   SetParmBTBpeakMode(int nParmType, int nMode);
int    GetParmBTBpeakMode(int nParmType);
int    GetParmHasTare(int nParmType);
void   SetParmTare(int nParmType, double dNewTareLevel);
double GetParmTare(int nParmType);
int    GetParmDefaultYScale(int nParmType);
double GetParmMinYScale(int nParmType, int UnitOfMeasure, int nYScaleFactor);
double GetParmMaxYScale(int nParmType, int UnitOfMeasure, int nYScaleFactor);
int    GetParmYScalePrecision(int nParmType, int UnitOfMeasure, int nYScaleFactor);
int    GetParmYScaleDivisions(int nParmType, int UnitOfMeasure, int nYScaleFactor);
int    GetParmYScaleNumScales(int nParmType, int UnitOfMeasure);
// If _USEMACROS is defined, attributes are accessed via inline code
#else
 PARMSTRUCT params[12];
#define GetParmID(nParmType) params[nParmType].nParmID
#define GetParmIDaddress(nParmType) &(params[nParmType].nParmID)
#define GetParmIcon(nParmType) params[nParmType].szIconSymbol
#define GetParmColor(nParmType) params[nParmType].nParmColor
#define GetParmName(nParmType) szParmName[nParmType]
#define GetParmCmdText(nParmType) szPselectCmdText[nParmType]
#define GetParmMenuText(nParmType) szPselectMenuText[nParmType]
#define GetParmCurrentData(nParmType) (params[nParmType].nBTBpeakMode ? params[nParmType].dCurrentPeakData : params[nParmType].dCurrentData)
#define GetParmMaxData(nParmType) (params[nParmType].nBTBpeakMode ? params[nParmType].dMaxPeakData : params[nParmType].dMaxData)
#define GetParmMinData(nParmType) (params[nParmType].nBTBpeakMode ? params[nParmType].dMinPeakData : params[nParmType].dMinData)
#define GetParmAvgData(nParmType) (params[nParmType].nBTBpeakMode ? params[nParmType].dAvgPeakData : params[nParmType].dAvgData)
#define GetParmDataset(nParmType, nDatum) &(params[nParmType].dDataset[nDatum])
#define GetParmTime(nParmType, nDatum) params[nParmType].dDataset[nDatum].dTime
#define GetParmData(nParmType, nDatum) params[nParmType].dDataset[nDatum].dData
#define GetParmDataStart(nParmType) params[nParmType].nDataStart
#define GetParmDataEnd(nParmType) params[nParmType].nDataEnd
#define GetParmUnitsList(nParmType) params[nParmType].szUnits
#define GetParmUnitsCount(nParmType) params[nParmType].nNumUnits
#define GetParmUnitsText(nParmType, nUnits) params[nParmType].szUnits[nUnits]
#define SetParmUnitOfMeasure(nParmType, nUnits) (params[nParmType].nUnitOfMeasure = nUnits)
#define GetParmUnitOfMeasure(nParmType) params[nParmType].nUnitOfMeasure
#define GetParmHasFlowCorrectionMode(nParmType) params[nParmType].nHasFlowCorrectionMode
#define GetParmFlowCorrectionModeList(nParmType) params[nParmType].szFlowCorrectionModes
#define GetParmFlowCorrectionModeCount(nParmType) params[nParmType].nNumFlowCorrectionModes
#define GetParmFlowCorrectionModeText(nParmType, nMode) params[nParmType].szFlowCorrectionModes[nMode]
#define SetParmFlowCorrectionMode(nParmType, nMode) (params[nParmType].nFlowCorrectionMode = nMode)
#define GetParmFlowCorrectionMode(nParmType) params[nParmType].nFlowCorrectionMode
#define GetParmHasGasType(nParmType) params[nParmType].nHasGasType
#define GetParmGasTypeList(nParmType) params[nParmType].GasTypeList
#define GetParmGasTypeCount(nParmType) params[nParmType].nNumGasTypes
#define GetParmGasTypeID(nParmType, nGasTypeListItem) params[nParmType].GasTypeList[nGasTypeListItem].GasType->nGasTypeID
#define GetParmGasTypeTxt(nParmType, nGasTypeListItem) params[nParmType].GasTypeList[nGasTypeListItem].GasType->szGasTypeText
#define SetParmGasTypeListItem(nParmType, nNewGasTypeListItem) (params[nParmType].nGasTypeListItem = nNewGasTypeListItem)
#define GetParmGasTypeListItem(nParmType) params[nParmType].nGasTypeListItem
#define GetParmHasThreshold(nParmType) params[nParmType].nHasThreshold
#define GetParmHasTemperature(nParmType) params[nParmType].nHasTemperature
#define GetParmTemprList(nParmType) params[nParmType].szTemprUnits
#define GetParmTemprCount(nParmType) params[nParmType].nNumTemprs
#define GetParmTemprTxt(nParmType, nTempr) params[nParmType].szTemprUnits[nTempr]
#define SetParmTempr(nParmType, nNewTempr) (params[nParmType].nTempr = nNewTempr)
#define GetParmTempr(nParmType) params[nParmType].nTempr
#define GetParmTemprParm(nParmType) params[nParmType].nTemprParm
#define GetParmHasBTBpeakMode(nParmType) params[nParmType].nHasBTBpeakMode
#define GetParmBTBpeakModeList(nParmType) params[nParmType].szBTBpeakModes
#define GetParmBTBpeakModeCount(nParmType) params[nParmType].nNumBTBpeakModes
#define GetParmBTBpeakModeText(nParmType, nMode) params[nParmType].szBTBpeakModes[nMode]
#define SetParmBTBpeakMode(nParmType, nMode) (params[nParmType].nBTBpeakMode = nMode)
#define GetParmBTBpeakMode(nParmType) params[nParmType].nBTBpeakMode
#define GetParmHasTare(nParmType) params[nParmType].nHasTare
//#define SetParmTare(nParmType, dNewTareLevel) (*(params[nParmType].dTare) = dNewTareLevel)
void   SetParmTare(int nParmType, double dNewTareLevel);
#define GetParmTare(nParmType) (*(params[nParmType].dTare))
#define GetParmDefaultYScale(nParmType) params[nParmType].nDefaultYScale
#define GetParmMinYScale(nParmType, UnitOfMeasure, nYScaleFactor) params[nParmType].Yscales[UnitOfMeasure].ChartScale[nYScaleFactor].dYMin
#define GetParmMaxYScale(nParmType, UnitOfMeasure, nYScaleFactor) params[nParmType].Yscales[UnitOfMeasure].ChartScale[nYScaleFactor].dYMax
#define GetParmYScalePrecision(nParmType, UnitOfMeasure, nYScaleFactor) params[nParmType].Yscales[UnitOfMeasure].ChartScale[nYScaleFactor].nYPrecision
#define GetParmYScaleDivisions(nParmType, UnitOfMeasure, nYScaleFactor) params[nParmType].Yscales[UnitOfMeasure].ChartScale[nYScaleFactor].nYDivisions
#define GetParmYScaleNumScales(nParmType, UnitOfMeasure) params[nParmType].Yscales[UnitOfMeasure].nNumScales
#endif

// Common functions to set/get/clear/format parameter data
void ClearAllParmData(void);
void ClearParmData(int nParmType);
void SetParmCurrentData(int nParmType, double dTime, double dNewCurrentData);
//void StoreParmCurrentData(int nParmType, double dTime, double dNewCurrentData);
void UpdatePeakDataStats(void);
void SetParmPeakData(int nParmType, double dNewCurrentData);
char *FormatParmData(int nParmType, double dData, char *cBuf, double dLimitTestData);

// List of units for various parameters

	// Gas Types
extern GASTYPESTRUCT GasTypes[];
extern GASTYPELIST FHGasTypes[];
extern GASTYPELIST FLGasTypes[];

  // Parameter Volume
extern char *(szVolUnits[]);
extern int num_szVolUnits;
extern FMTDIGITSSTRUCT VolDigits[];

  // Parameter Lo Flow
extern char *(szLoFloUnits[]);
extern int num_szLoFloUnits;
extern FMTDIGITSSTRUCT LoFloDigits[];

  // Parameter Hi Flow
extern char *(szHiFloUnits[]);
extern int num_szHiFloUnits;
extern FMTDIGITSSTRUCT HiFloDigits[];

  // Parameter Lo Psr
extern char *(szLoPsrUnits[]);
extern int num_szLoPsrUnits;
extern FMTDIGITSSTRUCT LoPsrDigits[];

  // Parameter Hi Psr
extern char *(szHiPsrUnits[]);
extern int num_szHiPsrUnits;
extern FMTDIGITSSTRUCT HiPsrDigits[];

  // Parameter %Oxygen
extern char *(szOxygenUnits[]);
extern int num_szOxygenUnits;
extern FMTDIGITSSTRUCT OxygenDigits[];

  // Parameter Barometric Psr
extern char *(szBaromUnits[]);
extern int num_szBaromUnits;
extern FMTDIGITSSTRUCT BaromDigits[];

  // List of 'Temperatures'
extern char *(szTempr[]);
extern int num_szTempr;

extern char *(szNoTempr[]);
extern int num_szNoTempr;

  // List of 'X-Scale' time units
extern char *(szTimeUnits[]);
extern int num_szTimeUnits;

extern char *(szNoUnits[]);
int num_szNoUnits;

// List of modes for various parameters

extern char *(szHiFlowCorrectionModes[]);
extern int num_szHiFlowCorrectionModes;

extern char *(szNoFlowCorrectionModes[]);
extern int num_szNoFlowCorrectionModes;

extern char *(szBTBpeakModes[]);
extern int num_szBTBpeakModes;

extern char *(szNoBTBpeakModes[]);
extern int num_szNoBTBpeakModes;

// Common functions to set/get/format flow temperature data
double GetTemprCurrentData(int nParmType);
void SetTemprCurrentData(int nParmType, double dTime, double dNewCurrentData);
char *FormatTemprData(int nParmType, double dData, char *cBuf);

#endif // _PARMS
