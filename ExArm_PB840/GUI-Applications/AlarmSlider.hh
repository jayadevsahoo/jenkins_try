#ifndef AlarmSlider_HH
#define AlarmSlider_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmSlider - Displays a slider for setting high and low
// alarm limits, containing buttons for setting the limits and a
// pointer displaying the current value of the associated patient datum.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmSlider.hhv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010 By: gdc    Date: 15-Feb-2005   DR Number: 6144
//  Project:  NIV1
//	Description:
//      DCS 6144 - NIV1 - changes to support low circuit pressure limit
//      added parameter to define slider's auto-scaling behavior
//
//  Revision: 009 By: srp    Date: 28-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 008  By:  erm	   Date:  25-Mar-2002    DCS Number:  5790
//  Project:  VTPC
//  Description:  
//     Added support to change associated alarm.
//
//  Revision: 007  By:  heatherw	   Date:  04-Mar-2002    DCS Number:  5790
//  Project:  VTPC
//  Description:
//      Added new method 'setLabelText' so the text can be changed for
//      a TextField.
//      Added new method 'setUpperButtonMessageId' so the help message
//      associated with the upper alarm button could be set.
//
//  Revision: 006  By:  Healy	   Date:  08-Feb-2000    DCS Number:  5504
//  Project:  NeoMode
//  Description:
//      Added recent-activity range to alarm bars whereby the standard deviation 
//      the last N consecutive breaths is displayed.
//
//  Revision: 005  By:  hhd	   Date:  30-Jul-1999    DCS Number:  5327
//  Project:  NeoMode
//  Description:
//      Added auto-scaling capability into alarm sliders.
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By:  hhd	   Date:  01-Feb-1999    DCS Number:  5322
//  Project:  ATC
//  Description:
//		Initial version.
//	
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"
#include "PatientDataTarget.hh"
#include "BdEventTarget.hh"

//@ Usage-Classes
#include "AlarmLimitSettingButton.hh"
#include "AlarmName.hh"
#include "Box.hh"
#include "NumericField.hh"
#include "BreathDatumHandle.hh"
#include "TextField.hh"
#include "Triangle.hh"
#include "SettingObserver.hh"
#include "StdDeviationBuffer.hh"
//@ End-Usage

class AlarmSlider : public Container, 
					public PatientDataTarget, 
					public BdEventTarget,
					public SettingObserver
{
public:
	//@ Type: SliderId
	// This enum represents the type of sliders.
  	enum SliderId
  	{
  		PEAK_CIRCUIT_PRESSURE,
  		RESPIRATORY_RATE,
  		EXHALED_MINUTE_VOLUME,
  		EXHALED_MAND_TIDAL_VOLUME,
  		EXHALED_SPONT_TIDAL_VOLUME,
  		INHALED_SPONT_TIDAL_VOLUME,
  		MAX_SLIDER_ID
  	};
  
	AlarmSlider(SettingId::SettingIdType lowerSettingId,
				StringId lowerMessageId,
				SettingId::SettingIdType upperSettingId,
				StringId upperMessageId,
				AlarmName lowerAlarmName,
				AlarmName upperAlarmName,
				PatientDataId::PatientItemId patientDataId,
				StringId minLabel, StringId maxLabel,
				SubScreen *pFocusSubScreen,
				Real32 *pSummationBuffer,
				Real32 *pDeviationBuffer,
				Uint32 bufferSize,
				Boolean autoScalingOn);
	~AlarmSlider(void);

	void hidePatientDataPointer();
    void hideStdDeviationBox();
	 
    void setLabelText(const wchar_t * pString);
	 
    void setUpperButtonMessageId(const StringId messageId);
    void setUpperAlarm( AlarmName newAlarm );


	// Drawable virtual method
	virtual void activate(void);
	virtual void deactivate(void);

	// PatientDataTarget virtual method
	virtual void patientDataChangeHappened(
								PatientDataId::PatientItemId patientDataId);

	// BdEventTarget virtual method
    virtual void bdEventHappened(EventData::EventId eventId,
								 EventData::EventStatus eventStatus,
								 EventData::EventPrompt eventPrompt);

	virtual void alarmEventHappened(void);

	// SettingObserver virtual method
    virtual void  applicabilityUpdate(
							const Notification::ChangeQualifier qualifierId,
						    const SettingSubject*               pSubject
									 );
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject);


	inline SettingId::SettingIdType  getUpperSettingId(void) const;
	inline SettingId::SettingIdType  getLowerSettingId(void) const;

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);
  
	//@ Constant: CONTAINER_WIDTH
	// The width of the whole alarm slider.
	static const Int32 CONTAINER_WIDTH;
	
	//@ Constant: CONTAINER_HEIGHT
	// The height of the whole alarm slider.
	static const Int32 CONTAINER_HEIGHT;


protected:

private:
	AlarmSlider(void);							// not implemented...
	AlarmSlider(const AlarmSlider&);			// not implemented...
	void operator=(const AlarmSlider&);			// not implemented...

	Uint16 valueToPoint_(Real32 sliderValue);

	void updateButtonPosition_(AlarmLimitSettingButton* pButton,
							   const SettingSubject*    pSubject);
	void updateValueDisplay_(void);
	void updateAlarmDisplay_(void);
	void updateScale_(Real32);
	void updateLowerButtonPosition_(Real32);

	//@ Data-Member: sliderBox_
	// The grey box for displaying the slider.
	Box sliderBox_;

	//@ Data-Member: min_
	// The minimum value on the slider.
	Real32 min_;

	//@ Data-Member: max_
	// The maximum value on the slider.
	Real32 max_;

	//@ Data-Member: oldMax_
	// The old maximum value on the slider.
	Real32 oldMax_;

	//@ Data-Member: fullMax_
	// The maximum value on the slider in full scale.
	Real32 fullMax_;

	//@ Data-Member: halfMax_
	// The maximum value on the slider in 1/2 scale.
	Real32 halfMax_;

	//@ Data-Member: minLabel_
	// The text field which displays a label below the minimum value on
	// the slider.  This should be the symbol which stands for the patient
	// datum displayed on the slider.
	TextField minLabel_;

	//@ Data-Member: maxLabel_
	// The text field which displays a label above the maximum value on
	// the slider.  This should be the symbol for the units in which the
	// patient datum displayed on the slider is measured.
	TextField maxLabel_;

	//@ Data-Member: minField_
	// The numeric field used to display the minimum value of the slider.
	NumericField minField_;

	//@ Data-Member: maxField_
	// The numeric field used to display the maximum value of the slider.
	NumericField maxField_;
	
	//@ Data-Member:  LOWER_SETTING_ID_
	// The GUI setting id for the value of the lower alarm limit.
	const SettingId::SettingIdType  LOWER_SETTING_ID_;

	//@ Data-Member: UPPER_SETTING_ID_
	// The GUI setting id for the value of the upper alarm limit.
	const SettingId::SettingIdType  UPPER_SETTING_ID_;
    
	//@ Data-Member: lowerButtonCntnr_
	// A container for the lower alarm limit button, used to contain the
	// setting button along with a pointer triangle.
	Container lowerButtonCntnr_;

	//@ Data-Member: upperButtonCntnr_
	// A container for the upper alarm limit button, used to contain the
	// setting button along with a pointer triangle.
	Container upperButtonCntnr_;

	//@ Data-Member: lowerButton_
	// The lower alarm limit setting button.
	AlarmLimitSettingButton lowerButton_;

	//@ Data-Member: upperButton_
	// The upper alarm limit setting button.
	AlarmLimitSettingButton upperButton_;

	//@ Data-Member: lowerTriangle_
	// The triangle used as the pointer at the top left of the lower alarm
	// limit setting button.
	Triangle lowerTriangle_;

	//@ Data-Member: upperTriangle_
	// The triangle used as the pointer at the top left of the lower alarm
	// limit setting button.
	Triangle upperTriangle_;

	//@ Data-Member: value_
	// The current value of the patient datum on the slider.
	Real32 value_;

	//@ Data-Member: patientDataHandle_
	// A handle to the patient datum value, used for accessing the Patient
	// Data subsystem.
	BreathDatumHandle patientDataHandle_;

	//@ Data-Member: valuePointer_
	// The container used to group the drawables which make up the patient
	// datum pointer at the left side of the slider.
	Container valuePointer_;

	//@ Data-Member: valueDisplay_
	// A numeric field used for displaying the current value of the patient
	// datum.
	NumericField valueDisplay_;

	//@ Data-Member: valueBox_
	// The white box on which the value of the patient datum is displayed.
	Box valueBox_;

	//@ Data-Member: valueTriangle_
	// The triangle used as the ``point'' on the patient datum pointer.
	Triangle valueTriangle_;

	//@ Data-Member: lowerAlarmName_
	// The lower alarm name
	AlarmName lowerAlarmName_;

	//@ Data-Member: upperAlarmName_
	// The upper alarm name
	AlarmName upperAlarmName_;

	//@ Data-Member: hidePointer_
	// To flag whether to hide Alarm Sliders' value pointers
	// Value pointers are hidden until new patient data arrives
	Boolean hidePointer_;

	//@ Data-Member: scaleFactor_
	// A scaling factor used in converting setting and patient id values to
	// a corresponding pixel value for positioning graphicals along the alarm
	// slider.
	Real32 scaleFactor_;

	//@ Data-Member: changedScale_
	// A Boolean value used to determine if the scaling factor has been changed.
	Boolean changedScale_;

	//@ Data-Member: stdDeviationBuffer_
	// The buffer used to do standard deriviation.
	StdDeviationBuffer stdDeviationBuffer_;

	//@ Data-Member: stdDeviationBox_
	// The grey box for displaying the slider.
	Box stdDeviationBox_;

	//@ Data-Member: isAutoScalingOn_
	// Auto-scaling is requested for this slider when TRUE
	Boolean isAutoScalingOn_;


};


#include "AlarmSlider.in"


#endif // AlarmSlider_HH 
