//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#ifndef NETWORKMESSAGEHEADER_H
#define NETWORKMESSAGEHEADER_H
namespace swat{

    class NetworkMessageHeader
    {
        public:
            unsigned int iLength;
            unsigned int iOffset;
    };

}

#endif
