#pragma once

enum GlobalPenType
{
	Pen_Curve,
	Pen_OldCurve,
	Pen_Wiper
};

class GlobalObjects
{
	void initialize_( );
	GlobalObjects(void);
	GlobalObjects(const GlobalObjects& ); //Not implemented.
	GlobalObjects operator=(const GlobalObjects& );// Not implemented.

	CFont Arial_16_Bold_;
	CFont Arial_32_Bold_;
	CFont Arial_40_Bold_;
	CPen  PenForCurve_;
	CPen  PenForOldCurve_;
	CPen  PenForWiper_;

	static GlobalObjects* Instance_;
	~GlobalObjects(void);
public:

	static GlobalObjects* getInstance();

	CFont* getArial_Bold(int fontHeight);

	CPen* getPen( GlobalPenType penType );


};
