
#ifndef MutableC_AlarmUpdate_NUM_ALARM_HH
#define MutableC_AlarmUpdate_NUM_ALARM_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: MutableC_AlarmUpdate_NUM_ALARM - Mutable Fixed Sorted List of 'AlarmUpdate' Copies.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedMutableSortC.H_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "AlarmConstants.hh"
#include "AlarmConstants.hh"

//@ Usage-Classes
#include "SortC_AlarmUpdate_NUM_ALARM.hh"
//@ End-Usage


class MutableC_AlarmUpdate_NUM_ALARM : public SortC_AlarmUpdate_NUM_ALARM
{
  public:
    MutableC_AlarmUpdate_NUM_ALARM(const MutableC_AlarmUpdate_NUM_ALARM& sortList);
    MutableC_AlarmUpdate_NUM_ALARM(const SortC_AlarmUpdate_NUM_ALARM& sortList);
    MutableC_AlarmUpdate_NUM_ALARM(void);
    virtual ~MutableC_AlarmUpdate_NUM_ALARM(void);

    virtual Boolean  isSorted(void) const;

    inline AlarmUpdate&  currentItem(void);

    void  reSort(void);

    inline void  operator=(const MutableC_AlarmUpdate_NUM_ALARM& list);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);

  protected:
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
};


// Inlined methods...
#include "MutableC_AlarmUpdate_NUM_ALARM.in"


#endif  // MutableC_AlarmUpdate_NUM_ALARM_HH
