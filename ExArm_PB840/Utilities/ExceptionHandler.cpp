#include "stdafx.h"

//#############################################################################
//
//  ExceptionHandler.cpp - Implementation of the ExceptionHandler_t class
//
//#############################################################################

#include "ExceptionHandler.h"
#include "GlobalObjects.h"
#ifdef SIGMA_GUIPC_CPU
// TODO E600 #include "ExceptionDialog.h"
#endif
//#include "SystemErrorAlarm.h"
//#include "EventsThread.h"

#define EXCEPTION_RECORD_FILE_NAME      _T("\\FlashFX Disk\\ProgramFiles\\Exception.dat")

//#############################################################################
//
//  ExceptionHandler_t::ExceptionHandler_t - Constructor for ExceptionHandler_t
//
//#############################################################################

ExceptionHandler_t::ExceptionHandler_t()
{
    ExceptionOccurred = false;
    StopVentilation = false;
    EventsExceptionOccurred = false;
}


//#############################################################################
//
//  ExceptionHandler_t::~ExceptionHandler_t - Destructor for ExceptionHandler_t
//
//#############################################################################

ExceptionHandler_t::~ExceptionHandler_t()
{
}


//#############################################################################
//
//  ExceptionHandler_t::EvaluateException - Capture the exception context.
//
//#############################################################################

int ExceptionHandler_t::EvaluateException(LPEXCEPTION_POINTERS p)
{
    memcpy(&ExceptionRecord, p->ExceptionRecord, sizeof(EXCEPTION_RECORD));
    memcpy(&Context, p->ContextRecord, sizeof(CONTEXT));

    return EXCEPTION_EXECUTE_HANDLER;
}



//#############################################################################
//
//  ExceptionHandler_t::StopVentilating - Save the exception context and stop
//      ventilation by killing the watchdog. Set the alarm condition.
//
//#############################################################################

void ExceptionHandler_t::StopVentilating(int32_t ExceptionCode, char* ThreadName)
{
/*
SystemErrorAlarm.ExceptionOccurred();               // Make sure the system alarm gets activated

    EventsThread.SetEvent(MSG_EVENTS_EXCEPTION, ExceptionCode, 0);
    ExceptionOccurred = true;
    StopVentilation = true;

    SaveExceptionContext(ExceptionCode, ThreadName);
*/
}


//#############################################################################
//
//  ExceptionHandler_t::VentilateWithUi - Save the exception context and set
//      the alarm condition, but assume the UI is still operating.
//
//#############################################################################

void ExceptionHandler_t::VentilateWithUi(int32_t ExceptionCode, char* ThreadName)
{
/*
	SystemErrorAlarm.ExceptionOccurred();               // Make sure the system alarm gets activated

    EventsThread.SetEvent(MSG_EVENTS_EXCEPTION, ExceptionCode, 0);
    ExceptionOccurred = true;

    SaveExceptionContext(ExceptionCode, ThreadName);
*/
}


//#############################################################################
//
//  ExceptionHandler_t::VentilateWithoutUi - Save the exception context but
//      assume the UI is gone, so put up a failure message. Set the alarm
//      condition.
//
//#############################################################################
static const int UI_SLEEP_TIME =   1000;
void ExceptionHandler_t::VentilateWithoutUi(int32_t ExceptionCode, char* ThreadName)
{
/*
	SystemErrorAlarm.ExceptionOccurred();               // Make sure the system alarm gets activated

    EventsThread.SetEvent(MSG_EVENTS_EXCEPTION, ExceptionCode, 0);
    ExceptionOccurred = true;

    SaveExceptionContext(ExceptionCode, ThreadName);

#ifdef SIGMA_GUIPC_CPU
    ExceptionDialog_t ExceptionDialog;
    ExceptionDialog.DoModal();
#endif

    while(true)
    {
        Sleep(UI_SLEEP_TIME);
    }
*/
}


//#############################################################################
//
//  ExceptionHandler_t::EventsFailure - Save the exception context but
//      assume the event system is gone. Set the alarm
//      condition.
//
//#############################################################################

void ExceptionHandler_t::EventsFailure(int32_t ExceptionCode, char* ThreadName)
{
//    SystemErrorAlarm.ExceptionOccurred();               // Make sure the system alarm gets activated

    ExceptionOccurred = true;
    EventsExceptionOccurred = true;

    SaveExceptionContext(ExceptionCode, ThreadName);
}


//#############################################################################
//
//  ExceptionHandler_t::SaveExceptionContext -
//
//#############################################################################
static const int MAX_BUFFER_SIZE    =  250;
static const int SAVED_STACK_LENGTH =  100;
static const int ERROR_MESSAGE_SIZE =  1024;
static unsigned int MAX_SIZE_OF_EXCEPTION    =  10000;
void ExceptionHandler_t::SaveExceptionContext(int32_t ExceptionCode, char* ThreadName)
{
// E600 BDIO --- This needs to be modified to not use MFC or to handle MFC
#if 0
    CFile ExceptionHandler;
    CFileException fileException;

    RETAILMSG(true, (_T(">>>> Exception occured, saving context.\r\n")));
    if(!ExceptionHandler.Open(EXCEPTION_RECORD_FILE_NAME,           // open file
        CFile::modeWrite, &fileException))
    {
        if(!ExceptionHandler.Open(EXCEPTION_RECORD_FILE_NAME,       // create file
            CFile::modeCreate | CFile::modeWrite, &fileException))
        {
            TCHAR ErrorMessage[ERROR_MESSAGE_SIZE];

            fileException.GetErrorMessage(ErrorMessage,ERROR_MESSAGE_SIZE); // retrieve the error message

            RETAILMSG(true, (_T("Can not create the file named: %s, the reason is\r\n%s\r\n"), EXCEPTION_RECORD_FILE_NAME,ErrorMessage));

            return;
        }
    }

    if(ExceptionHandler.GetLength() > MAX_SIZE_OF_EXCEPTION)
    {
        ExceptionHandler.SeekToBegin();
    }
    else 
    {
        // file open
        ExceptionHandler.SeekToEnd();                               // seek to end
    }

    CTime CurrentTime = CTime::GetCurrentTime();

    char buffer[MAX_BUFFER_SIZE];

    sprintf(buffer, "\r\n\r\n<<<%02d:%02d:%02d - %02d/%02d, %4d>>>\r\n",
            CurrentTime.GetHour(),
            CurrentTime.GetMinute(),
            CurrentTime.GetSecond(),
            CurrentTime.GetMonth(),
            CurrentTime.GetDay(),
            CurrentTime.GetYear());
    ExceptionHandler.Write(buffer, strlen(buffer));

    sprintf(buffer, "Thread Name: %s\r\nException Code(%08X): ", ThreadName, ExceptionCode);
    ExceptionHandler.Write(buffer, strlen(buffer));

    switch(ExceptionCode)
    {
        case EXCEPTION_ACCESS_VIOLATION:
            strcpy(buffer, "Access violation.\r\n");
            break;

        case EXCEPTION_DATATYPE_MISALIGNMENT:
            strcpy(buffer, "Datatype Misalignment.\r\n");

        case EXCEPTION_BREAKPOINT:
            strcpy(buffer, "Breakpoint.\r\n");
            break;

        case EXCEPTION_SINGLE_STEP:
            strcpy(buffer, "Single step.\r\n");
            break;

        case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:
            strcpy(buffer, "Array bounds exceeded.\r\n");
            break;

        case EXCEPTION_FLT_DENORMAL_OPERAND:
            strcpy(buffer, "Floating Denormal operand.\r\n");
            break;

        case EXCEPTION_FLT_DIVIDE_BY_ZERO:
            strcpy(buffer, "Floating Divide by zero.\r\n");
            break;

        case EXCEPTION_FLT_INEXACT_RESULT:
            strcpy(buffer, "Floating Inexact result.\r\n");
            break;

        case EXCEPTION_FLT_INVALID_OPERATION:
            strcpy(buffer, "Floating Invalid operation.\r\n");
            break;

        case EXCEPTION_FLT_OVERFLOW:
            strcpy(buffer, "Floating Overflow.\r\n");
            break;

        case EXCEPTION_FLT_STACK_CHECK:
            strcpy(buffer, "Floating Stack check.\r\n");
            break;

        case EXCEPTION_FLT_UNDERFLOW:
            strcpy(buffer, "Floating Underflow.\r\n");
            break;

        case EXCEPTION_INT_DIVIDE_BY_ZERO:
            strcpy(buffer, "Integer divide by zero.\r\n");
            break;

        case EXCEPTION_INT_OVERFLOW:
            strcpy(buffer, "Integer Overflow.\r\n");
            break;

        case EXCEPTION_PRIV_INSTRUCTION:
            strcpy(buffer, "PRIV instruction.\r\n");
            break;

        case EXCEPTION_IN_PAGE_ERROR:
            strcpy(buffer, "In page error.\r\n");
            break;

        case EXCEPTION_ILLEGAL_INSTRUCTION:
            strcpy(buffer, "Illegal instruction.\r\n");
            break;

        case EXCEPTION_NONCONTINUABLE_EXCEPTION:
            strcpy(buffer, "Noncontinuable exception.\r\n");
            break;

        case EXCEPTION_STACK_OVERFLOW:
            strcpy(buffer, "Stack overflow.\r\n");
            break;

        case EXCEPTION_INVALID_DISPOSITION:
            strcpy(buffer, "Invalid disposition.\r\n");
            break;

        case EXCEPTION_GUARD_PAGE:
            strcpy(buffer, "Guard Page\r\n");
            break;

        case EXCEPTION_INVALID_HANDLE:
            strcpy(buffer, "Invalid handle\r\n");
            break;

        default:
            strcpy(buffer, "Unrecognized exception.\r\n");
            break;
    }
    ExceptionHandler.Write(buffer, strlen(buffer));

    sprintf(buffer, "Exception Address: %08X\r\n", ExceptionRecord.ExceptionAddress);
    ExceptionHandler.Write(buffer, strlen(buffer));

    // save parameters. MAX 15 (EXCEPTION_MAXIMUM_PARAMETERS)
    DWORD count = 0;
    strcpy(buffer, " ");
    char tmp[50];
    while(count < EXCEPTION_MAXIMUM_PARAMETERS)
    {
        if(count >= ExceptionRecord.NumberParameters)
        {
            break;
        }

        sprintf(tmp, "%08X ", ExceptionRecord.ExceptionInformation[count++]);
        strcat(buffer, tmp);
    }
    ExceptionHandler.Write(buffer, strlen(buffer));

#ifndef SIGMA_GUIPC_CPU
    // now save the context
    sprintf(buffer, "\r\n\t>>>ARM Context<<< \r\n\tContextFlags= %08X\r\n\tR0-R12= {%08X %08X %08X %08X %08X %08X %08X %08X %08X %08X %08X %08X %08X}\r\n",
            Context.ContextFlags,
            Context.R0,
            Context.R1,
            Context.R2,
            Context.R3,
            Context.R4,
            Context.R5,
            Context.R6,
            Context.R7,
            Context.R8,
            Context.R9,
            Context.R10,
            Context.R11,
            Context.R12);
    ExceptionHandler.Write(buffer, strlen(buffer));

    // special registers
    sprintf(buffer, "\tStackPointer= %08X\r\n\tLink Reg= %08X\r\n\tPC= %08X\r\n\tPSR=%08X\r\n",
            Context.Sp,
            Context.Lr,
            Context.Pc,
            Context.Psr);
    ExceptionHandler.Write(buffer, strlen(buffer));
    // we do not need floating point registers.

    // now try to save the stack
    ULONG * StackPointer = (ULONG *)(Context.Sp);
    sprintf(buffer, "\t>>>Save Stack (%d Bytes)", SAVED_STACK_LENGTH);
    ExceptionHandler.Write(buffer, strlen(buffer));

#define NUMBER_PER_LINE     10
    for(int i=0; i < SAVED_STACK_LENGTH; i++)
    {
        if(!(i%NUMBER_PER_LINE))
        {
            sprintf(buffer, "\r\n\t%08X ", *StackPointer);
        }
        else
        {
            sprintf(buffer, "%08X ", *StackPointer++);
        }
        ExceptionHandler.Write(buffer, strlen(buffer));
    }
#endif

    ExceptionHandler.Flush();
    ExceptionHandler.Close();
#endif
}
