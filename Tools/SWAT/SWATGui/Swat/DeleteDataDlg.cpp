// DeleteDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DeleteDataDlg.h"
#include "Swat.h"


// CDeleteDataDlg dialog

IMPLEMENT_DYNAMIC(CDeleteDataDlg, CDialog)

CDeleteDataDlg::CDeleteDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDeleteDataDlg::IDD, pParent)
{

}

CDeleteDataDlg::~CDeleteDataDlg()
{
}

void CDeleteDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

}


BEGIN_MESSAGE_MAP(CDeleteDataDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CDeleteDataDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CDeleteDataDlg message handlers



BOOL CDeleteDataDlg::OnInitDialog()
{
	CDialog::OnInitDialog();


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}



void CDeleteDataDlg::OnBnClickedOk()
{
	CWaitCursor wait;
	((CSwatApp*)AfxGetApp())->m_TestResultSet.m_pDatabase->ExecuteSQL("DELETE FROM \"TestResults\".test_result");
	Sleep(1000);

	OnOK();
}