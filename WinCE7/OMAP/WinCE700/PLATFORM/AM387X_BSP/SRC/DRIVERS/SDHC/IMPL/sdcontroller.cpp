// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File: sdcontroller.cpp
//
//

#include <windows.h>
#include <initguid.h>
#include "SDController.h"
#include <nkintr.h>
// needed for CPU revision constants
#include <oalex.h>

//#define RETAIL_MSG_ENABLED  1
#define DEBOUNCE_TIME           100     // 100ms debounce timeout

//for NETRA: BIT-6 CINS_ENABLE;  BIT-7 CREM_ENABLE
#define MMC_CD_INT_EN_MASK      (MMCHS_IE_CREM|MMCHS_IE_CINS) // 0x000000C0

//------------------------------------------------------------------------------
//  Global variables

CSDIOController::CSDIOController()
    : CSDIOControllerBase()
{
    eSDHCCDIntr = SDHC_INTR_DISABLED;
}

CSDIOController::~CSDIOController()
{
}


//-----------------------------------------------------------------------------
BOOL CSDIOController::InitializeInterrupt()
{
    return TRUE;
}


BOOL CSDIOController::InitializeWPDetect(void)
{
    return TRUE;
}

BOOL CSDIOController::DeInitializeWPDetect(void)
{
    return TRUE;
}

// Is this card write protected?
BOOL CSDIOController::IsWriteProtected()
{
    DWORD MMC_PSTATE = INREG32(&m_pbRegisters->MMCHS_PSTATE);

    return ( (MMC_PSTATE & MMCHS_PSTAT_WP) >> MMCHS_PSTAT_WP_SHIFT );
}

// Is the card present?
//          CDP
//       | 0   1
//---------------
//      0| F   T
// CINS  |
//      1| T   F
BOOL CSDIOController::SDCardDetect()
{
    DWORD MMC_CON    = INREG32(&m_pbRegisters->MMCHS_CON);
    DWORD MMC_PSTATE = INREG32(&m_pbRegisters->MMCHS_PSTATE);

    return ( ((MMC_CON & MMCHS_CON_CDP) >> MMCHS_CON_CDP_SHIFT) !=
             ((MMC_PSTATE & MMCHS_PSTAT_CINS) >> MMCHS_PSTAT_CINS_SHIFT) );
}

//-----------------------------------------------------------------------------
BOOL CSDIOController::InitializeCardDetect()
{
    return TRUE;
}

//-----------------------------------------------------------------------------
BOOL CSDIOController::DeInitializeCardDetect()
{
    return TRUE;
}


typedef volatile struct
{
    UINT32 SD_HL_REV;
    UINT32 SD_HL_HWINFO;
    UINT32 SD_HL_SYSCONFIG;
} AM387X_MMCHS_EXT_REGS;


//-----------------------------------------------------------------------------
BOOL CSDIOController::InitializeHardware()
{
    DWORD               dwCountStart;

    volatile UINT32  *pCM_ALWON_SDIO_CLKCTRL;
    PHYSICAL_ADDRESS physicalAddress;

    AM387X_MMCHS_EXT_REGS *pExtRegs = NULL;

    AM387X_SYSC_PADCONFS_REGS *pPadRegs = NULL;

#if RETAIL_MSG_ENABLED
    RETAILMSG(1,  (L">>>>> +InitializeHardware\r\n")); 
#endif

    // Turn on SDIO clk (if needed)
    physicalAddress.HighPart = 0;
    physicalAddress.LowPart  = 0x481815B0;  // CM_ALWON_SDIO_CLKCTRL
	pCM_ALWON_SDIO_CLKCTRL = (volatile UINT32 *)MmMapIoSpace(physicalAddress, sizeof(UINT32), FALSE);

    if (!pCM_ALWON_SDIO_CLKCTRL)
        return FALSE;

    if ((*pCM_ALWON_SDIO_CLKCTRL & 0x00030000) != 0)
    {
        RETAILMSG(1,  (L"CM_ALWON_SDIO_CLKCTRL status: 0x%08X\r\n", 
            (*pCM_ALWON_SDIO_CLKCTRL & 0x00030000))); 
        RETAILMSG(1,  (L"Turn on CM_ALWON_SDIO_CLKCTRL\r\n")); 

        // Clock is disabled.  This might be the case in NAND boot.  Turn on SDIO clk.
        *pCM_ALWON_SDIO_CLKCTRL &= 0xFFFFFFFC;  // disable
        while ((*pCM_ALWON_SDIO_CLKCTRL & 0x00030000) != 0x00030000); // wait for module to be disabled
        *pCM_ALWON_SDIO_CLKCTRL |= 0x00000002;  // enable
        while ((*pCM_ALWON_SDIO_CLKCTRL & 0x00030000) != 0);  // wait for module to be fully functional
    }
#if RETAIL_MSG_ENABLED
    else
    {
        RETAILMSG(1,  (L"CM_ALWON_SDIO_CLKCTRL is already fully functional.\r\n")); 
    }
#endif
#if 1
#if RETAIL_MSG_ENABLED
    // Print netra sdhc ext regs
    physicalAddress.HighPart = 0;
    physicalAddress.LowPart  = 0x481D8000;
    pExtRegs = (AM387X_MMCHS_EXT_REGS *)MmMapIoSpace(physicalAddress, sizeof(AM387X_MMCHS_EXT_REGS), FALSE);

    RETAILMSG(1,  (L">>>>> AM387X_MMCHS_EXT_REGS(0x%08X): Rev=0x%08X HL_HWINFO=0x%08X HL_SYSC=0x%08X\r\n", 
        pExtRegs, pExtRegs->SD_HL_REV, pExtRegs->SD_HL_HWINFO, pExtRegs->SD_HL_SYSCONFIG ));
#endif
#endif
#if RETAIL_MSG_ENABLED
    // Print SD PAD configs
    physicalAddress.HighPart = 0;
    physicalAddress.LowPart  = AM387X_SYSC_PADCONFS_REGS_PA; //0x48180800
	pPadRegs = (AM387X_SYSC_PADCONFS_REGS *)MmMapIoSpace(physicalAddress, sizeof(AM387X_SYSC_PADCONFS_REGS), FALSE);

    RETAILMSG(1,  (L">>>>> AM387X_SYSC_PADCONFS_REGS(0x%08X): \r\n", pPadRegs ));
/*    RETAILMSG(1,  (L"      MMC_POW        (0x%08X)  0x%08X \r\n", 
        &(pPadRegs->CONTROL_PADCONF_MMC0_POW), pPadRegs->CONTROL_PADCONF_MMC_POW ));*/
    RETAILMSG(1,  (L"      MMC_CLK        (0x%08X)  0x%08X \r\n", 
        &(pPadRegs->CONTROL_PADCONF_MMC1_CLK), pPadRegs->CONTROL_PADCONF_MMC1_CLK ));
    RETAILMSG(1,  (L"      MMC_CMD        (0x%08X)  0x%08X \r\n", 
        &(pPadRegs->CONTROL_PADCONF_MMC1_CMD), pPadRegs->CONTROL_PADCONF_MMC1_CMD ));
    RETAILMSG(1,  (L"      MMC_DAT0       (0x%08X)  0x%08X \r\n", 
        &(pPadRegs->CONTROL_PADCONF_MMC1_DAT0), pPadRegs->CONTROL_PADCONF_MMC1_DAT0 ));
    RETAILMSG(1,  (L"      MMC_DAT1_SDIRQ (0x%08X)  0x%08X \r\n", 
        &(pPadRegs->CONTROL_PADCONF_MMC1_DAT1), pPadRegs->CONTROL_PADCONF_MMC1_DAT1));
    RETAILMSG(1,  (L"      MMC_DAT2_SDRW  (0x%08X)  0x%08X \r\n", 
        &(pPadRegs->CONTROL_PADCONF_MMC1_DAT2), pPadRegs->CONTROL_PADCONF_MMC1_DAT2 ));
    RETAILMSG(1,  (L"      MMC_DAT3       (0x%08X)  0x%08X \r\n", 
        &(pPadRegs->CONTROL_PADCONF_MMC0_DAT3), pPadRegs->CONTROL_PADCONF_MMC1_DAT3 ));
/*    RETAILMSG(1,  (L"      MMC_SDCD       (0x%08X)  0x%08X \r\n", 
        &(pPadRegs->CONTROL_PADCONF_MMC0_SDCD), pPadRegs->CONTROL_PADCONF_MMC_SDCD ));
    RETAILMSG(1,  (L"      MMC_SDWP       (0x%08X)  0x%08X \r\n", 
        &(pPadRegs->CONTROL_PADCONF_MMC_SDWP), pPadRegs->CONTROL_PADCONF_MMC_SDWP ));*/
#endif

    // Reset the controller
    OUTREG32(&m_pbRegisters->MMCHS_SYSCONFIG, MMCHS_SYSCONFIG_SOFTRESET);

    // calculate timeout conditions
    dwCountStart = GetTickCount();

    // Verify that reset has completed.
    while (!(INREG32(&m_pbRegisters->MMCHS_SYSSTATUS) & MMCHS_SYSSTATUS_RESETDONE))
    {
        Sleep(0);

        // check for timeout
        if (GetTickCount() - dwCountStart > m_dwMaxTimeout)
        {
            DEBUGMSG(ZONE_ENABLE_ERROR, (TEXT("InitializeModule() - exit: TIMEOUT.\r\n")));
            goto cleanUp;
        }
    }

    InitializeCardDetect();
    InitializeWPDetect();
#if RETAIL_MSG_ENABLED
    RETAILMSG(1,  (L">>>>> -InitializeHardware\r\n")); 
#endif
    return TRUE;
cleanUp:
    RETAILMSG(1,  (L"-InitializeHardware: Error\r\n")); 
    return FALSE;
}

void CSDIOController::DeinitializeHardware()
{
    DeInitializeCardDetect();
    DeInitializeWPDetect();
}

//-----------------------------------------------------------------------------
//  SDHCCardDetectIstThreadImpl - card detect IST thread for driver
//  Input:  lpParameter - pController - controller instance
//  Output:
//  Return: Thread exit status
DWORD CSDIOController::SDHCCardDetectIstThreadImpl()
{
    // check for the card already inserted when the driver is loaded
    if(SDCardDetect())
    {
        CardInterrupt(TRUE);
    }

#if RETAIL_MSG_ENABLED
    RETAILMSG(1, (L"SDHCCardDetectIstThreadImpl: Thread not needed in NETRA.  Exit.\r\n"));
#endif

    return ERROR_SUCCESS;
}

//-----------------------------------------------------------------------------
VOID CSDIOController::PreparePowerChange(CEDEVICE_POWER_STATE curPowerState, BOOL bInPowerHandler)
{
    UNREFERENCED_PARAMETER(bInPowerHandler);
    UNREFERENCED_PARAMETER(curPowerState);
}

VOID CSDIOController::PostPowerChange(CEDEVICE_POWER_STATE curPowerState, BOOL bInPowerHandler)
{
	UNREFERENCED_PARAMETER(bInPowerHandler);
	UNREFERENCED_PARAMETER(curPowerState);
}

VOID CSDIOController::TurnCardPowerOn()
{
    SetSlotPowerState( D0 );
}

VOID CSDIOController::TurnCardPowerOff()
{
    SetSlotPowerState( D4 );
}

CSDIOControllerBase *CreateSDIOController()
{
    return new CSDIOController();
}

//-----------------------------------------------------------------------------
// Enable SDHC Card Detect Interrupts.
VOID CSDIOController::EnableSDHCCDInterrupts()
{
    EnterCriticalSection( &m_critSec );
    SETREG32(&m_pbRegisters->MMCHS_ISE, MMC_CD_INT_EN_MASK);
    SETREG32(&m_pbRegisters->MMCHS_IE,  MMC_CD_INT_EN_MASK);
    eSDHCCDIntr = SDHC_MMC_INTR_ENABLED;
    LeaveCriticalSection( &m_critSec );
}

//-----------------------------------------------------------------------------
// Disable SDHC Card Detect Interrupts.
void CSDIOController::DisableSDHCCDInterrupts()
{
    EnterCriticalSection( &m_critSec );
    SETREG32(&m_pbRegisters->MMCHS_ISE, ~MMC_CD_INT_EN_MASK);
    SETREG32(&m_pbRegisters->MMCHS_IE,  ~MMC_CD_INT_EN_MASK);
    eSDHCCDIntr = SDHC_INTR_DISABLED;
    LeaveCriticalSection( &m_critSec );
}

void CSDIOController::Write_MMC_CD_STAT( DWORD dwVal )
{
    EnterCriticalSection( &m_critSec );
    dwVal &= (MMCHS_STAT_CREM|MMCHS_STAT_CINS);
    OUTREG32(&m_pbRegisters->MMCHS_STAT, dwVal);
    LeaveCriticalSection( &m_critSec );
}

void CSDIOController::SetCDPolarity()
{
    return;
}

