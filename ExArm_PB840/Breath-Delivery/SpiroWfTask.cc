#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ class SpiroWfTask - Spirometry, waveform and breath data tasks.
//---------------------------------------------------------------------
//@ Interface-Description
//
//	This class contains tasks that that are responsible for communicating
//	patient information to GUI. The tasks includes WaveformTask, 
//	BreathDataTask and SpirometryTask.
//	The SpirometryTask is the task that calculates spirometry and sends
//	the data to GUI. WaveformTask is responsible for sending waveform 
//	data to GUI. BreathDataTask is responsible for sending current
//	breath information to GUI.
//---------------------------------------------------------------------
//@ Rationale
//	Tasks definition.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Each task has its own initialization method and all tasks can not
//	be terminated.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SpiroWfTask.ccv   25.0.4.1   20 Nov 2013 17:34:48   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 016  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.
// 
//  Revision: 015   By:   rhj    Date: 20-Apr-2010     SCR Number: 6436
//  Project:  PROX
//  Description:
//        Added Prox pressure, flow, phase and volumes to the waveform
//        packet.
//
//  Revision: 014   By:   rpr    Date: 21-Jul-2009     SCR Number: 6615
//  Project:  MAT
//  Description:
//      Removed assertions for when data is not being process fast enough
//		for queue protection. Instead just flush the queue maybe losing data
//		points for spirometry etc.
//
//  Revision: 013   By:   gdc    Date: 18-Aug-2009     SCR Number: 6147
//  Project:  XB
//  Description:
//        Moved handling of BdMonitoredData to BdMonitorTask.
//
//  Revision: 013   By:   gdc    Date: 18-Aug-2009     SCR Number: 6147
//  Project:  XB
//  Description:
//        Moved handling of BdMonitoredData to BdMonitorTask.
//
//  Revision: 012   By:   rhj    Date: 05-March-2007    DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 011   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//      Added support for new 'isApneaActive' and 'isErrorState' fields.
//
//  Revision: 010  By:  jja   Date:  08-Mar-2002    DR Number: 5860
//  Project:  VCP
//  Description:
//		Updated to handle VC+/VS Startup messages.
//
//  Revision: 009   By: syw   Date:  27-Jul-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added handle for SEND_PAV_DATA.
//		Added lung data info, isPeepRecovery.
//
//  Revision: 008   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Eliminate unused BdSignal.hh reference
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By:  yyy    Date:  01-Oct-1998    DCS Number:
//  Project:  BiLevel
//  Description:
//			Bilevel initial version.
//
//  Revision: 005  By: gdc   Date:  25-Jun-1998    DR Number:
//  	Project:  Color
//		Description:
//			Added breath type to waveform record.
//
//  Revision: 004  By: syw   Date:  02-Apr-1998    DR Number: 5048
//  	Project:  Sigma (840)
//		Description:
//			Removed old TIMING_TEST code.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By:  sp   Date:  25-Feb-1997    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Inspection rework.
//
//  Revision: 001  By:  sp    Date:  10-Oct-1996    DR Number: DCS NONE
//       Project:  Sigma (840)
//       Description:
//             Initial version
//
//=====================================================================

#include "SpiroWfTask.hh"

//@ Usage-Classes
#include "AveragedBreathData.hh"
#include "BreathMiscRefs.hh"
#include "MsgQueue.hh"
#include "Waveform.hh"
#include "WaveformRecord.hh"
#include "WaveformData.hh"
#include "PatientDataMgr.hh"
#include "TaskControlAgent.hh"
#include "TaskMonitor.hh"
#include "TaskMonitorQueueMsg.hh"
#include "InterTaskMessage.hh"
#include "NetworkApp.hh"
#include "BdMonitorRefs.hh"

#if defined (_WIN32_WCE)
#include <winbase.h>
#include <tchar.h>
#endif

//@ End-Usage
extern Boolean BdTimerEnabled;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SpirometryTask
//
//@ Interface-Description
//      This method takes no parameter and has no return value.
//	This method is responsible for the calculation of averaged breath data 
//	and the sending of the data to the GUI.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	This task in pending on SPIROMETRY_TASK_Q. The message received
//	is then passed on the AveragedBreathData object.
//---------------------------------------------------------------------
//@ PreCondition
//      Return status after accessing the queue is Ipc::OK.
//---------------------------------------------------------------------
//@ PostCondition
//      The task has to be completed before the arrival of the next message. 
//@ End-Method
//=====================================================================

void 
SpiroWfTask::SpirometryTask(void)
{
	Int32 msgPend;
	Int32 msgCount;
	Int32 status;
	MsgQueue spirometryMsgQue(::SPIROMETRY_TASK_Q);

	for(;;)
	{
		status = spirometryMsgQue.pendForMsg(msgPend); 
		CLASS_ASSERTION (status == Ipc::OK);


		// to differentiate spirometry que msg from task monitor msg.
		// task monitor msg is located on the most significant byte.
		if (msgPend & 0x00FFFFFF)
		{
			//$[TI1]
			if ( NetworkApp::IsCommunicationUp() &&
			     TaskControlAgent::GetGuiState() == STATE_ONLINE )
			{
				//$[TI2]
				RAveragedBreathData.updateAveragedBreathData(msgPend);
			}
			//$[TI3]

			// There should not be no more than two msg left in the queue.
			// If there are more than two entry in the que it means that
			// this task is not being processed fast enough
			msgCount = spirometryMsgQue.numMsgsAvail();
			if (msgCount > 2)
			{
				TaskMonitor::Report();
				spirometryMsgQue.flush();
			}
			//$[TI4]
		}
		else
		{
			// to process task monitor msg
			//$[TI5]
			TaskMonitorQueueMsg taskMonitorMsg(msgPend);
			if (taskMonitorMsg.getMsgId() == TaskMonitorQueueMsg::TASK_MONITOR_MSG)
			{
				//$[TI6]
				TaskMonitor::Report();
			}
			else
			{
				CLASS_PRE_CONDITION(taskMonitorMsg.getMsgId() == TaskMonitorQueueMsg::TASK_MONITOR_MSG);
			}
		}
#ifdef SIGMA_UNIT_TEST
break;
#endif //SIGMA_UNIT_TEST
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaveformTask
//
//@ Interface-Description
//      This method takes no parameter and has no return value.
//	This is responsible for updating waveform and communicate the
//	waveform packets to the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This task is pending on WAVEFORM_TASK_Q. Each time that a message
//	is received means that a set of waveform packets is ready to be
//	sent to the GUI. The waveform packets are sent through 
//	PatientDataMgr's interface. TUV data is send after the waveform
//	data has been transmitted. Method BdMonitoredData::readComplete is called
//	to update the read index after the data has been sent.
//	$[06169] $[06173]
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      The task has to be completed before the arrival of the next message. 
//@ End-Method
//=====================================================================

void
SpiroWfTask::WaveformTask(void)
{
	Int32 msgPend;
	Int32 msgCount;
	WaveformData wfDataXmit[NUM_WAVEFORM_SET];
	WaveformSet* pWaveformSet = NULL;
	const WaveformRecord* pWaveformRecord = NULL;
	MsgQueue waveformMsgQue(::WAVEFORM_TASK_Q);
	Int32 status;

	for(;;)
	{
		status = waveformMsgQue.pendForMsg(msgPend); 
		CLASS_ASSERTION (status == Ipc::OK);


		if (msgPend == BdQueuesMsg::SEND_WAVEFORM_MSG)
		{
			// $[TI1]
			if ( NetworkApp::IsCommunicationUp() &&
			     TaskControlAgent::GetGuiState() == STATE_ONLINE )
			{
				// To prevent the main task from overwriting the 
				// current waveform set that is being send.

				// $[TI2]
				Waveform::WaveformTaskLockFlag = TRUE;
				pWaveformSet = RWaveform.getWaveformSetToSend();

				CLASS_PRE_CONDITION(pWaveformSet != NULL);
				pWaveformSet->resetCurrentIndex();

				pWaveformRecord = pWaveformSet->getNextWaveformRecord();
				CLASS_PRE_CONDITION(pWaveformRecord != NULL);

				for(Int32 ii = 0; (pWaveformRecord != NULL) && (ii <NUM_WAVEFORM_SET); ii++)
				{
					// $[TI3]
					wfDataXmit[ii].circuitPressure = pWaveformRecord->patientPressure_; 
					wfDataXmit[ii].netFlow = pWaveformRecord->netFlow_;
					wfDataXmit[ii].netVolume = pWaveformRecord->netVolume_; 
					wfDataXmit[ii].lungPressure = pWaveformRecord->lungPressure_ ;
					wfDataXmit[ii].lungFlow = pWaveformRecord->lungFlow_ ;
					wfDataXmit[ii].lungVolume = pWaveformRecord->lungVolume_ ;
					wfDataXmit[ii].bdPhase = pWaveformRecord->phaseType_;
					wfDataXmit[ii].breathType = pWaveformRecord->breathType_;
					wfDataXmit[ii].isAutozeroActive = pWaveformRecord->isAutozeroActive_;
					wfDataXmit[ii].isPeepRecovery = pWaveformRecord->isPeepRecovery_;
					wfDataXmit[ii].isApneaActive = pWaveformRecord->isApneaActive_;
					wfDataXmit[ii].isErrorState = pWaveformRecord->isErrorState_;
					wfDataXmit[ii].modeSetting = pWaveformRecord->modeSetting_ ;
					wfDataXmit[ii].supportTypeSetting = pWaveformRecord->supportTypeSetting_ ;
					wfDataXmit[ii].proxManuever = pWaveformRecord->proxManuever_ ;
					wfDataXmit[ii].proxCircuitPressure = pWaveformRecord->proxPatientPressure_ ;
					pWaveformRecord = pWaveformSet->getNextWaveformRecord();
				}

				// Release lock after a copy of waveform has been made
				Waveform::WaveformTaskLockFlag = FALSE;

				PatientDataMgr::NewWaveformData(wfDataXmit,sizeof(wfDataXmit));

			}
			// $[TI6]

			msgCount = waveformMsgQue.numMsgsAvail();

			// There should not be not more than 2 msg left in the queue.
			// If there are more than 2 entries in the que it means that
			// this task is not being processed fast enough
			if (msgCount > 2)
			{
				TaskMonitor::Report();
				waveformMsgQue.flush();
			}
			// $[TI7]
		}
		else
		{
			// to process task monitor msg
			//$[TI8]
			TaskMonitorQueueMsg taskMonitorMsg(msgPend);
			if (taskMonitorMsg.getMsgId() == TaskMonitorQueueMsg::TASK_MONITOR_MSG)
			{
				//$[TI9]
				TaskMonitor::Report();
			}
			else
			{
				CLASS_PRE_CONDITION(taskMonitorMsg.getMsgId() == TaskMonitorQueueMsg::TASK_MONITOR_MSG);
			}
		}
#ifdef SIGMA_UNIT_TEST
break;
#endif //SIGMA_UNIT_TEST
	}

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BreathDataTask
//
//@ Interface-Description
//      This method takes no parameter and has no return value.
//	This task is responsible for communication for the BdMainTask.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The message from BREATH_DATA_Q is passed to BreathData object
//	to start communication.	
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      The task has to be completed within one breath cycle.
//@ End-Method
//=====================================================================

void 
SpiroWfTask::BreathDataTask(void)
{
	Int32 msgPend, msgCount;
	MsgQueue breathDataQue(::BREATH_DATA_Q);
	Int32 status;

	for(;;)
	{
		status = breathDataQue.pendForMsg(msgPend);
		CLASS_ASSERTION (status == Ipc::OK);

		if (msgPend >= BdQueuesMsg::LOW_BREATH_DATA_MSG_ID
				&&  msgPend <= BdQueuesMsg::HIGH_BREATH_DATA_MSG_ID)
		{
			//$[TI1]
			if ( NetworkApp::IsCommunicationUp() &&
			     TaskControlAgent::GetGuiState() == STATE_ONLINE )
			{
				//$[TI2]
				RBreathData.sendPatientData(msgPend);
			}
			//$[TI3]

			msgCount = breathDataQue.numMsgsAvail();

			// There should not be more than 3 msg left in the queue.
			// If there are more than 3 entry in the que it means that
			// this task is not being processed within one breath cycle.
			if (msgCount > 3)
			{
				TaskMonitor::Report();
				breathDataQue.flush();
			}
			//$[TI4]
		}
		else
		{
			// to process task monitor msg
			//$[TI5]
			TaskMonitorQueueMsg taskMonitorMsg(msgPend);
			if (taskMonitorMsg.getMsgId() == TaskMonitorQueueMsg::TASK_MONITOR_MSG)
			{
				//$[TI6]
				TaskMonitor::Report();
			}
			else
			{
				CLASS_PRE_CONDITION(taskMonitorMsg.getMsgId() == TaskMonitorQueueMsg::TASK_MONITOR_MSG);
			}
		}
		
#ifdef SIGMA_UNIT_TEST
break;
#endif //SIGMA_UNIT_TEST
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
SpiroWfTask::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, SPIROWFTASK,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
 
//=====================================================================
//
//  Private Methods...
//
//=====================================================================

