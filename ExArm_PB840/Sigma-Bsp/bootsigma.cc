#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================
 
// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: bootsigma.c - Boot Process Glue Logic
//
//---------------------------------------------------------------------
//@ Interface-Description
//  This module contains the entry point for starting the VRTX boot
//  process.
//---------------------------------------------------------------------
//@ Module-Decomposition
//  boot_sigma_crt0
//---------------------------------------------------------------------
//@ Rationale
//  This module contains the boot process initialization for the 840.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//  not applicable
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  not applicable
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Sigma-Bsp/vcssrc/bootsigma.ccv   25.0.4.0   19 Nov 2013 14:29:24   pvcs  $
//
//@ Modification-Log
//
//   Revision 001   By: Gary Cederquist Date: 07-JUL-1997 DR Number: 2176
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version.
//
//=====================================================================
 
#ifdef SIGMA_PRODUCTION

#include <boot.h>
#include <stdlib.h>
//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  boot_sigma_crt0
//
//@ Interface-Description
//  This function is adapted from bootc.c supplied by MRI.  It is called
//  from the boot entry point in crt0.asm as the first step of the boot
//  process to set up the exception vectors and initialize the VRTX CPU
//  library used during device driver initialization.  The MRI supplied
//  bootc.c sets up the exception vector table to trap exceptions to
//  its own exception handlers.  This modified version instead simply
//  maintains the exception vectors installed by POST.  This way, any
//  unexpected exceptions that occur during the boot process or VRTX
//  initialization are trapped and processed by the POST exception
//  handler.  The VRTX function boot_cold_start() is called to install
//  the device drivers defined in the Sigma-Bsp subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//  See Interface-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Free-Function
//=====================================================================

extern "C" void boot_sigma_crt0(void);  // forward declaration

void 
boot_sigma_crt0(void)
{
    // do not initialize exception vectors here - use installed POST vectors

	// initialize CPU library 
	cpu_init();
	cpu_set_target(boot_item_target);

    // cold start to create device drivers and call VRTX boot
	boot_reset_counter = 0;
	boot_cold_start();
}

#endif // SIGMA_PRODUCTION
