#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PsolLoopbackTest - Test the PSOL loopback current
//---------------------------------------------------------------------
//@ Interface-Description
//  This class is used to test PSOL loopback current vs PSOL
//	command at various test points for each PSOL.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the PSOL loopback test.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method runTest() is responsible for testing the PSOL loopback
//	current for each PSOL.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/PsolLoopbackTest.ccv   25.0.4.0   19 Nov 2013 14:23:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: gdc    Date: 19-Feb-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//		Modified per code review.
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "PsolLoopbackTest.hh"

//@ Usage-Classes

#include "ValveRefs.hh"
#include "Task.hh"
#include "SmRefs.hh"
#include "SmManager.hh"
#include "SmConstants.hh"

#ifdef SIGMA_DEVELOPMENT
# include <stdio.h>
#endif  // SIGMA_DEVELOPMENT

#if defined(SIGMA_UNIT_TEST) && defined(SIGMA_BD_CPU)
#include "PsolLoopbackTest_UT.hh"
#include "FakeIo.hh"
#endif	// defined(SIGMA_UNIT_TEST) && defined(SIGMA_BD_CPU)

//@ End-Usage

//@ Constant: PSOL_LOOPBACK_MAX_GAIN_ERROR
// maximum gain error, sum of PSOL driver + loopback
const Real32 PSOL_LOOPBACK_MAX_GAIN_ERROR = 0.091;

//@ Constant: PSOL_LOOPBACK_MAX_OFFSET_ERROR
// maximum offset error, sum of PSOL driver + loopback
const Real32 PSOL_LOOPBACK_MAX_OFFSET_ERROR = 13.73;  // mA


//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PsolLoopbackTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.  No arguments.  Initializes test point array.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize array of PSOL commands to be used in the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	# of test points initialized == PLT_NUMBER_OF_TEST_POINTS
//@ End-Method
//=====================================================================
PsolLoopbackTest::PsolLoopbackTest(void)
{
// $[TI1]
	CALL_TRACE("PsolLoopbackTest()");

	Uint32 counter = 0;
	testPoint_[counter++] = 100;
	testPoint_[counter++] = 2100;
	testPoint_[counter++] = 4095;

	CLASS_ASSERTION( counter == PLT_NUMBER_OF_TEST_POINTS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PsolLoopbackTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PsolLoopbackTest::~PsolLoopbackTest(void)
{
	CALL_TRACE("~PsolLoopbackTest()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest()
//
//@ Interface-Description
//	This method performs the PSOL loopback test.  It takes no arguments
//  and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09019]
//	For each gas, check the PSOL loopback current via isLoopbackOk_().
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
PsolLoopbackTest::runTest(void)
{
	CALL_TRACE("PsolLoopbackTest::runTest(void)");

	// Move to the air PSOL loopback
	RSmManager.advanceTestPoint (SmDataId::PSOL_LOOPBACK_AIR_100_CURRENT);

	if (!isLoopbackOk_(&RAirPsol))
	{ // $[TI1]
		// bad PSOL current loopback
		// Minor failure, Condition 1
		// Air PSOL loopback.
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_1);
	}
	// $[TI2]
	// implied else
	
	// Move to the O2 PSOL loopback
	RSmManager.advanceTestPoint (SmDataId::PSOL_LOOPBACK_O2_100_CURRENT);

	if (!isLoopbackOk_(&RO2Psol))
	{ // $[TI3]
		// bad PSOL current loopback
		// Minor failure, Condition 2
		// O2 PSOL loopback.
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_2);
	}
	// $[TI4]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PsolLoopbackTest::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, PSOLLOOPBACKTEST,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isLoopbackOk_()
//
//@ Interface-Description
//	This method is used to test PSOL current loopback for a given
//	PSOL.  A pointer to the PSOL object for the PSOL to be tested
//	is the argument.  Returns TRUE if test passed at all of the
//	test points, else returns FALSE.
//---------------------------------------------------------------------
//@ Implementation-Description
//	For each test point, command the PSOL to the test point, wait
//	for PSOL to stabilize, then compare command current vs loopback
//	current.  Return TRUE if loopback current is in range for all
//	test points, else return FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
PsolLoopbackTest::isLoopbackOk_(Psol* pPsol)
{
	CALL_TRACE("PsolLoopbackTest::isLoopbackOk_(Psol* pPsol)");

    Boolean loopbackPassed = TRUE;
    
    // Test the PSOL at each test point
	for (Int32 testNumber = 0;
	     testNumber < PLT_NUMBER_OF_TEST_POINTS;
	     testNumber++)
	{
	// $[TI1]
		// Open the PSOL to the test point
		pPsol->updatePsol( testPoint_[testNumber]);

		// Wait for the PSOL to stabilize
		Task::Delay( 0, 100);

		// Read the PSOL current loopback
		Real32 loopbackCurrent = pPsol->getCurrent();

		RSmManager.signalTestPoint();

		RSmManager.sendTestPointToServiceData(loopbackCurrent);

		// Convert the PSOL command (counts) into mA
		Real32 commandCurrent = testPoint_[testNumber] * PSOL_COUNTS_TO_MA;

		// Calculate the max error
		Real32 maxError = PSOL_LOOPBACK_MAX_GAIN_ERROR * commandCurrent +
							PSOL_LOOPBACK_MAX_OFFSET_ERROR;  // mA

		if (ABS_VALUE(commandCurrent - loopbackCurrent) > maxError)
		{ 
		// $[TI1.1]
			loopbackPassed = FALSE;
		}
		// $[TI1.2]
		// implied else
		
#ifdef SIGMA_DEBUG
		printf("command count = %d cts\n", testPoint_[testNumber]);
		printf("command current = %7.3f mA\n", commandCurrent);
		printf("loopback current = %7.3f mA\n", loopbackCurrent);
		printf("max error = %7.3f mA\n", maxError);
		printf("current difference = %7.3f mA\n",
			ABS_VALUE(commandCurrent - loopbackCurrent));
#endif  // SIGMA_DEBUG
	}
	// end for

	// Close the PSOL
	pPsol->updatePsol(MIN_DAC_COUNT);

	return( loopbackPassed);
}
