/*****************************************************************************/
/*   Module Name:  copy.c                                        
/*   Purpose:      Purpose                                 
/*                                                                          
/*   Requirements:   This module copy the IsUninst.exe to 
/*					 the windows\840vts directory.
/*          
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/


#include <utility.h>
#include <ansi_c.h>
void main()
{
	DeleteFile("c:\\windows\\840vts\\IsUninst.exe");
	DeleteDir("c:\\windows\\840vts");
	MakeDir("c:\\windows\\840vts");
	CopyFile ("c:\\brthlab\\840vts\\IsUninst.exe", "c:\\windows\\840vts\\IsUninst.exe");
}
