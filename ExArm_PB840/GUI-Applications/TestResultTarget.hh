#ifndef TestResultTarget_HH
#define TestResultTarget_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TestResultTarget - A target for changes in the TestResult.
// Classes can specify this class as an additional parent class and register to
// be informed of changes to specific test result.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TestResultTarget.hhv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

//@ Usage-Classes
#include "GuiAppClassIds.hh"
class TestResult;
//@ End-Usage

class TestResultTarget
{
public:

	virtual void updateChangedData(TestResult *pResult);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	TestResultTarget(void);
	virtual ~TestResultTarget(void);

private:
	// these methods are purposely declared, but not implemented...
	TestResultTarget(const TestResultTarget&);			// not implemented...
	void operator=(const TestResultTarget&);		// not implemented...

};


#endif // TestResultTarget_HH 
