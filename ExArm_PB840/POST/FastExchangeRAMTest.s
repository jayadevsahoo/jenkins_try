;=====================================================================
; This is a proprietary work to which Puritan-Bennett corporation of
; California claims exclusive right.  No part of this work may be used,
; disclosed, reproduced, sorted in an information retrieval system, or
; transmitted by any means, electronic, mechanical, photocopying,
; recording, or otherwise without the prior written permission of
; Puritan-Bennett Corporation of California.
;
;            Copyright (c) 1995, Puritan-Bennett Corporation
;=====================================================================


;============================ M O D U L E   D E S C R I P T I O N ====
;@ Filename: FastExchangeRAMTest.s - C++ Interface to CellIntegrityTest
;                                    for Phase 2 POST
;
;---------------------------------------------------------------------
;@ Interface-Description
;    The FastExchangeRAMTest function implemented in assembly language
;    provides a C++ interface to the CellIntegrityTest contained in the
;    POST-Library subsystem.  It performs the memory test on DRAM not
;    tested by Kernel POST.  The CellIntegrityTest is used by both
;    Phase 1 (Kernel) POST and Phase 2 POST.
;---------------------------------------------------------------------
;@ Rationale
;    Provides a C++ callable interface function to the CellIntegrityTest.
;---------------------------------------------------------------------
;@ Implementation-Description
;    See Interface-Description
;---------------------------------------------------------------------
;@ Fault-Handling
;    A failure of any test in this file is considered a Major Fault and
;    causes an immediate suspension of testing.  The processor is put
;    into a state from which it cannot exit without a power cycle or reset.
;---------------------------------------------------------------------
;@ Restrictions
;    The 68040 data cache controller must be OFF for all memory tests, so
;    as to insure that all memory accesses result in real accesses.  The 
;    instruction cache is turned ON to increase time performance for 
;    this test, implicitly testing the instruction cache.
;---------------------------------------------------------------------
;@ Invariants
;    None
;---------------------------------------------------------------------
;@ End-Preamble
;
;@ Version
; @(#) $Header:   /840/Baseline/POST/vcssrc/FastExchangeRAMTest.s_v   25.0.4.0   19 Nov 2013 14:17:04   pvcs  $
;
;@ Modification-Log
;
;
;  Revision: 003    By: Gary Cederquist  Date: 15-AUG-1997  DR Number: 1875
;    Project: Sigma (R8027)
;    Description:
;       Added testable item annotations.
;
;  Revision: 002    By: Gary Cederquist  Date: 16-MAY-1997 DR Number: 2121
;    Project: Sigma (R8027)
;    Description:
;       Removed assembler options specified in source in favor of
;       command line options.
;
;  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997 DR Number:
;    Project: Sigma (R8027)
;    Description:
;       Initial version
;
;=====================================================================

                INCLUDE  kernel.inc

                XREF        CellIntegrityTest

;============== F R E E   F U N C T I O N   D E S C R I P T I O N =====
;@ Free-Function: FastExchangeRAMTest - Phase 2 POST Cell Integrity Test
;
;@ Interface-Description
;  This is a shell routine, callable from C++, that calls the
;  CellIntegrityTest to test the Phase 2 POST DRAM region.
;  This tests the DRAM not tested in the Kernel test.
;---------------------------------------------------------------------
;@ Implementation-Description
;  Registers Modified: d0,d1,a0,a1 - by MRI C++ convention, these can 
;  be modified
;---------------------------------------------------------------------
;@ PreCondition
;    None
;---------------------------------------------------------------------
;@ PostCondition
;    None
;
;@ End-Free-Function
;=====================================================================

      SECTION  code,,C     

      XDEF   _FastExchangeRAMTest__Fv

; Boolean FastExchangeRAMTest() {
_FastExchangeRAMTest__Fv:
      link     A6,#0
      movem.l  d2-d7/a2-a6,-(sp)             ;save registers that will be used
      move.l   #DRAM_TEST_START,a0           ;defined in kernel.inc
      move.l   #DRAM_TEST_BYTES,d0           ;defined in kernel.inc
      move.l   #return_address,a6
      jmp      CellIntegrityTest
return_address:
      move.l   d7,d0                         ;copy the return status to the C
                                             ; return register

                                             UNIT_TEST_POINT POSTUT_20

      movem.l  (sp)+,d2-d7/a2-a6             ;restore registers that were used
      unlk     A6
; // $[TI1]
      rts
; }


      END
