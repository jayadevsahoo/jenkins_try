#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: ComplianceCalibration - Calibrates compliance compensation
//  for a given patient circuit.
//---------------------------------------------------------------------
//@ Interface-Description
//  Measure the patient circuit compliance to allow compliance
//	compensation during breath delivery.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the SST compliance calibration.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The method runTest() is responsible for measuring the circuit
//  compliance.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class is allowed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ComplianceCalibration.ccv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By: rhj    Date: 14-June-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added PROX compliance calibration.
// 
//  Revision: 011  By: rhj    Date: 06-May-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Modified to show the unblock the wye prompt only when 
//      PROX test is not tested.
// 
//  Revision: 010  By: sah    Date: 30-Jun-1999     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Incorporation of NeoMode Project requirements.
//
//  Revision: 009  By: dosman    Date: 15-May-1999     DR Number: 5391
//  Project:  ATC
//  Description:
//	Change test so that we ask if humidifier is wet regardless
//	of whether ATC is installed on the data key.
//
//  Revision: 008  By: dosman    Date: 02-Feb-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release.
//	Changing the compliance calibration test 
//	to store "total compliance" minus "humidifier compliance".
//	Total compliance is the value sent to (displayed on) GUI.
//	Added #ifdef SIGMA_UNIT_TEST around some code because it was causing
//	compiler warning messages of use before definition during unit test.
//	If the user hits EXIT, then we need to leave the test 
//	(return) immediately.
//
//  Revision: 007  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 006  By:  quf    Date: 11-Nov-1997    DR Number: DCS 2625
//       Project:  Sigma (840)
//       Description:
//			Fixed to select a gas at the start of the test and use
//			the same gas for both the low and high flow phases.
//
//  Revision: 005  By:  quf    Date: 11-Sep-1997    DR Number: DCS 2446
//       Project:  Sigma (840)
//       Description:
//			Adiabatic factor OOR Failure now occurs only if low limit is
//			reached by either factor.  If upper limit is exceeded, the
//			appropriate factor is truncated to its upper limit.  Added
//          method limitAdiabaticFactors_().
//
//  Revision: 004  By:  syw    Date: 11-Aug-1997    DR Number: DCS 2356
//       Project:  Sigma (840)
//       Description:
//			Added adiabatic factor calibration.
//
//  Revision: 003  By:  syw    Date: 08-Jul-1997    DR Number: DCS 2285
//       Project:  Sigma (840)
//       Description:
//			Changed low flow calibration to 10 lpm and high flow to 20 lpm.
//			Removed	calculateHighFlowAndApproxTime_().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix.
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "ComplianceCalibration.hh"

//@ Usage-Classes

#include "SmRefs.hh"
#include "SmFlowController.hh"
#include "SmExhValveController.hh"
#include "SmUtilities.hh"
#include "SmManager.hh"
#include "TestData.hh"
#include "SmConstants.hh"

#include "Task.hh"
#include "OsTimeStamp.hh"
#include "NovRamManager.hh"

#include "MainSensorRefs.hh"
#include "BreathMiscRefs.hh"
#include "PressureSensor.hh"
#include "CircuitCompliance.hh"

#include "PhasedInContextHandle.hh"
#include "PatientCctTypeValue.hh"
#include "ProxTest.hh"
// HumidTypeValue has humidification type values for runTest()
#include "HumidTypeValue.hh"

#if defined(SIGMA_DEVELOPMENT)
#include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "ComplianceCalibration_UT.hh"
# include "FakeIo.hh"
# include "FakeControllers.hh"
#endif  // SIGMA_UNIT_TEST


//@ End-Usage

//@ Constant: COMP_CAL_NUM_TEST_PTS_
// number of compliance calibration test points

// TODO E600 MS Changed from 20 to 15. Change back later...
static const Int32 COMP_CAL_NUM_TEST_PTS_ = 16;

//@ Constant: COMP_CAL_PRESSURE_STEP_
// pressure step size between calibration points
static const Int32 COMP_CAL_PRESSURE_STEP_ = 5;  // cmH2O

//@ Constant: COMP_CAL_LOW_FLOWS_
// flows for compliance low flow calibration
static const Real32 COMP_CAL_LOW_FLOWS_[PatientCctTypeValue::TOTAL_CIRCUIT_TYPES] =
	{
		5.0f,  // L/min -- ADULT circuit...
		5.0f,  // L/min -- PEDIATRIC circuit...
		 3.0f   // L/min -- NEONATAL circuit...
	};

//@ Constant: COMP_CAL_HIGH_FLOWS_
// flow for compliance high flow calibration
static const Real32 COMP_CAL_HIGH_FLOWS_[PatientCctTypeValue::TOTAL_CIRCUIT_TYPES] =
	{
		10.0f,  // L/min -- ADULT circuit...
		10.0f,  // L/min -- PEDIATRIC circuit...
		 6.0f   // L/min -- NEONATAL circuit...
	};

//@ Constant: COMP_CAL_TIMEOUT_
// timeout between calibration points
static const Uint32 COMP_CAL_TIMEOUT_ = 1000;

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ComplianceCalibration()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
ComplianceCalibration::ComplianceCalibration(void)
{
	CALL_TRACE("ComplianceCalibration::ComplianceCalibration()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~ComplianceCalibration()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

ComplianceCalibration::~ComplianceCalibration(void)
{
	CALL_TRACE("ComplianceCalibration::~ComplianceCalibration()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest(void)
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.  It is used
//  to calculate the patient circuit compliance.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[10012]
//	Prompt to block wye.
//	Autozero the pressure transducers.
//	Calculate the flow offset (flow sensor offsets with PSOLs off).
//	Measure compliance, pressurization time, and adiabatic factor using low
//	flow, and if measurement can't be completed due to timeout generate FAILURE
//	and exit test.
//	Measure compliance, pressurization time, and adiabatic factor using high
//	flow, if measurement can't be completed due to timeout generate FAILURE
//	and exit test.
//	If either adiabatic factor is too low, generate a FAILURE and exit test.
//	If either the high flow or low flow adiabatic factor exceeds its
//  maximum limit, set it to the maximum.
//	Check if the compliances measured at high and low flows are in bounds,
//	generate ALERTs and FAILUREs as appropriate.
//	If compliances are within the ALERT criteria bounds or better,
//	store the low flow compliance, low flow pressurization time,
//	high flow compliance, and high flow presurization times in the
//	CircuitCompliance object and in novram.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ComplianceCalibration::runTest( void)
{
	CALL_TRACE("ComplianceCalibration::runTest( void)") ;

	RETAILMSG ( TRUE, ( L"ComplianceCalibration\r\n") );

#ifdef SM_TEST
	Int32 selection;
	printf("Select circuit type, 0 = Adult, 1 = Ped, 2 = Neo: ");
	scanf("%d", &selection);

	if (selection < 0 || selection > 2)
	{
		printf("Illegal value, default to Adult\n");
		selection = 1;
	}
	
	printf("Circuit type = %s\n", (selection == 0) ? "Adult"
				      : ((selection == 1) ? "Ped" : "Neo"));

	PhasedInContextHandle::SetDiscreteValue(SettingId::PATIENT_CCT_TYPE, 
											(PatientCctTypeValue::Id)selection);
#endif  // SM_TEST

	const DiscreteValue  CIRCUIT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	Boolean isNeoCircuit = (CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT);

	SmPromptId::PromptId  smPromptId1;
	if (RProxTest.isProxTestEnabled() && isNeoCircuit)
	{
		smPromptId1  = SmPromptId::PX_BLOCK_SENSOR_OUTPUT_PROMPT;
	}
	else
	{
		smPromptId1  = SmPromptId::BLOCK_WYE_PROMPT;
	}

	if (!RSmUtilities.messagePrompt( smPromptId1))
	{
	// $[TI1]
		RSmManager.sendOperatorExitToServiceData();
	}
	else
	
	{
	// $[TI2]

		// If humidification type is non-HME,
		// then we need to ask whether humidifier is wet (half empty)
		// or dry (empty); otherwise, we do not need to ask.
		// In either case, we need access to isHumidifierWet in a later branch 
		// so we cannot declare isHumidifierWet within a smaller scope.
		// Assume humidifier is dry for the default case -- i.e.,
		// humidifier type is HME.
		Boolean isHumidifierWet = FALSE;

		
		DiscreteValue humidType =
		PhasedInContextHandle::GetDiscreteValue(SettingId::HUMID_TYPE);
		if (humidType != HumidTypeValue::HME_HUMIDIFIER)
		{
		// $[TI2.3]
			// prompt operator "Is water in humidifier?"
			RSmManager.promptOperator(SmPromptId::IS_HUMIDIFIER_WET_PROMPT,
			SmPromptId::ACCEPT_YES_AND_CLEAR_NO_ONLY);

			// get operator keypress and process it
			SmPromptId::ActionId operatorAction =
			RSmManager.getOperatorAction();

			Boolean operatorExit = FALSE;
 
			// record whether humidifier is wet or dry.

			if (operatorAction == SmPromptId::KEY_ACCEPT)
			{
			// $[TI2.3.1]
				// humidifier is wet
				isHumidifierWet = TRUE;
			}
			else if (operatorAction == SmPromptId::USER_EXIT)
			{
			// $[TI2.3.2]
				// EXIT button hit
				operatorExit = TRUE;
			}
			else
			{
			// $[TI2.3.3]
				// humidifier is dry.
				CLASS_ASSERTION (operatorAction == SmPromptId::KEY_CLEAR);
			}

			if (operatorExit)
			{
			// $[TI2.3.4]
				RSmManager.sendOperatorExitToServiceData();
				return;
			}
			// $[TI2.3.5], implied else, not exiting
		} // endif humidType != HME
		// $[TI2.4], implied else, humidType == HME

		

		GasType gasType = RSmUtilities.selectGasType();

		SmUtilities::TestResult result = 
			RSmUtilities.autozeroPressureXducers();

		// TODO E600 MS for debugging only. Remove later.
		if ( result != SmUtilities::AUTOZERO_PASSED )
		{
			RETAILMSG( TRUE, (L"Autozero failed\r\n") );
		}
		// calculate psol leak offsets and flow sensor
		// offsets so they can be subtracted from
		// the measured flows during volume calculation
		RTestData.calculateFlowOffset();

		Real32 highCompliance;
		Real32 highComplianceAt30Cm;
		Uint32 lowFlowTime;
		Real32 lowFlowAdiabaticFactor ;

		// Measure compliance using low flow
#ifdef SIGMA_UNIT_TEST
		Boolean lowFlowResult = ComplianceCalibration_UT::LowFlowResult;
		highCompliance = ComplianceCalibration_UT::HighCompliance;
		// highComplianceAt30Cm is set to ensure definition before use for unit test
		highComplianceAt30Cm = ComplianceCalibration_UT::HighCompliance;
		lowFlowTime = ComplianceCalibration_UT::LowFlowTime;
		lowFlowAdiabaticFactor =
							ComplianceCalibration_UT::LowFlowAdiabaticFactor;
#else

		Boolean lowFlowResult = measureCompliance_(
				COMP_CAL_LOW_FLOWS_[CIRCUIT_TYPE], highCompliance,
				highComplianceAt30Cm, lowFlowTime, lowFlowAdiabaticFactor,
				gasType);

#endif  // SIGMA_UNIT_TEST

		if (!lowFlowResult)
		{
		// $[TI2.1]
	   		// Can't pressurize to a test point
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE,
				SmStatusId::CONDITION_1 );
		}
		else
		{
		// $[TI2.2]
			Real32 lowCompliance;
			Real32 lowComplianceAt30Cm;
			Uint32 highFlowTime;
			Real32 highFlowAdiabaticFactor ;

			// Measure compliance using high flow
#ifdef SIGMA_UNIT_TEST
			Boolean highFlowResult = ComplianceCalibration_UT::HighFlowResult;
			lowCompliance = ComplianceCalibration_UT::LowCompliance;
		      	// lowComplianceAt30Cm is set to ensure definition before use for unit test.
			lowComplianceAt30Cm = ComplianceCalibration_UT::LowCompliance;
			highFlowTime = ComplianceCalibration_UT::HighFlowTime;
			highFlowAdiabaticFactor =
						ComplianceCalibration_UT::HighFlowAdiabaticFactor;
#else
			Boolean highFlowResult = measureCompliance_(
				COMP_CAL_HIGH_FLOWS_[CIRCUIT_TYPE], lowCompliance,
				lowComplianceAt30Cm, highFlowTime, highFlowAdiabaticFactor,
				gasType);

#endif  // SIGMA_UNIT_TEST

			if (!highFlowResult)
			{
			// $[TI2.2.1]
		   		// Can't pressurize to a test point
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_2);
			}
			else if ( lowFlowAdiabaticFactor < MIN_ADIABATIC_FACTOR ||
						highFlowAdiabaticFactor < MIN_ADIABATIC_FACTOR )
			{
			// $[TI2.2.2]
				// adiabatic factors out of range
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE,
					SmStatusId::CONDITION_3);
			}
			else
			{
			// $[TI2.2.3]
				// limit the high end of the adiabatic factors
				limitAdiabaticFactors_( lowFlowAdiabaticFactor,
										highFlowAdiabaticFactor );
					
				// Bounds-check the compliance values
				Boolean highComplianceNonFailure =
					highComplianceTest_(highCompliance);

				Boolean lowComplianceNonFailure =
					lowComplianceTest_(lowCompliance);

				// update CircuitCompliance object and novram ONLY IF
				// there are no FAILURES (ALERTS are OK since
				// they can be overridden)
				if (highComplianceNonFailure && lowComplianceNonFailure)
				{
				// $[TI2.2.3.1]
					Real32 nominalCompliance =
					(highComplianceAt30Cm + lowComplianceAt30Cm) / 2.0F;

					RSmManager.sendTestPointToServiceData(
											nominalCompliance);

#ifdef SIGMA_DEBUG
					printf("Updating CircuitCompliance object and Novram...");
#endif  // SIGMA_DEBUG

					// Implementation decision:
					// We will modify lowCompliance and highCompliance
					// by subtracting out humidifier compliance from
					// the total compliance when storing the value.
					// Then, CircuitCompliance class will be responsible
					// for adding back in the humidifier compliance to get total compliance.
					// The reason that we are subtracting out the humidifier compliance now
					// is so that we do not have to store it.  
					Real32 humidifierCompliance = 
						RCircuitCompliance.getHumidifierCompliance(isHumidifierWet);
					highCompliance -= humidifierCompliance;
					lowCompliance -= humidifierCompliance;

					// Update CircuitCompliance object
					RCircuitCompliance.setHighFlowTime(highFlowTime);
					RCircuitCompliance.setLowFlowTime(lowFlowTime);
					RCircuitCompliance.setHighCompliance(highCompliance);
					RCircuitCompliance.setLowCompliance(lowCompliance);
					RCircuitCompliance.setLowFlowAdiabaticFactor(
												lowFlowAdiabaticFactor );
					RCircuitCompliance.setHighFlowAdiabaticFactor(
												highFlowAdiabaticFactor ); 

					// Update novram
					NovRamManager::UpdateCircuitComplianceTable(
												RCircuitCompliance);

		

#ifdef SIGMA_DEBUG
					printf("DONE!!\n");
#endif  // SIGMA_DEBUG

					if ( !(RProxTest.isProxTestEnabled() && isNeoCircuit))
					{					
						// prompt to unblock wye to prepare for
						// ventilation
						if (!RSmUtilities.messagePrompt(
								SmPromptId::UNBLOCK_WYE_PROMPT))
						{
						// $[TI2.2.3.1.1]
							RSmManager.sendOperatorExitToServiceData();
						}
						
					}
					// $[TI2.2.3.1.2]
					// implied else
				}
				// $[TI2.2.3.2]
				// implied else: compliance FAILURE, so don't
				// save values
			}
			// end else
		}			
		// end else
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 
void
ComplianceCalibration::SoftFault(const SoftFaultID  softFaultID,
				 const Uint32       lineNumber,
				 const char*        pFileName,
				 const char*        pPredicate)
{

  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SERVICE_MODE, COMPLIANCECALIBRATION,
                          lineNumber, pFileName, pPredicate);

}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: highComplianceTest_
//
//@ Interface-Description
//	This method takes a compliance value as an argument and checks if
//	it exceeds the FAILURE or ALERT high compliance criteria,
//	generates the appropriate FAILURE or ALERT message, and
//	returns Boolean TRUE if compliance is within the ALERT limit or
//	better, FALSE if not.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Get current circuit type from Settings-Validation.
//	Check if compliance falls within range -- if not, send the
//	appropriate FAILURE or ALERT message.
//	Return FALSE if a FAILURE occurred, TRUE if not.
//---------------------------------------------------------------------
//@ PreCondition
//	circuit type = ADULT_CIRCUIT or PEDIATRIC_CIRCUIT or NEONATAL_CIRCUIT
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
ComplianceCalibration::highComplianceTest_( Real32 compliance)
{
	CALL_TRACE("ComplianceCalibration::highComplianceTest_( \
												Real32 compliance)");
															
	Boolean noFailure = TRUE;

	// get current circuit type from Settings-Validation
	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);
	
//initial values are given for Neonantal to remove warnigs.
	Real32 highFailureLimit = NEO_COMPLIANCE_FAILURE_HIGH;
	Real32 highAlertLimit = NEO_COMPLIANCE_ALERT_HIGH;
	Real32 lowFailureLimit = NEO_COMPLIANCE_FAILURE_LOW;
	Real32 lowAlertLimit = NEO_COMPLIANCE_ALERT_LOW;

	switch (PATIENT_CCT_TYPE)
	{
	case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
	// $[TI2]
		highFailureLimit = PED_COMPLIANCE_FAILURE_HIGH;
		highAlertLimit   = PED_COMPLIANCE_ALERT_HIGH;
		lowFailureLimit  = PED_COMPLIANCE_FAILURE_LOW;
		lowAlertLimit    = PED_COMPLIANCE_ALERT_LOW;
		break;

	case PatientCctTypeValue::ADULT_CIRCUIT :
	// $[TI1]
		highFailureLimit = ADULT_COMPLIANCE_FAILURE_HIGH;
		highAlertLimit   = ADULT_COMPLIANCE_ALERT_HIGH;
		lowFailureLimit  = ADULT_COMPLIANCE_FAILURE_LOW;
		lowAlertLimit    = ADULT_COMPLIANCE_ALERT_LOW;
		break;

	case PatientCctTypeValue::NEONATAL_CIRCUIT :
	// $[TI8]

		if (RProxTest.isProxTestEnabled())
		{
			highFailureLimit = PROX_NEO_COMPLIANCE_FAILURE_HIGH;
			highAlertLimit   = PROX_NEO_COMPLIANCE_ALERT_HIGH;

		}
		else
		{
			highFailureLimit = NEO_COMPLIANCE_FAILURE_HIGH;
			highAlertLimit   = NEO_COMPLIANCE_ALERT_HIGH;
		}


		lowFailureLimit  = NEO_COMPLIANCE_FAILURE_LOW;
		lowAlertLimit    = NEO_COMPLIANCE_ALERT_LOW;
		break;

	default :
		// unexpected circuit type value...
		AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
		break;
	}

	if (compliance > highFailureLimit)
	{
	// $[TI3]
		// Compliance high FAILURE
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_4);

		noFailure = FALSE;
	}
	else if (compliance > highAlertLimit)
	{
	// $[TI4]
		// Compliance high ALERT
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_5);
	}		
	else if (compliance < lowFailureLimit)
	{
	// $[TI5]
		// Compliance low FAILURE
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_6);

		noFailure = FALSE;
	}
	else if (compliance < lowAlertLimit)
	{
	// $[TI6]
		// Compliance low ALERT
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_7);
	}
	// $[TI7]
	// implied else

	return( noFailure);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: lowComplianceTest_
//
//@ Interface-Description
//	This method takes a compliance value as an argument and checks if
//	it exceeds the FAILURE or ALERT low compliance criteria,
//	generates the appropriate FAILURE or ALERT message, and
//	returns Boolean TRUE if compliance is within the ALERT limit or
//	better, FALSE if not.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Get current circuit type from Settings-Validation.
//	Check if compliance falls within range -- if not, send the
//	appropriate FAILURE or ALERT message.
//	Return FALSE if a FAILURE occurred, TRUE if not.
//---------------------------------------------------------------------
//@ PreCondition
//	circuit type = ADULT_CIRCUIT or PEDIATRIC_CIRCUIT or NEONATAL_CIRCUIT
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
ComplianceCalibration::lowComplianceTest_( Real32 compliance)
{
	CALL_TRACE("ComplianceCalibration::lowComplianceTest_( \
												Real32 compliance)");
															
	Boolean noFailure = TRUE;

	// get current circuit type from Settings-Validation
	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	//initial values are given for Neonantal to remove warnigs.
		Real32 highFailureLimit = NEO_COMPLIANCE_FAILURE_HIGH;
		Real32 highAlertLimit = NEO_COMPLIANCE_ALERT_HIGH;
		Real32 lowFailureLimit = NEO_COMPLIANCE_FAILURE_LOW;
		Real32 lowAlertLimit = NEO_COMPLIANCE_ALERT_LOW;


	switch (PATIENT_CCT_TYPE)
	{
	case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
	// $[TI2]
		highFailureLimit = PED_COMPLIANCE_FAILURE_HIGH;
		highAlertLimit   = PED_COMPLIANCE_ALERT_HIGH;
		lowFailureLimit  = PED_COMPLIANCE_FAILURE_LOW;
		lowAlertLimit    = PED_COMPLIANCE_ALERT_LOW;
		break;

	case PatientCctTypeValue::ADULT_CIRCUIT :
	// $[TI1]
		highFailureLimit = ADULT_COMPLIANCE_FAILURE_HIGH;
		highAlertLimit   = ADULT_COMPLIANCE_ALERT_HIGH;
		lowFailureLimit  = ADULT_COMPLIANCE_FAILURE_LOW;
		lowAlertLimit    = ADULT_COMPLIANCE_ALERT_LOW;
		break;

	case PatientCctTypeValue::NEONATAL_CIRCUIT :
	// $[TI8]
		highFailureLimit = NEO_COMPLIANCE_FAILURE_HIGH;
		highAlertLimit   = NEO_COMPLIANCE_ALERT_HIGH;
		lowFailureLimit  = NEO_COMPLIANCE_FAILURE_LOW;
		lowAlertLimit    = NEO_COMPLIANCE_ALERT_LOW;
		break;

	default :
		// unexpected circuit type value...
		AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
		break;
	}

	if (compliance > highFailureLimit)
	{
	// $[TI3]
		// Compliance high FAILURE
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_8);

		noFailure = FALSE;
	}
	else if (compliance > highAlertLimit)
	{
	// $[TI4]
		// Compliance high ALERT
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_9);
	}		
	else if (compliance < lowFailureLimit)
	{
	// $[TI5]
		// Compliance low FAILURE
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE,
			SmStatusId::CONDITION_10);

		noFailure = FALSE;
	}
	else if (compliance < lowAlertLimit)
	{
	// $[TI6]
		// Compliance low ALERT
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_ALERT,
			SmStatusId::CONDITION_11);
	}
	// $[TI7]
	// implied else

	return( noFailure);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: measureCompliance_
//
//@ Interface-Description
//	This method measures circuit compliance by taking delivered
//	volume measurements as pressure is ramped up.
//	Five arguments:
//		flow rate to used for pressurization
//		weighted compliance (passed as reference),
//		compliance at 30 cmH2O (passed as reference)
//		pressurizing time to 100 cmH2O (passed as reference)
//		gas type
//	Returns Boolean TRUE if compliance measurement is successful,
//	FALSE if not.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Reset linear regression parameters.
//	Initialize Vold = Pold = 0.
//	Establish a flow at passed-in flow level.
//	Delay then read the alpha-filtered pressure offset (Poffset).
//	Reset the flow integrator and close the exhalation valve.
//	Perform the following at 5, 10, 15, ..., 100 cmH2O target pressures:
//		Wait until (expiratory pressure - pressure offset) equals or exceeds
//		the next target pressure level or until timeout occurs.
//		If timeout occurred, return FALSE and exit test.
//		Read delivered volume V and expiratory pressure P.
//		Calculate compliance = [V - Vold] / [P - Pold].
//		Let Vold = V and Pold = P for next iteration.
//		Update linear regression parameters (x = pressure, y = compliance)
//		starting at the 10 cmH2O target pressure (i.e. skip this for
//		the 5 cmH2O target pressure).
//	Stop the flow then measure the adiabatic factor.
//	Open the exhalation valve.
//	Display the compliance and pressurization time, then return TRUE
//	and exit.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Boolean
ComplianceCalibration::measureCompliance_( Real32 flow,
									Real32& testedCompliance,
									Real32& complianceAt30Cm,
									Uint32& pressurizeTime,
									Real32 &adiabaticFactor,
									GasType gasType)
{
	CALL_TRACE("ComplianceCalibration::measureCompliance_( Real32 flow, \
									Real32& testedCompliance, \
									Real32& complianceAt30Cm, \
									Uint32& pressurizeTime, \
									Real32 &adiabaticFactor, \
									GasType gasType)");
		
	Boolean testPassed = TRUE;

	// initialize linear regression sums
	// X = pressure, Y = compliance
	Real32 sumX = 0;  // sum of pressure
	Real32 sumY = 0;  // sum of compliance
	Real32 sumXY = 0; // sum of (pressure * compliance)
	Real32 sumX2 = 0; // sum of (pressure^2)

#if defined(SIGMA_DEBUG)
	struct MeasurementPoints
	{
		Real32 volume ;     // ml
		Real32 pressure ;   // cmH2O
		Real32 compliance ; // ml/cmH2O (dV/dP)
	} data[COMP_CAL_NUM_TEST_PTS_];
#endif  // defined(SIGMA_DEBUG)

	RSmExhValveController.open();

	// start the flow controller
    RSmFlowController.establishFlow( flow, DELIVERY, gasType);

	// delay then read the alpha-filtered exh pressure
	Task::Delay(10);

	Real32 pressureOffset = RExhPressureSensor.getFilteredValue();

	// reset the flow integrator
	RTestData.resetDeliveredVolume();

	// close the exh valve, allowing EV DAC to reach max and
	// without waiting for EV to close
    RSmExhValveController.close(TRUE, FALSE);

	// begin counting time
	OsTimeStamp mainTimer;
	OsTimeStamp timeoutTimer;

	// reset peak pressure
	RTestData.resetPeakPexh() ;
	
	Uint32 timeToNextPressureTarget;
	Real32 currentPressure;
	Real32 currentVolume;
	Real32 currentCompliance;
	Real32 targetPressure;
	Real32 oldPressure = 0.0;
	Real32 oldVolume = 0.0;
    for (Int32 testPoint = 0;
    	testPoint < COMP_CAL_NUM_TEST_PTS_ && testPassed;
    	testPoint++)
    {
	// $[TI1]
		// calculate target pressure for this iteration
    	targetPressure = pressureOffset +
    			(testPoint + 1) * COMP_CAL_PRESSURE_STEP_;

		// build to the target pressure
		do
    	{
		// $[TI1.1]
			Task::Delay( 0, SM_CYCLE_TIME);
			timeToNextPressureTarget = timeoutTimer.getPassedTime();

			// read pressure and volume
			currentPressure = RExhPressureSensor.getValue();
			currentVolume = RTestData.getNetDeliveredVolume();

    	} while ( currentPressure < targetPressure &&
    				timeToNextPressureTarget < COMP_CAL_TIMEOUT_ );

		// update the pressurize time
		pressurizeTime = mainTimer.getPassedTime();


		// reset timeout timer for next loop
		timeoutTimer.now();

		// make sure timeout didn't occur
   		if ( timeToNextPressureTarget >= COMP_CAL_TIMEOUT_ )
   		{
		// $[TI1.2]
			// didn't reach pressure target in time
   			testPassed = FALSE;
   		}
		else
		{    	
		// $[TI1.3]
			// calculate the compliance for this point
			currentCompliance = (currentVolume - oldVolume) /
								(currentPressure - oldPressure);

			// update old values for next iteration
			oldVolume = currentVolume;
			oldPressure = currentPressure;

			// update linear regression values, starting
			// at the 10 cmH2O point
			if (testPoint > 0)
			{
			// $[TI1.3.1]
				sumX += currentPressure;
				sumY += currentCompliance;
				sumXY += currentPressure * currentCompliance;
				sumX2 += currentPressure * currentPressure;
			}
			// $[TI1.3.2]
			// implied else

#if defined(SIGMA_DEBUG)
			data[testPoint].volume = currentVolume;
	      	data[testPoint].pressure = currentPressure;
			data[testPoint].compliance = currentCompliance;
#endif  // defined(SIGMA_DEBUG)

		}
		// end else
	}
	// end for

    RSmFlowController.stopFlowController();
    
	Task::Delay( 0, 200) ;
	Real32 initPressure = RTestData.getPeakPexh() ;
	Real32 finalPressure = RExhPressureSensor.getValue();

	adiabaticFactor = finalPressure / initPressure ;

	
#if defined (SPIRO_DEBUG)
	printf( "Pi = %5.1f  Pf = %5.1f  factor = %f\n",
			initPressure, finalPressure, adiabaticFactor) ;
#endif // defined(SPIRO_DEBUG)

    RSmExhValveController.open();
	RSmUtilities.openExhValve();

	// TODO E600 MS added this since the exhalation valve is very slow to 
	// react so we need to make sure the exh pressure drops before moving
	// on to the next part of the test. Remove when a bettery solution is
	// available.
	Real32 tempPressure = 0;
	Real32 counter = 0;
	do
	{
		Task::Delay ( 0, 200 );
		tempPressure = RExhPressureSensor.getValue();
		
		
	} while ( tempPressure > 5 && ++counter < 5);

#if defined(SIGMA_DEBUG)
	printf("pressurize time = %u msec @ %.1f lpm\n", pressurizeTime, flow);
	printf("pressure offset = %.3f cmH2O @ %.1f lpm\n", pressureOffset, flow);
	printf("adiabatic factor = %.4f\n", adiabaticFactor);
	printf("***** Measured Data @ flow = %.1f lpm *****\n", flow);
	printf("Vol      Pres    Comp\n");
	for( Uint32 idx = 0; idx < testPoint; idx++)
	{
		printf("%-8.3f %-7.3f %-7.3f\n", data[idx].volume,
				data[idx].pressure, data[idx].compliance);
	}
	printf("\n");
	printf("sumX = %.4f\n", sumX);
	printf("sumY = %.4f\n", sumY);
	printf("sumXY = %.4f\n", sumXY);
	printf("sumX2 = %.4f\n", sumX2);
#endif  // defined(SIGMA_DEBUG)

	if (testPassed)
	{
	// $[TI2]
		Uint32 numPts = COMP_CAL_NUM_TEST_PTS_ - 1;
		Real32 xBar = sumX / numPts;
		Real32 yBar = sumY / numPts;
		Real32 slope = (sumXY - numPts * xBar * yBar) /
					   (sumX2 - numPts * xBar * xBar);
		Real32 intercept = yBar - slope * xBar;

		testedCompliance = (3.0F * slope * 30.0F + slope * 60.0F +
						slope * 85.0F) / 5.0F + intercept;

		complianceAt30Cm = slope * 30.0F + intercept;
		
#if defined(SIGMA_DEBUG)
		printf("# of points = %u\n", numPts);
		printf("sumX = %.4f\n", sumX);
		printf("sumY = %.4f\n", sumY);
		printf("sumXY = %.4f\n", sumXY);
		printf("sumX2 = %.4f\n", sumX2);
		printf("xBar = %.4f\n", xBar);
		printf("yBar = %.4f\n", yBar);
		printf("slope = %.4f, intercept = %.4f\n", slope, intercept);
		printf("testedCompliance = %.4f\n", testedCompliance);
		printf("complianceAt30Cm = %.4f\n", complianceAt30Cm);
#endif  // defined(SIGMA_DEBUG)

		RSmManager.sendTestPointToServiceData( testedCompliance);
		RSmManager.sendTestPointToServiceData( pressurizeTime);

	}
	// $[TI3]
	// implied else

	return( testPassed);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: limitAdiabaticFactors_
//
//@ Interface-Description
//	This method is used to limit the upper values of the low flow and
//	high flow adiabatic factors.  Arguments: the low flow and high flow
//	adiabatic factors.  No return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If low flow factor exceeds MAX_ADIABATIC_FACTOR, set it equal to
//	MAX_ADIABATIC_FACTOR.
//	If high flow factor exceeds low flow factor, set it equal to the
//	low flow factor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
ComplianceCalibration::limitAdiabaticFactors_( Real32& lowFlowFactor,
												Real32& highFlowFactor )
{
	CALL_TRACE("ComplianceCalibration::limitAdiabaticFactors_( \
						 Real32& lowFlowFactor, Real32& highFlowFactor)");

	if (lowFlowFactor > MAX_ADIABATIC_FACTOR)
	{
	// $[TI1]
		lowFlowFactor = MAX_ADIABATIC_FACTOR;
	}
	// $[TI2]
	// implied else

	if (highFlowFactor > lowFlowFactor)
	{
	// $[TI3]
		highFlowFactor = lowFlowFactor;
	}
	// $[TI4]
	// implied else
}												
