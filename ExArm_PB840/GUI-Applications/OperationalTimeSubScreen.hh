#ifndef OperationalTimeSubScreen_HH
#define OperationalTimeSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OperationalTimeSubScreen - The screen selected by pressing the
// Operational Time Record button on the Upper Other Screens Subscreen.
// Displays operational times of the major ventilator system modules
// (GUI, vent head, and compressor).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/OperationalTimeSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision:  002  By: yyy   Date:  20-May-1997      DR Number: DCS 2151
//    Project:    Sigma (R8027)
//    Description:
//      Registered the compressor and operational time with NovRam update
//		manager to enable an realtime update of the hours.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"

//@ Usage-Classes
#include "SubScreenTitleArea.hh"
#include "TextField.hh"
#include "NumericField.hh"
#include "NovRamEventTarget.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

// FORNOW
#include "TextButton.hh"

class SubScreenArea;

class OperationalTimeSubScreen : public SubScreen, 
								 public NovRamEventTarget
{
public:
	OperationalTimeSubScreen(SubScreenArea* pSubScreenArea);
	~OperationalTimeSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	// Inherited from NovRamEventTarget
	virtual void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
																	updateId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	OperationalTimeSubScreen(void);								// not implemented...
	OperationalTimeSubScreen(const OperationalTimeSubScreen&);	// not implemented...
	void operator=(const OperationalTimeSubScreen&);			// not implemented...

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen
	SubScreenTitleArea titleArea_;

	//@ Data-Member: compressorOperationalHoursLabel_
	// The text field for displaying the label of compressorOperationalHours_.
	TextField compressorOperationalHoursLabel_;

	//@ Data-Member: compressorOperationalHoursUnit_
	// The text field for displaying the unit of compressor Operational Hours.
	TextField compressorOperationalHoursUnit_;

	//@ Data-Member: compressorOperationalHoursValue_
	// Value for compressorOperationalHours.
	NumericField compressorOperationalHoursValue_;

	//@ Data-Member: systemOperationalHoursLabel_
	// The text field for displaying the label of systemOperationalHours_.
	TextField systemOperationalHoursLabel_;

	//@ Data-Member: systemOperationalHoursUnit_
	// The text field for displaying the unit of systemOperationalHours_.
	TextField systemOperationalHoursUnit_;

	//@ Data-Member: systemOperationalHoursValue_
	// Value for systemOperationalHours.
	NumericField systemOperationalHoursValue_;

};

#endif // OperationalTimeSubScreen_HH 
