
#ifndef DiscreteValue_HH
#define DiscreteValue_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Filename:  DiscreteValue - Type Definition of 'DiscreteValue'.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/DiscreteValue.hhv   25.0.4.0   19 Nov 2013 14:27:22   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: gdc    Date:  09-Feb-2009   SCR Number: 6435
//  Project: 840S
//  Description:
//      Added NO_SELECTION constant as value for when no value has 
//      been selected by the operator for a disrete setting.
//  
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//            Initial version 
//  
//====================================================================

#include "Sigma.hh"

//@ Type:  DiscreteValue
// A discrete value type.
typedef Int16  DiscreteValue;

//@ Constant:  NO_SELECTION_
// A DiscreteValue used to indicate the operator has not selected a
// value for a DiscreteSetting.
static const DiscreteValue NO_SELECTION_ = -1;

#endif // DiscreteValue_HH 
