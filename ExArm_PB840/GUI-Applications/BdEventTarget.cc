#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: BdEventTarget - A target for new breath events.
// Classes can specify this class as an additional parent class and register to
// be informed of occurrences of new breath events.
//---------------------------------------------------------------------
//@ Interface-Description
// This is a virtual base class.  Any classes which need to be informed
// of breath events from the Breath-Delivery subsystem need to inherit
// from this class and to register for the event types with the 
// BdEventRegistrar class.
//---------------------------------------------------------------------
//@ Rationale
// Used to implement the callback mechanism needed to communicate
// new events.
//---------------------------------------------------------------------
//@ Implementation-Description
// If classes need to be informed of breath events, they must do 3
// things: a) inherit from this class, b) define the
// bdEventHappened() method of this class and c) register for
// breath event notices via the BdEventRegistrar class.  They will then
// be informed of new events via the bdEventHappened() method.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/BdEventTarget.ccv   25.0.4.0   19 Nov 2013 14:07:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd		Date:  19-MAY-1995	Number: 
//  Project:  Sigma (R8027)
//  Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "BdEventTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdEventTarget()  [Default Constructor, Protected]
//
//@ Interface-Description
//	The constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdEventTarget::BdEventTarget(void)
{
	CALL_TRACE("BdEventTarget::BdEventTarget(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BdEventTarget  [Destructor, Protected]
//
//@ Interface-Description
//	Nothing to destruct!
//---------------------------------------------------------------------
//@ Implementation-Description
// 	N/A 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BdEventTarget::~BdEventTarget(void)
{
	CALL_TRACE("BdEventTarget::~BdEventTarget(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: bdEventHappened	[virtual]
//
//@ Interface-Description
// Called normally via BdEventRegistrar/Cascade when a new breath event occurs,
// though it may be called directly if needed.  This a virtual function which
// should be overridden in inheriting classes.  Passed the following
// parameters:
// >Von
//  eventId     The type of the breath event.
//  eventStatus The status of the breath event.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//  N/A
//---------------------------------------------------------------------
//@ PreCondition                                              
//  none                                                      
//---------------------------------------------------------------------
//@ PostCondition                                             
//  none                                                      
//@ End-Method                                                
//=====================================================================
                                                              
void                                                          
BdEventTarget::bdEventHappened(EventData::EventId,
    				            EventData::EventStatus,
    				            EventData::EventPrompt eventPrompt)
{                                                             
    CALL_TRACE("BdEventTarget::bdEventHappened(EventData::EventId "
								                "EventData::EventStatus, EventData::EventPrompt eventPrompt)");
                                                              
    // Do nothing                                             
}                                                             
                                                              


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
BdEventTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, BDEVENTTARGET,
									lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
