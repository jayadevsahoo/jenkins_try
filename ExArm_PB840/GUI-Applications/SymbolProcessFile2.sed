
#=====================================================================
# This is a proprietary work to which Puritan-Bennett corporation of
# California claims exclusive right.  No part of this work may be used,
# disclosed, reproduced, stored in an information retrieval system, or
# transmitted by any means, electronic, mechanical, photocopying,
# recording, or otherwise without the prior written permission of
# Puritan-Bennett Corporation of California.
#
#            Copyright (c) 1993, Puritan-Bennett Corporation
#=====================================================================

#=====================================================================
#@ Filename:  SymbolProcessFile.sed -- 'sed' filter file for converting
#                                      all symbol names to symbol values.
#---------------------------------------------------------------------
# This is used by 'processStrings.x', to replace all of the symbol names
# (e.g., '[Vt]') in the '.IN' files, to the corresponding symbol values
# (e.g., 'V{S:T}') in the '.in' files.  This file must be modified when
# new symbols are added or modified.
#---------------------------------------------------------------------
#
#@ Version-Information
# @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SymbolProcessFile2.sev   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
#
#@ Modification-Log
#
#  Revision: 014   By: rhj   Date: 19-Feb-2010   SCR Number: 6436
#  Project:  PROX
#  Description:
#      Added new symbols for PROX.
#
#  Revision: 013   By: mnr   Date: 23-Oct-2009   SCR Number: 6517
#  Project:  S840BUILD3
#  Description:
#      New symbols created for H2O and cmH2O.
#
#  Revision: 012   By: rhj   Date: 10-Nov-2008   SCR Number: 6435
#  Project:  840S
#  Description:
#      Added Vtl.
#
#  Revision: 011   By: rpr    Date: 11-Nov-2008    SCR Number: 6345
#  Project:  840S
#  Description:
#      Modified to support Leak Compensation and Work of Breathing
# 
#  Revision: 010   By: gdc   Date: 20-Jun-2007   SCR Number: 6237
#  Project:  Trend
#  Description:
#      Added Trending related string substitutions.
#
#  Revision: 009   By: ljs   Date: 24-Mar-2003   DR Number: 5313
#  Project:  PAV
#  Description:
#      Added Pav code from original PAV branch tip. 
#
#  Revision: 008   By: erm    Date:  22-Oct-2002    DCS Number: 5818
#  Project:  VTPC
#  Description:
#      VTPC project-related changes:
#      *  remove HI Vti mand  reason: language specific symbol
#      *  remove Vti mand     reason: language specific symbol
#      *  remove HI Vti mand LIMIT     reason: language specific symbol
#
#  Revision: 007   By: heatherw    Date:  07-Mar-2002    DCS Number: 5790
#  Project:  VTPC
#  Description:
#      VTPC project-related changes:
#      *  added HI Vti mand 
#
#  Revision: 006   By: heatherw    Date:  03-Mar-2002    DCS Number: 5790
#  Project:  VTPC
#  Description:
#      VTPC project-related changes:
#      *  added Vti mand
#      *  added Vti
#      *  added HI Vti mand LIMIT
#      *  added HI Vti LIMIT
#
#  Revision: 005   By: sah    Date:  11-Jul-2000    DCS Number: 5313
#  Project:  PAV
#  Description:
#      PAV project-related changes:
#      *  added Plung
#      *  added PAV-based symbols
#
#  Revision: 004   By: sah    Date:  11-Jul-2000    DCS Number: 5755
#  Project:  VTPC
#  Description:
#      VTPC project-related changes:
#      *  changed "minute volume" to "set minute volume"
#      *  added Ti-spont, Ttot, Vlung, Pcarinal, Vdot-lung,
#      *  removed 'Pmean' and 'Ppeak' which are language-dependent
#         (see 'processStrings.x')
#      *  removed obsoleted P_BAR identifier
#
#  Revision 003  By:  sph    Date: 15-Feb-00     DR Number: 5327
#  Project:  NeoMode
#  Description:
#      Removed Pcirc up arrow and Pcirc limit up arrow bar
#
#  Revision 002  By:  gdc    Date: 22-Jul-99     DR Number: 5513
#  Project:  ATC
#  Description:
#      Changed symbols, as per customer feedback.
#
#  Revision 001  By:  gdc    Date: 22-Jul-99     DR Number: 5499
#  Project:  ATC
#  Description:
#      Changed 'gp' to 'p' and split into SymbolProcessFile1.sed and 
#      SymbolProcessFile2.sed for Solaris compatibility.
#
#=====================================================================

# from symbol name into cheap text (some with intermediate symbol names)...
s/\[Ta\]/T{S:A}/g
s/\[Pcirc\]/P{S:CIRC}/g
s/\[Pwye\]/P{S:{c=12,N:Y}}/g
s/\[Vdot wye\]/'V_DOT'{S:{c=12,N:Y}}/g
s/\[Vdot e\]/'V_DOT'{S:E SET}/g
s/\[Vdot e tot\]/'V_DOT'{S:E TOT}/g
s/\[Vdot e spont\]/'V_DOT'{S:E SPONT}/g
s/\[Vti spont\]/V{S:TI SPONT}/g
s/\[Vti\]/V{S:TI}/g
s/\[Vte spont\]/V{S:TE SPONT}/g
s/\[Vte\]/V{S:TE}/g
s/\[Esens\]/E{S:SENS}/g
s/\[Te\]/T{S:E}/g
s/\[Vdot sens\]/'V_DOT'{S:SENS}/g
s/\[HI O2\]/'UP_ARROW'O{S:2}/g
s/\[HI Pcomp\]/'UP_ARROW'P{S:COMP}/g
s/\[HI Vte\]/'UP_ARROW'V{S:TE}/g
s/\[HI Vte LIMIT\]/'UP_ARROW_BAR'V{S:TE}/g
s/\[HI Vti spont\]/'UP_ARROW'V{S:TI SPONT}/g
s/\[HI Vti spont LIMIT\]/'UP_ARROW_BAR'V{S:TI SPONT}/g
s/\[HI Vti LIMIT\]/'UP_ARROW_BAR'V{S:TI}/g
s/\[HI Vdot e tot\]/'UP_ARROW''V_DOT'{S:E TOT}/g
s/\[HI Vdot e tot LIMIT\]/'UP_ARROW_BAR''V_DOT'{S:E TOT}/g
s/\[HI ftot\]/'UP_ARROW'f{S:TOT}/g
s/\[HI ftot LIMIT\]/'UP_ARROW_BAR'f{S:TOT}/g
s/\[HI Pvent\]/'UP_ARROW'P{S:VENT}/g
s/\[Pi\]/P{S:I}/g
s/\[Ti\]/T{S:I}/g
s/\[Ti spont\]/T{S:I SPONT}/g
s/\[HI Ti spont\]/'UP_ARROW'T{S:I SPONT}/g
s/\[HI Ti spont LIMIT\]/'UP_ARROW_BAR'T{S:I SPONT}/g
s/\[Ttot\]/T{S:TOT}/g
s/\[LO O2\]/'DN_ARROW'O{S:2}/g
s/\[LO Vte spont\]/'DN_ARROW'V{S:TE SPONT}/g
s/\[LO Vte spont LIMIT\]/'DN_ARROW_BAR'V{S:TE SPONT}/g
s/\[LO Vte mand LIMIT\]/'DN_ARROW_BAR'V{S:TE MAND}/g
s/\[LO Vdot e tot\]/'DN_ARROW''V_DOT'{S:E TOT}/g
s/\[LO Vdot e tot LIMIT\]/'DN_ARROW_BAR''V_DOT'{S:E TOT}/g
s/\[O2\]/O{S:2}/g
s/\[Vdot max\]/'V_DOT'{S:MAX}/g
s/\[Psens\]/P{S:SENS}/g
s/\[Vt wye\]/V{S:T-{c=12,N:Y}}/g
s/\[Vt\]/V{S:T}/g
s/\[ftot\]/f{S:TOT}/g
s/\[Dsens\]/D{S:SENS}/g
s/\[Pcarinal\]/P{S:CARI}/g
s/\[Plung\]/P{S:LUNG}/g
s/\[Cstatic\]/C{S:STAT}/g
s/\[Rstatic\]/R{S:STAT}/g
s/\[Cdyn\]/C{S:DYN}/g
s/\[Rdyn\]/R{S:DYN}/g
s/\[Cpav\]/C{S:PAV}/g
s/\[Epav\]/E{S:PAV}/g
s/\[Rpav\]/R{S:PAV}/g
s/\[Rtot\]/R{S:TOT}/g
s/\[WOBpt\]/WOB{S:PT}/g
s/\[WOBtot\]/WOB{S:TOT}/g
s/\[WOBe\]/E/g
s/\[WOBr\]/R/g
s/\[P100\]/P{S:0.1}/g
s/\[Vtl\]/V{S:TL}/g
s/\[%LEAK\]/%LEAK/g
s/\[LEAK\]/LEAK/g
s/\[V Leak\]/V{S:LEAK}/g
s/\[H2O\]/H{S:2}O/g
s/\[cmH2O\]/cmH{S:2}O/g
s/\[WYE\]/{S:-{c=12,N:Y}}/g

# finally, replace all of the intermediate symbol names with their
# corresponding octal value...
s/'V_DOT'/\\001/g
s/'DN_ARROW'/\\003/g
s/'UP_ARROW'/\\004/g
s/'DN_ARROW_BAR'/\\012/g
s/'UP_ARROW_BAR'/\\013/g
