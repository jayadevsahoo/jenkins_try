
#ifndef PressureInspTrigger_HH
#define PressureInspTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PressureInspTrigger - Triggers if conditions for pressure 
//	triggering an inspiration are met.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureInspTrigger.hhv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added virtual method getPressureSens() to allow for derived
//		methods of setting the pressure sensitivity. Removed setExhTraj()
//		as dead code.
//
//  Revision: 006  By: syw     Date:  31-Oct-2000    DR Number: 5793
//  Project:  Metabolics
//  Description:
//		Eliminate unused functionality because of new puffless controller
//		implementation.
//
//  Revision: 005  By: syw     Date:  02-Nov-1999    DR Number: 5573
//  Project:  ATC
//  Description:
//      Added pressureTriggerReference_ and setPressureTriggerReference().
//
//  Revision: 006  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 005  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 004  By:  sp    Date:  25-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods and add unit test items.
//
//  Revision: 003  By:  sp    Date:  26-Jun-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Implement new patient trigger's algorithm.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

#  include "Sigma.hh"

#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "PatientTrigger.hh"

//@ End-Usage

class PressureInspTrigger : public PatientTrigger
{
  public:
    PressureInspTrigger(const Trigger::TriggerId triggerid, const Real32 pressureSens);
    virtual ~PressureInspTrigger(void);
    
    virtual void enable();
	
	// virtual to provide variable pressure sensitivity setting
	virtual Real32 getPressureSens(void) const;

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  protected:
    virtual Boolean triggerCondition_();

  private:
    PressureInspTrigger(const PressureInspTrigger&);		// not implemented...
    void   operator=(const PressureInspTrigger&);	// not implemented...
    PressureInspTrigger(void);		// not implemented...

    //@ Data member: isZeroExhFlow_
    // Zero exhalation flow flag.
    Boolean isZeroExhFlow_;

    //@ Data member: flowChangeCount_
    // Counter for flow change. 
    Int32 flowChangeCount_;

    //@ Data member: exhTraj_
    // Exhalation trajectory.
    Real32 exhTraj_;

    //@ Data member: pressureSens_
    // Fixed pressure sensitivity
    const Real32 pressureSens_;

};


// Inlined methods...


#endif // PressureInspTrigger_HH 
