#ifndef	DciString_HH
#define	DciString_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ====================================================================
//@ Class: DciString - Store a null-terminated character string with an
// associated validity tag
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/DCI/vcssrc/DciString.hhv   25.0.4.0   19 Nov 2013 14:01:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002    By: Gary Cederquist  Date: 17-DEC-1997  DR Number: 2605
//  Project: Sigma (R8027)
//  Description:
//      Restructuring of DCI to fix several problems. 
//
//  Revision  1.0	By: jdm	Date: 8/15/95	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include <string.h>

//@ Usage-Classes
#include "DciClassIds.hh"
#include "DciDebug.hh"
#include "DciValueStatus.hh"
//@ End-Usage


	// must be at least 1 larger than largest DCI report field
enum { MAX_DCI_STRING_SIZE = 20 };

class DciString : public DciValueStatus
{
  public:
	DciString(void);
	~DciString(void);

	static	void	SoftFault(const SoftFaultID	softFaultID,
					const	Uint32		lineNumber,
					const	char *		pFileName = NULL,
					const	char *		pPredicate = NULL);

	DciValueStatus::DciValueStatusId get(char * pString = NULL);

	inline	char *	put(const char * pString);
	inline	char *	put(const char byte);

  protected:

  private:
    // TODO E600
	DciString& operator=(const DciString &);	// not implemented

	char	string_[MAX_DCI_STRING_SIZE];
};


// inlined methods...
#include "DciString.in"

#endif	// DciString_HH
