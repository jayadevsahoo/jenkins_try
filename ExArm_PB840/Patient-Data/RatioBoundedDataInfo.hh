#ifndef RatioBoundedDataInfo_HH
#define RatioBoundedDataInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, NellCor/Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// RatioBoundedDataInfo: Patient Data processing agent class that
// handles data types that need to be represented in a negative reverse
// ratio format.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/RatioBoundedDataInfo.hhv   25.0.4.0   19 Nov 2013 14:18:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  20-May-1996   DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "BoundedDataInfo.hh"
#include  "PatientDataId.hh"
#include  "PatientDataClassIds.hh"

class  RatioBoundedDataInfo : public  BoundedDataInfo
{
#if	defined(SIGMA_UNIT_TEST)
  friend void DoTest(void);
#endif //defined(SIGMA_UNIT_TEST)

  public:
    RatioBoundedDataInfo( const PatientDataId::PatientItemId id,
    const Real32 min, const Real32 max, const Real32 lowerChange,
	const Real32 higherChange, const Precision lowerP, const Precision midP,
	const Precision higherP );
    virtual ~RatioBoundedDataInfo(void); 

    static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

  
  protected:
    virtual void modifyData_(void *pValue );
  private:
    RatioBoundedDataInfo(const RatioBoundedDataInfo&);	// not implemented...
    void   operator=(const RatioBoundedDataInfo&);	// not implemented...

    //@ Constant:    UPPER_CHANGE_POINT_
    // This is a precision delimeter value. A data value higher than this
    // value is to use UPPER_PRECISION  and lower than this value is to
	// use MID_PRECISION.
    const Real32  UPPER_CHANGE_POINT_;

    //@ Constant:    MID_PRECISION_
    // This is a precision used for a patient data value that is lower than
	// an upper change point but higher than the mid change point value.
    const Precision  MID_PRECISION_;
};


#endif  //SIGMA_GUI_CPU || SIGMA_COMMON

#endif // RatioBoundedDataInfo_HH 
