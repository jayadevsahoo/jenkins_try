#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Psol - Proportional solenoid class.
//---------------------------------------------------------------------
//@ Interface-Description
//      This class provides methods for driving the PSOLs, for reading
//      the PSOL current, and for setting or retrieving PSOL
//      calibration information (liftoff).  Methods are implemented to
//		output a command to a PSOL; to return the feedback (current);
//		to return the current PSOL command; to return the liftoff value;
// 		and to set the liftoff value.
//---------------------------------------------------------------------
//@ Rationale
//      Abstraction of a PSOL.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The class contains the last command to the PSOL, the DAC
//      address, and the PSOL current sensor.  The liftoff is the only 
//      calibration information.  The method getCurrent returns the PSOL
//		current sensor value.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//        None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Psol.ccv   25.0.4.0   19 Nov 2013 13:54:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 003  By: syw    Date:  25-Jan-1996    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Added psolLiftoff to constructor and initialize private data
//			member so we do not need to get value from novram.
//
//  Revision: 002  By: syw    Date:  05-Dec-1995    DR Number: DCS 562
//  	Project:  Sigma (R8027)
//		Description:
//			Moved updatePsol() method from Posl.in to here.  Removed from
//			updatePsol() method two occurances of "#ifndef SIGMA_UNIT_TEST"
//			code for SAFE_CLASS_PRE_CONDITION().  Removed getCloseLevel()
//			method.
//
//	Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: INITIAL RELEASE
//      Project:  Sigma (R8027)
//      Description:
//      	Initial version
//
//=====================================================================

#include "Psol.hh"
#include "AdcChannels.hh"
#include "DebugAssert.h"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Psol()
//
//@ Interface-Description
//      Constructor.  Four arguments are needed for construction:
//      the PSOL DAC address, the PSOL current sensor, the PSOL id and
//		a liftoff value.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The current PSOL command is initialized to 0, the other
//      data members are initialized with the arguments passed in.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Psol::Psol( const PsolId id,
            const Uint16 psolLiftoff)
: psolId_( id),
  liftoff_(psolLiftoff)
{
#if defined(SIGMA_BD_CPU)
	CALL_TRACE("Psol::Psol( const Register& psolDacPort, \
                const LinearSensor &psolCurrentSensor, \
                const PsolId id, const Uint psolLiftoff)") ;
	            
	bool status = FALSE;
    currPsolCommand_ = 0 ;              // $[TI2]

	switch(psolId_)
	{
	case AIR_PSOL:
		psolDacPort_.setDacId(DAC_AIR_VALVE);
		break;
	case O2_PSOL:
		psolDacPort_.setDacId(DAC_O2_VALVE);
		break;
	case EXH_PSOL:
		psolDacPort_.setDacId(DAC_EXHALATION_VALVE);
		break;
	case DESIRED_FLOW:
		psolDacPort_.setDacId(DAC_DESIRED_FLOW);
		break;
	default:
		DEBUG_ASSERT(status);
		break;
	}
#endif
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Psol ()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      None
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
Psol::~Psol( void)
{
	CALL_TRACE("Psol::~Psol( void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePsol()
//
//@ Interface-Description
//      This method takes one argument, psolCommand, and outputs the command
//      to the PSOL DAC address.  It returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Limit the PSOL command and output the command to the DAC.
//---------------------------------------------------------------------
//@ PreCondition
//      None
//---------------------------------------------------------------------
//@ PostCondition
//      None
//@ End-Method
//=====================================================================
void
Psol::updatePsol( DacCounts psolCommand)
{
	CALL_TRACE("Psol::updatePsol( const DacCounts psolCommand)") ;
	
    // Limit the PSOL command to the DAC limits.
    if (psolCommand < 0)
    {
    	// $[TI1.1]
        psolCommand = 0 ;                       
    }
    else if (psolCommand > MAX_COUNT_VALUE)
    {
    	// $[TI1.2]
        psolCommand = MAX_COUNT_VALUE ;         
    }   // $[TI1.3] implied else 

    // Send the PSOL command to the DAC.

    currPsolCommand_ = psolCommand ;
#if defined(SIGMA_BD_CPU)

    psolDacPort_.setDac( psolCommand) ;

#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault()
//
//@ Interface-Description
//      This method takes four arguments: softFaultID, lineNumber, pFileName,
//      and pPredicate and is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
Psol::SoftFault( const SoftFaultID  softFaultID,
                 const Uint32       lineNumber,
                 const char*        pFileName,
                 const char*        pPredicate)
{
    CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
    
    FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, PSOL, lineNumber,
                             pFileName, pPredicate) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================
