#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: DiagCodeLog - Diagnostic Code Log.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is the base class for all of the diagnostic code logs.  This
//  contains the majority of the functionality for the diagnostic code
//  logs, where the derived classes provide the arrays and size
//  parameters.
//---------------------------------------------------------------------
//@ Rationale
//  This is a location to place common functionality amongst the
//  diagnostic code logs.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A reference to the derived class's array of log entries is used,
//  along with "head" and "tail" indices, to provide the functionality
//  of a queue of entries.
//
//  This class protects against multiple threads by using a multi-reader,
//  single-writer semaphore.  This has the effect of a mutual-exclusion
//  semaphore for "write" access, while allowing multiple "read" accesses
//  through when no "write" access is grabbeed.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//  See the derived classes for the multi-tasking issues.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/DiagCodeLog.ccv   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  sah    Date:  07-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Added calling of 'NovRamManager::ReportAuxillaryInfo()' to
//	verification method.  Also, changed 'SoftFault()' to a non-
//	inlined method.
//
//  Revision: 004  By:  sah    Date:  10-Oct-1997    DCS Number: 2172
//  Project:  Sigma (R8027)
//  Description:
//	Mapped requirement for clearing out diagnostic logs to the
//	'clearOutAllEntries()' method.
//
//  Revision: 003  By:  sah    Date:  24-Sep-1997    DCS Number: 2513
//  Project:  Sigma (R8027)
//  Description:
//	Added new method, 'clearOutAllEntries()', for emptying the
//	contents of a diagnostic log, and issuing an update once the
//	log is cleared.
//
//  Revision: 002  By:  sah    Date:  15-Jul-1997    DCS Number: 2175
//  Project:  Sigma (R8027)
//  Description:
//	Added method for removing the last overall EST/SST result diagnostic
//	in the log.  Also, changed name of 'backoutDiagnostics()' to
//	'backoutLastDiags()', to better distinguish its functionality.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "DiagCodeLog.hh"

//@ Usage-Classes...
#include "Array_CodeLogEntry.hh"
#include "NovRamManager.hh"
#include "NovRamSemaphore.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~DiagCodeLog()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this fault log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

DiagCodeLog::~DiagCodeLog(void)
{
  CALL_TRACE("~DiagCodeLog()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  clearOutAllEntries()
//
//@ Interface-Description
//  Remove all of the diagnostic entries from this log, leaving an empty
//  log.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  Set both 'headIndex_' and 'tailIndex' to 'MAX_ENTRIES_'.  By setting
//  'headIndex_', the log will be set to an empty state, but 'verifySelf()'
//  expects 'tailIndex_' to be set to 'MAX_ENTRIES_' in the empty state,
//  as well.
//
//  $[08055] -- the entries from each diagnostic log shall be deleted upon
//              request. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  (isEmpty())
//@ End-Method 
//===================================================================== 

void
DiagCodeLog::clearOutAllEntries(void)
{
  CALL_TRACE("clearOutAllEntries()");

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  headIndex_ = MAX_ENTRIES_;
  tailIndex_ = MAX_ENTRIES_;  // this must be done (see above)...

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  backoutLastDiags(startingSeqNumber)
//
//@ Interface-Description
//  Remove, from this diagnostic log, all of the diagnostics that have
//  been added up to the sequence number given by 'startingSeqNumber'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagCodeLog::backoutLastDiags(const Uint startingSeqNumber)
{
  CALL_TRACE("backoutLastDiags(startingSeqNumber)");

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  if (!isEmpty())
  {   // $[TI1]
    while (startingSeqNumber <=
			  P_ARR_ENTRIES_[headIndex_].getSequenceNumber())
    {   // $[TI1.1] -- at least one diagnostic later...
      if (headIndex_ != tailIndex_)
      {  // $[TI1.1.1] -- MORE than one entry left in this log...
	headIndex_ = getPrevIndex_(headIndex_);
      }
      else
      {  // $[TI1.1.2] -- ONLY one entry left in this log...
	// removing the only entry in the log, therefore leave as an empty
	// log...
	tailIndex_ = MAX_ENTRIES_;
	headIndex_ = MAX_ENTRIES_;

	// break out of this loop; the list is now empty...
	break;
      }
    }   // $[TI1.2] -- no diagnostics later than 'startingSeqNumber'...
  }   // $[TI2] -- the log is empty...

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  backoutOverallDiag(overallTestNumber)
//
//@ Interface-Description
//  Remove, from this diagnostic log, all diagnostics between the new
//  diagnostic and the latest overall result diagnostic contained in this
//  log.  'overallTestNumber' is used to identify the overall result
//  diagnostic, based on its test number.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagCodeLog::backoutOverallDiag(const Uint overallTestNumber)
{
  CALL_TRACE("backoutOverallDiag(overallTestNumber)");

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  if (!isEmpty())
  {   // $[TI1]
    Boolean  isFound = FALSE;

    while (!isFound)
    {   // $[TI1.1] -- always enters this path...
      const DiagnosticCode&  R_DIAG_CODE =
			       P_ARR_ENTRIES_[headIndex_].getDiagnosticCode();

      if (R_DIAG_CODE.getDiagCodeTypeId() ==
				      DiagnosticCode::EXTENDED_SELF_TEST_DIAG)
      {   // $[TI1.1.1] -- found EST diagnostic, is overall result?...
	const DiagnosticCode::ExtendedSelfTestBits  EST_BITS =
					R_DIAG_CODE.getExtendedSelfTestCode();

	isFound = (EST_BITS.testNumber == overallTestNumber);
      }
      else if (R_DIAG_CODE.getDiagCodeTypeId() ==
					 DiagnosticCode::SHORT_SELF_TEST_DIAG)
      {   // $[TI1.1.2] -- found SST diagnostic, is overall result?...
	const DiagnosticCode::ShortSelfTestBits  SST_BITS =
					R_DIAG_CODE.getShortSelfTestCode();

	isFound = (SST_BITS.testNumber == overallTestNumber);
      }
      else
      {   // $[TI1.1.3] -- neither EST nor SST diagnostic...
	isFound = FALSE;
      }

      if (headIndex_ != tailIndex_)
      {  // $[TI1.1.4] -- MORE than one entry left in this log...
	headIndex_ = getPrevIndex_(headIndex_);
      }
      else
      {  // $[TI1.1.5] -- ONLY one entry left in this log...
	// removing the only entry in the log, therefore leave as an empty
	// log...
	tailIndex_ = MAX_ENTRIES_;
	headIndex_ = MAX_ENTRIES_;

	// break out of this loop; the list is now empty...
	break;
      }
    }
  }   // $[TI2] -- the log is empty...

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);
}


#if defined(SIGMA_GUI_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  updateEntries(newBdEntries)
//
//@ Interface-Description
//  Update the volatile entries of this log with the new log entries
//  from BD.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (NovRamManager::IsVolatileAddr(this))
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagCodeLog::updateEntries(const AbstractArray(CodeLogEntry)& newBdEntries)
{
  CALL_TRACE("updateEntries(newBdEntries)");
  CLASS_PRE_CONDITION((NovRamManager::IsVolatileAddr(this)));

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  headIndex_ = MAX_ENTRIES_;  // make empty...
  tailIndex_ = MAX_ENTRIES_;

  const Uint  LAST_INDEX = MAX_ENTRIES_ - 1u;

  Uint  srcIdx, dstIdx; 

  // copy all of the entries from 'newBdEntries' to this log...
  for (srcIdx = 0u; srcIdx < MAX_ENTRIES_; srcIdx++)
  {   // $[TI1] -- ALWAYS executes this path...
    // calculate the index of the "destination"...
    dstIdx = (LAST_INDEX - srcIdx);

    // copy over the new entry...
    P_ARR_ENTRIES_[dstIdx] = newBdEntries[srcIdx];

    if (!newBdEntries[srcIdx].isClear())
    {   // $[TI1.1] -- non-clear entry...
      // set the "tail" to include this new entry...
      tailIndex_ = dstIdx;

      // set 'headIndex_' to the "last" (and "newest") entry...
      headIndex_ = LAST_INDEX;
    }   // $[TI1.2] -- clear entry...
  }   // end of for loop...

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  // activate the registered callback, if any...
  NovRamUpdateManager::UpdateHappened(UPDATE_TYPE_ID_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  MergeCodeLogs(rMergedLog, arrGuiCodes, arrBdCodes,
//			   maxEntries)  [static]
//
//@ Interface-Description
//  This method is used to merge the entries of two diagnostic code logs
//  (e.g., the GUI's and BD's diagnostic code logs, respectively).  The
//  entries are returned in 'rMergedLog' as a sorted (by time of entry)
//  list of entries.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method takes the two passed arrays of 'CodeLogEntry' instances
//  and determines how these arrays are to be merged into one array.  This
//  determination is based on the number of entries in each (i.e., whether
//  there are corresponding non-"cleared" entries in each of the arrays for
//  each of the entries), the type of entry (i.e., whether it's a system
//  clock update diagnostic), and the relative times of two corresponding
//  entries.  Some of the basic rules include:  if there is not a
//  corresponding non-"cleared" entry from one array to the other, the
//  "non-cleared" entry is to be stored into 'rMergedLog'; if there are no
//  non-"cleared" entries in either array, a "cleared" entry is to be stored
//  into 'rMergedLog'; don't return a system clock update diagnostic for the
//  BD when the GUI's is available; if a system clock update is located,
//  disable the use of the entry's time-stamp for sorting purposes until
//  the other array's corresponding entry is found, or the other array
//  is fully processed; and, finally, if neither of the above rules apply,
//  sort by the entrys' time-stamps.
//--------------------------------------------------------------------- 
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none
//@ End-Method 
//===================================================================== 

void
DiagCodeLog::MergeCodeLogs(AbstractArray(CodeLogEntry)&       rMergedLog,
			   const AbstractArray(CodeLogEntry)& arrGuiCodes,
			   const AbstractArray(CodeLogEntry)& arrBdCodes,
			   const Uint                         maxEntries)
{
  CALL_TRACE("MergeCodeLogs(rMergedLog, arrGuiCodes, arrBdCodes, maxEntries)");

  Uint  guiIdx = 0u;
  Uint  bdIdx  = 0u;

  for (Uint dstIdx = 0u; dstIdx < maxEntries; dstIdx++)
  {   // $[TI1] -- this path is ALWAYS taken...
    Boolean  isGuiEntryAvail = (guiIdx < maxEntries  &&
				!arrGuiCodes[guiIdx].isClear());
    Boolean  isBdEntryAvail  = (bdIdx < maxEntries  &&
				!arrBdCodes[bdIdx].isClear());

    if (isGuiEntryAvail  &&  isBdEntryAvail)
    {   // $[TI1.1] -- both entries are available...
      const DiagnosticCode::DiagCodeTypeId  GUI_DIAG_CODE_TYPE =
		arrGuiCodes[guiIdx].getDiagnosticCode().getDiagCodeTypeId();
      const DiagnosticCode::DiagCodeTypeId  BD_DIAG_CODE_TYPE =
		arrBdCodes[bdIdx].getDiagnosticCode().getDiagCodeTypeId();

      Boolean  isGuiBlockedOnClockUpdate;
      Boolean  isBdBlockedOnClockUpdate;

      if (GUI_DIAG_CODE_TYPE == DiagnosticCode::SYSTEM_EVENT_DIAG)
      {   // $[TI1.1.1]
	// check to see if this System Event Diagnostic is a clock update...
	const DiagnosticCode::SystemEventBits  SYSTEM_EVENT_BITS =
		arrGuiCodes[guiIdx].getDiagnosticCode().getSystemEventCode();
				

	isGuiBlockedOnClockUpdate = (SYSTEM_EVENT_BITS.systemEventId ==
				DiagnosticCode::REAL_TIME_CLOCK_UPDATE_DONE);
      }
      else
      {   // $[TI1.1.2] -- the GUI's entry is NOT a system event...
	isGuiBlockedOnClockUpdate = FALSE;
      }

      if (BD_DIAG_CODE_TYPE == DiagnosticCode::SYSTEM_EVENT_DIAG)
      {   // $[TI1.1.3]
	// check to see if this System Event Diagnostic is a clock update...
	const DiagnosticCode::SystemEventBits  SYSTEM_EVENT_BITS =
		  arrBdCodes[bdIdx].getDiagnosticCode().getSystemEventCode();
				

	isBdBlockedOnClockUpdate = (SYSTEM_EVENT_BITS.systemEventId ==
				DiagnosticCode::REAL_TIME_CLOCK_UPDATE_DONE);

 
        if (SYSTEM_EVENT_BITS.systemEventId ==
			       DiagnosticCode::REAL_TIME_CLOCK_UPDATE_PENDING)
	{   // $[TI1.1.3.1]
	  // throw-away the BD recording of a pending clock update (display
	  // only the GUI recording)...
	  bdIdx++;
	}   // $[TI1.1.3.2] -- this entry is NOT a "pending" clock update...
      }
      else
      {   // $[TI1.1.4] -- the BD's entry is NOT a system event...
	isBdBlockedOnClockUpdate = FALSE;
      }

      if (!isGuiBlockedOnClockUpdate  &&  !isBdBlockedOnClockUpdate)
      {   // $[TI1.1.5]
	// neither array is "blocked" on a clock update, therefore pick
	// the newest entry...
	// NOTE:  since 'bdIdx' could have been incremented above, we must
	//        check to make sure the entry that it refers to is not
	//	  cleared...
	if (arrBdCodes[bdIdx].isClear()  ||
	    arrGuiCodes[guiIdx] > arrBdCodes[bdIdx])
	{   // $[TI1.1.5.1] -- the GUI entry is newer than the BD entry...
	  rMergedLog[dstIdx] = arrGuiCodes[guiIdx++];
	}
	else
	{   // $[TI1.1.5.2] -- the BD entry is newer than the GUI entry...
	  rMergedLog[dstIdx] = arrBdCodes[bdIdx++];
	}
      }
      else if (!isGuiBlockedOnClockUpdate)
      {   // $[TI1.1.6]
	// the GUI entry is NOT blocked while the BD entry is, therefore
	// pick the GUI entry...
	rMergedLog[dstIdx] = arrGuiCodes[guiIdx++];
      }
      else if (!isBdBlockedOnClockUpdate)
      {   // $[TI1.1.7]
	// the BD entry is NOT blocked while the GUI entry is, therefore
	// pick the BD entry...
	rMergedLog[dstIdx] = arrBdCodes[bdIdx++];
      }
      else
      {   // $[TI1.1.8]
	// BOTH entries are "blocked", therefore take the GUI's clock update
	// entry, and skip over the BD's...
	rMergedLog[dstIdx] = arrGuiCodes[guiIdx++];
	bdIdx++;
      }
    }
    else if (isGuiEntryAvail)
    {   // $[TI1.2] -- only the GUI entry is available...
      rMergedLog[dstIdx] = arrGuiCodes[guiIdx++];
    }
    else if (isBdEntryAvail)
    {   // $[TI1.3] -- only the BD entry is available...
      rMergedLog[dstIdx] = arrBdCodes[bdIdx++];
    }
    else
    {   // $[TI1.4] -- NO entries are available...
      rMergedLog[dstIdx].clear();
    }
  }
}

#endif // defined(SIGMA_GUI_CPU)


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//  			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
DiagCodeLog::SoftFault(const SoftFaultID  softFaultID,
		       const Uint32       lineNumber,
		       const char*        pFileName,
		       const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, DIAG_CODE_LOG,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  DiagCodeLog(pArrEntries, maxEntries, updateTypeId)  [Constructor]
//
//@ Interface-Description
//  Construct a fault log that uses the array pointed to by 'pArrEntries'
//  for the log entries.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The array of entries that 'pArrEntries' references is NOT
//  constructed, until AFTER this constructor has completed, therefore
//  it can't be used within this constructor.
//---------------------------------------------------------------------
//@ PreCondition
//  (pArrEntries != NULL)
//  (maxEntries > 0)
//  (updateTypeId >= NovRamUpdateManager::LOW_DIAG_LOG_VALUE  &&
//   updateTypeId <= NovRamUpdateManager::HIGH_DIAG_LOG_VALUE)
//---------------------------------------------------------------------
//@ PostCondition
//  (isEmpty())
//@ End-Method
//=====================================================================

DiagCodeLog::DiagCodeLog(CodeLogEntry* pArrEntries, const Uint  maxEntries,
			 const NovRamUpdateManager::UpdateTypeId updateTypeId)
			 : P_ARR_ENTRIES_(pArrEntries),
			   MAX_ENTRIES_(maxEntries),
			   UPDATE_TYPE_ID_(updateTypeId),
			   headIndex_(maxEntries),
			   tailIndex_(maxEntries)
{
  CALL_TRACE("DiagCodeLog(pArrEntries, maxEntries, updateTypeId)");
  SAFE_CLASS_PRE_CONDITION(
	      ((Byte*)P_ARR_ENTRIES_ == ((Byte*)this + sizeof(DiagCodeLog)))
	  		  );
  SAFE_CLASS_PRE_CONDITION((maxEntries > 0));
  SAFE_CLASS_PRE_CONDITION(
		(updateTypeId >= NovRamUpdateManager::LOW_DIAG_LOG_VALUE  &&
		 updateTypeId <= NovRamUpdateManager::HIGH_DIAG_LOG_VALUE)
		          );
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  logDiagCode_(diagnosticCode)
//
//@ Interface-Description
//  Log the fault indicated by 'diagnosticCode' into this fault log.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Due to some implementation differences between the logging into the
//  different logs, the derived class's log method is responsible for
//  notifying 'NovRamUpdateManager' of the updates.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (!isEmpty())
//@ End-Method
//=====================================================================

void
DiagCodeLog::logDiagCode_(const DiagnosticCode diagnosticCode)
{
  CALL_TRACE("logDiagCode_(diagnosticCode)");

  Uint  newHeadIndex;

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  if (!isEmpty())
  {   // $[TI1]
    // there is already at least one entry in this log, therefore get the
    // next available entry for the new fault...
    newHeadIndex = getNextIndex_(headIndex_);

    if (isFull())
    {   // $[TI1.1]
      // the log is full, therefore remove the last item in the log...
      tailIndex_ = getNextIndex_(newHeadIndex);
    }   // $[TI1.2] -- this log is NOT full...
  }
  else
  {   // $[TI2]
    // this log is currently empty, therefore add this new fault as the
    // only entry in this log...
    newHeadIndex = 0u;
    tailIndex_   = 0u;
  }

  // log the fault indicated by 'diagnosticCode' as a new entry into this
  // log...
  (P_ARR_ENTRIES_[newHeadIndex]).logDiagnosticCode(diagnosticCode);

  // update 'headIndex_' to include the newly logged entry...
  headIndex_ = newHeadIndex;

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::DIAG_LOG_ACCESS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  copyCurrEntries_(rCurrEntries)  [const]
//
//@ Interface-Description
//  Copy all of the current entries from this log into 'rCurrEntries'.
//  The array's index locations that are not currently storing a logged
//  diagnostic, are set to the "cleared" state (see 'isClear()').
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DiagCodeLog::copyCurrEntries_(AbstractArray(CodeLogEntry)& rCurrEntries) const
{
  CALL_TRACE("copyCurrEntries_(rCurrEntries)");

  NovRamSemaphore::RequestReadAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  Uint32  dstIdx = 0;  // starting at entry 0 of 'rCurrEntries'...

  if (!isEmpty())
  {   // $[TI1]
    // this log is NOT empty, therefore...
    const Uint32  FIRST_IDX = headIndex_;
    const Uint32  LAST_IDX  = tailIndex_;

    Uint32  srcIdx;

    // copy, from the newest entry to the oldest entry, each entry from
    // this fault log into 'rCurrEntries'...
    for (srcIdx = FIRST_IDX; srcIdx != LAST_IDX;
	 srcIdx = getPrevIndex_(srcIdx))
    {   // $[TI1.1] -- at least one iteration...
      rCurrEntries[dstIdx++] = P_ARR_ENTRIES_[srcIdx];
    }   // $[TI1.2] -- no iterations...

    // now copy over the last entry indicated by 'tailIndex_'...
    rCurrEntries[dstIdx++] = P_ARR_ENTRIES_[LAST_IDX];
  }   // $[TI2]

  NovRamSemaphore::ReleaseReadAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  // for each of the entries not copied over from this error log, clear
  // the value...
  for (; dstIdx < rCurrEntries.getNumElems(); dstIdx++)
  {   // $[TI3] -- at least one iteration...
    rCurrEntries[dstIdx].clear();  // clear this entry...
  }   // $[TI4] -- no iterations...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initLog_(maxEntries)  [const]
//
//@ Interface-Description
//  This is run as part of a system reset, where the NOVRAM may not have
//  been initialized before.  There are three states that a diagnostic
//  log can be in at this point:  totally valid, "correctable", and
//  "non-correctable".
//
//  If this log is completely valid, the log is left untouched and
//  'NovRamStatus::ITEM_VERIFIED' is returned.
//
//  If this log is NOT valid, but only the entries themselves are invalid
//  (i.e., the internal infrastructure members are fine), the entries are
//  "reset" to contain a diagnostic code that is specific for the
//  "correction" done here.  For this situation the "corrections" leave
//  this log in a completely valid state, and 'NovRamStatus::ITEM_RECOVERED'
//  is returned.
//
//  If this log's internal infrastructure is corrupted (e.g., the "head"
//  or "tail" indices), the log is deemed "non-correctable" and a full
//  intialization is needed.  For this situation the log is left untouched
//  by this method, and 'NovRamStatus::ITEM_INITIALIZED' is returned to
//  indicate that a full initialization is needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The infrastructure members include those members that can only be
//  initialized via an explicit constructor call (i.e., the constant
//  'MAX_ENTRIES_', and the reference 'P_ARR_ENTRIES_'), and the indices
//  into the log's internal array (i.e., 'headIndex_' and 'tailIndex_').
//  If any of these data members are deemed invalid, a full initialization
//  of the log is needed to get back to a valid state.
//
//  When the infrastructure members are deemed valid, the only thing left is
//  the log entries, themselves.  Each entry that is invalid, is "corrected".
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
DiagCodeLog::initLog_(const Uint maxEntries)
{
  CALL_TRACE("initLog_(maxEntries)");

  NovRamStatus::InitializationId  initStatusId;
  Boolean                         isInitNeeded;

  // first test the part of the log that if corrupted will require a full
  // initialization to recover...
  // $[TI1] (TRUE)  $[TI2] (FALSE)...
  isInitNeeded =
  	(UPDATE_TYPE_ID_ < NovRamUpdateManager::LOW_DIAG_LOG_VALUE   ||
	 UPDATE_TYPE_ID_ > NovRamUpdateManager::HIGH_DIAG_LOG_VALUE  ||
	 MAX_ENTRIES_ != maxEntries  				     ||
	 headIndex_   >  maxEntries  				     ||
	 tailIndex_   >  maxEntries  				     ||
	 (headIndex_ != maxEntries  &&  tailIndex_ == maxEntries)    ||
	 (Byte*)P_ARR_ENTRIES_ != ((Byte*)this + sizeof(DiagCodeLog)));

  if (!isInitNeeded)
  {   // $[TI3]
    initStatusId = NovRamStatus::ITEM_VERIFIED;

    if (!isEmpty())
    {   // $[TI3.1]
      // the maximum value and the indices are (at least somewhat) valid,
      // therefore test and "correct" (if necessary) the individual
      // entries...

      Boolean  isDone;
      Uint     idx = tailIndex_;  // starting from the "tail"...

      do
      {   // $[TI3.1.1] -- ALWAYS executes...
	// while there are active entries...

	if (!(P_ARR_ENTRIES_[idx]).isValid())
	{   // $[TI3.1.1.1]
	  // the entry indexed by 'idx' is NOT valid, therefore "correct"
	  // it...
	  (P_ARR_ENTRIES_[idx]).correct();

	  initStatusId = NovRamStatus::ITEM_RECOVERED;
	}   // $[TI3.1.1.2] -- the entry indexed by 'idx' IS valid...

	isDone = (idx == headIndex_);
	 
	if (!isDone)
	{   // $[TI3.1.1.3]
	  // there are more active entries, therefore get the next entry in
	  // the log...
	  idx = getNextIndex_(idx);
	}    // $[TI3.1.1.4] -- done...
      } while (!isDone);
    }   // $[TI3.2] -- the log is currently empty...
  }
  else
  {   // $[TI4] -- a full initialization is needed...
    initStatusId = NovRamStatus::ITEM_INITIALIZED;
  }

  return(initStatusId);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifyLog_(maxEntries)  [const]
//
//@ Interface-Description
//  Verify the integrity of each of the "active" entries of this log.
//  If all of the entries between the "head" and "tail" indices
//  (inclusive) are determined to be valid, 'NovRamStatus::ITEM_VALID' is
//  returned.  If the entries are not valid, 'NovRamStatus::ITEM_INVALID'
//  is returned.
//
//  The derived class passes 'maxEntries' which is the constant, stored
//  in Flash EPROM, that represents the maximum number of entries
//  allowed in this diagnostic code log.  This parameter is used to
//  verify the integrity of the "head" and "tail" indices.
//---------------------------------------------------------------------
//@ Implementation-Description
//  After grabbing a READ-ACCESS semaphore, this method verifies first
//  the data members of this base class, then the "active" entries
//  provided by the derived class.  If a "corruption" is detected in
//  this base class's data members, auxillary information is reported
//  giving this class's beginning address (i.e., 'this') and size.  If
//  one or more entry corruptions are detected, auxillary information
//  is reported giving this entry memory space vital signs.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

NovRamStatus::VerificationId
DiagCodeLog::verifyLog_(const Uint maxEntries) const
{
  CALL_TRACE("verifyLog_(maxEntries)");

  NovRamSemaphore::RequestReadAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  Boolean  isValid;

  // verify that 'MAX_ENTRIES_' is equal to the value stored in FLASH,
  // and that the "head" and "tail" indices are within range of the
  // given maximum value...
  isValid = (UPDATE_TYPE_ID_ >= NovRamUpdateManager::LOW_DIAG_LOG_VALUE   &&
	     UPDATE_TYPE_ID_ <= NovRamUpdateManager::HIGH_DIAG_LOG_VALUE  &&
	     MAX_ENTRIES_ == maxEntries                                   &&
	     headIndex_   <= maxEntries                                   &&
	     tailIndex_   <= maxEntries                                   &&
	     ((headIndex_ == maxEntries  &&  tailIndex_ == maxEntries)  ||
	      (headIndex_ != maxEntries  &&  tailIndex_ != maxEntries))   &&
	     (const Byte*)P_ARR_ENTRIES_ ==
				  ((const Byte*)this + sizeof(DiagCodeLog))
	    ); // $[TI1] (TRUE)  $[TI2] (FALSE)

  if (isValid  &&  !isEmpty())
  {   // $[TI3]
    // the maximum value and indices are valid, now check the entries,
    // themselves...
    Boolean  isDone;
    Uint   idx = tailIndex_;  // starting from the "tail"...

    do
    {   // $[TI3.1] -- ALWAYS executes...
      // while each entry is valid and there are active entries...
      // is the entry indicated by 'idx' currently valid?...
      isValid = (P_ARR_ENTRIES_[idx]).isValid();  // $[TI3.1.1]  $[TI3.1.2]

      // is there NOT any more active entries?...
      isDone = (idx == headIndex_);
       
      if (!isDone)
      {   // $[TI3.1.3]
        // there are more active entries, therefore get the next entry in
        // the log...
        idx = getNextIndex_(idx);
      }    // $[TI3.1.4] -- done...
    } while (isValid  &&  !isDone);

    if (!isValid)
    {   // $[TI3.2] -- at least one "corrupted" entry was detected...
      const Uint  CORRUPTED_MEM_SIZE = (sizeof(CodeLogEntry) * MAX_ENTRIES_);

      NovRamManager::ReportAuxillaryInfo(P_ARR_ENTRIES_, CORRUPTED_MEM_SIZE);
    }   // $[TI3.3] -- all entries are valid...
  }
  else if (!isValid)
  {   // $[TI4] -- invalid data members...
    NovRamManager::ReportAuxillaryInfo(this, sizeof(DiagCodeLog));
  }   // $[TI5] -- this log is empty...

  NovRamSemaphore::ReleaseReadAccess(NovRamSemaphore::DIAG_LOG_ACCESS);

  // convert the boolean 'isValid' to a status...
  return((isValid) ? NovRamStatus::ITEM_VALID		// $[TI6]
  		   : NovRamStatus::ITEM_INVALID);	// $[TI7]
}


//=====================================================================
//
//  External Functions...
//
//=====================================================================

#if defined(SIGMA_DEVELOPMENT)

#include "Ostream.hh"

//============================ M E T H O D   D E S C R I P T I O N ====
// Free-Function:  operator<<(ostr, diagCodeLog)
//
// Interface-Description
//  Output the contents of the diagnostic code log given by 'diagCodeLog'
//  to 'ostr'.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Free-Function
//=====================================================================

Ostream&
operator<<(Ostream& ostr, const DiagCodeLog& diagCodeLog)
{
  CALL_TRACE("operator<<(ostr, diagCodeLog)");

  cout << "DiagCodeLog {" << endl;
  cout << "  MAX_ENTRIES_ = " << diagCodeLog.MAX_ENTRIES_  << endl;
  cout << "  headIndex_   = " << diagCodeLog.headIndex_    << endl;
  cout << "  tailIndex_   = " << diagCodeLog.tailIndex_    << endl;
  cout << "  P_ARR_ENTRIES_ ("  << (void*)diagCodeLog.P_ARR_ENTRIES_
       << "):  \n";

  for (Uint idx = 0u; idx < diagCodeLog.MAX_ENTRIES_; idx++)
  {
    ostr << "  [" << idx << "] = " << diagCodeLog.P_ARR_ENTRIES_[idx] << endl;
  }
 
  cout << "};\n" << endl;

  return(ostr);
}

#endif // defined(SIGMA_DEVELOPMENT)
