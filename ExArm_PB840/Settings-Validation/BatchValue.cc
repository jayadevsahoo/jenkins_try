#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: BatchValue - Union of the possible values for a batch setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a union of the three types of values that a batch setting can
//  contain -- bounded, discrete and sequential.  This type provides a means
//  of storing each of the three possible batch setting values into a single
//  location/variable.  Instances of this type can be initialized based
//  on either of the three value types, and the value of that instance
//  can be set and queried based on which of the three values is stored.
//  There is no means by which the instances of this union can guarantee
//  that the client operates on the value type that is stored within this
//  instance (i.e., either bounded, discrete or sequential values); it is
//  left as the responsibility of the client to ensure that the value type
//  of these instances are treated consistently.
//---------------------------------------------------------------------
//@ Rationale
//  This provides a generic batch storage type.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This type is the union of a bounded value, discrete value and sequential
//  value.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BatchValue.ccv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003 By: srp    Date: 28-May-2002   DR Number: 5898
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 002   By: sah    Date:  03-Jan-00    DR Number: 5327
//  Project: NeoMode
//  Description:
//	Updated for NeoMode
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "BatchValue.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  BatchValue(void)  [Default Constructor]
//
//@ Interface-Description
//  Construct a default batch value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is method is needed out-of-line for array initialization.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchValue::BatchValue(void)
{
  CALL_TRACE("BatchValue()");
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~BatchValue()  [Destructor]
//
//@ Interface-Description
//  Destroy this batch value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is method is needed out-of-line for array initialization.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BatchValue::~BatchValue(void)
{
  CALL_TRACE("~BatchValue()");
}
