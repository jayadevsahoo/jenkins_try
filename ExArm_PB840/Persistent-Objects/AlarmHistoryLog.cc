#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//============================ M O D U L E   D E S C R I P T I O N ====
//@ Class: AlarmHistoryLog - Alarm History Log.
//---------------------------------------------------------------------
//@ Interface-Description
//  This persistent information log contains the history of alarm
//  events.  When an alarm event occurs, an alarm log entry
//  ('AlarmLogEntry') is added to this log.  When an entry is added at
//  a time that the log is full, the oldest entry is removed to make
//  room for the new entry.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides an information log the alarm log entries.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An internal fixed array of alarm log entries ('AlarmLogEntry'),
//  along with indices of the front and back of the log, are used to
//  manage the entries within the log.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/AlarmHistoryLog.ccv   25.0.4.0   19 Nov 2013 14:18:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah    Date:  05-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Added calling of 'NovRamManager::ReportAuxillaryInfo()' to
//	initialization and verification methods.  Also, changed 'SoftFault()'
//	to a non-inlined method.
//
//  Revision: 003  By:  sah    Date:  06-Aug-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Added registering of changes to this log via 'NovRamUpdateManager'.
//
//  Revision: 002  By:  sah    Date:  15-Mar-1997    DCS Number: 1832
//  Project:  Sigma (R8027)
//  Description:
//      Fixed problem with detecting when to initialize, by adding
//	back the 'MAX_ENTRIES_' data member.
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//=====================================================================

#include "AlarmHistoryLog.hh"

//@ Usage-Classes...
#include "TimeStamp.hh"
#include "Array_AlarmLogEntry_MAX_ALARM_ENTRIES.hh"
#include "NovRamSemaphore.hh"
#include "NovRamUpdateManager.hh"
#include "NovRamManager.hh"
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  AlarmHistoryLog()  [Default Constructor]
//
//@ Interface-Description
//  Construct a default alarm history log, creating an empty log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (isEmpty())
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

AlarmHistoryLog::AlarmHistoryLog(void)
				 : MAX_ENTRIES_(::MAX_ALARM_ENTRIES),
				   headIndex_(::MAX_ALARM_ENTRIES),
				   tailIndex_(::MAX_ALARM_ENTRIES)
{
  CALL_TRACE("AlarmHistoryLog(arrEntries)");
  SAFE_CLASS_POST_CONDITION((isEmpty()  &&
			     verifySelf() == NovRamStatus::ITEM_VALID),
			    AlarmHistoryLog);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~AlarmHistoryLog()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this alarm history log.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

AlarmHistoryLog::~AlarmHistoryLog(void)
{
  CALL_TRACE("~AlarmHistoryLog()");
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  initialize()  [const]
//
//@ Interface-Description
//  A reset sequence occurred, therefore verify the integrity of
//  this instance.  If this log is determined to be completely valid,
//  'NovRamStatus::ITEM_VERIFIED' is returned.  If any of this log's
//  entries are not valid, but the overall infrastructure of the instance
//  is valid, then those entries are "corrected" and
//  'NovRamStatus::ITEM_RECOVERED' is returned.  Otherwise, this instance
//  is re-constructed from scratch, and 'NovRamStatus::ITEM_INITIALIZED'
//  is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The initialization involves verifying the integrity of this log's
//  internal infrastructure (i.e., 'headIndex_' and 'tailIndex_'),
//  and "active" log entries.  If the infrastructure is not valid, then
//  a full initialization is needed, thereby leaving empty log.
//  If the infrastructure is valid, then the "active" entries are verified.
//  If any of the entries are invalid, it is left in a "corrected" state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

NovRamStatus::InitializationId
AlarmHistoryLog::initialize(void)
{
  CALL_TRACE("initialize()");

  NovRamStatus::InitializationId  initStatus;

  Boolean  isInitNeeded;

  // $[TI1] (TRUE)  $[TI2] (FALSE)...
  isInitNeeded = (MAX_ENTRIES_ != ::MAX_ALARM_ENTRIES  ||
		  headIndex_   >  MAX_ENTRIES_         ||
		  tailIndex_   >  MAX_ENTRIES_         ||
		  (headIndex_ != MAX_ENTRIES_  &&
					      tailIndex_ == MAX_ENTRIES_));

  if (!isInitNeeded)
  {   // $[TI3]
    initStatus = NovRamStatus::ITEM_VERIFIED;

    if (!isEmpty())
    {   // $[TI3.1]
      // the maximum value and the indices are (at least somewhat) valid,
      // therefore test, and "correct" if necessary, the individual
      // entries...

      Boolean  isDone;
      Uint32   idx = tailIndex_;  // starting from the "tail"...

      do
      {   // $[TI3.1.1] -- ALWAYS executes...
	// while there are active entries...

	if (!(arrEntries_[idx]).isValid())
	{   // $[TI3.1.1.1]
	  // the entry indexed by 'idx' is NOT valid, therefore "correct"
	  // it...

	  // initialize the entry from scratch...
	  new (arrEntries_ + idx) AlarmLogEntry();

	  // "correct" entry by setting to known "NOVRAM correction"
	  // status...
	  (arrEntries_[idx]).correct();

	  initStatus = NovRamStatus::ITEM_RECOVERED;
	}   // $[TI3.1.1.2] -- the entry indexed by 'idx' IS valid...

	isDone = (idx == headIndex_);
	 
	if (!isDone)
	{   // $[TI3.1.1.3]
	  // there are more active entries, therefore get the next entry in
	  // the log...
	  idx = getNextIndex_(idx);
	}    // $[TI3.1.1.4] -- done...
      } while (!isDone);

      if (initStatus == NovRamStatus::ITEM_RECOVERED)
      {   // $[TI3.1.2] -- "corruption" detected...
	// a corruption was detected somewhere in the "code entries" portion of
	// this instance's memory, therefore report to this effect...
	NovRamManager::ReportAuxillaryInfo(arrEntries_, sizeof(arrEntries_),
					   initStatus);
      }   // $[TI3.1.3] -- all "active" entries valid...
    }   // $[TI3.2] -- the log is currently empty...
  }
  else
  {   // $[TI4] -- a full initialization is needed...
    new (this) AlarmHistoryLog();

    initStatus = NovRamStatus::ITEM_INITIALIZED;

    // a corruption was detected somewhere in this class's "management" data
    // members, therefore report to this effect...
    NovRamManager::ReportAuxillaryInfo(this, getEntriesOffset_(), initStatus);
  }

  SAFE_CLASS_POST_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID),
  			    AlarmHistoryLog);

  return(initStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  verifySelf()  [const]
//
//@ Interface-Description
//  Verify the integrity of this Alarm History Log.  If this log is
//  determined to be completely valid, 'NovRamStatus::ITEM_VALID' is
//  returned.  If any part of this log is determined to be invalid,
//  then 'NovRamStatus::ITEM_INVALID' is returned.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This uses '::MAX_ALARM_ENTRIES' which is the constant, stored
//  in Flash EPROM, that represents the maximum number of entries
//  allowed in the Alarm History Log.  This parameter is used to
//  verify the integrity of the "head" and "tail" indices.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID  ||
//   verifySelf() == NovRamStatus::ITEM_INVALID)
//@ End-Method
//=====================================================================

NovRamStatus::VerificationId
AlarmHistoryLog::verifySelf(void) const
{
  CALL_TRACE("verifySelf()");

  NovRamSemaphore::RequestReadAccess(NovRamSemaphore::ALARM_LOG_ACCESS);

  Boolean  isValid;

  // verify that the "head" and "tail" indices are within range of the
  // given maximum value...
  isValid = (MAX_ENTRIES_ == ::MAX_ALARM_ENTRIES  &&
	     headIndex_   <= MAX_ENTRIES_         &&
	     tailIndex_   <= MAX_ENTRIES_);   // $[TI1]  $[TI2]

  if (isValid  &&  !isEmpty())
  {   // $[TI3]
    // the maximum value and indices are valid, now check the entries,
    // themselves...
    Boolean  isDone;
    Uint32   idx = tailIndex_;  // starting from the "tail"...

    do
    {   // $[TI3.1] -- ALWAYS executes...
      // while each entry is valid and there are active entries...
      // is the entry indicated by 'idx' currently valid?...
      isValid = (arrEntries_[idx]).isValid();

      isDone = (idx == headIndex_);
       
      if (!isDone)
      {   // $[TI3.1.1]
        // there are more active entries, therefore get the next entry in
        // the log...
        idx = getNextIndex_(idx);
      }    // $[TI3.1.2] -- done...
    } while (isValid  &&  !isDone);
  }
  else if (!isValid)
  {   // $[TI4] -- invalid members...
    static const size_t  NON_ENTRY_SIZE_ =
			    (size_t)&(((AlarmHistoryLog*)0)->arrEntries_);

    NovRamManager::ReportAuxillaryInfo(this, NON_ENTRY_SIZE_);
  }   // $[TI5] -- this log is empty...

  NovRamSemaphore::ReleaseReadAccess(NovRamSemaphore::ALARM_LOG_ACCESS);

  return((isValid) ? NovRamStatus::ITEM_VALID		// $[TI6]
  		   : NovRamStatus::ITEM_INVALID);	// $[TI7]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getCurrEntries(rArrEntries)  [const]
//
//@ Interface-Description
//  Store into 'rArrEntries' all of the current Alarm History Log
//  entries, in an order from most recent -- at index '0' -- to least
//  recent.  If this log is not full, the remaining entries will be
//  "cleared" to indicate that they are not part of the current entries.
//  For example, if there are 14 current entries in this log, the entries
//  at, and beyond, index '14' will be cleared, and the entries from '0'
//  thru '13' will not.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//---------------------------------------------------------------------
//@ PostCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

void
AlarmHistoryLog::getCurrEntries(
		  FixedArray(AlarmLogEntry,MAX_ALARM_ENTRIES)& rArrEntries
			       ) const
{
  CALL_TRACE("getCurrEntries(rArrEntries)");
  CLASS_PRE_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID));

  NovRamSemaphore::RequestReadAccess(NovRamSemaphore::ALARM_LOG_ACCESS);

  Uint32  dstIdx = 0;  // starting at entry 0 of 'rArrEntries'...

  if (!isEmpty())
  {   // $[TI1]
    // this log is NOT empty, therefore...
    const Uint32  FIRST_IDX = headIndex_;
    const Uint32  LAST_IDX  = tailIndex_;

    Uint32  srcIdx;

    // copy, from the newest entry to the oldest entry, each entry from
    // this alarm log into 'rArrEntries'...
    for (srcIdx = FIRST_IDX; srcIdx != LAST_IDX;
	 srcIdx = getPrevIndex_(srcIdx))
    {   // $[TI1.1] -- at least one iteration...
      rArrEntries[dstIdx++] = arrEntries_[srcIdx];
    }   // $[TI1.2] -- no iterations...

    // now copy over the last entry indicated by 'tailIndex_'...
    rArrEntries[dstIdx++] = arrEntries_[srcIdx];
  }   // $[TI2] -- the log is empty...

  NovRamSemaphore::ReleaseReadAccess(NovRamSemaphore::ALARM_LOG_ACCESS);

  // for each of the entries not copied over from this error log, clear
  // the value...
  for (; dstIdx < MAX_ENTRIES_; dstIdx++)
  {   // $[TI3] -- at least one iteration...
    rArrEntries[dstIdx].clear();  // clear this entry...
  }   // $[TI4] -- no iterations...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  logAlarmAction(alarmAction)
//
//@ Interface-Description
//  Log the alarm event indicated by 'alarmAction' into this log.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If the log is already full, overwrite the "oldest" entry with
//  this new entry.
//---------------------------------------------------------------------
//@ PreCondition
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//---------------------------------------------------------------------
//@ PostCondition
//  (!isEmpty())
//  (verifySelf() == NovRamStatus::ITEM_VALID)
//@ End-Method
//=====================================================================

void
AlarmHistoryLog::logAlarmAction(const AlarmAction& alarmAction)
{

  CALL_TRACE("logAlarmAction(alarmAction)");
  SAFE_CLASS_PRE_CONDITION((verifySelf() == NovRamStatus::ITEM_VALID));

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::ALARM_LOG_ACCESS);

  Uint  newHeadIndex;

  if (!isEmpty())
  {   // $[TI1]
    // there is already at least one entry in this log, therefore get the
    // next available entry for the new fault...
    newHeadIndex = getNextIndex_(headIndex_);

    if (isFull())
    {   // $[TI1.1]
      // the log is full, therefore remove the last item in the log...
      tailIndex_ = getNextIndex_(newHeadIndex);
    }   // $[TI1.2] -- this log is NOT full...
  }
  else
  {   // $[TI2]
    // this log is currently empty, therefore add this new fault as the
    // only entry in this log...
    newHeadIndex = 0u;
    tailIndex_   = 0u;
  }

  // log the fault indicated by 'alarmAction' as a new entry into this
  // log...
  (arrEntries_[newHeadIndex]).logAction(alarmAction);

  // update 'headIndex_' to include the newly logged entry...
  headIndex_ = newHeadIndex;

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::ALARM_LOG_ACCESS);

  NovRamUpdateManager::UpdateHappened(NovRamUpdateManager::ALARM_LOG_UPDATE);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  clearAlarmEntries()
//
//@ Interface-Description
//  Clear out the alarm entries in this log, such that this log is
//  considered "empty".
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (isEmpty())
//@ End-Method
//=====================================================================

void
AlarmHistoryLog::clearAlarmEntries(void)
{
  CALL_TRACE("clearAlarmEntries()");

  NovRamSemaphore::RequestWriteAccess(NovRamSemaphore::ALARM_LOG_ACCESS);

  // reset the indices to make an empty log...
  headIndex_ = MAX_ENTRIES_;
  tailIndex_ = MAX_ENTRIES_;

  NovRamSemaphore::ReleaseWriteAccess(NovRamSemaphore::ALARM_LOG_ACCESS);

  NovRamUpdateManager::UpdateHappened(NovRamUpdateManager::ALARM_LOG_UPDATE);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//  			[static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method 
//===================================================================== 

void
AlarmHistoryLog::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, PERSISTENT_OBJS, ALARM_HISTORY_LOG,
                          lineNumber, pFileName, pPredicate);
}

