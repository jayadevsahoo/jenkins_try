#ifndef SmPromptId_HH
#define SmPromptId_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SmPromptId - defines prompt, keys allowed, and action ID's
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmPromptId.hhv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 024   By: mnr   Date: 04-May-2010    	SCR Number: 39
//  Project:  PROX
//  Description:
//		Added prompt for Prox SST Pressure cross check test
//
//  Revision: 023   By: mnr      Date: 24-Mar-2010   SCR Number: 6436
//  Project: PROX4
//  Description:
//   Added PROX sensor installation prompt for SST.
//
//  Revision: 022   By: erm      Date: 22-Jan-2002   DR Number: 5984
//  Project: GuiComms
//  Description:
//   Added CONNECT_CABLE_PROMPT for use during External LoopBack.
//
//  Revision: 021   By: dosman   Date: 21-Jan-1999   DR Number:
//       Project:  ATC
//       Description:
//         Added new prompt to ask user if humidifier is wet
//		for the compliance calibration test
//		and added a new KeysAllowedId so that the user
//          	can answer a yes/no question.
//
//  Revision: 020   By: syw   Date: 27-Aug-1998   DR Number:
//       Project:  BiLevel
//       Description:
//          BiLevel Initial Release.  Added INSERT_DK_TO_BE_COPIED_PROMPT
//			and INSERT_DK_TO_BE_COPIED_INTO_PROMPT.
//
//  Revision: 006  By:  quf    Date: 08-Oct-1997    DR Number: DCS 2326, 2443
//       Project:  Sigma (840)
//       Description:
//		 	Added PromptId CONNECT_HUMIDIFIER_PROMPT, replaced
//			CONNECT_INSP_FILTER_PROMPT with OBSOLETE_PROMPT_0
//
//  Revision: 005  By:  quf    Date: 01-Oct-1997    DR Number: DCS 1985
//       Project:  Sigma (840)
//       Description:
//            Assigned specific values to enumerators.
//
//  Revision: 004  By:  quf    Date: 23-Sep-1997    DR Number: DCS 2204
//       Project:  Sigma (840)
//       Description:
//            Added PromptId DISCONNECT_AIR_AND_O2_PROMPT and
//            UNPLUG_AC_PROMPT for revised EST Battery Test.
//
//  Revision: 003  By:  quf    Date: 30-Jul-1997    DR Number: DCS 2213
//       Project:  Sigma (840)
//       Description:
//            Added new KeysAllowedId.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  08-Nov-95    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//====================================================================

//@ Usage-Classes

//@ End-Usage


class SmPromptId
{
  public:
	//@ Type: PromptId
	// This enum lists all of the prompt IDs used by the
	// Service Mode tests.
	enum PromptId
	{
		  NULL_PROMPT_ID = -1

		, OBSOLETE_PROMPT_0 = 0
		, REMOVE_INSP_FILTER_PROMPT = 1
		, ATTACH_CIRCUIT_PROMPT = 2
		, UNBLOCK_WYE_PROMPT = 3
		, BLOCK_WYE_PROMPT = 4
		, UNBLOCK_INLET_PORT_PROMPT = 5
		, BLOCK_INLET_PORT_PROMPT = 6
		, DISCONNECT_AIR_PROMPT = 7
		, DISCONNECT_O2_PROMPT = 8
		, CONNECT_AIR_PROMPT = 9
		, CONNECT_AIR_IF_AVAIL_PROMPT = 10
		, CONNECT_O2_PROMPT = 11
		, DISCNT_TUBE_BFR_EXH_FLTR_PROMPT = 12
		, CONCT_TUBE_BFR_EXH_FLTR_PROMPT = 13
		, DISCONNECT_TUBE_FROM_INLET = 14
		, CONNECT_TUBE_TO_INLET = 15
		, ATTACH_GOLD_STD_TEST_TUBING_PROMPT = 16
		, CONNECT_WALL_AIR_PROMPT = 17
		, KEY_ACCEPT_PROMPT = 18
		, KEY_CLEAR_PROMPT = 19
		, KEY_INSP_PAUSE_PROMPT = 20
		, KEY_EXP_PAUSE_PROMPT = 21
		, KEY_MAN_INSP_PROMPT = 22
		, KEY_HUNDRED_PERCENT_O2_PROMPT = 23
		, KEY_INFO_PROMPT = 24
		, KEY_ALARM_RESET_PROMPT = 25
		, KEY_ALARM_SILENCE_PROMPT = 26
		, KEY_ALARM_VOLUME_PROMPT = 27
		, KEY_SCREEN_BRIGHTNESS_PROMPT = 28
		, KEY_SCREEN_CONTRAST_PROMPT = 29
		, KEY_SCREEN_LOCK_PROMPT = 30
		, KEY_LEFT_SPARE_PROMPT = 31
		, TURN_KNOB_LEFT_PROMPT = 32
		, TURN_KNOB_RIGHT_PROMPT = 33
		, NORMAL_PROMPT = 34
		, VENTILATOR_INOPERATIVE_PROMPT = 35
		, SAFETY_VALVE_OPEN_PROMPT = 36
		, LOSS_OF_GUI_PROMPT = 37
		, BATTERY_BACKUP_READY_PROMPT = 38
		, COMPRESSOR_READY_PROMPT = 39
		, COMPRESSOR_OPERATING_PROMPT = 40
		, COMPRESSOR_MOTOR_ON_PROMPT = 41
		, COMPRESSOR_MOTOR_OFF_PROMPT = 42
		, HUNDRED_PERCENT_O2_PROMPT = 43
		, ALARM_SILENCE_PROMPT = 44
		, SCREEN_LOCK_PROMPT = 45
		, HIGH_ALARM_PROMPT = 46
		, MEDIUM_ALARM_PROMPT = 47
		, LOW_ALARM_PROMPT = 48
		, ON_BATTERY_POWER_PROMPT = 49
		, SOUND_PROMPT = 50
		, POWER_DOWN_PROMPT = 51
		, GUI_SIDE_TEST_PROMPT = 52
		, VENT_INOP_A_TEST_PROMPT = 53
		, VENT_INOP_B_TEST_PROMPT = 54
		, TEN_SEC_A_TEST_PROMPT = 55
		, TEN_SEC_B_TEST_PROMPT = 56
		, NURSE_CALL_TEST_PROMPT = 57
		, NURSE_CALL_ON_PROMPT = 58
		, NURSE_CALL_OFF_PROMPT = 59
		, EV_CAL_PRESSURE_SENSOR_ALERT_PROMPT = 60
		, CONNECT_AC_PROMPT = 61
		, CONNECT_AIR_AND_O2_PROMPT = 62
		, DISCONNECT_AIR_AND_O2_PROMPT = 63
		, ACCEPT_TO_STOP_FLOW_PROMPT = 64
		, CONNECT_HUMIDIFIER_PROMPT = 65
		, UNPLUG_AC_PROMPT = 66
		, INSERT_DK_TO_BE_COPIED_TO_PROMPT = 67
		, INSERT_DK_TO_BE_COPIED_FROM_PROMPT = 68
		, IS_HUMIDIFIER_WET_PROMPT = 69
		, CONNECT_CABLE_PROMPT = 70
		, PROX_SENSOR_PROMPT = 71
		, CONNECT_PROX_SENSOR_PROMPT = 72
		, BLOCK_EXP_PORT_PROMPT = 73
		, PX_REMOVE_INSP_FILTER_PROMPT = 74
		, PX_REMOVE_EXP_LIMB_PROMPT = 75
		, PX_UNBLOCK_SENSOR_OUTPUT_PROMPT = 76
		, PX_BLOCK_SENSOR_OUTPUT_PROMPT = 77
		, UNBLOCK_EXP_PORT_PROMPT = 78
		, NUM_PROMPT_ID
	};


	//@ Type: KeysAllowedId
	// This enum lists the combinations of acceptable keypresses
	// in response to a prompt, e.g. only the accept key is
	// allowed to be pressed in response to a given prompt
	enum KeysAllowedId
	{
		  NULL_KEYS_ALLOWED_ID = -1

		, ACCEPT_KEY_ONLY = 0
		, ACCEPT_AND_CANCEL_ONLY = 1
		, NO_KEYS = 2
		, ACCEPT_PASS_AND_CANCEL_FAIL_ONLY = 3
		, USER_EXIT_ONLY =4
		, ACCEPT_YES_AND_CLEAR_NO_ONLY = 5
		, IF_CONNECTED_ACCEPT_YES_AND_CLEAR_NO_ONLY = 6
		, NUM_KEYS_ALLOWED_ID
	};

  
	//@ Type: ActionId
	// This enum lists the operator keypress actions
	enum ActionId
	{
		  NULL_ACTION_ID = -1

		, USER_EXIT = 0
		, KEY_ACCEPT = 1
		, KEY_CLEAR = 2

		, NUM_ACTION_ID
	};

};

#endif // SmPromptId_HH 
