// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#pragma comment(linker, "/nodefaultlib:libc.lib")
#pragma comment(linker, "/nodefaultlib:libcd.lib")

// NOTE - this value is not strongly correlated to the Windows CE OS version being targeted
#define WINVER _WIN32_WCE

#include <ceconfig.h>
#if defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
#define SHELL_AYGSHELL
#endif

#ifdef _CE_DCOM
#define _ATL_APARTMENT_THREADED
#endif

#include <windows.h>
#include <commctrl.h>



// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include "winsock2.h"

#if defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP)
#ifndef _DEVICE_RESOLUTION_AWARE
#define _DEVICE_RESOLUTION_AWARE
#endif
#endif

#ifdef _DEVICE_RESOLUTION_AWARE
#include "DeviceResolutionAware.h"
#endif

#if _WIN32_WCE < 0x500 && ( defined(WIN32_PLATFORM_PSPC) || defined(WIN32_PLATFORM_WFSP) )
	#pragma comment(lib, "ccrtrtti.lib")
	#ifdef _X86_	
		#if defined(_DEBUG)
			#pragma comment(lib, "libcmtx86d.lib")
		#else
			#pragma comment(lib, "libcmtx86.lib")
		#endif
	#endif
#endif

#include <altcecrt.h>

#ifdef USE_PCH
#include "sensor.hh"
#include "sigma.hh"
#include "bd_io_devices.hh"
#include "breath_delivery.hh"
#include "alarm.hh"
#include "networkappclassids.hh"
#include "pressuresensor.hh"
#include "faulthandlermacros.hh"
#include "smflowcontroller.hh"
#include "nonvolatilememory.hh"
#include "violationhistorymanager.hh"
#include "sigmatypes.hh"
#include "a2dreadingsstructs.h"
#include "sys_init.hh"
#include "networkapp.hh"
#include "exhflowsensor.h"
#include "linearsensor.hh"
#include "resistance.hh"
#include "networksocket.hh"
#include "calltracemacros.hh"
#include "sigmaconstants.hh"
#include "smmanager.hh"
#include "bdqueuesmsg.hh"
#include "alarmconstants.hh"
#include "patientdatamgr.hh"
#include "breathset.hh"
#include "psol.hh"
#include "safetynettest.hh"
#include "novramsemaphore.hh"
#include "messagenamearray.hh"
#include "proxiinterface.hh"
#include "xmitdata.hh"
#include "operatingparameventname.hh"
#include "breathphasescheduler.hh"
#include "templatemacros.hh"
#include "operatinggroup.hh"
#include "timestamp.hh"
#include "osfoundation.hh"
#include "ipcids.hh"
#endif
