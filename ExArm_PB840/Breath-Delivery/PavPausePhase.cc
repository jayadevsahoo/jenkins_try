#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PavPausePhase -  Implements a PAV pause phase.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from the base class BreathPhase.  Virtual
//      methods are implemented to do initialization before Pav pause
//		can begin, to execute the Pav pause every BD cycle, to wrap up
//      the pause phase to be executed at the end of Pav phase.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements Pav pause phase.	
//---------------------------------------------------------------------
//@ Implementation-Description
//      The exhalation valve is closed during this phase and the psols are
//		commanded closed.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		Only limited to one instantiation.
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PavPausePhase.ccv   25.0.4.0   19 Nov 2013 14:00:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 001   By: syw   Date:  14-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV initial release
//
//=====================================================================

#include "PavPausePhase.hh"

#include "ModeTriggerRefs.hh"
#include "ControllersRefs.hh"
#include "MainSensorRefs.hh"
#include "TriggersRefs.hh"
#include "BreathMiscRefs.hh"

//@ Usage-Classes

#include "DisconnectTrigger.hh"
#include "FlowController.hh"
#include "PhasedInContextHandle.hh"
#include "PressureSensor.hh"
#include "ExhValveIController.hh"
#include "HighCircuitPressureExpTrigger.hh"
#include "ImmediateBreathTrigger.hh"
#include "ApneaInterval.hh"
#include "PavManager.hh"

//@ End-Usage

//@ Constant: PRESSURE_TO_SEAL
// pressure above pat press for exhalation valve to seal
extern const Real32 PRESSURE_TO_SEAL ;

//@ Constant: PAUSE_INTERVAL_EXTENSION
// pause extension in msec
extern const Int32 PAUSE_INTERVAL_EXTENSION ;

//@ Code...

// $[PA24013]
static const Uint32 PAV_PAUSE_TIME_MS = 300 ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PavPausePhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called with the phase type argument.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PavPausePhase::PavPausePhase(BreathPhaseType::PhaseType phaseType)
 : BreathPhase(phaseType) 	   	// $[TI1]
{
	CALL_TRACE("PavPausePhase::PavPausePhase( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PavPausePhase()
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PavPausePhase::~PavPausePhase(void)
{
	CALL_TRACE("PavPausePhase::~PavPausePhase(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl()
//
//@ Interface-Description
//      This method has a BreathTrigger& as an argument and has no return 
//      value.  This method is called at the end of the Pav pause
//      phase to wrap up the phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Inform PavManager that pause is complete.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PavPausePhase::relinquishControl( const BreathTrigger& trigger)
{
   	CALL_TRACE("relinquishControl(BreathTrigger& trigger)");

   	// $[TI1]
	RPavManager.setPauseComplete( trigger.getId()) ;
	RPavManager.setExhPostPavPause( trigger.getId());
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is
//      called every BD cycle during a PAV pause to control the exhalation
//		valve and PSOLS to perform a PAV pause.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The Psols are closed.  The exhalation valve is commanded to patient
//		pressure plus PRESSURE_TO_SEAL with a maximum of HCP.  Enable
//		RPauseCompletionTrigger one cycle before PAV_PAUSE_TIME_MS so that the
//		next BD cycle the pause will terminate.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PavPausePhase::newCycle( void)
{
    CALL_TRACE("newCycle(void)");

	updateFlowControllerFlags_() ;

	RDisconnectTrigger.setDesiredFlow(0.0);
    RAirFlowController.updatePsol( 0.0F) ;
   	RO2FlowController.updatePsol( 0.0F) ;

	Real32 highCircuitPressure = PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS).value ;
	
	Real32	targetPressure = MIN_VALUE( 
				PRESSURE_TO_SEAL + RExhPressureSensor.getFilteredValue(),
				highCircuitPressure) ;
				
   	RExhValveIController.updateExhalationValve( targetPressure, 0.0F) ;

   	if (elapsedTimeMs_ >= PAV_PAUSE_TIME_MS - CYCLE_TIME_MS)
	{
	   	// $[TI1]
   		RPauseCompletionTrigger.enable() ;
   	}  	// $[TI2]

	elapsedTimeMs_ += CYCLE_TIME_MS ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newBreath()
//
//@ Interface-Description
//      This method has no arguments and has no return value.  This method is 
//      called at the beginning of the PAV pause to initialize the
//      phase.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The ExhValveIController and Psols are initalized via newBreath() method
//		calls.  
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
PavPausePhase::newBreath(void)
{
    CALL_TRACE("newBreath(void)");

   	// $[TI1]

    RExhValveIController.newBreath() ;
    RAirFlowController.newBreath() ;
   	RO2FlowController.newBreath() ;
    
    RHighCircuitPressureExpTrigger.enable();
	elapsedTimeMs_ = 0 ;
	BreathRecord::SetFirstCycleOfPlateauFlag() ;

	// lengthen apnea interval by PAUSE_INTERVAL_EXTENSION to avoid apnea from
	// triggering when pause starts.  See DCS 5463
    RApneaInterval.extendInterval( PAV_PAUSE_TIME_MS + PAUSE_INTERVAL_EXTENSION) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PavPausePhase::SoftFault( const SoftFaultID  softFaultID,
                   				 const Uint32       lineNumber,
		   						 const char*        pFileName,
		  						 const char*        pPredicate)
{
  	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PAVPAUSEPHASE,
    	                     lineNumber, pFileName, pPredicate) ;
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFlowControllerFlags_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called to set the controller shutdown flag of the flow controller to
//		TRUE.  This method overloads the base class method.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Set flags to true.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
PavPausePhase::updateFlowControllerFlags_( void)
{
	CALL_TRACE("PavPausePhase::updateFlowControllerFlags_( void)") ;

   	// $[TI1]
	RO2FlowController.setControllerShutdown( TRUE) ;
	RAirFlowController.setControllerShutdown( TRUE) ;
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================







