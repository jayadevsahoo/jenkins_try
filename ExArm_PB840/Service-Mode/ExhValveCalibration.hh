#ifndef ExhValveCalibration_HH
#define ExhValveCalibration_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class:  ExhValveCalibration - Calibrate the exhalation valve
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/ExhValveCalibration.hhv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 004  By:  quf    Date: 09-Oct-1997    DR Number: DCS 2452
//       Project:  Sigma (840)
//       Description:
//			Cleaned up measureTestPoints_() and calibrateExhValve_(),
//			added several new private methods for use in measureTestPoints_().
//
//  Revision: 003  By:  quf    Date: 09-Oct-1997    DR Number: DCS 1865
//       Project:  Sigma (840)
//       Description:
//			Removed const EV_LOOPBACK_OFFSET from here and moved it
//			to SmConstants.hh.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date:  2-Nov-1994    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "ExhValveTable.hh"

//@ End-Usage


extern const Uint32 MIN_CALIB_PRESSURE;
extern const Uint32 MAX_CALIB_PRESSURE;
extern const Real32 INSP_FLOW_ERROR_AT_5LPM;
extern const Real32 EXH_FLOW_ERROR_AT_5LPM;
extern const Int32  STEP_INTERVAL;
extern const Int32  DWELL_TIME;
extern const Real32 EV_LOOPBACK_ACCURACY;
extern const Real32 MIN_EV_SLOPE;
extern const Real32 MAX_EV_SLOPE;
extern const Real32 MIN_EV_OFFSET;
extern const Real32 MAX_EV_OFFSET;
extern const Real32 GAIN_TEST_MIN;
extern const Int32 INITIAL_PRESSURE_TIMEOUT;
extern const Int32 NORMAL_PRESSURE_TIMEOUT;


//@ Constant: NUM_TEST_PRESSURES
// # of test pressures for pressure sensor cross-check
const Int32 NUM_TEST_PRESSURES = 5;

class ExhValveCalibration
{
    //@ Friend: ExhValvePerformanceTest
    friend class ExhValvePerformanceTest;

  public:
	// result IDs for flow sensor cross-check
	enum FlowSensorTestResult
	{
		FLOW_SENSORS_OK,
		FLOW_NOT_ESTABLISHED,
		FS_CROSS_CHECK_FAILURE
	};

	// result IDs for pressure sensor cross-check
	enum PressureSensorTestResult
	{
		PRESSURE_SENSORS_OK,
		PRESSURE_NOT_ESTABLISHED,
		PS_CROSS_CHECK_FAILURE,
		PS_CROSS_CHECK_ALERT
	};

	// result IDs for calibration measurement
	enum CalMeasurementResult
	{
		CAL_MEASUREMENTS_OK,
		EXH_FLOW_NOT_ESTABLISHED,
		TIMEOUT_REACHED,
		GAIN_OOR,
		TEST_POINT_OOR
	};

	struct ExhValveMeasurementPoints
	{
		Uint16	command ;
	  	Real32 pressure ;
	};



    ExhValveCalibration(void);
    ~ExhValveCalibration(void);

    static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);
  
	void runTest(void);

  protected:

  private:
    ExhValveCalibration(const ExhValveCalibration&); // not implemented...
    void   operator=(const ExhValveCalibration&);    // not implemented...

	void calibrateExhValve_( void);

	FlowSensorTestResult checkFlowSensors_ (void);
	PressureSensorTestResult checkPressureSensors_ (void);
	Boolean checkTimeout_( const Uint32 pressure, const Int32 timer);

	Boolean testValvePressurization_();

	Boolean doLowToHighCalibration_();
	Boolean doHighToLowCalibration_();

	Boolean findStableLowPressure_();
	Boolean collectCalPointsL2H_();

	Boolean InitHighToLow_();
	Boolean findStableHighPressure_();
	Boolean collectCalPointsH2L_();

	// For debugging only
	void printReadValues( Boolean printElapsedTime = FALSE , Uint32 elapsedTime = 0 );

	//@ Data-Member: testPressure_[]
	// Table of pressure sensor cross-check test points
	Real32 testPressure_[NUM_TEST_PRESSURES] ;


	static const Uint32 EXH_VALVE_COUNT_INCREMENTS = 1; 
	static const Real32 EXH_CAL_STEP;

	Real32 prevExhPressure_;
	Int32 currentDacCount;
	Int32 currentSampleTableIdx;
	Real32 currentTargetPressure;

	ExhValveTable::ExhValveCalTable exhValveTable_;
};


#endif // ExhValveCalibration_HH 
