// TestSessionSet.h : Declaration of the CTestSessionSet

#include "afxdb.h"
#pragma once

// code generated on Thursday, September 17, 2009, 3:51 PM

class CTestSessionSet : public CRecordset
{
public:
	CTestSessionSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CTestSessionSet)

// Field/Param Data

// The string types below (if present) reflect the actual data type of the
// database field - CStringA for ANSI datatypes and CStringW for Unicode
// datatypes. This is to prevent the ODBC driver from performing potentially
// unnecessary conversions.  If you wish, you may change these members to
// CString types and the ODBC driver will perform all necessary conversions.
// (Note: You must use an ODBC driver version that is version 3.5 or greater
// to support both Unicode and these conversions).

	LONGLONG	m_start_event_sequence_id;
	LONGLONG	m_stop_event_sequence_id;
	CStringA	m_session_start_time;
	CStringA	m_session_stop_time;
	CStringA	m_user_name;
	CStringA	m_hw_version;
	CStringA	m_test_session_id;

// Overrides
	// Wizard generated virtual function overrides
	public:
	virtual CString GetDefaultConnect();	// Default connection string

	virtual CString GetDefaultSQL(); 	// default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX support

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};


