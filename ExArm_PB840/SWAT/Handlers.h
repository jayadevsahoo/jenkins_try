////////////////Copyright (c) 2013 Covidien/////////////////////////////////////
/*******************************************************************************
Name:		Handlers.h

Purpose:
	Defines SWAT command handler classes.

REVISION HISTORY:

REVISION: 001	BY: SF		DATE: 16-AUG-2013
PROJECT: e600 vent
DR NUMBER: N / A
DESCRIPTION:
	Initialize version.

END OF REVISIONS

*******************************************************************************/

#pragma once

#include "CommandHandlerBase.h"

/*
	CoordinateHandler provide the parse logics of these commands which will pass
	a screen location [x, y] as command parameters.

	p1 - parsed as x
	p2 - parsed as y
		*/
class CoordinateHandler : public CommandHandlerBase
{
private:
	// Do not allow create a CoordinateHandler object directly
	CoordinateHandler();

	// allow these two classes access the constructor of this class.
	friend class SimulateTouchHandler;

protected:
	int x_;
	int y_;

	// parse the p1, p2 xml element
	virtual void ParseCommand(TiXmlElement* ele);
};

/*
	Handle 'push' action messages from SWAT client, the parser is derived from 
	CoordinateHandler.

	  Command:	<push_xy><p1></p1><p2></p2></push_xy>
		p1 - x
		p2 - y

	  Response:	<ack_nak_resp><error></error></ack_nak_resp>
		*/
class SimulateTouchHandler : public CoordinateHandler
{
protected:
	virtual void EexecuteCommand();
};


/*
	Handle 'press bezel key' action messages from SWAT client.

	  Command:	<bezel_key_press><on_off></on_off><key_id></key_id></bezel_key_press>
		on_off - set the bezel on or off
		key_id - the id of the bezel key to be pressed

	  Response:	<ack_nak_resp><error></error></ack_nak_resp>
		*/
class SimulateBezelKeyHandler : public CommandHandlerBase
{
public:
	SimulateBezelKeyHandler();

protected:
	int keyId_;
	bool onOff_;

	virtual void ParseCommand(TiXmlElement* ele);
	virtual void EexecuteCommand();
};


/*
	Handle 'knob' action messages from SWAT client.

	  Command:	<knob><p1></p1></knob>
		p1 - delta of the knob rotation

	  Response:	<ack_nak_resp><error></error></ack_nak_resp>
		*/
class SimulateKnobHandler : public CommandHandlerBase
{
public:
	SimulateKnobHandler();

protected:
	int delta_;

	virtual void ParseCommand(TiXmlElement* ele);
	virtual void EexecuteCommand();
};


/*
	Handle 'screen capture' action messages from SWAT client.

	  Command:	<screen_capture><file_name></file_name></screen_capture>
		file_name	- file name for the target to store the screen capture

	  Response:	
	  
	  <ack_nak_resp><error></error></ack_nak_resp>

		*/
class ScreenCaptureHandler : public CommandHandlerBase
{
public:
	ScreenCaptureHandler();

protected:
	char fileName_[ 260 ];

	virtual void ParseCommand(TiXmlElement* ele);
	virtual void EexecuteCommand();
};

/*
	Handle 'query screen capture queue size' action messages from SWAT client.

	  Command:	<screen_capture_buffer_info></screen_capture_buffer_info>

	  Response:	
	  
	  <ack_nak_resp><error></error></ack_nak_resp>
	    
		or   

	  <screen_capture_buffer_info_resp><buffer_size></buffer_size></screen_capture_buffer_info_resp>

		buffer_size	- screen capture count in the buffer

		*/
class ScreenCaptureBufferInfoHandler : public CommandHandlerBase
{
public:
	ScreenCaptureBufferInfoHandler();

protected:

	int     screenQueueSize_;

	virtual void ParseCommand(TiXmlElement* ele);
	virtual void EexecuteCommand();
	virtual void BuildResponse();
};

/*
	Handle 'clear screens' action messages from SWAT client.

	  Command:	<clear_screens></clear_screens>

	  Response:	
	  
	  <ack_nak_resp><error></error></ack_nak_resp>

		*/
class ClearScreensHandler : public CommandHandlerBase
{
protected:
	virtual void ParseCommand(TiXmlElement* ele);
	virtual void EexecuteCommand();
};

/*
	Handle 'query screen capture FTP address' action messages from SWAT client.

	  Command:	<query_sc_ftp_addr><p1></p1></query_sc_ftp_addr>
		p1	-	index of the screen capture

	  Response:	
	  
	  <ack_nak_resp><error></error></ack_nak_resp>
	    
		or   

	  <query_sc_ftp_addr_resp><index></index><ftp_addr></ftp_addr></query_sc_ftp_addr_resp>

		index		- index of the screen capture from client, zeror based
		ftp_addr	- ftp download URL for the screen capture indicated by 'index'

		*/
class QueryScreenshotFileNameHandler : public CommandHandlerBase
{
public:
	QueryScreenshotFileNameHandler();

protected:
	int     index_;

	char    ScreenAddr_[260];

	virtual void ParseCommand(TiXmlElement* ele);
	virtual void EexecuteCommand();
	virtual void BuildResponse();
};

