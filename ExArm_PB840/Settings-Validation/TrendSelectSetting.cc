#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//============== C L A S S   D E S C R I P T I O N ====================
//@ Class:  TrendSelectSetting - Trend Data Selection Setting
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a non-batch, discrete setting that selects which trend 
//  data to display on the trending graph or table.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no data members, and no dependent settings.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/TrendSelectSetting.ccv   25.0.4.0   19 Nov 2013 14:27:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002   By: rpr    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation and Work of Breathing
// 
//  Revision: 001   By: gdc  Date:  10-Jan-2007   SCR Number:  6237
//  Project:  Trend
//  Description:
//      Initial version.
//=====================================================================

#include "TrendSelectSetting.hh"
#include "TrendSelectValue.hh"

//@ Usage-Classes
#include "ContextMgr.hh"
#include "AcceptedContext.hh"
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
#include "Sound.hh"

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  TrendSelectSetting()  [Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendSelectSetting::TrendSelectSetting(SettingId::SettingIdType settingId)
: NonBatchDiscreteSetting(settingId,
						  Setting::NULL_DEPENDENT_ARRAY_,
						  TrendSelectValue::TOTAL_TREND_SELECT_VALUES)
{
	CALL_TRACE("TrendSelectSetting()");
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method:  ~TrendSelectSetting()  [Virtual Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TrendSelectSetting::~TrendSelectSetting(void)
{
	CALL_TRACE("~TrendSelectSetting(void)");
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  Calculate, and initialize to, the new discrete value.  The value of
//  'knobDelta' is only used in so far as the "direction" of change; the
//  value, itself, is ignored a delta of '1' is applied in the direction
//  of change.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  (getApplicability() == Applicability::CHANGEABLE)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus& TrendSelectSetting::calcNewValue(const Int16 knobDelta)
{
	CALL_TRACE("calcNewValue(knobDelta)");

	DiscreteValue  newValue = getAdjustedValue();

	AUX_CLASS_PRE_CONDITION ( (getApplicability(Notification::ACCEPTED) 
							   == Applicability::CHANGEABLE), getId());

	BoundStatus&  rBoundStatus = getBoundStatus_();
	rBoundStatus.reset();

	if (knobDelta < 0)
	{

		if (newValue == 0)
		{
			//Sound::Start(Sound::REMIND);
		}
		else
		{
			newValue--;
		}
	}
	else if (knobDelta > 0)
	{
		if (newValue == TOTAL_NUM_VALUES_-1)
		{
			//Sound::Start(Sound::REMIND);
		}
		else
		{
			newValue++;
		}
	}
	setAdjustedValue(newValue);

	return(rBoundStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting is ALWAYS changeable.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id TrendSelectSetting::getApplicability(const Notification::ChangeQualifier) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	return(Applicability::CHANGEABLE);
}											

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  isEnabledValue()  [const, virtual]
//
//@ Interface-Description
//  Is this setting's value given by 'value', a currently-enabled value?
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TR01114] -- Trend parameters associated with uninstalled Ventilator
//                options shall be displayed dimmed...
//---------------------------------------------------------------------
//@ PreCondition
//  none 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Boolean TrendSelectSetting::isEnabledValue(const DiscreteValue value) const
{
	Boolean isEnabled = TRUE;

	switch (TrendSelectValue::TrendSelectValueId(value))
	{
	case TrendSelectValue::TREND_DYNAMIC_COMPLIANCE:
	case TrendSelectValue::TREND_DYNAMIC_RESISTANCE:
	case TrendSelectValue::TREND_VITAL_CAPACITY:
	case TrendSelectValue::TREND_P100:
	case TrendSelectValue::TREND_END_EXPIRATORY_FLOW:
	case TrendSelectValue::TREND_PEAK_EXPIRATORY_FLOW_RATE:
	case TrendSelectValue::TREND_PEAK_SPONTANEOUS_FLOW_RATE:
	case TrendSelectValue::TREND_NEGATIVE_INSPIRATORY_FORCE:
		{
			isEnabled = SoftwareOptions::IsOptionEnabled(SoftwareOptions::RESP_MECH);
		}
		break;
	case TrendSelectValue::TREND_PAV_COMPLIANCE:
	case TrendSelectValue::TREND_PAV_ELASTANCE:
	case TrendSelectValue::TREND_PAV_INTRINSIC_PEEP:
	case TrendSelectValue::TREND_PAV_RESISTANCE:
	case TrendSelectValue::TREND_PAV_TOTAL_AIRWAY_RESISTANCE:
	case TrendSelectValue::TREND_TOTAL_WORK_OF_BREATHING:
		{
			isEnabled = SoftwareOptions::IsOptionEnabled(SoftwareOptions::PAV);
		}
		break;
	case TrendSelectValue::TREND_SET_PEEP_HIGH:
	case TrendSelectValue::TREND_SET_PEEP_LOW:
	case TrendSelectValue::TREND_SET_PEEP_HIGH_TIME:
	case TrendSelectValue::TREND_SET_PEEP_LOW_TIME:
	case TrendSelectValue::TREND_SET_TH_TL_RATIO:
		{
			isEnabled = SoftwareOptions::IsOptionEnabled(SoftwareOptions::BILEVEL);
		}
		break;
	case TrendSelectValue::TREND_SET_PERCENT_SUPPORT:
		{
			isEnabled = SoftwareOptions::IsOptionEnabled(SoftwareOptions::PAV) ||
						SoftwareOptions::IsOptionEnabled(SoftwareOptions::ATC);
		}
		break;
	case TrendSelectValue::TREND_SET_VOLUME_SUPPORT_LEVEL_VS:
		{
			isEnabled = SoftwareOptions::IsOptionEnabled(SoftwareOptions::VTPC);
		}
		break;
	case TrendSelectValue::TREND_PERCENT_LEAK:
	case TrendSelectValue::TREND_LEAK:
	case TrendSelectValue::TREND_ILEAK:
		{
			isEnabled = SoftwareOptions::IsOptionEnabled(SoftwareOptions::LEAK_COMP);
		}
		break;

	default:                     
		{
			isEnabled = TRUE;
		}
		break;
	}

	return isEnabled;
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getDefaultValue()  [const, virtual]
//
//@ Interface-Description
//  Get the setting's default value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[TR01113] -- The first entry of the scroll list shall be "- -" ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue TrendSelectSetting::getDefaultValue(void) const
{
	CALL_TRACE("getDefaultValue()");

	return(DiscreteValue(TrendSelectValue::TREND_EMPTY));
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//          [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void TrendSelectSetting::SoftFault(const SoftFaultID  softFaultID,
								   const Uint32       lineNumber,
								   const char*        pFileName,
								   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, ::SETTINGS_VALIDATION,
							TREND_SELECT_SETTING, lineNumber, pFileName,
							pPredicate);
}
