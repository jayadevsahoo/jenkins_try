#ifndef FlowSensorCalibration_HH
#define FlowSensorCalibration_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: FlowSensorCalibration - performs the flow sensor calibration.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/FlowSensorCalibration.hhv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 002  By:  quf    Date:  09-Oct-97    DR Number: 2385
//       Project:  Sigma (840)
//       Description:
//             Added code to set the novram calibration status flag
//             to the proper state at the proper times.
//
//  Revision: 001  By:  syw    Date:  03-Jul-97    DR Number: none
//       Project:  Sigma (840)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes
//@ End-Usage

#include "FlowSensorOffset.hh"

extern const Int32 FS_CAL_MINIMUM_MAX_FLOW;


class FlowSensorCalibration {
  public:

    FlowSensorCalibration( void) ;
    ~FlowSensorCalibration( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
						   const Uint32 lineNumber,
						   const char *pFileName  = NULL, 
						   const char *pPredicate = NULL) ;
	void runTest( void) ;
	void initialize( void) ;

  protected:

  private:
  
    FlowSensorCalibration( const FlowSensorCalibration&) ;  // not implemented
    void  operator=( const FlowSensorCalibration&) ;		// not implemented 

	void performTest_( void) ;   
	Uint32 calibrate_( GasType gas) ;
	Uint32 calibrateOneFlow_(GasType gas, Real32 desiredFlow);
	Uint32 boundMaxFlow_( Int32& maxFlow);
	Real32 obtainInspExhOffset_( Uint32 desiredFlow, GasType desiredGas, Real32 &accumPressure) ;
	Real32 obtainDesiredFlowOffset_( Uint32 desiredFlow, GasType desiredGas) ;
	Uint32 storeOffset_( Uint32 flow, Real32 inspExhOffset,
									Real32 desiredFlowOffset);
	void setZeroOffsets_( Int32 maxFlow);

	//@ Data-Member: maxInspOffset_[MAX_FLOW_CALIBRATION]
	// max allowable insp flow sensor error
	Real32 maxInspOffset_[MAX_FLOW_CALIBRATION] ;

	//@ Data-Member: maxExhOffset_[MAX_FLOW_CALIBRATION]
	// max allowable exh flow sensor error
	Real32 maxExhOffset_[MAX_FLOW_CALIBRATION] ;

	//@ Data-Member: pFlowSensorOffset_
	// pointer to flow sensor offset object
	FlowSensorOffset* pFlowSensorOffset_;

	//@ Data-Member: filteredAirFlow_
	// alpha-filtered air flow
	Real32 filteredAirFlow_;

	//@ Data-Member: filteredO2Flow_
	// alpha-filtered O2 flow
	Real32 filteredO2Flow_;

} ;


#endif // FlowSensorCalibration_HH 
