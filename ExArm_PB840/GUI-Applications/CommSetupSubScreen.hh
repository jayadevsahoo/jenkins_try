#ifndef CommSetupSubScreen_HH
#define CommSetupSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: CommSetupSubScreen - Activated by selecting CommSetup
// in the Service Lower Other Screen after user press/release the
// Lower Other screen tab button.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/CommSetupSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:38   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 005   By: mnr    Date: 03-Mar-2010    SCR Number: 6556
//  Project: NEO
//  Description:
//      Now overriding buttonDownHappenned()_.
// 
//  Revision: 004   By: quf    Date:  13-Sep-2001    DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Print implementation changes:
//	- Removed printConfigureButton_ and onComPortSelect_.
//
//  Revision: 003   By: hct        Date: 06-DEC-1999     DR Number: 5493
//  Project:  GUIComms
//  Description:
//	Incorporated initial specifications for GUIComms Project.
//
//  Revision: 002  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 001  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BatchSettingsSubScreen.hh"
#include "DiscreteSettingButton.hh"
#include "SubScreenTitleArea.hh"
//@ End-Usage

class CommSetupSubScreen : public BatchSettingsSubScreen
{
public:
	CommSetupSubScreen(SubScreenArea *pSubScreenArea);
	~CommSetupSubScreen(void);

	// Redefine SubScreen methods
	virtual void acceptHappened(void);

	// GuiTimerTarget virtual method
	virtual void timerEventHappened(GuiTimerId::GuiTimerIdType timerId);

	virtual void buttonDownHappened(Button *pButton,
									Boolean isByOperatorAction);

	virtual void buttonUpHappened(Button *pButton,
									Boolean isByOperatorAction);

	static void SoftFault(const SoftFaultID softFaultID,
				  		  const Uint32      lineNumber,
				  		  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									  );

private:
	// these methods are purposely declared, but not implemented...
	CommSetupSubScreen(void);							// not implemented..
	CommSetupSubScreen(const CommSetupSubScreen&);	// not implemented..
	void operator=(const CommSetupSubScreen&);			// not implemented..

	// Constant: MAX_SETTING_BUTTONS_ 
	// Maximum number of buttons that can be displayed in this subscreen.
	// Must be modified when new buttons are added.
	enum { MAX_SETTING_BUTTONS_ = 13 };

	//@ Data-Member: dciBaudRateButton_;
	// A discrete button for adjusting the DCI Baud Rate. 
	DiscreteSettingButton dciBaudRateButton_;

	//@ Data-Member: dciDataBitButton_;
	// A discrete button for adjusting the DCI Data Bit.
	DiscreteSettingButton dciDataBitButton_;

	//@ Data-Member: dciParityModeButton_;
	// A discrete button for adjusting the DCI Parity Mode.
	DiscreteSettingButton dciParityModeButton_;

	//@ Data-Member: com2BaudRateButton_;
	// A discrete button for adjusting the DCI Baud Rate. 
	DiscreteSettingButton com2BaudRateButton_;

	//@ Data-Member: com2DataBitButton_;
	// A discrete button for adjusting the DCI Data Bit.
	DiscreteSettingButton com2DataBitButton_;

	//@ Data-Member: com2ParityModeButton_;
	// A discrete button for adjusting the DCI Parity Mode.
	DiscreteSettingButton com2ParityModeButton_;

	//@ Data-Member: com3BaudRateButton_;
	// A discrete button for adjusting the DCI Baud Rate. 
	DiscreteSettingButton com3BaudRateButton_;

	//@ Data-Member: com3DataBitButton_;
	// A discrete button for adjusting the DCI Data Bit.
	DiscreteSettingButton com3DataBitButton_;

	//@ Data-Member: com3ParityModeButton_;
	// A discrete button for adjusting the DCI Parity Mode.
	DiscreteSettingButton com3ParityModeButton_;

	//@ Data-Member: com1ConfigureButton_;
	// A discrete button for adjusting the COM1 configuration.
	DiscreteSettingButton com1ConfigureButton_;

	//@ Data-Member: com2ConfigureButton_;
	// A discrete button for adjusting the COM2 configuration.
	DiscreteSettingButton com2ConfigureButton_;

	//@ Data-Member: com3ConfigureButton_;
	// A discrete button for adjusting the COM3 configuration.
	DiscreteSettingButton com3ConfigureButton_;

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;
	
	//@ Data-Member:  arrSettingButtonPtrs_
	// The subscreen buttons.
	SettingButton*  arrSettingButtonPtrs_[MAX_SETTING_BUTTONS_ + 1];
};

#endif // CommSetupSubScreen_HH 
