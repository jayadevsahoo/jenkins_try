
#ifndef SigmaConstants_HH
#define SigmaConstants_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Filename:  SigmaConstants - System-Wide Constants.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/SigmaConstants.hhv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modifications
//
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from include line.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SigmaTypes.hh"

//@ Usage-Class
//@ End-Usage


//=====================================================================
//
//  Min/Max values of the standard types...
//
//=====================================================================

//@ Constant:  MAX_UINT8_VALUE
// Maximum unsigned, 8-bit integer value.
extern const Uint8   MAX_UINT8_VALUE;

//@ Constant:  MAX_UINT16_VALUE
// Maximum unsigned, 16-bit integer value.
extern const Uint16  MAX_UINT16_VALUE;

//@ Constant:  MAX_UINT32_VALUE
// Maximum unsigned, 32-bit integer value.
extern const Uint32  MAX_UINT32_VALUE;

//@ Constant:  MIN_INT8_VALUE
// Minimum signed, 8-bit integer value.
extern const Int8    MIN_INT8_VALUE;

//@ Constant:  MAX_INT8_VALUE
// Maximum signed, 8-bit integer value.
extern const Int8    MAX_INT8_VALUE;

//@ Constant:  MIN_INT16_VALUE
// Minimum signed, 16-bit integer value.
extern const Int16   MIN_INT16_VALUE;

//@ Constant:  MAX_INT16_VALUE
// Maximum signed, 16-bit integer value.
extern const Int16   MAX_INT16_VALUE;

//@ Constant:  MIN_INT32_VALUE
// Minimum signed, 32-bit integer value.
extern const Int32   MIN_INT32_VALUE;

//@ Constant:  MAX_INT32_VALUE
// Maximum signed, 32-bit integer value.
extern const Int32   MAX_INT32_VALUE;

//@ Constant:  MAX_REAL32_VALUE
// Maximum 32-bit floating-point value.
extern const Real32  MAX_REAL32_VALUE;


#endif  // SigmaConstants_HH
