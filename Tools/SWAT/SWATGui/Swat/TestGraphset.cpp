// TestGraphset.h : Implementation of the CTestGraphset class



// CTestGraphset implementation

// code generated on Monday, December 14, 2009, 3:26 PM

#include "stdafx.h"
#include "TestGraphset.h"
IMPLEMENT_DYNAMIC(CTestGraphset, CRecordset)

CTestGraphset::CTestGraphset(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_time;
	m_value = 0.0;
	m_nFields = 2;
	m_nDefaultType = dynaset;
}

CString CTestGraphset::GetDefaultConnect()
{
	return _T("DSN=PostgreSQL30;DATABASE=viking_integration_test;SERVER=127.0.0.1;PORT=5432;UID=postgres;PWD=its4me@1234;SSLmode=disable;ReadOnly=0;Protocol=7.4;FakeOidIndex=0;ShowOidColumn=0;RowVersioning=0;ShowSystemTables=0;ConnSettings=;Fetch=100;Socket=4096;UnknownSizes=0;MaxVarcharSize=255;MaxLongVarcharSize=8190;Debug=0;CommLog=0;Optimizer=0;Ksqo=1;UseDeclareFetch=0;TextAsLongVarchar=1;UnknownsAsLongVarchar=0;BoolsAsChar=1;Parse=0;CancelAsFreeStmt=0;ExtraSysTablePrefixes=dd_;;LFConversion=1;UpdatableCursors=1;DisallowPremature=0;TrueIsMinus1=0;BI=0;ByteaAsLongVarBinary=0;UseServerSidePrepare=0;LowerCaseIdentifier=0;XaOpt=1");
}

CString CTestGraphset::GetDefaultSQL()
{
	return _T("[TestResults].[test_graph]");
}

void CTestGraphset::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// Macros such as RFX_Text() and RFX_Int() are dependent on the
// type of the member variable, not the type of the field in the database.
// ODBC will try to automatically convert the column value to the requested type
	RFX_Date(pFX, _T("[time]"), m_time);
	RFX_Single(pFX, _T("[value]"), m_value);

}
/////////////////////////////////////////////////////////////////////////////
// CTestGraphset diagnostics

#ifdef _DEBUG
void CTestGraphset::AssertValid() const
{
	CRecordset::AssertValid();
}

void CTestGraphset::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG


