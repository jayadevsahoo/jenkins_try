#ifndef AlarmQueueMsg_HH
#define AlarmQueueMsg_HH
 
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================
 
#include "TaskControlQueueMsg.hh"
#include "OperatingParamEventName.hh"

//=====================================================================
// File: AlarmQueueMsg - Data structure definition for UserAnnuncation
// VRTX queue messages.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmQueueMsg.hhv   25.0.4.0   19 Nov 2013 13:51:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  hct    Date:  22-MAR-94    DR Number: 795
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//
//  Revision: 002  By:  hct    Date:  23-AUG-96    DR Number: 10041
//       Project:  Sigma (R8027)
//       Description:
//             Created new AQMEventType for Alarm Broadcasting.  Formerly,
//             the ALARM_BROADCAST_EN event was handled like a regular 
//             Alarm event and unnecessarily caused an AlarmUpdateBuffer to be 
//             processed by the GUI-Apps subsystem.  This change enhances GUI
//             throughput.
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//=====================================================================
#include "InterTaskMessage.hh"
#include "TaskMonitorQueueMsg.hh"

//@ Type: AlarmQueueMsg
union AlarmQueueMsg
{
    //@ Type: AQMEventType
    // This enum identifies the event type
    enum AQMEventType
    {
        AQM_NULL_EVENT = -1,
        OPERATING_PARAMETER_EVENT,        
        TASK_MONITOR_EVENT    = TaskMonitorQueueMsg::TASK_MONITOR_MSG,
        VIOLATION_HISTORY_EVENT = FIRST_APPLICATION_MSG,
        ALARM_BROADCAST_EVENT,
        AQM_MAX_EVENT
    };
 
    //  struct for all event types
    struct
    {
        AQMEventType                  eventType          : 8;
        Int32                         eventData          : 24;
    } event;

    // Use the following member to simply pass the 32 bits of
    // AlarmQueueMsg data to message queues.
    Uint32 qWord;

};

#endif // AlarmQueueMsg_HH
