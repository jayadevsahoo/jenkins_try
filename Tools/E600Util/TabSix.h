#pragma once

#include "TabOne.h"
#include "BdTxPacket.h"

enum SettingIdType_t
{
	NULL_SETTING_ID      = -1,		  // undefined setting id...
	LOW_SETTING_ID_VALUE =  0,		  // lowest setting value...

	//==================================================================
	// Batch Setting Ids...
	//==================================================================

	LOW_BATCH_ID_VALUE = LOW_SETTING_ID_VALUE,	// must start at 0!!

	// Batch Settings that are sent to Breath-Delivery...
	LOW_BATCH_BD_ID_VALUE = LOW_BATCH_ID_VALUE,
	LOW_BATCH_BD_BOUNDED_ID_VALUE = LOW_BATCH_BD_ID_VALUE, 

	APNEA_FLOW_ACCEL_PERCENT = LOW_BATCH_BD_BOUNDED_ID_VALUE,
	APNEA_INSP_PRESS,	  // Apnea Insp. Press.
	APNEA_INSP_TIME,	  // Apnea Inspiratory Time...
	APNEA_INTERVAL,		  // Apnea Interval Time...
	APNEA_MIN_INSP_FLOW,  // Apnea Minimum Inspiratory Flow...
	APNEA_O2_PERCENT,	  // Apnea Oxygen Percentage...
	APNEA_PEAK_INSP_FLOW, // Apnea Peak Inspiratory Flow...
	APNEA_PLATEAU_TIME,	  // Apnea Plateau Time...
	APNEA_RESP_RATE,	  // Apnea Respiratory Rate...
	APNEA_TIDAL_VOLUME,	  // Apnea Tidal Volume...
	DISCO_SENS,		  // Disconnection Sensitivity...
	EXP_SENS,		  // Expiratory Sensitivity...
	FLOW_ACCEL_PERCENT,	  // Flow Acceleration Percent...
	FLOW_SENS,		  // Flow Sensitivity...
	HIGH_CCT_PRESS,		  // High Circuit Pressure...
	HIGH_INSP_TIDAL_VOL,  // High Inspired Tidal Volume (ATC/PA/VS/VC+)...
	HUMID_VOLUME,	  // Humidifier Volume (ATC)...
	IBW,		  // Ideal Body Weight...
	INSP_PRESS,		  // Inspiratory Pressure...
	INSP_TIME,		  // Inspiratory Time...
	MIN_INSP_FLOW,		  // Minimum Inspiratory Flow...
	OXYGEN_PERCENT,		  // Oxygen Percentage...
	PEAK_INSP_FLOW,		  // Peak Inspiratory Flow...
	PERCENT_SUPPORT,	  // Percent (Work) Support (ATC)...
	PLATEAU_TIME,	  // Plateau Time...
	PEEP,			  // Positive End-Expiratory Press...
	PEEP_HIGH,		  // High PEEP (BiLevel)...
	PEEP_HIGH_TIME,		  // High PEEP time (BiLevel)...
	PEEP_LOW,		  // Low PEEP (BiLevel)...
	PRESS_SENS,		  // Pressure Sensitivity...
	PRESS_SUPP_LEVEL,	  // Pressure Support Level...
	RESP_RATE,		  // Respiratory Rate...
	TIDAL_VOLUME,	  // Tidal Volume...
	TUBE_ID,		  // Tube Id (ATC)...
	VOLUME_SUPPORT,		  // Volume Support (VTPC)...
	HIGH_SPONT_INSP_TIME, // High spontaneous inspiratory time (NIV)...
	HIGH_BATCH_BD_BOUNDED_ID_VALUE = HIGH_SPONT_INSP_TIME,
	LOW_BATCH_BD_DISCRETE_ID_VALUE, 

	APNEA_FLOW_PATTERN = LOW_BATCH_BD_DISCRETE_ID_VALUE,// Apnea Flow Pattern
	APNEA_MAND_TYPE,	  // Apnea Mandatory Type...
	FIO2_ENABLED,	  // FIO2 Option Enabled...
	FLOW_PATTERN,	  // Flow Pattern...
	HUMID_TYPE,		  // Humidification Type...
	NOMINAL_VOLT,	  // Nominal Line Voltage...

	// Maintain the order of the main vent setup settings that follow
	// as they are required to be in order of dependency for proper 
	// processing by their valueUpdate callbacks.
	// BEGIN -- main vent setup settings
	PATIENT_CCT_TYPE,		// Patient Circuit Type...
	VENT_TYPE,				// Vent Type (NIV)... 
	MODE,					// Mode...
	MAND_TYPE,				// Mandatory Type..
	SUPPORT_TYPE,			// Support Type...
	TRIGGER_TYPE,			// Trigger Type...
	// END -- main vent setup settings

	TUBE_TYPE,		  		// Tube Type (ATC)...
    PROX_ENABLED,           // PROX enabled...
	LEAK_COMP_ENABLED,		// Leak Compensation enabled...

	HIGH_BATCH_BD_DISCRETE_ID_VALUE = LEAK_COMP_ENABLED,
	HIGH_BATCH_BD_ID_VALUE          = HIGH_BATCH_BD_DISCRETE_ID_VALUE,

	// Batch Settings that are NOT sent to Breath-Delivery...
	LOW_BATCH_NONBD_ID_VALUE,
	LOW_BATCH_NONBD_BOUNDED_ID_VALUE = LOW_BATCH_NONBD_ID_VALUE,

	APNEA_EXP_TIME = LOW_BATCH_NONBD_BOUNDED_ID_VALUE,// Apnea Exp. Time...
	APNEA_IE_RATIO,		  // Apnea Inspiratory-to-Expiratory Time Ratio
	ATM_PRESSURE,	  // Atmospheric Pressure...
	EXP_TIME,		  // Expiratory Time...
	HIGH_EXH_MINUTE_VOL,  // High Exhaled Minute Volume...
	HIGH_EXH_TIDAL_VOL,	  // High Exhaled Tidal Volume...
	HIGH_RESP_RATE,		  // High Respiratory Rate...
	HL_RATIO,		  // Ratio of Th to Tl (BiLevel)...
	IE_RATIO,		  // Inspiratory-to-Expiratory Time Ratio...
	LOW_CCT_PRESS,		  // Low Circuit Pressure...
	LOW_EXH_MAND_TIDAL_VOL,	  // Low Exhale Mandatory Tidal Volume...
	LOW_EXH_MINUTE_VOL,	  // Low Exhale Minute Volume...
	LOW_EXH_SPONT_TIDAL_VOL,  // Low Exhale Spontaneous Tidal Volume...
	MINUTE_VOLUME,		  // Minute Volume...
	PEEP_LOW_TIME,		  // Low PEEP time (BiLevel)...
	VSUPP_IBW_RATIO,	  // Vsupp-to-IBW Ratio...
	VT_IBW_RATIO,	  // Vt-to-IBW Ratio...

	HIGH_BATCH_NONBD_BOUNDED_ID_VALUE = VT_IBW_RATIO,
	LOW_BATCH_NONBD_DISCRETE_ID_VALUE,

	APNEA_CONSTANT_PARM = LOW_BATCH_NONBD_DISCRETE_ID_VALUE,// Apnea Constant
	CONSTANT_PARM,			  // Constant-During-Rate-Change...
	DCI_BAUD_RATE,			  // DCI's Baud Rate...
	DCI_DATA_BITS,			  // DCI's Data Bits...
	DCI_PARITY_MODE,		  // DCI's Parity Mode...
	COM1_BAUD_RATE=DCI_BAUD_RATE, // COM1's Baud Rate...
	COM1_DATA_BITS,			  // COM1's Data Bits...
	COM1_PARITY_MODE,		  // COM1's Parity Mode...
	MONTH,				  // Month of the Year...
	PRESS_UNITS,		  // Units to use for pressures...
	LANGUAGE,				  // Language...
	SERVICE_BAUD_RATE,		  // Service Mode's Baud Rate...
	COM2_BAUD_RATE,			  // COM2's Baud Rate...
	COM2_DATA_BITS,			  // COM2's Data Bits...
	COM2_PARITY_MODE,		  // COM2's Parity Mode...
	COM3_BAUD_RATE,			  // COM3's Baud Rate...
	COM3_DATA_BITS,			  // COM3's Data Bits...
	COM3_PARITY_MODE,		  // COM3's Parity Mode...
	COM1_CONFIG,			  // COM1's configuration
	COM2_CONFIG,			  // COM2's configuration
	COM3_CONFIG,			  // COM3's configuration
	DATE_FORMAT,			  // Date format


	HIGH_BATCH_NONBD_DISCRETE_ID_VALUE = DATE_FORMAT,
	LOW_BATCH_NONBD_SEQUENTIAL_ID_VALUE,

	DAY = LOW_BATCH_NONBD_SEQUENTIAL_ID_VALUE,// Day of the Week...
	DISPLAY_CONTRAST_DELTA,		  // Display Contrast Delta...
	HOUR,			  // Hour of the Day...
	MINUTE,				  // Minute of the Hour...
	YEAR,			  // Year...

	HIGH_BATCH_NONBD_SEQUENTIAL_ID_VALUE = YEAR,

	HIGH_BATCH_NONBD_ID_VALUE = HIGH_BATCH_NONBD_SEQUENTIAL_ID_VALUE,
	HIGH_BATCH_ID_VALUE       = HIGH_BATCH_NONBD_ID_VALUE,

	NUM_BATCH_IDS = (HIGH_BATCH_ID_VALUE - LOW_BATCH_ID_VALUE + 1),

	//==================================================================
	// Non-Batch Setting Ids...
	//==================================================================

	LOW_NON_BATCH_ID_VALUE = HIGH_BATCH_ID_VALUE + 1,
	LOW_NON_BATCH_BOUNDED_ID_VALUE = LOW_NON_BATCH_ID_VALUE,

	PRESS_VOL_LOOP_BASE = LOW_NON_BATCH_BOUNDED_ID_VALUE,//Baseline for P-V Loop Plot...
	TREND_CURSOR_POSITION,								 //Trend cursor position
	HIGH_NON_BATCH_BOUNDED_ID_VALUE = TREND_CURSOR_POSITION,
	LOW_NON_BATCH_DISCRETE_ID_VALUE,

	FLOW_PLOT_SCALE = LOW_NON_BATCH_DISCRETE_ID_VALUE,// Flow Axis Scale...
	PLOT1_TYPE,			  // Type of Waveform Plot #1...
	PLOT2_TYPE,			  // Type of Waveform Plot #2...
	TREND_SELECT_1,			   // Trend display 1
	TREND_SELECT_2,			   // Trend display 2
	TREND_SELECT_3,			   // Trend display 3
	TREND_SELECT_4,			   // Trend display 4
	TREND_SELECT_5,			   // Trend display 5
	TREND_SELECT_6,			   // Trend display 6
	TREND_TIME_SCALE,		   // Trend time scale to display
	TREND_PRESET,			   // Trend preset                   
	SHADOW_TRACE_ENABLE,	  // Enable/Disable Shadow Trace...
	PRESSURE_PLOT_SCALE,	  // Scale of the Pressure Axis...
	TIME_PLOT_SCALE,		  // Scale of the Time Axis...
	VOLUME_PLOT_SCALE,		  // Scale of the Volume Axis...

	HIGH_NON_BATCH_DISCRETE_ID_VALUE  = VOLUME_PLOT_SCALE,
	LOW_NON_BATCH_SEQUENTIAL_ID_VALUE,

	ALARM_VOLUME = LOW_NON_BATCH_SEQUENTIAL_ID_VALUE,// Alarm Volume...
	DISPLAY_BRIGHTNESS,		  // Display Brightness...
	DISPLAY_CONTRAST_SCALE,		  // Display Contrast Scale...

	HIGH_NON_BATCH_SEQUENTIAL_ID_VALUE = DISPLAY_CONTRAST_SCALE,
	HIGH_NON_BATCH_ID_VALUE = HIGH_NON_BATCH_SEQUENTIAL_ID_VALUE,

	NUM_NON_BATCH_IDS = (HIGH_NON_BATCH_ID_VALUE -
						 LOW_NON_BATCH_ID_VALUE + 1),

	HIGH_SETTING_ID_VALUE = HIGH_NON_BATCH_ID_VALUE,

	TOTAL_SETTING_IDS = (HIGH_SETTING_ID_VALUE  + 1)
};

enum PacketSettingId_t
{
	FIRST_SETTING_ID,
	MAND_TYPE_ID=FIRST_SETTING_ID,
	MODE_ID,
	VENT_TYPE_ID,
	TRIGGER_TYPE_ID,
	FLOW_PATTERN_ID,
	SUPPORT_TYPE_ID,
	LAST_DISCRETE_SETTING_ID=SUPPORT_TYPE_ID,
	NUM_DISCRETE_SETTING_IDS,
	FIRST_BOUNDED_SETTING_ID=NUM_DISCRETE_SETTING_IDS,
	LAST_BOUNDED_SETTING_ID= 20,
	// Calculated settings start
	MIN_INSP_FLOW_ID,
	// Calculated settings end
	NUM_TEST_SETTING_IDS,
	NUM_BOUNDED_SETTING_IDS=LAST_BOUNDED_SETTING_ID-FIRST_BOUNDED_SETTING_ID+1
};


enum EditCtrl_Control_ID{
	RESP_RATE_ID = 0,
	TIDAL_VOLUME_ID, 
	PEAK_INSP_FLOW_ID,
	PLATEAU_TIME_ID,
	FLOW_SENS_ID,
	PRESS_SENS_ID,
	OXYGEN_PERCENT_ID,
	PEEP_ID,
	INSP_PRESS_ID, 
	INSP_TIME_ID,
	FLOW_ACCEL_PERCENT_ID,
	PRESS_SUPP_LEVEL_ID,
	EXP_SENS_ID,
	IBW_ID,
	HIGH_CCT_PRESS_ID
};

struct SettingData_t
{
	UINT16	settingId;
	INT8   reserved;
	INT8   boundedPrec;

	union   // anonymous union...
	{
		float  boundedValue;
		INT   discreteValue;
	};
};

struct SettingsPacket_t
{
	UINT16			MsgId;
	UINT16			NumOfSettings;
	SettingData_t	Data[NUM_TEST_SETTING_IDS];
};

struct SettingsRequestPacket_t
{
	UINT16			MsgId;
	UINT16			NumOfSettings;
};

struct SettingsReplyPacket_t
{
	UINT16			MsgId;
	UINT16			NumOfSettings;
	SettingData_t	Data[NUM_TEST_SETTING_IDS];
	UINT			Checksum;
};


////////////////////////////////////////
enum SettingDebugIdType_t
{
	LOW_SETTING_DEBUG_ID,
	COMPLIANCE_ID = LOW_SETTING_DEBUG_ID,
	NUM_BOUNDED_SETTING_DEBUG_IDS,
	NUM_SETTING_DEBUG_IDS=NUM_BOUNDED_SETTING_DEBUG_IDS
};

struct SettingsDebugPacket_t
{
	UINT16			MsgId;
	UINT16			NumOfSettings;
	SettingData_t	Data[NUM_SETTING_DEBUG_IDS];
};
/////////////////////////////////////////////////////////////////
// TabSix dialog

class CTabSix : public CDialog
{
	DECLARE_DYNAMIC(CTabSix)

public:
	CTabSix(CWnd* pParent);   // standard constructor
	virtual ~CTabSix();

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB6 };

	CTabOne *pTabOne;
	CStatusBar *pStatusBar;

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	void SendCompleteSettingPacket(SettingData_t Data[]);
	void SendUpdateSettingPacket(SettingData_t Data[]);
	void ReadSettings();
	void GetEditControlPointers();
	void SendPacketToBD(char *pBuff, int Size);

	CComboBox ComboBoxInt[NUM_DISCRETE_SETTING_IDS];
	SettingData_t SettingData[NUM_TEST_SETTING_IDS];
	SettingData_t SettingDataNew[NUM_TEST_SETTING_IDS];
	int ComboCurSel[NUM_DISCRETE_SETTING_IDS];

	afx_msg void OnPaint();

public:
	afx_msg void OnBnClickedButtonXmitSettings();
	afx_msg void OnBnClickedButtonLoadSettings();

	afx_msg LRESULT OnSettingsReply(WPARAM x, LPARAM y);
	afx_msg void OnBnClickedButtonXmitDebugSettings();
};
