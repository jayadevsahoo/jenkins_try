#ifndef OsTimeStamp_HH
#define OsTimeStamp_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OsTimeStamp - A time marker based off the VRTX32 clock.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/OsTimeStamp.hhv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

//@ Usage-Classes
//@ End-Usage

class OsTimeStamp
{
  public:
	OsTimeStamp(void);
	OsTimeStamp(const OsTimeStamp& rTime);

	~OsTimeStamp(void);


	void now(void);
	Uint32 diffTime(const OsTimeStamp& rTime) const; 
	Uint32 getPassedTime(void) const;
	void invalidate(void);
#ifdef SIGMA_DEVELOPMENT
	void print(void) const;
#endif

	static OsTimeStamp CurrentTime(void);

	inline void operator=(const OsTimeStamp& rTime);
	inline Boolean isValid(void) const;
	inline operator Uint32() const;

	static void SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL, 
			      const char*       pPredicate = NULL);


  protected:

  private:
	Uint32 diffTime_(const Uint32 compareStamp) const;

	//@ Data-Member: osStamp_
	//  Holds the VRTX32 time in ticks
	Uint32 osStamp_;
};

// Inlined methods
#include "OsTimeStamp.in"

#endif // OsTimeStamp_HH 
