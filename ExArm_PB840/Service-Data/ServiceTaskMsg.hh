#ifndef ServiceTaskMsg_HH
#define ServiceTaskMsg_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

//#include "GuiTimerId.hh"
//#include "Keys.hh"
#include "InterTaskMessage.hh"

//=====================================================================
// File: ServiceTaskMsg - Data structure definition for task message
// VRTX queue messages.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Data/vcssrc/ServiceTaskMsg.hhv   25.0.4.0   19 Nov 2013 14:21:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  yyy    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

union ServiceTaskMsg
{
	//@ Type: MessageEventType
	// This enum identifies the event type
	enum MessageEventType
	{
    	NOVRAM_UPDATE_EVENT = FIRST_APPLICATION_MSG,
    	SERVICE_MODE_PROMPT_EVENT,
    	SERVICE_MODE_DATA_EVENT
	};

	struct 
	{
    	Uint   msgType  : 8;
    	Uint   msgId    : 8;
	    Uint   msgData  : 16;
	} ServiceDataTaskMessage;

	// Use the following member to simply pass the 32 bits of
	// UserAnnunciationMsg data to message queues.
	Uint32 qWord;
};

#endif // ServiceTaskMsg_HH
