#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Covidien Plc claims exclusive 
// right. No part of this work may be used, disclosed, reproduced, 
// stored in an information retrieval system, or transmitted by any 
// means, electronic, mechanical, photocopying, recording, or otherwise 
// without the prior written permission of Covidien Plc.
//
//            Copyright (c) 2014-2025, Covidien Plc.
//====================================================================
#include "Barometer.hh"
#include "Barometer_MPL115A2.h"

Barometer* Barometer::instance_ = NULL;

Barometer::Barometer()
:offset_(0)
{
};

Barometer::~Barometer()
{
}

Barometer* Barometer::getInstance()
{
    if(instance_ == NULL)
        instance_ = Barometer_MPL115A2::getInstance();

    return instance_;
}

void Barometer::updateValue()
{
	pressure_ = getPressureRaw() + offset_;
}

void Barometer::setOffset(Real32 offset)
{
	offset_ = offset;
}

Real32 Barometer::getOffset() const
{
	return offset_;
}

