#ifndef AlarmRender_HH
#define AlarmRender_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: AlarmRender - Low-level word wrapping routines for alarm displays.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/AlarmRender.hhv   25.0.4.0   19 Nov 2013 14:07:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision:  005    By:  hhd	 Date:  04-10-1997      DR Number: 2521 
//  Project:    Sigma (R8027)
//  Description:
//		Fixed problem with English text being displayed in smaller font unnecessarily, 
//     	causing vent to reset.
//
//  Revision:  004  By: sah   Date:  20-Aug-1997      DR Number:  2379
//    Project:    Sigma (R8027)
//    Description:
//		Cleaning up declaration of 'stripDelimiterChar()'.
//
//  Revision:  003  By: hhd   Date:  30-July-997      DR Number: DCS 2265
//    Project:    Sigma (R8027)
//    Description:
//	 Modified to accomodate foreign language version of the Alarm messages.
// 
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "TextField.hh"
#include "TextFont.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class AlarmRender
{
public:
	static void Initialize(void);

	static Boolean WithoutWordWrap(
					wchar_t* pString1, wchar_t* pString2, wchar_t* pString3,
					const int* pPhraseIds, TextFont::Size fontSize,
					Int32 width, Int32 height, Alignment alignment,
					Int32 margin);

	static Int32 WithWordWrap(
					wchar_t* pString1, wchar_t* pString2, wchar_t* pString3,
					const int* pPhraseIds, TextFont::Size fontSize,
					Int32 width, Int32 height, Alignment alignment,
					Int32 margin, Int32 isBaseMsg=FALSE);

	static StringId GetSymbolHelpMessage(const int* pPhraseIds);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	AlarmRender(void);					// not implemented...
	~AlarmRender(void);					// not implemented...
	AlarmRender(const AlarmRender&);	// not implemented...
	void operator=(const AlarmRender&);	// not implemented...

	// The following method does word wrapping for alarm messages.
	static Int32 WordWrapping_(wchar_t* pString1, wchar_t* pString2, wchar_t* pString3,  
				TextFont::Size fontSize,
				Int32 areaWidth, Int32 areaHeight, Alignment alignment,
				Int32 margin, Int32 isBaseMsg=FALSE);
	
	static TextFont::Size GetNextSmallerFontSize_(TextFont::Size fontSize);
	static wchar_t *GetTok_(wchar_t *pTokenString); 	// get next token from Buf_  
	static void StripDelimiterChar_(const wchar_t *inString, wchar_t *outString);

	inline static Boolean  IsWhiteSpaceChar_(const wchar_t testChar);  

	enum { MAX_FORMAT_SIZE_ = 22 };
	enum { MAX_PHRASES_ = 3 };
	enum { MAX_BUFFER_SIZE_ = (MAX_PHRASES_ * TextField::MAX_STRING_LENGTH) };

	// Set to a value that allows an average token length of 4 characters, for
	// a string buffer that is full.
	enum { MAX_TOKENS_ = (MAX_BUFFER_SIZE_ / 4) };

	struct AlarmTextInfo_
	{
		 
		wchar_t*  pText;			// pointer to array of text characters...
		Int    width;			// width required by the text (in pixels)...
		Int    length;			// length (in chars) of array of text characters
		Int    height;			// height required by the text...
		Int    descent;			// descent required by the text...
		Int    yPos;			// calculated y position...
	};

	//@ Data-Member: Buf_
	// General-purpose buffer for message tokens (normal C strings, not
	// Cheap Text).
	static wchar_t Buf_[MAX_BUFFER_SIZE_];  

	//@ Data-Member: PBuf_
	// Pointer to buffer for message tokens (normal C strings, not
	// Cheap Text).
	static wchar_t* PBuf_;  
				       
	//@ Data-Member: CheapText_
	// Buffer for holding a single line's worth of Cheap Text.
	static wchar_t CheapText_[TextField::MAX_STRING_LENGTH];  

	//@ Data-Member: CurrentLine_
	// Buffer for holding a single line's worth of tokens that are ready
	// for conversion to Cheap Text format.
	static wchar_t CurrentLine_[MAX_BUFFER_SIZE_];  

	//@ Data-Member: rTextField_
	// A reference to a text field used for checking the width and height
	// of cheap text strings so that they can be auto-positioned.
	static TextField& RTextField_;

	//@ Data-Member: textFieldMemory_
	// A reference to a text field used for checking the width and height
	// of cheap text strings so that they can be auto-positioned.
	static Int TextFieldMemory_[(sizeof(TextField) + sizeof(Int) - 1)
							/ sizeof(Int)];

	//@ Constant:  DELIMITER_CHAR_
	// A constant character value for indicating hyphen locations.
	static const wchar_t  DELIMITER_CHAR_;  

};

// Inlined methods
#include "AlarmRender.in"

#endif // AlarmRender_HH 
