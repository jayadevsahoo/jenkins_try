/***************************************************************************
*
*		Copyright (c) 1995 MICROTEC RESEARCH INCORPORATED.
*
*	All rights reserved. MICROTEC RESEARCH's source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	MICROTEC RESEARCH. Any attempt or participation in deciphering,
*	decoding, reverse engineering or in any way altering the source code
*	is strictly prohibited, unless the prior written consent of
*	MICROTEC RESEARCH is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/


static const char sccsid[] = "%Z% %I% %M% %G%";
/*
 *
 * File:
 *
 *    timer.c
 *
 * Author:
 *
 *    Eugene Walden
 *
 * Date:
 *
 *    5-12-92
 *
 * Contents:
 *
 *    This file contains functions that implement the logio 
 *    functions for the DEV_TIMER for the real target.
 *
 * Revision History:
 *
 *    Name |   Date   | Description of modification
 *    -----+----------+--------------------------------------------
 *    EMW  | 06-18-92 | Added logio interrupt handling code
 *    DAR  | 08-19-92 | generic timer implementation
 */

#include <ansiprot.h>     	/* ANSI prototypes			*/
#include <logio.h>	     	/* logio defines and typedefs		*/
#include <timer.h>	  	/* timer specific values 	*/

#include "vbsp.h"      		/* VBSP return codes and typedefs	*/
#include "logio_charbuf.h"	/* typedef for character buffers      */ 
#include "compiler.h"           /* compiler-specific typedefs           */



/*
 * definition of device id
 */

logio_status_t logio_timer_init(
   _ANSIPROT1(logio_device_id_t));

logio_status_t logio_timer_control(
   _ANSIPROT3(
      logio_device_id_t, 
      logio_control_t,
      void *));

const logio_fops_t logio_timer_fops = {
	logio_timer_init,
	(logio_status_t (*)(logio_device_id_t , char *, int *))logio_not_supported,
	(logio_status_t (*)(logio_device_id_t , char *, int *))logio_not_supported,
	logio_timer_control,
	(logio_status_t (*)(logio_device_id_t ,logio_buff_t **))logio_not_supported,
	(logio_status_t (*)(logio_device_id_t , logio_buff_t *))logio_not_supported,
	(logio_status_t (*)(logio_device_id_t , logio_control_t , void *))logio_not_supported,
};


/*
 *
 * Function name:
 *
 *     logio_timer_init 
 *
 * Description:
 *
 *     This function initializes the timer device, with interrupts
 *     disabled. 
 *
 * Inputs:
 *
 *     id:	ID of the target device
 *
 * Outputs:
 *
 *     NONE
 *
 * Return codes:
 *
 *    LOGIO_STATUS_OK
 *    LOGIO_STATUS_RESOURCES 
 *
 * Side-effects:
 * Global variables:
 *
 */

logio_status_t logio_timer_init(id)
logio_device_id_t id;
{
    logio_status_t ret;		/* return code                   */ 
    timer_dev_desc_t *tm = (timer_dev_desc_t *)id->data;

    /* 
     * Set up the vectors and test routines for the TIMER device 
     * 
     * The TIMER device has one vector associated with it, 
     * corresponding to a single event (a timer tick).
     * 
     * The logio structures for TIMER look like: 
     * 
     *     +-------------------------------------------+
     *  +--+-next                                      |
     *  |  +-------------------------------------------+
     *  |  | vector number = TIMER_VECTOR             |
     *  |  +-------------------------------------------+
     *  |  | table_size = 1                            |
     *  |  +-------------------------------------------+
     *  |  | num_events = 1                            |
     *  |  +-------------------------------------------+
     *  |  | table     o                               |
     *  |  +-----------+-------------------------------+
     *  |              |
     *  |              V
     *  |   +----------+------------+---------+----+----+--------------+
     *  |   | timer    | TIMERTICK  | <flags> | <> | <> | tim_tick_tst |
     *  |   +----------+------------+---------+----+----+--------------+
     *  V
     */
    
    /* load ISR for DEV_TIMER */
    ret = logio_set_vector_and_test(
				    id, 
				    LOGIO_INT_EVENT_TIMERTICK, 
				    tm->config.vector, 
				    tm->fops.test
				    );
    
#if 0
    if (ret != LOGIO_STATUS_OK)
	sys_abort();
#endif

    /*
     * Perform the device-specific initialization.
     *
     */
    (*tm->fops.init)(id->data);
    
    return(LOGIO_STATUS_OK);
}

/*
 * Function name:
 *
 * 	logio_timer_control
 *
 * Description:
 * logio_device_control() is used for device specific commands.
 * The last parameter "arg" is a pointer to data to be used by the
 * function or to be filled in by the function.  Other functions
 * may ignore arg or may treat it directly as a data item; they
 * may, for example, be passed an int value.
 * 
 * For each device, the following controls are required for the
 * BSP developer to provide.
 * 
 * Control LOGIO_CONTROL_GETDEVNAME can be used for getting the
 * name of a device. In this case "arg" is a buffer up to
 * LOGIO_DEV_NAME_LEN, currently 16 bytes.
 * 
 * Control LOGIO_CONTROL_GETDEVTYPE can be used to get the type of
 * the underlying physical device. In this case "arg" points to a
 * variable of type logio_device_type_t. The only device type
 * currently defined is LOGIO_DEV_TYPE_GENERIC.
 * 
 * Controls LOGIO_CONTROL_GETCOUNTTIME and
 * LOGIO_CONTROL_SETCOUNTTIME get and set the number of
 * nanoseconds per timer count increment.
 * 
 * For LOGIO_CONTROL_GETCOUNTTIME, logio_device_control returns
 * the number of nanoseconds per timer count increment in "arg".
 * 
 * For LOGIO_CONTROL_SETCOUNTTYPE, logio_device_control sets the
 * number of nanoseconds that per counter increment to "arg".
 * 
 * If logio_device_control cannot set the timer count increment
 * time to "arg", then it makes the best attempt, writes into
 * "arg" the actual value that was set, and returns
 * LOGIO_STATUS_OK.
 * 
 * If this parameter cannot be modified at all, then
 * logio_device_control returns LOGIO_STATUS_NOTSUPPORTED.
 * 
 * Controls LOGIO_CONTROL_GETTIMERPRELOAD and
 * LOGIO_CONTROL_SETTIMERPRELOAD get and set the timer's preload
 * value, in counts.
 * 
 * For LOGIO_CONTROL_GETTIMERPRELOAD, logio_device_control returns
 * the number of counts per clock rollover in "arg".
 * 
 * For LOGIO_CONTROL_SETTIMERPRELOAD, logio_device_control sets
 * the number of counts per clock rollover to "arg".
 * 
 * If logio_device_control cannot set the preload value to "arg",
 * then it makes the best attempt, writes into "arg" the actual
 * value that was set, and returns LOGIO_STATUS_OK.
 * 
 * If this parameter cannot be modified at all, then
 * logio_device_control returns LOGIO_STATUS_NOTSUPPORTED.
 * 
 * Control LOGIO_CONTROL_GETTIMERROLLOVER gets the value at which
 * the timer count will be reloaded with the preload value.
 * logio_device_control returns the rollover value, in counts, in
 * "arg".
 * 
 * Control LOGIO_CONTROL_READTIMER gets the current value of the
 * timer's counter, in counts. logio_device_control returns the
 * counter value in "arg".
 * 
 * Controls LOGIO_CONTROL_SETHWADDR and LOGIO_CONTROL_GETHWADDR
 * can be used for devices that support the notion of a physical
 * address. In this case "arg" is a structure.
 * 
 * #define LOGIO_HW_ADDR_LEN 6
 * 
 * typedef struct { 
 *            int bufflen; 
 *            char addr[LOGIO_HW_ADDR_LEN]; 
 *            }
 *    logio_hw_addr_t;
 * 
 * Control LOGIO_CONTROL_RESET can be used to reset the device
 * when applicable.  The result is device dependent.
 * 
 * Control LOGIO_CONTROL_INTINFOGET is used to get information
 * about a specific interrupt.  Currently the only information is
 * "flags".  In this case "arg" points to a structure of type
 * logio_int_entry_t defined in logio.h by
 * 
 * typedef struct { 
 *            logio_int_event_t event; 
 *            int flags; 
 *            }
 *         logio_int_entry_t;
 * 
 * "event" is the only input parameter. "flags" values currently
 * supported are LOGIO_INT_ENABLED and LOGIO_INT_DISABLED.
 * 
 * Control LOGIO_CONTROL_INTINFOSET is used to set the values
 * specified in the structure.  It returns the old values. The
 * action specified by the control option is also performed at the
 * device level. LOGIO_INT_DISABLED will disable delivery to the
 * CPU of interrupts for the given <device,event> while
 * LOGIO_INT_ENABLED will enable them.
 * 
 * Other device specific control command can be added to the list
 * of available commands. Ready Systems reserves the use of
 * constants in the range 0-0xffff.
 * 
 * This function is nonblocking. Certain operation may result the
 * status LOGIO_STATUS_INPROGRESS. In this case, the function
 * logio_device_poll() can be used to find out the status of this
 * operation.
 * 
 *
 *
 * Inputs:
 *
 *      id:		The ID of the target device
 *      control:	The control command
 *      arg: 		A pointer to data used by the function or 
 *                      to be filled in by the function
 *
 * Outputs:
 *
 *      arg: 		A pointer to data used by the function or 
 *                      to be filled in by the function
 *
 * Return codes:
 *
 * 	LOGIO_STATUS_OK
 * 	LOGIO_STATUS_NOTSUPPORTED
 * 	LOGIO_STATUS_INPROGRESS
 *
 * Side-effects:
 * Global variables:
 *
 */
int logio_timer_control(id, control, arg)
logio_device_id_t id;
logio_control_t control;
void *arg;
{
    logio_status_t		ret;
    logio_int_entry_t 	*info;
    timer_dev_desc_t *tm = (timer_dev_desc_t *)id->data;
    
    if (control == LOGIO_CONTROL_READTIMER)
	return ((*tm->fops.read_timer)(id->data, (unsigned long *) arg));
    
    ret = LOGIO_STATUS_OK;
    
    switch(control) {
	
    case LOGIO_CONTROL_GETDEVNAME:
	ret = LOGIO_STATUS_OK;
	break;
	
    case LOGIO_CONTROL_GETDEVTYPE:
	ret = LOGIO_STATUS_OK;
	break;
	
    case LOGIO_CONTROL_RESET:
	ret = LOGIO_STATUS_OK;
	break;
	
    case LOGIO_CONTROL_GETCOUNTTIME:
	ret = (*tm->fops.get_count)(id->data, (unsigned long *) arg);
	break;
	
    case LOGIO_CONTROL_SETCOUNTTIME:
	ret = (*tm->fops.set_count)(id->data, (unsigned long *) arg); 
	break;
	
    case LOGIO_CONTROL_GETTIMERPRELOAD:
	ret = (*tm->fops.get_preload)(id->data, (unsigned long *) arg); 
	break;
	
    case LOGIO_CONTROL_SETTIMERPRELOAD:
	/* same as LOGIO_CONTROL_SETTIMERROLLOVER */
	ret = (*tm->fops.set_preload)(id->data, (unsigned long *) arg); 
	break;

    case LOGIO_CONTROL_GETTIMERROLLOVER:
	ret = (*tm->fops.get_preload)(id->data, (unsigned long *) arg); 
	break;
	
    case LOGIO_CONTROL_TIMERGETMAXCOUNT:
	ret = (*tm->fops.get_rollover)(id->data, (unsigned long *) arg); 
	break;
	
    case LOGIO_CONTROL_INTINFOSET:
	info = (logio_int_entry_t *) arg;
	if (info->flags == LOGIO_INT_ENABLED) {
	    ret = (*tm->fops.int_enable)(id->data); 
	}
	else if (info->flags == LOGIO_INT_DISABLED) {
	    ret = (*tm->fops.int_disable)(id->data); 
	} 
	else
	    ret = LOGIO_STATUS_NOTSUPPORTED;
	break;
	
    case LOGIO_CONTROL_INTINFOGET:
	info = (logio_int_entry_t *) arg;
	ret = (*tm->fops.int_info_get)(id->data, &info->flags);
	break;
	
    default:
	return(LOGIO_STATUS_NOTSUPPORTED);
    }
    
    return(ret);
}

