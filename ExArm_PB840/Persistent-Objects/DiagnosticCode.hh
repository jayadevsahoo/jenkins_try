
#ifndef DiagnosticCode_HH
#define DiagnosticCode_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class:  DiagnosticCode - Diagnostic Code Layout Class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Persistent-Objects/vcssrc/DiagnosticCode.hhv   25.0.4.0   19 Nov 2013 14:18:48   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By:  rhj    Date:  16-Apr-2009    SCR Number: 6495  
//  Project:  840S2
//  Description:
//      Added isSingleTest data member to ExtendedSelfTestBits struct
//      and added isSingleTest parameter to setExtendedSelfTestCode
//      function.
//
//  Revision: 005  By:  sah    Date:  01-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 004  By:  sah    Date:  08-May-1998    DCS Number: 5041
//  Project:  Sigma (R8027)
//  Description:
//      Added two new system event ids, 'APP_NOVRAM_ACCESS_FAILURE' and
//	'APP_NOVRAM_RESTORE_FAILURE', for providing investigation
//	information to NOVRAM failures.
//
//  Revision: 003  By:  sah    Date:  30-Sep-1997    DCS Number: 1923
//  Project:  Sigma (R8027)
//  Description:
//      Replaced the system event ID for a diagnostic log clearance,
//	with an event ID for a task-control transition timeout.
//
//  Revision: 002  By:  sah    Date:  13-May-1997    DCS Number: 2085
//  Project:  Sigma (R8027)
//  Description:
//      Added the storing of auxillary information for POST diagnostics
//      (see 'setStartupSelfTestCode()').
//
//  Revision: 001  By:  sah    Date:  15-Mar-1997    DCS Number: <NONE>
//  Project:  Sigma (R8027)
//  Description:
//          Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "PersistentObjsId.hh"
#include "TestResultId.hh"

//@ Usage-Classes
#include "Uint96.hh"
//@ End-Usage


class DiagnosticCode
{
  public:
    //@ Type:  DiagCodeTypeId
    // Type of diagnostic code that this instance contains.
    enum DiagCodeTypeId
    {
      UNDEFINED_DIAG_CODE       = 0,  // this MUST be zero...

      SYSTEM_EVENT_DIAG		= 1,
      SOFTWARE_TEST_DIAG	= 2,
      EXCEPTION_DIAG		= 3,
      BACKGROUND_TEST_DIAG	= 4,
      STARTUP_SELF_TEST_DIAG	= 5,
      COMMUNICATION_DIAG	= 6,
      SHORT_SELF_TEST_DIAG	= 7,
      EXTENDED_SELF_TEST_DIAG	= 8,
      NMI_DIAG			= 9,

      NUM_FAULT_TYPE_IDS
    };

    //@ Type:  CpuEventId
    // This defines the CPU in which this event occurred.
    enum CpuEventId
    {
      UNDEFINED_CPU_EVENT	= 0,  // this MUST be zero...

      BD_CPU_DIAG		= 1,
      GUI_CPU_DIAG		= 2
    };

    DiagnosticCode(const DiagnosticCode& diagCode);
    DiagnosticCode(void);
    ~DiagnosticCode(void);

    inline Boolean  isClear  (void) const;
    inline void     clearCode(void);

    inline DiagnosticCode::DiagCodeTypeId  getDiagCodeTypeId(void) const;

#if defined(SIGMA_GUI_CPU)
    void  getDiagCodeString(wchar_t pDiagCodeString[7]) const;  
#endif // defined(SIGMA_GUI_CPU)

    //================================================================
    //  System Event Diagnostic...
    //================================================================

    //@ Type:  SystemEventId
    // This defines the system events that can be stored in a diagnostic
    // code.
    enum SystemEventId
    {
      UNDEFINED_SYSTEM_EVENT		=  0,  // this MUST be zero...

      REAL_TIME_CLOCK_UPDATE_PENDING	=  1,
      REAL_TIME_CLOCK_UPDATE_DONE	=  2,
      TASK_CONTROL_TRANSITION_TIMEOUT	=  3,
      DIAG_LOG_ENTRY_CORRECTED		=  4,
      WATCHDOG_TIMER_FAILED		=  5,
      BD_CLOCK_SYNC_PENDING		=  6,
      BD_CLOCK_SYNC_DONE		=  7,
      VENT_IN_OP_LATCHED		=  8,
      INTENTIONAL_RESET_FORCED		=  9,
      SETTINGS_XACTION_FAILED		= 10,
      SETTINGS_XACTION_SUCCEEDED	= 11,
      SETTINGS_STATE_MISMATCH		= 12,
      APP_NOVRAM_ACCESS_FAILURE		= 13,
      APP_NOVRAM_RESTORE_FAILURE	= 14
    };

    //@ Type:  SystemEventBits
    // This defines the bit layout of system event codes.
    struct SystemEventBits
    {
      Uint16  diagId          :  4;  // 'SYSTEM_EVENT_DIAG'...
      Uint16  cpuFlagId       :  2;  // '1' for BD; '2' for GUI...
      Uint16  systemEventId   : 10;  // system event identifier...
      Uint16  UNUSED_WORD;           // unused word of the first long word...
      Uint32  auxEventCode;          // auxilliary system event code...
      Uint32  UNUSED_LWORD4;         // unused long word #4...
    };

    inline SystemEventBits  getSystemEventCode(void) const;

    void  setSystemEventCode(const SystemEventId systemEventId,
    			     const Uint32        auxEventCode = 0u);

    //================================================================
    //  Software Test Diagnostic...
    //================================================================

    //@ Type:  SoftwareTestBits
    // This defines the bit layout of software test codes.
    struct SoftwareTestBits
    {
      Uint16  diagId          :  4;  // 'SOFTWARE_TEST_DIAG'...
      Uint16  cpuFlagId       :  2;  // '1' for BD; '2' for GUI...
      Uint16  assertionId     :  3;  // software assertion identifier...
      Uint16  UNUSED_BITS     :  7;  // unused bits of the first word...
      Uint8   activeTaskId;          // active task identifier...
      Uint8   subsystemId;           // subsystem identifier...
      Uint16  moduleId;              // module identifier...
      Uint16  lineNumber;            // line number...
      Uint32  auxErrorCode;          // line number...
    };

    inline SoftwareTestBits  getSoftwareTestCode(void) const;

    void  setSoftwareTestCode(const SoftFaultID softFaultId,
			      const SubSystemID subsystemId,
			      const Uint16      moduleId,
			      const Uint16      lineNumber,
			      const Uint32      auxErrorCode);

    //================================================================
    //  Exception Diagnostic...
    //================================================================

    //@ Type:  ExceptionBits
    // This defines the bit layout of exception codes.
    struct ExceptionBits
    {
      Uint16  diagId          :  4;  // 'EXCEPTION_DIAG'...
      Uint16  cpuFlagId       :  2;  // '1' for BD; '2' for GUI...
      Uint16  vectorNumber    : 10;  // exception identifier...
      Uint16  activeTaskId;          // active task identifier...
      Uint32  instructionAddr;       // program counter at exception point...
      Uint32  faultAddr;             // this address may have cause exception
    };

    inline ExceptionBits  getExceptionCode(void) const;

    void  setExceptionCode(const Uint16 vectorNumber,
			   const Uint32 instructionAddr,
			   const Uint32 faultAddr,
			   const Uint16 taskId);

    //================================================================
    //  Non-Maskable Interrupt Diagnostic...
    //================================================================

    //@ Type:  NmiBits
    // This defines the bit layout of processor trap codes.
    struct NmiBits
    {
      Uint16  diagId          :  4;  // 'EXCEPTION_DIAG'...
      Uint16  cpuFlagId       :  2;  // '1' for BD; '2' for GUI...
      Uint16  UNUSED_BITS     : 10;  // unused bits in first word...
      Uint16  nmiSourceId;           // source ID of this NMI...
      Uint32  errorCode;             // generic error code...
      Uint32  UNUSED_LWORD3;         // unused long word #3...
    };

    inline NmiBits  getNmiCode(void) const;

    void  setNmiCode(const Uint16 nmiSourceId,
		     const Uint32 errorCode);

    //================================================================
    //  Communication Diagnostic...
    //================================================================

    //@ Type:  CommunicationBits
    // This defines the bit layout of communication diagnostic codes.
    struct CommunicationBits
    {
      Uint16  diagId          :  4;  // 'COMMUNICATION_DIAG'...
      Uint16  cpuFlagId       :  2;  // '1' for BD; '2' for GUI...
      Uint16  UNUSED_BITS     : 10;  // unused bits in first word...
      Uint16  commFaultId;           // communication diagnostic code...
      Uint32  UNUSED_LWORD2;         // unused long word #2...
      Uint32  UNUSED_LWORD3;         // unused long word #3...
    };

    inline CommunicationBits  getCommunicationCode(void) const;

    void  setCommunicationCode(const Uint32 commFaultId);

    //================================================================
    //  Startup (POST and Sys-Init) Self-Test Diagnostic...
    //================================================================

    //@ Type:  StartupSelfTestBits
    // This defines the bit layout of Startup-Self-Test (i.e., POST and INIT)
    // diagnostic codes.
    struct StartupSelfTestBits
    {
      Uint16  diagId          :  4;  // 'STARTUP_SELF_TEST_DIAG'...
      Uint16  cpuFlagId       :  2;  // '1' for BD; '2' for GUI...
      Uint16  failureLevel    :  3;  // failure level (major, minor, etc.)
      Uint16  UNUSED_BITS     :  7;  // unused bits of the first word...
      Uint16  startupFaultId;        // startup fault identifer...
      Uint32  programCounter;        // code location of failure...
      Uint8   UNUSED_BYTE;           // unused byte...
      Uint8   vectorNum;             // exception vector number...
      Uint8   nmiSourceReg;          // NMI source register contents...
      Uint8   auxErrorCode;          // auxillary error code...
    };

    inline StartupSelfTestBits  getStartupSelfTestCode(void) const;

    void  setStartupSelfTestCode(const TestResultId failureCriticality,
				 const Uint16       startupFaultId,
				 const Uint32       programCounter,
				 const Uint8        vectorNum,
				 const Uint8        nmiSourceReg,
				 const Uint8        auxErrorCode);

    //================================================================
    //  Background Test Diagnostic...
    //================================================================

    //@ Type:  BackgroundTestBits
    // This defines the bit layout of the Background Test diagnostic codes.
    struct BackgroundTestBits
    {
      Uint16  diagId          :  4;  // 'BACKGROUND_TEST_DIAG'...
      Uint16  cpuFlagId       :  2;  // '1' for BD; '2' for GUI...
      Uint16  failureLevel    :  3;  // failure level (major, minor, etc.)
      Uint16  testNumber      :  7;  // background fault identifer...
      Uint16  activeTaskId;          // active task identifier...
      Uint32  errorCode;             // additional error info, if needed...
      Uint32  UNUSED_LWORD3;         // unused long word #3...
    };

    inline BackgroundTestBits  getBackgroundTestCode(void) const;

    void  setBackgroundTestCode(const TestResultId testFailureLevel,
				const Uint16       backgroundTestNumber,
				const Uint32       errorCode = 0u);

    //================================================================
    //  Short Self-Test Diagnostic...
    //================================================================

    //@ Type:  ShortSelfTestBits
    // This defines the bit layout of SST diagnostic codes.
    struct ShortSelfTestBits
    {
      Uint16  diagId          :  4;  // 'SHORT_SELF_TEST_DIAG'...
      Uint16  failureLevel    :  3;  // failure level (major, minor, etc.)
      Uint16  testNumber      :  9;  // SST test number...
      Uint8   UNUSED_BYTE;           // unused byte of the first long word...
      Uint8   stepNumber;            // SST test step number...
      Uint32  UNUSED_LWORD2;         // unused long word #2...
      Uint32  UNUSED_LWORD3;         // unused long word #3...
    };

    inline ShortSelfTestBits  getShortSelfTestCode(void) const;

    void  setShortSelfTestCode(const TestResultId testFailureLevel,
			       const Uint16  sstTestNumber,
			       const Uint16  testStepNumber = 1u);

    //================================================================
    //  Extended Self-Test Diagnostic...
    //================================================================

    //@ Type:  ExtendedSelfTestBits
    // This defines the bit layout of EST diagnostic codes.
    struct ExtendedSelfTestBits
    {
      Uint16  diagId          :  4;  // 'EXTENDED_SELF_TEST_DIAG'...
      Uint16  failureLevel    :  3;  // failure level (major, minor, etc.)
      Uint16  testNumber      :  9;  // EST test number...
      Uint8   isSingleTest;          // Is this a Single test?
      Uint8   stepNumber;            // EST test step number...
      Uint32  UNUSED_LWORD2;         // unused long word #2...
      Uint32  UNUSED_LWORD3;         // unused long word #3...
    };

    inline ExtendedSelfTestBits  getExtendedSelfTestCode(void) const;

    void  setExtendedSelfTestCode(const TestResultId testFailureLevel,
				  const Uint16  estTestNumber,
				  const Uint16  testStepNumber = 1u,
				  const Boolean isSingleTest   = FALSE);


    inline void  operator=(const DiagnosticCode& diagCode);

    inline Boolean  operator==(const DiagnosticCode& diagCode) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);

  private:
    //@ Type:  CommonLayoutBits_
    // This defines the common bit layout of all of the diagnostic codes.
    struct CommonLayoutBits_
    {
      Uint16  diagId         :  4;  // type of fault that occurred...
      Uint16  RESERVED_BITS  : 12;  // to be defined by each code layout...
      Uint16  RESERVED_WORD;        // to be defined by each code layout...
      Uint32  RESERVED_LWORD2;      // to be defined by each code layout...
      Uint32  RESERVED_LWORD3;      // to be defined by each code layout...
    };

    union	// anonymous union...
    {
      //@ Data-Member:  systemEventCode_
      // This contains the code of a system event.
      SystemEventBits  systemEventCode_;

      //@ Data-Member:  softwareTestCode_
      // This contains the code of a software test failure.
      SoftwareTestBits  softwareTestCode_;

      //@ Data-Member:  exceptionCode_
      // This contains the code of an exception.
      ExceptionBits  exceptionCode_;

      //@ Data-Member:  nmiCode_
      // This contains the code of a non-maskable interrupt.
      NmiBits  nmiCode_;

      //@ Data-Member:  backgroundTestCode_
      // This contains the code of a background test fault.
      BackgroundTestBits  backgroundTestCode_;

      //@ Data-Member:  communicationCode_
      // This contains the code of a communication fault.
      CommunicationBits  communicationCode_;

      //@ Data-Member:  startupSelfTestCode_
      // This contains the code of a POST or INIT fault.
      StartupSelfTestBits  startupSelfTestCode_;

      //@ Data-Member:  shortSelfTestCode_
      // This contains the code of an SST fault.
      ShortSelfTestBits  shortSelfTestCode_;

      //@ Data-Member:  extendedSelfTestCode_
      // This contains the code of an EST fault.
      ExtendedSelfTestBits  extendedSelfTestCode_;

      //@ Data-Member:  commonBits_
      // This contains the common bit layout of all of the diagnostic codes.
      CommonLayoutBits_  commonBits_;

      //@ Data-Member:  code_
      // This is the 96-bit representation of the diagnostic code.
      Uint96  code_;
    };

    //@ Constant:  CPU_EVENT_ID_
    // This static constant is defined to contain the ID of the CPU in
    // which this code is running.
    static const DiagnosticCode::CpuEventId  CPU_EVENT_ID_;
};


// Inlined methods..
#include "DiagnosticCode.in"


#endif // DiagnosticCode_HH 
