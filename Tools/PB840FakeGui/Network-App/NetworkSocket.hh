#ifndef  NetworkSocket_HH
#define	NetworkSocket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Filename: NetworkSocket - Class definition of the Network Socket
//   which provided interfaces to the standard Network Socket
//   functionality, a parent class of all the socket classes.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/NetworkSocket.hhv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//@ Modification-Log
//
//  Revision: 006  By: gdc     Date: 30-Dec-2010  SCR Number: 6722
//  Project:  XENA2
//  Description:
//      Generate message sequence numbers only for debug builds.
//      Removed getCurrentSequenceNum() as dead code.
//
//  Revision: 005  By: gdc     Date: 30-Dec-2010  SCR Number: 6717
//  Project:  XENA2
//  Description:
//      Corrected creation and initialization of all socket types.
//
//  Revision: 004  By: fh     Date: 20-July-10    DR Number: 6617
//  Project:  MAT
//  Description:
//      Modified Communications down threshold to 1.4 seconds or 7 missed
// 		packets at 200 ms interval, MAX_ALIVE_MSG_MISS.
//		The smoothed round trip timer, TCP_REXMTVAL, is set to 1 second also in 
//		Stackware\tcp_var.h. Vent loses communication with a LC0070, 
// 		Max Msg Delay Exceeded message in the logs. Communications are declared 
// 		down when either CPU does not receive a TCP message within 1 second. 
// 		The comms down declaration from either cpu occurs after 1 second.  
// 		The smoothed round trip timer, TCP_REXMTVAL. That is computed dynamically 
// 		based on message traffic transmissions which was larger than 1 second.  
// 		When data does not get transmitted by the hardware/driver layer the 
// 		protocol layer should attempt to retransmit but the comms goes down 
// 		before this is attempted. Fix was to make the comms down interval 1.4 
// 		seconds and the smoother round trip timer to a fixed number of 1 second.  
//
//  Revision: 003  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002  By:   hct     Date:  04 JUN 1997    DR Number: 1967 
//       Project:  Sigma (R8027)
//       Description:
//             Reduce communications down timeout from 4 seconds to 
//             1 second (KEEP_ALIVE_INTERVAL * MAX_ALIVE_MSG_MISS). 
//
//  Revision: 001  By:  <jhv>    Date:  06-June-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

enum NetworkAppConstants
{
    TOTAL_MSG_NUMBER=HIGHEST_NETWORK_MSG_ID-LOWEST_NETWORK_MSG_ID + 1,
    TOTAL_RECVMSG_CALLBACKS = TOTAL_MSG_NUMBER*2, 
    TOTAL_OPEN_CALLBACKS = 2,
    COMMUNICATION_RETRY = 3,
    MAX_NETWORK_WAIT_TIME = 10000,  // maximum wait interval in ms before
    // declaring a communication down state
    // when the system powers up.
    INITIAL_COMMUNICATION_LINK_RETRY = 5,
    MAX_NETWORK_BUF_SIZE = 2000,
    COMM_OPEN_ITERATION_TIME = 2000,    // 2000 milliseconds.
    KEEP_ALIVE_INTERVAL = 200,          // 200 milli seconds.
    MAX_INPUT_WAIT_DURATION = 2000,     // 2000 milli seconds.
    MAX_ALIVE_MSG_MISS = 7,             // 1.4 seconds
    MAX_BROADCAST_MSG_SIZE = 1000
};

class NetworkSocket
{
public:

    NetworkSocket();
    virtual ~NetworkSocket(void);
    virtual void reInit(Int id);
    virtual Int sendMsg( XmitDataMsgId msgId, char *buf, Uint nbytes);
    virtual Int recvMsg( XmitDataMsgId msgId, char *buf, Uint nbytes);
    virtual Boolean anyLostMsg() const;
    virtual SigmaStatus verifyConnection(XmitDataMsgId thisActive,
                                         XmitDataMsgId otherActive);
    virtual Int readData(char *pData, Uint nbytes);
    Uint32 getNextSequenceNum();

    Int openTcpServer(PortId portId, Int numClients);
    Int acceptConnection( NetworkSocket &rSocket, Uint32 waittime );
    Int openTcpClient(Uint32 ipAddr, PortId portId);

    virtual void down(void);
    inline Int getSocketId() const;
    inline Boolean opened(void) const;


    static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL, 
                          const char*       pPredicate = NULL);

protected:
    void setLostMsgFlag_(Boolean);

    //@ Data-Member:  lostMsgFlag_
    // This flag indicates whether a network message has been
    // discarded due to communication errors.
    Boolean lostMsgFlag_;

    //@ Data-Member:  maxMsgRecvTime_
    // amount of time to wait for a complete input message.
    Uint32 maxMsgRecvTime_;

    //@ Data-Member:  socketId_
    // Socket file descriptor id.
    Int socketId_;

private:
    NetworkSocket(const NetworkSocket&);    // not implemented...
    void   operator=(const NetworkSocket&); // not implemented...

    //@ Data-Member:  sequenceNum_
    // Sent Data Packet sequence number.
    Uint32      sequenceNum_;

};

// Inlined methods...
#include "NetworkSocket.in"


#endif // NetworkSocket_HH 
