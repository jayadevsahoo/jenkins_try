#ifndef ETHERNET_BROADCAST_MANAGER_H
#define ETHERNET_BROADCAST_MANAGER_H

#include <winsock2.h>
#include "A2dReadingsStructs.h"

 struct EthernetBroadcastMsg
{
	enum 	
    {
		NUM_SIGNALS = 18,
		NUM_SLOTS = 10
	};
	UINT16	msgId;		// = 9925;
	UINT16	pktSize;	// = sizeof(BdEthernetBroadcastMsg.data);
	UINT	seqNumber;	// = 0;
	FLOAT	signalData[NUM_SIGNALS][NUM_SLOTS];
	BYTE    ZeroTermination;
	UINT	CheckSum;
};

#define BD_REPLY_SETTINGS_MSG_ID			9813
#define BD_SIGNALS_RESPONSE_MSG_ID			9926
#define BD_RESPONSE_MSG_ID					9927
#define BD_PING_MSG_ID						9928
#define BD_CAL_STATUS_MSG_ID				9929
#define BD_ALL_GAINS_REPLY_MSG_ID			9930
#define BD_READ_DEBUG_REPLY_MSG_ID			9831

#define PC_DISABLE_BROADCAST_MSG_ID			9800
#define PC_ENABLE_BROADCAST_MSG_ID			9801
#define PC_SET_SIGNALS_MSG_ID				9802

#define PC_CALIBRATION_MSG_ID				9810
#define PC_CAL_CANCEL_MSG_ID				9811
#define PC_SEND_SETTINGS_MSG_ID				9812
#define PC_REQUEST_SETTINGS_MSG_ID			9813
#define PC_SEND_SETTINGS_DEBUG_MSG_ID		9814
#define PC_TEST_MSG_ID						9815

#define PC_PING_MSG_ID						9828

#define PC_SET_PC_GAIN_MSG_ID				9850
#define PC_SET_FC_GAIN_MSG_ID				9851
#define PC_SET_EV_GAIN_MSG_ID				9852
#define PC_SET_PEEP_GAIN_MSG_ID				9853
#define PC_SET_PID_GAIN_MSG_ID				9854
#define PC_SET_PBP_GAIN_MSG_ID				9855
#define PC_SET_ALL_GAINS_MSG_ID				9856
#define PC_GET_PC_GAIN_MSG_ID				9857
#define PC_GET_FC_GAIN_MSG_ID				9858
#define PC_GET_EV_GAIN_MSG_ID				9859
#define PC_GET_PEEP_GAIN_MSG_ID				9860
#define PC_GET_PID_GAIN_MSG_ID				9861
#define PC_GET_PBP_GAIN_MSG_ID				9862
#define PC_GET_ALL_GAINS_MSG_ID				9863
#define PC_ENABLE_GAIN_MSG_ID				9864
#define PC_WRITE_DEBUG_MSG_ID				9865
#define PC_READ_DEBUG_MSG_ID				9866

class EthernetBroadcastManager_t
{
public:
	enum
	{
		BD_DEBUG_MSG_ID = 9925,
		UDP_PORT_ID = 50000,   // Jeff is changing this to a non-registered port > 49152
		NUM_MSG_BUFFERS = 2
	};
	EthernetBroadcastManager_t();
	~EthernetBroadcastManager_t();

	void collect(ConvertedA2dReadings_t A2dData);
	void collect(ConvertedA2dReadings_t A2dData, RawA2dReadings_t RawA2dReadings, CommandValues_t CommandValues);
    void SendDebug(float * pData);
	void send();
	int initialize();
	void SendPacket(char *pPacket, int Size);

protected:
	//void SwapNetworkByteOrder(UINT *pSrc, UINT *pDst, int Size);

	int		slotIndex_;	
	bool	isTransmitInProgress_;
	int		collectMsgIndex_;
	int		sendMsgIndex_;
	EthernetBroadcastMsg	msg_[NUM_MSG_BUFFERS];

	//SOCKET	Socket;
	bool	SocketInitialized;
	int		nOptiontValue;
	//struct sockaddr_in SockAddr;
};

extern EthernetBroadcastManager_t EthernetBroadcastManager;


//----------------------------------------------------------------------------------------------------

class EthernetBroadcastRXThread_t
{

public:
	static uint32_t ThreadProcedure();
	static void Thread();
	static void Initialize();

protected:
	static void ProcessRXCommand(char RxBuf[], int Size);
	//static void SwapNetworkByteOrder(UINT *pSrc, UINT *pDst, int Size);

private:
	EthernetBroadcastRXThread_t(void);
	~EthernetBroadcastRXThread_t(void);
};


#endif // ETHERNET_BROADCAST_MANAGER_H
