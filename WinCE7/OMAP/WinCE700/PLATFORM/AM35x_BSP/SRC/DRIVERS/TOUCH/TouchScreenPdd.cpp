//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
//
// Use of this sample source code is subject to the terms of the Microsoft
// license agreement under which you licensed this sample source code. If
// you did not accept the terms of the license agreement, you are not
// authorized to use this sample source code. For the terms of the license,
// please see the license agreement between you and Microsoft or, if applicable,
// see the LICENSE.RTF on your install media or the root of your tools installation.
// THE SAMPLE SOURCE CODE IS PROVIDED "AS IS", WITH NO WARRANTIES OR INDEMNITIES.
//
// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/


//------------------------------------------------------------------------------
// Public
//
#include <windows.h>
#include <types.h>
#include <nkintr.h>
#include <creg.hxx>
#include <tchstream.h>
#include <tchstreamddsi.h>
#include <pm.h>

//------------------------------------------------------------------------------
// Platform
//
#include "omap.h"
#include <ceddk.h>
#include <ceddkex.h>
#include "soc_cfg.h"
#include <initguid.h>
#include <sdk_gpio.h>
#include "sdk_i2c.h"
#include "touchscreenpdd.h"

//------------------------------------------------------------------------------
// Debug zones
//
#ifndef SHIP_BUILD

#undef ZONE_ERROR
#undef ZONE_WARN
#undef ZONE_FUNCTION
#undef ZONE_INFO
#undef ZONE_TIPSTATE

#define ZONE_ERROR          DEBUGZONE(0)
#define ZONE_WARN           DEBUGZONE(1)
#define ZONE_FUNCTION       DEBUGZONE(2)
#define ZONE_INFO           DEBUGZONE(4)
#define ZONE_TIPSTATE       DEBUGZONE(9)

#endif


//------------------------------------------------------------------------------
// globals
//
static DWORD                          s_mddContext;
static PFN_TCH_MDD_REPORTSAMPLESET    s_pfnMddReportSampleSet;


//==============================================================================
// Function Name: TchPdd_Init
//
// Description: PDD should always implement this function. MDD calls it during
//              initialization to fill up the function table with rest of the
//              Helper functions.
//
// Arguments:
//              [IN] pszActiveKey - current active touch driver key
//              [IN] pMddIfc - MDD interface info
//              [OUT]pPddIfc - PDD interface (the function table to be filled)
//              [IN] hMddContext - mdd context (send to MDD in callback fn)
//
// Ret Value:   pddContext.
//==============================================================================
extern "C" DWORD WINAPI TchPdd_Init(LPCTSTR pszActiveKey,
    TCH_MDD_INTERFACE_INFO* pMddIfc,
    TCH_PDD_INTERFACE_INFO* pPddIfc,
    DWORD hMddContext
    )
{
    DWORD pddContext = 0;

    DEBUGMSG(ZONE_FUNCTION, (TEXT("TchPdd_Init+\r\n")));

    // Initialize once only
    if (s_TouchDevice.bInitialized)
    {
        pddContext = (DWORD) &s_TouchDevice;
        goto cleanup;
    }

    // Remember the callback function pointer
    s_pfnMddReportSampleSet = pMddIfc->pfnMddReportSampleSet;

    // Remember the mdd context
    s_mddContext = hMddContext;

    s_TouchDevice.nSampleRate = DEFAULT_SAMPLE_RATE;
    s_TouchDevice.dwPowerState = D0;
    s_TouchDevice.dwSamplingTimeOut = INFINITE;
    s_TouchDevice.bTerminateIST = FALSE;
    s_TouchDevice.hTouchPanelEvent = 0;

    // Initialize HW
    if (!PDDInitializeHardware(pszActiveKey))
    {
        DEBUGMSG(ZONE_ERROR,  (TEXT("ERROR: TOUCH: Failed to initialize touch PDD.\r\n")));
        goto cleanup;
    }

    // Get sysintr values from the OAL for PENIRQ interrupt
    if (!KernelIoControl(
            IOCTL_HAL_REQUEST_SYSINTR,
            &s_TouchDevice.nPenIRQ,
            sizeof(s_TouchDevice.nPenIRQ),
            &s_TouchDevice.dwSysIntr,
            sizeof(s_TouchDevice.dwSysIntr),
            NULL
            ) )
    {
        DEBUGMSG(ZONE_ERROR, (TEXT("ERROR: TOUCH: Failed to request the touch sysintr.\r\n")));
        s_TouchDevice.dwSysIntr = (DWORD)SYSINTR_UNDEFINED;
        goto cleanup;
    }

	// Set the interrupt as a wake-up source
	KernelIoControl(IOCTL_HAL_ENABLE_WAKE, &s_TouchDevice.dwSysIntr,
			sizeof(s_TouchDevice.dwSysIntr), NULL, 0, NULL);

    //Calibrate the screen, if the calibration data is not already preset in the registry
    PDDStartCalibrationThread();

    //Initialization of the h/w is done
    s_TouchDevice.bInitialized = TRUE;

    pddContext = (DWORD) &s_TouchDevice;

    // fill up pddifc table
    pPddIfc->version        = 1;
    pPddIfc->pfnDeinit      = TchPdd_Deinit;
    pPddIfc->pfnIoctl       = TchPdd_Ioctl;
    pPddIfc->pfnPowerDown   = TchPdd_PowerDown;
    pPddIfc->pfnPowerUp     = TchPdd_PowerUp;


cleanup:
    DEBUGMSG(ZONE_FUNCTION, (TEXT("TchPdd_Init-\r\n")));
    return pddContext;
}


//==============================================================================
// Function Name: TchPdd_DeInit
//
// Description: MDD calls it during deinitialization. PDD should deinit hardware
//              and deallocate memory etc.
//
// Arguments:   [IN] hPddContext. pddcontext returned in TchPdd_Init
//
// Ret Value:   None
//
//==============================================================================
void WINAPI TchPdd_Deinit(DWORD hPddContext)
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("TchPdd_Deinit+\r\n")));

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hPddContext);

    // Close the IST and release the resources before
    // de-initializing.
    PDDTouchPanelDisable();

    //  Shutdown HW
    PDDDeinitializeHardware();

    // Release interrupt
    if (s_TouchDevice.dwSysIntr != 0)
        {
        KernelIoControl(
            IOCTL_HAL_RELEASE_SYSINTR,
            &s_TouchDevice.dwSysIntr,
            sizeof(s_TouchDevice.dwSysIntr),
            NULL,
            0,
            NULL
            );
        }

    s_TouchDevice.bInitialized = FALSE;


    DEBUGMSG(ZONE_FUNCTION, (TEXT("TchPdd_Deinit-\r\n")));
}


//==============================================================================
// Function Name: TchPdd_Ioctl
//
// Description: The MDD controls the touch PDD through these IOCTLs.
//
// Arguments:   [IN] hPddContext. pddcontext returned in TchPdd_Init
//              DOWRD dwCode. IOCTL code
//              PBYTE pBufIn. Input Buffer pointer
//              DWORD dwLenIn. Input buffer length
//              PBYTE pBufOut. Output buffer pointer
//              DWORD dwLenOut. Output buffer length
//              PWORD pdwAcutalOut. Actual output buffer length.
//
// Ret Value:   TRUE if success else FALSE. SetLastError() if FALSE.
//==============================================================================
BOOL WINAPI TchPdd_Ioctl(DWORD hPddContext,
      DWORD dwCode,
      PBYTE pBufIn,
      DWORD dwLenIn,
      PBYTE pBufOut,
      DWORD dwLenOut,
      PDWORD pdwActualOut
)
{
    DWORD dwResult = ERROR_INVALID_PARAMETER;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hPddContext);

    switch (dwCode)
    {
        //  Enable touch panel
        case IOCTL_TOUCH_ENABLE_TOUCHPANEL:
            DEBUGMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_TOUCH_ENABLE_TOUCHPANEL\r\n"));
            PDDTouchPanelEnable();
            dwResult = ERROR_SUCCESS;
            break;

        //  Disable touch panel
        case IOCTL_TOUCH_DISABLE_TOUCHPANEL:
            DEBUGMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_TOUCH_DISABLE_TOUCHPANEL\r\n"));
            PDDTouchPanelDisable();
            dwResult = ERROR_SUCCESS;
            break;


        //  Get current sample rate
        case IOCTL_TOUCH_GET_SAMPLE_RATE:
            DEBUGMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_TOUCH_GET_SAMPLE_RATE\r\n"));

            //  Check parameter validity
            if ((pBufOut != NULL) && (dwLenOut == sizeof(DWORD)))
            {
                if (pdwActualOut)
                    *pdwActualOut = sizeof(DWORD);

                //  Get the sample rate
                *((DWORD*)pBufOut) = s_TouchDevice.nSampleRate;
                dwResult = ERROR_SUCCESS;
            }
            break;

        //  Set the current sample rate
        case IOCTL_TOUCH_SET_SAMPLE_RATE:
            DEBUGMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_TOUCH_SET_SAMPLE_RATE\r\n"));

            //  Check parameter validity
            if ((pBufIn != NULL) && (dwLenIn == sizeof(DWORD)))
            {
                //  Set the sample rate
                s_TouchDevice.nSampleRate = *((DWORD*)pBufIn);
                dwResult = ERROR_SUCCESS;

            }
            break;

        //  Get touch properties
        case IOCTL_TOUCH_GET_TOUCH_PROPS:
            //  Check parameter validity
            if ((pBufOut != NULL) && (dwLenOut == sizeof(TCH_PROPS)))
            {
                if (pdwActualOut)
                    *pdwActualOut = sizeof(TCH_PROPS);\

                //  Fill out the touch driver properties
                ((TCH_PROPS*)pBufOut)->minSampleRate            = TOUCHPANEL_SAMPLE_RATE_LOW;
                ((TCH_PROPS*)pBufOut)->maxSampleRate            = TOUCHPANEL_SAMPLE_RATE_HIGH;
                ((TCH_PROPS*)pBufOut)->minCalCount              = 5;
                ((TCH_PROPS*)pBufOut)->maxSimultaneousSamples   = 1;
                ((TCH_PROPS*)pBufOut)->touchType                = TOUCHTYPE_SINGLETOUCH;
                ((TCH_PROPS*)pBufOut)->calHoldSteadyTime        = CAL_HOLD_STEADY_TIME;
                ((TCH_PROPS*)pBufOut)->calDeltaReset            = CAL_DELTA_RESET;
                ((TCH_PROPS*)pBufOut)->xRangeMin                = RANGE_MIN;
                ((TCH_PROPS*)pBufOut)->xRangeMax                = RANGE_MAX;
                ((TCH_PROPS*)pBufOut)->yRangeMin                = RANGE_MIN;
                ((TCH_PROPS*)pBufOut)->yRangeMax                = RANGE_MAX;

                dwResult = ERROR_SUCCESS;
            }
            break;

        //  Power management IOCTLs
        case IOCTL_POWER_CAPABILITIES:
            DEBUGMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_POWER_CAPABILITIES\r\n"));
            if (pBufOut != NULL && dwLenOut == sizeof(POWER_CAPABILITIES))
            {
                PPOWER_CAPABILITIES ppc = (PPOWER_CAPABILITIES) pBufOut;
                memset(ppc, 0, sizeof(*ppc));
                ppc->DeviceDx = DX_MASK(D0) | DX_MASK(D1) | DX_MASK(D2) | DX_MASK(D3) | DX_MASK(D4);

                if (pdwActualOut)
                    *pdwActualOut = sizeof(POWER_CAPABILITIES);

                dwResult = ERROR_SUCCESS;
            }
            break;

        case IOCTL_POWER_GET:
            DEBUGMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_POWER_GET\r\n"));
            if(pBufOut != NULL && dwLenOut == sizeof(CEDEVICE_POWER_STATE))
            {
                *(PCEDEVICE_POWER_STATE) pBufOut = (CEDEVICE_POWER_STATE) s_TouchDevice.dwPowerState;

                if (pdwActualOut)
                    *pdwActualOut = sizeof(CEDEVICE_POWER_STATE);

                dwResult = ERROR_SUCCESS;
            }
            break;

        case IOCTL_POWER_SET:
            DEBUGMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_POWER_SET\r\n"));
            if(pBufOut != NULL && dwLenOut == sizeof(CEDEVICE_POWER_STATE))
            {
                CEDEVICE_POWER_STATE dx = *(CEDEVICE_POWER_STATE*)pBufOut;
                if( VALID_DX(dx) )
                {
                    DEBUGMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_POWER_SET = to D%u\r\n", dx));
                    s_TouchDevice.dwPowerState = dx;

                    if (pdwActualOut)
                        *pdwActualOut = sizeof(CEDEVICE_POWER_STATE);

                    //  Enable touchscreen for D0-D2; otherwise disable
                    switch( dx )
                    {
                        case D0:
                        case D1:
                        case D2:
                            //  Enable touchscreen
                            PDDTouchPanelPowerHandler(FALSE);    //Enable Touch ADC
                            break;

                        case D3:
                        case D4:
                            //  Disable touchscreen
                            PDDTouchPanelPowerHandler(TRUE); //Disable Touch ADC
                            break;

                        default:
                            //  Ignore
                            break;
                    }

                    dwResult = ERROR_SUCCESS;
                }
            }
            break;

        default:
            dwResult = ERROR_NOT_SUPPORTED;
            break;
    }

    if (dwResult != ERROR_SUCCESS)
    {
        SetLastError(dwResult);
        return FALSE;
    }
    return TRUE;
}


//==============================================================================
// Function Name: TchPdd_PowerUp
//
// Description: MDD passes xxx_PowerUp stream interface call to PDD.
//
// Arguments:   [IN] hPddContext. pddcontext returned in TchPdd_Init
//
// Ret Value:   None
//==============================================================================
void WINAPI TchPdd_PowerUp(
    DWORD hPddContext
    )
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("TchPdd_PowerUp+\r\n")));
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hPddContext);
    DEBUGMSG(ZONE_FUNCTION, (TEXT("TchPdd_PowerUp-\r\n")));
}

//==============================================================================
// Function Name: TchPdd_PowerDown
//
// Description: MDD passes xxx_PowerDown stream interface call to PDD.
//
// Arguments:   [IN] hPddContext. pddcontext returned in TchPdd_Init
//
// Ret Value:   None
//==============================================================================
void WINAPI TchPdd_PowerDown(
    DWORD hPddContext
    )
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("TchPdd_PowerDown+\r\n")));
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hPddContext);
    DEBUGMSG(ZONE_FUNCTION, (TEXT("TchPdd_PowerDown-\r\n")));
}


//==============================================================================
//Internal Functions
//==============================================================================

//==============================================================================
// Function Name: PDDCalibrationThread
//
// Description: This function is called from the PDD Init to calibrate the screen.
//              If the calibration data is already present in the registry,
//              this step is skipped, else a call is made into the GWES for calibration.
//
// Arguments:   None.
//
// Ret Value:   Success(1), faliure(0)
//==============================================================================
static HRESULT PDDCalibrationThread()
{
    HKEY hKey;
    DWORD dwType;
    LONG lResult;
    HANDLE hAPIs;

    DEBUGMSG(ZONE_FUNCTION, (TEXT("CalibrationThread+\r\n")));

    // try to open [HKLM\hardware\devicemap\touch] key
    if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_LOCAL_MACHINE, RK_HARDWARE_DEVICEMAP_TOUCH, 0, KEY_ALL_ACCESS, &hKey))
    {
        DEBUGMSG(ZONE_CALIBRATE, (TEXT("CalibrationThread: calibration: Can't find [HKLM/%s]\r\n"), RK_HARDWARE_DEVICEMAP_TOUCH));
        return E_FAIL;
    }

    // check for calibration data (query the type of data only)
    lResult = RegQueryValueEx(hKey, RV_CALIBRATION_DATA, 0, &dwType, NULL, NULL);
    RegCloseKey(hKey);
    if (lResult == ERROR_SUCCESS)
    {
        // registry contains calibration data, return
        return S_OK;
    }

    hAPIs = OpenEvent(EVENT_ALL_ACCESS, FALSE, TEXT("SYSTEM/GweApiSetReady"));
    if (hAPIs)
    {
        WaitForSingleObject(hAPIs, INFINITE);
        CloseHandle(hAPIs);
    }

    // Perform calibration
    TouchCalibrate();

    // try to open [HKLM\hardware\devicemap\touch] key
    if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_LOCAL_MACHINE, RK_HARDWARE_DEVICEMAP_TOUCH, 0, KEY_ALL_ACCESS, &hKey))
    {
        DEBUGMSG(ZONE_CALIBRATE, (TEXT("CalibrationThread: calibration: Can't find [HKLM/%s]\r\n"), RK_HARDWARE_DEVICEMAP_TOUCH));
        return E_FAIL;
    }

    // display new calibration data
    lResult = RegQueryValueEx(hKey, RV_CALIBRATION_DATA, 0, &dwType, NULL, NULL);
    if (lResult == ERROR_SUCCESS)
    {
        TCHAR szCalibrationData[100];
        DWORD Size = sizeof(szCalibrationData);

        RegQueryValueEx(hKey, RV_CALIBRATION_DATA, 0, &dwType, (BYTE *) szCalibrationData, (DWORD *) &Size);
        DEBUGMSG(ZONE_CALIBRATE, (TEXT("touchp: calibration: new calibration data is \"%s\"\r\n"), szCalibrationData));
    }
    RegCloseKey(hKey);

    DEBUGMSG(ZONE_FUNCTION, (TEXT("CalibrationThread-\r\n")));

    return S_OK;
}

//==============================================================================
// Function Name: PDDStartCalibrationThread
//
// Description: This function is creates the calibration thread with
//              PDDCalibrationThread as entry.
//
// Arguments:   None.
//
// Ret Value:   None
//==============================================================================
void PDDStartCalibrationThread()
{
    HANDLE hThread;

    hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)PDDCalibrationThread, NULL, 0, NULL);
    // We don't need the handle, close it here
    CloseHandle(hThread);
}


//==============================================================================
// Function Name: PDDGetTouchIntPinState
//
// Description: This function is called from the IST to get the status of pen.
//
// Arguments:   None.
//
// Ret Value:   Status of the pen.
//==============================================================================
inline BOOL PDDGetTouchIntPinState()
{
    //  Return state of pen down
	UCHAR ucCommand = 0;
	UCHAR ucData[2];

	// read PSM bit in CFG0
	ucCommand = 0x80;		// MSB D7 is always set to 1
	I2CWrite(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,&ucCommand,sizeof(ucCommand));
	ucCommand = (CFG0_REG << 3)| COMMAND_ENABLE_READ;
	I2CWrite(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,&ucCommand,sizeof(ucCommand));
	I2CRead(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,ucData,2*sizeof(UCHAR));

	return ((ucData[0] & PINTS1) ==PINTS1 ? TRUE : FALSE);
}

//==============================================================================
// Function Name: PDDGetControllerData
//
// Description: This function is used to get the ADC data through I2C
//
// Arguments:   None.
//
// Ret Value:   Status of the pen.
//==============================================================================
void PDDGetControllerData(UINT32 control, USHORT* read_data)
{
	UCHAR *p = (UCHAR *) read_data;
	UCHAR ucCommand = 0;
	UCHAR ucTemp = 0;

	// select conversion mode
	ucCommand = (UCHAR)(control | 0x80);		// MSB D7 is always set to 1
	I2CWrite(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,&ucCommand,sizeof(ucCommand));

	// enable read after conversion
	ucCommand = COMMAND_ENABLE_READ;
	I2CWrite(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,&ucCommand,sizeof(ucCommand));

	// read X & Y values
	I2CRead(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,read_data,2*sizeof(USHORT));

	ucTemp = p[0];
	p[0] = p[1];
	p[1] = ucTemp;
	ucTemp = p[2];
	p[2] = p[3];
	p[3] = ucTemp;

	DEBUGMSG(ZONE_INFO, (TEXT("PDDGetControllerData: X %x Y %x \r\n"),read_data[0], read_data[1]));
}

//==============================================================================
// Function: PDDTouchPanelPowerHandler
//
// This function indicates to the driver that the system is entering
// or leaving the suspend state.
//
// Parameters:
//      bOff
//          [in] TRUE indicates that the system is turning off. FALSE
//          indicates that the system is turning on.
//
// Returns:
//      None.
//==============================================================================
void PDDTouchPanelPowerHandler(BOOL boff)
{
    DEBUGMSG(ZONE_FUNCTION, (_T("PDDTouchPanelPowerHandler+\r\n")));

    if (s_TouchDevice.dwSysIntr != SYSINTR_NOP)
        InterruptMask(s_TouchDevice.dwSysIntr, boff);

    DEBUGMSG(ZONE_FUNCTION, (_T("PDDTouchPanelPowerHandler-\r\n")));

    return;
}


//==============================================================================
// Function Name: PDDTouchPanelGetPoint
//
// Description: This function is used to read the touch coordinates from the h/w.
//
// Arguments:
//                  TOUCH_PANEL_SAMPLE_FLAGS * - Pointer to the sample flags. This flag is filled with the
//                                                                      values TouchSampleDownFlag or TouchSampleValidFlag;
//                  INT * - Pointer to the x coordinate of the sample.
//                  INT * - Pointer to the y coordinate of the sample.
//
// Ret Value:   None
//==============================================================================
void PDDTouchPanelGetPoint(
    TOUCH_PANEL_SAMPLE_FLAGS *pTipStateFlags,
    INT *pUncalX,
    INT *pUncalY
    )
{
    // MDD needs us to hold on to last valid sample and previous pen state.
    static ULONG usLastFilteredX    	= 0;	// This holds the previous X sample
    static ULONG usLastFilteredY    	= 0;	// This holds the previous Y sample
    static ULONG usSavedFilteredX  	= 0;	// This holds the last reported X sample
    static ULONG usSavedFilteredY  	= 0;	// This holds the last reported Y sample
    static BOOL bPrevReportedPenDown	= FALSE;
    static DWORD DelayedSampleCount		= 0;
	
    BOOL bActualPenDown         		= FALSE; // This indicates if the pen is actually down, whether we report or not
    UINT32 xpos = 0;
    UINT32 ypos = 0;

    DEBUGMSG(ZONE_FUNCTION&&ZONE_SAMPLES, (TEXT("PDDTouchPanelGetPoint+\r\n")));

    // By default, any sample returned will be ignored.
    *pTipStateFlags = TouchSampleIgnore;

    // Check if pen data are available If so, get the data.
	// Note that we always return data from the previous sample to avoid returning data nearest
	// the point where the pen is going up.  Data during light touches is not accurate and should
	// normally be rejected.  Without the ability to do pressure measurement, we are limited to 
	// rejecting points near the beginning and end of the pen down period.
    bActualPenDown  = PDDGetTouchIntPinState();
    if (bActualPenDown == TRUE)
    {
        PDDGetTouchData(&xpos, &ypos);


    }

    // Check if we have valid data to report to the MDD.
    if (bActualPenDown)
    {
    
        // Return the valid pen data.  Note that this data was actually obtained on the
		// previous sample
        *pUncalX = xpos;
        *pUncalY = ypos;
		
	// Save the data that we returned.  
	// This will also be returned on penup to help avoid returning data obtained during
	// a light press
	usSavedFilteredX = xpos;
	usSavedFilteredY = ypos;
		
        DEBUGMSG(ZONE_SAMPLES, ( TEXT( "X(0x%X) Y(0x%X)\r\n" ), *pUncalX, *pUncalY ) );
        
        // Store reported pen state.
        *pTipStateFlags = TouchSampleDownFlag | TouchSampleValidFlag;
    }
    else    // Otherwise, assume pen is up.
    {
 
        DEBUGMSG(ZONE_TIPSTATE, ( TEXT( "Pen Up!\r\n" ) ) );

        // Use the last valid sample. MDD needs an up with valid data.
        *pUncalX = usSavedFilteredX;
        *pUncalY = usSavedFilteredY;
        *pTipStateFlags = TouchSampleValidFlag;

        DEBUGMSG(ZONE_SAMPLES, ( TEXT( "Point: (%d,%d)\r\n" ), *pUncalX, *pUncalY ) );
    }
		
	// Set up interrupt/timer for next sample
	if (bActualPenDown)
	{
        // Pen down so set MDD timeout for polling mode.
        s_TouchDevice.dwSamplingTimeOut = 1000 / s_TouchDevice.nSampleRate;
		// Save point measured during this sample so it can be reported on the next sample
		usLastFilteredX = xpos;
		usLastFilteredY = ypos;
	}
	else
	{
		// Reset the delayed sample counter
		DelayedSampleCount = 0;
		usLastFilteredX = 0;
		usLastFilteredY = 0;
        
		// Pen up so set MDD timeout for interrupt mode.
        s_TouchDevice.dwSamplingTimeOut = INFINITE;

        // Set the proper state for the next interrupt.
        InterruptDone( s_TouchDevice.dwSysIntr );
    }

    DEBUGMSG(ZONE_FUNCTION&&ZONE_SAMPLES, (TEXT("PDDTouchPanelGetPoint-\r\n")));

    return;
}

//==============================================================================
// Function Name: PDDGetTouchData
//
// Description: Helper function to read 3 samples and find the average of them.
//
// Arguments:
//                  INT * - Pointer to the x coordinate of the sample.
//                  INT * - Pointer to the y coordinate of the sample.
//
// Ret Value:   TRUE
//==============================================================================
BOOL PDDGetTouchData(UINT32 * xpos,  UINT32 * ypos)
{
    USHORT tempdata[2];
    USHORT px, py;
    UINT32 pxAvg = 0, pyAvg = 0;
    int i;
    int iAvg = 0;

    DEBUGMSG(ZONE_FUNCTION, ( TEXT("PDDGetTouchData+\r\n" )) );

    do
    {
        for( i = 0; i < MAX_PTS; i++ )
        {
            PDDGetControllerData(COMMAND_CNV_SELECTXY, tempdata); 
            px = tempdata[0];
            py = tempdata[1];

            if (i == 0)
            {
                pxAvg = px;
                pyAvg = py;
                iAvg = 1;
            }
            else if  ((((pxAvg >= px)  &  ((UINT16)(px+DELTA) >= pxAvg)) | ((px > pxAvg)  &  ((UINT16)(pxAvg+DELTA) > px))) &
                          (((pyAvg >=py)  &  ((UINT16)(py+DELTA) >=pyAvg)) | ((py > pyAvg)  &  ((UINT16)(pyAvg+DELTA) > py))) )
            {
                pxAvg = (pxAvg +px)/2;
                pyAvg = (pyAvg +py)/2;
                iAvg++;
            }
            else
                break;
        }

    }while(iAvg < MAX_PTS);

    *xpos = pxAvg;
    *ypos = pyAvg;
    
    DEBUGMSG(ZONE_SAMPLES, (TEXT("PDDGetTouchData: i=%d, pxAvg=%d, pyAvg=%d\r\n"), i, pxAvg, pyAvg));

    DEBUGMSG(ZONE_FUNCTION, ( TEXT("PDDGetTouchData-\r\n" )) );

    return TRUE;
}


//==============================================================================
// Function Name: PDDTouchIST
//
// Description: This is the IST which waits on the touch event or Time out value.
//              Normally the IST waits on the touch event infinitely, but once the
//              pen down condition is recognized the time out interval is changed
//              to dwSamplingTimeOut.
//
// Arguments:
//                  PVOID - Reseved not currently used.
//
// Ret Value:   None
//==============================================================================
ULONG PDDTouchIST(PVOID   reserved)
{
    TOUCH_PANEL_SAMPLE_FLAGS    SampleFlags = 0;
    CETOUCHINPUT input;
    INT32                       RawX, RawY;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(reserved);

    DEBUGMSG(ZONE_TOUCH_IST, (L"PDDTouchIST: IST thread started\r\n"));

    //  Loop until told to stop
    while(!s_TouchDevice.bTerminateIST)
    {
        //  Wait for touch IRQ, timeout or terminate flag
       WaitForSingleObject(s_TouchDevice.hTouchPanelEvent, s_TouchDevice.dwSamplingTimeOut);

        //  Check for terminate flag
        if (s_TouchDevice.bTerminateIST)
            break;

        PDDTouchPanelGetPoint( &SampleFlags, &RawX, &RawY);

        if ( SampleFlags & TouchSampleIgnore )
        {
            // do nothing, not a valid sample
            continue;
        }

         //convert x-y coordination to one sample set before sending it to touch MDD.
         if(ConvertTouchPanelToTouchInput(SampleFlags, RawX, RawY, &input))
         {
               //send this 1 sample to mdd
               s_pfnMddReportSampleSet(s_mddContext, 1, &input);
         }

        DEBUGMSG(ZONE_TOUCH_SAMPLES, ( TEXT( "Sample:   (%d, %d)\r\n" ), RawX, RawY ) );

    }

    // IST thread terminating
    InterruptDone(s_TouchDevice.dwSysIntr);
    InterruptDisable(s_TouchDevice.dwSysIntr);

    CloseHandle(s_TouchDevice.hTouchPanelEvent);
    s_TouchDevice.hTouchPanelEvent = NULL;

    DEBUGMSG(ZONE_TOUCH_IST, (L"PDDTouchIST: IST thread ending\r\n"));

    return ERROR_SUCCESS;
}

//==============================================================================
// Function Name: PDDInitializeHardware
//
// Description: This routine configures the SPI channel and GPIO pin for interrupt mode.
//
// Arguments:  None
//
// Ret Value:   TRUE - Success
//                   FAIL - Failure
//==============================================================================
BOOL PDDInitializeHardware(LPCTSTR pszActiveKey)
{
    BOOL   rc = FALSE;
    UCHAR  ucCommand = 0;
    UCHAR  ucData[4];

    DEBUGMSG(ZONE_FUNCTION, (TEXT("PDDInitializeHardware+\r\n")));
	
    if (IsDVIMode())
        goto cleanup;

    // Read parameters from registry
    if (GetDeviceRegistryParams(
            pszActiveKey,
            &s_TouchDevice,
            dimof(s_deviceRegParams),
            s_deviceRegParams) != ERROR_SUCCESS)
    {
        DEBUGMSG(ZONE_ERROR, (TEXT("ERROR: PDDInitializeHardware: Error reading from Registry.\r\n")));
        goto cleanup;
    }
        DEBUGMSG(ZONE_ERROR, (TEXT("ERROR: PDDInitializeHardware: SampleRate=%x,	PenGPIO=%d, I2CBus=%d, I2CAddr=%d, I2CBaudrateIndex=%d\r\n"),
			s_TouchDevice.nSampleRate,
			s_TouchDevice.nPenGPIO,
			s_TouchDevice.nI2CBusIndex,
			s_TouchDevice.nI2CAddr,
			s_TouchDevice.nI2CBaudrateIndex));

    // Open GPIO driver
    s_TouchDevice.hGPIO = GPIOOpen();
    if (s_TouchDevice.hGPIO == NULL)
    {
        DEBUGMSG(ZONE_ERROR, (TEXT("ERROR: PDDInitializeHardware: Failed open GPIO device driver\r\n")));
        goto cleanup;
    }

    // Setup nPENIRQ for input mode, low detect
    GPIOSetMode(s_TouchDevice.hGPIO,s_TouchDevice.nPenGPIO,GPIO_DIR_INPUT | GPIO_INT_LOW);

    // Get the IRQ for the GPIO
    s_TouchDevice.nPenIRQ = GPIOGetSystemIrq(s_TouchDevice.hGPIO, s_TouchDevice.nPenGPIO);

    // Open I2C driver
    s_TouchDevice.hI2C = I2COpen(SOCGetI2CDeviceByBus(s_TouchDevice.nI2CBusIndex));
    I2CSetTimeout(s_TouchDevice.hI2C, 10);
    if (s_TouchDevice.hI2C == NULL) 
    {
        DEBUGMSG(ZONE_ERROR, (TEXT("ERROR: PddInitializeHardware: Failed open I2C device driver\r\n")));
        goto cleanup;
    }
    I2CSetBaudIndex(s_TouchDevice.hI2C,s_TouchDevice.nI2CBaudrateIndex);
    I2CSetSlaveAddress(s_TouchDevice.hI2C, (UINT16)s_TouchDevice.nI2CAddr);
    I2CSetSubAddressMode(s_TouchDevice.hI2C, I2C_SUBADDRESS_MODE_0);

    // setup PENINTDAV to be output only on Pen Touch
    // read CFG2 register
    ucCommand = 0x80;		// MSB D7 is always set to 1
    I2CWrite(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,&ucCommand,sizeof(ucCommand));
    ucCommand = (CFG2_REG << 3)| COMMAND_ENABLE_READ;
    I2CWrite(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,&ucCommand,sizeof(ucCommand));
    I2CRead(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,ucData,2*sizeof(UCHAR));
    
    // enable PINTS1
    ucData[2] = (ucData[1] |
    			MAV_FLTR_EN_X | 
    			MAV_FLTR_EN_Y);
    
    ucData[1] = (ucData[0] |
    			PINTS1 | 
    			PINTS0 |
    			MAV_M0M1_FLTR_SIZE_15 | 
    			MAV_W0W1_FLTR_SIZE_7_8);
    
    // write to CFG2 register
    ucCommand = 0x80;		// MSB D7 is always set to 1
    I2CWrite(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,&ucCommand,sizeof(ucCommand));
    
    // enable read after conversion
    ucData[0] = (CFG2_REG << 3)| COMMAND_ENABLE_WRITE;
    I2CWrite(s_TouchDevice.hI2C,I2C_SUBADDRESS_MODE_0,ucData,3*sizeof(UCHAR));
    
    Sleep(200);


    // Done
    rc = TRUE;

cleanup:

    DEBUGMSG(ZONE_FUNCTION, (TEXT("PDDInitializeHardware-\r\n")));
    if( rc == FALSE )
    {
        PDDDeinitializeHardware();
    }

    return rc;
}


//==============================================================================
// Function Name: PDDDeinitializeHardware
//
// Description: This routine Deinitializes the h/w by closing the SPI channel and GPIO pin
//
// Arguments:  None
//
// Ret Value:   None
//==============================================================================
VOID PDDDeinitializeHardware()
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("PDDDeinitializeHardware+\r\n")));

    // Close SPI device
    if (s_TouchDevice.hI2C != NULL)
    	{
    		I2CClose(s_TouchDevice.hI2C);
    		s_TouchDevice.hI2C = NULL;
    	}

    // Close GPIO device
    if (s_TouchDevice.hGPIO != NULL)
    	{
    		GPIOClose(s_TouchDevice.hGPIO);
    		s_TouchDevice.hGPIO = NULL;
    	}

    DEBUGMSG(ZONE_FUNCTION, (TEXT("PDDDeinitializeHardware-\r\n")));
}

//==============================================================================
// Function Name: PDDTouchPanelEnable
//
// Description: This routine creates the touch thread(if it is not already created)
//              initializes the interrupt and unmasks it.
//
// Arguments:  None
//
// Ret Value:   TRUE - Success
//==============================================================================
BOOL  PDDTouchPanelEnable()
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("PDDTouchPanelEnable+\r\n")));

    //  Check if already running
    if( s_TouchDevice.hTouchPanelEvent == NULL )
    {
        //  Clear terminate flag
        s_TouchDevice.bTerminateIST = FALSE;

        //  Create touch event
        s_TouchDevice.hTouchPanelEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
        if( !s_TouchDevice.hTouchPanelEvent )
        {
            DEBUGMSG(ZONE_ERROR,(TEXT("PDDTouchPanelEnable: Failed to initialize touch event.\r\n")));
            return FALSE;
        }

        //  Map SYSINTR to event
        if (!InterruptInitialize(s_TouchDevice.dwSysIntr, s_TouchDevice.hTouchPanelEvent, NULL, 0))
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("PDDTouchPanelEnable: Failed to initialize interrupt.\r\n")));
            return FALSE;
        }

        //  Create IST thread
       s_TouchDevice.hIST = CreateThread( NULL, 0, PDDTouchIST, 0, 0, NULL );
        if( s_TouchDevice.hIST == NULL )
        {
            DEBUGMSG(ZONE_ERROR, (TEXT("PDDTouchPanelEnable: Failed to create IST thread\r\n")));
            return FALSE;
        }

        // set IST thread priority
        CeSetThreadPriority ( s_TouchDevice.hIST, s_TouchDevice.dwISTPriority);


        if (s_TouchDevice.dwSysIntr != SYSINTR_NOP)
            InterruptMask(s_TouchDevice.dwSysIntr, FALSE);
    }

    DEBUGMSG(ZONE_FUNCTION, (TEXT("PDDTouchPanelEnable-\r\n")));
    return TRUE;
}

//==============================================================================
// Function Name: PDDTouchPanelDisable
//
// Description: This routine closes the IST and releases other resources.
//
// Arguments:  None
//
// Ret Value:   TRUE - Success
//==============================================================================
VOID  PDDTouchPanelDisable()
{
    DEBUGMSG(ZONE_FUNCTION, (TEXT("PDDTouchPanelDisable+\r\n")));

    //  Disable touch interrupt service thread
    if( s_TouchDevice.hTouchPanelEvent )
    {
        if (s_TouchDevice.dwSysIntr != SYSINTR_NOP)
            InterruptMask(s_TouchDevice.dwSysIntr, TRUE);

        s_TouchDevice.bTerminateIST = TRUE;
        SetEvent( s_TouchDevice.hTouchPanelEvent );
        s_TouchDevice.hTouchPanelEvent = 0;
    }

    //Closing IST handle
    if(s_TouchDevice.hIST)
    {
        CloseHandle(s_TouchDevice.hIST);
        s_TouchDevice.hIST=NULL;
     }

    DEBUGMSG(ZONE_FUNCTION, (TEXT("PDDTouchPanelDisable-\r\n")));
}

