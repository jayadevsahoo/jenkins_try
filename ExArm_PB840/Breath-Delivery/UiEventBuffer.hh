#ifndef UiEventBuffer_HH
#define UiEventBuffer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: UiEventBuffer - circular buffer for user interface events.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/UiEventBuffer.hhv   10.7   08/17/07 09:45:00   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  iv    Date:  01-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version - per Breath Delivery formal code review 
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "EventData.hh"

//@ Usage-Classes

//@ End-Usage

//@ Constant: UE_BUF_SIZE
// Size of the buffer for user events
static const Int32 UE_BUF_SIZE = 50;

extern const Int32 MAX_NUM_MSGS; 

class UiEventBuffer {
  public:
    UiEventBuffer(void);
    ~UiEventBuffer(void);

    static void SoftFault(const SoftFaultID softFaultID,
  const Uint32      lineNumber,
  const char*       pFileName  = NULL, 
  const char*       pPredicate = NULL);

    void addMsg(const EventData eventData);
    Boolean readCurrentMsg(EventData& eventData);
    inline void reset(void);

  protected:

  private:
    UiEventBuffer(const UiEventBuffer&);  // not implemented...
    void   operator=(const UiEventBuffer&); // not implemented...
	
    Int32 numMsgs_(void) const;
    inline void incrementIndex_(Int32& index);
    inline Boolean isEmpty_(void) const;
    
    //@ Data-Member:  userEventMsg_
    // storage for messages received from the user 
    EventData userEventMsg_[UE_BUF_SIZE];

    //@ Data-Member:  writeIndex_
    // used as an index to write an entry to the buffer 
    Int32 writeIndex_;

    //@ Data-Member:  readIndex_
    // used as an index to read an entry from the buffer 
    Int32 readIndex_;

};

// Inlined methods...
#include "UiEventBuffer.in"


#endif // UiEventBuffer_HH 
