//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file UILogger.cpp
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "stdafx.h"
#include "UILogger.h"
#include "SWAT.h"

#include < stdio.h    >
#include < winsock2.h >
#include < time.h     >



struct timespec 
{
    time_t tv_sec;  /* Seconds since 00:00:00 GMT, */
                    /* 1 January 1970 */

    long tv_nsec;   /* Additional nanoseconds since */
                    /* tv_sec */
}; 


#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif


//-------------------------------------
// C O D E
//-------------------------------------


//---------------------------
// gettimeofday
//---------------------------
static int gettimeofday(struct timeval *tv, struct timezone *tz)
{
  FILETIME ft;
  unsigned __int64 tmpres = 0;
  static int tzflag;
 
  if (NULL != tv)
  {
    GetSystemTimeAsFileTime(&ft);
 
    tmpres |= ft.dwHighDateTime;
    tmpres <<= 32;
    tmpres |= ft.dwLowDateTime;
 
    /*converting file time to unix epoch*/
    tmpres /= 10;  /*convert into microseconds*/
    tmpres -= DELTA_EPOCH_IN_MICROSECS; 
    tv->tv_sec = (long)(tmpres / 1000000UL);
    tv->tv_usec = (long)(tmpres % 1000000UL);
  }
  return 0;
}




//---------------------------
// UILogger
//---------------------------
UILogger::UILogger():
bLM_DEBUG(true),
bLM_INFO(true),
bLM_TRACE(true),
bLM_NOTICE(true),
bLM_WARNING(true),
bLM_ERROR(true),
bLM_CRITICAL(true),
bLM_ALERT(true),
bLM_GENERAL(true)
{}


//---------------------------
// ~UILogger
//---------------------------
UILogger::~UILogger(){}


void UILogger::disable(){
    bLM_DEBUG    =false;
    bLM_INFO     =false;
    bLM_TRACE    =false;
    bLM_NOTICE   =false;
    bLM_WARNING  =false;
    bLM_ERROR    =false;
    bLM_CRITICAL =false;
    bLM_ALERT    =false;
    bLM_GENERAL  =false;
}

void UILogger::enable(){
    bLM_DEBUG    =true;
    bLM_INFO     =true;
    bLM_TRACE    =true;
    bLM_NOTICE   =true;
    bLM_WARNING  =true;
    bLM_ERROR    =true;
    bLM_CRITICAL =true;
    bLM_ALERT    =true;
    bLM_GENERAL  =true;
}

void UILogger::disable(int logtype){
    
    switch(logtype)
    {
        case LM_DEBUG:  
            bLM_DEBUG=false;
            break; 
        
        case LM_INFO:
            bLM_INFO=false;
            break; 
        
        case LM_TRACE:
            bLM_TRACE=false;
            break; 
        
        case LM_NOTICE:
            bLM_NOTICE=false;
            break; 

        case LM_WARNING:
            bLM_WARNING=false;
            break; 

        case LM_ERROR:
            bLM_ERROR=false;
            break; 

        case LM_CRITICAL:
            bLM_CRITICAL=false;
            break; 

        case LM_ALERT:
            bLM_ALERT=false;
            break; 


        case LM_GENERAL:
            bLM_GENERAL=false;
            break;
    }


}
void UILogger::enable(int logtype)
{
    switch(logtype)
    {
        case LM_DEBUG:  
            bLM_DEBUG=true;
            break; 
        
        case LM_INFO:
            bLM_INFO=true;
            break; 
        
        case LM_TRACE:
            bLM_TRACE=true;
            break; 
        
        case LM_NOTICE:
            bLM_NOTICE=true;
            break; 

        case LM_WARNING:
            bLM_WARNING=true;
            break; 

        case LM_ERROR:
            bLM_ERROR=true;
            break; 

        case LM_CRITICAL:
            bLM_CRITICAL=true;
            break; 

        case LM_ALERT:
            bLM_ALERT=true;
            break; 


        case LM_GENERAL:
            bLM_GENERAL=true;
            break;
    }



}

bool UILogger::is_enabled(int logtype){
    switch(logtype)
    {
        case LM_DEBUG   :return bLM_DEBUG;
        case LM_INFO    :return bLM_INFO;
        case LM_TRACE   :return bLM_TRACE;
        case LM_NOTICE  :return bLM_NOTICE;
        case LM_WARNING :return bLM_WARNING;
        case LM_ERROR   :return bLM_ERROR;
        case LM_CRITICAL:return bLM_CRITICAL;
        case LM_ALERT   :return bLM_ALERT;
        case LM_GENERAL :return bLM_GENERAL;
    }

    return false;

}


//---------------------------
// logMessage
//---------------------------
void UILogger::logMessage(unsigned int line, int logtype, char *filename,
            char *str, ...){

    struct timeval tv;
    struct tm *tm;
    va_list args;
    time_t now;
    
    char log_date[300];
    char *tagname;

	//stop logging if error occured or user clicked stop
	//if(((CSwatApp*)AfxGetApp())->m_bRun == 0) return;

   switch(logtype)
    {
        case LM_DEBUG:  
            
            if(bLM_DEBUG==false)
            {
                return;
            }
            tagname="LM_DEBUG";
            
            break; 



        case LM_INFO:

            if(bLM_INFO==false)
            {
                return;
            }
            tagname="LM_INFO";
            
            break; 



        case LM_TRACE:

            if(bLM_TRACE==false)
            {
                return;
            }
            tagname="LM_TRACE";
            
            break; 



        case LM_NOTICE:
            if(bLM_NOTICE==false)
            {
                return;
            }
            tagname="LM_NOTICE";
            
            break; 




        case LM_WARNING:
            if(bLM_WARNING==false)
            {
                return;
            }
            tagname="LM_WARNING";
            
            break; 



        case LM_ERROR:
            if(bLM_ERROR==false)
            {
                return;
            }
			tagname="LM_ERROR";
			//error occured, stop the script
			((CSwatApp*)AfxGetApp())->m_bRun = 0;
            
            break; 


        case LM_CRITICAL:
            if(bLM_CRITICAL==false)
            {
                return;
            }
            tagname="LM_CRITICAL";
            
            break; 



        case LM_ALERT:
            if(bLM_ALERT==false)
            {
                return;
            }
            tagname="LM_ALERT";
            
            break; 



        default:
            if(bLM_GENERAL==false)
            {
                return;
            }
            tagname="LM_GENERAL";
            
    }


    gettimeofday(&tv, NULL);

    now = tv.tv_sec;
    tm = localtime(&now);

/*    sprintf(log_date, "%04d.%02d.%02d@%02d:%02d:%02d "
            , tm->tm_year + 1900
            , tm->tm_mon + 1
            , tm->tm_mday
            , tm->tm_hour
            , tm->tm_min
            , tm->tm_sec);
*/
	sprintf(log_date, "%02d:%02d:%02d "
            , tm->tm_hour
            , tm->tm_min
            , tm->tm_sec);

 
    char buffer[4000];
    va_start(args, str);
    vsnprintf(buffer, sizeof(buffer)-1, str , args);
    va_end(args);

    //Print out data
    //printf("(UI)%s %s %s@%i: %s\n",log_date,tagname,filename,line,buffer);

	CString strLog;

	CString strFile(filename);
	strFile = strFile.Right(strFile.GetLength() - strFile.ReverseFind('\\')-1);

//	strLog.Format("%s, %s@%i, %s ,%s\n",tagname,strFile,line,buffer, log_date);

	strLog.Format("%s, %s %s@%i, %s\n",log_date, tagname, strFile, line, buffer);

	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	EnterCriticalSection(&pApp->m_CriticalSection);

	pApp->RefreshSWATLogs(strLog);

	LeaveCriticalSection(&pApp->m_CriticalSection);




}

