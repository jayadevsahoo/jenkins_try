#ifndef MsgTimer_HH
#define MsgTimer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: MsgTimer - General purpose timer class that tasks use to
// get notification of a future time event.  There are two types of
// time events supported -- one-shot events and periodic events.
// When the timer expires, the given message is posted to the specified
// task queue.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/MsgTimer.hhv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code. Removed include of MsgTimer.in since
//		there are no more inlined methods.
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//====================================================================

#include "Sigma.hh"
#include "OsFoundation.hh"

//@ Usage-Classes
//@ End-Usage

class MsgTimer
{
  public:

	//@ Type: EventType
	// EventType defines a type of time event.
	// ONE_SHOT a one time event.
	// PERIODIC a cyclical event.
	enum EventType
	{
		ONE_SHOT,
		PERIODIC
	};

	//@ Constant: MIN_MSEC_REQUEST
	// minimum request time
	enum {MIN_MSEC_REQUEST = 100};	

	MsgTimer(const Int32 qId, const Int32 msg, const Int32 msec,
				MsgTimer::EventType eventType = ONE_SHOT);

	~MsgTimer(void);

	void set(void);
	void set(const Int32 msec);
	void cancel(void);
	Boolean isSet(void) const;

	Boolean operator==(const MsgTimer&) const;
	Boolean operator!=(const MsgTimer&) const;

	static void SoftFault(const SoftFaultID softFaultID,
			      const Uint32      lineNumber,
			      const char*       pFileName  = NULL, 
			      const char*       pPredicate = NULL);

  protected:

  private:
	// these methods are purposely declared, but not implemented...
	MsgTimer();				// not implemented...
	MsgTimer(const MsgTimer&);		// not implemented...
	void operator=(const MsgTimer&);	// not implemented...

	//@ Data-Member: timerId_
	// Id into Timer Table. 
	Int32 timerId_;

	//@ Data-Member: qId_
	// Destination queue for the timer message.
	Int32 qId_;

	//@ Data-Member: msg_
	// User-defined, 32-bit message placed into the destination queue.
	Int32 msg_;

	//@ Data-Member: msec_
	// Millisecond interval for this timer.
	Int32 msec_;

	//@ Data-Member: eventType_
	// User-defined event type for this timer.
	MsgTimer::EventType eventType_;

};

#endif // MsgTimer_HH 
