#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SstTestSubScreen - The subscreen is activated after the user selects
// PROCEED in the SstSetupSubScreen and presses the Accept key.
//---------------------------------------------------------------------
//@ Interface-Description
// This subscreen displays SST test information (status and result data) while
// SST is running, and provides a means of conveying operator prompts from 
// the Service-Mode subsystem to the operator.
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// buttonDownHappened() and buttonUpHappened() are called when any button
// in the subscreen is pressed.  Timer events  are passed into timerEventHappened().
// Adjust Panel events are communicated via the various Adjust Panel
// "happened" methods.  NovRam events are passed into novRamUpdateEventHappened().
// Register all test status, data and prompt to Service-Data subsystem and process
// these data by various GuiTestManager methods.
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the SstTestSubScreen in
// a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
// The processing of the SstTestSubscreen is based on a pre-defined internal
// state table.  All the possible state and events are defined in the header
// of this file.  Upon the activation of this subscreen, we set the state to
// INITIAL_STATE, and reset all the variables to the initial values.  The
// subsequent event can happen either by external input (timer event, keyboard
// and knob input, button input, test control input, etc) or internal control
// (auto event).
// We drop into the next state wenever a new event occures.  Appropriated
// processing (screen update, control of test flow, etc) will be executed to
// maintain the requirements for this state transition process.  
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstTestSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:32   pvcs  $
//
//@ Modification-Log  
//
//  Revision: 025   By: rhj   Date: 09-June-2010    SCR Number: 6599
//  Project:  PROX
//  Description:
//	    Cleared the primary, advisory and secondary prompts when 
//      complete and repeat buttons are pressed down.
// 
//  Revision: 024   By: rhj   Date: 09-June-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//	    Fixed a bug where SST can skip the SST PROX Test unexpectedly.
// 
//  Revision: 023   By: rhj   Date: 09-June-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//		Removed nonsensical prompt in SST after touching COMPLETE 
//	
//  Revision: 022   By: mnr   Date: 04-May-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//		Added PX_REMOVE_INSP_FILTER_PROMPT Prompt.	
//	
//  Revision: 021   By: mnr   Date: 13-APR-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Activate and deactivate PROX SST subscreen properly and other updates.
//
//  Revision: 020   By: mnr   Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      PROX SST related updates.
// 
//  Revision: 019   By: gdc   Date:  03-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Refined text positioning as part of automated text positioning.
//
//  Revision: 018   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//    
//  Revision: 017   By: sah   Date:  31-Oct-2000    DR Number: 5780
//  Project:  VTPC
//  Description:
//      Added use of 'SST_NEO_FILTER_TEST_DATA_LABEL' with a NEONATAL
//      run of SST.
//
//  Revision: 016  By:  hhd	   Date:  01-June-1999    DCS Number: 5385
//  Project:  ATC
//  Description:
//	Moved Date/Time stamp format into *MiscStrs.LA file because the X-pos
//	is language dependent.
//
//  Revision: 015  By:  dosman	   Date:  12-May-1999    DCS Number: 5389
//  Project:  ATC
//  Description:
//	Removed unused code in adjustPanelClearPressHappened
//
//  Revision: 014  By:  dosman	   Date:  27-Jan-1999    DCS Number: 5322 
//  Project:  ATC
//  Description:
//	Initial version.
//	Added prompt in SST to ask if humidifier is wet.
//	Also added prompt to allow user to answer a yes/no question:
//	yes (with ACCEPT) and no (with CLEAR).
//
//  Revision: 013  By:  yyy	   Date:  29-Sep-1998    DCS Number: 5137
//  Project:  BiLevel
//  Description:
//		Display appropriate prompts in between state transition.
//
//  Revision: 012  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 011  By:  clw    Date:  19-May-98    DR Number:
//  Project:  Sigma (R8027)
//  Description:
//      Added japanese-only code to implement different date format for Japanese.
//
//  Revision: 010  By:  yyy    Date: 10-Dec-1997    DR Number: DCS 2673
//  Project:  Sigma (840)
//  Description:
//		Reset overrideRequested_ flag when EXIT action is requested.
//
//  Revision: 009  By:  yyy    Date: 16-Oct-1997    DR Number: DCS 2565
//  Project:  Sigma (840)
//  Description:
//		Mapped new SRS.
//
//  Revision: 008  By:  yyy    Date: 08-Oct-1997    DR Number: DCS 2326, 2443
//  Project:  Sigma (840)
//  Description:
//		Added PromptId CONNECT_HUMIDIFIER_PROMPT.
//
//  Revision: 013  By: gdc      Date: 23-Sep-1997  DR Number: 2513
//    Project:  Sigma (R8027)
//    Description:
//     Cleaned up Service-Mode interfaces to support remote test.
//
//  Revision: 012  By:  yyy    Date:  08-Sep-97    DR Number: 2439
//    Project:  Sigma (R8027)
//    Description:
//      Changed ML_CM_H2O_LABEL to ML_LABEL. Added pressure unit to
//		display the correct pressure unit.
//
//  Revision: 011  By:  yyy    Date:  28-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Recover revision 7.
//
//  Revision: 010  By:  yyy    Date:  28-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      undo revision 7 per "7C" release.
//
//  Revision: 009  By:  yyy    Date:  20-Aug-97    DR Number: 1988
//    Project:  Sigma (R8027)
//    Description:
//      Added handles for different pressure unit display.
//
//  Revision: 008  By:  yyy    Date:  13-AUG-1997    DCS Number: 2365
//  Project:  Sigma (R8027)
//  Description:
//      Use the appropriated ResultEntryData method to query the test running
//		time.
//
//  Revision: 007  By:  yyy    Date:  11-AUG-1997    DCS Number: 1803
//  Project:  Sigma (R8027)
//  Description:
//      Removed NovRamEventRegistrar::RegisterTarget() call from activate()
//		to constructor.
//
//  Revision: 006  By: yyy      Date: 30-Jul-1997  DR Number: 2066
//    Project:  Sigma (R8027)
//    Description:
//      Removed the call to NovRamManager::UpdateLastSstPatientCctType().
//		Service Mode manager is handling it now.
//
//  Revision: 005  By: yyy      Date: 29-Jul-1997  DR Number: 2271
//    Project:  Sigma (R8027)
//    Description:
//      Disabled all the keys when exiting command is requested.
//
//  Revision: 004  By:  yyy    Date:  22-JUNE-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      22-JUNE-97  Renamed sstStatus_ to sstStatusStr_ to identify it as a string
//		rather than a status field for translation.
//		22-Jul-97 Changed the column size to fix the German translation.
//		25-Jul-97 Added TEST_GUI_MISC for qualifying the DoTest() call.
//
//  Revision: 003  By: yyy      Date: 20-May-1997  DR Number: 2129
//    Project:  Sigma (R8027)
//    Description:
//      After the SST exiting command is issued, disable all the buttons
//		that could trigger the exiting processing.
//
//  Revision: 002  By: yyy      Date: 07-May-1997  DR Number: 2059
//    Project:  Sigma (R8027)
//    Description:
//      In execute_state_() when executing the EXIT_FALLBACK_STATE, if the
//		current state is already in DO_TEST_STATE then do nothing.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SstTestSubScreen.hh"

#include "ServiceLowerScreen.hh"
#include "ServiceLowerScreenSelectArea.hh"
#include "ServiceUpperScreen.hh"
#include "PatientCctTypeValue.hh"
#include "SmManager.hh"

#include "MiscStrs.hh"
#include "GuiEventRegistrar.hh"

//@ Usage-Classes
#include "AdjustPanel.hh"
#include "GuiTimerId.hh"
#include "GuiTimerRegistrar.hh"
#include "LowerScreen.hh"
#include "PromptArea.hh"
#include "ScrollableLog.hh"
#include "SubScreenArea.hh"
#include "TextUtil.hh"
#include "ResultEntryData.hh"
#include "TestResultId.hh"
#include "NovRamEventRegistrar.hh"
#include "DiagnosticFault.hh"
#include "SettingContextHandle.hh"
#include "SstLeakSubScreen.hh"
#include "SstProxSubScreen.hh"
#include "DiagnosticCode.hh"
#include "ExitServiceModeSubScreen.hh"
#include "SettingContextHandle.hh"
#include "ServiceStatusArea.hh"
#include "PatientCctTypeValue.hh"
//@ End-Usage

//@ Code...


// Initialize static constants.
static const Uint16 BUTTON_Y_ = 362;
static const Uint16 SMALL_BUTTON_WIDTH_ = 84;
static const Uint16 BIG_BUTTON_WIDTH_ = 110;
static const Uint16 BUTTON_HEIGHT_ = 34;
static const Uint8  BUTTON_BORDER_ = 3;
static const Uint16 EXIT_BUTTON_X_ = 5;
static const Uint16 REPEAT_BUTTON_X_ = 548;
static const Uint16 NEXT_BUTTON_X_ = 445;
static const Uint16 RESTART_BUTTON_X_ = 110;
static const Uint16 OVERRIDE_BUTTON_X_ = 520;
static const Int32 SST_STATUS_LABEL_X_ = 180;
static const Int32 SST_STATUS_Y_ = 3;

static const Int32 SST_COMPLETION_LABEL_X_ = 150;
static const Int32 SST_COMPLETION_VALUE_X_ = 245;
static const Int32 SST_OUTCOME_LABEL_X_ = 425;
static const Int32 SST_OUTCOME_VALUE_X_ = 540;

static const Int16 LOG_X_ = 2;
static const Int16 LOG_Y_ = 56;
static const Int16 SST_ROW_HEIGHT_ = 38;
static const Int16 SST_COL_1_WIDTH_ = 55;
static const Int16 SST_COL_2_WIDTH_ = 113;
static const Int16 SST_COL_3_WIDTH_ = 371;
static const Int16 SST_COL_4_WIDTH_ = 90;

static const Int32 SST_SUB_SCREEN_DIVIDE_LINE_START_X_ = 0;
static const Int32 SST_SUB_SCREEN_DIVIDE_LINE_END_X_ =
							SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH +
							SST_SUB_SCREEN_DIVIDE_LINE_START_X_ - 1;
static const Int32 SST_SUB_SCREEN_DIVIDE_LINE_Y_ = 353;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SstTestSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the SstTestSubscreen.  Passed a pointer to the subscreen
// area which creates it (Service's LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various buttons displayed in the subscreen and adds them
// to the container.  Registers for callbacks from each button.  Registers
// for NovRam event callbacks.  Do necessary setups for buttons on the
// subscreen display.  Register for timer event callbacks.  Setup the test
// name, prompt and status tables.
// Most importantly, setup the state transition table.
// $[01288] During SST, a message shall be displayed in the Title ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstTestSubScreen::SstTestSubScreen(SubScreenArea *pSubScreenArea) :
		SubScreen(pSubScreenArea),
		titleArea_(MiscStrs::SST_STATUS_SUBSCREEN_TITLE),
		exitButton_(EXIT_BUTTON_X_,		 	BUTTON_Y_,
				SMALL_BUTTON_WIDTH_,		BUTTON_HEIGHT_,
				Button::DARK, 				BUTTON_BORDER_,
				Button::SQUARE,				Button::NO_BORDER,
				MiscStrs::SST_EXIT_MSG),
		repeatButton_(REPEAT_BUTTON_X_,	 	BUTTON_Y_,
				SMALL_BUTTON_WIDTH_,		BUTTON_HEIGHT_,
				Button::DARK, 				BUTTON_BORDER_,
				Button::SQUARE,				Button::NO_BORDER,
				MiscStrs::SST_REPEAT_MSG),
		nextButton_(NEXT_BUTTON_X_, 		BUTTON_Y_,
				SMALL_BUTTON_WIDTH_,		BUTTON_HEIGHT_,
				Button::DARK, 				BUTTON_BORDER_,
				Button::SQUARE,				Button::NO_BORDER,
				MiscStrs::SST_NEXT_MSG),
		overrideButton_(OVERRIDE_BUTTON_X_,	BUTTON_Y_,
				BIG_BUTTON_WIDTH_,			BUTTON_HEIGHT_,
				Button::DARK, 				BUTTON_BORDER_,
				Button::SQUARE,				Button::NO_BORDER,
				MiscStrs::SST_OVERRIDE_MSG),
	 	restartButton_(RESTART_BUTTON_X_,	BUTTON_Y_,
				BIG_BUTTON_WIDTH_,			BUTTON_HEIGHT_,
				Button::DARK, 				BUTTON_BORDER_,
				Button::SQUARE,				Button::NO_BORDER,
				MiscStrs::SST_RESTART_SST_MSG),
		sstSubScreenDivideLine_(SST_SUB_SCREEN_DIVIDE_LINE_START_X_,
				SST_SUB_SCREEN_DIVIDE_LINE_Y_,
				SST_SUB_SCREEN_DIVIDE_LINE_END_X_,
				SST_SUB_SCREEN_DIVIDE_LINE_Y_),
		pCurrentButton_(NULL),
		sstStatusStr_(NULL_STRING_ID),
		sstCompletedLabel_(MiscStrs::SST_COMPLETED_LABEL),
		sstOutcomeLabel_(MiscStrs::SST_OUTCOME_LABAEL),
		sstCompletedValue_(NULL_STRING_ID),
		sstOutcomeValue_(NULL_STRING_ID),
		pLog_(NULL),
		pSstLeakSubScreen_(NULL),
		pSstProxSubScreen_(NULL),
		pUpperSubScreenArea_(NULL),
		testManager_(this),
		sstCurrentTestIndex_(SmTestId::SST_NO_TEST_ID),
		maxServiceModeTest_(0),
		clearLogEntry_(FALSE),
		promptId_(SmPromptId::NULL_PROMPT_ID),
		keyAllowedId_(SmPromptId::NULL_KEYS_ALLOWED_ID),
		conditionId_(-1),
		userKeyPressedId_(SmPromptId::NULL_ACTION_ID),
		isThisSstTestCompleted_(FALSE),
		mostSevereStatusId_(PASSED_TEST_ID),
		overallStatusId_(PASSED_TEST_ID),
		previousState_(INITIAL_STATE),
		nextState_(INITIAL_STATE),
		fallBackState_(INITIAL_STATE),
		overrideRequested_(FALSE),
		repeatTestRequested_(FALSE),
		RecursionCount_(0),
		newEvent_(-1),
		unitId_(GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
								MiscStrs::TITLE_CMH20_UNIT :
								MiscStrs::TITLE_HPA_UNIT)	// $[TI1]	$[TI2]
{
	CALL_TRACE("SstTestSubScreen::SstTestSubScreen(SubScreenArea "
													"*pSubScreenArea)");
	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	// Get the SST Leak subscreen address.
	pSstLeakSubScreen_ = UpperSubScreenArea::GetSstLeakSubScreen();

	// Get the SST PROX subscreen address.
	pSstProxSubScreen_ = UpperSubScreenArea::GetSstProxSubScreen();

	// Get the ServiceUpperSubScreen area address.
	pUpperSubScreenArea_ = ServiceUpperScreen::RServiceUpperScreen.
							getUpperSubScreenArea();


	// Add the title area
	addDrawable(&titleArea_);

	exitButton_.setButtonCallback(this);
	repeatButton_.setButtonCallback(this);
	nextButton_.setButtonCallback(this);
	overrideButton_.setButtonCallback(this);
	restartButton_.setButtonCallback(this);
	
	// Setup menu stuff
	sstStatusStr_.setX(SST_STATUS_LABEL_X_);
	sstStatusStr_.setY(SST_STATUS_Y_);
	sstCompletedLabel_.setX(SST_COMPLETION_LABEL_X_);
	sstCompletedLabel_.setY(SST_STATUS_Y_);
	sstOutcomeLabel_.setX(SST_OUTCOME_LABEL_X_);
	sstOutcomeLabel_.setY(SST_STATUS_Y_);
	sstCompletedValue_.setX(SST_COMPLETION_VALUE_X_);
	sstCompletedValue_.setY(SST_STATUS_Y_);
	sstOutcomeValue_.setX(SST_OUTCOME_VALUE_X_);
	sstOutcomeValue_.setY(SST_STATUS_Y_);
	sstStatusStr_.setColor(Colors::WHITE);
	sstCompletedLabel_.setColor(Colors::WHITE);
	sstOutcomeLabel_.setColor(Colors::WHITE);
	sstCompletedValue_.setColor(Colors::WHITE);
	sstOutcomeValue_.setColor(Colors::WHITE);

	// Hide the following drawables
	repeatButton_.setShow(FALSE);
	nextButton_.setShow(FALSE);
	overrideButton_.setShow(FALSE);
	restartButton_.setShow(FALSE);
	sstStatusStr_.setShow(FALSE);
	sstCompletedLabel_.setShow(FALSE);
	sstOutcomeLabel_.setShow(FALSE);
	sstCompletedValue_.setShow(FALSE);
	sstOutcomeValue_.setShow(FALSE);
	
	// Add the following drawables
	addDrawable(&exitButton_);
	addDrawable(&repeatButton_);
	addDrawable(&nextButton_);
	addDrawable(&overrideButton_);
	addDrawable(&restartButton_);
	addDrawable(&sstStatusStr_);
	addDrawable(&sstCompletedLabel_);
	addDrawable(&sstOutcomeLabel_);
	addDrawable(&sstCompletedValue_);
	addDrawable(&sstOutcomeValue_);

	//
	// Register for timer event callbacks.
	//
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT, this);

	//
	// Register to the NovRam Register 
	// We have to do registration at activation time to guarantee this
	// subscreen will get the callback notice.
	//
	NovRamEventRegistrar::RegisterTarget(NovRamUpdateManager::SST_RESULT_TABLE_UPDATE,
							   this);
	NovRamEventRegistrar::RegisterTarget(NovRamUpdateManager::SST_LOG_UPDATE,
							   this);

	// Associate the testResults object with the pointer to the test result
	// array.
	testManager_.setupTestCommandArray();

	// Associate the testResults object with the pointer to the test result
	// array.
	testManager_.setupTestResultDataArray();

	// Setup the test logs.
	setSstTestNameTable_();
	setSstTestPromptTable_();
	setSstTestStatusTable_();
		
	// Initialize the sstStateTable
	for (Int state = 0; state < SST_STATE_NUM; state++)
	{
		for (Int event = 0; event < SST_EVENT_NUM; event++)
		{
			sstStateTable_[state][event] = UNDEFINED_NEXT_TEST;
		}
	}
	
	// Build up the sstStateTable
	sstStateTable_[INITIAL_STATE]			[AUTO_EVENT] 				= READY_TO_TEST_STATE;
	sstStateTable_[READY_TO_TEST_STATE]		[AUTO_EVENT] 				= DO_TEST_STATE;

	// $[01282] The operator is allowed at any time during testing to ...
	sstStateTable_[DO_TEST_STATE]			[EXIT_EVENT]			 	= PAUSE_REQUEST_STATE;
	sstStateTable_[DO_TEST_STATE]			[USER_PROMPT_EVENT] 		= PROMPT_STATE;
	sstStateTable_[DO_TEST_STATE]			[END_ALERT_EVENT] 			= END_ALERT_STATE;
	sstStateTable_[DO_TEST_STATE]			[END_FAILURE_EVENT] 		= END_FAILURE_STATE;
	sstStateTable_[DO_TEST_STATE]			[USER_END_EVENT] 			= TEST_PASS_STATE;

	sstStateTable_[PROMPT_STATE]			[ACCEPT_EVENT] 				= EXECUTE_PROMPT_STATE;
	sstStateTable_[PROMPT_STATE]			[CLEAR_EVENT] 				= EXECUTE_PROMPT_STATE;
	sstStateTable_[PROMPT_STATE]			[EXIT_EVENT] 				= EXIT_REQUEST_STATE;

	// $[01274] Following an SST test alert, the operator shall be able to ...
	sstStateTable_[END_ALERT_STATE]			[EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	sstStateTable_[END_ALERT_STATE]			[REPEAT_EVENT] 				= REPEAT_STATE;
	sstStateTable_[END_ALERT_STATE]			[NEXT_EVENT] 				= NEXT_STATE;
	sstStateTable_[END_ALERT_STATE]			[RESTART_EVENT] 			= RESTART_STATE;

	// $[01274] Following an SST test failure, the operator shall be able to ...
	sstStateTable_[END_FAILURE_STATE]		[EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	sstStateTable_[END_FAILURE_STATE]		[REPEAT_EVENT]				= REPEAT_STATE;
	sstStateTable_[END_FAILURE_STATE]		[RESTART_EVENT]				= RESTART_STATE;

	sstStateTable_[TEST_PASS_STATE]			[EXIT_EVENT]				= EXIT_REQUEST_STATE;
	sstStateTable_[TEST_PASS_STATE]			[USER_END_EVENT]			= DO_NEXT_STATE;
	sstStateTable_[TEST_PASS_STATE]			[AUTO_EVENT]				= DO_NEXT_STATE;

	sstStateTable_[EXECUTE_PROMPT_STATE]	[AUTO_EVENT] 				= DO_TEST_STATE;

	sstStateTable_[EXIT_REQUEST_STATE]		[ACCEPT_EVENT] 				= EXECUTE_EXIT_SST_STATE;
	sstStateTable_[EXIT_REQUEST_STATE]		[EXIT_EVENT] 				= EXIT_FALLBACK_STATE;
	sstStateTable_[EXIT_REQUEST_STATE]		[REPEAT_EVENT] 				= REPEAT_STATE;
	sstStateTable_[EXIT_REQUEST_STATE]		[NEXT_EVENT] 				= NEXT_STATE;
	sstStateTable_[EXIT_REQUEST_STATE]		[OVERRIDE_EVENT]			= OVERRIDE_STATE;
	sstStateTable_[EXIT_REQUEST_STATE]		[RESTART_EVENT]				= RESTART_STATE;

	sstStateTable_[REPEAT_STATE]			[ACCEPT_EVENT] 				= EXECUTE_REPEAT_STATE;
	sstStateTable_[REPEAT_STATE]			[EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	sstStateTable_[REPEAT_STATE]			[REPEAT_EVENT]  			= EXIT_FALLBACK_STATE;
	sstStateTable_[REPEAT_STATE]			[NEXT_EVENT]  				= NEXT_STATE;
	sstStateTable_[REPEAT_STATE]			[RESTART_EVENT]  			= RESTART_STATE;

	sstStateTable_[NEXT_STATE]				[ACCEPT_EVENT] 				= DO_NEXT_STATE;
	sstStateTable_[NEXT_STATE]				[EXIT_EVENT] 				= EXIT_REQUEST_STATE;
	sstStateTable_[NEXT_STATE]				[REPEAT_EVENT]  			= REPEAT_STATE;
	sstStateTable_[NEXT_STATE]				[NEXT_EVENT]  				= EXIT_FALLBACK_STATE;
	sstStateTable_[NEXT_STATE]				[RESTART_EVENT]  			= RESTART_STATE;

	sstStateTable_[DO_NEXT_STATE]			[AUTO_EVENT] 				= DO_TEST_STATE;
	sstStateTable_[DO_NEXT_STATE]			[AUTO_END_OF_TEST_EVENT]	= END_OF_SST_STATE;

	sstStateTable_[OVERRIDE_STATE]			[ACCEPT_EVENT]				= EXIT_SST_STATE;
	sstStateTable_[OVERRIDE_STATE]			[EXIT_EVENT]				= EXIT_REQUEST_STATE;
	sstStateTable_[OVERRIDE_STATE]			[OVERRIDE_EVENT]			= EXIT_FALLBACK_STATE;
	sstStateTable_[OVERRIDE_STATE]			[RESTART_EVENT]  			= RESTART_STATE;

	// $[01279] Following completion of SST, if no alert or failure ..
	// $[01280] Following completion of SST, if one or more alerts ...
	sstStateTable_[END_OF_SST_STATE]		[OVERRIDE_EVENT] 			= OVERRIDE_STATE;
	sstStateTable_[END_OF_SST_STATE]		[RESTART_EVENT] 			= RESTART_STATE;
	sstStateTable_[END_OF_SST_STATE]		[EXIT_EVENT] 				= EXIT_REQUEST_STATE;

	sstStateTable_[PAUSE_REQUEST_STATE]		[EXIT_EVENT] 				= EXIT_FALLBACK_STATE;
	sstStateTable_[PAUSE_REQUEST_STATE]		[USER_PROMPT_EVENT] 		= EXIT_BEFORE_PROMPT_STATE;
	sstStateTable_[PAUSE_REQUEST_STATE]		[END_ALERT_EVENT] 			= END_EXIT_ALERT_STATE;
	sstStateTable_[PAUSE_REQUEST_STATE]		[END_FAILURE_EVENT] 		= END_EXIT_FAILURE_STATE;
	sstStateTable_[PAUSE_REQUEST_STATE]		[USER_END_EVENT] 			= END_EXIT_TEST_STATE;

	sstStateTable_[EXIT_BEFORE_PROMPT_STATE][EXIT_EVENT]				= PROMPT_STATE;
	sstStateTable_[EXIT_BEFORE_PROMPT_STATE][ACCEPT_EVENT] 				= EXECUTE_EXIT_SST_STATE;

	sstStateTable_[END_EXIT_ALERT_STATE]	[AUTO_EVENT] 				= EXIT_REQUEST_STATE;
	sstStateTable_[END_EXIT_FAILURE_STATE]	[AUTO_EVENT] 				= EXIT_REQUEST_STATE;
	sstStateTable_[END_EXIT_TEST_STATE]		[AUTO_EVENT] 				= EXIT_REQUEST_STATE;

	sstStateTable_[EXECUTE_EXIT_SST_STATE]	[USER_END_EVENT] 			= EXIT_SST_STATE;
	sstStateTable_[EXECUTE_REPEAT_STATE]	[AUTO_EVENT]				= DO_TEST_STATE;

	sstStateTable_[RESTART_STATE]			[ACCEPT_EVENT]				= EXECUTE_RESTART_STATE;
	sstStateTable_[RESTART_STATE]			[EXIT_EVENT]				= EXIT_REQUEST_STATE;
	sstStateTable_[RESTART_STATE]			[REPEAT_EVENT] 				= REPEAT_STATE;
	sstStateTable_[RESTART_STATE]			[NEXT_EVENT]  				= NEXT_STATE;
	sstStateTable_[RESTART_STATE]			[OVERRIDE_EVENT]			= OVERRIDE_STATE;
	sstStateTable_[RESTART_STATE]			[RESTART_EVENT]  			= EXIT_FALLBACK_STATE;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SstTestSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys SstTestSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstTestSubScreen::~SstTestSubScreen(void)
{
	CALL_TRACE("SstTestSubScreen::~SstTestSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initializes the subscreen display.  Registers all possible test commands
//	and results for test events.  Initializes all subscreen state variables.
//	Set test prompts.  Initializes data scroallble log.
//
// $[01265] The lower LCD screen shall provide the following functions ..
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::activate(void)
{
	CALL_TRACE("SstTestSubScreen::activate(void)");

#if defined FORNOW
printf("SstTestSubScreen::activate() at line  %d, this = %d\n", __LINE__, this);
#endif	// FORNOW

	RecursionCount_ = 0;
	
	// Hide the drawables within the lower screen select area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

	// Release Adjust Panel focus if exists.
	AdjustPanel::TakeNonPersistentFocus(NULL);

	// Disable the knob sound.
	// $[01360] Sigma is capable of creating the following user interface sound.
	AdjustPanel::DisableKnobRotateSound();
	
	// Register all general possible test results for test events
	testManager_.registerTestCommands();

	// Register all general possible test results for test events
	testManager_.registerTestResultData();
	
	nextState_ = INITIAL_STATE;
	previousState_ = INITIAL_STATE;
	fallBackState_ = INITIAL_STATE;
	
	//
	// Set NULL to high priority prompts
	//
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
						PromptArea::PA_HIGH, NULL_STRING_ID);
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_HIGH, NULL_STRING_ID);
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_HIGH, NULL_STRING_ID);

	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::SST_PLEASE_WAIT_P);
	
	// Hide the following drawables
	exitButton_.setShow(FALSE);
	repeatButton_.setShow(FALSE);
	nextButton_.setShow(FALSE);
	overrideButton_.setShow(FALSE);
	restartButton_.setShow(FALSE);
	sstStatusStr_.setShow(FALSE);
	sstCompletedLabel_.setShow(FALSE);
	sstOutcomeLabel_.setShow(FALSE);
	sstCompletedValue_.setShow(FALSE);
	sstOutcomeValue_.setShow(FALSE);

	// Show the following drawables
	sstStatusStr_.setShow(TRUE);

    Boolean displayProx = FALSE;

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
	SettingContextHandle::GetSettingValue(ContextId::ADJUSTED_CONTEXT_ID,
										  SettingId::PATIENT_CCT_TYPE);

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			if (GuiApp::IsProxInstalled())
			{
				displayProx = TRUE;
			}
			else
			{
				displayProx = FALSE;
			}
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :
            displayProx = FALSE;
			break;
		default :
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}


	if (displayProx)
	{
		// Create scrollable log
		pLog_ = ScrollableLog::New(this, SST_DISPLAY_ROW_MAX, SST_DISPLAY_COL_MAX,
														SST_ROW_HEIGHT_, FALSE);
	}
	else
	{
		// Create scrollable log
		pLog_ = ScrollableLog::New(this, SST_DISPLAY_ROW_MAX -1 , SST_DISPLAY_COL_MAX,
														SST_ROW_HEIGHT_, FALSE);
	}

	// Setup the log
	pLog_->setX(LOG_X_);
	pLog_->setY(LOG_Y_);

	// Setup the column titles
	pLog_->setColumnInfo(COL_1_TIME, MiscStrs::SST_LOG_COL_1_TITLE,
					SST_COL_1_WIDTH_, CENTER, 0, TextFont::NORMAL,
					TextFont::TEN, TextFont::EIGHT);
	pLog_->setColumnInfo(COL_2_TEST, MiscStrs::SST_LOG_COL_2_TITLE,
					SST_COL_2_WIDTH_, LEFT, 2, TextFont::NORMAL,
					TextFont::TEN, TextFont::TEN);
	pLog_->setColumnInfo(COL_3_DIAG_CODE, MiscStrs::SST_LOG_COL_3_TITLE,
					SST_COL_3_WIDTH_, LEFT, 2, TextFont::NORMAL,
					TextFont::TEN, TextFont::TEN);
	pLog_->setColumnInfo(COL_4_RESULT, MiscStrs::SST_LOG_COL_4_TITLE,
					SST_COL_4_WIDTH_, CENTER, 0, TextFont::NORMAL,
					TextFont::TEN, TextFont::TEN);

	// Display log in hidden state
	addDrawable(pLog_);

	pLog_->setShow(TRUE);


	if (displayProx)
	{
	    maxServiceModeTest_ = SmTestId::SST_TEST_MAX;
	    pLog_->setEntryInfo(SmTestId::SST_TEST_START_ID, SmTestId::SST_TEST_MAX);
	}
	else
	{

		maxServiceModeTest_ = SmTestId::SST_TEST_MAX -1;
		// Setup the display page 
		pLog_->setEntryInfo(SmTestId::SST_TEST_START_ID, SmTestId::SST_TEST_MAX - 1);

	}



	// Disable user scrollable function in the log.
	pLog_->setUserScrollable(FALSE);

	// Signal to display the log cell infos.
	clearLogEntry_ = TRUE;

	// Activate the log
	pLog_->activate();

	// Start the appropriated countdown timer to display the SST
	// screen.
	GuiTimerRegistrar::StartTimer(GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT, this);
																// $[TI1]	

	nextButton_.setTitleText(MiscStrs::SST_NEXT_MSG,GRAVITY_CENTER);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed and clears the Adjust Panel
// focus in case we had it.  Also removes scrollable log from container.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::deactivate(void)
{
	CALL_TRACE("SstTestSubScreen::deactivate(void)");

	pLog_->deactivate();

	// Remove scrollable log
	removeDrawable(pLog_);
	pLog_ = NULL;
	
	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
							 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Primary, Advisory and Secondary prompts
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
											NULL_STRING_ID);
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
											NULL_STRING_ID);
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
											PromptArea::PA_LOW, NULL_STRING_ID);
											// $[TI1]

	if (pUpperSubScreenArea_->isCurrentSubScreen(pSstProxSubScreen_))
	{													// $[TI1]
		ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea()->deactivateSubScreen();
	}		
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
//	Called by ScrollableLog when it needs to retrieve the text content of a
//	particular column of a log entry.  Passed an entry number, a column number,
//	a CheapText flag.  Return four strings via parameter list.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	If log entry is empty then a NULL string is passed back.  Otherwise,
//  fill up temporary buffer for the content of each log column.  This
//  temporary buffer is what will be passed back through the parameter
//  list.  The four columns are TIME, TEST NAME, DIAG_CODE and RESULT.
// $[01284] During SST, the SST status subscreen shall display a table ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
SstTestSubScreen::getLogEntryColumn(Uint16 entryNumber, Uint16 columnNumber,
						Boolean &rIsCheapText, StringId &rString1,
						StringId &rString2, StringId &rString3,
						StringId &rString1Message)
{
	CALL_TRACE("SstTestSubScreen::getLogEntryColumn(Uint16, Uint16, Boolean &, StringId &, StringId &)");

#if defined FORNOW
printf("getLogEntryColumn() entryNumber=%d, columnNumber=%d, clearLogEntry=%d at line  %d\n",
				entryNumber, columnNumber, (clearLogEntry_)?1:0, __LINE__);
#endif	// FORNOW
				
	wchar_t resultBuffer[256];
	static wchar_t TmpBuffer1_[256];
	static wchar_t TmpBuffer2_[256];
	ResultEntryData resultData;
	Int smTestId;

	rIsCheapText = FALSE;
	rString1 = NULL_STRING_ID;
	rString2 = NULL_STRING_ID;
	
	if (!clearLogEntry_)
	{											// $[TI1]
		TmpBuffer1_[0] = L'\0';
		TmpBuffer2_[0] = L'\0';

		// Convert the log entry number to corresponding SST result table index.
		smTestId = SmTestId::GetSstIdFromSmId(sstTestId_[entryNumber]);
		
		// Get the corresponding Result Data for the entry number.
		sstResultTable_[smTestId].getResultData(resultData);

#if defined FORNOW							
printf("   	sstResultTable_[%d].isClear= %d\n",
		smTestId, sstResultTable_[smTestId].isClear());
#endif	// FORNOW
			
		switch (columnNumber)
		{
		case COL_1_TIME:						// $[TI2]
		{
			// Check if there is anything to be displayed
    		if (sstResultTable_[smTestId].isClear())       
	    	{									// $[TI3]
				return;
	    	}									// $[TI4]

			TimeStamp timeStamp;
			
	        // Timestamp, let's format it
			timeStamp = resultData.getTimeOfCurrResult();

			// Formatting Time/Date string
			TextUtil::FormatTime(TmpBuffer1_, MiscStrs::SST_DATE_TIMESTAMP_LINE1,
											timeStamp);

			wchar_t format[30];
			swprintf(format, MiscStrs::SST_DATE_TIMESTAMP_LINE2, 
											MiscStrs::LOG_DATE_FORMAT);
			TextUtil::FormatTime(TmpBuffer2_, format, timeStamp);

			rString1 = (StringId)&TmpBuffer1_;
			rString2 = (StringId)&TmpBuffer2_;
			// rIsCheapText set FALSE so LogCell centers text
			rIsCheapText = FALSE;
			break;
		}
		
		case COL_2_TEST:						// $[TI5]
			wcscpy(resultBuffer, sstTestName_[entryNumber]);
			if (TextUtil::WordWrap(resultBuffer, TmpBuffer1_, TmpBuffer2_,
					TextFont::NORMAL, TextFont::TEN,
					SST_COL_2_WIDTH_, SST_ROW_HEIGHT_, LEFT, 0) == 2)
			{									// $[TI6]
				rString2 = (StringId)&TmpBuffer2_;
			}									// $[TI7]
			
			rString1 = (StringId)&TmpBuffer1_;
			rIsCheapText = TRUE;
			break;
		
		case COL_3_DIAG_CODE:					// $[TI8]
			// Format the test data
			if (sstTestData_[entryNumber][0] != NULL)
			{									// $[TI9]
				wcscpy(TmpBuffer1_, sstTestData_[entryNumber]);
				rString1 = (StringId)&TmpBuffer1_;
#if defined FORNOW				
printf("   TestData string: TmpBuffer1_=%s\n", TmpBuffer1_);
#endif	// FORNOW
	
			}									// $[TI10]

			// Check if there is anything to be displayed
    		if (sstResultTable_[smTestId].isClear()) 
	    	{									// $[TI11]
				return;
	    	}									// $[TI12]

			TestResultId currentTestResultId;
			currentTestResultId = resultData.getResultOfCurrRun();
			
#if defined FORNOW				
printf("   	currentTestResultId=%d\n", currentTestResultId);
#endif	// FORNOW

			// If current test doesn't pass, show the diagnostic information
			if (currentTestResultId == MINOR_TEST_FAILURE_ID ||
				currentTestResultId == OVERRIDDEN_TEST_ID ||
				currentTestResultId == MAJOR_TEST_FAILURE_ID)
			{									// $[TI13]
				// Compose diagnostic display string(s)
				DiagnosticFault::ComposeDisplayDiagStrings(
						DiagnosticCode::SHORT_SELF_TEST_DIAG, smTestId, 
						TmpBuffer2_, resultBuffer);

#if defined FORNOW							
printf("   	TmpBuffer2_=%s, resultBuffer=%s\n",
			TmpBuffer2_, resultBuffer);
#endif	// FORNOW
			

				CLASS_ASSERTION(resultBuffer != NULL);
			
				if (rString1 == NULL_STRING_ID)
				{								// $[TI14]
					rString1 = (StringId)&TmpBuffer2_;
				}
				else
				{								// $[TI15]
					rString2 = (StringId)&TmpBuffer2_;
				}
			}									// $[TI16]
			break;
		
		case COL_4_RESULT:						// $[TI17]
			// Check if there is anything to be displayed
    		if (sstResultTable_[smTestId].isClear())  // $[TI18]
	    	{
				return;
	    	}									// $[TI19]

	    	formatStatusOutcome_(resultData, resultBuffer, FALSE);
			if (resultBuffer[0] != L'\0')
			{	// Not an empty string.			// $[TI20]
				if (TextUtil::WordWrap(resultBuffer, TmpBuffer1_, TmpBuffer2_,
						TextFont::NORMAL, TextFont::TEN,
						SST_COL_4_WIDTH_, SST_ROW_HEIGHT_, CENTER, 0) == 2)
				{								// $[TI21]
					rString2 = (StringId)&TmpBuffer2_;
				}								// $[TI22]
			}									// $[TI23]
			
			rString1 = (StringId)&TmpBuffer1_;
			rIsCheapText = TRUE;
			break;
		
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}

#if defined FORNOW
printf("rString1=%s\n", rString1);
printf("rString2=%s\n", rString2);
#endif	// FORNOW
		
	}											// $[TI24]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when either button on this subscreen is pressed down.  Passed a
// pointer to the button as well as 'byOperatorAction', a flag which indicates
// whether the operator pressed the button down or whether the setToDown()
// method was called (flag is ignored).  We display any appropriate prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must display some new prompts advising the operator on the use of the
// Proceed button.  Setup the "newEvent" based on the touch button. Then
// we figure out the "nextState_" and call executeState_to execute the needed
// functionality.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::buttonDownHappened(Button *pButton,
									Boolean byOperatorAction)
{
	CALL_TRACE("SstTestSubScreen::buttonDownHappened(Button *pButton, "
																"Boolean)");
#if defined FORNOW																
printf("buttonDownHappened at line  %d, pButton=%d, pCurrentButton=%d\n"
		, __LINE__, pButton, pCurrentButton_);
#endif	// FORNOW
		
		
	Int newEvent;

	if (!isVisible())
	{											// $[TI1]
		return;
	}											// $[TI2]

	// Check if the event is operator initiated
	if (byOperatorAction)
	{											// $[TI3]
		if (NULL != pCurrentButton_ && pCurrentButton_ != pButton)
		{										// $[TI4]
			// User pressed two different buttons in sequence.
			// The previous button needs to be popped up.
			// The adjustPanel focus shall set to NULL.
			pCurrentButton_->setToUp();
		
			// Lose Adjust Panel focus
			AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

			// Clear the Primary, Advisory and Secondary prompts
			testManager_.setTestPrompt(PromptArea::PA_PRIMARY,  PromptArea::PA_HIGH, 
									                NULL_STRING_ID);
			testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
													NULL_STRING_ID);
			testManager_.setTestPrompt(PromptArea::PA_SECONDARY,PromptArea::PA_HIGH, 
									                NULL_STRING_ID);

		}										// $[TI5]
	
		if (pButton ==& repeatButton_)
		{										// $[TI6]
			newEvent = REPEAT_EVENT;
		}
		else if (pButton == &nextButton_)
		{										// $[TI7]
			newEvent = NEXT_EVENT;
		}
		else if (pButton == &exitButton_)
		{										// $[TI8]
			newEvent = EXIT_EVENT;
		}
		else if (pButton == &overrideButton_)
		{										// $[TI9]
			newEvent = OVERRIDE_EVENT;
		}
		else if (pButton == &restartButton_)
		{										// $[TI10]
			newEvent = RESTART_EVENT;
		}
		else
		{
			CLASS_ASSERTION_FAILURE();
		}	


		// The call sequence of the next two statements is very important
		// in determining the fallbackState_.
		setupFallbackState_();
		nextState_ = getNextState_(newEvent);
		executeState_();
		pCurrentButton_ = pButton;
	}
	else
	{											// $[TI11]
#if defined FORNOW	
printf("IN buttonDownHappened, pButton=%d\n", pButton);
#endif	// FORNOW

		pCurrentButton_ = pButton;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when the Proceed button is pressed up.  Passed a pointer to the
// button as well as 'byOperatorAction', a flag which indicates whether the
// operator pressed the button up or whether the setToUp() method was called.
// We restore prompts appropriately.
//---------------------------------------------------------------------
//@ Implementation-Description
// We setup the "newEvent" based on the touch button. Then
// we figure out the "nextState_" and call executeState_to execute the needed
// functionality.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::buttonUpHappened(Button *pButton, Boolean byOperatorAction)
{
	CALL_TRACE("SstTestSubScreen::buttonUpHappened(Button *pButton, "
																"Boolean)");
#if defined FORNOW																
printf("buttonUpHappened at line  %d, pButton=%d, pCurrentButton=%d\n"
		, __LINE__, pButton, pCurrentButton_);
#endif	// FORNOW
		
		
	Int newEvent;

	// Don't do anything if this subscreen isn't displayed
	if (!isVisible())
	{											// $[TI1]
		return;
	}											// $[TI2]

	// Check if the event is operator initiated
	if (byOperatorAction)
	{											// $[TI3]
		if (pButton ==& repeatButton_)
		{										// $[TI4]
			newEvent = REPEAT_EVENT;
		}
		else if (pButton == &nextButton_)
		{										// $[TI5]
			newEvent = NEXT_EVENT;
		}
		else if (pButton == &exitButton_)
		{										// $[TI6]
			newEvent = EXIT_EVENT;
		}
		else if (pButton == &overrideButton_)
		{										// $[TI7]
			newEvent = OVERRIDE_EVENT;
		}
		else if (pButton == &restartButton_)
		{										// $[TI8]
			newEvent = RESTART_EVENT;
		}
		else
		{
			CLASS_ASSERTION_FAILURE();
		}	

		nextState_ = getNextState_(newEvent);
		executeState_();
		pCurrentButton_ = NULL;
	}
	else
	{							
#if defined FORNOW	
printf("IN buttonUpHappened, pButton=%d\n", pButton);
#endif	// FORNOW

	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the SST process.  
//---------------------------------------------------------------------
//@ Implementation-Description
// Invoke the test manager's method to restore the prompts displayed before
// the focus is taken.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("SstTestSubScreen::adjustPanelRestoreFocusHappened(void)");

#if defined FORNOW
printf("adjustPanelRestoreFocusHappened at line  %d\n", __LINE__);
#endif	// FORNOW
	testManager_.restoreTestPrompt();
									// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.  It is
// normally called when the operator presses down the Accept key to signal the
// accepting of continuing the SST process. 
//---------------------------------------------------------------------
//@ Implementation-Description
// Pop up the button that is currently down.  Clear the Adjust Panel focus.
// Clear high prioritied prompts.  Reset the keyAllowedId_ and userKeyPressedId_.
// Finally, execute the next state in the state machine.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("SstTestSubScreen::adjustPanelAcceptPressHappened(void)");

#if defined FORNOW
printf("adjustPanelAcceptPressHappened at line  %d\n", __LINE__);
#endif	// FORNOW

	CLASS_ASSERTION((pCurrentButton_ != NULL) ||
						(pCurrentButton_ == NULL && nextState_ == PROMPT_STATE));

	if (pCurrentButton_ != NULL)
	{								// $[TI1]
		// Pop up the button.
		pCurrentButton_->setToUp();
		pCurrentButton_ = NULL;
	}								// $[TI2]

	// Reset the keyAllowedId_
   //	keyAllowedId_ = SmPromptId::NULL_KEYS_ALLOWED_ID;
		
	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompts
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);
			
	// Clear the Advisory prompts
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
				 PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
				PromptArea::PA_HIGH, NULL_STRING_ID);

	// Reset the keyAllowedId_
	keyAllowedId_ = SmPromptId::NULL_KEYS_ALLOWED_ID;
	userKeyPressedId_ = SmPromptId::KEY_ACCEPT;

	nextState_ = getNextState_(ACCEPT_EVENT);
	executeState_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelClearPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.
//  It is called when the operator presses the Clear key. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Pop up the button that is currently down.  Clear the Adjust Panel focus.
//  Clear high prioritied prompts.  Reset the keyAllowedId_ and userKeyPressedId_.
//  Finally, execute the next state in the state machine.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::adjustPanelClearPressHappened(void)
{
	CALL_TRACE("SstTestSubScreen::adjustPanelClearPressHappened(void)");

#if defined FORNOW
printf("adjustPanelClearPressHappened at line  %d\n", __LINE__);
#endif	// FORNOW

        // Clear key is designed to be used only with prompts.  Exit request should
        // ignore any Clear request
        if (nextState_ == EXIT_REQUEST_STATE || nextState_ == EXIT_BEFORE_PROMPT_STATE)
	{       //      $[TI1]
  	  return;
	}       //      $[TI2]


	if (keyAllowedId_ == SmPromptId::ACCEPT_YES_AND_CLEAR_NO_ONLY ||
        keyAllowedId_ == SmPromptId::USER_EXIT_ONLY ||
		keyAllowedId_ == SmPromptId::IF_CONNECTED_ACCEPT_YES_AND_CLEAR_NO_ONLY)
	{
		// $[TI3]
                // Service mode had request a CLEAR prompt action.
                SAFE_CLASS_ASSERTION((pCurrentButton_ != NULL) ||
                                     (pCurrentButton_ == NULL && nextState_ == PROMPT_STATE));

                // Reset the keyAllowedId_
                keyAllowedId_ = SmPromptId::NULL_KEYS_ALLOWED_ID;
 
                // Make sure that nothing in this subscreen keeps focus.
                AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);
 
                // Clear the Primary prompts
                testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
                                         PromptArea::PA_HIGH, NULL_STRING_ID);
 
                // Clear the Advisory prompts
                testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
                                         PromptArea::PA_HIGH, NULL_STRING_ID);
 
                // Clear the Secondary prompt
                testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
                                        PromptArea::PA_HIGH, NULL_STRING_ID);

                userKeyPressedId_ = SmPromptId::KEY_CLEAR;
                nextState_ = getNextState_(CLEAR_EVENT);
                executeState_();

	} // end if keyAllowedId_
	// $[TI4]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: novRamUpdateEventHappened
//
//@ Interface-Description
// Called by NovRam manager, this function updates the display with
// the latest, real-time data in the log.
//---------------------------------------------------------------------
//@ Implementation-Description
// Request the scrollable log to display the log information based on
// the type of novram.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId
											updateId)
{
	CALL_TRACE("SstTestSubScreen::novRamUpdateEventHappened"
				"(NovRamUpdateManager::UpdateTypeId	updateId)");

#if defined FORNOW	
printf("novRamUpdateEventHappened at line  %d, updateId=%d, isThisSstTestCompleted_=%d, mostSevereStatusId_=%d\n",
				__LINE__, updateId, isThisSstTestCompleted_, mostSevereStatusId_);
#endif	// FORNOW

	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	if (pLog_ != NULL)
	{												// $[TI3]
		// Update the novRam message only when the scrollable log is
		// instantiated.

		// Get Sst test result table from NovRam
		NovRamManager::GetSstResultEntries(sstResultTable_);

		switch (updateId)
		{
		case NovRamUpdateManager::SST_RESULT_TABLE_UPDATE:	// $[TI4]
			// Inform the scrollable log of the change in log entries.
			pLog_->updateEntryColumn(sstCurrentTestIndex_, COL_1_TIME);
			pLog_->updateEntryColumn(sstCurrentTestIndex_, COL_4_RESULT);
			break;
		case NovRamUpdateManager::SST_LOG_UPDATE:	// $[TI5]
			// Inform the scrollable log of the change in log entries.
			pLog_->updateEntryColumn(sstCurrentTestIndex_, COL_3_DIAG_CODE);
			break;
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}

		if (mostSevereStatusId_ == MAJOR_TEST_FAILURE_ID &&
			isThisSstTestCompleted_)
		{											// $[TI6]
			// Display the SST outcome and time stamp as directed by
			// NovRam manager.
			displaySstResultOutcome_();
		}											// $[TI7]
	}												// $[TI8]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when a countdown timer tick occurs.  The given `timerId'
// tells us which case occurred.
//---------------------------------------------------------------------
//@ Implementation-Description
// If the SERVICE_MODE_TEST_PAUSE_TIMEOUT countdown timer tick timeout
// then based on the "nextState_" to proceed to different state.  If
// we are at INITIAL_STATE, then we just completed the screen update
// and we are in READY_TO_TEST_STATE state.  Else, user has requested
// "exiting" from SST, then we will activate the ExitServiceModeSubScreen
// to reboot the system.
//---------------------------------------------------------------------
//@ PreCondition
// The given `timerId' must be SERVICE_MODE_TEST_PAUSE_TIMEOUT.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("SstTestSubScreen::timerEventHappened("
					"GuiTimerId::GuiTimerIdType timerId)");

	if (isVisible())
	{														// $[TI1]
		switch (timerId)
		{
		case GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT:			// $[TI2]	
			GuiTimerRegistrar::CancelTimer(GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT);

			if (nextState_ == INITIAL_STATE)
			{												 // $[TI3]
				nextState_ = getNextState_(AUTO_EVENT);
				executeState_();
			}
			else
			{												 // $[TI4]
				// Get the SST service Lower subScreen area address.

				// Activate the Exit service subscreen to exit SST mode.
				getSubScreenArea()->activateSubScreen(
						LowerSubScreenArea::GetExitServiceModeSubScreen(), NULL);
			}

			break;
			
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}
	}														//  // $[TI5]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestPrompt
//
//@ Interface-Description
//  Called when we are informed of a change in test prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Save the given command in private class storage.  Determine the next state in
//  the testing process then call the executeState() method to continue with the SST
//	tests.
//---------------------------------------------------------------------
//@ PreCondition
//	 none
//---------------------------------------------------------------------
//@ PostCondition
//	 none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("SstTestSubScreen::processTestPrompt(Int command)");

#if defined FORNOW
printf("processTestPrompt at line  %d\n", __LINE__);
#endif	// FORNOW

	// Remember the most recent prompt ID
	promptId_ = command;

	nextState_ = getNextState_(USER_PROMPT_EVENT);
#if defined FORNOW	
printf("SstTestSubScreen::processTestPrompt_() nextState is %d\n", nextState_);
#endif	// FORNOW

	executeState_();

#if defined FORNOW
printf("SstTestSubScreen::processTestPrompt_() nextState is %d\n", nextState_);
#endif	// FORNOW
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//	Called when we are informed of a change in Service-Mode's "key allowed"
//	command.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Copy the new command value to our own variable keyAllowedId_
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::processTestKeyAllowed(Int command)
{
	CALL_TRACE("SstTestSubScreen::processTestKeyAllowed(Int command)");

#if defined FORNOW
printf("processTestKeyAllowed at line  %d\n", __LINE__);
#endif	// FORNOW

	// Remember the expected user key response ID
	keyAllowedId_ = (SmPromptId::KeysAllowedId) command;
											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//	Called when we are informed of a change in test result status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Based on the result of the SST test, determine the next state and
//	event in the testing process.  If a SST test ends or if the user
//  presses exit, the method executeState() is then  called to execute
//  the SST tests. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("SstTestSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)");

	CLASS_ASSERTION((resultStatus >= (Int) SmStatusId::TEST_END) &&
						 (resultStatus < (Int) SmStatusId::NUM_TEST_DATA_STATUS));
	Int newEvent=-1;

#if defined FORNOW
printf("processTestResultStatus_ at line  %d, resultStatus= %d, testResultId = %d, mostSevereStatusId_ = %d\n",
			__LINE__, resultStatus, testResultId, mostSevereStatusId_);
#endif	// FORNOW
				
	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	switch (resultStatus)
	{
	case SmStatusId::TEST_END:			// End of test status received
										// $[TI3]
		switch (mostSevereStatusId_) 	// Check against the previous status
		{
		// Test end with no error
		case PASSED_TEST_ID:			// $[TI3.1]	
			newEvent = USER_END_EVENT;
			break;
		case MINOR_TEST_FAILURE_ID:		// Test end with an alert error
										// $[TI3.2]
			newEvent = END_ALERT_EVENT;
			break;
		case MAJOR_TEST_FAILURE_ID:	// Test end with an failure error
										// $[TI3.3]
			newEvent = END_FAILURE_EVENT;
			break;
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}

		// Set isThisSstTestCompleted_ flag.
		isThisSstTestCompleted_ = TRUE;
		
		// Get Sst test result table from NovRam
		NovRamManager::GetSstResultEntries(sstResultTable_);

		// Inform the scrollable log of the change in log entries.
		pLog_->updateEntryColumn(sstCurrentTestIndex_, COL_1_TIME);
		pLog_->updateEntryColumn(sstCurrentTestIndex_, COL_4_RESULT);

		nextState_ = getNextState_(newEvent);
		executeState_();
		break;
		
	case SmStatusId::TEST_START:		// $[TI4]
		// Do nothing.
		break;

	case SmStatusId::TEST_ALERT:		// $[TI5]

		// Skip the prox test due to a user confirmation.
		if (conditionId_ ==	SmStatusId::CONDITION_10 &&
			testResultId == SmTestId::SST_TEST_PROX_ID)
		{
			break;
		}

	case SmStatusId::TEST_FAILURE:		// $[TI6]
		if ((mostSevereStatusId_ == PASSED_TEST_ID) ||
			(mostSevereStatusId_ == MINOR_TEST_FAILURE_ID))
		{								// $[TI6.1]
			// Update mostSevereStatusId_ only when there are no previous error or
			// there are previous minor error and we just get a major error.
			mostSevereStatusId_ = sstStatusDecodingTable_[resultStatus];
		}								// $[TI6.2]
		break;
		
	case SmStatusId::TEST_OPERATOR_EXIT:// $[TI7]
		AUX_CLASS_ASSERTION(nextState_ == EXECUTE_EXIT_SST_STATE, nextState_);

		newEvent = USER_END_EVENT;
		nextState_ = getNextState_(newEvent);
		executeState_();
		break;

	case SmStatusId::NOT_INSTALLED:		// $[TI8]
		mostSevereStatusId_ = PASSED_TEST_ID;
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
	
#if defined FORNOW	
printf("$$$$$$$$testResultId=%d, conditionId_=%d\n",
				testResultId, conditionId_);
#endif	// FORNOW

}
	


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//	Called when we are informed of a change in Service-Mode's test
//	result condition.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Copy the new resultCondition to our copy of the same.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("SstTestSubScreen::processTestResultCondition(Int resultCondition)");

#if defined (FORNOW)
printf("SstTestSubScreen::processTestResultCondition_() condition=%d\n",
				resultCondition);
#endif 	// FORNOW
	
	// Filling the condition up. This will be updated to NVram.

	// Remember the most recent condition ID
	conditionId_ = resultCondition;
											// $[TI1]
}
	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//	Called when we are informed of a change in Service-Mode's test
//	data.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Based on the sST test, format the data string into sstTestData_[]
//  accordingly.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
SstTestSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("SstTestSubScreen::processTestData(Int dataIndex, TestResult *pResult)");

#if defined FORNOW
printf("processTestData at line  %d, dataIndex = %d, dataValue = %f, sstCurrentTestIndex_ = %d\n",
				__LINE__, dataIndex, pResult->getTestDataValue(), sstCurrentTestIndex_);
#endif	// FORNOW

	Boolean	displayThisData;

	displayThisData = FALSE;

	switch (sstTestId_[sstCurrentTestIndex_])
	{
	case SmTestId::CIRCUIT_RESISTANCE_TEST_ID:			// $[TI1]
		if (dataIndex == SmDataId::CIRCUIT_RESISTANCE_INSP_PRESSURE)
		{										// $[TI1.1]
			sstTestData_[sstCurrentTestIndex_][0] = NULL;
			swprintf(sstTestData_[sstCurrentTestIndex_], L"%s %s %s",
								MiscStrs::SST_CIRCUIT_RESISTANCE_TEST_INSP_LIMB_LABEL,
								pResult->getStringValue(),
								unitId_);
			displayThisData = TRUE;
		}		
		else if (dataIndex == SmDataId::CIRCUIT_RESISTANCE_EXP_PRESSURE)
		{										// $[TI1.2]
			wchar_t sstTestData[256];
			sstTestData[0] = NULL;
			swprintf(sstTestData, L", %s %s %s",
								MiscStrs::SST_CIRCUIT_RESISTANCE_TEST_EXH_LIMB_LABEL,
								pResult->getStringValue(),
								unitId_);
			wcscat(sstTestData_[sstCurrentTestIndex_], sstTestData);  	
			displayThisData = TRUE;
		}										// $[TI1.3]
		break;

	case SmTestId::SST_FILTER_TEST_ID:			// $[TI2]
		if (dataIndex == SmDataId::SST_FILTER_DELTA_PRESSURE)
		{										// $[TI2.1]
			const DiscreteValue  PATIENT_CCT_TYPE =
					SettingContextHandle::GetSettingValue(
												ContextId::ACCEPTED_CONTEXT_ID,
												SettingId::PATIENT_CCT_TYPE
														 );

			const StringId  DATA_LABEL =
					(PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
					 ? MiscStrs::SST_NEO_FILTER_TEST_DATA_LABEL	// $[TI2.1.1]
					 : MiscStrs::SST_FILTER_TEST_DATA_LABEL;	// $[TI2.1.2]

			swprintf(sstTestData_[sstCurrentTestIndex_], L"%s %s %s",
								DATA_LABEL,
								pResult->getStringValue(),
								unitId_);

			displayThisData = TRUE;
		}										// $[TI2.2]
		break;

	case SmTestId::SST_LEAK_TEST_ID:			// $[TI3]
		if (dataIndex == SmDataId::SST_LEAK_MONITOR_PRESSURE)
		{										// $[TI3.1]
			pSstLeakSubScreen_->setValue(pResult->getTestDataValue());
		}
		else if (dataIndex == SmDataId::SST_LEAK_DELTA_PRESSURE)
		{										// $[TI3.2]
			swprintf(sstTestData_[sstCurrentTestIndex_], L"%s %s %s",
								MiscStrs::SST_LEAK_TEST_DATA_LABEL,
								pResult->getStringValue(),
								unitId_);
			displayThisData = TRUE;
		}
		break;
	case SmTestId::CIRCUIT_PRESSURE_TEST_ID:	// $[TI4]

	case SmTestId::SM_SST_PROX_TEST_ID:
		sstTestData_[sstCurrentTestIndex_][0] = NULL;
		break;

	case SmTestId::SST_FS_CC_TEST_ID:			// $[TI5]
		sstTestData_[sstCurrentTestIndex_][0] = NULL;
		break;

	case SmTestId::COMPLIANCE_CALIB_TEST_ID:	// $[TI6]
		if (dataIndex == SmDataId::COMPLIANCE_CALIB_NOMINAL_COMPLIANCE)
		{										// $[TI7]
			swprintf(sstTestData_[sstCurrentTestIndex_], L"%s %s %s/%s",
								MiscStrs::COMPLIANCE_CALIB_TEST_DATA_LABEL,
								pResult->getStringValue(),
								MiscStrs::ML_LABEL, unitId_);
			displayThisData = TRUE;
		}										// $[TI8]
		break;
	case SmTestId::SST_TEST_MAX:							// $[TI9]
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

	if (displayThisData)
	{											// $[TI10]
		// Inform the scrollable log of the change in log entries.
		pLog_->updateEntryColumn(sstCurrentTestIndex_, COL_3_DIAG_CODE);
	}											// $[TI11]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// Sets the subscreen into one of the display configurations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If screen id is SST_START_SCREEN, we clear the selected entry highlight,
//  enforce the log_ starts at the first log entry, Display test name,
//  inform the service mode  manager of the test type, and show exit button.
//  If screen id is SST_EXIT_SCREEN, we clear the advisory and secondary
//  prompts and deactivate this subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::layoutScreen_(SstScreenId newScreenId)
{
	CALL_TRACE("SstTestSubScreen::layoutScreen_(SstScreenId newScreenId)");

#if defined FORNOW
printf("layoutScreen_, newScreenId = %d at line  %d\n", newScreenId, __LINE__);
#endif	// FORNOW

	switch (newScreenId)
	{
	case SST_START_SCREEN:					 // $[TI1]
		// Clear the selected entry highlight whichever row it may be.
		pLog_->clearSelectedEntry();
		
		// To enforce the log_ starts at the first log entry.
		// High light the test item

		sstCurrentTestIndex_ = SmTestId::SST_TEST_START_ID;

		// Initialize the starting item test.
		pLog_->setSelectedEntry(sstCurrentTestIndex_);

		// Signal to generate the log cell infos.
		clearLogEntry_ = FALSE;

		// Display test name.
		pLog_->updateColumn(COL_2_TEST);

		// Initialize the next status
		overrideRequested_ = FALSE;

		// Let the service mode  manager knows the type of Service mode.
		SmManager::DoFunction(SmTestId::SET_TEST_TYPE_SST_ID);

		// Show EXIT button
		exitButton_.setShow(TRUE);
		break;
		
	case SST_EXIT_SCREEN:					// $[TI2]
		// Clear the Advisory prompts
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
			 PromptArea::PA_LOW, NULL_STRING_ID);

		// Clear the Secondary prompt.
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
			PromptArea::PA_LOW, NULL_STRING_ID);

		// Deactivate this subscreen.
		deactivate();
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSstTestNameTable_
//
//@ Interface-Description
//  Initialize the sstTestName_, sstTestId_, and sstTestData_ tables.
//  The sstTestId_ array contains Service-Mode test ids.
//  The sstTestName_ table contains translation text of the test Name.
//  The sstTestData_ table contains formated tested data result.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::setSstTestNameTable_(void)
{
	CALL_TRACE("SstTestSubScreen::setSstTestNameTable_(void)");

	// First clear each entry in the array.
	for (Int32 i = 0; i < SmTestId::SST_TEST_MAX; i++)
	{
		sstTestName_[i] = NULL_STRING_ID;
		sstTestId_[i]	= SmTestId::TEST_NOT_START_ID;
	}

	Int testIndex=0;


	sstTestId_[testIndex] 	= SmTestId::SST_FS_CC_TEST_ID;
	sstTestData_[testIndex][0] = NULL;
	sstTestName_[testIndex++]	= MiscStrs::SM_SST_FS_CC_TEST_ID;
	
	sstTestId_[testIndex] 	= SmTestId::CIRCUIT_PRESSURE_TEST_ID;
	sstTestData_[testIndex][0] = NULL;
	sstTestName_[testIndex++]	= MiscStrs::SM_CIRCUIT_PRESSURE_TEST_ID;	// SM_PRESSURE_SENSOR_TEST_ID;

	sstTestId_[testIndex] 	= SmTestId::SST_LEAK_TEST_ID;
	sstTestData_[testIndex][0] = NULL;
	sstTestName_[testIndex++]	= MiscStrs::SM_SST_LEAK_TEST_ID;

	sstTestId_[testIndex] 	= SmTestId::SST_FILTER_TEST_ID;
	sstTestData_[testIndex][0] = NULL;
	sstTestName_[testIndex++]	= MiscStrs::SM_SST_FILTER_TEST_ID;
	
	sstTestId_[testIndex] 	= SmTestId::CIRCUIT_RESISTANCE_TEST_ID;
	sstTestData_[testIndex][0] = NULL;
	sstTestName_[testIndex++]	= MiscStrs::SM_CIRCUIT_RESISTANCE_TEST_ID;
	
	sstTestId_[testIndex] 	= SmTestId::COMPLIANCE_CALIB_TEST_ID;
	sstTestData_[testIndex][0] = NULL;
	sstTestName_[testIndex++]	= MiscStrs::SM_COMPLIANCE_CALIB_TEST_ID;

	sstTestId_[testIndex] 	= SmTestId::SM_SST_PROX_TEST_ID;
	sstTestData_[testIndex][0] = NULL;
	sstTestName_[testIndex++]	= MiscStrs::SM_SST_PROX_STRING_ID;

	maxServiceModeTest_ = testIndex;

#ifdef SIGMA_DEVELOPMENT

	// Make sure total test count is within the maximum allowed.
	CLASS_ASSERTION(maxServiceModeTest_ <= SST_TEST_MAX);

	// Do some safe assertions that all entries in the array have been filled
	for (i = 0; i < maxServiceModeTest_; i++)
	{
		CLASS_ASSERTION(sstTestName_[i] != NULL_STRING_ID);
		CLASS_ASSERTION(sstTestId_[i]	!= SmTestId::TEST_NOT_START_ID);
	}
#endif // SIGMA_DEVELOPMENT
											 // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSstTestPromptTable_
//
//@ Interface-Description
//  Initialize the sstTestPromptId_, and sstTestPromptName_ tables.  
//  The sstTestPromptId_ array contains Service-Mode prompt ids.
//  The sstTestPromptName_ table contains translation text of Service-Mode
//	prompt code.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::setSstTestPromptTable_(void)
{
	CALL_TRACE("SstTestSubScreen::setSstTestPromptTable_(void)");

	// First clear each entry in the array.
	for (Int32 i = 0; i < NUM_SST_TEST_PROMPT_MAX; i++)
	{
		sstTestPromptId_[i]		= SmPromptId::NULL_PROMPT_ID;
		sstTestPromptName_[i] 	= NULL_STRING_ID;
	}

	Int promptIndex = 0;
	
	sstTestPromptId_[promptIndex]		= SmPromptId::CONNECT_O2_PROMPT;
	sstTestPromptName_[promptIndex++] 	= PromptStrs::SM_CONNECT_O2_P;
	
	sstTestPromptId_[promptIndex]		= SmPromptId::CONNECT_AIR_PROMPT;
	sstTestPromptName_[promptIndex++] 	= PromptStrs::SM_CONNECT_AIR_P;
	
	sstTestPromptId_[promptIndex]		= SmPromptId::UNBLOCK_WYE_PROMPT;
	sstTestPromptName_[promptIndex++] 	= PromptStrs::SM_UNBLOCK_WYE_P;
	
	sstTestPromptId_[promptIndex]		= SmPromptId::BLOCK_WYE_PROMPT;
	sstTestPromptName_[promptIndex++] 	= PromptStrs::SM_BLOCK_WYE_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::IS_HUMIDIFIER_WET_PROMPT;
	sstTestPromptName_[promptIndex++] 	= PromptStrs::SM_IS_HUMIDIFIER_WET_P;
	
	sstTestPromptId_[promptIndex]		= SmPromptId::ATTACH_CIRCUIT_PROMPT;
	sstTestPromptName_[promptIndex++] 	= PromptStrs::SM_ATTACH_CIRCUIT_P;
	
	sstTestPromptId_[promptIndex]		= SmPromptId::CONCT_TUBE_BFR_EXH_FLTR_PROMPT;
	sstTestPromptName_[promptIndex++] 	= PromptStrs::SM_CONCT_TUBE_BFR_EXH_FLTR_P;
	
	sstTestPromptId_[promptIndex]		= SmPromptId::DISCNT_TUBE_BFR_EXH_FLTR_PROMPT;
	sstTestPromptName_[promptIndex++] 	= PromptStrs::SM_DISCNT_TUBE_BFR_EXH_FLTR_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::CONNECT_HUMIDIFIER_PROMPT;
	sstTestPromptName_[promptIndex++] 	= PromptStrs::SM_CONNECT_HUMIDIFIER_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::PROX_SENSOR_PROMPT;
	sstTestPromptName_[promptIndex++]   = PromptStrs::SM_PROX_SENSOR_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::CONNECT_PROX_SENSOR_PROMPT;
	sstTestPromptName_[promptIndex++]   = PromptStrs::SM_CONNECT_PROX_SENSOR_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::REMOVE_INSP_FILTER_PROMPT;
	sstTestPromptName_[promptIndex++]   = PromptStrs::SM_REMOVE_INSP_FILTER_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::BLOCK_EXP_PORT_PROMPT;
	sstTestPromptName_[promptIndex++]   = PromptStrs::SM_BLOCK_EXP_PORT_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::PX_REMOVE_INSP_FILTER_PROMPT;
	sstTestPromptName_[promptIndex++]   = PromptStrs::SM_PX_REMOVE_INSP_FILTER_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::PX_REMOVE_EXP_LIMB_PROMPT;
	sstTestPromptName_[promptIndex++]   = PromptStrs::SM_REMOVE_EXP_LIMB_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::PX_UNBLOCK_SENSOR_OUTPUT_PROMPT;
	sstTestPromptName_[promptIndex++]   = PromptStrs::SM_UNBLOCK_PROX_SENSOR_OUTPUT_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::PX_BLOCK_SENSOR_OUTPUT_PROMPT;
	sstTestPromptName_[promptIndex++]   = PromptStrs::SM_BLOCK_PROX_SENSOR_OUTPUT_P;

	sstTestPromptId_[promptIndex]		= SmPromptId::UNBLOCK_EXP_PORT_PROMPT;
	sstTestPromptName_[promptIndex++]   = PromptStrs::SM_UNBLOCK_EXP_PORT_P;
	

#ifdef SIGMA_DEVELOPMENT

	// Make sure total test count is within the maximum allowed.
	CLASS_ASSERTION(testIndex == NUM_SST_TEST_PROMPT_MAX);

	// Do some safe assertions that all entries in the array have been filled
	for (i = 0; i < NUM_SST_TEST_PROMPT_MAX; i++)
	{
		CLASS_ASSERTION(sstTestPromptId_[i] != SmPromptId::NULL_PROMPT_ID);
		CLASS_ASSERTION(sstTestPromptName_[i] != NULL_STRING_ID);
	}
#endif // SIGMA_DEVELOPMENT
													 // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSstTestStatusTable_
//
//@ Interface-Description
// 	Sets the status text in the status table.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::setSstTestStatusTable_(void)
{
	CALL_TRACE("SstTestSubScreen::setSstTestStatusTable_(void)");

#if defined FORNOW
printf("setSstTestStatusTable_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// Get Sst test result table from NovRam
    NovRamManager::GetSstResultEntries(sstResultTable_);

#if defined FORNOW
printf("   After NovRamManager::GetSstResultEntries(sstResultTable_)\n");
#endif	// FORNOW

	for (Int idx = 0; idx < SmStatusId::NUM_TEST_DATA_STATUS; idx++)
	{
		sstStatusDecodingTable_[idx] = -1;
	}

	// Setup the status decoding table information
	sstStatusDecodingTable_[SmStatusId::TEST_END] = PASSED_TEST_ID;
	sstStatusDecodingTable_[SmStatusId::TEST_START] = UNDEFINED_TEST_RESULT_ID;
	sstStatusDecodingTable_[SmStatusId::TEST_ALERT] = MINOR_TEST_FAILURE_ID;
	sstStatusDecodingTable_[SmStatusId::TEST_FAILURE] = MAJOR_TEST_FAILURE_ID;
	sstStatusDecodingTable_[SmStatusId::TEST_OPERATOR_EXIT] = INCOMPLETE_TEST_RESULT_ID;
	sstStatusDecodingTable_[SmStatusId::NOT_INSTALLED] = NOT_INSTALLED_TEST_ID;
													 // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: formatStatusOutcome_
//
//@ Interface-Description
//	Called when we are need to update the status result.  We passed the
//  "resultData" from which we can retrieve the overall test result or
//  the result of current run.  The "tmpBuffer" is where we store the
//  formatted status result.  The "isOutcome" flag tells us whether we
//  are formatting a overall or a current status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Based on the result of the SST test, and the "isOutcome" flag we 
//	format the test result string accordingly.
// $[01365] If a test is stopped prior to completion, its status shall be...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
SstTestSubScreen::formatStatusOutcome_(ResultEntryData resultData, wchar_t *tmpBuffer, Boolean isOutcome)
{
	CALL_TRACE("formatStatusOutcome_(ResultEntryData resultData, char *tmpBuffer, Boolean isOutcome)");

#if defined FORNOW
printf("formatStatusOutcome_ at line  %d\n", __LINE__);
#endif	// FORNOW

	TestResultId currentTestResultId=resultData.getResultOfCurrRun();
			
#if defined FORNOW
printf("   currentTestResultId = %d\n", currentTestResultId);
#endif	// FORNOW


	if (isOutcome)
	{										// $[TI1]
		// overall Test Result
		switch (currentTestResultId)
		{
		case PASSED_TEST_ID:				// $[TI1.1]
			wcscpy(tmpBuffer, MiscStrs::PASSING_OVERALL_MSG);
			break;
			
		case MINOR_TEST_FAILURE_ID:			// $[TI1.2]
			wcscpy(tmpBuffer, MiscStrs::ALERTING_OVERALL_MSG);
			break;
			
		case MAJOR_TEST_FAILURE_ID:			// $[TI1.3]	
			wcscpy(tmpBuffer, MiscStrs::FAILURE_OVERALL_MSG);
			break;
			
		case OVERRIDDEN_TEST_ID:			// $[TI1.4]
			wcscpy(tmpBuffer, MiscStrs::OVERRIDEN_OVERALL_MSG);
			break;
			
		case INCOMPLETE_TEST_RESULT_ID:		// $[TI1.5]
			wcscpy(tmpBuffer, MiscStrs::IMCOMPLETE_OVERALL_MSG);
			break;
		case UNDEFINED_TEST_RESULT_ID:		// $[TI1.6]
			wcscpy(tmpBuffer, MiscStrs::UNDEFINED_OVERALL_MSG);
			break;
		case NOT_APPLICABLE_TEST_RESULT_ID:	// $[TI1.7]
		case NOT_INSTALLED_TEST_ID:			// $[TI1.8]
			tmpBuffer[0] = NULL;
			break;
			
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}
	}
	else
	{										// $[TI2]
		// Test Result
		switch (currentTestResultId)
		{								
		case PASSED_TEST_ID:				// $[TI2.1]
			wcscpy(tmpBuffer, MiscStrs::PASSED_STATUS_MSG);
			break;
			
		case OVERRIDDEN_TEST_ID:			 // $[TI2.2]

		case MINOR_TEST_FAILURE_ID:			 // $[TI2.3]
			wcscpy(tmpBuffer, MiscStrs::ALERT_STATUS_MSG);
			break;
			
		case MAJOR_TEST_FAILURE_ID:			 // $[TI2.4]
			wcscpy(tmpBuffer, MiscStrs::FAILED_STATUS_MSG);
			break;
			
		case NOT_INSTALLED_TEST_ID:			 // $[TI2.5]
			wcscpy(tmpBuffer, MiscStrs::NOT_INSTALLED_STATUS_MSG);
			break;
			
		case INCOMPLETE_TEST_RESULT_ID:		 // $[TI2.6]
		case NOT_APPLICABLE_TEST_RESULT_ID:	 // $[TI2.7]

		case UNDEFINED_TEST_RESULT_ID:		 // $[TI2.8]
			tmpBuffer[0] = NULL;
			break;
			
		default:
			CLASS_ASSERTION_FAILURE();
			break;
		}
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//	Preparation step for a test to start.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set various flags; Hide the data/time/ diagnostic code and status
//	log cells; Update the prompt area; Erase the test data result for
//	repeated test; Setup the upper screen layout to display the Leak
//	graphic; Set data format then do the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::startTest_(void)
{
	CALL_TRACE("SstTestSubScreen::startTest_(void)"); 

#if defined FORNOW
printf("startTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// High light the test item
	pLog_->setSelectedEntry(sstCurrentTestIndex_);

	// Hide the following drawables
	repeatButton_.setShow(FALSE);
	nextButton_.setShow(FALSE);
	restartButton_.setShow(FALSE);

	// Signal to clear the log cell infos.
	clearLogEntry_ = TRUE;

	// $[01277] When the operator commands to return an SST test ...
	// Hide the data/time, diagnostic code and status cells
	pLog_->updateEntryColumn(sstCurrentTestIndex_, COL_1_TIME);
	pLog_->updateEntryColumn(sstCurrentTestIndex_, COL_3_DIAG_CODE);
	pLog_->updateEntryColumn(sstCurrentTestIndex_, COL_4_RESULT);

	// Signal to display the log cell infos.
	clearLogEntry_ = FALSE;

	// Display the overall testing status on the service status area
	sstStatusStr_.setText(MiscStrs::EST_RUNNING_MSG);

	// Clear the SST outcome due to major failure.
	if (mostSevereStatusId_ == MAJOR_TEST_FAILURE_ID)
	{								// $[TI1]
		// Hide test completed time stamp and SST outcome
		sstCompletedLabel_.setShow(FALSE);
		sstOutcomeLabel_.setShow(FALSE);
		sstCompletedValue_.setShow(FALSE);
		sstOutcomeValue_.setShow(FALSE);

		// Show the overall testing status on the service status area
		sstStatusStr_.setShow(TRUE);
	}								// $[TI2]

	// Reset the isThisSstTestCompleted_ for next test
	isThisSstTestCompleted_ = FALSE;
	
	// Reset the mostSevereStatusId_ for next test
	mostSevereStatusId_ = PASSED_TEST_ID;

	// Set Primary prompt to "Testing....."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::EST_TESTING_P);
	
	// Set the Advisory prompt to Testing is in progress.
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
		 PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
		PromptArea::PA_LOW, NULL_STRING_ID);

#if defined FORNOW	
printf("   sstCurrentTestIndex=%d, sstTestId_[%d]=%d, repeatTestRequested_=%d, sstTestData_[%d]=%s\n",
		sstCurrentTestIndex_, sstCurrentTestIndex_,
		sstTestId_[sstCurrentTestIndex_], repeatTestRequested_,
		sstCurrentTestIndex_, sstTestData_[sstCurrentTestIndex_]);
#endif	// FORNOW

	// Erase the test data result for repeated test.
	if (repeatTestRequested_)
	{									// $[TI3]
		sstTestData_[sstCurrentTestIndex_][0] = NULL;
	}									// $[TI4]

	// Setup the Upper Screen Area to display the leak graphic.
	setupUpperScreenLayout_(sstCurrentTestIndex_);
	setDataFormat_(sstTestId_[sstCurrentTestIndex_]);
	
	// Start the test.
#ifndef TEST_GUI_MISC
	SmManager::DoTest(sstTestId_[sstCurrentTestIndex_], repeatTestRequested_);
#endif	// TEST_GUI_MISC
	repeatTestRequested_ = FALSE;
}

		
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: nextTest_
//
//@ Interface-Description
//	Handles the test and interface control at the end of each test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, update the overallStatusId_ to reflect the most severe status
//	of all tests completed so far.  Then decide whether we have more tests
//	to do or it is at the end of the SST test and execute it.
//  
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::nextTest_(void)
{
	CALL_TRACE("SstTestSubScreen::nextTest_(void)");

#if defined FORNOW
printf("nextTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// Need to record the overall status id because the user had chosen
	// to ignore the errors.
	switch (overallStatusId_)
	{	// Only in NEXT state the overall status id needs to be updated.
	case PASSED_TEST_ID:					// $[TI1]
		overallStatusId_ = mostSevereStatusId_;
		break;
	case MINOR_TEST_FAILURE_ID:				// $[TI2]
		if (mostSevereStatusId_ == MAJOR_TEST_FAILURE_ID)
		{									// $[TI3]
			overallStatusId_ = mostSevereStatusId_;
		}									// $[TI4]
		break;
	case MAJOR_TEST_FAILURE_ID:				// $[TI5]
		break;
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
	// Set next test index
	sstCurrentTestIndex_++;

	if (sstCurrentTestIndex_ < maxServiceModeTest_)
	{	// More test to do.					// $[TI6]
		// This is purposely done to force a automatically generated
		// state to happen.
		nextState_ = getNextState_(AUTO_EVENT);
	}	
	else
	{	// End of the test.					// $[TI7]
		sstCurrentTestIndex_--;
		nextState_ = getNextState_(AUTO_END_OF_TEST_EVENT);
	}

	executeState_();
}

		
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: repeatOrNextTest_
//
//@ Interface-Description
//	Provide the user an selection to either repeat the same test or proceed
//  to next test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the appropriate prompt, status and repeat/next/restart buttons.
//
// $[01275] Following an SST test alert (minor finding), the operator...
//---------------------------------------------------------------------
//@ PreCondition
//	none 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::repeatOrNextTest_(void)
{
	CALL_TRACE("SstTestSubScreen::repeatOrNextTest_(void)");

#if defined FORNOW
printf("repeatOrNextTest_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// Set Primary prompt to "Touch a button....."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::TOUCH_A_BUTTON_P);
	
	// Display the overall testing status on the service status area
	sstStatusStr_.setText(MiscStrs::EST_HALTED_MSG);

	// Show the following drawables
	repeatButton_.setShow(TRUE);

	// change the next button title to complete
	if (sstCurrentTestIndex_ == SmTestId::SST_TEST_PROX_ID)
	{
		nextButton_.setTitleText(MiscStrs::SST_COMPLETE_MSG,GRAVITY_CENTER);
	}

	nextButton_.setShow(TRUE);

	restartButton_.setShow(TRUE);
												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayPausingRequest_
//
//@ Interface-Description
//	Provide user the feedback when the EXIT key is pressed.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Grep the NonPersistentFocus, disable the knob sound, and set advisory/
//  secondry prompts.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::displayPausingRequest_(void)
{
	CALL_TRACE("SstTestSubScreen::displayPausingRequest_(void)");

#if defined FORNOW
printf("displayPausingRequest_ at line  %d\n", __LINE__);
#endif	// FORNOW


	// Grab Adjust Panel focus
	// $[01360] Sigma is capable of creating the following user interface sound.
	AdjustPanel::TakeNonPersistentFocus(this, TRUE);

	// Disable the knob sound.
	AdjustPanel::DisableKnobRotateSound();
	//
	// Set primary prompt to "Press ACCEPT to confirm."
	//
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
						PromptStrs::SST_PRESS_ACCEPT_TO_CONFIRM_P);

	// Set advisory prompt to "Exit SST is requested"
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
						PromptStrs::SST_USER_REQUESTING_EXIT_A);

	// Set secondry prompt to "To cancel: touch EXIT SST."
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_HIGH, PromptStrs::SST_TEST_CANCEL_S);
												// $[TI1]
}

		
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayFailure_
//
//@ Interface-Description
//	Provide user the feedback of a failed test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Display advisory/secondry prompts, show the repeat/restart buttons and
//  display the SST outcome.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::displayFailure_(void)
{
	CALL_TRACE("SstTestSubScreen::displayFailure_(void)");

#if defined FORNOW
printf("displayFailure_ at line  %d\n", __LINE__);
#endif	// FORNOW

	// Set Primary prompt to "Touch a button....."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
				PromptArea::PA_LOW, PromptStrs::TOUCH_A_BUTTON_P);

	// Clear advisory prompt
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_LOW,	NULL_STRING_ID);

	// Set secondry prompt to "To cancel: touch EXIT SST."
	testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_LOW, PromptStrs::SST_EXIT_RESTART_S);

	// Show the following drawables
	restartButton_.setShow(TRUE);
	repeatButton_.setShow(TRUE);

	// Display the SST outcome and time stamp as directed by Service-Data
	// manager.
	displaySstResultOutcome_();
										// $[TI1]
}

		
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupUpperScreenLayout_
//
//@ Interface-Description
//	Provide user the feedback of real time Leak test result.
//---------------------------------------------------------------------
//@ Implementation-Description
//	For SST leak test, activate the Leak test subscreen on the
//	Service upper screen.  Else, deactivate the Leak test subscreen if
//	present.
// $[01287] During the SST leak test, a real time indication ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::setupUpperScreenLayout_(Int sstCurrentTestIndex)
{
	CALL_TRACE("SstTestSubScreen::setupUpperScreenLayout_(Int sstCurrentTestIndex)");
#if defined FORNOW	
printf("setupUpperScreenLayout_ at line  %d\n", __LINE__);
#endif	// FORNOW

	switch (sstTestId_[sstCurrentTestIndex])
	{
	case SmTestId::SST_LEAK_TEST_ID:				// $[TI1]
		// Activate the SstLeakSubScreen
		pUpperSubScreenArea_->activateSubScreen(
					UpperSubScreenArea::GetSstLeakSubScreen(), NULL);
		break;

	case SmTestId::CIRCUIT_RESISTANCE_TEST_ID:				// $[TI2]

	case SmTestId::SST_FILTER_TEST_ID:				// $[TI3]

	case SmTestId::CIRCUIT_PRESSURE_TEST_ID:		// $[TI4]

	case SmTestId::SST_FS_CC_TEST_ID:				// $[TI5]

	case SmTestId::COMPLIANCE_CALIB_TEST_ID:		// $[TI6]
		// Deactive upper subscreen area.
		if (pUpperSubScreenArea_->isCurrentSubScreen(
						UpperSubScreenArea::GetSstLeakSubScreen()))
		{											// $[TI7]
			pUpperSubScreenArea_->deactivateSubScreen();
		}											// $[TI8]
		break;

	case SmTestId::SM_SST_PROX_TEST_ID:
		// Activate the SstProxSubScreen
		pUpperSubScreenArea_->activateSubScreen(pSstProxSubScreen_, NULL);
		break;

	default:
		CLASS_ASSERTION_FAILURE();
	break;
	}
}
	

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setDataFormat_
//
//@ Interface-Description
//	Provide a mean to set the precision of the SST test data.
//---------------------------------------------------------------------
//@ Implementation-Description
//	For each piece of data that we are instered to display on the EVENT
//  column, we need to set the precision_ for this test data id through
//  the collaboration of the testManager.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::setDataFormat_(SmTestId::ServiceModeTestId testId)
{
	CALL_TRACE("SstTestSubScreen::setDataFormat_(SmTestId::ServiceModeTestId testId)");

#if defined FORNOW
printf("SstResultSubScreen::setDataFormat_() at line  %d\n", __LINE__);
#endif	// FORNOW

	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	// Initialize all the following items
	for (Int idx = 0; idx < TestDataId::MAX_DATA_ENTRIES; idx++)
	{
		testManager_.setPrecision((SmDataId::ItemizedTestDataId) idx, 0);
	}

	switch (testId)
	{
	case SmTestId::CIRCUIT_RESISTANCE_TEST_ID:					// $[TI3]
		testManager_.setPrecision(SmDataId::CIRCUIT_RESISTANCE_INSP_PRESSURE, 2);
		testManager_.setPrecision(SmDataId::CIRCUIT_RESISTANCE_EXP_PRESSURE, 2);
		break;
		
	case SmTestId::SST_FILTER_TEST_ID:					// $[TI4]
		testManager_.setPrecision(SmDataId::SST_FILTER_DELTA_PRESSURE, 2);
	break;
	
	case SmTestId::SST_LEAK_TEST_ID:					// $[TI5]
		testManager_.setPrecision(SmDataId::SST_LEAK_INSPIRATORY_PRESSURE1, 2);
		testManager_.setPrecision(SmDataId::SST_LEAK_MONITOR_PRESSURE, 2);
		testManager_.setPrecision(SmDataId::SST_LEAK_INSPIRATORY_PRESSURE2, 2);
		testManager_.setPrecision(SmDataId::SST_LEAK_DELTA_PRESSURE, 2);
		break;
		
	case SmTestId::CIRCUIT_PRESSURE_TEST_ID:			// $[TI6]
		break;
		
	case SmTestId::SST_FS_CC_TEST_ID:					// $[TI7]
		break;
		
	case SmTestId::COMPLIANCE_CALIB_TEST_ID:			// $[TI8]
		testManager_.setPrecision(SmDataId::COMPLIANCE_CALIB_NOMINAL_COMPLIANCE, 2);
		break;

	case SmTestId::SM_SST_PROX_TEST_ID:
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;		
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displayPrompt_
//
//@ Interface-Description
//	Provide user the feedback of displaying the prompt requested from
//  the Service-Mode subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Based on the "keyAllowedId", we grab the non-persistent focus, disable
//  the knob rotate sound, set the prompts, and display the overall testing
//  status on the service status area
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::displayPrompt_(void)
{
	CALL_TRACE("SstTestSubScreen::displayPrompt_(void)");

#if defined FORNOW
printf("displayPrompt_ at line  %d, keyAllowedId_ = %d\n", __LINE__, keyAllowedId_);
#endif	// FORNOW

	// Search the corresponding prompt string according to the promptId_

	Int idx;
	for (idx = 0;
			 idx < NUM_SST_TEST_PROMPT_MAX && sstTestPromptId_[idx] != promptId_;
			 idx++);
	
	CLASS_ASSERTION(idx < NUM_SST_TEST_PROMPT_MAX);

#if defined FORNOW
printf("SstTestSubScreen::displayPrompt_() clear advisory prompt\n");
#endif	// FORNOW

	// Clear advisory prompt
	testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
						NULL_STRING_ID);

	switch (keyAllowedId_)
	{
	case SmPromptId::ACCEPT_KEY_ONLY:			// $[TI1]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();
	
		// Set the Secondary prompt to "When done: press Accept."
		// "To cancel SST: touch EXIT EXT, then Accept."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
							PromptStrs::SST_PROMPT_ACCEPT_CANCEL_S);
		break;

	case SmPromptId::ACCEPT_YES_AND_CLEAR_NO_ONLY:		// $[TI2]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();
	
		// Set the Secondary prompt to "If yes press ACCEPT; if no press CLEAR."
		// "To cancel SST: touch EXIT SST, then Accept."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
							PromptStrs::SST_PROMPT_ACCEPT_YES_AND_CLEAR_NO_S);
		break;
	case SmPromptId::IF_CONNECTED_ACCEPT_YES_AND_CLEAR_NO_ONLY:		// $[TI2]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();
	
		// Set the Secondary prompt to "If yes press ACCEPT; if no press CLEAR."
		// "To cancel SST: touch EXIT SST, then Accept."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY, PromptArea::PA_HIGH,
							PromptStrs::SST_PROMPT_IF_CONNECTED_ACCEPT_YES_AND_CLEAR_NO_S);
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

	// Set primary prompt according to the promptId_
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
							sstTestPromptName_[idx]);

	// Display the overall testing status on the service status area
	sstStatusStr_.setText(MiscStrs::EST_WAITING_MSG);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displaySstFinishedScreen_
//
//@ Interface-Description
//	Provide user the feedback of displaying the SST's completed screen.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Display appropriate prompts and buttons.  For MINOR FAILURE, we
//  will show the "Override" button.  The "Restart" button is always
//  displayed at the end of SST test.  Display the overall testing
//  status is also necessary.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::displaySstFinishedScreen_(void)
{
	CALL_TRACE("SstTestSubScreen::displaySstFinishedScreen_(void)");

#if defined FORNOW
printf("displaySstFinishedScreen_ at line  %d\n", __LINE__);
#endif	// FORNOW


	ResultEntryData overallSstResultData;

	// Get the overall testing status
	NovRamManager::GetOverallSstResult(overallSstResultData);

	TestResultId currentTestResultId=overallSstResultData.getResultOfCurrRun();


	// Set Primary prompt to "Finished SST testes."
	testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
			PromptArea::PA_LOW, PromptStrs::SST_DONE_P);

	if (currentTestResultId == MINOR_TEST_FAILURE_ID)
	{								// $[TI1]
		// Set Secondary prompt to "Touch OVERRIDE to ignore minor failure,
		//							To restart: touch RESTART SST."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, PromptStrs::SST_RESTART_OVERRIDE_S);

		// Show the following drawables
		overrideButton_.setShow(TRUE);
	}
	else
	{								// $[TI2]
		//#########ACCORDING TO CHARLIE, THE SST SHOULD REBOOT THE SYSTEM
		// AT THIS POINT.
		// Clear the Secondary prompt.
		// "To ventilate: touch EXIT SST."
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
					PromptArea::PA_LOW, NULL_STRING_ID);

		// Hide the following drawables
		overrideButton_.setShow(FALSE);
	}

	// Show the following drawables
	restartButton_.setShow(TRUE);

	// Hide the following drawables
	repeatButton_.setShow(FALSE);
	nextButton_.setShow(FALSE);

	// Unhighlight the entry.
	pLog_->clearSelectedEntry();

	// Display the SST completion time stamp and outcome.
	displaySstResultOutcome_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: displaySstResultOutcome_
//
//@ Interface-Description
//	Provide user the feedback of displaying the SST's overall status result.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Show test completed time stamp and overall SST outcome.
// $[01285] The SST status subscreen shall display the following ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
 
void
SstTestSubScreen::displaySstResultOutcome_(void)
{
	CALL_TRACE("SstTestSubScreen::displaySstResultOutcome_(void)");

	ResultEntryData overallSstResultData;
	wchar_t resultBuffer[256];  
	wchar_t resultBuffer2[256];  

	// Show test completed time stamp and SST outcome
	sstCompletedLabel_.setShow(TRUE);
	sstOutcomeLabel_.setShow(TRUE);
	sstCompletedValue_.setShow(TRUE);
	sstOutcomeValue_.setShow(TRUE);

	// Hide the overall testing status on the service status area
	sstStatusStr_.setShow(FALSE);

	// Get the overall testing status
	NovRamManager::GetOverallSstResult(overallSstResultData);

	TestResultId currentTestResultId=overallSstResultData.getResultOfCurrRun();

#if defined FORNOW
printf("displaySstResultOutcome_ at line  %d, currentTestResultId = %d\n",
						__LINE__, currentTestResultId);
#endif	// FORNOW

	CLASS_ASSERTION(currentTestResultId != NOT_APPLICABLE_TEST_RESULT_ID);

   	// Format sst outcome
	formatStatusOutcome_(overallSstResultData, resultBuffer2, TRUE);

	// Format Status outcome.
	swprintf(resultBuffer, L"{%s%s}", MiscStrs::SST_OUTCOME_VALUE,  
								resultBuffer2);
									
	sstOutcomeValue_.setText(resultBuffer);
		
	if (currentTestResultId != UNDEFINED_TEST_RESULT_ID)
	{									// $[TI1]

		// Format date and time
		const TimeStamp & timeStamp = overallSstResultData.getTimeOfResult();

		wchar_t format[30];  
		swprintf(format, L"%%T %s", MiscStrs::LOG_DATE_FORMAT);  
		TextUtil::FormatTime(resultBuffer2, format, timeStamp);
	
		// Format the COMPLETION time stamp.
		swprintf(resultBuffer, L"{%s%s}", MiscStrs::SST_COMPLETED_VALUE,  
								resultBuffer2);
		sstCompletedValue_.setText(resultBuffer);
	}									// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getNextState_
//
//@ Interface-Description
//	Handles the state transition based on the latest coming event.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Update newEvent_, previousState_ and nextState_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Int
SstTestSubScreen::getNextState_(Int newEvent)
{
	CALL_TRACE("SstTestSubScreen::getNextState_(Int newEvent)");

#if defined FORNOW
printf("getNextState_ at line %d, state = %d, event = %d\n",
			 __LINE__, nextState_, newEvent);
#endif	// FORNOW
			 
	if (!isVisible())
	{													// $[TI1]
		return(nextState_);
	}													// $[TI2]

	newEvent_ = newEvent;
			 
	previousState_ = nextState_;
	return(sstStateTable_[previousState_][newEvent]);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: executeState_
//
//@ Interface-Description
//	Handles the execution of state transition request.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Based on "nextState_", appropriated processing (screen update, control
//  of test flow, etc) will be executed to maintain the requirements for
//  this state transition process.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::executeState_(void)
{
	CALL_TRACE("SstTestSubScreen::executeState_(void");

#if defined FORNOW
printf("executeState_ at line %d, nowState = %d\n", __LINE__, nextState_);

printf("   BEGIN executeState_ Recursion count=%d, nowState = %d, event=%d\n",
				RecursionCount_, nextState_, newEvent_);
#endif	// FORNOW
				
RecursionCount_++;

	if (!isVisible())
	{													// $[TI1]
		return;
	}													// $[TI2]

	switch (nextState_)
	{
	case INITIAL_STATE:						// $[TI3]
		// Do nothing.
		break;

	case READY_TO_TEST_STATE:				// $[TI4]
		// The initial test screen will be displayed correctly.
		layoutScreen_(SST_START_SCREEN);

		// Initialize the overall status.
		overallStatusId_ = PASSED_TEST_ID;
		
		// This is purposely done to force a automatically generated
		// state to happen.
		nextState_ = getNextState_(AUTO_EVENT);
		
		// These two states have to be in this sequence so that after
		// the setup of the initial testing screen, the first test
		// shall start automatically.
	case DO_TEST_STATE:						// $[TI5]
		startTest_();
		break;
		
	case DO_NEXT_STATE:						// $[TI6]
		nextTest_();
		break;
		
	case PROMPT_STATE:						// $[TI7]
		displayPrompt_();
		break;
		
	case TEST_PASS_STATE:					// $[TI8]
		// Test is successful without user exiting.  Start 2 second
		// waiting before the next test starts.
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
		break;
		
	case EXIT_BEFORE_PROMPT_STATE:			// $[TI9]
		// Display the overall testing status on the service status area
		sstStatusStr_.setText(MiscStrs::EST_PAUSING_MSG);
		displayPausingRequest_();
		setupFallbackState_();
		break;
		
	case EXECUTE_PROMPT_STATE:				// $[TI10]
		nextState_ = DO_TEST_STATE;
		promptId_ = SmPromptId::NULL_PROMPT_ID;

		// Signal the Service Test Manager to continue the test.
		SmManager::OperatorAction(sstTestId_[sstCurrentTestIndex_],
						  userKeyPressedId_);
						
		// Display the overall testing status on the service status area
		sstStatusStr_.setText(MiscStrs::EST_RUNNING_MSG);
		break;
		
	case END_ALERT_STATE:					// $[TI11]
		repeatOrNextTest_();
		break;
		
	case END_FAILURE_STATE:					// $[TI12]
		displayFailure_();
		break;
		
	case END_EXIT_ALERT_STATE:				// $[TI13]
		// At the end of test with an ALERT error and EXIT request
		setupFallbackState_();
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
		break;
		
	case END_EXIT_FAILURE_STATE:			// $[TI14]
		// At the end of test with an FAILURE error and EXIT request
		setupFallbackState_();
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
		break;
		
	case END_EXIT_TEST_STATE:				// $[TI15]
		// At the end of test with EXIT requested
		setupFallbackState_();
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
		break;
		
	case EXIT_REQUEST_STATE:				// $[TI16]
		// SM test has completed.  Confirm the request with users.
		overrideRequested_ = FALSE;
		displayPausingRequest_();
		break;
		
	case PAUSE_REQUEST_STATE:				// $[TI17]
		// Set Primary prompt to "Waiting for test to complete...."
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW, 
							PromptStrs::EST_WAIT_TEST_TO_COMPLETE_P);
		setupFallbackState_();
		break;
		
	case EXIT_FALLBACK_STATE:				// $[TI18]
		// Make sure that nothing in this subscreen keeps focus.
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Clear the high priority prompts
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
							NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
							PromptArea::PA_HIGH, NULL_STRING_ID);

		// Reset the nextState_
#if defined FORNOW		
printf("   fallBackState = %d at line  %d\n", fallBackState_, __LINE__);
#endif	// FORNOW
			
		nextState_ = fallBackState_;

		if (nextState_ == DO_TEST_STATE)
		{									// $[TI18.1]
			// This is a normal test condition, user just pressed Exit buttom twice
			// And there was no prompt required user to process.  We are already in
			// the right state, so do nothing.
		}
		else
		{									// $[TI18.2]
			// Force into the appropriate state when the next state is not in
			// DO_TEST_STATE or in any other state.
			executeState_();
		}
		break;

	case END_OF_SST_STATE:					// $[TI19]
		if (nextState_ != fallBackState_)
		{									// $[TI20]	
			// Reach END_OF_SST_STATE not by fallback mechanism.
			// Let the service mode  manager knows that completed status
			// has been issued.
			SmManager::DoFunction(SmTestId::SST_COMPLETE_ID);

			const DiscreteValue  HUMID_TYPE_VALUE =
						SettingContextHandle::GetSettingValue(
												ContextId::ACCEPTED_CONTEXT_ID,
												SettingId::HUMID_TYPE
															 );

			// $[01276] On successful completion of circuit calibration ...
			// Update the last humidifier type
			NovRamManager::UpdateLastSstHumidifierType(HUMID_TYPE_VALUE);

			//If SST has been completed then take down SstProxSubScreen
			pSstProxSubScreen_->deactivate();

			displaySstFinishedScreen_();
		}										// $[TI21]
		break;
		
	case RESTART_STATE:							// $[TI22]

	case REPEAT_STATE:							// $[TI23]
	case NEXT_STATE:							// $[TI24]
	case OVERRIDE_STATE:						// $[TI25]
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();
	
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_HIGH,
							PromptStrs::SST_PRESS_ACCEPT_TO_CONFIRM_P);

		if (nextState_ == OVERRIDE_STATE || nextState_ == RESTART_STATE)
		{										// $[TI26]
			//
			// Set advisory prompt to empty string.
			// secondary prompt to "To cancel: touch EXIT SST."
			//
			testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
								NULL_STRING_ID);
			testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
								PromptArea::PA_HIGH, PromptStrs::SST_TEST_CANCEL_S);

			overrideRequested_ = TRUE;
		}
		else
		{										// $[TI27]
			//
			// Set primary prompt to "Press ACCEPT to confirm.", and set
			// advisory prompt to "automatic testing will begin."
			//
			if (sstCurrentTestIndex_ != SmTestId::SST_TEST_PROX_ID)
			{
				testManager_.setTestPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_HIGH,
									PromptStrs::SST_AUTO_TEST_WILL_BEGIN_A);
				testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
									PromptArea::PA_HIGH, NULL_STRING_ID);
			}
		}

		if( nextState_ == RESTART_STATE || 
			(nextState_ == REPEAT_STATE && sstCurrentTestIndex_ == SmTestId::SST_TEST_PROX_ID )
			)
		{
			//If Prox Test is going to be repeated or the whole SST is going to be 
			//restarted then do some housekeeping for SstProxSubScreen
			pSstProxSubScreen_->deactivate();
		}
		break;
		
	case EXECUTE_RESTART_STATE:					// $[TI28]
		// Clear adjustable panel target.
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Deactivate this subscreen.
		getSubScreenArea()->deactivateSubScreen();

		// Inform the GUI state registrar that there has been a change in
		// the state of the GUI.
		GuiEventRegistrar::GuiEventHappened(GuiApp::SERVICE_READY);
		break;

	case EXECUTE_REPEAT_STATE:					// $[TI29]
		repeatTestRequested_ = TRUE;
		nextState_ = getNextState_(AUTO_EVENT);
		executeState_();
		break;
		
	case EXECUTE_EXIT_SST_STATE:				// $[TI30]
		if (sstCurrentTestIndex_ == SmTestId::SST_NO_TEST_ID || isThisSstTestCompleted_)
		{										// $[TI31]
			nextState_ = getNextState_(USER_END_EVENT);
			executeState_();
		}
		else
		{										// $[TI32]
			// Signal the Service Test Manager to exit the SST test.
			SmManager::OperatorAction(sstTestId_[sstCurrentTestIndex_],
						  SmPromptId::USER_EXIT);
		}
		break;
		
	case EXIT_SST_STATE:						// $[TI33]
		// Hide EXIT RESTART and OVERRIDE buttons
		exitButton_.setShow(FALSE);
		restartButton_.setShow(FALSE);
		overrideButton_.setShow(FALSE);
		repeatButton_.setShow(FALSE);
		nextButton_.setShow(FALSE);

		// From this point the state machine is not active any more.
		nextState_ = UNDEFINED_NEXT_TEST;

		if (overallStatusId_ == MINOR_TEST_FAILURE_ID && overrideRequested_)
		{										// $[TI34]
			overallStatusId_ = OVERRIDDEN_TEST_ID;

			// Let the service mode  manager know that override status
			// has been issued.
			SmManager::DoFunction(SmTestId::SST_OVERRIDE_ID);
		}										// $[TI35]

		//
		// Set NULL to high priority prompts
		//
		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
						PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_HIGH, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_HIGH, NULL_STRING_ID);

		testManager_.setTestPrompt(PromptArea::PA_PRIMARY,
						PromptArea::PA_LOW, PromptStrs::SST_PLEASE_WAIT_P);
		testManager_.setTestPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_LOW, NULL_STRING_ID);
		testManager_.setTestPrompt(PromptArea::PA_SECONDARY,
						PromptArea::PA_LOW, NULL_STRING_ID);
	
		// Delay the rebooting of the system for 2 seconds so the NovRam
		// manager will have enough time to process the overall status.
		// Start the appropriated countdown timer for each test.
		GuiTimerRegistrar::StartTimer(GuiTimerId::SERVICE_MODE_TEST_PAUSE_TIMEOUT, this);
		break;
		
		
	case UNDEFINED_NEXT_TEST:					// $[TI36]
		break;

	case SST_STATE_NUM:							// [$TI37]
	default:
		CLASS_ASSERTION_FAILURE();
		break;

	}

RecursionCount_--;

#if defined FORNOW
printf("   END executeState_ Recursion count=%d, nextState = %d, event=%d\n",
					RecursionCount_, nextState_, newEvent_);
#endif	// FORNOW

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setupFallbackState_
//
//@ Interface-Description
//	Handles the reversal of the state transition request.
//---------------------------------------------------------------------
//@ Implementation-Description
// 	Depending on the value of nextState_ variable set the "fallBackState_",
//  so that if the user decide to change his mind of certain key press,
//  we can backup to the previous state and continue on.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::setupFallbackState_()
{
	CALL_TRACE("SstTestSubScreen::setupFallbackState_()");

#if defined FORNOW
printf ("setupFallbackState_ at line %d, nextState=%d\n",
			 __LINE__, nextState_);
#endif	// FORNOW

	switch( nextState_ )
	{			 
	case END_EXIT_ALERT_STATE:
											// $[TI1]
		fallBackState_ = END_ALERT_STATE;
		break;
	
	case END_EXIT_FAILURE_STATE:
											// $[TI2]
		fallBackState_ = END_FAILURE_STATE;
	   break;
	
	case END_EXIT_TEST_STATE:
											// $[TI3]
		fallBackState_ = DO_NEXT_STATE;
		break;

	case PAUSE_REQUEST_STATE:
											// $[TI4]
		fallBackState_ = DO_TEST_STATE;
		break;
	
	case EXIT_BEFORE_PROMPT_STATE:
											// $[TI5]
		fallBackState_ = PROMPT_STATE;
		break;
	
	case EXIT_REQUEST_STATE:
	case REPEAT_STATE:
	case NEXT_STATE:
	case OVERRIDE_STATE:
	case RESTART_STATE:
											// $[TI6]
		break;

	default:
 											// $[TI7]
	  	fallBackState_ = nextState_;
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SstTestSubScreen::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
			SSTTESTSUBSCREEN, lineNumber, pFileName, pPredicate);
}
