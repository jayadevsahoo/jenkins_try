/*****************************************************************************/
/*   Module Name:    screens.h                                            
/*   Purpose:      Contains structures and methods for modifying screen configurations                                 
/*                                                                          
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/

/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/
#ifndef _SCREENS
#define _SCREENS

#include "globals.h"

// structure for keeping track of parameter attributes on a panel
typedef struct tag_ONE_SINGLETRACE {
	HPNL hPanel;          // panel ID
	int nMeasuredParm;    // offset to current Parameter in Parameters table
	int nUnitOfMeasure;   // offset to current Unit of Measure in Units of Measure table
	int nFlowCorrectionMode; // offset to current Flow Correction Mode in Flow Correction Modes table
	int nGasTypeListItem; // offset to current Gas Type
	int nTempr;           // offset to current temperature Unit of Measure 
	                      //     in temperature Units of Measure table
	double dThreshold1;   // Threshold1 value
	double dThreshold2;   // Threshold2 value
	int nBTBpeakMode;     // offset to current BTBpeak Mode in BTBpeak Modes table
	int nXScaleFactor;    // offset to current X-Scale Factor in table of Xscale values
	int nYScaleFactor;    // offset to current Y-Scale Factor in table of YScale values
} ONE_SINGLETRACE;

// A TWO_SINGLETRACE screen displays two parameters
typedef struct tag_TWO_SINGLETRACE {
	ONE_SINGLETRACE Trace[2];
} TWO_SINGLETRACE;

// A THREE_SINGLETRACE screen displays three parameters
typedef struct tag_THREE_SINGLETRACE {
	ONE_SINGLETRACE Trace[3];
} THREE_SINGLETRACE;

// A DUALTRACE screen displays two parameters on a common chart
typedef struct tag_DUALTRACE {
	ONE_SINGLETRACE Trace[2];	
} DUALTRACE;

// A DATAWITHDUALTRACE screen displays three parameters, one dataonly panel and a dual trace chart panel
typedef struct tag_DATAWITHDUALTRACE {
	ONE_SINGLETRACE DataOnlyPanel;
	DUALTRACE DualTracePanel; 
} DATAWITHDUALTRACE;

// An All Parameters screen displays seven parameters
typedef struct tag_ALLPARMS {
	ONE_SINGLETRACE Trace[7];
} ALLPARMSDATAONLY;

// A displayed parm structure holds any one choice of the above screen structures
typedef union tag_DISPLAYEDPARM {
	ONE_SINGLETRACE SingleTrace;
	TWO_SINGLETRACE TwoSingleTrace;
	THREE_SINGLETRACE ThreeSingleTrace;
	DUALTRACE DualTrace;
	DATAWITHDUALTRACE DataWithDualTrace;
	ALLPARMSDATAONLY AllParms;
	ONE_SINGLETRACE VolAcc;
	DATAWITHDUALTRACE BrthTest;
} DISPLAYEDPARM;

// A screen configuration is a base panel and a displayed parm structure
typedef struct tag_CONFIGSCREEN {
	HPNL hBasePanel; // when the base panel is visible, the screen configuration on it is also visible
	                 // when the base panel is hidden, the screen configuration on it is also hidden
	DISPLAYEDPARM DisplayedParm; // the displayed parm panel(s) are 'child' panels of the base panel
} CONFIGSCREEN;

// The Current Screen structure identifies the currently selected screen 
// and stores attributes configurations for the available screen types
typedef struct tag_CURRENTSCREEN {
	int nScreenType;
	CONFIGSCREEN ScreenConfig[7];
} CURRENTSCREEN;

// Named offsets into the current screen structure table
#define ONE_SINGLETRACE_SCREEN    0
#define TWO_SINGLETRACE_SCREEN    1
#define THREE_SINGLETRACE_SCREEN  2
#define DUALTRACE_SCREEN          3
#define DATA_WITHDUALTRACE_SCREEN 4
#define ALLPARMS_SCREEN           5
#define VOL_ACC_SCREEN            6

// A chart X-Scale structure stores a chart X-Minimum, X-Maximum,
// Precision (digits after decimal) for the tick labels,
// Divisions (number of grid lines on the chart)
// Axis Text (X-axis label at bottom of chart)
typedef struct tag_CHARTXSCALE {
	int nXscaleItem;
	int nXMin;
	int nXMax;
	int nXPrecision; // ATTR_YPRECISION
	int nXDivisions; // ATTR_XDIVISIONS
	char *szAxisText; // Label to put under the chart X-Axis
} CHARTXSCALE;

// Named offsets into the X-Scale table
#define X1SECOND   0
#define X2SECONDS  1
#define X5SECONDS  2
#define X10SECONDS 3
#define X15SECONDS 4
#define X30SECONDS 5
#define X60SECONDS 6

#define XDEFAULTSECONDS X10SECONDS

extern CURRENTSCREEN CrntScrn;
extern int AllParmsParmOrder[];

extern CHARTXSCALE XscaleCommon[];

extern CHARTYSCALE HF_SLPM[];
extern int num_HF_SLPM;
extern CHARTYSCALE HF_LPM[];
extern int num_HF_LPM;
extern CHARTYSCALE HF_CFM[];
extern int num_HF_CFM;
extern UNITSCHARTYSCALE HF_SCALES[];

extern CHARTYSCALE LF_SLPM[];
extern int num_LF_SLPM;
extern CHARTYSCALE LF_LPM[];
extern int num_LF_LPM;
extern UNITSCHARTYSCALE LF_SCALES[];

extern CHARTYSCALE HP_PSIG[];
extern int num_HP_PSIG;
extern CHARTYSCALE HP_KPA[];
extern int num_HP_KPA;
extern CHARTYSCALE HP_INHG[];
extern int num_HP_INHG;
extern UNITSCHARTYSCALE HP_SCALES[];

extern CHARTYSCALE LP_CMH2O[];
extern int num_LP_CMH2O;
extern CHARTYSCALE LP_KPA[];
extern int num_LP_KPA;
extern CHARTYSCALE LP_INHG[];
extern int num_LP_INHG;
extern CHARTYSCALE LP_INH2O[];
extern int num_LP_INH2O;
extern CHARTYSCALE LP_MMHG[];
extern int num_LP_MMHG;
extern UNITSCHARTYSCALE LP_SCALES[];

extern CHARTYSCALE VOL_LITERS[];
extern int num_VOL_LITERS;
extern CHARTYSCALE VOL_ML[];
extern int num_VOL_ML;
extern UNITSCHARTYSCALE VOL_SCALES[];

extern CHARTYSCALE O2_PCT[];
extern int num_O2_PCT;
extern UNITSCHARTYSCALE O2_SCALES[];

extern CHARTYSCALE BAR_PSIA[];
extern int num_BAR_PSIA;
extern CHARTYSCALE BAR_ATM[];
extern int num_BAR_ATM;
extern CHARTYSCALE BAR_MBAR[];
extern int num_BAR_MBAR;
extern CHARTYSCALE BAR_BAR[];
extern int num_BAR_BAR;
extern UNITSCHARTYSCALE BAR_SCALES[];

int CreateSceens (int ScreenWidth, int ScreenHeight);
void AdjustMainPnlSize(int ScreenWidth, int ScreenHeight);

#ifndef _USEMACROS
void SetCrntScreenType(int nNewScreenType);
int GetCrntScreenType(void);
#else
#define SetCrntScreenType(nNewScreenType) (CrntScrn.nScreenType = nNewScreenType)
#define GetCrntScreenType() CrntScrn.nScreenType
#endif

void SetDisplayedParmDefaultConfiguration(CURRENTSCREEN *ScrnRcds, int nScreenType, int nParmNum, int nParmID, HPNL hPanel);
void CopyScreenConfiguration(CURRENTSCREEN *ScrnRcdsTarget, CURRENTSCREEN *ScrnRcdsSource);
void CopyParmAttrib(ONE_SINGLETRACE *Target, ONE_SINGLETRACE *Source);
void ConfigureScreen(CURRENTSCREEN *ScrnRcds, int nScreenType, int nParm1, int nParm2, int nParm3);
unsigned int GetSupportPnlBits(int nParmType);
void UpdateSupportVisibility(void);
int UpdateClock(void);
int UpdateDate(void);

#endif
