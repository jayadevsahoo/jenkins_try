#include "stdafx.h"
//#############################################################################
//
//  ButtonsThread.cpp - Implementation of the ButtonsThread_t class.
//
//#############################################################################
#if !defined(__SPLIT__GUI__) && !defined(__SPLIT__BD__PC__)
#include <windows.h>
#endif
#include "ButtonsThread.h"
#include "GlobalObjects.h"
#include "IntraVentGUIMsgids.h"
#include "ExceptionHandler.h"
#include "I2cThread.h"
#if defined(E600_GUI)
#include "TopBaseWindow.h"
#include "DevelopmentDlg.h"
#endif

static const int MIN_BUTTON_PRESSSED_COUNT = 2;
static const int MIN_BUTTON_RELEASED_COUNT = 2;

static const ULONG BUTTON_READ_INTERVAL    =   15;
extern  DevelopmentDlg_t    *DevelopmentScreen;
HANDLE  hMembraneButtonEvent = NULL;
#define MEMBRANE_BUTTON_PRESSED_NAME    TEXT("BUTTON_ON_IRQ_EVENT")

LPCTSTR lpMembraneButtonEventName = MEMBRANE_BUTTON_PRESSED_NAME;
DWORD dwMembraneButtonEventCreated;
static bool PowerButtonPushed = false;

//#############################################################################
//
//  ButtonsThread_t::ButtonsThread_t - ButtonsThread_t constructor
//
//#############################################################################

ButtonsThread_t::ButtonsThread_t()
{
	BtnEnable=FALSE;
	Running=false;
    int i;
    CountFromStart = 0;
// E600 BDIO     for (i=0; i<NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS; i++)
// E600 BDIO     {
// E600 BDIO         ButtonPressedCounter[i] = 0;
// E600 BDIO         ButtonReleasedCounter[i] = MIN_BUTTON_RELEASED_COUNT;
// E600 BDIO     }
// E600 BDIO     for (i=0; i<NUMBER_OF_MEMBRANE_BUTTONS; i++)
// E600 BDIO     {
// E600 BDIO         LogicalButtonPressed[i] = false;
// E600 BDIO         WakeupButtonsPressed[i] = false;
// E600 BDIO     }
    hMembraneButtonEvent = CreateEvent(NULL, FALSE, FALSE, lpMembraneButtonEventName);
	if(!hMembraneButtonEvent)
	{
		RETAILMSG(TRUE, (_T("ButtonsThread: Creating BUTTON_ON_IRQ_EVENT event failed\r\n")));
		return;
	}

// E600 BDIO 	IntraVentApplicationThread.RegisterMessageUser(this, INTRAVENT_MSG_PB_SHUTDOWN); // For GUI only

}


//#############################################################################
//
//  ButtonsThread_t::~ButtonsThread_t - ButtonsThread_t destructor
//
//#############################################################################

ButtonsThread_t::~ButtonsThread_t()
{
}


//#############################################################################
//
//  ButtonsThread_t::GetButtonsPressed
//
//#############################################################################

void ButtonsThread_t::GetButtonsPressed(bool *Pressed, uint8_t Size)
{
    int i;

    for (i=0; i<Size; i++)
    {
// E600 BDIO         Pressed[i] = LogicalButtonPressed[i];
    }
}


//#############################################################################
//
//  ButtonsThread_t::ThreadProcedure
//
//#############################################################################

uint32_t ButtonsThread_t::ThreadProcedure(void *ProcedureArg)
{
bool			Rtn;
static	bool	IsHi=false;
bool			WheelRtn = false;
uint8_t			WheelCount=0;
uint8_t			WheelDir = 0;
//Gui_WheelChngMsg_t	GuiMessage;
//bool			WheelStartRtnOk;

// E600 BDIO     DEBUG_ASSERT(SetEvent(ThreadReadyEvent));                           // Let other threads know we're ready to go

// E600 BDIO     bool HardwareButtons[NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS];
//    DWORD ButtonDataByte = NO_BUTTON_PRESSED_DATA_BYTE_VALUE;

    // try to read power on button status using the bootloader method
    //bool ReadPowerOnButtonSuccessful = AioDioDriver.ReadPowerOnButtons(HardwareButtons, &ButtonDataByte);

    // if for any reason, we fail to read power on button status using the bootloader method or button data byte read is 0xFF,
    // we still use the old way to read the power on button status
#if 0 //+++    if(ReadPowerOnButtonSuccessful == false || ButtonDataByte == NO_BUTTON_PRESSED_DATA_BYTE_VALUE)
    {
    AioDioDriver.ReadButtons(HardwareButtons);
    }
#endif
	//ButtonDataByte = FtdiDriver.ReadButtons(HardwareButtons);
	//if(ButtonDataByte == NO_BUTTON_PRESSED_DATA_BYTE_VALUE)
	//{
	//	AioDioDriver.ReadPowerOnButtons(HardwareButtons, &ButtonDataByte);
	//}

	// For the first NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS, MEMBRANE_BUTTON_xx are equal to PHYSICAL_MEMBRANE_BUTTON_xx
    Sleep(BUTTON_READ_INTERVAL);
// E600 BDIO     Rtn = I2cThread.ReadButtons(HardwareButtons);

    int i;
// E600 BDIO     for (i=0; i<NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS; i++)
// E600 BDIO     {
// E600 BDIO         WakeupButtonsPressed[i]    = HardwareButtons[i];
// E600 BDIO     }

// E600 BDIO     WakeupButtonsPressed[MEMBRANE_BUTTON_STORE_BMP]       = false;
// E600 BDIO     LogicalButtonPressed[MEMBRANE_BUTTON_STORE_BMP]       = false;

    while (RunThread)
	{
		ThreadTimeStart=GetTickCount();
		//***************************************************
		//Read the buttons

//        unsigned long Return = WaitForSingleObject(hMembraneButtonEvent, WHEEL_READ_INTERVAL);
//        if(Return == WAIT_TIMEOUT )
//        {
            //WAIT_TIMEOUT
//		   Rtn =I2cThread.ReadButtons(HardwareButtons);
//        }
//        else if(Return == WAIT_OBJECT_0)
//        {
// E600 BDIO            Rtn =I2cThread.ReadButtons(HardwareButtons);
//        }

		if(!BtnEnable)
			{
// E600 BDIO             for (i=0; i<NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS; i++)
// E600 BDIO                 {
// E600 BDIO                     WakeupButtonsPressed[i] = HardwareButtons[i];
// E600 BDIO                 }
			}
		Running=true;
		//***************************************************
		//Read the encoder wheel
        Sleep(WHEEL_READ_INTERVAL);
        CountFromStart ++;
        if ((CountFromStart > 100))
			{
            CountFromStart = 100;
			WheelRtn = AioDioDriver.ReadKnob(&WheelCount,&WheelDir);
			if (WheelRtn)
				{
				if (WheelCount>0)
					{
#if defined(__SPLIT__GUI__)

					TopBaseWindow->PostMessageW(WM_ON_WHEEL_UPDATE,WheelCount,WheelDir);
#endif
					}//end of: if (WheelCount!=WheelLastCount)
				}//end of: if (WheelRtn)
			}//end of: if (WheelStartRtnOk)

        //Rtn =FtdiDriver.ReadButtons(HardwareButtons);
		if (Rtn)
        {
// E600 BDIO
#if 0
            if(PowerButtonPushed)
			{
                 HardwareButtons[PHYSICAL_MEMBRANE_BUTTON_RESERVED1] = true;
			}
			else
			{
                 HardwareButtons[PHYSICAL_MEMBRANE_BUTTON_RESERVED1] = false;
			}

            for (i=0; i<NUMBER_OF_PHYSICAL_MEMBRANE_BUTTONS; i++)
				{
				if (HardwareButtons[i])                     // is the button currently active?
					{
					if (!DebouncedPhysicalButtonPressed[i])
						{
						if (ButtonReleasedCounter[i] >= MIN_BUTTON_RELEASED_COUNT)
							{
							if (++ButtonPressedCounter[i] > MIN_BUTTON_PRESSSED_COUNT)
								{
								DebouncedPhysicalButtonPressed[i] = true;
								ButtonReleasedCounter[i] = 0;
								}
							}
						}
					}
				else                                        // button is not active
					{
					if (DebouncedPhysicalButtonPressed[i])
						{
						if (++ButtonReleasedCounter[i] >= MIN_BUTTON_RELEASED_COUNT)
							{
							DebouncedPhysicalButtonPressed[i] = false;
							ButtonPressedCounter[i] = 0;
							}
						}
					}

				if (BtnEnable)
					{
					if (DebouncedPhysicalButtonPressed[i])
                        LogicalButtonPressed[i] = DebouncedPhysicalButtonPressed[i];
					else
                        LogicalButtonPressed[i] = DebouncedPhysicalButtonPressed[i];
					}
				}

#endif
			}//end of: if (Rtn)


// This stuff isn't normally used. Definitely not used in production!
#if DUMP_REAL_TIME_DATA || ALLOW_SAVE_WINDOWS
        // Up and down together is the "store BMP" command, not the individual commands
// E600 BDIO
#if 0
         if (LogicalButtonPressed[MEMBRANE_BUTTON_UP] && LogicalButtonPressed[MEMBRANE_BUTTON_DOWN])
         {
             LogicalButtonPressed[MEMBRANE_BUTTON_UP] = false;
             LogicalButtonPressed[MEMBRANE_BUTTON_DOWN] = false;
             LogicalButtonPressed[MEMBRANE_BUTTON_STORE_BMP]    = true;
         }
         else
         {
             LogicalButtonPressed[MEMBRANE_BUTTON_STORE_BMP]    = false;
         }
#endif
#endif

        //ServosThread.SetThreadActive(THREAD_ID_SMARTIO);
    ThreadTimeDelta=(GetTickCount()-ThreadTimeStart-BUTTON_READ_INTERVAL);

    }
    return 0;
}



//#############################################################################
//
//  ThreadStartUp - This function gives the Win32 function ::CreateThread an
//      address to be passed as the startup location.
//
//#############################################################################

static DWORD WINAPI ThreadStartUp_ButtonsThread(PVOID ThreadArg)
{
    uint32_t ThreadResult=0;

    __try
    {
// E600 BDIO         ThreadResult = ButtonsThread.ThreadProcedure(ThreadArg);
    }
    __except(ExceptionHandler.EvaluateException(GetExceptionInformation()))
    {
        // Get exception info
        int32_t Code = _exception_code();

        // Call exception handler
        ExceptionHandler.VentilateWithUi(Code, "Buttonsthread");
    }

    return ThreadResult;

}


//#############################################################################
//
//  ButtonsThread_t::CreateThread - This function creates the thread
//
//#############################################################################

void ButtonsThread_t::CreateThread(PVOID ThreadArgument)
{
    ThreadHandle = ::CreateThread(NULL, 0, ThreadStartUp_ButtonsThread, ThreadArgument, 0, &ThreadId);
}


void ButtonsThread_t::HandleIntraVentMsg(IntraVentMsgIds_t msg, uint8_t* data)
{
	switch(msg)
	{
	case INTRAVENT_MSG_PB_SHUTDOWN :
	{
		PowerButtonPushed = true;	// signal ButtonThread of power button pushed

		// this code is a temporary response to the BD; will be some where else later
// E600 BDIO
#if 0
		IntraVentMsgPushButtonShutDownMsg_t ShutDownMsg;
		ShutDownMsg.MsgId = INTRAVENT_MSG_PB_SHUTDOWN;
		ShutDownMsg.ShutDownAccepted = true;
		IntraVentApplicationThread.SendIntraVentMsg((uint8_t*)&ShutDownMsg, sizeof(ShutDownMsg));
#endif
		Sleep(20000); // Wait for power off ------------------------------------------------------------------------------->
	}
		break;
	default:
		break;
	}
}
