
#ifndef Heap_Alarm_MAX_NUM_ALARMS_HH
#define Heap_Alarm_MAX_NUM_ALARMS_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: Heap_Alarm_MAX_NUM_ALARMS - Fixed Heap of Memory Blocks.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/FixedHeap.H_v   25.0.4.0   19 Nov 2013 14:03:46   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "AlarmName.hh"
#include "AlarmName.hh"

//@ Usage-Classes
#include "Alarm.hh"
#include "Heap.hh"
//@ End-Usage


class Heap_Alarm_MAX_NUM_ALARMS : public Heap
{
  public:
    Heap_Alarm_MAX_NUM_ALARMS(void);
    virtual ~Heap_Alarm_MAX_NUM_ALARMS(void);

    static void  SoftFault(const SoftFaultID softFaultID, 
			   const Uint32      lineNumber, 
			   const char*       pFileName = NULL, 
			   const char*       pBoolTest = NULL); 

  protected:
    virtual const Node*  getFirstNode_(void) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    Heap_Alarm_MAX_NUM_ALARMS(const Heap_Alarm_MAX_NUM_ALARMS&);	// not implemented...
    void  operator=(const Heap_Alarm_MAX_NUM_ALARMS&);	// not implemented...

    //@ Constant:  BLOCK_NODES_
    // The number of 'Nodes' that are in a single memory block.
    enum { BLOCK_NODES_ = ((sizeof(Alarm) + sizeof(Node)-1) / sizeof(Node)) };

    //@ Data-Member:  pMemBlock_
    // A block of nodes provides the memory block.
    Node  pMemBlock_[BLOCK_NODES_ * MAX_NUM_ALARMS];
};


#endif  // Heap_Alarm_MAX_NUM_ALARMS_HH
