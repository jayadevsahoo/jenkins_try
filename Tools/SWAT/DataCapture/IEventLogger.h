//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file EventLogger.h
//----------------------------------------------------------------------------


#ifndef IEVENTLOGGER_H
#define IEVENTLOGGER_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "Message.h"
#include <boost/shared_ptr.hpp>



//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class IEventLogger
{
	public:
	virtual bool enqueueEvent(const boost::shared_ptr<Message> pMsg) = 0;
    virtual bool addMessageHandler(unsigned int id, bool (*function)
            (boost::shared_ptr<Message> msg, void *pUser), void *pUser) = 0;


	virtual bool enable()  = 0;
	virtual bool disable() = 0;
};
#endif

