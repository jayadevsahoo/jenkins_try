
#ifndef Settings_Validation_HH
#define Settings_Validation_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  Settings_Validation - Settings-Validation Subsystem's class.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/Settings_Validation.hhv   25.0.4.0   19 Nov 2013 14:27:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//		Initial version 
//  
//====================================================================

#include "Sigma.hh"
#include "SettingClassId.hh"

#if defined(SIGMA_DEVELOPMENT)
#include "BitUtilities.hh"
#endif  // defined(SIGMA_DEVELOPMENT)

//@ Usage-Classes
//@ End-Usage


class Settings_Validation
{
  public:
    static void  Initialize();

#if defined(SIGMA_DEVELOPMENT)
    static inline Boolean  IsDebugOn(const SettingClassId settingClassId);

    static inline void  TurnDebugOn (const SettingClassId settingClassId);
    static inline void  TurnDebugOff(const SettingClassId settingClassId);

    static void  TurnAllDebugOn (void);
    static void  TurnAllDebugOff(void);
#endif  // defined(SIGMA_DEVELOPMENT)

  private:
    //@ Constant:  NUM_WORDS_
    // The number of words in 'ArrDebugFlags_[]' to contain all of this
    // subsystem's debug flags.
    enum
    {
      NUM_WORDS_ = (::NUM_SETTING_CLASSES + sizeof(Uint) - 1) / sizeof(Uint)
    };

    //@ Data-Member:  ArrDebugFlags_
    // A static array of debug flags (one bit each) to be used by this
    // subsystem's code.
    static Uint  ArrDebugFlags_[Settings_Validation::NUM_WORDS_];
};


// Inlined Methods...
#include "Settings_Validation.in"


#endif // Settings_Validation_HH 
