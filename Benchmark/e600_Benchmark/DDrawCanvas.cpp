#include "StdAfx.h"
#include "DDrawCanvas.h"

#pragma comment(lib, "ddraw.lib")

DDrawCanvas::DDrawCanvas(HWND hWnd)
{
	PDD_ = NULL;
	PDDSPrimary_ = NULL;
	PWndClipper_ = NULL;
	init_(hWnd);
}

DDrawCanvas::~DDrawCanvas(void)
{
	release_();
}

bool DDrawCanvas::init_(HWND hWnd)
{
	// initialize direct draw environment
	HRESULT hr = DirectDrawCreate( NULL, &PDD_, NULL );
	if ( failedCheck( hr, L"DirectDrawCreate failed. ") )
		return false;

	// set the cooperative level to Normal mode
	hr = PDD_->SetCooperativeLevel( hWnd, DDSCL_NORMAL );
	if ( failedCheck( hr, L"SetCooperativeLevel failed. ") )
		return false;

	// surface description
	DDSURFACEDESC ddsd;
	memset( &ddsd, 0, sizeof(DDSURFACEDESC) );
	ddsd.dwSize = sizeof(DDSURFACEDESC);
	ddsd.dwFlags = DDSD_CAPS;
	ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;

	// create the main surface
	hr = PDD_->CreateSurface( &ddsd, &PDDSPrimary_, NULL );
	if ( failedCheck( hr, L"CreateSurface failed. ") )
		return false;

	// create and set hwnd to clipper
 	hr = PDD_->CreateClipper( NULL, &PWndClipper_, NULL );
 	if ( failedCheck( hr, L"CreateClipper failed. ") )
 		return false;
 	hr = PWndClipper_->SetHWnd( NULL, hWnd );
	if ( failedCheck( hr, L"SetHWnd failed. ") )
		return false;
 
	// set clipper to primary surface
 	hr = PDDSPrimary_->SetClipper( PWndClipper_ );
 	if ( failedCheck( hr, L"SetClipper failed. ") )
 		return false;

	return true;
}

void DDrawCanvas::release_()
{
	if ( PDDSPrimary_ != NULL )
		PDDSPrimary_->Release();

	if ( PDD_ != NULL )
		PDD_->Release();
}


const TCHAR* DDrawCanvas::err_string_( HRESULT hr )
{
	switch( hr )
	{
	case DD_OK: return L"DD_OK";
	case DDERR_CURRENTLYNOTAVAIL: return L"DDERR_CURRENTLYNOTAVAIL";
	case DDERR_GENERIC: return L"DDERR_GENERIC";
	case DDERR_HEIGHTALIGN: return L"DDERR_HEIGHTALIGN";
	case DDERR_INCOMPATIBLEPRIMARY: return L"DDERR_INCOMPATIBLEPRIMARY";
	case DDERR_INVALIDCAPS: return L"DDERR_INVALIDCAPS";
	case DDERR_INVALIDCLIPLIST: return L"DDERR_INVALIDCLIPLIST";
	case DDERR_INVALIDMODE: return L"DDERR_INVALIDMODE";
	case DDERR_INVALIDOBJECT: return L"DDERR_INVALIDOBJECT";
	case DDERR_INVALIDPARAMS: return L"DDERR_INVALIDPARAMS";
	case DDERR_INVALIDPIXELFORMAT: return L"DDERR_INVALIDPIXELFORMAT";
	case DDERR_INVALIDRECT: return L"DDERR_INVALIDRECT";
	case DDERR_LOCKEDSURFACES: return L"DDERR_LOCKEDSURFACES";
	case DDERR_NOCLIPLIST: return L"DDERR_NOCLIPLIST";
	case DDERR_NOALPHAHW: return L"DDERR_NOALPHAHW";
	case DDERR_NOCOLORCONVHW: return L"DDERR_NOCOLORCONVHW";
	case DDERR_NOCOOPERATIVELEVELSET: return L"DDERR_NOCOOPERATIVELEVELSET";
	case DDERR_NOCOLORKEYHW: return L"DDERR_NOCOLORKEYHW";
	case DDERR_NOFLIPHW: return L"DDERR_NOFLIPHW";
	case DDERR_NOTFOUND: return L"DDERR_NOTFOUND";
	case DDERR_NOOVERLAYHW: return L"DDERR_NOOVERLAYHW";
	case DDERR_OVERLAPPINGRECTS: return L"DDERR_OVERLAPPINGRECTS";
	case DDERR_NORASTEROPHW: return L"DDERR_NORASTEROPHW";
	case DDERR_NOSTRETCHHW: return L"DDERR_NOSTRETCHHW";
	case DDERR_NOVSYNCHW: return L"DDERR_NOVSYNCHW";
	case DDERR_NOZOVERLAYHW: return L"DDERR_NOZOVERLAYHW";
	case DDERR_OUTOFCAPS: return L"DDERR_OUTOFCAPS";
	case DDERR_OUTOFMEMORY: return L"DDERR_OUTOFMEMORY";
	case DDERR_OUTOFVIDEOMEMORY: return L"DDERR_OUTOFVIDEOMEMORY";
	case DDERR_PALETTEBUSY: return L"DDERR_PALETTEBUSY";
	case DDERR_COLORKEYNOTSET: return L"DDERR_COLORKEYNOTSET";
	case DDERR_SURFACEBUSY: return L"DDERR_SURFACEBUSY";
	case DDERR_CANTLOCKSURFACE: return L"DDERR_CANTLOCKSURFACE";
	case DDERR_SURFACELOST: return L"DDERR_SURFACELOST";
	case DDERR_SURFACENOTATTACHED: return L"DDERR_SURFACENOTATTACHED";
	case DDERR_TOOBIGHEIGHT: return L"DDERR_TOOBIGHEIGHT";
	case DDERR_TOOBIGSIZE: return L"DDERR_TOOBIGSIZE";
	case DDERR_TOOBIGWIDTH: return L"DDERR_TOOBIGWIDTH";
	case DDERR_UNSUPPORTED: return L"DDERR_UNSUPPORTED";
	case DDERR_UNSUPPORTEDFORMAT: return L"DDERR_UNSUPPORTEDFORMAT";
	case DDERR_VERTICALBLANKINPROGRESS: return L"DDERR_VERTICALBLANKINPROGRESS";
	case DDERR_WASSTILLDRAWING: return L"DDERR_WASSTILLDRAWING";
	case DDERR_INVALIDDIRECTDRAWGUID: return L"DDERR_INVALIDDIRECTDRAWGUID";
	case DDERR_DIRECTDRAWALREADYCREATED: return L"DDERR_DIRECTDRAWALREADYCREATED";
	case DDERR_PRIMARYSURFACEALREADYEXISTS: return L"DDERR_PRIMARYSURFACEALREADYEXISTS";
	case DDERR_REGIONTOOSMALL: return L"DDERR_REGIONTOOSMALL";
	case DDERR_CLIPPERISUSINGHWND: return L"DDERR_CLIPPERISUSINGHWND";
	case DDERR_NOCLIPPERATTACHED: return L"DDERR_NOCLIPPERATTACHED";
	case DDERR_NOPALETTEATTACHED: return L"DDERR_NOPALETTEATTACHED";
	case DDERR_NOPALETTEHW: return L"DDERR_NOPALETTEHW";
	case DDERR_NOBLTHW: return L"DDERR_NOBLTHW";
	case DDERR_OVERLAYNOTVISIBLE: return L"DDERR_OVERLAYNOTVISIBLE";
	case DDERR_NOOVERLAYDEST: return L"DDERR_NOOVERLAYDEST";
	case DDERR_INVALIDPOSITION: return L"DDERR_INVALIDPOSITION";
	case DDERR_NOTAOVERLAYSURFACE: return L"DDERR_NOTAOVERLAYSURFACE";
	case DDERR_EXCLUSIVEMODEALREADYSET: return L"DDERR_EXCLUSIVEMODEALREADYSET";
	case DDERR_NOTFLIPPABLE: return L"DDERR_NOTFLIPPABLE";
	case DDERR_NOTLOCKED: return L"DDERR_NOTLOCKED";
	case DDERR_CANTCREATEDC: return L"DDERR_CANTCREATEDC";
	case DDERR_NODC: return L"DDERR_NODC";
	case DDERR_WRONGMODE: return L"DDERR_WRONGMODE";
	case DDERR_IMPLICITLYCREATED: return L"DDERR_IMPLICITLYCREATED";
	case DDERR_NOTPALETTIZED: return L"DDERR_NOTPALETTIZED";
	case DDERR_DCALREADYCREATED: return L"DDERR_DCALREADYCREATED";
	case DDERR_MOREDATA: return L"DDERR_MOREDATA";
	case DDERR_VIDEONOTACTIVE: return L"DDERR_VIDEONOTACTIVE";
	case DDERR_DEVICEDOESNTOWNSURFACE: return L"DDERR_DEVICEDOESNTOWNSURFACE";
	}

	return L"Unknown";
}


bool DDrawCanvas::failedCheck( HRESULT hr, const TCHAR* szMessage )
{
	if (FAILED(hr))
	{
		TCHAR buf[1024];
		swprintf_s( buf, 1024, L"%s (%s)n", szMessage, err_string_(hr) );
		OutputDebugString( buf );
		return true;
	}
	return false;
}


bool DDrawCanvas::createOffscreenSurface( DWORD width, DWORD height, LPDIRECTDRAWSURFACE& offScrnSurface )
{
	DDSURFACEDESC ddsd;
	memset( &ddsd, 0, sizeof(DDSURFACEDESC) );
	ddsd.dwSize = sizeof(DDSURFACEDESC);
	ddsd.dwFlags = DDSD_WIDTH | DDSD_HEIGHT;
	ddsd.dwWidth = width;
	ddsd.dwHeight = height;

	HRESULT hr = PDD_->CreateSurface( &ddsd, &offScrnSurface, NULL );
	if ( failedCheck( hr, L"CreateSurface failed. ") )
		return false;

	return true;
}


LPDIRECTDRAWSURFACE DDrawCanvas::getPrimarySurface()
{
	return PDDSPrimary_;
}
