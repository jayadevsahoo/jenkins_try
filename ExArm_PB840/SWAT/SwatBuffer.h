//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
#ifndef SWATBUFFER_H
#define SWATBUFFER_H

#include"SwatBatchUser.h"

#ifndef SWATBUFFER_BUFFER_SIZE
#define SWATBUFFER_BUFFER_SIZE  200*60*3
#endif



namespace swat{

    class SwatBuffer
    {
        private:
            static float m_data[SWATBUFFER_BUFFER_SIZE];

        public:
            static inline float getBufferData(unsigned int indx)
            {
                if(indx<SWATBUFFER_BUFFER_SIZE)
                {
                    return m_data[indx];
                }
                return 0.0f;
            }

            static  void copyToBuffer(char *pData, unsigned int iLength);

            static  void copyToBuffer(char *pData, unsigned int iLength, unsigned int iOffset);

    };

}

#endif
