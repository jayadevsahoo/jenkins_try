#pragma once

#include "Luna.h"

struct lua_State;
class SWATGuiFunctions
{
private:
	bool connected_;
	char ipaddr_[32];
	unsigned int port_;
	
	bool CheckConnection();

public:
	SWATGuiFunctions( lua_State* L );
	~SWATGuiFunctions(void);

	int simulate_touch( lua_State* L );
	int simulate_bezel_key( lua_State* L );
	int simulate_knob( lua_State* L );
	int capture_screen( lua_State* L );
	int query_screen_capture_buffer_info( lua_State* L );
	int query_screenshot_path( lua_State* L );
	int clear_screens( lua_State* L );	

	int init( lua_State* L );
	int connect( lua_State* L );
	int disconnect( lua_State* L );
	int sleep( lua_State* L );

	static const char className[];
    static const Luna<SWATGuiFunctions>::RegType Register[];
};
