#ifndef GuiIoLed_HH
#define GuiIoLed_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// File: GuiIoLed.hh - GUI_CPU front panel & keyboard LED mnemonics
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-IO-Devices/vcssrc/GuiIoLed.hhv   25.0.4.0   19 Nov 2013 14:12:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  clw    Date:  28-Mar-1997    DR Number: <none>
//       Project:  Sigma (R8027)
//       Description:
//             Initial version to coding standards (Integration baseline).
//=====================================================================

//@ Usage-Classes
//@ End-Usage

//@Type: GuiLeds
// Arbitrary, but unique, naming/numbering of GUI LEDS.
enum GuiLeds		// all GUI-related LEDs
{
	REDLEDSTRIP = 0,		//HIGH_ALARM_URGENCY						
	YELLOWLEDSTRIP,			//MEDIUM_ALARM_URGENCY, LOW_ALARM_URGENCY	
	DS7,				    //NORMAL
	DS8AND9,			    //VENTILATOR_INOPERATIVE
	DS10AND11,			    //SAFETY_VALVE_OPEN
	DS12,				//BATTERY_BACKUP_READY
	DS13,				//ON_BATTERY_POWER
	DS14,				//COMPRESSOR_READY
	DS15,				//COMPRESSOR_OPERATING
	GREEN,				//HUNDRED_PERCENT_O2 // ...then, keyboard (right to left)
	RIGHT_YELLOW,		//ALARM_SILENCE
	LEFT_YELLOW,		//SCREEN_LOCK

	MAX_LED_COUNT		// the total number of GUI LEDs
};

#endif	// GuiIoLed_HH
