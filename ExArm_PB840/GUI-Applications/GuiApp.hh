#ifndef GuiApp_HH
#define GuiApp_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class: GuiApp - main subsystem class, which contains the globals
// used by the subsystem.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiApp.hhv   25.0.4.0   19 Nov 2013 14:07:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 022  By: mnr   Date: 25-Feb-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX Revision and Serial number related updates.
//
//  Revision: 021   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 020  	By: gdc    Date:  26-May-2007    SCR Number: 6330
//  Project: Trend
//  Description:
// 	Removed SUN prototype code.
//
//  Revision: 019   By: gdc    Date:  26-May-2007    SCR Number: 6237
//  Project: Trend
//  Description:
// 	Added PrintTarget pointer for printer callback events.
//
//  Revision: 018   By: hhd    Date:  30-Mar-2000    DCS Number: 5675
//  Project: Neo-Mode
//  Description:
// 	Changed method IsDataKeyInstall() into public.
//
//  Revision: 017   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 	Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 016  By: gdc    Date: 22-Jul-1999    DR Number: 5129
//  Project:  806 Compressor
//  Description:    
//      Changed to process all alarm annunciations prior to periodic
//      tasks. Added gating mechanism for SCREEN_TOUCH events as well
//      as SCREEN_RELEASE events.
//  
//  Revision: 015  By: healey    Date:  08-Feb-1999   DR Number: 5322
//  Project:  ATC
//  Description:
//      Initial ATC revision.
//
//  Revision: 014  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 013  By:  syw   Date:  19-Nov-1998    DR Number: 5218
//       Project:  BiLevel
//       Description:
//			Added DATAKEY_INVALID enum and IsSetFlashSerialNumber().
//
//  Revision: 012  By: syw      Date: 27-Oct-1998  DR Number: 5215
//    Project:  BiLevel
//    Description:
//		Changed ConfigurationCode_ to VentilatorOptions_, GetConfigurationCode
//		to GetVentilatorOptions.
//
//  Revision: 011  By: yyy      Date: 05-Jan-1998  DR Number: 
//    Project:  BiLevel (R8027)
//    Description:
//     Initial release for BiLevel adding software option.
//     Merged /840/Baseline/GUI-Applications/vcssrc/GuiApp.hhv   1.99.1.0   07/30/98 10:13:26   gdc 
//	   Deleted SetSerialNumberMatchedFlad method and data member.
//	   Defined GetConfigurationCode(), IsSetFlashserialNumbers()..  Rename
//	   IsDemoDataKey with IsStandardDevelopmentOptions.  Added SoftwareOptions.
//	   Added IsSalesDataKey().
//
//  Revision: 010  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 009 By:  yyy    Date:  26-SEP-97    DR Number: 2410
//    Project:  Sigma (R8027)
//    Description:
//      Updated access method for pressure transducer calibrationtest due to changes
//		in service mode.
//
//  Revision: 008 By:  yyy    Date:  25-SEP-97    DR Number: 2522
//    Project:  Sigma (R8027)
//    Description:
//      Pass the transaction successful message to interested subscreen before test starts.
//
//  Revision: 007  By:  yyy    Date:  24-SEP-1997    DR Number:   2385
//    Project:  Sigma (R8027)
//    Description:
//      Added methods for flow sensor calibration status due to changes in service
//		mode.
//
//  Revision: 006  By:  yyy    Date:  10-Sep-97    DR Number: 2289
//    Project:  Sigma (R8027)
//    Description:
//      Incorporated DEMO datakey type handle.
//
//  Revision: 005  By:  sah    Date:  20-AUG-1997    DCS Number: 2379
//  Project:  Sigma (R8027)
//  Description:
//      Added method to set the 'IsAlarmUpdateNeeded_' flag, so that the
//		flag can be set when an Alarm History Log update notification is
//		received on the queue.
//
//  Revision: 004  By:  yyy    Date:  30-JUL-97    DR Number: 2279
//    Project:  Sigma (R8027)
//    Description:
//      Added IsFlowSensorInfoRequired(), SetFlowSensorInfoRequiredFlag()
//		methods to parse the flow sensor information.
//
//  Revision: 004  By:  yyy    Date:  30-JUL-97    DR Number: 2313
//    Project:  Sigma (R8027)
//    Description:
//      30-JUL-97  Added IsTauCharacterizationRequired(), SetTauCharacterizationRequiredFlag()
//		methods to parse the tau characterization information.
//		13-AUG-97  Removed all references for TauCharacterization
//
//  Revision: 003  By:  sah    Date:  01-Aug-1997    DCS Number: 2017
//  Project:  Sigma (R8027)
//  Description:
//      Added IsExhValveCalSuccessful(), SetExhValveCalSuccessfulFlag(),
//		methods to parse the exh. valve status information.
//
//  Revision: 002  By:  yyy    Date:  09-Jun-97    DR Number: 2202
//       Project:  Sigma (R8027)
//       Description:
//             Removed all referenced to IsPowerFailure....
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "GuiAppClassIds.hh"
#if defined(SIGMA_DEVELOPMENT)
#	include "BitUtilities.hh"
#endif // defined(SIGMA_DEVELOPMENT)
#include "PatientDataId.hh"
#include "TestDataId.hh"
#include "DiscreteValue.hh"
#include "PressUnitsValue.hh"
#include "SigmaState.hh"
#include "PostDefs.hh"
//TODO E600
//#include <string.h>

//@ Usage-Classes
class BdReadyMessage;
class BigSerialNumber;
class ChangeStateMessage;
class CommDownMessage;
class InitScreen;
class KeyHandlers;
class LowerScreen;
class MessageArea;
class MiscBdEventHandler;
class MsgQueue;
class MutEx;
class NodeReadyMessage;
class PrintTarget;
class PromptArea;
class SerialNumber;
class ServiceLowerScreen;
class ServiceUpperScreen;
class UpperScreen;
//@ End-Usage


// Structure for storing information regarding GUI internal task
struct GuiTaskInfo_
{
	Int16 stateMask;					// State Mask of the task
	Int16 priorityConst;				// Priority of the task
	Int16 priority;						// Priority of the task
	Int16 timeoutPeriodConst;			// Minimim timer count needed to
										// start the activation of the task
	Int16 timeoutCount;					// Currently accumulated task count
	Int16 executionBiasCount;				// In case we don't have enough time to
										// execute the task, then we simply
										// accumulate the execution count we missed
										// This is the most important field to
										// indicating if the task need to be
										// executed.
										// Whenever, a task has been executed
										// we need to reset this field to 0;
	Int16 executionSequence;			// 
};

class GuiApp
{
public:
	//@ Type: GuiDisplayMode
	// Identifies the three major display states the GUI-App can be in:
	// INITIALIZING 		: It occurs at initialization time and remains
	//						: until the GUI-App is informed by Sys-Init to go
	//						: to GUIInop, GUIService, GUIOnline or GUITimeout
	//						: from GUIStart, and GUIInit.
	// NORMAL_VENTILATION 	: Sys-Init informes GUI-App that it should go to
	//						: GUIInop, GUIOnline or GUITimeout with screen
	//						: being displayed in NORMAL VENTILATION format.
	// SERVICE_MODE 		: Sys-Init informes GUI-App that it should go to
	//						: GUIService or GUISst with screen being displayed
	//						: in SERVICE MODE format.
	enum GuiDisplayMode
	{
		INITIALIZING,
		NORMAL_VENTILATION,
		SERVICE_MODE,
		MAX_DISPLAY_STATE
	};

	//@ Type: GuiAppEvent
	// Identifies all the possible event the GUI-App can occure:
	//	UNDEFINED				: Initial state before Sys-Init starts
	//	GUI_ONLINE_READY		: Sys-Init informs of GUI on line
	//	SERVICE_READY			: Sys-Init informs of GUI in service mode
	//	SETTINGS_LOCKOUT		: Alarm subsystem informs of Setting locked out
	//	COMMUNICATIONS_DOWN		: Sys-Init informs of communication down
	//	SETTINGS_TRANSACTION_SUCCESS
	//							: Settings subsystem informs of Setting
	//							  transaction event happened.
	//	INOP					: Sys-Init informs of GUI in STATE_INOP or
	//							  STATE_TIMEOUT state.
	//	PATIENT_DATA_DISPLAY	: Gui App's VitalPatientDataArea inform of
	//							: waveform subscreen to update the new
	//							: waveform data.
	//	PATIENT_SETUP_ACCEPTED	: Gui App's VitalPatientDataArea inform of
	//							  waveform subscreen to update the new
	//							  waveform data.
	enum GuiAppEvent
	{
		UNDEFINED=0,
		GUI_ONLINE_READY=1,
		SERVICE_READY=2,
		SETTINGS_LOCKOUT=4,
		COMMUNICATIONS_DOWN=8,
		SETTINGS_TRANSACTION_SUCCESS=16,
		INOP=32,
		PATIENT_DATA_DISPLAY=64,
		PATIENT_SETUP_ACCEPTED=128
	};

	//@ Type: SerialNumberStates
	// Identifies the three major states the Serial numbers can be in:
	enum SerialNumberStates
	{
		SERIAL_NUMBER_OK,
		DATAKEY_NOT_INSTALLED,
		DATAKEY_INVALID,
		SERIAL_NUMBER_NOT_MATCHED,
		MAX_SERIAL_NUMBER_STATES
	};

	//@ Type: GuiTaskId
	// Lists the possible GUI task ID
	enum GuiTaskId
	{
		GUI_NO_TASK=-1,
		GUI_BREATH_BAR_TASK,
		GUI_SCHEDULER_TASK,
		GUI_EXECUTOR_TASK,
		MAX_GUI_TASK
	};
	
	//@ Type: GuiEventId
	// Lists the possible GUI event ID
	enum GuiEventId
	{
		GUI_NO_EVENT=-1,
		GUI_REPAINT_UPPER_EVENT,
		GUI_REPAINT_LOWER_EVENT,
		GUI_PATIENT_DATA_EVENT,
		GUI_WAVEFORM_EVENT,
		GUI_ALARM_EVENT,
		GUI_PLOT1_EVENT,
		GUI_PLOT2_EVENT,
		GUI_BD_EVENT,
		GUI_TIMER_EVENT,
		MAX_GUI_EVENT
	};

    //@ Type: GuiPriority
    // Set the priority constant when immediate attention is
    // needed to execute a GUI event.
    enum GuiPriority
	{
		GUI_HIGHEST_PRIORITY = 100,
		GUI_SECOND_HIGHEST_PRIORITY = 60,
		GUI_THIRD_HIGHEST_PRIORITY = 40,
		GUI_FOURTH_HIGHEST_PRIORITY = 2
    };

	static void Initialize(void);
	static void TaskBreathBar(void);
	static void TaskScheduler(void);
	static void TaskExecutor(void);
	static void BdReadyCallback(const BdReadyMessage &rMessage);
	static void ChangeStateCallback(const ChangeStateMessage &rMessage);
	static void CommDownCallback(const CommDownMessage &rMessage);
	static void BeginNormalVentilation(void);
	static void BeginServiceMode(void);
	static void AlarmEventHappened(void);
	static void PatientDataEventHappened(
							const PatientDataId::PatientItemId changedDatumId);
	static void ServiceDataEventHappened(
						const TestDataId::TestDataItemId changedDatumId);
	static void UserActivityHappened(void);
	static void SetSerialCommunication(Boolean isSerialCommunicationUp);
	static SerialNumberStates GetSerialNumberInfoState(void);
	static const SerialNumber& GetCompressorSerialNum(void);
	static const SerialNumber& GetGuiDataKeySN(void);
	static const SerialNumber& GetBdDataKeySN(void);
	static const SerialNumber& GetGuiSN(void);
	static const SerialNumber& GetBdSN(void);
	static const SerialNumber& GetGuiNovRamSN(void);
	static const SerialNumber& GetBdNovRamSN(void);
	static const BigSerialNumber& GetProxFirmwareRev(void);
	static Uint32 GetProxSerialNum(void);

	static void SetCompressorSerialNum(SerialNumber compressorSerialNum);
	static void SetGuiSN(SerialNumber guiSN);
	static void SetBdSN(SerialNumber bdSN);
	static void SetProxFirmwareRev(BigSerialNumber proxFirmwareRev);
	static void SetProxSerialNum(Uint32 proxSerialNum);
	static void SetProxIsInstalled(Boolean isProxInstalled);

	static void RaiseGuiEventPriority(Int16 eventId, Int16 priority);

	static Boolean  IsSettingsLockedOut(void);
	static Boolean  IsSettingsTransactionSuccess(void);
	static void     SetSettingsLockedOut(Boolean);
	static void     SetSettingsTransactionSuccess(Boolean isTransactionSuccess);

	static StringId GetPressureUnitsSmall(void);
	static StringId GetPressureUnitsLarge(void);

	static inline PressUnitsValue::PressUnitsValueId PressUnits(void);
	static inline SigmaState GetGuiState(void);
	static inline SigmaState GetBdState(void);
	static inline char* GetBdSoftwareRevNum(void);  
	static inline char* GetBdKernelPartNumber(void);  
	static inline Boolean IsSerialCommunicationUp(void);
	static inline Boolean IsExhValveCalRequired(void);
	static inline Boolean IsExhValveCalSuccessful(void);
	static inline Boolean IsFlowSensorInfoRequired(void);
	static inline Boolean IsFlowSensorCalPassed(void);
	static inline Boolean IsBdVentInopTestInProgress(void);
	static inline Boolean IsGuiVentInopTestInProgress(void);
    static inline Boolean IsBdAndGuiFlashTheSame(void);
    static inline Boolean IsServiceInitialStateOK(void);
    static inline Boolean IsVentStartupSequenceComplete(void);
	static inline Boolean IsGuiPostFailed(void);
	static inline Boolean IsBdPostFailed(void);
	static inline Boolean IsGuiBackgroundFailed(void);
	static inline Boolean IsBdBackgroundFailed(void);
	static inline Boolean IsVentInopMajorFailure(void);
	static inline Boolean IsAtmPresXducerCalSuccessful(void);
	static inline Boolean IsSstFailure(void);
	static inline Boolean IsServiceRequired(void);
	static inline Boolean IsPowerFailed(void);
	static inline Boolean IsUnexpectedReset(void);
	static inline Boolean IsDataKeyInstalled(void);
	static inline Boolean IsProxInstalled(void);

	static inline void SetExhValveCalRequiredFlag(Boolean requiredFlag);
	static inline void SetExhValveCalSuccessfulFlag(Boolean statusFlag);
	static inline void SetFlowSensorInfoRequiredFlag(Boolean);
	static inline void SetFlowSensorCalPassedFlag(Boolean);
	static inline void SetBdVentInopTestInProgressFlag(Boolean);
	static inline void SetGuiVentInopTestInProgressFlag(Boolean);
	static inline void SetVentInopMajorFailureFlag(Boolean);
	static inline void SetAtmPresXducerCalStatusFlag(Boolean);
	static inline void SetSstRequiredFlag(Boolean);
	static inline void SetVentStartupSequenceComplete(Boolean);
	static inline void SetDataKeyInstalledFlag(Boolean);
	static inline void SetBdAndGuiFlash(Boolean);
	 
	static inline void SetBdSoftwareRevNum(const char* bdSoftwareRevNum);
	static inline void SetBdKernelPartNumber(const char* bdKernelPartNumber);	
	 
    
	static inline void SetAlarmUpdateNeededFlag(void);
	static inline void SetUpdateCompletedBatchProcessFlag(void);

    static inline void SetPrintTarget(PrintTarget* pTarget);
	
	static inline Uint16 GetGuiBreathType(void);
	static inline Uint16 GetGuiBreathPhase(void);

	static inline Uint16 GetLanguage(void);
	static inline Uint16 GetLanguageIndex(void);
#if defined(SIGMA_DEVELOPMENT)
	static inline Boolean  IsDebugOn(const GuiAppClassId moduleId);

	static inline void  TurnDebugOn (const GuiAppClassId moduleId);
	static inline void  TurnDebugOff(const GuiAppClassId moduleId);

	static void  TurnAllDebugOn (void);
	static void  TurnAllDebugOff(void);
#endif  // defined(SIGMA_DEVELOPMENT)

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	//@ Data-Member: RLowerScreen
	// Lower Screen container during normal ventilation.
	static LowerScreen& RLowerScreen;

	//@ Data-Member: RUpperScreen
	// Upper Screen container during normal ventilation.
	static UpperScreen& RUpperScreen;

	//@ Data-Member: PPromptArea
	// Prompt Area container during normal ventilation and service mode.
	static PromptArea* PPromptArea;

	//@ Data-Member: PMessageArea
	// Message Area container.
	static MessageArea* PMessageArea;

	//@ Data-Member: RKeyHandlers
	// Key Handlers for off-screen keys
	static KeyHandlers& RKeyHandlers;

	//@ Data-Member: RMiscBdEventHandler
	// MiscBdEvent Handlers for BdEvents
	static MiscBdEventHandler& RMiscBdEventHandler;

protected:

private:
	// these methods are purposely declared, but not implemented...
	GuiApp(void);					// not implemented...
	GuiApp(const GuiApp&);			// not implemented...
	~GuiApp(void);					// not implemented...
	void operator=(const GuiApp&);	// not implemented...

	static Boolean AreSerialNumbersMatched_(void);
	static void AlarmEventChangeHappened_(void);
	static void BoostScreenUpdate_(Int16 screenTouched);
	static void BoostKnobUpdate_(Int16 screenTouched);
	
	static void SetConfigurationAgentFields_(const BdReadyMessage& rMessage);
	static void SetNextGuiEvent_(void);
	static Int16 GetNextGuiEvent_(void);
	//static void AccumulateEvent_(MsgQueue &userAnnunciationQ); //TODo E600 - may be revisited later
	static void ProcessAlarmAnnunciations_(void);


#ifdef  BROADCAST_TOUCHED_POSITION
    static void broadcastTouchedPosition_(Int16 x, Int16 y);
#endif


	//@ Data-Member: MajorScreenMode_
	// Stores which of the three major display modes the GUI-App can be in:
	// INITIALIZING_, NORMAL_VENTILATION_ or SERVICE_MODE_.
	static GuiDisplayMode MajorScreenMode_;

	//@ Data-Member: BdState_
	// Stores the BD State reported by Sys-Init when BdReadyCallBack()
	// happened.
	static SigmaState BdState_;
	
	//@ Data-Member: GuiState_
	// Stores the GUI State reported by Sys-Init when GuiEventHappenedk()
	// happened.
	static SigmaState GuiState_;

	//@ Data-Member: IsSettingsLockedOut_
	// Flag which indicates whether changes to the Settings-Validation
	// subsystem have been locked out or not due to Settings locked out,
	// Communication down or Vent inop.
	static Boolean IsSettingsLockedOut_;

	//@ Data-Member: IsSettingsTransactionSuccess_
	// When the last accepted batch of settings fails to be transmitted
	// to the Breath-Delivery system this flag is set to FALSE.  It is set
	// to TRUE once the settings are successfully transmitted.
	static Boolean IsSettingsTransactionSuccess_;

	//@ Data-Member: RInitScreen_
	// Reference to the Initialization screen which is displayed on the
	// Upper screen.
	static InitScreen& RInitScreen_;

	//@ Data-Member: IsSerialCommunicationUp_
	// Flag used to indicate if the Service Mode is in serial communication 
	// mode.
	static Boolean IsSerialCommunicationUp_;

	//@ Data-Member: PressUnitsSetting_
	// Stores the current value of the pressure units setting.  This value is
	// stored for quick access by various application constructors that need
	// to know what the pressure units are so that the correct units display
	// can be determined.
	static DiscreteValue  PressUnitsSetting_;

	//@ Data-Member: ExhValveCalRequiredFlag_
	// Exhalation Valve Calibration Required Flag
	static Boolean ExhValveCalRequiredFlag_;

	//@ Data-Member: ExhValveCalStatusFlag_
	// Exhalation Valve Calibration Required Flag
	static Boolean ExhValveCalStatusFlag_;

	//@ Data-Member: FlowSensorInfoRequiredFlag_
	// Flow Sensor Information Required Flag
	static Boolean FlowSensorInfoRequiredFlag_;

	//@ Data-Member: FlowSensorCalPassedFlag_
	// Flow Sensor Calibration required Status Flag
	static Boolean FlowSensorCalPassedFlag_;

	//@ Data-Member: DataKeyInstalledFlag_
	// Data Key Installed Flag
	static Boolean DataKeyInstalledFlag_;

	//@ Data-Member: IsVentStartupSequenceComplete_
	// Vent Startup state indicating if there was a completed patient
	// setup available.
	static Boolean IsVentStartupSequenceComplete_;

	//@ Data-Member: IsBdAndGuiFlashTheSame_
	// Flag used to indicate if the Flashes are same for both GUI and BD
	// boards.
	static Boolean IsBdAndGuiFlashTheSame_;

	//@ Data-Member: IsGuiPostFailed_
	// Flag used to indicate GUI POST failed or not.
	static Boolean IsGuiPostFailed_;
	
	//@ Data-Member: IsBdPostFailed_
	// Flag used to indicate BD POST failed or not.
	static Boolean IsBdPostFailed_;

	//@ Data-Member: IsGuiBackgroundFailed_
	// Flag used to indicate GUI background failed or not.
	static Boolean IsGuiBackgroundFailed_;

	//@ Data-Member: IsBdBackgroundFailed_
	// Flag used to indicate BD background failed or not.
	static Boolean IsBdBackgroundFailed_;
	
	//@ Data-Member: IsServiceRequired_
	// Flag used to indicate service is required or not.
	static Boolean IsServiceRequired_;

	//@ Data-Member: StartupState_
	// The Vent's start up state as recorded by POST.
	static ShutdownState StartupState_;

	//@ Data-Member: BdVentInopTestInProgress_
	// Flag used to indicate Bd vent inop test is in progress.
	static Boolean BdVentInopTestInProgress_;

	//@ Data-Member: GuiVentInopTestInProgress_
	// Flag used to indicate Gui vent inop major failure occured.
	static Boolean GuiVentInopTestInProgress_;

	//@ Data-Member: VentInopMajorFailureFlag_
	// Flag used to indicate Gui vent inop test is in progress.
	static Boolean VentInopMajorFailureFlag_;

	//@ Data-Member: AtmPresXducerCalSuccessfulFlag_
	// Flag used to indicate ATM pressure transducer test is successful.
	static Boolean AtmPresXducerCalSuccessfulFlag_;
	
	//@ Data-Member: SstRequiredFlag_
	// Flag used to indicate SST run is required.
	static Boolean SstRequiredFlag_;

	//@ Data-Member: DataKeyGuiSerialNumber_
	// GUI Serial Number DataKey flag
	static SerialNumber DataKeyGuiSerialNumber_;

	//@ Data-Member: DataKeyBdSerialNumber_
	// BD Serial Number DataKey flag
	static SerialNumber DataKeyBdSerialNumber_;

	//@ Data-Member: GuiSerialNumber_
	// GUI Serial Number Matched flag
	static SerialNumber GuiSerialNumber_;

	//@ Data-Member: BdSerialNumber_
	// BD Serial Number Matched flag
	static SerialNumber BdSerialNumber_;

	//@ Data-Member: GuiNovRamSerialNumber_
	// GUI NovRam Serial Number Matched flag
	static SerialNumber GuiNovRamSerialNumber_;

	//@ Data-Member: BdNovRamSerialNumber_
	// BD NovRam Serial Number Matched flag
	static SerialNumber BdNovRamSerialNumber_;

	//@ Data-Member: CompressorSerialNum_
	// The place holder for compressor serial number.
	static SerialNumber CompressorSerialNum_;

	//@ Data-Member: BdSoftwareRevNum_;
	// The place holder for BD software revision number.
	 
	static char BdSoftwareRevNum_[50];
	
	//@ Data-Member: BdKernelPartNumber_;
	// The place holder for BD kernel POST version.
	static char BdKernelPartNumber_[50];
	 	
    //@ Data-Member:    RGuiEventAccessMutEx_
    // Reference to the MutEx used for the GuiEventInfoArray_.
    static MutEx&  RGuiEventAccessMutEx_;   

	//@ Data-Member: GuiEventInfoArray_
	// An array of GuiTaskInfo_ structures for storing GUI task
	// information on each task.
	static GuiTaskInfo_ GuiEventInfoArray_[MAX_GUI_EVENT];

	//@ Data-Member: IsAlarmUpdateNeeded_
	// Flag used to indicate that alarm update is required
	static Boolean IsAlarmUpdateNeeded_;

	//@ Data-Member: GuiBreathPhase_
	// Flag used to indicate that alarm update is required
	static Uint16 GuiBreathPhase_;

	//@ Data-Member: GuiBreathType_
	// Flag used to indicate that alarm update is required
	static Uint16 GuiBreathType_;

	//@ Data-Member: UpdateCompletedBatchProcessFlag_
	// Flag used to indicate Apps is waiting for setting transaction
	// to complete.
	static Boolean UpdateCompletedBatchProcessFlag_;
	
    //@ Constant:  GA_NUM_WORDS_
    // The number of words needed in 'arrDebugFlags_[]' to contain all of
    // this subsystem's debug flags.
    enum
	{
		GA_NUM_WORDS_ = (::NUM_GUIAPP_CLASSES + sizeof(Uint)-1) / sizeof(Uint)
    };

    //@ Data-Member:  arrDebugFlags_
    // A static array of debug flags (one bit each) to be used by this
    // subsystem's code.
    static Uint ArrDebugFlags_[GuiApp::GA_NUM_WORDS_];


    //@ Data-Member:  pPrintTarget
    // Pointer to the PrintTarget to receive printing callbacks
	static PrintTarget* PPrintTarget_;
	
	// @ Data-Member: ProxFirmwareRev_
	// The place holder for PROX Firmware Revision.
	static BigSerialNumber ProxFirmwareRev_;

	// @ Data-Member: ProxSerialNum_
	// The place holder for PROX Serial Number.
	static Uint32 ProxSerialNum_;

	// @ Data-Member: Language_
	// The LanguageValue setting in effect at start-up. The user can change the
	// LanguageValue setting during service-mode, but it doesn't take effect
	// until the next power cycle when this variable is initialized.
	static Uint16 Language_;

	// @ Data-Member: LanguageIndex_
	// The index into the array of strings defined for AlarmStrs, MiscStrs and
	// PromptStrs. Set during GuiApp initialization based on the LanguageValue
	// setting.
	static Uint16 LanguageIndex_;

	//@ Data-Member: IsProxInstalled_
	// Is proximal board installed
	static Boolean IsProxInstalled_;
};

// Inlined methods...
#include "GuiApp.in"

#endif // GuiApp_HH 
