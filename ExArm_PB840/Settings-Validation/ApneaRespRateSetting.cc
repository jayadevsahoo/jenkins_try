#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class:  ApneaRespRateSetting - Apnea Respiratory Rate Setting.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that contains the rate of respiration
//  (as breaths-per-minute) that is to be applied to the patient during
//  apnea ventilation.  This class inherits from 'BatchBoundedSetting' and
//  provides the specific information needed for representation of apnea
//  respiratory rate.  This information includes the interval and range of
//  this setting's values, this batch setting's new-patient value (see
//  'getNewPatientValue()'), and the dependent settings of this setting.
//
//  This class provides a static method for calculating the apnea breath
//  period based on the current apnea respiratory rate.  This calculation
//  method provides a standard way for all settings (as needed) to calculate
//  an apnea breath period.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class defines an 'updateConstraints_()' method for updating
//  the constraints of this setting.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ApneaRespRateSetting.ccv   25.0.4.0   19 Nov 2013 14:27:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By: sah   Date:  16-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  removed apnea minute volume as a dependent setting, now
//         automatically tracks changes to this setting
//
//  Revision: 005   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//	*  incorporated new, circuit-specific new-patient values
//	*  incorporated new 'warpToRange()' method
//
//  Revision: 004   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  added new apnea minute volume setting as a dependent setting
//	*  added new 'getApplicability()' method
//	*  cleaned up DEVELOPMENT-only 'is{Adjusted,Accepted}Valid()' methods
//
//  Revision: 003   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 002   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	No longer "round" the calculated breath period to a resolution
//	value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//=====================================================================

#include "ApneaRespRateSetting.hh"
#include "SettingConstants.hh"
#include "ConstantParmValue.hh"
#include "MandTypeValue.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
//@ End-Usage

//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

// $[02094] -- this setting's dependent settings...
static const SettingId::SettingIdType  ARR_DEPENDENT_SETTING_IDS_[] =
  {
    SettingId::APNEA_EXP_TIME,
    SettingId::APNEA_IE_RATIO,

    SettingId::NULL_SETTING_ID
  };


//  $[02090] -- The setting's range ...
//  $[02092] -- The setting's resolution ...
static const BoundedInterval  LAST_NODE_ =
  {
    2.0f,		// absolute minimum...
    0.0f,		// unused...
    TENTHS,		// unused...
    NULL
  };
static const BoundedInterval  NODE1_ =
  {
    10.0f,		// change-point...
    0.1f,		// resolution...
    TENTHS,		// precision...
    &::LAST_NODE_
  };
static const BoundedInterval  INTERVAL_LIST_ =
  {
    40.0f,		// absolute maximum...
    1.0f,		// resolution...
    ONES,		// precision...
    &::NODE1_
  };



//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: ApneaRespRateSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This setting inherits from BoundedSetting.  The method passes to
//  BoundedSetting it's default information.  Also, this method initializes
//  the value interval.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaRespRateSetting::ApneaRespRateSetting(void)
      : BatchBoundedSetting(SettingId::APNEA_RESP_RATE,
			    ::ARR_DEPENDENT_SETTING_IDS_,
			    &::INTERVAL_LIST_,
			    APNEA_RESP_RATE_MAX_ID,	// maxConstraintId...
			    APNEA_RESP_RATE_MIN_ID)	// minConstraintId...
{
  CALL_TRACE("ApneaRespRateSetting()");
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~ApneaRespRateSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

ApneaRespRateSetting::~ApneaRespRateSetting(void)
{
  CALL_TRACE("~ApneaRespRateSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability()  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Apnea respiratory rate is ALWAYS changeable.
//
//  $[01071] -- main settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
ApneaRespRateSetting::getApplicability(const Notification::ChangeQualifier) const
{
  CALL_TRACE("getApplicability(qualifierId)");

  return(Applicability::CHANGEABLE);
}   // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new-patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[02091] -- new-patient value...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
ApneaRespRateSetting::getNewPatientValue(void) const
{
  CALL_TRACE("getNewPatientValue()");

  BoundedValue  newPatient;

  const Setting*  pPatientCctType =
		    SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

  const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
					pPatientCctType->getAdjustedValue();

  switch (PATIENT_CCT_TYPE_VALUE)
  {
  case PatientCctTypeValue::NEONATAL_CIRCUIT :		// $[TI1]
    newPatient.value = SettingConstants::NP_RESP_RATE_NEO_VALUE;
    break;
  case PatientCctTypeValue::PEDIATRIC_CIRCUIT :		// $[TI2]
    newPatient.value = SettingConstants::NP_RESP_RATE_PED_VALUE;
    break;
  case PatientCctTypeValue::ADULT_CIRCUIT :		// $[TI3]
    newPatient.value = SettingConstants::NP_RESP_RATE_ADULT_VALUE;
    break;
  default :
    // unexpected patient circuit type value...
    AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
    break;
  }

  newPatient.precision = ONES;

  return(newPatient);
}


#if defined(SIGMA_DEVELOPMENT)

//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAcceptedValid()  [const, virtual]
//
// Interface-Description
//  Is the accepted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaRespRateSetting::isAcceptedValid(void) const
{
  CALL_TRACE("isAcceptedValid()");

  Boolean  isValid = BoundedSetting::isAcceptedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ACCEPTED) != Applicability::INAPPLICABLE)
    {
      // is the accepted apnea respiratory rate value consistent with the
      // accepted apnea expiratory time, apnea I:E ratio, and apnea inspiratory
      // time?...
      const Setting*  pApneaExpTime =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME);
      const Setting*  pApneaIeRatio =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO);
      const Setting*  pApneaInspTime =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

      isValid = (pApneaExpTime->isAcceptedValid()  &&
		 pApneaIeRatio->isAcceptedValid()  &&
		 pApneaInspTime->isAcceptedValid());
    }
  }

  return(isValid);
}


//============== M E T H O D   D E S C R I P T I O N =================
// Method:  isAdjustedValid()  [const, virtual]
//
// Interface-Description
//  Is the adjusted value of this setting valid as determined by its
//  dependencies?
//---------------------------------------------------------------------
// Implementation-Description
//  The setting determines, and returns its validity.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

Boolean
ApneaRespRateSetting::isAdjustedValid(void)
{
  CALL_TRACE("isAdjustedValid()");

  Boolean  isValid = BoundedSetting::isAdjustedValid();

  if (isValid)
  {
    if (getApplicability(Notification::ADJUSTED) != Applicability::INAPPLICABLE)
    {
      // is the adjusted apnea respiratory rate value consistent with the
      // adjusted apnea expiratory time, apnea I:E ratio, and apnea inspiratory
      // time?...
      Setting*  pApneaExpTime =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME);
      Setting*  pApneaIeRatio =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO);
      Setting*  pApneaInspTime =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

      isValid = (pApneaExpTime->isAdjustedValid()  &&
		 pApneaIeRatio->isAdjustedValid()  &&
		 pApneaInspTime->isAdjustedValid());
    }
  }

  return(isValid);
}

#endif  // defined(SIGMA_DEVELOPMENT)


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  GetApneaBreathPeriod(basedOnCategory)  [static]
//
//@ Interface-Description
//  Return the breath period that is based on the apnea respiratory rate's
//  "proposed" value.  Only the adjusted breath period can be queried (in
//  PRODUCTION mode).  The result is "rounded" to apnea expiratory time's
//  resolution.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  (basedOnCategory == BASED_ON_ADJUSTED_VALUES  ||
//   basedOnCategory == BASED_ON_NEW_PATIENT_VALUES)
//@ End-Method
//=====================================================================

Real32
ApneaRespRateSetting::GetApneaBreathPeriod(
				      const BasedOnCategory basedOnCategory
					  )
{
  CALL_TRACE("GetApneaBreathPeriod(basedOnCategory)");

  const Setting*  pApneaRespRate =
		      SettingsMgr::GetSettingPtr(SettingId::APNEA_RESP_RATE);

  Real32  apneaRespRateValue;

  switch (basedOnCategory)
  {
  case BASED_ON_NEW_PATIENT_VALUES :	// $[TI1]
    // get apnea respiratory rate's "new-patient" value...
    apneaRespRateValue =
		    BoundedValue(pApneaRespRate->getNewPatientValue()).value;
    break;
  case BASED_ON_ADJUSTED_VALUES :	// $[TI2]
    // get apnea respiratory rate's "adjusted" value...
    apneaRespRateValue =
		    BoundedValue(pApneaRespRate->getAdjustedValue()).value;
    break;
  case BASED_ON_ACCEPTED_VALUES :
    // fall through to the 'default' case, if not DEVELOPMENT...
#if defined(SIGMA_DEVELOPMENT)
    // get apnea respiratory rate's "accepted" value...
    apneaRespRateValue =
		    BoundedValue(pApneaRespRate->getAcceptedValue()).value;
    break;
#endif // defined(SIGMA_DEVELOPMENT)
  default :
    // unexpected 'basedOnCategory' value...
    AUX_CLASS_ASSERTION_FAILURE(basedOnCategory);
    break;
  };

  BoundedValue apneaBreathPeriod;

  // calculate the apnea breath period based on the apnea respiratory rate...
  apneaBreathPeriod.value = (SettingConstants::MSEC_TO_MIN_CONVERSION /
				       apneaRespRateValue);

  // round breath period
  BoundedSetting*  pApneaExpTime =
		  SettingsMgr::GetBoundedSettingPtr(SettingId::APNEA_EXP_TIME);

  pApneaExpTime->warpToRange(apneaBreathPeriod, BoundedRange::WARP_NEAREST,
			     FALSE);

  return(apneaBreathPeriod.value);
} 


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaRespRateSetting::SoftFault(const SoftFaultID  softFaultID,
			        const Uint32       lineNumber,
			        const char*        pFileName,
			        const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
			  APNEA_RESP_RATE_SETTING, lineNumber, pFileName,
			  pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Determine the most restrictive upper and lower bounds, and set the
//  limits to them.
//
//  $[02090] -- this setting's dynamic bounds...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
ApneaRespRateSetting::updateConstraints_(void)
{
  CALL_TRACE("updateConstraints_()");

  const Setting*  pApneaInterval =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_INTERVAL);

  // get the current apnea interval from the adjusted context...
  const Real32  APNEA_INTERVAL_VALUE =
		      BoundedValue(pApneaInterval->getAdjustedValue()).value;

  // get apnea respiratory rate's absolute minimum value...
  const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

  //===================================================================
  // calculate apnea respiratory rate's MINIMUM BOUND that is based on
  // the current apnea interval...
  //===================================================================

  BoundedRange::ConstraintInfo  minConstraintInfo;

  BoundedValue  apneaIntervalBasedMin;

  // calculate the apnea interval bound (apnea can not be possible in
  // apnea)...
  apneaIntervalBasedMin.value =
	(SettingConstants::MSEC_TO_MIN_CONVERSION / APNEA_INTERVAL_VALUE);

  //===================================================================
  // determine the most restrictive of apnea respiratory rate's two
  // contending minimum bounds...
  //===================================================================

  if (apneaIntervalBasedMin.value > ABSOLUTE_MIN)
  {   // $[TI1]
    // reset this range's minimum constraint to allow for "warping" of a
    // new minimum value...
    getBoundedRange_().resetMinConstraint();

    // warp this value "UP", because we don't want the apnea breath
    // period to EQUAL the apnea interval, it should be greater than...
    getBoundedRange_().warpValue(apneaIntervalBasedMin, BoundedRange::WARP_UP);

    // apnea respiratory rate's apnea-interval bound is more restrictive...
    minConstraintInfo.id    = APNEA_RESP_RATE_MIN_BASED_INTERVAL_ID;
    minConstraintInfo.value = apneaIntervalBasedMin.value;
  }
  else
  {   // $[TI2]
    // apnea respiratory rate's absolute minimum bound is more restrictive...
    minConstraintInfo.id    = APNEA_RESP_RATE_MIN_ID;
    minConstraintInfo.value = ABSOLUTE_MIN;
  }

  // this setting has no minimum soft bounds...
  minConstraintInfo.isSoft = FALSE;

  getBoundedRange_().updateMinConstraint(minConstraintInfo);
}


//============== M E T H O D   D E S C R I P T I O N ================
//@ Method:  calcNewValue(knobDelta)  [virtual]
//
//@ Interface-Description
//  This method determines the new setting value from the knob delta.
//  It also checks the new value against its bounds, and updates any
//  dependent settings.  A constant reference to this setting's bound
//  status is returned; the bound status will contain bound information
//  about this setting's violated bound, if any.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is overridden to modify the changing of this setting.
//  Due to the dual dependency of respiratory rate's dependent settings
//  on respiratory rate's value for both the dependent setting VALUES
//  and their bounds, an implementation that a set of changes by '1' is
//  necessary.  Otherwise, as respiratory rate changes, the dependent
//  settings' values and bounds move towards each other, which requires
//  a complex balancing of changing respiratory rate, then backing out,
//  etc.  This overridden behavior of changing by deltas of only '1' at
//  a time, greatly simplifies the implementation.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

const BoundStatus&
ApneaRespRateSetting::calcNewValue(const Int16 knobDelta)
{
  CALL_TRACE("calcNewValue(knobDelta)");

  const Uint  ABS_KNOB_DELTA   = ABS_VALUE(knobDelta);
  const Uint  MAX_DELTA_COUNTS = MIN_VALUE(ABS_KNOB_DELTA,	// $[TI1]
					   10);			// $[TI2]
  const Int   KNOB_CLICK       = (knobDelta > 0) ?  1		// $[TI3]
  						 : -1;		// $[TI4]

  const BoundStatus&  rBoundStatus = getBoundStatus();
  Uint                deltaCount = 0u;

  do
  {
    BoundedSetting::calcNewValue(KNOB_CLICK);
  } while (!rBoundStatus.isInViolation()  &&
  	   ++deltaCount < MAX_DELTA_COUNTS);

  return(rBoundStatus);
}

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method: calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)  [const]
//
//@ Interface-Description
//  This method is responsible for calculating, and returning, a new
//  "adjusted" value for this setting, based on the "adjusted" value
//  of the dependent setting given by 'basedOnSettingId'.  This method will
//  calculate values for each of this setting's dependent settings,
//  based on values in the Adjusted Context.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  ((basedOnSettingId == SettingId::APNEA_EXP_TIME  ||
//    basedOnSettingId == SettingId::APNEA_IE_RATIO)  &&
//   basedOnCategory == BASED_ON_ADJUSTED_VALUES)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

BoundedValue
ApneaRespRateSetting::calcBasedOnSetting_(
			  const SettingId::SettingIdType basedOnSettingId,
			  const BoundedRange::WarpDir    warpDirection,
			  const BasedOnCategory          basedOnCategory
					 ) const
{
  CALL_TRACE("calcBasedOnSetting_(basedOnSettingId, warpDirection, basedOnCategory)");

  AUX_CLASS_ASSERTION((basedOnCategory == BASED_ON_ADJUSTED_VALUES),
		      basedOnCategory);

#if defined(SIGMA_DEVELOPMENT)
  // make sure that the apnea constant parm value is insp time...
  const Setting*  pApneaConstantParm =
		   SettingsMgr::GetSettingPtr(SettingId::APNEA_CONSTANT_PARM);
  const DiscreteValue  APNEA_CONSTANT_PARM_VALUE =
					pApneaConstantParm->getAdjustedValue();
  AUX_CLASS_ASSERTION(
	  (APNEA_CONSTANT_PARM_VALUE == ConstantParmValue::INSP_TIME_CONSTANT),
	  APNEA_CONSTANT_PARM_VALUE
		     );
#endif // defined(SIGMA_DEVELOPMENT)

  const Setting*  pApneaInspTime =
		       SettingsMgr::GetSettingPtr(SettingId::APNEA_INSP_TIME);

  // get the apnea insp time value from the Adjusted Context...
  const Real32  APNEA_INSP_TIME_VALUE =
		       BoundedValue(pApneaInspTime->getAdjustedValue()).value;

  Real32  apneaBreathPeriodValue;

  switch (basedOnSettingId)
  {
  // $[02018](1) -- using apnea I:E and Ti, calculate apnea Ttot...
  case SettingId::APNEA_IE_RATIO :
    {   // $[TI1]
      const Setting*  pApneaIeRatio =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_IE_RATIO);

      // get the apnea I:E ratio value from the Adjusted Context...
      const Real32  APNEA_IE_RATIO_VALUE =
			BoundedValue(pApneaIeRatio->getAdjustedValue()).value;

      Real32  eRatioValue;
      Real32  iRatioValue;

      if (APNEA_IE_RATIO_VALUE < 0.0f)
      {   // $[TI1.1]
	// the apnea I:E ratio value is negative, therefore we have 1:xx with
	// "xx" the apnea expiratory portion of the ratio...
	iRatioValue = 1.0f;
	eRatioValue = -(APNEA_IE_RATIO_VALUE);
      }
      else
      {   // $[TI1.2]
	// the apnea I:E ratio value is positive, therefore we have xx:1 with
	// "xx" the apnea inspiratory portion of the ratio...
	iRatioValue = APNEA_IE_RATIO_VALUE;
	eRatioValue = 1.0f;
      }

      // calculate the apnea breath period from the apnea I:E and Ti...
      apneaBreathPeriodValue = APNEA_INSP_TIME_VALUE *
				((iRatioValue + eRatioValue) / iRatioValue);
    }
    break;

  // $[02018](2) -- using apnea Te and Ti, calculate apnea Ttot...
  case SettingId::APNEA_EXP_TIME :
    {   // $[TI2]
      const Setting*  pApneaExpTime =
			SettingsMgr::GetSettingPtr(SettingId::APNEA_EXP_TIME);

      // get the apnea expiratory time value from the Adjusted Context...
      const Real32  APNEA_EXP_TIME_VALUE =
			BoundedValue(pApneaExpTime->getAdjustedValue()).value;

      // calculate apnea breath period...
      apneaBreathPeriodValue = APNEA_INSP_TIME_VALUE + APNEA_EXP_TIME_VALUE;
    }
    break;

  default :
    // unexpected dependent setting id...
    AUX_CLASS_ASSERTION_FAILURE(basedOnSettingId);
    break;
  };

  BoundedValue  apneaRespRate;

  // calculate an apnea respiratory rate from the breath period calculated
  // above...
  apneaRespRate.value = (SettingConstants::MSEC_TO_MIN_CONVERSION /
				 apneaBreathPeriodValue);

  // place apnea resp rate on a "click" boundary...
  getBoundedRange().warpValue(apneaRespRate, warpDirection, FALSE);

  return(apneaRespRate);
}
