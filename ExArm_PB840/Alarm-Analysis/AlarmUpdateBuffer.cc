#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmUpdateBuffer - Contains AlarmUpdate information used for annunciation.
//---------------------------------------------------------------------
//@ Interface-Description
//  This class provides constructors to easily build AlarmUpdateBuffer objects 
//  from constituent data and other AlarmUpdateBuffer objects.  Once 
//  constructed, the data is never changed.  Methods to read the data are 
//  provided.
//---------------------------------------------------------------------
//@ Rationale
//  AlarmUpdateBuffer contains information needed to analyze an alarm as well as
//  annunciate an alarm.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class is a simple repository for information pertaining to annunciate 
//  the alarm.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmUpdateBuffer.ccv   25.0.4.0   19 Nov 2013 13:51:18   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 001   By:   hct      Date: 06/27/95         DR Number:   499
//      Project:   Sigma   (R8027)
//      Description:
//         Added constructor to build and AlarmUpdateBuffer from an
//         AlarmCommList.  
//  
//   Revision 002   By:   hct      Date: 07/11/95         DR Number:   499
//      Project:   Sigma   (R8027)
//      Description:
//         Made attribute alarmUpdateList_ non-const so it can be changed in
//         the body of the constructor added in revision 001.
//
//   Revision 003   By:   gbs      Date: 03/12/96         DR Number:   784
//      Project:   Sigma   (R8027)
//      Description:
//         Add info to AlarmUpdate to determine from which CPU it
//         originated.
//
//   Revision 004   By:   gbs      Date: 05/17/96         DR Number:   995
//      Project:   Sigma   (R8027)
//      Description:
//         Fix CALL_TRACE and add passed parameter comments.
//         These changes do not affect Unit Test.
//  
//   Revision 005   By:   hct      Date: 01/03/97         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.  Removed fromCpu handling.
//  
//   Revision 006   By:   hct      Date: 02/10/97         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.  Don't need AlarmComm anymore.
//  
//   Revision 007   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "AlarmUpdateBuffer.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmUpdateBuffer
//
//@ Interface-Description
//      Constructor.
//
// >Von
//  rAlarmUpdateList            Holds the AlarmUpdateList reference.
//  updateType                  Holds the value of
//                              Alarm::GetUpdateType().
//  cancelScreenLockStatus      Holds the Boolean value of 
//                              Alarm::GetCancelScreenLockStatus().
//  noPatientDataDisplayStatus  Holds the Boolean value of 
//                              Alarm::GetNoPatientDataDisplayStatus().
//  noGuiSettingChangesStatus   Holds the Boolean value of
//                              Alarm::GetNoGuiSettingChangesStatus().
//  ventInopLedOnStatus         Holds the Boolean value of
//                              Alarm::GetVentInopLedOnStatus().
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   ((Alarm::UPDATE_TYPE_NULL <= updateType) &&
//       (updateType < Alarm::MAX_NUM_UPDATE_TYPE))
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

AlarmUpdateBuffer::AlarmUpdateBuffer(const AlarmUpdateList& rAlarmUpdateList,
                   Alarm::UpdateType          updateType,
                   Boolean                    cancelScreenLockStatus,
                   Boolean                    noPatientDataDisplayStatus,
                   Boolean                    noGuiSettingChangesStatus,
                   Boolean                    ventInopLedOnStatus):
                   alarmUpdateList_(rAlarmUpdateList),
                   updateType_(updateType),
                   cancelScreenLockStatus_(cancelScreenLockStatus),
                   noPatientDataDisplayStatus_(noPatientDataDisplayStatus),
                   noGuiSettingChangesStatus_(noGuiSettingChangesStatus),
                   ventInopLedOnStatus_(ventInopLedOnStatus)
{
  CALL_TRACE("AlarmUpdateBuffer(rAlarmUpdateList, updateType, cancelScreenLockStatus, noPatientDataDisplayStatus, noGuiSettingChangesStatus, ventInopLedOnStatus)");

  CLASS_PRE_CONDITION((Alarm::UPDATE_TYPE_NULL <= updateType) &&
                      (updateType < Alarm::MAX_NUM_UPDATE_TYPE));
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: AlarmUpdateBuffer
//
//@ Interface-Description
//   Copy constructor.
//
// >Von
//   rBuff  Reference to the object to be copied from.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

AlarmUpdateBuffer::AlarmUpdateBuffer(const AlarmUpdateBuffer& rBuff):
                alarmUpdateList_(rBuff.alarmUpdateList_),
                updateType_(rBuff.updateType_),
                cancelScreenLockStatus_(rBuff.cancelScreenLockStatus_),
                noPatientDataDisplayStatus_(rBuff.noPatientDataDisplayStatus_),
                noGuiSettingChangesStatus_(rBuff.noGuiSettingChangesStatus_),
                ventInopLedOnStatus_(rBuff.ventInopLedOnStatus_)
 
{
  CALL_TRACE("AlarmUpdateBuffer(rBuff)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~AlarmUpdateBuffer
//
//@ Interface-Description
//   Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

AlarmUpdateBuffer::~AlarmUpdateBuffer(void)
{
  CALL_TRACE("~AlarmUpdateBuffer(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================
void
AlarmUpdateBuffer::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::ALARMUPDATEBUFFER, lineNumber,  
                          pFileName, pPredicate);
}
