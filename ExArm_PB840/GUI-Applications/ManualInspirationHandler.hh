#ifndef ManualInspirationHandler_HH
#define ManualInspirationHandler_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ManualInspirationHandler - Handles control of Manual Inspiration
// maneuver (initiated by the Manual Inspiration off-screen key).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ManualInspirationHandler.hhv   25.0.4.0   19 Nov 2013 14:08:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd Date:  17-MAY-95	DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//  	Integration baseline.
//====================================================================

#include "KeyPanelTarget.hh"
#include "BdEventTarget.hh"
#include "GuiAppClassIds.hh"

class ManualInspirationHandler :	public KeyPanelTarget, 
									public BdEventTarget 
{
public:
	ManualInspirationHandler(void);
	~ManualInspirationHandler(void);

	// Key Panel Target virtual methods
	virtual void keyPanelPressHappened(KeyPanel::KeyId key);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId key);

    virtual void bdEventHappened(EventData::EventId eventId,
		                            EventData::EventStatus eventStatus,
									EventData::EventPrompt eventPrompt=EventData::NULL_EVENT_PROMPT); 

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	ManualInspirationHandler(const ManualInspirationHandler&);// not implemented...
	void   operator=(const ManualInspirationHandler&);		// not implemented...
};


#endif // ManualInspirationHandler_HH
