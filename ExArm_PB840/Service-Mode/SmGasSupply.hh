#ifndef SmGasSupply_HH
#define SmGasSupply_HH
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: SmGasSupply - Gas supply detection and compressor control
//	for Service Mode
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmGasSupply.hhv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date: 11-Sep-1997    DR Number: DCS 2280
//       Project:  Sigma (840)
//       Description:
//            Eliminated global variable LOW_AC_FACTOR, eliminated
//            use of local calculation of the low AC criteria and
//            now this is done via PowerSource::DetermineLowAcThresh().
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  mad    Date: 09-Feb-1996    DR Number: none
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "ServiceMode.hh"

//@ Usage-Classes

#include "DiscreteValue.hh"

//@ End-Usage

class SmGasSupply
{
  public:
	enum CompressorState
	{
		INIT,
		DISABLED,
		RUN,
		STANDBY
	};

	SmGasSupply(void);
	~SmGasSupply(void);

	static void SoftFault(const SoftFaultID softFaultID,
		  const Uint32      lineNumber,
		  const char*       pFileName  = NULL, 
		  const char*       pPredicate = NULL);

	inline Boolean isWallAirPresent(void) const;
	inline Boolean isCompressorAirPresent(void) const;
	inline Boolean isAnyAirPresent(void) const;
	inline Boolean isO2Present(void) const;
	inline Boolean isAnyGasPresent(void) const;

	void enableCompressorAndControl(void);
	void disableCompressorAndControl(void);

	void setNominalVoltage(DiscreteValue nominalLineVolt);

	void newCycle (void);

  protected:

  private:
	SmGasSupply(const SmGasSupply&);	// not implemented...
	void operator=(const SmGasSupply&);	// not implemented...

	void enableCompressor_(void);

    //@ Data-Member: isWallAirPresent_
    // indicates if wall air is available
    Boolean isWallAirPresent_;

    //@ Data-Member: isCompressorAirPresent_
    // indicates if compressor air is available
    Boolean isCompressorAirPresent_;

    //@ Data-Member: isAnyAirPresent_
    // indicates if any air source is available
    Boolean isAnyAirPresent_;

    //@ Data-Member: isO2Present_
    // indicates if o2 is available
    Boolean isO2Present_;

    //@ Data-Member: isAnyGasPresent_
    // indicates if any gas source is available
    Boolean isAnyGasPresent_;

    //@ Data-Member:  nominalVoltage_
    // nominal AC line voltage
    Real32 nominalVoltage_;

    //@ Data-Member: compressorControlEnabled_
    // indicates if automatic compressor control is enabled
    Boolean compressorControlEnabled_;

    //@ Data-Member:  compressorState_
    // current state of the compressor controller
	CompressorState compressorState_;

};


// Inlined methods...
#include "SmGasSupply.in"

#endif // SmGasSupply_HH 
