#include "StdAfx.h"
#include "GlobalObjects.h"

GlobalObjects::GlobalObjects(void)
{
	initialize_();
}

GlobalObjects::~GlobalObjects(void)
{
}

GlobalObjects* GlobalObjects::getInstance()
{
	if ( Instance_ == NULL )
	{
		Instance_ = new	GlobalObjects;
	}

	return Instance_;
}

void GlobalObjects::initialize_()
{
	LOGFONT logFont;
	memset(&logFont, 0, sizeof(LOGFONT));
	wcscpy_s(logFont.lfFaceName, 32, _T("Arial"));
	logFont.lfHeight = 16;
	logFont.lfWeight = FW_BOLD;
	logFont.lfCharSet = ANSI_CHARSET;
	logFont.lfQuality = ANTIALIASED_QUALITY;
	
	Arial_16_Bold_.CreateFontIndirect(&logFont);

	logFont.lfHeight = 24;
	Arial_24_Bold_.CreateFontIndirect(&logFont);

	logFont.lfHeight = 48;
	Arial_48_Bold_.CreateFontIndirect(&logFont);


	////////////////////////////////////////////////////////////////////////

	PenForCurve_.CreatePen(PS_SOLID, 1, RGB(0, 255, 0));
	PenForOldCurve_.CreatePen(PS_SOLID, 1, RGB(0, 127, 0));
	PenForWiper_.CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
}

CFont* GlobalObjects::getArial_16_Bold()
{
	return &Arial_16_Bold_;
}

CFont* GlobalObjects::getArial_24_Bold()
{
	return &Arial_24_Bold_;
}

CFont* GlobalObjects::getArial_48_Bold()
{
	return &Arial_48_Bold_;
}

CPen* GlobalObjects::getPen( GlobalPenType penType )
{
	switch ( penType )
	{
	case Pen_Curve:
		return &PenForCurve_;
		break;
	case Pen_OldCurve:
		return &PenForOldCurve_;
		break;
	case Pen_Wiper:
		return &PenForWiper_;
		break;
	}
	return &PenForWiper_;
}


GlobalObjects* GlobalObjects::Instance_ = NULL;
