#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: MsgTimer - General purpose timer class that tasks use to
// get notification of a future time event.  There are two types of
// time events supported -- one-shot events and periodic events.
//---------------------------------------------------------------------
//@ Interface-Description
// When the timer expires, the given message is posted to the specified
// task queue.
//---------------------------------------------------------------------
//@ Rationale
//  Provides a object that can be used as a timer for periodic or one shot
//  time events.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A MsgTimer consists of a qId, msg, time, and event type. This object
//  is placed in a TimerTbl::MsgTimerTbl where TimerTbl::MsgTimerTask will 
//  check for time expiration and post the given message to the given 
//  queue.
//---------------------------------------------------------------------
//@ Fault-Handling
//  Assertions are used to catch fault conditions, along with pre and
//  post conditions where applicable.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/MsgTimer.ccv   25.0.4.0   19 Nov 2013 14:15:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
// 
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "MsgTimer.hh"
#include "IpcIds.hh"

#include "TimerTbl.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: MsgTimer  [Constructor]
//
//@ Interface-Description
//  Constructs a MsgTimer object. Four parameters exist, qId, msg, 
//  msec, and eventType. Qid specifies which queue will be posted a 
//  message when the timer expires. The time will be in msec, this is
//  how long the interval should be. The eventType is either PERIODIC or
//  ONE_SHOT.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Intializes private data members and calls TimerTbl::AddMsgTimer.
//  TimerTbl::AddMsgTimer returns the timerId_.
//---------------------------------------------------------------------
//@ PreCondition
//  (msec >= MIN_MSEC_REQUEST)
//  (qId > 0 && qId < MAX_QID)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MsgTimer::MsgTimer(const Int32 qId, const Int32 msg, const Int32 msec,
                   MsgTimer::EventType eventType) :
	qId_(qId), 
	msg_(msg),
	msec_(msec),
	eventType_(eventType)
{
	CLASS_PRE_CONDITION(msec >= MIN_MSEC_REQUEST);
	CLASS_PRE_CONDITION(qId > 0 && qId < MAX_QID);
	timerId_ = TimerTbl::AddMsgTimer_(qId, msg, msec, eventType);
	CLASS_ASSERTION(timerId_ > 0);								// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~MsgTimer  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

MsgTimer::~MsgTimer(void)
{
	CALL_TRACE("MsgTimer::~MsgTimer(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: set
//
//@ Interface-Description
//  Activates this timer object in the TimerTbl. Has no parameters. 
//  Returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls TimerTbl::Set(timerId_).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MsgTimer::set(void)
{
	CALL_TRACE("MsgTimer::set(void)");

	TimerTbl::Set_(timerId_);
	                        //$[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: set
//
//@ Interface-Description
//  Activates this Timer entry using the new timer count. Takes an Int32
//  as the new time in milliseconds. Returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls TimerTbl::Set(timerId_, msec). 
//---------------------------------------------------------------------
//@ PreCondition
//  msec >= MIN_MSEC_REQUEST
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
MsgTimer::set(const Int32 msec)
{
	CALL_TRACE("MsgTimer::set(void)");

	msec_ = msec;

	CLASS_PRE_CONDITION(msec >= MIN_MSEC_REQUEST);
	TimerTbl::Set_(timerId_, msec);
	                        //$[TI2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: cancel
//
//@ Interface-Description
//  Cancels timer for a given timerId. Returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls TimerTbl::Cancel(timerId_).
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
MsgTimer::cancel(void)
{
	CALL_TRACE("MsgTimer::cancel(void)");

	TimerTbl::Cancel_(timerId_);		// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isSet
//
//@ Interface-Description
//  Returns true if "timerId" is set.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls TimerTbl::IsSet(timerId_).
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
Boolean
MsgTimer::isSet(void) const
{
    CALL_TRACE("MsgTimer::isSet(void)");

    return TimerTbl::IsSet_(timerId_);	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator==(const MsgTimer &)
//
//@ Interface-Description
//  This method is not useable; only defined for the list class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

Boolean
MsgTimer::operator==(const MsgTimer &) const
{
	CALL_TRACE("MsgTimer::operator==(MsgTimer &)");
	CLASS_ASSERTION_FAILURE();		// should never get here

	return(TRUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  operator!=(const MsgTimer &)
//
//@ Interface-Description
//  This method is not useable; only defined for the list class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

Boolean
MsgTimer::operator!=(const MsgTimer &) const
{
	CALL_TRACE("MsgTimer::operator!=(MsgTimer)");
	CLASS_ASSERTION_FAILURE();		// should never get here

	return(FALSE); 
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
MsgTimer::SoftFault(const SoftFaultID  softFaultID,
		    const Uint32       lineNumber,
		    const char*        pFileName,
		    const char*        pPredicate)
{
        CALL_TRACE("MsgTimer::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

        FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::MSG_TIMER, lineNumber, pFileName, pPredicate);
}
