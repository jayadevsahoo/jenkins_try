//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIUserEnum.cpp
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"PatientDataUIUserEnum.h"


//-------------------------------------
// C O D E
//-------------------------------------

//---------------------------
// UserEnum::UserEnum   
//---------------------------
UserEnum::UserEnum(char *name, Enum::EnumDef *pEnumDef):
	UserInt(name,0),
	m_pEnumDef(pEnumDef)
{
}

//---------------------------
// UserEnum::isEnum    
//---------------------------
bool UserEnum::isEnum()
{
    return true;
}

//---------------------------
// UserEnum::getEnum    
//---------------------------
char* UserEnum::getEnum(int cnt)
{
    Enum::EnumDef *pTemp;
    
    pTemp=Enum::getEnum(m_pEnumDef,cnt+1);

    if(pTemp)
    {
        return pTemp->name;
    }

    return NULL;    
}

//---------------------------
// UserEnum::getEnumValue    
//---------------------------
char *UserEnum::getEnumValue()
{
    Enum::EnumDef *temp;


    if(m_pEnumDef==NULL)
    {
        return NULL;
    }

    temp=Enum::getEnum(m_pEnumDef,m_iValue);

    if(temp==NULL)
    {
        return NULL;
    }

    return temp->name;
}

//---------------------------
// UserEnum::setEnum    
//---------------------------
bool  UserEnum::setEnum (unsigned int uiValue)
{
    Enum::EnumDef *pTemp;
    if( 
         (m_pEnumDef == NULL)
        || ((pTemp=Enum::getEnum(m_pEnumDef,uiValue+1))==NULL)
      )
    {
        return false;
    }

    UserInt::setInt(pTemp->def_value);


    return true;

}
