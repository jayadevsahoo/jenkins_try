#ifndef EventSubScreen_HH
#define EventSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: EventSubScreen - Activated by selecting the event tab button
//                          in the upper screen. This subscreen allows 
//                          the user to enter manual events. 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/EventSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: mnr    Date: 28-Dec-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      Removed isNeoModeUpdateEnabled_.
//
//  Revision: 002   By: mnr    Date: 23-Nov-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//      New Trend Manual Event buttons added.
//
//  Revision: 001  By: rhj    Date: 06-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Initial version.
//=====================================================================

#include "SubScreen.hh"
#include "TextButton.hh"
#include "SubScreenTitleArea.hh"
#include "GuiTimerRegistrar.hh"
#include "GuiTimerTarget.hh"
#include "GuiTimerId.hh"
#include "AdjustPanelTarget.hh"
#include "Box.hh"


class SubScreenArea;

class EventSubScreen : public SubScreen,
public GuiTimerTarget
{
public:
	EventSubScreen(SubScreenArea* pSubScreenArea);
	~EventSubScreen(void);

	virtual void activate(void);
	virtual void deactivate(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	virtual void buttonDownHappened(Button* pButton,
									Boolean isByOperatorAction);
	virtual void buttonUpHappened(Button* pButton,
								  Boolean isByOperatorAction);

	// GuiTimerRegistrar virtual method
	void timerEventHappened(GuiTimerId::GuiTimerIdType);

private:
	// these methods are purposely declared, but not implemented...
	EventSubScreen(void);						// not implemented...
	EventSubScreen(const EventSubScreen&);	// not implemented...
	void operator=(const EventSubScreen&);		// not implemented...

	Boolean isEventButton_(Button *pButton );

	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: eventSuctionButton_
	//  Event Button - "Suction"
	TextButton eventSuctionButton_;

	//@ Data-Member: eventRxButton_
	//  Event Button - "Rx"
	TextButton eventRxButton_;

	//@ Data-Member: eventBloodGasButton_
	//  Event Button - "Blood Gas"
	TextButton eventBloodGasButton_;

	//@ Data-Member: eventRespiratoryManeuverButton_
	//  Event Button - "Respiratory Maneuver"
	TextButton eventRespiratoryManeuverButton_;

	//@ Data-Member: eventCircuitChangeButton_
	//  Event Button - "Circuit Change"
	TextButton eventCircuitChangeButton_;

	//@ Data-Member: eventButton_
	//  Event Button - "Start Weaning"
	TextButton eventStartWeaningButton_;

	//@ Data-Member: eventStopWeaningButton_
	//  Event Button - "Stop Weaning"
	TextButton eventStopWeaningButton_;

	//@ Data-Member: eventBronchoscopyButton_
	//  Event Button - "Bronchoscopy"
	TextButton eventBronchoscopyButton_;

	//@ Data-Member: eventXrayButton_
	//  Event Button - "X-Ray"
	TextButton eventXrayButton_;

	//@ Data-Member: eventRecruitmentManeuverButton_
	//  Event Button - "Recruitment Maneuver"
	TextButton eventRecruitmentManeuverButton_;

	//@ Data-Member: eventRespositionPatientButton_
	//  Event Button - "Reposition Patient"
	TextButton eventRespositionPatientButton_;

	//@ Data-Member: eventOtherButton_
	//  Event Button - "Other"
	TextButton eventOtherButton_;

	//@ Data-Member: eventSurfactantAdminButton_
	//  Event Button - "Surfactant Admin"
	TextButton eventSurfactantAdminButton_;

	//@ Data-Member: eventPronePositionButton_
	//  Event Button - "Prone Position"
	TextButton eventPronePositionButton_;

	//@ Data-Member: eventSupinePositionButton_
	//  Event Button - "Supine Position"
	TextButton eventSupinePositionButton_;

	//@ Data-Member: eventLeftSidePositionButton_
	//  Event Button - "Left Side Position"
	TextButton eventLeftSidePositionButton_;

	//@ Data-Member: eventRightSidePositionButton_
	//  Event Button - "Right Side Position"
	TextButton eventRightSidePositionButton_;

	//@ Data-Member: eventManualStimulationButton_
	//  Event Button - "Manual Stimulation"
	TextButton eventManualStimulationButton_;

	//@ Data-Member: eventPrevScreenButton_
	//  Event Button - "Prev Screen"
	TextButton eventPrevScreenButton_;

	//@ Data-Member: eventNextScreenButton_
	//  Event Button - "Next Screen"
	TextButton eventNextScreenButton_;

	//@ Data-Member: numEventButtonsPressed_
	//  Stores the number of event buttons pressed down.
	Uint32 numEventButtonsPressed_;

	enum
	{
		NUM_EVENT_BUTTONS_ = 18
	};


	//@ Data-Member: eventButtons_
	//  Stores all Event Buttons
	TextButton * eventButtons_[NUM_EVENT_BUTTONS_ + 1];

	//@ Data-Member: eventAcceptButton_
	//  Accepts the events
	TextButton eventAcceptButton_;

	//@ Data-Member: eventClearButton_
	//  Clears the events
	TextButton eventClearButton_;

	//@ Data-Member: eventCommandBox_
	// The box around the accept and clear buttons
	Box eventDialogBox_;

	//@ Data-Member: eventDialogCtr_
	// The container used to group the drawables which make up the
	// event dialog.
	Container eventDialogCtr_;

	//@ Data-Member: eventDialogTitle_
	// The title of the event dialog title.
	TextField  eventDialogTitle_;

};

#endif // EventSubScreen_HH 
