
#ifndef BitArray_HH
#define BitArray_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BitArray - Abstract Array of Bits.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/BitArray.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "FoundationClassId.hh"
#include "TemplateMacros.hh"

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "BitUtilities.hh"
//@ End-Usage


class BitArray
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&        ostr,
				       const BitArray& bitArray);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  public:
    virtual ~BitArray(void);

    inline Uint  getMaxBits(void) const;

    Uint         getNumSet  (void) const;
    inline Uint  getNumClear(void) const;

    inline Boolean  isBitSet  (const Uint bitNum) const;
    inline Boolean  isBitClear(const Uint bitNum) const;

    inline Boolean  areAnySet  (void) const;
    inline Boolean  areAnyClear(void) const;

    inline Boolean  areAllSet  (void) const;
    inline Boolean  areAllClear(void) const;

    inline void  setBit  (const Uint bitNum);
    inline void  clearBit(const Uint bitNum);

    inline void  setAllBits  (void);
    inline void  clearAllBits(void);

    inline void  operator=(const BitArray& bitArray);

    inline Boolean  operator==(const BitArray& bitArray) const;
    inline Boolean  operator!=(const BitArray& bitArray) const;

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);

  protected:
    inline BitArray(Uint* const pInitBits,
			    const Uint  initMaxBits);

    inline Uint  getMaxElems_(void) const;

    Boolean  isEquivTo_(const BitArray& bitArray) const;

    void  copyAllElems_(const Uint* pNewBits);
    void  setAllElems_ (const Uint  newValue);

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0; 
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    BitArray(const BitArray&);	// not implemented...
    BitArray(void);			// not implemented...

    Boolean  areAllEqualTo_(const Uint elemValue) const;

    //@ Data-Member:  pBits_
    //  The array of integers that contains 'MAX_BITS_' bits.
    Uint* const  pBits_;

    //@ Constant:  MAX_BITS_
    //  The maximum number of bits in this bit array.
    const Uint  MAX_BITS_;

    //@ Constant:  ALL_BITS_ON_
    //  An integer with all of the bits set ("on").
    static const Uint  ALL_BITS_ON_;

    //@ Constant:  ALL_BITS_OFF_
    //  An integer with all of the bits cleared ("off").
    static const Uint  ALL_BITS_OFF_;
};


// Inlined methods...
#include "BitArray.in"


#endif  // BitArray_HH
