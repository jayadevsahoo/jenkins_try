#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TouchableText - Touch-sensitive text objects which can display
// an associated message in the Message Area.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is derived from the GUI-Foundation TextField class and inherits
// most of its functionality from this base class.  The additions are local
// storage for a 'message id', a public function setMessage() to set the
// message id, and overridden virtual functions (touch(), leave(), and
// release()) to handle the touch/leave/release events.  The translated text is
// displayed in the Message Area as long as the operator has their finger on
// this text object.
//
// This class is typically used to instantiate the shorthand symbols which are
// used to represent patient data or alarm base messages.
//---------------------------------------------------------------------
//@ Rationale
// This class was created to satisfy the need for text objects which,
// when touched, display an explanatory message on the Message Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// There are two ways to specify the message associated with text.  The
// first is the constructor, and the second is the setMessage() method.
// If the 'msgId_' is NULL_STRING_ID, then there is no associated
// message for this text.  The key action performed by the constructor
// is to mark this object as "sensitive" (i.e., touchable).
//
// Once setup, the touch() and release() methods do the work of
// displaying or clearing the message, respectively, in the lower screen
// Message Area.  Dragging one's finger off of the text has the same
// effect as releasing the text, so the leave() method simply calls the
// release() method.
//---------------------------------------------------------------------
//@ Fault-Handling
// None.
//---------------------------------------------------------------------
//@ Restrictions
// None.  In particular, there is no restriction on changing the message
// associated with this object at any time.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TouchableText.ccv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "TouchableText.hh"

//@ Usage-Classes
#include "MessageArea.hh"
#include "GuiApp.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TouchableText [constructor]
//
//@ Interface-Description
// Constructor.  The 'textId' is the normally displayed text, if any,
// for this text object.  The 'textId' is used by the TextField base
// class.  The 'msgId' is the associated message, if any, to be
// displayed on the Message Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// In addition to initializing the base class and the 'msgId_' member,
// the constructor marks this object as being touch-sensitive by setting
// the inherited 'isSensitive_' flag TRUE.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TouchableText::TouchableText(StringId textId, StringId msgId) :
	TextField(textId),
	msgId_(msgId)
{
    CALL_TRACE("TouchableText::TouchableText(textId, msgId)");

	Drawable::isSensitive_ = TRUE;	// this is now touchable
														// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TouchableText  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// n/a
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

TouchableText::~TouchableText(void)
{
    CALL_TRACE("TouchableText::~TouchableText()");

    // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setMessage [public]
//
//@ Interface-Description
// Updates the message associated with this text object.  Passed 'msgId',
// the id for the new message.
//---------------------------------------------------------------------
//@ Implementation-Description
// Overwrites the private 'msgId_' field.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TouchableText::setMessage(StringId msgId)
{
	CALL_TRACE("TouchableText::setMessage(StringId msgId)");

	msgId_ = msgId;										// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: touch [public, virtual]
//
//@ Interface-Description
// Called when this object is touched by the user.  Displays the message
// associated with this text in the Message Area.  The normal X and Y location
// parameters are unused.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is called when this text object is first touched.  If there is
// an associated message (i.e., 'msgId_' is not NULL_STRING_ID), the
// message is displayed on the lower screen MessageArea as a high
// priority message.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TouchableText::touch(Int, Int)
{
	CALL_TRACE("TouchableText::touch(Int, Int)");

	if (NULL_STRING_ID != msgId_)
	{													// $[TI1]
		GuiApp::PMessageArea->setMessage(msgId_, MessageArea::MA_HIGH);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: leave [public, virtual]
//
//@ Interface-Description
// Called when the user's touching finger leaves the display area of this
// object.  We undisplay our message.
//---------------------------------------------------------------------
//@ Implementation-Description
// This leave action has the same effect as releasing the text object, so just
// call the release() method.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TouchableText::leave(Int, Int)
{
	CALL_TRACE("TouchableText::leave(Int, Int)");

	release();											// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: release  [Destructor]
//
//@ Interface-Description
// Called when the user removes their finger from this text object.  We
// must remove the message associated with this text from the Message Area.
//---------------------------------------------------------------------
//@ Implementation-Description
// If there is an associated message (i.e., 'msgId_' is not NULL_STRING_ID)
// that is displayed in the MessageArea (see the touch() method), remove the
// message from the MessageArea.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TouchableText::release(void)
{
	CALL_TRACE("TouchableText::release(void)");

	if (NULL_STRING_ID != msgId_)
	{													// $[TI1]
		GuiApp::PMessageArea->clearMessage(MessageArea::MA_HIGH);
	}													// $[TI2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TouchableText::SoftFault(const SoftFaultID  softFaultID,
						 const Uint32       lineNumber,
						 const char*        pFileName,
						 const char*        pPredicate)  
{
	CALL_TRACE("TouchableText::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TOUCHABLETEXT,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
