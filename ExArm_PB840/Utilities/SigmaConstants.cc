#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//    Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class:  SigmaConstants - Standard System-Wide Constants.
//---------------------------------------------------------------------
//@ Interface-Description
//  This defines the system-wide constants.
//---------------------------------------------------------------------
//@ Rationale
//  Central place for all of the system-wide constants.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The limit values are initialized based on the values provided by the
//  compiler.  Each compiler ships with a 'limits.h' file where these
//  limits are defined.  This allows for this code to be portable
//  across architectures, despite the differences between the data
//  types of different architectures.
//---------------------------------------------------------------------
//@ Fault-Handling
//  none
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Utilities/vcssrc/SigmaConstants.ccv   25.0.4.0   19 Nov 2013 14:35:14   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "SigmaConstants.hh"
#include <limits.h>

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  External Data Definitions...
//
//=====================================================================

const Uint8   MAX_UINT8_VALUE  = UCHAR_MAX;		//  255u...
const Uint16  MAX_UINT16_VALUE = USHRT_MAX;		//  65535u...
const Uint32  MAX_UINT32_VALUE = (Uint32)UINT_MAX;	//  4294967295u..
const Int8    MIN_INT8_VALUE   = SCHAR_MIN;		// -128...
const Int8    MAX_INT8_VALUE   = SCHAR_MAX;		//  127...
const Int16   MIN_INT16_VALUE  = SHRT_MIN;		// -32768...
const Int16   MAX_INT16_VALUE  = SHRT_MAX;		//  32767...
const Int32   MIN_INT32_VALUE  = INT_MIN;		// -2147483648...
const Int32   MAX_INT32_VALUE  = INT_MAX;		//  2147483647...

#include <float.h>
const Real32  MAX_REAL32_VALUE = FLT_MAX;
