#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendRequestInfo
//---------------------------------------------------------------------
//@ Interface-Description
//  The class encapsulates the information needed to process a trend 
//  data request.  The client constructs a TrendRequestInfo object and
//  fills in the timescale, starting frame sequence number and trend ids
//  for each column.  This represents the desired trend data to be returned
//  from the manager.  The TrendRequestInfo is then copied in the TrendDataSet
//  via the TrendDataSet::setRequestInfo() method before the TrendDataSet
//  is sent to the manager to be processed.
//---------------------------------------------------------------------
//@ Rationale
//  Separates the Trend request data from the data which is returned in
//  the Trend Data Set
//---------------------------------------------------------------------
//@ Implementation-Description
//  The client side of this class has public methods to configure the 
//  request.  The Manager side of the request is via "friend" and has no
//  method interface.  The Manager will add additional information and 
//  modify the existing request information to create a valid request
//  which will be used by the repositories to fulfills the request and 
//  populate the Trend Data Set.
//---------------------------------------------------------------------
//@ Fault-Handling
//  SoftFault is used
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendRequestInfo.ccv   25.0.4.0   19 Nov 2013 14:34:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjf    Date:  13-May-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//
//=====================================================================

#include <string.h>

#include "Trend_Database.hh"
#include "TrendRequestInfo.hh"


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendRequestInfo [constructor]
//
//@ Interface-Description
//  Construct the TrendRequestInfo class
//---------------------------------------------------------------------
//@ Implementation-Description
//  Does no other action
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendRequestInfo::TrendRequestInfo( void )
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendRequestInfo [destructor]
//
//@ Interface-Description
//  Does no action  
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendRequestInfo::~TrendRequestInfo()   
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendRequestInfo [copy constructor]
//
//@ Interface-Description
//  Copy the TrendRequestInfo class
//---------------------------------------------------------------------
//@ Implementation-Description
//  makes a deep copy
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendRequestInfo::TrendRequestInfo( const TrendRequestInfo & source )
{
	memcpy( (void *) this, (void *) &source, sizeof( TrendRequestInfo ) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendRequestInfo [Assignment]
//
//@ Interface-Description
//  Copy the TrendRequestInfo class
//---------------------------------------------------------------------
//@ Implementation-Description
//  makes a deep copy
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
TrendRequestInfo & TrendRequestInfo::operator=( const TrendRequestInfo & source )
{
	if (this != &source)
	{
		memcpy( (void *) this, (void *) &source, sizeof( TrendRequestInfo ) );
	}
	return( *this );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: clear
//
//@ Interface-Description
//  Initializes all data to 0x00
//---------------------------------------------------------------------
//@ Implementation-Description
//  Depends on all of the members having zero as there default initial state
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//
//@ End-Method
//=====================================================================
void TrendRequestInfo::clear( void )
{
	memset( (void *)this, 0x00, sizeof( TrendRequestInfo ) );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendRequestInfo::SoftFault(const SoftFaultID  softFaultID,
								 const Uint32       lineNumber,
								 const char*        pFileName,
								 const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, TREND_DATABASE, TREND_REQUEST_INFO,
							lineNumber, pFileName, pPredicate);
}

