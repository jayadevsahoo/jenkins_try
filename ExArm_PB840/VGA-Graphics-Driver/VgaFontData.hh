
#ifndef VgaFontData_HH
#define VgaFontData_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VgaFontData - Manager of the VGA font tables.
//---------------------------------------------------------------------
//@ Restrictions
// This header file, "VgaFontData.hh" (derived from "Vga_font.h") is also 
// used in the PC version of the font editor.  This tool creates the     
// compilable font files according to format specified in this header 
// file.  It also specifies this file by the name, Vga_font.h, as the 
// include file for the compilable font files.  The file VgaFont.hh is 
// used for the VGA library.  This file can be copied and renamed 
// Vga_font.h for compilation with the font files. The structure 
// definitions in this file must match the structure definitions used 
// by the font editor to produce the compilable font files.  Changes
// to this file must be reflected by the font editor (which uses a 
// different compiler and runs on an intel PC) and this font engine 
// for the Motorola based product.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaFontData.hhv   25.0.4.0   19 Nov 2013 14:37:36   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 007   By: gdc      Date:  03-Jun-2007    SCR Number: 6237 
//  Project: Trend
//  Description:
//	Provided font metrics through FontMetrics struct for improved text 
//  positioning.
//
//  Revision: 006   By: Boris Kuznetsov  Date:  03-May-2003   SCR Number:6036
//  Project: Russian
//  Description:
//	Added macro that allows to use the Alternate labels for Russian language 
//
//  Revision: 005   By: S. Peters    Date:  16-Oct-2002   DCS Number:6092
//  Project: Polish
//  Description:
//	Added an accessor function to get the alternate character code from
//  the alternate character matrix.
//
//  Revision: 004   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//	Implemented single screen compatibility.
//
//  Revision: 003   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 002   By: clw    Date:  06-Mar-1998    DCS Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Added support for Japanese fonts.  Added a Japanese entry to the FontType enum.
//  Added a new enum FontIndex to enumerate all of the fonts currently available.
//  This includes 2 experimental Japanese fonts (pt size 16), more will be added later.
//  ALso changed the name of the FONT_TABLE array to FontTable since it is not constant -
//  it just points to const FontTableEntry objects.
//
//  Revision: 001   By: sah    Date:  01-Aug-1997    DCS Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "VgaGraphicsDriver.hh"

//@ Usage-Classes
//@ End-Usage


#if defined (SIGMA_POLISH) || defined (SIGMA_RUSSIAN)    
	#define VGA_ALTERNATE_FONT 1  /* will allow differnt languages to use the Alternate labels */
#endif

class DisplayContext;

class VgaFontData 
{
public:
	//@ Type:  CharHeader
	// Contains information about individual characters.
	struct CharHeader
	{
		Uint8        asciiRep;					 // ascii representation
		Int8         hotspotX;					 // horizontal distance to left base of char
		Int8         hotspotY;					 // vertical distance to lower base of char
		Int8         width;						 // width of char in pixels (bits)    
		Int8         height;					 // height of char in pixels (bits)   
		Int8         cellIncX;					 // horizontal spacing following character
		Int8         cellIncY;					 // vertical spacing above character
		char         extraByte;
		const char*  pBits;						 // raw bitmap data    
	};

	//@ Type:  FontType
	// Enumerates the different types of fonts that are supported.
	enum FontType
	{
		REGULAR     = 'r',
		BOLD        = 'b',
		ITALIC      = 'i',
		ITALIC_BOLD = 'z',
		JAPANESE    = 'j'
	};


	//@ Data-Member: MAX_JAPANESE_FONTS_PER_PT_SIZE
	// This indicates how many sets of 256-character fonts are allowed for a
	// given point size of Japanese characters.
	static const Uint16 MAX_JAPANESE_FONTS_PER_PT_SIZE;

	//@ Type:  FontIndex
	// Enumerates each different font that is supported. This is used as an
	// index into the FontTable array.
	// NOTE: for Japanese fonts, make sure that MAX_JAPANESE_FONTS_PER_PT_SIZE
	// entries are provided for each supported pt size, regardless of how many font
	// sets are actually used for that size.  These must be contiguous.
	// Also: all Japanese fonts must follow the non-Japanese fonts, after enum
	// FIRST_JAPANESE_FONT_INDEX.  The first Japanese font enum must be set
	// equal to FIRST_JAPANESE_FONT_INDEX.

	enum FontIndex
	{
		DEFAULT_FONT_INDEX,

		THIN_6_INDEX,
		NORMAL_6_INDEX,
		ITALIC_6_INDEX,

		THIN_8_INDEX,
		NORMAL_8_INDEX,
		ITALIC_8_INDEX,

		THIN_10_INDEX,
		NORMAL_10_INDEX,    
		ITALIC_10_INDEX,

		THIN_12_INDEX,
		NORMAL_12_INDEX,
		ITALIC_12_INDEX,

		THIN_14_INDEX,
		NORMAL_14_INDEX,
		ITALIC_14_INDEX,

		THIN_16_INDEX,
		NORMAL_16_INDEX,
		ITALIC_16_INDEX,

		THIN_18_INDEX,
		NORMAL_18_INDEX,
		ITALIC_18_INDEX,

		THIN_24_INDEX,
		NORMAL_24_INDEX,
		ITALIC_24_INDEX,

		THIN_32_INDEX,
		NORMAL_32_INDEX,
		ITALIC_32_INDEX,

		THIN_60_INDEX,
		NORMAL_60_INDEX,
		ITALIC_60_INDEX,


#if defined  VGA_ALTERNATE_FONT
		//SRP
		FIRST_ALTERNATE_FONT_INDEX,

		ALTERNATE_THIN_6_INDEX = FIRST_ALTERNATE_FONT_INDEX,
		ALTERNATE_NORMAL_6_INDEX,
		ALTERNATE_ITALIC_6_INDEX,

		ALTERNATE_THIN_8_INDEX ,
		ALTERNATE_NORMAL_8_INDEX,
		ALTERNATE_ITALIC_8_INDEX,

		ALTERNATE_THIN_10_INDEX,
		ALTERNATE_NORMAL_10_INDEX,
		ALTERNATE_ITALIC_10_INDEX,

		ALTERNATE_THIN_12_INDEX,
		ALTERNATE_NORMAL_12_INDEX,
		ALTERNATE_ITALIC_12_INDEX,

		ALTERNATE_THIN_14_INDEX,
		ALTERNATE_NORMAL_14_INDEX,
		ALTERNATE_ITALIC_14_INDEX,


		ALTERNATE_THIN_16_INDEX,
		ALTERNATE_NORMAL_16_INDEX,
		ALTERNATE_ITALIC_16_INDEX,

		ALTERNATE_THIN_18_INDEX,
		ALTERNATE_NORMAL_18_INDEX,
		ALTERNATE_ITALIC_18_INDEX,

		ALTERNATE_THIN_24_INDEX,
		ALTERNATE_NORMAL_24_INDEX,
		ALTERNATE_ITALIC_24_INDEX,

		// Program note:  Alternate 32 and Alternate 60 point size was not needed for Polish.
		// There character that need to be used are in the 'regular' alph


#endif  // VGA_ALTERNATE_FONT 


		FIRST_JAPANESE_FONT_INDEX,
		JAPANESE_8A_INDEX = FIRST_JAPANESE_FONT_INDEX,
		JAPANESE_8B_INDEX,
		JAPANESE_8C_INDEX,
		JAPANESE_8D_INDEX,

		JAPANESE_10A_INDEX,
		JAPANESE_10B_INDEX,
		JAPANESE_10C_INDEX,
		JAPANESE_10D_INDEX,

		JAPANESE_12A_INDEX,
		JAPANESE_12B_INDEX,
		JAPANESE_12C_INDEX,
		JAPANESE_12D_INDEX,

		JAPANESE_14A_INDEX,
		JAPANESE_14B_INDEX,
		JAPANESE_14C_INDEX,
		JAPANESE_14D_INDEX,

		JAPANESE_16A_INDEX,
		JAPANESE_16B_INDEX,
		JAPANESE_16C_INDEX,
		JAPANESE_16D_INDEX,

		JAPANESE_18A_INDEX,
		JAPANESE_18B_INDEX,
		JAPANESE_18C_INDEX,
		JAPANESE_18D_INDEX,

		JAPANESE_24A_INDEX,
		JAPANESE_24B_INDEX,
		JAPANESE_24C_INDEX,
		JAPANESE_24D_INDEX,

		FONT_COUNT
	};

	//@ Type:  FontMetricsEntry
	// Entry defining a font's extents information
	struct FontMetricsEntry
	{
		Int32 width;								 // typical character width in font
		Int32 height;							 // maximum character height in font
		Int32 ascent;							 // ascent for most alphanumeric characters in font 
		Int32 maxAscent;							 // maximum ascent in the font (above baseline)
		Int32 descent;							 // descent for most alphanumeric characters with descenders
		Int32 maxDescent;						 // maximum descent found in the font
	};

	static void  Initialize(void);

	static void  SoftFault(const SoftFaultID softFaultID,
						   const Uint32      lineNumber,
						   const char*       pFileName  = NULL, 
						   const char*       pPredicate = NULL);

	static class CFont*  FontTable[];
	static VgaFontData::FontMetricsEntry  FontMetrics[];

	// Change 'regular' Font id in an alternate Font id
	static Int16 getAlternateFontId(Int16 fontId);
	 
	static void TextRect(Int xPos, Int yPos, Uint fontId, const wchar_t* pString, struct VgaRect *pRect);


private:
	enum FontInternalType_
	{
		NORMAL_,
		THIN_,
		ITALIC_,
	};
	static void SetFontMetrics(int fontIdx);
	static void ConfigureFont(CFont& font, Int size, FontInternalType_ type);
	//Maintain a CDC to the screen for font real-estate computation.
	static CDC* m_screenCDC;

};


#endif // VgaFontData_HH 
