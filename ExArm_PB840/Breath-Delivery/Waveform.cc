#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Waveform - Waveform data management.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class manages WaveformSet objects to be sent to the Patient-Data
//	subsystem.  The WaveformSet is updated with a new WaveformRecord every
//	waveform cycle.  Once a WaveformSet is completed, 
//	a communication task is notified. The initialization method is 
//	used to reset WaveformSet and other data members.
//	The method synchronize is used to resynch with BreathRecord object
//	on every new breath.
//---------------------------------------------------------------------
//@ Rationale
//	This class manages Waveform data and determines when to
//	start a communication task to deliver the waveform data to the
//	Patient-Data-Management subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This class is implemented by using two WaveformSet objects for
//	double buffering. One WaveformSet is used to collect 
//	data and the other WaveformSet is used to send data to the Patient-
//	Data subsystem. The data member writeIndex_ is an index
//	to WaveformSet used to collect data. The data member sendIndex_ is
//	an index to WaveformSet used to send data to GUI.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//	The communication task has to be completed within 100 msec
//	before a new waveform set is completed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Waveform.ccv   25.0.4.1   20 Nov 2013 17:34:50   rhjimen  $
//
//@ Modification-Log
//
//  Revision: 013  By: rpr    Date:  6-Nov-2012    SCR Number: 6777
//  Project:  BI
//  Description:
//      Removed all monitor software.
// 
//  Revision: 012   By:   gdc    Date: 18-Aug-2009     SCR Number: 6147
//  Project:  XB
//  Description:
//        Moved handling of BdMonitoredData to BdMonitorTask.
//
//  Revision: 011  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 010  By:  sp    Date:  08-Jan-1997    DR Number: DCS 1656
//      Project:  Sigma (R8027)
//      Description:
//             Changed rBdMonitoredData to RBdMonitoredData.
//  
//  Revision: 009  By:  sp    Date:  29-Oct-1996    DR Number: NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection Rework.
//
//  Revision: 008  By:  sp    Date:  17-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 007  By:  sp    Date:  16-Sept-1996    DR Number: DCS 976
//       Project:  Sigma (R8027)
//       Description:
//             Log dropped waveform data directly.
//
//  Revision: 006  By:  hct    Date:  14-May-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Enable TUV interface.
//
//  Revision: 005  By:  sp    Date:  5-Apr-1996    DR Number: DCS 976
//       Project:  Sigma (R8027)
//       Description:
//             Report background error when waveform data is dropped.
//
//  Revision: 004  By:  sp    Date:  5-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Add TUV interface with BdMonitoredData.
//
//  Revision: 003  By:  sp    Date:  4-Mar-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 002  By:  iv    Date:  6-Jan-1996    DR Number: DCS 704, 705
//       Project:  Sigma (R8027)
//       Description:
//             Add const WAVEFORM_MSG to diffrentiate waveform task messages from
//             monitor task messages (704). 
//             Add WaveformTaskLockFlag to implement task locking mechanism (705).
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Waveform.hh"

#include "IpcIds.hh"

//@ Usage-Classes

#include "MsgQueue.hh"

#include "BdMonitorRefs.hh"

#include "Background.hh"

#include "BdQueuesMsg.hh"
//@ End-Usage

//@ Code...

// instantiate and initialize static data members
Boolean Waveform::WaveformTaskLockFlag = FALSE;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Waveform()  
//
//@ Interface-Description
//	Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Waveform::Waveform(void)
{
  CALL_TRACE("Waveform()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Waveform()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Waveform::~Waveform(void)
{
  CALL_TRACE("~Waveform()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//  This method takes a WaveformRecord as an argument and has no
//  return value. This method updates WaveformSet that is currently
//  writable.  The record is added to the set to write.  If the set
//  to write is full, then the communication task must be notified, and
//  the sets must be swapped (the set to write becomes the set to send
//  and the set to send becomes the set to write).
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class maintains two pointers to WaveformSet objects.  One points
//  to the set to write the new data to and one points to the set to
//  send to the Patient-Data subsystem.
//  The WaveformSet that is referenced by the data member writeIndex_ is only
//  updated every WAVEFORM_TIME_CYCLE msec. When the waveformSet is completed
//  and the WaveformTaskLockFlag is not set, the writeIndex_ is switched with 
//  sendIndex_ and a communication task is notified. If waveformSet is completed
//  and the WaveformTaskLockFlag is set, then the waveformSet that is referenced by
//  writeIndex_ is cleared. The data member count_ is incremented every CYCLE_TIME_MS msec
//  and reset to 0 when it reaches WAVEFORM_TIME_CYCLE msec.
//  $[03011] $[03054] $[03057] 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
Waveform::newCycle(const WaveformRecord& waverec)
{
    CALL_TRACE("newCycle(const WaveformRecord& waverec)");

    if (count_ >= WAVEFORM_TIME_CYCLE)
    {
	// $[TI1]
        waveformSet_[writeIndex_].add(waverec);
        if( waveformSet_[writeIndex_].isFull() ) 
        {
	    // $[TI2]
            // do not switch the waveform set unless the waveform task has completed.
            if (!WaveformTaskLockFlag)
            {
	        // $[TI5]
                Int32 tempIndex = writeIndex_;
                writeIndex_ = sendIndex_;
                sendIndex_ = tempIndex;
                waveformSet_[writeIndex_].clearAll();

                // Notify the waveform task of a full waveform set data
	        // available for communication.
	        MsgQueue::PutMsg(::WAVEFORM_TASK_Q, BdQueuesMsg::SEND_WAVEFORM_MSG);
            }
            else
            {
	        // $[TI6]
                waveformSet_[writeIndex_].clearAll();
                Background::LogDiagnosticCodeUtil(::BK_BD_WAVEFORM_DROP);
            }
         }
	 // $[TI3]
         count_ = 0;
    }
    // $[TI4]
    count_ += CYCLE_TIME_MS;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
//  Initialize this object before used.  The set to write and the set
//  to send are initialized to empty, and the indicies to the sets are
//  initialized.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Clear data member waveformSet_. Initialize data member writeIndex_
//  and sendIndex_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
Waveform::initialize(void)
{
    CALL_TRACE("initialize()");

    // $[TI1]
    count_ = 0;
    writeIndex_ = 0;
    sendIndex_ = 1;
    waveformSet_[writeIndex_]. clearAll();
    waveformSet_[sendIndex_]. clearAll();
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Waveform::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, WAVEFORM,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

