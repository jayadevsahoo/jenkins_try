//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUITriggerSignal.h
//----------------------------------------------------------------------------

#ifndef TRIGGERSIGNAL_H
#define TRIGGERSIGNAL_H


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"PatientDataUIUserInt.h"

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

class TriggerSignal:public UserInt
{
    public:
    TriggerSignal(char *pName, unsigned int &uiTime, 
        double dFrequency  =  1.0,
        double dPhase      =  0.0,
        double dMin        = 40.0,
        double dMax        = 70.0,
        double trigger_low = 60.0,
        double trigger_high= 70.0
    );


    void updatePacket(void *address);

    bool isTrigger();

    bool setTriggerSignal(
            double dFrequency,
            double dPhase,
            double dMin,
            double dMax,
            double trigger_high,
            double trigger_low
    );



    bool unsetValue();

    double getFrequency();
    double getPhase();
    double getMax();
    double getMin();
    double getTriggerHigh();
    double getTriggerLow ();

    private:
    bool   m_bIsDesired;
    double m_dMax     ;
    double m_dMin     ;
    double m_dFreqency;
    double m_dPhase   ;
    double m_dRange   ;

    double m_dTime    ;
    double m_Trigger_high; 
    double m_Trigger_low;

    double m_fLast;

    int           m_iValue;
    unsigned int &m_uiTime;
};


#endif


