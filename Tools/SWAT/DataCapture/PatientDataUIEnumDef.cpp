//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PatientDataUIEnumDef.h
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include "PatientDataUIEnumDef.h"

namespace Enum{


static const EnumDef ENUM_BreathType[]={
    {NULL                           ,4 ,0                                      },
    {"CONTROL"                      ,1 ,CONTROL                                },
    {"ASSIST"                       ,2 ,ASSIST                                 },
    {"SPONT"                        ,3 ,SPONT                                  },
    {"NON_MEASURED"                 ,4 ,NON_MEASURED                           },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_PhaseType[]={
    {NULL                           ,11,0                                      },
    {"EXHALATION"                   ,1 ,BreathPhaseType::EXHALATION            },
    {"INSPIRATION"                  ,2 ,BreathPhaseType::INSPIRATION           },
    {"NULL_INSPIRATION"             ,3 ,BreathPhaseType::NULL_INSPIRATION      },
    {"EXPIRATORY_PAUSE"             ,4 ,BreathPhaseType::EXPIRATORY_PAUSE      },
    {"INSPIRATORY_PAUSE"            ,5 ,BreathPhaseType::INSPIRATORY_PAUSE     },
    {"BILEVEL_PAUSE_PHASE"          ,6 ,BreathPhaseType::BILEVEL_PAUSE_PHASE   },
    {"PAV_INSPIRATORY_PAUSE"        ,7 ,BreathPhaseType::PAV_INSPIRATORY_PAUSE },
    {"NIF_PAUSE"                    ,8 ,BreathPhaseType::NIF_PAUSE             },
    {"P100_PAUSE"                   ,9 ,BreathPhaseType::P100_PAUSE            },
    {"VITAL_CAPACITY_INSP"          ,10,BreathPhaseType::VITAL_CAPACITY_INSP   },
    {"NON_BREATHING"                ,11,BreathPhaseType::NON_BREATHING         },
    {NULL                            ,0,0                                      }
};

static const EnumDef ENUM_SupportType[]={
    {NULL                           ,5 ,0                                      },
//    {"OFF_SUPPORT_TYPE"           ,1 ,SupportTypeValue::OFF_SUPPORT_TYPE     },
    {"PSV_SUPPORT_TYPE"             ,1 ,SupportTypeValue::PSV_SUPPORT_TYPE     },
    {"ATC_SUPPORT_TYPE"             ,2 ,SupportTypeValue::ATC_SUPPORT_TYPE     },
    {"VSV_SUPPORT_TYPE"             ,3 ,SupportTypeValue::VSV_SUPPORT_TYPE     },
    {"PAV_SUPPORT_TYPE"             ,4 ,SupportTypeValue::PAV_SUPPORT_TYPE     },
//TODO e600	{"PA_VS_SUPPORT_TYPE"           ,5 ,SupportTypeValue::PA_VS_SUPPORT_TYPE     },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_MandType[]={
    {NULL                           ,3 ,0                                      },
    {"PCV_MAND_TYPE"                ,1 ,MandTypeValue::PCV_MAND_TYPE           },
    {"VCV_MAND_TYPE"                ,2 ,MandTypeValue::VCV_MAND_TYPE           },
    {"VCP_MAND_TYPE"                ,3 ,MandTypeValue::VCP_MAND_TYPE           },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_Bool[]={
    {NULL                           ,2 ,0                                      },
    {"FALSE"                        ,1 ,0                                      },
    {"TRUE"                         ,2 ,1                                      },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_Trigger[]={
    {NULL                           ,50,0                                      },
    {"NULL_TRIGGER_ID"              ,1 ,Trigger::NULL_TRIGGER_ID               },                                      
    {"DELIVERED_FLOW_EXP"           ,2 ,Trigger::DELIVERED_FLOW_EXP            },
    {"HIGH_CIRCUIT_PRESSURE_EXP"    ,3 ,Trigger::HIGH_CIRCUIT_PRESSURE_EXP     },
    {"HIGH_CIRCUIT_PRESSURE_INSP"   ,4 ,Trigger::HIGH_CIRCUIT_PRESSURE_INSP    },
    {"IMMEDIATE_BREATH"             ,5 ,Trigger::IMMEDIATE_BREATH              },
    {"IMMEDIATE_EXP"                ,6 ,Trigger::IMMEDIATE_EXP                 },
    {"NET_FLOW_EXP"                 ,7 ,Trigger::NET_FLOW_EXP                  },
    {"NET_FLOW_INSP"                ,8 ,Trigger::NET_FLOW_INSP                 },
    {"PAUSE_PRESS"                  ,9 ,Trigger::PAUSE_PRESS                   },
    {"PRESSURE_BACKUP_INSP"         ,10,Trigger::PRESSURE_BACKUP_INSP          },
    {"PRESSURE_INSP"                ,11,Trigger::PRESSURE_INSP                 },
    {"PRESSURE_EXP"                 ,12,Trigger::PRESSURE_EXP                  },
    {"SIMV_TIME_INSP"               ,13,Trigger::SIMV_TIME_INSP                },
    {"BL_LOW_TO_HIGH_INSP"          ,14,Trigger::BL_LOW_TO_HIGH_INSP           },
    {"BL_HIGH_TO_LOW_EXP"           ,15,Trigger::BL_HIGH_TO_LOW_EXP            },
    {"PEEP_REDUCTION_EXP"           ,16,Trigger::PEEP_REDUCTION_EXP            },
    {"ASAP_INSP"                    ,17,Trigger::ASAP_INSP                     },
    {"OPERATOR_INSP"                ,18,Trigger::OPERATOR_INSP                 },
    {"TIME_INSP"                    ,19,Trigger::TIME_INSP                     },
    {"PAUSE_TIMEOUT"                ,20,Trigger::PAUSE_TIMEOUT                 },
    {"PAUSE_COMPLETE"               ,21,Trigger::PAUSE_COMPLETE                },
    {"BACKUP_TIME_EXP"              ,22,Trigger::BACKUP_TIME_EXP               },
    {"HIGH_VENT_PRESSURE_EXP"       ,23,Trigger::HIGH_VENT_PRESSURE_EXP        }, 
    {"OSC_TIME_BACKUP_INSP"         ,24,Trigger::OSC_TIME_BACKUP_INSP          },
    {"SVC_TIME"                     ,25,Trigger::SVC_TIME                      },
    {"PRESSURE_SVC"                 ,26,Trigger::PRESSURE_SVC                  },
    {"OSC_TIME_INSP"                ,27,Trigger::OSC_TIME_INSP                 },
    {"SVC_COMPLETE"                 ,28,Trigger::SVC_COMPLETE                  },
    {"PEEP_RECOVERY_MAND_INSP"      ,29,Trigger::PEEP_RECOVERY_MAND_INSP       },
    {"PEEP_RECOVERY"                ,30,Trigger::PEEP_RECOVERY                 },
    {"HIGH_PRESS_COMP_EXP"          ,31,Trigger::HIGH_PRESS_COMP_EXP           },
    {"LUNG_FLOW_EXP"                ,32,Trigger::LUNG_FLOW_EXP                 },
    {"LUNG_VOLUME_EXP"              ,33,Trigger::LUNG_VOLUME_EXP               },
    {"ARM_MANEUVER"                 ,34,Trigger::ARM_MANEUVER                  },
    {"DISARM_MANEUVER"              ,35,Trigger::DISARM_MANEUVER               },
    {"P100_MANEUVER"                ,36,Trigger::P100_MANEUVER                 },
    {"P100_COMPLETE"                ,37,Trigger::P100_COMPLETE                 },
    {"APNEA"                        ,38,Trigger::APNEA                         },
    {"APNEA_AUTORESET"              ,39,Trigger::APNEA_AUTORESET               },
    {"DISCONNECT"                   ,40,Trigger::DISCONNECT                    },
    {"DISCONNECT_AUTORESET"         ,41,Trigger::DISCONNECT_AUTORESET          },
    {"MANUAL_RESET"                 ,42,Trigger::MANUAL_RESET                  },
    {"OCCLUSION"                    ,43,Trigger::OCCLUSION                     },
    {"OCCLUSION_AUTORESET"          ,44,Trigger::OCCLUSION_AUTORESET           },
    {"SAFETY_PCV"                   ,45,Trigger::SAFETY_PCV                    },
    {"SETTING_CHANGE_MODE"          ,46,Trigger::SETTING_CHANGE_MODE           },
    {"STARTUP"                      ,47,Trigger::STARTUP                       },
    {"SVO"                          ,48,Trigger::SVO                           },
    {"SVO_RESET"                    ,49,Trigger::SVO_RESET                     },
    {"VENT_SETUP_COMPLETE"          ,50,Trigger::VENT_SETUP_COMPLETE           },
    {NULL                           ,0 ,0                                      }    
};

static const EnumDef  ENUM_VentType[]={
    {NULL                           ,2 ,0                                      },
    {"INVASIVE_VENT_TYPE"           ,1 ,VentTypeValue::INVASIVE_VENT_TYPE      },
    {"NIV_VENT_TYPE"                ,2 ,VentTypeValue::NIV_VENT_TYPE           },
    {NULL                           ,0 ,0                                      }    
};

static const EnumDef ENUM_ManeuverType[]={

    {NULL                           ,2 ,0                                      },
    {"MANUAL"                       ,1 ,PauseTypes::MANUAL                     },
    {"AUTO"                         ,2 ,PauseTypes::AUTO                       },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_ManeuverState[]={
    {NULL                           ,5 ,0                                      },
    {"IDLE"                         ,1 ,PauseTypes::IDLE                       },
    {"PENDING"                      ,2 ,PauseTypes::PENDING                    },
    {"ACTIVE"                       ,3 ,PauseTypes::ACTIVE                     },
    {"IDLE_PENDING"                 ,4 ,PauseTypes::IDLE_PENDING               },
    {"ACTIVE_PENDING"               ,5 ,PauseTypes::ACTIVE_PENDING             },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_PedState[]={
    {NULL                           ,3 ,0                                      },
    {"PED_IDLE"                     ,1 ,PauseTypes::PED_IDLE                   },
    {"PED_ACTIVE"                   ,2 ,PauseTypes::PED_ACTIVE                 },
    {"PED_COMPLETE"                 ,3 ,PauseTypes::PED_COMPLETE               },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_PpiState[]={
    {NULL                           ,2 ,0                                      },
    {"UNSTABLE"                     ,1 ,PauseTypes::UNSTABLE                   },
    {"STABLE"                       ,2 ,PauseTypes::STABLE                     },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_ComplianceState[]={
    {NULL                           ,8 ,0                                      },
    {"VALID"                        ,1 ,PauseTypes::VALID                      },
    {"OUT_OF_RANGE"                 ,2 ,PauseTypes::OUT_OF_RANGE               },
    {"CORRUPTED_MEASUREMENT"        ,3 ,PauseTypes::CORRUPTED_MEASUREMENT      },
    {"SUB_THRESHOLD_INPUT"          ,4 ,PauseTypes::SUB_THRESHOLD_INPUT        },
    {"UNAVAILABLE"                  ,5 ,PauseTypes::UNAVAILABLE                },
    {"EXH_TOO_SHORT"                ,6 ,PauseTypes::EXH_TOO_SHORT              },
    {"NOT_STABLE"                   ,7 ,PauseTypes::NOT_STABLE                 },
    {"NOT_REQUIRED"                 ,8 ,PauseTypes::NOT_REQUIRED               },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_VtpcvState[]={
    {NULL                           ,2 ,0                                      },
    {"STARTUP_VTPCV"                ,1 ,VtpcvState::STARTUP_VTPCV              },
    {"NORMAL_VTPCV"                 ,2 ,VtpcvState::NORMAL_VTPCV               },
    {NULL                           ,0 ,0                                      }
};

static const EnumDef ENUM_PavState[]={
    {NULL                           ,2 ,                                       },
    {"STARTUP"                      ,1 ,PavState::STARTUP                      },
    {"CLOSED_LOOP"                  ,2 ,PavState::CLOSED_LOOP                  },
    {NULL                           ,0                                         }
};



const EnumDef *e[]={
    ENUM_BreathType,
    ENUM_PhaseType,
    ENUM_SupportType,
    ENUM_MandType,
    ENUM_Bool,
    ENUM_Trigger,
    ENUM_VentType,
    ENUM_ManeuverType,
    ENUM_ManeuverState,
    ENUM_PedState,
    ENUM_PpiState,
    ENUM_ComplianceState,
    ENUM_VtpcvState,
    ENUM_PavState,
    NULL
};


const int ENUM_BreathType_ID          = 0;
const int ENUM_PhaseType_ID           = 1;
const int ENUM_SupportType_ID         = 2;
const int ENUM_MandType_ID            = 3;
const int ENUM_Bool_ID                = 4;
const int ENUM_Trigger_ID             = 5; 
const int ENUM_VentType_ID            = 6;
const int ENUM_ManeuverType_ID        = 7;
const int ENUM_ManeuverState_ID       = 8;
const int ENUM_PedState_ID            = 9;
const int ENUM_PpiState_ID            = 10;
const int ENUM_ComplianceState_ID     = 11;
const int ENUM_VtpcvState_ID          = 12;
const int ENUM_PavState_ID            = 13;


EnumDef *getEnumList(unsigned int in)
{
    if(in < (sizeof(e)/sizeof(EnumDef *)))
    {
        return (EnumDef *)e[in];
    }
    return NULL;
}

EnumDef *getEnum(EnumDef *e,unsigned int in)
{
    if( (in<=e[0].index_value) && (in != 0))
    {
        return &e[in];
    }
    return NULL;
}



int convert(BreathType &x)
{

    switch(ntohl(x))
    {
        case CONTROL:                                  return 1;
        case ASSIST:                                   return 2;
        case SPONT:                                    return 3;
        case NON_MEASURED:                             return 4;
        default: return 0;
    }
}

int convert(BreathPhaseType::PhaseType &x)
{
    switch(ntohl(x))
    {
        case BreathPhaseType::EXHALATION:              return 1;
        case BreathPhaseType::INSPIRATION:             return 2;
        case BreathPhaseType::NULL_INSPIRATION:        return 3;
        case BreathPhaseType::EXPIRATORY_PAUSE:        return 4;
        case BreathPhaseType::INSPIRATORY_PAUSE:       return 5;
        case BreathPhaseType::BILEVEL_PAUSE_PHASE:     return 6;
        case BreathPhaseType::PAV_INSPIRATORY_PAUSE:   return 7;
        case BreathPhaseType::NIF_PAUSE:               return 8;
        case BreathPhaseType::P100_PAUSE:              return 9;
        case BreathPhaseType::VITAL_CAPACITY_INSP:     return 10;
        case BreathPhaseType::NON_BREATHING:           return 11;
        default: return 0;
    };
}

float convert(float x)
{

    union{
        float f;
        unsigned int i;
    } f;
    f.f = x;
    f.i = ntohl(f.i);
    return f.f;
}

int convertPhaseType(float x)
{

    union{
        float f;
        unsigned int i;
    } f;
    f.f = x;
    f.i = ntohl(f.i);

    static char buffer[100];

    switch((int)f.f)
    {
        case BreathPhaseType::EXHALATION:              return 1;
        case BreathPhaseType::INSPIRATION:             return 2;
        case BreathPhaseType::NULL_INSPIRATION:        return 3;
        case BreathPhaseType::EXPIRATORY_PAUSE:        return 4;
        case BreathPhaseType::INSPIRATORY_PAUSE:       return 5;
        case BreathPhaseType::BILEVEL_PAUSE_PHASE:     return 6;
        case BreathPhaseType::PAV_INSPIRATORY_PAUSE:   return 7;
        case BreathPhaseType::NIF_PAUSE:               return 8;
        case BreathPhaseType::P100_PAUSE:              return 9;
        case BreathPhaseType::VITAL_CAPACITY_INSP:     return 10;
        case BreathPhaseType::NON_BREATHING:           return 11;
        default: 
            return 0;
    };
}



int convert(SupportTypeValue::SupportTypeValueId &x)
{
    switch(ntohl(x))
    {
        //case SupportTypeValue::OFF_SUPPORT_TYPE:       return 1;
        case SupportTypeValue::PSV_SUPPORT_TYPE:       return 1;
        case SupportTypeValue::ATC_SUPPORT_TYPE:       return 2;
        case SupportTypeValue::VSV_SUPPORT_TYPE:       return 3;
        case SupportTypeValue::PAV_SUPPORT_TYPE:       return 4;
//TODO e600		case SupportTypeValue::PA_VS_SUPPORT_TYPE:	   return 5;
        default: return 0;
    };
}

int convert(MandTypeValue::MandTypeValueId &x)
{
    switch(ntohl(x))
    {
        case MandTypeValue::PCV_MAND_TYPE:             return 1;
        case MandTypeValue::VCV_MAND_TYPE:             return 2;
        case MandTypeValue::VCP_MAND_TYPE:             return 3;
        default: return 0;
    };
}

int convert(Trigger::TriggerId &x)
{
    switch(ntohl(x))
    {
        case Trigger::NULL_TRIGGER_ID:                 return 1;                                       
        case Trigger::DELIVERED_FLOW_EXP:              return 2;
        case Trigger::HIGH_CIRCUIT_PRESSURE_EXP:       return 3;
        case Trigger::HIGH_CIRCUIT_PRESSURE_INSP:      return 4;
        case Trigger::IMMEDIATE_BREATH:                return 5;
        case Trigger::IMMEDIATE_EXP:                   return 6;
        case Trigger::NET_FLOW_EXP:                    return 7;
        case Trigger::NET_FLOW_INSP:                   return 8;
        case Trigger::PAUSE_PRESS:                     return 9;
        case Trigger::PRESSURE_BACKUP_INSP:            return 10; 
        case Trigger::PRESSURE_INSP:                   return 11; 
        case Trigger::PRESSURE_EXP:                    return 12;
        case Trigger::SIMV_TIME_INSP:                  return 13;
        case Trigger::BL_LOW_TO_HIGH_INSP:             return 14;
        case Trigger::BL_HIGH_TO_LOW_EXP:              return 15;
        case Trigger::PEEP_REDUCTION_EXP:              return 16;
        case Trigger::ASAP_INSP:                       return 17;
        case Trigger::OPERATOR_INSP:                   return 18;
        case Trigger::TIME_INSP:                       return 19;
        case Trigger::PAUSE_TIMEOUT:                   return 20;
        case Trigger::PAUSE_COMPLETE:                  return 21;
        case Trigger::BACKUP_TIME_EXP:                 return 22;
        case Trigger::HIGH_VENT_PRESSURE_EXP:          return 23;
        case Trigger::OSC_TIME_BACKUP_INSP:            return 24;
        case Trigger::SVC_TIME:                        return 25;
        case Trigger::PRESSURE_SVC:                    return 26;
        case Trigger::OSC_TIME_INSP:                   return 27;
        case Trigger::SVC_COMPLETE:                    return 28;
        case Trigger::PEEP_RECOVERY_MAND_INSP:         return 29;
        case Trigger::PEEP_RECOVERY:                   return 30;
        case Trigger::HIGH_PRESS_COMP_EXP:             return 31;
        case Trigger::LUNG_FLOW_EXP:                   return 32;
        case Trigger::LUNG_VOLUME_EXP:                 return 33;
        case Trigger::ARM_MANEUVER:                    return 34;
        case Trigger::DISARM_MANEUVER:                 return 35;
        case Trigger::P100_MANEUVER:                   return 36;
        case Trigger::P100_COMPLETE:                   return 37;
        case Trigger::APNEA:                           return 38;
        case Trigger::APNEA_AUTORESET:                 return 39;
        case Trigger::DISCONNECT:                      return 40;
        case Trigger::DISCONNECT_AUTORESET:            return 41;
        case Trigger::MANUAL_RESET:                    return 42;
        case Trigger::OCCLUSION:                       return 43;
        case Trigger::OCCLUSION_AUTORESET:             return 44;
        case Trigger::SAFETY_PCV:                      return 45;
        case Trigger::SETTING_CHANGE_MODE:             return 46;
        case Trigger::STARTUP:                         return 47;
        case Trigger::SVO:                             return 48;
        case Trigger::SVO_RESET:                       return 49;
        case Trigger::VENT_SETUP_COMPLETE:             return 50;
        default: return 0;
    };
}

int convert(VentTypeValue::VentTypeValueId &x)
{
    switch(ntohl(x))
    {
        case VentTypeValue::INVASIVE_VENT_TYPE:        return 1;
        case VentTypeValue::NIV_VENT_TYPE:             return 2;
        default: return 0;
    };
}

int convert(PauseTypes::ManeuverType &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::MANUAL:                       return 1;
        case PauseTypes::AUTO:                         return 2;
        default: return 0;
    };
}

int convert(PauseTypes::ManeuverState &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::IDLE:                         return 1;
        case PauseTypes::PENDING:                      return 2;
        case PauseTypes::ACTIVE:                       return 3;
        case PauseTypes::IDLE_PENDING:                 return 4;
        case PauseTypes::ACTIVE_PENDING:               return 5;
        default: return 0;
    };
}

int convert(PauseTypes::PedState &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::PED_IDLE:                     return 1;
        case PauseTypes::PED_ACTIVE:                   return 2;
        case PauseTypes::PED_COMPLETE:                 return 3;
        default: return 0;
    };
}

int convert(PauseTypes::PpiState &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::UNSTABLE:                     return 1;
        case PauseTypes::STABLE:                       return 2;
        default: return 0;
    };
}

int convert(PauseTypes::ComplianceState &x)
{
    switch(ntohl(x))
    {
        case PauseTypes::VALID:                        return 1;
        case PauseTypes::OUT_OF_RANGE:                 return 2;
        case PauseTypes::CORRUPTED_MEASUREMENT:        return 3;
        case PauseTypes::SUB_THRESHOLD_INPUT:          return 4;
        case PauseTypes::UNAVAILABLE:                  return 5;
        case PauseTypes::EXH_TOO_SHORT:                return 6;
        case PauseTypes::NOT_STABLE:                   return 7;
        case PauseTypes::NOT_REQUIRED:                 return 8;
        default: return 0;
    };
}

int convert(VtpcvState::Id &x)
{
    switch(ntohl(x))
    {
        case VtpcvState::STARTUP_VTPCV:                return 1;
        case VtpcvState::NORMAL_VTPCV:                 return 2;
        default: return 0;
    };
}


int convert(PavState::Id &x)
{
    switch(ntohl(x))
    {
        case PavState::STARTUP:                        return 1;
        case PavState::CLOSED_LOOP:                    return 2;
        default: return 0;
    };
}


int convert(Boolean &x)
{
    if(x==0)
    {
        return 0;
    }
    return 1;
}




}
