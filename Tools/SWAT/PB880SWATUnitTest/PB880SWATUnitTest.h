
// PB880SWATUnitTest.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CPB880SWATUnitTestApp:
// See PB880SWATUnitTest.cpp for the implementation of this class
//

class CPB880SWATUnitTestApp : public CWinAppEx
{
public:
	CPB880SWATUnitTestApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CPB880SWATUnitTestApp theApp;