﻿using System;
using System.Collections.Generic;
using System.Text;
using NDesk.Options;
using System.IO;

namespace PchAnalysis
{
    class Program
    {
        public static bool Opt_ShowHelp = false;
        public static bool Opt_AutoAddPch = false;
        public static bool Opt_NoWarning = false;
        public static bool Opt_NoInfo = false;

        static void Main(string[] args)
        {
            String sourceDirectory = null;
  
            var p = new OptionSet()
            {
                { "d=", "the directory of the source codes to analyse.",
                    v => sourceDirectory = v },

                { "a|autoadd", "auto add PCH including to file's first line if PCH is not included.",
                    v => Opt_AutoAddPch = v != null },

                { "nw|nowarning", "do not show warning messages.",
                    v => Opt_NoWarning = v != null },

                { "ni|noinfo", "do not show info messages.",
                    v => Opt_NoInfo = v != null },

                { "h|help", "show help message and exit.",
                    v => Opt_ShowHelp = v != null }
            };

            List<String> extra;
            try
            {
                extra = p.Parse(args);
            }
            catch
            {
                showHelp_(p);
                return;
            }

            if (sourceDirectory == null)
            {
                Opt_ShowHelp = true;
            }
            else
            {
                if (!Directory.Exists(sourceDirectory))
                {
                    Console.WriteLine(String.Format("The source directory {0} does not exist.", sourceDirectory));
                    return;
                }
            }

            if (Opt_ShowHelp)
            {
                showHelp_(p);
                return;
            }

            Analyser analyser = new Analyser();

            analyser.execute(sourceDirectory, Opt_AutoAddPch);
        }

        static void showHelp_(OptionSet p)
        {
            Console.WriteLine("This tool is used to analyse the Pre-Compiled Header includings.");            
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }
    }
}
