#ifndef	DciInterface_HH
# define DciInterface_HH

//====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett
// corporation of California claims exclusive right.  No part of this
// work may be used, disclosed, reproduced, sorted in an information
// retrieval system, or transmitted by any means, electronic,
// mechanical, photocopying, recording, or otherwise without the prior
// written permission of Nellcor Puritan Bennett Corporation.
//
//     Copyright (c) 1997, Nellcor Puritan Bennett Corporation
//=====================================================================

//=====================================================================
//@ Class:  DciInterface - GUI Digital Communications Interface Protocol
//                         Handler
//---------------------------------------------------------------------
//@ Version-Information
// @ $Header:   /840/Baseline/GUI-Serial-Interface/vcssrc/DciInterface.hhv   25.0.4.0   19 Nov 2013 14:12:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 003    By: hct           Date: 22-OCT-1999  DR Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//  Revision: 002    By: Gary Cederquist  Date: 24-Nov-1997  DR Number: 2605
//    Project: Sigma (R8027)
//    Description:
//       Complete restructuring of GUI-Serial-Interface to fix
//       several problems.
//
//  Revision  1.X	By: jdm	Date: X/XX/96	DR Number:
//	Project:    Sigma (R8027)
//	Description:
//		Initial version to coding standards (Integration baseline).
//=====================================================================

#include "GUI_Serial_Interface.hh"
#include "SerialInterface.hh"
#include "SettingId.hh"

//@ Usage-Classes
//@ End-Usage


class SerialPort;

class DciInterface : public SerialInterface
{
  public:

	DciInterface( SerialPort & rSerialPort,
            SerialInterface::PortNum portNum );
	~DciInterface();

	// virtual from SerialInterface
	virtual Boolean startPrint(void);

	static void  SoftFault(const SoftFaultID softFaultID,
					       const Uint32	lineNumber,
					       const char *	pFileName = NULL,
					       const char *	pPredicate = NULL);

  protected:
	// virtual from SerialInterface
	virtual void processRequests_(void);
	virtual void setCurrentPrinter_(void);

  private:

};

#endif	// ! DciInterface_HH
