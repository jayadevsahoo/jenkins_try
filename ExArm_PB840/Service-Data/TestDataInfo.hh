
#ifndef TestDataInfo_HH
#define TestDataInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  TestDataInfo - Service Data Item processing information for 
//                        data type of service test result.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Service-Data/vcssrc/TestDataInfo.hhv   10.7   08/17/07 10:35:00   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "ServiceDataClassIds.hh"
#include "ServiceDataMgr.hh"
#include "TestDataId.hh"
#include "TestInfo.hh"
#include "TestDatum.hh"


class TestDataInfo : public TestInfo
{
	//@ Friend: ServiceDataMgr.
	// Allows ServiceDataMgr class to call the 'modifyData_' private method
	// when it receives new data from the network.
	friend class ServiceDataMgr;

public:
    TestDataInfo(TestDataId::TestDataItemId id);
    ~TestDataInfo(void);
    
    static void  SoftFault(const SoftFaultID softFaultID, 
				  const Uint32      lineNumber, 
				  const char*       pFileName = NULL, 
				  const char*       pBoolTest = NULL);
				  
protected:
    virtual void modifyData_(void *pValue);

private:
    TestDataInfo(void); 						   	// not implemented ...
    TestDataInfo(const TestDataInfo& range);		// not implemented ...
    void  operator=(const TestDataInfo&);			// not implemented...

};


#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

#endif  // TestDataInfo_HH
