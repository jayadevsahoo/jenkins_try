#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: AlarmResponses - Alarm matrix system and reset system responses.
//---------------------------------------------------------------------
//@ Interface-Description
// All of the system and reset system responses are static methods.  They are
// pointed to and invoked by objects derived from AlarmAnalysisInformation.
//---------------------------------------------------------------------
//@ Rationale
// The Technical and Non-Technical alarm matrices contain columns System
// Response and Reset System Response which indicate processing which must be
// done when an AlarmAugmentation changes state.  (System Response on
// transition to active and Reset Response on transition to inactive).  The
// processing is collected in methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
// Each method in AlarmResponses specifies processing defined in the alarm
// matrices by calling methods in the necessary classes.  All of the public
// methods will be invoked on the BD_CPU only and the private methods, where
// necessary, will act as a server to move the necessary information across
// the Ethernet link.
//---------------------------------------------------------------------
//@ Fault-Handling
//      The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmResponses.ccv   25.0.4.0   19 Nov 2013 13:51:18   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "AlarmResponses.hh"

//@ Usage-Classes
#include "Alarm.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VILedOnPatDataOff
//                                      
//@ Interface-Description
//  Initiate processing to turn on the GUI_CPU Vent Inoperative Status LED.
//  Initiate processing to turn off displays of Patient Data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::VILedOnPatDataOff(void)
{
  CALL_TRACE("VILedOnPatDataOff(void)");

  AlarmResponses::VentInopLedOn_();
  AlarmResponses::PatientDataDisplayOff_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: VILedOffPatDataOn
//                                      
//@ Interface-Description
//  Initiate processing to turn off the GUI_CPU Vent Inoperative Status LED.
//  Initiate processing to turn on displays of Patient Data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::VILedOffPatDataOn(void)
{
  CALL_TRACE("VILedOffPatDataOn(void)");

  AlarmResponses::VentInopLedOff_();
  AlarmResponses::PatientDataDisplayOn_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdAlarmOnGuiSetOff
//                                      
//@ Interface-Description
//  Initiate processing to turn on the BD_CPU audible alarm.
//  Initiate processing to lock out GUI settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::BdAlarmOnGuiSetOff(void)
{
  CALL_TRACE("BdAlarmOnGuiSetOff(void)");

  AlarmResponses::BdAudibleAlarmOn_();
  AlarmResponses::GuiSettingChangesOff_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BdAlarmOffGuiSetOn
//                                      
//@ Interface-Description
//  Initiate processing to turn off the BD_CPU audible alarm.
//  Initiate processing to allow access to GUI settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::BdAlarmOffGuiSetOn(void)
{
  CALL_TRACE("BdAlarmOffGuiSetOn(void)");

  AlarmResponses::BdAudibleAlarmOff_();
  AlarmResponses::GuiSettingChangesOn_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LOILedOnBdAlarmOn
//                                      
//@ Interface-Description
//  Initiate processing to turn on the BD_CPU Loss of Interface LED.
//  Initiate processing to turn on the BD_CPU audible alarm.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::LOILedOnBdAlarmOn(void)
{
  CALL_TRACE("LOILedOnBdAlarmOn(void)");

  AlarmResponses::LossOfInterfaceLedOn_();
  AlarmResponses::BdAudibleAlarmOn_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LOILedOffBdAlarmOff
//                                      
//@ Interface-Description
//  Initiate processing to turn off the BD_CPU Loss of Interface LED.
//  Initiate processing to turn off the BD_CPU audible alarm.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::LOILedOffBdAlarmOff(void)
{
  CALL_TRACE("LOILedOffBdAlarmOff(void)");

  AlarmResponses::LossOfInterfaceLedOff_();
  AlarmResponses::BdAudibleAlarmOff_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiSetOffPatDataOff
//                                      
//@ Interface-Description
//  Initiate processing to lock out GUI settings.
//  Initiate processing to turn off displays of Patient Data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::GuiSetOffPatDataOff(void)
{
  CALL_TRACE("GuiSetOffPatDataOff(void)");

  AlarmResponses::GuiSettingChangesOff_();
  AlarmResponses::PatientDataDisplayOff_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiSetOnPatDataOn
//                                      
//@ Interface-Description
//  Initiate processing to allow access to GUI settings.
//  Initiate processing to turn on displays of Patient Data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::GuiSetOnPatDataOn(void)
{
  CALL_TRACE("GuiSetOnPatDataOn(void)");

  AlarmResponses::GuiSettingChangesOn_();
  AlarmResponses::PatientDataDisplayOn_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LOILedOnBdAlarmOnGuiSetOffPatDataOff
//                                      
//@ Interface-Description
//  Initiate processing to turn on the BD_CPU Loss of Interface LED.
//  Initiate processing to turn on the BD_CPU audible alarm.
//  Initiate processing to lock out GUI settings.
//  Initiate processing to turn off displays of Patient Data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::LOILedOnBdAlarmOnGuiSetOffPatDataOff(void)
{
  CALL_TRACE("LOILedOnBdAlarmOnGuiSetOffPatDataOff(void)");

  AlarmResponses::LossOfInterfaceLedOn_();
  AlarmResponses::BdAudibleAlarmOn_();
  AlarmResponses::GuiSettingChangesOff_();
  AlarmResponses::PatientDataDisplayOff_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LOILedOffBdAlarmOffGuiSetOnPatDataOn
//                                      
//@ Interface-Description
//  Initiate processing to turn off the BD_CPU Loss of Interface LED.
//  Initiate processing to turn off the BD_CPU audible alarm.
//  Initiate processing to allow access to GUI settings.
//  Initiate processing to turn on displays of Patient Data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::LOILedOffBdAlarmOffGuiSetOnPatDataOn(void)
{
  CALL_TRACE("LOILedOffBdAlarmOffGuiSetOnPatDataOn(void)");

  AlarmResponses::LossOfInterfaceLedOff_();
  AlarmResponses::BdAudibleAlarmOff_();
  AlarmResponses::GuiSettingChangesOn_();
  AlarmResponses::PatientDataDisplayOn_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LOILedOnBdAlarmOnGuiSetOff
//                                      
//@ Interface-Description
//  Initiate processing to turn on the BD_CPU Loss of Interface LED.
//  Initiate processing to turn on the BD_CPU audible alarm.
//  Initiate processing to lock out GUI settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::LOILedOnBdAlarmOnGuiSetOff(void)
{
  CALL_TRACE("LOILedOnBdAlarmOnGuiSetOff(void)");

  AlarmResponses::LossOfInterfaceLedOn_();
  AlarmResponses::BdAudibleAlarmOn_();
  AlarmResponses::GuiSettingChangesOff_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LOILedOffBdAlarmOffGuiSetOn
//                                      
//@ Interface-Description
//  Initiate processing to turn off the BD_CPU Loss of Interface LED.
//  Initiate processing to turn off the BD_CPU audible alarm.
//  Initiate processing to allow access to GUI settings.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::LOILedOffBdAlarmOffGuiSetOn(void)
{
  CALL_TRACE("LOILedOffBdAlarmOffGuiSetOn(void)");

  AlarmResponses::LossOfInterfaceLedOff_();
  AlarmResponses::BdAudibleAlarmOff_();
  AlarmResponses::GuiSettingChangesOn_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiSetOff
//                                      
//@ Interface-Description
//  Initiate processing to disallow settings changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::GuiSetOff(void)
{
  CALL_TRACE("GuiSetOff(void)");

  AlarmResponses::GuiSettingChangesOff_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiSetOn
//                                      
//@ Interface-Description
//  Initiate processing to allow settings changes.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::GuiSetOn(void)
{
  CALL_TRACE("GuiSetOn(void)");

  AlarmResponses::GuiSettingChangesOn_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PatDataOff
//                                      
//@ Interface-Description
//  Initiate processing to turn off displays of Patient Data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::PatDataOff(void)
{
  CALL_TRACE("PatDataOff(void)");

  AlarmResponses::PatientDataDisplayOff_();
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PatDataOn
//                                      
//@ Interface-Description
//  Initiate processing to turn on displays of Patient Data.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::PatDataOn(void)
{
  CALL_TRACE("PatDataOn(void)");

  AlarmResponses::PatientDataDisplayOn_();
  // $[TI1]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
AlarmResponses::SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName,
						  const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::ALARMRESPONSES, 
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

