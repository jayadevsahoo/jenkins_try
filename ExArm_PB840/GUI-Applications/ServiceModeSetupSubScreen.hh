#ifndef ServiceModeSetupSubScreen_HH
#define ServiceModeSetupSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceModeSetupSubScreen - The sub-screen in the Lower screen
// selected by pressing the OtherScreen tab button followed by the
// ServiceMode Setup button.  It displays the four settings buttons
// to allow the operator to adjust service settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceModeSetupSubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By:  sah   Date:  17-Jul-1997    DCS Number:  2270
//  Project:  Sigma (R8027)
//  Description:
//      Provide means for 'ScreenContrastHandler::ProcessContrastChange()'
//      to determine whether to retrieve the contrast delta value from the
//      ADJUSTED context (because this screen is currently active), or from
//      the ACCEPTED context.
//
//  Revision: 001  By:  hhd Date:  13-MAY-95    DR Number:
//  Project:  Sigma (R8027)
//  Description:
//      Integration baseline.
//
//====================================================================

#include "BatchSettingsSubScreen.hh"

//@ Usage-Classes
#include "DiscreteSettingButton.hh"
#include "SequentialSettingButton.hh"
#include "SubScreenTitleArea.hh"
//@ End-Usage

class ServiceModeSetupSubScreen : public BatchSettingsSubScreen
{
public:
	ServiceModeSetupSubScreen(SubScreenArea *pSubScreenArea);
	~ServiceModeSetupSubScreen(void);
	
	void acceptHappened(void);

	void buttonDownHappened(Button *, Boolean);
	void buttonUpHappened(Button *, Boolean);

	void timerEventHappened(GuiTimerId::GuiTimerIdType);

	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);
protected:
	// BatchSettingsSubScreen virtual method...
	virtual void  activateHappened_  (void);
	virtual void  deactivateHappened_(void);
	virtual void  valueUpdateHappened_(
					  const BatchSettingsSubScreen::TransitionId_ transitionId,
					  const Notification::ChangeQualifier         qualifierId,
					  const ContextSubject*                       pSubject
									  );

private:
	// these methods are purposely declared, but not implemented...
	ServiceModeSetupSubScreen(void);						// not implemented...
	ServiceModeSetupSubScreen(const ServiceModeSetupSubScreen&);// not implemented...
	void   operator=(const ServiceModeSetupSubScreen&);		// not implemented...

	// Constant: MAX_SETTING_BUTTONS_ 
	// Maximum number of buttons that can be displayed in this subscreen.
	// Must be modified when new buttons are added.
	enum { MAX_SETTING_BUTTONS_ = 5 };

	//@ Data-Member: nlVoltageButton_;
	// A text button which activates Nominal Line Voltage Setting Subscreen
	DiscreteSettingButton nlVoltageButton_;

	//@ Data-Member: stBaudRateButton_;
	// A text button which activates the System Test Baud Rate Setting Subscreen 
	DiscreteSettingButton stBaudRateButton_;

	//@ Data-Member: contrastDeltaButton_;
	// A text button which activates the Contrast Delta Setting Subscreen. 
	SequentialSettingButton contrastDeltaButton_;
	
	//@ Data-Member: pressureUnitButton_;
	// A text button which activates the Pressure Units Setting Subscreen
	DiscreteSettingButton pressureUnitButton_;
	
	DiscreteSettingButton languageButton_;
	//@ Data-Member: titleArea_
	// The subscreen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member:  arrSettingButtonPtrs_
	// The subscreen buttons.
	SettingButton*  arrSettingButtonPtrs_[MAX_SETTING_BUTTONS_ + 1];
};


#endif // ServiceModeSetupSubScreen_HH 
