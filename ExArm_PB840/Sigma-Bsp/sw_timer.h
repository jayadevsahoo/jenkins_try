/***************************************************************************
*
*		Copyright (c) 1992 READY SYSTEMS CORPORATION.
*
*	All rights reserved. READY SYSTEMS' source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	READY SYSTEMS. Any attempt or participation in deciphering, decoding,
*	reverse engineering or in any way altering the source code is
*	strictly prohibited, unless the prior written consent of
*	READY SYSTEMS is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/

/*****************************************************************************
 *                                                                           
 * File:                                                                     
 *                                                                           
 *    sw_timer.h                                                             
 *                                                                           
 * Author:                                                                   
 *                                                                           
 *    Donn Rochette                                                          
 *                                                                           
 * Date:                                                                     
 *                                                                           
 *    8-7-92                                                                 
 *                                                                           
 * Contents:                                                                 
 *                                                                           
 * Revision History:                                                         
 *                                                                           
 *    Name |   Date   | Description of modification                          
 *    -----+----------+----------------------------------------------------- 
 *    DAR  | 08-07-92 | First version (adapted/Frankenstiened from 332)
 *                                                                           
 *****************************************************************************/

#ifndef _SW_TIMER_H_
#define _SW_TIMER_H_

#include "timer.h"

/* Interrupt priority values */

#define SW_TIMER_IRQ_0   0
#define SW_TIMER_IRQ_1   1 
#define SW_TIMER_IRQ_2   2 
#define SW_TIMER_IRQ_3   3 
#define SW_TIMER_IRQ_4   4 
#define SW_TIMER_IRQ_5   5 
#define SW_TIMER_IRQ_6   6 
#define SW_TIMER_IRQ_7   7 

/* Timer periodic constants */

#define SW_TIMER_10MS    0x52
#define SW_TIMER_500MS   0x0108
#define SW_TIMER_1SEC    0x0110
#define SW_TIMER_MAX     0x01FF

/* register addresses for the timer */

typedef struct {
   unsigned short *pitr;
   unsigned short *picr;
} sw_timer_reg_map_t;

/* register addresses for the timer */

typedef struct {
   unsigned char interrupt_priority_level; /* ipl bits */
   unsigned long read_timer;
} sw_timer_specifics_t;

/* timer device descriptor */

typedef struct {
   sw_timer_reg_map_t regs;
   timer_config_t tcfg;
   sw_timer_specifics_t pit_cfg;
} sw_timer_config_t;

/* function prototypes */

vbsp_return_t sw_timer_install(_ANSIPROT2(       /* cfg, desc */
                sw_timer_config_t *,
		timer_dev_desc_t *));

vbsp_return_t sw_timer_init(_ANSIPROT1(          /* desc */
                struct timer_dev_desc_t *));
 
vbsp_return_t sw_timer_enable(_ANSIPROT1(        /* desc */
                timer_dev_desc_t *));
 
vbsp_return_t sw_timer_disable(_ANSIPROT1(       /* desc */
                timer_dev_desc_t *));
 
vbsp_return_t sw_timer_status(_ANSIPROT2(        /* desc, status */
                timer_dev_desc_t *,
		int *));

vbsp_return_t sw_timer_set_count(_ANSIPROT2(     /* desc, count */
                timer_dev_desc_t *,
		unsigned long *));

vbsp_return_t sw_timer_get_count(_ANSIPROT2(     /* desc, count */
                timer_dev_desc_t *,
		unsigned long *));

vbsp_return_t sw_timer_set_preload(_ANSIPROT2(   /* desc, preload */
                timer_dev_desc_t *,
		unsigned long *));

vbsp_return_t sw_timer_get_preload(_ANSIPROT2(   /* desc, preload */
                timer_dev_desc_t *,
		unsigned long *));

vbsp_return_t sw_timer_get_rollover(_ANSIPROT2(  /* desc, count */
                timer_dev_desc_t *,
		unsigned long *));

vbsp_return_t sw_timer_read_timer(_ANSIPROT2(    /* desc, count */
                timer_dev_desc_t *,
		unsigned long *));

int sw_timer_test(_ANSIPROT1(          /* entry */
                logio_interrupt_entry_t *));

#endif 
