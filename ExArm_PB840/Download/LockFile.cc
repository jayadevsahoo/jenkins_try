#include "stdafx.h"
#if	defined(SIGMA_SERVER)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Nellcor/Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LockFile - Provides capability to create a lock file while
//  running a download server.
//  To support multiple tftpServer execution but still protect
//  individual user's download environment, the LockFile
//  provides protection mechanism that enables locking an exclusive
//  right to download to a specific target.
//---------------------------------------------------------------------
//@ Interface-Description
//		A LockFile object can be used to create a lock on a specific
//      target or on Master mode download server.
//      Only one lock on a specific target machine is allowed.
//      When a lock is locked as a Master mode, no other locks
//      can be created.
//      It also cleans up lock files when they are no longer needed.
//---------------------------------------------------------------------
//@ Rationale
//  The LockFile class was written to hide the details of utilizing
//  a lock file to synchronize the use of tftpServer.
//---------------------------------------------------------------------
//@ Implementation-Description
//	   LockFile class creates a file to indicate that there is
//     a lock on a tftpServer that runs on a certain mode.
//     A lock file is named  'targetName'.lock file and it is created
//     on a central Lock file directory. 
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions  and pre-conditions are used to
//  ensure correctness of software conditons.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/LockFile.ccv   25.0.4.0   19 Nov 2013 14:02:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  25-Jan-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//=====================================================================

#include "LockFile.hh"
#include "DownloadTypes.hh"
#include "Download.hh"
#include "TargetAddr.hh"
#include "sys/types.h"
#include "sys/stat.h"

extern TargetAddr TargetAddrTbl[];
#if	defined(SIGMA_DEBUG)
char *LockFile::PMasterLockFile_ = "/840/tlocks/debugTftpServer.lock";
#else
char *LockFile::PMasterLockFile_ = "/840/tlocks/tftpServer.lock";
#endif //defined(SIGMA_DEBUG)
const char *LockFile::PLockDir_ = "/840/tlocks/";

//@ Code...

//=====================================================================
//@ Method: LockFile()  :: Default Constructor
//
//@ Interface-Description
//    Initialize member variables to initial values.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
LockFile::LockFile(void) 
{
	pHandle_ = NULL;
	fileName_[0] = '\0';
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LockFile 
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
LockFile::~LockFile()
{
    
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setLock(char *pTarget)
//
//@ Interface-Description
//	  Sets a lock on a given Target. If a lock already exists on
//    a given target name or a Master version of lock is already
//    exist, the method fails and returns -1.
//    If a given pTarget is a NULL pointer, the method sets a master
//    lock if no other locks exists. When it fails to set a lock,
//    it returns a negative return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	   A unique lock file name is created for each target, a target
//     name file followed by "tftp.lock". The Master version of lock
//     file name is maintained the static 'PMasterLockFile_' member
//	   variable.
//     When the stat function returns a zero value, it indicates
//     that file already exists, then the method determines that
//     that lock is already used.
//---------------------------------------------------------------------
//@ PreCondition
//    CLASS_PRE_CONDITION( msg );
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int
LockFile::setLock(char *targetName)
{
struct stat statBuf;

	if ( stat(PMasterLockFile_, &statBuf) == 0 )
	{
		Download::ReportError("Master tftpServer is already running"); 
		return(-1);
	}

	if ( targetName[0] == '\0' )
	{
		if ( anyTargetLocked() )
		{
			Download::ReportError("Failed to run Master tftpServer, another tftpServer is active"); 
			return(-1);
		}
		strcpy(fileName_, PMasterLockFile_);
	}
	else
	{
#if	defined(SIGMA_DEBUG)
		sprintf(fileName_, "%s%s.debugtftp.lock", PLockDir_,targetName ); 
#else
		sprintf(fileName_, "%s%s.tftp.lock", PLockDir_,targetName ); 
#endif //defined(SIGMA_DEBUG)
		if ( stat(fileName_, &statBuf) == 0 )
		{
			Download::ReportError("tftpServer is already running on target %s", targetName ); 
			fileName_[0] = '\0';
			return(-1);
		}
	}
	if (!( pHandle_ =  fopen( fileName_, "w" )))
	{
		Download::ReportError("Cannot create a lock file %s.\n", fileName_); 
		fileName_[0] = '\0';
		return(-1);
	}
	return(0);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: unLock()
//
//@ Interface-Description
//    Unlock the lockfile, if the Lockfile is already unlocked, 
//    do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	  In order unlock a LockFile object, the file opened for the LockFile
//    object should be closed and the file should be unlinked so the system
//    deletes the file. 
//---------------------------------------------------------------------
//@ PreCondition
//    
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
void
LockFile::unLock(void)
{
	if (pHandle_)
	{
		fclose(pHandle_);
		pHandle_ = NULL;
	}
	if ( fileName_[0] )
	{
		unlink(fileName_);
	    fileName_[0] = 0;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: anyTargetLocked()
//
//@ Interface-Description
//		The anyTargetLocked() method returns true if a lock file for
//      any of the targets in the Target table exists.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The method searches through the Target table and checks to
//      see if a lock file for each target exists using the stat
//      system call.
//---------------------------------------------------------------------
//@ PreCondition
//    
//---------------------------------------------------------------------
//@ PostCondition
//              none
//@ End-Method
//=====================================================================
Boolean
LockFile::anyTargetLocked(void)
{
char fileName[100];
struct stat statBuf;
TargetAddr *pTbl;
Int i = 0;

	for(i=0;  TargetAddrTbl[i].name; i++)
	{
		pTbl = &TargetAddrTbl[i];
#if	defined(SIGMA_DEBUG)
		sprintf(fileName_, "%s%s.debugtftp.lock", PLockDir_, pTbl->name);
#else
		sprintf(fileName_, "%s%s.tftp.lock", PLockDir_, pTbl->name);
#endif //defined(SIGMA_TARGET)
		if ( stat(fileName_, &statBuf) == 0 )
		{
			return(TRUE);
		}
	}
	return(FALSE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 
void
LockFile::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
           const char*        pFileName,
           const char*        pPredicate)
{
      DownloadFault::SoftFault(softFaultID, DOWNLOAD,
      LOCKFILE_CLASS, lineNumber, pFileName, pPredicate);
}

#endif	//defined(SIGMA_SERVER)
