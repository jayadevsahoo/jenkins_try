#include "stdafx.h"

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Heap - Abstract Heap of Memory Blocks.
//---------------------------------------------------------------------
//@ Interface-Description
//  Provides a means of dynamically managing 'getMaxBlocks()' memory
//  blocks.  These blocks may be reused many times -- given they're
//  properly deallocated.  These blocks are long word-aligned, and are
//  'getBlockSize()' bytes big.  This is an abstract base class that
//  is to be accessed through 'FixedHeap<TYPE,SIZE>'.  Initialization
//  and deinitialization of a heap's allocated memory blocks is left
//  to the client -- the memory blocks are raw, uninitialized memory
//  blocks.
//
//      An overloaded 'new' operator is provided that takes a heap as an
//      argument, and runs the specified constructor on the newly allocated
//      memory.
//---------------------------------------------------------------------
//@ Rationale
//  This will be used for data blocks (e.g., classes) that may
//  have a short life-span, and/or whose memory location may be
//  reused by other data blocks, of the same type, at a later time.
//---------------------------------------------------------------------
//@ Implementation-Description
//  An internal array of 'Node' items is provided by this class's
//  derived class ('FixedHeap<TYPE,SIZE>'), for the memory blocks.
//  The array is big enough to hold 'getMaxBlocks()' blocks, with
//  each block containing 'getBlockSize()' bytes.  The non-allocated
//  blocks are kept in a "free" list, which is linked together using
//  the 'Node' objects.  Each memory block must be at least
//  'sizeof(Node)' bytes big, to allow the use of 'Node' in the
//  blocks.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//  ((getNumAvailable() + getNumAllocated()) == getMaxBlocks())
//  (getBlockSize() >= sizeof(Node))
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/Heap.C_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 15-Jun-1999   DR Number: 5405
//   Project:  ATC
//   Description:
//	Enhanced information from failed assertion by added auxillary
//      error codes.
//
//   Revision: 002   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "Heap.hh"

#if defined(SIGMA_DEBUG)
#  include "Foundation.hh"
#  include "Ostream.hh"
#endif  // defined(SIGMA_DEBUG)

//@ Usage-Classes...
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  ~Heap()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this empty heap.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The memory blocks have already been deinitialized by this class's
//  derived class by the time this destructor is run.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Heap::~Heap(void)
{
  CALL_TRACE("~Heap()");
}  // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  isLegalBlock(pRawMemory)  [const, virtual]
//
//@ Interface-Description
//  Is 'pRawMemory' a legal pointer to a block within this heap?  To
//  be a "legal" pointer, 'pRawMemory' has to have an address within
//  this heap, it has to reside on a block boundary, and the block it
//  points to has to be currently allocated.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Boolean
Heap::isLegalBlock(const void* pRawMemory) const
{
  CALL_TRACE("isLegalBlock(pRawMemory)");

  Boolean  isLegal = FALSE;

  if (pRawMemory != NULL)
  {   // $[TI1]
    // 'pRawMemory' is not 'NULL', therefore determine if it is a pointer
    // to an allocated block of memory from this heap...
    const Uint32 TOTAL_NODES = (MAX_BLOCKS_ * NODES_PER_BLOCK_);

    const Node*  pNode      = (const Node*)pRawMemory;
    const Node*  pFirstNode = getFirstNode_();

    // verify that 'pNode' points to a location within this heap...
    if (pNode >= pFirstNode  &&  pNode < (pFirstNode + TOTAL_NODES))
    {   // $[TI1.1]
      // 'pNode' DOES point to a memory location that is within this heap,
      // therefore, verify that 'pNode' is at a block boundary within this
      // heap by determining if the offset (in bytes) between 'pNode' and the
      // beginning of this heap is an even multiple of the block size...
      const Uint32 OFFSET = (Uint32)((const char*)pNode -
				     (const char*)pFirstNode);

      if ((OFFSET % getBlockSize()) == 0u)
      {    // $[TI1.1.1]
	// 'pNode' DOES point to a block within this heap, therefore verify
	// that 'pNode' points to an allocated block by making sure that it
	// is not currently on the list of free blocks...

	Node* pFreeNode;
	for (pFreeNode = pFreeList_;
	     pFreeNode != NULL  &&  pNode != pFreeNode;
	     pFreeNode = pFreeNode->pNext)
	{   // $[TI1.1.1.1] -- at least one iteration...
	  // do nothing...
	}   // $[TI1.1.1.2] -- no iterations...

	// if 'pFreeNode' is 'NULL' then 'pNode' is not in the list of
	// free blocks, thereby showing that 'pNode' is a legal,
	// allocated block of this heap...$[TI1.1.1.3] (T) $[TI1.1.1.4] (F)
	isLegal = (pFreeNode == NULL);
      }   // $[TI1.1.2] -- NOT at a block boundardy...
    }   // $[TI1.2] -- 'pNode' points to memory "LESS THAN" this heap, OR
	//             'pNode' points to memory "GREATER THAN" this heap...
  }   // $[TI2] -- 'pRawMemory' is equal to 'NULL'...

  return(isLegal);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getNumAvailable()  [const]
//
//@ Interface-Description
//  Return the number of blocks currently available for allocation
//  from this heap.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This iterates through the linked list of free nodes, counting each
//  node along the way to determine the number of available nodes.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

Uint32
Heap::getNumAvailable(void) const
{
  CALL_TRACE("getNumAvailable()");

  Uint  numAvail = 0u;

  if (!isFull())
  {   // $[TI1]
    // this heap is NOT full, therefore the number of available blocks
    // is greater than '0' and is determined by starting with the first
    // free node and counting until the end...
    Node*  pFreeNode = pFreeList_;

    do
    {
      // while 'pFreeNode' is not 'NULL', increment 'numAvail'...
      numAvail++;
      pFreeNode = pFreeNode->pNext;
    } while (pFreeNode != NULL);
  }   // $[TI2] -- the heap is currently full...

  return(numAvail);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  allocate()
//
//@ Interface-Description
//  Return a pointer to an uninitialized block of memory from this
//  heap, that is 'getBlockSize()' bytes big.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The first node from the free list is returned.
//---------------------------------------------------------------------
//@ PreConditions
//  (!isFull())
//---------------------------------------------------------------------
//@ PostConditions
//  (isLegalBlock(allocate()))
//@ End-Method
//=====================================================================

void*
Heap::allocate(void)
{
  CALL_TRACE("allocate()");
  AUX_CLASS_PRE_CONDITION((!isFull()),
			  ((MAX_BLOCKS_ << 16) | NODES_PER_BLOCK_));

  // store the first node in this free list for allocation...
  Node*  pAllocedNode = pFreeList_;

  // store the next -- which may be 'NULL' -- as the new first node...
  pFreeList_ = pFreeList_->pNext;

#if defined(SIGMA_DEBUG)
  if (Foundation::IsDebugOn(::ABSTRACT_HEAP)  ||
      Foundation::IsDebugOn(::FIXED_HEAP))
  {
    cout << "Allocating a Block:\n"
	 << "  Block Memory:  " << (void*)pAllocedNode << '\n' << endl;
  }
#endif  // defined(SIGMA_DEBUG)

  return(pAllocedNode);
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  deallocate(pRawMemory)
//
//@ Interface-Description
//  Free up a block of memory for reuse.  For safety reasons,
//  'pRawMemory' is set to 'NULL' after deallocating.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Insert 'pRawMemory' as the first node in this heap's free list.
//---------------------------------------------------------------------
//@ PreConditions
//  (isLegalBlock(pRawMemory))
//---------------------------------------------------------------------
//@ PostConditions
//  (pRawMemory == NULL)
//@ End-Method
//=====================================================================

void
Heap::deallocate(void*& pRawMemory)
{
  CALL_TRACE("deallocate(pRawMemory)");
  AUX_CLASS_PRE_CONDITION((isLegalBlock(pRawMemory)), Uint32(pRawMemory));

#if defined(SIGMA_DEBUG)
  if (Foundation::IsDebugOn(::ABSTRACT_HEAP)  ||
      Foundation::IsDebugOn(::FIXED_HEAP))
  {
    cout << "Deallocating a Block:\n"
	 << "  Block Memory:  " << pRawMemory << '\n' << endl;
  }
#endif  // defined(SIGMA_DEBUG)

  // convert 'pRawMemory' to a node pointer...
  Heap::Node*  pNode = (Node*)pRawMemory;

  // prepend 'pNode' to the free list...
  pNode->pNext = pFreeList_;
  pFreeList_   = pNode;

  pRawMemory = NULL;  // nullify the client's pointer...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)
//	               [static]
//
//@ Interface-Description
//	Report the software fault that occured within the source code
//	of this class.  The fault, indicated by 'softFaultID', occured
//	at 'lineNumber' in 'pFileName'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//	none
//---------------------------------------------------------------------
//@ PostConditions
//	none
//@ End-Method
//=====================================================================

void
Heap::SoftFault(const SoftFaultID softFaultID,
			const Uint32      lineNumber,
			const char*       pFileName,
			const char*       pBoolTest)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pBoolTest)");
  FaultHandler::SoftFault(softFaultID, FOUNDATION, ABSTRACT_HEAP, lineNumber,
                          pFileName, pBoolTest);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Heap(pInitFreeList, numMaxBlocks, nodesPerBlock)
//      [Constructor]
//
//@ Interface-Description
//  Construct this heap using 'pInitFreeList' for the blocks, where each
//  block is 'nodesPerBlock' nodes big.  This heap has a maximum of
//  'numMaxBlocks' blocks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initialize the block of nodes as a linked list of free nodes.
//---------------------------------------------------------------------
//@ PreConditions
//  (pInitFreeList != NULL)
//  (numMaxBlocks > 0  &&  nodesPerBlock > 0)
//---------------------------------------------------------------------
//@ PostConditions
//  (isEmpty())
//@ End-Method
//=====================================================================

Heap::Heap(Heap::Node* pInitFreeList,
			   const Uint32        numMaxBlocks,
			   const Uint32        nodesPerBlock)
			   : pFreeList_(pInitFreeList),
			     MAX_BLOCKS_(numMaxBlocks),
			     NODES_PER_BLOCK_(nodesPerBlock)
{
  CALL_TRACE("Heap(pInitFreeList, numMaxBlocks, nodesPerBlock)");
  SAFE_CLASS_PRE_CONDITION((pInitFreeList != NULL));
  SAFE_CLASS_PRE_CONDITION((numMaxBlocks > 0  &&  nodesPerBlock > 0));

  Node*  pFreeNode = pFreeList_;
  Uint   blockNum;

  // initialize the free list of heap nodes...
  for (blockNum = 0u; blockNum < (MAX_BLOCKS_ - 1); blockNum++)
  {
    // for each block in this heap, set the next free block to be
    // 'NODES_PER_BLOCK_' from the current free node, and then move
    // the current node to the next node...
    pFreeNode->pNext = (pFreeNode + NODES_PER_BLOCK_);
    pFreeNode        = pFreeNode->pNext;
  }

  pFreeNode->pNext = NULL;  // terminate the free list...
}   // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getFirstNode_()  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that is to be overridden by any
//  derived class.  The derived classes are to define this overridden
//  method to return a pointer to the first node of this heap's memory
//  heap.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  print_(ostr)  [const, pure virtual]
//
//@ Interface-Description
//  This is a pure virtual method that all derived classes are to
//  override and define to dump the contents of an instance to 'ostr'.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//=====================================================================
//
//  Overloaded New Operator...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Free-Function:  operator new(objectSize, objectHeap)
//          [Overloaded New Operator]
//
//@ Interface-Description
//  This is an global operator, that is to be used to run a specified
//  constructor on a block of memory "allocated" from 'objectHeap'.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The second pre-condition is "safe", because 'Heap::allocate()'
//  checks it.
//---------------------------------------------------------------------
//@ PreConditions
//  (objectSize <= objectHeap.getBlockSize())
//  (!objectHeap.isFull())
//---------------------------------------------------------------------
//@ PostConditions
//  ((new (objectHeap) TYPE(...)) != NULL)
//@ End-Method
//=====================================================================

void*
operator new(size_t objectSize, Heap& objectHeap)
{
  CALL_TRACE("::operator new(objectSize, objectHeap)");
  FREE_PRE_CONDITION((objectSize <= objectHeap.getBlockSize()),
		     FOUNDATION, ABSTRACT_HEAP);
  SAFE_FREE_PRE_CONDITION((!objectHeap.isFull()), FOUNDATION, ABSTRACT_HEAP);

  return(objectHeap.allocate());
}   // $[TI1]
