#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SstResultsSubScreen - The screen selected by pressing the
// SST result tab button on the Upper Screens Select Area during Service
// Mode, or automatically displayed during SST Setup subscreen activation,
// or by pressing the SST result log button on the Upper Other screen during
// normal ventilation mode.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is a container, derived from the SubScreen and LogTarget classes.
// It contains a SubScreenTitleArea object for the subscreen title, a
// ScrollableLog object (with scrollability disabled, with possible future
// expansion in mind).  This log displays the results of SST tests.  It also
// has constants which define the log's location, dimension and format.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.
// The getLogEntryColumn() method is called to retrieve the contents of any
// newly displayed rows. 
//---------------------------------------------------------------------
//@ Rationale
// Created to display the SST test results on the upper screen. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The overall responsibility of this subscreen is to provide a
// scrollable display of the SST test result history maintained by the
// Persistent subsystem. 
// The SST result log is presented in table format with 6 rows and 4 columns
// using a ScrollableLog object.  Whenever the ScrollableLog needs to know
// the contents of a log cell it calls getLogEntryColumn().  This method
// retrieves the appropriate text for the specified entry, and returns it to
// ScrollableLog for display.
//
// There is tricky problem with the Other Screens screen select button
// (contained by the UpperScreenSelectArea).  That is, when a new "other"
// subscreen is activated from this Other Screens subscreen, the default
// deactivation behavior for the Other Screens subscreen pops up the Other
// Screens screen select button.  However, this is not the desired result -- we
// want the Other Screens screen select button to remain selected when a new
// "other" subscreen is activated.  Thus, the Other Screens screen select
// button must be "manually" pushed down (selected) when a new "other" subscreen
// is activated.  See the currentLogEntryChangeHappened() method for details.
//
// The ScrollableLog menu and the subscreen title are added to the Container 
// when it is constructed.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// None.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SstResultsSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:30   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 015   By: mnr   Date: 24-Mar-2010   SCR Number: 6436
//  Project:  PROX4
//  Description:
//      PROX SST related updates.
// 
//  Revision: 014   By: gdc   Date:  03-Jun-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//      Refined text positioning as part of automated text positioning
//      and added Date International feature. (rhj)
//
//  Revision: 013   By: sah   Date:  31-Oct-2000    DR Number: 5780
//  Project:  VTPC
//  Description:
//      Added use of 'SST_NEO_FILTER_TEST_DATA_LABEL' with a NEONATAL
//      run of SST.
//
//  Revision: 012   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 011  By:  hhd	   Date:  27-Apr-1999    DCS Number:  5322
//  Project:  ATC
//  Description:
//		Initial version.
//
//  Revision: 010  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 009  By:  clw    Date:  19-May-98    DR Number:
//    Project:  Sigma (R8027)
//    Description:
//      Added japanese-only code to implement different date format for Japanese.
//
//  Revision: 008  By:  yyy    Date:  03-Nov-97    DR Number: 2439
//    Project:  Sigma (R8027)
//    Description:
//      Removed inVisible() checking when setting change call occured
//		to make sure the unitId_ will always be updated.
//
//  Revision: 007  By:  hhd	   Date:  06-Oct-97    DR Number: 2540 
//    Project:  Sigma (R8027)
//    Description:
//		Removed code that supports commas in floating point format.
//
//  Revision: 006  By:  yyy    Date:  08-Sep-97    DR Number: 2439
//    Project:  Sigma (R8027)
//    Description:
//      Changed ML_CM_H2O_LABEL to ML_LABEL. Added pressure unit to
//		display the correct pressure unit.
//
//  Revision: 005  By:  yyy    Date:  20-Aug-97    DR Number: 1988
//    Project:  Sigma (R8027)
//    Description:
//      Added handles for different pressure unit display.
//
//  Revision: 004  By:  hhd    Date:  18-AUG-97    DR Number: 2321
//    Project:  Sigma (R8027)
//    Description:
//       Converted decimal point to comma for all European but German language.
//
//  Revision: 003  By:  yyy    Date:  01-AUGL-97    DR Number: 2223
//    Project:  Sigma (R8027)
//    Description:
//		Changed the column 4 size from 80 to 90 for incomplete strings
//      
//  Revision: 002  By:  yyy    Date:  02-JUL-97    DR Number: 2265
//    Project:  Sigma (R8027)
//    Description:
//      02-JUL-97  Removed hard coded strings for translation.
//      22-JUL-97  Changed all margin for each column from 10 to 0.
//				   Changed date/time, result columns from LEFT to CENTER
//				   for better fitting.
//		22-Jul-97 Changed the column size to fix the German translation.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SstResultsSubScreen.hh"
#include <limits.h>
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "DiagnosticFault.hh"
#include "MiscStrs.hh"
#include "NovRamManager.hh"
#include "ResultEntryData.hh"
#include "ScrollableLog.hh"
#include "SmDataId.hh"
#include "SmTestId.hh"
#include "TextUtil.hh"
#include "CodeLogEntry.hh"
#include "SettingSubject.hh"
#include "GuiApp.hh"
#include "SettingContextHandle.hh"
//@ End-Usage

//@ Code...

// Initialize static constants.

static const Int32 SST_SM_STATUS_X_ = 406;
static const Int32 SST_SM_STATUS_Y_ = 0;
static const Int32 SST_STATUS_WIDTH_ = 300;
static const Int32 SST_STATUS_HEIGHT_ = 30;

static const Int32 SST_APP_STATUS_X_ = 406;
static const Int32 SST_APP_STATUS_Y_ = 0;
static const Int32 SST_DATE_TIME_X1_ = 175;
static const Int32 SST_DATE_TIME_X2_ = 269;
static const Int32 SST_DATE_TIME_Y_ = 5;
static const Int32 SST_MAX_STRING_ = 88;

static const Int32 SST_TIME_STAMP_ = 0;
static const Int32 SST_TEST_NAME_= 1;
static const Int32 SST_TEST_DATA_ = 2;
static const Int32 SST_TEST_RESULT_ = 3;

static const Int32 LOG_X_ = 2;
static const Int32 LOG_Y_ = 44;
static const Int32 LOG_ROW_HEIGHT_ = 35;
static const Int32 LOG_NUM_ROWS_ = 6;
static const Int32 LOG_NUM_COLUMNS_ = 4;

static const Int32 LOG_COLUMN1_WIDTH_ = 56;
static const Int32 LOG_COLUMN2_WIDTH_ = 113;
static const Int32 LOG_COLUMN3_WIDTH_ = 351; 
static const Int32 LOG_COLUMN4_WIDTH_ = 89;
static const Uint16 TEXT_MARGIN2_ = 0;

static const Uint16 FIRST_DISPLAYED_ENTRY_ = 0;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SstResultsSubScreen()  [Constructor]
//
//@ Interface-Description
// Constructor.  The 'pSubScreenArea' parameter is the pointer to the
// parent SubScreenArea which contains this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initializes all members; Sets the position, size and color of the subscreen
// area. Add the subscreen title object and the log to the 
// parent container for display.
//---------------------------------------------------------------------
//@ PreCondition
// The given 'pSubScreenArea' pointer must not be NULL.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstResultsSubScreen::SstResultsSubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	titleArea_(MiscStrs::TEST_RESULT_SUBSCREEN_TITLE),
	overallResult_(UNDEFINED_TEST_RESULT_ID),
	sstStatus_(SST_APP_STATUS_X_, SST_APP_STATUS_Y_,
						SST_STATUS_WIDTH_, SST_STATUS_HEIGHT_,
						MiscStrs::SST_STATUS_LABEL,
						NULL_STRING_ID),
	sstDateTimeLabel_(MiscStrs::SST_DATE_TIME_LABEL),
	sstDateTimeValue_(MiscStrs::EMPTY_STRING),
	pResultLog_(NULL),
	unitId_(GuiApp::PressUnits() == PressUnitsValue::CMH2O_UNIT_VALUE ?
							MiscStrs::TITLE_CMH20_UNIT :
							MiscStrs::TITLE_HPA_UNIT)	// $[TI1]	$[TI2]

{
	CALL_TRACE("SstResultsSubScreen::SstResultsSubScreen(pSubScreenArea)");


	// Size and position the area
	setX(0);
	setY(0);


	// Set subscreen background color
	setFillColor(Colors::MEDIUM_BLUE);

	sstDateTimeLabel_.setX(SST_DATE_TIME_X1_);
	sstDateTimeLabel_.setY(SST_DATE_TIME_Y_);
	sstDateTimeLabel_.setColor(Colors::WHITE);
	sstDateTimeValue_.setX(SST_DATE_TIME_X2_);
	sstDateTimeValue_.setY(SST_DATE_TIME_Y_);
	sstDateTimeValue_.setColor(Colors::WHITE);

	// Add screen title object and text buttons.
	addDrawable(&titleArea_);
	addDrawable(&sstStatus_);
	addDrawable(&sstDateTimeLabel_);
	addDrawable(&sstDateTimeValue_);

	TestResultStrings_[SST_NOT_APPLICABLE] = MiscStrs::NOT_APPLICABLE_STATUS_MSG;
	TestResultStrings_[SST_UNDEFINED] = MiscStrs::VTS_UNDEFINED_RESULT_LABEL;
	TestResultStrings_[SST_INCOMPLETE] = MiscStrs::VTS_INCOMPLETE_TEST_LABEL;
	TestResultStrings_[SST_NOT_INSTALLED] = MiscStrs::NOT_INSTALLED_STATUS_MSG;
	TestResultStrings_[SST_PASSED] = MiscStrs::PASSED_STATUS_MSG;
	TestResultStrings_[SST_OVERRIDDEN] = MiscStrs::ALERT_STATUS_MSG;
	TestResultStrings_[SST_MINOR] = MiscStrs::ALERT_STATUS_MSG;
	TestResultStrings_[SST_MAJOR] = MiscStrs::FAILED_STATUS_MSG;

	// Setup the test id and test name table
	setSstTestNameTable_();

	// Register for Pressure Units setting changes
	attachToSubject_(SettingId::PRESS_UNITS, Notification::VALUE_CHANGED);
}								  


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SstResultsSubScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SstResultsSubScreen::~SstResultsSubScreen(void)
{
	CALL_TRACE("SstResultsSubScreen::~SstResultsSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate() [virtual]
//
//@ Interface-Description
// Prepares this subscreen for display.  Displays the SST result log
// with the latest status/data.
//---------------------------------------------------------------------
//@ Implementation-Description
// First create and setup the ScrollableLog.  The set the view to the
// "top" of the log.  That is, set the display to show the six SST entries
// in the log.  This is done by setting the 'FIRST_DISPLAYED_ENTRY_' index
// to the top of the log and then calling the normal screen refresh routine.
// Sets the position, the format of and number of entries in the 
// ScrollableLog menu. 
// $[01206] The SST result log subscreen shall allow the user to view ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstResultsSubScreen::activate(void)
{
	CALL_TRACE("SstResultsSubScreen::activate(void)");

	if (GuiApp::GetGuiState() == STATE_SERVICE)
	{	// Service Mode 						    // $[TI1]
		setWidth(SERVICE_UPPER_SUB_SCREEN_AREA_WIDTH);
		setHeight(SERVICE_UPPER_SUB_SCREEN_AREA_HEIGHT);
		sstStatus_.setX(SST_SM_STATUS_X_);
		sstStatus_.setY(SST_SM_STATUS_Y_);
	}
	else
	{	// Normal ventilation mode					// $[TI2]
		setWidth(SUB_SCREEN_AREA_WIDTH);
		setHeight(UPPER_SUB_SCREEN_AREA_HEIGHT);
		sstStatus_.setX(SST_APP_STATUS_X_);
		sstStatus_.setY(SST_APP_STATUS_Y_);
	}



	// Create the result log
	pResultLog_ = ScrollableLog::New(this, LOG_NUM_ROWS_, LOG_NUM_COLUMNS_,
														LOG_ROW_HEIGHT_, TRUE);

	// Setup menu stuff
	pResultLog_->setX(LOG_X_);
	pResultLog_->setY(LOG_Y_);

	// Setup the test logs to non-scrollable and non-selectable.
	pResultLog_->setUserScrollable(TRUE);
	pResultLog_->setUserSelectable(FALSE);

	// Setup column info for the menu's ScrollableLog
	pResultLog_->setColumnInfo(0, MiscStrs::SST_LOG_COL_1_TITLE, LOG_COLUMN1_WIDTH_, CENTER,
					0, TextFont::NORMAL, TextFont::TEN, TextFont::TEN);
	pResultLog_->setColumnInfo(1, MiscStrs::SST_LOG_COL_2_TITLE, LOG_COLUMN2_WIDTH_, LEFT, 
					2, TextFont::NORMAL, TextFont::TEN, TextFont::TEN);
	pResultLog_->setColumnInfo(2, MiscStrs::SST_LOG_COL_3_TITLE, LOG_COLUMN3_WIDTH_, LEFT, 
					2, TextFont::NORMAL, TextFont::TEN, TextFont::TEN);
	pResultLog_->setColumnInfo(3, MiscStrs::SST_LOG_COL_4_TITLE, LOG_COLUMN4_WIDTH_, CENTER, 
					0, TextFont::NORMAL, TextFont::TEN, TextFont::TEN);

	addDrawable(pResultLog_);

	// Get ResultLog from NovRam
	NovRamManager::GetSstResultEntries(sstResTableEntries_);

	// Load test data from NovRam into class storage for display
	sstTestData_.loadDataFromNovRam();

	// Format and initialize string to display SST's overall status 
	FormatStatusAndDate();


	const DiscreteValue CIRCUIT_TYPE =
		SettingContextHandle::GetSettingValue(ContextId::ACCEPTED_CONTEXT_ID,		 
											SettingId::PATIENT_CCT_TYPE);

    // Add the prox test if circuit type is NEO
	if ((CIRCUIT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT) &&
		 (GuiApp::IsProxInstalled()))
	{
		// Inform resultLog_ of the first log entry number that should be displayed
		// on the first row of the log, and the number of entries in the menu 
		// (this action refreshes the menu if it's currently displayed.)
		pResultLog_->setEntryInfo(FIRST_DISPLAYED_ENTRY_, SmTestId::SST_TEST_MAX); 
	}
	else
	{
		// Inform resultLog_ of the first log entry number that should be displayed
		// on the first row of the log, and the number of entries in the menu 
		// (this action refreshes the menu if it's currently displayed.)
		pResultLog_->setEntryInfo(FIRST_DISPLAYED_ENTRY_, SmTestId::SST_TEST_MAX -1); 
	}



	// Activate the menu
	pResultLog_->activate();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate() [virtual]
//
//@ Interface-Description
// Prepares this subscreen for removal from the display.
//---------------------------------------------------------------------
//@ Implementation-Description
// We must remove the scrollable log from the subscreen because it is
// a shared object, used by other screens.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstResultsSubScreen::deactivate(void)
{
	CALL_TRACE("SstResultsSubScreen::deactivate(void)");

	pResultLog_->deactivate();

	// Remove scrollable log
	removeDrawable(pResultLog_);
}								    // $[TI1]


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: valueUpdate
//
//@ Interface-Description
//  Called when setting values on this subscreen has changed.
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
// We first check if we're visible.  We ignore all setting changes when this
// subscreen is not active.  If settings have changed from being current
// to proposed then change the screen title and display the Proceed button,
// else if settings have reverted to current from being proposed then
// change back the screen title and remove the Proceed button.
//
// $[01058] Proceed button only displayed on first setting change
// $[01118] As soon as any settings have changed display Proceed button.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstResultsSubScreen::valueUpdate(
							const Notification::ChangeQualifier qualifierId,
							const SettingSubject*               pSubject
								)
{
	CALL_TRACE("valueUpdate(qualifierId, pSubject)");

	if (isVisible()  &&  qualifierId == Notification::ACCEPTED  &&
		pSubject->getId() == SettingId::PRESS_UNITS)
	{											// $[TI1]
		const DiscreteValue  UNITS_VALUE = pSubject->getAcceptedValue();

		if (UNITS_VALUE == PressUnitsValue::CMH2O_UNIT_VALUE)
		{										// $[TI1.1]
			unitId_ = MiscStrs::TITLE_CMH20_UNIT;
		}
		else
		{										// $[TI1.2]
			unitId_ = MiscStrs::TITLE_HPA_UNIT;
		}
	}											// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getLogEntryColumn
//
//@ Interface-Description
// Called by ScrollableLog when it needs to retrieve the text content
// of a particular column of an alarm log entry.  Passed the following
// parameters:
// >Von
//	entryNumber			The index of the log entry
//	columnNumber		The column number of the log entry
//	rIsCheapText		Output parameter.  A reference to a flag which
//						determines whether the text content of the log
//						entry is specified as cheap text or not.
//	rString1			String id of the first string. Can be
//						set to NULL_STRING_ID to indicate an empty entry
//						column.
//	rString2			String id of the second string.
// >Voff
// We retrieve information for the appropriate column, make sure it's
// formatted correctly and return it to ScrollableLog.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First, turn off CheapText option because no cheap text is used here.
//	Next, for the given column in the ScrollableLog menu, set the appropriate
//	column title.
//---------------------------------------------------------------------
//@ PreCondition
//  Entry and column numbers has to be within bounds set. 
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void
SstResultsSubScreen::getLogEntryColumn(Uint16 entry, Uint16 column,
		Boolean &rIsCheapText, StringId &rString1, StringId &rString2, 
		StringId &, StringId &)
{
	CALL_TRACE("getLogEntryColumn(...)");

	CLASS_PRE_CONDITION((entry <= SmTestId::SST_TEST_MAX) && (column < LOG_NUM_COLUMNS_));

    // Set return parameters 
    Real32	testData;
    static wchar_t tmpBuffer1[256];
    static wchar_t tmpBuffer2[256];
    wchar_t displayString[SST_MAX_STRING_];
    ResultEntryData resultData;

    displayString[0] = L'\0';
    tmpBuffer1[0] = L'\0';
    tmpBuffer2[0] = L'\0';
    rString1 = &tmpBuffer1[0];
    rString2 = &tmpBuffer2[0];
    rIsCheapText = FALSE;

    if  (entry == ::MAX_SST_RESULTS)
    {								// $[TI1]
		return;
    }

	// If no status is there to display, only display test entry's name
    if  (sstResTableEntries_[sstTestId_[entry]].isClear())
    {								//  $[TI2]
		if (column == SST_TEST_NAME_)
		{						    // $[TI2.1]
			TextUtil::WordWrap((wchar_t *)DiagnosticFault::GetSstTestNameStr(sstTestId_[entry]),
								tmpBuffer1, tmpBuffer2,
								TextFont::NORMAL, TextFont::TEN,
								LOG_COLUMN2_WIDTH_, LOG_ROW_HEIGHT_, 
								LEFT, TEXT_MARGIN2_);
		}						    // $[TI2.2]
		rIsCheapText = TRUE;
		return;
    }								 // $[TI3]

	sstResTableEntries_[sstTestId_[entry]].getResultData(resultData);
	TestResultId testResult = resultData.getResultId();

	switch (column)
    {
	case SST_TIME_STAMP_:
	{							    // $[TI4]
	// Timestamp, let's format it
		const TimeStamp & timeStamp = resultData.getTimeOfResult();

		// Formatting Time/Date string
		TextUtil::FormatTime(tmpBuffer1, L"{p=10,y=10:%T}", timeStamp);

		wchar_t format[30];
		swprintf(format, L"{p=8,y=10:%s}", MiscStrs::LOG_DATE_FORMAT);
		TextUtil::FormatTime(tmpBuffer2, format, timeStamp);
		// rIsCheapText set FALSE so LogCell centers text
		rIsCheapText = FALSE;
		break;
	}
	case SST_TEST_NAME_:
		// Test name					 // $[TI5]
		TextUtil::WordWrap((wchar_t *)DiagnosticFault::GetSstTestNameStr(sstTestId_[entry]),
								tmpBuffer1, tmpBuffer2,
								TextFont::NORMAL, TextFont::TEN,
								LOG_COLUMN2_WIDTH_, LOG_ROW_HEIGHT_, 
								LEFT, TEXT_MARGIN2_);
		rIsCheapText = TRUE;
		break;

	case SST_TEST_DATA_:											 
	{							    // $[TI6]
		// Test Data

		switch (sstTestId_[entry])
		{
		case SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID:
		{						    // $[TI6.1]
			// Initial data value, INT_MIN, is used to detect meaningful data 
			if ((testData = sstTestData_.getTestResultData(sstTestId_[entry],
							SmDataId::CIRCUIT_RESISTANCE_INSP_PRESSURE)) != INT_MIN)
			{					    // $[TI6.1.1]
				swprintf(tmpBuffer1, L"%s %.2f %s",
								MiscStrs::SST_CIRCUIT_RESISTANCE_TEST_INSP_LIMB_LABEL,
								testData, 
								unitId_);
				if ((testData = sstTestData_.getTestResultData(sstTestId_[entry],
							SmDataId::CIRCUIT_RESISTANCE_EXP_PRESSURE)) != INT_MIN)
				{					    // $[TI6.1.1.1]
					swprintf(displayString, L", %s %.2f %s",
								MiscStrs::SST_CIRCUIT_RESISTANCE_TEST_EXH_LIMB_LABEL,
								testData, 
								unitId_);
					wcscat(tmpBuffer1, displayString);
				}
			}
			else
			{					    // $[TI6.1.2]
				rString1 = &tmpBuffer1[0];
			}
			break;
		}
		case SmTestId::SST_TEST_EXPIRATORY_FILTER_ID:
		{						    // $[TI6.2]
			// Display test data if applied 
			if ((testData = sstTestData_.getTestResultData(sstTestId_[entry],
						SmDataId::SST_FILTER_DELTA_PRESSURE)) != INT_MIN)

			{					    // $[TI6.2.1]
				const DiscreteValue  PATIENT_CCT_TYPE =
					SettingContextHandle::GetSettingValue(
												ContextId::ACCEPTED_CONTEXT_ID,
												SettingId::PATIENT_CCT_TYPE
														 );

				const StringId  DATA_LABEL =
					(PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
					 ? MiscStrs::SST_NEO_FILTER_TEST_DATA_LABEL	// $[TI6.2.1.1]
					 : MiscStrs::SST_FILTER_TEST_DATA_LABEL;	// $[TI6.2.1.2]

				
				swprintf(tmpBuffer1, L"%s %.2f %s", DATA_LABEL, testData, 
								unitId_);
			}
			else					 // $[TI6.2.2]
			{
				rString1 = &tmpBuffer1[0];
			}
			break;
		}
		case SmTestId::SST_TEST_CIRCUIT_LEAK_ID:
		{						    // $[TI6.3]
			if ((testData = sstTestData_.getTestResultData(sstTestId_[entry],
							SmDataId::SST_LEAK_DELTA_PRESSURE)) != INT_MIN)
			{	 					 // $[TI6.3.1]
				swprintf(tmpBuffer1, L"%s %.2f %s",
								MiscStrs::SST_LEAK_TEST_DATA_LABEL,
								testData, unitId_);
			}
			else
			{					    // $[TI6.3.2]
				rString1 = &tmpBuffer1[0];
			}
			break;
		}
		case SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID:
									// $[TI6.4]
			rString1 = &tmpBuffer1[0];
			break;

		case SmTestId::SST_TEST_FLOW_SENSORS_ID:
									// $[TI6.5]
			rString1 = &tmpBuffer1[0];
			break;

		case SmTestId::SST_TEST_PROX_ID:
            rString1 = &tmpBuffer1[0];
			break;

		case SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID:
		{						    // $[TI6.6]
			if ((testData = sstTestData_.getTestResultData(sstTestId_[entry],
					SmDataId::COMPLIANCE_CALIB_NOMINAL_COMPLIANCE)) != INT_MIN)
			{					    // $[TI6.6.1]
				swprintf(tmpBuffer1, L"%s %.2f %s/%s",
							MiscStrs::COMPLIANCE_CALIB_TEST_DATA_LABEL,
							testData, MiscStrs::ML_LABEL, unitId_);
			}
			else
			{					    // $[TI6.6.2]
		  		rString1 = &tmpBuffer1[0];
			}
			break;
		}
		default:					    
			CLASS_ASSERTION_FAILURE();
			break;
		}	// End switch(entry)	

		// If current test doesn't pass, show the diagnostic information
		if (testResult != PASSED_TEST_ID)
		{						    // $[TI6.6.3]
			// Compose diagnostic display string(s)
			DiagnosticFault::ComposeDisplayDiagStrings(
						DiagnosticCode::SHORT_SELF_TEST_DIAG, sstTestId_[entry], 
						displayString, NULL);

//	Due to the current lower code layer's design, tmpBuffer1 has to be filled
//	before tmpBuffer2 can be filled
			if (tmpBuffer1[0] != L'\0')		   // $[TI6.6.3.1]
			{
				wcscpy(tmpBuffer2, displayString);
			}
			else
			{					    // $[TI6.6.3.2]
				wcscpy(tmpBuffer1, displayString);
				rString2 = &tmpBuffer2[0];
			}
		}
		else
		{						    // $[TI6.6.4]
			rString2 = &tmpBuffer2[0];
		}
		break;
    } 

	case SST_TEST_RESULT_:											
	{							    // $[TI7]
		// SST Test Result
		wchar_t testBuff[250];

		wcscpy(testBuff, TestResultStrings_[testResult]);
		if (TextUtil::WordWrap(testBuff, tmpBuffer1, tmpBuffer2,
				TextFont::NORMAL, TextFont::TEN,
				LOG_COLUMN4_WIDTH_, LOG_ROW_HEIGHT_, CENTER, 0) == 2)
		{								// $[TI21]
			rString2 = (StringId)&tmpBuffer2;
		}								// $[TI22]

		rString1 = (StringId)&tmpBuffer1;
		rIsCheapText = TRUE;
		break;
	}

    default:													   
		CLASS_ASSERTION_FAILURE();
		break;
   }		// End switch(column)
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FormatStatusAndDate
//	[private]
//
//---------------------------------------------------------------------
//@ Interface-Description
//	Called when we are need to update the status result and time.  We
//  get the overall testing status and format the status string.  Finally,
//  we display time of last test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Get test's overall status from NovRam then format and set a string to be
//	displayed in the Overall Status area.  We also display time of last test.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
 
void 
SstResultsSubScreen::FormatStatusAndDate()
{
	// If test is incomplete, no status is displayed
	ResultEntryData overallSstResultData;

	// Get the overall testing status
	NovRamManager::GetOverallSstResult(overallSstResultData);
	overallResult_ = overallSstResultData.getResultId();

	// Format the status string
	switch (overallResult_)
	{
	case PASSED_TEST_ID:  			 // $[TI1]
		sstStatus_.setStatus(MiscStrs::PASSING_OVERALL_MSG);
		break;
	case MINOR_TEST_FAILURE_ID:	// $[TI2]
		sstStatus_.setStatus(MiscStrs::ALERTING_OVERALL_MSG);
		break;
	case MAJOR_TEST_FAILURE_ID:	// $[TI3]
		sstStatus_.setStatus(MiscStrs::FAILURE_OVERALL_MSG);
		break;
	case OVERRIDDEN_TEST_ID:		 // $[TI4]
		sstStatus_.setStatus(MiscStrs::OVERRIDEN_OVERALL_MSG);
		break;
	case INCOMPLETE_TEST_RESULT_ID:		 // $[TI5]
		sstStatus_.setStatus(MiscStrs::IMCOMPLETE_OVERALL_MSG);
		break;
	case UNDEFINED_TEST_RESULT_ID:		 // $[TI6]
		sstStatus_.setStatus(MiscStrs::UNDEFINED_OVERALL_MSG);
		break;
	case NOT_APPLICABLE_TEST_RESULT_ID:
	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}

	if (overallResult_ != UNDEFINED_TEST_RESULT_ID)
	{				   // $[TI7]
		// Display time of last test 
		wchar_t dateTime[100];
		const TimeStamp & timeStamp = overallSstResultData.getTimeOfResult();

		wchar_t format[30];
		swprintf(format, L"{p=12,y=18:%%T, %s}", MiscStrs::LOG_DATE_FORMAT);
		TextUtil::FormatTime(dateTime, format, timeStamp);

		sstDateTimeValue_.setText(dateTime);
	} 				 // $[TI8]
}								   


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setSstTestNameTable_
//
//@ Interface-Description
//  Initialize the sstTestId_ tables.
//  The sstTestId_ array contains Service-Mode test ids.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply set the proper text and ids to the corresponding tables.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SstResultsSubScreen::setSstTestNameTable_(void)
{
	CALL_TRACE("SstResultsSubScreen::setSstTestNameTable_(void)");

	Int testIndex = 0;

	sstTestId_[testIndex++]			= SmTestId::SST_TEST_FLOW_SENSORS_ID;
	sstTestId_[testIndex++]			= SmTestId::SST_TEST_CIRCUIT_PRESSURE_ID;
	sstTestId_[testIndex++]			= SmTestId::SST_TEST_CIRCUIT_LEAK_ID;
	sstTestId_[testIndex++]			= SmTestId::SST_TEST_EXPIRATORY_FILTER_ID;
	sstTestId_[testIndex++]			= SmTestId::SST_TEST_CIRCUIT_RESISTANCE_ID;
	sstTestId_[testIndex++]			= SmTestId::SST_TEST_COMPLIANCE_CALIBRATION_ID;
	sstTestId_[testIndex++]			= SmTestId::SST_TEST_PROX_ID;

#ifdef SIGMA_DEVELOPMENT

	// Make sure total test count is within the maximum allowed.
	SAFE_CLASS_ASSERTION(testIndex <= SmTestId::SST_TEST_MAX);
#endif // SIGMA_DEVELOPMENT
	 // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//        [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
SstResultsSubScreen::SoftFault(const SoftFaultID  softFaultID,
                                const Uint32       lineNumber,
                                const char*        pFileName,
                                const char*        pPredicate)
{
    CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
    FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SSTRESULTSSUBSCREEN,
                            lineNumber, pFileName, pPredicate);
}
