//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file PcapHelper.cpp
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------
#define _CRT_SECURE_NO_WARNINGS

#include "CConfiguration.h"
#include "PcapHelper.h"
#include <string.h>
#include <stdio.h>
#include "CLogger.h"


//-------------------------------------
// C O D E
//-------------------------------------

namespace PcapHelper{

bool g_winpcap=true;

static unsigned char pushback_buffer[5000];
static unsigned int pindex = 0;

//---------------------------
// push_back 
//---------------------------

static void push_back(char in){
    pushback_buffer[pindex++]=in;
}

//---------------------------
// pop_back 
//---------------------------
static char pop_back(){
    return pushback_buffer[--pindex];
}

//---------------------------
// disable_pcap 
//---------------------------
void disable_pcap(){
     g_winpcap=false;
}

//---------------------------
// reset_pcap 
//---------------------------
void reset_pcap(){
    g_winpcap=false;
    pindex = 0;
    g_winpcap=true; 
}


//---------------------------
// read_packet_ 
//---------------------------
int
read_packet_(pcap_t *handle, char *data_out, int len)
{

    static int count = 1;                   /* packet counter */
    
    /* declare pointers to packet headers */
    const struct capture_ethernet *ethernet;  /* The ethernet header [1] */
    const struct capture_ip *ip;              /* The IP header */
    const struct capture_tcp *tcp;            /* The TCP header */
    unsigned char *payload;                    /* Packet payload */

    int size_ip;
    int size_tcp;
    int size_payload;
    
    struct pcap_pkthdr *header;
    const u_char *packet;
    int res=0;


    // If there is already data to be read, read and return.
    if(pindex)
    {
        int cnt = 0;

        while(len && pindex)
        {
            data_out[cnt]=pop_back();
            len--;cnt++;
        }

        return cnt;
    }

    // Wait for packet
    while(res == 0)
    {
        if(g_winpcap == false){
            throw 1;
        }
        res = pcap_next_ex( handle, &header, &packet);
    }
    
    
    // define ethernet header 
    ethernet = (struct capture_ethernet*)(packet);
    
    // define/compute ip header offset 
    ip = (struct capture_ip*)(packet + SIZE_ETHERNET);
    size_ip = IP_HL(ip)*4;

    
    // define/compute tcp header offset 
    tcp = (struct capture_tcp*)(packet + SIZE_ETHERNET + size_ip);
    size_tcp = TH_OFF(tcp)*4;

    
    // define/compute tcp payload (segment) offset 
    payload = (u_char *)(packet + SIZE_ETHERNET + size_ip + size_tcp);
    
    // compute tcp payload (segment) size 
    size_payload = ntohs(ip->ip_len) - (size_ip + size_tcp);
    

    // return buffer
#if 0
    if (size_payload > 0 && size_payload >= len ) {
        memcpy(data_out,payload, size_payload);
        return size_payload;

    }
#endif

    if(size_payload >0){

        if (size_payload <= len ) {
            memcpy(data_out,payload, size_payload);
            return size_payload;
        }
        
        memcpy(data_out,payload, len);

        while(size_payload>len){
              push_back(payload[size_payload-1]);
              size_payload--;
        }
        return len;
    }
    return 0;

}

//---------------------------
// readblock 
//---------------------------
int
readblock (pcap_t *handle, char *buff, size_t len) {
    int rcnt = 0;
    int tcnt;

    while (len) 
    {

        if(g_winpcap == false)
        {
            throw 1;
        }

        tcnt = read_packet_(handle, buff + rcnt, 1 ); //todo make len

        if (tcnt <= 0) 
        {
            fprintf(stderr,"Error(readblock1) %i\n",tcnt);
            break;
        }
        rcnt += tcnt;
        len -= tcnt;
    }

    if(len!=0){

            fprintf(stderr,"Error(readblock2) %i %i\n",tcnt,len);
    }

    return rcnt;
}



//---------------------------
// strIp2ulongIp 
//---------------------------
unsigned long strIp2ulongIp(char *strIp)
{
    unsigned int  i[4];

    union{
        unsigned char j[4];
        unsigned long k;
    } m;

    if(strIp)
    {
        sscanf (strIp,"%i.%i.%i.%i",&i[0],&i[1],&i[2],&i[3]);
        m.j[0]=(unsigned char)i[0];
        m.j[1]=(unsigned char)i[1];
        m.j[2]=(unsigned char)i[2];
        m.j[3]=(unsigned char)i[3];
    }
    else
    {
        m.k = (unsigned long)0;
    }

    return m.k;
}




//---------------------------
// getDeviceList 
//---------------------------
char * getDeviceList(char *str, char *buffer){

    char errbuf[PCAP_ERRBUF_SIZE];
    static char pbuffer[300];
	char *pTempDesc;

    unsigned long desired_address;
    pcap_if_t *alldevs, *d, *user_device;
    int user_device_count;

    user_device = NULL;
    user_device_count = 0;
    desired_address = strIp2ulongIp(str);


    if (pcap_findalldevs(&alldevs, errbuf) == -1 )
    {
        return NULL;
    }

    for(d= alldevs; d != NULL; d= d->next)
    {
        pcap_addr_t *a;

        for(a=d->addresses;a;a=a->next) 
        {
            if(a->addr->sa_family == AF_INET)
            {
                unsigned long net_device, net_calc;

                net_device = 
                    ((struct sockaddr_in *)a->addr)->sin_addr.s_addr &
                    ((struct sockaddr_in *)a->netmask)->sin_addr.s_addr;


                net_calc = 
                    desired_address & 
                    ((struct sockaddr_in *)a->netmask)->sin_addr.s_addr;


                if(net_calc == net_device)
                {
                    user_device=d;
                    user_device_count++;
                }
            }
        }
    }

    if(user_device == NULL)
    {
        return NULL;
    }

#if 0
    if(user_device_count>1)
    {
        //TODO[ME]: Should add error reporting
        return NULL;
    }
#endif

    if(buffer==NULL){
        buffer=pbuffer;
    }

    strcpy(buffer, user_device->name);

	//Log message a
	if(user_device->description == NULL)
	{
		user_device->name="";
	}
	else
	{
		pTempDesc=user_device->description;
	}

	LOG_MESSAGE(LM_CRITICAL,"PcapHelper: Listening to %s %s.",user_device->name,user_device->name);


    pcap_freealldevs(alldevs);

    return buffer;

}

}
