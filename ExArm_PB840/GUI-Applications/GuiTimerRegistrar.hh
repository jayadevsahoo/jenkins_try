#ifndef GuiTimerRegistrar_HH
#define GuiTimerRegistrar_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-2007, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: GuiTimerRegistrar - The central registration, control, and
// dispatch point for all GUI timer events.  The timer events are
// received from the OS-Foundation subsystem via the GUI task's User
// Annunciation queue.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiTimerRegistrar.hhv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: gdc    Date: 26-May-2007   DCS Number:  6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 003   By: srp   Date:  24-Apr-2002    DR Number: 5327 & 5908
//  Project:  NeoMode
//  Description:
//      Added Modification log. Updated for NeoMode  03/01/00
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


//@ Usage-Classes
#include "UserAnnunciationMsg.hh"
#include "GuiTimerId.hh"
#include "MsgTimer.hh"
#include "GuiAppClassIds.hh"
class GuiTimerTarget;
//@ End-Usage


class GuiTimerRegistrar
{
public:
	static Boolean TimerEventHappened(void);
	static void TimerChangeHappened(UserAnnunciationMsg &eventMsg);
	static void RegisterTarget(GuiTimerId::GuiTimerIdType timerId,
							   GuiTimerTarget* pTarget);
	static void StartTimer(GuiTimerId::GuiTimerIdType timerId);
	static void StartTimer(GuiTimerId::GuiTimerIdType timerId,
						   GuiTimerTarget* pTarget);
	static void CancelTimer(GuiTimerId::GuiTimerIdType timerId);
	static void Initialize(void);
	static Boolean isActive(GuiTimerId::GuiTimerIdType timerId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	//
	// GuiTimerRecord -- private structure which defines elements
	// of TimerArray_[]
	//
	struct GuiTimerRecord_
	{
		GuiTimerRecord_(void);		// Never called but needed by the compiler
									// because MsgTimer below has no default
									// constructor so compiler expects this
									// struct to have one.
		MsgTimer 		msgTimer;	// timer object provided by Operating-Systems
									// Note: DO NOT MOVE, must be the first
									// definition in the structure.
		GuiTimerTarget*	pTarget;	// ptr to callback object, if any
		Boolean			isActive;	// TRUE when this is an active timer
		Uint16			stateMask;	// Allowable state for this timer to function.
	};

protected:

private:
	// these methods are purposely declared, but not implemented...
	GuiTimerRegistrar(void);						// not implemented...
	GuiTimerRegistrar(const GuiTimerRegistrar&);	// not implemented...
	~GuiTimerRegistrar(void);						// not implemented...
	void operator=(const GuiTimerRegistrar&);		// not implemented...

	//@ Data-Member: TimerArray_
	// A static array of GuiTimerRecord_'s, indexed by the
	// GuiTimerId::GuiTimerIdType enum.  Each array element contains a MsgTimer
	// object, a pointer to a registered callback object and a flag showing
	// whether the timer is currently active.
	static GuiTimerRecord_* TimerArray_;

	//@ Data-Member: IsDataChangedArray_
	// An array that parallels the CascadeArray_ and indicates which timer
	// items have been changed since the last call to TimerChangeHappened().
	static Int32 IsDataChangedArray_[2*GuiTimerId::NUM_TIMER_IDS];

	//@ Data-Member: NextArrayIndex_
	// An array index keeps tracks the next available array element
	// location for the future coming timer event.
	// data items have been changed since the last call to UpdateHappened().
	static Int32 NextArrayIndex_;
};

typedef GuiTimerRegistrar::GuiTimerRecord_ GuiTimerRecordInRegistrar;


#endif // GuiTimerRegistrar_HH 
