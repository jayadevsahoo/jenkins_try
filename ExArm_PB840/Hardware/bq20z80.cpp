#include "stdafx.h"
//#############################################################################
//
//  Bq20z80.cpp - Implementation file for Bq20z80_t class.
//
//
//#############################################################################

#include "bq20z80.h"

static const int RETRY_COUNT = 4;

static const uint16_t   MAX_EXTERNAL_BATTERY_CAPACITY = 10000;

//#define DUMP_DEBUG_SUMMARY  true
#define DUMP_DEBUG_SUMMARY  false


//#define DUMP_DEBUG_DETAIL  true
#define DUMP_DEBUG_DETAIL  false


//#############################################################################
//
//  Bq20z80_t::Bq20z80_t
//
//#############################################################################

Bq20z80_t::Bq20z80_t(HANDLE FileHandle, uint8_t BusAddress, I2cBaudIndex_t BaudIndex) : I2C(FileHandle, BusAddress, I2C_SUBADDRESS_MODE_1, BaudIndex)
{
}


//#############################################################################
//
//  Bq20z80_t::GetBatteryData - This reads the various battery data.
//
//#############################################################################

void Bq20z80_t::GetBatteryData(Bq20z80Data_t *BatteryData)
{
    TCHAR DebugOutput[120];

    int     RetryCounter = RETRY_COUNT;

    // Get state of charge
    DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("Get RelativeStateOfCharge\r\n")));
    BatteryData->ChargeValid = false;
    RetryCounter = RETRY_COUNT;

    while (RetryCounter-- > 0)
    {
        I2cReturnCode_t ReturnCode = ReadDevicePec(COMMAND_CODE_READ_RELATIVE_STATE_OF_CHARGE, &BatteryData->Charge);
        if (ReturnCode == I2C_SUCCESSFUL)
        {
            BatteryData->ChargeValid = true;
            break;
        }
        else
        {
            if (ReturnCode == I2C_PEC_ERROR)
            {
                DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("MUX 1 I2C_PEC_ERROR\r\n")));
            }
        }
        Sleep(300);
    }
    if (BatteryData->ChargeValid)
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Level: %d\r\n"), BatteryData->Charge));
    }
    else
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Level: N/A\r\n")));
    }




    // Get full charge capacity
    DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("Get FullChargeCapacity\r\n")));
    BatteryData->FullChargeCapacityValid = false;
    RetryCounter = RETRY_COUNT;

    while (RetryCounter-- > 0)
    {
        I2cReturnCode_t ReturnCode = ReadDevicePec(COMMAND_CODE_READ_FULL_CHARGE_CAPACITY, &BatteryData->FullChargeCapacity);
        if (ReturnCode == I2C_SUCCESSFUL)
        {
            BatteryData->FullChargeCapacityValid = true;
            break;
        }
        else
        {
            if (ReturnCode == I2C_PEC_ERROR)
            {
                DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("I2C_PEC_ERROR\r\n")));
            }
        }
        Sleep(300);
    }
    if (BatteryData->FullChargeCapacityValid && (BatteryData->FullChargeCapacity > MAX_EXTERNAL_BATTERY_CAPACITY))
    {
        BatteryData->FullChargeCapacityValid = false;
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Capacity OOR!\r\n")));
    }
    if (BatteryData->FullChargeCapacityValid)
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Capacity: %d\r\n"), BatteryData->FullChargeCapacity));
    }
    else
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Capacity: N/A\r\n")));
    }




    // Get Current
    DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("Get Current\r\n")));
    BatteryData->CurrentValid = false;
    RetryCounter = RETRY_COUNT;

    while (RetryCounter-- > 0)
    {
        I2cReturnCode_t ReturnCode = ReadDevicePec(COMMAND_CODE_READ_CURRENT, &BatteryData->Current);
        if (ReturnCode == I2C_SUCCESSFUL)
        {
            BatteryData->CurrentValid = true;
            break;
        }
        else
        {
            if (ReturnCode == I2C_PEC_ERROR)
            {
                DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("MUX I2C_PEC_ERROR\r\n")));
            }
        }
        Sleep(300);
    }
    if (BatteryData->CurrentValid)
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Current: %d\r\n"), BatteryData->Current));
    }
    else
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Current: N/A\r\n")));
    }




    static const int KELVIN_TO_CELSIUS_CONVERSION = 2732;
    static const int MAX_SIGNED_KELVIN_VALUE = 0x7fff;          // any temperature higher will not fit in our signed value

    // Get temperature
    DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("Get temperature")));
    BatteryData->BatteryTemperatureValid = false;
    RetryCounter = RETRY_COUNT;
    uint16_t KelvinBatteryTemperature;

    while (RetryCounter-- > 0)
    {
        I2cReturnCode_t ReturnCode = ReadDevicePec(COMMAND_CODE_READ_TEMPERATURE, &KelvinBatteryTemperature);
        if (ReturnCode == I2C_SUCCESSFUL)
        {
            if (KelvinBatteryTemperature > MAX_SIGNED_KELVIN_VALUE)
            {
                KelvinBatteryTemperature = MAX_SIGNED_KELVIN_VALUE;
            }
            BatteryData->BatteryTemperature = (int16_t)KelvinBatteryTemperature - KELVIN_TO_CELSIUS_CONVERSION;    // temp in tenths, celsius
            BatteryData->BatteryTemperatureValid = true;
            break;
        }
        else
        {
            if (ReturnCode == I2C_PEC_ERROR)
            {
                DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("MUX 2 I2C_PEC_ERROR\r\n")));
            }
        }
        Sleep(300);
    }
    if (BatteryData->BatteryTemperatureValid)
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Temperature: %d\r\n"), BatteryData->BatteryTemperature));
    }
    else
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Temperature: N/A\r\n")));
    }




    // Get battery status
    BatteryData->BatteryStatusValid = false;

    DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("Get BatteryStatus\r\n")));

    RetryCounter = RETRY_COUNT;

    while (RetryCounter-- > 0)
    {
        I2cReturnCode_t ReturnCode = ReadDevicePec(COMMAND_CODE_READ_BATTERY_STATUS, &BatteryData->BatteryStatus);
        if (ReturnCode == I2C_SUCCESSFUL)
        {
            BatteryData->BatteryStatusValid = true;
            break;
        }
        else
        {
            if (ReturnCode == I2C_PEC_ERROR)
            {
                DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("MUX 3 I2C_PEC_ERROR\r\n")));
            }
        }
        Sleep(300);
    }

    if (BatteryData->BatteryStatusValid)
    {
        _stprintf(DebugOutput, _T("Battery Status: 0x%04X"), BatteryData->BatteryStatus);
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("%s\r\n"), DebugOutput));
    }
    else
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Battery Status: N/A\r\n")));
    }



     // Get the average time to empty battery information
    BatteryData->BatteryTimeValid = false;

    DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("Get average time to empty battery information\r\n")));

    RetryCounter = RETRY_COUNT;

    while (RetryCounter-- > 0)
    {
        I2cReturnCode_t ReturnCode = ReadDevicePec(COMMAND_CODE_READ_BATTERY_TIME, &BatteryData->BatteryTime);
        if (ReturnCode == I2C_SUCCESSFUL)
        {
            BatteryData->BatteryTimeValid = true;
            break;
        }
        else
        {
            if (ReturnCode == I2C_PEC_ERROR)
            {
                DEBUGMSG(DUMP_DEBUG_DETAIL, (_T("MUX 4 I2C_PEC_ERROR\r\n")));
            }
        }
        Sleep(300);
    }

    if (BatteryData->BatteryTimeValid)
    {
        _stprintf(DebugOutput, _T("Battery remaining time: %d minutes"), BatteryData->BatteryTime);
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("%s\r\n"), DebugOutput));
    }
    else
    {
        DEBUGMSG(DUMP_DEBUG_SUMMARY, (_T("Battery remaining time: N/A\r\n")));
    }
}




