#include "stdafx.h"

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TestDataInfo -  Service Data Processing agent class
//			  for data type of service test result.
//---------------------------------------------------------------------
//@ Interface-Description
//
//>Von
//  Description:
//  ------------
//  The 'TestDataInfo' class processes data type of the service test result.
//  This object buffers the latest data within its double buffer
//  to maintain data integrity between a reader and a writer of the data.
//
//  Calss Key Methods:
//  -----------------
//     modifyData_  -- The method sends out a Service Item change notification.
//
//@ Rationale
//       The purpose of this class is to put all the procssing
//       smarts of a data service item type into one object to
//       avoid these smarts spreading all over the service data manager.
//
//---------------------------------------------------------------------
//@ Implementation-Description
//     The class is a subclass of the "TestInfo" class.
//     The parent class handles most of the double buffer indexing,
//     provides data access methods to outside and data update notice.
//     This class processes a raw service data value and maintains the
//	   modified data in its buffer.
//---------------------------------------------------------------------
//@ Fault-Handling
//    Follows the standard Sigma contract programming model, Class
//    precodition and Class assertion are used to raise a fatal software
//    fault. SAFE_CLASS_PRE_CONDITIONS are used only during the development
//    cycle for debuggin purpose.
//>Voff
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header::   /840/Baseline/Service-Data/vcssrc/TestDataInfo.ccv   10.7   08/17/07 10:34:58   pvcs
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  15-Jan-99   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' to a non-inlined method.
//
//  Revision: 001  By:  yyy    Date:  16-May-95   DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "TestDataInfo.hh"

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TestDataInfo
//
//@ Interface-Description
//      Initializes all the private and inherited member variables.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TestDataInfo::TestDataInfo(TestDataId::TestDataItemId id) :
		TestInfo(id)
{
  CALL_TRACE("TestDataInfo(TestDataId::TestDataItemId id)");
  SAFE_CLASS_PRE_CONDITION(ServiceDataMgr::IsTestItemId(id));

												// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TestDataInfo		  [Destructor]
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TestDataInfo::~TestDataInfo(void)
{
  CALL_TRACE("~TestDataInfo()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: modifyData_			 [virtual]
//
//@ Interface-Description
//	Processes a raw service data value and maintains the modified data
//  in its buffer.
//  The method informs other subsystems about the new data change for
//  this data type.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The modifyData_ method is called whenever a  new service data
//      is received from the network. The method puts its processed data
//      to one of the double service data buffer that is not active.
//      Data is put into the network buffer to avoid overriding
//      an active data in the middle of being accessed by other tasks.
//
//      The parent class's TestInfo::modifyData_ is called
//      to send data change notification and make the newly received data
//      data to be an active one.
//---------------------------------------------------------------------
//@ PreCondition
//	 ( pValue )
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
#include "MathUtilities.hh"
void
TestDataInfo::modifyData_( void *pValue)
{
	Real32 rawValue = *((Real32 *) pValue);

	SAFE_CLASS_PRE_CONDITION(pValue);

#if defined (FORNOW)
	cout << "TestDataInfo::modifyData_()::TI1" << endl;
#endif	// (FORNOW)

												// $[TI1]

#if defined (FORNOW)
printf("TestDataInfo::modifyData_() rawValue = %f, dataId_ = %d, index_.network = %d\n",
				rawValue, dataId_, index_.network);
#endif	// (FORNOW)

	// Assign the value
	data_[index_.network].testValueType.rValue = rawValue;

	TestInfo::modifyData_(pValue);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
TestDataInfo::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SERVICE_DATA,
			TEST_DATA_INFO, lineNumber, pFileName, pPredicate);
}


#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
