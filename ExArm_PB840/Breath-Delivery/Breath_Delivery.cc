#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: Breath_Delivery - SubSystem definitions
//---------------------------------------------------------------------
//@ Interface-Description
// This class provides a method to initialize all globally  referenced
// objects in the Breath_Delivery subsystem.  This method must be invoked upon
// completion of post, during system initialization.  Any BD objects that
// need additional initialization, other than the constructor, must provide
// an initialize method for this class to invoke.  This class also
// contains an enum for the id of each Breath-Delivery class.
// Since the class is used on both the BD and the GUI CPUs, preprocessor 
// directives are used to partition the code accordingly. 
//---------------------------------------------------------------------
//@ Rationale
// Breath delivery subsystem initialization is performed here, for both the 
// BD and the GUI CPUs.
// Each object is initialized at run time to control the order of execution.
// All objects are initialized here since no dynamic memory allocation is allowed.
//---------------------------------------------------------------------
//@ Implementation-Description
// All the references are initialized using their own constructors.
// Additional initializations are required for certain objects.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/Breath_Delivery.ccv   25.0.4.0   19 Nov 2013 13:59:42   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 050   By: rhj    Date: 07-May-2010    SCR Number: 6436  
//  Project:  PROX
//  Description:
//      Added Manual purge, and ProxManager class.
// 
//  Revision: 049   By: rpr    Date: 23-Jan-2009    SCR Number: 6435  
//  Project:  840S
//  Description:
//      Modified for code review comments.
// 
//  Revision: 048   By: rpr    Date: 17-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 047   By: rhj    Date: 07-July-2008   SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       Added net flow backup inspiratory trigger,
//       PED_MAX_FLOW_LEAK, and NEO_MAX_FLOW_LEAK.      
//       Added Leak Comp objects and initialization.
//       
//  Revision: 046   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM objects and initialization.
//
//  Revision: 045   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Define RPavPhase, RPavPausePhase, PavManager object.
//
//  Revision: 044  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//			Define RVolumeTargetedManager, RVolumeTargetedController,
//			RVtpcvPhase, RVsvPhase.
//
//  Revision: 043  By: syw   Date:  21-Mar-2000    DR Number: 5611
//  Project:  NeoMode
//	Description:
//		Eliminate SvSwitchSideRangeTest class.
//
//  Revision: 042  By: syw     Date:  09-Mar-2000    DR Number: 5684
//  Project:  NeoMode
//  Description:
//		Added arguments to VolumeController and FlowController constructor
//		calls.
//
//  Revision: 041  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added neonatal-specific maximum flow value
//
//  Revision: 040  By: yyy     Date:  5-May-1999    DR Number: 5367
//  Project:  ATC
//  Description:
//      Use same pause trigger for both inspiration and expiration
//		pause handles.
//
//  Revision: 039  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added TC required triggers and objects.
//
//  Revision: 038  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//		ATC initial version.
//      Changed 'SoftFault()' method to a non-inlined method.  Eliminate
//		ExhValveEController class.
//
//  Revision: 037  By:  syw    Date:  05-Jan-1998    DR Number: none
//       Project:  BiLevel (840)
//       Description:
//  		Changed IMMEDIATE_PEEP_REDUCTION to PEEP_REDUCTION_EXP.
//
//  Revision: 036  By:  syw    Date:  18-Dec-1997    DR Number: none
//       Project:  BiLevel (840)
//       Description:
//		 	BiLevel initial version.
//          Added RBiLevelScheduler, RBiLevelLowToHighInsp,	RBiLevelCycleTimer,
//          InspiratoryPausePhase, LowToHighPeepPhase, InspiratoryPauseEvent,
//			HiLevelPsvPhase, BiLevelSpontPausePhase.
//
//  Revision: 035  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 034  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 033  By:  iv     Date:  15-Apr-1997    DR Number: DCS 589,
//                                                              1631, 1638
//       Project:  Sigma (840)
//       Description:
//       	These DCSs were implemented by this file.
//
//  Revision: 032  By:  syw    Date:  07-Apr-1997    DR Number: None 
//       Project:  Sigma (840)
//       Description:
//       	Eliminate SIGMA_COMMON as specified by formal code review.
//
//  Revision: 031  By:  by    Date:  16-Mar-1997    DR Number: None 
//       Project:  Sigma (840)
//       Description:
//             Changed all instances of SVLoopbackRangeTest to
//             SvClosedLoopbackTest as specified by formal code review.
//
//  Revision: 030  By:  by    Date:  11-Feb-1997    DR Number: 1620 
//       Project:  Sigma (840)
//       Description:
//             Removed the construction of AtmPressureRangeTest object
//             since the test is moved to Sensor object updateValue method.
//
//  Revision: 029  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 028  By:  sp    Date:  04-Feb-1997    DR Number: DCS 600
//       Project:  Sigma (840)
//       Description:
//             Code clean up (For breath misc stuff).
//
//  Revision: 027  By:  iv    Date:  14-Jan-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 026  By:  sp    Date:  9-Jan-1997    DR Number: DCS 1339
//       Project:  Sigma (R8027)
//       Description:
//             Add HighCircuitPressureInspTrigger.
//
//  Revision: 025  By:  iv    Date:  20-Dec-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 024  By:  iv    Date:  20-Dec-1996    DR Number: DCS 1297
//       Project:  Sigma (R8027)
//       Description:
//             The handling of 100 percent O2 request was moved to another file.
//
//  Revision: 023  By:  iv    Date:  11-Nov-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Added a call: BdSystemStateHandler::UpdateNovRam() to update
//             NovRam on short power interruption when 100% O2 is not requested.
//             Cleanup for code inspections.
//
//  Revision: 022  By:  iv    Date:  11-Nov-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed the callback method for rGuiFaultEvent instance of UserEvent.
//
//  Revision: 021  By:  iv    Date:  06-Nov-1996    DR Number: DCS 1338
//       Project:  Sigma (R8027)
//       Description:
//             Added alarm events BdAlarmId::BDALARM_100_PERCENT_O2_REQUEST
//             and BdAlarmId::BDALARM_NOT_100_PERCENT_O2_REQUEST.
//             Deleted BdAlarmId::BDALARM_O2_PERCENT_SETTING_CHANGE.
//
//  Revision: 020  By:  iv    Date:  08-Oct-1996    DR Number: DCS 1395
//       Project:  Sigma (R8027)
//       Description:
//             Added rGuiFaultEvent instance of UserEvent
//
//  Revision: 019  By:  sp    Date:  26-Sep-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changed PeepRecoveryTrigger to PeepRecoveryInspTrigger.
//
//  Revision: 018 By: iv   Date:   15-Sep-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//			Post a message on the BdStatus task to specify the ACTIVE status
//          of the 100 percent O2.
//          9-17-1996 - undid this change since it does not preserve the
//          100% O2 request in memory. 
//
//  Revision: 017 By: iv   Date:   07-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//			Added a peep recovery trigger object
//
//  Revision: 016 By: iv   Date:   08-Aug-1996    DR Number: DCS 600, 811
//       Project:  Sigma (R8027)
//       Description:
//			Fixed initialization of 100% O2, removed managing of apnea
//          interval during startup.
//
//  Revision: 015 By: iv   Date:   07-Jul-1996    DR Number: DCS 1116
//       Project:  Sigma (R8027)
//       Description:
//			Added a call to calculate BTPS factor: Btps::CalculateBtps();
//
//  Revision: 014 By: syw   Date:  05-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//			Added rCalInfoFlashBlock.initialize() call to Iniitialize().
//
//  Revision: 013 By: syw   Date:  14-May-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//			remove XmitExhValveTable.
//
//  Revision: 012 By: syw   Date:  06-May-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//			Added rPeepRecoveryPhase instance.
//
//  Revision: 011 By: quf   Date:  18-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//			Removed factor of 100 from downTimeDuration due to change in POST
//
//  Revision: 010 By:  iv    Date:  15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//          Deleted UserEvent& rO2CalibrationRequest
//
//  Revision: 009 By: syw    Date: 10-Apr-1996   DR Number: DCS 669
//  	Project:  Sigma (R8027)
//		Description:
//			Removed RELIEF_FLOW_TARGET... no longer used.
//
//  Revision: 008 By: syw   Date:  15-Mar-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//			Multiply PostAgent::GetDowntimeDuration() by 100 to get time to msecs.
//
//  Revision: 007 By: syw   Date:  12-Mar-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//			Added call to XmitExhValveTable::Initialize().
//
//  Revision: 006 By: syw   Date:  26-Feb-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//				Added rExhHeaterController object.
//
//  Revision: 005 By:  iv   Date:  12-Feb-1996    DR Number: DCS 600, 674
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated the PiPeDriftTest, added initialization for the
//             SystemBattery class.
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 004 By:  iv   Date:  08-Feb-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//             Changed the initialization of rApneaTimer and rO2Timer from
//             restart() to start().
//
//  Revision: 003 By:  kam   Date:  06-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with GUI for Vent Status and
//             User Events.
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#if defined(SIGMA_BD_CPU) || defined(SIGMA_UNIT_TEST)

#include "Breath_Delivery.hh"

//@ Usage-Classes

extern Boolean BdTimerEnabled;

#  include "ExhHeaterController.hh"
#  include "VolumeController.hh"
#  include "PressureController.hh"
#  include "ExhValveIController.hh"
#  include "PidPressureController.hh"
#  include "PeepController.hh"
#  include "VolumeTargetedController.hh"
#  include "StartupPhase.hh"
#  include "ExhalationPhase.hh"
#  include "ExpiratoryPausePhase.hh"
#  include "InspiratoryPausePhase.hh"
#  include "NifPausePhase.hh"
#  include "P100PausePhase.hh"
#  include "PavPausePhase.hh"
#  include "InspPauseManeuver.hh"
#  include "ExpPauseManeuver.hh"
#  include "NifManeuver.hh"
#  include "P100Maneuver.hh"
#  include "VitalCapacityManeuver.hh"
#  include "VcvPhase.hh"
#  include "PcvPhase.hh"
#  include "VtpcvPhase.hh"
#  include "PsvPhase.hh"
#  include "VsvPhase.hh"
#  include "TcvPhase.hh"
#  include "PavPhase.hh"
#  include "LowToHighPeepPhase.hh"
#  include "HiLevelPsvPhase.hh"
#  include "PeepRecoveryPhase.hh"
#  include "DisconnectPhase.hh"
#  include "SafeStatePhase.hh"
#  include "SafetyValveClosePhase.hh"
#  include "OscPcvPhase.hh"
#  include "OscBiLevelPhase.hh"
#  include "OscExhPhase.hh"
#  include "VcmPsvPhase.hh"
#  include "AcScheduler.hh"
#  include "SpontScheduler.hh"
#  include "SimvScheduler.hh"
#  include "BiLevelScheduler.hh"
#  include "BiLevelLowToHighInspTrigger.hh"
#  include "BiLevelHighToLowExpTrigger.hh"
#  include "OscScheduler.hh"
#  include "PowerupScheduler.hh"
#  include "StandbyScheduler.hh"
#  include "DisconnectScheduler.hh"
#  include "ApneaScheduler.hh"
#  include "SvoScheduler.hh"
#  include "SafetyPcvScheduler.hh"
#  include "BreathSet.hh"
#  include "BreathData.hh"
#  include "BdSystemStateHandler.hh"
#  include "UiEvent.hh"
#  include "ApneaInterval.hh"
#  include "O2Mixture.hh"
#  include "SmSwitchConfirmation.hh"
#  include "Peep.hh"
#  include "PressureXducerAutozero.hh"
#  include "IntervalTimer.hh"
#  include "TimerMediator.hh"
#  include "AsapInspTrigger.hh"
#  include "SimvTimeInspTrigger.hh"
#  include "TimerBreathTrigger.hh"
#  include "DeliveredFlowExpTrigger.hh"
#  include "HighCircuitPressureExpTrigger.hh"
#  include "HighCircuitPressureInspTrigger.hh"
#  include "HighVentPressureExpTrigger.hh"
#  include "ImmediateBreathTrigger.hh"
#  include "NetFlowInspTrigger.hh"
#  include "P100Trigger.hh"
#  include "OperatorInspTrigger.hh"
#  include "PeepRecoveryInspTrigger.hh"
#  include "PressureExpTrigger.hh"
#  include "PressureInspTrigger.hh"
#  include "PressureSetInspTrigger.hh"
#  include "PausePressTrigger.hh"
#  include "PressureSvcTrigger.hh"
#  include "OscTimeInspTrigger.hh"
#  include "BreathTriggerMediator.hh"
#  include "TimerModeTrigger.hh"
#  include "ApneaAutoResetTrigger.hh"
#  include "DisconnectTrigger.hh"
#  include "DiscAutoResetTrigger.hh"
#  include "ImmediateModeTrigger.hh"
#  include "OcclusionTrigger.hh"
#  include "SvoResetTrigger.hh"
#  include "SvoTrigger.hh"
#  include "VentSetupCompleteTrigger.hh"
#  include "LungFlowExpTrigger.hh"
#  include "LungVolumeExpTrigger.hh"
#  include "HighPressCompExpTrigger.hh"
#  include "ModeTriggerMediator.hh"
#  include "CircuitCompliance.hh"
#  include "Waveform.hh"
#  include "AveragedBreathData.hh"
#  include "LungData.hh"
#  include "DynamicMechanics.hh"
#  include "PavManager.hh"
#  include "VolumeTargetedManager.hh"
#  include "Tube.hh"
#  include "BdAlarms.hh"
#  include "PiStuckTest.hh"
#  include "PeStuckTest.hh"
#  include "O2PsolStuckOpenTest.hh"
#  include "AirPsolStuckOpenTest.hh"
#  include "SvClosedLoopbackTest.hh"
#  include "ExhFlowSensorFlowTest.hh"
#  include "SafetyNetTestMediator.hh"
#  include "BD_IO_Devices.hh"
#  include "MainSensorRefs.hh"
#  include "MiscSensorRefs.hh"
#  include "VentObjectRefs.hh"
#  include "BDIORefs.hh"
#  include "ValveRefs.hh"
#  include "IpcIds.hh"
#  include "SafetyNetRefs.hh"
#  include "GasSupply.hh"
#  include "Compressor.hh"
#  include "Fio2Monitor.hh"
#  include "PowerSource.hh"
#  include "SystemBattery.hh"
#  include "VentAndUserEventStatus.hh"
#  include "Btps.hh"
#  include "NovRamManager.hh"
#  include "AirFlowSensorTest.hh"
#  include "O2FlowSensorTest.hh"
#  include "InspPressureSensorTest.hh"
#  include "ExhPressureSensorTest.hh"
#  include "AirPsolStuckTest.hh"
#  include "O2PsolStuckTest.hh"
#  include "SvOpenedLoopbackTest.hh"
#  include "Post.hh"
#  include "LeakCompMgr.hh"
#  include "ProxManager.hh"
#endif // defined(SIGMA_BD_CPU) || defined(SIGMA_UNIT_TEST)

#if defined(SIGMA_GUI_CPU) || defined(SIGMA_UNIT_TEST)
#  include "BdGuiEvent.hh"
#endif // defined(SIGMA_GUI_CPU) || defined(SIGMA_UNIT_TEST)

#include "CalInfoFlashBlock.hh"
#include "CalInfoRefs.hh"

//@ End-Usage

#ifdef SIGMA_DEBUG
 
#include "Ostream.hh"
 
#endif //SIGMA_DEBUG

#if defined(SIGMA_BD_CPU) || defined(SIGMA_UNIT_TEST)
//@ Code...
const Real32 BASE_FLOW_MIN = 1.50F;			// LPM $[04194]
const Real32 ADULT_MAX_FLOW = 200.0F; 			// LPM $[04075]
const Real32 PED_MAX_FLOW = 80.0F; 			// LPM $[04075]
const Real32 NEO_MAX_FLOW = 30.0F;			// LPM $[04075]
const Real32 PRESSURE_TO_SEAL = 40.0F; 			// cmH2O $[04165]
const Real32 PRESSURE_ABOVE_HCP = 5.0F; 		// cmH2O $[04166]
const Real32 BACKUP_PRESSURE_SENS_VALUE = 2.0F; // cmH2O $[04008]
const Int32 MAX_INSP_TIME_MS = 8000 ;   		// millisecond $[02172]
const Int32 SV_CLOSE_TIME_MS = 500 ;   			// millisecond $[04205]
const DiscreteValue NULL_MANDATORY_TYPE = 32767; // MAX_INT16_VALUE; 
const Real32 PED_MAX_FLOW_LEAK = 120.0F; 	     // LPM $[LC24004]
const Real32 NEO_MAX_FLOW_LEAK = 50.0F;			 // LPM $[LC24004]
const Real32 PURGE_FLOW = 1.0F ;                 // LPM $[04310]

//====================================================================
//
//   // Global member initialization
//
// The memory blocks are defined as globals (not static) to make them
// available in the executable map file.
//====================================================================

 
/////////////////////////////////////////////////////////////////////////
// o2 volume controller

Uint 
    PO2VolumeController[(sizeof(VolumeController) + sizeof(Uint) - 1) 
    / sizeof(Uint)];
// Alias a volume controller reference to that block of memory
VolumeController& RO2VolumeController = 
	*((VolumeController*) PO2VolumeController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// air volume controller

Uint
	PAirVolumeController[(sizeof(VolumeController) + sizeof(Uint) -1)
	/ sizeof(Uint)];
// Alias a volume controller reference to that block of memory
VolumeController& RAirVolumeController = 
	*((VolumeController*) PAirVolumeController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// O2 Flow Controller

Uint
	PO2FlowController[(sizeof(FlowController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a Flow controller reference to a block of memory
FlowController& RO2FlowController = 
	*((FlowController*) PO2FlowController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Air Flow Controller

Uint
	PAirFlowController[(sizeof(FlowController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a Flow controller reference to a block of memory
FlowController& RAirFlowController = 
	*((FlowController*) PAirFlowController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pressure Controller

Uint
	PPressureController[(sizeof(PressureController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PressureController reference to a block of memory
PressureController& RPressureController = 
	*((PressureController*) PPressureController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Exhalation Valve Controller in inspiration phase

Uint
	PExhValveIController[(sizeof(ExhValveIController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an ExhValveIController reference to a block of memory
ExhValveIController& RExhValveIController = 
	*((ExhValveIController*) PExhValveIController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Proportional, Integral plus Derivative Controller (Pid)

Uint
	PPidPressureController[(sizeof(PidPressureController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an PidPressureController reference to a block of memory
PidPressureController& RPidPressureController = 
	*((PidPressureController*) PPidPressureController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Peep Controller

Uint
	PPeepController[(sizeof(PeepController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PeepController reference to a block of memory
PeepController& RPeepController = 
	*((PeepController*) PPeepController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Volume Targeted Controller

Uint
	PVolumeTargetedController[(sizeof(VolumeTargetedController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a VolumeTargetedController reference to a block of memory
VolumeTargetedController& RVolumeTargetedController = 
	*((VolumeTargetedController*) PVolumeTargetedController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Exhalation Heater Controller

Uint
	PExhHeaterController[(sizeof(ExhHeaterController) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ExhHeaterController reference to a block of memory
ExhHeaterController& RExhHeaterController = 
	*((ExhHeaterController*) PExhHeaterController);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Exhalation Phase

Uint
	PExhalationPhase[(sizeof(ExhalationPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an ExhalationPhase to a block of memory
ExhalationPhase& RExhalationPhase = 
	*((ExhalationPhase*) PExhalationPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Expiratory Pause Phase

Uint
	PExpiratoryPausePhase[(sizeof(ExpiratoryPausePhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ExpiratoryPausePhase to a block of memory
ExpiratoryPausePhase& RExpiratoryPausePhase = 
	*((ExpiratoryPausePhase*) PExpiratoryPausePhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Inspiratory Pause Phase

Uint
	PInspiratoryPausePhase[(sizeof(InspiratoryPausePhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a InspiratoryPausePhase to a block of memory
InspiratoryPausePhase& RInspiratoryPausePhase = 
	*((InspiratoryPausePhase*) PInspiratoryPausePhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// NIF Pause Phase

Uint
	PNifPausePhase[(sizeof(NifPausePhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a NifPausePhase to a block of memory
NifPausePhase& RNifPausePhase = 
	*((NifPausePhase*) PNifPausePhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// P100 Pause Phase

Uint
	PP100PausePhase[(sizeof(P100PausePhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a P100PausePhase to a block of memory
P100PausePhase& RP100PausePhase = 
	*((P100PausePhase*) PP100PausePhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pav Pause Phase

Uint
	PPavPausePhase[(sizeof(PavPausePhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PavPausePhase to a block of memory
PavPausePhase& RPavPausePhase = 
	*((PavPausePhase*) PPavPausePhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Bilevel Spont Pause Phase

Uint
	PBiLevelSpontPausePhase[(sizeof(InspiratoryPausePhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a BiLevelSpontPausePhase to a block of memory
InspiratoryPausePhase& RBiLevelSpontPausePhase = 
	*((InspiratoryPausePhase*) PBiLevelSpontPausePhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Inspiratory Pause Maneuver

Uint
	PInspPauseManeuver[(sizeof(InspPauseManeuver) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a InspPauseManeuver to a block of memory
InspPauseManeuver& RInspPauseManeuver = 
	*((InspPauseManeuver*) PInspPauseManeuver);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Expiratory Pause Maneuver

Uint
	PExpPauseManeuver[(sizeof(ExpPauseManeuver) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ExpPauseManeuver to a block of memory
ExpPauseManeuver& RExpPauseManeuver = 
	*((ExpPauseManeuver*) PExpPauseManeuver);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// NIF Maneuver

Uint
	PNifManeuver[(sizeof(NifManeuver) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a NifManeuver to a block of memory
NifManeuver& RNifManeuver = 
	*((NifManeuver*) PNifManeuver);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// P100 Maneuver

Uint
	PP100Maneuver[(sizeof(P100Maneuver) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a P100Maneuver to a block of memory
P100Maneuver& RP100Maneuver = 
	*((P100Maneuver*) PP100Maneuver);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// VitalCapacity Maneuver

Uint
	PVitalCapacityManeuver[(sizeof(VitalCapacityManeuver) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a VitalCapacityManeuver to a block of memory
VitalCapacityManeuver& RVitalCapacityManeuver = 
	*((VitalCapacityManeuver*) PVitalCapacityManeuver);
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
// VCV Phase

Uint
	PVcvPhase[(sizeof(VcvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a VcvPhase to a block of memory
VcvPhase& RVcvPhase =
	*((VcvPhase*) PVcvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Apnea VCV Phase

Uint
	PApneaVcvPhase[(sizeof(VcvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a VcvPhase to a block of memory
VcvPhase& RApneaVcvPhase =
	*((VcvPhase*) PApneaVcvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Startup Phase

Uint
	PStartupPhase[(sizeof(StartupPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a StartupPhase to a block of memory
StartupPhase& RStartupPhase =
	*((StartupPhase*) PStartupPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// PCV Phase

Uint
	PPcvPhase[(sizeof(PcvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PcvPhase to a block of memory
PcvPhase& RPcvPhase =
	*((PcvPhase*) PPcvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Apnea PCV Phase

Uint
	PApneaPcvPhase[(sizeof(PcvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PcvPhase to a block of memory
PcvPhase& RApneaPcvPhase =
	*((PcvPhase*) PApneaPcvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// VTPCV (VC+) Phase

Uint
	PVtpcvPhase[(sizeof(VtpcvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a VtpcvPhase to a block of memory
VtpcvPhase& RVtpcvPhase =
	*((VtpcvPhase*) PVtpcvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// PSV Phase

Uint
	PPsvPhase[(sizeof(PsvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PsvPhase to a block of memory
PsvPhase& RPsvPhase = 
	*((PsvPhase*) PPsvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// VSV Phase

Uint
	PVsvPhase[(sizeof(VsvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a VsvPhase to a block of memory
VsvPhase& RVsvPhase = 
	*((VsvPhase*) PVsvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// TCV Phase

Uint
	PTcvPhase[(sizeof(TcvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a TcvPhase to a block of memory
TcvPhase& RTcvPhase = 
	*((TcvPhase*) PTcvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// PAV Phase

Uint
	PPavPhase[(sizeof(PavPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PavPhase to a block of memory
PavPhase& RPavPhase = 
	*((PavPhase*) PPavPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Low To High Peep Phase

Uint
	PLowToHighPeepPhase[(sizeof(LowToHighPeepPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a LowToHighPeepPhase to a block of memory
LowToHighPeepPhase& RLowToHighPeepPhase = 
	*((LowToHighPeepPhase*) PLowToHighPeepPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// High Level Psv Phase

Uint
	PHiLevelPsvPhase[(sizeof(HiLevelPsvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a HiLevelPsvPhase to a block of memory
HiLevelPsvPhase& RHiLevelPsvPhase = 
	*((HiLevelPsvPhase*) PHiLevelPsvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Peep Recovery Phase

Uint
	PPeepRecoveryPhase[(sizeof(PeepRecoveryPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PeepRecoveryPhase to a block of memory
PeepRecoveryPhase& RPeepRecoveryPhase = 
	*((PeepRecoveryPhase*) PPeepRecoveryPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// DisconnectPhase Phase

Uint
	PDisconnectPhase[(sizeof(DisconnectPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a DisconnectPhase to a block of memory
DisconnectPhase& RDisconnectPhase = 
	*((DisconnectPhase*) PDisconnectPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SafeStatePhase Phase

Uint
	PSafeStatePhase[(sizeof(SafeStatePhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a SafeStatePhase to a block of memory
SafeStatePhase& RSafeStatePhase = 
	*((SafeStatePhase*) PSafeStatePhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SafetyValveClosePhase Phase

Uint
	PSafetyValveClosePhase[(sizeof(SafetyValveClosePhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a SafetyValveClosePhase to a block of memory
SafetyValveClosePhase& RSafetyValveClosePhase = 
	*((SafetyValveClosePhase*) PSafetyValveClosePhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// OscPcvPhase Phase

Uint
	POscPcvPhase[(sizeof(OscPcvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a OscPcvPhase to a block of memory
OscPcvPhase& ROscPcvPhase = 
	*((OscPcvPhase*) POscPcvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// OscBiLevelPhase Phase

Uint
	POscBiLevelPhase[(sizeof(OscBiLevelPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a OscBiLevelPhase to a block of memory
OscBiLevelPhase& ROscBiLevelPhase = 
	*((OscBiLevelPhase*) POscBiLevelPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// OscExhPhase Phase

Uint
	POscExhPhase[(sizeof(OscExhPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a OscExhPhase to a block of memory
OscExhPhase& ROscExhPhase = 
	*((OscExhPhase*) POscExhPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Vital Capacity Maneuver PSV Phase

Uint
	PVcmPsvPhase[(sizeof(VcmPsvPhase) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a VcmPsvPhase to a block of memory
VcmPsvPhase& RVcmPsvPhase = 
	*((VcmPsvPhase*) PVcmPsvPhase);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// AC Scheduler

Uint
	PAcScheduler[(sizeof(AcScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a AcScheduler to a block of memory
AcScheduler& RAcScheduler = 
	*((AcScheduler*) PAcScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Spont Scheduler

Uint
	PSpontScheduler[(sizeof(SpontScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a SpontScheduler to a block of memory
SpontScheduler& RSpontScheduler = 
	*((SpontScheduler*) PSpontScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Disconnect Scheduler 

Uint
	PDisconnectScheduler[(sizeof(DisconnectScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a Disconnect Scheduler to a block of memory
DisconnectScheduler& RDisconnectScheduler =
	*((DisconnectScheduler*) PDisconnectScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Occlusion Scheduler 

Uint
	POscScheduler[(sizeof(OscScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an Occlusion Scheduler to a block of memory
OscScheduler& 	ROscScheduler = 
	*((OscScheduler*) POscScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Simv Scheduler 

Uint
	PSimvScheduler[(sizeof(SimvScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a Simv Scheduler to a block of memory
SimvScheduler& 	RSimvScheduler = 
	*((SimvScheduler*) PSimvScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// BiLevel Scheduler 

Uint
	PBiLevelScheduler[(sizeof(BiLevelScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a BiLevel Scheduler to a block of memory
BiLevelScheduler& 	RBiLevelScheduler = 
	*((BiLevelScheduler*) PBiLevelScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Apnea Scheduler 

Uint
	PApneaScheduler[(sizeof(ApneaScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a Apnea Scheduler to a block of memory
ApneaScheduler& 	RApneaScheduler = 
	*((ApneaScheduler*) PApneaScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Svo Scheduler 

Uint
	PSvoScheduler[(sizeof(SvoScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a Svo Scheduler to a block of memory
SvoScheduler& 	RSvoScheduler = 
	*((SvoScheduler*) PSvoScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SafetyPcv Scheduler 

Uint
	PSafetyPcvScheduler[(sizeof(SafetyPcvScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a SafetyPcv Scheduler to a block of memory
SafetyPcvScheduler& 	RSafetyPcvScheduler = 
	*((SafetyPcvScheduler*) PSafetyPcvScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Power Up Scheduler

Uint
	PPowerupScheduler[(sizeof(PowerupScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a  PowerupScheduler to a block of memory
PowerupScheduler& RPowerupScheduler = 
	*((PowerupScheduler*) PPowerupScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Standby Scheduler

Uint
	PStandbyScheduler[(sizeof(StandbyScheduler) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a StandbyScheduler to a block of memory
StandbyScheduler& RStandbyScheduler = 
	*((StandbyScheduler*) PStandbyScheduler);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Breath Set

Uint
	PBreathSet[(sizeof(BreathSet) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a BreathSet to a block of memory
BreathSet& RBreathSet = 
	*((BreathSet*) PBreathSet);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Breath Data

Uint
	PBreathData[(sizeof(BreathData) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference BreathData to a block of memory
BreathData& RBreathData = 
	*((BreathData*) PBreathData);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Manual Inspiration Event

Uint
	PManualInspEvent[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RManualInspEvent = 
	*((UiEvent*) PManualInspEvent);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Expiratory Pause Event

Uint
	PExpiratoryPauseEvent[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RExpiratoryPauseEvent = 
	*((UiEvent*) PExpiratoryPauseEvent);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Inspiratory Pause Event

Uint
	PInspiratoryPauseEvent[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RInspiratoryPauseEvent = 
	*((UiEvent*) PInspiratoryPauseEvent);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// NIF Maneuver Event

Uint
	PNifManeuverEvent[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RNifManeuverEvent = 
	*((UiEvent*) PNifManeuverEvent);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// P100 Maneuver Event

Uint
	PP100ManeuverEvent[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RP100ManeuverEvent = 
	*((UiEvent*) PP100ManeuverEvent);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SVC Maneuver Event

Uint
	PVitalCapacityManeuverEvent[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RVitalCapacityManeuverEvent = 
	*((UiEvent*) PVitalCapacityManeuverEvent);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// O2 Request Event

Uint
	PO2Request[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RO2Request = 
	*((UiEvent*) PO2Request);
/////////////////////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////////////////////
// Cal O2 Request Event

Uint
	PCALO2Request[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RCalO2Request = 
	*((UiEvent*) PCALO2Request);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Service Request Event

Uint
	PServiceRequest[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RServiceRequest = 
	*((UiEvent*) PServiceRequest);
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
// Purge Request Event

Uint
	PPurgeRequest[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RPurgeRequest = 
	*((UiEvent*) PPurgeRequest);
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
// Alarm Reset Event

Uint
	PAlarmResetEvent[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RAlarmResetEvent = 
	*((UiEvent*) PAlarmResetEvent);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SST Exit to Online Event

Uint
	PSstExitToOnlineEvent[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RSstExitToOnlineEvent =
	*((UiEvent*) PSstExitToOnlineEvent);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Gui Fault Event

Uint
	PGuiFaultEvent[(sizeof(UiEvent) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference UiEvent to a block of memory
UiEvent& RGuiFaultEvent = 
	*((UiEvent*) PGuiFaultEvent);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Apnea Interval

Uint
	PApneaInterval[(sizeof(ApneaInterval) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference ApneaInterval to a block of memory
ApneaInterval& RApneaInterval = 
	*((ApneaInterval*) PApneaInterval);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// O2 Mixture

Uint
	PO2Mixture[(sizeof(O2Mixture) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference O2Mixture to a block of memory
O2Mixture& RO2Mixture = 
	*((O2Mixture*) PO2Mixture);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SST Confirmation

Uint
	PSmSwitchConfirmation[(sizeof(SmSwitchConfirmation) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference SmSwitchConfirmation to a block of memory
SmSwitchConfirmation& RSmSwitchConfirmation = 
	*((SmSwitchConfirmation*) PSmSwitchConfirmation);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Peep

Uint
	PPeep[(sizeof(Peep) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference Peep to a block of memory
Peep& RPeep = 
	*((Peep*) PPeep);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pressure Transducer Autozero

Uint
	PPressureXducerAutozero[(sizeof(PressureXducerAutozero) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference PressureXducerAutozero to a block of memory
PressureXducerAutozero& RPressureXducerAutozero = 
	*((PressureXducerAutozero*) PPressureXducerAutozero);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// BD Alarms

Uint
	PBdAlarms[(sizeof(BdAlarms) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference BdAlarms to a block of memory
BdAlarms& RBdAlarms = 
	*((BdAlarms*) PBdAlarms);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Apnea Timer

Uint
	PApneaTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RApneaTimer = 
	*((IntervalTimer*) PApneaTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Backup Time Expiration Timer

Uint
	PBackupTimeExpTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RBackupTimeExpTimer = 
	*((IntervalTimer*) PBackupTimeExpTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Simv Inspiration Timer

Uint
	PSimvInspTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RSimvInspTimer = 
	*((IntervalTimer*) PSimvInspTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Simv Cycle Timer

Uint
	PSimvCycleTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RSimvCycleTimer = 
	*((IntervalTimer*) PSimvCycleTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// BiLevel Cycle Timer

Uint
	PBiLevelCycleTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RBiLevelCycleTimer = 
	*((IntervalTimer*) PBiLevelCycleTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Inspiration Timer

Uint
	PInspTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RInspTimer = 
	*((IntervalTimer*) PInspTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Timeout Timer for pause

Uint
	PPauseTimeoutTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RPauseTimeoutTimer =
	*((IntervalTimer*) PPauseTimeoutTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// 100% O2 timer

Uint
	PO2Timer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RO2Timer =
	*((IntervalTimer*) PO2Timer);
/////////////////////////////////////////////////////////////////////////
// 
/////////////////////////////////////////////////////////////////////////
// O2 Mix timer

Uint 
	PO2MixTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RO2MixTimer =
	*((IntervalTimer*) PO2MixTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pressure Transducer Autozero timer

Uint
	PPressureXducerAutozeroTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RPressureXducerAutozeroTimer =
	*((IntervalTimer*) PPressureXducerAutozeroTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Osc Insp timer

Uint
	POscInspTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& ROscInspTimer =
	*((IntervalTimer*) POscInspTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Osc Backup Insp timer

Uint
	POscBackupInspTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& ROscBackupInspTimer =
	*((IntervalTimer*) POscBackupInspTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Safety Valve Closed timer - indicates time to start SVC

Uint
	PSvcTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RSvcTimer =
        *((IntervalTimer*) PSvcTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Average Breath Data timer

Uint
	PAvgBreathDataTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RAvgBreathDataTimer =
        *((IntervalTimer*) PAvgBreathDataTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SST confirmation Timer

Uint
	PSmSwitchConfirmationTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RSmSwitchConfirmationTimer =
	*((IntervalTimer*) PSmSwitchConfirmationTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Inspiratory Pause Maneuver Timer

Uint
	PInspPauseManeuverTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RInspPauseManeuverTimer = 
	*((IntervalTimer*) PInspPauseManeuverTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Expiratory Pause Maneuver Timer

Uint
	PExpPauseManeuverTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RExpPauseManeuverTimer = 
	*((IntervalTimer*) PExpPauseManeuverTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// NIF Maneuver Timer

Uint
	PNifManeuverTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RNifManeuverTimer = 
	*((IntervalTimer*) PNifManeuverTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// P100 Maneuver Timer

Uint
	PP100ManeuverTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RP100ManeuverTimer = 
	*((IntervalTimer*) PP100ManeuverTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Vital Capacity Maneuver Timer

Uint
	PVitalCapacityManeuverTimer[(sizeof(IntervalTimer) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference IntervalTimer to a block of memory
IntervalTimer& RVitalCapacityManeuverTimer = 
	*((IntervalTimer*) PVitalCapacityManeuverTimer);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Timer Mediator 

Uint
	PTimerMediator[(sizeof(TimerMediator) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference TimerMediator to a block of memory
TimerMediator& RTimerMediator =
	*((TimerMediator*) PTimerMediator);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Asap Inspiration Trigger

Uint
	PAsapInspTrigger[(sizeof(AsapInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference AsapInspTrigger to a memory block
AsapInspTrigger& RAsapInspTrigger = 
	*((AsapInspTrigger*) PAsapInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Backup Time Exp Trigger

Uint
	PBackupTimeExpTrigger[(sizeof(TimerBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference TimerBreathTrigger to a block of memory
TimerBreathTrigger& RBackupTimeExpTrigger = 
	*((TimerBreathTrigger*) PBackupTimeExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SIMV Time Inspiration Trigger

Uint
	PSimvTimeInspTrigger[(sizeof(SimvTimeInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference SimvTimeInspTrigger to a memory block
SimvTimeInspTrigger& RSimvTimeInspTrigger = 
	*((SimvTimeInspTrigger*) PSimvTimeInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Time Inspiration Trigger

Uint
	PTimeInspTrigger[(sizeof(TimerBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference TimerBreathTrigger to a memory block
TimerBreathTrigger& RTimeInspTrigger = 
	*((TimerBreathTrigger*) PTimeInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pause Time Out Trigger

Uint
	PPauseTimeoutTrigger[(sizeof(TimerBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference TimerBreathTrigger to a memory block
TimerBreathTrigger& RPauseTimeoutTrigger = 
	*((TimerBreathTrigger*) PPauseTimeoutTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Delivered Flow Exp Trigger

Uint
	PDeliveredFlowExpTrigger[(sizeof(DeliveredFlowExpTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference DeliveredFlowExpTrigger to a memory block
DeliveredFlowExpTrigger& RDeliveredFlowExpTrigger = 
	*((DeliveredFlowExpTrigger*) PDeliveredFlowExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Immediate Breath Trigger

Uint
	PImmediateBreathTrigger[(sizeof(ImmediateBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an ImmediateBreathTrigger to a block of memory
ImmediateBreathTrigger& RImmediateBreathTrigger =
	*((ImmediateBreathTrigger*) PImmediateBreathTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Immediate Exp Trigger

Uint
	PImmediateExpTrigger[(sizeof(ImmediateBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an ImmediateBreathTrigger to a block of memory
ImmediateBreathTrigger& RImmediateExpTrigger =
	*((ImmediateBreathTrigger*) PImmediateExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// BiLevel LowToHigh Inspiration Trigger

Uint
	PBiLevelLowToHighInspTrigger[(sizeof(BiLevelLowToHighInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference BiLevelLowToHighInspTrigger to a memory block
BiLevelLowToHighInspTrigger& RBiLevelLowToHighInspTrigger = 
	*((BiLevelLowToHighInspTrigger*) PBiLevelLowToHighInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// BiLevel HighToLow Expiration Trigger

Uint
	PBiLevelHighToLowExpTrigger[(sizeof(BiLevelHighToLowExpTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference BiLevelHighToLowExpTrigger to a memory block
BiLevelHighToLowExpTrigger& RBiLevelHighToLowExpTrigger = 
	*((BiLevelHighToLowExpTrigger*) PBiLevelHighToLowExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Peep Reduction Exp Trigger

Uint
	PPeepReductionExpTrigger[(sizeof(ImmediateBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an ImmediateBreathTrigger to a block of memory
ImmediateBreathTrigger& RPeepReductionExpTrigger =
	*((ImmediateBreathTrigger*) PPeepReductionExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Arm Maneuver Trigger

Uint
	PArmManeuverTrigger[(sizeof(ImmediateBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an ImmediateBreathTrigger to a block of memory
ImmediateBreathTrigger& RArmManeuverTrigger =
	*((ImmediateBreathTrigger*) PArmManeuverTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Disarm Maneuver Trigger

Uint
	PDisarmManeuverTrigger[(sizeof(ImmediateBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an ImmediateBreathTrigger to a block of memory
ImmediateBreathTrigger& RDisarmManeuverTrigger =
	*((ImmediateBreathTrigger*) PDisarmManeuverTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Osc Time Insp Trigger

Uint
	POscTimeInspTrigger[(sizeof(OscTimeInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an OscTimeInspTrigger to a block of memory
OscTimeInspTrigger& ROscTimeInspTrigger =
	*((OscTimeInspTrigger*) POscTimeInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pressure Svc Trigger

Uint
	PPressureSvcTrigger[(sizeof(PressureSvcTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an PressureSvc Breath Trigger to a block of memory
PressureSvcTrigger& RPressureSvcTrigger =
	*((PressureSvcTrigger*) PPressureSvcTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Osc Time Backup Insp Trigger

Uint
	POscTimeBackupInspTrigger[(sizeof(TimerBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an Timer Breath Trigger to a block of memory
TimerBreathTrigger& ROscTimeBackupInspTrigger =
	*((TimerBreathTrigger*) POscTimeBackupInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Svc Time Trigger

Uint
	PSvcTimeTrigger[(sizeof(TimerBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an Timer TimerBreathTrigger to a block of memory
TimerBreathTrigger& RSvcTimeTrigger =
	*((TimerBreathTrigger*) PSvcTimeTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Svc Complete Trigger

Uint
	PSvcCompleteTrigger[(sizeof(ImmediateBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a immediate ImmediateBreathTrigger to a block of memory
ImmediateBreathTrigger& RSvcCompleteTrigger =
	*((ImmediateBreathTrigger*) PSvcCompleteTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pause Completion Trigger

Uint
	PPauseCompletionTrigger[(sizeof(ImmediateBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an Immediate Breath Trigger to a block of memory
ImmediateBreathTrigger& RPauseCompletionTrigger =
	*((ImmediateBreathTrigger*) PPauseCompletionTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// P100 Completion Trigger

Uint
	PP100CompletionTrigger[(sizeof(ImmediateBreathTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias an Immediate Breath Trigger to a block of memory
ImmediateBreathTrigger& RP100CompletionTrigger =
	*((ImmediateBreathTrigger*) PP100CompletionTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// High Circuit Pressure Exp Trigger

Uint
	PHighCircuitPressureExpTrigger[(sizeof(HighCircuitPressureExpTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a HighCircuitPressureExpTrigger to a block of memory
HighCircuitPressureExpTrigger& RHighCircuitPressureExpTrigger =
	*((HighCircuitPressureExpTrigger*)
								PHighCircuitPressureExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// High Circuit Pressure Insp Trigger

Uint
	PHighCircuitPressureInspTrigger[(sizeof(HighCircuitPressureInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a HighCircuitPressureInspTrigger to a block of memory
HighCircuitPressureInspTrigger& RHighCircuitPressureInspTrigger =
	*((HighCircuitPressureInspTrigger*)
								PHighCircuitPressureInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// High Vent Pressure Exp Trigger

Uint
	PHighVentPressureExpTrigger[(sizeof(HighVentPressureExpTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a HighVentPressureExpTrigger to a block of memory
HighVentPressureExpTrigger& RHighVentPressureExpTrigger =
	*((HighVentPressureExpTrigger*) PHighVentPressureExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Net Flow Insp Trigger

Uint
	PNetFlowInspTrigger[(sizeof(NetFlowInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a NetFlowInspTrigger to a block of memory
NetFlowInspTrigger& RNetFlowInspTrigger = 
	*((NetFlowInspTrigger*) PNetFlowInspTrigger);
/////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////
// Net Flow Backup Insp Trigger

Uint
	PNetFlowBackupInspTrigger[(sizeof(NetFlowInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a NetFlowInspTrigger to a block of memory
NetFlowInspTrigger& RNetFlowBackupInspTrigger = 
	*((NetFlowInspTrigger*) PNetFlowBackupInspTrigger);
/////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////
// P100 Trigger

Uint
	PP100Trigger[(sizeof(P100Trigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a P100Trigger to a block of memory
P100Trigger& RP100Trigger = 
	*((P100Trigger*) PP100Trigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Operator Insp Trigger

Uint
	POperatorInspTrigger[(sizeof(OperatorInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a OperatorInspTrigger to a block of memory
OperatorInspTrigger& ROperatorInspTrigger = 
	*((OperatorInspTrigger*) POperatorInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Peep Recovery Trigger

Uint
	PPeepRecoveryInspTrigger[(sizeof(PeepRecoveryInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PeepRecoveryInspTrigger to a block of memory
PeepRecoveryInspTrigger& RPeepRecoveryInspTrigger = 
	*((PeepRecoveryInspTrigger*) PPeepRecoveryInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Peep Recovery Mand Insp Trigger

Uint
	PPeepRecoveryMandInspTrigger[(sizeof(PeepRecoveryInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PeepRecoveryMandInspTrigger to a block of memory
PeepRecoveryInspTrigger& RPeepRecoveryMandInspTrigger = 
	*((PeepRecoveryInspTrigger*) PPeepRecoveryMandInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pressure Backup Inspiration Trigger

Uint
	PPressureBackupInspTrigger[(sizeof(PressureInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a reference PressureInspTrigger to a memory block
PressureInspTrigger& RPressureBackupInspTrigger = 
	*((PressureInspTrigger*) PPressureBackupInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pause Pressure Exp Trigger

Uint
	PPausePressTrigger[(sizeof(PausePressTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PausePressTrigger to a block of memory
PausePressTrigger& RPausePressTrigger = 
	*((PausePressTrigger*) PPausePressTrigger);
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
// Pressure Exp Trigger

Uint
	PPressureExpTrigger[(sizeof(PressureExpTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PressureExpTrigger to a block of memory
PressureExpTrigger& RPressureExpTrigger = 
	*((PressureExpTrigger*) PPressureExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Pressure Insp Trigger

Uint
	PPressureInspTrigger[(sizeof(PressureSetInspTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PressureInspTrigger to a block of memory
PressureSetInspTrigger& RPressureInspTrigger = 
	*((PressureSetInspTrigger*) PPressureInspTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Breath Trigger Mediator

Uint
	PBreathTriggerMediator[(sizeof(BreathTriggerMediator) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a BreathTriggerMediator to a block of memory
BreathTriggerMediator& RBreathTriggerMediator = 
	*((BreathTriggerMediator*) PBreathTriggerMediator);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Apnea Trigger

Uint
	PApneaTrigger[(sizeof(TimerModeTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a TimerModeTrigger to a block of memory
TimerModeTrigger& RApneaTrigger = 
	*((TimerModeTrigger*) PApneaTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// ApneaAutoResetTrigger

Uint
	PApneaAutoResetTrigger[(sizeof(ApneaAutoResetTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ApneaAutoResetTrigger to a block of memory
ApneaAutoResetTrigger& RApneaAutoResetTrigger =
	*((ApneaAutoResetTrigger*) PApneaAutoResetTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// DisconnectTrigger

Uint
	PDisconnectTrigger[(sizeof(DisconnectTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a DisconnectTrigger to a block of memory
DisconnectTrigger& RDisconnectTrigger =
	*((DisconnectTrigger*) PDisconnectTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// DiscAutoResetTrigger

Uint
	PDiscAutoResetTrigger[(sizeof(DiscAutoResetTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a DiscAutoResetTrigger to a block of memory
DiscAutoResetTrigger& RDiscAutoResetTrigger =
	*((DiscAutoResetTrigger*) PDiscAutoResetTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SettingChangeModeTrigger

Uint
	PSettingChangeModeTrigger[(sizeof(ImmediateModeTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ImmediateModeTrigger to a block of memory
ImmediateModeTrigger& RSettingChangeModeTrigger =
	*((ImmediateModeTrigger*) PSettingChangeModeTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// ManualResetTrigger

Uint
	PManualResetTrigger[(sizeof(ImmediateModeTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ImmediateModeTrigger to a block of memory
ImmediateModeTrigger& RManualResetTrigger =
	*((ImmediateModeTrigger*) PManualResetTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Startup Trigger

Uint
	PStartupTrigger[(sizeof(ImmediateModeTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ImmediateModeTrigger to a block of memory
ImmediateModeTrigger& RStartupTrigger = 
	*((ImmediateModeTrigger*) PStartupTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Occlusion Auto Reset Trigger

Uint
	POcclusionAutoResetTrigger[(sizeof(ImmediateModeTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ImmediateModeTrigger to a block of memory
ImmediateModeTrigger& ROcclusionAutoResetTrigger =
	*((ImmediateModeTrigger*) POcclusionAutoResetTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// OcclusionTrigger

Uint
	POcclusionTrigger[(sizeof(OcclusionTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a OcclusionTrigger to a block of memory
OcclusionTrigger& ROcclusionTrigger =
	*((OcclusionTrigger*) POcclusionTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SvoTrigger

Uint
	PSvoTrigger[(sizeof(SvoTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a SvoTrigger to a block of memory
SvoTrigger& RSvoTrigger =
	*((SvoTrigger*) PSvoTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SvoResetTrigger

Uint
	PSvoResetTrigger[(sizeof(SvoResetTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a SvoResetTrigger to a block of memory
SvoResetTrigger& RSvoResetTrigger =
	*((SvoResetTrigger*) PSvoResetTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// VentSetupCompleteTrigger

Uint
	PVentSetupCompleteTrigger[(sizeof(VentSetupCompleteTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a VentSetupComplete to a block of memory
VentSetupCompleteTrigger& RVentSetupCompleteTrigger =
	*((VentSetupCompleteTrigger*) PVentSetupCompleteTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// LungFlowExpTrigger

Uint
	PLungFlowExpTrigger[(sizeof(LungFlowExpTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a LungFlowExp to a block of memory
LungFlowExpTrigger& RLungFlowExpTrigger =
	*((LungFlowExpTrigger*) PLungFlowExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// LungVolumeExpTrigger

Uint
	PLungVolumeExpTrigger[(sizeof(LungVolumeExpTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a LungVolumeExp to a block of memory
LungVolumeExpTrigger& RLungVolumeExpTrigger =
	*((LungVolumeExpTrigger*) PLungVolumeExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// HighPressCompExpTrigger

Uint
	PHighPressCompExpTrigger[(sizeof(HighPressCompExpTrigger) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a HighPressCompExpTrigger to a block of memory
HighPressCompExpTrigger& RHighPressCompExpTrigger =
	*((HighPressCompExpTrigger*) PHighPressCompExpTrigger);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Mode Trigger Mediator

Uint
	PModeTriggerMediator[(sizeof(ModeTriggerMediator) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ModeTriggerMediator to a block of memory
ModeTriggerMediator& RModeTriggerMediator = 
	*((ModeTriggerMediator*) PModeTriggerMediator);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Circuit Compliance

Uint
	PCircuitCompliance[(sizeof(CircuitCompliance) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a CircuitCompliance to a block of memory
CircuitCompliance& RCircuitCompliance = 
	*((CircuitCompliance*) PCircuitCompliance);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Waveform

Uint
	PWaveform[(sizeof(Waveform) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a Waveform to a block of memory
Waveform& RWaveform = 
	*((Waveform*) PWaveform);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// AveragedBreathData

Uint
	PAveragedBreathData[(sizeof(AveragedBreathData) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a AveragedBreathData to a block of memory
AveragedBreathData& RAveragedBreathData = 
	*((AveragedBreathData*) PAveragedBreathData);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// LungData

Uint
	PLungData[(sizeof(LungData) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a LungData to a block of memory
LungData& RLungData = 
	*((LungData*) PLungData);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// PavManger

Uint
	PPavManager[(sizeof(PavManager) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PavManager to a block of memory
PavManager& RPavManager = 
	*((PavManager*) PPavManager);
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
// Leak Compensation Manager

Uint
	PLeakCompMgr[(sizeof(LeakCompMgr) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a LeakCompMgr to a block of memory
LeakCompMgr& RLeakCompMgr = 
	*((LeakCompMgr*) PLeakCompMgr);
/////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
// DynamicMechanics

Uint
	PDynamicMechanics[(sizeof(DynamicMechanics) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a DynamicMechanics to a block of memory
DynamicMechanics& RDynamicMechanics = 
	*((DynamicMechanics*) PDynamicMechanics);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// VolumeTargetedManger

Uint
	PVolumeTargetedManager[(sizeof(VolumeTargetedManager) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a VolumeTargetedManager to a block of memory
VolumeTargetedManager& RVolumeTargetedManager = 
	*((VolumeTargetedManager*) PVolumeTargetedManager);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Tube

Uint
	PTube[(sizeof(Tube) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a Tube to a block of memory
Tube& RTube = 
	*((Tube*) PTube);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SafetyNetTestMediator

Uint
	PSafetyNetTestMediator[(sizeof(SafetyNetTestMediator) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a SafetyNetTestMediator to a block of memory
SafetyNetTestMediator& RSafetyNetTestMediator = 
	*((SafetyNetTestMediator*) PSafetyNetTestMediator);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// AirPsolStuckTest

Uint
	PAirPsolStuckTest[(sizeof(AirPsolStuckTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a AirPsolStuckTest to a block of memory
AirPsolStuckTest& RAirPsolStuckTest = 
	*((AirPsolStuckTest*) PAirPsolStuckTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// O2PsolStuckTest

Uint
	PO2PsolStuckTest[(sizeof(O2PsolStuckTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a O2PsolStuckTest to a block of memory
O2PsolStuckTest& RO2PsolStuckTest = 
	*((O2PsolStuckTest*) PO2PsolStuckTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// AirFlowSensorTest

Uint
	PAirFlowSensorTest[(sizeof(AirFlowSensorTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a AirFlowSensorTest to a block of memory
AirFlowSensorTest& RAirFlowSensorTest = 
	*((AirFlowSensorTest*) PAirFlowSensorTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// O2FlowSensorTest

Uint
	PO2FlowSensorTest[(sizeof(O2FlowSensorTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a O2FlowSensorTest to a block of memory
O2FlowSensorTest& RO2FlowSensorTest = 
	*((O2FlowSensorTest*) PO2FlowSensorTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// InspPressureSensorTest

Uint
	PInspPressureSensorTest[(sizeof(InspPressureSensorTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a InspPressureSensorTest to a block of memory
InspPressureSensorTest& RInspPressureSensorTest = 
	*((InspPressureSensorTest*) PInspPressureSensorTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// ExhPressureSensorTest

Uint
	PExhPressureSensorTest[(sizeof(ExhPressureSensorTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ExhPressureSensorTest to a block of memory
ExhPressureSensorTest& RExhPressureSensorTest = 
	*((ExhPressureSensorTest*) PExhPressureSensorTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// PiStuckTest

Uint
	PPiStuckTest[(sizeof(PiStuckTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PiStuckTest to a block of memory
PiStuckTest& RPiStuckTest = 
	*((PiStuckTest*) PPiStuckTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// PeStuckTest

Uint
	PPeStuckTest[(sizeof(PeStuckTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a PeStuckTest to a block of memory
PeStuckTest& RPeStuckTest = 
	*((PeStuckTest*) PPeStuckTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SvOpenedLoopbackTest 

Uint
	PSvOpenedLoopbackTest[(sizeof(SvOpenedLoopbackTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a SvOpenedLoopbackTest to a block of memory
SvOpenedLoopbackTest& RSvOpenedLoopbackTest = 
	*((SvOpenedLoopbackTest*) PSvOpenedLoopbackTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// O2PsolStuckOpenTest

Uint
	PO2PsolStuckOpenTest[(sizeof(O2PsolStuckOpenTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a O2PsolStuckOpenTest to a block of memory
O2PsolStuckOpenTest& RO2PsolStuckOpenTest = 
	*((O2PsolStuckOpenTest*) PO2PsolStuckOpenTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// AirPsolStuckOpenTest

Uint
	PAirPsolStuckOpenTest[(sizeof(AirPsolStuckOpenTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a AirPsolStuckOpenTest to a block of memory
AirPsolStuckOpenTest& RAirPsolStuckOpenTest = 
	*((AirPsolStuckOpenTest*) PAirPsolStuckOpenTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// SvClosedLoopbackTest

Uint
	PSvClosedLoopbackTest[(sizeof(SvClosedLoopbackTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a SvClosedLoopbackTest to a block of memory
SvClosedLoopbackTest& RSvClosedLoopbackTest = 
	*((SvClosedLoopbackTest*) PSvClosedLoopbackTest);
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Exhalation Flow Sensor Flow Test

Uint
	PExhFlowSensorFlowTest[(sizeof(ExhFlowSensorFlowTest) + sizeof(Uint) - 1)
	/ sizeof(Uint)];
// Alias a ExhFlowSensorFlowTest to a block of memory
ExhFlowSensorFlowTest& RExhFlowSensorFlowTest = 
	*((ExhFlowSensorFlowTest*) PExhFlowSensorFlowTest);
/////////////////////////////////////////////////////////////////////////
// 

Uint 
   PProxManager[ (sizeof(ProxManager) + sizeof(Uint) - 1)
    / sizeof(Uint)];
//alias a ProxManagerRecord 
ProxManager& RProxManager = 
 *((ProxManager*) PProxManager);


#endif // defined(SIGMA_BD_CPU) || defined(SIGMA_UNIT_TEST)

//====================================================================
//
//   Breath_Delivery, sub-system initialization
//
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize(void)
//
//@ Interface-Description
// The method takes no argument and returns no value. 
// It constructs all breath delivery sub-system objects and implements
// breath delivery initialization.
//---------------------------------------------------------------------
//@ Implementation-Description
// the placement new operator is used to control the construction of 
// the objects. The second part of the method consists of initialization
// of breath delivery objects.
//---------------------------------------------------------------------
//@ PreCondition
// For every placement new operator, the address of the object created is
// checked to have the proper address.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================

void
Breath_Delivery::Initialize(void)
{
#if defined(SIGMA_BD_CPU) || defined(SIGMA_UNIT_TEST)

   CALL_TRACE("Initialize()");
// construct the Breath_Delivery objects in the pre-allocated memory space.

   // 100% O2 Interval Timer
   IntervalTimer* pIntervalTimer = 
      new (&RO2Timer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RO2Timer, BREATH_DELIVERY, BREATHDELIVERY);

   //  O2 Mix Interval Timer
   IntervalTimer* pO2MixIntervalTimer = 
      new (&RO2MixTimer) IntervalTimer();
   FREE_POST_CONDITION(pO2MixIntervalTimer ==
                &RO2MixTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // O2 Mixture
   // Note: Instanciation order constraint: rO2Timer -> rO2Mixture
   O2Mixture* pO2Mixture = 
      new (&RO2Mixture) O2Mixture(RO2Timer, RO2MixTimer);
   FREE_POST_CONDITION(pO2Mixture ==
                &RO2Mixture, BREATH_DELIVERY, BREATHDELIVERY);

	// register a call back with the gas supply object to notify
	// about gas supply changes:
	RGasSupply.registerCallBack(O2Mixture::SetGasSupplyStatus);

   //set up the client of the timer
   RO2Timer.setupTarget((TimerTarget*)(&RO2Mixture));

   //set up the client of the timer
   RO2MixTimer.setupTarget((TimerTarget*)(&RO2Mixture));

   // SST Confirmation Interval Timer
   pIntervalTimer = 
      new (&RSmSwitchConfirmationTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RSmSwitchConfirmationTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Sst Confirmation
   SmSwitchConfirmation* pSmSwitchConfirmation = 
      new (&RSmSwitchConfirmation) SmSwitchConfirmation(RSmSwitchConfirmationTimer);
   FREE_POST_CONDITION(pSmSwitchConfirmation ==
                &RSmSwitchConfirmation, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RSmSwitchConfirmationTimer.setupTarget((TimerTarget*)(&RSmSwitchConfirmation));

   // o2 volume controller
   VolumeController* pO2VolumeController = 
      new (&RO2VolumeController)
        VolumeController(RO2Psol, RO2FlowSensor, PsolLiftoff::O2_SIDE);
   FREE_POST_CONDITION(pO2VolumeController ==
                &RO2VolumeController, BREATH_DELIVERY, BREATHDELIVERY);

   // air volume controller
   VolumeController* pAirVolumeController = 
      new (&RAirVolumeController)
        VolumeController(RAirPsol, RAirFlowSensor, PsolLiftoff::AIR_SIDE);
   FREE_POST_CONDITION(pAirVolumeController ==
                &RAirVolumeController, BREATH_DELIVERY, BREATHDELIVERY);

   // o2 flow controller
   FlowController* pO2FlowController = 
      new (&RO2FlowController)
        FlowController(RO2Psol, RO2FlowSensor, PsolLiftoff::O2_SIDE);
   FREE_POST_CONDITION(pO2FlowController ==
                &RO2FlowController, BREATH_DELIVERY, BREATHDELIVERY);

   // air flow controller
   FlowController* pAirFlowController = 
      new (&RAirFlowController)
        FlowController(RAirPsol, RAirFlowSensor, PsolLiftoff::AIR_SIDE);
   FREE_POST_CONDITION(pAirFlowController ==
                &RAirFlowController, BREATH_DELIVERY, BREATHDELIVERY);

   // pressure controller
   PressureController* pPressureController = 
      new (&RPressureController)
        PressureController(RInspPressureSensor, RExhPressureSensor, 
		RO2FlowController, RAirFlowController);
   FREE_POST_CONDITION(pPressureController ==
                &RPressureController, BREATH_DELIVERY, BREATHDELIVERY);

   // exhalation valve controller during inspiration
   ExhValveIController* pExhValveIController = 
      new (&RExhValveIController)
        ExhValveIController();
   FREE_POST_CONDITION(pExhValveIController ==
                &RExhValveIController, BREATH_DELIVERY, BREATHDELIVERY);

   // Proportional Integral Derivative (Pid) controller
   PidPressureController* pPidPressureController = 
      new (&RPidPressureController)
        PidPressureController(RO2FlowController, RAirFlowController);
   FREE_POST_CONDITION(pPidPressureController ==
                &RPidPressureController, BREATH_DELIVERY, BREATHDELIVERY);

   // peep controller
   PeepController* pPeepController = 
      new (&RPeepController) PeepController() ;
   FREE_POST_CONDITION(pPeepController ==
                &RPeepController, BREATH_DELIVERY, BREATHDELIVERY);

   // volume targeted controller
   VolumeTargetedController* pVolumeTargetedController = 
      new (&RVolumeTargetedController) VolumeTargetedController() ;
   FREE_POST_CONDITION(pVolumeTargetedController ==
                &RVolumeTargetedController, BREATH_DELIVERY, BREATHDELIVERY);

   // Startup Phase
   StartupPhase* pStartupPhase = 
      new (&RStartupPhase) StartupPhase();
   FREE_POST_CONDITION(pStartupPhase ==
                &RStartupPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Exhalation Phase
   ExhalationPhase* pExhalationPhase = 
      new (&RExhalationPhase)
        ExhalationPhase();
   FREE_POST_CONDITION(pExhalationPhase ==
                &RExhalationPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Expiratory Pause Phase 
   ExpiratoryPausePhase* pExpiratoryPausePhase = 
      new (&RExpiratoryPausePhase)
        ExpiratoryPausePhase();
   FREE_POST_CONDITION(pExpiratoryPausePhase ==
                &RExpiratoryPausePhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Inspiratory Pause Phase 
   InspiratoryPausePhase* pInspiratoryPausePhase = 
      new (&RInspiratoryPausePhase)
        InspiratoryPausePhase(BreathPhaseType::INSPIRATORY_PAUSE);
   FREE_POST_CONDITION(pInspiratoryPausePhase ==
                &RInspiratoryPausePhase, BREATH_DELIVERY, BREATHDELIVERY);

   // NIF Pause Phase 
   NifPausePhase* pNifPausePhase = 
      new (&RNifPausePhase)
        NifPausePhase();
   FREE_POST_CONDITION(pNifPausePhase ==
                &RNifPausePhase, BREATH_DELIVERY, BREATHDELIVERY);

   // P100 Pause Phase 
   P100PausePhase* pP100PausePhase = 
      new (&RP100PausePhase)
        P100PausePhase();
   FREE_POST_CONDITION(pP100PausePhase ==
                &RP100PausePhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Pav Pause Phase 
   PavPausePhase* pPavPausePhase = 
      new (&RPavPausePhase)
        PavPausePhase(BreathPhaseType::PAV_INSPIRATORY_PAUSE);
   FREE_POST_CONDITION(pPavPausePhase ==
                &RPavPausePhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Bilevel Spont Pause Phase
   InspiratoryPausePhase* pBiLevelSpontPausePhase = 
      new (&RBiLevelSpontPausePhase)
        InspiratoryPausePhase(BreathPhaseType::BILEVEL_PAUSE_PHASE);
   FREE_POST_CONDITION(pBiLevelSpontPausePhase ==
                &RBiLevelSpontPausePhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Inspiratory Pause Maneuver Timer
   pIntervalTimer = 
      new (&RInspPauseManeuverTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RInspPauseManeuverTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Inspiratory Pause Maneuver
   InspPauseManeuver* pInspPauseManeuver = 
      new (&RInspPauseManeuver)
        InspPauseManeuver(Maneuver::INSP_PAUSE, RInspPauseManeuverTimer);
   FREE_POST_CONDITION(pInspPauseManeuver ==
                &RInspPauseManeuver, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RInspPauseManeuverTimer.setupTarget((TimerTarget*)(&RInspPauseManeuver));

   // Expiratory Pause Maneuver Timer
   pIntervalTimer = 
      new (&RExpPauseManeuverTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RExpPauseManeuverTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Expiratory Pause Maneuver
   ExpPauseManeuver* pExpPauseManeuver = 
      new (&RExpPauseManeuver)
        ExpPauseManeuver(Maneuver::EXP_PAUSE, RExpPauseManeuverTimer);
   FREE_POST_CONDITION(pExpPauseManeuver ==
                &RExpPauseManeuver, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RExpPauseManeuverTimer.setupTarget((TimerTarget*)(&RExpPauseManeuver));

   // NIF Maneuver Timer
   pIntervalTimer = 
      new (&RNifManeuverTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RNifManeuverTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // NIF Maneuver
   NifManeuver* pNifManeuver = 
      new (&RNifManeuver)
        NifManeuver(Maneuver::NIF_MANEUVER, RNifManeuverTimer);
   FREE_POST_CONDITION(pNifManeuver ==
                &RNifManeuver, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RNifManeuverTimer.setupTarget((TimerTarget*)(&RNifManeuver));

   // P100 Maneuver Timer
   pIntervalTimer = 
	  new (&RP100ManeuverTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
				&RP100ManeuverTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // P100 Maneuver
   P100Maneuver* pP100Maneuver = 
	  new (&RP100Maneuver)
		P100Maneuver(Maneuver::P100_MANEUVER, RP100ManeuverTimer);
   FREE_POST_CONDITION(pP100Maneuver ==
				&RP100Maneuver, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RP100ManeuverTimer.setupTarget((TimerTarget*)(&RP100Maneuver));

   // SVC Maneuver Timer
   pIntervalTimer = 
      new (&RVitalCapacityManeuverTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RVitalCapacityManeuverTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // SVC Maneuver
   VitalCapacityManeuver* pVitalCapacityManeuver = 
      new (&RVitalCapacityManeuver)
        VitalCapacityManeuver(Maneuver::VITAL_CAPACITY_MANEUVER, RVitalCapacityManeuverTimer);
   FREE_POST_CONDITION(pVitalCapacityManeuver ==
                &RVitalCapacityManeuver, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RVitalCapacityManeuverTimer.setupTarget((TimerTarget*)(&RVitalCapacityManeuver));

   // VCV Phase
   VcvPhase* pVcvPhase = 
      new (&RVcvPhase) VcvPhase(SettingId::TIDAL_VOLUME, SettingId::MIN_INSP_FLOW,
			SettingId::PLATEAU_TIME, SettingId::FLOW_PATTERN, SettingId::INSP_TIME);
   FREE_POST_CONDITION(pVcvPhase ==
                &RVcvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Apnea VCV Phase
   VcvPhase* pApneaVcvPhase = 
      new (&RApneaVcvPhase)
      	VcvPhase(SettingId::APNEA_TIDAL_VOLUME, SettingId::MIN_INSP_FLOW,
			SettingId::APNEA_PLATEAU_TIME, SettingId::APNEA_FLOW_PATTERN,
			SettingId::APNEA_INSP_TIME);
   FREE_POST_CONDITION(pApneaVcvPhase ==
                &RApneaVcvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Pressure Control Phase
   PcvPhase* pPcvPhase = 
      new (&RPcvPhase) PcvPhase( SettingId::INSP_PRESS, SettingId::INSP_TIME,
			SettingId::FLOW_ACCEL_PERCENT);
   FREE_POST_CONDITION(pPcvPhase ==
                &RPcvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Apnea Pressure Control Phase
   PcvPhase* pApneaPcvPhase = 
      new (&RApneaPcvPhase) PcvPhase( SettingId::APNEA_INSP_PRESS, 
			SettingId::APNEA_INSP_TIME, SettingId::APNEA_FLOW_ACCEL_PERCENT);
   FREE_POST_CONDITION(pApneaPcvPhase ==
                &RApneaPcvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Volume-Targetted, Pressure Control Phase (VC+)
   VtpcvPhase* pVtpcvPhase = 
      new (&RVtpcvPhase) VtpcvPhase( SettingId::TIDAL_VOLUME, SettingId::INSP_TIME,
			SettingId::FLOW_ACCEL_PERCENT);
   FREE_POST_CONDITION(pVtpcvPhase ==
                &RVtpcvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Pressure Support Phase
   PsvPhase* pPsvPhase = 
      new (&RPsvPhase) PsvPhase() ;
   FREE_POST_CONDITION(pPsvPhase ==
                &RPsvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Volume Support Phase
   VsvPhase* pVsvPhase = 
      new (&RVsvPhase) VsvPhase() ;
   FREE_POST_CONDITION(pVsvPhase ==
                &RVsvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Tracheal compensation Phase
   TcvPhase* pTcvPhase = 
      new (&RTcvPhase) TcvPhase() ;
   FREE_POST_CONDITION(pTcvPhase ==
                &RTcvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Proportional Assist Phase
   PavPhase* pPavPhase = 
      new (&RPavPhase) PavPhase() ;
   FREE_POST_CONDITION(pPavPhase ==
                &RPavPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Low To High Peep Phase
   LowToHighPeepPhase* pLowToHighPeepPhase = 
      new (&RLowToHighPeepPhase) LowToHighPeepPhase() ;
   FREE_POST_CONDITION(pLowToHighPeepPhase ==
                &RLowToHighPeepPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // High Level Psv Phase
   HiLevelPsvPhase* pHiLevelPsvPhase = 
      new (&RHiLevelPsvPhase) HiLevelPsvPhase() ;
   FREE_POST_CONDITION(pHiLevelPsvPhase ==
                &RHiLevelPsvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // Peep Recovery Phase
   PeepRecoveryPhase* pPeepRecoveryPhase = 
      new (&RPeepRecoveryPhase) PeepRecoveryPhase() ;
   FREE_POST_CONDITION(pPeepRecoveryPhase ==
                &RPeepRecoveryPhase, BREATH_DELIVERY, BREATHDELIVERY);
                
   // Disconnect Phase
   DisconnectPhase* pDisconnectPhase = 
      new (&RDisconnectPhase) DisconnectPhase() ;
   FREE_POST_CONDITION(pDisconnectPhase ==
                &RDisconnectPhase, BREATH_DELIVERY, BREATHDELIVERY);
                
   // SafeState Phase
   SafeStatePhase* pSafeStatePhase = 
      new (&RSafeStatePhase) SafeStatePhase() ;
   FREE_POST_CONDITION(pSafeStatePhase ==
                &RSafeStatePhase, BREATH_DELIVERY, BREATHDELIVERY);
                
   // SafetyValveClose Phase
   SafetyValveClosePhase* pSafetyValveClosePhase = 
      new (&RSafetyValveClosePhase) SafetyValveClosePhase() ;
   FREE_POST_CONDITION(pSafetyValveClosePhase ==
                &RSafetyValveClosePhase, BREATH_DELIVERY, BREATHDELIVERY);
                
   // OscExh Phase
   OscExhPhase* pOscExhPhase = 
      new (&ROscExhPhase) OscExhPhase() ;
   FREE_POST_CONDITION(pOscExhPhase ==
                &ROscExhPhase, BREATH_DELIVERY, BREATHDELIVERY);
                
   // OscPcv Phase
   OscPcvPhase* pOscPcvPhase = 
      new (&ROscPcvPhase) OscPcvPhase() ;
   FREE_POST_CONDITION(pOscPcvPhase ==
                &ROscPcvPhase, BREATH_DELIVERY, BREATHDELIVERY);
                
   // OscBiLevel Phase
   OscBiLevelPhase* pOscBiLevelPhase = 
      new (&ROscBiLevelPhase) OscBiLevelPhase() ;
   FREE_POST_CONDITION(pOscBiLevelPhase ==
                &ROscBiLevelPhase, BREATH_DELIVERY, BREATHDELIVERY);
                
   // Vital Capacity Maneuver Pressure Support Phase
   VcmPsvPhase* pVcmPsvPhase = 
      new (&RVcmPsvPhase) VcmPsvPhase() ;
   FREE_POST_CONDITION(pVcmPsvPhase ==
                &RVcmPsvPhase, BREATH_DELIVERY, BREATHDELIVERY);

   // AC Scheduler
   AcScheduler* pAcScheduler = 
      new (&RAcScheduler)
        AcScheduler();
   FREE_POST_CONDITION(pAcScheduler ==
                &RAcScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   // OSC Scheduler
   OscScheduler* pOscScheduler = new (&ROscScheduler) OscScheduler();
   FREE_POST_CONDITION(pOscScheduler ==
                &ROscScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   // Spont Scheduler
   SpontScheduler* pSpontScheduler = 
      new (&RSpontScheduler)
        SpontScheduler();
   FREE_POST_CONDITION(pSpontScheduler ==
                &RSpontScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   // Powerup Scheduler
   PowerupScheduler* pPowerupScheduler = 
      new (&RPowerupScheduler)
        PowerupScheduler();
   FREE_POST_CONDITION(pPowerupScheduler ==
                &RPowerupScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   // Standby Scheduler
   StandbyScheduler* pStandbyScheduler = new (&RStandbyScheduler) StandbyScheduler();
   FREE_POST_CONDITION(pStandbyScheduler ==
                &RStandbyScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   // Disconnect Scheduler
   DisconnectScheduler* pDisconnectScheduler = new (&RDisconnectScheduler)
   															DisconnectScheduler();
   FREE_POST_CONDITION(pDisconnectScheduler ==
                &RDisconnectScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   // Apnea Scheduler
   ApneaScheduler* pApneaScheduler = new (&RApneaScheduler) ApneaScheduler();
   FREE_POST_CONDITION(pApneaScheduler ==
                &RApneaScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   // Svo Scheduler
   SvoScheduler* pSvoScheduler = new (&RSvoScheduler) SvoScheduler();
   FREE_POST_CONDITION(pSvoScheduler ==
                &RSvoScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   // SafetyPcv Scheduler
   SafetyPcvScheduler* pSafetyPcvScheduler = new (&RSafetyPcvScheduler)
   																SafetyPcvScheduler();
   FREE_POST_CONDITION(pSafetyPcvScheduler ==
                &RSafetyPcvScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   // Breath Set
   BreathSet* pBreathSet = 
      new (&RBreathSet) BreathSet();
   FREE_POST_CONDITION(pBreathSet ==
                &RBreathSet, BREATH_DELIVERY, BREATHDELIVERY);

   // Breath Data
   BreathData* pBreathData = 
      new (&RBreathData) BreathData();
   FREE_POST_CONDITION(pBreathData ==
                &RBreathData, BREATH_DELIVERY, BREATHDELIVERY);

   // Manual Insp User Event
   UiEvent* pUserEvent = 
      new (&RManualInspEvent) UiEvent(BreathPhaseScheduler::ReportEventStatus,
			EventData::MANUAL_INSPIRATION);
   FREE_POST_CONDITION(pUserEvent ==
                &RManualInspEvent, BREATH_DELIVERY, BREATHDELIVERY);

   // Expiratory Pause User Event
   pUserEvent = 
      new (&RExpiratoryPauseEvent) UiEvent(BreathPhaseScheduler::ReportEventStatus,
			EventData::EXPIRATORY_PAUSE);
   FREE_POST_CONDITION(pUserEvent ==
                &RExpiratoryPauseEvent, BREATH_DELIVERY, BREATHDELIVERY);

   // Inspiratory Pause User Event
   pUserEvent = 
      new (&RInspiratoryPauseEvent) UiEvent(BreathPhaseScheduler::ReportEventStatus,
			EventData::INSPIRATORY_PAUSE);
   FREE_POST_CONDITION(pUserEvent ==
                &RInspiratoryPauseEvent, BREATH_DELIVERY, BREATHDELIVERY);

   // NIF Maneuver User Event
   pUserEvent = 
      new (&RNifManeuverEvent) UiEvent(BreathPhaseScheduler::ReportEventStatus,
			EventData::NIF_MANEUVER);
   FREE_POST_CONDITION(pUserEvent ==
                &RNifManeuverEvent, BREATH_DELIVERY, BREATHDELIVERY);

   // P100 Maneuver User Event
   pUserEvent = 
      new (&RP100ManeuverEvent) UiEvent(BreathPhaseScheduler::ReportEventStatus,
			EventData::P100_MANEUVER);
   FREE_POST_CONDITION(pUserEvent ==
                &RP100ManeuverEvent, BREATH_DELIVERY, BREATHDELIVERY);

   // SVC Maneuver User Event
   pUserEvent = 
      new (&RVitalCapacityManeuverEvent) UiEvent(BreathPhaseScheduler::ReportEventStatus,
			EventData::VITAL_CAPACITY_MANEUVER);
   FREE_POST_CONDITION(pUserEvent ==
                &RVitalCapacityManeuverEvent, BREATH_DELIVERY, BREATHDELIVERY);

   // 100% O2 User Event
   // this class has to be instanciated after rO2Mixture.
   pUserEvent = 
      new (&RO2Request) UiEvent(O2Mixture::Request100PercentO2,
			EventData::PERCENT_O2);
   FREE_POST_CONDITION(pUserEvent ==
                &RO2Request, BREATH_DELIVERY, BREATHDELIVERY);

   // CAL O2 User Event
   // this class has to be instanciated after rO2Mixture.
   pUserEvent = 
      new (&RCalO2Request) UiEvent(O2Mixture::Request100PercentO2,
			EventData::CALIBRATE_O2);
   FREE_POST_CONDITION(pUserEvent ==
                &RCalO2Request, BREATH_DELIVERY, BREATHDELIVERY);
   // SST Confirmation User Event
   // this class has to be instanciated after rSmSwitchConfirmation.
   pUserEvent = 
      new (&RServiceRequest) UiEvent(SmSwitchConfirmation::RequestSmSwitchConfirmation,
			EventData::SST_CONFIRMATION);
   FREE_POST_CONDITION(pUserEvent ==
                &RServiceRequest, BREATH_DELIVERY, BREATHDELIVERY);

   // Manual Purge User Event
   // this class has to be instanciated after rProxManager.
   pUserEvent = 
      new (&RPurgeRequest) UiEvent(ProxManager::RequestManualPurge,
			EventData::MANUAL_PURGE);
   FREE_POST_CONDITION(pUserEvent ==
                &RPurgeRequest, BREATH_DELIVERY, BREATHDELIVERY);

   // Alarm reset User Event
   pUserEvent = 
      new (&RAlarmResetEvent) UiEvent(BreathPhaseScheduler::ReportEventStatus,
			EventData::ALARM_RESET);
   FREE_POST_CONDITION(pUserEvent ==
                &RAlarmResetEvent, BREATH_DELIVERY, BREATHDELIVERY);

      // SST Exit to Online Event
   pUserEvent =
      new (&RSstExitToOnlineEvent) UiEvent(UiEvent::SstToOnlineEvent,
											EventData::SST_EXIT_TO_ONLINE);
   FREE_POST_CONDITION(pUserEvent ==
                &RSstExitToOnlineEvent, BREATH_DELIVERY, BREATHDELIVERY);

   // Gui fault Event
   pUserEvent = 
      new (&RGuiFaultEvent) UiEvent(UiEvent::ProcessEvent,
											EventData::GUI_FAULT);
   FREE_POST_CONDITION(pUserEvent ==
                &RGuiFaultEvent, BREATH_DELIVERY, BREATHDELIVERY);

   // Apnea Interval
   ApneaInterval* pApneaInterval = 
      new (&RApneaInterval) ApneaInterval();
   FREE_POST_CONDITION(pApneaInterval ==
                &RApneaInterval, BREATH_DELIVERY, BREATHDELIVERY);

   // BD Alarms
   BdAlarms* pBdAlarms = 
      new (&RBdAlarms) BdAlarms();
   FREE_POST_CONDITION(pBdAlarms ==
                &RBdAlarms, BREATH_DELIVERY, BREATHDELIVERY);

   // Peep
   Peep* pPeep = 
      new (&RPeep) Peep();
   FREE_POST_CONDITION(pPeep ==
                &RPeep, BREATH_DELIVERY, BREATHDELIVERY);

   // Apnea timer for apnea mode trigger 
   // Construct the object that is required as an argument for the next constructor
  pIntervalTimer = 
      new (&RApneaTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RApneaTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Apnea Trigger
   // Note: Instanciation order constraint: rApneatimer -> rApneaTrigger
   TimerModeTrigger* pTimerModeTrigger =
      new (&RApneaTrigger) TimerModeTrigger(RApneaTimer,Trigger::APNEA);
   FREE_POST_CONDITION(pTimerModeTrigger ==
                &RApneaTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RApneaTimer.setupTarget((TimerTarget*)(&RApneaTrigger));

   // SIMV Cycle Timer
   pIntervalTimer = 
      new (&RSimvCycleTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RSimvCycleTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Simv Scheduler
   // Note: Instanciation order constraint: rSimvCycleTimer -> rSimvScheduler
   SimvScheduler* pSimvScheduler = new (&RSimvScheduler)
   													SimvScheduler(RSimvCycleTimer);
   FREE_POST_CONDITION(pSimvScheduler ==
                &RSimvScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RSimvCycleTimer.setupTarget((TimerTarget*)(&RSimvScheduler));

   // SIMV Inspiration Interval Timer
   pIntervalTimer = 
      new (&RSimvInspTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RSimvInspTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // SIMV Time Inspiration Trigger
   // Note: Instanciation order constraint: rSimvInspTimer -> rSimvTimeInspTrigger
   SimvTimeInspTrigger* pSimvTimeInspTrigger =
      new (&RSimvTimeInspTrigger) SimvTimeInspTrigger(RSimvInspTimer);
   FREE_POST_CONDITION(pSimvTimeInspTrigger ==
                &RSimvTimeInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RSimvInspTimer.setupTarget((TimerTarget*)(&RSimvTimeInspTrigger));

   // BiLevel Cycle Timer
   pIntervalTimer = 
      new (&RBiLevelCycleTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RBiLevelCycleTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // BiLevel Scheduler
   // Note: Instanciation order constraint: rBiLevelCycleTimer -> rBiLevelScheduler
   BiLevelScheduler* pBiLevelScheduler = new (&RBiLevelScheduler)
   													BiLevelScheduler(RBiLevelCycleTimer);
   FREE_POST_CONDITION(pBiLevelScheduler ==
                &RBiLevelScheduler, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RBiLevelCycleTimer.setupTarget((TimerTarget*)(&RBiLevelScheduler));

   // Pressure Transducer Autozero Timer
   // Note: Instanciation order constraint: rPressureXducerAutozeroTimer -> rPressureXducerAutozero
   pIntervalTimer =
      new (&RPressureXducerAutozeroTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RPressureXducerAutozeroTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Pressure Transducer Autozero
   PressureXducerAutozero* pPressureXducerAutozero = 
      new (&RPressureXducerAutozero)
      							PressureXducerAutozero(RPressureXducerAutozeroTimer);
   FREE_POST_CONDITION(pPressureXducerAutozero ==
                &RPressureXducerAutozero, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RPressureXducerAutozeroTimer.setupTarget((TimerTarget*)(&RPressureXducerAutozero));

   // Backup Time Expiration Interval Timer
   pIntervalTimer = 
      new (&RBackupTimeExpTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RBackupTimeExpTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Backup Time Expiration Trigger
   // Note: Instanciation order constraint: rBackupTimeExpTimer -> rBackupTimeExpTrigger
   TimerBreathTrigger* pTimerBreathTrigger =
      new (&RBackupTimeExpTrigger) TimerBreathTrigger(RBackupTimeExpTimer,
			Trigger::BACKUP_TIME_EXP);
   FREE_POST_CONDITION(pTimerBreathTrigger ==
                &RBackupTimeExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RBackupTimeExpTimer.setupTarget((TimerTarget*)(&RBackupTimeExpTrigger));

   // Inspiration Time Interval Timer
   pIntervalTimer = 
      new (&RInspTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RInspTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Time Inspiration Trigger
   // Note: Instanciation order constraint: rInspTimer -> rTimeInspTrigger
   pTimerBreathTrigger =
      new (&RTimeInspTrigger) TimerBreathTrigger(RInspTimer,Trigger::TIME_INSP);
   FREE_POST_CONDITION(pTimerBreathTrigger ==
                &RTimeInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RInspTimer.setupTarget((TimerTarget*)(&RTimeInspTrigger));

   // Pause time out Time Interval Timer
   pIntervalTimer = 
      new (&RPauseTimeoutTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RPauseTimeoutTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Time Out Trigger for pause
   // Note: Instanciation order constraint: rPauseTimeoutTimer -> rPauseTimeoutTrigger
   pTimerBreathTrigger =
      new (&RPauseTimeoutTrigger) TimerBreathTrigger(RPauseTimeoutTimer,
      														Trigger::PAUSE_TIMEOUT);
   FREE_POST_CONDITION(pTimerBreathTrigger ==
                &RPauseTimeoutTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RPauseTimeoutTimer.setupTarget((TimerTarget*)(&RPauseTimeoutTrigger));

   // Osc Insp Interval Timer
   pIntervalTimer = 
      new (&ROscInspTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &ROscInspTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Osc Time Insp Trigger
   // Note: Instanciation order constraint: rOscInspTimer -> rOscTimeInspTrigger
   OscTimeInspTrigger* pOscTimeInspTrigger =
      new (&ROscTimeInspTrigger) OscTimeInspTrigger(ROscInspTimer,
      														Trigger::OSC_TIME_INSP);
   FREE_POST_CONDITION(pOscTimeInspTrigger ==
                &ROscTimeInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   ROscInspTimer.setupTarget((TimerTarget*)(&ROscTimeInspTrigger));

   // Osc Time Backup Interval Timer
   pIntervalTimer = 
      new (&ROscBackupInspTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &ROscBackupInspTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Osc Time Backup Trigger
   // Note: Instanciation order constraint: rOscBackupInspTimer -> rOscTimeBackupInspTrigger
   pTimerBreathTrigger =
      new (&ROscTimeBackupInspTrigger) TimerBreathTrigger(
		ROscBackupInspTimer, Trigger::OSC_TIME_BACKUP_INSP);
   FREE_POST_CONDITION(pTimerBreathTrigger ==
                &ROscTimeBackupInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   ROscBackupInspTimer.setupTarget((TimerTarget*)(&ROscTimeBackupInspTrigger));

   // Svo Interval Timer - indicates when to start Svc phase
   pIntervalTimer = 
      new (&RSvcTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RSvcTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // Svc Time Backup Trigger
   // Note: Instanciation order constraint: rSvcTimer -> rSvcTimeTrigger
   pTimerBreathTrigger =
      new (&RSvcTimeTrigger) TimerBreathTrigger(
		RSvcTimer, Trigger::SVC_TIME);
   FREE_POST_CONDITION(pTimerBreathTrigger ==
                &RSvcTimeTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   //set up the client of the timer
   RSvcTimer.setupTarget((TimerTarget*)(&RSvcTimeTrigger));

   // Average Breath Data Timer 
   pIntervalTimer = 
      new (&RAvgBreathDataTimer) IntervalTimer();
   FREE_POST_CONDITION(pIntervalTimer ==
                &RAvgBreathDataTimer, BREATH_DELIVERY, BREATHDELIVERY);

   // AveragedBreathData
   // Note: Instanciation order constraint: rAvgBreathDataTimer -> rAveragedBreathData
   AveragedBreathData* pAveragedBreathData =
      new (&RAveragedBreathData) AveragedBreathData(RAvgBreathDataTimer);
   FREE_POST_CONDITION(pAveragedBreathData == &RAveragedBreathData, BREATH_DELIVERY,
   																	BREATHDELIVERY);

   //set up the client of the timer
   RAvgBreathDataTimer.setupTarget((TimerTarget*)(&RAveragedBreathData));

   // LungData
   LungData* pLungData =
      new (&RLungData) LungData();
   FREE_POST_CONDITION(pLungData == &RLungData, BREATH_DELIVERY, BREATHDELIVERY);

   // Note: Instanciation order constraint: RPavPhase -> RPavManager
   // PavManager
   PavManager* pPavManager =
      new (&RPavManager) PavManager();
   FREE_POST_CONDITION(pPavManager == &RPavManager, BREATH_DELIVERY, BREATHDELIVERY);

   // 1L/MIN * 1000 mL/L * 1 min/60 sec * 1 sec/1000 ms * CYCLE_TIME_MS = mL
   const Real32 L_PER_MIN_TO_ML = 1.0F * (1000.0F/1.0F) * (1.0F/60.0F) * (1.0F/1000.0F) * (Real32) CYCLE_TIME_MS; 

   // LeakCompMgr
   LeakCompMgr* pLeakCompMgr =
      new (&RLeakCompMgr) LeakCompMgr(L_PER_MIN_TO_ML);
   FREE_POST_CONDITION(pLeakCompMgr == &RLeakCompMgr, BREATH_DELIVERY, BREATHDELIVERY);

   // DynamicMechanics
   DynamicMechanics* pDynamicMechanics =
	  new (&RDynamicMechanics) DynamicMechanics();
   FREE_POST_CONDITION(pDynamicMechanics == &RDynamicMechanics, BREATH_DELIVERY, BREATHDELIVERY);

   // Note: Instanciation order constraint: RVolumeTargetedController -> RVolumeTargetedManager
   // VolumeTargetedManager
   VolumeTargetedManager* pVolumeTargetedManager =
      new (&RVolumeTargetedManager) VolumeTargetedManager();
   FREE_POST_CONDITION(pVolumeTargetedManager == &RVolumeTargetedManager, BREATH_DELIVERY, BREATHDELIVERY);

   // Tube
   Tube* pTube =
      new (&RTube) Tube();
   FREE_POST_CONDITION(pTube == &RTube, BREATH_DELIVERY, BREATHDELIVERY);

   // Timer Mediator
   TimerMediator* pTimerMediator =
      new (&RTimerMediator) TimerMediator();
   FREE_POST_CONDITION(pTimerMediator ==
                &RTimerMediator, BREATH_DELIVERY, BREATHDELIVERY);

   // Asap Inspiration Trigger
   AsapInspTrigger* pAsapInspTrigger =
      new (&RAsapInspTrigger) AsapInspTrigger();
   FREE_POST_CONDITION(pAsapInspTrigger ==
                &RAsapInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Delivered Flow Exp Trigger
   DeliveredFlowExpTrigger* pDeliveredFlowExpTrigger =
      new (&RDeliveredFlowExpTrigger) DeliveredFlowExpTrigger();
   FREE_POST_CONDITION(pDeliveredFlowExpTrigger ==
                &RDeliveredFlowExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // High Circuit Pressure Expiration Trigger
   HighCircuitPressureExpTrigger* pHighCircuitPressureExpTrigger =
      new (&RHighCircuitPressureExpTrigger) HighCircuitPressureExpTrigger();
   FREE_POST_CONDITION(pHighCircuitPressureExpTrigger ==
                &RHighCircuitPressureExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // High Circuit Pressure Inspiration Trigger
   HighCircuitPressureInspTrigger* pHighCircuitPressureInspTrigger =
      new (&RHighCircuitPressureInspTrigger) HighCircuitPressureInspTrigger();
   FREE_POST_CONDITION(pHighCircuitPressureInspTrigger ==
                &RHighCircuitPressureInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // High Vent Pressure Expiration Trigger
   HighVentPressureExpTrigger* pHighVentPressureExpTrigger =
      new (&RHighVentPressureExpTrigger) HighVentPressureExpTrigger();
   FREE_POST_CONDITION(pHighVentPressureExpTrigger ==
                &RHighVentPressureExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Immediate Breath Trigger
   ImmediateBreathTrigger* pImmediateBreathTrigger =
      new (&RImmediateBreathTrigger)
      								ImmediateBreathTrigger(Trigger::IMMEDIATE_BREATH);
   FREE_POST_CONDITION(pImmediateBreathTrigger ==
                &RImmediateBreathTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Immediate Exp Trigger
   ImmediateBreathTrigger* pImmediateExpTrigger =
      new (&RImmediateExpTrigger) ImmediateBreathTrigger(Trigger::IMMEDIATE_EXP);
   FREE_POST_CONDITION(pImmediateExpTrigger ==
                &RImmediateExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Peep Reduction Exp Trigger
   ImmediateBreathTrigger* pPeepReductionExpTrigger =
      new (&RPeepReductionExpTrigger) ImmediateBreathTrigger(Trigger::PEEP_REDUCTION_EXP);
   FREE_POST_CONDITION(pPeepReductionExpTrigger ==
                &RPeepReductionExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Arm Maneuver Trigger
   ImmediateBreathTrigger* pArmManeuverTrigger =
      new (&RArmManeuverTrigger) ImmediateBreathTrigger(Trigger::ARM_MANEUVER);
   FREE_POST_CONDITION(pArmManeuverTrigger ==
                &RArmManeuverTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Disarm Maneuver Trigger
   ImmediateBreathTrigger* pDisarmManeuverTrigger =
      new (&RDisarmManeuverTrigger) ImmediateBreathTrigger(Trigger::DISARM_MANEUVER);
   FREE_POST_CONDITION(pDisarmManeuverTrigger ==
                &RDisarmManeuverTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Pause Completion Trigger
   pImmediateBreathTrigger =
      new (&RPauseCompletionTrigger) ImmediateBreathTrigger(Trigger::PAUSE_COMPLETE);
   FREE_POST_CONDITION(pImmediateBreathTrigger ==
                &RPauseCompletionTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // P100 Completion Trigger
   ImmediateBreathTrigger* pP100CompletionTrigger =
	  new (&RP100CompletionTrigger) ImmediateBreathTrigger(Trigger::P100_COMPLETE);
   FREE_POST_CONDITION(pP100CompletionTrigger ==
				&RP100CompletionTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // BiLevel LowToHigh Inspiration Trigger
   BiLevelLowToHighInspTrigger* pBiLevelLowToHighInspTrigger =
      new (&RBiLevelLowToHighInspTrigger) BiLevelLowToHighInspTrigger();
   FREE_POST_CONDITION(pBiLevelLowToHighInspTrigger ==
                &RBiLevelLowToHighInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // BiLevel HighToLow Expiration Trigger
   BiLevelHighToLowExpTrigger* pBiLevelHighToLowExpTrigger = 
      new (&RBiLevelHighToLowExpTrigger) BiLevelHighToLowExpTrigger();
   FREE_POST_CONDITION(pBiLevelHighToLowExpTrigger ==
                &RBiLevelHighToLowExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Net Flow Insp Trigger
   NetFlowInspTrigger* pNetFlowInspTrigger =
      new (&RNetFlowInspTrigger) 
          NetFlowInspTrigger(Trigger::NET_FLOW_INSP);
   FREE_POST_CONDITION(pNetFlowInspTrigger ==
                &RNetFlowInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);


   // Net Flow Backup Insp Trigger
   NetFlowInspTrigger* pNetFlowBackupInspTrigger =
      new (&RNetFlowBackupInspTrigger) 
          NetFlowInspTrigger(Trigger::NET_FLOW_BACKUP_INSP);
   FREE_POST_CONDITION(pNetFlowBackupInspTrigger ==
                &RNetFlowBackupInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);


   // P100 Trigger
   P100Trigger* pP100Trigger =
      new (&RP100Trigger) P100Trigger();
   FREE_POST_CONDITION(pP100Trigger ==
                &RP100Trigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Operator Insp Trigger
   OperatorInspTrigger* pOperatorInspTrigger =
      new (&ROperatorInspTrigger) OperatorInspTrigger();
   FREE_POST_CONDITION(pOperatorInspTrigger ==
                &ROperatorInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Peep Recovery Trigger
   PeepRecoveryInspTrigger* pPeepRecoveryInspTrigger =
      new (&RPeepRecoveryInspTrigger) PeepRecoveryInspTrigger(Trigger::PEEP_RECOVERY);
   FREE_POST_CONDITION(pPeepRecoveryInspTrigger ==
                &RPeepRecoveryInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Peep Recovery Mand Trigger
   PeepRecoveryInspTrigger* pPeepRecoveryMandInspTrigger =
      new (&RPeepRecoveryMandInspTrigger) PeepRecoveryInspTrigger(Trigger::PEEP_RECOVERY_MAND_INSP);
   FREE_POST_CONDITION(pPeepRecoveryMandInspTrigger ==
                &RPeepRecoveryMandInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Pressure Backup Inspiration Trigger
   PressureInspTrigger* pPressureBackupInspTrigger =
      new (&RPressureBackupInspTrigger)
      							PressureInspTrigger(Trigger::PRESSURE_BACKUP_INSP, BACKUP_PRESSURE_SENS_VALUE);
   FREE_POST_CONDITION(pPressureBackupInspTrigger ==
                &RPressureBackupInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Pause Pressure Exp Trigger
   PausePressTrigger* pPausePressTrigger =
      new (&RPausePressTrigger)
      							PausePressTrigger(Trigger::PAUSE_PRESS);
   FREE_POST_CONDITION(pPausePressTrigger ==
                &RPausePressTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Pressure Exp Trigger
   PressureExpTrigger* pPressureExpTrigger =
      new (&RPressureExpTrigger) PressureExpTrigger(Trigger::PRESSURE_EXP);
   FREE_POST_CONDITION(pPressureExpTrigger ==
                &RPressureExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Pressure Insp Trigger
   PressureSetInspTrigger* pPressureInspTrigger =
      new (&RPressureInspTrigger) PressureSetInspTrigger(Trigger::PRESSURE_INSP);
   FREE_POST_CONDITION(pPressureInspTrigger ==
                &RPressureInspTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Pressure Svc Trigger
   PressureSvcTrigger* pPressureSvcTrigger =
      new (&RPressureSvcTrigger) PressureSvcTrigger();
   FREE_POST_CONDITION(pPressureSvcTrigger ==
                &RPressureSvcTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Svc complete Trigger
   pImmediateBreathTrigger =
      new (&RSvcCompleteTrigger) ImmediateBreathTrigger(Trigger::SVC_COMPLETE);
   FREE_POST_CONDITION(pImmediateBreathTrigger ==
                &RSvcCompleteTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Breath Trigger Mediator
   BreathTriggerMediator* pBreathTriggerMediator =
      new (&RBreathTriggerMediator) BreathTriggerMediator();
   FREE_POST_CONDITION(pBreathTriggerMediator ==
                &RBreathTriggerMediator, BREATH_DELIVERY, BREATHDELIVERY);

   // Apnea Auto Reset Trigger
   ApneaAutoResetTrigger* pApneaAutoResetTrigger =
      new (&RApneaAutoResetTrigger) ApneaAutoResetTrigger();
   FREE_POST_CONDITION(pApneaAutoResetTrigger ==
                &RApneaAutoResetTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Disconnect Trigger
   DisconnectTrigger* pDisconnectTrigger =
      new (&RDisconnectTrigger) DisconnectTrigger();
   FREE_POST_CONDITION(pDisconnectTrigger ==
                &RDisconnectTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Disc Auto Reset Trigger
   DiscAutoResetTrigger* pDiscAutoResetTrigger =
      new (&RDiscAutoResetTrigger) DiscAutoResetTrigger();
   FREE_POST_CONDITION(pDiscAutoResetTrigger ==
                &RDiscAutoResetTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Setting Change Mode Trigger
   ImmediateModeTrigger* pImmediateModeTrigger =
      new (&RSettingChangeModeTrigger)
      							ImmediateModeTrigger(Trigger::SETTING_CHANGE_MODE);
   FREE_POST_CONDITION(pImmediateModeTrigger ==
                &RSettingChangeModeTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Manual Reset Mode Trigger
   pImmediateModeTrigger =
      new (&RManualResetTrigger) ImmediateModeTrigger(Trigger::MANUAL_RESET);
   FREE_POST_CONDITION(pImmediateModeTrigger ==
                &RManualResetTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Startup Trigger
   pImmediateModeTrigger =
      new (&RStartupTrigger) ImmediateModeTrigger(Trigger::STARTUP);
   FREE_POST_CONDITION(pImmediateModeTrigger ==
                &RStartupTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Occlusion Auto Reset Trigger
   pImmediateModeTrigger =
      new (&ROcclusionAutoResetTrigger)
      							ImmediateModeTrigger(Trigger::OCCLUSION_AUTORESET);
   FREE_POST_CONDITION(pImmediateModeTrigger ==
                &ROcclusionAutoResetTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Occlusion Trigger
   OcclusionTrigger* pOcclusionTrigger =
      new (&ROcclusionTrigger) OcclusionTrigger();
   FREE_POST_CONDITION(pOcclusionTrigger ==
                &ROcclusionTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Svo Trigger
   SvoTrigger* pSvoTrigger =
      new (&RSvoTrigger) SvoTrigger();
   FREE_POST_CONDITION(pSvoTrigger ==
                &RSvoTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Svo Reset Trigger
   SvoResetTrigger* pSvoResetTrigger =
      new (&RSvoResetTrigger) SvoResetTrigger();
   FREE_POST_CONDITION(pSvoResetTrigger ==
                &RSvoResetTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // VentSetupComplete Trigger
   VentSetupCompleteTrigger* pVentSetupCompleteTrigger =
      new (&RVentSetupCompleteTrigger) VentSetupCompleteTrigger();
   FREE_POST_CONDITION(pVentSetupCompleteTrigger ==
                &RVentSetupCompleteTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // LungFlowExp Trigger
   LungFlowExpTrigger* pLungFlowExpTrigger =
      new (&RLungFlowExpTrigger) LungFlowExpTrigger();
   FREE_POST_CONDITION(pLungFlowExpTrigger ==
                &RLungFlowExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // LungVolumeExp Trigger
   LungVolumeExpTrigger* pLungVolumeExpTrigger =
      new (&RLungVolumeExpTrigger) LungVolumeExpTrigger();
   FREE_POST_CONDITION(pLungVolumeExpTrigger ==
                &RLungVolumeExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // HighPressCompExp Trigger
   HighPressCompExpTrigger* pHighPressCompExpTrigger =
      new (&RHighPressCompExpTrigger) HighPressCompExpTrigger();
   FREE_POST_CONDITION(pHighPressCompExpTrigger ==
                &RHighPressCompExpTrigger, BREATH_DELIVERY, BREATHDELIVERY);

   // Mode Trigger Mediator
   ModeTriggerMediator* pModeTriggerMediator =
      new (&RModeTriggerMediator) ModeTriggerMediator();
   FREE_POST_CONDITION(pModeTriggerMediator ==
                &RModeTriggerMediator, BREATH_DELIVERY, BREATHDELIVERY);

   // Circuit Compliance
   CircuitCompliance* pCircuitCompliance =
      new (&RCircuitCompliance) CircuitCompliance();
   FREE_POST_CONDITION(pCircuitCompliance ==
                &RCircuitCompliance, BREATH_DELIVERY, BREATHDELIVERY);

   // Waveform
   Waveform* pWaveform =
      new (&RWaveform) Waveform();
   FREE_POST_CONDITION(pWaveform == &RWaveform, BREATH_DELIVERY, BREATHDELIVERY);

   // SafetyNetTestMediator
   SafetyNetTestMediator* pSafetyNetTestMediator = 
      new (&RSafetyNetTestMediator) SafetyNetTestMediator();
   FREE_POST_CONDITION(pSafetyNetTestMediator == &RSafetyNetTestMediator, BREATH_DELIVERY, BREATHDELIVERY);

   // PiStuckTest
   PiStuckTest* pPiStuckTest = 
      new (&RPiStuckTest) PiStuckTest();
   FREE_POST_CONDITION(pPiStuckTest == &RPiStuckTest, BREATH_DELIVERY, BREATHDELIVERY);

   // PeStuckTest
   PeStuckTest* pPeStuckTest = 
      new (&RPeStuckTest) PeStuckTest();
   FREE_POST_CONDITION(pPeStuckTest == &RPeStuckTest, BREATH_DELIVERY, BREATHDELIVERY);

   // SvOpenedLoopbackTest 
    SvOpenedLoopbackTest* pSvOpenedLoopbackTest = 
      new (&RSvOpenedLoopbackTest) SvOpenedLoopbackTest();
   FREE_POST_CONDITION(pSvOpenedLoopbackTest == &RSvOpenedLoopbackTest, BREATH_DELIVERY, BREATHDELIVERY);

   // AirFlowSensorTest
   AirFlowSensorTest* pAirFlowSensorTest = 
      new (&RAirFlowSensorTest) AirFlowSensorTest();
   FREE_POST_CONDITION(pAirFlowSensorTest == &RAirFlowSensorTest, BREATH_DELIVERY, BREATHDELIVERY);

   // O2FlowSensorTest
   O2FlowSensorTest* pO2FlowSensorTest = 
      new (&RO2FlowSensorTest) O2FlowSensorTest();
   FREE_POST_CONDITION(pO2FlowSensorTest == &RO2FlowSensorTest, BREATH_DELIVERY, BREATHDELIVERY);

   // InspPressureSensorTest
   InspPressureSensorTest* pInspPressureSensorTest = 
      new (&RInspPressureSensorTest) InspPressureSensorTest();
   FREE_POST_CONDITION(pInspPressureSensorTest == &RInspPressureSensorTest, BREATH_DELIVERY, BREATHDELIVERY);

   // ExhPressureSensorTest
   ExhPressureSensorTest* pExhPressureSensorTest = 
      new (&RExhPressureSensorTest) ExhPressureSensorTest();
   FREE_POST_CONDITION(pExhPressureSensorTest == &RExhPressureSensorTest, BREATH_DELIVERY, BREATHDELIVERY);

   // O2PsolStuckOpenTest
   O2PsolStuckOpenTest* pO2PsolStuckOpenTest = 
      new (&RO2PsolStuckOpenTest) O2PsolStuckOpenTest();
   FREE_POST_CONDITION(pO2PsolStuckOpenTest == &RO2PsolStuckOpenTest, BREATH_DELIVERY, BREATHDELIVERY);

   // AirPsolStuckOpenTest
   AirPsolStuckOpenTest* pAirPsolStuckOpenTest = 
      new (&RAirPsolStuckOpenTest) AirPsolStuckOpenTest();
   FREE_POST_CONDITION(pAirPsolStuckOpenTest == &RAirPsolStuckOpenTest, BREATH_DELIVERY, BREATHDELIVERY);

   // O2PsolStuckTest
   O2PsolStuckTest* pO2PsolStuckTest = 
      new (&RO2PsolStuckTest) O2PsolStuckTest();
   FREE_POST_CONDITION(pO2PsolStuckTest == &RO2PsolStuckTest, BREATH_DELIVERY, BREATHDELIVERY);

   // AirPsolStuckTest
   AirPsolStuckTest* pAirPsolStuckTest = 
      new (&RAirPsolStuckTest) AirPsolStuckTest();
   FREE_POST_CONDITION(pAirPsolStuckTest == &RAirPsolStuckTest, BREATH_DELIVERY, BREATHDELIVERY);

   // SvClosedLoopbackTest
   SvClosedLoopbackTest* pSvClosedLoopbackTest = 
      new (&RSvClosedLoopbackTest) SvClosedLoopbackTest();
   FREE_POST_CONDITION(pSvClosedLoopbackTest == &RSvClosedLoopbackTest, BREATH_DELIVERY, BREATHDELIVERY);

   // ExhFlowSensorFlowTest
   ExhFlowSensorFlowTest* pExhFlowSensorFlowTest = 
      new (&RExhFlowSensorFlowTest) ExhFlowSensorFlowTest();
   FREE_POST_CONDITION(pExhFlowSensorFlowTest == &RExhFlowSensorFlowTest, BREATH_DELIVERY, BREATHDELIVERY);

 
   // ProxManager
   ProxManager* pProxManager = 
      new (&RProxManager) ProxManager();
   FREE_POST_CONDITION(pProxManager == &RProxManager, BREATH_DELIVERY, BREATHDELIVERY);



   // initialize objects that setup the callbacks for vent objects
   BdAlarms::Initialize();
   VentAndUserEventStatus::Initialize();

   // initialize Vent Objects with register call backs
// Until Luan has this working

   RFio2Monitor.initialize();

   RSystemBattery.initialize();
   RPowerSource.initialize();

#ifdef E600_840_TEMP_REMOVED
// E600 BDIO   RCompressor.initialize();
#endif

   RGasSupply.initialize();

   // $[00531]
   const ShutdownState  startupState = Post::GetStartupState();
   const DowntimeStatus downTimeStatus = Post::GetDowntimeStatus();
   if( (startupState == ACSWITCH) || (startupState == INTENTIONAL) ||  
       (startupState == POWERFAIL && downTimeStatus == DOWN_LONG) )
   {
    // $[TI1]
   		// reset the NovRam data for BdSystemState
   		BdSystemStateHandler::Reset();
   }// $[TI2]
   
   BdSystemStateHandler::Initialize();
   RWaveform.initialize();   
   RBreathSet.initialize();   
   RPowerupScheduler.initialize();
   //set up current Scheduler
   BreathPhaseScheduler::Initialize(); 
   // Enable the BD timer for use during the initialization of the
   // pressure xducer autozero
   BdTimerEnabled = TRUE;
// Until Luan has this working
#ifdef E600_840_TEMP_REMOVED
   RPressureXducerAutozero.initialize();
#endif
   BdTimerEnabled = FALSE;

   // Note: The following method should be called after BdSystemStateHandler::Initialize() 
   UiEvent::Initialize();

   Btps::CalculateBtps();
   NovRamManager::GetCircuitComplianceTable(RCircuitCompliance);
   
#endif //defined(SIGMA_BD_CPU) || defined(SIGMA_UNIT_TEST)

#if defined(SIGMA_GUI_CPU) || defined(SIGMA_UNIT_TEST)
   BdGuiEvent::Initialize();
#endif // defined(SIGMA_GUI_CPU) || defined(SIGMA_DEVELOPMENT)

	// the following line must be called after the last call to rSensorMediator.newCycle()
	// in the "Initialization" of the subsystems.  Currently rPressureXducerAutozero.initialize()
	// is the last method to call rSensorMediator.newCycle().  This is true for both BD and GUI CPUs.
	RCalInfoFlashBlock.initialize() ;
}

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Breath_Delivery::SoftFault(const SoftFaultID  softFaultID,
			   const Uint32       lineNumber,
			   const char*        pFileName,
			   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, BREATHDELIVERY,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

