/*****************************************************************************/
/*   Module Name:    pb_util.h                                          
/*   Purpose:     
/*
/*   Modification History:
/*
/*   Description of Change			        By			      	Date 
/*   ---------------------					-----   		  	------
/*	 
/* 	 The parameters of all the			Hamsavally(CGS)   		12 Jan,2001 
/*   functions are modifed .
/*
/*
/*	 $Revision::   1.1      $                                                
/*                                                                          
/*                                              
/*****************************************************************************/

#ifndef _PB_UTIL_INCLUDED
#define _PB_UTIL_INCLUDED
 
/***********************************************/
/******************* DEFINES *******************/
/***********************************************/
#include "testing.h"
#define FACT_INT   "1"
#define FACT_REAL  "2"
#define FACT_STR_P "3"
#define FACT_STR_F "4"
#define FACT_STR_N "5"


/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

int UpdateRevisionInfo(char *rev_string);

int PostReal(int index, char **data, char *Fact,
             double MinRdg, double Reading, double MaxRdg) ;
int PostStringFail(int index, char **data, char *Fact, char *str) ;
int PostStringPass(int index, char **data, char *Fact, char *str) ;
int PostStringNoMsg(char **data, char *Fact) ;
int PostStringSkip( int index, char **data, char *Fact, char *str);

void PostTitle(char *title);
void PostResults(int index, char* Results) ;

void PostInfo2Window(int index, char *Results, char *title);

char *Tfmtdisplay(char *rec_string, BOOL db_format, BOOL nLocalized, 
                  char *serial_num, char *rev, BOOL nDB_Jet, int index);

#endif

