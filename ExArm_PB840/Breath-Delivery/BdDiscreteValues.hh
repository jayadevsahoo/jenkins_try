
#ifndef BdDiscreteValues_HH
#define BdDiscreteValues_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// File:  BdDiscreteValues - Includes Definition of the values of BD Discrete Settings.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/BdDiscreteValues.hhv   25.0.4.0   19 Nov 2013 13:59:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah   Date:  04-Aug-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Obsoleted patient type setting.
//
//  Revision: 002  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 001  By: sah    Date: 01-May-95    DR Number: None
//	Project:  Sigma (R8027)
//	Description:
//		Initial version.
//  
//====================================================================

//@ Usage-Classes
#  include "FlowPatternValue.hh"
#  include "HumidTypeValue.hh"
#  include "MandTypeValue.hh"
#  include "ModeValue.hh"
#  include "PatientCctTypeValue.hh"
#  include "SupportTypeValue.hh"
#  include "TriggerTypeValue.hh"
//@ End-Usage


#endif // BdDiscreteValues_HH 
