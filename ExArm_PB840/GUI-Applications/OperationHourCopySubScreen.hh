#ifndef OperationHourCopySubScreen_HH
#define OperationHourCopySubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: OperationHourCopySubScreen - Activated by selecting Copy
// Operation hour button on the the Service lower subScreen.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/OperationHourCopySubScreen.hhv   25.0.4.0   19 Nov 2013 14:08:12   pvcs  $
//
//
//@ Modification-Log
//
//  Revision: 002  By: dosman     Date:  05-May-1999    DR Number: 5379
//       Project:  ATC
//       Description:
//		Removed an enum element, CAL_DK_HOURS_OKAY_PROMPT,
//		that was not being used.
//
//  Revision: 001  By:  yyy    Date:  01-Sep-1998    DR Number:
//       Project:  BiLevel (R8027)
//       Description:
//             Initial coding.
//====================================================================

#include "SubScreen.hh"
#include "GuiTimerTarget.hh"
#include "AdjustPanelTarget.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "GuiTestManager.hh"
#include "ServiceStatusArea.hh"
#include "SmPromptId.hh"
#include "SubScreenTitleArea.hh"
#include "TextButton.hh"
#include "GuiAppClassIds.hh"
class TestResult;
//@ End-Usage

class OperationHourCopySubScreen : public SubScreen, public GuiTimerTarget,
							public AdjustPanelTarget,
							public GuiTestManagerTarget 
{
public:
	OperationHourCopySubScreen(SubScreenArea *pSubScreenArea);
	~OperationHourCopySubScreen(void);

	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelClearPressHappened(void);
	virtual void adjustPanelRestoreFocusHappened(void);

	// GuiTestMangerTarget virtual methods
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);



	static void SoftFault(const SoftFaultID softFaultID,
								 const Uint32      lineNumber,
								 const char*       pFileName  = NULL, 
								 const char*       pPredicate = NULL);

protected:

private:
	enum CalConditionId
	{
		MAX_CONDITION_ID = 4 
	};

	enum CalScreenId
	{
		DK_OP_HOUR_START,
		PROMPT_ACCEPT_BUTTON_PRESSED,
		PROMPT_CLEAR_BUTTON_PRESSED,
		MAX_CAL_SCREENS
	};

	enum CalPromptId
	{
		CAL_INSERT_DK_TO_BE_COPIED_FROM_PROMPT,
		CAL_INSERT_DK_TO_BE_COPIED_TO_PROMPT,
		MAX_CAL_PROMPTS
	};

	// these methods are purposely declared, but not implemented...
	OperationHourCopySubScreen(void);						// not implemented...
	OperationHourCopySubScreen(const OperationHourCopySubScreen&);	// not implemented...
	void operator=(const OperationHourCopySubScreen&);		// not implemented...

	void layoutScreen_(CalScreenId calScreenId);
	
	void setPromptTable_(void);
	void setErrorTable_(void);

	//@ Data-Member: calStatus_
	// A container for displaying the current datakey update status.
	ServiceStatusArea calStatus_;

	//@ Data-Member: errDisplayArea_
	// An array of TextField objects used to display test conditions in the
	// lower screen
	TextField errDisplayArea_[MAX_CONDITION_ID];
	
	//@ Data-Member: conditionText_
	// Text for error condition prompts
	StringId conditionText_[MAX_CONDITION_ID];

	//@ Data-Member: errorHappened_
	// Flag that indicates status of test
	Int	errorHappened_;
	
	//@ Data-Member: errorIndex_
	// Keep tracks of index into TextFields in the error display area
	Int	errorIndex_;
	
	//@ Data-Member: keyAllowedId_
	// The key sent from BD test, which is allowed for the operator
	// to press.
	Int keyAllowedId_;

	//@ Data-Member: userKeyPressedId_
	// Hold the operator pressed key Id.
	SmPromptId::ActionId userKeyPressedId_;

	//@ Data-Member: promptId_
	// The prompt id sent from BD test.
	SmPromptId::PromptId promptId_[MAX_CAL_PROMPTS];
	
	//@ Data-Member: promptName_
	// The table of prompt messages .
	StringId promptName_[MAX_CAL_PROMPTS];

	//@ Data-Member: titleArea_
	// The sub-screen's title at the top left of the screen.
	SubScreenTitleArea titleArea_;

	//@ Data-Member: guiTestMgr_ 
	// Object instance of GuiTestManager
	GuiTestManager guiTestMgr_;
};

#endif // OperationHourCopySubScreen_HH 
