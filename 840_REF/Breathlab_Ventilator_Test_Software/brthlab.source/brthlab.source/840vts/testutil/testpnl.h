/*****************************************************************************/
/*   Module Name:   		 testpnl.h                                             
/*   Purpose:       		 Header for PTS Ventilator Serial Communications Module                               
/*   
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*	 TestExecutive related code is  
/* 	 removed and replaced corresponding 
/*   functionality.							Hamsavally(CGS)   06 Oct,2000
/*
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                          
/*                                                                           
/*   $Revision::   1.1      $                                                
/*                                                                          
/*****************************************************************************/
/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH NELLCOR PURITAN BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF NELLCOR PURITAN BENNETT CORPORATION.
 
Copyright (c) 1999, Mallinckrodt Nellcor Puritan Bennett
All rights reserved.
*******************************************************************************/

#ifndef _TESTPNL
#define _TESTPNL



extern void DisplayTestPanel(void);
extern void SetCheckXTest(int index, int state);
extern void ResetSeqDisplay(void);
extern void UpdateTestListing(int index,int status);
extern void SystemWait(int panel, int control);
extern void SetSystemStatus (int msg);
extern void AutoScrollResults(void);
extern void DrawResultsOptMenu(void);

enum CHK_X_STATE{
	CX_CLEAR =0,   
	CX_FAIL,
	CX_PASS
};

#endif // #ifndef _TESTPNL
