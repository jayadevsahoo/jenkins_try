#ifndef StaticMechanicsBuffer_HH
#define StaticMechanicsBuffer_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: StaticMechanicsBuffer - circular buffer for static mechanics
//		storage.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/StaticMechanicsBuffer.hhv   10.7   08/17/07 09:43:36   pvcs  
//
//@ Modification-Log
//
//  Revision: 003  By: rhj     Date:  06-May-2010   SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added GetNetLeakFlowBuffer() and GetExhWyePressureBuffer()
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  syw    Date:  05-Mar-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"

//@ Usage-Classes

//@ End-Usage

const Uint32 LR_BUFFER_SIZE = 20 ;
const Uint32 INSP_FLOW_BUFFER_SIZE = 20 ;
const Uint32 PLATEAU_PRESS_BUFFER_SIZE = 20 ;
const Uint32 EXH_PRESS_BUFFER_SIZE = 20 ;
const Uint32 EXH_WYE_PRESS_BUFFER_SIZE = 10;
const Uint32 NET_LEAK_FLOW_BUFFER_SIZE = 10;

class StaticMechanicsBuffer {
  public:
    ~StaticMechanicsBuffer( void) ;

    static void SoftFault( const SoftFaultID softFaultID,
				 const Uint32      lineNumber,
				 const char*       pFileName  = NULL, 
				 const char*       pPredicate = NULL) ;

	static Real32 *GetTimeBuffer( void) ;
	static Real32 *GetPressureBuffer( void) ;
	static Real32 *GetTime2Buffer( void) ;
	static Real32 *GetTimePressureBuffer( void) ;
	static Real32 *GetInspFlowBuffer( void) ;
	static Real32 *GetPlateauPressureBuffer( void) ;
	static Real32 *GetExhPressureBuffer( void) ;
	static Real32 *GetExhWyePressureBuffer( void) ;
	static Real32 *GetNetLeakFlowBuffer(void);
	
	static inline Uint32 GetLinearRegressionBufferSize( void) ;
	static inline Uint32 GetInspFlowBufferSize( void) ;
	static inline Uint32 GetPlateauPressBufferSize( void) ;
	static inline Uint32 GetExhPressBufferSize( void) ;
	static inline Uint32 GetNetLeakFlowBufferSize( void) ;
	static inline Uint32 GetExhWyePressBufferSize( void) ;
	
  protected:

  private:
    StaticMechanicsBuffer( void) ;							// not implemented...
    StaticMechanicsBuffer( const StaticMechanicsBuffer&) ;  // not implemented...
    void   operator=( const StaticMechanicsBuffer&) ; 	    // not implemented...
	
} ;

#include "StaticMechanicsBuffer.in"

#endif // StaticMechanicsBuffer_HH 
