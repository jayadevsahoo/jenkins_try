// SWATGuiCore.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "SWATGuiCore.h"
#include "Luna.h"
#include "SWATGuiFunctions.h"

int SWATGUICORE_API luaopen_swat_guicore(lua_State* L)
{
	Luna<SWATGuiFunctions>::Register(L);
	return 0;
}