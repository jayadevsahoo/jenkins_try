#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SpontScheduler - the active BreathPhaseScheduler when the 
//  current mode is SPONT.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for scheduling breath phases during SPONT mode.
// The class is also responsible for relinquishing control to the next valid
// scheduler and for assuming control when SPONT mode is requested.  SPONT mode
// can be requested by the operator or upon emergency mode recovery and
// power-up sequence, when transition to the previous mode is required.  Note
// that mode transition, as a response for setting change, occurs once the new
// mode setting is accepted (by the operator).  This scheduler is responsible
// for enabling valid mode triggers for SPONT mode and for enabling breath
// triggers that are not under the breath phase direct control. The
// SpontScheduler is responsible for handling user events like 100% O2, manual
// inspiration request, etc. and for handling setting change events like rate
// and mode changes.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates all the breathing rules specified for SPONT mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
// The class implements methods for breath and mode transitions and for
// breath data updates. The SPONT scheduler is responsible for:
// - Determining the next breath when a breath trigger becomes active.
// - Initiating the activities required for the proper start of a new breath:
//    phase in settings, update breath record, set breath triggers, 
//    enable breath triggers, and phase out the previous breath phase, 
// - Relinquishing control when a mode trigger becomes active.
//    Activities when control is relinquished include: 
//    disable out of date breath triggers, determine which is the next
//    valid scheduler, enable breath triggers for the next scheduler,
//    and instruct the next scheduler to take control.
// - Taking control when the previous scheduler relinquishes control. 
//    Activities while taking control include:
//    register self with the BreathPhaseScheduler object,  and activate
//    the mode trigger list.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class exists at any given time.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SpontScheduler.ccv   25.0.4.0   19 Nov 2013 14:00:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 047   By:   gdc    Date: 01-May-2009     SCR Number: 6488
//  Project:  840S
//  Description:
//		Fixed SIMV-SPONT-SIMV transition problem caused by using mode 
// 		setting value from phased-in context before the mode setting
// 		was phased in by an inspiration.
//
//  Revision: 046   By: rhj    Date: 05-Jan-2009    SCR Number: 6451
//  Project:  840S
//  Description:
//      Added missing net flow backup inspiratory triggers.
// 
//  Revision: 045   By: rhj    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Added net flow backup inspiratory trigger.
//
//  Revision: 044   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 043   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 042   By: gdc   Date:  23-Feb-2007    SCR Number: 6307
//  Project:  RESPM
//  Description:
//		Added missing RM requirement numbers.
//
//  Revision: 041   By: gdc   Date:  03-Nov-2006    SCR Number: 6305
//  Project:  RESPM
//  Description:
//		Added PEEP recovery breath after SVC inspiratory phase is 
// 		canceled to restart the Apnea timer.
//
//  Revision: 040   By: gdc   Date:  25-Oct-2006    SCR Number: 6293
//  Project:  RESPM
//  Description:
//		Changed to use PEEP recovery breath where applicable to restart
//      breath timers after an RM maneuver.
//
//  Revision: 039   By: gdc   Date:  26-Oct-2006    SCR Number: 6290
//  Project:  RESPM
//  Description:
//		Changed to issue PEEP recovery breath after NIF maneuver and
//		after canceled VCM or NIF. This restarts the Apnea timer.
//		Also, changes made per code review.
//
//  Revision: 038   By: gdc   Date:  25-Oct-2006    SCR Number: 6296
//  Project:  RESPM
//  Description:
//		Changed to pressure expiratory trigger to cycle out of
// 		vital capacity inspiratory phase.
//
//  Revision: 037   By: gdc   Date:  13-Oct-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added scheduling of RM maneuvers.
//
//  Revision: 036   By: erm   Date:  10-Jul-2003    DR Number: 6070
//  Project:  PAV
//  Description:
//		Modified settingChanged_ to be concern to Mode changes only
//
//  Revision: 035   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added PavPhase and pav pause handle.
//
//  Revision: 034  By: jja     Date:  17-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added handle for VS.
//
//  Revision: 033  By:  healey    Date:  14-Jan-99    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//          Added checking for TC support type to set up corresponding phase
//
//  Revision: 032  By:  healey    Date:  08-Jan-99    DR Number: DCS 5323
//       Project:  ATC
//       Description:
//          Remove #define private public
//
//  Revision: 031  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//          ATC initial release
//
//  Revision: 030  By:  yyy    Date:  18-Nov-98    DR Number: 5263
//       Project:  Sigma (R8027)
//       Description:
//			Calling resetBiLevelModeChangeCondition() to reset the
//			biLevelNoticeWasReceived_ flag during startInspiration of Spont mode.
//
//  Revision: 029  By:  syw    Date:  17-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (R8027)
//       Description:
//			Call RPeep.setPeepChange if breath is to be Peep Recovery.
//
//  Revision: 028  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling for Inspiratory Pause event and
//             Inspiratoty Pause phase.
//
//  Revision: 027  By: iv    Date:  15-Oct-1997   DR Number: DCS 2556
//  	Project:  Sigma (R8027)
//		Description:
//          In determineBreathPhase(), for the case of BreathPhaseType=EXHALATION,
//          and when triggerId=AsapTrigger, Added the SpontSchedulerId to the
//          assertion for valid previous schedulers.
//
//  Revision: 026  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 025  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 024  By:  iv    Date:  15-Apr-1997    DR Number: DCS 799
//       Project:  Sigma (840)
//       Description:
//             Code was change per DCS 799.
//
//  Revision: 023  By:  iv    Date:  03-Apr-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per unit test peer review
//             Deleted duplicate testable item.
//
//  Revision: 022  By:  iv    Date:  12-Feb-1997    DR Number: DCS 1613
//       Project:  Sigma (840)
//       Description:
//             In determineBreathPhase(), disable the asap insp trigger after it
//             triggers during exhalation.
//
//  Revision: 021  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 020  By:  iv    Date:  03-Dec-1996    DR Number: DCS 1608, 1541
//       Project:  Sigma (R8027)
//       Description:
//             determineBreathPhase(), case = NON_BREATHING : eliminate assertion
//             and add initialization for peep recovery state.
//             Prepare code for inspections.
//
//  Revision: 019  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 018  By:  iv    Date:  24-Oct-1996    DR Number: DCS 1456
//       Project:  Sigma (R8027)
//       Description:
//             determinBreathPhase(): Added a pre-condition to include
//             Apnea and SafetyPcv schedulers to the exhalation case when
//             the trigger is AsapInsp.
//
//  Revision: 017  By:  iv    Date:  06-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Deleted class assertion in determineBreathPhase() for NON_BREATHING
//             case.
//
//  Revision: 016  By:  iv    Date:  30-Sep-1996    DR Number: DCS 1331
//       Project:  Sigma (R8027)
//       Description:
//             Added a call: RBreathTriggerMediator.changeBreathListName()
//             in determineBreathPhase(), where phase is exhalation and
//             trigger id is asap insp.
//
//  Revision: 015  By:  sp    Date:  26-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Changed PeepRecoveryTrigger to PeepRecoveryInspTrigger.
//
//  Revision: 014 By:  iv   Date:   11-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//               Removed the Boolean noInspiration argument from newExhalation_().
//
//  Revision: 013 By:  iv   Date:   09-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//               Added a check for peep recovery trigger in determineBreathPhase(),
//             and disable the trigger on relinquishControl().
//
//  Revision: 012 By:  iv   Date:   13-Aug-1996    DR Number: DCS 1218
//       Project:  Sigma (R8027)
//       Description:
//               Changed the range of SettingId values as checked in a setting change
//             call back assertion.
//
//  Revision: 011 By:  iv   Date:   02-Aug-1996    DR Number: DCS 10035, 1044, 1046
//       Project:  Sigma (R8027)
//       Description:
//             Incorporated PEEP recovery after OSC and Powerup.
//
//  Revision: 010 By:  iv   Date:   17-Jul-1996    DR Number: DCS 1056
//       Project:  Sigma (R8027)
//       Description:
//             Changed the response for BreathPhaseType::EXHALATION case in
//             determineBreathPhase() when ASAP_INSP trigger is detected - check
//             for AC and SIMV as possible previous schedulers.
//
//  Revision: 009 By:  iv   Date:   21-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Added a Boolean isPeepRecovery to newBreathRecord(...) call.
//
//  Revision: 008 By:  iv   Date:   16-Jun-1996    DR Number: DCS 967, 1055
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated a redundant call to BreathSet.newBreathRecord(), before
//             a call is made to start inspiration.
//
//  Revision: 007 By:  iv   Date:   04-Jun-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Fixed call to base class takeControl() from takeControl().
//
//  Revision: 006 By:  iv   Date:   07-May-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Changed takeControl(), determineBreathPhase(), and
//             startInspiration_() methods - to allow for a PEEP recovery
//             breath phase to take place on transitions from disconnect,
//             standby, and SVO.
//
//  Revision: 005 By:  iv   Date:   15-Apr-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 004 By:  iv   Date:   21-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 003 By:  kam   Date:  06-Nov-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from takeControl().
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem
//             and for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "SpontScheduler.hh"
#include "SchedulerRefs.hh"
#include "BreathMiscRefs.hh"
#include "PhaseRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "BdDiscreteValues.hh"

//@ Usage-Classes
#include "ApneaInterval.hh"
#include "AsapInspTrigger.hh"
#include "BdSystemStateHandler.hh"
#include "BiLevelScheduler.hh"
#include "BreathPhase.hh"
#include "BreathSet.hh"
#include "BreathTrigger.hh"
#include "BreathTriggerMediator.hh"
#include "ExpPauseManeuver.hh"
#include "ImmediateBreathTrigger.hh"
#include "InspPauseManeuver.hh"
#include "ManeuverRefs.hh"
#include "ModeTrigger.hh"
#include "ModeTriggerMediator.hh"
#include "NifManeuver.hh"
#include "NifPausePhase.hh"
#include "O2Mixture.hh"
#include "P100Maneuver.hh"
#include "P100PausePhase.hh"
#include "P100Trigger.hh"
#include "PavManager.hh"
#include "Peep.hh"
#include "PeepRecoveryInspTrigger.hh"
#include "PendingContextHandle.hh"
#include "PhasedInContextHandle.hh"
#include "PhaseInEvent.hh"
#include "PressureInspTrigger.hh"
#include "SettingConstants.hh"
#include "Trigger.hh"
#include "VcmPsvPhase.hh"
#include "VitalCapacityManeuver.hh"
#include "VolumeTargetedManager.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SpontScheduler() [Constructor]
//
//@ Interface-Description
//        Constructor.
// $[04119] $[04125] $[04130] $[04141]
// The constructor takes no arguments. it constructs the object with the proper id.
// The list of valid mode triggers, used by the mode trigger mediator is
// initialized as well. The triggers are ordered based on priority - the most urgent
// is first. The list also guarantees that when a trigger fires - the triggers
// that follow it do not have to be processed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor invokes the base class constructor with the SPONT id
// argument. It initializes the private data member pModeTriggerList_.
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
// The number of elements on the trigger list is asserted
// to be MAX_NUM_SPONT_TRIGGERS and MAX_NUM_SPONT_TRIGGERS to be less than
// MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_SPONT_TRIGGERS = 5;

SpontScheduler::SpontScheduler(void)
: BreathPhaseScheduler(SchedulerId::SPONT),
pPrevSpontPhase_(NULL),
pLostSpontPhase_(NULL),
spontModeSetting_(ModeValue::SPONT_MODE_VALUE)
{
	// $[TI1]
	CALL_TRACE("SpontScheduler(void)");

	Int32 ii = 0;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RDisconnectTrigger;
	pModeTriggerList_[ii++] = (ModeTrigger*)&ROcclusionTrigger;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RApneaTrigger;
	pModeTriggerList_[ii++] = (ModeTrigger*)&RSettingChangeModeTrigger;
	pModeTriggerList_[ii] = (ModeTrigger*)NULL;

	CLASS_ASSERTION(ii == MAX_NUM_SPONT_TRIGGERS &&
					MAX_NUM_SPONT_TRIGGERS < MAX_MODE_TRIGGERS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SpontScheduler()  [Default Destructor]
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
SpontScheduler::~SpontScheduler(void)
{
	CALL_TRACE("SpontScheduler(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
// This method accepts a breath trigger as an argument. it determines
// which breath phase to start, based on the trigger id, the current phase,
// and the vent settings.
// Once the next breath phase is determined, a call to start inspiration or
// to start exhalation is performed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The activities handled by this method are dependent on the current breath
// phase. The current breath phase is retrieved from the BreathPhase object.  A
// switch statement is implemented for the various breath phases which are:
// NON_BREATHING, EXHALATION, and INSPIRATION.  Inspiration begins by calling
// the method startInspiration_(). Before that method is called, the breath
// type is set (e.g. CONTROL, ASSIST, etc.)  When current phase is EXHALATION,
// If a peep reduction trigger is detected then exhalation (with null
// inspiration flag) is delivered, followed by enabling a asap insp. trigger. 
// Otherwise, a call is made to start inspiration. A CONTROL breath type is
// set for OPERATOR_INSP trigger id.  When current phase is NON_BREATHING, the
// breathType is set to SPONT to start peep recovery inspiration.
//---------------------------------------------------------------------
//@ PreCondition
// For each case in the switch statement, the expected range of valid
// triggers and/or previous schedulers is checked. The valid range for
// phase type is also checked.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
SpontScheduler::determineBreathPhase(const BreathTrigger& breathTrigger) 
{

	CALL_TRACE("SpontScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

	// the type of breath to communicate to the user
	::BreathType breathType;

	const Trigger::TriggerId triggerId = breathTrigger.getId();

	// Get the scheduler id from the current breath record. Note that the current
	// breath record stores the id of the scheduler that was active at the time the
	// record was created.  
	const SchedulerId::SchedulerIdValue previousSchedulerId =
	(RBreathSet.getCurrentBreathRecord())->getSchedulerId();
	// A pointer to the current breath phase
	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
	// determine the current phase type (e.g. EXHALATION, INSPIRATION)
	const BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();

	switch ( phase )
	{
		case BreathPhaseType::NON_BREATHING:
			{
				// $[TI1]        
				// $[04321]Deliver a peep recovery inspiration:
				BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
				startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
			}
			break;

		case BreathPhaseType::EXHALATION:
			{
				if ( triggerId == Trigger::ARM_MANEUVER )
				{
					if ( RNifManeuver.getManeuverState() == PauseTypes::PENDING ||
						 RVitalCapacityManeuver.getManeuverState() == PauseTypes::PENDING )
					{
						// $[RM24010] \3\ During the exhalation phase recognize a NIF maneuver as pending (if not already pending), enable the backup pressure trigger but disable pressure and flow triggers.
						// $[RM12205] When the VC maneuver is armed, only the backup pressure trigger shall be enabled.

						// this disables all triggers including the control breath trigger
						RBreathTriggerMediator.resetTriggerList();
						// retain expiratory triggers list
						RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);
						// enable pressure trigger for NIF/VC breath phase -- all others disabled 
						RPressureBackupInspTrigger.enable();
						// -- still in exhalation as required to activate a NIF/VC maneuver
					}
					else
					{
						// no maneuver pending --- the arm maneuver trigger wasn't disarmed properly
						CLASS_ASSERTION_FAILURE();
					}
				}
				else if ( triggerId == Trigger::DISARM_MANEUVER )
				{
					// $[RM12078] A NIF maneuver shall be cancelled...
					// $[RM12085] An armed Vital Capacity maneuver shall be aborted...

					// deliver a PEEP recovery breath to trigger VCM out of IDLE PENDING
					// restart the breath timers and restart the Apnea timer for NIF and VCM
					BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
					startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
				}
				else if ( triggerId == Trigger::PRESSURE_BACKUP_INSP &&
						  RVitalCapacityManeuver.getManeuverState() == PauseTypes::PENDING )
				{
					// $[RM12207] The maneuver shall transition to active at the beginning of the next patient inspiratory effort signaled by the backup pressure trigger.

					// patient effort sensed with vital capacity maneuver pending
					RBreathTriggerMediator.resetTriggerList();
					// set up the trigger list for the VC inspiration
					RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::VCM_INSP_LIST);
					// current breath phase is given a chance to phase-out
					pBreathPhase->relinquishControl(breathTrigger);
					// register a Vital Capacity Maneuver PSV phase:
					BreathPhase::SetCurrentBreathPhase(RVcmPsvPhase, breathTrigger);
					//Create a new breath record
					RBreathSet.newBreathRecord(SchedulerId::SPONT, MandTypeValue::PCV_MAND_TYPE, ::SPONT,
											   BreathPhaseType::VITAL_CAPACITY_INSP, FALSE);
					// start the vital capacity inspiratory phase   			
					BreathPhase::NewBreath() ;
				}
				else if ( triggerId == Trigger::PRESSURE_BACKUP_INSP
						  && RNifManeuver.getManeuverState() == PauseTypes::PENDING )
				{
					// $[RM12207] NIF shall transition to active at the beginning of the next patient inspiratory effort signaled by the backup pressure trigger. 

					// this is a patient triggered breath with NIF pause pending
					// NIF pause is triggered on patient effort detected by backup pressure trigger (PEEP-2.0)

					//start NIF maneuver...

					// Reset currently active triggers 
					RBreathTriggerMediator.resetTriggerList();
					// Change triggers to those active during an active NIF
					RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::NIF_PAUSE_LIST);
					// current breath phase is given a chance to phase-out
					pBreathPhase->relinquishControl(breathTrigger);
					// register a NIF pause phase:
					BreathPhase::SetCurrentBreathPhase(RNifPausePhase, breathTrigger);
					// update the breath record
					(RBreathSet.getCurrentBreathRecord())->startNifPause();
					// start the pause
					BreathPhase::NewBreath() ;
				}
				else if ( triggerId == Trigger::P100_MANEUVER )
				{
					// $[RM12306] P100 shall transition to active when the P100 inspiratory trigger conditions (see Control Specification) have been met.

					//start P100 maneuver...

					// Reset currently active triggers
					RBreathTriggerMediator.resetTriggerList();
					// Change triggers to those active during an active P100 (same as NIF)
					RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::NIF_PAUSE_LIST);
					// current breath phase is given a chance to phase-out
					pBreathPhase->relinquishControl(breathTrigger);
					// register a P100 pause phase:
					BreathPhase::SetCurrentBreathPhase(RP100PausePhase, breathTrigger);
					// update the breath record
					(RBreathSet.getCurrentBreathRecord())->startP100Pause();
					// start the pause
					BreathPhase::NewBreath() ;
				}		 // $[TI2]
				else if ( triggerId == Trigger::PEEP_REDUCTION_EXP )
				{
					// $[TI2.4]
					startExhalation_(breathTrigger, pBreathPhase, TRUE);

					// On Peep reduction transition mode, the apnea interval should be extended
					// if necessary
					RApneaInterval.extendIntervalForPeepReduction();
				}
				else if ( triggerId == Trigger::PEEP_RECOVERY )
				{
					// $[TI2.3]
					breathType = ::SPONT;
					CLASS_ASSERTION( BreathPhaseScheduler::PeepRecovery_ ==
									 BreathPhaseScheduler::START);
					startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
				}
				else if ( (triggerId == Trigger::ASAP_INSP ||
						   triggerId == Trigger::PEEP_RECOVERY_MAND_INSP)
						  && RVitalCapacityManeuver.getManeuverState() != PauseTypes::IDLE_PENDING )
				{
					// $[TI2.1]
					RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);
					((BreathTrigger&)RAsapInspTrigger).disable();
					((BreathTrigger&)RPeepRecoveryMandInspTrigger).disable();
					// This case is possible when two mode changes are done in a row during a
					// long inspiration and the last mode accepted was Spont. It is also possible
					// when a mode transition is made during peep recovery exhalation (this case
					// may involve the Apnea and the SafetyPcv schedulers as well as AC and SIMV
					// schedulers. In that case there is no need to start a new exhalation since
					// it is already in progress 
					CLASS_ASSERTION(previousSchedulerId == SchedulerId::AC   ||
									previousSchedulerId == SchedulerId::SIMV ||
									previousSchedulerId == SchedulerId::BILEVEL ||
									previousSchedulerId == SchedulerId::SAFETY_PCV ||
									previousSchedulerId == SchedulerId::SPONT ||
									previousSchedulerId == SchedulerId::APNEA);
				}
				else
				{
					// $[TI2.2]
					// a breath is to be delivered
					if ( RVitalCapacityManeuver.getManeuverState() == PauseTypes::IDLE_PENDING )
					{
						// $[RM12208] The VC maneuver shall allow for a full exhalation effort after the inspiration.
						if ( (triggerId == Trigger::OPERATOR_INSP) )
						{
							// $[RM12233] A manual inspiration request shall be accepted during the expiratory phase...
							// The operator insp trigger fires only when not in restricted phase of exhalation
							RVitalCapacityManeuver.complete();
							breathType = ::CONTROL;
						}
						else if ( (triggerId == Trigger::NET_FLOW_INSP) || 
								  (triggerId == Trigger::NET_FLOW_BACKUP_INSP) ||
								  (triggerId == Trigger::PRESSURE_BACKUP_INSP) || 
								  (triggerId == Trigger::PRESSURE_INSP) ||
								  (triggerId == Trigger::ASAP_INSP) )
						{
							// $[RM12064] upon VCM completion, a PEEP Recovery breath shall be delivered, followed by a resumption of normal breath delivery. 
							// $[RM12086] An active Vital Capacity maneuver shall be considered to be successfully completed if a subsequent patient inspiratory effort occurs
							RVitalCapacityManeuver.complete();
							BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
							breathType = ::SPONT;
						}
						else
						{
							AUX_CLASS_ASSERTION_FAILURE(triggerId);
						}            
					}
					// set the breath type: $[04249] $[03008] 
					else if ( triggerId == Trigger::OPERATOR_INSP )
					{
						// $[TI2.2.1]
						//This is a VIM breath
						breathType = ::CONTROL;
					}
					else if ( (triggerId == Trigger::NET_FLOW_INSP) || 
							  (triggerId == Trigger::PRESSURE_BACKUP_INSP) || 
							  (triggerId == Trigger::NET_FLOW_BACKUP_INSP) ||
							  (triggerId == Trigger::PRESSURE_INSP) )
					{
						// $[TI2.2.2]
						breathType = ::SPONT;
					}
					else
					{
						// $[TI2.2.3]
						AUX_CLASS_ASSERTION_FAILURE(triggerId);
					}            
					startInspiration_(breathTrigger, pBreathPhase, breathType);
				}    
			}
			break;

		case BreathPhaseType::INSPIRATION:
			{
				// $[TI3]
				//The trigger should be one of the following legal triggers (note that
				//inspiratory phase can be a spont inspiration during mode transition):
				AUX_CLASS_PRE_CONDITION(
									   (triggerId == Trigger::DELIVERED_FLOW_EXP) || 
									   (triggerId == Trigger::HIGH_PRESS_COMP_EXP) || 
									   (triggerId == Trigger::LUNG_FLOW_EXP) || 
									   (triggerId == Trigger::LUNG_VOLUME_EXP) || 
									   (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) || 
									   (triggerId == Trigger::PRESSURE_EXP) || 
									   (triggerId == Trigger::BACKUP_TIME_EXP) || 
									   (triggerId == Trigger::IMMEDIATE_EXP) || 
									   (triggerId == Trigger::HIGH_VENT_PRESSURE_EXP), triggerId );

				RVolumeTargetedManager.setExhalationTrigger( triggerId, pBreathPhase) ;

				// check conditions for inspiratory pause
				if ( (triggerId == Trigger::IMMEDIATE_EXP) &&
					 RInspPauseManeuver.isManeuverAllowed() &&
					 (RInspiratoryPauseEvent.getEventStatus() == EventData::PENDING ||
					  RInspPauseManeuver.getManeuverState() == PauseTypes::ACTIVE) )
				{
					// $[TI3.1]
					//Phase in new settings (for start of exhalation or inspiratory pause)
					PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_EXHALATION);

					//start inspiratory pause when in mandatory breath....
					// $[BL04005]
					// $[BL04006]
					// $[BL04009] :a transition to inspiratory pause phase
					// Reset currently active triggers and setup triggers that are active during
					// pause:
					RBreathTriggerMediator.resetTriggerList();
					RBreathTriggerMediator.changeBreathListName(
															   BreathTriggerMediator::INSP_PAUSE_LIST);
					// notify the user that inspiratory pause is active:
					RInspiratoryPauseEvent.setEventStatus(EventData::ACTIVE);
					// current breath phase is given a chance to phase-out
					pBreathPhase->relinquishControl(breathTrigger);
					// register an inspiratory pause phase:
					BreathPhase::SetCurrentBreathPhase((BreathPhase&)RInspiratoryPausePhase,
													   breathTrigger);
					// update the breath record
					(RBreathSet.getCurrentBreathRecord())->startInspiratoryPause();
					// start the pause
					BreathPhase::NewBreath() ;
				}
				else if ( RPavManager.isPerformPavPause( pBreathPhase, triggerId) )
				{
					// $[TI3.3]
					// Phase in new settings (for start of exhalation or inspiratory pause)
					PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_EXHALATION);

					RBreathTriggerMediator.resetTriggerList();
					RBreathTriggerMediator.changeBreathListName(
															   BreathTriggerMediator::PAV_INSP_PAUSE_LIST);
					// current breath phase is given a chance to phase-out
					pBreathPhase->relinquishControl(breathTrigger);
					// register an inspiratory pause phase:
					BreathPhase::SetCurrentBreathPhase((BreathPhase&)RPavPausePhase,
													   breathTrigger);
					// update the breath record
					(RBreathSet.getCurrentBreathRecord())->startPavInspiratoryPause();
					BreathPhase::NewBreath() ;
				}
				else
				{	 // $[TI3.2]
					startExhalation_(breathTrigger, pBreathPhase);
				}
			}
			break;

		case BreathPhaseType::INSPIRATORY_PAUSE: 
		case BreathPhaseType::BILEVEL_PAUSE_PHASE: 
			{
				// $[TI5]
				// set the pause event status to notify user of insp pause status:
				EventData::EventStatus eventStatus;
				// $[TI5.1] COMPLETE
				// $[TI5.2] CANCEL
				eventStatus = (triggerId==Trigger::PAUSE_COMPLETE) ?
							  EventData::COMPLETE:EventData::CANCEL;
				RInspiratoryPauseEvent.setEventStatus(eventStatus);

				//The trigger should have been one of the above triggers
				// $[BL04078] :m terminate active manual inspiratory pause
				// $[BL04012] :m terminate active auto inspiratory pause
				AUX_CLASS_ASSERTION( (triggerId == Trigger::PAUSE_TIMEOUT) || 
									 (triggerId == Trigger::PAUSE_COMPLETE) ||
									 (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) || 
									 (triggerId == Trigger::PAUSE_PRESS) ||
									 (triggerId == Trigger::IMMEDIATE_BREATH),
									 triggerId );

				startExhalation_(breathTrigger, pBreathPhase);
			}
			break;

		case BreathPhaseType::PAV_INSPIRATORY_PAUSE:
			{
				// $[TI6]
				AUX_CLASS_ASSERTION( triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP ||
									 triggerId == Trigger::IMMEDIATE_BREATH ||
									 triggerId == Trigger::PAUSE_COMPLETE,
									 triggerId ) ;
				startExhalation_(breathTrigger, pBreathPhase);
			}
			break ;

		case BreathPhaseType::NIF_PAUSE: 
			{

				if ( triggerId == Trigger::PAUSE_COMPLETE ||
					 triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_INSP ||
					 triggerId == Trigger::IMMEDIATE_BREATH )
				{
					// $[RM24010] Upon NIF completion, resume normal ventilation
					BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
					startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
				}
				else if ( triggerId == Trigger::OPERATOR_INSP )
				{
					// $[RM12110] An active NIF maneuver shall be successfully terminated upon detecting a Manual Inspiration request.
					startInspiration_(breathTrigger, pBreathPhase, ::CONTROL);
				}
				else
				{
					AUX_CLASS_ASSERTION_FAILURE( triggerId );
				}

			}
			break;

		case BreathPhaseType::P100_PAUSE: 
			{
				// $[RM12096] When an active P100 maneuver terminates, breath delivery shall return to the previous scheduler.
				if ( triggerId == Trigger::P100_COMPLETE ||
					 triggerId == Trigger::PAUSE_COMPLETE ||
					 triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_INSP ||
					 triggerId == Trigger::IMMEDIATE_BREATH )
				{
					// $[RM12096] P100 complete - deliver a SPONT breath
					startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
				}
				else if ( triggerId == Trigger::OPERATOR_INSP )
				{
					// $[RM12094] A P100 maneuver shall be canceled for a manual insp request
					startInspiration_(breathTrigger, pBreathPhase, ::CONTROL);
				}
				else
				{
					AUX_CLASS_ASSERTION_FAILURE( triggerId );
				}
			}
			break;

		case BreathPhaseType::VITAL_CAPACITY_INSP: 
			{
				// Check the validity of the triggers. 
				AUX_CLASS_PRE_CONDITION( triggerId == Trigger::PRESSURE_EXP || 
										 triggerId == Trigger::PAUSE_COMPLETE ||
										 triggerId == Trigger::HIGH_PRESS_COMP_EXP || 
										 triggerId == Trigger::LUNG_FLOW_EXP || 
										 triggerId == Trigger::LUNG_VOLUME_EXP || 
										 triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP || 
										 triggerId == Trigger::IMMEDIATE_EXP || 
										 triggerId == Trigger::HIGH_VENT_PRESSURE_EXP,
										 triggerId) ;

				// if this was a "normal" termination of inspiration then the 
				// maneuver can proceed with data collection otherwise 
				// cancel the maneuver.

				if ( triggerId != Trigger::PRESSURE_EXP )
				{
					// $[RM12062] A Vital Capacity maneuver shall be canceled if...

					// If the VC inspiratory phase was not completed normally then
					// restart the breath timer here, otherwise the breath timer will
					// be restarted after the VitalCapacityManeuver collects data from
					// the exhalation phase and delivers a PEEP recovery breath.
					// Restart the breath timer by delivering PEEP recovery breath
					// In SPONT, even though there are no breath timers per se, the 
					// Apnea timer is restarted here with the PEEP recovery breath

					BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
					startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
				}
				else
				{
					// $[RM12208] The VC maneuver shall allow for a full exhalation effort after the inspiration.
					startExhalation_(breathTrigger, pBreathPhase);
				}
			}
			break;

		default:
			{
				// $[TI4]
				AUX_CLASS_ASSERTION_FAILURE(phase);
			}
			break;

	} //switch
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
// The method accepts a mode trigger reference as an argument and has no return value. 
// The asap breath trigger is enabled on transition to SIMV, AC, and Apnea modes.
// The immediate breath trigger is enabled on transition to emergency modes.
// Since Spont mode may follow SVO scheduler, a NON_BREATHING breath phase
// is possible. In that case, any breath delivery is avoided until the completion
// of the transitional NON_BREATHING phase that guarantees a complete closure of
// the safety valve. This is implemented by not setting any breath triggers when a
// transition to a breathing mode is detected.  
// When an immediate breath trigger is enabled, the rest of the triggers on the
// active list are disabled to ensure safe transition to the emergency mode.
// The argument passed, provides the method with the information
// required to determine the next scheduler. Before the next scheduler takes control,
// it resets all mode triggers on its list.
//---------------------------------------------------------------------
//@ Implementation-Description
// Individual breath triggers are enabled and disabled by directly accessing
// the trigger instance.  Disabling all triggers on the current list is
// accomplished by using the only one instance of the breath trigger mediator,
// calling the method resetTriggerList().  The mode trigger id passed as an
// argument is checked to determine the requested scheduler which then
// instructed to take control.
//---------------------------------------------------------------------
//@ PreCondition
// The mode setting is checked for valid range of values.  The trigger id is
// checked for valid range of mode triggers.  When the mode trigger is either
// apnea or mode change, the non breathing phase is checked to be
// SafetyValveClosePhase.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================

void
SpontScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{
	CALL_TRACE("SpontScheduler::relinquishControl(const ModeTrigger& modeTrigger)");

	const Trigger::TriggerId modeTriggerId = modeTrigger.getId();

	// determine the current phase type (e.g. EXHALATION, INSPIRATION)
	const BreathPhaseType::PhaseType phase = BreathRecord::GetPhaseType();

	// disable the peep recovery trigger, to avoid its interference with
	// the next scheduler.
	((BreathTrigger&)RPeepRecoveryInspTrigger).disable();

	// reset all mode triggers for this scheduler
	RModeTriggerMediator.resetTriggerList();

	if ( (modeTriggerId == Trigger::SETTING_CHANGE_MODE) ||
		 (modeTriggerId == Trigger::APNEA) )
	{
		// $[TI1]
		if ( phase != BreathPhaseType::NON_BREATHING )
		{
			// $[TI1.1]
			//$[04263]
			// set the asap trigger
			((BreathTrigger&)RAsapInspTrigger).enable();
		}
		else // phase == BreathPhaseType::NON_BREATHING
		{
			// $[TI1.2]
			// If the breath phase is a non breathing phase, it is the
			// safety valve closed phase that is in progress, and since a time trigger
			// is already set to terminate that phase, there is no need to set any
			// breath trigger.
			CLASS_PRE_CONDITION(BreathPhase::GetCurrentBreathPhase()
								== (BreathPhase*)&RSafetyValveClosePhase);
		}
	}
	else
	{
		// $[TI2]
		//Reset currently active triggers and then enable the only valid breath
		// trigger for that transition.
		RBreathTriggerMediator.resetTriggerList();
		((BreathTrigger&)RImmediateBreathTrigger).enable();
		// make sure trigger is valid:
		AUX_CLASS_PRE_CONDITION(
							   (modeTriggerId == Trigger::OCCLUSION) ||
							   (modeTriggerId == Trigger::DISCONNECT) ||
							   (modeTriggerId == Trigger::SVO), modeTriggerId );
	}

	// store the current phase, as being lost with the scheduler's
	// relinquishing of control...
	pLostSpontPhase_ = pPrevSpontPhase_;

	if ( pPrevSpontPhase_ != NULL )
	{	// $[TI18]
		// notify SpontScheduler's previous inspiratory phase that it is no
		// longer active, and why...
		pPrevSpontPhase_->deactivate(modeTriggerId);

		pPrevSpontPhase_ = NULL;
	}	// $[TI19]

	if ( modeTriggerId == Trigger::SETTING_CHANGE_MODE )
	{
		// $[TI3]
		// check what mode to switch to
		const DiscreteValue mode = PendingContextHandle::GetDiscreteValue(SettingId::MODE);

		if ( mode == ModeValue::AC_MODE_VALUE )
		{
			// $[TI3.1]
			// $[04230] $[04231]
			((BreathPhaseScheduler&)RAcScheduler).takeControl(*this);
		}
		else if ( mode == ModeValue::SIMV_MODE_VALUE )
		{
			// $[TI3.2]
			// $[04247] $[04248]
			((BreathPhaseScheduler&)RSimvScheduler).takeControl(*this);
		}
		else if ( mode == ModeValue::BILEVEL_MODE_VALUE )
		{
			// $[TI3.4]
			// $[04253]
			((BreathPhaseScheduler&)RBiLevelScheduler).takeControl(*this);
		}
		else if ( mode == ModeValue::SPONT_MODE_VALUE )
		{
			// transition from CPAP to SPONT
			((BreathPhaseScheduler&)RSpontScheduler).takeControl(*this);
		}
		else if ( mode == ModeValue::CPAP_MODE_VALUE )
		{
			// transition from SPONT to CPAP
			((BreathPhaseScheduler&)RSpontScheduler).takeControl(*this);
		}
		else
		{
			// $[TI3.3]
			// unexpected mode value...
			AUX_CLASS_ASSERTION_FAILURE(mode);
		}
	}
	else if ( modeTriggerId == Trigger::APNEA )
	{
		// $[TI4]
		((BreathPhaseScheduler&)RApneaScheduler).takeControl(*this);
	}
	else if ( modeTriggerId == Trigger::OCCLUSION )
	{
		// $[TI5]
		((BreathPhaseScheduler&)ROscScheduler).takeControl(*this);
	}
	else if ( modeTriggerId == Trigger::DISCONNECT )
	{
		// $[TI6]
		((BreathPhaseScheduler&)RDisconnectScheduler).takeControl(*this);
	}
	else
	{
		// $[TI7]
		AUX_CLASS_ASSERTION((modeTriggerId == Trigger::SVO), modeTriggerId);
		((BreathPhaseScheduler&)RSvoScheduler).takeControl(*this);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
// The method is invoked by the method relinquishControl() from the scheduler
// instance that relinquishes control.  It accepts a breath phase scheduler
// reference as an argument and has no return value.  The base class method for
// takeControl() is invoked to handle activities common to all schedulers.
// Peep recovery flag is set on transition from a non breathing mode.  The NOV
// RAM instance rBdSystemState is updated with the new scheduler to enable BD
// system to properly recover from power interruption.  The scheduler registers
// with the BreathPhaseScheduler as the current scheduler, and the mode
// triggers relevant to that mode are set to be enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler reference passed as an argument is used to get the id of the
// current scheduler.  The static method of BreathPhaseScheduler is used to
// register the new scheduler. The BdSystemStateHandler is used to update the
// NOV RAM object for BdSystemState with the new scheduler id.  The private
// data member pModeTriggerList_ is passed as an argument to the method in the
// mode trigger mediator object that is responsible for changing the current
// mode trigger list.
//---------------------------------------------------------------------
//@ PreCondition
// The CLASS_PRE_CONDITION method is used to validate the IDs of the
// valid breath phase schedulers.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
void
SpontScheduler::takeControl(const BreathPhaseScheduler& scheduler) 
{
	CALL_TRACE("SpontScheduler::takeControl(const BreathPhaseScheduler& scheduler)");

	// Invoke the base class takeControl method
	BreathPhaseScheduler::takeControl(scheduler);

	const DiscreteValue MODE = PendingContextHandle::GetDiscreteValue(SettingId::MODE);
	AUX_CLASS_ASSERTION(MODE == ModeValue::SPONT_MODE_VALUE || 
						MODE == ModeValue::CPAP_MODE_VALUE, MODE);

	// save the mode that forced us to take control
	spontModeSetting_ = MODE;

	const SchedulerId::SchedulerIdValue schedulerId = scheduler.getId();

	PauseTypes::ManeuverState maneuverState = RExpPauseManeuver.getManeuverState();
	if ( maneuverState != PauseTypes::IDLE )
	{
		// $[TI1]
		// $[04017] expiratory pause is not allowed in spont
		Maneuver::CancelManeuver();
	}
	// $[TI2]

	// update the reference to the newly active breath scheduler 
	BreathPhaseScheduler::SetCurrentScheduler_(*this);

	// clear the previous inspiratory phase...
	pPrevSpontPhase_ = NULL;

	if ( (schedulerId == SchedulerId::DISCONNECT) || 
		 (schedulerId == SchedulerId::STANDBY) ||
		 (schedulerId == SchedulerId::SVO) ||
		 (schedulerId == SchedulerId::OCCLUSION) ||
		 (schedulerId == SchedulerId::POWER_UP) )
	{
		// $[TI3]
		// PEEP recovery from a non-breathing mode
		BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;

		if ( schedulerId == SchedulerId::DISCONNECT )
		{
			// $[TI4]
			// $[VC24013a]
			if ( PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value <= 7.0 )
			{
				// $[TI5]
				RVolumeTargetedManager.reset() ;
			}
			// $[TI6]
		}
		else
		{
			// $[TI7]
			// $[VC24013b]
			RVolumeTargetedManager.reset() ;
		}
	}
	else
	{
		// $[TI8]
		AUX_CLASS_ASSERTION(
						   (schedulerId == SchedulerId::AC) || 
						   (schedulerId == SchedulerId::BILEVEL) || 
						   (schedulerId == SchedulerId::SIMV) || 
						   (schedulerId == SchedulerId::APNEA) || 
						   (schedulerId == SchedulerId::SPONT) || 
						   (schedulerId == SchedulerId::SAFETY_PCV), schedulerId ); 

		if ( ModeValue::CPAP_MODE_VALUE == MODE )
		{
			// enable the DisarmManeuverTrigger to start a PEEP recovery breath when
			// transitioning to CPAP to force phase in of pending settings
			RDisarmManeuverTrigger.enable();
		}

		if ( schedulerId == SchedulerId::APNEA ||
			 schedulerId == SchedulerId::SAFETY_PCV )
		{
			// $[TI9]
			RVolumeTargetedManager.reset() ;
		}
		// $[TI10]
	}

	//The BreathRecord is not being updated with the new scheduler id, so that
	// the identity of the scheduler that originated this breath is maintained. 

	// update the BD state, in case of power interruption
	BdSystemStateHandler::UpdateSchedulerId(getId()); 

	// Set the mode triggers for that scheduler.
	enableTriggers_();    
	RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);
} 


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SpontScheduler::SoftFault(const SoftFaultID  softFaultID,
						  const Uint32       lineNumber,
						  const char*        pFileName,
						  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, SPONTSCHEDULER,
							lineNumber, pFileName, pPredicate);
}





//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: settingChangeHappened_
//
//@ Interface-Description
// The method takes a SettingId as an argument and has no return value.
// The method is called when a setting change is recognized, and before it
// is phased-in. Only mode change is considered since no rate is involved
// is spontaneous ventilation.
// When a mode change is recognized, the setting change mode trigger is
// enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// The static variable BreathPhaseScheduler::ModeChange_ indicates
// that mode change is currently in progress.
// The SettingId argument indicates which setting has changed.
// When a mode change occurs, the instance rSettingChangeModeTrigger is
// enabled.
//---------------------------------------------------------------------
//@ PreCondition
// The setting id is within the folowing range:
//   id >= SettingId::LOW_BATCH_BD_ID_VALUE && id <= SettingId::HIGH_BATCH_BD_ID_VALUE
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
SpontScheduler::settingChangeHappened_(const SettingId::SettingIdType id)
{
	CALL_TRACE("SpontScheduler::settingChangeHappened_(const SettingId::SettingIdType id)");

	// mode setting is not phased in until the start of inspiration so the
	// mode setting may differ from the active scheduler if no intervening
	// breath has occured to phase in the settings so we can't refer to the 
	// phased-in context for the current mode

	// check for mode change
	const DiscreteValue NEW_MODE = PendingContextHandle::GetDiscreteValue(SettingId::MODE);

	if ( id == SettingId::MODE )
	{
		if ( NEW_MODE != spontModeSetting_ )
		{
			// change schedulers
			BreathPhaseScheduler::ModeChange_ = TRUE;    
			((Trigger&)RSettingChangeModeTrigger).enable();
		}
	}
	else
	{
		// only consider other setting changes if we're in CPAP and not changing to another mode
		if ( spontModeSetting_ == ModeValue::CPAP_MODE_VALUE && NEW_MODE == ModeValue::CPAP_MODE_VALUE )
		{
			// trigger PEEP recovery to force phase in of pending settings since there may
			// never be an inspiratory phase while in CPAP to phase them in
			RDisarmManeuverTrigger.enable();
		}

		AUX_CLASS_ASSERTION((id >= SettingId::LOW_BATCH_BD_ID_VALUE &&
							 id <= SettingId::HIGH_BATCH_BD_ID_VALUE), id);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[01255] $[04252]
// The argument of type EventId indicates which user event is active.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is accepted by
// invoking a call to the static method:
// BreathPhaseScheduler::AcceptManualInspiration_(eventStatus).
// The alarm reset event is ignored, while the eventStatus argument value is checked for
// TRUE.
// The inspiratory pause events is accepted by invoking a call to the
// static method: BreathPhaseScheduler::AcceptInspiratoryPause_(eventStatus).
// The expiratory pause event is rejected by invoking a call to the static method:
// BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus).
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

EventData::EventStatus
SpontScheduler::reportEventStatus_(const EventData::EventId id,
								   const Boolean eventStatus)
{
	CALL_TRACE("SpontScheduler::reportEventStatus_(const EventData::EventId id,\
                                                     const Boolean eventStatus)");

	EventData::EventStatus rtnStatus = EventData::IDLE;

	switch ( id )
	{
		case EventData::MANUAL_INSPIRATION:
			// $[TI1]
			rtnStatus =
			BreathPhaseScheduler::AcceptManualInspiration_(eventStatus);
			break;

		case EventData::ALARM_RESET:
			// $[TI2]
			// ignore alarm reset
			CLASS_PRE_CONDITION(eventStatus);
			break;

		case EventData::EXPIRATORY_PAUSE:
			// $[TI3]
			rtnStatus =
			BreathPhaseScheduler::RejectExpiratoryPause_(eventStatus);
			break;

		case EventData::INSPIRATORY_PAUSE:
			// $[TI4]
			rtnStatus =
			BreathPhaseScheduler::AcceptInspiratoryPause_(eventStatus);
			break;

		case EventData::NIF_MANEUVER:
			rtnStatus = BreathPhaseScheduler::AcceptNifManeuver_(eventStatus);
			break;

		case EventData::P100_MANEUVER:
			rtnStatus = BreathPhaseScheduler::AcceptP100Maneuver_(eventStatus);
			break;

		case EventData::VITAL_CAPACITY_MANEUVER:
			rtnStatus = BreathPhaseScheduler::AcceptVitalCapacityManeuver_(eventStatus);
			break;

		default:
			// $[TI6]
			AUX_CLASS_ASSERTION_FAILURE(id);
	}    

	return(rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// $[04119] $[04125] $[04130] $[04141]
// The method takes no arguments and returns no value.  A request to enable
// SPONT scheduler valid mode triggers is issued whenever the SPONT scheduler
// is taking control. Immediate triggers are not set here since by design they
// are only set once the trigger they instanciate becomes active.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode trigger instances are accessed directly with a call to their
// setIsEnableRequested() method.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
SpontScheduler::enableTriggers_(void) 
{
// $[TI1]
	CALL_TRACE("SpontScheduler::enableTriggers_(void)");

	((ModeTrigger&)RApneaTrigger).setIsEnableRequested(TRUE);
	((ModeTrigger&)ROcclusionTrigger).setIsEnableRequested(TRUE);
	((ModeTrigger&)RDisconnectTrigger).setIsEnableRequested(TRUE);
	((ModeTrigger&)RSvoTrigger).setIsEnableRequested(TRUE);

	// Note that the immediate, or event driven triggers are not
	// enabled here: rSettingChangeModeTrigger
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startInspiration_ 
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, and a
// breath type as arguments. It does not return any value.
// The following activities are performed: Any pending mode or rate
// change status is cleared, new settings are phased in, the scheduler is checked
// to be in sync with the mode setting. a new breath record is
// created, the oxygen mix, the apnea interval, and peep get updated, the
// breath trigger list is changed for inspiration list, the previous breath
// relinquishes control, and the breath phase is set with the reference to
// the breath phase to be delivered.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method collaborates with the handle PhasedInContextHandle to phase in new
// settings. A check that the scheduler matches the mode setting is done by
// calling the method VerifyModeSynchronization_().
// The method creates a new breath record using the object RBreathSet.  The
// actual oxygen mix, apnea interval, and peep are updated using the instances
// rO2Mixture, rApneaInterval, and rPeep.  The breath trigger mediator instance
// RBreathTriggerMediator is used to set the inspiratory breath trigger list.
// The breath phase instance pointed to by argument pBreathPhase is instructed
// to phase out. The static method BreathPhase::SetCurrentBreathPhase
// is used to set up the new breath type.
//---------------------------------------------------------------------
//@ PreCondition
// Check ranges for mandatory type and breath type.
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
SpontScheduler::startInspiration_(const BreathTrigger& breathTrigger,
								  BreathPhase* pBreathPhase,  BreathType breathType)
{

	CALL_TRACE("SpontScheduler::startInspiration_(const BreathTrigger& breathTrigger,\
                BreathPhase* pBreathPhase,  BreathType breathType)");

	// Reset any flags due to bilevel mode transition.
	RAsapInspTrigger.resetBiLevelModeChangeCondition();

	// flags peep recovery in process
	const Boolean  IS_PEEP_RECOVERY = (breathType == ::SPONT
									   &&  BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::START);

	BreathPhaseScheduler::ModeChange_ = FALSE;    
	BreathPhaseScheduler::RateChange_ = FALSE;

	//Phase in new settings (for start of inspiration)
	PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

	// verify BD is in synch with GUI:
	BreathPhaseScheduler::VerifyModeSynchronization_(getId());

	const DiscreteValue  CURR_SUPPORT_TYPE
	= PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE);

	const DiscreteValue  CURR_MAND_TYPE
	= PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);
	DiscreteValue  mandatoryType = CURR_MAND_TYPE;

	RVolumeTargetedManager.updateData(CURR_MAND_TYPE, CURR_SUPPORT_TYPE) ;

	// pointer to a breath phase
	BreathPhase*  pNewInspPhase = NULL;	// new insp phase (spont or manual insp)...
	BreathPhase*  pNewSpontPhase
	= determineSpontBreathPhase_(breathTrigger.getId(),
								 IS_PEEP_RECOVERY,
								 CURR_SUPPORT_TYPE,
								 mandatoryType, TRUE);

	if ( breathType == ::CONTROL )
	{
		// $[TI1]
		switch ( CURR_MAND_TYPE )
		{
			case MandTypeValue::PCV_MAND_TYPE :
				// $[TI1.2]
				pNewInspPhase = (BreathPhase*)&RPcvPhase ;
				break ;

			case MandTypeValue::VCV_MAND_TYPE :
				// $[TI1.1]
				pNewInspPhase = (BreathPhase*)&RVcvPhase ;
				break ;

			default :
				// unexpected mand type...
				AUX_CLASS_ASSERTION_FAILURE(CURR_MAND_TYPE) ;
				break ;
		}
	}
	else if ( breathType == ::SPONT )
	{
		// $[TI2]
		pNewInspPhase = pNewSpontPhase;

		mandatoryType = ::NULL_MANDATORY_TYPE;
	}
	else
	{
		// $[TI3]
		AUX_CLASS_ASSERTION_FAILURE(breathType);
	}

	//Reset currently active triggers and setup triggers that are active during
	//inspiration.
	RBreathTriggerMediator.resetTriggerList();
	RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::INSP_LIST);

	// phase-out current breath phase
	pBreathPhase->relinquishControl(breathTrigger);

	// $[04118] $[04122]
	// Once settings are phased in,
	// determine the O2 mix, apnea interval and peep
	//for this phase:
	RO2Mixture.determineO2Mix(*this, *pNewInspPhase);
	RApneaInterval.newPhase(BreathPhaseType::INSPIRATION);
	RPeep.updatePeep(BreathPhaseType::INSPIRATION);

	if ( pNewInspPhase == (BreathPhase*)&RPeepRecoveryPhase )
	{	// $[TI4]
		RPeep.setPeepChange( TRUE) ;
	}	// $[TI5]

	//  register new phase:
	BreathPhase::SetCurrentBreathPhase(*pNewInspPhase, breathTrigger);

	//Update the breath record
	RBreathSet.newBreathRecord(SchedulerId::SPONT, mandatoryType, breathType,
							   BreathPhaseType::INSPIRATION, IS_PEEP_RECOVERY);

	// start delivering inspiration
	BreathPhase::NewBreath() ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startExhalation_
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, a
// breath type as arguments, and a null inspiration boolean flag. It
// does not return any value.  
// Activities for start of exhalation are:
// - Phase-in new settings, and update the applied O2%. Update the apnea interval.
// - Change the active breath trigger list, disable the breath triggers on the
//   current list.
// - Instruct the current breath phase to relinquish control and setup
//   the new breath phase.
// - Update the current breath record with new data.
//---------------------------------------------------------------------
//@ Implementation-Description
// The phase-in setting context is used to phase-in settings for exhalation.
// Other instances used are rO2Mixture - to set the O2 mix, rApneaInterval -
// to set the Apnea interval, and RBreathTriggerMediator to reset previous
// triggers and to set new triggers. The instance RBreathSet is used to setup
// the breath record - after the static method
// BreathPhase::SetCurrentBreathPhase is called to set the new breath phase.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

void
SpontScheduler::startExhalation_(const BreathTrigger& breathTrigger,
								 BreathPhase* pBreathPhase,
								 const Boolean noInspiration)
{
// $[TI1]
	CALL_TRACE("SpontScheduler::startExhalation_(const BreathTrigger& breathTrigger, \
            BreathPhase* pBreathPhase)");

	//Phase in new settings (for start of exhalation)
	PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_EXHALATION);

#ifdef INTEGRATION_TEST_ENABLE
	// Signal Event for swat
	swat::EventManager::signalEvent(swat::EventManager::START_OF_EXH);
#endif // INTEGRATION_TEST_ENABLE

	RBreathTriggerMediator.resetTriggerList();
	RBreathTriggerMediator.changeBreathListName(
											   BreathTriggerMediator::EXP_LIST);

	// phase-out current breath phase
	pBreathPhase->relinquishControl(breathTrigger);

	// Once settings are phased in, determine the O2 mix, the apnea interval
	// and the peep for this phase:
	RO2Mixture.determineO2Mix(*this, (BreathPhase&)RExhalationPhase);
	RApneaInterval.newPhase(BreathPhaseType::EXHALATION);
	RPeep.updatePeep(BreathPhaseType::EXHALATION);

	// register new phase:
	BreathPhase::SetCurrentBreathPhase((BreathPhase&)RExhalationPhase,
									   breathTrigger);

	if ( noInspiration )
	{
		// $[TI2]
		//Update the breath record
		RBreathSet.newBreathRecord(SchedulerId::SPONT,
								   ::NULL_MANDATORY_TYPE,
								   RBiLevelScheduler.getFirstBreathType(),
								   BreathPhaseType::NULL_INSPIRATION,
								   FALSE);
	}
	else
	{
		// $[TI1]
		//Update the breath record
		(RBreathSet.getCurrentBreathRecord())->newExhalation();
	}

	// start delivering exhalation
	BreathPhase::NewBreath() ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineSpontBreathPhase_
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

BreathPhase*
SpontScheduler::determineSpontBreathPhase_(
										  const Trigger::TriggerId triggerId,
										  const Boolean            isPeepRecovery,
										  const DiscreteValue      spontTypeValue,
										  DiscreteValue&           rMandTypeValue,
										  const Boolean            isReadyToActivate
										  )
{
	CALL_TRACE("determineSpontBreathPhase_(...)");

	BreathPhase*  pNewSpontPhase = NULL;	// new insp phase (spont only)...

	//---------------------------------------------------------------------
	//  This block of code was introduced to support the starting and resetting
	//  of the PAV alarms and state.  It is setup as a general-purpose mechanism
	//  for any spont phase to manage when it is becoming active or deactive.
	//
	//  NOTE:  Manual Inspirations are kept out of this mechanism so that the
	//         use of this OIM feature doesn't interfere with the state and
	//         timer management facilitated by this mechanism.
	//---------------------------------------------------------------------

	if ( isPeepRecovery )
	{
		// $[TI1]
		// Set the PeepRecovery_ variable to OFF to flag the activation of a normal
		// exhalation once this inspiration is over.
		BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::OFF;
		pNewSpontPhase = (BreathPhase*)&RPeepRecoveryPhase;
	}
	else
	{
		// $[TI2]
		switch ( spontTypeValue )
		{
			case SupportTypeValue::OFF_SUPPORT_TYPE :
			case SupportTypeValue::PSV_SUPPORT_TYPE :
				// $[TI2.1] -- use PSV phase...
				pNewSpontPhase = (BreathPhase*)&RPsvPhase;
				break;
			case SupportTypeValue::ATC_SUPPORT_TYPE :
				// $[TI2.2] -- use TCV phase...
				pNewSpontPhase = (BreathPhase*)&RTcvPhase;
				break;
			case SupportTypeValue::VSV_SUPPORT_TYPE :
				// $[TI2.3] -- determine VSV phase...
				RVolumeTargetedManager.updateData(rMandTypeValue, spontTypeValue);
				RVolumeTargetedManager.determineBreathType(pNewSpontPhase,
														   rMandTypeValue);
				break;
			case SupportTypeValue::PAV_SUPPORT_TYPE :
				// $[TI2.4] -- use PAV phase...
				pNewSpontPhase = (BreathPhase*)&RPavPhase;
				break;
			default :
				// unexpected spont type value...
				AUX_CLASS_ASSERTION_FAILURE(spontTypeValue);
				break;
		}
	}

	//---------------------------------------------------------------------
	//  Now that the appropriate spont phase has been determined, we need to
	//  check as to whether it is different from the last spont phase.  If so,
	//  let the previous spont phase know about its deacivation, and the new
	//  spont phase know about its activation.
	//---------------------------------------------------------------------

	if ( pNewSpontPhase != pPrevSpontPhase_ )
	{	// $[TI3]
		if ( pPrevSpontPhase_ != NULL )
		{	// $[TI3.1]
			pPrevSpontPhase_->deactivate(triggerId);
		}	// $[TI3.2]

		if ( isReadyToActivate )
		{	// $[TI3.3] -- activate and store...
			pNewSpontPhase->activate(pPrevSpontPhase_, pLostSpontPhase_);

			pPrevSpontPhase_ = pNewSpontPhase;
		}
		else
		{	// $[TI3.4] -- clear pointer to force future activation...
			pPrevSpontPhase_ = NULL;
		}
	}	// $[TI4]

	return(pNewSpontPhase);
}
