#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CompressorLeakTest - Test for compressor leak
//---------------------------------------------------------------------
//@ Interface-Description
//
//	This class implements the compressor leak test, which tests for a
//	compressor leak via the compressor timer -- a leak during this test
//	causes the compressor motor to turn on momentarily, which causes
//	the compressor timer to increment.
//---------------------------------------------------------------------
//@ Rationale
//  This class encapsulates the compressor leak test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs the compressor leak test.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class is allowed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/CompressorLeakTest.ccv   25.0.4.0   19 Nov 2013 14:23:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008  By: dosman Date: 27-May-1999     DR Number: 5413
//  Project:  ATC
//  Description:
//	Changed total time that compressor leak test takes
//	from 5 minutes to 1 minute.
//
//  Revision: 007  By: dosman Date: 16-Apr-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release.
//	Removed some SIGMA_UNIT_TEST code that was causing unit test not to work.
//
//  Revision: 006  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 005  By:  syw    Date: 28-Sep-1998    DR Number: DCS 5180
//       Project:  BiLevel
//       Description:
//			If no wall air is present (compressor only) change the test time
//			to a shorter test period.
//
//  Revision: 004  By:  quf    Date: 20-Nov-1997    DR Number: DCS 2644
//       Project:  Sigma (840)
//       Description:
//			Fixed runTest() to use a more reliable method of preventing
//	 		the Background Maintenance Task from accessing the compressor
//			serial EEPROM during this test.
//
//  Revision: 003  By:  quf    Date: 06-Jun-1997    DR Number: DCS 1911
//       Project:  Sigma (840)
//       Description:
//            Added gross leak check via compressor air presence signal.
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  mad    Date:  27-Oct-1995    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "CompressorLeakTest.hh"

//@ Usage-Classes

#include "Task.hh"

#ifdef SIGMA_DEBUG
# include "OsTimeStamp.hh"
#endif  // SIGMA_DEBUG

#include "VentObjectRefs.hh"
// E600 BDIO #include "CompressorTimer.hh"
#include "Compressor.hh"
#include "SafetyNet.hh"
#include "BackgroundMaintApp.hh"

#include "SmRefs.hh"
#include "SmManager.hh"
#include "SmUtilities.hh"
#include "SmFlowController.hh"
#include "SmGasSupply.hh"
#include "SmConstants.hh"
#include "CompressorTest.hh"
#include "OsTimeStamp.hh"

#if defined(SIGMA_DEVELOPMENT)
#include <stdio.h>
#endif

#ifdef SIGMA_UNIT_TEST
# include "SmUnitTest.hh"
# include "FakeIo.hh"
# include "FakeControllers.hh"
#endif  // SIGMA_UNIT_TEST

//@ End-Usage

//@ Code...

//@ Constant: COMP_LEAK_TEST_MAX_TIMER_INCR
// maximum allowable timer increment for no leak
const Uint32 COMP_LEAK_TEST_MAX_TIMER_INCR = 1;  // sec


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CompressorLeakTest()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
CompressorLeakTest::CompressorLeakTest(void)
{
	CALL_TRACE("CompressorLeakTest::CompressorLeakTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CompressorLeakTest()  [Destructor]
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CompressorLeakTest::~CompressorLeakTest(void)
{
	CALL_TRACE("CompressorLeakTest::~CompressorLeakTest(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest()
//
//@ Interface-Description
//  This method takes no arguments and returns nothing.  It is used
//  to test for compressor leak during service mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09149]  $[09078]
//	Check if the compressor is present -- if not, indicate "option not
//	installed" to gui and exit.
//	Check if timer tests passed in the Compressor Test -- if not,
//	generate a FAILURE due to unreliable timer, and exit.
//	Set compressor to STANDBY mode and fully charge the accumulator.
//	Read initial compressor time.
//	Wait for 5 minutes, checking the compressor air presence signal
//  periodically.  If signal indicates loss of compressor pressure,
//	generate FAILURE and exit immediately.
//	At end of 5 minutes, check if timer incremented greater than
//	(0 + margin) secs.  If it did, then leak occurred, so generate
//	FAILURE and exit.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
CompressorLeakTest::runTest(void)
{
	CALL_TRACE("CompressorLeakTest::runTest(void)");

	Boolean compressedAirOnly = FALSE ;

	if (!RCompressor.smCheckCompressorInstalled_())
	{
	// $[TI1]
		// Compressor not installed
		RSmManager.sendOptionNotInstalledToServiceData();
	}
	else if (!RCompressorTest.isTimerWorking())
	{
	// $[TI2]
		// Compressor timer not working correctly: can't test
		// for leak
		RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_ALERT,
				SmStatusId::CONDITION_1);
	}
	else
	{
	// $[TI3]
#ifdef SIGMA_DEBUG
		Real32 leakRate;
		printf("enter simulated leak rate (lpm): ");
		scanf("%f", &leakRate);
		if (leakRate < 0.0) leakRate = 0.0;
		else if (leakRate > 100.0) leakRate = 100.0;
		printf("simulated leak rate = %5.1f lpm\n", leakRate);
#endif  // SIGMA_DEBUG

		Boolean testFailed = FALSE;
		Boolean operatorExit = FALSE;

		// Disable the compressor and compressor control
		RSmGasSupply.disableCompressorAndControl();

		// If wall air is connected, prompt operator to disconnect
		// it then recheck
		if (RSmGasSupply.isWallAirPresent())
		{
		// $[TI3.1]
			if ( !RSmUtilities.messagePrompt(
					SmPromptId::DISCONNECT_AIR_PROMPT) )
			{
			// $[TI3.1.1]
				// operator hit EXIT button

				operatorExit = TRUE;
			}
			else
			{
			// $[TI3.1.2]
				// operator hit ACCEPT key

				// Bleed trapped wall air then recheck
				RSmUtilities.drainWallAir();

				if (RSmGasSupply.isWallAirPresent())
				{
				// $[TI3.1.2.1]
					// wall air not disconnected
					RSmManager.sendStatusToServiceData(
						SmStatusId::TEST_ALERT,
						SmStatusId::CONDITION_2);

					testFailed = TRUE;
				}
				// $[TI3.1.2.2]
				// implied else
			}
			// end else
		}
		// $[TI3.2]
		// implied else

		if (!operatorExit && !testFailed)
		{
		// $[TI3.3]
			// bleed the accumulator
			RSmUtilities.drainAir();

			// ACCEPT key = user connected wall air,
			// CLEAR key = user wants to use compressor air
			RSmManager.promptOperator( SmPromptId::CONNECT_AIR_IF_AVAIL_PROMPT,
									   SmPromptId::ACCEPT_AND_CANCEL_ONLY) ;

			SmPromptId::ActionId operatorAction = RSmManager.getOperatorAction();

			if (operatorAction == SmPromptId::USER_EXIT)
			{
				// $[TI4]
				operatorExit = TRUE;
			}
			else if (operatorAction == SmPromptId::KEY_ACCEPT)
			{
				// $[TI5]
				if (!RSmGasSupply.isWallAirPresent())
				{
					// $[TI6]
					// ACCEPT key was pressed, but wall air
					// was not detected
					RSmManager.sendStatusToServiceData(
							SmStatusId::TEST_ALERT,
							SmStatusId::CONDITION_3);

					testFailed = TRUE;
				}   // implied else $[TI7]: ACCEPT key was pressed and wall air detected
			}
			else if (operatorAction == SmPromptId::KEY_CLEAR)
			{
				// $[TI8]
				compressedAirOnly = TRUE ;
			}
			else
			{
				CLASS_ASSERTION(
					operatorAction == SmPromptId::USER_EXIT ||
					operatorAction == SmPromptId::KEY_ACCEPT ||
					operatorAction == SmPromptId::KEY_CLEAR );
			}
		}
		// $[TI3.4]
		// implied else

		if (!operatorExit && !testFailed)
		{
		// $[TI3.5]
			// stop bkg maint task from updating compressor operational time
			BackgroundMaintApp::IssueNewSemphCommand(
								BackgroundMaintApp::REQUEST_TO_SUSPEND) ;
			while ( BackgroundMaintApp::RetrieveCurrentSemphStatus()
						!= BackgroundMaintApp::TASK_SUSPENDED)
			{
			// $[TI3.5.4]
				Task::Delay( 0, 20) ;

# ifdef SIGMA_UNIT_TEST
// added for unit testing, otherwise we're stuck in this loop
				SmUnitTest::SuspendBkgMaintAppTask();
# endif  // SIGMA_UNIT_TEST
	        }

			// Set the compressor to STANDBY mode
			RCompressor.smEnableCompressor_();
            RCompressor.smSetCompressorMotorOn_();

			// Let accumulator fill
			Task::Delay( 0, COMPRESSOR_FILL_TIME);

			RCompressor.smSetCompressorMotorReady_();

			const Uint32 FULL_TEST_DURATION = 60000;
			const Uint32 SHORT_TEST_DURATION = 5000;
			const Uint32 STANDYBY_DELAY = 1000 ;
			Task::Delay( 0, STANDYBY_DELAY) ;

			OsTimeStamp timer ;
			Uint32 initTime = timer.getPassedTime() ;

			// Read compressor air presence signal
			Boolean compressorAirPresent =
								RSmGasSupply.isCompressorAirPresent();

#ifdef SIGMA_DEBUG
			OsTimeStamp syncTimer;

			if (leakRate > 0.0)
			{
				RSmFlowController.establishFlow( leakRate, DELIVERY, AIR);
			}
#endif  // SIGMA_DEBUG

			// Read the initial compressor time
          //  Uint32 initialTime = 0; // E600 BDIO RCompressorTimer.readActiveCounter();
			
			Uint32 testDuration = FULL_TEST_DURATION - STANDYBY_DELAY ;

			if (compressedAirOnly)
			{
				// $[TI9]
				testDuration = SHORT_TEST_DURATION - STANDYBY_DELAY ;
			}	// implied else $[TI10]

			// send initial remaining time in secs
			RSmManager.sendMonitorPointToServiceData((Real32) (testDuration / 1000));

			// wait for the required test time, exiting early
			// if compressor air presence is not detected

			Uint32 elapsedTime = timer.getPassedTime() - initTime;

			while (elapsedTime < testDuration && compressorAirPresent)
			{
			// $[TI3.5.1]
				Task::Delay(1);

				elapsedTime = timer.getPassedTime() - initTime;

				// Read compressor air presence signal
				compressorAirPresent =
								RSmGasSupply.isCompressorAirPresent();

				// send remaining time in secs
				RSmManager.sendMonitorPointToServiceData((Real32) ((testDuration - elapsedTime) / 1000));
			}
			// end for

#ifdef SIGMA_DEBUG
#if !defined(SIGMA_UNIT_TEST)
			Uint32 passedTime = syncTimer.getPassedTime();
			printf("OsTimeStamp elapsed time = %u secs\n", passedTime);
			printf("delay loop elapsed time = %u secs\n", secTimer - 1);
#endif // !defined(SIGMA_UNIT_TEST)
#endif  // SIGMA_DEBUG

			// Verify the compressor has not run
            Uint32 runTime = 0; // E600 BDIO RCompressorTimer.readActiveCounter() - initialTime;

#ifdef SIGMA_DEBUG
			printf("run time = %u secs\n", runTime);
#endif  // SIGMA_DEBUG

			RSmManager.sendTestPointToServiceData(
				SmDataId::COMPRESSOR_LEAK_ELAPSED_TIME, (Real32)runTime );

			if (runTime > COMP_LEAK_TEST_MAX_TIMER_INCR ||
				!compressorAirPresent)
			{
			// $[TI3.5.2]
				// leak occurred
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_ALERT,
					SmStatusId::CONDITION_4);
			}
			// $[TI3.5.3]
			// implied else: test passed

#ifdef SIGMA_DEBUG
			RSmFlowController.stopFlowController();
#endif  // SIGMA_DEBUG

			// let bkg maint task resume updating compressor operational time
			BackgroundMaintApp::IssueNewSemphCommand(
										BackgroundMaintApp::TASK_RESUME);
		}
		// $[TI3.6]
		// implied else

		if (operatorExit)
		{
		// $[TI3.7]
			RSmManager.sendOperatorExitToServiceData();
		}
		// $[TI3.8]
		// implied else

		// Re-enable the compressor controller
		RSmGasSupply.enableCompressorAndControl();
	}
	// end else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
void
CompressorLeakTest::SoftFault(const SoftFaultID  softFaultID,
			  const Uint32       lineNumber,
			  const char*        pFileName,
			  const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SERVICE_MODE, COMPRESSORLEAKTEST,
		                      lineNumber, pFileName, pPredicate);

}
// runTest


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
