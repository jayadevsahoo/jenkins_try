//-------------------------------------------------------------------------------
//                        Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//-------------------------------------------------------------------------------
/// @file EventManager.h
///
/// @brief
/// Class definitions for the singleton class EventManager


#ifndef EVENTMANAGER_H
#define EVENTMANAGER_H

#include "SwatBatchUser.h"
#include "BreathPhaseType.hh"
#include "SchedulerId.hh"

namespace swat{

#ifndef EVENTMANAGER_NEVENTS
#define EVENTMANAGER_NEVENTS 128
#endif

    /// @class EventManager
    ///
    /// @brief A class that provides a way to communicate events between system and batch scripts.
    ///
    /// Events are signaled by calling EventManager::signalEvent with the event id that needs to
    /// be signaled.  signalEvent increments a counter to indicate that an event has occured.
    /// Those waiting for events use getEventState to get the current count of event.
    /// A new event is detected by observing a change in the count returned by getEventState.
    class EventManager
    {


        private:
            static unsigned int m_EventState[EVENTMANAGER_NEVENTS];

        public:

            enum
            {
                EVENT_CLASS_OLDSTYLE      = 0,
                EVENT_CLASS_OLDSTYLE_MAX = 64, 
                MESSAGE_DROPPED          = 0  + EVENT_CLASS_OLDSTYLE,
                MESSAGE_INVALID_LENGTH   = 1  + EVENT_CLASS_OLDSTYLE,
                MESSAGE_INVALID_UUID     = 2  + EVENT_CLASS_OLDSTYLE,
                MEMORY_NO_ALLOCATE       = 3  + EVENT_CLASS_OLDSTYLE,
                QUEUE_NO_ALLOCATE        = 4  + EVENT_CLASS_OLDSTYLE,
                SWAT_DISABLED            = 5  + EVENT_CLASS_OLDSTYLE,
                START_OF_INSP            = 6  + EVENT_CLASS_OLDSTYLE,
                START_OF_EXH             = 7  + EVENT_CLASS_OLDSTYLE,
                START_OF_AUTOZERO_EXH    = 8  + EVENT_CLASS_OLDSTYLE,
                START_OF_AUTOZERO_INSP   = 9  + EVENT_CLASS_OLDSTYLE,
                START_OF_PEEP_HIGH       = 10 + EVENT_CLASS_OLDSTYLE,
                START_OF_PEEP_LOW        = 11 + EVENT_CLASS_OLDSTYLE,

                EVENT_CLASS_PHASE           = EVENT_CLASS_OLDSTYLE_MAX, //Start id for Phase related events
                EVENT_CLASS_PHASE_MAX       = 20, //Max number of Phase events
                EVENT_EXHALATION            = 0  + EVENT_CLASS_PHASE,
                EVENT_INSPIRATION           = 1  + EVENT_CLASS_PHASE,
                EVENT_NULL_INSPIRATION      = 2  + EVENT_CLASS_PHASE,
                EVENT_EXPIRATORY_PAUSE      = 3  + EVENT_CLASS_PHASE,
                EVENT_INSPIRATORY_PAUSE     = 4  + EVENT_CLASS_PHASE,
                EVENT_BILEVEL_PAUSE_PHASE   = 5  + EVENT_CLASS_PHASE,
                EVENT_PAV_INSPIRATORY_PAUSE = 6  + EVENT_CLASS_PHASE,
                EVENT_NIF_PAUSE             = 7  + EVENT_CLASS_PHASE, 
                EVENT_P100_PAUSE            = 8  + EVENT_CLASS_PHASE,
                EVENT_VITAL_CAPACITY_INSP   = 9  + EVENT_CLASS_PHASE,
                EVENT_NON_BREATHING         = 10 + EVENT_CLASS_PHASE,

                EVENT_CLASS_SCHEDULER       = EVENT_CLASS_PHASE + EVENT_CLASS_PHASE_MAX, //Start id for Scheduler events
                EVENT_CLASS_SCHEDULER_MAX   = 20,                                        //Max Scheduler id
                EVENT_APNEA                 = 0  + EVENT_CLASS_SCHEDULER,
                EVENT_AC                    = 1  + EVENT_CLASS_SCHEDULER,
                EVENT_SPONT                 = 2  + EVENT_CLASS_SCHEDULER,
                EVENT_SIMV                  = 3  + EVENT_CLASS_SCHEDULER,
                EVENT_SAFETY_PCV            = 4  + EVENT_CLASS_SCHEDULER,
                EVENT_BILEVEL               = 5  + EVENT_CLASS_SCHEDULER,
                EVENT_DISCONNECT            = 6  + EVENT_CLASS_SCHEDULER,
                EVENT_OCCLUSION             = 7  + EVENT_CLASS_SCHEDULER,
                EVENT_SVO                   = 8  + EVENT_CLASS_SCHEDULER,
                EVENT_STANDBY               = 9  + EVENT_CLASS_SCHEDULER,
                EVENT_POWER_UP              = 10 + EVENT_CLASS_SCHEDULER,
            };

            EventManager()
            {
                //Empty constructor
            }

            /// @brief Signal that an event has occured.
            /// @param ev Id of event to signal.  Must be in range of 0 to 63.
            inline static void signalEvent(unsigned int ev)
            {
                if( ev < EVENTMANAGER_NEVENTS)
                {
                    m_EventState[ev]++;
                }
            }

            /// @brief Get the current state of the event counter.
            /// @param ev Id of event to get the state from.  Must be in range of 0 to 63.
            inline static unsigned int getEventState(unsigned int ev)
            {
                if( ev < EVENTMANAGER_NEVENTS)
                {
                    return m_EventState[ev];
                }

                return 0;
            }
    };

}

#endif
