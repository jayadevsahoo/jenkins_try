#ifndef TrendTableDbVars_HH
#define TrendTableDbVars_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendTableDaDbVars
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbTableVars.hhv   25.0.4.0   19 Nov 2013 14:34:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjf    Date:  18-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//=====================================================================
//@ Usage-Classes
//@ End-Usage

#include "TrendDataMgr.hh"
#include "CompactFlash.hh"




class TrendDbTableVars
{
public:

	TrendDbTableVars( void );
	~TrendDbTableVars();

	void config( Uint32 startBlock, Uint32 capacityLba,
				 Uint32 headBlock, Uint32 sizeLba );

	void clear( void );

	inline Uint32 getHeadLba( void ) const;
	inline Uint32 getTailLba( void ) const;
	inline Uint32 getSizeLba( void ) const;
	inline Uint32 getStartLba( void ) const;
	inline Uint32 getEndLba( void ) const;
	inline Uint32 getCapacityLba( void ) const;
	Int32 getPositionLba( Uint32 position ) const;

	void incLba( void );



	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:
	// no copy or assignment constructor
	TrendDbTableVars& TrendDbTableDBVars( const TrendDbTableVars& );	// not implemented
	void operator=( const TrendDbTableVars& );		// not implemented

	//@Data-Member: headLba_
	// Head block offset from startBlock_
	Uint32              headLba_;

	//@Data-Member: tailLba_
	// Tail block offset from startBlock_
	Uint32              tailLba_;

	//@Data-Member: sizeLBA_
	// Size in LBA blocks
	Uint32              sizeLba_;

	//@Data-Member: startLBA_
	// Starting LBA block on the compact flash
	Uint32              startLba_;

	//@Data-Member: capLBA_
	// Capacity in blocks for the table
	Uint32              capLba_;
};

// Inlined methods
#include "TrendDbTableVars.in"

#endif // TrendDbTableVars_HH
