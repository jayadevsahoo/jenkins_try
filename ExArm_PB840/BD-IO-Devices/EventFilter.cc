#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: EventFilter - implements a filter for reporting events
//---------------------------------------------------------------------
//@ Interface-Description
//  Methods are provided to register for alarms and vent status callbacks,
//	to post alarm and event status notification messages, and to perform
//	periodic updates every BD secondary cycle (20 ms).
//---------------------------------------------------------------------
//@ Rationale
//	To encapsulate alarm and event status message filtering to prevent
//	filling up the message queue on the GUI CPU
//---------------------------------------------------------------------
//@ Implementation-Description
//	All the alarms and vent status messages filtereing is managed by the
//	Debouncer class.  Objects for each alarm and vent status are instanciated.
//	Two arrays of pointers to alarm debouncers and vent status debouncers are
//	maintained to process the debouncer objects.  
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/EventFilter.ccv   10.7   08/17/07 09:29:12   pvcs  
//
//@ Modification-Log
//
//  Revision: 004  By:  syw    Date:  08-Nov-1999    DR Number: 5574
//       Project:  840
//       Description:
//			Use COMP_ALARM_DELAY for compressor delay time.
//
//  Revision: 003  By:  syw    Date:  27-Oct-1999    DR Number: 5556
//       Project:  840
//       Description:
//			Added COMPRESSOR_DELAY_CYCLES to delay annunciating compressor
//			air, comp ready and on compressur status.
//
//  Revision: 002  By:  syw    Date:  16-Aug-1999    DR Number: 5511
//       Project:  840
//       Description:
//			Use alarmInit_[] and ventStatusInit_[] to keep track whether
//			the coreesponding Debouncer objects needs initialization.
//
//  Revision: 001  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//			Initial revision
//
//=====================================================================

#include "EventFilter.hh"

//@ Usage-Classes
#include "BdAlarmId.hh"
#include "EventData.hh"
//@ End-Usage

//@ Code...

static const Uint32 DEBOUNCE_CYCLES = 5 ; // number of 20 msec cycles in 100 ms

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: EventFilter()  
//
//@ Interface-Description
//		Default Constructor.  This method has no arguments and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Debouncer objects are instanciated and initialized to arbitrary
//		value.  Debouncer::initialize() method will initialize to the
//		actual value.  pAlarmDebouncer_ and	pVentStatusDebouncer_ are filled.
//		Boolean init states are initialized to indicate that init is required.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

EventFilter::EventFilter(void)
 : wallAirDebouncer_( DEBOUNCE_CYCLES, BdAlarmId::BDALARM_NOT_WALL_AIR_PRESENT),
   wallO2Debouncer_( DEBOUNCE_CYCLES, BdAlarmId::BDALARM_NOT_WALL_O2_PRESENT),
   compOptionDebouncer_( DEBOUNCE_CYCLES, BdAlarmId::BDALARM_NOT_COMP_OPTION_PRESENT),
   compAirDebouncer_( DEBOUNCE_CYCLES, BdAlarmId::BDALARM_NOT_COMP_AIR_PRESENT, COMP_ALARM_DELAY / SECONDARY_CYCLE_TIME_MS),
   svoDebouncer_( DEBOUNCE_CYCLES, EventData::CANCEL),
   compOperatingDebouncer_( DEBOUNCE_CYCLES, EventData::CANCEL, COMP_ALARM_DELAY / SECONDARY_CYCLE_TIME_MS),
   compReadyDebouncer_( DEBOUNCE_CYCLES, EventData::CANCEL, COMP_ALARM_DELAY / SECONDARY_CYCLE_TIME_MS)
{
	CALL_TRACE("EventFilter::EventFilter(void)") ;
	
	// $[TI1]
	pAlarmDebouncer_[ EventFilter::WALL_AIR] = &wallAirDebouncer_ ;
	pAlarmDebouncer_[ EventFilter::WALL_O2] = &wallO2Debouncer_ ;
	pAlarmDebouncer_[ EventFilter::COMP_OPTION] = &compOptionDebouncer_ ;
	pAlarmDebouncer_[ EventFilter::COMP_AIR] = &compAirDebouncer_ ;
	pVentStatusDebouncer_[ EventFilter::SVO] = &svoDebouncer_ ;
	pVentStatusDebouncer_[ EventFilter::COMP_OPERATING] = &compOperatingDebouncer_ ;
	pVentStatusDebouncer_[ EventFilter::COMP_READY] = &compReadyDebouncer_ ;

	Uint32 ii;
	for (ii=0; ii < EventFilter::NUM_ALARM_FILTERS; ii++)
	{
		// $[TI2]
		alarmInit_[ii] = TRUE ;
	}

	for (ii=0; ii < EventFilter::NUM_VENT_STATUS; ii++)
	{
		// $[TI3]
		ventStatusInit_[ii] = TRUE ;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~EventFilter()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

EventFilter::~EventFilter(void)
{
	CALL_TRACE("EventFilter::~EventFilter(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: postAlarm()  
//
//@ Interface-Description
//		This mehod has AlarmFilterType, bdAlarmId, and an initInProg state as
//		arguments and returns nothing.  This method is called to filter
//		an alarm to be posted.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	If alarmInit_[] then initialize Debouncer, else update it with
//		the current alarm id.  If initInProg, post alarm to pAlarmCallBack_
//		function.
//---------------------------------------------------------------------
//@ PreCondition
//		type >= 0 && type < NUM_ALARM_FILTERS
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
EventFilter::postAlarm( const EventFilter::AlarmFilterType type,
						const BdAlarmId::BdAlarmIdType bdAlarmId,
						const Boolean initInProg)
{
	CALL_TRACE("EventFilter::postAlarm( const EventFilter::AlarmFilterType type, \
		const BdAlarmId::BdAlarmIdType bdAlarmId, const Boolean initInProg)") ;

	AUX_CLASS_ASSERTION( type >= 0 && type < NUM_ALARM_FILTERS, type) ;

	if (alarmInit_[type] == TRUE)
	{
		// $[TI1]
		alarmInit_[type] = FALSE ;
		pAlarmDebouncer_[type]->initialize( bdAlarmId) ;
	}
	else
	{
		// $[TI2]
		pAlarmDebouncer_[type]->updateValue( bdAlarmId) ;
	}
	
	if (initInProg && pAlarmDebouncer_[type]->getDelayCounts() <= 0)
	{
		// $[TI3]
        pAlarmCallBack_( bdAlarmId, initInProg) ;
    }	// implied else $[TI4]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: postEventStatus()  
//
//@ Interface-Description
//		This mehod has VentStatusFilterType, status to be posted, and a
//		prompt state as	arguments and returns nothing.  This method is called
//		to filter event status messages to be posted.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If ventStatusInit_[] then initialize Debouncer, else pdate debouncer
//		with current value.
//---------------------------------------------------------------------
//@ PreCondition
//		type >= 0 && type < NUM_VENT_STATUS
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
EventFilter::postEventStatus( const EventFilter::VentStatusFilterType type,
							  const EventData::EventStatus status,
							  const EventData::EventPrompt prompt)
{
	CALL_TRACE("EventFilter::postEventStatus( const EventFilter::VentStatusFilterType type, \
			const EventData::EventStatus status, const EventData::EventPrompt prompt)");
	UNUSED_SYMBOL(prompt);
	SAFE_CLASS_ASSERTION( prompt == EventData::NULL_EVENT_PROMPT) ;
	AUX_CLASS_ASSERTION( type >= 0 && type < NUM_VENT_STATUS, type) ;

	if (ventStatusInit_[type] == TRUE)
	{
		// $[TI1]
		ventStatusInit_[type] = FALSE ;
		pVentStatusDebouncer_[type]->initialize( status) ;
	}
	else
	{
		// $[TI2]
		pVentStatusDebouncer_[type]->updateValue( status) ;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newSecondaryCycle()  
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method should be
//		called every 20 msec.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Check if any of the alarm debouncers are in a reset state, if they are,
//		notify the pAlarmCallBack_ function.  repeat for vent status debouncers.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
EventFilter::newSecondaryCycle( void)
{
	CALL_TRACE("EventFilter::newSecondaryCycle( void)") ;

	Uint32 ii = 0 ;
	
	for (ii=0; ii < EventFilter::NUM_ALARM_FILTERS; ii++)
	{
		BdAlarmId::BdAlarmIdType bdAlarmId ;
		
		if (pAlarmDebouncer_[ii]->isReset())
		{
			// $[TI1]
			bdAlarmId = (BdAlarmId::BdAlarmIdType)pAlarmDebouncer_[ii]->getDebouncedValue() ;
            pAlarmCallBack_( bdAlarmId, FALSE) ;
		}
		// $[TI2]
	}
	 
	for (ii=0; ii < EventFilter::NUM_VENT_STATUS; ii++)
	{
		EventData::EventStatus status ;
		
		if (pVentStatusDebouncer_[ii]->isReset())
		{
			// $[TI3]
			status= (EventData::EventStatus)pVentStatusDebouncer_[ii]->getDebouncedValue() ;

			EventData::EventId id = EventData::START_OF_EVENT_IDS ;
			
			switch( ii)
			{
				case EventFilter::SVO:
					// $[TI4]
					id = EventData::SVO ;
					break ;

				case EventFilter::COMP_OPERATING:
					// $[TI5]
					id = EventData::COMPRESSOR_OPERATING ;
					break ;

				case EventFilter::COMP_READY:
					// $[TI6]
					id = EventData::COMPRESSOR_READY ;
					break ;

				default:
					AUX_CLASS_ASSERTION_FAILURE( ii) ;
					break ;
			}	
			pVentStatusCallBack_( id, status, EventData::NULL_EVENT_PROMPT) ;
		}
		// $[TI7]
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
EventFilter::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, EVENTFILTER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================
