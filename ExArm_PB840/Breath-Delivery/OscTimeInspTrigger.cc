#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OscTimeInspTrigger - Inspiratory trigger during Occlusion Status Cycling.
//---------------------------------------------------------------------
//@ Interface-Description
//    This trigger is used during Occlusion Status cycling to trigger
//    an inspiration.  It is contained on a list in the 
//    BreathTriggerMediator.  It is enabled by the OscScheduler when
//    it is applicable.
//---------------------------------------------------------------------
//@ Rationale
//    This trigger implements the algorithm for determining when to 
//    transition from exhalation to inspiration during OSC.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This trigger is derived from the TimerBreathTrigger class.  The
//    method that monitors the trigger's condition is redefined for
//    this trigger.  This trigger fires when its interval timer expires
//    and pressure measured by the inspiratory pressure sensor <= 5 cmH2O.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this trigger may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/OscTimeInspTrigger.ccv   25.0.4.0   19 Nov 2013 13:59:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 004  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 003  By:  sp    Date:  31-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "OscTimeInspTrigger.hh"

//@ Usage-Classes
 
#  include "PressureSensor.hh"

#  include "MainSensorRefs.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OscTimeInspTrigger()  
//
//@ Interface-Description
//     This constructor takes an interval timer and a trigger id as arguments. 
//---------------------------------------------------------------------
//@ Implementation-Description
//     iTimer and id are passed to the base class constructor.  This 
//     constructor does not need to initialize anything.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OscTimeInspTrigger::OscTimeInspTrigger(IntervalTimer& iTimer,const Trigger::TriggerId id)
: TimerBreathTrigger(iTimer, id)
{
  CALL_TRACE("OscTimeInspTrigger(IntervalTimer& iTimer,const TriggerId id)");
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OscTimeInspTrigger()  
//
//@ Interface-Description
//	Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OscTimeInspTrigger::~OscTimeInspTrigger(void)
{
  CALL_TRACE("~OscTimeInspTrigger()");

}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
OscTimeInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, OSCTIMEINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//     This method takes no parameters, and returns a Boolean, indicating
//     whether the condition monitored by this trigger has occured.
//---------------------------------------------------------------------
//@ Implementation-Description
//     $[04207]
//     If the inspiratory pressure measured is <= 5 cmH2O and 
//     the interval timer expires, then TRUE is returned.  
//     Otherwise, false is returned.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
OscTimeInspTrigger:: triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_(void)");

  Real32 inspPressure = RInspPressureSensor.getValue();
  Boolean returnValue = FALSE;

  if ((inspPressure <= 5.0F) && timeUp_)
  {
    // $[TI1]
    returnValue = TRUE;
  }
  // $[TI2]

  //TRUE: $[TI3]
  //FALSE: $[TI4]
  return (returnValue);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
