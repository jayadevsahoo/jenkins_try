#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SubScreenButton -  A subscreen selection button.  All
// Main Settings buttons and all buttons in the Upper and Lower Screen
// Select Areas are subscreen buttons.
//---------------------------------------------------------------------
//@ Interface-Description
// SubScreenButton is a TextButton which can activate/deactivate a
// subscreen.  In addition to the construction parameters passed to
// a TextButton, SubScreenButton requires a pointer to a SubScreen.
// If this pointer is non-NULL then this subscreen will be
// activated when the button is pressed down.
//
// The only method accessed by other objects is setSubScreen(), which sets the
// subscreen which is activated by this button (for times when the subscreen
// pointer is not known at construction of SubScreenButton).
//---------------------------------------------------------------------
//@ Rationale
// Two types of buttons activate screens, Main Settings buttons and
// the screen selection buttons (or Tab Buttons) in the lower left
// part of the Upper and Lower screen.  In SubScreenButton we have
// localized the screen selection mechanism and SettingButton and
// TabButton are derived from it and, hence, both types of buttons
// receive the functionality.
//---------------------------------------------------------------------
//@ Implementation-Description
// The implementation principle behind SubScreenButton is that this button can
// be viewed as competing for control of a subscreen area (either the Upper or
// Lower subscreen area, but never both).  It is the subscreen area which
// implements the exclusivity of subscreen buttons (the fact that only one can
// be active at any one time).  When the button is pressed down it tells the
// particular subscreen area to activate its (the button's) associated
// subscreen, passing to the subscreen area pointers to the subscreen and to
// itself.  The subscreen area can then tell any previous active subscreen
// button to "un-press" and then remember the current subscreen button for a
// similar operation later on.
//
// In short, when a SubScreenButton is pressed down by the operator it calls
// the activateSubScreen() method of SubScreenArea.  When a SubScreenButton is
// pressed up by the operator it calls the deactivateSubScreen() method
// of SubScreenArea.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SubScreenButton.ccv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified constructor to take arguments for auxillary title text
//      and position, and forward on to TextButton base class.  Also,
//      added calls to new 'setAuxTitleShowFlag_()' method on down and
//      up events.  This functionality will be used to plug-in the "above
//      PEEP" phrases to the "Pi" and "Psupp" setting buttons.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SubScreenButton.hh"

//@ Usage-Classes
#include "SubScreen.hh"
#include "SubScreenArea.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SubScreenButton
//
//@ Interface-Description
// As normal with a button, SubScreenButton is first passed the dimensions,
// type, bevel size, corner type and border type of the button.  It is then
// passed the extra parameter for being a TextButton, i.e.  the titleText of
// the button (displayed within the button's label container).  The parameter
// particular to SubScreenButton is 'pSubScreen', a pointer to the subscreen
// which is activated when this button is pressed.  If this is NULL then this
// class loses its subscreen activation functionality and will act just like a
// TextButton!  Note that this pointer can be set later using the
// setSubScreen() method.
//---------------------------------------------------------------------
//@ Implementation-Description
// Simply passes any required parameters to the parent class and
// initializes all data members.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SubScreenButton::SubScreenButton(Uint16 xOrg, Uint16 yOrg, Uint16 width,
			Uint16 height, Button::ButtonType buttonType, Uint8 bevelSize,
			Button::CornerType cornerType, Button::BorderType borderType,
			StringId title, SubScreen *pSubScreen, StringId auxTitleText,
			Gravity auxTitlePos) :
			TextButton(xOrg, yOrg, width, height, buttonType, bevelSize,
					cornerType, borderType, title, auxTitleText, auxTitlePos),
			pSubScreen_(pSubScreen)
{
	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SubScreenButton()  [Destructor]
//
//@ Interface-Description
// Destroys a SubScreenButton.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SubScreenButton::~SubScreenButton(void)
{
	CALL_TRACE("SubScreenButton::~SubScreenButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: downHappened_
//
//@ Interface-Description
// This is a virtual method inherited from being a Button.  It is called when
// this button is sent into the Down state, either by the operator pressing us
// down (byOperatorAction = TRUE) or by the application calling the method
// setToDown() (byOperatorAction = FALSE).  This method causes the associated
// subscreen (if any) to be activated but only if the button press was
// performed by the operator.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this button has an associated subscreen then this subscreen is activated
// as long as byOperatorAction is TRUE.  It is activated via the subscreen's
// associated subscreen area (method activateSubScreen())
//
// $[01061] If a user selects a screen select button ...
// $[01062] A screen select button shall be selected if its corresponding
//			screen is selected.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenButton::downHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("SubScreenButton::downHappened_(Boolean byOperatorAction)");

	// show any auxillary title text that may be defined...
	setAuxTitleShowFlag_(TRUE);

	// If we have an associated subscreen then activate it (as long
	// as the operator pressed the button down).
	if (byOperatorAction && (NULL != pSubScreen_))
	{													// $[TI1]
		pSubScreen_->getSubScreenArea()->activateSubScreen(
				pSubScreen_, this, byOperatorAction);
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: upHappened_
//
//@ Interface-Description
// This is a virtual method inherited from being a Button.  It is called when
// this button is sent into the Up state, either by the operator pressing us
// down (parameter 'byOperatorAction' = TRUE) or by the application calling the
// method setToUp() ('byOperatorAction' = FALSE).  It causes the associated
// subscreen  (if any) to be deactivated but only if the button press was
// performed by the operator.
//---------------------------------------------------------------------
//@ Implementation-Description
// If this button has an associated subscreen then this subscreen is
// deactivated as long as byOperatorAction is TRUE.  It is deactivated via the
// subscreen's associated subscreen area (method deactivateSubScreen())
//
// $[01061] If a user deselects a screen select button ...
// $[01062] No screen select button shall be selected if no screen is selected.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreenButton::upHappened_(Boolean byOperatorAction)
{
	CALL_TRACE("SubScreenButton::upHappened_(Boolean byOperatorAction)");

	// remove any auxillary title text that may be shown...
	setAuxTitleShowFlag_(FALSE);

	// If the operator caused this button to go up AND we have an associated
	// subscreen then deactivate it
	if (byOperatorAction && (NULL != pSubScreen_))
	{													// $[TI1]
		pSubScreen_->getSubScreenArea()->deactivateSubScreen();
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SubScreenButton::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SUBSCREENBUTTON,
							lineNumber, pFileName, pPredicate);
}
