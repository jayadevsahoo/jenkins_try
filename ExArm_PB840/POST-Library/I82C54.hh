#ifndef I82C54_HH
#define I82C54_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================ H E A D E R   D E S C R I P T I O N ====
//@ Filename: I82C54.hh - Intel 82C54 Timer Definitions
//
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/POST-Library/vcssrc/I82C54.hhv   25.0.4.0   19 Nov 2013 14:16:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

struct I82C54RegMap
{
   Uint8  counter_0;
   Uint8  hole_1;
   Uint8  counter_1;
   Uint8  hole_3;
   Uint8  counter_2;
   Uint8  hole_5;
   Uint8  control;
};

struct CounterReg
{
   volatile Uint8  counter;
            Uint8  hole;
};

//  Alternate Register Map
struct I82C54RegMapAlt
{
   CounterReg      counterReg[3];
   Uint8           control;
};
   

// **** misc constants for timer programming
#define  COUNTER_0          (0x00)
#define  COUNTER_1          (0x40)
#define  COUNTER_2          (0x80)
#define  READ_BACK_STATUS   (0xe0)
#define  ALL_CTRS           (0x0e)
#define  STATUS_MASK        (0x3f)
#define  READ_BACK_COUNTS   (0xd0)

#endif // I82C54_HH
