//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file CConfiguration.h
//----------------------------------------------------------------------------

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#include"ICConfiguration.h"
#include"CParameter.h"
#include<xercesc/dom/DOMNode.hpp>
#include<boost/shared_ptr.hpp>
#include<set>
#include<string>


//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

/// Container of all the configuration parameters.
class CConfiguration:public IConfiguration{

    private:
	
	/// Compair operator used for stl container.
    struct CParameterOps
    {
      bool operator()( const boost::shared_ptr<CParameter>  & a, const boost::shared_ptr<CParameter>  & b ) const{
          return *(a.get()) < *(b.get()); 
      }
    };

	/// Set of all parameters.
    std::set<boost::shared_ptr<CParameter >,CParameterOps > m_CParameterSet;


    /// Translate ID to transmit ID.
    const int xlateXidToId(const int id) const;


	static int connect_to_server (char *cli_ip, short cli_port);
	static int readblock (int sock, char *buff, size_t len);
	static int writeblock (int sock, char *buff, size_t len);
	static int pushSettingsToHost (IConfiguration * pX, char *pServerName, char *pPort);


    public:

    CConfiguration();
    ~CConfiguration();

	/// Read in a context from memory.
	/// 
	/// @param ptr A pointer to a context.
	/// @param iSize Size of context.
	/// 
	/// @return 
    const bool readContext(const char *ptr, const int iSize);


	///@{
	/// Actions that can be preformed on a context.

    ///TODO Make private
    const int  makeContext(char *pBuffer, const int iLength) const;
    const bool printContext() const;
    const char * getCsv () const; 
    const bool writeContextFile(const char *fileName) ;
    const bool loadContextFile(const char *fileName);
    ///@}

	/// Write out an XML settings file.
	///
	/// @param fileName A pointer to a string containing the filename. 
	/// 
	/// @return true if able to write out an XML config file, otherwise false.
    const bool writeXMLFile(const char fileName) const;


	/// Load settings into configuration from an XML document. 
	///
	/// @param fileName A pointer to a string containing the filename.
	/// 
	/// @return true if able to load settings from XML, otherwise false.
    const bool loadXMLDocument(const char *fileName);

	///@{
	/// Set a parameter givin parmeter name and value.
	/// 
	/// @return true if was able to set the parameter. 
    const bool setParam(const char *pParamName, const char *pszValue);
    const bool setParam(const int iValue, const char *pszValue);
    const bool setParam(const int iValue, const float fValue);
    const bool setParam(const char *pParamName, const float fValue);
	///@}



   int sendSettings(char **str, char *pHost, char *pPort);



    void destroy();


	/// Add a paramter from a DOMNode pointer.
	/// @param ptr Pointer to a DOMNode.
	/// @param pThis Pointer to an instance of this class.
    static int addParam(XERCES_CPP_NAMESPACE::DOMNode *ptr, void *pThis);


};


