#ifndef IeRatioSettingButton_HH
#define IeRatioSettingButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: IeRatioSettingButton - A setting button which implements the
// setting display particular to the I to E Ratio, i.e. 1:num or num:1.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/IeRatioSettingButton.hhv   25.0.4.0   19 Nov 2013 14:07:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001   By: xxx  Date:  05-Dec-1998    DR Number:  <NONE>
//  Project:  BILEVEL
//  Description:
//		Initial BiLevel version.
//
//====================================================================


//@ Usage-Classes
#include "SettingButton.hh"
//@ End-Usage

class IeRatioSettingButton : public SettingButton
{
public:
	IeRatioSettingButton(Uint16 xOrg, Uint16 yOrg, Uint16 width,
			Uint16 height, ButtonType buttonType, Uint8 bevelSize,
			CornerType cornerType, BorderType borderType,
			TextFont::Size valuePointSize,
			Uint16 valueCenterX, Uint16 valueCenterY,
			StringId titleText, SettingId::SettingIdType settingId,
			SubScreen *pFocusSubScreen, StringId messageId,
			Boolean isMainSetting = FALSE);

	~IeRatioSettingButton(void);

	// SettingObserver virtual method
    virtual void  applicabilityUpdate(
                              const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject
                                     );

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	// SettingButton virtual method
	virtual void updateDisplay_(Notification::ChangeQualifier qualifierId,
                                const SettingSubject*         pSubject);


private:
	// these methods are purposely declared, but not implemented...
	IeRatioSettingButton(const IeRatioSettingButton&);		// not implemented...
	void   operator=(const IeRatioSettingButton&);	// not implemented...

	enum
	{
		//@ Constant: IRSB_MAX_VALUE_STRING_LENGTH
		// The maximum size of the buffer which contains the cheap text for
		// the I:E Ratio value string (see below for more info).
		IRSB_MAX_VALUE_STRING_LENGTH = 32
	};

	//@ Data-Member: valuePointSize_
	// The point size of the text which displays the I:E Ratio value.
	TextFont::Size  valuePointSize_;

	//@ Data-Member: valueCenterX_
	// The X coordinate of the point at which the center of the I:E Ratio
	// value is positioned.
	Uint16 valueCenterX_;

	//@ Data-Member: valueCenterY_
	// The Y coordinate of the point at which the center of the I:E Ratio
	// value is positioned.
	Uint16 valueCenterY_;

	//@ Data-Member: valueText_
	// The Text Field which displays the I:E Ratio value in either 1:num
	// or num:1 format.
	TextField valueText_;

	//@ Data-Member: valueString_
	// A character array used for storing the cheap text that specifies
	// the I:E Ratio value for valueText_.
	wchar_t valueString_[IRSB_MAX_VALUE_STRING_LENGTH];  
};


#endif // IeRatioSettingButton_HH 
