#ifndef EXH_FLOW_SENSOR_CALIBRATION_H

#define EXH_FLOW_SENSOR_CALIBRATION_H

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: ExhFlowSensorCalibrationTable_t - Implements calibration for
//			exhalation flow sensor.
//***********************************************************************

#include "CalibrationTable.h"

enum ExhCalState_t
{
	INVALID_STATE = -1,
	FEXCAL_FLUSH_E,
	FEXCAL_AZ_INIT_E,
	FEXCAL_AZ_WAIT_E,
	FEXCAL_AUTOZERO_E,
	FEXCAL_INIT_E,
	FEXCAL_ZERO_E,
	FEXCAL_SERVO_E,
	FEXCAL_CHECK_E,
	FEXCAL_FLOW_AVG_E,
	FEXCAL_WAIT_E,
	FEXCAL_PRINT_E,
	FEXCAL_SAVE_E,
	FEXCAL_FAILED_E,
};

class ExhFlowSensorCalibrationTable_t : public CalibrationTable_t
{
	//friend class ServosThread_t;

public:	
	ExhFlowSensorCalibrationTable_t(CalibrationID_t CalID, AnalogOutputs_t OutputAir, AnalogOutputs_t OutputO2);
	CalibrationStatus_t		Calibrate(Flow_t FilteredFlow, InspFlowType_t FlowType);

private:
	bool DoCalibration(Flow_t FilteredFlow, InspFlowType_t FlowType);

	AnalogOutputs_t AnalogOutput;
	AnalogOutputs_t AnalogOutputAir;
	AnalogOutputs_t AnalogOutputO2;

	Flow_t			FexFlowTab[EXH_FLOW_SUBTABLE_SIZE];
	uint16_t		FexAdcTab[EXH_FLOW_SUBTABLE_SIZE];
	ExhCalState_t	ExhCalState;
	uint8_t			AutozeroCount;

	float			*pCalData;
};

#endif