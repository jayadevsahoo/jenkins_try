
#ifndef IeRatioSetting_HH
#define IeRatioSetting_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  IeRatioSetting - Inspiratory to expiratory ratio setting
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/IeRatioSetting.hhv   25.0.4.0   19 Nov 2013 14:27:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009  By: sah     Date:  18-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added new 'calcDependentValues_()' methods for determining list
//         of dependents
//
//  Revision: 008   By: sah    Date: 29-Jun-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//	NeoMode project-related changes:
//	*  modified to use newly-modified 'BoundedRange' class to handle
//         range, via linked list of interval nodes
//      *  added new 1.00:1 soft limit
//
//  Revision: 007   By: sah   Date:  04-Feb-1999    DR Number: 5314
//  Project:  ATC
//  Description:
//	Added support for new applicability functionality:
//	*  moved BiLevel-specific implementation (including all soft-bound
//	   implementation) to new H:L ratio setting
//	*  added new 'getApplicability()' method
//	*  added 'acceptTransition()' method due to new Transition Rule
//	   implementation
//	*  now an observer of constant parm setting, therefore 'valueUpdate()',
//	   'doRetainAttachment()' and 'settingObserverInit()' overridden from
//	   observer base class
//
//  Revision: 006   By: dosman Date:  08-Sep-1998    DR Number: BILEVEL 130
//  Project:  BILEVEL
//  Description:
//      Added overrideAllSoftBoundStates_ because sometimes it is necessary to
//	disable all the soft bounds for this setting.
//      Changed name of method to updateAllSoftBoundStates_ to clarify that all
//      soft bounds for this setting are updated through this method.
//
//  Revision: 005   By: dosman Date:  16-Jun-1998    DR Number: 116
//  Project:  BILEVEL
//  Description:
//  change updateSoftBoundState_() to handle case when setting is unchanged
//
//  Revision: 004   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 003   By: dosman Date:  19-Feb-1998    DR Number:
//  Project:  BILEVEL
//  Description:
//	Initial BiLevel version.
//      Changed QuintupleIntervalRange to SeptupleIntervalRange
//
//  Revision: 002   By: sah    Date:  18-Sep-1997    DR Number: 2360
//  Project: Sigma (R8027)
//  Description:
//	Intermediate calculations no longer need to be "warped" to a
//	resolution value.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
#include "BatchBoundedSetting.hh"
#include "SettingObserver.hh"
//@ End-Usage


class IeRatioSetting : public BatchBoundedSetting, public SettingObserver
{
  public:
    IeRatioSetting(void); 
    virtual ~IeRatioSetting(void);

    virtual Applicability::Id  getApplicability(
			    const Notification::ChangeQualifier qualifierId
					       ) const;

    virtual SettingValue  getNewPatientValue(void) const;

    virtual const BoundStatus*  acceptPrimaryChange(
				    const SettingId::SettingIdType primaryId
						   );

    virtual void  acceptTransition(const SettingId::SettingIdType settingId,
				   const DiscreteValue            newValue,
				   const DiscreteValue            currValue);

    // SettingObserver methods...
    virtual void  valueUpdate(const Notification::ChangeQualifier qualifierId,
			      const SettingSubject*               pSubject);
    virtual Boolean  doRetainAttachment(const SettingSubject* pSubject) const;

    virtual void  settingObserverInit(void);

#if defined(SIGMA_DEVELOPMENT)
    virtual Boolean  isAcceptedValid(void) const;
    virtual Boolean  isAdjustedValid(void);
#endif // defined (SIGMA_DEVELOPMENT)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  protected:
    virtual Boolean calcDependentValues_(const Boolean isValueIncreasing);
    virtual void  updateConstraints_(void);

    virtual void  findSoftMinMaxValues_(Real32& rSoftMinValue,
					Real32& rSoftMaxValue) const;

    virtual BoundedValue  calcBasedOnSetting_(
			      const SettingId::SettingIdType basedOnSettingId,
			      const BoundedRange::WarpDir    warpDirection,
			      const BasedOnCategory          basedOnCategory
					     ) const;

  private:
    IeRatioSetting(const IeRatioSetting&);	// not implemented...
    void  operator=(const IeRatioSetting&);	// not implemented...
};


#endif // IeRatioSetting_HH 
