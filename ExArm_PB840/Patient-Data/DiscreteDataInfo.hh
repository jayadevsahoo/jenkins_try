
#ifndef DiscreteDataInfo_HH
#define DiscreteDataInfo_HH

#if		defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
//@ Class: DiscreteDataInfo - a processing agent class for the Discrete 
//         breath data type.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/DiscreteDataInfo.hhv   10.7   08/17/07 10:27:34   pvcs  
//
//@ Modification-Log
//
//  Revision: 002  By: sah   Date:  13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//  
//  Revision: 001  By:  jhv    Date:  26-Jan-94    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================


#include  "DiscreteValue.hh"
#include  "BreathItemDataInfo.hh"
#include  "DataValue.hh"
#include  "PatientDataId.hh"

class DiscreteDataInfo:public BreathItemDataInfo {
  public:
    DiscreteDataInfo(const PatientDataId::PatientItemId id, const DiscreteValue min,
    	const DiscreteValue max );
    virtual ~DiscreteDataInfo(void);  
    inline Boolean isValid(DiscreteValue value);
	virtual void setDataTimeout(void);

    static void  SoftFault(const SoftFaultID softFaultID, 
						   const Uint32      lineNumber, 
						   const char*       pFileName = NULL, 
						   const char*       pBoolTest = NULL); 
  protected:
    virtual void modifyData_(void *pValue );
  private:
      DiscreteDataInfo(void);       // not implemented.
	  DiscreteDataInfo(const DiscreteDataInfo& range); // not implemented ...
	  void  operator=(const DiscreteDataInfo&); // not implemented...

  //@Data-Member: MIN_VALUE_
  // MIN_VALUE_ defines the smallest value allowed for this type
  	const DiscreteValue MIN_VALUE_;

  //@Data-Member: MAX_VALUE_
  // MAX_VALUE_ defines the largest value allowed for this type
	const DiscreteValue MAX_VALUE_;
};


// Inlined methods...
#include "DiscreteDataInfo.in"

#endif		//defined(SIGMA_GUI_CPU) || defined(SIGMA_COMMON)
#endif  // DiscreteDataInfo_HH
