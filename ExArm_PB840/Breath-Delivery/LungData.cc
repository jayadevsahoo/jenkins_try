#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sotred in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LungData - The class for central location to calculate the
//		related lung flow and volume data.
//---------------------------------------------------------------------
//@ Interface-Description
//      This is the class with methods to provide various lung flow and volume
//		information for other client object usage.
//
//      1)  A method to getLungFlow
//		2)  A method to getFilteredLungFlow
//		3)	A method to getPeakInspLungFlow
//		4)	A method to getPeakExpLungFlow
//		5)	A method to getLungAlpha
//		6)	A method to getInspLungVolume
//		7)	A method to getExhLungVolume
//		8)	A method to getInspLungBtpsVolume
//		9)	A method to getExhLungBtpsVolume
//		10)	A method to getLungFlowLimit
//      11) A method to getLungPressure
//---------------------------------------------------------------------
//@ Rationale
// 		This class serves as a central control to provide the lung flow and
//		volume information for the BD sub-system.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The class provides access (get methods) to the current lung flow
//      and volume calculations which are stored in the LungData object.
//      These values are updated very BD cycle through the newCycle() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/LungData.ccv   25.0.4.0   19 Nov 2013 13:59:52   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006   By:   rhj    Date: 07-Nov-2008      SCR Number: 6435
//  Project:  S840
//  Description:
//       Leak Compensation project-related changes.
//       Modified calculateLungFlow_() function to add leak compensation 
//       algorithms.
// 
//  Revision: 005   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 004   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Code formatting change only.
//
//  Revision: 003   By: syw   Date:  01-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Implemented new lung flow calculation.
//
//  Revision: 002  By:  syw    Date:  23-Jun-1999    DR Number: DCS 5447
//       Project:  ATC
//       Description:
//			Add DCS 5322 to header
//
//  Revision: 001  By:  yyy    Date:  07-Jan-1999    DR Number: DCS 5322
//       Project:  ATC (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "LungData.hh"


//@ Usage-Classes

#include <math.h>
#include "MainSensorRefs.hh"
#include "PressureSensor.hh"
#include "BreathSet.hh"
#include "BreathRecord.hh"
#include "CircuitCompliance.hh"
#include "FlowSensor.hh"
#include "ExhFlowSensor.h"
#include "PavManager.hh"
#include "NovRamManager.hh"
#include "HumidTypeValue.hh"
#include "PavFilters.hh"
#include "BreathMiscRefs.hh"
#include "PhasedInContextHandle.hh"
#include "LeakCompEnabledValue.hh"
#include "SettingId.hh"
#include "ControllersRefs.hh"
#include "ExhValveIController.hh"
#include "LeakCompMgr.hh"
#include "BreathMiscRefs.hh"

//@ End-Usage

//@ Code...

//@ Constant: SEC_TO_MIN_CF
//  second to minute conversion factor
static const Real32 SEC_TO_MIN_CF = 60.0F ;

//@ Constant: MIN_TO_SEC_CF
//  minute to second conversion factor
static const Real32 MIN_TO_SEC_CF = 60.0F ;


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LungData
//
//@ Interface-Description
//	Default Contstructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      Various private data members are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
LungData::LungData(void)
{
	CALL_TRACE("LungData::LungData(const LungDataType::PhaseType phase)");

   	// $[TI1]

	piAlphaFiltered_ = RInspPressureSensor.getValue();
	peAlphaFiltered_ = RExhPressureSensor.getValue();

	inspLungVolume_ = 0.0;
	exhLungVolume_ = 0.0;

	lungFlow_ = 0.0;
	lungPressure_ = 0.0;
	filteredLungFlow_ = 0.0;
	
	peakInspLungFlow_ = 0.0f;
	peakExpLungFlow_ = 0.0f;

	totalTrajLimitReached_ = FALSE;
	
    NovRamManager::GetExhResistance( exhResistance_);
    NovRamManager::GetInspResistance( inspResistance_);

	centerPinsp_1_ = RInspPressureSensor.getValue() ;
	centerPinspSlope_1_ = 0.0 ;
	centerPinspSlope_2_ = 0.0 ;
	
	centerPexh_1_ = RExhPressureSensor.getValue() ; 
	centerPexhSlope_1_ = 0.0 ;
	centerPexhSlope_2_ = 0.0 ;

	peAlphaFiltered_1_ = peAlphaFiltered_ ;
	pExhSlope_1_ = 0.0 ;
	pExhSlope_2_ = 0.0 ;

	lungFlowNegDuringExh_ = FALSE ;
	autozeroOccurred_ = FALSE ;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~LungData 
//
//@ Interface-Description
//      Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//      none
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
LungData::~LungData(void)
{
	CALL_TRACE("LungData::~LungData()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()
//
//@ Interface-Description
// 		This method has no arguments and has no return value.  This method is
// 		called every BD cycle to calculate various lung flow and volume
//		information for other client object usage.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Use alpha beta filter to compute filtered pressure and slope values
//		for both inspPress and exhPress.  Use the total insp and exp flow
//		to compute lung flow every BD cycle.  Compute lungAlpha during
//		inspiration phase.  During inspiration, calculate inspLungVolume_ by
//		integrating lungFlow, and recording the peakInspLungFlow_.  During 
//		expiration, calculate expLungVolume_ by integrating lungFlow, and
//		recording the peakExpLungFlow_.  
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LungData::newCycle( void)
{
	CALL_TRACE("LungData::newCycle( void)");

	// Compute filtered values and slope values for inspPress and exhPress
	// every BD cycle
	Real32 inspPress = RInspPressureSensor.getValue();
	Real32 exhPress = RExhPressureSensor.getValue();

	if (RPressureXducerAutozero.getIsAutozeroInProgress() && autozeroOccurred_ == FALSE)
	{
	  	// $[TI6]
		autozeroOccurred_ = TRUE ;
		
		if (RPressureXducerAutozero.getAutozeroSide() == PressureXducerAutozero::INSP)
		{
		  	// $[TI7]
			autozeroSide_ = PressureXducerAutozero::INSP ;
		}
		else if (RPressureXducerAutozero.getAutozeroSide() == PressureXducerAutozero::EXH)
		{
		  	// $[TI8]
			autozeroSide_ = PressureXducerAutozero::EXH ;
		}
		else
		{
			AUX_CLASS_ASSERTION_FAILURE( RPressureXducerAutozero.getAutozeroSide()) ;
		}
	}
  	// $[TI9]

	// Get the total flow
	Real32 inspFlow = RAirFlowSensor.getValue() + RO2FlowSensor.getValue() ;
	Real32 exhFlow = RExhFlowSensor.getDryValue() ;

	// $[PA24041]
	Real32 flow1 = exhFlow ;
	Real32 flow2 = inspFlow ;
	const Real32 FACTOR = 0.92 ;
		
    if (PhasedInContextHandle::GetDiscreteValue( SettingId::HUMID_TYPE) != HumidTypeValue::HME_HUMIDIFIER)
    {
	  	// $[TI2]
    	flow1 /= FACTOR ;
    	flow2 /= FACTOR ;
    }
  	// $[TI3]

	
	if (autozeroOccurred_)
	{
	  	// $[TI10]
		if (autozeroSide_ == PressureXducerAutozero::INSP)
		{
		  	// $[TI11]
			inspPress = exhPress + exhResistance_.calculateDeltaP( flow1)
					+ inspResistance_.calculateDeltaP( flow2) ;
		}
		else if (autozeroSide_ == PressureXducerAutozero::EXH)
		{
		  	// $[TI12]
			exhPress = inspPress - inspResistance_.calculateDeltaP( flow2)
					- exhResistance_.calculateDeltaP( flow1) ;
		}
		else
		{
			AUX_CLASS_ASSERTION_FAILURE( autozeroSide_) ;
		}
	}
  	// $[TI13]
	
	// $[PA24041]
	const Real32 PI_ALPHA = 0.65 ;
	const Real32 PE_ALPHA = 0.45 ;
	
	piAlphaFiltered_ = piAlphaFiltered_ * PI_ALPHA + inspPress * (1.0F - PI_ALPHA) ;
	peAlphaFiltered_ = peAlphaFiltered_ * PE_ALPHA + exhPress * (1.0F - PE_ALPHA) ;

	BreathPhaseType::PhaseType phaseType = BreathPhase::GetCurrentBreathPhase()->getPhaseType();

	Real32 pWye ;

	if (phaseType == BreathPhaseType::INSPIRATION ||
			phaseType == BreathPhaseType::INSPIRATORY_PAUSE)
	{
	  	// $[TI4]
		pWye = peAlphaFiltered_ + exhResistance_.calculateDeltaP( flow1) ;
	}
	else
	{
	  	// $[TI5]
		pWye = piAlphaFiltered_ - inspResistance_.calculateDeltaP( flow2) ;
	}

	// $[PA24039]
	const Real32 GAIN = 5.0 ;
	Real32 centerPinsp = (piAlphaFiltered_ + pWye) / 2.0F ;
	Real32 centerPinspSlope = PavFilters::SlopeEstimator( centerPinsp,
				centerPinsp_1_, centerPinspSlope_1_,
				centerPinspSlope_2_, GAIN) ;

	// $[PA24040]
	Real32 centerPexh = (peAlphaFiltered_ + pWye) / 2.0 ;
	Real32 centerPexhSlope = PavFilters::SlopeEstimator( centerPexh,
				centerPexh_1_, centerPexhSlope_1_,
				centerPexhSlope_2_, GAIN) ;

	// $[PA24042]
	Real32 pExhSlope = PavFilters::SlopeEstimator( peAlphaFiltered_,
				peAlphaFiltered_1_, pExhSlope_1_,
				pExhSlope_2_, GAIN) ;

	// Compute insp and exh side tubing flow every BD cycle
    Real32 inspTime = (RBreathSet.getCurrentBreathRecord())->getInspTime();
    Real32 phaseDurationMs = (RBreathSet.getCurrentBreathRecord())->getBreathDuration();

	// Compute lungAlpha and filteredLungFlow every BD cycle
	
	if (phaseType == BreathPhaseType::INSPIRATION ||
		phaseType == BreathPhaseType::INSPIRATORY_PAUSE)
	{
 												  	// $[TI1.1]
		// Compute lung flow every BD cycle
		calculateLungFlow_(inspFlow, exhFlow, phaseDurationMs, phaseType);

		// Compute lungAlpha during inspiration
		//	$[TC04013] 
		updateInspLungAlpha_(inspTime, exhPress);
		
		if (inspTime == CYCLE_TIME_MS)
		{	// Reset the inspLungVolume_
 												  	// $[TI1.1.1]
			//	$[TC04009] 
			inspLungVolume_ = 0;
			peakInspLungFlow_ = 0;
			totalTrajLimitReached_ = FALSE;
			lungFlowNegDuringExh_ = FALSE ;
		}										  	// $[TI1.1.2]

		// Integrate lungFlow during inspiration to obtain inspLungVolume_
		//	$[TC04009] 
		inspLungVolume_ += lungFlow_ * CYCLE_TIME_MS / MIN_TO_SEC_CF;

		//	$[TC04030] g
		//	$[TC04038]
		peakInspLungFlow_ = MAX_VALUE( lungFlow_, peakInspLungFlow_);
		// $[TI1.1.3] lungFlow_ is MAX
		// $[TI1.1.4] peakInspLungFlow_ is MAX

		autozeroOccurred_ = FALSE ;
	}
	else if (phaseType == BreathPhaseType::EXHALATION ||
			 phaseType == BreathPhaseType::EXPIRATORY_PAUSE ||
			 phaseType == BreathPhaseType::BILEVEL_PAUSE_PHASE)
	{
 		Real32 exhTime = phaseDurationMs-inspTime;
 												  	// $[TI1.2]
		// Compute lung flow every BD cycle
		calculateLungFlow_(inspFlow, exhFlow, exhTime, phaseType);

		if (exhTime == CYCLE_TIME_MS)
		{										  	// $[TI1.2.1]
			// Reset the exhLungVolume_
			//	$[TC04010] 
			// Initialize the input to the filter LfilteredLungFlow_
			//	$[PA24048] 
			exhLungVolume_ = 0;
			peakExpLungFlow_ = 0;
			totalTrajLimitReached_ = FALSE;
			prevFilteredLungFlow_ = filteredLungFlow_ = lungFlow_;
		}
	  	// $[TI1.2.2]

		if (!BreathRecord::IsExhFlowFinished())
		{										  	// $[TI1.2.3]
			// Integrate lungFlow during exhalation while condition for terminating
			// spirometry is not met to obtain exhLungVolume_
			//	$[TC04010] 
			exhLungVolume_ -= lungFlow_ *  CYCLE_TIME_MS / MIN_TO_SEC_CF;
		}										  	// $[TI1.2.4]
		
		//	$[TC04030] g
		//	$[TC04038]
		peakExpLungFlow_ = MAX_VALUE( -1 * lungFlow_,  peakExpLungFlow_);
		// $[TI1.2.5] lungFlow_ is MAX
		// $[TI1.2.6] peakExpLungFlow_ is MAX
	}											  	// $[TI1.3]

	Real32 wyePressure = exhPress + exhResistance_.calculateDeltaP( ABS_VALUE( exhFlow)) ;

	lungPressure_ = computeLungPressure_( lungFlow_, wyePressure) ;
	
	// Compute filteredLungFlow
	updateFilteredLungFlow_();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LungData::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("LungData::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, LUNGDATA,
							lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateLungFlow_()
//
//@ Interface-Description
// 		This method has three arguments to calculate the lung flow.
//		inspFlow	- total inspiratory flow.
//		exhFlow	- total expiratory flow.
//		phaseDurationMs	- elapsed time (in ms) in the phase.
//		phaseType - phase type
//---------------------------------------------------------------------
//@ Implementation-Description
//		Get the tube compliance, based on this value to calculate the
//		inspiratory side of tube flow and expiratory side of tube flow.
//		The total tube flow is calculated by adding the inspiratory and
//		expiratory side of tube flow values.  The net lung flow is
//		calculated by subtracting the expiratory flow, the tube flow, 
//		manifold flow and humidifier flow from the total inspiratory flow.
//      If Leak Compensation is enabled, the net lung flow is
//		calculated by subtracting the expiratory flow, the tube flow, 
//		manifold flow, humidifier flow from the total inspiratory flow
//      and amount of leak loss.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LungData::calculateLungFlow_(const Real32 inspFlow, const Real32 exhFlow,
			const Real32 phaseDurationMs, const BreathPhaseType::PhaseType phaseType)
{
	CALL_TRACE("LungData::calculateLungFlow_(const Real32 inspFlow, \
			const Real32 exhFlow, const Real32 phaseDurationMs, \
			const BreathPhaseType::PhaseType phaseType)");

	Real32 tubeCompl = 0.0 ;
	
	if (phaseType == BreathPhaseType::INSPIRATION ||
		phaseType == BreathPhaseType::INSPIRATORY_PAUSE ||
		phaseType == BreathPhaseType::PAV_INSPIRATORY_PAUSE)
	{
	   	// $[TI1]
		// $[PA24047]
		tubeCompl = RCircuitCompliance.getComplianceVolume(1, (Uint32)phaseDurationMs,
						CircuitCompliance::TUBE_COMPLIANCE) ;
	}
	else if (phaseType == BreathPhaseType::EXHALATION ||
			 phaseType == BreathPhaseType::EXPIRATORY_PAUSE ||
			 phaseType == BreathPhaseType::BILEVEL_PAUSE_PHASE)
	{
	   	// $[TI2]
		// $[PA24047]
		Real32 exhDurationMs = 3.0F * RCircuitCompliance.getLowFlowTime() - phaseDurationMs ;

		if (exhDurationMs < 5)
		{
			// $[TI10]
			exhDurationMs = 5 ;
		}
		// $[TI11]

		tubeCompl = RCircuitCompliance.getComplianceVolume(1, (Uint32)exhDurationMs,
						CircuitCompliance::TUBE_COMPLIANCE, FALSE);
	}
   	// $[TI3]

	// $[PA24042]
	Real32 manifoldFlow = RCircuitCompliance.getBacteriaFilterCompliance() * pExhSlope_1_ * SEC_TO_MIN_CF ;
	Real32 humidifierFlow = RCircuitCompliance.getHumidifierCompliance() * centerPinspSlope_1_ * SEC_TO_MIN_CF ;

	Real32 inspTubingFlow = tubeCompl * centerPinspSlope_1_ * SEC_TO_MIN_CF ;

	Real32 exhTubingFlow = tubeCompl * centerPexhSlope_1_ * SEC_TO_MIN_CF ;
		
	Real32 tubeFlow = inspTubingFlow + exhTubingFlow ;



    // If Leak Compensation is enabled, calculate the leak compensated
	// lung flow, otherwise use the original algorithm to calculate lung flow.
	// $[LC24031] $[LC24036]
	const DiscreteValue  LEAK_COMP_ENABLED_VALUE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::LEAK_COMP_ENABLED);

	if (LEAK_COMP_ENABLED_VALUE == LeakCompEnabledValue::LEAK_COMP_ENABLED)
	{
	
		lungFlow_  = inspFlow - exhFlow - tubeFlow - manifoldFlow - humidifierFlow - 
											   RLeakCompMgr.getFilteredLeakFlow();

		if (phaseType == BreathPhaseType::INSPIRATION)
		{
			
			if( (lungFlow_ < 0.0F) && (RExhValveIController.getErrorGain() < 0.01F) )
			{
				lungFlow_ = 0.0F;
			}

		}

	}
	else
	{
		lungFlow_ = inspFlow - exhFlow - tubeFlow - manifoldFlow - humidifierFlow ;
	}


	// $[PA24048]
	const Uint32 FLOW_LIMIT_TIME = 100 ;
		
	if (phaseType == BreathPhaseType::EXHALATION ||
		phaseType == BreathPhaseType::EXPIRATORY_PAUSE ||
		phaseType == BreathPhaseType::BILEVEL_PAUSE_PHASE)
	{
	   	// $[TI4]
		if (lungFlow_ < 0.0)
		{
		   	// $[TI5]
			lungFlowNegDuringExh_ = TRUE ;
		}
	   	// $[TI6]
		
		if (lungFlowNegDuringExh_ && phaseDurationMs < FLOW_LIMIT_TIME && lungFlow_ > 0.0)
	  	{
		   	// $[TI7]
	  		lungFlow_ = 0.0 ;
	  	}
	   	// $[TI8]
	}
	// $[TI9]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateInspLungAlpha_()
//
//@ Interface-Description
// 		This method has two arguments to calculate the lungAlpha_.
//		inspTime	- inspiratory time.
//		exhPress	- expiratory pressure
//---------------------------------------------------------------------
//@ Implementation-Description
//		Calculate the lungAlpha_ based on various conditions.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LungData::updateInspLungAlpha_(Real32 inspTime, Real32 exhPress)
{
	CALL_TRACE("LungData::updateInspLungAlpha_(Real32 inspTime, Real32 exhPress)");

	Real32 tubeSize = RTube.getId();

	//	$[TC04013] 
	if (totalTrajLimitReached_)
	{											  	// $[TI1.1]
		lungAlpha_ = -0.0167F * tubeSize + 1.0167F;
	}
	else if (inspTime > 100.0)
	{											  	// $[TI1.2]
		lungAlpha_ = 0.75F + 0.0035F * exhPress;
	}
	else
	{											  	// $[TI1.3]
		lungAlpha_ = 0.7;
	}

	if (lungAlpha_ < 0.0)
	{											  	// $[TI2.1]
		lungAlpha_ = 0.0f;
	}
	else if (lungAlpha_ > 0.95)
	{											  	// $[TI2.2]
		lungAlpha_ = 0.95;
	}											  	// $[TI2.3]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateFilteredLungFlow_()
//
//@ Interface-Description
// 		This method has no argument and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Adjust alpha based on lungFlow_ and filteredLungFlow_ and set
//		filteredLungFlow_.  When the first exhalation cycle starts,
//		update the prevFilteredLungFlow_ value.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
LungData::updateFilteredLungFlow_(void)
{
	CALL_TRACE("LungData::updateFilteredLungFlow_()");

	Real32 alpha = 0.65;
	
    Real32 inspTime = (RBreathSet.getCurrentBreathRecord())->getInspTime();
    Real32 phaseDurationMs = (RBreathSet.getCurrentBreathRecord())->getBreathDuration();
	BreathPhaseType::PhaseType phaseType = BreathPhase::GetCurrentBreathPhase()->getPhaseType();

	// $[PA24048]
	// $[PA24050]
	if (phaseType == BreathPhaseType::EXHALATION ||
		phaseType == BreathPhaseType::EXPIRATORY_PAUSE ||
		phaseType == BreathPhaseType::BILEVEL_PAUSE_PHASE)
	{
 		Real32 exhTime = phaseDurationMs - inspTime ;
 		if( exhTime < 200 )
 		{
 			alpha = 0.5 ;
 		}
 	}
   	prevFilteredLungFlow_ = filteredLungFlow_;

   	if (lungFlow_ > filteredLungFlow_)
   	{					// $[TI1.1]
   		alpha = 0.85;
   	}					// $[TI1.2]

	//	$[TC04043]
	filteredLungFlow_ = (1.0F - alpha) * lungFlow_ +
						alpha * filteredLungFlow_;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: computeLungPressure_()
//
//@ Interface-Description
// 		This method has absolute lung flow and wye pressure as arguments
//		and returns the lung pressure or carina pressure depending on
//		the support type.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Depending on the support type, the lung pressure is calculated.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
LungData::computeLungPressure_( const Real32 lungFlow, const Real32 wyePressure)
{
	CALL_TRACE("LungData::computeLungPressure_( const Real32 lungFlow, \
									const Real32 wyePressure)") ;

	Real32 lungPressure = 0.0 ;
	Real32 absLungFlow = ABS_VALUE( lungFlow) ;
	Real32 cFactor = 1.0;
	BreathPhaseType::PhaseType phaseType = BreathPhase::GetCurrentBreathPhase()->getPhaseType();
	
   	DiscreteValue supportType = 
					PhasedInContextHandle::GetDiscreteValue( SettingId::SUPPORT_TYPE) ;
    
    if (phaseType == BreathPhaseType::INSPIRATION ||
			phaseType == BreathPhaseType::INSPIRATORY_PAUSE ||
			phaseType == BreathPhaseType::PAV_INSPIRATORY_PAUSE)
	  {
	     cFactor = Btps::GetInspBtpsCf() ;
	  }
	else
	  {
	     cFactor = Btps::GetExpBtpsCf() ;
	  }

    
	if (supportType == SupportTypeValue::ATC_SUPPORT_TYPE)
	{
	  	// $[TI1]
		if (lungFlow > 0.0)
		{
		  	// $[TI2]
			lungPressure = wyePressure - RTube.getPressDrop( absLungFlow) ;
		}
		else
		{
		  	// $[TI3]
			lungPressure = wyePressure + RTube.getPressDrop( absLungFlow) ;
		}
	}
	else if (supportType == SupportTypeValue::PAV_SUPPORT_TYPE)
	{
	  	// $[TI4]

		if (lungFlow > 0.0)
		{
		  	// $[TI5]
			lungPressure = wyePressure - RTube.getPressDrop( absLungFlow)
							- (RPavManager.getRPatient( PavState::CLOSED_LOOP) * (cFactor * absLungFlow)) ;
		}
		else
		{
		  	// $[TI6]
			lungPressure = wyePressure + RTube.getPressDrop( absLungFlow)
							+  (RPavManager.getRPatient( PavState::CLOSED_LOOP) * (cFactor * absLungFlow)) ;
		}
	}
	else
	{
	  	// $[TI7]
		lungPressure = wyePressure ;
	}
	
	return( lungPressure) ;
}
