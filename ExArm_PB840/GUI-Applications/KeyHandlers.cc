#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: KeyHandlers - A class whose purpose is as a repository for
// various offscreen key handler classes such as ExpiratoryPauseHandler
// and ManualInspirationHandler.  
//---------------------------------------------------------------------
//@ Interface-Description
// This is a class which manages the allocated space for all the KeyPanel 
// key classes: the Expiratory Pause Handler class, the Manual Inspiratory
// Handler class, the Hundred Percent O2 Handler class, the Alarm Volume Handler
// class, the Screen Brightness Handler class, the Screen Contrast Handler
// class, the ScreenLock Handler class and the InspiratoryPause Handler class.  
//---------------------------------------------------------------------
//@ Rationale
//	Used to manage memory and construction of all key panel buttons in one shot.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor of this class does nothing.
//---------------------------------------------------------------------
//@ Fault-Handling
//  
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/KeyHandlers.ccv   25.0.4.0   19 Nov 2013 14:08:02   pvcs  $
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  hhd		Date:  15-MAY-95    DR Number: 
//  Project:  Sigma (R8027)
//  Description:
//  	Integration baseline.
//=====================================================================

#include "KeyHandlers.hh"



//@ Code...
// 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: KeyHandlers()  [Default Constructor]
//
//@ Interface-Description
//	Constructor.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Empty. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

KeyHandlers::KeyHandlers(void) 
{
        CALL_TRACE("KeyHandlers::KeyHandlers(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~KeyHandlers  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

KeyHandlers::~KeyHandlers(void)
{
        CALL_TRACE("KeyHandlers::~KeyHandlers(void)");

        // Do nothing
}



#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
KeyHandlers::SoftFault(const SoftFaultID  softFaultID,
				  const Uint32       lineNumber,
				  const char*        pFileName,
				  const char*        pPredicate)  
{
	CALL_TRACE("KeyHandlers::SoftFault(softFaultID, lineNumber, pFileName,"
												" pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, KEYHANDLERS,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
