#pragma once

#include <list>

// TabThree dialog

class CTabThree : public CDialog
{
	DECLARE_DYNAMIC(CTabThree)

public:
	CTabThree(CWnd* pParent);   // standard constructor
	virtual ~CTabThree();

// Dialog Data
	enum { IDD = IDD_DIALOG_TAB3 };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	CDC m_dcMonitorBkgnd;
	CBitmap m_bmpMonitorBkgnd;
	CDC m_dcMemMonitorArea;		// device context for monitor area
	CBitmap m_MonitorsBufferBmp;// used to buffering the monitor area

	int m_rows;					// monitors cell rows
	int m_columns;				// monitors cell columns
	int m_activeRows;			// cell rows to be updated
	int m_activeColumns;		// cell columns to be updated
	int m_cellWidth;			// cell width in pixels
	int m_cellHeight;			// cell height in pixels
	int m_baseX;				// monitors base X pos based on screen
	int m_baseY;				// monitors base Y pos based on screen

	int nClientWidth;
	int nClientHeight;

	CBrush	brush;				// brush for render background
	CPen verticalPen;			// pen for grid vertical lines
	CPen horizontalPen;			// pen for grid horizontal lines

	float MonitorData[NUM_SIGNALS];
	CString MonLabelArr[NUM_SIGNALS];
	vector <CString> MonSignals;

	void PrepareMonitorCanvas( );
	void DrawMonitorData( );
	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg LRESULT OnBDData(WPARAM x, LPARAM y);
	afx_msg LRESULT OnHeaderString(WPARAM x, LPARAM y);

	DECLARE_MESSAGE_MAP()
};
