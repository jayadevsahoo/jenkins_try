/*****************************************************************************/
/*   Module Name:    DETFUN.c                                            
/*   Purpose:       Functions called by com detection routines to display connection 
/*                  information and to handle the cancel system.                                                         
/*                                                                           
/*   Requirements:   This module implements requirements detailed in         
/*          <75359-93>                                                       
/*                                                                           
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Added handler for messages back to     Dave Richardson   19 Nov, 2002
/*   panel containing list of images for
/*   software download
/*    
/*   $Revision::   1.2      $                                                
/*
/*   $LOG$
/*
/*****************************************************************************/

/*****************************************************************************
 *   THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS               
 *   EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,           
 *   STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY      
 *   MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE       
 *   PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.                             
 *   Copyright (c) 1995-2002, Puritan-Bennett                                                  
 *   All rights reserved.                                                                                                                                                * 
 *****************************************************************************************/

/***********************************************/
/****************** INCLUDES *******************/
/***********************************************/

#include <ansi_c.h>
#include <toolbox.h>
#include <userint.h>
#include <utility.h>

#include "840.h"
#include "com.h"
#include "cpvtmain.h"
#include "detfun.h"
#include "ldb.h"
#include "mainpnls.h"
#include "pts.h"
#include "rt_bmps.h"
#include "softdload.h"
#include "sysmenu.h"

/***********************************************/
/************** GLOBAL VARIABLES ***************/
/***********************************************/

static int StopPTS  = 0;
static int Stop840  = 0;
static int StopBoth = 0;

static int LastPort = -1;
static int LastConnected = -1;

/***********************************************/
/************ FUNCTION DECLARATION  ************/
/***********************************************/

/*****************************************************************************/
/* Name:     DetFun_840	                                                     
/*                                                                           
/* Purpose:  Displays status messages about the state of the autodetection
/*		sequence of the 840. 
/*                                                                           
/* Example Call:   int DetFun_840(int port, int connected, int panel, int control);                                    
/*                                                                           
/* Input:   port - port which is being searched
/*			connected - state of connection on port
/*			panel - panel to display message on
/*			control - control to display message on
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:  LastPort, LastConnected                                                        
/*                                                                           
/* Globals Used:  StopBoth, Stop840,LastPort, LastConnected                                                           
/*                                                  
/*                                                                           
/* Return:   1 if message is being displayed, 0 otherwise                                                        
/*                                                                           
/*****************************************************************************/
int DetFun_840(COM_STRUCT *com, int connected, int panel, int control)
{
	double displayDelay = 0.0;
	char buffer[100]; // must be large enough for longest message + string terminator

	ProcessSystemEvents();

	if(StopBoth){
		return 1;
	}

	if(Stop840){
		return 1;
	}

	if(LastPort == com->port 
	&& LastConnected == connected 
	&& connected != ESTABLISH_COM_IN_PROGRESS 
	&& connected != ESTABLISH_COM_FAILURE 
	&& connected != ESTABLISH_COM_NO_PORT){
		return 0;
	}

	switch(connected){
		case ESTABLISH_COM_STARTING: // 0
			sprintf(buffer, "%s", LDBf(Ldb_S_Starting));
			break;
		case ESTABLISH_COM_IN_PROGRESS: // 1
			sprintf(buffer, "%s %d (%d,%s,%d,%d)", LDBf(Ldb_S_840_Establishing), com->port, 
			        com->baud, szComParity[com->parity], com->databits, com->stopbits);
			displayDelay = 0.5;
			break;
		case ESTABLISH_COM_SUCCESS: // 2
			sprintf(buffer, "%s %d", LDBf(Ldb_S_840_Success), com->port);
			displayDelay = 2.0;
			break;
		case ESTABLISH_COM_FAILURE: // 3
			sprintf(buffer, "%s", LDBf(Ldb_S_840_Failure));
			displayDelay = 2.0;
			break;
		case ESTABLISH_COM_NO_PORT: // 4
			sprintf(buffer, "%s", "");
			break;
	} // end switch(connected)

	if(panel == gPanelHandle[SYSPANEL] && control == SYSPANEL_STATUS){
		SetSysPanelMessage(buffer);
	}
	else{
		SetCtrlVal(panel, control, buffer);
	}
	LastPort = com->port;
    LastConnected = connected;
	ProcessDrawEvents();
	Delay(displayDelay);

	return 0;
}


/*****************************************************************************/
/* Name:     DetFun_PTS	                                                     
/*                                                                           
/* Purpose:  Displays status messages about the state of the autodetection
/*		sequence of the PTS. 
/*                                                                           
/* Example Call:  int DetFun_PTS(int port, int connected, int panel, int control);                                    
/*                                                                           
/* Input:   port - port which is being searched
/*			connected - state of connection on port
/*			panel - panel to display message on
/*			control - control to display message on
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:	LastConnected, LastPort                                                          
/*                                                                           
/* Globals Used:  StopBoth, StopPTS, LastConnected, LastPort                                                           
/*                                                  
/*                                                                           
/* Return:   0-Message is being displayed, 1 message not displayed                                                     
/*                                                                           
/*****************************************************************************/
int DetFun_PTS(COM_STRUCT *com, int connected, int panel, int control)
{
	double displayDelay = 0.0;
	char buffer[100]; // must be large enough for longest message + string terminator
	int nBreakOnLibErrsFlag; // flag to break/notbreak on library errors

	ProcessSystemEvents();

	if(StopBoth){
		return 1;
	}

	if(StopPTS){
		return 1;
	}

	if(LastPort == com->port && LastConnected == connected 
	&& connected != ESTABLISH_COM_FAILURE && connected != ESTABLISH_COM_NO_PORT){
		return 0;
	}

	switch(connected){
		case ESTABLISH_COM_STARTING:
			sprintf(buffer, "%s", LDBf(Ldb_S_Starting));
			displayDelay = 1.0;
			break;
		case ESTABLISH_COM_IN_PROGRESS:
			sprintf(buffer, "%s %d (%d,%s,%d,%d)", LDBf(Ldb_S_PTS_Establishing), com->port, 
			        com->baud, szComParity[com->parity], com->databits, com->stopbits);
			displayDelay = 0.5;
			break;
		case ESTABLISH_COM_SUCCESS:
			sprintf(buffer, "%s %d", LDBf(Ldb_S_PTS_Success), com->port);
			displayDelay = 2.0;
			break;
		case ESTABLISH_COM_FAILURE:
			sprintf(buffer, "%s", LDBf(Ldb_S_PTS_Failure));
			displayDelay = 2.0;
			break;
		case ESTABLISH_COM_NO_PORT:
			sprintf(buffer, "%s", "");
			break;
	} // end switch(connected)

	nBreakOnLibErrsFlag = SetBOLE(FALSE); // do not break if setting a non-existent control
	SetCtrlVal(panel, control, buffer);
	SetBOLE(nBreakOnLibErrsFlag); // restore previous nBreakOnLibErrsFlag

	LastPort = com->port;
	LastConnected = connected;
	ProcessDrawEvents();
	Delay(displayDelay);

	return 0;
}


/*****************************************************************************/
/* Name:     DetFun_840_Splash	                                                     
/*                                                                           
/* Purpose:  Intermediate status display function
/*                                          
/*                                                                           
/* Example Call:   int DetFun_840_Splash(int port, int connected);                                    
/*                                                                           
/* Input:    port - port which we're trying to talk to 
/*			 connected - which stage of the connection we're at
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   value of DetFun_840 call                                                         
/*                                                                           
/*****************************************************************************/
int DetFun_840_Splash(COM_STRUCT *com, int connected)
{
	return DetFun_840(com, connected, gPanelHandle[SPLASH], SPLASH_COMSTATUS);
}


/*****************************************************************************/
/* Name:     DetFun_PTS_Splash                                                   
/*                                                                           
/* Purpose:  Intermediate status display function
/*                                                                           
/* Example Call:   int DetFun_PTS_Splash(int port, int connected);                                    
/*                                                                           
/* Input:    port - port we're trying to talk to 
/*			 connected - stage of connect we're at
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   value of DetFun_PTS call                                                         
/*                                                                           
/*****************************************************************************/
int DetFun_PTS_Splash(COM_STRUCT *com, int connected)
{
	return DetFun_PTS(com, connected, gPanelHandle[SPLASH], SPLASH_COMSTATUS);
}


/*****************************************************************************/
/* Name:     DetFun_840_Logon	                                                   
/*                                                                           
/* Purpose:  Intermediate status display function
/*                                                                           
/* Example Call:   int DetFun_840_Logon(int port, int connected);                                    
/*                                                                           
/* Input:    port - port we're trying to talk to 
/*			 connected - stage of connect we're at
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   value of DetFun_840 call                                                         
/*                                                                           
/*****************************************************************************/
int DetFun_840_Logon(COM_STRUCT *com, int connected)
{
	return DetFun_840(com, connected, gPanelHandle[LOGONPANEL], LOGONPANEL_840_LBL_4);
}


/*****************************************************************************/
/* Name:     DetFun_PTS_Logon	                                                   
/*                                                                           
/* Purpose:  Intermediate status display function 
/*                                                                           
/* Example Call:   int DetFun_PTS_Logon(int port, int connected);                                    
/*                                                                           
/* Input:    port - port we're trying to talk to 
/*			 connected - stage of connect we're at
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   value of DetFun_PTS call                                                         
/*                                                                           
/*****************************************************************************/
int DetFun_PTS_Logon(COM_STRUCT *com, int connected)
{
	return DetFun_PTS(com, connected, gPanelHandle[LOGONPANEL], LOGONPANEL_PTS2000_LBL_3);
}


/*****************************************************************************/
/* Name:     DetFun_840_Sys	                                                 
/*                                                                           
/* Purpose:  Intermediate status display function
/*                                          
/*                                                                           
/* Example Call:   int DetFun_840_Sys(int port, int connected);              
/*                                                                           
/* Input:    port - port which we're trying to talk to 
/*			 connected - which stage of the connection we're at
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   value of DetFun_840 call                                                         
/*                                                                           
/*****************************************************************************/
int DetFun_840_Sys(COM_STRUCT *com, int connected)
{
	return DetFun_840(com, connected, gPanelHandle[SYSPANEL], SYSPANEL_STATUS);
}


/*****************************************************************************/
/* Name:     DetFun_PTS_Sys                                                  
/*                                                                           
/* Purpose:  Intermediate status display function
/*                                                                           
/* Example Call:   int DetFun_PTS_Sys(int port, int connected);              
/*                                                                           
/* Input:    port - port we're trying to talk to 
/*			 connected - stage of connect we're at
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   value of DetFun_PTS call                                                         
/*                                                                           
/*****************************************************************************/
int DetFun_PTS_Sys(COM_STRUCT *com, int connected)
{
	return DetFun_PTS(com, connected, gPanelHandle[SYSPANEL], SYSPANEL_STATUS);
}


/*****************************************************************************/
/* Name:     DetFun_840_Setup                                                    
/*                                                                           
/* Purpose:  Intermediate status display function
/*                                                                           
/* Example Call:   int DetFun_840_Setup(int port, int connected);                                    
/*                                                                           
/* Input:    port - port we're trying to talk to 
/*			 connected - stage of connect we're at
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   value of DetFun_840 call                                                         
/*                                                                           
/*****************************************************************************/
static int DetFun_840_Setup(COM_STRUCT *com, int connected)
{
	if(connected == ESTABLISH_COM_SUCCESS || connected == ESTABLISH_COM_FAILURE){
		setButtonBMP(gOPanelHandle[AUTODETECT], AUTODETECT_ACCEPT, BMP_Okay192);
	}
	else{
		setButtonBMP(gOPanelHandle[AUTODETECT], AUTODETECT_ACCEPT, BMP_Cancel192);
	}

	return DetFun_840(com, connected, gOPanelHandle[AUTODETECT], AUTODETECT_AUTODET_STATUS);
}


/*****************************************************************************/
/* Name:     DetFun_PTS_Setup                                                 
/*                                                                           
/* Purpose:  Intermediate status display function 
/*                                                                           
/* Example Call:  int DetFun_PTS_Setup(int port, int connected);                                    
/*                                                                           
/* Input:    port - port we're trying to talk to 
/*			 connected - stage of connect we're at
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   value of DetFun_PTS call                                                         
/*                                                                           
/*****************************************************************************/
static int DetFun_PTS_Setup(COM_STRUCT *com, int connected)
{
	if(connected == ESTABLISH_COM_SUCCESS || connected == ESTABLISH_COM_FAILURE){
		setButtonBMP(gOPanelHandle[AUTODETECT], AUTODETECT_ACCEPT, BMP_Okay192);
	}
	else{
		setButtonBMP(gOPanelHandle[AUTODETECT], AUTODETECT_ACCEPT, BMP_Cancel192);
	}

	return DetFun_PTS(com, connected, gOPanelHandle[AUTODETECT], AUTODETECT_AUTODET_STATUS);
}


/*****************************************************************************/
/* Name:     DetFun_840_Logon	                                                   
/*                                                                           
/* Purpose:  Intermediate status display function
/*                                                                           
/* Example Call:   int DetFun_840_Logon(int port, int connected);                                    
/*                                                                           
/* Input:    port - port we're trying to talk to 
/*			 connected - stage of connect we're at
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:                                                          
/*           <None>                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   value of DetFun_840 call                                                         
/*                                                                           
/*****************************************************************************/
extern int IMAG_PANEL;
int DetFun_840_ImageSetup(COM_STRUCT *com, int connected)
{
	return DetFun_840(com, connected, gPanelHandle[IMAG_PANEL], IMAG_PANEL_840_AUTODET_STATUS);
}


/*****************************************************************************/
/* Name:    Register_Splash_CB                                                     
/*                                                                           
/* Purpose:  registers the appropriate function to display status for autodetection
/*                                                                           
/* Example Call:   void Register_Splash_CB(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed: Stop840, StopPTS, StopBoth,LastPort,LastConnected                                                        
/*                                                                           
/* Globals Used:   See above                                                          
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void Register_Splash_CB(void)
{
	ResetDetect();
	LastPort = -1;
	LastConnected = -1;

	RegisterFor840DetectCB((ComDetectStatusFuncPtr)DetFun_840_Splash);
	RegisterForPTSDetectCB((ComDetectStatusFuncPtr)DetFun_PTS_Splash);

	return;
}


/*****************************************************************************/
/* Name:     Register_Logon_CB	                                                    
/*                                                                           
/* Purpose:  registers the appropriate function to display status for autodetection
/*                                                                           
/* Example Call:   void Register_Logon_CB(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed: Stop840, StopPTS, StopBoth,LastPort,LastConnected                                                        
/*                                                                           
/* Globals Used:   See above                                                          
/*                                                  
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void Register_Logon_CB(void)
{
	ResetDetect();
	LastPort = -1;
	LastConnected = -1;

	RegisterFor840DetectCB((ComDetectStatusFuncPtr)DetFun_840_Logon);
	RegisterForPTSDetectCB((ComDetectStatusFuncPtr)DetFun_PTS_Logon);

	return;
}


/*****************************************************************************/
/* Name:     Register_Sys_CB	                                             
/*                                                                           
/* Purpose:  registers the appropriate function                              
/*           to display status for autodetection                             
/*                                                                           
/* Example Call:   void Register_Sys_CB(void);                               
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed: Stop840, StopPTS, StopBoth,LastPort,LastConnected                                                        
/*                                                                           
/* Globals Used:   See above                                                          
/*                                                  
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void Register_Sys_CB(void)
{
	ResetDetect();
	LastPort = -1;
	LastConnected = -1;

	RegisterFor840DetectCB((ComDetectStatusFuncPtr)DetFun_840_Sys);
	RegisterForPTSDetectCB((ComDetectStatusFuncPtr)DetFun_PTS_Sys);

	return;
}


/*****************************************************************************/
/* Name:    Register_Setup_CB	                                                      
/*                                                                           
/* Purpose:  registers the appropriate function to display status for autodetection
/*                                                                           
/* Example Call:   void Register_Setup_CB(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed: Stop840, StopPTS, StopBoth,LastPort,LastConnected                                                        
/*                                                                           
/* Globals Used:   See above                                                          
/*                                                  
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void Register_Setup_CB(void)
{
	ResetDetect();
	LastPort = -1;
	LastConnected = -1;

	RegisterFor840DetectCB((ComDetectStatusFuncPtr)DetFun_840_Setup);
	RegisterForPTSDetectCB((ComDetectStatusFuncPtr)DetFun_PTS_Setup);

	return;
}


/*****************************************************************************/
/* Name:    Register_ImageSetup_CB	                                                      
/*                                                                           
/* Purpose:  registers the appropriate function to display status for autodetection
/*                                                                           
/* Example Call:   void Register_ImageSetup_CB(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed: Stop840, StopPTS, StopBoth,LastPort,LastConnected                                                        
/*                                                                           
/* Globals Used:   See above                                                          
/*                                                  
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void Register_ImageSetup_CB(void)
{
	ResetDetect();
	LastPort = -1;
	LastConnected = -1;

	RegisterFor840DetectCB((ComDetectStatusFuncPtr)DetFun_840_ImageSetup);

	return;
}


/*****************************************************************************/
/* Name:     StopPTSDetect                                                    
/*                                                                           
/* Purpose:  Sets the flag to stop the detection of the PTS
/*                                          
/*                                                                           
/* Example Call:   void StopPTSDetect(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:Stop840                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                  
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void StopPTSDetect(void)
{
	StopPTS = 1;

	return;
}


/*****************************************************************************/
/* Name:     Stop840Detect                                                   
/*                                                                           
/* Purpose:  Sets the flag to stop the detection of the 840
/*                                                                           
/* Example Call:   void Stop840Detect(void);                                    
/*                                                                           
/* Input:    <None>          
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:   StopPTS                                                       
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void Stop840Detect(void)
{
	Stop840 = 1;

	return;
}


/*****************************************************************************/
/* Name:     StopDetect                                                   
/*                                                                           
/* Purpose:  Sets flag to stop detection of both devices
/*                                          
/*                                                                           
/* Example Call:   void StopDetect(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:StopBoth                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void StopDetect(void)
{
	StopBoth = 1;

	return;
}


/*****************************************************************************/
/* Name:     ResetDetect                                                   
/*                                                                           
/* Purpose:  Sets flag to for detection of both devices
/*                                          
/*                                                                           
/* Example Call:   void ResetDetect(void);                                    
/*                                                                           
/* Input:    <None>         
/*                                                                           
/* Output:   <None>                                                          
/*                                                                           
/* Globals Changed:StopBoth, Stop840, StopPTS                                                          
/*                                                                           
/* Globals Used:                                                             
/*           <None>                                                          
/*                                                                           
/* Return:   <None>                                                         
/*                                                                           
/*****************************************************************************/
void ResetDetect(void)
{
	Stop840  = 0;
	StopPTS  = 0;
	StopBoth = 0;

	return;
}


