#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@  Class: GasSupply - Monitoring the gas supply (wall air and O2)
//---------------------------------------------------------------------
//@ Interface-Description
//  This class implements the gas supply functionality.  It provides
//  its client with the ability to check whether or not wall air, and O2
//  are present. A client that needs to be notified about a gas supply
//  change can register with the gas supply object. Only one client can
//  be registered at any given time. Each time a new client is registering,
//  the previous client is dropped out. The GasSupply class is responsible
//  for detecting changes in air and O2 supply, and notify the Alarm
//  sub-system about these changes.
//---------------------------------------------------------------------
//@ Rationale
//  The class implements the functionality of the gas supply requirements.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Three private data members are implemented to keep track on the gas supply
//  status: The wall air status, the O2 status, and the total air status
//  (considering the compressed air as well.) One private data member is
//  implemented to store a pointer to the client's call back function for
//  gas supply changes.  Another private data member is implemented to
//  store a pointer to the alarm call back function. Two BinaryIndicator
//  objects are declared as private data members to provide bit
//  level I/O access to the air and O2 pressure switch register.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None.
//---------------------------------------------------------------------
//@ Invariants
//  None.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/GasSupply.ccv   25.0.4.0   19 Nov 2013 13:54:06   pvcs  $
//
//@ Modification-Log
//
// Revision: 005  By:  erm   Date:  10-Aug-2009    DR Number: 6424
//  Project:  840
//  Description:
//		Added debouncer to O2 and air gas switch
//
//  Revision: 004  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//  Project:  840
//  Description:
//		Eliminate callbacks since EventFilter class called explicitly.
//
//  Revision: 003 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 002  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 001  By:  by    Date:  11-Nov-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "GasSupply.hh"

// TODO E600_LL: To be checked and implemented for E600 hardware
//#include "Compressor.hh"

#include "VentObjectRefs.hh"
#include "RegisterRefs.hh"
#include "EventFilter.hh"
#include "BDIORefs.hh"
#include "MainSensorRefs.hh"
#include "GasInletPressureSensor.hh"

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GasSupply()
//
//@ Interface-Description
//
//  The constructor initializes the two BinaryIndicator objects
//  airPressureSwitch_ and o2PressureSwitch_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
//
GasSupply::GasSupply(void)
{
    // $[TI1]
    CALL_TRACE("GasSupply::GasSupply(void)");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GasSupply()  [Destructor]
//
//@ Interface-Description
//  Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Not implemented.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

GasSupply::~GasSupply(void)
{
    CALL_TRACE("GasSupply::~GasSupply(void)");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle
//
//@ Interface-Description
//  The method takes no parameters and returns no value.
//  This method can be executed only after the newCycle() method for the
//  Compressor is run.  A check for a change in one of the following
//  parameters is performed:
//
//  wall air supply, O2 supply, total air supply, and background
//  PSOL or flow sensor failures.
//
//  If a change is detected, the Alarm-Analysis sub-system is notified.
//  When a change in the O2, total air supply, or background failures
//  is detected, the client of this object is notified via the call back
//  mechanism.
//
//  $[05151] $[05157]
//---------------------------------------------------------------------
//@ Implementation-Description
//  The gas supply status is checked and compared to the stored status.
//  Any change is reported to the Alarm-Analysis sub-system. The gas
//  supply internal status is stored by the three private data members:
//  isAirPresent_, isO2Present_, and isTotalAirPresent_.
//
//  Private data members airBkgndFail_ and o2BkgndFail_ are flags used
//  by Safety Net background checks to activate when a failure in
//  PSOL or flow sensor has been detected.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
GasSupply::newCycle( void )
{
    CALL_TRACE("GasSupply::newCycle(void)");
	
    Boolean isGasSupplyChanged = FALSE;

    Boolean wallAirPresent = checkAirPresence();
    Boolean o2Present = checkO2Presence();

    // Note that Compressor.newCycle() should preceed this method.
	
	// TODO E600 MS Take care of the compressor later
    Boolean airPresent = wallAirPresent ;//|| RCompressor.isCompressedAirUsable();

    // check for a status change in wall air availability:
    if( isAirPresent_ != wallAirPresent )
    {

        if( wallAirPresent )
        {
            // notify Alarms about re-connection of wall air
            REventFilter.postAlarm( EventFilter::WALL_AIR, BdAlarmId::BDALARM_WALL_AIR_PRESENT, FALSE) ;
        }
        else // wall air is not present
        {
            // notify Alarms about disconnection of wall air
            REventFilter.postAlarm( EventFilter::WALL_AIR, BdAlarmId::BDALARM_NOT_WALL_AIR_PRESENT, FALSE) ;
        }

        isAirPresent_ = wallAirPresent;
    }


    // check for a status change in O2 availability:
    if( isO2Present_ != o2Present )
    {

        isGasSupplyChanged = TRUE;

        if( o2Present )
        {
            // notify Alarms about re-connection of O2
            REventFilter.postAlarm( EventFilter::WALL_O2, BdAlarmId::BDALARM_WALL_O2_PRESENT, FALSE) ;
        }
        else // o2 isn't present
        {
            // notify Alarms about disconnection of oxygen
            REventFilter.postAlarm( EventFilter::WALL_O2, BdAlarmId::BDALARM_NOT_WALL_O2_PRESENT, FALSE) ;
        }

        isO2Present_ = o2Present;
    }
    // $[TI4]

    // check for a status change in total air availability:
    if( isTotalAirPresent_ != airPresent )
    {
    // $[TI5]
        isGasSupplyChanged = TRUE;
        isTotalAirPresent_ = airPresent;
    }
    // $[TI6]

    if ( ( TRUE == airBkgndFail_ ) && ( FALSE == airBkgndFailureReported_ ) )
    {
    // $[TI7]
        isGasSupplyChanged = TRUE;
        airBkgndFailureReported_ = TRUE;
    }
    // $[TI8]

    if ( ( TRUE == o2BkgndFail_ ) && ( FALSE == o2BkgndFailureReported_ ) )
    {
    // $[TI9]
        isGasSupplyChanged = TRUE;
        o2BkgndFailureReported_ = TRUE;
    }
    // $[TI10]

    if( isGasSupplyChanged )
    {
    // $[TI11]
        // notify the O2 mixture object (the client) about the change:
		if ( pSupplyChangeCallBack_ )
			pSupplyChangeCallBack_(*this);
    }
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetAirInletPressure
//
//@ Interface-Description
//  This method takes no arguments and returns the inlet air pressure
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the engineering value of the air inlet pressure sensor
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
Real32 GasSupply::GetAirInletPressure( void )
{
	return RAirInletPressureSensor.getValue();
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetO2InletPressure
//
//@ Interface-Description
//  This method takes no arguments and returns the inlet O2 pressure
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns the engineering value of the O2 inlet pressure sensor
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
Real32 GasSupply::GetO2InletPressure( void )
{
	return RO2InletPressureSensor.getValue();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize
//
//@ Interface-Description
//  This method takes no arguments and has no return value. It sets up
//  the GasSupply object to an initial known state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The data members are initialized.  Current pnuematic status
//  are checked and stored in their respective variables.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
GasSupply::initialize( void )
{
	
    CALL_TRACE("GasSupply::initialize( void )");

    // $[TI1]
    airBkgndFail_ = FALSE;
    o2BkgndFail_ = FALSE;

    o2BkgndFailureReported_ = FALSE;
    airBkgndFailureReported_ = FALSE;

	isAirPresent_ = GetAirInletPressure() > MIN_PSI_REQUIRED;
	isO2Present_  = GetO2InletPressure() > MIN_PSI_REQUIRED;

	// TODO E600 MS put it back when compressor is ported
    isTotalAirPresent_ = isAirPresent_ ;//|| RCompressor.isCompressedAirPresent();

    // Force the initialization of the O2Mixture class
	if ( pSupplyChangeCallBack_ )
		pSupplyChangeCallBack_(*this);
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialStatusNotification
//
//@ Interface-Description
//  This method takes no arguments and has no return value. It is invoked
//  after system initialization and whenever communications is restored
//  to notify Alarms and the GUI of initial compressor status.
//
//  This cannot be done as part of the initialize method because
//  communications must be up.
//
//  $[05151] $[05157]
//---------------------------------------------------------------------
//@ Implementation-Description
//  The data members isO2Present_ and isAirPresent are used to
//  determine if O2 and air are available.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
GasSupply::initialStatusNotification( void )
{
	
    CALL_TRACE("GasSupply::initialStatusNotification(void)");

    if ( isAirPresent_ )
    {
    // $[TI1]
        // notify Alarms about availability of wall air
        REventFilter.postAlarm( EventFilter::WALL_AIR, BdAlarmId::BDALARM_WALL_AIR_PRESENT, TRUE) ;
    }
    else
    {
    // $[TI2]
        // notify Alarms about non-availability of wall air
        REventFilter.postAlarm( EventFilter::WALL_AIR, BdAlarmId::BDALARM_NOT_WALL_AIR_PRESENT, TRUE) ;
    }

    if ( isO2Present_ )
    {
    // $[TI3]
        // notify Alarms about availability of wall O2
        REventFilter.postAlarm( EventFilter::WALL_O2, BdAlarmId::BDALARM_WALL_O2_PRESENT, TRUE) ;
    }
    else
    {
    // $[TI4]
        // notify Alarms about non-availability of wall O2
        REventFilter.postAlarm( EventFilter::WALL_O2, BdAlarmId::BDALARM_NOT_WALL_O2_PRESENT, TRUE) ;
    }
	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CheckGas
//
//@ Interface-Description
//  This method takes the current status of the gas, the number of 
//  required transition cycles, reference to the number of cyles that the 
//  gas source has been in recovery, reference to the num of cycles that
//  the gas has not been present and the current gas inlet pressure and
//  determines whether the gas is present or not
//
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================
Boolean 
GasSupply::CheckGas( Boolean currStatus, Uint numCyclestoTransition, 
	Uint& numCyclesRecovered, Uint& numCyclesNotPresent, 
	Real32 inletPressure )
{
	Boolean retVal = FALSE;

    // If we declared a gas not present then it has to recover before it is present again
    if( currStatus == FALSE )
    {
		Boolean recovered = inletPressure > MIN_RECOVERY_PSI_REQUIRED ;

    	if( recovered == FALSE )
    		numCyclesRecovered = 0;

    	else if( ++numCyclesRecovered < numCyclestoTransition )
    		recovered = FALSE;

    	retVal = recovered;
    }
    else
    {
		Boolean present = inletPressure > MIN_PSI_REQUIRED;

		if( present == TRUE )
			numCyclesNotPresent = 0;

		else if( ++numCyclesNotPresent < numCyclestoTransition )
			present = TRUE;

		retVal = present;
    }

	return retVal;
}

#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//  [static]
//
//@ Interface-Description
//
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  None.
//---------------------------------------------------------------------
//@ PostCondition
//  None.
//@ End-Method
//=====================================================================

void
GasSupply::SoftFault( const SoftFaultID softFaultID,
                      const Uint32      lineNumber,
                      const char*       pFileName,
                      const char*       pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, GASSUPPLY,
                           lineNumber, pFileName, pPredicate );
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

