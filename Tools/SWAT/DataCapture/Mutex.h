//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Mutex.h
//----------------------------------------------------------------------------
#ifndef MUTEX_H
#define MUTEX_H

//-------------------------------------
// I N C L U D E S
//-------------------------------------
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

//-------------------------------------
// C L A S S  D E C L
//-------------------------------------

/// A class to manage a Mutex.
class Mutex
{
    public:

    /// @brief Mutex
    /// Constructor
    Mutex();

    /// @brief ~Mutex
    /// Destructor
    ~Mutex();

    /// @brief lock
    /// 
    /// @return True if able to lock mutex, otherwise false. 
    bool lock();

    /// @brief unlock
    /// 
    /// @return True of able to unlock mutex, otherwise false.
    bool unlock();


    private:
    bool m_bActive;
    HANDLE m_Mutex;
    
};

#endif
