#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//        Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Foundation - Project-Wide Information of the Foundation Subsystem.
//---------------------------------------------------------------------
//@ Interface-Description
//  This is an interface class for the Foundation Subsystem.  This
//  class contains the initialization routine that initializes any
//  of this subsystem's units that need explicit initialization.
//---------------------------------------------------------------------
//@ Rationale
//  This class provides a standard way for the startup routines to
//  initialize the Foundation Subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
//  A static counter is used to ensure that this subsystem's initialize
//  routine is only called once.
//---------------------------------------------------------------------
//@ Fault-Handling
//      The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/Foundation.ccv   25.0.4.0   19 Nov 2013 14:03:48   pvcs  $
//
//@ Modification-Log
//
//   Revision: 002   By: sah   Date: 26-Feb-1997   DR Number: 491 & 1799
//   Project:  Sigma (R8027)
//   Description:
//	Removed #ifdef's around 'arrDebugFlags_[]'.
//
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//=====================================================================

#include "Foundation.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Static Member Definitions...
//
//=====================================================================

Uint   Foundation::arrDebugFlags_[] = {0};


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize()  [static]
//
//@ Interface-Description
//  This is to be used to initialize all of the data of the Foundation
//  subsystem.  This is only to be called once.
//---------------------------------------------------------------------
//@ Implementation-Description
//  There is nothing to initialize for this subsystem.
//---------------------------------------------------------------------
//@ PreConditions
//  none
//---------------------------------------------------------------------
//@ PostConditions
//  none
//@ End-Method
//=====================================================================

void
Foundation::Initialize(void)
{
  // do nothing...
}


#if defined(SIGMA_DEBUG)

//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOn(foundationId)  [static]
//
// Interface-Description
//  Turn all of the the debug flags for this subsystem on.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
Foundation::TurnAllDebugOn(void)
{
  CALL_TRACE("TurnAllDebugOn()");

  for (Uint32 idx = 0; idx < Foundation::NUM_LWORDS_; idx++) {
    Foundation::arrDebugFlags_[idx] = ~(0u);
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method:  TurnAllDebugOff(foundationId)  [static]
//
// Interface-Description
//  Turn all of the the debug flags for this subsystem off.
//---------------------------------------------------------------------
// Implementation-Description
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

void
Foundation::TurnAllDebugOff(void)
{
  CALL_TRACE("TurnAllDebugOff()");

  for (Uint32 idx = 0; idx < Foundation::NUM_LWORDS_; idx++) {
    Foundation::arrDebugFlags_[idx] = 0u;
  }
}

#endif  // defined(SIGMA_DEBUG)
