#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: FlowSensorCalibration - performs the flow sensor calibration.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class performs the Service Mode flow calibration which obtains the
//	flow offsets seen by the exhalation flow sensor when air and o2 are
//	delivered.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the methods required to perform the flow
//	sensor calibration test.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The method runTest() performs the flow sensor calibration.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/FlowSensorCalibration.ccv   25.0.4.0   19 Nov 2013 14:23:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 006  By: quf       Date: 20-Apr-2000     DR Number: 5709
//  Project:  NeoMode
//  Description:
//	Fix for high gas supply pressure issue: since SmUtilities::getMaximumFlow()
//	no longer closes the psols on completion, close the psols at the end of
//	calibrate_().
//
//
//  Revision: 005  By: dosman    Date: 01-Mar-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	Put some code within #ifndef SIGMA_UNIT_TEST #endif because
//	it was causing compiler warnings during unit test.
//
//  Revision: 004  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 003  By:  quf    Date:  16-Dec-97    DR Number: 2684
//       Project:  Sigma (840)
//       Description:
//			Mapped SRS requirement 09192.
//
//  Revision: 002  By:  quf    Date:  23-Sep-97    DR Number: 2385
//       Project:  Sigma (840)
//       Description:
//             Added code to set the novram calibration status flag
//             to the proper state at the proper times.
//
//  Revision: 001  By:  syw    Date:  03-Jul-97    DR Number: none
//       Project:  Sigma (840)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================
#include "FlowSensorCalibration.hh"

#include "MainSensorRefs.hh"
#include "SmRefs.hh"
#include "CalInfoRefs.hh"
#include "GasInletPressureSensor.hh"

//@ Usage-Classes

#include "ExhFlowSensor.h"
#include "SmFlowController.hh"
#include "Task.hh"
#include "FlowSensorOffset.hh"
#include "SmManager.hh"
#include "NovRamManager.hh"
#include "SmUtilities.hh"
#include "ServiceModeNovramData.hh"
#include "ValveRefs.hh"

//@ End-Usage

#ifdef SPIRO_DEBUG
#include "MiscSensorRefs.hh"
#include <stdio.h>
#include "PrintQueue.hh"
static char string[80] ;
#include "TemperatureSensor.hh"
#include "MathUtilities.hh"
static Uint32 delayTime = 500 ;
#endif // SPIRO_DEBUG

#ifdef SIGMA_UNIT_TEST
#include "FakeIo.hh"
#include "FakeControllers.hh"
#include "FlowSensorCalibration_UT.hh"
#include "FakeTask.hh"
#endif  // SIGMA_UNIT_TEST

#ifdef SIGMA_DEVELOPMENT
#include <stdio.h>
#endif  // SIGMA_DEVELOPMENT


//@ Code...

// error flag bit definitions
static const Uint32 NO_ERRORx = 0 ;
static const Uint32 FLOW_ERROR = 1 ;
static const Uint32 TOLERANCE_ERROR = 2 ;
static const Uint32 INLET_PRESSURE_ERROR = 4;

// minimum value for the max FS cal test flow
const Int32 FS_CAL_MINIMUM_MAX_FLOW = 60;  // lpm

// these constants are used in the calibration algorithm ported from 980
static const Uint32 CAL_STARTING_FLOW = 22;				// the flow value in LPM used to start the calibration
static const Real32 INLET_PRESSURE_SETPOINT = 15.0;		// the set pressure of air and o2 inlet regulator
static const Real32 MAX_ACCUM_PRESS_TOLERANCE = 6.0;	// the tolerance for inlet pressure
static const Uint32 O2_FLUSH_DELAY_SECS = 178;			// time in seconds taken to flush circuit with o2
static const Uint32 AIR_FLUSH_DELAY_SECS = 58;			// time in seconds taken to flush circuit with air

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: FlowSensorCalibration()  [Default Constructor]
//
//@ Interface-Description
//		Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the max insp/exh flow offset arrays.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

FlowSensorCalibration::FlowSensorCalibration( void) :
								pFlowSensorOffset_(0)
{
	CALL_TRACE("FlowSensorCalibration::FlowSensorCalibration( void)") ;

	for (Uint32 ii=1; ii < MAX_FLOW_CALIBRATION; ii++)
	{
	// $[TI1]
		// (-4.55/5.0*0.01*q - 15.0/10.0*(0.01*q + 0.02) - (0.0275*q + 0.05))
		maxInspOffset_[ii] =
			ABS_VALUE( -0.0091F*ii - 1.5F*(0.01F*ii+0.02F) - (0.0275F*ii+0.05F) );

		// (-4.05/5.0*0.01*q - 15.0/10.0*(0.01*q + 0.04) - (0.0275*q + 0.1))
		maxExhOffset_[ii] =
			ABS_VALUE( -0.0081F*ii - 1.5F*(0.01F*ii+0.04F) - (0.0275F*ii+0.1F) );
	}
	// end for
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~FlowSensorCalibration()  [Destructor]
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

FlowSensorCalibration::~FlowSensorCalibration( void)
{
	CALL_TRACE("FlowSensorCalibration::~FlowSensorCalibration( void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initialize()
//
//@ Interface-Description
//	This method is used to initialize the flow sensor calibration status.
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If flow sensor info in FLASH is invalid (e.g. changed flow sensor),
//	set the flow sensor cal status in novram to NEVER_RUN to force the
//	operator to run the flow sensor cal.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================
void
FlowSensorCalibration::initialize( void)
{
	CALL_TRACE("FlowSensorCalibration::initialize( void)") ;

	if (!ServiceMode::IsFlowSensorInfoValid())
	{
	// $[TI1]
		ServiceModeNovramData::SetFlowSensorCalStatus(ServiceMode::NEVER_RUN);
	}
	// $[TI2]
	// implied else
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: runTest
//
//@ Interface-Description
//	This method performs the flow sensor calibration.  No arguments,
//	no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	First verify AC power, air, and O2 are connected, then proceed with
//	the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlowSensorCalibration::runTest( void)
{
	CALL_TRACE("FlowSensorCalibration::runTest( void)") ;

	RETAILMSG( TRUE, (L"****Reqested flow sensor calibration\r\n") );
	// set novram status flag to "in progress"
	ServiceModeNovramData::SetFlowSensorCalStatus(
								ServiceMode::IN_PROGRESS );

	// check if AC power is connected
#ifdef SIGMA_UNIT_TEST
	SmUtilities::TestResult acResult = FlowSensorCalibration_UT::AcResult;
#else
	SmUtilities::TestResult acResult = RSmUtilities.checkAcPower();
#endif  // SIGMA_UNIT_TEST

#ifdef SPIRO_DEBUG
//	printf( "Enter in delay time (500 msec nominal): ") ;
//	scanf( "%d", &delayTime) ;
#endif // SPIRO_DEBUG

	if (acResult == SmUtilities::AC_CONNECTED)
	{
	// $[TI1]
		// check if air is connected
#ifdef SIGMA_UNIT_TEST
		SmUtilities::TestResult airResult = FlowSensorCalibration_UT::AirResult;
#else
		SmUtilities::TestResult airResult =	RSmUtilities.verifyAirConnected() ;
#endif  // SIGMA_UNIT_TEST

		if (airResult == SmUtilities::GAS_CONNECTED)
		{
		// $[TI1.1]
			// check if O2 is connected
#ifdef SIGMA_UNIT_TEST
			SmUtilities::TestResult o2Result = FlowSensorCalibration_UT::O2Result;
#else
			SmUtilities::TestResult o2Result =	RSmUtilities.verifyO2Connected() ;
#endif  // SIGMA_UNIT_TEST

			if (o2Result == SmUtilities::GAS_CONNECTED)
			{
			// $[TI1.1.1]
				if (!RSmUtilities.messagePrompt( SmPromptId::REMOVE_INSP_FILTER_PROMPT))
				{
				// $[TI1.1.1.1]
					RSmManager.sendOperatorExitToServiceData() ;
				}
				else
				{
				// $[TI1.1.1.2]
					performTest_() ;
				}
				// end else
			}
			else if (o2Result == SmUtilities::GAS_NOT_CONNECTED)
			{
			// $[TI1.1.2]
				RSmManager.sendStatusToServiceData(
					SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_3) ;

				// set novram status flag to "failed"
				ServiceModeNovramData::SetFlowSensorCalStatus(
								ServiceMode::FAILED );
			}
			else
			{
			// $[TI1.1.3]
				RSmManager.sendOperatorExitToServiceData() ;
			}
			// end else
		}
		else if (airResult == SmUtilities::GAS_NOT_CONNECTED)
		{
		// $[TI1.2]
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_2) ;

			// set novram status flag to "failed"
			ServiceModeNovramData::SetFlowSensorCalStatus(
								ServiceMode::FAILED );
		}
		else
		{
		// $[TI1.3]
			RSmManager.sendOperatorExitToServiceData() ;
		}
		// end else
	}
	else if (acResult == SmUtilities::AC_NOT_CONNECTED)
	{
	// $[TI2]
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_1) ;

		// set novram status flag to "failed"
		ServiceModeNovramData::SetFlowSensorCalStatus(
								ServiceMode::FAILED );
	}
	else
	{
	// $[TI3]
		RSmManager.sendOperatorExitToServiceData() ;
	}
	// end else

	RETAILMSG ( TRUE, (L"Flow sensor cal done\r\n") );
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
FlowSensorCalibration::SoftFault( const SoftFaultID  softFaultID,
                  			   const Uint32 lineNumber,
		   					   const char *pFileName,
		   					   const char *pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;
	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, FLOWSENSORCALIBRATION,
                          	 lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: obtainInspExhOffset_
//
//@ Interface-Description
//		This method has the desired flow and desired gas as arguments and
//		returns the exhalation vs inspiration flow offset when the desired
//		flow and gas type is delivered.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Establish the desired flow rate with the desired gas for FLOW_TIME.
//		After TRANSIENT_TIME, initialize the flow filtered values to the
//		current flow reading.  After FLOW_TIME, compute offset.
//---------------------------------------------------------------------
//@ PreCondition
//      CLASS_PRE_CONDITION( desiredGas == AIR || desiredGas == O2)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Real32
FlowSensorCalibration::obtainInspExhOffset_(
								Uint32 desiredFlow, GasType desiredGas, Real32 &accumPressure)
{
// $[TI1]
	CALL_TRACE("FlowSensorCalibration::obtainInspExhOffset_( \
								Uint32 desiredFlow, GasType desiredGas)") ;

	CLASS_PRE_CONDITION( desiredGas == AIR || desiredGas == O2) ;

	const Uint32 FLOW_TIME = 500 ;
	const Uint32 TRANSIENT_TIME = 100 ;

	RSmFlowController.establishFlow( (Real32)desiredFlow, DELIVERY, desiredGas);
#ifdef SPIRO_DEBUG
	Task::Delay( 0, delayTime*0.2) ;
#else
	Task::Delay( 0, TRANSIENT_TIME) ;
#endif // SPIRO_DEBUG

// TODO E600_LL: This original block of code is kept for now for reference/comparison purpose,
// probably until enough testing has been done for the new method below
#if 0	
	Real32 airFlow = 0.0 ;
	Real32 o2Flow = 0.0 ;
	Real32 exhFlow = 0.0 ;

	airFlow = RAirFlowSensor.getValue() ;
	RAirFlowSensor.initValues( airFlow, 0.0, 0.0, airFlow) ;

	o2Flow = RO2FlowSensor.getValue() ;
	RO2FlowSensor.initValues( o2Flow, 0.0, 0.0, o2Flow) ;

	exhFlow = RExhFlowSensor.getValue() ;
	RExhFlowSensor.initValues( exhFlow, 0.0, 0.0, exhFlow) ;

#ifdef SPIRO_DEBUG
	Task::Delay( 0, delayTime*0.8) ;
#else
	Task::Delay( 0, FLOW_TIME - TRANSIENT_TIME) ;
#endif // SPIRO_DEBUG

	filteredAirFlow_ = RAirFlowSensor.getFilteredValue() ;
	filteredO2Flow_ = RO2FlowSensor.getFilteredValue() ;
	exhFlow = RExhFlowSensor.getUnoffsettedFilteredValue() ;

#else
// This is the way similar to PB980 of filtering air and o2 flow for the purpose
// of flow sensor calibration. More testing is required to determine if it is good/optimal

	// Take an average of the flow
	Real32 airFlowSum = 0.0;
	Real32 o2FlowSum = 0.0;
	Real32 exhFlowSum = 0.0;
	Uint32 numSamples = 0;
	Real32 accumPressureSum = 0.0;
	Uint32 DWELL_TIME = 1000; // 1 sec;

	if(desiredFlow <= CAL_STARTING_FLOW)
	{
		//Wait 2 secs
		Task::Delay( 2, 0) ;
		if (desiredFlow == CAL_STARTING_FLOW)
		{
			//this is the starting flow
			DWELL_TIME = 2000; // 2 secs;
		}
	}
	else
	{
		if (desiredFlow == CAL_STARTING_FLOW + 1)
		{
			//Wait 10 secs due to closure of purge valve
			Task::Delay( 10, 0) ;
		}
		else
		{
			//Wait 200 msecs
			Task::Delay( 0, 200) ;
		}
		DWELL_TIME = 50; // 50 msec
	}

	for (Uint32 filterTime = 0; filterTime < DWELL_TIME; filterTime += SM_CYCLE_TIME)
	{
		Task::Delay (0, SM_CYCLE_TIME);

		airFlowSum += RAirFlowSensor.getValue();
		o2FlowSum += RO2FlowSensor.getValue();
		exhFlowSum += RExhFlowSensor.getUnoffsettedValue();
		if(desiredGas == AIR)
		{
			accumPressureSum += RAirInletPressureSensor.getValue();
		}
		else
		{
			accumPressureSum += RO2InletPressureSensor.getValue();
		}
		numSamples++;
	}

	filteredAirFlow_ = airFlowSum / numSamples;
	filteredO2Flow_ = o2FlowSum / numSamples ;
	Real32 exhFlow = exhFlowSum / numSamples;
	accumPressure = accumPressureSum / numSamples;

#endif

	Real32 offset = exhFlow - (filteredAirFlow_ + filteredO2Flow_) ;

	return( offset) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: performTest_
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		the air and o2 test driver to obtain the flow offsets.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Calibrate the air flow offsets, store the offsets if they are all
//	within spec.  Repeat for the O2 flow offsets.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
FlowSensorCalibration::performTest_( void)
{
	RETAILMSG( TRUE, ( L"Calling performTest_()\r\n") );
	CALL_TRACE("FlowSensorCalibration::performTest_( void)") ;

	const Real32 FS_CAL_ALPHA = 120.0 / 130.0 ;

	RAirFlowSensor.setAlpha( FS_CAL_ALPHA) ;
	RO2FlowSensor.setAlpha( FS_CAL_ALPHA) ;
	RExhFlowSensor.setAlpha( FS_CAL_ALPHA) ;

	//Flush using 22 LPM when starting
	RSmFlowController.establishFlow( CAL_STARTING_FLOW, DELIVERY, O2);

	Task::Delay(O2_FLUSH_DELAY_SECS, 0) ;

	Uint32 error = calibrate_( O2);

	RETAILMSG( TRUE, ( L"Done calibrating O2\r\n") );
	if (error == NO_ERROR)
	{
	// $[TI1]
		// store the air flow sensor offsets in novram
		NovRamManager::UpdateO2FlowSensorOffset( RO2SideFlowSensorOffset) ;

		//Flush using starting flow
		RSmFlowController.establishFlow( CAL_STARTING_FLOW, DELIVERY, AIR);
		
		Task::Delay(AIR_FLUSH_DELAY_SECS, 0) ;

		error = calibrate_( AIR);
	    if (error == NO_ERROR)
	    {
		// $[TI1.1]
			// store the O2 flow sensor offsets in novram
			NovRamManager::UpdateAirFlowSensorOffset( RAirSideFlowSensorOffset) ;

			// set novram status flag to "passed"
			ServiceModeNovramData::SetFlowSensorCalStatus(
									ServiceMode::PASSED );
	    }
		else if (error & FLOW_ERROR)
		{
		// $[TI1.2]
			// can't achieve min O2 flow
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_5) ;

			// set novram status flag to "failed"
			ServiceModeNovramData::SetFlowSensorCalStatus(
									ServiceMode::FAILED );
		}
		else if (error & TOLERANCE_ERROR)
		{
		// $[TI1.3]
			// O2 offset OOR
			RSmManager.sendStatusToServiceData(
				SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_7) ;

			// set novram status flag to "failed"
			ServiceModeNovramData::SetFlowSensorCalStatus(
									ServiceMode::FAILED );
		}
		else
		{
		// $[TI1.4]
			// no more legal errors
			CLASS_ASSERTION_FAILURE();
		}
	}
	else if (error & FLOW_ERROR)
	{
	// $[TI2]
		// can't achieve min air flow
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_4) ;

		// set novram status flag to "failed"
		ServiceModeNovramData::SetFlowSensorCalStatus(
								ServiceMode::FAILED );
	}
	else if (error & TOLERANCE_ERROR)
	{
	// $[TI3]
		// air offset OOR
		RSmManager.sendStatusToServiceData(
			SmStatusId::TEST_FAILURE, SmStatusId::CONDITION_6) ;

		ServiceModeNovramData::SetFlowSensorCalStatus(
								ServiceMode::FAILED );
	}
	else
	{
	// $[TI4]
		// no more legal errors
		CLASS_ASSERTION_FAILURE();
	}

	// restore the flow sensor alphas to the service mode default
	RAirFlowSensor.setAlpha( SERVICE_MODE_ALPHA) ;
	RO2FlowSensor.setAlpha( SERVICE_MODE_ALPHA) ;
	RExhFlowSensor.setAlpha( SERVICE_MODE_ALPHA) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calibrateOneFlow_
//
//@ Interface-Description
//		This method takes gas type, calibrated flow to obtain 2 offset values and store them
//		(1) difference between insp. flow and exh. flow
//		(2) difference between desired flow and insp. flow
//---------------------------------------------------------------------
//@ Implementation-Description
//	Calibrate the input flow.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Uint32
FlowSensorCalibration::calibrateOneFlow_(GasType gas, Real32 desiredFlow)
{
	Real32 accumPressure;
	Boolean accumPressWithinTolerance = TRUE;
	Uint32 result = NO_ERROR;
	Real32 maxFlow;
	Real32 inspExhOffset;
	Real32 desiredFlowOffset;

	// measure the insp vs exh flow offset
	inspExhOffset = obtainInspExhOffset_( desiredFlow, gas, accumPressure) ;

	//Check if accumulator pressure is within tolerance
	if ((INLET_PRESSURE_SETPOINT - accumPressure) > MAX_ACCUM_PRESS_TOLERANCE)
	{
		accumPressWithinTolerance = FALSE;
	}

	// measure the desired vs attained insp flow
	desiredFlowOffset = obtainDesiredFlowOffset_( desiredFlow, gas);

	// bounds check and store the offset if it's in range
	Uint32 currentResult = storeOffset_( desiredFlow, inspExhOffset, desiredFlowOffset);

	//Check if max flow reached before flagging an error
	if (!accumPressWithinTolerance)
	{
		if (gas == AIR)
		{
			maxFlow = (Uint32)filteredAirFlow_;
		}
		else
		{
			maxFlow = (Uint32)filteredO2Flow_;
		}

		//Check max flow
		if (maxFlow < FS_CAL_MINIMUM_MAX_FLOW)
		{
			result = currentResult | INLET_PRESSURE_ERROR;
		}
	}
	else
	{
		result = currentResult;
	}

#ifdef SPIRO_DEBUG
	if (gas == AIR)
	{
		Real32 airOffset = RAirSideFlowSensorOffset.getOffset( desiredFlow) ;
		sprintf( string, "Air %3d %7.3f %7.3f %6.3f %6.3f %6.3f  %4.1f %4.1f %4.1f  %4.1f",
				desiredFlow, filteredAirFlow_, desiredFlowOffset,
				inspExhOffset,
				airOffset,
				inspExhOffset * 100 / desiredFlow,
				RO2TemperatureSensor.getValue(),
				RAirTemperatureSensor.getValue(),
				RExhGasTemperatureSensor.getValue(),
				RExhHeaterTemperatureSensor.getValue()) ;
	}
	else
	{
		Real32 o2Offset = RO2SideFlowSensorOffset.getOffset( desiredFlow) ;
		sprintf( string, "O2 %3d %7.3f %7.3f %6.3f %6.3f %6.3f  %4.1f %4.1f %4.1f  %4.1f",
				desiredFlow, filteredO2Flow_, desiredFlowOffset,
				inspExhOffset,
				o2Offset,
				inspExhOffset * 100 / desiredFlow,
				RO2TemperatureSensor.getValue(),
				RAirTemperatureSensor.getValue(),
				RExhGasTemperatureSensor.getValue(),
				RExhHeaterTemperatureSensor.getValue()) ;
	}

//			if (!(desiredFlow % 5))
		PrintQueue::SendString( string) ;
#endif // SPIRO_DEBUG	
	
	return result;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calibrate_
//
//@ Interface-Description
//		This method has GasType as an argument and returns the result of the
//		test.  A non-zero value implies no error.  This	method is called to perform
//		the test to obtain the flow offsets.
//---------------------------------------------------------------------
//@ Implementation-Description
//	For the input gas type:
//	Test what the maximum test flow is, and if it does not
//	meet the minimum spec, set result flag to FLOW_ERROR and skip
//	the flow offset measurement loop.
//	Loop to measure the flow offsets in 1 lpm increments starting at
//	the max test flow and ending at 1 lpm:
//		Measure the insp vs exh flow offset.
//		Measure the desired vs achieved insp flow offset.
//		If either offset is out of range for the desired flow, set
//		result flag to TOLERANCE_ERROR and exit loop.
//		If both offsets are in range, store the insp vs exh offset in
//		the appropriate flow offset object.
//	Return result flag.
//---------------------------------------------------------------------
//@ PreCondition
//		gas == AIR or O2
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Uint32
FlowSensorCalibration::calibrate_( GasType gas)
{
	CALL_TRACE("FlowSensorCalibration::calibrate_( GasType gas)") ;

	CLASS_PRE_CONDITION( gas == AIR || gas == O2);

#ifndef SIGMA_UNIT_TEST
	const Real32 MAX_FLOW_REDUCTION = 10.0;  // lpm
	const Uint32 MAX_FLOW_TEST_TIME = 10;  // secs
#endif  // SIGMA_UNIT_TEST

	Uint32 result = NO_ERROR ;

	if (gas == AIR)
	{
	// $[TI1]
		// setup for air flow sensor offset
		pFlowSensorOffset_ = &RAirSideFlowSensorOffset;

		// set exh flow sensor to read air table only
		RExhFlowSensor.setO2Percent( 21.0) ;
	}
	else
	{
	// $[TI2]
		// setup for O2 flow sensor offset
		pFlowSensorOffset_ = &RO2SideFlowSensorOffset;

		// set exh flow sensor to read o2 table only
		RExhFlowSensor.setO2Percent( 100.0) ;
	}
	// end else

	// determine the max achievable flow
	Int32 maxFlow = MAX_FLOW_CALIBRATION - 1;
	Int32 desiredFlow = CAL_STARTING_FLOW;

	RSmFlowController.establishFlow( desiredFlow, DELIVERY, gas) ;
	Task::Delay( 1, 0) ;

	// flow calibration loop
	while (result == NO_ERROR)
	{		
		// accumulate the overall result
		result = result | calibrateOneFlow_(gas, desiredFlow);

		if(desiredFlow <= CAL_STARTING_FLOW)
		{
			// when cal flow less than the starting flow, cal down to 1LPM
			if(--desiredFlow == 0)
			{
				//jump to 1 higher than the starting flow and then cal up from there
				desiredFlow = CAL_STARTING_FLOW + 1;
			}
		}
		else if(++desiredFlow > maxFlow)
		{
			break;
		}
	} // end while

	if(result & INLET_PRESSURE_ERROR)
	{
		maxFlow = desiredFlow - 1;
		if (maxFlow < FS_CAL_MINIMUM_MAX_FLOW)
		{
			maxFlow = 0;
		}
	}

	// set the 0 lpm offset and offsets above the max flow to 0
	setZeroOffsets_( maxFlow);

    RSmFlowController.stopFlowController() ;
	RAirPsol.updatePsol(0);
	RO2Psol.updatePsol(0);

	// re-initialize exh flow sensor to read air table only
	RExhFlowSensor.setO2Percent( 21.0) ;

    return (result) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: obtainDesiredFlowOffset_
//
//@ Interface-Description
//	This method calculates the offset between the desired flow and the
//	achieved inspiratory flow.  Arguments: desired flow and gas type.
//	Returns the offset.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Subtract the desired flow from the filtered insp flow for the
//	proper gas, return this value.
//---------------------------------------------------------------------
//@ PreCondition
//	gas == AIR or O2
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Real32
FlowSensorCalibration::obtainDesiredFlowOffset_(
								Uint32 desiredFlow, GasType gas)
{
	CALL_TRACE("FlowSensorCalibration::obtainDesiredFlowOffset_( \
								Uint32 desiredFlow, GasType gas)") ;

	CLASS_PRE_CONDITION( gas == AIR || gas == O2);

	Real32 offset;
	if (gas == AIR)
	{
	// $[TI1]
		offset = desiredFlow - filteredAirFlow_;
	}
	else
	{
	// $[TI2]
		offset = desiredFlow - filteredO2Flow_;
	}
	// end else

	return( offset);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: storeOffset_
//
//@ Interface-Description
//	This method is used to test if the insp vs exh offset and desired flow
//	offset are in range and if so to store the insp-exh offset.
//	Arguments: desired flow, insp vs exh offset, and desired flow offset.
//	Returns the test status.
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[09192]
//	Check if both offsets are within spec.  If not, set test status
//	flag to indicate a tolerance failure.
//	Store the insp vs exh offset if it is within spec.
//	Return the test status.
//---------------------------------------------------------------------
//@ PreCondition
//	pFlowSensorOffset_ != 0
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32
FlowSensorCalibration::storeOffset_(
		Uint32 desiredFlow, Real32 inspExhOffset, Real32 desiredFlowOffset)
{
	CALL_TRACE("FlowSensorCalibration::storeOffset_( \
		Uint32 desiredFlow, Real32 inspExhOffset, Real32 desiredFlowOffset)") ;

	CLASS_PRE_CONDITION( pFlowSensorOffset_ != 0);

	Uint32 result = NO_ERROR;

	const Real32 SPEC = maxInspOffset_[desiredFlow] + maxExhOffset_[desiredFlow] ;

	// TODO E600_LL: for debugging, to be removed
	WCHAR Str1[10], Str2[10], Str3[10];
	wsprintf(Str1, L"%03.3f", inspExhOffset);
	wsprintf(Str2, L"%03.3f", desiredFlowOffset);
	wsprintf(Str3, L"%03.3f", SPEC);
	
	if ( ABS_VALUE( inspExhOffset) > SPEC ||
			ABS_VALUE( desiredFlowOffset) > SPEC )
	{
	// $[TI1]
		// TODO E600_LL: don't set any error for now until the final version of the exh. flow sensor;
		//result = TOLERANCE_ERROR ;
		pFlowSensorOffset_->setOffset( desiredFlow, inspExhOffset) ;
		DEBUGMSG(1, (L"FAILED: desiredFlow = %3u  ieOffset = %s dfOffset = %s  spec = %s\r\n",
			desiredFlow, Str1, Str2, Str3)) ;	

#ifdef SPIRO_DEBUG
		sprintf( string, "desiredFlow = %3u  ieOffset = %7.3f "
							"dfOffset = %7.3f  spec = %6.3f\n",
				desiredFlow, inspExhOffset, desiredFlowOffset, SPEC) ;
		PrintQueue::SendString( string) ;
#endif // SPIRO_DEBUG
	}
	else
	{
	// $[TI2]
		pFlowSensorOffset_->setOffset( desiredFlow, inspExhOffset) ;
			DEBUGMSG(1, (L"desiredFlow = %3u  ieOffset = %s dfOffset = %s  spec = %s\r\n",
			desiredFlow, Str1, Str2, Str3)) ;	

	}
	// end else
	return( result);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: boundMaxFlow_
//
//@ Interface-Description
//	This method limits the max value of the max test flow and checks
//	if it exceeds the min value.  Arguments: max flow.
//	Returns the status of the min value check.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the max flow exceeds the maximum spec, set it to the maximum spec.
//	Else if the max flow is above the min spec, set test status to
//	indicate max flow error.
//	Return the test status.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Uint32
FlowSensorCalibration::boundMaxFlow_( Int32& maxFlow)
{
	CALL_TRACE("FlowSensorCalibration::boundMaxFlow_( Int32& maxFlow)") ;

	Uint32 result = NO_ERROR;

	if (maxFlow > (Int32) (MAX_FLOW_CALIBRATION - 1))
	{
	// $[TI1]
		maxFlow = MAX_FLOW_CALIBRATION - 1 ;
	}
	else if (maxFlow < FS_CAL_MINIMUM_MAX_FLOW)
	{
	// $[TI2]
		result = FLOW_ERROR ;
	}
	// $[TI3]
	// implied else

	return( result);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setZeroOffsets_
//
//@ Interface-Description
//	This method sets the 0 lpm offset and all offsets above the max
//	flow to 0 lpm.  Arguments: gas type and max flow, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Set the 0 lpm offset and all offsets above the max flow to 0 lpm.
//---------------------------------------------------------------------
//@ PreCondition
//	pFlowSensorOffset_ != 0
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
FlowSensorCalibration::setZeroOffsets_( Int32 maxFlow)
{
	CALL_TRACE("FlowSensorCalibration::setZeroOffsets_( Int32 maxFlow)") ;

	CLASS_PRE_CONDITION( pFlowSensorOffset_ != 0);

	// set 0 offset for 0 lpm
	pFlowSensorOffset_->setOffset( 0, 0.0) ;

	// set 0 offset for flows above maxFlow
	for (Int32 flow = maxFlow + 1; flow < MAX_FLOW_CALIBRATION; flow++)
	{
	// $[TI1]
		pFlowSensorOffset_->setOffset( flow, 0.0) ;
	}
	// end for
}
