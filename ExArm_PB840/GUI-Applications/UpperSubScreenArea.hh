#ifndef UpperSubScreenArea_HH
#define UpperSubScreenArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: UpperSubScreenArea - The area on the Upper screen
// in which subscreens are displayed.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/UpperSubScreenArea.hhv   25.0.4.0   19 Nov 2013 14:08:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 021   By: mnr   Date: 13-APR-2010    	SCR Number: 39
//  Project:  PROX
//  Description:
// 		SstProxSubScreen addition related updates.
//
//  Revision: 009  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 008  By:  rhj	  Date:  21-Oct-2008    SCR Number: 6435
//  Project:  S840
//  Description:     
//      Added Software Options Subscreen.
// 
//  Revision: 007  By:  rhj	  Date:  03-Apr-2007    SCR Number: 6237
//  Project:  Trend
//  Description:     
//      Added TrendGraphSubScreen, TrendTableSubScreen and EventSubScreen.
//
//  Revision: 006   By: rhj   Date:  20-Sep-2006    SCR Number: 6236
//  Project:  RESPM
//  Description:
//      Added the RM Data Subscreen 
//
//  Revision: 005  By:  btray	   Date:  21-Jul-1999    DCS Number: 5424 
//  Project: Neonatal 
//  Description:
//		Initial Neonatal version. Removed development option button
//		from normal mode.
//
//  Revision: 004  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 003  By:  yyy   Date:  04-Aug-1997    DCS Number:  2315
//  Project:  Sigma (R8027)
//  Description:
//      Removed the clearDisplayForVentInop() method.
//
//  Revision: 002  By:  yyy    Date:  16-JUL-97    DR Number: 2265
//       Project:  Sigma (R8027)
//       Description:
//             Added getApneaVentilationSubScreen(void) inline function
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreenArea.hh"

//@ Usage-Classes
class AlarmLogSubScreen;
class ApneaVentilationSubScreen;
class DiagCodeLogSubScreen;
class DiagLogMenuSubScreen;
class HelpSubScreen;
class MoreAlarmsSubScreen;
class MoreDataSubScreen;
class OperationalTimeSubScreen;
class SafetyVentilationSubScreen;
class SstResultsSubScreen;
class UpperOtherScreensSubScreen;
class VentTestSummarySubScreen;
class VentConfigSubScreen;
class WaveformsSubScreen;
class EstResultSubScreen;
class SstLeakSubScreen;
class SstProxSubScreen;

class RmDataSubScreen;
class TrendGraphsSubScreen;
class TrendTableSubScreen;
class EventSubScreen;
class SoftwareOptionsSubScreen;

#include "GuiAppClassIds.hh"
// FORNOW
#include "DevelopmentOptionsSubScreen.hh"
//@ End-Usage

class UpperSubScreenArea : public SubScreenArea
{
public:
	UpperSubScreenArea(void);
	~UpperSubScreenArea(void);

	virtual void keyPanelPressHappened(KeyPanel::KeyId keyId);
	virtual void keyPanelReleaseHappened(KeyPanel::KeyId key);
	void updateVentTestSummaryScreen(void);

	static void SoftFault(const SoftFaultID softFaultID,
							     const Uint32      lineNumber,
							     const char*       pFileName  = NULL, 
							     const char*       pPredicate = NULL);

	static ApneaVentilationSubScreen  *GetApneaVentilationSubScreen(void);
	static SafetyVentilationSubScreen *GetSafetyVentilationSubScreen(void);

	static AlarmLogSubScreen	*GetAlarmLogSubScreen(void);
	static DiagCodeLogSubScreen	*GetDiagCodeLogSubScreen(void);
	static DiagLogMenuSubScreen	*GetDiagLogMenuSubScreen(void);
	static HelpSubScreen		*GetHelpSubScreen(void);
	static MoreAlarmsSubScreen	*GetMoreAlarmsSubScreen(void);
	static MoreDataSubScreen	*GetMoreDataSubScreen(void);
	static OperationalTimeSubScreen	*GetOperationalTimeSubScreen(void);
	static SstResultsSubScreen	*GetSstResultsSubScreen(void);
	static UpperOtherScreensSubScreen *GetUpperOtherScreensSubScreen(void);
	static VentConfigSubScreen	*GetVentConfigSubScreen(void);
	static VentTestSummarySubScreen	*GetVentTestSummarySubScreen(void);
	static WaveformsSubScreen	*GetWaveformsSubScreen(void);
    static RmDataSubScreen  * GetRmDataSubScreen(void);
	static TrendGraphsSubScreen     *GetTrendGraphsSubScreen(void);
    static TrendTableSubScreen      *GetTrendTableSubScreen(void);
    static EventSubScreen       *GetEventSubScreen(void);
    static SoftwareOptionsSubScreen  * GetSoftwareOptionsSubScreen(void);

	static EstResultSubScreen 	*GetEstResultSubScreen(void);
	static SstLeakSubScreen		*GetSstLeakSubScreen(void);
	static SstProxSubScreen		*GetSstProxSubScreen(void);

	// FORNOW
	static DevelopmentOptionsSubScreen *GetDevelopmentOptionsSubScreen(void);
	
	void activateSafetyVentilationSubScreen(void);
	void deactivateSafetyVentilationSubScreen(void);
	void activateApneaVentilationSubScreen(void);
   	void deactivateApneaVentilationSubScreen(void);

	//@ Constant: CONTAINER_X
	// The X coordinate of the Upper SubScreen Area
	static const Int32 CONTAINER_X;
 
	//@ Constant: CONTAINER_Y
	// The Y coordinate of the Upper SubScreen Area
	static const Int32 CONTAINER_Y;
 
	//@ Constant: CONTAINER_WIDTH
	// The width of the Upper SubScreen Area in pixels
	static const Int32 CONTAINER_WIDTH;
 
	//@ Constant: CONTAINER_HEIGHT
	// The height of the Upper SubScreen Area in pixels
	static const Int32 CONTAINER_HEIGHT;
 
protected:

private:
	// these methods are purposely declared, but not implemented...
	UpperSubScreenArea(const UpperSubScreenArea&);	// not implemented...
	void operator=(const UpperSubScreenArea&);		// not implemented...



};

#endif // UpperSubScreenArea_HH 
