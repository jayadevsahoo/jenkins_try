#pragma once

class FaultHandler
{
public:
	static void  SoftFault(const SoftFaultID softFaultId,
			   const SubSystemID subSystemId,
			   const Uint32      moduleId,
			   const Uint32      lineNumber,
			   const char*       pFileName = NULL,
			   const char*       pBoolTest = NULL);

private:
	FaultHandler(void);
	~FaultHandler(void);
};
