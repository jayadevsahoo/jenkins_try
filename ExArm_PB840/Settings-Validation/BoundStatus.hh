
#ifndef BoundStatus_HH
#define BoundStatus_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  BoundStatus - Bound Status 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/BoundStatus.hhv   25.0.4.0   19 Nov 2013 14:27:16   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003   By: sah   Date:  04-Mar-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//	Project-related changes:
//	*  as part of adding soft-bound to Tube I.D., the violation
//	   id is now more managed
//
//  Revision: 002   By: sah  Date:  25-Mar-1998    DR Number: <NONE>
//  Project:  BILEVEL
//  Description:
//	Initial restructuring for adding soft-bound capability.
//
//  Revision: 001   By: sah    Date:  10-Oct-1996    DR Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "Sigma.hh"
#include "SettingClassId.hh"
#include "SettingId.hh"
#include "SettingBoundTypes.hh"
#include "SettingBoundId.hh"

//@ Usage-Classes
//@ End-Usage


class BoundStatus
{
  public:
    BoundStatus(const SettingId::SettingIdType settingId); 
    ~BoundStatus(void);

    inline Boolean  isInViolation(void) const;
    inline Boolean  isSoftBound  (void) const;

    inline SettingBoundId            getBoundId          (void) const;
    inline BoundState                getBoundState       (void) const;
    inline SettingId::SettingIdType  getViolatedSettingId(void) const;

    inline void  reset(void);  

    inline void  setBoundStatus(const BoundState               newBoundState,
				const SettingBoundId           newBoundId);
    inline void  setViolationId(const SettingId::SettingIdType violatedSettId);

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);

  private:
    BoundStatus(const BoundStatus&);		// not implemented...
    BoundStatus(void);				// not implemented...
    void  operator=(const BoundStatus&);	// not implemented...

    //@ Data-Member:  violatedSettingId_  
    // The settings identification associated with the bound status. 
    SettingId::SettingIdType  violatedSettingId_;

    //@  Data-Member:  boundState_  
    // Bound state indicates the status of the settings bounds.
    BoundState  boundState_;

    //@ Data-Member:  boundId_
    // This identifies the bound which had a violation in it.
    SettingBoundId  boundId_;
};


// Inlined methods
#include "BoundStatus.in"


#endif // BoundStatus_HH 
