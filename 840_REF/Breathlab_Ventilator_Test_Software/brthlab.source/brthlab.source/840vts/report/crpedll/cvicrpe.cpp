/*****************************************************************************/
/*   Module Name:  cvicrpe.cpp                                           
/*   Purpose:      Defines the initialization routines for the DLL.                              
/*                                                                          
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   Modified PrintReport() and             RHJ               27 Nov,2007    
/*   DisplayReportPreview() inorder to 
/*   be compiled under Microsoft Visual
/*   Studio 2003.         
/*
/*                                                                          
/*****************************************************************************/

#include "stdafx.h"
#include "cvicrpe.h"
#include <windows.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

/////////////////////////////////////////////////////////////////////////////
// CCvicrpeApp

BEGIN_MESSAGE_MAP(CCvicrpeApp, CWinApp)
	//{{AFX_MSG_MAP(CCvicrpeApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCvicrpeApp construction

CCvicrpeApp::CCvicrpeApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

CCvicrpeApp::~CCvicrpeApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCvicrpeApp object


CCvicrpeApp theApp;

extern "C" __declspec(dllexport) int OpenCrystalReportPrintEngine(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	return PEOpenEngine();
}

extern "C" __declspec(dllexport) int CloseCrystalReportPrintEngine(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	PECloseEngine();
	return 0;
}

extern "C" __declspec(dllexport) int DisplayReportPreview( char* report_path, char* db_path )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	int NavControls=PE_UNCHANGED;
	int CancelButton=PE_UNCHANGED;
	int PrintButton=PE_UNCHANGED;
	int ExportButton=PE_UNCHANGED;
	int ZoomControl=PE_UNCHANGED;
	int CloseButton=PE_UNCHANGED;
	int ProgressControls=PE_UNCHANGED;
	int SearchButton=PE_UNCHANGED;
	int PrintSetup=PE_UNCHANGED;
	int job_number = 0;
	BOOL result = FALSE;
	PETableLocation* Location=new PETableLocation;
    int errorCode = 0;


    //If a database is specifed change to it. 
    //WARNING: we don't make sure the db exists and is OK.
	if (report_path != NULL && db_path != NULL)
	{
	
    	job_number = PEOpenPrintJob( report_path );
    
        PEGetNthTableLocation (job_number, 0, Location);
        strcpy(Location->Location, db_path);
        PESetNthTableLocation (job_number, 0, Location);

    
    	if ( job_number )
    	{
    		PEWindowOptions ThisWindowsOptions={PE_SIZEOF_WINDOW_OPTIONS,
    			PE_UNCHANGED, PE_UNCHANGED, NavControls, CancelButton, PrintButton,
    			ExportButton, ZoomControl, CloseButton, ProgressControls, SearchButton,
    			PrintSetup, PE_UNCHANGED};
    
    	  	PESetWindowOptions(job_number, &ThisWindowsOptions);
    
    
    		// Force refresh of report data
    		PEDiscardSavedData(job_number);
    
    		result = PEOutputToWindow( job_number,
    								   "Print Preview",
    								   CW_USEDEFAULT,
    								   CW_USEDEFAULT,
    								   CW_USEDEFAULT,
    								   CW_USEDEFAULT,
    								   0,
    						  		   NULL );
    		if ( result )
    		{
    			// start the screen display, and wait until done
    			result = PEStartPrintJob( job_number, TRUE );
    
    			// "printing" is finished, so close the job; preview window remains open
    			PEClosePrintJob( job_number );
    		}
    	}
    
    	if ( !result )
    	{
    		char errtitle[15] = "";
    		char* errmessage = NULL;
    		short errorlen = 0;
    		HANDLE* errHandle = NULL;
    		sprintf(errtitle, "PE Error #%d", PEGetErrorCode(job_number));
    		PEGetErrorText(job_number, errHandle, &errorlen);
    		errmessage=new char[errorlen+15];
    		PEGetHandleString(errHandle, errmessage, errorlen);
    		strcat(errmessage, errtitle);
    		AfxMessageBox(LPCTSTR (errmessage), MB_OK);
    		delete errmessage;
    		errorCode = PEGetErrorCode(job_number);
    	}

    }

	delete Location;

	return errorCode;
														  
}


extern "C" __declspec(dllexport) int PrintReport(int jobNumber, int copies, int defaultPrinter)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	PEOutputToPrinter(jobNumber, 1);
	if(!defaultPrinter)
	{
		CPrintDialog PDlg(false, PD_ALLPAGES | PD_NOSELECTION | PD_NOPAGENUMS, NULL);
		if(PDlg.DoModal()==IDOK)
		{
			PESelectPrinter(jobNumber, (const char*) PDlg.GetDriverName().GetString(), 
				                       (const char*) PDlg.GetDeviceName().GetString(), 
									   (const char*) PDlg.GetPortName().GetString(), 
									   (DEVMODEA *) PDlg.GetDevMode());
		}
		else
		{
			return 1;
		}
	}
	if(jobNumber)
	{
		PEStartPrintJob(jobNumber, true);
	}
	else
	{
		char errtitle[15] = "";
		char* errmessage = NULL;
		short errorlen = 0;
		HANDLE* errHandle = NULL;
		sprintf(errtitle, "PE Error #%d", PEGetErrorCode(jobNumber));
		PEGetErrorText(jobNumber, errHandle, &errorlen);
		errmessage=new char[errorlen+15];
		PEGetHandleString(errHandle, errmessage, errorlen);
		strcat(errmessage, errtitle);
		AfxMessageBox(LPCTSTR (errmessage), MB_OK);
		delete errmessage;
		return PEGetErrorCode(jobNumber);
	}

	return 0;
														  
}
