@echo off
@REM ============================================================================
@REM @file   prelink.bat
@REM
@REM @path   $(DVSDK_INSTALL_DIR)/dvsdk_integration_xx_yy_zz
@REM
@REM @desc   This file copies all the necessary DVSDK dlls and apps to the 
@REM         BSP "FILES" and "FLATRELEASE" directories.
@REM
@REM ============================================================================
@REM Copyright (C) 2002-2009, Texas Instruments Incorporated -
@REM http://www.ti.com/
@REM
@REM Redistribution and use in source and binary forms, with or without
@REM modification, are permitted provided that the following conditions
@REM are met:
@REM 
@REM *  Redistributions of source code must retain the above copyright
@REM    notice, this list of conditions and the following disclaimer.
@REM 
@REM *  Redistributions in binary form must reproduce the above copyright
@REM    notice, this list of conditions and the following disclaimer in the
@REM    documentation and/or other materials provided with the distribution.
@REM 
@REM *  Neither the name of Texas Instruments Incorporated nor the names of
@REM    its contributors may be used to endorse or promote products derived
@REM    from this software without specific prior written permission.
@REM 
@REM THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
@REM AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
@REM THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
@REM PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
@REM CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
@REM EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
@REM PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
@REM OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
@REM WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
@REM OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
@REM EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
@REM ============================================================================

@REM Add pre-link commands below.
set COPYCMD=xcopy /q /r /y

if [%DVSDK_INSTALL_DIR%]==[] set DVSDK_INSTALL_DIR=%CD%
@echo DVSDK_INSTALL_DIR=%DVSDK_INSTALL_DIR%

@REM specify directory to copy object files
set DVSDK_OBJDIR=%DVSDK_INSTALL_DIR%\PreBuilt_obj\%_TGTCPU%\retail\std
set DVSDK_720P_OBJDIR=%DVSDK_INSTALL_DIR%\PreBuilt_obj\%_TGTCPU%\retail\720p
     

:copy_to_platform_files
    REM copy 720p binaries to target platform FILES dir
    if [%TI_ENABLE_720P_SUPPORT%]==[1] (
	@echo DVSDK_720P_OBJDIR=%DVSDK_720P_OBJDIR%
    	%COPYCMD% %DVSDK_720P_OBJDIR%\*.x64P %_TARGETPLATROOT%\FILES\
    	%COPYCMD% %DVSDK_720P_OBJDIR%\*.exe  %_TARGETPLATROOT%\FILES\
    	%COPYCMD% %DVSDK_720P_OBJDIR%\*.dll  %_TARGETPLATROOT%\FILES\
    	%COPYCMD% %DVSDK_720P_OBJDIR%\*.map  %_TARGETPLATROOT%\FILES\
    	%COPYCMD% %DVSDK_720P_OBJDIR%\*.pdb  %_TARGETPLATROOT%\FILES\
    ) else (
	REM copy binaries to target platform FILES dir
        %COPYCMD% %DVSDK_OBJDIR%\*.x64P %_TARGETPLATROOT%\FILES\
	%COPYCMD% %DVSDK_OBJDIR%\*.exe  %_TARGETPLATROOT%\FILES\
        %COPYCMD% %DVSDK_OBJDIR%\*.dll  %_TARGETPLATROOT%\FILES\
        %COPYCMD% %DVSDK_OBJDIR%\*.map  %_TARGETPLATROOT%\FILES\
	%COPYCMD% %DVSDK_OBJDIR%\*.pdb  %_TARGETPLATROOT%\FILES\
    )
  

   
:copy_to_flatreleasedir
    REM if WINCEREL is set, copy binaries to flat release dir
    if [%WINCEREL%]==[1] (
	if [%TI_ENABLE_720P_SUPPORT%]==[1] (
           @echo DVSDK_720P_OBJDIR=%DVSDK_720P_OBJDIR%
	    %COPYCMD% %DVSDK_720P_OBJDIR%\*.x64P %_FLATRELEASEDIR%
	    %COPYCMD% %DVSDK_720P_OBJDIR%\*.exe  %_FLATRELEASEDIR%
	    %COPYCMD% %DVSDK_720P_OBJDIR%\*.dll  %_FLATRELEASEDIR%
	    %COPYCMD% %DVSDK_720P_OBJDIR%\*.map  %_FLATRELEASEDIR%
	    %COPYCMD% %DVSDK_720P_OBJDIR%\*.pdb  %_FLATRELEASEDIR%
	) else (
	    %COPYCMD% %DVSDK_OBJDIR%\*.x64P %_FLATRELEASEDIR%
	    %COPYCMD% %DVSDK_OBJDIR%\*.exe  %_FLATRELEASEDIR%
	    %COPYCMD% %DVSDK_OBJDIR%\*.dll  %_FLATRELEASEDIR%
	    %COPYCMD% %DVSDK_OBJDIR%\*.map  %_FLATRELEASEDIR%
	    %COPYCMD% %DVSDK_OBJDIR%\*.pdb  %_FLATRELEASEDIR%
	)
    )
    
 
:end
