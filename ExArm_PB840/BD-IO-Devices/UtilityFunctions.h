
// ****************************************************************************
//
//  UtilityFunctions.h - This contains misc global functions which are not
//          associated with a class.
//
// ****************************************************************************

#ifndef UTILITY_FUNCTIONS_H

#define UTILITY_FUNCTIONS_H

#include "Sigma.hh"

void BinarySearchFloat( uint32_t *piLeft, uint32_t *piRight, float Value,
					   const float *pData, uint32_t iHi);

void LookUpFloat( float *pY, const float *pYData, float Value,
				 const float *pUData, uint32_t iHi);


#endif



