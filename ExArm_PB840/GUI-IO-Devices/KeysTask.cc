#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Filename: KeysTask.cc - Task that handles GUI keyboard input
//---------------------------------------------------------------------
//@ Interface-Description
// Keyboard Driver.
//---------------------------------------------------------------------
//@ Module-Decomposition
//	KeysDrvTask
//	KeysDrvTaskInit
//---------------------------------------------------------------------
//@ Rationale
// Process keyboard input for higher-level application tasks
//---------------------------------------------------------------------
//@ Implementation-Description
// The keys task polls the hardware for any change and determines Key Press 
// and Release actions.
//
// Valid key position changes are communicated to the higher-level GUI
// Application task through the User Annunciation Queue.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/GUI-IO-Devices/vcssrc/KeysTask.ccv   25.0.4.0   19 Nov 2013 14:12:20   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  sah    Date:  02-Jul-1997   DCS Number: 1848
//  Project:  Sigma (R8027)
//  Description:
//       Added this task into the task-monitoring mechanism.
//
//  Revision: 004  By:  sah    Date:  27-Jun-1997   DCS Number: 2023
//  Project:  Sigma (R8027)
//  Description:
//       Changed name of 'Keys.inl' file to 'Keys.in'.
//
//  Revision: 003  By:  gdc    Date:  28-MAY-1997    DR Number: 2008
//       Project:  Sigma (R8027)
//       Description:
//             Removed #include "Ipic.hh - obsolete.
//
//  Revision: 002  By:  clw    Date:  12-May-1997    DR Number: 2016
//       Project:  Sigma (R8027)
//       Description:
//             This file was including file MFP901.hh even though not needed.  This reference
//             was removed to allow that file to be obsoleted.
//
//  Revision: 001  By:  clw    Date:  28-Mar-1997    DR Number: <none>
//       Project:  Sigma (R8027)
//       Description:
//             Initial version to coding standards (Integration baseline).
//=====================================================================

//#define	GUI_IO_DEBUG	// uncomment to get verbose output

#include "Sigma.hh"
#include "MsgQueue.hh"
#include "UserAnnunciationMsg.hh"
#include "GuiIo.hh"
#include "Sound.hh"
#include "TaskMonitor.hh"
#include "AioDioDriver.h"


//@ Code...

Boolean	     KeyInputAllowed; // queue flow control

static const ULONG NOMINAL_KEY_WAIT_TIME    =   25; // ms to wait between readings

static const Uint16 NO_KEY_PRESSED_ = 0xffff; // no key pressed

static const Uint  QUEUE_TIMEOUT_ = 300;  // milliseconds for pending on Q...

static const IpcId  DEFAULT_KEY_Q_ = USER_ANNUNCIATION_Q;

static       IpcId  KeyQ_ = NULL_ID;  // post to when key event occurs


static       Uint16 KeyDownMask_ = 0;    // key being pressed
static       Uint16 LastKeyState_ = 0;   // key registers last time through here


enum AssertErrs
{
	BAD_KEY_MSG = 1
}; 


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  PostKey
//
//@ Interface-Description
// Post a key event message to the established MsgQueue
//---------------------------------------------------------------------
//@ Implementation-Description
// Form the standard UserAnnunciationMsg key event message and post it
// to the MsgQueue most recently established by calling RouteKeyEvents
//---------------------------------------------------------------------
//@ PreCondition
// A notifiable key event (press or release) has occurred
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

#define	WHAT_KEY_ACTION(action)					\
	(action == UserAnnunciationMsg::KEY_PRESS ? "PRESS" :	\
	 action == UserAnnunciationMsg::KEY_RELEASE ? "RELEASE" : "BOGUS")

static void
PostKey(UserAnnunciationMsg::KeyAction action, GuiKeys key)
{
	UserAnnunciationMsg keyMsg;			// appl message
	MsgQueue	userInputQueue(KeyQ_);		// appl queue

	keyMsg.qWord = 0;
	keyMsg.keyParts.eventType = UserAnnunciationMsg::INPUT_FROM_KEY;
	keyMsg.keyParts.action = action;
	keyMsg.keyParts.keyCode = key;

	DBG3("PostKey(): posting %s %s to MQ %d\n",
		 WHAT_KEY_ACTION(action), WHAT_KEY(key), KeyQ_);
	userInputQueue.putMsg((Uint32)keyMsg.qWord);
							//$[TI1]
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  RouteKeyEvents
//
//@ Interface-Description
// Until a subsequent call to this function, post key event messages
// to the passed MsgQueue ID
//---------------------------------------------------------------------
//@ Implementation-Description
// Assert if the passed MsgQueue ID is not a valid ID or 0 (NULL_ID).
// The special NULL_ID, a default value for the argument, re-establishes
// the DEFAULT_KEY_Q_ as the MsgQueue to post to.  If a new MsgQueue
// is being posted to, reset the history used by the KeysTask during
// interrupt processing.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Free-Function
//=====================================================================

void
RouteKeyEvents(const IpcId newQId)
{
	IpcId	oldQId = KeyQ_;
	MsgQueue triggerMQ(KEYBOARD_POLL_Q);		   	// our queue

	// trap bogus MsgQueue IDs
	FREE_ASSERTION((newQId >= NULL_ID  &&  newQId < MAX_QID),
			GUI_IO_DEVICES, 1);

	// special case of NULL_ID re-establishes the initial (GUI-Apps) Q
	if(newQId == NULL_ID)
	{		//$[TI1]
		KeyQ_ = DEFAULT_KEY_Q_;
	}
	else
	{		//$[TI2]
		KeyQ_ = newQId;
	}

	// there has been a new MsgQueue established
	if(KeyQ_ != oldQId)
	{		//$[TI3]
		// so reset any KeysTask history of prior events
		KeyDownMask_ = 0;		// no keys pressed
		LastKeyState_ = NO_KEY_PRESSED_;
		KeyInputAllowed =TRUE;		// posting is not blocked

		// dispose of any pending keys
		triggerMQ.flush();
	}		//$[TI4]
	DBG2("RouteKeyEvents(): old=%d new=%d\n", oldQId, newQId);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  KeysDrvTaskInit
//
//@ Interface-Description
// This initialization function is called before application tasks are
// executed.  All KeysDrvTask initialization is done here.
//---------------------------------------------------------------------
//@ Implementation-Description
// Sets up keymapping.
//---------------------------------------------------------------------
//@ PreCondition
// Application tasks have not started execution
//---------------------------------------------------------------------
//@ PostCondition
// none
//
//@ End-Free-Function
//=====================================================================

void
KeysDrvTaskInit(void)
{
	RouteKeyEvents(DEFAULT_KEY_Q_);
}


//============== F R E E   F U N C T I O N   D E S C R I P T I O N ====
//@ Free-Function:  KeysDrvTask
//
//@ Interface-Description
// This task's sole purpose is to poll for keyboard activity and
// respond accordingly.  Keyboard activity is defined to be any key state
// change (ie: at least one key was pressed or released).
// Activity polls the hardware on a regular basis.
//
//---------------------------------------------------------------------
//@ Implementation-Description
// This task polls the key registers every NOMINAL_KEY_WAIT_TIME ms.
//
//---------------------------------------------------------------------
//@ PreCondition
// KeysDrvTaskInit has already been called once
//---------------------------------------------------------------------
//@ PostCondition
// Since this is a task, it never returns and should only die when the
// system is reset
//
//@ End-Free-Function
//=====================================================================

void
KeysDrvTask(void)
{
	MsgQueue	triggerMQ(KEYBOARD_POLL_Q);		   	// our queue
	Uint16		key;
	GuiKeys		keyCode;
	Uint16		keyMask;
	Uint16		keyState;
	Uint16		keyStateChanges;

	// before entering task loop, report back to the task-monitor mechanism...
	TaskMonitor::Report();

	DBG("KeysDrvTask(): BEGIN\n");
	while(1)	// never stop
	{				//$[TI1] always enter here

		// report back to the task-monitor mechanism...
		TaskMonitor::Report();


		#ifdef SIGMA_GUIPC_CPU
			//For Key Simulation on GUI. 
			keyState = 0xffff;
		#else
			keyState = AioDioDriver_t::GetAioDioDriverHandle().ReadMembraneButtons();
		#endif

		DBG3("KeysDrvTask(): keyState=0x%X LastKeyState_=0x%X KeyDownMask_=0x%x\n",
			keyState, LastKeyState_, KeyDownMask_);

		// compare current state of keyboard with the one seen
		// most-recently (last time through here)
		keyStateChanges = keyState ^ LastKeyState_;
		DBG1("keyStateChanges=0x%X\n", keyStateChanges);

		if (keyStateChanges)	// there were changes
		{			//$[TI1.2]

			// figure out what the changes were
			for (key=0; key < NUMBER_OF_GUI_KEYS; key++)
			{		//$[TI1.2.1]
				keyMask = 1 << key;
				keyCode = GuiKeys(key);

				if (keyStateChanges & keyMask)	// this key changed state
				{	//$[TI1.2.1.1]
					if (keyState & keyMask)		// key has transitioned to up (active low logic)
					{		//$[TI1.2.1.1.1]
						if(keyMask & KeyDownMask_)	// and we posted the down event
						{		//$[TI1.2.1.1.1.1]
							PostKey((UserAnnunciationMsg::KeyAction)UserAnnunciationMsg::KEY_RELEASE, keyCode);
							// optionally, make a key release sound
							Sound::KeyReleaseSound(keyCode);

							//  the KeyInputAllowed is in GuiApp.cc 
							//  to keep the queue from overfilling...
							KeyDownMask_ = 0;	// forget the key
						}		//$[TI1.2.1.1.1.2]
					}
					else						// key has transitioned to down
					{		//$[TI1.2.1.1.2]
						DBG1("KeyInputAllowed=%d\n", KeyInputAllowed);
						if(KeyInputAllowed)
						{		//$[TI1.2.1.1.2.1]
							// *** key press is permitted by application ***
							PostKey((UserAnnunciationMsg::KeyAction)UserAnnunciationMsg::KEY_PRESS, keyCode);

							// make a key press sound
							Sound::KeyPressSound(keyCode);

							// prohibit further key *press*
							// events until GUI App permits
							KeyInputAllowed = FALSE;
							KeyDownMask_ = keyMask;	// remember which key
						}		//$[TI1.2.1.1.2.2]
					}
				}	//$[TI1.2.1.2]
			}
			LastKeyState_ = keyState;
		}
		else
		{			//$[TI1.3]
			DBG("KeysDrvTask(): No key changes\n");
		}

		// Wait for the nominal wait time:
		Task::Delay(0, NOMINAL_KEY_WAIT_TIME);
	}
}
