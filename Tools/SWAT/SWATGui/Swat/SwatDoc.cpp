// SwatDoc.cpp : implementation of the CSwatDoc class
//

#include "stdafx.h"
#include "Swat.h"
#include "SwatDoc.h"

#include "Swatview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSwatDoc

IMPLEMENT_DYNCREATE(CSwatDoc, CDocument)

BEGIN_MESSAGE_MAP(CSwatDoc, CDocument)
	ON_COMMAND(ID_FILE_NEW, &CSwatDoc::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CSwatDoc::OnFileOpen)
	ON_COMMAND(ID_FILE_SAVE, &CSwatDoc::OnFileSave)
	ON_COMMAND(ID_FILE_SAVE_AS, &CSwatDoc::OnFileSaveAs)
	ON_COMMAND(ID_FILE_MRU_FILE1, &CSwatDoc::OnFileMruFile1)
END_MESSAGE_MAP()


// CSwatDoc construction/destruction

CSwatDoc::CSwatDoc()
{
	// TODO: add one-time construction code here

}

CSwatDoc::~CSwatDoc()
{
}

BOOL CSwatDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CSwatDoc serialization

void CSwatDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CSwatDoc diagnostics

#ifdef _DEBUG
void CSwatDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSwatDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CSwatDoc commands

void CSwatDoc::OnFileNew()
{
	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)AfxGetApp()->m_pMainWnd;
	CMDIChildWnd *pChild = (CMDIChildWnd *) pFrame->GetActiveFrame();
	CSwatView *pView = (CSwatView *) pChild->GetActiveView();

	pView->NewScriptFile();
}

void CSwatDoc::OnFileOpen()
{
	CFileDialog dlg(TRUE, NULL,NULL,0, "SWAT Script List (swt)|*.swt|", NULL);
	if(dlg.DoModal()==IDCANCEL)		
		return;

	CMDIFrameWnd *pFrame = 
             (CMDIFrameWnd*)AfxGetApp()->m_pMainWnd;

	// Get the active MDI child window.
	CMDIChildWnd *pChild = 
				 (CMDIChildWnd *) pFrame->GetActiveFrame();

	// Get the active view attached to the active MDI child window.
	CSwatView *pView = (CSwatView *) pChild->GetActiveView();

	pView->OpenScriptFile(dlg.GetPathName());

}

void CSwatDoc::OnFileSave()
{
	CSwatApp* pApp = (CSwatApp*)AfxGetApp();
	if(pApp->m_strCurrentOpenScriptListFile.IsEmpty())
	{
		OnFileSaveAs();
		return;
	}

	CMDIFrameWnd *pFrame = (CMDIFrameWnd*)AfxGetApp()->m_pMainWnd;

	// Get the active MDI child window.
	CMDIChildWnd *pChild = (CMDIChildWnd *) pFrame->GetActiveFrame();

	// Get the active view attached to the active MDI child window.
	CSwatView *pView = (CSwatView *) pChild->GetActiveView();

	pView->SaveScriptFile(pApp->m_strCurrentOpenScriptListFile);
}

void CSwatDoc::OnFileSaveAs()
{
	CFileDialog dlg(FALSE, "swt", "*.swt",0, "SWAT Script List (swt)|*.swt|", NULL);
	if(dlg.DoModal()==IDCANCEL)
		return;

	CMDIFrameWnd *pFrame = 
             (CMDIFrameWnd*)AfxGetApp()->m_pMainWnd;

	// Get the active MDI child window.
	CMDIChildWnd *pChild = 
				 (CMDIChildWnd *) pFrame->GetActiveFrame();

	// Get the active view attached to the active MDI child window.
	CSwatView *pView = (CSwatView *) pChild->GetActiveView();

	pView->SaveScriptFile(dlg.GetPathName());
	(AfxGetMainWnd( ))->SetWindowText(SWAT_APPLICATION_TITLE + dlg.GetPathName());

}

void CSwatDoc::OnFileMruFile1()
{
	// TODO: Add your command handler code here
}


