#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: WaveformRecord - A record contains breath parameters used to display
//	user selectable waveforms.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class contains the basic parameters required for the display of the
//	various ventilator waveforms. The only method available for public access
//	is the assignment operator. Manipulation of the private data members is done
//	through friendship relationships.
//---------------------------------------------------------------------
//@ Rationale
//	This class defines a waveform data entry to class WaveformSet.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This class defines phase type, net volume, patient pressure and net flow as
//	a waveform record. A publicly defined assignment operator allows duplication
//	of that record. Friendship is bestowed on the class by the methods:
//	BreathRecord::newCycle(void) and SpiroWfTask::WaveformTask(void). 
//  friend void SpiroWfTask::WaveformTask(void);
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//	none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/WaveformRecord.ccv   25.0.4.0   19 Nov 2013 14:00:18   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 010   By: rhj   Date:  20-Apr-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added proxPatientPressure_ & proxManuever_
//
//  Revision: 009   By: erm   Date:  23-Apr-2002    DR Number: 5848
//  Project:  VCP
//  Description:
//      Clone PAV  rev 6.0.1.5
//      Added support for new 'isApneaActive_' and 'isErrorState_' fields.
//
//  Revision: 008   By: syw   Date:  01-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added lung pressure and lung flow (or wye).
//		Added is peep recovery status
//
//  Revision: 007  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 006  By:  gdc   Date:  25-Jun-1998    DR Number:
//       Project:  Color
//       Description:
//             Added breath type to waveform record.
//
//  Revision: 005  By:  sp    Date:  29-Oct-1996    DR Number:
//       Project:  Sigma (R8027)
//       Description:
//             Inspection Rework.
//
//  Revision: 004  By:  sp    Date:  17-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods.
//
//  Revision: 003  By:  sp   Date:  24-Apr-1996    DR Number: 718
//       Project:  Sigma (R8027)
//       Description:
//             Add autozero as a part of the waveform data.
//
//  Revision: 002  By:  sp   Date:  20-Feb-1995    DR Number: 600
//       Project:  Sigma (R8027)
//       Description:
//             Add unit test methods.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "WaveformRecord.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: WaveformRecord()  
//
//@ Interface-Description
//	Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformRecord::WaveformRecord(void)
{
  CALL_TRACE("WaveformRecord()");

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~WaveformRecord()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

WaveformRecord::~WaveformRecord(void)
{
  CALL_TRACE("~WaveformRecord()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: operator=
//
//@ Interface-Description
//	This method takes an argument of WaveformRecord and has no 
//	return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Duplicates the argument waverec.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
WaveformRecord::operator=(const WaveformRecord& waverec)
{
    CALL_TRACE("operator=(const WaveformRecord& waverec)");

    if (this != &waverec)
    {
        // $[TI1]
        patientPressure_ = waverec.patientPressure_;
        netFlow_ = waverec.netFlow_;
        netVolume_ = waverec.netVolume_;
		lungPressure_ = waverec.lungPressure_;
		lungFlow_ = waverec.lungFlow_;
		lungVolume_ = waverec.lungVolume_;
        phaseType_ = waverec.phaseType_;
        breathType_ = waverec.breathType_;
        isAutozeroActive_ = waverec.isAutozeroActive_;
        isPeepRecovery_ = waverec.isPeepRecovery_ ;
        isApneaActive_ = waverec.isApneaActive_ ;
        isErrorState_ = waverec.isErrorState_ ;

        modeSetting_ = waverec.modeSetting_ ;
        supportTypeSetting_ = waverec.supportTypeSetting_ ;
		proxManuever_ = waverec.proxManuever_;
		proxPatientPressure_ = waverec.proxPatientPressure_;
    }
    else
    {
        // $[TI2]
       SAFE_CLASS_ASSERTION(this != &waverec);
    }
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
WaveformRecord::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, WAVEFORMRECORD,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


#ifdef SIGMA_UNIT_TEST
void WaveformRecord::write(void) const
{
        cout << "patientPressure_:" << patientPressure_ << endl;
        cout << "netFlow_:" << netFlow_ << endl;
        cout << "netVolume_:" << netVolume_ << endl;
        cout << "lungPressure_:" << lungPressure_ << endl;
        cout << "lungFlow_:" << lungFlow_ << endl;
        cout << "lungVolume_:" << lungVolume_ << endl;
        cout << "phaseType_:" << (int)phaseType_ << endl;
        cout << "breathType_:" << (int)breathType_ << endl;
        cout << "isAutozeroActive_:" << (int)isAutozeroActive_ << endl;
        cout << "isPeepRecovery_:" << (int)isPeepRecovery_ << endl;
        cout << "mode_:" << mode_ << endl;
        cout << "supportType_:" << supportType_ << endl;
}
#endif //SIGMA_UNIT_TEST
