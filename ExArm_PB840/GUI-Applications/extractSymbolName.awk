# Extracts a list of symbol names. Assumes that the input is a file containing
# lines of the form "StringId Misc/PromptStrs::SymbolName ".
{
  array[NR] = $2
}

END {
 for (i=1;i<=NR;i++)
	print array[i]
}

