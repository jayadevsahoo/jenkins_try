#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: CompactFlashTestSubScreen - Compact Flash test service mode
//  test sub-screen.
//---------------------------------------------------------------------
//@ Interface-Description
// This sub-screen is activated by selecting Compact Flash Test
// button in the ServiceLowerOtherScreen.
// Only one instance of this class is created by LowerSubScreenArea.  The
// interface is pretty generic, as a sub-screen it defines the activate()
// and deactivate() methods and then receives button events
// through via the usual means (i.e., the buttonDownHappened(),
// buttonUpHappened() and adjustPanelAcceptPressHappened())
//---------------------------------------------------------------------
//@ Rationale
// Groups the components of the Compact Flash Test in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
//
// The main operation of this class is to wait for button down and up events,
// received via buttonUpHappened() ,buttonDownHappened() and
// adjustPanelAcceptPressHappened().  It also waits for test result data and
// test commands received from Service-Data and dispatched to the virtual
// methods processTestPrompt(), processTestKeyAllowed(),
// processTestResultStatus(), processTestResultCondition(), and
// processTestData().
// 
// On the other hand, button events are caught by the button callback methods.
// The display of this subscreen is organized into many different screen
// layouts corresponding to different stages of the test process.
// Pressing down the Start Test button causes the compact flash test process
// to begin.  The user's responses to the displayed prompts drives
// the process forward.
//
// The ServiceDataRegistrar is used to register for service data events.
// All other functionality is inherited from the SubScreenArea class.
//
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and
// pre-conditions to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only be
// displayed in the LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/CompactFlashTestSubScreen.ccv   25.0.4.0   19 Nov 2013 14:07:40   pvcs  $ 
//
//@ Modification-Log
//
//  Revision: 003  By:  rhj	   Date:  16-Jan-2008    SCR Number: 6408
//  Project:  Baseline
//  Description:
//		Modified activate() to change the status of 
//      ServiceMode::IN_PROGRESS to ServiceMode::FAILED.
// 
//  Revision: 002  By:  rhj	   Date:  20-Jul-2007    SCR Number: 6390
//  Project:  Trend
//  Description:
//		Implemented a way to reset the database after a successful 
//      pass of the compact flash test.     
//
//  Revision: 001  By:  gdc	   Date:  23-Mar-2007    SCR Number: 6237
//  Project:  Trend
//  Description:
//		Initial version.
//
//=====================================================================

#include "CompactFlashTestSubScreen.hh"

#include "AdjustPanel.hh"
#include "Colors.hh"
#include "LowerScreen.hh"
#include "MiscStrs.hh"
#include "NovRamManager.hh"
#include "PromptArea.hh"
#include "PromptStrs.hh"
#include "ServiceLowerScreen.hh"
#include "ServiceLowerScreenSelectArea.hh"
#include "ServiceUpperScreen.hh"
#include "SmManager.hh"
#include "SmTestId.hh"
#include "Sound.hh"
#include "UpperSubScreenArea.hh"
#include "TrendDataMgr.hh"

class SubScreenArea;

//@ Code...

// Initialize static constants.
static const Uint16 BUTTON_X_ = 520;
static const Uint16 BUTTON_Y_ = 367;
static const Uint16 BUTTON_WIDTH_ = 110;
static const Uint16 BUTTON_HEIGHT_ = 32;
static const Uint8  BUTTON_BORDER_ = 3;
static const Int32 STATUS_LABEL_X_ = 379;
static const Int32 STATUS_LABEL_Y_ = 0;
static const Int32 STATUS_WIDTH_ = 255;
static const Int32 STATUS_HEIGHT_ = 31;
static const Int32 ERR_DISPLAY_AREA_X1_ = 115;
static const Int32 ERR_DISPLAY_AREA_Y1_ = 100;
static const Int32 ERR_DISPLAY_ROW_WIDTH_ = 20;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: CompactFlashTestSubScreen()  [Default Constructor]
//
//@ Interface-Description
// Creates the Compact Flash Test Subscreen.  The given `pSubScreenArea' is 
// the SubScreenArea in which it will be displayed (in this case the upper 
// subscreen area).
//---------------------------------------------------------------------
//@ Implementation-Description
// Creates, positions, and initializes all the graphical components of this
// subscreen:  title and a number of static and dynamic textual items.
//  Also, register for callback for the Start Test button.
//
// $[TR01129] Method to test compact flash in service mode...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CompactFlashTestSubScreen::CompactFlashTestSubScreen(SubScreenArea *pSubScreenArea) 
:   SubScreen(              pSubScreenArea),
	testStatus_(            STATUS_LABEL_X_, STATUS_LABEL_Y_,
							STATUS_WIDTH_, STATUS_HEIGHT_,
							MiscStrs::EXH_V_CAL_STATUS_LABEL,
							NULL_STRING_ID),
	errorHappened_(         FALSE),
	errorIndex_(            0),
	keyAllowedId_(          -1),
	userKeyPressedId_(      SmPromptId::NULL_ACTION_ID),
	startTestButton_(       BUTTON_X_,              BUTTON_Y_,
							BUTTON_WIDTH_,          BUTTON_HEIGHT_,
							Button::DARK,           BUTTON_BORDER_,
							Button::SQUARE,         Button::NO_BORDER,
							MiscStrs::START_TEST_TAB_BUTTON_LABEL),
	guiTestMgr_(            this),
	titleArea_(             MiscStrs::COMPACT_FLASH_TEST_SUBSCREEN_TITLE)
{
	CALL_TRACE("CompactFlashTestSubScreen(pSubScreenArea)");

	// Size and position the sub-screen
	setX(0);
	setY(0);
	setWidth(SERVICE_LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(SERVICE_LOWER_SUB_SCREEN_AREA_HEIGHT);

	for (int i=0; i<MAX_CONDITION_ID; i++)
	{
		errDisplayArea_[i].setColor(Colors::WHITE);
		errDisplayArea_[i].setY(ERR_DISPLAY_AREA_Y1_+(i*ERR_DISPLAY_ROW_WIDTH_));
		errDisplayArea_[i].setX(ERR_DISPLAY_AREA_X1_);
		addDrawable(&errDisplayArea_[i]);
	}

	// Register for callbacks for all buttons for which we are interested.
	startTestButton_.setButtonCallback(this);

	// Add the title area
	addDrawable(&titleArea_);

	// Add the buttons.
	addDrawable(&startTestButton_);

	// Add status line text objects to main container
	addDrawable(&testStatus_);

	guiTestMgr_.setupTestResultDataArray();
	guiTestMgr_.setupTestCommandArray();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~CompactFlashTestSubScreen  [Destructor]
//
//@ Interface-Description
//  Destroys the Enter Service subscreen.  Needs to do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Empty 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

CompactFlashTestSubScreen::~CompactFlashTestSubScreen(void) 
{
	CALL_TRACE("CompactFlashTestSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just before this subscreen is displayed
//  on the screen.  No parameters needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  The first thing to do is to update the test status with result 
//  from the last Service test run, if any.  Next, register the subscreen with
//  Gui Test manager for test events.  Then we call the generic screen layout
//  method to display the initial subscreen configuration.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::activate(void)
{
	CALL_TRACE("activate(void)");

	ServiceMode::TestStatus status = ServiceMode::TestStatus(NovRamManager::GetCompactFlashTestStatus());

	if (status == ServiceMode::FAILED)
	{
		testStatus_.setStatus(MiscStrs::CF_TEST_FAILED_MSG);
	}
	else if (status == ServiceMode::PASSED)
	{
		testStatus_.setStatus(MiscStrs::CF_TEST_COMPLETE_MSG);
	}
	else if (status == ServiceMode::NEVER_RUN)
	{
		testStatus_.setStatus(MiscStrs::CF_TEST_NEVER_RUN_MSG);
	}
	else if (status == ServiceMode::IN_PROGRESS)
	{
		NovRamManager::UpdateCompactFlashTestStatus(ServiceMode::FAILED);
		testStatus_.setStatus(MiscStrs::CF_TEST_FAILED_MSG);
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(status);
	}

	//
	// Register all general possible test results for test events, but first
	// we have to initialize ServiceDataRegistrar.
	//
	guiTestMgr_.registerTestResultData();
	guiTestMgr_.registerTestCommands();

	// Let the service mode manager know the type of Service mode.
	SmManager::DoFunction(SmTestId::SET_TEST_TYPE_MISC_ID);

	layoutScreen_(TEST_SETUP);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
//  Called by LowerSubScreenArea just after this subscreen has been
//  removed from the screen.  No parameters are needed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  We perform any necessary cleanup.  First signal to Gui Test manager
//  about the end of test.  Release the AdjustPanel focus, then clear
//  the prompt area.  The Start Test button is shown in up position.
//  Finally, the LowerScreen Select area is shown.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::deactivate(void)
{
	CALL_TRACE("deactivate(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Clear the Primary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
							  PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
							  PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
							  PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Advisory prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
							  PromptArea::PA_LOW, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
							  PromptArea::PA_HIGH, NULL_STRING_ID);

	// Clear the Secondary prompt
	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
							  PromptArea::PA_LOW, NULL_STRING_ID);


	// Pop up the button.
	startTestButton_.setToUp();

	// Activate lower subscreen area.
	ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonDownHappened
//
//@ Interface-Description
// Called when start buttons in Compact Flash Sub-Screen is
// pressed down (because this button registers a callback).
// Passed a pointer to the button as well as 'byOperatorAction', a flag which
// indicates whether the operator pressed the button down or whether the
// setToDown() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::buttonDownHappened(Button *pButton,
												   Boolean byOperatorAction)
{
	CALL_TRACE("buttonDownHappened(Button *pButton, "
			   "Boolean byOperatorAction)");
	if (!isVisible())
	{
		return;
	}

	if (pButton == &startTestButton_)
	{
		// Grab Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(this, TRUE);

		// Disable the knob sound.
		AdjustPanel::DisableKnobRotateSound();

		//
		// Set all prompts 
		//
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY, 
								  PromptArea::PA_HIGH,
								  PromptStrs::SM_KEY_ACCEPT_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY, 
								  PromptArea::PA_HIGH,
								  NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY, 
								  PromptArea::PA_LOW,
								  NULL_STRING_ID);
		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
								  PromptArea::PA_HIGH,
								  PromptStrs::OTHER_SCREENS_CANCEL_S);

		// Set the next test status.
		testStateId_ = START_BUTTON_PRESSED;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: buttonUpHappened
//
//@ Interface-Description
// Called when any button in Compact Flash Test sub-screen is 
// deselected.  Passed a pointer to the button as well as 'byOperatorAction', 
// which indicates whether the operator pressed the button up or whether the 
// setToUp() method was called.
//---------------------------------------------------------------------
//@ Implementation-Description
//  There is a possibility that this event occurs after this subscreen is
//  removed from the screen (when this subscreen is deactivated and the
//  Adjust Panel focus is lost).  In this case we do nothing, simply return.
//  Otherwise, we clear the Adjust Panel focus, dehighlight the prompt area
//  and change the display state id. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::buttonUpHappened(Button *pButton, 
												 Boolean byOperatorAction)
{
	CALL_TRACE("buttonUpHappened(Button *pButton, Boolean byOperatorAction)");

	// Check if the event is operator-initiated
	if (byOperatorAction)
	{
		// Clear the all prompts
		// Clear the Adjust Panel focus
		AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

		// Make sure the prompt area's background is dehighlighted.
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
								  PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY, 
								  PromptArea::PA_HIGH, NULL_STRING_ID);

		// Set the next test status.
		testStateId_ = UNDEFINED_TEST_STATE;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelAcceptPressHappened
//
//@ Interface-Description
//  This is a virtual method inherited from being an AdjustPanelTarget.  It is
//  normally called when the operator presses down the Accept key to signal the
//  accepting of continuing the test process.
//---------------------------------------------------------------------
//@ Implementation-Description
//  First reset the Adjust Panel focus, then clear the prompt area.
//  Next, depending on the display state id, we layout the screen accordingly.
//  The test state is set to UNDEFINED_TEST_STATE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::adjustPanelAcceptPressHappened(void)
{
	CALL_TRACE("adjustPanelAcceptPressHappened(void)");

	// Make sure that nothing in this subscreen keeps focus.
	AdjustPanel::TakeNonPersistentFocus(NULL, TRUE);

	// Make sure the prompt area's background is dehighlighted.
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
							  PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
							  PromptArea::PA_HIGH, NULL_STRING_ID);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
							  PromptArea::PA_HIGH, NULL_STRING_ID); 

	switch (testStateId_)
	{
	case START_BUTTON_PRESSED:
		layoutScreen_(TEST_START_ACCEPTED);
		break;

	case PROMPT_ACCEPT_BUTTON_PRESSED:
		// Set user key pressed id.
		userKeyPressedId_ = SmPromptId::KEY_ACCEPT;

		// ACCEPT key being pressed in response to the test PROMPT.
		layoutScreen_(TEST_PROMPT_ACCEPTED_CLEARED);
		break;

	default:
		// Make the Invalid Entry sound
		Sound::Start(Sound::INVALID_ENTRY);
		break;
	}

	testStateId_ = UNDEFINED_TEST_STATE;
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: layoutScreen_
//
//@ Interface-Description
// Sets the subscreen into one of three display configurations.
//---------------------------------------------------------------------
//@ Implementation-Description
//  If screen id is TEST_SETUP, we update the prompt area and 
//  show the Start Test button.
//  If screen id is TEST_START_ACCEPTED, we reset the error flag,
//  and error display area, update the prompt area, hide the start test
//  button and the lower select area.  Then we start up the test.
//  The status area is also updated.
//  If screen id is TEST_PROMPT_ACCEPTED_CLEARED, we update the prompt, status,
//  then communicate this state to Service Mode manager.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::layoutScreen_(TestScreenId testScreenId)
{
	CALL_TRACE("layoutScreen_(TestScreenId testScreenId)");

	switch (testScreenId)
	{
	case TEST_SETUP:
		// Activate the lower screen 's select area.
		ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(FALSE);

		// Setup for transfer flash test information to GUI board.
		titleArea_.setTitle(MiscStrs::COMPACT_FLASH_TEST_SUBSCREEN_TITLE);

		// Set all prompts
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
								  PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_TOUCH_START_TEST_TO_BEGIN_P);
		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
								  PromptArea::PA_LOW, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
								  PromptArea::PA_LOW, PromptStrs::OTHER_SCREENS_CANCEL_S);

		// Show the startTest button
		startTestButton_.setShow(TRUE);

		break;

	case TEST_START_ACCEPTED:
		{
			// Set all prompts
			// Pop up the startTestButton_.
			startTestButton_.setToUp();

			// Hide the start button.
			startTestButton_.setShow(FALSE);

			// Hide the lower screen select area.
			ServiceLowerScreen::RServiceLowerScreen.getServiceLowerScreenSelectArea()->setBlank(TRUE);

			startTest_(SmTestId::COMPACT_FLASH_TEST_ID);

			break;
		}   

	case TEST_PROMPT_ACCEPTED_CLEARED:

		// Set Primary prompt to "Please Wait...."
		guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
								  PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_PLEASE_WAIT_P);

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
								  PromptArea::PA_HIGH, NULL_STRING_ID);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
								  PromptArea::PA_HIGH, NULL_STRING_ID);

		// Display the overall testing status on the service status area
		testStatus_.setStatus(MiscStrs::EXH_V_CAL_RUNNING_MSG);

		// Signal the Service Test Manager to continue the test.
		if (userKeyPressedId_ != SmPromptId::NULL_ACTION_ID)
		{
			SmManager::OperatorAction(SmTestId::COMPACT_FLASH_TEST_ID,
									  userKeyPressedId_);
		}

		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}


//============================ m e t h o d   d e s c r i p t i o n ====
//@ Method: processTestPrompt_
//
//@ Interface-Description
//  Process the prompt command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Do nothing, ignore all prompts.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::processTestPrompt(Int command)
{
	CALL_TRACE("processTestPrompt(Int command)");

	// do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultStatus
//
//@ Interface-Description
//  Process the test result status given by Service Mode. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Dispatch the given command and make the corresponding layoutScreen_
//  call to update the screen display.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::processTestResultStatus(Int resultStatus, Int testResultId)
{
	CALL_TRACE("processTestResultStatus_"
			   "(Int resultStatus Int testResultId)");

	switch (resultStatus)
	{
	case SmStatusId::TEST_END:
		{
			if (errorHappened_)
			{
				// Display the test failed status message.
				testStatus_.setStatus(MiscStrs::CF_TEST_FAILED_MSG);
			}
			else
			{
				// Display the test completed status message.
				testStatus_.setStatus(MiscStrs::CF_TEST_COMPLETE_MSG);
				
				// Reset the Database
                TrendDataMgr::ResetDatabase();

			}

			ServiceUpperScreen::RServiceUpperScreen.getUpperSubScreenArea()->updateVentTestSummaryScreen();
			layoutScreen_(TEST_SETUP);
		}
		break;

	case SmStatusId::TEST_START:
		break;

	case SmStatusId::TEST_ALERT:
	case SmStatusId::TEST_FAILURE:
		errorHappened_ = TRUE;
		break;

	default:
		CLASS_ASSERTION_FAILURE();
		break;
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestResultCondition
//
//@ Interface-Description
//  Process test result condition passed by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Display the text for the corresponding result condition on the screen
//  and update the prompt area.
//---------------------------------------------------------------------
//@ PreCondition
//  Result condition has to be in the range [0, MAX_CONDITION_ID]
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::processTestResultCondition(Int resultCondition)
{
	CALL_TRACE("processTestResultCondition(Int resultCondition)");

	SAFE_CLASS_ASSERTION((resultCondition >= 0) && (resultCondition <= MAX_CONDITION_ID));

	if (resultCondition > 0)
	{
		errDisplayArea_[errorIndex_].setText(conditionText_[resultCondition-1]);
		errDisplayArea_[errorIndex_].setShow(TRUE);
		errorIndex_++;

		guiTestMgr_.setTestPrompt(PromptArea::PA_ADVISORY,
								  PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_SEE_OP_MANUAL_A);

		guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
								  PromptArea::PA_HIGH, NULL_STRING_ID);
	}
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestKeyAllowed
//
//@ Interface-Description
//  Process keyAllowed command given by Service-Mode.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Saves key allowed in keyAllowedId_ for later processing in 
//  processTestPrompt_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::processTestKeyAllowed(Int command)
{
	CALL_TRACE("processTestKeyAllowed(Int command)");

	// Remember the expected user key response ID
	keyAllowedId_ = command;

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustPanelRestoreFocusHappened
//
//@ Interface-Description
// This is a virtual method inherited from being an AdjustPanelTarget.
// It is normally called when the operator release the off-screen
// keyboard key and the AdjustPanel needs to restore the default
// AdjustPanel target.  It is the duty of this inherited method to restore
// the previous test prompts in order to continue the testing
// process.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  Call GuiManager's virtual method to do the job.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::adjustPanelRestoreFocusHappened(void)
{
	CALL_TRACE("adjustPanelRestoreFocusHappened(void)");

	guiTestMgr_.restoreTestPrompt();

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: processTestData
//
//@ Interface-Description
//  Process data given by Service-Mode
//---------------------------------------------------------------------
//@ Implementation-Description
//  Since this subscreen is designed not to display test data,
//  this remains an empty method
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::processTestData(Int dataIndex, TestResult *pResult)
{
	CALL_TRACE("processTestData(Int dataIndex, TestResult *pResult)");
	// Empty method
	// Compact Flash Test doesn't display test data
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setErrorTable_
//
//@ Interface-Description
// 	Sets the error text in the error table.  Any condition that arises
//	has a unique id which is used as an index into this table of condition
//	text. The conditionText_ table contains translation text of Service-Mode
//  error conditions.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Assign condition text (non-cheap text) to conditionText_ table.
//	We also initialize the error display area's show flag.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::setErrorTable_(void)
{
	CALL_TRACE("setErrorTable_(void)");

	// Reset status flag
	errorHappened_ = FALSE;
	errorIndex_ = 0;

	Int testIndex = 0;
	if (currentTestId_ == SmTestId::COMPACT_FLASH_TEST_ID)
	{
		conditionText_[testIndex++] = MiscStrs::CF_INTERFACE_ERROR;
		conditionText_[testIndex]   = MiscStrs::CF_FLASH_ERROR;
	}
	else
	{
		CLASS_ASSERTION_FAILURE();
	}

	// Make sure total test count is within the maximum allowed.
	CLASS_ASSERTION(testIndex <= MAX_CONDITION_ID);

	for (int i=0; i < MAX_CONDITION_ID; i++)
	{
		errDisplayArea_[i].setShow(FALSE);
	}   
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startTest_
//
//@ Interface-Description
//	Preparation step for a test to start.	
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize the condition text (error) table.  Display prompts and status.
//	Finally, tell Service Mode Manager to do the test.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::startTest_(SmTestId::ServiceModeTestId currentTestId)
{
	CALL_TRACE("startTest_(SmTestId::ServiceModeTestId currentTestId)");

	currentTestId_ = currentTestId;

	// Set the appropriated error message table.
	setErrorTable_();

	// Set all prompts
	guiTestMgr_.setTestPrompt(PromptArea::PA_PRIMARY,
							  PromptArea::PA_LOW, PromptStrs::EXH_V_CAL_TESTING_P);

	guiTestMgr_.setTestPrompt(PromptArea::PA_SECONDARY,
							  PromptArea::PA_LOW, NULL_STRING_ID); 

	// Display the overall testing status on the service status area
	testStatus_.setStatus(MiscStrs::EXH_V_CAL_RUNNING_MSG);

	// Start the test.
	SmManager::DoTest(currentTestId);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void CompactFlashTestSubScreen::SoftFault(const SoftFaultID  softFaultID,
										  const Uint32       lineNumber,
										  const char*        pFileName,
										  const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							COMPACTFLASHTESTSUBSCREEN,
							lineNumber, pFileName, pPredicate);
}
