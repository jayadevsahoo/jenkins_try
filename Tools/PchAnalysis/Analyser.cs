﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace PchAnalysis
{
    enum LogLevel
    {
        Error,
        Warning,
        Info
    }

    /// <summary>
    /// This is a analyser used to analyse the Pre-Compiled Header includings in
    /// C++ implementation files.
    /// </summary>
    class Analyser
    {
        private List<String> PchAddedPchFileList_ = new List<string>();
        private List<String> AnalyserLog_ = new List<string>();
        static String FileNameCache_ = null;
        private String PchFileName_ = "stdafx.h";

        /// <summary>
        /// Write a log message to buffer list
        /// </summary>
        /// <param name="fileName">The file name of the log associated file</param>
        /// <param name="message">Log message</param>
        /// <param name="logLevel">Log level</param>
        private void writeToLogBuffer_(String fileName, String message, LogLevel logLevel)
        {
            if (Program.Opt_NoWarning && logLevel == LogLevel.Warning)
            {
                return;
            }

            if (Program.Opt_NoInfo && logLevel == LogLevel.Info)
            {
                return;
            }

            if (FileNameCache_ != fileName)
            {
                FileNameCache_ = fileName;

                // Add a empty line if the log list is not empty
                if (AnalyserLog_.Count != 0)
                {
                    printThenAddToLogList_("");
                }

                printThenAddToLogList_(fileName);
            }

            String newMsg = String.Format(" -- {0}: {1}", logLevel.ToString().ToUpper(), message);
            printThenAddToLogList_(newMsg);
        }

        /// <summary>
        /// Write the message to Console then add the log to list
        /// </summary>
        /// <param name="message"></param>
        private void printThenAddToLogList_(string message)
        {
            Console.WriteLine(message);
            AnalyserLog_.Add(message);
        }

        /// <summary>
        /// Get all cpp/cc source files from source folder
        /// </summary>
        /// <param name="sourceFolder">The base folder of source codes</param>
        /// <returns>c++ implementation files list</returns>
        private List<String> getAllCppFiles_(String sourceFolder)
        {
            String[] cpps = Directory.GetFiles(sourceFolder, "*.cpp", SearchOption.AllDirectories);
            String[] ccs = Directory.GetFiles(sourceFolder, "*.cc", SearchOption.AllDirectories);

            List<String> allCppFiles = new List<string>();

            allCppFiles.AddRange(cpps);
            allCppFiles.AddRange(ccs);

            return allCppFiles;
        }

        /// <summary>
        /// Get the executable path
        /// </summary>
        /// <returns>Excutable Path</returns>
        private String getExecutablePath_()
        {
            String location = System.Reflection.Assembly.GetExecutingAssembly().Location;

            return Path.GetDirectoryName(location);
        }

        /// <summary>
        /// Clear all output list
        /// </summary>
        private void clearOutputList_()
        {
            PchAddedPchFileList_.Clear();
            AnalyserLog_.Clear();
        }

        /// <summary>
        /// Save results to files
        /// </summary>
        private void saveResultsToFiles_()
        {
            String executablePath = getExecutablePath_();

            File.WriteAllLines(Path.Combine(executablePath, "PchAnalysis.log"), AnalyserLog_.ToArray());
            File.WriteAllLines(Path.Combine(executablePath, "PchAddedFileList.log"), PchAddedPchFileList_.ToArray());
        }

  
        /// <summary>
        /// Execute the parsing, according to the option
        /// </summary>
        /// <param name="sourceFolder"></param>
        /// <param name="autoAddPch"></param>
        public void execute(String sourceFolder, Boolean autoAddPch)
        {
            // Step 1: clear all output list
            clearOutputList_();

            // Step 2: get all c++ implementation files
            List<String> allCppFiles = getAllCppFiles_( sourceFolder);

            Regex regexIncludeParser = new Regex("^\\s*#\\s*include\\s*[\"|<](.*)[\"|>]\\s*", RegexOptions.Singleline);

            // Step 3: go through all c++ implementation files
            foreach (String cppFile in allCppFiles)
            {
                // Step 3.1: read all lines to memory
                String[] codeLines = File.ReadAllLines(cppFile);

                Boolean firstIncludingIsPch = true;
                
                // if a including is detected
                Boolean includingIsDetected = false;

                Int32 pchIncludingCount = 0;
                Int32 firstIncludingIndex = Int32.MaxValue;
                
                // Step 3.2: go through all lines 
                for (Int32 lineIndex = 0; lineIndex < codeLines.Length; ++lineIndex)
                {
                    String line = codeLines[lineIndex];

                    // Step 3.3: use regular expression to analyse the current line
                    Match match = regexIncludeParser.Match(line, 0);
                    if (match.Success)
                    {
                        String headerIncluded = match.Groups[1].Value;
                        Boolean headerIsPch = (headerIncluded.ToLower() == PchFileName_);

                        if (!includingIsDetected)
                        {
                            firstIncludingIsPch = headerIsPch;
                            firstIncludingIndex = lineIndex;
                            includingIsDetected = true;
                        }

                        if (headerIsPch)
                        {
                            pchIncludingCount++;
                        }
                    }
                }

                // Step 3.4: save analysis results to output list
                if (pchIncludingCount == 0)
                {
                    if (autoAddPch)
                    {
                        writeToLogBuffer_(cppFile, "adding PCH including to first line of file.", LogLevel.Info);

                        // Read the all bytes from file
                        byte[] oldBytes = File.ReadAllBytes(cppFile);
                        
                        // Open the file for write
                        FileStream fileStream = File.Open(cppFile, FileMode.Open, FileAccess.ReadWrite);
                        
                        // Preappending line
                        String pchIncludingLine = String.Format("#include \"{0}\"\r\n", PchFileName_);
                        byte[] pchIncludingBytes = Encoding.Default.GetBytes(pchIncludingLine);

                        // Change the length of the file
                        fileStream.SetLength(pchIncludingBytes.Length + oldBytes.Length);

                        // Set position to very beginning, then write two bytes array
                        fileStream.Position = 0;
                        fileStream.Write(pchIncludingBytes, 0, pchIncludingBytes.Length);
                        fileStream.Write(oldBytes, 0, oldBytes.Length);

                        // Close the file
                        fileStream.Close();

                        PchAddedPchFileList_.Add(cppFile);
                    }
                    else
                    {
                        writeToLogBuffer_(cppFile, "no PCH included.", LogLevel.Warning);
                    }
                }
                else if (pchIncludingCount == 1)
                {
                    if (!firstIncludingIsPch)
                    {
                        writeToLogBuffer_(cppFile, "PCH is not included as first including.", LogLevel.Error);
                    }
                    else if (firstIncludingIndex != 0)
                    {
                        writeToLogBuffer_(cppFile, "PCH is not included at first line of file.", LogLevel.Warning);
                    }
                }
                else
                {
                    writeToLogBuffer_(cppFile, "two or more PCH included.", LogLevel.Error);
                }
            } 
           
            // Step 4: save results to files
            saveResultsToFiles_();
        }       
    }
}
