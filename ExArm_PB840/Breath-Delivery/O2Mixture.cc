#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: O2Mixture - Determines what O2 mix to apply
//---------------------------------------------------------------------
//@ Interface-Description
//	$[04275]
//	The main purpose of this class is to determine the O2 mix that should be
//	applied to the system at any given time. An object that needs the applied
//	O2 information is responsible for acquiring it using the methods suppplied by
//	this class. The GasSupply object notifies the O2Mixture class when
//  a change in gas supply availability has occurred. The user event that
//	implements the operator request for 100% O2 is responsible for notifying the
//	class when an operator request is detected. To incorporate new settings,
//	the applied O2 is evaluated every start of breath phase -- after new
//	settings have been phased-in.  Since O2 mixture may be applied for
//  a predefined period of time, the class is also a
//	TimerTarget class. The class tracks the mix errors caused during breath
//	delivery and adjusts the applied O2 percent every start of a new breath
//	to compensate for these errors.
//  The class also implements the one point calibration for the O2 sensor.
//---------------------------------------------------------------------
//@ Rationale
//	O2 mixture is determined by various events and by the state of the system.
//	This class is needed to handle all events that affect the O2 mix.
//---------------------------------------------------------------------
//@ Implementation-Description
//	O2 mixture is determined based on ventilator settings, ventilator mode
//	(i.e. apnea, disconnect), user request for 100% O2, and availability of gas
//	supply (loss/recovery of either air, o2 or both). This class provides
//	methods that allow other objects to change or request a change in the O2
//	mix.  Methods are provided to accomplish the following:
//		-- To allow the user to request 100% O2.
//		-- To allow the user to interogate the 100% O2 request status
//		-- To determine O2 mix before a new breath phase starts.
//		-- To set the gas supply status whenever availabilty of either source
//		 is affected.
//		-- To implement the changes required in O2 mix when the  100 % timer
//		expires.
//		-- To calculate the breath mix so that appropriate compensation can be
//		applied to the O2 mix. 
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//	Only one instance of this class may be created 
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/O2Mixture.ccv   25.0.4.0   19 Nov 2013 13:59:56   pvcs  $
//
//@ Modification-Log
//
//  Revision: 024  By:  rpr    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 023   By: rpr    Date: 29-Jan-2009    SCR Number: 6463
//  Project:  840S
//  Description:
//      During CPU assertions cancel O2 manuever if present.
// 
//  Revision: 022   By: rpr    Date: 23-Jan-2009    SCR Number: 6435  
//  Project:  840S
//  Description:
//      Modified for code review comments.
// 
//  Revision: 021   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 020  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327, 5606
//  Project:  NeoMode
//  Description:
//		Initial version.
//		Handle user termination of 100% o2 suction events.
//		Use 40% for idle flow and OSC if NEO. 
//		Handle restoring o2% when gas supply is recovered during OSC.
//		Eliminate && !rGasSupply.hasO2BkgndFailed() condition in
//			setGasSupplyStatus_() since rGasSupply.isO2Present() checks it
//			already.  Same for air side.
//
//  Revision: 019  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added updateCycleToCycleCorrection_()  method.
//
//  Revision: 018  By: iv      Date:  18-Mar-1999    DR Number: 5352
//  Project:  ATC
//  Description:
//      In determineO2Mix() set a flag mixResetRequired_ to TRUE whenever
//      a change in O2 Mix is detected at the start of exhalation.
//
//  Revision: 017  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      ATC initial release.
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 016  By:  iv    Date:  18-Jan-1998    DR Number: None
//       Project:  BiLevel (R8027)
//       Description:
//             Bilevel initial version.
//
//  Revision: 015  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 014  By:  iv     Date: 15-Apr-1997    DR Number: DCS 1631
//       Project:  Sigma (R8027)
//       Description:
//			Code was changed to implement this DCS.
//
//  Revision: 013  By:  iv     Date: 03-Apr-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//			Rework per formal code review for BD Schedulers.
//
//  Revision: 012  By:  iv     Date: 21-Mar-1997    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//			Rework per unit test peer review.
//
//  Revision: 011  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1827
//       Project:  Sigma (R8027)
//       Description:
//			Changed FlowSesor::SetO2Percent to rExhFlowSensor.setO2Percent().
//
//  Revision: 010  By:  syw    Date:  19-Feb-1997    DR Number: DCS 1780
//       Project:  Sigma (840)
//       Description:
//       	Change FlowSensor::SetMixFactor() to FlowSensor::SetO2Percent().
//
//  Revision: 009  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per formal code review.
//
//  Revision: 008  By:  iv    Date:  06-Nov-1996    DR Number: DCS 1338
//       Project:  Sigma (R8027)
//       Description:
//             Added alarm events BdAlarmId::BDALARM_100_PERCENT_O2_REQUEST
//             and BdAlarmId::BDALARM_NOT_100_PERCENT_O2_REQUEST.
//             Deleted BdAlarmId::BDALARM_O2_PERCENT_SETTING_CHANGE.
//
//  Revision: 007  By:  iv    Date:  04-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Changes for unit test.
//
//  Revision: 006 By:  iv   Date:   27-Aug-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//				Added a check for air and o2 background test results in
//              setGasSupplyStatus_().
//
//  Revision: 005 By:  iv   Date:   04-Mar-1996    DR Number: DCS 600 
//       Project:  Sigma (R8027)
//       Description:
//				Added a one point calibration to the Fio2 sensor.
//
//  Revision: 004 By:  iv   Date:   01-Mar-1996    DR Number: DCS 764 
//       Project:  Sigma (R8027)
//       Description:
//             Eliminate class pre condition
//	               (isO2Available_ || isAirAvailable_)
//             in method determineO2Mix(). This precondition is true on power
//             up when no gas supply is available.
//
//  Revision: 003 By:  iv   Date:   15-Feb-1996    DR Number: DCS 674 
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem for reporting
//             BD alarms and with NetworkApp for sending ventilator status information
//             to the GUI. 
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "O2Mixture.hh"
#include "MathUtilities.hh"
#include "BreathMiscRefs.hh"
#include "VentObjectRefs.hh"
#include "TimersRefs.hh"
#include "MainSensorRefs.hh"

//@ Usage-Classes
#include "PhasedInContextHandle.hh"
#include "BreathPhaseScheduler.hh"
#include "GasSupply.hh"
#include "IntervalTimer.hh"
#include "FlowSensor.hh"
#include "ExhFlowSensor.h"
#include "BreathPhase.hh"
#include "BreathRecord.hh"
#include "BdSystemStateHandler.hh"
#include "BdAlarms.hh"
#include "Fio2Monitor.hh"
#include "PhaseRefs.hh"
#include "PatientCctTypeValue.hh"
#include "ModeValue.hh"
#include "BdSystemState.hh"
#include "SettingConstants.hh"
#include "SoftwareOptions.hh"
//@ End-Usage

//@ Code...

// constants for use by class methods
//@ Constant: HUNDRED_PERCENT_DURATION_MS
//two minutes of 100% O2
const Int32 HUNDRED_PERCENT_DURATION_MS = 2*60*1000;

// constants for use by class methods
//@ Constant: MIX_INTERVAL_DURATION_MS
// compute O2 mix interval every second when in NCPAP
const Int32 MIX_INTERVAL_DURATION_MS = 1000;

//@ Constant: MIX_GAIN
// The gain to sum the mix errors.
static const Real32 MIX_GAIN = 95.0F;

//@ Constant: MAX_ERROR_BAND
// The max allowed error band
static const Real32 MAX_ERROR_BAND = 3.0F;

//@ Constant: NEO_MIX
// o2% to be delivered during idle and OSC for NEO Circuit type
static const Real32 NEO_MIX = 40.0 ;

// Initialization of static members
Boolean O2Mixture::Is100PercentO2_ = FALSE;
Boolean O2Mixture::IsNeoCalO2Sensor_ = FALSE;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: O2Mixture()  [Constructor]
//
//@ Interface-Description
//	The constructor takes an IntervalTimer reference as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The private data member rO2Timer_ is initialized with the argument
//	passes to the constructor. The breath phase array is initialized to
//	zero elements with NULL pointer. The target and applied o2 percent
//	are both initialized (arbitrarily) to indicate 21% mix.
//  The Boolean mixResetRequired_ is initialized to FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

O2Mixture::O2Mixture(IntervalTimer& rO2Timer, IntervalTimer& rMixTimer) 
: TimerTarget(), rO2Timer_(rO2Timer), rNCPAPO2Timer_(rMixTimer)
{
// $[TI1]
	CALL_TRACE("O2Mixture::O2Mixture(IntervalTimer& rTimer)");
	phaseIndex_ = 0;
	pBreathPhase_[phaseIndex_] = NULL;
	targetO2Percent_ = 21.0F;
	appliedO2Percent_ = 21.0F;
	mixResetRequired_ = FALSE;
	rNCPAPO2Timer_.setTargetTime(MIX_INTERVAL_DURATION_MS);
	rNCPAPO2Timer_.restart();
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~O2Mixture()  [Destructor]
//
//@ Interface-Description
// Destructor
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

O2Mixture::~O2Mixture(void)
{
	CALL_TRACE("~O2Mixture()");
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Request100PercentO2
//
//@ Interface-Description
//	This method is called when a user request for 100% O2 is detected. The
//	request is limited for HUNDRED_PERCENT_DURATION_MS.
//	The request is honored pending availability of oxygen.  When a request
//	is honored, the BdAlarms object is notified of the change in O2, which
//	is then sent to the Alarm-Analysis subsystem.  When oxygen is
//	available, it is delivered for a preset amount of time.  The method takes
//	a EventId argument and a Boolean eventStatus argument, it returns
//	an event status. The actual start and end of the 100% O2 mix
//	delivery is synchronized with the beginning of a new breath phase. The
//	mix error sum is reset in all breath phases.  The flag isCalO2Sensor_ is
//  determined to be true if neo circuit is in use and the determined targeted
//  percentage of O2 is 100 percent.  This flag is used to control the on fly the 
//  calibration of the O2 sensor to accomplish that, the O2 mix at the start of 
//  the manuever is recorded. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	$[01244] $[01245] $[04282] If 100% is available, the method returns a ACTIVE
//	status, otherwise it returns a REJECTED status. If successful, the
//	IntervalTimer member rO2Timer_ is set to HUNDRED_PERCENT_DURATION_MS,
//	the private data member Is100PercentO2_ is set to TRUE, rBdAlarms::postBdAlarm()
//  is invoked with the change in O2 identifier, and the method to reset
//	the error sums in the breath phases is called. The flag isCalO2Sensor_ is
//  determined to be true if neo circuit is in use and the determined targeted
//  percentage of O2 is 100 percent.  This flag is used to control the on fly the 
//  calibration of the O2 sensor to accomplish that, the O2 mix at the start of 
//  the manuever is recorded in mixBeforeCalibration_.  
//---------------------------------------------------------------------
//@ PreCondition
//	none 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

EventData::EventStatus
O2Mixture::Request100PercentO2(const EventData::EventId id,
							   const Boolean eventStatus)
{
	CALL_TRACE("O2Mixture::Request100PercentO2(EventData::EventId id,\
								Boolean eventStatus)");

	EventData::EventStatus status;
	const DiscreteValue  PATIENT_CCT_TYPE =
	PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	if (RO2Mixture.isO2Available_)
	{	// $[TI1]

		if (eventStatus == TRUE)
		{	// $[TI1.1]
			O2Mixture::Is100PercentO2_ = TRUE;

			// GUI receives ACTIVE status when the method returns.
			status = EventData::ACTIVE;

			// the following causes the timer to count HUNDRED_PERCENT_DURATION_MS.
			// If already counting - it resets the count to
			//	HUNDRED_PERCENT_DURATION_MS
			RO2Mixture.rO2Timer_.setTargetTime(HUNDRED_PERCENT_DURATION_MS);

			// starting the timer has no effect if already started
			RO2Mixture.rO2Timer_.restart();

			// store the current O2 mix
			RO2Mixture.mixBeforeCalibration_ = RO2Mixture.appliedO2Percent_;

			// reset the error sum for the o2 mix - in all breath phase
			RO2Mixture.resetMixErrorSum_();

			// Notify Alarms of the change in requested O2
			RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_100_PERCENT_O2_REQUEST);

			// Update the NovRam object BdSystemState with the new request
			BdSystemStateHandler::UpdateO2RequestStatus(TRUE);
			if (id == EventData::CALIBRATE_O2)
			{
				BdSystemStateHandler::UpdateCalibrateRequestStatus(TRUE);
				RO2Mixture.isCalO2Sensor_ = TRUE;
			}
			else
			{
				BdSystemStateHandler::UpdateCalibrateRequestStatus(FALSE);
			}

			// apply 100% immediately if disconnected or OSC for Neo circuit 
			if ((RO2Mixture.isDisconnected_ || RO2Mixture.isOccluded_) &&
				PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
			{
				// $[TI3]
				RO2Mixture.targetO2Percent_ = RO2Mixture.neonatalMixHandle_() ; 
				RO2Mixture.appliedO2Percent_ = RO2Mixture.targetO2Percent_;
			}
		}
		else
		{	// $[TI1.2]
			O2Mixture::Is100PercentO2_ = FALSE;

			// GUI receives CANCEL status when the method returns.
			status = EventData::CANCEL;

			// stop the timer
			RO2Mixture.rO2Timer_.stop();

			// Notify Alarms of the change in requested O2
			RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NOT_100_PERCENT_O2_REQUEST);

			// Update the NovRam object BdSystemState with the new request
			BdSystemStateHandler::UpdateO2RequestStatus(FALSE);
			BdSystemStateHandler::UpdateCalibrateRequestStatus(FALSE);

			// restore back to the NEO_MIX if in (disconnect or OSC) and Neo circuit immediately
			if ((RO2Mixture.isDisconnected_ || RO2Mixture.isOccluded_) &&
				PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
			{
				// $[TI5]
				RO2Mixture.targetO2Percent_ = RO2Mixture.neonatalMixHandle_() ; 
				RO2Mixture.appliedO2Percent_ = RO2Mixture.targetO2Percent_;
			}
			// $[TI6]
		}
	}
	else
	{
		// $[TI2]
		// no change is made, request failed; Alarms is not notified
		// GUI receives REJECTED status when the method returns.
		status = EventData::REJECTED;
	}

	return(status);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineO2Mix
//
//@ Interface-Description
// $[04261] $[04282] $[04290] $[04327] 
//	This method is called when a new phase is determined so that the new O2 mix
//	value can be applied for that phase.  The method takes a scheduler
//	reference, and a BreathPhase reference as arguments and has no return
//	value.  The target O2 percent is determined based on the current scheduler
//	id and the request status for 100 percent O2. Once the target O2 mix is
//	determined, the applied O2 mix (the actual mix that is set to be delivered)
//	is set to either the target or to a value calculated based on the mix error
//	sums stored in the current breath phase. The mix error sums are managed by
//	this method, the sums are reset when a change in target mix is detected during
//  inspiration or during exhalation. At the end, the mix factor in the FlowSensor
//  class gets updated. The fio2 monitor is notified to set its mix target.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The order in which conditions are checked in this routine directly derived
//	from the priority a certain condition has in determining the O2 mix.
//	The first condition to be checked is the 100% O2 request which overrides
//	any schedulers requirements for O2 mix. Next in order of importance are:
//	disconnect and stanby schedulers, occlusion scheduler, apnea scheduler, and
//	the set scheduler. The target O2 percent and the applied O2 percent are both
//	implemented as private data members of this class. Two private methods are used here
//	one to reset the mix error sums and the other to calculate the breath mix.
//	A static method in the FlowSensor class is called to update the O2 mix factor.
//---------------------------------------------------------------------
//@ PreCondition
//	- The IDs of the schedulers that are expected to collaborate with this
//	method are checked. The variable isO2Available_ is asserted to be true.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
O2Mixture::determineO2Mix(const BreathPhaseScheduler& scheduler, BreathPhase& rPhase)
{
	CALL_TRACE("O2Mixture::determineO2Mix(const BreathPhaseScheduler& scheduler, \
																BreathPhase& rPhase)");
	Real32 o2Mix;

	SchedulerId::SchedulerIdValue schedulerId = scheduler.getId();

	const DiscreteValue  PATIENT_CCT_TYPE =
	PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	if ((schedulerId == SchedulerId::DISCONNECT) ||
		(schedulerId == SchedulerId::STANDBY))
	{
		// $[TI2]
		isDisconnected_ = TRUE;
		isOccluded_ = FALSE;

		switch (PATIENT_CCT_TYPE)
		{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :
			// $[TI6]
			// since disconnect mix requires oxygen, check if it is available:
			if (isO2Available_)
			{
				// $[TI2.1] $[04214]
				targetO2Percent_ = 100.0F;
			}
			else //only air is available
			{
				// $[TI2.2]
				targetO2Percent_ = 21.0F;
			}
			break ;

		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			// $[TI7] $[04214]
			targetO2Percent_ = neonatalMixHandle_() ;
			break ;

		default :
			// unexpected circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
			break;
		}

		appliedO2Percent_ = targetO2Percent_;
	}
	else if (schedulerId == SchedulerId::OCCLUSION)
	{
		// $[TI3]
		isDisconnected_ = FALSE;
		isOccluded_ = TRUE ;

		switch (PATIENT_CCT_TYPE)
		{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :
			// $[TI11]
			// since occlusion mix requires oxygen, check if it is available:
			if (isO2Available_)
			{
				// $[TI3.1] $[04204]
				targetO2Percent_ = 100.0F;
			}
			else //only air is available
			{
				// $[TI3.2]
				targetO2Percent_ = 21.0F;
			}
			break ;

		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			// $[TI12] $[04204]
			targetO2Percent_ = neonatalMixHandle_() ;
			break ;

		default :
			// unexpected circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
			break;
		}

		appliedO2Percent_ = targetO2Percent_;
	}
	else if (O2Mixture::Is100PercentO2_)
	{
		// $[TI1]
		CLASS_PRE_CONDITION(isO2Available_);

		// if neonatal make the targeted 02 be plus 20 percent of O2 setting
		if (isPlus20Enabled_() && !O2Mixture::IsNeoCalO2Sensor_)
		{
			targetO2Percent_ = neonatalMixHandle_();
			appliedO2Percent_ = targetO2Percent_;
		}
		else
		{
			targetO2Percent_ = 100.0F;
			appliedO2Percent_ = 100.0F;
		}
	}
	else if (schedulerId >= SchedulerId::FIRST_BREATHING_SCHEDULER &&
			 schedulerId <= SchedulerId::LAST_BREATHING_SCHEDULER)
	{
		// $[TI4]
		if (schedulerId == SchedulerId::APNEA)
		{
			// $[TI4.1]
			o2Mix = PhasedInContextHandle::
					GetBoundedValue(SettingId::APNEA_O2_PERCENT).value;
		}
		else
		{
			// $[TI4.2]
			o2Mix = PhasedInContextHandle::
					GetBoundedValue(SettingId::OXYGEN_PERCENT).value;
		}

		isDisconnected_ = FALSE;
		isOccluded_ = FALSE ;

		if (!isO2Available_)
		{
			// $[TI4.3]
			// note that air may not be available, in which case SVO conditions
			// will result on next cycle.
			o2Mix = 21.0F;
		}
		else if (!isAirAvailable_)
		{
			// $[TI4.4]
			o2Mix = 100.0F;
		}
		// $[TI4.5]
		if (IsEquivalent(targetO2Percent_, o2Mix, ::HUNDREDTHS) &&
			!mixResetRequired_)
		{
			// $[TI4.6]
			if (rPhase.getPhaseType() == BreathPhaseType::INSPIRATION)
			{
				// $[TI4.6.1]
				calculateBreathMix_(rPhase);
			}
			// $[TI4.6.2]
		}
		else // every start of breath, if the O2 mix is changed or if the
		// target mix is 21.0 or 100.0, then the applied o2 and the error
		// sums are reset.
		{
			// $[TI4.7]
			if (rPhase.getPhaseType() == BreathPhaseType::EXHALATION)
			{
				// $[TI4.7.1]
				// this will prevent calling calculateBreathMix_() on the next
				// inspiration so that the O2 volume calculation will be updated.
				mixResetRequired_ = TRUE;
			}
			else
			{
				// $[TI4.7.2]
				mixResetRequired_ = FALSE;
			}
			targetO2Percent_ = o2Mix;
			appliedO2Percent_ = o2Mix;
			resetMixErrorSum_();
		}
	}

	// set the mix factor for the flow sensor
	RExhFlowSensor.setO2Percent(appliedO2Percent_);

	// update the Fio2 monitor mix
	Fio2Monitor::SetTargetMix(targetO2Percent_);

	// if the target O2 is no longer 100 percent
	// disable the calibration process
	if (100.0f != targetO2Percent_)
	{
		isCalO2Sensor_ = FALSE;
	}


}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetGasSupplyStatus
//
//@ Interface-Description
// An interface method to provide a call back capability for a client that
// need to notify a gas supply change status. The method takes a reference to
// a gas supply object and has no return value. The method can handle only one
// client at a time.
// This method is called whenever a change in gas supply availability is detected.
//---------------------------------------------------------------------
//@ Implementation-Description
// A call to the member function that sets the gas supply status is made
// by the o2 mixture object
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void    
O2Mixture::SetGasSupplyStatus(const GasSupply& rGasSupply)
{
	// $[TI1]
	CALL_TRACE("O2Mixture::SetGasSupplyStatus(const GasSupply& rGasSupply)");

	RO2Mixture.setGasSupplyStatus_(rGasSupply);
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
//	This method overides the pure virtual method of the base class TimerTarget.
//	It takes an interval timer reference as an argument and has no return
//	value. The method initiates a 100% O2 completion event notification to the
//	user, resets the 100% O2 request - state variable, and notifies BdAlarms of
//	the change in O2, which is passed on to Alarm-Anaylsis.  Once the
//	Alarm-Analysis sub-system is notified, the 100 percent request staus is set
//	to false in the NOV RAM. There is no need to update the applied O2 percent
//	since it gets (for most schedulers) updated at the start of the next breath
//	phase. The disconnect scheduler is an exception, however, its O2 mix is
//	identical to the user request O2 mix which is 100%, therefore no extra
//	action is required. This method signals the Fio2 monitor to perform one
//	point calibration, passing it the O2 mix value before the 100% O2 manuever
//	started.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01244] The object referenced to by the member rO2Timer_ is instructed to
// stop counting. The state variable Is100PercentO2_ is set to FALSE, the
// status of the user event request for O2 is set to COMPLETE and and
// BdAlarms.postBdAlarm() is invoked with the O2 setting change id. The
// BdSystemStateHandler method UpdateO2RequestStatus() is used to set the
// request status.  The method to calibrate the O2 sensor is called for the
// object rFio2Monitor.  If NCPAP/NIV/NEO condition exits there maybe no
// inhalations/exhalations to trigger the computation of the O2 mix by the 
// method determineO2Mix.  In this case a one second interval is used to 
// force the computation.
//---------------------------------------------------------------------
//@ PreCondition
//	The argument passed is checked to be a reference to the O2 or Neonate CPAP
//  timer.
//	The state variable Is100PercentO2_ is checked to be TRUE if we are
// not in the NCPAP/NIV/NEO(Ta == OFF) condition.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
O2Mixture::timeUpHappened(const IntervalTimer& timer)
{
	CALL_TRACE("O2Mixture::timeUpHappened(const IntervalTimer& timer)");

	CLASS_PRE_CONDITION(&timer == &rO2Timer_ || &timer == &rNCPAPO2Timer_ );

	const DiscreteValue  PATIENT_CCT_TYPE =
	PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	// during NEO CPAP no inspirations/expirations maytake place so reset
	// tartget O2 percentage
	if (&timer == &rNCPAPO2Timer_)
	{
		const Int32 apneaIntervalSetValue =
		 (Int32)(PhasedInContextHandle::GetBoundedValue(SettingId::APNEA_INTERVAL).value);

		// if neo, NIV and CPAP (Ta == OFF) we maynot get breaths to trigger 
		// calculation of the O2 mix 
		if (isPlus20Enabled_() && apneaIntervalSetValue == DEFINED_UPPER_ALARM_LIMIT_OFF)
		{
			// A pointer to the current breath phase
			BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
			determineO2Mix(BreathPhaseScheduler::GetCurrentScheduler(), 
						   (BreathPhase&)pBreathPhase);
		}
		rNCPAPO2Timer_.restart();
		return;
	}

	CLASS_PRE_CONDITION(O2Mixture::Is100PercentO2_);

	rO2Timer_.stop();
	O2Mixture::Is100PercentO2_ = FALSE;

	// notify user that 100% O2 event is complete
	RO2Request.setEventStatus(EventData::COMPLETE);

	// Notify Alarms of the change in requested O2
	RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NOT_100_PERCENT_O2_REQUEST);

	// Update the NovRam object BdSystemState with the status change
	BdSystemStateHandler::UpdateO2RequestStatus(FALSE);
	BdSystemStateHandler::UpdateCalibrateRequestStatus(FALSE);

	// Perform one point calibration for the O2 Sensor only during 100 percent 
	// 02 delivery
	if (isCalO2Sensor_)
	{
		RFio2Monitor.calibrateFio2Sensor(mixBeforeCalibration_);
	}

	// restore back to the NEO_MIX if in (disconnect or OSC) and Neo circuit 
	// immediately
	if ((isDisconnected_ || isOccluded_) &&
		PATIENT_CCT_TYPE == PatientCctTypeValue::NEONATAL_CIRCUIT)
	{
		// $[TI1]
		targetO2Percent_ = neonatalMixHandle_() ;   
		appliedO2Percent_ = targetO2Percent_;
	}
	// $[TI2]


}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateO2Fraction
//
//@ Interface-Description
//	The method takes no arguments and returns the O2 fraction as a real32.
//	The fraction of O2 as part of the total mix is calculated so that the
//	required oxygen flow can be derived and fed to the O2 flow controller.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The O2 fraction is derived from the appliedO2Percent_.
//---------------------------------------------------------------------
//@ PreCondition
//	The appliedO2Percent_ is checked to be within the range of 21.0 to 100.0 %.
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
Real32
O2Mixture::calculateO2Fraction(Real32 desiredFlow)
{
	// $[TI1]
	CALL_TRACE("O2Mixture::calculateO2Fraction(Real32 desiredFlow)");
	CLASS_PRE_CONDITION((appliedO2Percent_ >= 21.0F) && (appliedO2Percent_ <= 100.0F));

	Real32 appliedO2Mix = appliedO2Percent_;
	BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();

	// $[TC04051]
	if (!IsEquivalent(appliedO2Percent_, 21.0F, ::HUNDREDTHS) &&
		!IsEquivalent(appliedO2Percent_, 100.0F, ::HUNDREDTHS) &&
		pBreathPhase != (BreathPhase*)&RVcvPhase &&
		pBreathPhase != (BreathPhase*)&RApneaVcvPhase)
	{	// $[TI1]
		appliedO2Mix = updateCycleToCycleCorrection_(desiredFlow);
	}	// $[TI2]

	Real32 value = (appliedO2Mix  - 21.0F) / 79.0F ;

	return( value);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerBreathPhaseCallBack
//
//@ Interface-Description
//	The method takes a constant pointer to a breath phase and returns no value.
//	It is used by any breath phase that needs to keep track on the accumulated
//	mix errors.  When a breath phase registers with the O2 mixture object,
//	using this method, a new element to an array of pointers to BreathPhase is
//	added.
//---------------------------------------------------------------------
//@ Implementation-Description
//	A list of pointers to BreathPhase is maintained as a private data member.
//	This list is always terminated with a NULL pointer. The method adds a new
//	breath phase pointer and increments the NULL index as long as the number of
//	breath phases do not exceed the maximum number of breath phases.
//---------------------------------------------------------------------
//@ PreCondition
//	MAX_NUM_PHASES > phaseIndex_ + 1
//---------------------------------------------------------------------
//@ PostCondition
//	None.
//@ End-Method
//=====================================================================
void 
O2Mixture::registerBreathPhaseCallBack(BreathPhase *const pBreathPhase)
{
	CALL_TRACE("O2Mixture::registerBreathPhaseCallBack(BreathPhase *const pBreathPhase)");

	if (MAX_NUM_PHASES > phaseIndex_ + 1)
	{
		// $[TI1]
		pBreathPhase_[phaseIndex_++] = pBreathPhase;
		pBreathPhase_[phaseIndex_] = NULL;
	}
	else
	{
		// $[TI2]
		CLASS_PRE_CONDITION(MAX_NUM_PHASES > phaseIndex_ + 1);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
O2Mixture::SoftFault(const SoftFaultID  softFaultID,
					 const Uint32       lineNumber,
					 const char*        pFileName,
					 const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, O2MIXTURE, lineNumber,
							pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setGasSupplyStatus_
//
//@ Interface-Description
//	$[04282] $[05058] $[05059] $[05064] $[05065] $[04327] 
//	The method takes a gas supply reference as an argument and has no
//	return value. The method is invoked when the status of the gas supply
//	availability changes; it interogates the gas supply object to determine
//	what sources of gas are available and sets the data members that indicate
//	the supply status. Availabiliy of gas supply is determined by the presence
//  of the gas and by the absence of any background fault related to the gas.
//  The target and applied O2 mixes are set based on what
//	gas source is available. Once the target mix is determined, the mix factor
//	in the FlowSensor class gets updated. When a change in the target O2
//	percent is deleted, a method to reset the mix error sums is called.  The
//	design does not provide for a mechanism to restore the previous O2% during
//	the current breath phase.  It is only when the breath phase completes that
//	the applied O2% is recalculated.  e.g. - if air is lost during the current
//	inspiratory phase, the applied O2 percent is set to 100%.  Once air
//	recovers, during the same phase, the applied O2 percent remains 100% until
//	the end of the current inspiration. The only exception occurs during
//	disconnect, because disconnect phase is not restricted in time as the rest
//	of the breath phases. The 100% O2 timer terminates when O2 is unavailable.
//  The fio2 monitor is notified to set its mix target.
//---------------------------------------------------------------------
//@ Implementation-Description
//	An if/else construct is implemented to check all different combinations of
//	gas supply availability. The private data members isO2Available_ and
//	isAirAvailable_ are set to reflect the current gas supply state. The
//	private member appliedO2Percent_ is set if there is a supply restriction.
//	A static method in the FlowSensor class is called to update the O2 mix factor.
//  The Fio2Monitor static method SetTargetMix() in used to set the new mix.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void    
O2Mixture::setGasSupplyStatus_(const GasSupply& rGasSupply)
{
	CALL_TRACE("O2Mixture::setGasSupplyStatus_(const GasSupply& rGasSupply)");

	Boolean isO2Present = rGasSupply.isO2Present() ;
	Boolean isAirPresent = rGasSupply.isTotalAirPresent() ;

	if (!isO2Present && !isAirPresent)
	{
		// $[TI1]
		// SVO has been declared
		isO2Available_ = FALSE;
		isAirAvailable_ = FALSE;

		if (O2Mixture::Is100PercentO2_)
		{
			// $[TI1.1]
			cancelO2Request_();
		}
		// $[TI1.2]
		// reset the error sum in all breath phases
		resetMixErrorSum_();
	}
	else if (!isO2Present)
	{
		// $[TI2]
		isO2Available_ = FALSE;
		isAirAvailable_ = TRUE;
		if (!IsEquivalent(targetO2Percent_, 21.0F, ::HUNDREDTHS))
		{
			// $[TI2.1]
			targetO2Percent_ = 21.0F;
			appliedO2Percent_ = 21.0F;
			// reset the error sum in all breath phases
			resetMixErrorSum_();
		}
		// $[TI2.2]
		if (O2Mixture::Is100PercentO2_)
		{
			// $[TI2.3]
			cancelO2Request_();
		}
		// $[TI2.4]
	}
	else if (!isAirPresent)
	{
		// $[TI3]

		isO2Available_ = TRUE;
		isAirAvailable_ = FALSE;
		if (!IsEquivalent(targetO2Percent_, 100.0F, ::HUNDREDTHS))
		{
			// $[TI3.1]
			targetO2Percent_ = 100.0F;
			appliedO2Percent_ = 100.0F;
			// reset the error sum in all breath phases
			resetMixErrorSum_();
		}

		const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

		// if no airsupply cancel +20 O2 setting 
		if (isPlus20Enabled_())
		{
			cancelO2Request_();
		}

		// $[TI3.2]
	}
	else // both gas supplies are present
	{
		// $[TI4]
		isO2Available_ = TRUE;
		isAirAvailable_ = TRUE;

		if (isDisconnected_ || isOccluded_)
		{
			// $[TI5]
			const DiscreteValue  PATIENT_CCT_TYPE =
			PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

			switch (PATIENT_CCT_TYPE)
			{
			case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
			case PatientCctTypeValue::ADULT_CIRCUIT :
				// $[TI6]
				targetO2Percent_ = 100.0F;
				appliedO2Percent_ = 100.0F;
				break ;

			case PatientCctTypeValue::NEONATAL_CIRCUIT :
				// $[TI7]
				targetO2Percent_ = neonatalMixHandle_() ;
				appliedO2Percent_ = targetO2Percent_ ;
				break ;

			default :
				// unexpected circuit type value...
				AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE);
				break;
			}   
		}
		// $[TI8]
	}

	// set the mix factor for the flow sensor
	RExhFlowSensor.setO2Percent( appliedO2Percent_);
	// update the Fio2 monitor mix
	Fio2Monitor::SetTargetMix(targetO2Percent_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetMixErrorSum_
//
//@ Interface-Description
//	This methods takes no arguments and has no return value. It uses 
//	the list of pointers to breath phases to set the mix error sums in each
//	registered breath phase. 
//---------------------------------------------------------------------
//@ Implementation-Description
//	The private data member pBreathPhase is used to address each breath phase
//	on the list and to invoke its method to set the error sums to zero.
//---------------------------------------------------------------------
//@ PreCondition
//	pBreathPhase_[0] != NULL and index < MAX_NUM_PHASES
//---------------------------------------------------------------------
//@ PostCondition
//	None
//@ End-Method
//=====================================================================
void
O2Mixture::resetMixErrorSum_(void)
{
	// $[TI1]
	CALL_TRACE("O2Mixture::resetMixErrorSum_(void)");
	CLASS_PRE_CONDITION(pBreathPhase_[0] != NULL);  
	Int16 index = 0;

	while (pBreathPhase_[index] != NULL)
	{
		pBreathPhase_[index++]->setMixErrorSum(0.0F);
	};
	CLASS_ASSERTION(MAX_NUM_PHASES > index);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: calculateBreathMix_
//	
//@ Interface-Description
//	$[04290]
//	The method takes a reference to a breath phase as an argument and has no
//	return value. If the total deliverd volume is zero, or the mix is 21% or
//	100%, the applied o2 mix is set to the target mix. Otherwise, the mix error
//	sum is incremented by the mix error value multiplied by the mix error gain.
//	If the calculated applied O2 percent exceeds the high limit mix or is less
//	than the low limit mix, it is corrected, and the new error sum is adjusted
//	accordingly. The applied O2 percent is adjusted by the mixErrorSum value
//	and the breath phase is called to set the new mixErrorSum.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the total delivered volume is zero, or the targetO2Percent_ is 100 or
//	21, then the mix calculation is not performed.  The delivered volume is
//	received from the breath record and is used for mix calculation. At the end
//	of the mix calculation, the appliedO2Percent_ is adjusted by the
//	mixErrorSum and the breath phase is updated with the new mix error sum.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================
void 
O2Mixture::calculateBreathMix_(BreathPhase& rPhase)
{
	CALL_TRACE("O2Mixture::calculateBreathMix_(void)");

	Real32 breathMix;
	Real32 mixError;
	Real32 mixErrorSum;
	const Real32 airVolume = BreathRecord::CalculateAirVolume();
	const Real32 o2Volume = BreathRecord::CalculateO2Volume();
	const Real32 totalVolume = airVolume + o2Volume;
	// $[04288]
	if (IsEquivalent(totalVolume, 0.00F, ::HUNDREDTHS) ||
		IsEquivalent(targetO2Percent_, 100.0F, ::HUNDREDTHS) ||
		IsEquivalent(targetO2Percent_, 21.0F, ::HUNDREDTHS))
	{
		// $[TI1]
		// do not calculate the applied O2, set it to the target
		appliedO2Percent_ = targetO2Percent_;
	}
	else
	{
		// $[TI2]
		Real32 highLimit = 99.0F ;
		Real32 lowLimit = 22.0F ;
		breathMix = 0.79F*o2Volume/totalVolume + 0.21F;
		mixError = targetO2Percent_/100.0F - breathMix;
		mixErrorSum = rPhase.getMixErrorSum();

		mixErrorSum += MIX_GAIN * mixError;
		// $[04287]
		if (mixErrorSum > MAX_ERROR_BAND)
		{
			// $[TI2.1]
			mixErrorSum = MAX_ERROR_BAND;
		}
		else if (mixErrorSum < -MAX_ERROR_BAND)
		{
			// $[TI2.2]
			mixErrorSum = -MAX_ERROR_BAND;
		}
		// $[TI2.3]
		appliedO2Percent_ = targetO2Percent_ + mixErrorSum;

		// $[04280]
		if (appliedO2Percent_ < lowLimit)
		{
			// $[TI2.4]
			mixErrorSum = lowLimit - targetO2Percent_; 
			appliedO2Percent_ = lowLimit ;
		}
		else if (appliedO2Percent_ > highLimit)
		{
			// $[TI2.5]
			mixErrorSum = highLimit - targetO2Percent_; 
			appliedO2Percent_ = highLimit;
		}
		// $[TI2.6]
		rPhase.setMixErrorSum(mixErrorSum);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateCycleToCycleCorrection_
//	
//@ Interface-Description
//	The method takes a desiredFlow as argument and returns
//	corrected O2 percent.
//---------------------------------------------------------------------
//@ Implementation-Description
//	If the total gas flow is less than zero then the delivered mix calculation
//	is not performed otherwise it will be calculated based on the air and o2
//	flow.  The desireded flow is used in mix calculation to determining the
//	integral value.  At the end the corrected O2 mix is adjusted by the
//	mixErrorSum and the mix error.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================

Real32
O2Mixture::updateCycleToCycleCorrection_(Real32 desiredFlow)
{
	CALL_TRACE("O2Mixture::updateCycleToCycleCorrection_(Real32 desiredFlow)");
	UNUSED_SYMBOL(desiredFlow);	

	Real32 airFlow = RAirFlowSensor.getValue() ;
	Real32 o2Flow = RO2FlowSensor.getValue() ;

	Real32 deliveredMix = appliedO2Percent_;
	const Real32 MIN_FLOW = 0.01 ;

	// $[TC04052]
	if (readyToCorrect_  && airFlow + o2Flow > MIN_FLOW)
	{		// $[TI1.1]
		deliveredMix = (0.21F * airFlow + o2Flow) / (airFlow + o2Flow) * 100.0F;
	}
	else
	{		// $[TI1.2]
		// Ensure that the very first cycle the mixError will be reset
		readyToCorrect_ = TRUE;
	}

	Real32 mixError = appliedO2Percent_ - deliveredMix;

	// $[TC04053]
	Real32 kpMix = 0.5;
	Real32 kiMix = 0.5 ;

	if (airFlow + o2Flow <= 1.0)
	{		// $[TI2.1]
		kiMix = 0.1 ;
	}		// $[TI2.2]

	mixErrorSum_ += mixError * kiMix  ;

	Real32 kpAdjustedAppliedO2Percent = appliedO2Percent_ + kpMix * mixError;

	// $[TC04054]
	Real32 correctedO2Mix = kpAdjustedAppliedO2Percent + mixErrorSum_ ;

	// $[TC04055]
	if (correctedO2Mix > 99.9)
	{		// $[TI3.1]
		mixErrorSum_ = 99.9F - kpAdjustedAppliedO2Percent ;
		correctedO2Mix = 99.9;
	}
	else if (correctedO2Mix < 21.1)
	{		// $[TI3.2]
		mixErrorSum_ = 21.1F - kpAdjustedAppliedO2Percent ;
		correctedO2Mix = 21.1;
	}		// $[TI3.3]

	return(correctedO2Mix);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: neonatalMixHandle_
//	
//@ Interface-Description
//	This method has no arguments and returns the target o2 percent for
//	neonatal.  
//---------------------------------------------------------------------
//@ Implementation-Description
//	If both gases are available, use NEO_MIX, else use the gas that is
//	available.
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================
Real32 
O2Mixture::neonatalMixHandle_(void)
{
	CALL_TRACE("O2Mixture::neonatalMixHandle_( void)") ;

	Real32 targetO2Percent = NEO_MIX;
	Real32 O2setting;

    const SchedulerId::SchedulerIdValue schedulerId = 
		BreathPhaseScheduler::GetCurrentScheduler().getId();

	if (O2Mixture::Is100PercentO2_)
	{
		switch (schedulerId)
		{
		// all non breathing modes use neo natal mix of 40% O2
		case SchedulerId::DISCONNECT:
		case SchedulerId::OCCLUSION:
		case SchedulerId::STANDBY:
		case SchedulerId::SAFETY_PCV:
		case SchedulerId::SVO:
		case SchedulerId::POWER_UP:
			O2setting = NEO_MIX;
			break;
		case SchedulerId::APNEA:
			O2setting = PhasedInContextHandle::
						GetBoundedValue(SettingId::APNEA_O2_PERCENT).value;
			break;
		default:
			O2setting = PhasedInContextHandle::
						GetBoundedValue(SettingId::OXYGEN_PERCENT).value;
			break;
		}

		// $[LC01006]
		// if neo natal circuit in use, use the O2 setting +20 percent
		if (isPlus20Enabled_())
		{
			targetO2Percent = O2setting + 20;

			// limit targetO2Percent to 100 percent 
			// calibration is being forced set target to 100 %
			if ( (targetO2Percent > 100.0F) || (O2Mixture::IsNeoCalO2Sensor_) )
			{
				targetO2Percent = 100.0F;
			}
		}
		else
		{
			targetO2Percent = 100.0F;
		}

		// if no air then terminate O2 request unless 100
		if (!isAirAvailable_ && targetO2Percent != 100.0F)
		{
			cancelO2Request_();
		}

	}
	// $[TI5]

	if (!isO2Available_)
	{
		// $[TI1]
		// note that air may not be available, in which case SVO conditions
		// will result on next cycle.
		targetO2Percent = 21.0F;
	}
	else if (!isAirAvailable_)
	{
		// $[TI2]
		targetO2Percent = 100.0F;
	}
	// $[TI3]
	return( targetO2Percent) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: cancelO2Request_
//	
//@ Interface-Description
//	This method has no arguments and returns nothing   
//---------------------------------------------------------------------
//@ Implementation-Description
//	Cancels the O2 request and Calibration O2 requests. Notify user about 
// premature termination of 100% O2, Notify Alarms of the change in requested O2,
// Update the NovRam object BdSystemState with the status change and
// stop the O2 timer
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================
// 
void
O2Mixture::cancelO2Request_(void)

{
	O2Mixture::Is100PercentO2_ = FALSE;

	// notify user about premature termination of 100% O2
	RO2Request.setEventStatus(EventData::CANCEL);

	// notify user about premature termination of CAL O2
	RCalO2Request.setEventStatus(EventData::CANCEL);

	// Notify Alarms of the change in requested O2
	RBdAlarms.postBdAlarm (BdAlarmId::BDALARM_NOT_100_PERCENT_O2_REQUEST);

	// Update the NovRam object BdSystemState with the status change
	BdSystemStateHandler::UpdateO2RequestStatus(FALSE);
	BdSystemStateHandler::UpdateCalibrateRequestStatus(FALSE);

	// stop the O2 timer
	rO2Timer_.stop();

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isPlus20Enabled_
//	
//@ Interface-Description
//	This method returns TRUE if NEO_MODE_UPDATE software ooption is 
//  enabled and the ciruit type is NEONATAL_CIRCUIT
//---------------------------------------------------------------------
//@ Implementation-Description
// none
//---------------------------------------------------------------------
//@ PreCondition
// none
//---------------------------------------------------------------------
//@ PostCondition
// none
//---------------------------------------------------------------------
//@ End-Method
//=====================================================================
// 
Boolean
O2Mixture::isPlus20Enabled_(void)

{
	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
	PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE);

	// NeoMode Update including CPAP and +20% O2
	return(SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_UPDATE) &&
		   (PatientCctTypeValue::NEONATAL_CIRCUIT == PATIENT_CCT_TYPE_VALUE));

}

