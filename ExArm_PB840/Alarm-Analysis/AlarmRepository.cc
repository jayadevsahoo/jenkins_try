#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Type: AlarmRepository - Holds Alarm status info used by GUI subsystem.
//---------------------------------------------------------------------
//@ Interface-Description
//  AlarmRepository is designed to hold the current Alarm urgencies
//  used by the GUI subsystem. Specifically, the AlarmSlider class
//  uses the urgency information retrieved from this class to display
//  the sliders and limit buttons in the color appropriate to the 
//  alarm's urgency.
//---------------------------------------------------------------------
//@ Rationale
//  The GUI subsystem requires current urgency information for alarms.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//      none
//---------------------------------------------------------------------
//@ Invariants
//      none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmRepository.ccv   25.0.4.0   19 Nov 2013 13:51:16   pvcs  $
//
//@ Modification-Log
//  
//   Revision 001   By:   gdc      Date: 27-FEB-1998      DR Number:  5017
//      Project:   Color
//      Description:
//   Initial version to supply alarm state information to Alarm Setup
//   Subscreen Slider Buttons.
//   
//=====================================================================
#include "Sigma.hh"
#include "AlarmRepository.hh"
#include "Alarm.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

static Alarm::Urgency AlarmArray_[MAX_NUM_ALARMS];

//=====================================================================
//
//      Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize
//
//@ Interface-Description
//   Initialization for AlarmRepository class.
//---------------------------------------------------------------------
//@ Implementation-Description
//   Initialize static data for AlarmRepository class.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmRepository::Initialize(void)
{
  CALL_TRACE("Initialize(void)");

#if defined(SIGMA_GUI_CPU) || defined(SIGMA_DEVELOPMENT)
	// Initialize data.

	for (Uint32 ii = 0; ii < MAX_NUM_ALARMS; ii++)
	{
		AlarmArray_[ii] = Alarm::NORMAL_URGENCY;
	}
	// $[TI1]
#endif
}


#if defined(SIGMA_GUI_CPU) || defined(SIGMA_DEVELOPMENT)
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetStatus
//
//@ Interface-Description
//   Return the urgency of the specified alarmName.
// >Von
//   alarmName                   Enumerated name of the Alarm.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   (alarmName > ALARM_NAME_NULL) && (alarmName < MAX_NUM_ALARMS)
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

Alarm::Urgency
AlarmRepository::GetStatus(AlarmName alarmName)
{
	CALL_TRACE("GetStatus(AlarmName)");

	CLASS_PRE_CONDITION((alarmName > ALARM_NAME_NULL) && 
		(alarmName < MAX_NUM_ALARMS));

	return AlarmArray_[alarmName];
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetStatus
//
//@ Interface-Description
//   Set the urgency of the specified alarmName to the specified 
//	 urgency.
// >Von
//   alarmName                   Enumerated name of the Alarm.
//   urgency                     Enumerated urgency of the alarm.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//   (alarmName > ALARM_NAME_NULL) && (alarmName < MAX_NUM_ALARMS)
//   (urgency > URGENCY_TYPE_NULL) && (urgency < MAX_NUM_URGENCIES)
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================

void
AlarmRepository::SetStatus(AlarmName alarmName, Alarm::Urgency urgency)
{
	CALL_TRACE("SetStatus(AlarmName, Alarm::Urgency)");

	CLASS_PRE_CONDITION((alarmName > ALARM_NAME_NULL) && 
		(alarmName < MAX_NUM_ALARMS));
	CLASS_PRE_CONDITION((urgency > Alarm::URGENCY_TYPE_NULL) && 
		(urgency < Alarm::MAX_NUM_URGENCIES));

	AlarmArray_[alarmName] = urgency;
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition   
//   none
//@ End-Method
//=====================================================================

void
AlarmRepository::SoftFault(const SoftFaultID  softFaultID,
                           const Uint32       lineNumber,
                           const char*        pFileName,
                           const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::ALARMREPOSITORY, lineNumber,  
                          pFileName, pPredicate);
}


//=====================================================================
//
//      Private Methods...
//
//=====================================================================

#endif

