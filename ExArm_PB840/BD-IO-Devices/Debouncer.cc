#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Debouncer - implementation of a debounced signal.
//---------------------------------------------------------------------
//@ Interface-Description
//  Methods are provided to update the debounced objects value each cycle,
//	to get the debounced value, and whether debounced object is reset or not.
//---------------------------------------------------------------------
//@ Rationale
//	To encapsulate a debounce signal.
//---------------------------------------------------------------------
//@ Implementation-Description
//	A debounced object's value needs to be updated every cycle.  When the
//	number of debounced cycles have elapsed, the object's debounced value
//	is updated if there is a change between the current value and the
//	debounced value.  If an update occurs, the object's cycle counter is
//	resetted and the process above repeats itself.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/BD-IO-Devices/vcssrc/Debouncer.ccv   10.7   08/17/07 09:29:04   pvcs  
//
//@ Modification-Log
//
//  Revision: 003   By: gdc    Date: 26-Jan-2009    SCR Number: 6407
//  Project:  840S
//  Description:
//      Modified to bring source into strict ARM compliance.
//
//  Revision: 002  By:  syw    Date:  27-Oct-1999    DR Number: 5556
//       Project:  840
//       Description:
//			Added delayCounts_ and methods to support delaying of debouncing.
//
//  Revision: 001  By:  syw    Date:  28-Jul-1999    DR Number: 5416
//       Project:  840
//       Description:
//             Initial version.
//
//=====================================================================

#include "Debouncer.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Debouncer()  
//
//@ Interface-Description
//		Constructor.  This method has the number of debounce cycles and the
//		initial value of the debounced object as arguments.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Initialize data members to their appropriate values.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Debouncer::Debouncer( const Uint32 debounceCounts, const Uint32 initValue, const Int32 delayCounts)
{
	CALL_TRACE("Debouncer::Debouncer( const Uint32 debounceCounts, \
				const Uint32 initValue, const Int32 delayCounts)") ;

	// $[TI1]
	debounceCounts_ = debounceCounts ;
	counts_ = debounceCounts ;
    currValue_ = initValue ;
    debouncedValue_ = initValue ;
    delayCounts_ = delayCounts ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~Debouncer()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Debouncer::~Debouncer(void)
{
	CALL_TRACE("Debouncer::~Debouncer(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isReset()  
//
//@ Interface-Description
//		This method has no arguments and reurns whether the debounced
//		object is in a reset state (it is ready for update).
//---------------------------------------------------------------------
//@ Implementation-Description
//		The counts_ are incremented, if counts_ meets or exceeds debounceCounts_
//		and the debouncedValue_ is different than currValue_, then a reset
//		is set.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Boolean
Debouncer::isReset( void)
{
	CALL_TRACE("Debouncer::isReset( void)") ;
	
	Boolean reset = FALSE ;

	counts_++ ;
	delayCounts_-- ;

	if (counts_ >= debounceCounts_ && delayCounts_ < 0)
	{
		// $[TI1]
		if (debouncedValue_ != currValue_)
		{
			// $[TI2]
			reset = TRUE ;
			debouncedValue_ = currValue_ ;
			delayCounts_ = 0 ;
			counts_ = 0 ;
		}
		// $[TI3]
	}
	// $[TI4]
	
	return( reset) ;
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
Debouncer::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BD_IO_DEVICES, DEBOUNCER,
                          lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================
