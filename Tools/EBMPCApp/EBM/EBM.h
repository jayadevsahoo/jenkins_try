
// EBM.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CEBMApp:
// See EBM.cpp for the implementation of this class
//

class CEBMApp : public CWinAppEx
{
    ULONG_PTR   GdiPlusToken_;
public:
	CEBMApp();

    bool    isClosing;

// Overrides
	public:
	virtual BOOL InitInstance();
    virtual int ExitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CEBMApp theApp;