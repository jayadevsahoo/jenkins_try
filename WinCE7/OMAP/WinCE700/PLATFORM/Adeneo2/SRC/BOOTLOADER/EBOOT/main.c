// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File:  main.c
//
//  This file implements main bootloader functions called from blcommon
//  library.
//
#include "bsp.h"
#include <eboot.h>
#include "sdk_i2c.h"
#include "sdk_gpio.h"
#include "oal_i2c.h"
#include "kitl_cfg.h"
#include "boot_cfg.h"
#include "oal_alloc.h"
#include "ceddkex.h"

#include "bsp_cfg.h"
#include "bsp_padcfg.h"
#include "tps659xx.h"
#include "omap_cpuver.h"

#include <omap_mcspi_regs.h>
#include <omap3530_base_regs.h>

UINT16 DefaultMacAddress[] = DEFAULT_MAC_ADDRESS;

const char NmiVersionString[NMI_VERSION_STRING_SIZE] = {__DATE__};
NMI_VERSION g_ebootVersion;
DWORD g_buttonFlags;
DWORD g_BSPtype;
DWORD g_ErrorCode = 0;

//------------------------------------------------------------------------------
//
//  Global: g_bootCfg
//
//  This global variable is used to save boot configuration. It is read from
//  flash memory or initialized to default values if flash memory doesn't
//  contain valid structure. It can be modified by user in bootloader
//  configuration menu invoked by BLMenu.
//
BOOT_CFG g_bootCfg;

//------------------------------------------------------------------------------
//
//  Global: gDevice_prefix
//
//  This global variable is used to save Device prefix according to CPU family.
//
CHAR  *gDevice_prefix;
const volatile DWORD dwEbootECCtype = (DWORD)-1;
UCHAR g_ecctype;
//------------------------------------------------------------------------------
//
//  Global: g_eboot
//
//  This global variable is used to save information about downloaded regions.
//
EBOOT_CONTEXT g_eboot;


//------------------------------------------------------------------------------
// External Variables
extern DEVICE_IFC_GPIO Omap_Gpio;
extern DEVICE_IFC_GPIO Tps659xx_Gpio;
//------------------------------------------------------------------------------
//
//  Global: g_CPUFamily
//
//  This global variable is used to save information about CPU family: 35x or 37x.
//
volatile UINT32 g_CPUFamily=0;

//------------------------------------------------------------------------------
// External Functions

VOID Launch(UINT32 address);
VOID JumpTo(UINT32 address);
VOID OEMDeinitDebugSerial();
extern BOOL EnableDeviceClocks(UINT devId, BOOL bEnable);
extern BOOL WriteFlashNK(UINT32 address, UINT32 size);

//------------------------------------------------------------------------------
//  Local Functions
#define NUM_VENT_BUTTONS		8
#define DEVICE_ALERT_BIT		7
#define AUDIO_ENABLE_BIT		0
#define BUZZER_DELAY_TIME		500000

#define DOUT_DIO_A0_BIT			14				// EIO0
#define DOUT_DIO_A1_BIT			15				// EIO1
#define DOUT_DIO_A2_BIT			16              // EIO2

#define DOUT_DIO_GATE_BIT		17              // EIO3

#define DOUT_DA_BLK_BIT			107				// Device Alert Block
#define DOUT_DA_BLK_BIT_MASK	(1 << (DOUT_DA_BLK_BIT % 32))

#define DOUT_CS_0_BIT			109             // EIO5
#define DOUT_CS_1_BIT			110             // EIO6
#define DOUT_CS_2_BIT			111             // EIO7
#define DOUT_CS_3_BIT			138             // S3

#define DOUT_OUT_DATA_BIT		20              // EIO8
#define DIN_IN_DATA_BIT			21              // EIO9

#define DOUT_BL_ENA_BIT			19
#define DOUT_BL_ENA_BIT_MASK	(1 << DOUT_BL_ENA_BIT)
#define DOUT_BL_CS_BIT			134				// Backlight control chip select

#define DOUT_DIO_A0_BIT_MASK	(1 << DOUT_DIO_A0_BIT)				// EIO0
#define DOUT_DIO_A1_BIT_MASK    (1 << DOUT_DIO_A1_BIT)				// EIO1
#define DOUT_DIO_A2_BIT_MASK    (1 << DOUT_DIO_A2_BIT)				// EIO2
#define DOUT_DIO_GATE_BIT_MASK  (1 << DOUT_DIO_GATE_BIT)			// EIO3
#define DOUT_CS_0_BIT_MASK      (1 << (DOUT_CS_0_BIT % 32))			// EIO5
#define DOUT_CS_1_BIT_MASK      (1 << (DOUT_CS_1_BIT % 32))			// EIO6
#define DOUT_CS_2_BIT_MASK      (1 << (DOUT_CS_2_BIT % 32))			// EIO7
#define DOUT_CS_3_BIT_MASK      (1 << (DOUT_CS_3_BIT % 32))			// EIO7
#define DOUT_OUT_DATA_BIT_MASK  (1 << DOUT_OUT_DATA_BIT)			// EIO8
#define DIN_IN_DATA_BIT_MASK    (1 << DIN_IN_DATA_BIT)				// EIO9

#define BSP_HT70_BIT			164		// High for HT70, Low for all others
#define BSP_ID0_BIT				186		// 186 for E600
#define BSP_ID1_BIT				10		// 10 for E600		
#define BSP_ID2_BIT				150		// 162 for E600

#define BSP_HT70_BIT_MASK		(1 << (BSP_HT70_BIT % 32))
#define BSP_ID0_BIT_MASK		(1 << (BSP_ID0_BIT % 32))
#define BSP_ID1_BIT_MASK		(1 << (BSP_ID1_BIT % 32))		
#define BSP_ID2_BIT_MASK		(1 << (BSP_ID2_BIT % 32))

#define	AURA_G_ID				7		// 1 1 1
#define AURA_C_ID				6		// 1 1 0
#define E600_UI_ID				5		// 1 0 1
#define E600_BD_ID				4		// 1 0 0
#define HT70_PLUS_ID			3		// 0 1 1
#define	SBC_ID_MASK				0x7
#define BSP_ID1_MASK			0x2
#define BSP_ID2_MASK			0x4

#if BSP_E600_UI_BUILD
const DWORD BSP_BUILD_TYPE		= BSP_E600_UI;

#if BSP_E600_UI_P1_BUILD
#define DVIMC_PUP_GPIO			19
#define DVIRC_PUP_GPIO			20
#else
#define DVIMC_PUP_GPIO			16
#define DVIRC_PUP_GPIO			17
#endif

#elif BSP_E600_BD_BUILD
const DWORD BSP_BUILD_TYPE		= BSP_E600_BD;
#else
const DWORD BSP_BUILD_TYPE		= BSP_HT70;
#endif

#define DOUT_TPS_BL_ENA_BIT		269
	       
#define UP_KEY_PRESSED_AURA			0xFB
#define DOWN_KEY_PRESSED_AURA		0xF7
#define FORCED_MENU_KEY_COMBO_AURA	0xF3
#define UP_KEY_PRESSED_HT70			0xFD
#define DOWN_KEY_PRESSED_HT70		0x7F
#define FORCED_MENU_KEY_COMBO_HT70	0x7D

#define BUTTONS_NOT_PRESSED		0xFF

#define PRODUCTION_BOOT_CFG		0x2F

#define NO_MENU_DELAY			0
#define SHORT_MENU_DELAY		1
#define LONG_MENU_DELAY			2

// Error Codes
#define ETHERNET_LINK_NOT_DETECTED		1
#define BUTTON_READ_FAILED				2
#define SYSTEM_WARM_RESET				4

UINT32 Crc32Table[256] =
{
	0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
	0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
	0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
	0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
	0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
	0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
	0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
	0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
	0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
	0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
	0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
	0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
	0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
	0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
	0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
	0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,

	0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
	0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
	0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
	0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
	0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
	0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
	0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
	0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
	0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
	0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
	0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
	0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
	0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
	0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
	0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
	0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,

	0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
	0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
	0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
	0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
	0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
	0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
	0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
	0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
	0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
	0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
	0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
	0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
	0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
	0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
	0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
	0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,

	0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
	0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
	0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
	0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
	0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
	0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
	0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
	0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
	0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
	0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
	0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
	0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
	0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
	0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
	0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
	0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D,
};

static DWORD ReadVentButtons();
static VOID InitAioDio();
static VOID ClrAioDioPort1Bit(DWORD);
static DWORD ReadVentButtonsI2C();
static VOID SetBackLightI2C(HANDLE hI2C, BYTE Level);

DWORD	UpKey = 0x00;
DWORD	DownKey = 0x00;
DWORD	ForcedMenuKeyCombo = 0x00;
static HANDLE  hKeypadI2C = NULL;

//------------------------------------------------------------------------------

void BSPGpioInit()
{
   BSPInsertGpioDevice(0,&Omap_Gpio,NULL);
   BSPInsertGpioDevice(TRITON_GPIO_PINID_START,&Tps659xx_Gpio,NULL);

}

void main()
{
	UINT32 CpuRevision;

	// Get CPU family
	g_CPUFamily = CPU_FAMILY_OMAP35XX;
	CpuRevision = Get_CPUVersion();
	g_CPUFamily = CPU_FAMILY(CpuRevision);

    EnableDeviceClocks(BSPGetDebugUARTConfig()->dev,TRUE);
    BootloaderMain();
}

//------------------------------------------------------------------------------
//
//  Function:  InitAioDio
//
//	Initialize AIODIO GPIOs to use in EBOOT

static VOID InitAioDio()
{
       
#if BSP_E600

#if BSP_E600_BD_P3_BUILD
	OMAP_GPIO_REGS* pGpio1;
	OMAP_GPIO_REGS* pGpio4;

	// Bank 1 GPIO 0..31
    pGpio1 = OALPAtoUA(OMAP_GPIO1_REGS_PA);
	CLRREG32(&pGpio1->OE, 1 << 18);					// BUZZER_OUT
	SETREG32(&pGpio1->DATAOUT, (1 << 18));			// 1 -> Set buzzer off
	CLRREG32(&pGpio1->OE, 1 << 26);			// DIS_PB_LED, power push button LED
	CLRREG32(&pGpio1->DATAOUT, (1 << 26));	// Low => ON, High => OFF

	// Bank 4 GPIO 96..127
	pGpio4 = OALPAtoUA(OMAP_GPIO4_REGS_PA);
	// Bank 5 GPIO 128..159

	// Bank 4 GPIO 96..127
	OUTREG32(&pGpio4->OE, ~(1 << 116 % 32));		// WD_TIMER_UP
	SETREG32(&pGpio4->DATAOUT, (1 << 116 % 32));
	OUTREG32(&pGpio4->OE, ~(1 << 117 % 32));		// WD_TIMER_SET
	SETREG32(&pGpio4->DATAOUT, (0 << 117 % 32));	// High for short timeout (30ms), low for long (60S)
	OUTREG32(&pGpio4->OE, ~(1 << 118 % 32));		// DEVICE_ALERT LED Control
	SETREG32(&pGpio4->DATAOUT, (1 << 118 % 32));	// 1 -> turn LED off 
#else
#endif

#else
	OMAP_GPIO_REGS* pGpio1;
	OMAP_GPIO_REGS* pGpio4;
	OMAP_GPIO_REGS* pGpio5;
	DWORD MaskPattern;

	// Bank 1 GPIO 0..31
    pGpio1 = OALPAtoUA(OMAP_GPIO1_REGS_PA);
	// Bank 4 GPIO 96..127
	pGpio4 = OALPAtoUA(OMAP_GPIO4_REGS_PA);
	// Bank 5 GPIO 128..159
	pGpio5 = OALPAtoUA(OMAP_GPIO5_REGS_PA);
	SETREG32(&pGpio4->DATAOUT, DOUT_DA_BLK_BIT_MASK);	// Set DA_BLK to disable WDO until App enables it 

	// Set as outputs for these pins on Port 1
	CLRREG32(&pGpio1->OE, DOUT_OUT_DATA_BIT_MASK|DOUT_DIO_GATE_BIT_MASK|DOUT_DIO_A2_BIT_MASK|
							DOUT_DIO_A1_BIT_MASK|DOUT_DIO_A0_BIT_MASK|DOUT_BL_ENA_BIT_MASK);
	SETREG32(&pGpio1->DATAOUT,	DOUT_BL_ENA_BIT_MASK);	// Enable backlight

	// Set as outputs for these pins on Port 4
	CLRREG32(&pGpio4->OE, DOUT_CS_2_BIT_MASK|DOUT_CS_1_BIT_MASK|DOUT_CS_0_BIT_MASK);
	CLRREG32(&pGpio5->OE, DOUT_CS_3_BIT_MASK);

	MaskPattern = DOUT_DIO_A2_BIT_MASK | DOUT_DIO_A1_BIT_MASK | DOUT_DIO_A0_BIT_MASK;

	// Setting the chip select for output 0, Y6
	CLRREG32(&pGpio4->DATAOUT,	DOUT_CS_0_BIT_MASK);
	CLRREG32(&pGpio5->DATAOUT,	DOUT_CS_3_BIT_MASK);
	SETREG32(&pGpio4->DATAOUT,	DOUT_CS_2_BIT_MASK | DOUT_CS_1_BIT_MASK);

	//Beep the buzzer
	MASKREG32(&pGpio1->DATAOUT, MaskPattern, DEVICE_ALERT_BIT << DOUT_DIO_A0_BIT);
    SETREG32(&pGpio1->DATAOUT, DOUT_OUT_DATA_BIT_MASK);
    CLRREG32(&pGpio1->DATAOUT, DOUT_DIO_GATE_BIT_MASK);
	SETREG32(&pGpio1->DATAOUT, DOUT_DIO_GATE_BIT_MASK);

	// Enable Speaker
    MASKREG32(&pGpio1->DATAOUT, MaskPattern, AUDIO_ENABLE_BIT << DOUT_DIO_A0_BIT);
	SETREG32(&pGpio1->DATAOUT, DOUT_OUT_DATA_BIT_MASK);
    CLRREG32(&pGpio1->DATAOUT, DOUT_DIO_GATE_BIT_MASK);
	SETREG32(&pGpio1->DATAOUT, DOUT_DIO_GATE_BIT_MASK);


	CLRREG32(&pGpio4->DATAOUT,	DOUT_CS_0_BIT_MASK | DOUT_CS_2_BIT_MASK | DOUT_CS_1_BIT_MASK);
	CLRREG32(&pGpio5->DATAOUT,	DOUT_CS_3_BIT_MASK);
#endif
}

//------------------------------------------------------------------------------
//
//  Function:  ClrAioDioPort1Bit
//
//	Clear a bit in AIODIO port 1
//
static VOID ClrAioDioPort1Bit(DWORD Bit)
{
	OMAP_GPIO_REGS* pGpio1;
	OMAP_GPIO_REGS* pGpio4;
	OMAP_GPIO_REGS* pGpio5;
	DWORD MaskPattern;

	// Bank 1 GPIO 0..31
    pGpio1 = OALPAtoUA(OMAP_GPIO1_REGS_PA);
	// Bank 4 GPIO 96..127
	pGpio4 = OALPAtoUA(OMAP_GPIO4_REGS_PA);
	// Bank 5 GPIO 128..159
	pGpio5 = OALPAtoUA(OMAP_GPIO5_REGS_PA);

	MaskPattern = DOUT_DIO_A2_BIT_MASK | DOUT_DIO_A1_BIT_MASK | DOUT_DIO_A0_BIT_MASK;

	// Setting the chip select for output 0, Y6
	CLRREG32(&pGpio4->DATAOUT,	DOUT_CS_0_BIT_MASK);
	CLRREG32(&pGpio5->DATAOUT,	DOUT_CS_3_BIT_MASK);
	SETREG32(&pGpio4->DATAOUT,	DOUT_CS_2_BIT_MASK | DOUT_CS_1_BIT_MASK);

	//Clear the input bit
	MASKREG32(&pGpio1->DATAOUT, MaskPattern, Bit << DOUT_DIO_A0_BIT);
    CLRREG32(&pGpio1->DATAOUT, DOUT_OUT_DATA_BIT_MASK);
    CLRREG32(&pGpio1->DATAOUT, DOUT_DIO_GATE_BIT_MASK);
	SETREG32(&pGpio1->DATAOUT, DOUT_DIO_GATE_BIT_MASK);

	CLRREG32(&pGpio4->DATAOUT,	DOUT_CS_0_BIT_MASK | DOUT_CS_2_BIT_MASK | DOUT_CS_1_BIT_MASK);
	CLRREG32(&pGpio5->DATAOUT,	DOUT_CS_3_BIT_MASK);
}

//------------------------------------------------------------------------------
//
//  Function:  ReadVentButtons
//
//	Read the state of front panel button at power-on time
//
static DWORD ReadVentButtons()
{
    OMAP_GPIO_REGS* pGpio1;
	OMAP_GPIO_REGS* pGpio4;
	OMAP_GPIO_REGS* pGpio5;
	DWORD SetPattern, MaskPattern, i;
	DWORD ButtonFlags = 0;
       
	if(g_BSPtype == BSP_INVALID)
		return BUTTONS_NOT_PRESSED;
	
	// Bank 1 GPIO 0..31
    pGpio1 = OALPAtoUA(OMAP_GPIO1_REGS_PA);
	// Bank 4 GPIO 96..127
	pGpio4 = OALPAtoUA(OMAP_GPIO4_REGS_PA);
	// Bank 5 GPIO 128..159
	pGpio5 = OALPAtoUA(OMAP_GPIO5_REGS_PA);

	// This assumes all buttons are on the same 8 bit port, DIGITAL_INPUT_1
    // set the chip select to the input buttons latch, Y4 for HT70 and Y9 for AURA
	if((g_BSPtype == BSP_AURA_G) || (g_BSPtype == BSP_AURA_C))
	{
		CLRREG32(&pGpio4->DATAOUT,	DOUT_CS_1_BIT_MASK | DOUT_CS_2_BIT_MASK);
		SETREG32(&pGpio4->DATAOUT,	DOUT_CS_0_BIT_MASK);
		SETREG32(&pGpio5->DATAOUT,	DOUT_CS_3_BIT_MASK);
	}
	else if(g_BSPtype == BSP_HT70)
	{
		CLRREG32(&pGpio4->DATAOUT,	DOUT_CS_1_BIT_MASK | DOUT_CS_0_BIT_MASK);
		SETREG32(&pGpio4->DATAOUT,	DOUT_CS_2_BIT_MASK);
		CLRREG32(&pGpio5->DATAOUT,	DOUT_CS_3_BIT_MASK);
	}
	else
	{
		ButtonFlags = 0xFF;
		return ButtonFlags;
	}
	
	MaskPattern = DOUT_DIO_A2_BIT_MASK | DOUT_DIO_A1_BIT_MASK | DOUT_DIO_A0_BIT_MASK;
	for(i=0; i<NUM_VENT_BUTTONS; i++)
	{
		SetPattern = i << DOUT_DIO_A0_BIT;
		MASKREG32(&pGpio1->DATAOUT, MaskPattern, SetPattern);

		// clock in the data
		CLRREG32(&pGpio1->DATAOUT, DOUT_DIO_GATE_BIT_MASK);				// Enable

		OALStall (20);													// Delay to make sure the input is ready, esp. in the case of faster DM3730

		if(INREG32(&pGpio1->DATAIN) & DIN_IN_DATA_BIT_MASK)				// Read the bit
		{
			ButtonFlags |= 1 << i;
		}
		
		SETREG32(&pGpio1->DATAOUT, DOUT_DIO_GATE_BIT_MASK);				// Disable
	}
	
	CLRREG32(&pGpio4->DATAOUT,	DOUT_CS_0_BIT_MASK | DOUT_CS_2_BIT_MASK | DOUT_CS_1_BIT_MASK);
	CLRREG32(&pGpio5->DATAOUT,	DOUT_CS_3_BIT_MASK);
	return ButtonFlags;
}

//------------------------------------------------------------------------------
//
//  Function:  SetBackLightOn
//
//	Turn on the backlight
//
static VOID SetBackLightOn()
{
	OMAP_MCSPI_REGS *pSPI3Regs;
	OMAP_MCSPI_CHANNEL_REGS *pSPI3Channel1Regs;
    	
	pSPI3Regs = (OMAP_MCSPI_REGS *)OALPAtoUA(OMAP_MCSPI3_REGS_PA);	// Set pointer to SPI3 registers
	pSPI3Channel1Regs = (OMAP_MCSPI_CHANNEL_REGS *)(&pSPI3Regs->MCSPI_CHCONF1);	// Set pointer to SPI3 Channel 1 Registers

	SETREG32(&pSPI3Regs->MCSPI_SYSCONFIG, MCSPI_SYSCONFIG_SOFTRESET);
	// Wait until resetting is done
    while ( !(INREG32(&pSPI3Regs->MCSPI_SYSSTATUS) & MCSPI_SYSSTATUS_RESETDONE)) {
        OALStall (10);
    }
		
	// Disable all interrupts.
    OUTREG32(&pSPI3Regs->MCSPI_IRQENABLE, 0);
	// Clear interrupts.
    OUTREG32(&pSPI3Regs->MCSPI_IRQSTATUS, 0xFFFF);
    // Setup Module Control as master 
    OUTREG32(&pSPI3Regs->MCSPI_MODULCTRL, 0); 
    // Set this driver to internal suspend mode
    OUTREG32(&pSPI3Regs->MCSPI_SYSCONFIG, MCSPI_SYSCONFIG_AUTOIDLE |
        MCSPI_SYSCONFIG_SMARTIDLE | MCSPI_SYSCONFIG_ENAWAKEUP);
	OUTREG32(&pSPI3Channel1Regs->MCSPI_CHCONF, 0x000127CC);			// Configure
	SETREG32(&pSPI3Channel1Regs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);	// Enable
	OUTREG32(&pSPI3Channel1Regs->MCSPI_TX, 0x0FF0);					// Transmit
}

//------------------------------------------------------------------------------
//
//  Function:  IsAURA
//
VOID DetermineBSPtype(HANDLE hGPIO)
{
	DWORD BSP_ID_State;
	DWORD ID0, ID1, ID2, ID_HT70;
	OMAP_GPIO_REGS* pGpio1;
	OMAP_GPIO_REGS* pGpio5;
	OMAP_GPIO_REGS* pGpio6;
       
	UNREFERENCED_PARAMETER(hGPIO);
	
	//For some unknown reason, the code hang if GPIOSetMode() or GPIOGetBit() are used for GPIO_186 and GPIO_10
	//but working OK with GPIO_150. So, do not use GPIO driver but access the registers directly  

	//GPIOSetMode(hGPIO, BSP_ID0_BIT, GPIO_DIR_INPUT);
	//GPIOSetMode(hGPIO, BSP_ID1_BIT, GPIO_DIR_INPUT);
	//GPIOSetMode(hGPIO, BSP_ID2_BIT, GPIO_DIR_INPUT);
	//ID0 = GPIOGetBit(hGPIO, BSP_ID0_BIT);
	//ID1 = GPIOGetBit(hGPIO, BSP_ID1_BIT);
	//ID2 = GPIOGetBit(hGPIO, BSP_ID2_BIT);
	
	// For Aura, ID0 = GPIO_186, ID1 = GPIO_10, ID2 = GPIO_150
	
	// Bank 1 GPIO 0..31
    pGpio1 = OALPAtoUA(OMAP_GPIO1_REGS_PA);
	SETREG32(&pGpio1->OE, BSP_ID1_BIT_MASK);			// Set as input
	ID1 = (INREG32(&pGpio1->DATAIN) & BSP_ID1_BIT_MASK) ? (1 << 1) : 0;

	// Bank 5 GPIO 128..159
	pGpio5 = OALPAtoUA(OMAP_GPIO5_REGS_PA);
	SETREG32(&pGpio5->OE, BSP_ID2_BIT_MASK);			// Set as input
	ID2 = (INREG32(&pGpio5->DATAIN) & BSP_ID2_BIT_MASK) ? (1 << 2) : 0;

	// Bank 6 GPIO 160..192
	pGpio6 = OALPAtoUA(OMAP_GPIO6_REGS_PA);
	SETREG32(&pGpio6->OE, BSP_ID0_BIT_MASK);			// Set as input
	ID0 = (INREG32(&pGpio6->DATAIN) & BSP_ID0_BIT_MASK) ? 1 : 0;

	SETREG32(&pGpio6->OE, BSP_HT70_BIT_MASK);			// Set as input
	ID_HT70 = (INREG32(&pGpio6->DATAIN) & BSP_HT70_BIT_MASK) ? 1 : 0;
	
	BSP_ID_State = ID2 | ID1 | ID0;

#if BSP_E600_UI_BUILD
	g_BSPtype = BSP_E600_UI;
	OALLog(L"BSP TYPE = BSP_E600_UI, ID = %d\r\n", BSP_ID_State);
	return;
#elif BSP_E600_BD_BUILD
	g_BSPtype = BSP_E600_BD;
	OALLog(L"BSP TYPE = BSP_E600_BD, ID = %d\r\n", BSP_ID_State);
	return;
#else	
	g_BSPtype = BSP_INVALID;
	if(ID_HT70 && (BSP_BUILD_TYPE == BSP_HT70))
	{
		g_BSPtype = BSP_HT70;
		UpKey = UP_KEY_PRESSED_HT70;
		DownKey = DOWN_KEY_PRESSED_HT70;
		ForcedMenuKeyCombo = FORCED_MENU_KEY_COMBO_HT70;
	}
	else if(BSP_ID_State & BSP_ID1_MASK)
	{
		if((BSP_ID_State == AURA_G_ID) && (BSP_BUILD_TYPE == BSP_HT70))
		{
			g_BSPtype = BSP_AURA_G;
		}
		else if((BSP_ID_State == AURA_C_ID) && (BSP_BUILD_TYPE == BSP_HT70))
		{
			g_BSPtype = BSP_AURA_C;
		}
		UpKey = UP_KEY_PRESSED_AURA;
		DownKey = DOWN_KEY_PRESSED_AURA;
		ForcedMenuKeyCombo = FORCED_MENU_KEY_COMBO_AURA;
	}
	else if((BSP_ID_State == E600_UI_ID) && (BSP_BUILD_TYPE == BSP_E600_UI))
	{
		g_BSPtype = BSP_E600_UI;
	}
	else if((BSP_ID_State == E600_BD_ID) && (BSP_BUILD_TYPE == BSP_E600_BD))
	{
		g_BSPtype = BSP_E600_BD;
	}
	
	OALLog(L"BSP TYPE = %d, ID = %d\r\n", g_BSPtype, BSP_ID_State);
#endif
}

//------------------------------------------------------------------------------
//
//  Function:  Convert_MonthString_To_Month_Digit
//
DWORD Convert_MonthString_To_Month_Digit(WCHAR *InputString)
{
    if(wcscmp(InputString, _T("Jan")) == 0)
    {
        return 1;
    }
    else if(wcscmp(InputString, _T("Feb")) == 0)
    {
        return 2;
    }
    else if(wcscmp(InputString, _T("Mar")) == 0)
    {
        return 3;
    }
    else if(wcscmp(InputString, _T("Apr")) == 0)
    {
        return 4;
    }
    else if(wcscmp(InputString, _T("May")) == 0)
    {
        return 5;
    }
    else if(wcscmp(InputString, _T("Jun")) == 0)
    {
        return 6;
    }
    else if(wcscmp(InputString, _T("Jul")) == 0)
    {
        return 7;
    }
    else if(wcscmp(InputString, _T("Aug")) == 0)
    {
        return 8;
    }
    else if(wcscmp(InputString, _T("Sep")) == 0)
    {
        return 9;
    }
    else if(wcscmp(InputString, _T("Oct")) == 0)
    {
        return 10;
    }
    else if(wcscmp(InputString, _T("Nov")) == 0)
    {
        return 11;
    }
    else if(wcscmp(InputString, _T("Dec")) == 0)
    {
        return 12;
    }
    else
    {
        return 13;  // something is wrong!!
    }
}

//------------------------------------------------------------------------------
//
//  Function:  myatoi
//
//	Convert a wide character string to integer
//
UINT16 myatoi(const WCHAR *str)
{
	UINT16 i, num = 0;
	int strlen;	
	
	strlen = wcslen(str);
	for(i = 0; i < strlen; i++) 
	{ 
		if(str[i] >= '0' && str[i] <= '9') 
			num = num * 10 + str[i] - '0';
	}

	return(num);
}

//#define DEBUG_BD	0
#if DEBUG_BD

UINT8 ReadDebugChar()
{
	UINT8 ch;

	for(;;)
	{
		ch = (UINT8)OEMReadDebugByte();
		if(ch != (UINT8)OEM_DEBUG_READ_NODATA)
		{
			OEMWriteDebugByte(ch);			
			if(((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z')))
			{
				break;
			}
		}
		OALStall(1000);
	}
	return ch;
}

DWORD ReadDebugValue()
{
	UINT8 ch;
	DWORD Val = 0;

	for(;;)
	{
		ch = (UINT8)OEMReadDebugByte();
		if(ch != (UINT8)OEM_DEBUG_READ_NODATA)
		{
			OEMWriteDebugByte(ch);
			
			if((ch >= '0') && (ch <= '9'))
			{
				Val = (Val * 10) + (ch - '0');	
			}
			else if((ch == 10) || (ch == 13) || (ch == 32))
				break;
		}
		OALStall(1000);
	}
	return Val;
}

VOID DebugSetGPIO(HANDLE hGPIO, DWORD bit, DWORD state)
{
	GPIOSetMode(hGPIO, bit, GPIO_DIR_OUTPUT);
	if(state)
	{
		OALLog(L"\r\nSet GPIO %d\r\n", bit);
		GPIOSetBit(hGPIO, bit);
	}
	else
	{
		OALLog(L"\r\nClear GPIO %d\r\n", bit);
		GPIOClrBit(hGPIO, bit);
	}
}

DWORD DebugGetGPIO(HANDLE hGPIO, DWORD bit)
{
	DWORD state;

	GPIOSetMode(hGPIO, bit, GPIO_DIR_INPUT);
	state = GPIOGetBit(hGPIO, bit);
	OALLog(L"Read GPIO %d = %d\r\n", bit, state);
	
	return state;            
}

#define spi1_CH3_DAC_config		0x00010BC8
#define spi1_CH0_ADC_config		0x00010FD0
#define spi3_CH0_ADC_config		0x00010FD0
#define spi1_CH3_AIR_config		0x000103D4
#define spi1_CH0_O2_config		0x000103D4


BOOL DebugWriteReadSPI(DWORD Port, DWORD Channel, DWORD Data, DWORD Config, BOOL Written, DWORD *pReadData)
{
	OMAP_MCSPI_REGS *pSPI1Regs, *pSPI3Regs, *pSPIRegs;
	OMAP_MCSPI_CHANNEL_REGS *pSPI1Channel3Regs, *pSPI1Channel0Regs;
	OMAP_MCSPI_CHANNEL_REGS *pSPI3Channel0Regs, *pSPIChannelRegs;

	pSPI1Regs = (OMAP_MCSPI_REGS *)OALPAtoUA(OMAP_MCSPI1_REGS_PA);
	pSPI1Channel3Regs = (OMAP_MCSPI_CHANNEL_REGS *)(&pSPI1Regs->MCSPI_CHCONF3);
	pSPI1Channel0Regs = (OMAP_MCSPI_CHANNEL_REGS *)(&pSPI1Regs->MCSPI_CHCONF0);

	pSPI3Regs = (OMAP_MCSPI_REGS *)OALPAtoUA(OMAP_MCSPI3_REGS_PA);
	pSPI3Channel0Regs = (OMAP_MCSPI_CHANNEL_REGS *)(&pSPI3Regs->MCSPI_CHCONF0);

	OALLog(L"DebugWriteSPI: Port=%d, Channel=%d, Data=0x%x\r\n", Port, Channel, Data);
				
	if(Port == 1)
	{
		pSPIRegs = pSPI1Regs;
		if(Channel == 3)
		{
			pSPIChannelRegs = pSPI1Channel3Regs;
			//Config = spi1_CH3_DAC_config;
		}
		else if(Channel == 0)
		{
			pSPIChannelRegs = pSPI1Channel0Regs;
			//Config = spi1_CH0_ADC_config;
		}
		else
		{
			OALLog(L"Invalid SPI Channel %d\r\n", Channel);
			return FALSE;
		}

	}
	else if(Port == 3)
	{
		pSPIRegs = pSPI3Regs;
		if(Channel == 0)
		{
			pSPIChannelRegs = pSPI3Channel0Regs;
			//Config = spi3_CH0_ADC_config;
		}
		else
		{
			OALLog(L"Invalid SPI Channel %d\r\n", Channel);
			return FALSE;
		}
	}
	else
	{
		OALLog(L"Invalid SPI Port %d\r\n", Port);
		return FALSE;
	}
	
	SETREG32(&pSPIRegs->MCSPI_SYSCONFIG, MCSPI_SYSCONFIG_SOFTRESET);
	// Wait until resetting is done
    while ( !(INREG32(&pSPIRegs->MCSPI_SYSSTATUS) & MCSPI_SYSSTATUS_RESETDONE)) {
        OALStall (10);
    }
		
	// Disable all interrupts.
    OUTREG32(&pSPIRegs->MCSPI_IRQENABLE, 0);
	// Clear interrupts.
    OUTREG32(&pSPIRegs->MCSPI_IRQSTATUS, 0xFFFF);
    // Setup Module Control as master 
    OUTREG32(&pSPIRegs->MCSPI_MODULCTRL, 0); 
    // Set this driver to internal suspend mode
    OUTREG32(&pSPIRegs->MCSPI_SYSCONFIG, MCSPI_SYSCONFIG_AUTOIDLE |
        MCSPI_SYSCONFIG_SMARTIDLE | MCSPI_SYSCONFIG_ENAWAKEUP);
	
	OUTREG32(&pSPIChannelRegs->MCSPI_CHCONF, Config);			// Configure
	OUTREG32(&pSPIChannelRegs->MCSPI_RX, 0);					// Transmit
	SETREG32(&pSPIChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);		// Enable
	OUTREG32(&pSPIChannelRegs->MCSPI_TX, Data);					// Transmit

	if(!Written)
	{
		OALStall(1000);
		*pReadData = INREG32(&pSPIChannelRegs->MCSPI_RX);
		OALLog(L"SPI Read Value = 0x%x\r\n", *pReadData);
	}
	CLRREG32(&pSPIChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);		// Enable
	return TRUE;
}


typedef enum {
	COMMAND_NONE,
	GPIO_SET,
	GPIO_CLEAR,
	GPIO_GET,
	SPI_WRITE,
	SPI_READ
};

typedef enum {
	DEBUG_COMMAND,
	DEBUG_ACTION
};

#define DAC_WRITE_CMD	0x00300000
#define DAC_OUTPUT_0	0x00010000

VOID DebugBD(HANDLE hGPIO, BOOL RunForever)
{
	UINT8 ch;
	DWORD GpioLevel, GpioBit;
	DWORD SPIPort, SPIChannel, SPIOutput, SPIData, ReadSPIData;
	DWORD DebugState = DEBUG_COMMAND;
	DWORD DebugCommand = COMMAND_NONE;
	DWORD ReadVal, Config;
	
	OALLog(L"Enter BD Debug Mode\r\n");

	GPIOSetMode(hGPIO, 141, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGPIO, 111, GPIO_DIR_OUTPUT);
	GPIOSetBit(hGPIO, 141);
	GPIOClrBit(hGPIO, 11);
	GPIOSetMode(hGPIO, 19, GPIO_DIR_OUTPUT);
	GPIOSetMode(hGPIO, 17, GPIO_DIR_OUTPUT);
	GPIOSetBit(hGPIO, 19);
	GPIOClrBit(hGPIO, 17);

	if(!DebugWriteReadSPI(1, 0, 0, spi1_CH0_ADC_config, 1, NULL))
		OALLog(L"Init SPI1 CH0 FAILED\r\n");
	
	if(!DebugWriteReadSPI(1, 3, 0, spi1_CH3_DAC_config, 1, NULL))
		OALLog(L"Init SPI1 CH3 FAILED\r\n");
	
	if(!DebugWriteReadSPI(3, 0, 0, spi3_CH0_ADC_config, 1, NULL))
		OALLog(L"Init SPI3 CH0 FAILED\r\n");

	
	OALLog(L"Command Uses: \r\n");
	OALLog(L"S - Set GPIO\r\n");
	OALLog(L"C - Clear GPIO\r\n");
	OALLog(L"G - Read GPIO\r\n");
	OALLog(L"W - Write SPI 1 Channel 3\r\n");
	OALLog(L"R - Read SPI 1 Channel 0 and SPI 3 Channel 0\r\n");
	OALLog(L"X - Exit Debug\r\n\r\n");

	while(RunForever)
	{
		switch(DebugState)
		{
		case DEBUG_COMMAND:
			ch = ReadDebugChar(TRUE);
			switch(ch)
			{
			case 'S': case 's':
				OALLog(L" - Set GPIO\r\n");
				DebugCommand = GPIO_SET;
				DebugState = DEBUG_ACTION;
				break;
			case 'C': case 'c':
				OALLog(L" - Clear GPIO\r\n");
				DebugCommand = GPIO_CLEAR;
				DebugState = DEBUG_ACTION;
				break;
			case 'G': case 'g':
				OALLog(L" - Read GPIO\r\n");
				DebugCommand = GPIO_GET;
				DebugState = DEBUG_ACTION;
				break;
			case 'W': case 'w':
				OALLog(L" - Write SPI DAC\r\n");
				DebugCommand = SPI_WRITE;
				DebugState = DEBUG_ACTION;
				break;
			case 'R': case 'r':
				OALLog(L" - Read SPI\r\n");
				DebugCommand = SPI_READ;
				DebugState = DEBUG_ACTION;
				break;
			case 'X': case 'x':
				return;
			default:
				OALLog(L" - Invalid Command\r\n");
				break;
			}
			break;
		case DEBUG_ACTION:
			switch(DebugCommand)
			{
			case GPIO_SET:
				OALLog(L"Enter Pin: ");
				GpioBit = ReadDebugValue();
				DebugSetGPIO(hGPIO, GpioBit, 1);
				DebugCommand = COMMAND_NONE;
				DebugState = DEBUG_COMMAND;
				break;

			case GPIO_CLEAR:
				OALLog(L"Enter Pin: ");
				GpioBit = ReadDebugValue();
				DebugSetGPIO(hGPIO, GpioBit, 0);
				DebugCommand = COMMAND_NONE;
				DebugState = DEBUG_COMMAND;
				break;

			case GPIO_GET:
				OALLog(L"Enter Pin: ");
				GpioBit = ReadDebugValue();
				GpioLevel = DebugGetGPIO(hGPIO, GpioBit);
				DebugCommand = COMMAND_NONE;
				DebugState = DEBUG_COMMAND;
				break;
			
			case SPI_WRITE:
				//OALLog(L"Enter Port: ");
				//SPIPort = ReadDebugValue();
				SPIPort = 1;
				//OALLog(L"\r\nEnter Channel: ");
				//SPIChannel = ReadDebugValue();
				SPIChannel = 3;
				GPIOSetBit(hGPIO, 141);
				GPIOClrBit(hGPIO, 111);
				Config = spi1_CH3_DAC_config;
				
				OALLog(L"\r\nEnter DAC Output (0 .. 3): ");
				SPIOutput = ReadDebugValue();
				OALLog(L"\r\nEnter Data in Decimal: ");
				SPIData = ReadDebugValue();
				SPIData = DAC_WRITE_CMD | (DAC_OUTPUT_0 << SPIOutput) | ((SPIData & 0xFFF) << 4);
				DebugWriteReadSPI(SPIPort, SPIChannel, SPIData, Config, TRUE, NULL);
				DebugCommand = COMMAND_NONE;
				DebugState = DEBUG_COMMAND;
				break;

			case SPI_READ:
				OALLog(L"Enter Port (1 .. 3): ");
				SPIPort = ReadDebugValue();
				OALLog(L"\r\nEnter Channel (0 .. 3): ");
				SPIChannel = ReadDebugValue();
				if(SPIPort == 1)
				{
					OALLog(L"\r\nON_BOARD or OFF_BOARD (0=ON, 1=OFF): ");
					ReadVal = ReadDebugValue();
				}
				else
					ReadVal = 0;
				if(ReadVal == 0)
				{
					GPIOSetBit(hGPIO, 141);
					GPIOClrBit(hGPIO, 111);
					if((SPIPort == 1) && (SPIChannel == 0))
						Config = spi1_CH0_ADC_config;
					else if((SPIPort == 3) && (SPIChannel == 0))
						Config = spi3_CH0_ADC_config;
					else
					{
						OALLog(L"Invalid Entry - Retry\r\n");
						Config = spi1_CH0_ADC_config;
						DebugCommand = COMMAND_NONE;
						DebugState = DEBUG_COMMAND;
						break;
					}
				}
				else if(ReadVal == 1)
				{
					GPIOSetBit(hGPIO, 111);
					GPIOClrBit(hGPIO, 141);
					if((SPIPort == 1) && (SPIChannel == 0))
						Config = spi1_CH0_O2_config;
					else if((SPIPort == 1) && (SPIChannel == 3))
						Config = spi1_CH3_AIR_config;
					else
					{
						OALLog(L"Invalid Entry - Retry\r\n");
						Config = spi1_CH3_AIR_config;
						DebugCommand = COMMAND_NONE;
						DebugState = DEBUG_COMMAND;
						break;
					}
				}
				else
				{
					OALLog(L"Invalid Entry - Retry\r\n");
					Config = spi1_CH3_AIR_config;
					DebugCommand = COMMAND_NONE;
					DebugState = DEBUG_COMMAND;
					break;
				}

				if(ReadVal == 0)	// if ON_BOARD
				{
					OALLog(L"\r\nEnter ADC Input (0 .. 7): ");
					SPIOutput = ReadDebugValue();
					SPIData = SPIOutput << 29;
				}
				else
					SPIData = 0;	// OFF_BOARD, Don't care data
				
				DebugWriteReadSPI(SPIPort, SPIChannel, SPIData, Config, FALSE, &ReadSPIData);

				if(ReadVal == 0)
				{
					ReadSPIData >>= 2;
					OALLog(L"SPI ADC Value = 0x%x\r\n", ReadSPIData);
				}

				DebugCommand = COMMAND_NONE;
				DebugState = DEBUG_COMMAND;
				break;
				
			default:
				break;
			}
			break;
		default:
			break;
		}
	}
}
#endif

void InitDAC()
{
#define spi1_CH3_DAC_config		0x00010BC8
#define DAC_WRITE_CMD			0x00300000
#define DAC_OUTPUT_0			0x00010000

	OMAP_MCSPI_REGS *pSPIRegs;
	OMAP_MCSPI_CHANNEL_REGS *pSPIChannelRegs;
	DWORD SPIData;

	OALLog(L"Intializing DAC\r\n");
	
	pSPIRegs = (OMAP_MCSPI_REGS *)OALPAtoUA(OMAP_MCSPI1_REGS_PA);
	pSPIChannelRegs = (OMAP_MCSPI_CHANNEL_REGS *)(&pSPIRegs->MCSPI_CHCONF3);
	SPIData = DAC_WRITE_CMD | (DAC_OUTPUT_0 << 0) | ((0 & 0xFFF) << 4);
	
	SETREG32(&pSPIRegs->MCSPI_SYSCONFIG, MCSPI_SYSCONFIG_SOFTRESET);
	// Wait until resetting is done
    while ( !(INREG32(&pSPIRegs->MCSPI_SYSSTATUS) & MCSPI_SYSSTATUS_RESETDONE)) {
        OALStall (10);
    }
		
	// Disable all interrupts.
    OUTREG32(&pSPIRegs->MCSPI_IRQENABLE, 0);
	// Clear interrupts.
    OUTREG32(&pSPIRegs->MCSPI_IRQSTATUS, 0xFFFF);
    // Setup Module Control as master 
    OUTREG32(&pSPIRegs->MCSPI_MODULCTRL, 0); 
    // Set this driver to internal suspend mode
    OUTREG32(&pSPIRegs->MCSPI_SYSCONFIG, MCSPI_SYSCONFIG_AUTOIDLE |
        MCSPI_SYSCONFIG_SMARTIDLE | MCSPI_SYSCONFIG_ENAWAKEUP);
	
	OUTREG32(&pSPIChannelRegs->MCSPI_CHCONF, spi1_CH3_DAC_config);	// Configure
	OUTREG32(&pSPIChannelRegs->MCSPI_RX, 0);						// Transmit
	SETREG32(&pSPIChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);		// Enable
	OUTREG32(&pSPIChannelRegs->MCSPI_TX, SPIData);					// Transmit
	OALStall (10);
	CLRREG32(&pSPIChannelRegs->MCSPI_CHCTRL, MCSPI_CHCONT_EN);		// Enable
}
	
//------------------------------------------------------------------------------
//
//  Function:  OEMPlatformInit
//
//  This function provide platform initialization functions. It is called
//  from boot loader after OEMDebugInit is called.  Note that boot loader
//  BootloaderMain is called from  s/init.s code which is run after reset.
//
BOOL 
OEMPlatformInit(
    )
{
    OMAP_GPTIMER_REGS *pTimerRegs;
	UINT32 CpuRevision, version;
    HANDLE hTwl,hGPIO;
    static UCHAR allocationPool[512];
#if BSP_E600_BD_BUILD
	static const PAD_INFO ebootPinMux[] = {
            GPIO_PADS
			USBOTG_PADS
			MCSPI1_PADS
			MCSPI3_PADS
			MCSPI4_PADS
			UART1_PADS
			I2C2_PADS
            I2C3_PADS
            END_OF_PAD_ARRAY
    };
    static const PAD_INFO ebootPinMux_37XX[] = {
            GPIO_PADS
	     	USBOTG_PADS
			MCSPI1_PADS
			MCSPI3_PADS
			MCSPI4_PADS
			UART1_PADS
			I2C2_PADS
            I2C3_PADS
            END_OF_PAD_ARRAY
    };
#else
	static const PAD_INFO ebootPinMux[] = {
            DSS_PADS
            GPIO_PADS
			USBOTG_PADS
			MCSPI3_PADS
			MCSPI4_PADS
			UART1_PADS
			I2C2_PADS
            I2C3_PADS
            END_OF_PAD_ARRAY
    };
    static const PAD_INFO ebootPinMux_37XX[] = {
            DSS_PADS_37XX
            GPIO_PADS
	     	USBOTG_PADS
			MCSPI3_PADS
			MCSPI4_PADS
			UART1_PADS
			I2C2_PADS
            I2C3_PADS
            END_OF_PAD_ARRAY
    };
#endif

    OMAP_PRCM_CORE_CM_REGS* pPrcmCoreCM = OALPAtoUA(OMAP_PRCM_CORE_CM_REGS_PA);
	DWORD i, ButtonFlags, PreviousButtonFlags;
	BSP_ARGS *pArgs;
	WCHAR MonthString[MONTH_STRING_LENGTH+1];
	WCHAR DayString[DAY_STRING_LENGTH+1];
	WCHAR YearString[YEAR_STRING_LENGTH+1];
	WCHAR *pStr;
	OMAP_GPIO_REGS *pGpio5 = OALPAtoUA(OMAP_GPIO5_REGS_PA);
	OMAP_GPIO_REGS *pGpio1 = OALPAtoUA(OMAP_GPIO1_REGS_PA);

#if BSP_AURA_PROTOTYPE	
	BOOL bInitKeypadResult = FALSE;
#endif

    // Get pointer to expected boot args location
    pArgs = OALCAtoUA(IMAGE_SHARE_ARGS_CA);
    
    OALLocalAllocInit(allocationPool,sizeof(allocationPool));

    // Get processor and companion chip versions
	g_CPUFamily = CPU_FAMILY_OMAP35XX;
    CpuRevision = Get_CPUVersion();
    version = CPU_REVISION(CpuRevision);
    g_CPUFamily = CPU_FAMILY(CpuRevision);

	// Set GPTIMER1 regs pointer
	pTimerRegs = OALPAtoUA(OMAP_GPTIMER1_REGS_PA);

    if(g_CPUFamily == CPU_FAMILY_DM37XX)
    {
		ConfigurePadArray(ebootPinMux_37XX);
    }
    else
    {
		ConfigurePadArray(ebootPinMux);
    }
    //OALLogSetZones( 
    //           (1<<OAL_LOG_VERBOSE)  |
    //           (1<<OAL_LOG_INFO)     |
    //           (1<<OAL_LOG_ERROR)    |
    //           (1<<OAL_LOG_WARN)     |
    //           (1<<OAL_LOG_FUNC)     |
    //           (1<<OAL_LOG_IO)     |
    //           0);

    /*OALLog(
        L"\r\nTexas Instruments Windows CE EBOOT for OMAP35xx/37xx, "
        L"Built %S at %S\r\n", __DATE__, __TIME__
        );
    OALLog(
        L"EBOOT Version %d.%d, BSP " BSP_VERSION_STRING L"\r\n", 
        EBOOT_VERSION_MAJOR, EBOOT_VERSION_MINOR        
        );*/

    // Soft reset GPTIMER1
    OUTREG32(&pTimerRegs->TIOCP, SYSCONFIG_SOFTRESET);
    // While until done
    while ((INREG32(&pTimerRegs->TISTAT) & GPTIMER_TISTAT_RESETDONE) == 0);
    // Enable posted mode
    OUTREG32(&pTimerRegs->TSICR, GPTIMER_TSICR_POSTED);
    // Start timer
    OUTREG32(&pTimerRegs->TCLR, GPTIMER_TCLR_AR|GPTIMER_TCLR_ST);
    
	// Enable device clocks used by the bootloader
    EnableDeviceClocks(OMAP_DEVICE_GPIO1,TRUE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO2,TRUE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO3,TRUE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO4,TRUE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO5,TRUE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO6,TRUE);
	SETREG32(&pPrcmCoreCM->CM_FCLKEN1_CORE, CM_CLKEN_MCSPI3);  // enable SPI3 clock
    SETREG32(&pPrcmCoreCM->CM_ICLKEN1_CORE, CM_CLKEN_MCSPI3);
	SETREG32(&pPrcmCoreCM->CM_FCLKEN1_CORE, CM_CLKEN_MCSPI1);  // enable SPI1 clock
    SETREG32(&pPrcmCoreCM->CM_ICLKEN1_CORE, CM_CLKEN_MCSPI1);
	EnableDeviceClocks(BSPGetDebugUARTConfig()->dev, TRUE);

	// configure i2c devices
    OALI2CInit(OMAP_DEVICE_I2C1);
    OALI2CInit(OMAP_DEVICE_I2C2);
    OALI2CInit(OMAP_DEVICE_I2C3);

    GPIOInit();
    // Note that T2 accesses must occur after I2C initialization
    hTwl = TWLOpen();
    hGPIO = GPIOOpen(); 

//-------------------------------------------------------------------------
// This block of code is added for HT70 / AURA
	OALLog(L"OEMPlatformInit: Initializing AIODIO\r\n");
	InitAioDio();

	if(hGPIO)
	{
		OALLog(L"OEMPlatformInit: hGPIO = 0x%x\r\n", hGPIO);
		DetermineBSPtype(hGPIO);
	}
	else
	{
		g_BSPtype = BSP_HT70;
		OALLog(L"OEMPlatformInit: ERROR opening GPIO\r\n");
	}

	if (g_BSPtype == BSP_E600_UI)
	{
#if BSP_E600_UI_BUILD
		CLRREG32(&pGpio1->OE,  1 << (DVIMC_PUP_GPIO % 32));
		CLRREG32(&pGpio1->OE,  1 << (DVIRC_PUP_GPIO % 32));
		OALLog(L"OEMPlatformInit: Enable DVI Control\r\n");
		SETREG32(&pGpio1->DATAOUT, 1 << (DVIMC_PUP_GPIO % 32));
		SETREG32(&pGpio1->DATAOUT, 1 << (DVIRC_PUP_GPIO % 32));
#else
		//GPIOSetMode(hGPIO, 120, GPIO_DIR_OUTPUT);	// Enable remote console
		//GPIOSetBit(hGPIO, 120);
		// Bank 1 GPIO 0..31
		//pGpio1 = OALPAtoUA(OMAP_GPIO1_REGS_PA);					// GPIO_20

#endif
	}
	else if(g_BSPtype == BSP_E600_BD)
	{
		/*GPIOSetMode(hGPIO, 70,GPIO_DIR_INPUT);
		GPIOSetMode(hGPIO, 71,GPIO_DIR_INPUT);
		GPIOSetMode(hGPIO, 72,GPIO_DIR_INPUT);*/
		//GPIOSetMode(hGPIO, 141,GPIO_DIR_OUTPUT);
		//GPIOSetMode(hGPIO, 111,GPIO_DIR_OUTPUT);		
		//GPIOSetBit(hGPIO, 141);
		//GPIOClrBit(hGPIO, 111);
			
		/*GPIOSetMode(hGPIO, 19,GPIO_DIR_OUTPUT);
		GPIOSetMode(hGPIO, 27,GPIO_DIR_OUTPUT);
		GPIOClrBit(hGPIO, 27);
		GPIOSetBit(hGPIO, 19);*/
		//pGpio1 = OALPAtoUA(OMAP_GPIO1_REGS_PA);
		CLRREG32(&pGpio1->OE,  1 << (27 % 32));			// Clear GPIO_27 for NURSE_CALL
		SETREG32(&pGpio1->DATAOUT, 1 << (19 % 32));		// Set GPIO_19 for DAC
		
		InitDAC();
	}

#if BSP_E600_UI_P1_BUILD	
	// Temp. workaround for debug ethernet since GPIO_17 is incorrectly used as ENET_CS on the E600 UI board
	// Set GPIO_17 as input on E600 UI
	if (g_BSPtype == BSP_E600_UI)
		SETREG32(&pGpio1->OE,  1 << 17);
#endif

	// Clear Reset on ethernet controller 
	if((g_BSPtype == BSP_AURA_G) || (g_BSPtype == BSP_AURA_C) || (g_BSPtype == BSP_E600_UI) || (g_BSPtype == BSP_E600_BD))
	{		
		        
		//GPIOSetBit(hGPIO, LAN9115_RESET_GPIO);            
		//GPIOSetMode(hGPIO, LAN9115_RESET_GPIO,GPIO_DIR_OUTPUT);
		
		// Bank 5 GPIO 128..159
		OALLog(L"OEMPlatformInit: Set LAN Reset Pin %d\r\n", LAN9115_RESET_GPIO);
		pGpio5 = OALPAtoUA(OMAP_GPIO5_REGS_PA);					// GPIO_141 for AURA and GPIO_157 for E600 UI & BD
		CLRREG32(&pGpio5->DATAOUT, 1 << (LAN9115_RESET_GPIO % 32));
		OALStall(100);
		SETREG32(&pGpio5->DATAOUT, 1 << (LAN9115_RESET_GPIO % 32));
	}

	GPIOSetMode(hGPIO, 258,GPIO_DIR_INPUT);
	GPIOSetMode(hGPIO, 271,GPIO_DIR_INPUT);
	
	// Read the state of the front panel buttons.  If two consecutive reads are the same, the data is considered valid
	if((g_BSPtype == BSP_AURA_G) || (g_BSPtype == BSP_AURA_C))
	{		
		GPIOSetMode(hGPIO, DOUT_TPS_BL_ENA_BIT,GPIO_DIR_OUTPUT);
		GPIOClrBit(hGPIO, DOUT_TPS_BL_ENA_BIT);            

#if BSP_AURA_PROTOTYPE
		bInitKeypadResult = InitKeypadI2C(hGPIO, BOTH_KEYPADS);
		if(bInitKeypadResult)						// if successfully initialize keypad
			ButtonFlags = ReadVentButtonsI2C();		// read it
		else
			ButtonFlags = BUTTONS_NOT_PRESSED;		// else, set to no buttons pressed
#else
		ButtonFlags = ReadVentButtons();
#endif
	}
	else if(g_BSPtype == BSP_E600_UI)
	{
		ButtonFlags = ReadVentButtonsI2C();
		OALLog(L"OEMPlatformInit: I2C Buttons=0x%x\r\n", ButtonFlags);
	}
	else
	{
		if (g_BSPtype == BSP_HT70)
			SetBackLightOn();
		ButtonFlags = ReadVentButtons();
	}

	for(i=0; i<5; i++)
	{
		PreviousButtonFlags = ButtonFlags;
#if BSP_AURA_PROTOTYPE			
		if(g_BSPtype == BSP_AURA)
		{
			if(bInitKeypadResult)		
				ButtonFlags = ReadVentButtonsI2C();
			else
			{
				ButtonFlags = BUTTONS_NOT_PRESSED;
				break;
			}
		}
		else //if (g_BSPtype == BSP_HT70)
#endif
		if(g_BSPtype == BSP_E600_UI)
		{
			ButtonFlags = ReadVentButtonsI2C();
			OALLog(L"OEMPlatformInit: I2C Buttons=0x%x\r\n", ButtonFlags);
		}
		else
		{
			ButtonFlags = ReadVentButtons();
		}
		if(PreviousButtonFlags == ButtonFlags)
			break;
	}
	
	g_buttonFlags = ButtonFlags;

	// Converting the version string from ASCII to wide character
	for (i = 0; i < NMI_VERSION_STRING_SIZE - 1; i++)
    {
        pArgs->NmiEbootVersion.VersionString[i] = 0;
        pArgs->NmiEbootVersion.VersionString[i] |= NmiVersionString[i];
    }
    pArgs->NmiEbootVersion.VersionString[NMI_VERSION_STRING_SIZE - 1] = 0;		// make sure the version string is terminated

	pStr = pArgs->NmiEbootVersion.VersionString;
	wcsncpy(MonthString, pStr, MONTH_STRING_LENGTH);
	MonthString[MONTH_STRING_LENGTH] = 0;
	pArgs->NmiEbootVersion.Month = Convert_MonthString_To_Month_Digit(MonthString);
	
	pStr += MONTH_STRING_LENGTH + 1;	// move pointer to the beginning of day string
	wcsncpy(DayString, pStr, DAY_STRING_LENGTH);
	DayString[DAY_STRING_LENGTH] = 0;
	pArgs->NmiEbootVersion.Day = myatoi(DayString);
	
	pStr += DAY_STRING_LENGTH + 1;		// move pointer to the beginning of year string
	wcsncpy(YearString, pStr, YEAR_STRING_LENGTH);
	YearString[YEAR_STRING_LENGTH] = 0;
	pArgs->NmiEbootVersion.Year = myatoi(YearString) - 2000;
	
	OALLog(L"\r\nNMI EBOOT Version V%d.%02d.%02d\r\n", 
		pArgs->NmiEbootVersion.Month, pArgs->NmiEbootVersion.Year, pArgs->NmiEbootVersion.Day);
	memcpy(&g_ebootVersion, &pArgs->NmiEbootVersion, sizeof(NMI_VERSION));

	OALLog(L"Button Flags = 0x%x\r\n", g_buttonFlags);
	
	OALStall(BUZZER_DELAY_TIME);					// Delay for the buzzer to sound
	ClrAioDioPort1Bit(DEVICE_ALERT_BIT);			// Turn off the buzzer

//-------------------------------------------------------------------------

	OALLog(L"\r\nTI OMAP%x Version 0x%08x (%s)\r\n", CPU_ID(CpuRevision), CPU_REVISION(CpuRevision),        
        version == CPU_FAMILY_35XX_REVISION_ES_1_0 ? L"ES1.0" :
        version == CPU_FAMILY_35XX_REVISION_ES_2_0 ? L"ES2.0" :
        version == CPU_FAMILY_35XX_REVISION_ES_2_1 ? L"ES2.1" :
        version == CPU_FAMILY_35XX_REVISION_ES_2_0_CRC ? L"ES2.0, ID determined using CRC" :
        version == CPU_FAMILY_35XX_REVISION_ES_2_1_CRC ? L"ES2.1, ID determined using CRC" :
        version == CPU_FAMILY_35XX_REVISION_ES_3_0 ? L"ES3.0" :
        version == CPU_FAMILY_35XX_REVISION_ES_3_1 ? L"ES3.1" : 
        version == CPU_FAMILY_37XX_REVISION_ES_1_0? L"ES1.0" :
        version == CPU_FAMILY_37XX_REVISION_ES_1_1? L"ES1.1" :
        version == CPU_FAMILY_37XX_REVISION_ES_1_2? L"ES1.2" :
        L"Unknown" );  
    /* Initialize Device Prefix */
    if(g_CPUFamily == CPU_FAMILY_DM37XX)
    {
        gDevice_prefix = BSP_DEVICE_37xx_PREFIX;
    }
    else if (g_CPUFamily == CPU_FAMILY_OMAP35XX)
    {
        gDevice_prefix = BSP_DEVICE_35xx_PREFIX;
    }
    else
    {
        OALLog(L"INFO: UnKnown CPU family:%d....\r\n", g_CPUFamily);
        gDevice_prefix = BSP_DEVICE_35xx_PREFIX;
    }

    version = TWLReadIDCode(hTwl);

    OALLog(L"TPS659XX Version 0x%02x (%s)\r\n", version,
        version == 0x00 ? L"ES1.0" : 
        version == 0x10 ? L"ES1.1" : 
        version == 0x20 ? L"ES1.2" : 
        version == 0x30 ? L"ES1.3" : L"Unknown" );
		
    g_ecctype = (UCHAR)dwEbootECCtype;
	
	// Done
#if DEBUG_BD    
	DebugBD(hGPIO, TRUE);
#endif

	return TRUE;
}

//------------------------------------------------------------------------------
//
//  Function:  OEMPlatformDeinit
//
static
VOID
OEMPlatformDeinit(
    )
{
    OMAP_GPTIMER_REGS *pTimerRegs = OALPAtoUA(OMAP_GPTIMER1_REGS_PA);

    // Soft reset GPTIMER
    OUTREG32(&pTimerRegs->TIOCP, SYSCONFIG_SOFTRESET);
    // While until done
    while ((INREG32(&pTimerRegs->TISTAT) & GPTIMER_TISTAT_RESETDONE) == 0);

	// Disable device clocks that were used by the bootloader
    EnableDeviceClocks(OMAP_DEVICE_GPIO1,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO2,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO3,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO4,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO5,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_GPIO6,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_GPTIMER2,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_UART1,FALSE);
	EnableDeviceClocks(OMAP_DEVICE_UART2,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_UART3,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_MMC1,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_MMC2,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_I2C1,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_I2C2,FALSE);
    EnableDeviceClocks(OMAP_DEVICE_I2C3,FALSE);
}
/*
//------------------------------------------------------------------------------
//
//  Function:  OALTritonSet
//
static void OALTritonSet(HANDLE hTwl, DWORD Register, BYTE Mask)
{
    BYTE regval;
    
    OALTritonRead(hTwl, Register, &regval);
    regval |= Mask;
    OALTritonWrite(hTwl, Register, regval);
}

//------------------------------------------------------------------------------
//
//  Function:  OALTritonClear
//
static void OALTritonClear(HANDLE hTwl, DWORD Register, BYTE Mask)
{
    BYTE regval;
    
    OALTritonRead(hTwl, Register, &regval);
    regval &= ~Mask;
    OALTritonWrite(hTwl, Register, regval);
}
*/
//------------------------------------------------------------------------------
//
//  Function:  CpuGpioOutput
//
/*
static void CpuGpioOutput(DWORD GpioNumber, DWORD Value)
{
    UINT32 fWkup, iWkup;
    UINT32 fPer, iPer;
    OMAP_GPIO_REGS* pGpio;
    OMAP_PRCM_PER_CM_REGS* pPrcmPerCM = OALPAtoUA(OMAP_PRCM_PER_CM_REGS_PA);
    OMAP_PRCM_WKUP_CM_REGS* pPrcmWkupCM = OALPAtoUA(OMAP_PRCM_WKUP_CM_REGS_PA);

    // Enable clocks to GPIO modules
    fWkup = INREG32(&pPrcmWkupCM->CM_FCLKEN_WKUP);
    iWkup = INREG32(&pPrcmWkupCM->CM_ICLKEN_WKUP);
    fPer = INREG32(&pPrcmPerCM->CM_FCLKEN_PER);
    iPer = INREG32(&pPrcmPerCM->CM_ICLKEN_PER);
    SETREG32(&pPrcmWkupCM->CM_FCLKEN_WKUP, CM_CLKEN_GPIO1);
    SETREG32(&pPrcmWkupCM->CM_ICLKEN_WKUP, CM_CLKEN_GPIO1);
    SETREG32(&pPrcmPerCM->CM_FCLKEN_PER, CM_CLKEN_GPIO2|CM_CLKEN_GPIO3|CM_CLKEN_GPIO4|CM_CLKEN_GPIO5|CM_CLKEN_GPIO6);
    SETREG32(&pPrcmPerCM->CM_ICLKEN_PER, CM_CLKEN_GPIO2|CM_CLKEN_GPIO3|CM_CLKEN_GPIO4|CM_CLKEN_GPIO5|CM_CLKEN_GPIO6);

    switch (GpioNumber >> 5)
    {
        case 0:
            pGpio = OALPAtoUA(OMAP_GPIO1_REGS_PA);
            break;
            
        case 1:
            pGpio = OALPAtoUA(OMAP_GPIO2_REGS_PA);
            GpioNumber -= 32;
            break;

        case 2:
            pGpio = OALPAtoUA(OMAP_GPIO3_REGS_PA);
            GpioNumber -= 64;
            break;

        case 3:
            pGpio = OALPAtoUA(OMAP_GPIO4_REGS_PA);
            GpioNumber -= 96;
            break;

        case 4:
            pGpio = OALPAtoUA(OMAP_GPIO5_REGS_PA);
            GpioNumber -= 128;
            break;

        case 5:
            pGpio = OALPAtoUA(OMAP_GPIO6_REGS_PA);
            GpioNumber -= 160;
            break;

        default:
            return;
    }

    // set output state of GPIO pin
    if (Value)
        SETREG32(&pGpio->DATAOUT, 1 << GpioNumber);
    else
        CLRREG32(&pGpio->DATAOUT, 1 << GpioNumber);

    // make GPIO pin an output
    CLRREG32(&pGpio->OE, 1 << GpioNumber);

    // Put clocks back
    OUTREG32(&pPrcmWkupCM->CM_FCLKEN_WKUP, fWkup);
    OUTREG32(&pPrcmWkupCM->CM_ICLKEN_WKUP, iWkup);
    OUTREG32(&pPrcmPerCM->CM_FCLKEN_PER, fPer);
    OUTREG32(&pPrcmPerCM->CM_ICLKEN_PER, iPer);
}*/

/*UINT8 ReadDebugChar()
{
	UINT8 ch;

	for(;;)
	{
		ch = (UINT8)OEMReadDebugByte();
		if(ch != (UINT8)OEM_DEBUG_READ_NODATA)
			break;
	}
	return ch;
}*/

//------------------------------------------------------------------------------
//
//  Function:  OEMPreDownload
//
//  This function is called before downloading an image. There is place
//  where user can be asked about device setup.
//
ULONG
OEMPreDownload(
    )
{
    ULONG rc = (ULONG) BL_ERROR;
    BSP_ARGS *pArgs = OALCAtoUA(IMAGE_SHARE_ARGS_CA);
    BOOL	bForceBootMenu;
    OMAP_PRCM_GLOBAL_PRM_REGS * pPrmGlobal = OALPAtoUA(OMAP_PRCM_GLOBAL_PRM_REGS_PA);
    ULONG dwTemp;
    UINT32 *pStatusControlAddr = OALPAtoUA(OMAP_STATUS_CONTROL_REGS_PA);
    UINT32 dwSysBootCfg;
	DWORD	dwDelayForMenu;
	BOOL	bProductionBootCfg = FALSE;
	BOOL bEthInitResult;
	UINT8 *pEthAddress;
	UINT32 EthOffset;
	UINT16 Mac[3];
	
	OALLog(L"INFO: Predownload....\r\n");
	    
	// We need to support multi bin notify
    g_pOEMMultiBINNotify = OEMMultiBinNotify;

    // Ensure bootloader blocks are marked as reserved
    BLReserveBootBlocks();

    // select default boot device based on boot select switch setting
    dwSysBootCfg = INREG32(pStatusControlAddr);
    OALLog(L"INFO: SW4 boot setting: 0x%02x\r\n", dwSysBootCfg & 0x3f);

	// Read saved configration
    if (BLReadBootCfg(&g_bootCfg) &&
        (g_bootCfg.signature == BOOT_CFG_SIGNATURE) &&
        (g_bootCfg.version == BOOT_CFG_VERSION))
    {
		OALLog(L"INFO: Boot configuration found\r\n");
    }
    else 
        {
        OALLog(L"WARN: Boot config wasn't found, using defaults\r\n");
        memset(&g_bootCfg, 0, sizeof(g_bootCfg));
		memcpy(&g_bootCfg.mac,DefaultMacAddress,sizeof(g_bootCfg.mac));
        g_bootCfg.signature = BOOT_CFG_SIGNATURE;
        g_bootCfg.version = BOOT_CFG_VERSION;

        g_bootCfg.oalFlags = 0;
        g_bootCfg.flashNKFlags = 0;
        g_bootCfg.ECCtype =  (UCHAR)dwEbootECCtype;
        // To make it easier to select USB or EBOOT from menus when booting from SD card,
        // preset the kitlFlags. This has no effect if booting from SD card.
        g_bootCfg.kitlFlags = /*OAL_KITL_FLAGS_DHCP|*/OAL_KITL_FLAGS_ENABLED;
        g_bootCfg.kitlFlags |= OAL_KITL_FLAGS_VMINI;
        g_bootCfg.kitlFlags |= OAL_KITL_FLAGS_EXTNAME;

		if(g_BSPtype == BSP_E600_BD)
			g_bootCfg.ipAddress = 0x0500A8C0;
		else
			g_bootCfg.ipAddress = 0x0400A8C0;

		g_bootCfg.ipMask = 0x00FFFFFF;

		if(g_BSPtype == BSP_E600_UI)
#if E600_1920X1080_DISPLAY
			g_bootCfg.displayRes = OMAP_DVI_1920W_1080H;
#else
			g_bootCfg.displayRes = OMAP_DVI_1280W_720H;
#endif
		else
			g_bootCfg.displayRes = OMAP_LCD_DEFAULT;
        
		if(g_CPUFamily == CPU_FAMILY_DM37XX)
        {
            g_bootCfg.opp_mode = BSP_OPM_SELECT_37XX-1;
        }
        else
        {
            g_bootCfg.opp_mode = BSP_OPM_SELECT_35XX-1;
        }
		switch (dwSysBootCfg & 0x3f)
        {
        case 0x24:
        case 0x26:
        case 0x3b:
            // 1st boot device is USB
            g_bootCfg.bootDevLoc.LogicalLoc = OMAP_USBHS_REGS_PA;
            g_bootCfg.kitlDevLoc.LogicalLoc = OMAP_USBHS_REGS_PA;
            break;
    
        case 0x06:
        case 0x12:
        case 0x18:
            // 1st boot device is MMC1 (SD Card Boot)
            g_bootCfg.bootDevLoc.LogicalLoc = OMAP_MMCHS1_REGS_PA;
            break;

		case 0x01: 
        case 0x0c: 
        case 0x15: 
        case 0x1b: 
            // 1st boot device is NAND
            g_bootCfg.bootDevLoc.LogicalLoc = BSP_NAND_REGS_PA + 0x20;
            break;
            
        case 0x00:
        case 0x02:
        case 0x04:
        case 0x10:
        case 0x16:
            // 1st boot device is ONENAND
            g_bootCfg.bootDevLoc.LogicalLoc = BSP_ONENAND_REGS_PA + 0x20;                       
            break;

        default:
            // UART,  Ethernet Boot
            g_bootCfg.bootDevLoc.LogicalLoc = BSP_LAN9115_REGS_PA;
            g_bootCfg.kitlDevLoc.LogicalLoc = BSP_LAN9115_REGS_PA;
            break;
        }

        if (g_bootCfg.kitlDevLoc.LogicalLoc == 0)
        {
            g_bootCfg.kitlDevLoc.LogicalLoc = BSP_LAN9115_REGS_PA;
        };
        g_bootCfg.deviceID = 0;
        //g_bootCfg.osPartitionSize = IMAGE_WINCE_CODE_SIZE; CE6
        g_bootCfg.osPartitionSize = IMAGE_NK_NAND_SIZE;
        //wcscpy_s(g_bootCfg.filename,_countof(g_bootCfg.filename), L"nk.bin"); //NMI CE6 delete
    }// else 

	if(g_BSPtype != BSP_E600_BD)
	{
		OALLog(L"INFO: Displaying Logo1 ...\r\n");
		BLShowLogo(LOGO1);
	}

	if((dwSysBootCfg & 0x3f) == PRODUCTION_BOOT_CFG)
		bProductionBootCfg = TRUE;
	
	OALLog(L"INFO: bProductionBootCfg = %d\r\n", bProductionBootCfg);
	
	if(bProductionBootCfg)
	{
		if(!XloaderExists() || ((g_buttonFlags == UpKey) && BLSDCardFileExists(XEL_NB0_FILE))
			|| ((g_BSPtype == BSP_E600_UI) && BLSDCardFileExists(XEL_NB0_FILE))
			|| ((g_BSPtype == BSP_E600_BD) && BLSDCardFileExists(XEL_NB0_FILE))
			)
		{
			OALLog(L"INFO: Boot file to load is %s\r\n", XEL_NB0_FILE);
			wcscpy(g_bootCfg.filename, XEL_NB0_FILE);
			g_bootCfg.bootDevLoc.LogicalLoc = OMAP_MMCHS1_REGS_PA;			// NK from SD	
		}
		else if(!NKExists() || ((g_buttonFlags == DownKey) && BLSDCardFileExists(NK_BIN_FILE))		
			|| ((g_BSPtype == BSP_E600_UI) && BLSDCardFileExists(NK_BIN_FILE))
			|| ((g_BSPtype == BSP_E600_BD) && BLSDCardFileExists(NK_BIN_FILE))
			)
		{
			OALLog(L"INFO: Boot file to load is %s\r\n", NK_BIN_FILE);
			wcscpy(g_bootCfg.filename, NK_BIN_FILE);
			g_bootCfg.bootDevLoc.LogicalLoc = OMAP_MMCHS1_REGS_PA;			// NK from SD
		}
		else
		{
			OALLog(L"INFO: No download\r\n");
			wcscpy(g_bootCfg.filename, L"");
			g_bootCfg.bootDevLoc.LogicalLoc = BSP_NAND_REGS_PA + 0x20;		// NK from NAND
		}
	}
		
	// Initialize flash partitions if needed
	BLConfigureFlashPartitions(FALSE);	
	
	// Initialize ARGS structure
    if ((pArgs->header.signature != OAL_ARGS_SIGNATURE) ||
        (pArgs->header.oalVersion != OAL_ARGS_VERSION) ||
        (pArgs->header.bspVersion != BSP_ARGS_VERSION))
        {
        memset(pArgs, 0, IMAGE_SHARE_ARGS_SIZE);
        }        
    
    // Save reset type
    dwTemp = INREG32(&pPrmGlobal->PRM_RSTST);
    if (dwTemp & (GLOBALWARM_RST /* actually SW reset */ | EXTERNALWARM_RST))
    {
        pArgs->coldBoot = FALSE;
		g_ErrorCode |= SYSTEM_WARM_RESET;
		OALLog(L"\r\n>>> Warm Boot <<< \r\n");
    }
    else
    {
        pArgs->coldBoot = TRUE;
		OALLog(L"\r\n>>> Forcing cold boot (non-persistent registry and other data will be wiped) <<< \r\n");
    }
    
    // Don't force the boot menu, use default action unless user breaks
    // into menu
#if BSP_NMI_REL    
	if((g_buttonFlags == ForcedMenuKeyCombo) && bProductionBootCfg)
	{
		bForceBootMenu = TRUE;
		dwDelayForMenu = NO_MENU_DELAY;
	}
	else if ((dwSysBootCfg & 0x3f) == 0x2f)
	{
		bForceBootMenu = FALSE;
		dwDelayForMenu = SHORT_MENU_DELAY;
	}
	else
#endif
	{
		bForceBootMenu = FALSE;
		dwDelayForMenu = LONG_MENU_DELAY;
	}
    
retryBootMenu:
    // Call configuration menu
    BLMenu(bForceBootMenu, dwDelayForMenu);
    // Update ARGS structure if necessary
    if ((pArgs->header.signature != OAL_ARGS_SIGNATURE) ||
        (pArgs->header.oalVersion != OAL_ARGS_VERSION) ||
        (pArgs->header.bspVersion != BSP_ARGS_VERSION))
        {
        pArgs->header.signature = OAL_ARGS_SIGNATURE;
        pArgs->header.oalVersion = OAL_ARGS_VERSION;
        pArgs->header.bspVersion = BSP_ARGS_VERSION;
        pArgs->kitl.flags = g_bootCfg.kitlFlags;
        pArgs->kitl.devLoc = g_bootCfg.kitlDevLoc;
        pArgs->kitl.ipAddress = g_bootCfg.ipAddress;
        pArgs->kitl.ipMask = g_bootCfg.ipMask;
        pArgs->kitl.ipRoute = g_bootCfg.ipRoute;
		memcpy(pArgs->kitl.mac,g_bootCfg.mac,sizeof(pArgs->kitl.mac)); 
 	    pArgs->updateMode = FALSE;
        pArgs->deviceID = g_bootCfg.deviceID;
        pArgs->oalFlags = g_bootCfg.oalFlags;
        pArgs->dispRes = g_bootCfg.displayRes;
        pArgs->ECCtype = g_bootCfg.ECCtype; 
        pArgs->opp_mode = g_bootCfg.opp_mode;
        memcpy(pArgs->DevicePrefix, gDevice_prefix, sizeof(pArgs->DevicePrefix));
        }  
    
	memcpy(&pArgs->NmiEbootVersion, &g_ebootVersion, sizeof(NMI_VERSION));
	pArgs->ButtonFlags = g_buttonFlags;		// save button power-on state to boot args
	pArgs->BSPtype = g_BSPtype;
	
	// taken out to get rid of flickering on the display when transitioning to new screen
	/*if(g_BSPtype != BSP_E600_BD)
	{	
		BLHideLogo();
		OALLog(L"INFO: Displaying Logo2 ...\r\n");
		BLShowLogo(LOGO2);
	}*/
	
	// Image download depends on protocol
    g_eboot.bootDeviceType = OALKitlDeviceType(
        &g_bootCfg.bootDevLoc, g_bootDevices
        );

	pEthAddress = (UINT8 *)0x15000000;
	EthOffset = 0x15000000;
	bEthInitResult = LAN911XInit(pEthAddress, EthOffset, Mac);
	if(bEthInitResult)
		OALLog(L"\r\nEBOOT: Ethernet Link is Detected\r\n");
	else
	{
		OALLog(L"\r\nEBOOT: Ethernet Link is NOT Detected\r\n");
		g_ErrorCode |= ETHERNET_LINK_NOT_DETECTED;
	}

    switch (g_eboot.bootDeviceType)
        {
        case BOOT_SDCARD_TYPE:
			OALLog(L"\r\nBootDeviceType = BOOT_SDCARD_TYPE\r\n");
            rc = BLSDCardDownload(g_bootCfg.filename);
			if(rc == BL_ERROR)
				OALLog(L"\r\nERROR: Downloading from SD Card Failed\r\n");
            break;
        case OAL_KITL_TYPE_FLASH:
			OALLog(L"\r\nBootDeviceType = OAL_KITL_TYPE_FLASH\r\n");
#if BSP_NMI_REL            
			if(bProductionBootCfg)
			{
				if(!VerifyBloaderCRC())
				{
					for(;;);
				}
			}
#endif
			rc = BLFlashDownload(&g_bootCfg, g_bootDevices);
            break;
        case OAL_KITL_TYPE_ETH:
			OALLog(L"\r\nBootDeviceType = OAL_KITL_TYPE_ETH\r\n");
            rc = BLEthDownload(&g_bootCfg, g_bootDevices);
            break;
        }
        
    if (rc == BL_ERROR)
    {
        // No automatic mode now, force the boot menu to appear
        bForceBootMenu = TRUE;
        goto retryBootMenu; 
    }   
    
    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:  OEMLaunch
//
//  This function is the last one called by the boot framework and it is
//  responsible for to launching the image.
//
VOID
OEMLaunch(
   ULONG start, 
   ULONG size, 
   ULONG launch, 
   const ROMHDR *pRomHeader
    )
{
    BSP_ARGS *pArgs = OALCAtoUA(IMAGE_SHARE_ARGS_CA);

	UNREFERENCED_PARAMETER(size);
	UNREFERENCED_PARAMETER(pRomHeader);

    OALMSG(OAL_FUNC, (
        L"+OEMLaunch(0x%08x, 0x%08x, 0x%08x, 0x%08x - %d/%d)\r\n", start, size,
        launch, pRomHeader, g_eboot.bootDeviceType, g_eboot.type
        ));

    // Depending on protocol there can be some action required
    switch (g_eboot.bootDeviceType)
        {
#if BUILDING_EBOOT_SD
        case BOOT_SDCARD_TYPE:            
            switch (g_eboot.type)
                {
#if 0
/*
                case DOWNLOAD_TYPE_FLASHRAM:
                    if (BLFlashDownload(&g_bootCfg, g_kitlDevices) != BL_JUMP)
                        {
                        OALMSG(OAL_ERROR, (L"ERROR: OEMLaunch: "
                            L"Image load from flash memory failed\r\n"
                            ));
                        goto cleanUp;
                        }
                    launch = g_eboot.launchAddress;
                    break;
*/
#endif
                case DOWNLOAD_TYPE_RAM:
                    launch = (UINT32)OEMMapMemAddr(start, launch);
                    break;
					
                case DOWNLOAD_TYPE_FLASHNAND:
                    if (BLFlashDownload(&g_bootCfg, g_kitlDevices) != BL_JUMP)
                        {
                        OALMSG(OAL_ERROR, (L"ERROR: OEMLaunch: "
                            L"Image load from flash memory failed\r\n"
                            ));
                        goto cleanUp;
                        }
					OALMSG(OAL_INFO, (L"\r\nINFO: "
						L"NK.bin downloaded successfully\r\n\r\n"
                        ));
					if(g_BSPtype != BSP_E600_BD)
					{					
						BLHideLogo();
						BLShowLogo(LOGO4);
					}
					launch = g_eboot.launchAddress;
                    break;

                case DOWNLOAD_TYPE_EBOOT:
                case DOWNLOAD_TYPE_XLDR:
				case DOWNLOAD_TYPE_LOGO:
				case DOWNLOAD_TYPE_XEL:
					if(g_BSPtype != BSP_E600_BD)
					{                    
						BLHideLogo();
						BLShowLogo(LOGO3);
					}
					OALMSG(OAL_INFO, (L"\r\nINFO: "
						L"Please recycle device power.\r\n"
                        ));
                    for(;;);
                    break;
                default:
                    OALMSG(OAL_ERROR, (L"ERROR: OEMLaunch: Unknown download type, spin forever\r\n"));
                    for(;;);
                    break;
                }
            break;

#endif

        case OAL_KITL_TYPE_ETH:
            BLEthConfig(pArgs);
            switch (g_eboot.type)
                {
#ifdef IMGMULTIXIP
                case DOWNLOAD_TYPE_EXT:
#endif					
                case DOWNLOAD_TYPE_FLASHNAND:
				case DOWNLOAD_TYPE_FLASHNOR:
                    if (BLFlashDownload(&g_bootCfg, g_kitlDevices) != BL_JUMP)
                        {
                        OALMSG(OAL_ERROR, (L"ERROR: OEMLaunch: "
                            L"Image load from flash memory failed\r\n"
                            ));
                        goto cleanUp;
                        }
                    launch = g_eboot.launchAddress;
                    break;

                case DOWNLOAD_TYPE_RAM:
                    launch = (UINT32)OEMMapMemAddr(start, launch);
                    break;

                case DOWNLOAD_TYPE_EBOOT:
                case DOWNLOAD_TYPE_XLDR:
                    OALMSG(OAL_INFO, (L"INFO: "
                        L"XLDR/EBOOT/IPL downloaded, spin forever\r\n"
                        ));
                    for(;;);
                    break;

				case DOWNLOAD_TYPE_LOGO:
                    OALMSG(OAL_INFO, (L"INFO: "
                        L"Splashcreen logo downloaded, spin forever\r\n"
                        ));
                    for(;;);
                    break;

                default:
                    OALMSG(OAL_ERROR, (L"ERROR: OEMLaunch: Unknown download type, spin forever\r\n"));
                    for(;;);
                    break;
                }
            break;

        default:        
            launch = g_eboot.launchAddress;
            break;
        }

#ifndef BSP_NO_NAND_IN_SDBOOT
    /* if loading from NAND then do not need to flash NAND again */	
    if ((g_bootCfg.flashNKFlags & ENABLE_FLASH_NK) && 
        /* if loading from NAND then do not need to flash NAND again */      
        (g_eboot.bootDeviceType != OAL_KITL_TYPE_FLASH) && 
	  (start != (IMAGE_WINCE_CODE_CA + NAND_ROMOFFSET)) &&
	  (start != (IMAGE_WINCE_CODE_CA + NOR_ROMOFFSET))) 
    {
        if( !WriteFlashNK(start, size))
	        OALMSG(OAL_ERROR, (L"ERROR: OEMLaunch: "
	            L"Flash NK.bin failed, start=%x, size=%x\r\n", start, size
	            ));
    }
#endif

    // Check if we get launch address
    if (launch == (UINT32)INVALID_HANDLE_VALUE)
        {
        OALMSG(OAL_ERROR, (L"ERROR: OEMLaunch: "
            L"Unknown image launch address, spin forever\r\n"
            ));
        for(;;);
        }        

    pArgs->ErrorCode = g_ErrorCode;
	
	// Print message, flush caches and jump to image
    OALLog(
        L"Launch Windows CE image by jumping to 0x%08x...\r\n\r\n", launch
        );
	
	OEMDeinitDebugSerial();
    OEMPlatformDeinit();
	JumpTo(OALVAtoPA((UCHAR*)launch));

cleanUp:
    return;
}

//------------------------------------------------------------------------------
//
//  Function:   OEMMultiBinNotify
//
VOID
OEMMultiBinNotify(
    MultiBINInfo *pInfo
    )
{
    BOOL rc = FALSE;
    UINT32 base = OALVAtoPA((UCHAR*)IMAGE_WINCE_CODE_CA);
    UINT32 start, length;
    UINT32 ix;


    OALMSGS(OAL_FUNC, (
        L"+OEMMultiBinNotify(0x%08x -> %d)\r\n", pInfo, pInfo->dwNumRegions
        ));
    OALMSG(OAL_INFO, (
        L"Download file information:\r\n"
        ));
    OALMSG(OAL_INFO, (
        L"-----------------------------------------------------------\r\n"
        ));

    // Copy information to EBOOT structure and set also save address
    g_eboot.numRegions = pInfo->dwNumRegions;
    for (ix = 0; ix < pInfo->dwNumRegions; ix++)
        {
        g_eboot.region[ix].start = pInfo->Region[ix].dwRegionStart;
        g_eboot.region[ix].length = pInfo->Region[ix].dwRegionLength;
        g_eboot.region[ix].base = base;
        base += g_eboot.region[ix].length;
        OALMSG(OAL_INFO, (
            L"[%d]: Address=0x%08x  Length=0x%08x  Save=0x%08x\r\n",
            ix, g_eboot.region[ix].start, g_eboot.region[ix].length,
            g_eboot.region[ix].base
            ));
        }
    OALMSG(OAL_INFO, (
        L"-----------------------------------------------------------\r\n"
        ));

#ifndef IMGMULTIXIP

    // Determine type of image downloaded
    if (g_eboot.numRegions > 1) 
        {
        OALMSG(OAL_ERROR, (L"ERROR: MultiXIP image is not supported\r\n"));
        goto cleanUp;
        }
#endif

    base = g_eboot.region[0].base;
    start = g_eboot.region[0].start;
    length = g_eboot.region[0].length;
    
    if (start == IMAGE_XLDR_CODE_PA)
        {
        g_eboot.type = DOWNLOAD_TYPE_XLDR;
        memset(OALPAtoCA(base), 0xFF, length);
        } 
    else if (start == IMAGE_EBOOT_CODE_CA)
        {
        g_eboot.type = DOWNLOAD_TYPE_EBOOT;
        memset(OALPAtoCA(base), 0xFF, length);
        }
    else if (start == (IMAGE_WINCE_CODE_CA + NAND_ROMOFFSET))
        {
        g_eboot.type = DOWNLOAD_TYPE_FLASHNAND;
        memset(OALPAtoCA(base), 0xFF, length);
        } 
#ifdef IMGMULTIXIP
    else if (start == (IMAGE_WINCE_EXT_CA))
        {
        g_eboot.type = DOWNLOAD_TYPE_EXT;
        memset(OALPAtoCA(base), 0xFF, length);
        } 
#endif	
	else if (start == (IMAGE_WINCE_CODE_CA + NOR_ROMOFFSET))
        {
        g_eboot.type = DOWNLOAD_TYPE_FLASHNOR;
        memset(OALPAtoCA(base), 0xFF, length);
        } 
	else if (start == 0) // Probably a NB0 file, let's find out
		{
		// Convert the file name to lower case
		CHAR szFileName[MAX_PATH];
		int i = 0;
		int fileExtPos = 0;

		while ((pInfo->Region[0].szFileName[i] != '\0') && (i < MAX_PATH))
		{
			if((pInfo->Region[0].szFileName[i] >= 'A') && (pInfo->Region[0].szFileName[i] <= 'Z')) 
			{
				szFileName[i] = (pInfo->Region[0].szFileName[i] - 'A' + 'a'); 
			}
			else
			{
				szFileName[i] = pInfo->Region[0].szFileName[i];
			}

			// Keep track of file extension position
			if (szFileName[i] == '.')
			{
				fileExtPos = i;
			}
			i++;
		}
	
		// Copy string terminator as well
		szFileName[i] = pInfo->Region[0].szFileName[i];
		
		OALMSG(OAL_INFO, (L"NB0 file: %s\r\n", g_bootCfg.filename));

		// Remap the start address to the correct NAND location
		g_eboot.region[0].start = IMAGE_EBOOT_CODE_CA;
		g_eboot.region[0].base = OALVAtoPA((UCHAR*)IMAGE_WINCE_CODE_CA);
		pInfo->Region[0].dwRegionStart = g_eboot.region[0].start;
		pInfo->dwNumRegions = 1;
		
		// Check if we support this file
		if(wcsncmp(g_bootCfg.filename, LOGO1_NB0_FILE, LOGO_NB0_FILE_LEN) == 0)
		{
			g_eboot.region[0].length = LOGO_NB0_FILE_SIZE;
			pInfo->Region[0].dwRegionLength = g_eboot.region[0].length;			
			g_eboot.type = DOWNLOAD_TYPE_LOGO;
		}
		else if(wcsncmp(g_bootCfg.filename, XEL_NB0_FILE, XEL_NB0_FILE_LEN) == 0)
		{
			g_eboot.region[0].length = MAX_NB0_FILE_SIZE;
			pInfo->Region[0].dwRegionLength = g_eboot.region[0].length;
			g_eboot.type = DOWNLOAD_TYPE_XEL;
		}
		else
		{
			OALMSG(OAL_ERROR, (L"Unsupported downloaded file: %s\r\n", g_bootCfg.filename));
			goto cleanUp;
		}
		}
    else 
        {
        g_eboot.type = DOWNLOAD_TYPE_RAM;
        }

    OALMSG(OAL_INFO, (
        L"Download file type: %d\r\n", g_eboot.type
    ));
    
    rc = TRUE;

cleanUp:
    if (!rc) 
        {
        OALMSG(OAL_ERROR, (L"Spin for ever...\r\n"));
        for(;;);
        }
    OALMSGS(OAL_FUNC, (L"-OEMMultiBinNotify\r\n"));
}

//------------------------------------------------------------------------------
//
//  Function:  OEMMapMemAddr
//
//  This function maps image relative address to memory address. It is used
//  by boot loader to verify some parts of downloaded image.
//
//  EBOOT mapping depends on download type. Download type is
//  set in OMEMultiBinNotify.
//
UINT8* 
OEMMapMemAddr(
    DWORD image,
    DWORD address
    )
{
    UINT8 *pAddress = NULL;

    OALMSG(OAL_FUNC, (L"+OEMMapMemAddr(0x%08x, 0x%08x)\r\n", image, address));
    
    switch (g_eboot.type) {

        
    case DOWNLOAD_TYPE_XLDR:
    case DOWNLOAD_TYPE_EBOOT:   
	case DOWNLOAD_TYPE_LOGO:
	case DOWNLOAD_TYPE_XEL:
        //  Map to scratch RAM prior to flashing
        pAddress = (UINT8*)g_eboot.region[0].base + (address - image);
        break;

    case DOWNLOAD_TYPE_RAM:            
        //  RAM based NK.BIN and EBOOT.BIN files are given in virtual memory addresses
        pAddress = (UINT8*)address;
        break;

    case DOWNLOAD_TYPE_FLASHNAND:
        pAddress = (UINT8*) (address - NAND_ROMOFFSET);
        break;

	case DOWNLOAD_TYPE_FLASHNOR:
        pAddress = (UINT8*) (address - NOR_ROMOFFSET);
        break;
        
#ifdef IMGMULTIXIP
	case DOWNLOAD_TYPE_EXT:
        pAddress = (UINT8*) (address);
        break;
#endif

    default:
        OALMSG(OAL_ERROR, (L"ERROR: OEMMapMemAddr: "
            L"Invalid download type %d\r\n", g_eboot.type
        ));

    }


    OALMSGS(OAL_FUNC, (L"-OEMMapMemAddr(pAddress = 0x%08x)\r\n", pAddress));
    return pAddress;
}

//------------------------------------------------------------------------------
//
//  Function:  OEMIsFlashAddr
//
//  This function determines whether the address provided lies in a platform's
//  flash or RAM address range.
//
//  EBOOT decision depends on download type. Download type is
//  set in OMEMultiBinNotify.
//
BOOL
OEMIsFlashAddr(
    ULONG address
    )
{
    BOOL rc;

	UNREFERENCED_PARAMETER(address);

    OALMSG(OAL_FUNC, (L"+OEMIsFlashAddr(0x%08x)\r\n", address));

    // Depending on download type
    switch (g_eboot.type)
        {
        case DOWNLOAD_TYPE_XLDR:
        case DOWNLOAD_TYPE_EBOOT:
		case DOWNLOAD_TYPE_LOGO:
		case DOWNLOAD_TYPE_XEL:
		case DOWNLOAD_TYPE_FLASHNAND:
        case DOWNLOAD_TYPE_FLASHNOR:
        case DOWNLOAD_TYPE_EXT:
            rc = TRUE;
            break;
        default:
            rc = FALSE;
            break;
        }

    OALMSG(OAL_FUNC, (L"-OEMIsFlashAddr(rc = %d)\r\n", rc));
    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:   OEMReadData
//
//  This function is called to read data from the transport during
//  the download process.
//
BOOL
OEMReadData(
    ULONG size, 
    UCHAR *pData
    )
{
    BOOL rc = FALSE;
    switch (g_eboot.bootDeviceType)
        {
        #if BUILDING_EBOOT_SD
        case BOOT_SDCARD_TYPE:
            rc = BLSDCardReadData(size, pData);
            break;
        #endif
        case OAL_KITL_TYPE_ETH:
            rc = BLEthReadData(size, pData);
            break;
        }
    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:  OEMShowProgress
//
//  This function is called during the download process to visualise
//  download progress.
//
VOID
OEMShowProgress(
    ULONG packetNumber
    )
{
    UNREFERENCED_PARAMETER(packetNumber);
    RETAILMSG(1,(TEXT(".")));
}

//------------------------------------------------------------------------------
//
//  Function:  OALGetTickCount
//
UINT32
OALGetTickCount(
    )
{
    OMAP_GPTIMER_REGS *pGPTimerRegs = OALPAtoUA(OMAP_GPTIMER1_REGS_PA);
    return INREG32(&pGPTimerRegs->TCRR) >> 5;
}

//------------------------------------------------------------------------------
//
//  Function:  OEMEthGetSecs
//
//  This function returns relative time in seconds.
//
DWORD
OEMEthGetSecs(
    )
{
    return OALGetTickCount()/1000;
}


void
GetDisplayResolutionFromBootArgs(
    DWORD * pDispRes
    )
{
    *pDispRes=g_bootCfg.displayRes;
}

BOOL
IsDVIMode()
{
    DWORD dispRes;    
    GetDisplayResolutionFromBootArgs(&dispRes);
    if (dispRes==OMAP_LCD_DEFAULT)
        return FALSE;
    else
        return TRUE;        
}

DWORD 
ConvertCAtoPA(DWORD * va)
{   
    return OALVAtoPA(va);
}

//------------------------------------------------------------------------------
//
//  Function:  CalcCrc32
//
DWORD CalcCrc32(DWORD Crc32, LPBYTE pData, DWORD count)
{
	LPBYTE pCrc32Data = pData;

	while(count--)      /* CRC each byte until count reach 0 */
	{
		Crc32 = ((Crc32) >> 8) ^ Crc32Table[(DWORD)*pCrc32Data ^ ((Crc32) & 0x000000FF)];
		pCrc32Data++;
	}
	return(Crc32);
}

//------------------------------------------------------------------------------
//
//  Function:  VerifyXELNB0
//
BOOL VerifyXELNB0(LPBYTE lpNB0Data)
{
	LPBYTE		lpbData;
	LPDWORD		lpdwTemp;
	DWORD		i;
	DWORD		dwChecksum = 0;
	DWORD		Crc32;
	DWORD		dwLogo1Size, dwLogo2Size;

	//-------------- Check Xloader CRC ----------------------
	lpbData = lpNB0Data + XLOADER_OFFSET;
	Crc32 = CalcCrc32(INITIAL_CRC32, lpbData, XLOADER_SIZE - CRC32_SIZE);
	lpdwTemp = (LPDWORD)(lpNB0Data + XLOADER_CRC_OFFSET);
	if(*lpdwTemp != Crc32)
	{
		KITLOutputDebugString ("VerifyXELNB0: Invalid Xloader CRC32, Calculated = 0x%x, Actual = 0x%x\r\n", Crc32, *lpdwTemp);
		return FALSE;
	}
	else
		KITLOutputDebugString ("VerifyXELNB0: Xloader CRC32 = 0x%x\r\n", Crc32);

	//-------------- Check Eboot CRC ----------------------
	lpbData = lpNB0Data + EBOOT_OFFSET;
	Crc32 = CalcCrc32(INITIAL_CRC32, lpbData, EBOOT_SIZE - CRC32_SIZE);
	lpdwTemp = (LPDWORD)(lpNB0Data + EBOOT_CRC_OFFSET);
	if(*lpdwTemp != Crc32)
	{
		KITLOutputDebugString ("VerifyXELNB0: Invalid Eboot CRC32, Calculated = 0x%x, Actual = 0x%x\r\n", Crc32, *lpdwTemp);
		return FALSE;
	}
	else
		KITLOutputDebugString ("VerifyXELNB0: Eboot CRC32 = 0x%x\r\n", Crc32);
	
	//-------------- Check Logo1 CRC ----------------------
	lpbData = lpNB0Data + LOGO1_OFFSET;
	Crc32 = CalcCrc32(INITIAL_CRC32, lpbData, LOGO_BITMAP_SIZE - CRC32_SIZE);
	lpdwTemp = (LPDWORD)(lpNB0Data + LOGO1_CRC32_OFFSET);
	if(*lpdwTemp != Crc32)
	{
		KITLOutputDebugString ("VerifyXELNB0: Invalid Logo1 CRC32, Calculated = 0x%x, Actual = 0x%x\r\n", Crc32, *lpdwTemp);
		return FALSE;
	}
	else
		KITLOutputDebugString ("VerifyXELNB0: Logo1 CRC32 = 0x%x\r\n", Crc32);

	//------------- Check Logo2 CRC ---------------------------------
	lpbData = lpNB0Data + LOGO2_OFFSET;
	Crc32 = CalcCrc32(INITIAL_CRC32, lpbData, LOGO_BITMAP_SIZE - CRC32_SIZE);
	lpdwTemp = (LPDWORD)(lpNB0Data + LOGO2_CRC32_OFFSET);
	if(*lpdwTemp != Crc32)
	{
		KITLOutputDebugString ("VerifyXELNB0: Invalid Logo2 CRC32, Calculated = 0x%x, Actual = 0x%x\r\n", Crc32, *lpdwTemp);
		return FALSE;
	}
	else
		KITLOutputDebugString ("VerifyXELNB0: Logo2 CRC32 = 0x%x\r\n", Crc32);
	
	//------------- Verify Logo1 size ---------------------------------
	lpdwTemp = (LPDWORD)(lpNB0Data + LOGO1_SIZE_OFFSET);			// set pointer to size of the logo
	dwLogo1Size = *lpdwTemp;										// get size of the logo
	
	// Check if image fit (last sector used for configuration)
    if (dwLogo1Size > ( LOGO_BITMAP_SIZE - GetFlashSectorSize()))
        {
        OALMSG(OAL_ERROR, (L"ERROR: OEMWriteFlash: "
            L"LOGO1 too big (size 0x%08x, maxSize = 0x%08x)\r\n",
            dwLogo1Size, LOGO_BITMAP_SIZE - GetFlashSectorSize()
            ));
        return FALSE;
        }
	
	//------------- Verify Logo2 size ---------------------------------
	lpdwTemp = (LPDWORD)(lpNB0Data + LOGO2_SIZE_OFFSET);			// set pointer to size of the logo
	dwLogo2Size = *lpdwTemp;

	// Check if image fit (last sector used for configuration)
    if (dwLogo2Size > ( LOGO_BITMAP_SIZE - GetFlashSectorSize()))
        {
        OALMSG(OAL_ERROR, (L"ERROR: OEMWriteFlash: "
            L"LOGO2 too big (size 0x%08x, maxSize = 0x%08x)\r\n",
            dwLogo2Size, LOGO_BITMAP_SIZE - GetFlashSectorSize()
            ));
        return FALSE;
        }
	
	//-------------- Verify checksum of the entire file --------------
	lpbData = lpNB0Data;
	for(i = 0; i < NMI_HEADER_SIZE-NMI_CHECKSUM_SIZE; i++)
	{
		dwChecksum += (DWORD)*lpbData;
		lpbData++;
	}

	lpbData =  lpNB0Data + NMI_HEADER_SIZE;
	for(i = 0; i < (BL_IMAGE_SIZE + (NUM_BITMAP_LOGO*LOGO_BITMAP_SIZE)); i++)
	{
		dwChecksum += (DWORD)*lpbData;
		lpbData++;
	}
	
	KITLOutputDebugString ("VerifyXELNB0: Checksum = 0x%x\r\n", dwChecksum);

	lpdwTemp = (LPDWORD)(lpNB0Data + NMI_HEADER_SIZE-NMI_CHECKSUM_SIZE);
	if(((long)dwChecksum + (long)(*lpdwTemp)) != 0)
	{
		KITLOutputDebugString ("VerifyXELNB0 ERROR: Failed verifying checksum, Header Checksum = 0x%x\r\n", *lpdwTemp);
		return FALSE;
	}
	
	//Shift the booloader image back by NMI_HEADER_SIZE for flashing
	memcpy(lpNB0Data, (lpNB0Data + NMI_HEADER_SIZE), BL_IMAGE_SIZE + (NUM_BITMAP_LOGO*LOGO_BITMAP_SIZE));
	
	return TRUE;
}

#if BSP_AURA_PROTOTYPE

//Register map of STMPE1208S
#define	GPIO_STA_L			0x1E
#define	GPIO_STA_H			0x1F
#define	GPIO_CFG_L			0x20
#define	GPIO_CFG_H			0x21
#define	GPIO_DIR_L			0x22
#define	GPIO_DIR_H			0x23
#define	CTRL_1				0x24
#define	CTRL_2				0x25
#define	INT_MASK			0x26
#define INT_CLR				0x27
#define BEEP_PERIOD			0x28
#define BEEP_FREQUENCY		0x29
#define CAL_INTERVAL		0x2A
#define EXT_INT_EN			0x2B
#define EXT_INT_POL			0x2C
#define INT_PENDING			0x77
#define GPIO_IN_L			0x78
#define GPIO_IN_H			0x79

#define LEFT_KEYPAD_SLAVE_ADDR		0x5B
#define RIGHT_KEYPAD_SLAVE_ADDR		0x58

// GPIO pins for keypad reset
#define KEYPAD_LEFT_RESET_GPIO		138		// SPARE2
#define KEYPAD_RIGHT_RESET_GPIO		18		// SPARE1

#define KEYPAD_DEBOUNCE_TIME		10
#define KEYPAD_I2C_TIMEOUT			15
#define KEYPAD_INTERRUPT_PRIORITY	99

#define KEYPAD_INT_MASK_ALL			0x67
#define KEYPAD_TOUCH_INT_MASK		0x47

#define MAX_KEYPAD_INIT_RETRIES		2

#define	LEFT_KEYPAD					0
#define RIGHT_KEYPAD				1
#define BOTH_KEYPADS				2

typedef struct
{
	HANDLE			hI2CPort3;
}KEYPAD_DEVICE;

KEYPAD_DEVICE	LKeypadDevice;
KEYPAD_DEVICE	RKeypadDevice;

//*****************************************************************************
//  KeypadWriteByte
//*****************************************************************************
DWORD KeypadWriteByte(HANDLE hI2C, DWORD Reg, UINT8 Byte)
{
	DWORD rc = 1;
	UINT8 ByteIn;

	if(I2CWrite(hI2C, Reg, (VOID*)&Byte, 1) == 1)
	{
		if(I2CRead(hI2C, Reg, (VOID*)&ByteIn, 1) == 1)
		{
			//OALLog(L"Reg 0x%02X = 0x%02x\r\n", Reg, ByteIn);
			if(Byte == ByteIn)
			{
				rc = 0;
			}
		}
	}
	return rc;
}

//*****************************************************************************
//  KeypadWriteByteNoRead
//*****************************************************************************

BOOL KeypadWriteByteNoRead(HANDLE hI2C, DWORD Reg, UINT8 Byte)
{
	if(I2CWrite(hI2C, Reg, (VOID*)&Byte, 1) == 1)
		return TRUE;
	else
		return FALSE;
}

//*****************************************************************************
//  ReadKeypad
//*****************************************************************************

BOOL ReadKeypad(DWORD Keypad, DWORD *ButtonFlags)
{
	UINT8 LData = 0x07, RData = 0x1F, Data = 0;
	DWORD nReadBytes;

	if((Keypad == LEFT_KEYPAD) || (Keypad != RIGHT_KEYPAD))
	{
		if(!LKeypadDevice.hI2CPort3)
		{
			OALLog(L"ReadKeypad ERROR: Invalid I2C Handle for Left Keypad\r\n");
			return FALSE;
		}
		nReadBytes = I2CRead(LKeypadDevice.hI2CPort3, GPIO_IN_L, (VOID*)&LData, 1);
		if(nReadBytes != 1)
		{
			OALLog(L"ReadKeypad ERROR: Can not read from LEFT Keypad\r\n");
			return FALSE;
		}
	}
	
	if((Keypad == RIGHT_KEYPAD) || (Keypad != LEFT_KEYPAD))
	{
		if(!RKeypadDevice.hI2CPort3)
		{
			OALLog(L"ReadKeypad: Invalid I2C Handle for Right Keypad\r\n");
			return FALSE;
		}
		nReadBytes = I2CRead(RKeypadDevice.hI2CPort3, GPIO_IN_L, (VOID*)&RData, 1);
		if(nReadBytes != 1)
		{
			OALLog(L"ReadKeypad ERROR: Can not read from RIGHT Keypad\r\n");
			return FALSE;
		}
	}

	if(Keypad == LEFT_KEYPAD)
		*ButtonFlags = (DWORD)(LData & 0x7);
	else if(Keypad == RIGHT_KEYPAD)
		*ButtonFlags = (DWORD)(RData & 0x1F);
	else
	{
// Maskings for Left Keypad
#define ON_OFF_MASK		0x01
#define BRI_ADJ_MASK	0x02
#define MANUAL_MASK		0x04

// Maskings for Right Keypad
#define CANCEL_MASK		0x01
#define ACCEPT_MASK		0x02
#define UP_MASK			0x04
#define DOWN_MASK		0x08
#define RESET_MASK		0x10		
		
		Data |= (LData & ON_OFF_MASK);
		Data |= (RData & UP_MASK) >> 1;
		Data |= (LData & BRI_ADJ_MASK) << 1;
		Data |= (RData & ACCEPT_MASK) << 2;
		Data |= (LData & MANUAL_MASK) << 2;
		Data |= (RData & RESET_MASK) << 1;
		Data |= (RData & CANCEL_MASK) << 6;
		Data |= (RData & DOWN_MASK) << 4;

		*ButtonFlags = (DWORD)Data;
	}

	return TRUE;
}

//*****************************************************************************
//  ResetKeypad
//*****************************************************************************
static VOID ResetKeypad(HANDLE hGPIO, DWORD GpioPin)
{
	GPIOClrBit(hGPIO, GpioPin);
	OALStall(20000);
	GPIOSetBit(hGPIO, GpioPin);
}

//*****************************************************************************
//  InitLeftKeypadI2C
//*****************************************************************************
BOOL InitLeftKeypadI2C(HANDLE hGPIO)
{
	HANDLE hI2C;
	BOOL rc = FALSE;
	DWORD retry = 0;
	DWORD returnVal = 0;
	BOOL success = FALSE;

	UNREFERENCED_PARAMETER(hGPIO);
	//OALLog(L"Resetting LEFT keypad ...\r\n");
	//ResetKeypad(hGPIO, KEYPAD_LEFT_RESET_GPIO);

	if(LKeypadDevice.hI2CPort3)
	{
		hI2C = LKeypadDevice.hI2CPort3;
	}
	else
	{
		hI2C = I2COpen(OMAP_DEVICE_I2C3);
		if (hI2C == NULL) 
		{
			OALLog(L"ERROR: Failed open I2C port 3 for Left Keypad\r\n");
			goto cleanUp;
		}
		LKeypadDevice.hI2CPort3 = hI2C;
	}
	
	OALLog(L"Initializing LEFT keypad ...\r\n");
	
	I2CSetSlaveAddress(hI2C, LEFT_KEYPAD_SLAVE_ADDR);
	I2CSetSubAddressMode(hI2C, I2C_SUBADDRESS_MODE_8);
	I2CSetBaudIndex(hI2C, SLOWSPEED_MODE);
	I2CSetTimeout(hI2C, KEYPAD_I2C_TIMEOUT);

	while(++retry <= MAX_KEYPAD_INIT_RETRIES)
	{
		returnVal += KeypadWriteByte(hI2C, GPIO_CFG_L, 0xFF);
		returnVal += KeypadWriteByte(hI2C, GPIO_CFG_H, 0x0F);
		returnVal += KeypadWriteByte(hI2C, GPIO_DIR_L, 0x0F);
		returnVal += KeypadWriteByte(hI2C, GPIO_DIR_H, 0x00);

		returnVal += KeypadWriteByte(hI2C, CTRL_1, 0x61);		// 400 KHz
		returnVal += KeypadWriteByte(hI2C, CTRL_2, 0x05);		// Enable internal regulator
		
		//returnVal += KeypadWriteByte(hI2C, EXT_INT_EN, 0x07);
		//returnVal += KeypadWriteByte(hI2C, EXT_INT_POL, 0x07);

		returnVal += KeypadWriteByte(hI2C, INT_MASK, KEYPAD_INT_MASK_ALL);

		if(returnVal > 0)
		{
			OALLog(L"Try %d Failed, returnVal = %d\r\n", retry, returnVal);
			returnVal = 0;
		}
		else
		{
			success = TRUE;
			break;
		}
	}

	if(success)
	{		
		ReadKeypad(LEFT_KEYPAD, &returnVal);
		OALLog(L"LKeypad Inputs = 0x%02X\r\n", returnVal);
	}
	else
	{
		OALLog(L"Failed Initializing Keypad I2C\r\n");
		goto cleanUp;
	}
	rc = TRUE;

cleanUp:
	return rc;
}

//*****************************************************************************
//  InitRightKeypadI2C
//*****************************************************************************
BOOL InitRightKeypadI2C(HANDLE hGPIO)
{
	HANDLE hI2C;
	BOOL rc = FALSE;
	DWORD retry = 0;
	DWORD returnVal = 0;
	BOOL success = FALSE;

	UNREFERENCED_PARAMETER(hGPIO);
	//OALLog(L"Resetting RIGHT keypad ...\r\n");
	//ResetKeypad(hGPIO, KEYPAD_RIGHT_RESET_GPIO);

	if(RKeypadDevice.hI2CPort3)
	{
		hI2C = RKeypadDevice.hI2CPort3;
	}
	else
	{
		hI2C = I2COpen(OMAP_DEVICE_I2C3);
		if (hI2C == NULL) 
		{
			OALLog(L"ERROR: Failed open I2C port 3 for Right Keypad\r\n");
			goto cleanUp;
		}
		RKeypadDevice.hI2CPort3 = hI2C;
	}
	
	OALLog(L"Initializing RIGHT keypad ...\r\n");

	I2CSetSlaveAddress(hI2C, RIGHT_KEYPAD_SLAVE_ADDR);
	I2CSetSubAddressMode(hI2C, I2C_SUBADDRESS_MODE_8);
	I2CSetBaudIndex(hI2C, SLOWSPEED_MODE);
	I2CSetTimeout(hI2C, KEYPAD_I2C_TIMEOUT);

	while(++retry <= MAX_KEYPAD_INIT_RETRIES)
	{
		returnVal += KeypadWriteByte(hI2C, GPIO_CFG_L, 0xFF);
		returnVal += KeypadWriteByte(hI2C, GPIO_CFG_H, 0x0F);
		returnVal += KeypadWriteByte(hI2C, GPIO_DIR_L, 0x1F);
		returnVal += KeypadWriteByte(hI2C, GPIO_DIR_H, 0x00);

		returnVal += KeypadWriteByte(hI2C, CTRL_1, 0x61);		// 400 KHz
		returnVal += KeypadWriteByte(hI2C, CTRL_2, 0x05);		// Enable internal regulator
		
		//returnVal += KeypadWriteByte(hI2C, EXT_INT_EN, 0x1F);
		//returnVal += KeypadWriteByte(hI2C, EXT_INT_POL, 0x1F);

		returnVal += KeypadWriteByte(hI2C, INT_MASK, KEYPAD_INT_MASK_ALL);

		if(returnVal > 0)
		{
			OALLog(L"Try %d Failed, returnVal = %d\r\n", retry, returnVal);
			returnVal = 0;
		}
		else
		{
			success = TRUE;
			break;
		}
	}

	if(success)
	{		
		ReadKeypad(RIGHT_KEYPAD, &returnVal);
		OALLog(L"RKeypad Inputs = 0x%02X\r\n", returnVal & 0x1F);
	}
	else
	{
		OALLog(L"Failed Initializing Keypad I2C\r\n");
		goto cleanUp;
	}
	rc = TRUE;

cleanUp:
	return rc;
}

//*****************************************************************************
//  InitKeypadI2C
//*****************************************************************************
BOOL InitKeypadI2C(HANDLE hGPIO, DWORD Keypad)
{
	BOOL rc = FALSE;
	
	switch(Keypad)
	{
		case LEFT_KEYPAD:
			rc = InitLeftKeypadI2C(hGPIO);
			break;
		case RIGHT_KEYPAD:
			rc = InitRightKeypadI2C(hGPIO);
			break;
		case BOTH_KEYPADS: default:
			rc = InitLeftKeypadI2C(hGPIO);
			if(!InitRightKeypadI2C(hGPIO))
			{
				rc = FALSE;
			}
			break;
	}

	return rc;
}

//*****************************************************************************
//  ReadVentButtonsI2C
//*****************************************************************************
static DWORD ReadVentButtonsI2C()
{
	DWORD ButtonFlags;

	if(!ReadKeypad(BOTH_KEYPADS, &ButtonFlags))
		ButtonFlags = 0xFF;

	return ButtonFlags;
}

#endif	// BSP_AURA_PROTOTYPE



// command byte to register definition
#define ACCESS_TO_GP0		0x00 // command to access GPIO port Register 0
#define ACCESS_TO_GP1		0x01 // command to access GPIO port Register 1
#define ACCESS_TO_OLAT0		0x02 // command to access Output Latch Register 0
#define ACCESS_TO_OLAT1		0x03 // command to access Output Latch Register 1
#define ACCESS_TO_IPOL0		0x04 // command to access Input Polarity Register 0
#define ACCESS_TO_IPOL1		0x05 // command to access Input Polarity Register 1
#define ACCESS_TO_IODIR0	0x06 // command to access I/O Direction Register 0
#define ACCESS_TO_IODIR1	0x07 // command to access I/O Direction Register 1
#define ACCESS_TO_INCON0	0x08 // command to Pull Up Resistors Enable 0
#define ACCESS_TO_INCON1	0x09 // command to Pull Up Resistors Enable  1
#define ACCESS_TO_INTCAP0	0x0A // command to access Interrupt Capture Register 0
#define ACCESS_TO_INTCAP1	0x0B // command to access Interrupt Capture Register 1

#define PIN_CONFIGURATION_MASK0			0xFF
#define PIN_CONFIGURATION_MASK1			0x70

#define I2C_SUBADDRESS_MODE_0			0
#define I2C_SUBADDRESS_MODE_8			1
#define SLOWSPEED_MODE					0       // corresponds to 100 khz

#define ON_BOARD_BACKLIGHT_ADDRESS		(0x98 >> 1)
#define ON_BOARD_GPIO_EXPANDER_ADDRESS	(0x4E >> 1)


//*****************************************************************************
//  InitBackLightI2C
//		
//*****************************************************************************
static VOID SetBackLightI2C(HANDLE hI2C, BYTE Level)
{
	BYTE Value[2];
	DWORD nBytes = 0;

	// set I2C settings for backlight
	I2CSetSlaveAddress(hI2C, ON_BOARD_BACKLIGHT_ADDRESS);
	I2CSetSubAddressMode(hI2C, I2C_SUBADDRESS_MODE_0);
	
	Value[0] = Level >> 4;
	Value[1] = Level << 4;

	nBytes = I2CWrite(hI2C, 0, (VOID*)Value, 2);
	if(nBytes != 2)
	{
		OALLog(L"ERROR: Failed setting backlight, nBytes=%d\r\n", nBytes);
	}
	
	// reset I2C settings for Keypad
	I2CSetSlaveAddress(hI2C, ON_BOARD_GPIO_EXPANDER_ADDRESS);
	I2CSetSubAddressMode(hI2C, I2C_SUBADDRESS_MODE_8);
}


//*****************************************************************************
//  InitKeypadI2C
//		
//*****************************************************************************
static HANDLE InitKeypadI2C()
{
	BYTE TxData;
	HANDLE hI2C = NULL;
	DWORD nBytes = 0;

	OALLog(L"ReadVentButtonsI2C+\r\n");
	OALStall(1000000);

	hI2C = I2COpen(OMAP_DEVICE_I2C3);
	if (hI2C == NULL) 
	{
		OALLog(L"ERROR: Failed open I2C port 3 for reading buttons\r\n");
		return 0;
	}
	
	I2CSetSlaveAddress(hI2C, ON_BOARD_GPIO_EXPANDER_ADDRESS);
	I2CSetSubAddressMode(hI2C, I2C_SUBADDRESS_MODE_8);
	I2CSetBaudIndex(hI2C, SLOWSPEED_MODE);
	I2CSetTimeout(hI2C, 40);

	TxData = (BYTE)PIN_CONFIGURATION_MASK0;
	nBytes = I2CWrite(hI2C, (UINT32)ACCESS_TO_IODIR0, (VOID*)&TxData, 1);
	if(nBytes != 1)
	{
		OALLog(L"ERROR: Failed configuring GPIO Expander Port 0, nBytes=%d\r\n", nBytes);
		return 0;
	}

	TxData = (BYTE)PIN_CONFIGURATION_MASK1;
	nBytes = I2CWrite(hI2C, ACCESS_TO_IODIR1, (VOID*)&TxData, 1);
	if(nBytes != 1)
	{
		OALLog(L"ERROR: Failed configuring GPIO Expander Port 1, nBytes=%d\r\n", nBytes);
		return 0;
	}

	// Enable BackLight
	TxData = 0x0F;
	nBytes = I2CWrite(hI2C, ACCESS_TO_OLAT1, &TxData, 1);

	//TxData = 0xFF;	 // Enable Interrupts for all keys
	TxData = 0x00;	 // Disable Interrupts for all keys
	nBytes = I2CWrite(hI2C, ACCESS_TO_INTCAP0, &TxData, 1);
	if(nBytes != 1)
	{
		OALLog(L"ERROR: Failed configuring GPIO Expander Interrupt, nBytes=%d\r\n", nBytes);
		return 0;
	}
	
	SetBackLightI2C(hI2C, 0);	// also set backlight for full brightness here as a convenience

	return hI2C;
}


//*****************************************************************************
//  ReadVentButtonsI2C
//		
//*****************************************************************************
static DWORD ReadVentButtonsI2C()
{
	DWORD nBytes = 0;
	BYTE ButtonFlags = 0xFF;
	BYTE RxData;
	
	// if already initializing Keypad, skip
	if(!hKeypadI2C)
	{
		hKeypadI2C = InitKeypadI2C();
		OALLog(L"ReadVentButtonsI2C: Init Keypad I2C\r\n");
	}

	if(hKeypadI2C)
	{
		nBytes = I2CRead(hKeypadI2C, ACCESS_TO_GP0, (VOID*)&RxData, 1);
		if(nBytes != 1)
		{
			OALLog(L"ERROR: Failed reading Expander Port 0, nBytes=%d\r\n", nBytes);
			g_ErrorCode |= BUTTON_READ_FAILED;
		}
		else
		{
			ButtonFlags = RxData;
		}
	}

	return ButtonFlags;
}

