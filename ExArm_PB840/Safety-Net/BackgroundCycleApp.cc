#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//============================ C L A S S   D E S C R I P T I O N ====
//@ Class: BackgroundCycleApp - Background cycle testing application
//---------------------------------------------------------------------
//@ Interface-Description
//  This class contains the functionality for performing the safety net
//  tests that are run periodically in the background cycle.  Contained
//  in this class is the application Task(), a standard do-forever task that 
//  is initialized during System Initialization.  The task pends on a
//  periodic message timer that is based on a basic background cycle
//  interval.  
//
//  This class' initialization consists of constructing
//  a list of BackgroundCycleTimer objects which contain
//  the functionality for testing the hardware systems of the ventilator.   
//  BackgroundCycleTimer objects perform their testing on multiples of the
//  basic background task cycle interval.  
//
//  A BackgroundCycleApp exists on both the GUI & BD target.  A separate
//  initialiation list of BackgroundCycleTimer objects defines the
//  differences between systems.
//---------------------------------------------------------------------
//@ Rationale
//  The class provides the ability to monitor the hardware systems of the 
//  ventilator on a continuous basis.  Responsibilities of the safety-net
//  background testing are distributed over this class, a memory test task,
//  and throughout the breath-delivery run time checks.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class provides the following functionality.
//
// 1. Constructs the list of BackgroundCycleTimer objects that link
//    individual test routines to a BackgroundCycleTimer that manages 
//    the schedule of each test.
//
// 2. Provides the application task that runs periodically to trigger the
//    individual BackgroundCycleTimer objects to perform their testing.
//---------------------------------------------------------------------
//@ Fault-Handling
//  N/A
//---------------------------------------------------------------------
//@ Restrictions
//  None
//---------------------------------------------------------------------
//@ Invariants
//  None
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/BackgroundCycleApp.ccv   25.0.4.0   19 Nov 2013 14:20:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 014  By: rpr   Date:  05-Nov-2010    SCR Number: 6690
//    Project:  PROX
//    Description:
//      Reverted revision 013.  The clock variance test has been removed so the 
//		test of the rtc has been brought back.  
//
//  Revision: 013  By: gdc   Date:  18-Aug-2009    SCR Number: 6147
//    Project:  XB
//    Description:
//      Removed background Time-of-Day clock test. Moved this function to
// 		the cycle-to-cycle and interval time variance tests in BdMonitoredData.
//
//  Revision: 012  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
//
//  Revision: 011  By: gdc   Date:  08-Jan-2001    DR Number: 5493
//    Project:  GuiComms
//    Description:
//      Modified for new TouchDriver interface.
//
//  Revision: 010  By: iv    Date:  20-May-1999    DR Number: DCS 5397
//    Project:  ATC (840)
//    Description:
//      Added initialization for Dac Wrap to be used in the BD ADC test.
//
//  Revision: 009  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 008    By: syw  Date: 09-APR-1998  DR Number: 5048
//    Project: Sigma (R8027)
//    Description:
//       Eliminate TIMING_TEST code.
//
//  Revision: 007  By: pc    Date:  22-Oct-1997    DR Number: DCS 2570
//      Project:  Sigma (840)
//      Description:
//        Removed all SIGMA_INTEGRATION compiler options, which are not used exclusively by the 
//          Sigma System Integration Test (aka: TestCommand Group) program
//
//  Revision: 006  By: iv    Date:  06-Aug-1997    DR Number: DCS 2032
//      Project:  Sigma (840)
//      Description:
//        Added an initial 10 seconds delay to the battery test to accomodate
//        for the new battery specifications.
//
//  Revision: 005   By: sp   Date: 9-July-1997    DR Number: 2387
//    Project:  Sigma (840)  
//    Description:
//      Added integration test code.
//
//  Revision: 004   By: iv    Date: 06-May-1997   DR Number: DCS 2040 
//    Project:  Sigma (R8027)
//    Description:
//      Changed BK_TEST_28SEC_DELAY to BK_TEST_60SEC_DELAY.
//
//  Revision: 003    By: by    Date: 18-Feb-1996  DR Number: 1772 
//    Project:  Sigma (840)
//    Description:
//        Process touch screen status request failures and touch
//        failures differently.  A touch screen status failure is
//        now reported and logged as a failure immediately.
//
//  Revision: 002    By: by    Date: 18-Feb-1996  DR Number: 1770 
//    Project:  Sigma (840)
//    Description:
//        Modified main task to check the system state for online
//        after every background test.
//
//  Revision: 001    By: by    Date: 12-DEC-1995  DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//=====================================================================

#include "BackgroundCycleApp.hh"

//@ Usage-Classes
#include "BackgroundFaultHandler.hh"
#include "BackgroundMsg.hh"
#include "BtatEventState.hh"
#include "IpcIds.hh"
#include "IpcQueue.hh"
#include "MsgTimer.hh"
#include "MsgQueue.hh"
#include "NovRamManager.hh"
#include "TaskMonitor.hh"
#include "TaskMonitorQueueMsg.hh"
#include "TaskControlQueueMsg.hh"
#include "TimerTbl.hh"
#include "TaskControlAgent.hh"
#include "BdSystemStateHandler.hh"
#include "IntervalTimer.hh"
#include "TimersRefs.hh"

#if defined( SIGMA_BD_CPU )
#include "BackgroundBdTests.hh"
#include "RegisterRefs.hh"
#endif // defined( SIGMA_BD_CPU )

#if defined( SIGMA_GUI_CPU )
#include "BackgroundGuiTests.hh"
#endif // defined( SIGMA_GUI_CPU )

///////////////////////////////////////////////////////////////////////////////
//    Local Constant Declaration
///////////////////////////////////////////////////////////////////////////////
static const Uint8 NO_TIMEOUT = 0;

///////////////////////////////////////////////////////////////////////////////
// Allocate memory for cycle timers.
// The int:0 definition is for alignment purposes
///////////////////////////////////////////////////////////////////////////////
struct ObjectMap
{
    // There are currently 5 BackgroundCycleTimer needed on GUI CPU
    char pCycleTimer0[ sizeof( BackgroundCycleTimer ) ];  int :0;
    char pCycleTimer1[ sizeof( BackgroundCycleTimer ) ];  int :0;
    char pCycleTimer2[ sizeof( BackgroundCycleTimer ) ];  int :0;
    char pCycleTimer3[ sizeof( BackgroundCycleTimer ) ];  int :0;
    char pCycleTimer4[ sizeof( BackgroundCycleTimer ) ];  int :0;
 
#if defined( SIGMA_BD_CPU )
 
    // There are currently 7 BackgroundCycleTimer needed on BD CPU
    char pCycleTimer5[ sizeof( BackgroundCycleTimer ) ];  int :0;
    char pCycleTimer6[ sizeof( BackgroundCycleTimer ) ];  int :0;
 
#endif // defined( SIGMA_BD_CPU )
};

static ObjectMap ObjectMapBuffer;

// Allocate space for a message timer
///////////////////////////////////////////////////////////////////////////////
static char MsgTimerBuffer[ sizeof( MsgTimer ) ];
static MsgTimer &rMessageTimer = *(MsgTimer*) MsgTimerBuffer;

BackgroundCycleTimer*
BackgroundCycleApp::PCycleTimers_[ BackgroundCycleApp::MAX_BK_CYCLE_TASKS ];

//=====================================================================
//
//  Public Methods...
//
//=====================================================================
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Task
//
//@ Interface-Description
//  This method accepts no parameter and does not return.
//
//  This is a periodic task that pends on its queue for messages.
//  The types of messages expected by this task are retrieved and processed.
//  Any unexpected message detected on this queue will cause a soft fault.
//  The primary objective of this task is to activate background tests
//  periodically based on each test's own schedule.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This stand-alone task is spawned at system initialization time with
//  low priority.  It initiates a periodic message which puts a
//  message of type PERIODIC_TIMER_EVENT on this queue every background
//  cycle interval.  Testing schedule is based around multiples of this
//  interval.
//
//  When a PERIODIC_TIMER_EVENT message is recieved this task will 
//  traverse through a list of BackgroundCycleTimer objects constructed
//  at initialization and execute individual test when system state is
//  online.  A background test is scheduled by the object's newCycle
//  method as multiples of the background cycle.
//
//  When a TASK_MONITOR_MSG is recieved, this task reports to TaskMonitor
//  to acknowledge task BackgroundCycleApp Task() is alive.
//
//  When compiled for the GUI CPU, this task also expects BK_NO_TOUCH_ERROR,
//  BK_TOUCH_ERROR_DETECTED, and BK_TOUCH_REQUEST_FAILED from GUI-IO-Devices
//  Subsystem in reply to a touch request made during TouchScreenTest().
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void 
BackgroundCycleApp::Task( void )
{
	//TODO E600 this is temp, remove when implemented
	bool forever = TRUE;
	while(forever)
	{
		RETAILMSG( FALSE, (_T("BackgroundCycleApp::Task\r\n")) );
		Task::Delay(1);
	}
	//TODO E600 enable when ready
/*
    MsgQueue       backgroundCycleQ( BACKGROUND_CYCLE_Q );
    Int32          rawMessage;
    Int32          messageError;
    BackgroundMsg  message;
    Uint8          bkTestNdx;

#ifndef SIGMA_UNIT_TEST
    rMessageTimer.set();   // Set the first timer to go off in 500ms.

    while( TRUE )
    {
#endif // SIGMA_UNIT_TEST

        /////////////////////////////////////////////////////////////
        // Wait for the next event on the background cycle queue.
        // The  message  timer  for  this  task  is  periodic,
        // it will be regenerated automatically.
        /////////////////////////////////////////////////////////////

        messageError = backgroundCycleQ.pendForMsg( rawMessage, NO_TIMEOUT );

        CLASS_ASSERTION ( messageError == Ipc::OK );

        message.queueData = rawMessage;

        switch ( message.tskMsg.msgId )
        {
            case TaskMonitorQueueMsg::TASK_MONITOR_MSG:
            // $[TI1]
                TaskMonitor::Report();
                break;

            case BackgroundMsg::PERIODIC_TIMER_EVENT:
            // $[TI2]

                // Iterate through each cycle timer of this class
                for ( bkTestNdx = 0; bkTestNdx < MAX_BK_CYCLE_TASKS; ++bkTestNdx )
                {
                    // Perform Background test only when system is ready
                    if ( STATE_ONLINE == TaskControlAgent::GetMyState() )
                    {
                    // $[TI2.1]

                        PCycleTimers_[ bkTestNdx ]->newCycle();
                    }
                    // $[TI2.2]
                }
                break;

#if defined( SIGMA_GUI_CPU )

            case BackgroundMsg::BK_NO_TOUCH_ERROR:
            // $[TI3]
                BackgroundGuiTests::GuiTouchOk();
                break;
            
            //  GUI Touch Screen Error are group on purpose
            case BackgroundMsg::BK_TOUCH_ERROR_DETECTED:
            // $[TI4]
                BackgroundGuiTests::GuiTouchFail();
                break;

            case BackgroundMsg::BK_TOUCH_REQUEST_FAILED:
            // $[TI5]
                Background::ReportBkEvent( BK_TOUCH_SCREEN_FAIL );
                break;

#endif // defined( SIGMA_GUI_CPU )

            default:
            // $[TI6]
                CLASS_ASSERTION( FALSE );
                break;
        } // switch

#ifndef SIGMA_UNIT_TEST
    }  // while
#endif // SIGMA_UNIT_TEST
*/
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  Initialize
//
//@ Interface-Description
//  This method accepts no parameter and returns no value.
//
//  This is called during system-initialization to setup the
//  BackgroundCycleTimer test classes.  It also creates and starts
//  the first message timer for the task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Each background test that is managed by this class is derived from
//  the BackgroundCycleTimer class in this routine.  All class objects
//  are instantiated once, a list of these objects is created for the
//  task to use during its processing.
//---------------------------------------------------------------------
//@ PreCondition
//  None
//---------------------------------------------------------------------
//@ PostCondition
//  None
//@ End-Method
//=====================================================================

void
BackgroundCycleApp::Initialize( void )
{
   BackgroundMsg  message;

   // $[TI1]

   message.tskMsg.msgId = BackgroundMsg::PERIODIC_TIMER_EVENT;

   // Create a message timer for this task and schedule its first cycle.
   (void) new (&::rMessageTimer) MsgTimer( BACKGROUND_CYCLE_Q,
                                           (Int32) message.queueData,
                                           BACKGROUND_CYCLE_TIME,
                                           MsgTimer::PERIODIC );

    // Define the background tasks specific for each CPU

#if defined( SIGMA_BD_CPU )

    PCycleTimers_[ NOVRAM_CHECKSUM_TEST ] = 
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer0)
        BackgroundCycleTimer( BackgroundBdTests::NovRamChecksumTest,
                              BK_TEST_FREQ_10SEC,
                              BK_TEST_NO_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ NOVRAM_CHECKSUM_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer0),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

    PCycleTimers_[ TOD_TEST ] =
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer1)
        BackgroundCycleTimer( BackgroundBdTests::TodTest,
                              BK_TEST_FREQ_10SEC,
                              BK_TEST_NO_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ TOD_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer1),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

    PCycleTimers_[ AUDIBLE_ALARM_CAPACITOR_TEST ] =
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer2)
        BackgroundCycleTimer( BackgroundBdTests::AudibleAlarmCapacitorTest,
                              BK_TEST_FREQ_500MS,
                              BK_TEST_60SEC_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ AUDIBLE_ALARM_CAPACITOR_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer2),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

    PCycleTimers_[ AUDIBLE_ALARM_DISCONNECT_TEST ] =
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer3)
        BackgroundCycleTimer( BackgroundBdTests::AudibleAlarmDisconnectTest,
                              BK_TEST_FREQ_500MS,
                              BK_TEST_60SEC_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ AUDIBLE_ALARM_DISCONNECT_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer3),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

    PCycleTimers_[ ADC_TEST ] =
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer4)
        BackgroundCycleTimer( BackgroundBdTests::AdcTest,
                              BK_TEST_FREQ_500MS,
                              BK_TEST_NO_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ ADC_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer4),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

    PCycleTimers_[ AC_SWITCH_POSITION_TEST ] =
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer5)
        BackgroundCycleTimer( BackgroundBdTests::AcSwitchPositionTest,
                              BK_TEST_FREQ_1SEC,
                              BK_TEST_NO_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ AC_SWITCH_POSITION_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer5),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

    PCycleTimers_[ BATTERY_TEST ] =
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer6)
        BackgroundCycleTimer( BackgroundBdTests::BatteryTest,
                              BK_TEST_FREQ_500MS,
                              BK_TEST_10SEC_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ BATTERY_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer6),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

    // Setup DAC value for the first test point
// E600 BDIO   RDacWrapDacPort.writeRegister( ADC_TEST_POINT_1 );

#elif defined( SIGMA_GUI_CPU )

    PCycleTimers_[ NOVRAM_CHECKSUM_TEST ] =
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer0)
        BackgroundCycleTimer( BackgroundGuiTests::NovRamChecksumTest,
                              BK_TEST_FREQ_10SEC,
                              BK_TEST_NO_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ NOVRAM_CHECKSUM_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer0),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

    PCycleTimers_[ TOD_TEST ] =
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer1)
        BackgroundCycleTimer( BackgroundGuiTests::TodTest,
                              BK_TEST_FREQ_10SEC,
                              BK_TEST_NO_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ TOD_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer1),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

    PCycleTimers_[ KEYBOARD_TEST ] =
        new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer3)
        BackgroundCycleTimer( BackgroundGuiTests::KeyboardTest,
                              BK_TEST_FREQ_500MS,
                              BK_TEST_NO_DELAY ); 

    FREE_POST_CONDITION( PCycleTimers_[ KEYBOARD_TEST ] ==
                         ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer3),
                         SAFETY_NET, BACKGROUNDCYCLEAPP );

	//To do E600 tests needs to be revisted for sound class
    //PCycleTimers_[ SAAS_ALIVE_TEST ] =
    //    new ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer4)
    //    BackgroundCycleTimer( BackgroundGuiTests::SaasAliveTest,
    //                          BK_TEST_FREQ_10SEC,
    //                          BK_TEST_NO_DELAY ); 

    //FREE_POST_CONDITION( PCycleTimers_[ SAAS_ALIVE_TEST ] ==
    //                     ((BackgroundCycleTimer*) ObjectMapBuffer.pCycleTimer4),
    //                     SAFETY_NET, BACKGROUNDCYCLEAPP );

#endif // elif defined( SIGMA_GUI_CPU )

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//  and 'lineNumber' are essential pieces of information.  The
//  'pFileName' and 'pPredicate' strings may be defaulted in the macro
//  to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//  None 
//---------------------------------------------------------------------
//@ PostCondition 
//  None 
//@ End-Method 
//===================================================================== 

void
BackgroundCycleApp::SoftFault( const SoftFaultID  softFaultID,
                               const Uint32       lineNumber,
                               const char*        pFileName,
                               const char*        pPredicate )
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, SAFETY_NET, BACKGROUNDCYCLEAPP,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
 
//=====================================================================
//
//  Private Methods...
//
//=====================================================================
