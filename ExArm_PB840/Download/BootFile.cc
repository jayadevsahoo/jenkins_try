#include "stdafx.h"
#if	defined(SIGMA_SERVER)
//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@  Class: BootFile - Provides I/O interfaces to a file that contains
//   the boot image.
//---------------------------------------------------------------------
//@ Interface-Description
//     A bootfile can be opened, read and closed from the path
//     defined in the FLASH_MEMORY_PATH or PATH.
//---------------------------------------------------------------------
//@ Rationale
//	 This class encapsulates all the file IO related data into
//   one class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  
//---------------------------------------------------------------------
//@ Fault-Handling
//  A standard pre-condition test is used to ensure correctness.
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Download/vcssrc/BootFile.ccv   25.0.4.0   19 Nov 2013 14:02:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By: gdc    Date: 09-Aug-99    DR Number: 5499
//  Project:  ATC
//  Description:
//      Removed superfluous "#include" statements causing Solaris compile
//		problems.
//
//  Revision: 003  By: sah    Date: 15-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002  By: Gary Cederquist  Date: 28-JUL-1997  DR Number: 2197
//       Project:  Sigma (R8027)
//       Description:
//             Changed to use SigmaTypes.hh
//
//  Revision: 001  By:  <jhv>    Date:  22-Jan-96    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "BootFile.hh"
#include "DownloadTypes.hh"
#include <sys/types.h>
#include "errno.h"

#if	defined(SIGMA_PC)
#include <sy/fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif //defined(SIGMA_PC)

#include "Download.hh"
#include "stdio.h"

#if	defined(SIGMA_SUN)
#include <unistd.h>
#endif //defined(SIGMA_SUN)

//=====================================================================
//
//  Static Data Initialization...
//
//=====================================================================
char **BootFile::LoadPath_ = NULL;
static char DirMem_[MAX_PATH_MEMORY];
static char	*PathMem_[MAX_PATH_ENTRIES];

//@ Code...
//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: BootFile()  [Default Constructor]
//
//@ Interface-Description
//    Constructor. Leave the socket unopened.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BootFile::BootFile()
{
	fileId_ = NULL;
	fileName_[0] = '\0';
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~BootFile()  [Destructor]
//
//@ Interface-Description
//   Default Destructor. Close the socket if it is opened.
//---------------------------------------------------------------------
//@ Implementation-Description
//	 none.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

BootFile::~BootFile(void)
{
	if ( opened() )
	{
		close();
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: close()
//
//@ Interface-Description
//	  Closes the file.
//--------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void
BootFile::close( void )
{
	fileName_[0] = '\0';
	if ( opened() )
	{
		::fclose(fileId_);
	}
	fileId_ = NULL;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: open(char *fileName)
//
//@ Interface-Description
//		Opens a given file. If the given file cannot be successfully
//      opened, it prints out an error message and returns a FAILURE
//	    status. The file is searched through all the directories
//	    in the load path. The method also finds out total size of
// 		the file.
//--------------------------------------------------------------------
//@ Implementation-Description
//	  Tries to open a given name file using the open function.
//	  When open fails, it tries to open from the next load path
//    in the load path list. The code employs the lseek routine
//    to find out a file size. If the lseek is called to move the
//	  file offset to the end of file, it retuns the file size in 
//    byte units. At the end, the file pointer is moved back to
//    the beginning of the file.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
BootFile::open( char *fileName )
{
int i;
char absName[MAX_FILE_NAME_LENGTH];
char *fileDir;

	CLASS_ASSERTION(LoadPath_);

	if ( fileId_  = ::fopen(fileName, "r") )
	{
		strcpy(fileName_, fileName);
	}
	else if ( fileName[0] != DIR_SEP_CHAR )
	{
		for( i= 0; fileDir = LoadPath_[i]; i++) 
		{
			sprintf(absName,"%s%s", fileDir, fileName);
			if (fileId_  = ::fopen( absName, "r"))
			{
				strcpy(fileName_, absName);
				break;
			}
		}
	}
	if (fileId_)
	{
		fseek(fileId_, 0,  SEEK_END ); // move to the end of file.
		fileSize_ = (int) ftell(fileId_); // get the file size.
		fseek(fileId_, 0, SEEK_SET); // move to the beginning of the file.
		offSet_ = 0;
		return(SUCCESS);
	}
	return(FAILURE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: creat(char *fileName)
//
//@ Interface-Description
//		Creates a given filename in the current working directory.
//      If the file already exist, the file is opened for writing.
//--------------------------------------------------------------------
//@ Implementation-Description
//      If the file already exists, the O_CREAT mode to the open
//      library routine truncates the file size to zero.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
BootFile::creat( char *fileName )
{
	if (!( fileId_  = ::fopen( fileName, "w")))
	{
		return(FAILURE);
	}
	strcpy(fileName_, fileName);
	fileSize_ = 0;
	offSet_ = 0;
	return(SUCCESS);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: write(char *pBuf, Int nbytes)
//
//@ Interface-Description
//		writes nbytes of the given data to this file.
//		If the file IO fails, it returns a negative number
//      and errno indicates an actual error condition.
//		If the write to this file is successful, the method
//      returns a number of bytes of data written out.
//--------------------------------------------------------------------
//@ Implementation-Description
//      C library routine, write is called to do IO.
//---------------------------------------------------------------------
//@ PreCondition
//	 	CLASS_PRE_CONDITION(fileId_)
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
BootFile::write(Byte *pBuf, Int nBytes )
{
	CLASS_PRE_CONDITION(opened());

	return(::fwrite( pBuf, 1, nBytes, fileId_ ));
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: read(char *pBuf, Int nbytes)
//
//@ Interface-Description
//		Reads nbytes of data from this Bootfile, returns total number
//		of data bytes read in. If the file IO fails, it returns
//		a negative number and errno indicates actual error condition.
//--------------------------------------------------------------------
//@ Implementation-Description
//    The Bootfile keeps track of the current file offset.
//	  When a user tries to read more than what's left before the end
//    of this file, it attempts to read only a left number of bytes
//    in the file.
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
Int
BootFile::read(Byte *pBuf, Int nBytes )
{
Int ret;

	CLASS_PRE_CONDITION(opened());

	if ( offSet_ == fileSize_ )
	{
		return(0);
	}
	if (( offSet_ + nBytes ) > fileSize_ )
	{
		nBytes = fileSize_ - offSet_;
	}

	if ( (ret = ::fread((char *)pBuf,1, nBytes, fileId_ )) < 0 )
	{
		return -1;
	}
	offSet_ += ret;
	return(ret);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: InitLoadPath( char **argv )
//
//@ Interface-Description
//		Establishes a list of directory names that a boot file is
//	    searched from. 
//--------------------------------------------------------------------
//@ Implementation-Description
//		First, it tries to read in the FLASH_MEMORY_PATH environment
//      variable, the variable contains a string that contains list
//      of directory names that are separated by a separator character.
//	    If the FLASH_MEMORY_PATH is not defined, it gets the directory
//      list from the PATH environment variable. 
//---------------------------------------------------------------------
//@ PreCondition
//	 	none.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
SigmaStatus
BootFile::InitLoadPath()
{
char *pathVar, *pName, *pCurDir;
Int i;
Char c;

	if (!(pathVar = getenv("FLASH_PATH")))
	{
#if	defined(SIGMA_PC)
		if (!(pathVar = getenv("PATH")))
#endif //defined(SIGMA_PC)
			pathVar = ".";
	}
	pName =  DirMem_;
	i = 0;
	pCurDir = NULL;
	while( c = *pathVar++ )
	{
		switch(c)
		{
			case ' ':
			case '\t':
			case PATH_SEP_CHAR:
				if ( pCurDir )
				{
					if ( *( pName -1 ) != DIR_SEP_CHAR )
					{
						*pName++ = DIR_SEP_CHAR;
					}
					*pName++ = '\0';
				 	PathMem_[i++] = pCurDir;
					pCurDir = NULL;
					if ( i >= ( MAX_PATH_ENTRIES - 1 ))
					{
						return(FAILURE);
					}
				}
				continue;
			default:
				if ( !pCurDir )
				{
					pCurDir = pName;
				}
				*pName++ = c;
    	}
	}
	if ( pCurDir)
	{
		if ( *( pName -1 ) != DIR_SEP_CHAR )
		{
			*pName++ = DIR_SEP_CHAR;
		}
		*pName = '\0';
		if ( i < ( MAX_PATH_ENTRIES - 1 ))
		{
			PathMem_[i++] = pCurDir;
		}
		else
		{
			return(FAILURE);
		}
	}
	PathMem_[i] = NULL;
	LoadPath_ = &PathMem_[0];
	return(SUCCESS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
BootFile::SoftFault(const FaultType  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  DownloadFault::SoftFault(softFaultID, DOWNLOAD,
  	BOOT_FILE_CLASS, lineNumber, pFileName, pPredicate);
}


#endif //defined(SIGMA_SERVER)
