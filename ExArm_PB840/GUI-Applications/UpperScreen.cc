#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1994, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: UpperScreen - The base container for all upper screen containers.
//---------------------------------------------------------------------
//@ Interface-Description
// Contains the following high-level containers for the Upper LCD Screen:
// >Von
//	1.	VitalPatientDataArea.
//	2.	AlarmAndStatusArea.
//	3.	UpperSubScreenArea.
//	4.	UpperScreenSelectArea.
// >Voff
// Provides public access to each of the contained member objects, via inline
// 'get()' methods.
//---------------------------------------------------------------------
//@ Rationale
// Convenient container object for the high-level components of the Upper LCD
// Screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is simply a holding area for the main containers of the Upper Screen.
// The UpperScreen class provides no functionality other than an interface
// which allows "global" access to each of the main containers.
//---------------------------------------------------------------------
//@ Fault-Handling
// none
//---------------------------------------------------------------------
//@ Restrictions
// none
//---------------------------------------------------------------------
//@ Invariants
// none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/UpperScreen.ccv   25.0.4.0   19 Nov 2013 14:08:40   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004   By: hhd    Date:  16-Nov-1999    DCS Number: 5327
//  Project: Neo-Mode
//  Description:
// 		Modified as part of a general cleanup effort for Neo-Mode.
//
//  Revision: 003  By:  sah	   Date:  11-Jan-1999    DCS Number: 5321
//  Project:  ATC
//  Description:
//		Added 'SIGMA_DEVELOPMENT' conditionals around this class (unused)
//		'SoftFault()' method.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "UpperScreen.hh"
#include "VentTestSummarySubScreen.hh"
#include "AlarmLogSubScreen.hh"
#include "WaveformsSubScreen.hh"
#include "MemoryStructs.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

// Initialize static constants.
static const Int32 CONTAINER_X_ = 0;
static const Int32 CONTAINER_Y_ = 0;
static const Int32 	CONTAINER_WIDTH_ = 640;
static const Int32 CONTAINER_HEIGHT_ = 480;

UpperScreen & UpperScreen::RUpperScreen = *((UpperScreen *)
		(((ScreenMemoryUnion *)::ScreenMemory)->normalVentMemory.upperScreen));


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: UpperScreen()  [Default Constructor]
//
//@ Interface-Description
// Default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// Size and position the container and set its background color.  Add
// all member objects to the parent container for display and initialize
// Upper Screen Select Area.  Display the Vent Test Summary subscreen
// if vent startup is not complete.
// $[01003] Upper screen contains four areas ...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UpperScreen::UpperScreen(void)
{
	CALL_TRACE("UpperScreen::UpperScreen(void)");
 
    // Size and position the area
    setX(CONTAINER_X_);
    setY(CONTAINER_Y_);
    setWidth(CONTAINER_WIDTH_);
    setHeight(CONTAINER_HEIGHT_);
 
    //
	// Default background color is wheat
	//
    setFillColor(Colors::FRAME_COLOR);
 
    //
	// Add the various member objects to the container.
	// $[01003] Upper screen contains four areas ...
	//
    addDrawable(&upperScreenSelectArea_);
    addDrawable(&upperSubScreenArea_);
    addDrawable(&alarmAndStatusArea_);
    addDrawable(&vitalPatientDataArea_);

	// Initialize Upper Screen Select Area
	upperScreenSelectArea_.initialize();

	// If Vent Startup is not completed or SettingsLockedOut happened due
	// to vent inop or vent timeout,  then automatically display the
	// Vent Test Summary subscreen.
	if (!GuiApp::IsVentStartupSequenceComplete() ||
		GuiApp::IsSettingsLockedOut())
	{													// $[TI1]
		// Display the Ventilator Test Summary subscreen.  Push the Other
		// Screens button down also.
		upperSubScreenArea_.activateSubScreen(
					UpperSubScreenArea::GetVentTestSummarySubScreen(),
					upperScreenSelectArea_.getUpperOtherScreensTabButton());
	}													// $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~UpperScreen  [Destructor]
//
//@ Interface-Description
// Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
// n/a
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

UpperScreen::~UpperScreen(void)
{
	CALL_TRACE("UpperScreen::~UpperScreen(void)");

	// Do nothing
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: patientSetupComplete
//
//@ Interface-Description
// Called when new/same patient setup is completed. Activates the 
// waveforms sub-screen if the operator has not already activated a 
// sub-screen on the upper screen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Notifies the AlarmLogSubScreen to update its display via 
// AlarmLogSubScreen::updateAlarmLogDisplay. Activates
// the waveforms sub-screen via SubScreenArea::activateSubScreen
// if the currently displayed sub-screen was not activated by the 
// operator as determined by SubScreenArea::isActivatedByOperator.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
UpperScreen::patientSetupComplete(void)
{
    // Inform the Vital patient area of change in display mode.
    vitalPatientDataArea_.guiEventHappened(GuiApp::PATIENT_SETUP_ACCEPTED);
 
    // Notify the AlarmLogSubScreen to update its display.  This
    // message is ignored if the screen is not active.
    UpperSubScreenArea::GetAlarmLogSubScreen()->updateAlarmLogDisplay();

	if ( !upperSubScreenArea_.isActivatedByOperator() )
	{															// $[TI1.1]
		// If the operator has not already activated an upper sub-screen...
		// Display the Waveforms subscreen.  Push the Waveforms Tab button also.
		upperSubScreenArea_.activateSubScreen(
					UpperSubScreenArea::GetWaveformsSubScreen(),
					upperScreenSelectArea_.getWaveformsTabButton());
	}															// $[TI1.2]
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
UpperScreen::SoftFault(const SoftFaultID  softFaultID,
					   const Uint32       lineNumber,
					   const char*        pFileName,
					   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, UPPERSCREEN,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)

