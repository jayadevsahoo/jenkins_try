#ifndef TrendDbRecord_HH
#define TrendDbRecord_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbRecord
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbRecord.hhv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  ksg    Date:  18-June-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
// 
//=====================================================================

#include "TrendDataMgr.hh"
#include "TimeStamp.hh"
#include "TrendDataValues.hh"


// The Trend record base class provides the defintion of the interface
// and it is up to the derived class to provide the implementation
class TrendDbRecord
{
public:

	virtual Int16 size( void ) const = 0;
    virtual Uint32 getSeqNumber( void ) const = 0;
	virtual void setSeqNumber( Uint32 newSeqNumber ) = 0;
	virtual const TimeOfDay & getTimeOfDay( void ) const = 0;
	virtual void setTimeOfDay( const TimeOfDay & newTimeStamp ) = 0;
    virtual const TrendValue & getTrendValue( Int8 slotNumber ) const = 0;
	virtual void setTrendValue( Int8 slotNumber, const TrendValue & value ) = 0;
	virtual void setChecksum( void ) = 0;

	// Validates that the memory pointer exists and that the
	// checksum of the record is correct
    Boolean isValid( void ) const;

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

	// Set the memory pointer for the record
	// A record does not provide memory, but just the structure
	// formated access to the memory
	inline MemPtr getMemoryPointer( void ) const;
	inline void setMemoryPointer( MemPtr pRawMemory );

	// Dump the raw record data
	void dump( void );

protected:

	// Virtual constructor/destructor does no action
	TrendDbRecord( void );
	virtual ~TrendDbRecord( void );

	// Computes a simple XOR checksum and returns
	Uint16 computeChecksum_( void ) const;


private:

	//@Data-Member: pRecordSchema_
	// A pointer into an already allocated memory
	MemPtr    pRecordSchema_;
};

// Inlined methods
#include "TrendDbRecord.in"

#endif // TrendDbRecord_HH
