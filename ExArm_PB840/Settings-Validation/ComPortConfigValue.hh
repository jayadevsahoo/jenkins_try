#ifndef ComPortConfigValue_HH
#define ComPortConfigValue_HH

//====================================================================
//
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  ComPortConfigValue - Values of Communications Port Device
//         Selection Setting.
//
// $[GC01001] The Communication setup subscreen shall allow ...
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/ComPortConfigValue.hhv   25.0.4.0   19 Nov 2013 14:27:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 007   By: gdc    Date: 09-Aug-2010     SCR Number: 6663
//  Project: API/MAT
//  Description:
//  Added VentSet value.
// 
//  Revision: 006   By: mnr    Date: 24-Feb-2010     SCR Number: 6556
//  Project: NEO
//  Description:
//  Added Philips value.
// 
//  Revision: 005   By: RHJ    Date:  23-Jan-2008    SCR Number: 6441
//  Project: S840 
//  Description: 
//  Added Sensor value.
// 
//  Revision: 004   By: erm    Date:  02-Jun-2003    DR Number: 6058
//  Project: AVER
//  Description: 
//  Added value for SPACELAB monitor
//
//  Revision: 004   By: hlg    Date:  09-Oct-2001    DR Number: 5966
//  Project: GUIComms
//  Description:
//	Added mapping to SRS print requirements.
//
//  Revision: 003   By: quf    Date:  17-Sep-2001    DR Number: 5943
//  Project: GUIComms
//  Description:
//	Removed comm device "None" setting.
//
//  Revision: 002   By: hlg    Date:  29-AUG-2001    DR Number: 5935
//  Project: GUIComms
//  Description:
//	Unused struct ComPortSelectValue removed.
//
//  Revision: 001   By: hct    Date:  14-FEB-2000    DR Number: 5493
//  Project: GUIComms
//  Description:
//	Initial version.
//
//====================================================================

//@ Usage-Classes
//@ End-Usage


struct ComPortConfigValue
{
  //@ Type:  ComPortConfigValueId
  // [GC01001] All of the possible values of the com port device selection setting.
  enum ComPortConfigValueId
  {
    DCI_VALUE,
    PRINTER_VALUE,
    SPACELAB_VALUE,
    SENSOR_VALUE,
	PHILIPS_VALUE,  //for Philips IntelliBridge monitor
    VSET_VALUE,
    TOTAL_CONFIG_VALUE
  };
};


#endif // ComPortConfigValue_HH 
















