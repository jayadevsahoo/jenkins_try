
#ifndef PhaseRefs_HH
#define PhaseRefs_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Header: PhaseRefs - All the external references of various breath 
//		   phases. 
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PhaseRefs.hhv   25.0.4.0   19 Nov 2013 14:00:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 008   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added RM phase references. Removed duplicate ExpiratoryPausePhase
//		reference.
//
//  Revision: 007   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		* added RPavPhase, RPavPausePhase object in the phase reference.
//
//  Revision: 006  By: sah     Date:  05-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//      VTPC project-related changes:
//		* added RVsvPhase and RVtpcvPhase
//
//  Revision: 005  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Added rTcvPhase object in the phase reference.
//		ATC initial version.
//
//  Revision: 004  By:  syw    Date:  08-Dec-1997    DR Number: none
//       Project:  Sigma (840)
//       Description:
//		 	BiLevel initial version.  Added RInspiratoryPausePhase,
//			LowToHighPeepPhase, HiLevelPsvPhase, OscBiLevelPhase,
//          BiLevelSpontPausePhase.
//
//  Revision: 003  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 002  By: syw    Date:  06-May-1996    DR Number: 600
//       Project:  Sigma (R8027)
//       Description:
//       		Added rPeepRecoveryPhase.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//====================================================================

// Breath Phases
class StartupPhase;
extern StartupPhase& RStartupPhase;

class ExpiratoryPausePhase;
extern ExpiratoryPausePhase& RExpiratoryPausePhase;

class InspiratoryPausePhase ;
extern InspiratoryPausePhase& RInspiratoryPausePhase ;
extern InspiratoryPausePhase& RBiLevelSpontPausePhase ;

class NifPausePhase;
extern NifPausePhase& RNifPausePhase;
 
class P100PausePhase;
extern P100PausePhase& RP100PausePhase;
 
class SvcPausePhase;
extern SvcPausePhase& RSvcPausePhase;
 
class PavPausePhase ;
extern PavPausePhase& RPavPausePhase ;

class VcvPhase;
extern VcvPhase& RVcvPhase;
extern VcvPhase& RApneaVcvPhase;

class ExhalationPhase;
extern ExhalationPhase& RExhalationPhase;

class PcvPhase;
extern PcvPhase& RPcvPhase;
extern PcvPhase& RApneaPcvPhase;
 
class VtpcvPhase;
extern VtpcvPhase& RVtpcvPhase;

class PsvPhase;
extern PsvPhase& RPsvPhase;

class VcmPsvPhase;
extern VcmPsvPhase& RVcmPsvPhase;

class TcvPhase;
extern TcvPhase& RTcvPhase;

class VsvPhase;
extern VsvPhase& RVsvPhase;

class PavPhase;
extern PavPhase& RPavPhase;

class LowToHighPeepPhase;
extern LowToHighPeepPhase& RLowToHighPeepPhase;

class HiLevelPsvPhase ;
extern HiLevelPsvPhase& RHiLevelPsvPhase ;

class PeepRecoveryPhase ;
extern PeepRecoveryPhase& RPeepRecoveryPhase ;
 
class DisconnectPhase;
extern DisconnectPhase& RDisconnectPhase;
 
class SafeStatePhase;
extern SafeStatePhase& RSafeStatePhase;
 
class SafetyValveClosePhase;
extern SafetyValveClosePhase& RSafetyValveClosePhase;
 
class OscPcvPhase;
extern OscPcvPhase& ROscPcvPhase;
 
class OscExhPhase;
extern OscExhPhase& ROscExhPhase;
 
class OscBiLevelPhase;
extern OscBiLevelPhase& ROscBiLevelPhase;
 
#endif // PhaseRefs_HH 


