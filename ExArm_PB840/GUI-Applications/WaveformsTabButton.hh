#ifndef WaveformsTabButton_HH
#define WaveformsTabButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: WaveformsTabButton - A specialized version of TabButton which
// is responsible for flashing the alarm bitmap to indicate an overflow
// of the current alarm display.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/WaveformsTabButton.hhv   25.0.4.0   19 Nov 2013 14:08:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  wks    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "TabButton.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage

class WaveformsTabButton : public TabButton
{
public:
	WaveformsTabButton(Uint16 xOrg, Uint16 yOrg, Uint16 width, Uint16 height,
						ButtonType buttonType, Uint8 bevelSize,
						CornerType cornerType, StringId title,
						SubScreen *pSubScreen);
	~WaveformsTabButton(void);

	virtual void updateDisplay(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	WaveformsTabButton();								// not implemented...
	WaveformsTabButton(const WaveformsTabButton&);		// not implemented...
	void operator=(const WaveformsTabButton&);			// not implemented...
};


#endif // WaveformsTabButton_HH 
