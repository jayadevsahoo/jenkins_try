
#ifndef PressureSetInspTrigger_HH
#define PressureSetInspTrigger_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2006, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: PressureSetInspTrigger - Triggers if conditions for pressure 
//	triggering an inspiration are met.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureSetInspTrigger.hhv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Initial implementation.
//
//====================================================================

#  include "Sigma.hh"
#  include "Breath_Delivery.hh"

//@ Usage-Classes
#  include "PressureInspTrigger.hh"

//@ End-Usage

class PressureSetInspTrigger : public PressureInspTrigger
{
  public:
    PressureSetInspTrigger(const Trigger::TriggerId triggerid);
    virtual ~PressureSetInspTrigger(void);
    
	// virtual from PressureInspTrigger
	virtual Real32 getPressureSens(void) const;

    static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);

  protected:

  private:
    PressureSetInspTrigger(const PressureSetInspTrigger&);		// not implemented...
    void   operator=(const PressureSetInspTrigger&);	// not implemented...
    PressureSetInspTrigger(void);		// not implemented...

};

#endif // PressureSetInspTrigger_HH 
