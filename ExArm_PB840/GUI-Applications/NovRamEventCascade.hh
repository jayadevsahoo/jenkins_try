#ifndef NovRamEventCascade_HH
#define NovRamEventCascade_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: NovRamEventCascade - A NovRamEventTarget which stores a list
// of NovRamEventTargets.  Allows multiple NovRamEventTargets to
// register for changes to a single NovRam event.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/NovRamEventCascade.hhv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================


#include "NovRamEventTarget.hh"

//@ Usage-Classes
#include "GuiAppClassIds.hh"
//@ End-Usage


class NovRamEventCascade : public NovRamEventTarget
{
public:
	NovRamEventCascade(void);
	~NovRamEventCascade(void);

	// Other NovRamEventTarget's register with this method.
	void addTarget(NovRamEventTarget *pTarget);

	// NovRam Event notices are communicated through here.
	void novRamUpdateEventHappened(NovRamUpdateManager::UpdateTypeId updateId);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	NovRamEventCascade(const NovRamEventCascade&);	// not implemented...
	void operator=(const NovRamEventCascade&);		// not implemented...

	enum
	{
		//@ Constant: GSC_MAX_TARGETS_
		// The maximum number of target pointers that can be stored by this
		// cascade
		GSC_MAX_TARGETS_ = 3
	};

    // @ Data-Member: targets_
    // The array which holds pointers to all of the targets.
    NovRamEventTarget *targets_[GSC_MAX_TARGETS_];

    //@ Data-Member: numTargets_
    // The number of targets stored in 'targets_[]'.
    Uint numTargets_;
};


#endif // NovRamEventCascade_HH 
