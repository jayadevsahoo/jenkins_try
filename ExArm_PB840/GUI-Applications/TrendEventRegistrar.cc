#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TrendEventRegistrar - The central registration, control, and
//  dispatch point for all trend data events.
//---------------------------------------------------------------------
//@ Interface-Description
//  Objects which handle trend data events use the RegisterTarget()
//  method to register for a callback. The callback is the virtual method
//  TrendEventTarget::trendDataReady() -- therefore all objects which
//  need to receive trend data callbacks are derived from the
//  TrendEventTarget class.
//
//---------------------------------------------------------------------
//@ Rationale
//  Provides a means of informing clients of trend events.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Only one instance of this class is created.  The TargetArray_[] is
//  an array of pointers to TrendEventTarget objects.  When the target
//  objects register via the RegisterTarget() method, the method adds a
//  pointer to the registering object to the TargetArray_[].
//
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//  There is a limit to the number of objects that can register with
//  TrendEventRegistrar.  Currently this is set to 5 but this can be increased
//  if needed.  "Un-registering" is not allowed, if you register for GUI
//  event changes you stay registered forever!
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TrendEventRegistrar.ccv   25.0.4.0   19 Nov 2013 14:08:38   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 001  By:  ksg    Date:  18-June-2007    SCR Number: 6237
//  Project:  Trend
//  Description: Initial version.
// 
//=====================================================================

#include "TrendEventRegistrar.hh"
#include "TrendEventTarget.hh"


//=====================================================================
//
//		Static Data...
//
//=====================================================================

TrendEventTarget * TrendEventRegistrar::TargetArray_[];
Int32 TrendEventRegistrar::NumTargets_ = 0;


//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendDataReady
//
//@ Interface-Description
//  Called based on recieving a "TREND_DATA_READY" message from the 
//  USER_ANNUNCIATION_Q	
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls all objects registered in the TargetArray_[] and passes them
//  the pointer to the TrendDataSet containing the data.
//  Only the lower 24 bits of the address are used, the other bits are 
//  masked to zero.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendEventRegistrar::TrendDataReady(Uint32 dataSetAddr)
{
	TrendDataSet* pTrendDataSet = (TrendDataSet*)(dataSetAddr & 0xFFFFFF);
	CLASS_ASSERTION(pTrendDataSet != 0);

	// Walk the 'TargetArray_[]' array and call the trendDataReady()
	// method on each registered target.
	for (Uint16 i = 0; i < NumTargets_; i++)
	{
		TargetArray_[i]->trendDataReady(*pTrendDataSet);
	}                                                   
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TrendManagerStatus
//
//@ Interface-Description
//  Called upon recieving a "TREND_MANAGER_STATUS" message from the 
//  USER_ANNUNCIATION_Q
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls all objects registered in the TargetArray_[] and passes the 
//  TrendDataMgr status.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendEventRegistrar::TrendManagerStatus( Boolean isEnabled, Boolean isRunning )
{
	// Walk the 'TargetArray_[]' array and call the trendManagerStatus()
	// method on each target.
	for (Uint16 i = 0; i < NumTargets_; i++)
	{
		TargetArray_[i]->trendManagerStatus(isEnabled, isRunning);
	}                                                   
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterTarget
//
//@ Interface-Description
//  Register a TrendEventTarget to receive updates on TrendDataReady or
//  TrendManagerStatus
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Stores the provided target pointer in the TargetArray_[]. 
//  Increments the count of registered targets.
// 
//---------------------------------------------------------------------
//@ PreCondition
//  Ensures the target pointer is non-null.
//  Ensures target array has space available for an additional target. 
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================
void TrendEventRegistrar::RegisterTarget( TrendEventTarget* pTarget )
{
	CLASS_PRE_CONDITION(pTarget != NULL);
	CLASS_PRE_CONDITION(NumTargets_ < MAX_TARGETS_);

	// Add the pointer to the TargetArray_
	TargetArray_[NumTargets_] = pTarget;
	NumTargets_++;  
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [public, static]
//
//@ Interface-Description
//  Initializes static member data.  Must be called before any other
//  methods.
// 
//---------------------------------------------------------------------
//@ Implementation-Description
//  Initializes the number of targets to zero.
// 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void TrendEventRegistrar::Initialize(void)
{
	NumTargets_ = 0;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void TrendEventRegistrar::SoftFault(const SoftFaultID  softFaultID,
									const Uint32       lineNumber,
									const char*        pFileName,
									const char*        pPredicate)  
{
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TRENDEVENTREGISTRAR,
							lineNumber, pFileName, pPredicate);
}

