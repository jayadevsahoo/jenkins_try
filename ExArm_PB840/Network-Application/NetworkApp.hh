
#ifndef NetworkApp_HH
#define NetworkApp_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//@ Class: NetworkApp - Network Interface Application. The class
//       provides highlevel API for the various Network communcation 
//       services mainly between the GUI and BD boards.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Network-Application/vcssrc/NetworkApp.hhv   25.0.4.0   19 Nov 2013 14:14:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: gdc     Date: 02-Jan-2011  SCR Number: 6726
//  Project:  XENA2
//  Description:
//      Changed WaitForInput to use maximum socket id plus one for
//      select call.
//
//  Revision: 004  By: gdc     Date: 30-Dec-2010  SCR Number: 6716
//  Project:  XENA2
//  Description:
//      Changed to use thread-safe socket library.
//
//  Revision: 003  By: gdc     Date: 30-Dec-2010  SCR Number: 6723
//  Project:  XENA2
//  Description:
//      Changed all BlockIOSocket class references to NetworkSocket. 
//
//  Revision: 002  By: sah     Date: 13-Jan-99    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  jhv    Date:  02-13-95    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "Sigma.hh"
#include "NetworkMsgId.hh"
#include "PortIds.hh"
#include "NetworkMsgHeader.hh"
#include "NetworkAppClassIds.hh"
#include "NetworkSocket.hh"	//it #include the socket warpper which #include necessary OS header files for socket
#include "NetworkDefs.hh"

#if defined( INTEGRATION_TEST_ENABLE )
#if defined( SIGMA_GUI_CPU )
#include "PDSimulationServer.hh"
#endif	//SIGMA_GUI_CPU
#endif	//INTEGRATION_TEST_ENABLE

#ifdef SIGMA_DEBUG 
    #include <stdio.h>
    #define PRINTF(A) printf A
#else
    #define PRINTF(A)
#endif

//Forward declaration
class BroadcastSocket;
class RecvSocket;
class SendSocket;

typedef void (*XmitCallBackPtr)(XmitDataMsgId, void*, Uint32);
typedef void (*OpenCallBackPtr)(Int SocketId);

#ifndef IP_ADDRESS
//macro to convert an IP address given as 4 int numbers format into a uint32 IP number
#define IP_ADDRESS(a, b, c, d)   ( ((unsigned long)(a) << 24) | \
                                   ((unsigned long)(b) << 16) | \
                                   ((unsigned long)(c) << 8)  | \
                                   ((unsigned long)(d)) )
#endif


class NetworkApp
{
    friend  class RecvSocket;
    friend  class NetworkSocket;

#if defined( INTEGRATION_TEST_ENABLE )
#if defined( SIGMA_GUI_CPU )
	friend  class  PDSimulationServer;
#endif
#endif

public:
    struct CallbackMsg 
    {
        XmitCallBackPtr  callback;
        CallbackMsg *pNext;
    };

    static void RegisterRecvMsg(XmitDataMsgId id, XmitCallBackPtr callback);
    static void Initialize();
    static void Task();
    static void XmitMsg( XmitDataMsgId msgId, void *pData, Uint size ); 
    static void RegisterForOpenSocket(OpenCallBackPtr pMethod);
    static Int  BOpenSocket();
    static inline Boolean IsValidId(XmitDataMsgId id );
    static inline Boolean IsValidPort( PortId portId);
    static void BCloseSocket(Int socketId);
    static void TurnOnKeepAlive();
    static void TurnOffKeepAlive();
    static Int  BXmitMsg(Int socketId, XmitDataMsgId msgId, void *pData,
                         Uint32 size);
    static Int  BRecvMsg(Int socketId, XmitDataMsgId msgId, void *pData,
                         Uint32 nbytes, Uint waitDuration);
    static Int XmitAndBroadcastMsg(XmitDataMsgId id, void *pData, 
                                   Uint32 size, PortId  port);
    static Int BroadcastMsg(XmitDataMsgId id, void *pData, Uint32 size, 
                            PortId  Port);
    static inline Boolean IsCommunicationUp(void);
    static inline Boolean ConfirmOffKeepAlive() { return(ConfirmOffKeepAlive_);}
    static void ReportCommError(char *pfuncname, int ErrorNumber);

	static Boolean startupOsNetworkSockets(void);

    static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL, 
                          const char*       pPredicate = NULL);

    //@ Data-Member: OtherBoardAliveMsg
    //  Contains a network message id that indicates the other board
    //  is active and alive.
    static XmitDataMsgId OtherBoardAliveMsg;

    //@ Data-Member: ThisBoardAliveMsg
    //  Contains a network Message id that is sent to indicate
    //  the this board is active and alive.
    static XmitDataMsgId ThisBoardAliveMsg;

protected:

private:
    NetworkApp(void);               // not implemented
    ~NetworkApp(void);              // not implemented
    NetworkApp(const NetworkApp&);      // not implemented...
    void   operator=(const NetworkApp&);    // not implemented...
    static SigmaStatus OpenConnections_();

    static Int AcceptConnection_(Int mainSocket, NetworkSocket&,
                                 Uint32  waitDuration );

    static inline void SetCommDown_(void);
    static inline void SetCommUp_(void);
	static void CloseActiveConnectedSockets_(void);
    static SigmaStatus AcceptBlockIOConnection_();
    static void RecvOffKeepAliveMsg_(XmitDataMsgId, void *, Uint);
    static void RecvOnKeepAliveMsg_(XmitDataMsgId, void *, Uint);
    static void RecvConfirmMsg_(XmitDataMsgId, void *, Uint);

    //@ Data-Member: POpenCallback_;
    // A function pointer to a callback routine which is registered to
    // be called  when a new  Block IO socket connection is made.
    static OpenCallBackPtr  POpenCallback_;

    //@ Data-Member: PCallbackTbl_[TOTAL_MSG_NUMBER] 
    //  A table of the callback buffer pointers. A callback buffer
    //  contains information about a callback routine that is
    //  registered to be called whenever a specified Network message
    //  type arrives.
	static CallbackMsg *PCallbackTbl_[];

#if defined( INTEGRATION_TEST_ENABLE )
#if defined( SIGMA_GUI_CPU )
    static Uint32      m_iIsCallbackTbl[TOTAL_MSG_NUMBER];
#endif
#endif

    //@ Data-Member: BufPool_[TOTAL_RECVMSG_CALLBACKS]
    //  A buffer pool for the receive network message callback
    //  routine data. 
	static CallbackMsg BufPool_[];

    //@ Data-Member: NestBufAvail_ 
    //  Index to next free msg callback buffer in the 'BufPool_'.
    static Uint NextBufAvail_;

    //@ Data-Member: RRecvSocket_
    //  RRecvSocket_ is  a dedicated read only socket connection to
    //  other Sigma board. It is mainly used to receive network
    //  data from the other board.
    static RecvSocket &RRecvSocket_;

    //@ Data-Member: RSendSocket_
    //  RSendSocket_ is a dedicated send only sokcet connection to the
    //  other Sigma board. It is mainly used to send network data
    //  to the other Sigma board.
    static SendSocket &RSendSocket_;

#if defined(SIGMA_BD_CPU)
    //@ Data-Member: RSettingsContactSocket_
    //  RSettingsContactSocket_ is the socket that accepts a dynamic 
    //  socket connection from other network node.
    static NetworkSocket &RSettingsContactSocket_;
#endif

    //@ Data-Member: RSettingsSocket_
    //  RSettingsSocket_ is the socket for the block IO communcation.
    static NetworkSocket &RSettingsSocket_;

    //@ Data-Member: RBroadcastSocket_
    //  RBroadcastSocket_ is the socket that broadcasts network data.
    static BroadcastSocket &RBroadcastSocket_;

    //@ Data-Member: CommunicationUp_
    //  Indicates whether all the Network Application sockets are
    //  open and functional.
    static Boolean CommunicationUp_;

    //@ Data-Member: KeepAliveFeature_;
    // A flag to indicate whether to keep track of keep alive
    // messages or not. When this flag is set to true, a message
    // delay counter is not incremented for each missed keep alive
    // message arrival.
    static Boolean KeepAliveFeature_;

    //@ Data-Member: ConfirmOffKeepAlive_;
    // A flag to indicate whether  a network message to turn off the
    // keep alive feature to a partner board has been confirmed by
    // the partner.
    static Boolean ConfirmOffKeepAlive_;

};


// Inlined methods...
#include "NetworkApp.in"


#endif // NetworkApp_HH 
