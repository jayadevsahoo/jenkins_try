#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SmTasks - Service Mode tasks.
//---------------------------------------------------------------------
//@ Interface-Description
//@	This class defines the Service Mode tasks and sub-tasks required to
//	perform the Service Mode tests and functions.
//---------------------------------------------------------------------
//@ Rationale
//	This class encapsulates the methods which implement the Service Mode
//	tasks and sub-tasks.
//---------------------------------------------------------------------
//@ Implementation-Description
//	ServiceModeManagerTask executes the Service Mode tests and functions.
//	ServiceModeBackgroundCycle, a sub-task of the BdExecutiveTask,
//	is used to collect sensor data and update the controllers, e.g. the
//	flow controller.
//	TaskControlMsgTask is used to process BdReady, GuiReady, CommDown,
//	and ChangeState messages.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a
//---------------------------------------------------------------------
//@ Restrictions
//	This is a static class only and may not be instantiated.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmTasks.ccv   25.0.4.0   19 Nov 2013 14:23:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 010   By: rhj   Date: 15-Jun-2010    	SCR Number: 6436
//  Project:  PROX
//  Description:
//      Removed prox newCycle method and moved it to the 5ms cycle timer
//
//  Revision: 009   By: syw   Date:  14-Sep-2000    DR Number: 5695
//  Project:  VTPC
//  Description:
//		Added BdSignal::Collect() if development options are allowed
//
//  Revision: 008  By: dosman    Date: 03-May-1999     DR Number: 5322
//  Project:  ATC
//  Description:
//	ATC initial release
//	Added some debug code to make the rest of the debug code work.
//
//  Revision: 007  By: sah    Date: 15-Jan-1999     DR Number: 5321
//  Project:  ATC
//  Description:
//		Changed 'SoftFault()' method to be a non-inlined method.
//
//  Revision: 006  By:  quf    Date: 06-Oct-1997    DR Number: DCS 2451
//       Project:  Sigma (840)
//       Description:
//			Added call to ServiceMode::CalculateIncreasedPriority() in
//			ServiceModeBackgroundCycle().
//
//  Revision: 005  By:  gdc    Date:  30-Sep-97    DR Number: DCS 2513/2483
//       Project:  Sigma (840)
//       Description:
//			Added callback registration on BD for enhanced communication
//          failure detection/recovery and retrieval of GUI FLASH
//          serial number.
//
//  Revision: 004  By:  quf    Date:  30-Jul-97    DR Number: DCS 2205
//       Project:  Sigma (840)
//       Description:
//			Added code for Exh Valve Velocity Transducer Test.
//
//  Revision: 003  By:  quf    Date: 11-Jun-1997    DR Number: DCS 2189
//       Project:  Sigma (840)
//       Description:
//          Run the exh heater controller in the sm background cycle
//          during SST only.
//
//  Revision: 002  By:  quf    Date: 23-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Fixed global reference names (i.e. "r" changed to "R").
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: none
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "SmTasks.hh"

//@ Usage-Classes

// ***** BD/GUI COMMON INCLUDES *****
#include "SmRefs.hh"
#include "SmManager.hh"
#include "Task.hh"
#include "AppContext.hh"
#include "RegisterRefs.hh"

// ***** BD-ONLY INCLUDES *****
#if defined (SIGMA_BD_CPU)
# include "NetworkApp.hh"
# include "PhasedInContextHandle.hh"
# include "TaskControlAgent.hh"

# include "VentObjectRefs.hh"
# include "ValveRefs.hh"
# include "Psol.hh"
# include "VentStatus.hh"
# include "ProxiInterface.hh"
# include "UiEvent.hh"

# ifdef SIGMA_DEBUG
// need these #include's for flow/pressure data capture
#  include "MainSensorRefs.hh"
#  include "FlowSensor.hh"
#  include "ExhFlowSensor.hh"
#  include "PressureSensor.hh"
# endif  // SIGMA_DEBUG

# include "ControllersRefs.hh"
# include "ExhHeaterController.hh"

# include "SmSensorMediator.hh"
# include "TestData.hh"
# include "SmFlowController.hh"
# include "SmPressureController.hh"
# include "SmExhValveController.hh"
# include "SmGasSupply.hh"
# include "BitAccessGpioMediator.hh"
# include "BdSignal.hh"
# include "SoftwareOptions.hh"

# include "BDIORefs.hh"
# include "BDIOTest.h"

# ifdef SIGMA_UNIT_TEST
#  include "FakeIo.hh"

#  define RExhHeaterController fakeExhHeaterController
#  include "FakeExhHeaterController.hh"
extern FakeExhHeaterController fakeExhHeaterController;
# endif  // SIGMA_UNIT_TEST

#endif  // defined(SIGMA_BD_CPU)

#ifdef SIGMA_UNIT_TEST
# include "FakeAppContext.hh"
#endif  // SIGMA_UNIT_TEST

//TODO E600 aded only to enable RETAILMDG. Remove if/when not needed
#if defined (_WIN32_WCE)
#include <winbase.h>
#include <tchar.h>
#endif

//@ End-Usage


//@ Code...

#if defined (SIGMA_BD_CPU)

#if defined (SIGMA_SAFETY_NET)
#include "SafetyNetSensorTestDriver.hh"

Boolean SmTasks::EstablishPressure_ = FALSE;
Real32 SmTasks::DesiredPressure_ = 0.0F;
MeasurementSide SmTasks::DesiredSide_ = DELIVERY;
GasType SmTasks::DesiredGas_ = AIR;
Real32 SmTasks::FlowRate_ = 0.0F;
#endif // SIGMA_SAFETY_NET

#ifdef SIGMA_DEBUG
Uint16 SmTasks::SampleIndex = 0;
Boolean SmTasks::CaptureData = FALSE;
#endif  // SIGMA_DEBUG


Boolean SmTasks::FirstBackgroundCycle_ = TRUE;

//@ Constant: SM_CYCLE_TIME
// Service mode cycle time
const Int32 SM_CYCLE_TIME = 10;  // msec

#ifdef SIGMA_DEBUG
Real32 SmTasks::ExhAirFlow[MAX_SAMPLE_INDEX];
Real32 SmTasks::ExhO2Flow[MAX_SAMPLE_INDEX];
Real32 SmTasks::InspPressure[MAX_SAMPLE_INDEX];
Real32 SmTasks::ExhPressure[MAX_SAMPLE_INDEX];
Real32 SmTasks::O2Flow[MAX_SAMPLE_INDEX];
Real32 SmTasks::AirFlow[MAX_SAMPLE_INDEX];
#endif // SIGMA_DEBUG

#endif  // SIGMA_BD_CPU

#ifdef SIGMA_DEBUG
Uint32 SmBkgCycleCounter = 0;
#endif  // SIGMA_DEBUG

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

#if defined (SIGMA_BD_CPU)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceModeBackgroundCycle
//
//@ Interface-Description
//	This method is executed within the BdExecutiveTask, periodically
//	reading sensors updating controller states (e.g. flow controller).
//	No arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	On first cycle, turn the PSOLs off.
//	Update comm-down status.
//	If flow sensor info is valid, update sensors and controllers.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmTasks::ServiceModeBackgroundCycle( void)
{
	CALL_TRACE("SmTasks::ServiceModeBackgroundCycle( void)") ;

#ifdef SIGMA_DEBUG
	SmBkgCycleCounter++;
#endif  // SIGMA_DEBUG

	// force psols closed and calculate the SM Manager Task increased
	// priority level on first cycle only
	if (FirstBackgroundCycle_)
	{
	// $[TI1]
		RAirPsol.updatePsol(0);
		RO2Psol.updatePsol(0);

		ServiceMode::CalculateIncreasedPriority();

		FirstBackgroundCycle_ = FALSE;
	}
	// $[TI2]
	// implied else

	// Don't execute newCycle's until flow sensor data is valid
	if (ServiceMode::IsFlowSensorInfoValid())
	{
	// $[TI5]
		RSmSensorMediator.newCycle();
		RTestData.newCycle();
		RSmFlowController.newCycle();

# if defined (SIGMA_SAFETY_NET)
if ( TRUE == EstablishPressure_ )
{
    RSmPressureController.establishPressure( DesiredPressure_, DesiredSide_,
	                                         DesiredGas_, FlowRate_ );
}
# endif // SIGMA_SAFETY_NET

		RSmPressureController.newCycle();
		RSmExhValveController.newCycle();

		// Update the gas supply switches and the compressor
		// Monitor air present switch for commanding the compressor
		// Monitor the line voltage for commanding the compressor
		RSmGasSupply.newCycle();

		PhasedInContextHandle::PhaseInPendingBatch (PhaseInEvent::NON_BREATHING);

		RSmManager.processTestSignal();

		UiEvent::NewCycle();

		// update the exh heater controller for SST only
		if (TaskControlAgent::GetBdState() == STATE_SST)
		{
		// $[TI5.1]
			// TODO E600 uncomment this when RExhHeaterController is 
			// ported
			//RExhHeaterController.newCycle();
		}
		// $[TI5.2]
		// implied else

# ifdef SIGMA_DEBUG
		if (CaptureData)
		{
			// capture flow/pressure data into buffers
			AirFlow[SampleIndex] = RAirFlowSensor.getValue();
			O2Flow[SampleIndex] = RO2FlowSensor.getValue();
			ExhAirFlow[SampleIndex] = RExhFlowSensor.getValue();
// E600 BDIO            ExhO2Flow[SampleIndex] = RExhO2FlowSensor.getValue();
			InspPressure[SampleIndex] = RInspPressureSensor.getValue();
			ExhPressure[SampleIndex] = RExhPressureSensor.getValue();

			// roll the index over
			if (++SampleIndex >= MAX_SAMPLE_INDEX)
			{
				SampleIndex = 0;
			}
		}
# endif  // SIGMA_DEBUG

		if (SoftwareOptions::GetSoftwareOptions().isStandardDevelopmentFunctions())
		{
		// $[TI5.3]
			BdSignal::Collect();
		}
		// $[TI5.4]
	}
	// $[TI6]
	// implied else

	// always update the registers
    RBitAccessGpio.newCycle() ;

	//TODO E600_LL: temp. addition for calibration; to be removed - LL_E600_CAL
	BdioTest.newCycle();
}

#endif  // defined(SIGMA_BD_CPU)


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ServiceModeManagerTask
//
//@ Interface-Description
//	This method implements the ServiceModeManagerTask, which is the
//	task which executes the Service Mode tests and functions.  No
//	arguments, no return value.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Via SmManager::processRequests(), pend for then perform the requested
//	tests/functions.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmTasks::ServiceModeManagerTask( void)
{
// $[TI1]
	CALL_TRACE("SmTasks::ServiceModeManagerTask( void)");

#if defined (SIGMA_BD_CPU) && defined (SIGMA_SAFETY_NET)
    SafetyNetSensorTestDriver::MainTask();
#endif  // defined (SIGMA_BD_CPU) && defined (SIGMA_SAFETY_NET)

	RSmManager.processRequests();

	// No return is expected
	CLASS_ASSERTION_FAILURE();
	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TaskControlMsgTask
//
//@ Interface-Description
//	This method implements the TaskControlMsgTask, which is an
//	event driven task that is pending for something to be put on the
//	SM_GUI_TASK_CTRL_MSG_Q.
//	This method takes no input and returns nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method sets up callbacks with AppContext for the appropriate
//	callback messages.  It then enters an infinite loop where it
//	pends on the SM_GUI_TASK_CTRL_MSG_Q.  Once a message is received
//	on the queue, this task invokes the appropriate methods to process
//	the message.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SmTasks::TaskControlMsgTask( void)
{
// $[TI1]
	MsgQueue msgQueue(SM_GUI_TASK_CTRL_MSG_Q);

	AppContext appContext (&msgQueue);
#ifdef SIGMA_GUI_CPU
	appContext.setCallback(ServiceMode::BdReadyCallback);
#elif SIGMA_BD_CPU
	appContext.setCallback(ServiceMode::GuiReadyCallback);
	appContext.setCallback(ServiceMode::CommDownCallback);
#endif
	appContext.setCallback(ServiceMode::ChangeStateCallback);

	appContext.mainLoop();
	
}

#if defined (SIGMA_BD_CPU)
#if defined (SIGMA_SAFETY_NET)
void
SmTasks::SetEstablishPressure(const Boolean establishPressure )
{
	EstablishPressure_ = establishPressure;
}


void
SmTasks::SetDesirePressure( const Real32           desiredPressure,
	                        const MeasurementSide  desiredSide,
							const GasType          desiredGas,
							const Real32           flowRate )
{
	DesiredPressure_ = desiredPressure;
	DesiredSide_ = desiredSide;
	DesiredGas_ = desiredGas;
	FlowRate_ = flowRate;
}
#endif  // SIGMA_SAFETY_NET
#endif  // SIGMA_BD_CPU


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SmTasks::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)") ;

	FaultHandler::SoftFault( softFaultID, SERVICE_MODE, SMTASKS,
    	                      lineNumber, pFileName, pPredicate) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//=====================================================================
//
//  Private Methods...
//
//=====================================================================
