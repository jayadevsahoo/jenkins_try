#ifndef ServiceLowerScreenSelectArea_HH
#define ServiceLowerScreenSelectArea_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: ServiceLowerScreenSelectArea - The area in the Lower screen which
// contains the four sub-screen select buttons for EST test, Date/time change,
// Exit service mode, and Other Screens.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ServiceLowerScreenSelectArea.hhv   25.0.4.0   19 Nov 2013 14:08:26   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 002  By: yyy      Date: 22-May-1997  DR Number: 2107
//    Project:  Sigma (R8027)
//    Description:
//      Removed access method to the EST tab buttom.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Container.hh"

//@ Usage-Classes
#include "Bitmap.hh"
#include "Box.hh"
#include "TabButton.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage


class ServiceLowerScreenSelectArea : public Container
{
public:
	ServiceLowerScreenSelectArea(void);
	~ServiceLowerScreenSelectArea(void);

	void initialize(void);
	void setBlank(Boolean blank);

	inline TabButton *getEstTestTabButton(void);
	inline TabButton *getServiceLowerOtherScreensTabButton(void);

	static void SoftFault(const SoftFaultID softFaultID,
			  const Uint32      lineNumber,
			  const char*       pFileName  = NULL, 
			  const char*       pPredicate = NULL);
protected:

private:
	// these methods are purposely declared, but not implemented...
	ServiceLowerScreenSelectArea(const ServiceLowerScreenSelectArea&);		// not implemented...
	void   operator=(const ServiceLowerScreenSelectArea&);	// not implemented...

	//@ Data-Member: estTestTabButton_
	// The Vent Setup tab button, displays SETUP and activates the
	// Vent Setup sub-screen.
	TabButton estTestTabButton_;

	//@ Data-Member: dateTimeSettingsTabButton_
	// Alarm Setup tab button, displays an alarm bitmap and activates
	// the Alarm Setup sub-screen.
	TabButton dateTimeSettingsTabButton_;

	//@ Data-Member: exitServiceModeTabButton_
	// Other Screens tab button, displays a multi-screen bitmap and
	// activates the Lower Other Screens sub-screen.
	TabButton exitServiceModeTabButton_;

	//@ Data-Member: serviceLowerOtherScreensTabButton_
	// Other Screens tab button, displays a multi-screen bitmap and
	// activates the Lower Other Screens sub-screen.
	TabButton serviceLowerOtherScreensTabButton_;

	//@ Data-Member: lowerOtherScreensBitmap_
	// Bitmap for the Other Screens tab button.
	Bitmap lowerOtherScreensBitmap_;

	//@ Data-Member: topLineBox_
	// Box used for displaying white line at the top of this area
	Box topLineBox_;

};

// Inlined methods
#include "ServiceLowerScreenSelectArea.in"

#endif // ServiceLowerScreenSelectArea_HH 
