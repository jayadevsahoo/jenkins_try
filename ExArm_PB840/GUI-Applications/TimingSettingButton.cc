#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TimingSettingButton - A numeric setting button which displays a
// timing value in seconds, scaled down by 1000 from the stored setting.
//---------------------------------------------------------------------
//@ Interface-Description
// The Timing Setting Button is simply a Numeric Setting Button which displays
// a timing value, either an inspiratory or expiratory time.  Timing values
// are stored in milliseconds but are displayed in seconds so the only
// difference between this class and Numeric Setting Button is the scaling done
// to the setting value before display.
//
// Only one interface method is defined, updateDisplay_(), which is called (via
// SettingButton) when the button must update its displayed setting value.
//---------------------------------------------------------------------
//@ Rationale
// Needed because the timing settings are the only settings whose stored
// values/precision do not equal their displayed value/precision and, hence, a
// generic NumericSettingButton cannot be used to display their value.
//---------------------------------------------------------------------
//@ Implementation-Description
// All the construction parameters are passed directly on to
// NumericSettingButton.  The only difference between this class and
// NumericSettingButton occurs in updateDisplay_() where the setting
// value is scaled down by 1000 (to convert it to seconds) before display.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TimingSettingButton.ccv   25.0.4.0   19 Nov 2013 14:08:36   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By:  hhd	   Date:  24-May-1999    DR Number: 5369 
//    Project:  Sigma (R8027)
//    Description:
//		Removed references to Button::setButtonType() (empty) method.
//
//  Revision: 004  By:  sah	   Date:  23-Feb-1999    DCS Number:  5314
//  Project:  ATC
//  Description:
//      Added integrated with new dynamic, setting applicability functionality.
//      This allows for looser coupling between settings and GUI, and the
//      need for significantly less "setting knowledge" in the GUI subsystem.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "Sigma.hh"
#include "Colors.hh"
#include "NumericSettingButton.hh"
#include "TimingSettingButton.hh"

//@ Usage-Classes
#include "SettingSubject.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TimingSettingButton()  [Constructor]
//
//@ Interface-Description
// Creates a Timing Setting Button. Passed the following arguments:
// >Von
//	xOrg			The X co-ordinate of the button.
//	yOrg			The Y co-ordinate of the button.
//	height			Height of the button.
//	width			Width of the button.
//	buttonType		Type of the button, either LIGHT, LIGHT_NON_LIT or DARK.
//	bevelSize		Width of the button's internal border.
//	cornerType		Tells whether its corners are rounded or not, can be
//					ROUND or SQUARE.
//	borderType		The border type (whether it has an external border or not).
//	numberSize		The size of the numeric field, one from
//					TextFont::(EIGHT, TEN, TWELVE, FOURTEEN, EIGHTEEN,
//					TWENTY_FOUR).
//	valueX			The X co-ordinate of the numeric value field within the
//					button.
//	valueY			The Y co-ordinate of the numeric value field within the
//					button.
//	titleText		The string displayed as the title of the button's numeric
//					value.
//	settingId		The setting whose value the button displays and modifies.
//	pFocusSubScreen	The sub-screen which receives Accept key events from the
//					button, normally the sub-screen which contains the button.
//	messageId		The help message displayed in the Message Area when this
//					button is pressed down.
//	isMainSetting	Flag which specifies whether this setting button is a
//					"Main" setting.  Main setting buttons have slightly
//					different functionality than normal setting buttons.
//>Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// All of the passed-in parameters are passed straight on to the inherited
// NumericSettingButton, which does all of the construction work.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimingSettingButton::TimingSettingButton(Uint16 xOrg, Uint16 yOrg,
					Uint16 width, Uint16 height, Button::ButtonType buttonType,
					Uint8 bevelSize, Button::CornerType cornerType,
					Button::BorderType borderType, TextFont::Size numberSize,
					Uint16 valueCenterX, Uint16 valueCenterY,
					StringId titleText, StringId unitsText,
					SettingId::SettingIdType settingId,
					SubScreen *pFocusSubScreen, StringId messageId,
					Boolean isMainSetting) :
			NumericSettingButton(xOrg, yOrg, width, height, buttonType,
						bevelSize, cornerType, borderType, numberSize,
						valueCenterX, valueCenterY, titleText, unitsText,
						settingId, pFocusSubScreen, messageId, isMainSetting)
{
	CALL_TRACE("TimingSettingButton::TimingSettingButton(Uint16 xOrg, "
					"Uint16 yOrg, Uint16 width, Uint16 height, "
					"ButtonType buttonType, Uint8 bevelSize, "
					"CornerType cornerType, BorderType borderType, "
					"TextFont::NumberSize numberSize, "
					"Uint16 valueCenterX, Uint16 valueCenterY, "
					"StringId titleText, SettingId::SettingIdType settingId, "
					"SubScreen *pFocusSubScreen, StringId messageId, "
					"Boolean isMainSetting)");

	// Do nothing	
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TimingSettingButton  [Destructor]
//
//@ Interface-Description
// Destroys a TimingSettingButton.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

TimingSettingButton::~TimingSettingButton(void)
{
	CALL_TRACE("TimingSettingButton::~TimingSettingButton(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: applicabilityUpdate
//
//@ Interface-Description
//  Called when applicability of the setting ids have changed.
// >Von
//  qualifierId	The context in which the change happened, either
//					ACCEPTED or ADJUSTED.
//  pSubject	The pointer to the Setting's Context subject.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Update the subscreen according to the applicability of the setting ids
//  on this subscreen.
//---------------------------------------------------------------------
//@ PreCondition
//  id of subject passed in is valid.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimingSettingButton::applicabilityUpdate(
                              const Notification::ChangeQualifier qualifierId,
                              const SettingSubject*               pSubject
									    )
{
	CALL_TRACE("applicabilityUpdate(qualifierId, pSubject)");

	SAFE_AUX_CLASS_PRE_CONDITION((SUBJECT_ID_ == pSubject->getId()),
								 ((SUBJECT_ID_ << 16) | pSubject->getId()));
	CLASS_PRE_CONDITION((qualifierId == Notification::ACCEPTED  ||
						 qualifierId == Notification::ADJUSTED));

	if ((!isThisMainSetting_()  &&  qualifierId != Notification::ADJUSTED)  ||
		(isThisMainSetting_()   &&  qualifierId != Notification::ACCEPTED))
	{  // $[TI1]
		// either this is NOT a main setting and its NOT an ADJUSTED change, OR
		// it IS a main setting and its NOT an ACCEPTED change...
		return;
	}  // $[TI2]

	const Applicability::Id  NEW_APPLICABILITY =
									pSubject->getApplicability(qualifierId);

	switch (NEW_APPLICABILITY)
	{
	case Applicability::CHANGEABLE :					// $[TI3]
		if (!isThisMainSetting_())
		{
			if (activityState_ == NO_ACTIVITY)
			{  // $[TI3.1] -- applic change NOT due to activation mechanism,
			   //             must be due to change in Constant Parm's value...
				// when a timing setting button becomes newly applicable due to
				// a change to Constant Parm setting, the button needs to be
				// set to the down state ($[CL01001])...
				setToDown();
			}
			else
			{  // $[TI3.2] -- applic change due to activation mechanism...
				setToUp();
			}
		}

		setShow(TRUE);
		break;

	case Applicability::VIEWABLE :
	case Applicability::INAPPLICABLE :					// $[TI4]
		// when timing settings are in the VIEWABLE state, they are only to
		// be used within the breath timing bar, therefore all timing setting
		// buttons should be hidden when VIEWABLE...
		setShow(FALSE);
		break;

	default :
		// unexpected applicability id...
		AUX_CLASS_ASSERTION_FAILURE(NEW_APPLICABILITY);
		break;
	}

	if (getShow())
	{		   // $[TI5]
		// Update our display according to the new setting value.
		updateDisplay_(qualifierId, pSubject);
	}		   // $[TI6]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_
//
//@ Interface-Description
// Called when the display of this setting button needs to be updated with
// the latest setting value.  Passed a setting context id informing us the
// setting context from which we should retrieve the new setting value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Retrieve the latest setting value from the Settings-Validation subsystem,
// scale it down by 1000 and display it.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
TimingSettingButton::updateDisplay_(Notification::ChangeQualifier qualifierId,
									const SettingSubject*         pSubject)
{
	CALL_TRACE("updateDisplay_(qualifierId, pSubject)");

	// Get the current value and precision from the timing setting
	BoundedValue settingValue;

	if (qualifierId == Notification::ACCEPTED)
	{													// $[TI7]
		settingValue = pSubject->getAcceptedValue();
	}
	else if (qualifierId == Notification::ADJUSTED)
	{													// $[TI8]
		settingValue = pSubject->getAdjustedValue();
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE(qualifierId);
	}

	// Decide which precision to display the seconds value in.  We simply
	// scale the stored precision down by 1000.
	Precision newPrecision;

	switch (settingValue.precision)
	{
	case THOUSANDS:										// $[TI1]
		newPrecision = ONES;
		break;

	case HUNDREDS:										// $[TI2]
		newPrecision = TENTHS;
		break;

	case TENS:											// $[TI3]
		newPrecision = HUNDREDTHS;
		break;

	default:						// All the rest default to THOUSANDTHS
		newPrecision = THOUSANDTHS;						// $[TI4]
		break;
	}

	// Set the timing value to be the stored value (in mSecs) divided by
	// 1000 (secs) along with the new precision
	setValue((settingValue.value / 1000), newPrecision);

	// Check to see if it's changed and set the value to italic if so.
	if (qualifierId == Notification::ADJUSTED  &&  pSubject->isChanged())
	{													// $[TI5]
		buttonValue_.setItalic(TRUE);
		buttonValue_.setHighlighted(TRUE);
	}
	else
	{													// $[TI6]
		buttonValue_.setItalic(FALSE);
		buttonValue_.setHighlighted(FALSE);
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
TimingSettingButton::SoftFault(const SoftFaultID  softFaultID,
							   const Uint32       lineNumber,
							   const char*        pFileName,
							   const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, TIMINGSETTINGBUTTON,
									lineNumber, pFileName, pPredicate);
}
