#ifndef AlarmAnnunciator_HH
#define AlarmAnnunciator_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: AlarmAnnunciator - 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/AlarmAnnunciator.hhv   25.0.4.0   19 Nov 2013 13:51:12   pvcs  $
//
//@ Modification-Log

//  
//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 01/11/96         DR Number:   649
//      Project:   Sigma   (R8027)
//      Description:
//         Changed conditional compilation label from SIGMA_DEVELOPMENT to
//         SIGMA_SIMULATOR.
//  
//   Revision 003   By:   gbs      Date: 04/23/96         DR Number:   942
//      Project:   Sigma   (R8027)
//      Description:
//         Added UpdateSetFromCpu() unit test method.
//  
//   Revision 004   By:   hct      Date: 05/06/96         DR Number:   938
//      Project:   Sigma   (R8027)
//      Description:
//         High Urgency audible and led fails to clear when alarm autoresets.
//         Got rid of separate variables for keeping track of BD and GUI 
//         highest urgency.  UpdateAlarmAnnunciation_() works on the complete
//         list of current alarms so when the BD reported a high urgency alarm
//         and there was subsequently a low urgency alarm reported by the GUI
//         cpu, the highest urgency for the GUI was HIGH because the urgency
//         for the alarm reported by the BD was included in determining the
//         GUI highest urgency.
//  
//   Revision 005   By:   hct      Date: 12/05/96         DR Number:  1338
//      Project:   Sigma   (R8027)
//      Description:
//         Moved all alarm annunciation to GUI CPU.  BD_CPU forwards alarm
//         events to the GUI_CPU.
//
//   Revision 006	By:   sah	   Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//		   Removed all referenced to newly-obsoleted class ('AlarmMessages').
//  
//   Revision 006   By:   sah      Date: 01/15/98       DR Number: 5004
//      Project:   Sigma   (R8O27)
//      Description:
//         Removed all referenced to newly-obsoleted class ('AlarmMessages').
//
//   Revision 007   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "Alarm.hh"

#ifdef SIGMA_SIMULATOR
#  ifndef Ostream_HH
#    include "Ostream.hh"
#  endif
#endif // SIGMA_SIMULATOR

#if defined(SIGMA_GUI_CPU)
#  include "AlarmUpdateBufferList.hh"
#  include "MutEx.hh"
#endif // SIGMA_GUI_CPU

#include "AlarmUpdateSortedList.hh"
#include "AlarmManager.hh"
#include "MsgTimer.hh"
#include "NovRamManager.hh"
#include "XmitData.hh"

class AlarmUpdateList;

//@ End-Usage


class AlarmAnnunciator 
{
  public:

    static void    Initialize               (void);

#if defined(SIGMA_GUI_CPU)
    static void    InitMemory               (void);
    static void    InitData                 (void);
    static void    Update(void);
    static void    CancelAlarmSilence(void);
    static void    AlarmSilence(void);

    inline static Boolean GetHighUrgencyAutoReset(void);
    inline static Boolean GetCancelScreenLockStatus(void);
    inline static Boolean GetNoPatientDataDisplayStatus(void);
    inline static Boolean GetNoGuiSettingChangesStatus(void);
    inline static Boolean GetVentInopLedOnStatus(void);
    inline static Alarm::Urgency GetHighestUrgency      (void);
    inline static Boolean        GetAlarmsAreSilenced   (void);
    inline static const   AlarmUpdateSortedList&   GetAlarmUpdates    (void);

#  ifdef SIGMA_UNIT_TEST
    inline static Int32                  GetAlarmSilenceInterval(void);
    inline static AlarmUpdateBufferList& GetAlarmUpdateBufferList(void);
    inline static void                   ClearList(void);
#  endif // SIGMA_UNIT_TEST

#  ifdef SIGMA_SIMULATOR 
    // The following methods are for the non-GUI simulation only
    static void DisplayAlarmLog (void);
    inline static void    DisplayCurrentAlarms   (void);

    // The following methods are for GUI simulation only.
    static Boolean GetAlarmSemaphore        (void);
    static void    ReleaseAlarmSemaphore    (void);
    inline static void ManualResetPressed(void);
#  endif // SIGMA_SIMULATOR

#endif // SIGMA_GUI_CPU

#if defined(SIGMA_GUI_CPU) && defined(SIGMA_SIMULATOR)
    inline static void    ClearAlarmLog          (void);
#endif // SIGMA_GUI_CPU && SIGMA_SIMULATOR

    static void    BuildBuffer(const AlarmUpdateList& rAlarmUpdateList,
                               Alarm::UpdateType updateType,
                               Boolean           cancelScreenLockStatus,
                               Boolean           noPatientDataDisplayStatus,
                               Boolean           noGuiSettingChangesStatus,
                               Boolean           ventInopLedOnStatus);

    static void SoftFault(const SoftFaultID softFaultID,
                          const Uint32      lineNumber,
                          const char*       pFileName  = NULL, 
                          const char*       pPredicate = NULL);
  
  protected:

  private:
    AlarmAnnunciator (void);                   // Declared only
    ~AlarmAnnunciator(void);                   // Declared only
    AlarmAnnunciator(const AlarmAnnunciator&); // Declared only
    void   operator=(const AlarmAnnunciator&); // Declared only

#if defined(SIGMA_GUI_CPU)

#  ifdef SIGMA_SIMULATOR
    // The following methods are for the non-GUI simulation only
    static void UpdateAlarmSummary_ (void);             
    static void UpdateAlarmAudio_   (void);
    inline static void DisplayUpdateType_  (MessageName updateTypeMessageName);
    inline static void DisplayUrgency_     (MessageName urgencyMessageName);
#  endif // SIGMA_SIMULATOR

    static void UpdateDciEntry_(const AlarmUpdate& rAlarmUpdate);
    static void UpdateDciArray_(Alarm::UpdateType updateType);
    static void RemoveAnnunciatedAlarm_(AlarmName name);
    static void UpdateAlarmAnnunciation_(
        Alarm::UpdateType       updateType = Alarm::UPDATE_TYPE_NULL);
    static void CancelAlarmSilence_(Alarm::UpdateType updateType);

    //@ Data-Member:    RAlarmUpdateBufferList_
    // List of alarm update information.
    static AlarmUpdateBufferList&    RAlarmUpdateBufferList_;

    //@ Data-Member:    RCurrentAlarmLog_
    // List of currently active alarms (Current Alarm Log).
    static AlarmUpdateSortedList&    RCurrentAlarmLog_;

    //@ Data-Member:    RAlarmSilenceTimer_
    // Reference to the MsgTimer used for Alarm Silence timeout.
    static MsgTimer&  RAlarmSilenceTimer_;   

    //@ Data-Member:    RBufferListMutEx_
    // Reference to the MutEx used for the AlarmUpdateBufferList.
    static MutEx&  RBufferListMutEx_;   

    //@ Data-Member:    AlarmsAreSilenced_
    // Status of Alarm Silence function.
    static Boolean                   AlarmsAreSilenced_;

    //@ Data-Member:    HighUrgencyAutoReset_
    // Status of whether or not any High Urgency alarms have autoreset to
    // lower urgencies.  Cleared by pressing Manual Reset.
    static Boolean                   HighUrgencyAutoReset_;

    //@ Data-Member:    AlarmSilenceInterval_
    //  Alarm Silence Interval timeout.
    static Int32                    AlarmSilenceInterval_;

    //@ Data-Member:    HighestUrgency_
    // Maximum urgency of all active alarms
    static Alarm::Urgency            HighestUrgency_;

    //@ Data-Member:    CancelScreenLockStatus_
    // Holds the Boolean value of Alarm::GetCancelScreenLockStatus().
    static Boolean CancelScreenLockStatus_;

    //@ Data-Member:    NoPatientDataDisplayStatus_
    // Holds the Boolean value of Alarm::GetNoPatientDataDisplayStatus().
    static Boolean NoPatientDataDisplayStatus_;

    //@ Data-Member:    NoGuiSettingChangesStatus_
    // Holds the Boolean value of Alarm::GetNoGuiSettingsChangesStatus().
    static Boolean NoGuiSettingChangesStatus_;

    //@ Data-Member:    VentInopLedOnStatus_
    // Holds the Boolean value of Alarm::GetVentInopLedOnStatus().
    static Boolean VentInopLedOnStatus_;

#  ifdef SIGMA_SIMULATOR
    // Data-Member:    AlarmSemaphore_
    // GUI simulation.         
    static Boolean                   AlarmSemaphore_;
#  endif // SIGMA_SIMULATOR

#endif // SIGMA_GUI_CPU

};


// Inlined methods
#include "AlarmAnnunciator.in"


#endif // AlarmAnnunciator_HH 
