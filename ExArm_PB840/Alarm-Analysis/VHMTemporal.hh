#ifndef VHMTemporal_HH
#define VHMTemporal_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VHMTemporal - 
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/VHMTemporal.hhv   25.0.4.0   19 Nov 2013 13:51:26   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct     Date: 01/01/95       DR Number:   n/a
//     Project:   Sigma   (R8027)
//     Description:
//       Initial version (Integration baseline)
//  
//   Revision 002   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 003   By:   hct      Date: 06/08/99         DR Number:   5395
//      Project:   ATC
//      Description:
//         Corrected changes made for 5359 so that TemporalConstraint/
//         VHMTemporal objects work properly.
//  
//   Revision 004   By:   hct      Date: 08/30/99         DR Number:  5513
//      Project:   NewAlarms
//      Description:
//         Removed de-escalation from Patient-Data alarms.
//  
//====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"

//@ Usage-Classes
#include "ViolationHistoryManager.hh"

//@ End-Usage


class VHMTemporal : public ViolationHistoryManager 
{
  public:
    VHMTemporal(ViolationHistoryManager::
        DurationFunctionPtrType pDurationFunction = NULL);
   virtual ~VHMTemporal(void);

   virtual void reset(void);
   virtual void update(Boolean groupState);
    virtual void setMaxNumViolations (OperatingGroup* pGroup);

   static void SoftFault(const SoftFaultID softFaultID,
						 const Uint32      lineNumber,
						 const char*       pDFileName  = NULL, 
						 const char*       pPredicate = NULL);
  
  protected:

  private:
    //@ Data-Member:    previousState_
    // Attribute which tracks the previous state of the OperatingGroup
    // pointed to by pGroup_.
    Boolean previousState_;


};


#endif // VHMTemporal_HH 
