#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Task - Task creation and control.
//---------------------------------------------------------------------
//@ Interface-Description
// The Task class is a purely static class, so it is never created
// or deleted, but provides a set of task-related functions.  These
// static functions provide the ability to control (Delay() and Yield()),
// and retrieve information about the executing task.  Since the 
// methods are static, they are called using the class name and
// scope resolution operator, e.g., Task::Delay(1,500) would put the
// task to sleep for 1.5 seconds.  Each task has a Task object
// created for it, and this object can be accessed using the 
// Task::GetTaskInfo() method, which returns a constant reference to
// the Task object associated with this task.  The public const interface
// to the Task class is then accessible, for instance, by calling
// Task::GetTaskInfo().getName() the name given to this task can be
// read. 
// To actually create tasks, the information about the task must be 
// entered into the TaskTable array defined in TaskTableDef.cc. 
// Included in this information is
// the name of the free function that will run as this task.
//---------------------------------------------------------------------
//@ Rationale
// The Task class was created to provide a safe, easy to use layer 
// between the application software and the real time operating system
// being used.  It simplifies the job of creating and controlling
// tasks, as well as providing debugging and monitoring capabilities.
//---------------------------------------------------------------------
//@ Implementation-Description
// The Task class uses a static array of Task objects that are
// initialized from the TaskTable::Table_ array.
// The task Ids are given starting at 1 and up to MAX_NUM_TASKS, so the
// task with Id=2 corresponds to the 2nd offset into the Task array.
//---------------------------------------------------------------------
//@ Fault-Handling
// The SoftFault class is used with the CLASS_ASSERTION macro to
// detect software bugs and non-recoverable conditions.
//---------------------------------------------------------------------
//@ Restrictions
// The number of applications tasks is limited to the constant
// MAX_NUM_TASKS.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/OS-Foundation/vcssrc/Task.ccv   25.0.4.0   19 Nov 2013 14:15:18   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  gdc    Date:  12-Dec-2008    SCR Number: 6068
//  Project:  840S
//  Description:
//		Removed SUN prototype code.
// 
//  Revision: 003   By: sah   Date: 13-Jan-1999  DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 002    By: Gary Cederquist  Date: 15-APR-1997  DR Number: 1943
//    Project: Sigma (R8027)
//    Description:
//       Added revision headers.
//
//  Revision: 001    By: Greg Hamm        Date: 01-OCT-1996  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#if defined(WIN32)
#undef Yield
#endif


#include "Sigma.hh"
#include "OsFoundation.hh"
#include "Task.hh"
#include "TaskFlags.hh"
 
#ifdef SIGMA_DEVELOPMENT
#  include "Ostream.hh"
#  include <stdio.h>
#endif // SIGMA_DEVELOPMENT
 
//@ Usage-Classes
#include "Sys_Init.hh" 
//@ End-Usage


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetId()
//
//@ Interface-Description
//  This static method returns the OS task ID of the calling task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method calls the corresponding OS function that returns the
//  the current thread ID
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Uint32
Task::GetId(void)
{
#if ( defined(_WIN32_WCE) || defined(WIN32) )	//ported to WinCE or WIN32
	CLASS_PRE_CONDITION( Task::IsTaskingOn() )
	Uint32 id = 0;
	id = (Uint32)GetCurrentThreadId();
	CLASS_ASSERTION(id != 0)
	return id;
#else
	return 0;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetPriority()
//
//@ Interface-Description
//  This static method returns the priority of the calling task.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method is a wrapper of the actual OS call. It calls the corresponding
//  OS function to return the calling task's priority.
//---------------------------------------------------------------------
//@ PreCondition
//      (IsTaskingOn() == TRUE)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
Int32
Task::GetPriority(void)
{
	CLASS_PRE_CONDITION( Task::IsTaskingOn() )

	//If ported to WinCE or WIN32
#if ( defined(_WIN32_WCE) || defined(WIN32) )
	//the caller thread is obviously the current thread, so this is me
	HANDLE myHandle = GetCurrentThread();
	CLASS_ASSERTION(myHandle != NULL)
	Int32 retVal = THREAD_PRIORITY_ERROR_RETURN;
	#if defined(_WIN32_WCE)
	retVal = CeGetThreadPriority(myHandle);
	#elif defined(WIN32)
	retVal = GetThreadPriority(myHandle);
	#endif

	if(retVal == THREAD_PRIORITY_ERROR_RETURN)
	{
		retVal = TaskInfo::INVALID_PRIORITY; //our internal error value code (not an OS specific return..)
		DEBUGMSG( FALSE, ( _T("Failed to return priority, error code: %u \n"), (Uint32)GetLastError() ) );
	}
	return retVal;

#else //No OS defined..
	return INVALID_PRIORITY;
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SetPriority()
//
//@ Interface-Description
//  Changes the priority of the current task to the input value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Calls the corresponding OS function to change the task's priority.
//---------------------------------------------------------------------
//@ PreCondition
//  (IsTaskingOn() == TRUE)
//  (priority >= ::HIGHEST_PRIORITY && priority <= ::LOWEST_PRIORITY)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Task::SetPriority(const Int32 priority)
{
    CLASS_PRE_CONDITION( Task::IsTaskingOn() )
    CLASS_PRE_CONDITION(priority >= TaskInfo::HIGHEST_PRIORITY &&
						priority <= TaskInfo::LOWEST_PRIORITY)
	//If ported to WinCE or WIN32
#if ( defined(_WIN32_WCE) || defined(WIN32) )
	//the caller thread is obviously the current thread, so this is me
	HANDLE myHandle = GetCurrentThread();
	CLASS_ASSERTION(myHandle != NULL)
	Uint32 retVal = FALSE;
	#if defined(_WIN32_WCE)
	retVal = CeSetThreadPriority(myHandle, priority);
	#elif defined(WIN32)
	Int32 winPriority = TaskInfo::MapOsPriority(priority);
	retVal = SetThreadPriority(myHandle, winPriority);
	#endif
	if(retVal == FALSE)
	{
		DEBUGMSG( FALSE, ( _T("Failed to set thread priority, error code: %u \n"), (Uint32)GetLastError() ) );
	}
#endif//WinCE or Win32
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Exit()
//
//@ Interface-Description
//  exit() accepts a TaskState and does not return once
//  the task is terminated.  The state is recorded for debugging
//  purposes prior to deleting the task thread.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method sets the state of the Task object to the exit state
//  and resquests from the OS to deletes the task thread.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
Task::Exit(const TaskState exitState)
{
	//TODO E600 to be ported
/*
    if ( Task::IsTaskingOn() )
    {					// $[TI1]
        state_ = exitState;

        Int32 err;
        sc_tdelete(taskId_, 0, &err);

       // sc_tdelete should not return...
    }
    AUX_CLASS_ASSERTION_FAILURE(taskId_);
*/
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Delay()
//
//@ Interface-Description
// Delay() takes as input the number of seconds and milliseconds that
// the calling task wishes to sleep.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    (IsTaskingOn() == TRUE);
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void 
Task::Delay(const Uint32 seconds, const Uint32 mils)
{
	CLASS_PRE_CONDITION( Task::IsTaskingOn() )
#if ( defined(_WIN32_WCE) || defined(WIN32) )	//ported to WinCE oe WIN32 PC app
	Uint32 totalMils = ( seconds * 1000 ) + mils;
	Sleep(totalMils);
#endif
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Yield()
//
//@ Interface-Description
// Takes no arguments and returns nothing.  The result of Yield() is that
// the calling task yields to let other tasks, of the same priority that
// are waiting, to run.  It puts the calling task at the end of the
// queue of same priority ready tasks.  It has no effect if there are
// no same priority tasks ready to run.
//---------------------------------------------------------------------
//@ Implementation-Description
// Calls Sleep(0), which with a zero argument has the desired effect.
//---------------------------------------------------------------------
//@ PreCondition
//    (IsTaskingOn() == TRUE);
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void 
Task::Yield(void)
{
    CLASS_PRE_CONDITION( Task::IsTaskingOn() )
#if ( defined(_WIN32_WCE) || defined(WIN32) )	//ported to WinCE oe WIN32 PC app
    Sleep(0);
#endif
}

#ifdef SIGMA_DEVELOPMENT
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Print()
//
//@ Interface-Description
//  No arguments and passes each Task object in the array to an Ostream,
//  to be printed out.  
//---------------------------------------------------------------------
//@ Implementation-Description
//  It assumes that TaskInfo object has been overloaded
//  for the Ostream output operator, <<.
//---------------------------------------------------------------------
//@ PreCondition
//    none 
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void 
Task::Print()
{
    CLASS_PRE_CONDITION(Task::Initialized_ == TRUE);
    cout << "TASK TABLE:" << endl;
    for (Int32 i=1; i <= Task::TaskCount_; i++)
        cout << Task::GetTaskInfo(i) << endl;
    printf("\n");
}

#endif // SIGMA_DEVELOPMENT


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//      'lineNumber' are essential pieces of information.  The 'pFileName'
//      and 'pPredicate' strings may be defaulted in the macro to reduce
//      code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
 
void
Task::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
                   const char*        pFileName,
                   const char*        pPredicate)
{
   FaultHandler::SoftFault(softFaultID, OS_FOUNDATION, OsFoundation::TASK,
           lineNumber, pFileName, pPredicate);
}
