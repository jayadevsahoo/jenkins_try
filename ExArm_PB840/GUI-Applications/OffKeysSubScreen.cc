#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: OffKeysSubScreen - The subscreen activated by the offkeys.
// Allows performing alarm silence, O2 calibration, and maneuvers.
//---------------------------------------------------------------------
//@ Interface-Description
// The subscreen contains up to 4 panels (3 at this time) to display 
// the progress of the activated offkey request.
//
// The activate()/deactivate() methods are called automatically on
// display/removal of this subscreen by our subscreen area owner.  The
// timer event is passed into timerEventHappened() to display maneuver
// complete, cancel, or reject messages.  updatePanel() method is the
// main display engine to control the messages being displayed based
// on the various input. registerCallbackPtr_() provides a mean for
// the active offkeys to perfom the cancel operation.  updateDisplay_()
// controls if this subscreen shall be activated or deactivated based
// on the existance of an acive offkey request.
//---------------------------------------------------------------------
//@ Rationale
// Implements the complete functionality of the OffKeysSubScreen
// in a single class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// Only one instance of this class should be created.  It should only
// be displayed in LowerSubScreenArea.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/OffKeysSubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:10   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 012  By:  mnr    Date:  24-Feb-2010    SCR Number: 6555
//  Project:  NEO
//  Description:
//		Implemented Screen Lock panel related changes.
//
//  Revision: 011  By:  rpr    Date:  18-Feb-2009    SCR Number: 6476
//  Project:  840S
//  Description:
//		Implemented NeoMode Update option.
//
//  Revision: 010   By: rpr    Date: 23-Jan-2009    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support code review comments.
// 
//  Revision: 009   By: rpr    Date: 10-Oct-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal Plus 20 O2.
// 
//  Revision: 008 By: srp    Date: 28-May-2002   DR Number: 5908
//  Project:  VCP
//	Description:
//		Incorporated Remediation code review issue(s) -- commentary changes only.
//
//  Revision: 007  By: jja	Date: 18-Apr-2002   DCS Number:  5908 & 5914
//  Project:  Baseline
//  Description:
//		Modified to meet coding standards.
//
//  Revision: 006  By: hhd	Date: 01-May-2000   DCS Number:  5657
//  Project:  Baseline
//  Description:
//		Modified to display Settings Locked Out prompt if this condition applies
// when this subscreen is deactivated.
//
//  Revision: 005  By:  sah	   Date:  13-Apr-2000    DCS Number: 5691, 5673
//  Project:  NeoMode
//  Description:
//      Changed handling of 100% O2 panel, such that it automatically is
//      displayed each time the key is pressed.
//		Mapped requirement BLO1023 to code.
//
//  Revision: 004  By: sah	Date: 10-Apr-2000   DCS Number:  5683
//  Project:  NeoMode
//  Description:
//      As part of this DCS, removed unneeded parameters from 'activatePanel()',
//      to go along with the removal of the corresponding unneeded strings.
//
//  Revision: 003  By:  sph	   Date:  28-Mar-2000    DCS Number: 5646
//  Project:  NeoMode
//  Description:
//      Removed pause cancelled and pause active message.  Changed pending
//      message and added manual prompt message.
//
//  Revision: 002  By:  hhd	   Date:  08-Feb-2000    DCS Number: 5504
//  Project:  NeoMode
//  Description:
//		Modified to provide additional off-screen key management, whereby 100% O2
//		and Alarm Silence can be cancelled.
//
//  Revision: 001  By:  yyy	   Date:  09-Nov-1999	DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		09-Nov-1999 (yyy) - Initial version.
//		17-Nov-1999 (hhd) - Modified to reduce header file dependencies
//		29-Nov-99	(hhd) - Modified to fix bugs.
//=====================================================================

#include "OffKeysSubScreen.hh"

//@ Usage-Classes
#include "ConditionEvaluations.hh"
#include "AcceptedContextHandle.hh"
#include "GuiApp.hh"
#include "PromptStrs.hh"
#include "PromptArea.hh"
#include "MiscStrs.hh"
#include "SubScreenArea.hh"
#include "LowerSubScreenArea.hh"
#include "LowerScreen.hh"
#include "AlarmSilenceResetHandler.hh"
#include "HundredPercentO2Handler.hh"
#include "InspiratoryPauseHandler.hh"
#include "ExpiratoryPauseHandler.hh"
#include "GuiTimerRegistrar.hh"
#include "KeyHandlers.hh"
#include "PatientCctTypeValue.hh"
#include "Setting.hh"
#include "SafetyPcvSettingValues.hh"
#include "VitalPatientDataArea.hh"
#include "UpperScreen.hh"
#include "SoftwareOptions.hh"
#include "PhasedInContextHandle.hh"
//@ End-Usage
// TODO E600 remove
//#include "Ostream.hh"
//@ Code...

// Initialize static constants.
static const Int32 PANEL_HEIGHT = LOWER_SUB_SCREEN_AREA_HEIGHT;
static const Int32 HALF_PANEL_HEIGHT = LOWER_SUB_SCREEN_AREA_HEIGHT / 2;
static const Int32 HALF_PANEL_WIDTH = LOWER_SUB_SCREEN_AREA_WIDTH / 2;
static const Int32 TOP_PANEL_Y = 0;
static const Int32 BOTTOM_PANEL_Y =  TOP_PANEL_Y + HALF_PANEL_HEIGHT;
static const Int32 LEFT_PANEL_X = 1;
static const Int32 RIGHT_PANEL_X = LEFT_PANEL_X + HALF_PANEL_WIDTH;
static const Int32 LINE1_PT1_Y = TOP_PANEL_Y + HALF_PANEL_HEIGHT;
static const Int32 LINE1_PT2_X = LEFT_PANEL_X + HALF_PANEL_WIDTH; 
static const Int32 LINE2_PT2_Y = TOP_PANEL_Y + PANEL_HEIGHT;
static const Int32 LINE_PEN_WIDTH = 2;

static Boolean  FirstTimeActive_ = TRUE;


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: OffKeysSubScreen()  [Constructor]
//
//@ Interface-Description
// Creates the OffKeysSubScreen.  Passed a pointer to the subscreen
// area which creates it (LowerSubScreenArea). 
//---------------------------------------------------------------------
//@ Implementation-Description
// Sizes, positions and colors the subscreen container.
// Creates the various panels displayed in the subscreen and adds them
// to the container.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OffKeysSubScreen::OffKeysSubScreen(SubScreenArea *pSubScreenArea) :
	SubScreen(pSubScreenArea),
	alarmSilencePanel_(LEFT_PANEL_X, TOP_PANEL_Y,
						HALF_PANEL_WIDTH, HALF_PANEL_HEIGHT,
						0, MiscStrs::ALARM_SILENCE_TITLE, this,
						EventData::ALARM_RESET,
						AlarmSilenceResetHandler::Cancel),
	hundredPercentO2Panel_(LEFT_PANEL_X, BOTTOM_PANEL_Y,
						HALF_PANEL_WIDTH, HALF_PANEL_HEIGHT,
						0, MiscStrs::HUNDRED_PERCENT_O2_TITLE, this,
						EventData::PERCENT_O2,
						HundredPercentO2Handler::Cancel),
	maneuverPanel_(RIGHT_PANEL_X, TOP_PANEL_Y,
						HALF_PANEL_WIDTH, PANEL_HEIGHT,
						0, MiscStrs::EMPTY_STRING, this,
						EventData::INSPIRATORY_PAUSE,
						InspiratoryPauseHandler::Cancel),
	horizLine_(LEFT_PANEL_X, LINE1_PT1_Y, LINE1_PT2_X, LINE1_PT1_Y, LINE_PEN_WIDTH),
	vertLine_(RIGHT_PANEL_X, TOP_PANEL_Y, RIGHT_PANEL_X, LINE2_PT2_Y, LINE_PEN_WIDTH),  
	alarmSilencePanelStatus_(FALSE),
	hundredPercentO2PanelStatus_(FALSE),
	maneuverPanelStatus_(FALSE),
	displayCancelButton_(TRUE)
{
	CALL_TRACE("OffKeysSubScreen::OffKeysSubScreen(SubScreenArea "
													"*pSubScreenArea)");
	// Size and position the subscreen
	setX(0);
	setY(0);
	setWidth(LOWER_SUB_SCREEN_AREA_WIDTH);
	setHeight(LOWER_SUB_SCREEN_AREA_HEIGHT);
	setFillColor(Colors::LIGHT_BLUE);

	addDrawable(&alarmSilencePanel_);
	addDrawable(&hundredPercentO2Panel_);
	addDrawable(&maneuverPanel_);

	// Set attribs of lines and add them to drawable list.
	horizLine_.setColor(Colors::LIGHT_BLUE);
	vertLine_.setColor(Colors::LIGHT_BLUE);
	addDrawable(&horizLine_);
	addDrawable(&vertLine_);
	horizLine_.setShow(FALSE);
	vertLine_.setShow(FALSE);

	// Register for transition to hard Alarm Silence timer.
	GuiTimerRegistrar::RegisterTarget(GuiTimerId::DELAYED_PAUSE_TIMEOUT, this);

	const Setting*  pPatientCctType =
	SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
	pPatientCctType->getAdjustedValue();

	// NeoMode Update including CPAP and +20% O2
	isPlus20Enabled_ = SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_UPDATE) &&
					   (PatientCctTypeValue::NEONATAL_CIRCUIT == PATIENT_CCT_TYPE_VALUE);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~OffKeysSubScreen  [Destructor]
//
//@ Interface-Description
// Destroys OffKeysSubScreen.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

OffKeysSubScreen::~OffKeysSubScreen(void)
{
	CALL_TRACE("OffKeysSubScreen::~OffKeysSubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// Called by our subscreen area before this subscreen is to be displayed
// allowing us to do any necessary setup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Starts the subscreen setting change timeout timer.  Set the size and
// locations of the setting buttons in this subscreen.  Tells the Settings-
//  Validation subsystem to begin adjusting settings.  Calls the activate() method
// of each setting button in order to sync their displays with the current
// setting values.
// If we're in Settings lockout mode then we need to display special prompts
// and flatten all setting buttons.  Otherwise, prompt the user for what to 
// do next, bounces up the buttons.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeysSubScreen::activate(void)
{
	CALL_TRACE("OffKeysSubScreen::activate(void)");

	alarmSilencePanel_.showPanel(alarmSilencePanelStatus_);

	hundredPercentO2Panel_.showPanel(hundredPercentO2PanelStatus_);

	maneuverPanel_.showPanel(maneuverPanelStatus_, displayCancelButton_);

	Boolean redraw;

	if (maneuverPanelStatus_  ||  hundredPercentO2PanelStatus_)
	{
		redraw =
			!(LowerScreen::RLowerScreen.isInPatientSetupMode());
		// $[TI3]
	}
	else
	{
		redraw =
			!(LowerScreen::RLowerScreen.getLowerSubScreenArea()->isSubScreenActive()) &&
			!(LowerScreen::RLowerScreen.isInPatientSetupMode());
					 	// $[TI1],  $[TI2]		   
	}

	horizLine_.setShow(redraw && (hundredPercentO2PanelStatus_ || alarmSilencePanelStatus_));
	vertLine_.setShow(redraw);

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// Called by our subscreen area just after this subscreen is removed from
// the display allowing us to do any necessary cleanup. Needs no parameters.
//---------------------------------------------------------------------
//@ Implementation-Description
// Clears any prompts which may have been displayed.  If settings are locked
// out, set the secondary prompt to inform the users of this effect.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeysSubScreen::deactivate(void)
{
	CALL_TRACE("OffKeysSubScreen::deactivate(void)");

	// Clear the Primary, Advisory and Secondary prompts
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_PRIMARY, PromptArea::PA_LOW,
											NULL_STRING_ID);
	GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY, PromptArea::PA_LOW,
											NULL_STRING_ID);

	if (GuiApp::IsSettingsLockedOut())
	{	 // $[TI2.1]
	 	// Set Advisory prompt to "Settings have been locked out."
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_ADVISORY,
						PromptArea::PA_LOW, PromptStrs::SETTINGS_LOCKED_OUT_A);
	}
	else
	{	// $[TI2.2]
		GuiApp::PPromptArea->setPrompt(PromptArea::PA_SECONDARY,
											PromptArea::PA_LOW, NULL_STRING_ID);
	}

	horizLine_.setShow(FALSE);
	vertLine_.setShow(FALSE);
													// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened
//
//@ Interface-Description
// Called when the delayed pause timer runs out.  Simply clear all
// the command and title and update this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// Initialize maneuverPanelStatus_, maneuverCommand_, and maneuverPanel_.
// Update this subscreen.
// $[NE01012] Automatic dismissal of Offscreen Key Status SubScreen... 
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeysSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
	CALL_TRACE("OffKeysSubScreen::timerEventHappened(GuiTimerId::GuiTimerIdType)");

	maneuverPanelStatus_ = FALSE;
	maneuverCommand_[0] = NULL;
	maneuverPanel_.resetTitle(MiscStrs::EMPTY_STRING);
	maneuverPanel_.deactivatePanel();
	vertLine_.setShow(hundredPercentO2PanelStatus_ || alarmSilencePanelStatus_);
	
	updateDisplay_(
				   !(LowerScreen::RLowerScreen.getLowerSubScreenArea()->isSubScreenActive()) &&
				   !(LowerScreen::RLowerScreen.isInPatientSetupMode()));

													// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updatePanel
//
//@ Interface-Description
// Called by the active offkey handlers to update the display based on
// the event.
// >Von
//	eventId			event Id.
//  eventStatus		event status
//  eventPrompt		event prompt
//  elapseTime		elapse time
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// For cancel, reject, and complete events simply deactivate the
// associated panel.  For active, plateau active events, update
// the associated panel and display the necessary message.  For
// pending, and active pending events activate the associated
// panel and display the necessary message.
// $[NE01012] Automatic dismissal of Offscreen Key Status SubScreen...  
// $[BL01020] The Offscreen Key Status Subscreen shall be displayed ..
// $[BL01023] A cancel button shall be displayed for a pending maneuver...
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeysSubScreen::updatePanel(EventData::EventId eventId, EventData::EventStatus eventStatus,
							  EventData::EventPrompt eventPrompt, Int32 elapseTime)
{
	CALL_TRACE("OffKeysSubScreen::updatePanel(EventData::EventId eventId, EventData::EventStatus eventStatus)");

	const Boolean  PATIENT_SETUP_IS_COMPLETE =
							!(LowerScreen::RLowerScreen.isInPatientSetupMode());

	const Setting*  pPatientCctType =
	SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
	pPatientCctType->getAdjustedValue();

	int requestedO2;
	Boolean isInstandbymode;
	VitalPatientDataArea::VpdaDisplayMode displaymode;

	displaymode = UpperScreen::RUpperScreen.getVitalPatientDataArea()->getDisplayMode();

	isInstandbymode = (displaymode == VitalPatientDataArea::MODE_WAITING_FOR_PT) ||
					  (displaymode == VitalPatientDataArea::MODE_VENT_STARTUP);

	if (eventId == EventData::CALIBRATE_O2)
	{
		requestedO2 = 100;
	}
	else
	{
		if (ConditionEvaluations::c10()) // $[TI2.1]
		{
			requestedO2 = (int)AcceptedContextHandle::GetBoundedValue
						  (SettingId::APNEA_O2_PERCENT).value;
		}
		// If disconnect, standby or occlussion c195 doesn't work as expected
		else if (ConditionEvaluations::c193() || isInstandbymode || ConditionEvaluations::c290())
		{
			BoundedValue settingValue;
			settingValue = SafetyPcvSettingValues::GetValue(SettingId::OXYGEN_PERCENT);
			requestedO2 = settingValue.value;
		}
		else  // Otherwise, use the normal mode oxygen percent.  $[TI2.2] 
		{
			requestedO2 = (int)AcceptedContextHandle::GetBoundedValue
						  (SettingId::OXYGEN_PERCENT).value;
		}

		// $[LC01006]
		requestedO2 += 20;
		if (requestedO2 > 100)
		{
			requestedO2 = 100;
		}
	}

	static Boolean isCalO2Sensor_ = TRUE;
	if (100 != requestedO2)
	{
		isCalO2Sensor_ = FALSE;
	}
	 
	wchar_t tmpBuffer1[70];			// Buffers for storing text of "{p=12,y=11:XXX % [O2] }" 
	swprintf(tmpBuffer1, MiscStrs::PERCENT_O2_NEO_TITLE, requestedO2);

	Boolean redraw;

	switch (eventId)
	{
	case EventData::EXPIRATORY_PAUSE :
	case EventData::INSPIRATORY_PAUSE :				// $[TI2]
		// display, and update, this subscreen's pause panel so long as patient
		// setup has completed...
		redraw = PATIENT_SETUP_IS_COMPLETE;
		break;
	case EventData::PERCENT_O2 :					// $[TI3]
	case EventData::CALIBRATE_O2 :					// $[TI3]
		// display, and update, this subscreen's 100% O2 panel so long as
		// patient setup has completed, and the operation is being initiated OR
		// it's timer is being updated and there is no other subscreen currently
		// displayed in the lower subscreen area...
		redraw = (PATIENT_SETUP_IS_COMPLETE  &&
					  (eventStatus == EventData::ACTIVE  ||
						  !LowerScreen::RLowerScreen.getLowerSubScreenArea()->isSubScreenActive()));

		break;
	case EventData::ALARM_RESET :					// $[TI4]
		// update (no explicit display) this subscreen's alarm silence panel so
		// long as patient setup has completed and there is no other subscreen
		// currently displayed in the lower subscreen area...
		redraw = (PATIENT_SETUP_IS_COMPLETE  &&
					  !(LowerScreen::RLowerScreen.getLowerSubScreenArea()->isSubScreenActive()));
		break;
	default :
		// assert on unexpected case
		AUX_CLASS_ASSERTION_FAILURE(eventId);
		break;
	};

	static StringId pauseMsg[EventData::PLATEAU_ACTIVE+1] =
					{NULL_STRING_ID, 				// IDLE
					 MiscStrs::PAUSE_PENDING_MSG, 	// PENDING
					 NULL_STRING_ID,	            // REJECTED
					 NULL_STRING_ID,				// ACTIVE
					 MiscStrs::PAUSE_DONE_MSG,		// COMPLETE
					 NULL_STRING_ID,            	// CANCEL
					 NULL_STRING_ID,				// AUDIO_ACK
					 NULL_STRING_ID,				// NO_ACTION_ACK
					 MiscStrs::PAUSE_PENDING_MSG, 	// ACTIVE_PENDING
					 NULL_STRING_ID				 	// PLATEAU_ACTIVE
					};
	
	switch (eventStatus)
	{											// $[TI1]
	case EventData::CANCEL:
	case EventData::REJECTED:
	case EventData::COMPLETE:					// $[TI1.1]
		switch (eventId)
		{
		case EventData::ALARM_RESET:			// $[TI1.1.1]
			alarmSilencePanelStatus_ = FALSE;
			alarmSilencePanel_.deactivatePanel();
			::FirstTimeActive_ = FALSE;
			break;

		case EventData::PERCENT_O2:				// $[TI1.1.2]
		case EventData::CALIBRATE_O2:				// $[TI1.1.2]
			hundredPercentO2PanelStatus_ = FALSE;
			hundredPercentO2Panel_.deactivatePanel();
			::FirstTimeActive_ = FALSE;
			break;

		case EventData::EXPIRATORY_PAUSE:	
		case EventData::INSPIRATORY_PAUSE:		// $[TI1.1.3]

			displayCancelButton_ = FALSE;

			if (maneuverPanelStatus_)
			{									// $[TI1.1.3.1]
				// $[NE01012] -- dismissal of subscreen following maneuvers
				//               shall be at least one second after completion
				//               or cancel...
				if (eventStatus	== EventData::COMPLETE)
				{					 			// $[TI1.1.3.1.1]

					maneuverPanel_.activatePanel(pauseMsg[eventStatus],
												 TRUE, displayCancelButton_, redraw);						
					// Inform TimerRegistrar of event
					GuiTimerRegistrar::StartTimer(GuiTimerId::DELAYED_PAUSE_TIMEOUT);
				}
				else if (eventStatus == EventData::CANCEL)
				{								// $[TI1.1.3.1.2]
					// Bypass the timer's period when cancellation happens.
					timerEventHappened(GuiTimerId::DELAYED_PAUSE_TIMEOUT);
				}// $[TI1.1.3.1.3]
			}		  // $[TI1.1.3.2]
			break;
		default :
			// assert on unexpected case
			AUX_CLASS_ASSERTION_FAILURE(eventId);
			break;	
		}

		break;

	case EventData::ACTIVE:
		if ( (EventData::PERCENT_O2 == eventId)  || 
			 (EventData::CALIBRATE_O2 == eventId) )
		{
			isCalO2Sensor_ = (100 == requestedO2);
		}

	case EventData::PLATEAU_ACTIVE:				// $[TI1.2]
		switch (eventId)
		{
		case EventData::ALARM_RESET:			// $[TI1.2.1]
			alarmSilencePanelStatus_ = TRUE;
			alarmSilencePanel_.activatePanel(MiscStrs::ALARM_SILENCE_TITLE,
										TRUE, elapseTime, redraw);
			break;

		case EventData::PERCENT_O2:				// $[TI1.2.2]
		case EventData::CALIBRATE_O2:				// $[TI1.2.2]
			hundredPercentO2PanelStatus_ = TRUE;
			if (isPlus20Enabled_ && !HundredPercentO2Handler::IsCalibrateO2KeyPressed())
			{
				if (isCalO2Sensor_)
				{
					hundredPercentO2Panel_.activatePanel(MiscStrs::HUNDRED_PERCENT_O2_NEO_TITLE,
														 TRUE, elapseTime, redraw);
				}
				else
				{
					hundredPercentO2Panel_.activatePanel(tmpBuffer1,
														 TRUE, elapseTime, redraw);
				}
			}
			else
			{
				hundredPercentO2Panel_.activatePanel(MiscStrs::HUNDRED_PERCENT_O2_TITLE,
													 TRUE, elapseTime, redraw);
			}

			::FirstTimeActive_ = TRUE;
			break;

		case EventData::EXPIRATORY_PAUSE:   
		case EventData::INSPIRATORY_PAUSE:		// $[TI1.2.3]
			maneuverPanelStatus_ = TRUE;

			if (eventPrompt == EventData::MANUAL_PROMPT)
			{									// $[TI1.2.3.1]
				// Prompt the user to release pause key to end the pause if
				// he/she wishes.
				// $[BL01009] :a release key to end the pause
				// $[BL01012] :a release key to end the pause
				swprintf(maneuverCommand_, L"%s", MiscStrs::END_PAUSE_MSG);  
				displayCancelButton_ = FALSE;

			}
			else
			{	//during active pause, don't display anything 
				// $[TI1.2.3.2]
				swprintf(maneuverCommand_, L"%s", MiscStrs::EMPTY_STRING);  
			}

			// $[BL01004] remove instruction for user to wait
			// $[01252] remove instruction for user to wait
			maneuverPanel_.activatePanel(maneuverCommand_, TRUE, 
										 displayCancelButton_, redraw);
			break;
		default :
			// assert on unexpected case
			AUX_CLASS_ASSERTION_FAILURE(eventId);
			break;
		}
		break;
	case EventData::PENDING:
	case EventData::ACTIVE_PENDING:				// $[TI1.3]
		switch (eventId)
		{
		case EventData::ALARM_RESET:
			break;
		case EventData::PERCENT_O2:				// $[TI1.3.1]
		case EventData::CALIBRATE_O2:				// $[TI1.3.1]
			if (isPlus20Enabled_ && !HundredPercentO2Handler::IsCalibrateO2KeyPressed())
			{
				if (isCalO2Sensor_)
				{
					hundredPercentO2Panel_.activatePanel(MiscStrs::HUNDRED_PERCENT_O2_NEO_TITLE,
														 TRUE, elapseTime, redraw);
				}
				else
				{
					hundredPercentO2Panel_.activatePanel(tmpBuffer1,
														 TRUE, elapseTime, redraw);
				}
			}
			else
			{
				hundredPercentO2Panel_.activatePanel(MiscStrs::HUNDRED_PERCENT_O2_TITLE,
													 TRUE, elapseTime, redraw);
			}
			break;
		case EventData::EXPIRATORY_PAUSE:		// $[TI1.3.2]	
			// stop and clear the "delayed pause" timer
			GuiTimerRegistrar::CancelTimer(GuiTimerId::DELAYED_PAUSE_TIMEOUT);

			registerCallbackPtr_(eventId);
			maneuverPanelStatus_ = TRUE;
			displayCancelButton_ = TRUE;
			maneuverPanel_.activatePanel(pauseMsg[eventStatus],
					TRUE, displayCancelButton_, redraw);

			// if current screen is not default screen then activate screen
			if (LowerScreen::RLowerScreen.getLowerSubScreenArea()->isSubScreenActive())
			{ // $[TI1.3.2.1]
				::FirstTimeActive_ = TRUE;
			} // $[TI1.3.2.2]
			break;

		case EventData::INSPIRATORY_PAUSE:		// $[TI1.3.3]
			// stop and clear the "alarm transition" timer
			GuiTimerRegistrar::CancelTimer(GuiTimerId::DELAYED_PAUSE_TIMEOUT);

			registerCallbackPtr_(eventId);
			maneuverPanelStatus_ = TRUE;
			displayCancelButton_ = TRUE;

			maneuverPanel_.activatePanel(pauseMsg[eventStatus],
								TRUE, displayCancelButton_, redraw);

			// if current screen is not default screen then activate screen
			if (LowerScreen::RLowerScreen.getLowerSubScreenArea()->isSubScreenActive())
			{  // $[TI1.3.3.1]
				::FirstTimeActive_ = TRUE;
			}  // $[TI1.3.3.2]
			break;
		default :
			// assert on unexpected case
			AUX_CLASS_ASSERTION_FAILURE(eventId);
			break;
		}
		break;
	default :
		// assert on unexpected case
		AUX_CLASS_ASSERTION_FAILURE(eventStatus);
		break;
	}
			 
	updateDisplay_(redraw);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: registerCallbackPtr
//
//@ Interface-Description
// provides a mean for the active offkeys to perfom the cancel operation
// for maneuvers.
//---------------------------------------------------------------------
//@ Implementation-Description
// Based on the eventId setup the appropriate callBack pointer.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeysSubScreen::registerCallbackPtr_(EventData::EventId eventId)
{
	CALL_TRACE("OffKeysSubScreen::registerCallbackPtr_(EventData::EventId eventId)");

	switch (eventId)
	{
	case EventData::ALARM_RESET:
	case EventData::PERCENT_O2:					// $[TI1.1]
		break;
	case EventData::EXPIRATORY_PAUSE:			// $[TI1.2]
		maneuverPanel_.registerCallbackPtr(ExpiratoryPauseHandler::Cancel, MiscStrs::EXP_PAUSE_MANEUVER_TITLE);
		break;
	case EventData::INSPIRATORY_PAUSE:			// $[TI1.3]
		maneuverPanel_.registerCallbackPtr(InspiratoryPauseHandler::Cancel, MiscStrs::INSP_PAUSE_MANEUVER_TITLE);
		break;
	default :
		// assert on unexpected case
		AUX_CLASS_ASSERTION_FAILURE(eventId);
		break;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateDisplay_
//
//@ Interface-Description
// Controls the activation or deactivation of this subscreen based
// on the existance of an active offkey request.
//---------------------------------------------------------------------
//@ Implementation-Description
// If there is no active offkey request, then deactivate this subScreen.
// If there exist active offkey request, then activate this subscreen.
//
// $[NE01012] -- dismiss subscreen when no operations are active
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
OffKeysSubScreen::updateDisplay_(Boolean redrawMode)
{
	CALL_TRACE("OffKeysSubScreen::updateDisplay_(Boolean redrawMode)");

	// Get the service Lower subScreen area address.
	LowerSubScreenArea *pLowerSubScreenArea =
							LowerScreen::RLowerScreen.getLowerSubScreenArea();

	if (!alarmSilencePanelStatus_ && !maneuverPanelStatus_ && !hundredPercentO2PanelStatus_)
	{								// $[TI1.1]
		pLowerSubScreenArea->deactivateOffKeysSubScreen();
		::FirstTimeActive_ = TRUE;
	}
	else if (::FirstTimeActive_)  
	{								// $[TI1.2]
		// Activate the OffKeysSubScreenSubScreen
		pLowerSubScreenArea->activateOffKeysSubScreen(!redrawMode);
		vertLine_.setShow(redrawMode);
		::FirstTimeActive_ = (maneuverPanelStatus_) ? FALSE : !redrawMode;
		// $[TI1.2.1], $[TI1.2.2] 
	}

	Boolean horizLineStatus = horizLine_.getShow();
		
	//if maneuver screen is active and other off keys are pressed, make sure horizontal line is shown
	if (!horizLineStatus)
	{  // $[TI1.3]
		horizLine_.setShow(redrawMode && (hundredPercentO2PanelStatus_ || alarmSilencePanelStatus_));
	}  // $[TI1.4]

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
OffKeysSubScreen::SoftFault(const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS,
							OFFKEYSSUBSCREEN, lineNumber, pFileName, pPredicate);
}
