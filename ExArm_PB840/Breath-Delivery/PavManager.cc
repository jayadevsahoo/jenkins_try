#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmtted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PavManager - Central location to manage pav related data required 
//					for PAV.
//---------------------------------------------------------------------
//@ Interface-Description
//	This class manages the scheduling of the PAV pause and all the data
//	collection for assessing the patient's lung resistance and compliance.
//	It also manages the alarms associated with the failure of getting
//	acceptable result for resistance and compliance.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates the rules for assessing R and C during PA.
//---------------------------------------------------------------------
//@ Implementation-Description
//	The PavPhase class informs this class when a new PA inspiration occurs
//	so that the resistance and compliance values can be validated from the
//	previous breath.  Calculations are done each cycle during inspiration
//	and during exhalation phases to assess R and C.  The SpontScheduler class
//	interrogates this class whether to perform a PA pause or not.
//---------------------------------------------------------------------
//@ Fault-Handling
//	n/a.
//---------------------------------------------------------------------
//@ Restrictions
//  only one instance is allowed.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header::   /840/Baseline/Breath-Delivery/vcssrc/PavManager.ccv   10.7   08/17/07 09:40:30   pvcs  
//
//@ Modification-Log
//
//  Revision: 010   By:   rpr    Date: 08-April-2009      DR Number: 6490
//  Project:  S840BUILD2
//  Description:
//  Pav Optimization.  In newInspiration only perform calculations upon change 
//  of ideal body weight (ibw).  This keeps the execution of the pow function, 
//  which can be expensive, down to a minimum.
// 
//  Revision: 009   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 008   By: gdc   Date:  28-Nov-2006    DR Number: 6316
//  Project:  RESPM
//  Description:
//  Removed isPavInspiration_ boolean and references to it since it
//  did not reflect the current state of the PAV breath once newCycle()
//  was changed to run only during PA/SPONT (see SCR 6116 fix below).
//
//  Revision: 007   By: gdc   Date:  10-Feb-2005    DR Number: 6116
//  Project:  NIV1
//  Description:
//      DCS 6116 - changed newcycle() to exit if not in PA
// 
//  Revision: 006   By: gdc   Date:  10-Dec-2004    DR Number: 6116
//  Project:  NIV1
//  Description:
//		newCycle() checks support type and exits if not in PA.
//
//  Revision: 005   By: gfu   Date:  27-July-2004    DR Number: 6135
//  Project:  PAV
//  Description:
//      Modified module per SDCR #6135.  Based upon input from the Field Evaluations in Canada,
//      the following PAV specific breath delivery change is needed:  
//      Remove the accuracy correction factor for Resistance in PAV which will result in less
//      aggressive delivery at the start of a breath.
//   
//  Revision: 004   By: gfu   Date:  09-Sept-2003    DR Number: 6076
//  Project:  PAV
//  Description:
//      Modified per SDCR 6076 -- WOB calculations were being performed for
//      manual inspirations during Spont/PA breathing.  Changed to only perform
//      WOB calcs if breath type is sSpont
//
//  Revision: 003   By: gfu   Date:  17-July-2003    DR Number: 6080
//  Project:  PAV
//  Description:
//      Updated per DCS #6080 -- Added ppressure difference between wye 
//      pressure at intrinsic peep measurement and at start of inspiration
//      to end exhalation luncg pressure calculation
//
//  Revision: 002   By: syw   Date:  27-Nov-2000    DR Number: 5792
//  Project:  PAV
//  Description:
//		"Averaged" rRaw calculations.
//		Eliminate PA24022d tracing.
//
//  Revision: 001   By: syw   Date:  19-Jun-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV initial version.
//
//=====================================================================

#include "PavManager.hh"
#include <math.h>
#include <stdlib.h>

#include "MainSensorRefs.hh"
#include "BreathMiscRefs.hh"
#include "PhaseRefs.hh"
#include "TriggersRefs.hh"
#include "SoftwareOptions.hh"

//@ Usage-Classes

#include "NovRamManager.hh"
#include "Btps.hh"
#include "PressureSensor.hh"
#include "FlowSensor.hh"
#include "ExhFlowSensor.h"
#include "PhasedInContextHandle.hh"
#include "MathUtilities.hh"
#include "BreathRecord.hh"
#include "BreathPhase.hh"
#include "LungData.hh"
#include "PressureXducerAutozero.hh"
#include "BreathSet.hh"
#include "LungFlowExpTrigger.hh"
#include "PavPhase.hh"
#include "BdAlarms.hh"
#include "BreathPhaseScheduler.hh"
#include "HumidTypeValue.hh"
#include "Peep.hh"
#include "PavPhase.hh"

//@ End-Usage

//@ Code...

static const Uint32 TEN_MINUTES = 10 * 60000 ;
static const Real32 ZERO_VALUE = 0.001 ;
	
// $[PA24025]
static const Uint32  MAX_QUALIFIED_ = 4 ;


#ifdef SIGMA_PAV_UNIT_TEST
extern BdAlarmId::BdAlarmIdType BdAlarmId_UT ;
#endif // SIGMA_PAV_UNIT_TEST	

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PavManager()  
//
//@ Interface-Description
//		Default Constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	Data members are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PavManager::PavManager(void)
{
	CALL_TRACE("PavManager::PavManager(void)") ;
	
	// $[TI1]

	// $[PA24037]
	wyePressure_ = RInspPressureSensor.getValue() ;
	wyePressure_1_ = wyePressure_ ;
	wyePressureSlope_ = 0.0 ;
	wyePressureSlope_1_ = 0.0 ;
	wyePressureSlope_2_ = 0.0 ;

	NovRamManager::GetExhResistance( exhResistance_) ;
	NovRamManager::GetInspResistance( inspResistance_) ;
	
    intrinsicPeep_ = 0.0 ;  
	endExhLungPressure_ = 0.0 ;
	endPlateauPressure_ = 0.0 ;
	inspLungVolume_ = 0.0 ;
	endPlateauPressureSlope_ = 0.0 ;
	prevEndExhLungPressure_ = 0.0 ;
	
	pauseComplete_ = FALSE ;
	isPlateauDataReady_ = FALSE ;
	isPavPause_	= FALSE ;
	forcePavPauseFlag_ = FALSE ;
	
	breathsSinceLastPause_ = 0 ;

	// Schedule mnvrs at a fixed rate for engineering lab use otherwise random
	if ((SoftwareOptions::GetSoftwareOptions().getDataKeyType()) ==
	     SoftwareOptions::ENGINEERING)
	{
		nextPauseTarget_ = 3;
	}
	else
	{
		nextPauseTarget_ = 4 + (rand() % 7) ;
	}

	timeElapsedSinceFirstReconnect_ = TEN_MINUTES ;
	totalWob_ = 0.0 ;
	patientWob_ = 0.0 ;
	patientElastanceWob_ = 0.0 ;
	patientResistiveWob_ = 0.0 ;
	previousPercentSupport_ = 0.0 ;

	ibw_ = 0.0;
	
	// Initialize maxLung.  It gets used to initialize other variables in reset() below
	complianceData_.maxLung = 0.0;
	
	reset() ;		// must be called before newInspiration()
	newInspiration() ;
	
	// $[PA24027]
	complianceData_.filter.resetDirectionalFilter( complianceData_.maxLung) ;
	resistanceData_.filter.resetDirectionalFilter( resistanceData_.minLung) ;
	
	
	
     // zero PEEPi data buffers
     for(Uint32 ii = 0; ii < INTRINSIC_PEEP_BUFFER_SIZE; ii++)
       {
          intrinsicPeepBuffer_.lungPressure[ii] = 0.0;
          intrinsicPeepBuffer_.wyePressure[ii] = 0.0;
       }
      
     peepHundredMsTick_ = 0;
     nextPeepPosition_ =  0;
	 intrinsicPeepWyePressure_ = 0.0;
	 triggerPressureDrop_ = 0.0;

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PavManager()  
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  	none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PavManager::~PavManager(void)
{
	CALL_TRACE("PavManager::~PavManager(void)") ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newInspiration()  
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called at the begining of each PA breath by the PavPhase class.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If a pav pause was performed on the last inspiration, complete
//		the calculation for R and C.  Initialize data members for a start
//		of another inspiration.
//
//		The calculation of each limit is in its own block to the prevent
//		misuse that can occur through maintenance (e.g., using 'A_MIN_C'
//		instead of 'A_MIN_R').
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavManager::newInspiration( void)
{
	if (performPavPause_)
	{
		// $[TI1]
		completePreviousBreathCalculation_() ;
	}
	// $[TI2]

	autozeroOccurred_ = FALSE ;
    resistiveTargetQLungSum_ = 0.0 ;
    elasticTargetQLungSum_ = 0.0 ;	
	numRRawValues_ = 0 ;
	
	const Real32 IBW_VALUE =
		PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value ;

	if (ibw_ != IBW_VALUE)
	{
		// $[PA24035]
		// $[PA24021] a -- Clung-max...
		{
			const Real32  A_MAX_C = 0.4396;		// mL/cmH2O...
			const Real32  B_MAX_C = 1.1500;		// mL/cmH2O-kg...

			complianceData_.maxLung = A_MAX_C + (B_MAX_C * IBW_VALUE);
		}

		// $[PA24035]
		// $[PA24021] b -- Clung-min...
		{
			const Real32  A_MIN_C = -0.0112;		// mL/cmH2O...
			const Real32  B_MIN_C =  0.0993;		// mL/cmH2O-kg...

			complianceData_.minLung = A_MIN_C + (B_MIN_C * IBW_VALUE);
		}

		// Clung startup...
		{
			Real32 complianceFactor = 2.0 ;
			complianceData_.startupLung = complianceData_.maxLung * complianceFactor ;
		}

		// stored as "static", because result is the same with every run;
		// "static" eliminates need to re-calculate this with every call...
		static const Real32  SEC_TO_MIN_FACTOR_ = 1.0 / 60.0 ;

		// $[PA24022] b -- Rlung-max...
		{
			const Real32  A_MAX_R   =  12.87844;
			const Real32  B_MAX_R   =   0.0026925479;
			const Real32  C_MAX_R   = 405.97947;
			const Real32  D_MAX_R   =  -2.3564501;
			const Real32  POW_VALUE = (Real32)(pow(IBW_VALUE, D_MAX_R));

			// stored as "static", because result is the same with every run;
			// "static" eliminates need to re-calculate this with every call...
			static const Real32  AB_MAX_R_ = A_MAX_R * B_MAX_R;

			resistanceData_.maxLung =
			((AB_MAX_R_ + (C_MAX_R * POW_VALUE)) / (B_MAX_R + POW_VALUE))
			* SEC_TO_MIN_FACTOR_;	// cmh20/lpm
		}

		// $[PA24027] -- Rlung-min...
		{
			const Real32  A_MIN_R = 0.26296206;
			const Real32  B_MIN_R = 0.012029096;
			const Real32  C_MIN_R = 0.36810559;

			// stored as "static", because result is the same with every run;
			// "static" eliminates need to re-calculate this with every call...
			static const Real32  INVERSE_C_MIN_R_ = 1.0F / C_MIN_R;

			resistanceData_.minLung = (Real32)(
			pow((A_MIN_R + (B_MIN_R * IBW_VALUE)), -INVERSE_C_MIN_R_)
			* SEC_TO_MIN_FACTOR_);		   // cmh20/lpm

			// Rlung startup...
			resistanceData_.startupLung = resistanceData_.minLung * 0.5F ;
		}

		ibw_ = IBW_VALUE;
	} // if (ibw_ != IBW_VALUE)...
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: newCycle()  
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called every BD cycle to calculate data required for R and C
//		calculations.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Instantaneous wye pressures, slopes, work of breathing, raw compliance
//		and resistance values are calculated during inspiration and/or
//		exhalation phases.  Alarm status are also managed depending on the
//		state of the assessments.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavManager::newCycle( void)
{
  CALL_TRACE("PavManager::newCycle( void)") ;

  const SchedulerId::SchedulerIdValue schedulerId =
    			BreathPhaseScheduler::GetCurrentScheduler().getId();

  DiscreteValue supportType =
				PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE);

  if (schedulerId == SPONT && supportType == SupportTypeValue::PAV_SUPPORT_TYPE)
  {
	// $[TI26.1]
	Real32 inspPress = RInspPressureSensor.getValue() ;
	Real32 inspFlow = RAirFlowSensor.getValue() + RO2FlowSensor.getValue() ;

    DiscreteValue humidType = PhasedInContextHandle::GetDiscreteValue(SettingId::HUMID_TYPE) ;
   	Real32 inspBtps = Btps::GetInspBtpsCf() ;

	// limit inspBtps value to avoid divide by zero later
	if (inspBtps < ZERO_VALUE)
	{
		// $[TI1]
		inspBtps = ZERO_VALUE ; 
	}
	// $[TI2]

	// $[PA24016]
	// compensate for btps for non-hme humidifier
    if (humidType != HumidTypeValue::HME_HUMIDIFIER)
    {
		// $[TI3]
    	inspFlow /= inspBtps ;
    }
	// $[TI4]
	
	// $[PA24016]
	// $[PA24014]
	wyePressure_ = inspPress - inspResistance_.calculateDeltaP( inspFlow) ;
	
	// $[PA24018]
	wyePressureSlope_ = PavFilters::SlopeEstimator( wyePressure_, wyePressure_1_,
								wyePressureSlope_1_, wyePressureSlope_2_,
								0.125) ;

	BreathPhaseType::PhaseType phaseType =
				BreathPhase::GetCurrentBreathPhase()->getPhaseType() ;

    Real32 inspTime = (RBreathSet.getCurrentBreathRecord())->getInspTime();

	if (phaseType == BreathPhaseType::INSPIRATION
			|| phaseType == BreathPhaseType::PAV_INSPIRATORY_PAUSE)
	{
		// $[TI5]
		if (inspTime == CYCLE_TIME_MS)
		{
			// $[TI6]
			// first cycle of inspiration
			if ((BreathPhase *)&RPavPhase == BreathPhase::GetCurrentBreathPhase())
			{
				// $[TI7]
				// notify LungFlowExpTrigger to use esens in Lpm with PAV
                        	RLungFlowExpTrigger.setIsUseLpmEsens() ;

				if (!firstPaBreathOccurred_)
				{
					// $[TI8]
					// this is the first PA breath, notify alarms that STARTUP is beginning
					firstPaBreathOccurred_ = TRUE ;
					RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_PAV_STARTUP) ;
#ifdef SIGMA_PAV_UNIT_TEST
					BdAlarmId_UT = BdAlarmId::BDALARM_PAV_STARTUP ;
#endif // SIGMA_PAV_UNIT_TEST	
				}
				// $[TI9]

				// notify LungFlowExpTrigger to use special esens value during startup
				if (pavState_ == PavState::STARTUP)
				{
					// $[TI10]
					RLungFlowExpTrigger.setIsUsePavEsens() ;
				}
				// $[TI11]
			}

			// Compute the difference in wye pressure from the point intrinsic PEEP was
			// computed and the start of inspiration only if auto Peep was present.
			if (intrinsicPeep_ > 0.25) 
			{
				triggerPressureDrop_ = intrinsicPeepWyePressure_ - wyePressure_;
			}
			else
			{
				triggerPressureDrop_ = 0.0;
			}
		}
		// $[TI13]
		
		// $[PA24049]
		Real32 dt = CYCLE_TIME_MS / 1000.0F ;							// sec
		Real32 lungFlow = RLungData.getLungFlow() / 60.0F / inspBtps ;	// lps
        Real32 elasticTarget = RPavPhase.getElasticTarget() ;            // cmH2O
		Real32 resistiveTarget = RPavPhase.getResistiveTarget() ;        // cmH2O

        elasticTargetQLungSum_ += elasticTarget * lungFlow * dt ;       // cmH2O-l
        resistiveTargetQLungSum_ += resistiveTarget * lungFlow * dt;    // cmH2O-l                
	}
	else if (phaseType == BreathPhaseType::EXHALATION)
	{
		// $[TI14]
    	Real32 phaseDurationMs =
    			(RBreathSet.getCurrentBreathRecord())->getBreathDuration();

 		Int32 exhTime = (Int32)(phaseDurationMs - inspTime) ;

		if (exhTime == CYCLE_TIME_MS)
		{  	// first cycle of exhalation
			peakLungFlow_ = 0.0 ;
            nextPeepPosition_ = 0;
            peepHundredMsTick_ = 0;
			
			// $[PA24014]
			endPlateauPressure_ = wyePressure_ ;

			prevEndExhLungPressure_ = endExhLungPressure_ +  getIntrinsicPeep() + getTriggerPressureDrop();
			inspLungVolume_ = RLungData.getInspLungBtpsVolume() ;
			endPlateauPressureSlope_ = wyePressureSlope_ ;

           	intrinsicPeepBuffer_.lungPressure [nextPeepPosition_] = RLungData.getLungPressure();         
            intrinsicPeepBuffer_.wyePressure [nextPeepPosition_++] = wyePressure_ ;         
			
			// $[PA24049]
			const Real32 CMH20_L_TO_JOULES = 0.0981 ;
			
			// Only update Wob related values if this is a PA spontaneous breath
			BreathType breathType = (RBreathSet.getCurrentBreathRecord())->getBreathType();
           	if (breathType == ::SPONT)
			{
				Real32 totalElasticWob = CMH20_L_TO_JOULES * elasticTargetQLungSum_ / inspLungVolume_ * 1000.0F ;
				Real32 totalResistiveWob = CMH20_L_TO_JOULES * resistiveTargetQLungSum_ / inspLungVolume_ * 1000.0F ;
			
				Real32 percentSupport = RPavPhase.getPercentSupport() ;

				// avoid divide by zero
				if (percentSupport > ZERO_VALUE)
				{
					// $[TI16]
					Real32 factor = 1.0F - percentSupport ;
			
					Real32 totalWobRaw = totalElasticWob + totalResistiveWob ;
					Real32 patientWobRaw = factor * totalWobRaw ;
					Real32 patientElastanceWobRaw = factor * totalElasticWob ;
					Real32 patientResistiveWobRaw = factor * totalResistiveWob ;
				
					if (pavState_ == PavState::CLOSED_LOOP &&  percentSupport == previousPercentSupport_)
				    {        
					    const Real32 ALPHA_WOB = 0.9 ;
        	            totalWob_ = ALPHA_WOB * totalWob_ + (1.0F - ALPHA_WOB) * totalWobRaw ;
            	        patientWob_ = ALPHA_WOB * patientWob_ + (1.0F - ALPHA_WOB) * patientWobRaw ;
                	    patientElastanceWob_ = ALPHA_WOB * patientElastanceWob_ + (1.0F - ALPHA_WOB) * patientElastanceWobRaw ;
                    	patientResistiveWob_ = ALPHA_WOB * patientResistiveWob_ + (1.0F - ALPHA_WOB) * patientResistiveWobRaw ;
					}
					else
					{
            	        totalWob_ = totalWobRaw ;
                	    patientWob_ = patientWobRaw ;
                    	patientElastanceWob_ = patientElastanceWobRaw ;
	                    patientResistiveWob_ = patientResistiveWobRaw ;
					}
				}
				else
				{
					// $[TI17]
					totalWob_ = -1.0 ;
					patientWob_ = -1.0 ;
					patientElastanceWob_ = -1.0 ;
					patientResistiveWob_ = -1.0 ;
				}

	       	    previousPercentSupport_ = percentSupport;
			
				// $[PA24013]
				if (pauseComplete_)
				{
					// $[TI18]
					pauseComplete_ = FALSE ;
					complianceData_.cRaw = computeCRaw_() ;
				}
				// $[TI19]
			}
		}
		else 
		{  // All exhalation cycles except first

			if (!BreathRecord::IsExhFlowFinished() )
			{
				// $[TI21]
				endExhLungPressure_ = wyePressure_ ;

				// Wrap intrinsic PEEP buffer pointer
    	        if ( nextPeepPosition_ >= INTRINSIC_PEEP_BUFFER_SIZE )
        	    {
                      peepHundredMsTick_++;
                      nextPeepPosition_ = 0;
                }
        
            	intrinsicPeepBuffer_.lungPressure [nextPeepPosition_] = RLungData.getLungPressure();         
	            intrinsicPeepBuffer_.wyePressure [nextPeepPosition_++] = wyePressure_ ; 
			}
			else
			{
				// Exhalation flow is finished so compute intrinsic PEEP at this point.
				intrinsicPeep_ = RLungData.getLungPressure() - wyePressure_ ;            
			}
		
			// We have a crossover so snapShot intrinsicPeep_
			if ((RLungData.getLungPressure() > wyePressure_)  &&  !BreathRecord::IsExhFlowFinished())
		 	{
		    	if (!peepHundredMsTick_)
            	{
              		if(nextPeepPosition_ > 0)
              		{
	 					intrinsicPeepWyePressure_ = intrinsicPeepBuffer_.wyePressure[nextPeepPosition_ - 1];
                 		intrinsicPeep_ = intrinsicPeepBuffer_.lungPressure[nextPeepPosition_ - 1] -
										intrinsicPeepWyePressure_;
             		}			
              		else
              		{
						intrinsicPeepWyePressure_ = intrinsicPeepBuffer_.wyePressure[0];
                  		intrinsicPeep_ = intrinsicPeepBuffer_.lungPressure[0] - 
                  						intrinsicPeepWyePressure_;
              		}
            	}
	            else
    	        {
        	         if ( nextPeepPosition_ >= INTRINSIC_PEEP_BUFFER_SIZE )
            	     {
						intrinsicPeepWyePressure_ = intrinsicPeepBuffer_.wyePressure[0];
						intrinsicPeep_ = intrinsicPeepBuffer_.lungPressure[0] - 
                    	  					intrinsicPeepWyePressure_;
	                 }
    	             else
        	         {
	 					intrinsicPeepWyePressure_ = intrinsicPeepBuffer_.wyePressure[nextPeepPosition_];
            	     	intrinsicPeep_ = intrinsicPeepBuffer_.lungPressure[nextPeepPosition_] -
											intrinsicPeepWyePressure_;
                  	}
	            }
			}
		}

		if (!autozeroOccurred_)
		{
			// $[TI23]
			autozeroOccurred_ = RPressureXducerAutozero.getIsAutozeroInProgress() ;
		}
		// $[TI24]

		// $[PA24013]
		// $[PA24017]
		computeRRaw_() ;

	}
	// $[TI25]
	
	timeElapsedSinceFirstReconnect_ += CYCLE_TIME_MS ;
  }
  // $[TI26.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getEPatient()
//
//@ Interface-Description
//		This method has the PavState as an argument and returns the
//		calculated patient elastance.
//---------------------------------------------------------------------
//@ Implementation-Description
//		During STARTUP, the elastance is set the minimum value until more
//		data is collected; otherwise, the patient's elastance is returned.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
PavManager::getEPatient( const PavState::Id state)
{
	CALL_TRACE("PavManager::getEPatient( const PavState::Id state)") ;
		
	Real32 rtnValue ;

	// $[PA24028]
	if (state == PavState::STARTUP)
	{
		// $[TI1]
		Real32 percentSupport = RPavPhase.getPercentSupport() ;

		rtnValue = 1.0F / complianceData_.startupLung +
					complianceData_.nE * (percentSupport / complianceData_.cPatient -
							1.0F / complianceData_.startupLung) / Real32(MAX_QUALIFIED_) ;
	}
	else
	{
		// $[TI2]
		// $[PA24031]
		rtnValue = 1.0F / complianceData_.cPatient ;
	}
	
	return( rtnValue) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getRPatient()
//
//@ Interface-Description
//		This method has the PavState as an argument and returns the
//		calculated patient resistance.
//---------------------------------------------------------------------
//@ Implementation-Description
//		During STARTUP, the resistance is set the minimum value until more
//		data is collected; otherwise, the patient's resistance is returned.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32
PavManager::getRPatient( const PavState::Id state)
{
	CALL_TRACE("PavManager::getRPatient( const PavState::Id state)") ;
	
	Real32 rtnValue ;

	// $[PA24029]
	if (state == PavState::STARTUP)
	{
		// $[TI1]
		Real32 percentSupport = RPavPhase.getPercentSupport() ;

		rtnValue = resistanceData_.startupLung +
				resistanceData_.nR * (percentSupport * resistanceData_.rPatient
							 - resistanceData_.startupLung) / Real32(MAX_QUALIFIED_) ;
	}
	else
	{
		// $[TI2]
		// $[PA24031]
		rtnValue = resistanceData_.rPatient ;
	}
		
	return( rtnValue) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isPerformPavPause()
//
//@ Interface-Description
//		This method has the current breath phase pointer and the terminating
//		inspiration trigger id as arguments and returns whether to perform
//		the PA inspiratory pause maneuver.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Returns true if
//		1) the current breath was a PA breath
//		2) terminated via lung flow method
//		3) inspiration lasted for at least 300 msec
//		4) inspired volume meets the specified ratio
//		5) The delta pressure is greater than 2 cmH20
//		6) last breath's exhaled volume is 0.75 inspired volume
//		7) statistics on Ti are met
//		8) statistics Vti are met
//		9) enough breaths have occurred since last pause
//		Otherwise FALSE
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Boolean
PavManager::isPerformPavPause( const BreathPhase* pBreathPhase, const Trigger::TriggerId id)
{
	CALL_TRACE("PavManager::isPerformPavPause( const BreathPhase* pBreathPhase, \
						const Trigger::TriggerId id)") ;

	// $[PA24013]
	if ((BreathPhase *)&RPavPhase == BreathPhase::GetCurrentBreathPhase())
	{
		// $[TI1]
		
		const Real32 IBW_VALUE =
					PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value ;

		// $[PA24036]
		Uint32 inspTime = pBreathPhase->getElapsedTimeMs() ;
		Real32 currentInspVolume = RLungData.getInspLungBtpsVolume() ;	

		performPavPause_ = TRUE ;
		updateStatistics_( inspTime, currentInspVolume) ;

		Real32 tiAbsDiff = ABS_VALUE( inspTime - tiStats_.average) ;
		Real32 vTiAbsDiff = ABS_VALUE( currentInspVolume - vTiStats_.average) ;
		
		Real32 tolerance;

		if (BreathRecord::GetExhTime() <= 600)
		{
			CLASS_ASSERTION((BreathRecord::GetExhTime() > 0));

			tolerance = 10
				+ ((0.10F * inspLungVolume_ * 600) / BreathRecord::GetExhTime());
		}
		else
		{
			tolerance = 10 + (0.10F * inspLungVolume_);
		}

		if (forcePavPauseFlag_)
		{
			// $[TI11]
			// leave 'performPavPause_' as 'TRUE', thereby forcing a pause...
			forcePavPauseFlag_ = FALSE;
		}
		else if (id != Trigger::LUNG_FLOW_EXP)
		{
			// $[TI2]
			// $[PA24020] a
			performPavPause_ = FALSE ;
		}
		else if (inspTime < 300)
		{
			// $[TI3]
			// $[PA24020] b
			performPavPause_ = FALSE ;
		}
		else if (currentInspVolume / IBW_VALUE < 1.0)
		{
			// $[TI4]
			// $[PA24020] c
			performPavPause_ = FALSE ;
		}
		else if (wyePressure_ < endExhLungPressure_ + 2.0)
		{
			// $[TI5]
			// $[PA24020] d
			performPavPause_ = FALSE ;
		}
		// compare last breath's exhaled vs. inspired volumes
		else if ((RLungData.getExhLungBtpsVolume() + tolerance)
											< (0.75 * inspLungVolume_))	
		{
			// $[TI6]
			// $[PA24020] e
			performPavPause_ = FALSE ;
		}
		else if (tiAbsDiff > (3.0 * tiStats_.stdDev)  &&  tiAbsDiff > 100.0)
		{
			// $[TI7]
			// $[PA24020] f
			performPavPause_ = FALSE ;
		}
		else if (vTiAbsDiff > (3.0 * vTiStats_.stdDev)
					&&  vTiAbsDiff > (vTiStats_.average / 4.0))
		{
			// $[TI8]
			// $[PA24020] g
			performPavPause_ = FALSE ;
		}
		else
		{
			// $[TI9]
			// pause is allowed, check scheduler to see if okay
			performPavPause_ = pauseScheduler_() ;
		}

		breathsSinceLastPause_++ ;
	}
	else
	{
		// $[TI10]
		performPavPause_ = FALSE ;
	}
	
	return( performPavPause_) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: recoveryFromDisconnect()
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called by the PavPhase when recovering from a disconnect.
//---------------------------------------------------------------------
//@ Implementation-Description
//		During STARTUP, reset PA breath scheduling.  During CLOSED_LOOP,
//		use 0.5 of patient resistance if the time from last
//		disconnect is greater than TEN_MINUTES.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavManager::recoveryFromDisconnect( void)
{
	CALL_TRACE("recoveryFromDisconnect()") ;

	if (pavState_ == PavState::STARTUP)
	{
		// $[TI1]
		// $[PA24030] c
		// $[PA24033]
		reset() ;			
	}
	else if (pavState_ == PavState::CLOSED_LOOP)
	{
		// $[TI2]
		// force a PAV pause on the first breath following a disconnect...
		forcePavPauseFlag_ = TRUE ;

		if (timeElapsedSinceFirstReconnect_ > TEN_MINUTES)
		{
			// $[TI3]
			// $[PA24034]
			resistanceData_.rPatient *= 0.5 ;
			timeElapsedSinceFirstReconnect_ = 0 ;
		}
		// $[TI4]
	}
	else
	{
		AUX_CLASS_ASSERTION_FAILURE( pavState_) ;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reset()
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called by the SpontScheduler to reset the PA breath scheduling
//		to STARTUP.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Initialize data members to initial state.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavManager::reset( void)
{
	CALL_TRACE("PavManager::reset( void)") ;

	pavState_ = PavState::STARTUP ;
	firstPaBreathOccurred_ = FALSE ;
	timeElapsedSinceFirstReconnect_ = TEN_MINUTES ;
	forcePavPauseFlag_ = TRUE ;
		
	performPavPause_ = FALSE ;

	complianceData_.nE = 0 ;
	complianceData_.filter.resetStatisticalFilter() ;
	complianceData_.cRaw = complianceData_.maxLung ;
	complianceData_.cPatient = complianceData_.maxLung ;

	resistanceData_.nR = 0 ;
	resistanceData_.filter.resetStatisticalFilter() ;
	resistanceData_.rRaw = 0.0 ;
    resistanceData_.rTotal = 0.0 ;
	resistanceData_.rPatient = 0.0 ;

	// $[PA24036]
	tiStats_.sum = 0.0 ;
	tiStats_.sumSquared = 0.0 ;
	tiStats_.average = 0.0 ;
	tiStats_.stdDev = 0.0 ;
	tiStats_.index = 0 ;
	tiStats_.bufferFull = FALSE ;
	
	vTiStats_.sum = 0.0 ;
	vTiStats_.sumSquared = 0.0 ;
	vTiStats_.average = 0.0 ;
	vTiStats_.stdDev = 0.0 ;
	vTiStats_.index = 0 ;
	vTiStats_.bufferFull = FALSE ;

	for (Uint32 ii=0; ii < VTI_TI_BUFFER_SIZE; ii++)
	{
		// $[TI1]
		tiStats_.buffer[ii] = 0.0 ;
		vTiStats_.buffer[ii] = 0.0 ;
	}
	// $[TI2]

	RPavPhase.reInitPercentSupport() ;

	numTruncatedBreaths_ = 0 ;
	numPaBreaths_ = 0 ;
	breathTruncatedIndex_ = 0 ;

	for (Uint32 ii=0; ii < TOTAL_BREATHS; ii++)
	{
		// $[TI3]
		isBreathTruncated_[ii] = FALSE ;
	}
	// $[TI4]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: paBreathTruncated()
//
//@ Interface-Description
//		This method has a boolean as an argument and returns nothing.  This
//		method is called by PavPhase to inform this class if the current
//		PA breath was prematurely truncated.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If the paState is CLOSED_LOOP, update the number of truncated breaths
//		counter and update the circular buffer with the	current boolean status.
//		If the number of truncated breaths exceeds or equals RESET_BREATHS,
//		then reset the PA breath scheduling	to STARTUP and reset alarms.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavManager::paBreathTruncated( const Boolean status)
{
	CALL_TRACE("PavManager::paBreathTruncated( const Boolean status)") ;

	// $[PA24043]
	if (pavState_ == PavState::CLOSED_LOOP)
	{
		// $[TI1]
		if (isBreathTruncated_[breathTruncatedIndex_] == TRUE)
		{
			// $[TI2]
			numTruncatedBreaths_-- ;
		}
		// $[TI3]
		
		if (status == TRUE)
		{
			// $[TI4]	
			numTruncatedBreaths_++ ;
		}
		// $[TI5]	

		isBreathTruncated_[breathTruncatedIndex_] = status ;
	
		numPaBreaths_++ ;
	
		if (++breathTruncatedIndex_ >= TOTAL_BREATHS)
		{
			// $[TI6]	
			breathTruncatedIndex_ = 0 ;
		}
		// $[TI7]	

		const Uint32 RESET_BREATHS = 6 ;
		if (numPaBreaths_ >= TOTAL_BREATHS && numTruncatedBreaths_ >= RESET_BREATHS)
		{
			// $[TI8]	
			reset() ;
			resetAlarms() ;
		}
		// $[TI9]	
	}
	// $[TI10]	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resetAlarms()
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called to disable all PA assosciated alarms.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Disable STARTUP and ASSESSMENT alarms.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavManager::resetAlarms( void)
{
	CALL_TRACE("PavManager::resetAlarms( void)") ;
	
	// $[TI1]	
	RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_NOT_PAV_STARTUP) ;
	RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_NOT_PAV_ASSESSMENT) ;
	
#ifdef SIGMA_PAV_UNIT_TEST
	BdAlarmId_UT = BdAlarmId::BDALARM_NOT_PAV_ASSESSMENT ;
#endif // SIGMA_PAV_UNIT_TEST	
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: resyncAlarms()
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called by UiEvent when ALARM_RESET is pressed on the GUI.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Reset the appropriate alarm depending on pavState.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PavManager::resyncAlarms( void)
{
	CALL_TRACE("PavManager::resyncAlarms( void)") ;

    const SchedulerId::SchedulerIdValue schedulerId =
    			BreathPhaseScheduler::GetCurrentScheduler().getId();

	DiscreteValue supportType =
				PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE);

	if (schedulerId == SPONT && supportType == SupportTypeValue::PAV_SUPPORT_TYPE)
	{
	    if (pavState_ == PavState::STARTUP)
	    {
		    // $[TI1]	
		    RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_PAV_STARTUP) ;
#ifdef SIGMA_PAV_UNIT_TEST
		    BdAlarmId_UT = BdAlarmId::BDALARM_PAV_STARTUP ;
#endif // SIGMA_PAV_UNIT_TEST	
	    }
	    else if (pavState_ == PavState::CLOSED_LOOP)
	    {
		    // $[TI2]	
		    RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_PAV_ASSESSMENT) ;
#ifdef SIGMA_PAV_UNIT_TEST
		    BdAlarmId_UT = BdAlarmId::BDALARM_PAV_ASSESSMENT ;
#endif // SIGMA_PAV_UNIT_TEST	
	    }
	    else
	    {
		    AUX_CLASS_ASSERTION_FAILURE( pavState_) ;
	    }
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PavManager::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PAVMANAGER,
                          lineNumber, pFileName, pPredicate);
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================
//=====================================================================
//
//  Private Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: computeCRaw_()  
//
//@ Interface-Description
//		This method has no arguments and returns the computed raw patient
//		compliance value (ml/cmh20).
//---------------------------------------------------------------------
//@ Implementation-Description
//		Compute cRaw.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Real32 
PavManager::computeCRaw_( void) 
{
	CALL_TRACE("PavManager::computeCRaw_( void)") ;
		
	// $[PA24014]
	Real32 pressureDelta = endPlateauPressure_ - prevEndExhLungPressure_ ;

	if (ABS_VALUE( pressureDelta) < 0.01)
	{
		// $[TI1]	
		pressureDelta = 0.01 ;
	}
	// $[TI2]	
		
	
	const Real32 VOLUME_OFFSET = 0.0;
		
	Real32 cRaw = (inspLungVolume_ + VOLUME_OFFSET) / pressureDelta ;
	
	return( cRaw) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: computeRRaw_()  
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method is
//		called to compute and record the raw patient resistance value (cmH20/lpm).
//---------------------------------------------------------------------
//@ Implementation-Description
//		The rRaw value is calculated at the lung flow's peak value.
//
//  SDCR #6135  Based upon input from the Field Evaluations in Canada, the following
//              PAV specific breath delivery change is needed:  Remove the accuracy 
//              correction factor for Resistance in PAV which will result in less 
//              aggressive delivery at the start of a breath.
//
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void 
PavManager::computeRRaw_( void)
{
	CALL_TRACE("PavManager::computeRRaw_( void)") ;
		
	Real32 exhLungBtpsVolume = RLungData.getExhLungBtpsVolume() ;

	// $[PA24016]
	Real32 pLung = endPlateauPressure_ - exhLungBtpsVolume / complianceData_.cRaw ;

	Real32 btpsLungFlow = ABS_VALUE( RLungData.getLungFlow() * Btps::GetExpBtpsCf()) ;

	// avoid divide by zero
	if (btpsLungFlow < ZERO_VALUE)
	{
		// $[TI1]	
		btpsLungFlow = ZERO_VALUE ;
	}
	// $[TI2]	

	Real32 rTotal = (pLung - wyePressure_) / btpsLungFlow ;
        
	Real32 rRaw = rTotal - RTube.getPressDrop( btpsLungFlow) / btpsLungFlow ;	// [cmh20/lpm]

	if (rRaw < 0.0)
	{
		// $[TI5]
		rRaw = 0.0 ;
	}
	// $[TI6]
	
	// $[PA24017]
	// record rRaw at peak flow value
	if (btpsLungFlow > peakLungFlow_)
	{
		// $[TI3]	
		peakLungFlow_ = btpsLungFlow ;
		resistanceData_.rRaw = rRaw ;
        resistanceData_.rTotal = rTotal ;
		numRRawValues_ = 1 ;
	}
	else
	{
		// $[TI4]
		static const Real32  ALPHA_ = 2.0 / 3.0 ;

		if (numRRawValues_ < 3)
		{
			// $[TI7]
			resistanceData_.rRaw = (ALPHA_ * resistanceData_.rRaw)
										+ ((1.0F - ALPHA_) * rRaw);
			numRRawValues_++ ;
		}
		// $[TI8]
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: completePreviousBreathCalculation_()  
//
//@ Interface-Description
//		This method has no arguments and returns nothing.  This method
//		is called at the beginning of each inspiration where a PAV pause
//		took place on the previous breath to complete the R and
//		assessment.
//---------------------------------------------------------------------
//@ Implementation-Description
//		If isCRawAccepted, then apply statistical and directional
//		filtering to obtain patient compliance values.  If isRRawAccepted,
//		then apply statistical and directional filtering to obtain patient
//		resistance values.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void 
PavManager::completePreviousBreathCalculation_( void)
{
	CALL_TRACE("PavManager::completePreviousBreathCalculation_( void)") ;

	Boolean isCRawAccepted = validateCRaw_() ;
	Boolean isRRawAccepted = validateRRaw_( isCRawAccepted) ;
	
	Real32 cAccepted = 0.0F ;

	if (isCRawAccepted)
	{
		// $[TI1]	
		// $[PA24023] 
		cAccepted = complianceData_.filter.statisticalFilter( complianceData_.cPatient, complianceData_.cRaw) ;

		// $[PA24024] 
		Real32 alphaUp = 0.725 ;
		Real32 alphaDown = 0.875 ;

		if (pavState_ == PavState::STARTUP)
		{
			// $[TI2]	
			alphaUp = 0.5 ;
			alphaDown = 0.5 ;
		}
		// $[TI3]	

		if (complianceData_.nE == 0)
		{
			// $[TI4]	
			complianceData_.filter.resetDirectionalFilter( cAccepted) ;
		}
		// $[TI5]	
		
		complianceData_.cPatient = complianceData_.filter.directionalFilter( cAccepted, alphaUp, alphaDown) ;

		// $[PA24035]
		if (complianceData_.cPatient < complianceData_.minLung)
		{
			// $[TI6]	
			complianceData_.cPatient = complianceData_.minLung ;
		}
		else if (complianceData_.cPatient > 2.0 * complianceData_.maxLung)
		{
			// $[TI7]	
			complianceData_.cPatient = 2.0F * complianceData_.maxLung ;
		}
		// $[TI8]	
		
		if (++complianceData_.nE > MAX_QUALIFIED_)
		{
			// $[TI9]	
			complianceData_.nE = MAX_QUALIFIED_ ;
		}
		// $[TI10]	
	}
	// $[TI11]	

	Real32 rAccepted = 0.0 ;

	if (isRRawAccepted)
	{
		// $[TI12]	
		// $[PA24023] 
		rAccepted = resistanceData_.filter.statisticalFilter( resistanceData_.rPatient, resistanceData_.rRaw) ;

		// $[PA24024] 
		Real32 alphaUp = 0.875 ;
		Real32 alphaDown = 0.725 ;

		if (pavState_ == PavState::STARTUP)
		{
			// $[TI13]	
			alphaUp = 0.5 ;
			alphaDown = 0.5 ;
		}
		// $[TI14]	

		if (resistanceData_.nR == 0)
		{
			// $[TI15]
			resistanceData_.filter.resetDirectionalFilter( rAccepted) ;
		}
		// $[TI16]
		
		resistanceData_.rPatient = resistanceData_.filter.directionalFilter( rAccepted, alphaUp, alphaDown) ;

		// $[PA24019] 
		if (resistanceData_.rPatient < 0.0)
		{
			// $[TI17]
			resistanceData_.rPatient = 0.0 ;
		}
		else if (resistanceData_.rPatient > resistanceData_.maxLung)
		{	
			// $[TI18]
			resistanceData_.rPatient = resistanceData_.maxLung ;
		}
		// $[TI19]
		
		if (++resistanceData_.nR > MAX_QUALIFIED_)
		{
			// $[TI20]
			resistanceData_.nR = MAX_QUALIFIED_ ;
		}
		// $[TI21]

		if (pavState_ == PavState::CLOSED_LOOP)
		{
			// $[TI22]
			// valid assessment restart alarm timers
			RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_NOT_PAV_ASSESSMENT) ;
			RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_PAV_ASSESSMENT) ;
#ifdef SIGMA_PAV_UNIT_TEST
			BdAlarmId_UT = BdAlarmId::BDALARM_PAV_ASSESSMENT ;
#endif // SIGMA_PAV_UNIT_TEST	
		}
		// $[TI23]
	}
	else
	{
		// $[TI24]
		// $[PA24030] b
		// schedule another assesment on the next breath
		breathsSinceLastPause_ = nextPauseTarget_ ;
	}

	// $[PA24025]
	// $[PA24029]
	if (complianceData_.nE >= MAX_QUALIFIED_
			&&  resistanceData_.nR >= MAX_QUALIFIED_
			&&  pavState_ == PavState::STARTUP)
	{
		// $[TI25]
		// terminate startup alarm and start rc assessment alarm timer
		RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_NOT_PAV_STARTUP) ;
		RBdAlarms.postBdAlarm( BdAlarmId::BDALARM_PAV_ASSESSMENT) ;

		pavState_ = PavState::CLOSED_LOOP ;
	}
	// $[TI26]

	// plateau data is ready when both are accepted...
	isPlateauDataReady_ = (isCRawAccepted   &&  isRRawAccepted);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: updateStatistics_()  
//
//@ Interface-Description
//		This method has the insp time and the current inspired volume
//		as arguments and returns nothing.  This method is called to update
//		the insp time and inspired volumes statistical buffers.
//---------------------------------------------------------------------
//@ Implementation-Description
//		Update buffers with the data passed in and the sum calculatiions.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void 
PavManager::updateStatistics_( const Uint32 inspTime, const Real32 currentInspVolume)
{
	CALL_TRACE("PavManager::updateStatistics_( const Uint32 inspTime, \
							const Real32 currentInspVolume)") ;

	// $[PA24036]
	Uint32 index = tiStats_.index % VTI_TI_BUFFER_SIZE ;
	
	tiStats_.sum = tiStats_.sum - tiStats_.buffer[index] + inspTime ;
	tiStats_.sumSquared = tiStats_.sumSquared
					- tiStats_.buffer[index] * tiStats_.buffer[index]
					+ inspTime * inspTime ;
	tiStats_.buffer[index] =(Real32) inspTime ;
	tiStats_.index++ ;

	if (tiStats_.index > 1)
	{
		// $[TI1]
		if (tiStats_.index < VTI_TI_BUFFER_SIZE)
		{
			// $[TI2]
			tiStats_.stdDev = (tiStats_.sumSquared - tiStats_.sum * tiStats_.sum / tiStats_.index)
							/ (tiStats_.index - 1) ;
			tiStats_.average = tiStats_.sum / tiStats_.index ;
		}
		else
		{
			// $[TI3]
			tiStats_.stdDev = (tiStats_.sumSquared - tiStats_.sum * tiStats_.sum / VTI_TI_BUFFER_SIZE)
							/ (VTI_TI_BUFFER_SIZE - 1) ;
			tiStats_.average = tiStats_.sum / VTI_TI_BUFFER_SIZE ;
		}
		//TODO E600 changed 0.5 to 0.5f to avoid compiler confusion
		tiStats_.stdDev = (Real32)(pow( tiStats_.stdDev, 0.5f)) ;				
	}
	else
	{
		// $[TI4]
		tiStats_.stdDev = 0.0 ;
		tiStats_.average = tiStats_.sum ;
	}

	// $[PA24036]
	if (inspTime >= 300)
	{
		// $[TI5]
		index = vTiStats_.index % VTI_TI_BUFFER_SIZE ;
	
		vTiStats_.sum = vTiStats_.sum - vTiStats_.buffer[index] + currentInspVolume ;
		vTiStats_.sumSquared = vTiStats_.sumSquared
						- vTiStats_.buffer[index] * vTiStats_.buffer[index]
						+ currentInspVolume * currentInspVolume ;
		vTiStats_.buffer[index] = currentInspVolume ;
		vTiStats_.index++ ;

		if (vTiStats_.index > 1)
		{
			// $[TI6]
			if (vTiStats_.index < VTI_TI_BUFFER_SIZE)
			{
				// $[TI7]
				vTiStats_.stdDev = (vTiStats_.sumSquared - vTiStats_.sum * vTiStats_.sum / vTiStats_.index)
								/ (vTiStats_.index - 1) ;

				vTiStats_.average = vTiStats_.sum / vTiStats_.index ;
			}
			else
			{
				// $[TI8]
				vTiStats_.stdDev = (vTiStats_.sumSquared - vTiStats_.sum * vTiStats_.sum / VTI_TI_BUFFER_SIZE)
								/ (VTI_TI_BUFFER_SIZE - 1) ;
				vTiStats_.average = vTiStats_.sum / VTI_TI_BUFFER_SIZE ;
			}
			//TODO E600 changed 0.5 to 0.5f to avoid compiler confusion
			vTiStats_.stdDev = (Real32)(pow( vTiStats_.stdDev, 0.5f)) ;				
		}
		else
		{
			// $[TI9]
			vTiStats_.stdDev = 0.0 ;
			vTiStats_.average = vTiStats_.sum ;
		}
	}
	// $[TI10]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: pauseScheduler_()  
//
//@ Interface-Description
//		This method has no arguments and returns whether it is time to
//		perform a PA pause.
//---------------------------------------------------------------------
//@ Implementation-Description
//		During STARTUP, return TRUE.  During CLOSED_LOOP, return TRUE if
//		the number of breaths since last pause is equal to a random number
//		between 4 and 10 or for engineering use 3; else FALSE.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Boolean
PavManager::pauseScheduler_( void)
{
	CALL_TRACE("PavManager::pauseScheduler_( void)") ;

	Boolean rtnValue = FALSE ;

	if (pavState_ == PavState::STARTUP)
	{
		// $[TI1]
		// $[PA24025]
		rtnValue = TRUE ;
		breathsSinceLastPause_ = 0 ;
	}
	else if (pavState_ == PavState::CLOSED_LOOP
				&&  breathsSinceLastPause_ >= nextPauseTarget_)
	{
		// $[TI2]
		// $[PA24020]
		// $[PA24032]
		rtnValue = TRUE ;
		
		// Schedule mnvrs at a fixed rate for engineering lab use otherwise random
		if ((SoftwareOptions::GetSoftwareOptions().getDataKeyType()) ==
		     SoftwareOptions::ENGINEERING)
		{
			nextPauseTarget_ = 3;
		}
		else
		{
	    	nextPauseTarget_ = 4 + (rand() % 7) ;
		}
	   
		breathsSinceLastPause_ = 0 ;
	}
	// $[TI3]

	return( rtnValue) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: validateCRaw_()  
//
//@ Interface-Description
//		This method has no arguments and returns whether the cRaw value
//		obtained is valid or not.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The cRaw value is invalid if it meets the following
//		1) PA pause is terminated before 300 msec
//		2) cRaw < minLung
//		3) cRaw > maxLung
//		4) expired volume < 0.25 * inspired volume
//		5) endPlateauPressureSlope_ > maxSlope
//		6) endPlateauPressureSlope_ < minSlope
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Boolean
PavManager::validateCRaw_( void)
{
	CALL_TRACE("Boolean validateCRaw_( void)");

	Boolean isValid = TRUE ;

	if (!isPavPause_)
	{
		// $[TI1]
		// $[PA24021] f
		isValid = FALSE ;
	}
	else if (complianceData_.cRaw < complianceData_.minLung)
	{
		// $[TI2]
		// $[PA24021] b
		isValid = FALSE ;
	}
	else if (complianceData_.cRaw > 2.0 * complianceData_.maxLung)
	{
		// $[TI3]
		// $[PA24021] a
		isValid = FALSE ;
	}
	else if (RLungData.getExhLungBtpsVolume() < 0.25 * inspLungVolume_)
	{
		// $[TI4]
		// $[PA24021] e
		isValid = FALSE ;
	}
	// $[TI5]

	// $[PA24021] c
	// $[PA24021] d
	Real32 percentSupport = RPavPhase.getPercentSupport() ;

	Real32 maxSlope = 0.02F + 0.03F * (1.0F - percentSupport) ;
	Real32 minSlope = -0.02F - 0.02F * percentSupport ;

	if (pavState_ == PavState::STARTUP)
	{
		// $[TI6]
		maxSlope = 0.05 ;
		minSlope = -0.04 ;
	}
	// $[TI7]
	
	if (endPlateauPressureSlope_ > maxSlope || endPlateauPressureSlope_ < minSlope)
	{
		// $[TI8]
		isValid = FALSE ;
	}
	// $[TI9]

	return( isValid) ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: validateRRaw_()  
//
//@ Interface-Description
//		This method has whether cRaw is accepted as an argument and returns
//		whether the rRaw value obtained is valid or not.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The rRaw value is invalid if it meets the following
//		1) autozero occurred
//		2) cRaw not accepted
//		3) rRaw > 2.0 * maxLung
//		4) rRaw < 0.0
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

Boolean
PavManager::validateRRaw_( const Boolean isCRawAccepted)
{
	CALL_TRACE("PavManager::validateRRaw_( const Boolean isCRawAccepted)") ;
	
	Boolean isValid = TRUE ;

	if (autozeroOccurred_ || !isCRawAccepted)
	{
		// $[TI1]
		// $[PA24022] a
		// $[PA24022] c
		// $[PA24015]
		isValid = FALSE ;
	}
	else if (resistanceData_.rRaw > 2.0 * resistanceData_.maxLung)
	{
		// $[TI2]
		// $[PA24022] b
		isValid = FALSE ;
	}
	// $[TI4]

	return( isValid) ;
}


