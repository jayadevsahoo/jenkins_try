
#ifndef DataCorruptionInfo_HH
#define DataCorruptionInfo_HH

//=====================================================================
// This is a proprietary work to which Nellcor Puritan Bennett Inc of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Nellcor Puritan Bennett Inc of California.
//
//            Copyright (c) 1996, Nellcor Puritan Bennett Inc
//=====================================================================

//====================================================================
// Class: DataCorruptionInfo - used as an interface to NovRamManager.
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Safety-Net/vcssrc/DataCorruptionInfo.hhv   25.0.4.0   19 Nov 2013 14:20:38   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  15-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  hct   Date:  22-Jan-1996    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Post.hh"
#include "Background.hh"
#include "SafetyNet.hh"

//@ Usage-Classes
//@ End-Usage


//@ Constant: MAX_DATA_CORRUPTIONS
// The maximum number of data corruptions allowed before Vent Inop is declared.
static const Int16 MAX_DATA_CORRUPTIONS = 3;

//@ Constant: ONE_DAY_IN_HOURS
// The number of hours in a day.
static const Uint32 ONE_DAY_IN_HOURS = 24;

class DataCorruptionInfo
{
  public:

    struct CorruptionDatum
    {
        Uint32   time;
        Boolean  active;
    };

    DataCorruptionInfo(void);
    DataCorruptionInfo(const DataCorruptionInfo& corruptionInfo);
    ~DataCorruptionInfo(void);
    
    void  operator=(const DataCorruptionInfo& corruptionInfo);
    Boolean  update(void);

    static void  SoftFault (const SoftFaultID  softFaultID,
							const Uint32       lineNumber,
							const char*        pFileName,
							const char*        pPredicate);

  private:
    void clear_(void);

    //@ Data-Member:    corruptionData_
    // Array of corruption information.
    CorruptionDatum corruptionData_[MAX_DATA_CORRUPTIONS];
};


#endif // DataCorruptionInfo_HH 
