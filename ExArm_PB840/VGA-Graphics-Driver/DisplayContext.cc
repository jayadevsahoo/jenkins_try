#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1994-1995, Puritan-Bennett Corporation
//=====================================================================  

// =========================== M O D U L E   D E S C R I P T I O N ====
//@ Class: DisplayContext -- Display Context for VGA Library Functions
//---------------------------------------------------------------------
//@ Interface-Description
//  The DisplayContext class is a virtual base class that provides an
//  abstract interface for the application to control and display data
//  on a device. It provides an interface for device independent drawing
//  methods. Using the DisplayContext interface class, the application
//  should be able to draw to any type of display or memory device
//  using the same interface. VgaDisplayContext and NullDisplayContext
//  are derived from this class. The former is used to control a VGA
//  display, the latter is used to write to a display context with no
//  associated display.
//---------------------------------------------------------------------
//@ Rationale
//  Contains a context information and generic drawing interface for 
//  any display device.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/DisplayContext.ccv   25.0.4.0   19 Nov 2013 14:37:28   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: rpr     Date: 11-Mar-2011     SCR Number: 6752
//  Project: XENA2
//      ExpandFont and ExpandIcon passes the original witdth of the font/bitmap
//		so clipping can be accounted for properly.
// 
//  Revision: 008   By: mnr    Date: 24-Sep-2010    SCR Number: 6672
//  Project: XENA2
//  Description:
//      New method expandIcon() added.
//
//  Revision: 007   By: gdc    Date: 01-Feb-2007     SCR Number: 6237
//  Project: Trend
//  Description:
//  Trend project related changes. Moved the no-operation methods from
//  NullDisplayContext to the base class DisplayContext so
//  NullDisplayContext won't have to be modified every time a method 
//  is added to DisplayContext. Added off-screen image storage and
//  recovery to implement the trend cursor overlay.
//
//  Revision: 006   By: erm    Date:  12-Oct-2001    DCS Number: 5959
//  Project: GUIComms
//  Description:
//  Code clean-up
//
//  Revision: 005   By: gdc    Date:  27-Sep-2001    DCS Number: 5493
//  Project: GuiComms
//  Description:
//  Made this class a base class and moved all VGA related functionality
//  to the VgaDisplayContext class. The NullDisplayContext implements
//  functionality for controlling a display context with no associated
//  display device.
//
//  Revision: 004   By: gdc    Date:  20-Sep-2000    DCS Number: 5770
//  Project: GUIComms
//  Description:
//	Copy additional longword in expandFont to account for non-longword
//  alignment of bitmaps.
//
//  Revision: 003   By: gdc    Date:  28-Aug-2000    DCS Number: 5493
//  Project: GUIComms
//  Description:
//	Implemented single screen compatibility. Made this class the virtual base
//  class to allow for "pseudo" DisplayContexts not associated with 
//  a VGA device. VgaDeviceContext is now the concrete class.
//
//  Revision: 002   By: hct    Date:  28-MAR-2000    DCS Number: 5493
//  Project: GuiComms
//  Description:
//      Add GuiComms functionality.
//
//  Revision: 001   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//=====================================================================  
#include "DisplayContext.hh"
#include "GraphicsContext.hh"

//@ Usage-Classes
//@ End-Usage

//=====================================================================  
//
//  Public Methods...
//
//=====================================================================  

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: DisplayContext [constructor]
//
//@ Interface-Description
//  Contructor for the DisplayContext class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
DisplayContext::DisplayContext(Uint32 width, Uint32 height, Uint32 bitsPerPixel)
  : pDisplayMemory_(NULL)
  , pDisplayFrame_(NULL)
  , memorySize_(0)
  , width_(width)
  , height_(height)
  , bitsPerPixel_(bitsPerPixel)
  , lockPending_(FALSE)
{  
  // $[TI1]
	width_ = 640; height_ = 480; bitsPerPixel_ = 8;
	for (Int i=0; i<countof(colormapCache_); i++)
	{
		colormapCache_[i].rgbRed = 0;
		colormapCache_[i].rgbGreen = 0;
		colormapCache_[i].rgbBlue = 0;
		colormapCache_[i].rgbReserved = 0;
	}
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~DisplayContext [destructor]
//
//@ Interface-Description
//  Destructor for the DisplayContext class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
DisplayContext::~DisplayContext()
{
  // no code
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: accessDisplay
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
DisplayContext::accessDisplay() const
{
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: lock
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
DisplayContext::lock(void)
{
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: unlock
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
DisplayContext::unlock()
{
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: showFrame
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
DisplayContext::showFrame() 
{
  // $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: allocateColor
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
DisplayContext::allocateColor(Byte & index, const RgbQuad & rgb)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: storeColor
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
DisplayContext::storeColor(Byte index, const RgbQuad & rgb, Boolean readOnly)
{
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getColor
//
//@ Interface-Description
//  Returns a RgbQuad with rgb indexes set to zero to satisfy client,
//  otherwise performs no operation.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
DisplayContext::getColor(Byte index, RgbQuad & rgbQuad) 
{
  rgbQuad.rgbRed = 0;
  rgbQuad.rgbGreen = 0;
  rgbQuad.rgbBlue = 0;
  // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: moveBlock
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition                         
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
DisplayContext::moveBlock(Int xSrc, Int ySrc, Int xDest, Int yDest, 
						  Int xWidth, Int yHeight)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: allocateImage
//
//@ Interface-Description
// Performs no operation in this class. Returns NULL.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void * 
DisplayContext::allocateImage(Uint32 size)
{
	return NULL;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: freeImage
//
//@ Interface-Description
// Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
DisplayContext::freeImage(void * pImage)
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: storeImage
//
//@ Interface-Description
// Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
DisplayContext::storeImage(void * pDest, Int xSrc, Int ySrc, Int xWidth, Int yHeight)
{
}



//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: recoverImage
//
//@ Interface-Description
// Performs no operation in this class.
//  Recreates the block stored in off-screen memory by storeImage back
//  onto the screen. Client specifies the block's x and y destination, 
//  width and height, and source off-screen memory location.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Uses the BitBLT engine to move the data inside VGA memory and 
//  expand the data to on-screen memory.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
DisplayContext::recoverImage(Int xDest, Int yDest, const void * pSrc, 
								Int xWidth, Int yHeight)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillSolid
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
DisplayContext::fillSolid(
					Int xDest, Int yDest, Int xWidth, Int yHeight, Int color)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: expandFont
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
DisplayContext::expandFont(Int xDest, Int yDest, Int xWidth, Int yHeight,
		                   const Byte * pFontData, Uint fontOffset,
						   Int color, Int orignalWidth)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: expandIcon
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void 
DisplayContext::expandIcon(Int xDest, Int yDest, Int xWidth, Int yHeight,
		                   const Byte * pFontData, Uint fontOffset,
						   Int color, Int orignalWidth)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setClip
//
//@ Interface-Description
//  Sets the clipping area for the specified GraphicsContext to
//  the specified coordinates.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
DisplayContext::setClip(
					GraphicsContext & rGc, 
					Int x0, Int y0, Int x1, Int y1)
{
  // $[TI1]
  rGc.setClip(*this, x0, y0, x1, y1);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawPoint
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
DisplayContext::drawPoint(
					GraphicsContext & rGc, 
					Int x0, Int y0)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawLine
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
DisplayContext::drawLine(
					GraphicsContext & rGc, 
					Int x0, Int y0, Int x1, Int y1)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillRectangle
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
DisplayContext::fillRectangle( 
					GraphicsContext & rGc, 
					Int x0, Int y0, Int x1, Int y1 )
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawTriangle
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
DisplayContext::drawTriangle( 
					GraphicsContext & rGc,
					Int x0, Int y0,
					Int x1, Int y1,
					Int x2, Int y2 )
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: fillTriangle
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
DisplayContext::fillTriangle( 
					GraphicsContext & rGc,
					Int x0, Int y0,
					Int x1, Int y1,
					Int x2, Int y2 )
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawBitmap
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
DisplayContext::drawBitmap( 
					GraphicsContext & rGc,
					DibInfo & rDibInfo,
					Int xPos, Int yPos )
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawText
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
 
void  
DisplayContext::drawText(
					GraphicsContext & rGc,
					Int xPos, Int yPos, Uint fontId, const wchar_t* pString)
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: drawJapaneseText
//
//@ Interface-Description
//  Performs no operation in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void  
DisplayContext::drawJapaneseText(
					GraphicsContext & rGc,
					Int xPos, Int yPos, Uint fontId, const char* pString)
{
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getCDC
//
//@ Interface-Description
//  Returns NULL to the client to indicate CDC is not assigned 
//  performs no operation in this class. It is just a interface.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
CDC*
DisplayContext::getCDC( void) const
{
  return NULL;
}
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: setCDC
//
//@ Interface-Description
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//===================================================================== 
void
DisplayContext::setCDC( CDC* )
{
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
DisplayContext::SoftFault(const SoftFaultID softFaultID,
		   const Uint32 lineNumber,
		   const char *pFileName,
		   const char *pPredicate)
{
  FaultHandler::SoftFault(softFaultID, VGA_GRAPHICS_DRIVERS,
			  VgaGraphicsDriver::DISPLAY_CONTEXT_ID, 
			  lineNumber, pFileName, pPredicate);
}

//=====================================================================  
//
//  Private Methods...
//
//=====================================================================  


