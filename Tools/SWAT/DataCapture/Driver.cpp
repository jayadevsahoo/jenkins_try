//----------------------------------------------------------------------------
//            Copyright (c) 2009 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
/// @file Driver.cpp
//----------------------------------------------------------------------------


//-------------------------------------
// I N C L U D E S
//-------------------------------------

#include"SocketServer.h" 
#include"SocketClient.h" 
#include"EventLogger.h"
#include"CDatabase.h"
#include"EventReader.h"
#include"CLogger.h"
#include<signal.h>   
#include<stdio.h>

#include"PatientDataDBHandler.h"
#include"PatientDataUIHandler.h"
#include"BdSignalDataDBHandler.h"

#include "DataCaptureIf.h"
#include "STDLogger.h"
#include "PortIds.hh"

static bool g_bRunning = false;

//STDLogger clSTDLogger("out.txt");

STDLogger clSTDLogger;

static void handler(int signum) 
{
    g_bRunning = false;
}



static bool delete_table(CDatabase *pDatabase, const char *table_name, std::string eid)
{
            char buffer[2000];

            sprintf_s(buffer,
                "DELETE "
                "FROM \"PatientData\".\"%s\" pd "
                "WHERE pd.event_sequence_id IN " 
                "(SELECT  event_sequence_id from \"PatientData\".event_data "
                "where (event_sequence_id < %s) AND (save_data = false) )",
                table_name,
                eid.c_str()
                );

            bool bRetval=  pDatabase->insert_query(buffer);
                    

            return bRetval;
}



///DataCapture implimentes the interface DataCaptureIf and is responsable for initialation.
///
class DataCapture:public DataCaptureIf
{
    public:

    DataCapture();

    virtual bool start();   
    virtual bool stop();

    virtual bool enable_logging();
    virtual bool disable_logging();
    virtual bool clean_database(unsigned int iTimeMin);

    
    virtual bool register_callback(bool (*function)(std::string pdata, void *pUser), void *pUser); 

    virtual bool register_logger(ILogger *pILogger);

    virtual bool get(char *pName, void *pData, unsigned int *pDataLength);

    virtual bool set(char *pName, void *pData, unsigned int *pDataLength);
    virtual bool command(char *command);

    virtual bool updateClientInfo(char *ip, char *port);


    private:


    void run_loop(CDatabase *pCDatabase);

    void ThreadEntranceProc();
    static void *ThreadEntranceProc   (void *pVoid);
    
    Semaphore *m_pSemaphore;
    bool      m_bRunning;
    
    // Varables used for cleanup.
    bool         m_bCleanup;
    unsigned int m_iTimeMin;

    Thread    *m_pThread;
    Mutex     *m_pMutex;
    std::string   m_ventGuiIP;
    SocketClient  *m_pSocketClient;     // Patient data simulation socket client

    EventLogger   *m_pEventLogger;
    EventLogger   *m_pEventLoggerPD;
    EventLogger   *m_pEventLoggerBdSignal;
 


};


bool DataCapture::updateClientInfo(char *ip, char *port)
{
	 	return m_pSocketClient->updateConfigureation(ip,port);
}


bool DataCapture::register_logger(ILogger *pILogger)
{
    LOG_MESSAGE(LM_DEBUG,"Registering of logger.");
    g_GlobalLogger.set(pILogger);
    return true;
}


bool  DataCapture::register_callback(bool (*function)(std::string pdata, void *pUser), void *pUser) 
{

    return true;
}

#if 0

DataCapture::DataCapture():m_bRunning(false),m_ip("10.168.0.203")
//DataCapture::DataCapture():m_bRunning(false),m_ip("192.168.0.2")
{
	//m_pSocketClient = new SocketClient(m_ip.c_str(),"7018","UDP");

	m_pSocketClient = new SocketClient(m_ip.c_str(),"7003","UDP");
}
#endif


DataCapture::DataCapture()
    : m_bRunning(false)
    , m_ventGuiIP("192.168.0.2")       // Ventilator GUI IP address, 
                                       // TODO E600, move this IP to common header
    , m_bCleanup(false)
{
    g_GlobalLogger.set(&clSTDLogger);
    
    // Convert Uint32 to char*, since SocketClient class accept char* port
    char patientDataSimulationPort[16];
    sprintf( patientDataSimulationPort, "%d", SWAT_PD_SIMULATION_UDP_PORT );

	m_pSocketClient = new SocketClient(m_ventGuiIP.c_str(), patientDataSimulationPort , "UDP");
}




bool DataCapture::start()
{

    LOG_MESSAGE(LM_INFO,"DataCapture(%X)::start() called.",this);

    m_pMutex     = new Mutex();  
    m_pSemaphore = new Semaphore();
    m_pThread    = new Thread(ThreadEntranceProc, (void *)this);

    m_bRunning = true;
    return true;
}

bool DataCapture::stop()
{
    LOG_MESSAGE(LM_INFO,"DataCapture(%X)::stop() called.",this);

    if(m_bRunning == true)
    {
        m_bRunning = false;
        m_pThread->join();

        delete m_pThread;
        delete m_pSemaphore;
        delete m_pMutex;
	//delete m_pSocketClient;
    }
    return true;
}


bool DataCapture::enable_logging()
{
	if(m_pEventLogger && m_pEventLoggerBdSignal)
	{
		m_pEventLogger->enable();
		m_pEventLoggerBdSignal->enable();
		return true;
	}
    return false;
}

bool DataCapture::disable_logging()
{
	if(m_pEventLogger && m_pEventLoggerBdSignal)
	{
		m_pEventLogger->disable();
		m_pEventLoggerBdSignal->disable();
		return true;
	}

    return false;
}

bool DataCapture::clean_database(unsigned int iTimeMin)
{
    int iMaxTime = 5;
    m_iTimeMin = iTimeMin;


    if(m_bCleanup==true)
    {
	return false;
    }
    m_bCleanup = true;



    return true;
}



bool DataCapture::get(char *pName, void *pData, unsigned int *pDataLength)
{

    return true;
}

bool DataCapture::set(char *pName, void *pData, unsigned int *pDataLength)
{
    return true;
}

bool DataCapture::command(char *command)
{
    return true;
}



void DataCapture::run_loop(CDatabase *pCDatabase)
{


    while(m_bRunning)
    {
        Sleep(2000);

        if(m_bCleanup==true)
        {

            char buffer[1024];
           
            LOG_MESSAGE(LM_INFO,"DataCapture(%X)::run_loop() cleaning database started.",this);

            sprintf_s(buffer,
                "SELECT  max(event_sequence_id) as event_sequence_id "
                "from \"PatientData\".event_data "
                "where event_time < (now()-interval '%i minutes');",m_iTimeMin);

            CResult *r  =pCDatabase->select_query(buffer);

            std::vector<CRow> v=r->getCRowVector();
            if(v.size()!=1)
            {
                LOG_MESSAGE(LM_ERROR,"Failed in select query.");
            }
            else 
            {
                int error=0;
                std::string eid = (v.begin())->getCol("event_sequence_id").get();
                if(eid.length() != 0)
                {
                    error+=!delete_table(pCDatabase, "AVERAGE_BREATH_DATA", eid);
                    error+=!delete_table(pCDatabase, "BILEVEL_MAND_DATA", eid);
                    error+=!delete_table(pCDatabase, "BILEVEL_MAND_EXH_DATA", eid);
                    error+=!delete_table(pCDatabase, "COMPUTED_INSP_PAUSE_DATA", eid);
                    error+=!delete_table(pCDatabase, "END_EXP_PAUSE_PRESSURE_STATE", eid);
                    error+=!delete_table(pCDatabase, "END_OF_BREATH_DATA", eid);
                    error+=!delete_table(pCDatabase, "FI02_FOR_GUI_MSG", eid);
                    error+=!delete_table(pCDatabase, "NIF_PRESSURE_DATA", eid);
                    error+=!delete_table(pCDatabase, "P100_PRESSURE_DATA", eid);
                    error+=!delete_table(pCDatabase, "PAUSE_PEEND_DATA", eid);
                    error+=!delete_table(pCDatabase, "PAUSE_PIEND_DATA", eid);
                    error+=!delete_table(pCDatabase, "PAV_BREATH_DATA", eid);
                    error+=!delete_table(pCDatabase, "PAV_PLATEAU_DATA", eid);
                    error+=!delete_table(pCDatabase, "PAV_STARTUP_DATA", eid);
                    //error+=!delete_table(pCDatabase, "PEEP_RECOVERY_MSG", eid);
                    error+=!delete_table(pCDatabase, "REALTIME_BREATH_DATA", eid);
                    error+=!delete_table(pCDatabase, "VITAL_CAPACITY_DATA", eid);
                    error+=!delete_table(pCDatabase, "VTPCV_STATE_DATA", eid);
                    error+=!delete_table(pCDatabase, "WAVEFORM_BREATH_DATA", eid);

                    error+=!delete_table(pCDatabase, "BDSIGNAL",   eid);
                    error+=!delete_table(pCDatabase, "event_data", eid);

                    if(error)
                    {
                        LOG_MESSAGE(LM_INFO,"Failed to delete %i tables.",error);
                    }
                }
                else {
                    LOG_MESSAGE(LM_INFO,"Nothing to delete.");
                }

                bool bRetval=  pCDatabase->insert_query("VACUUM");

                if(bRetval == false)
                {
                    LOG_MESSAGE(LM_ERROR,"Failed to vacume database.");
                }
            }


            LOG_MESSAGE(LM_INFO,"DataCapture(%X)::run_loop() cleaning database compleated.",this);

	    Sleep(30*1000);

            m_bCleanup=false;
        }
    }


}


void DataCapture::ThreadEntranceProc()
{



    SocketServer  *pSocketServer         = NULL;
    SocketServer  *pSocketServerW        = NULL;
    SocketServer  *pSocketServerBdSignal = NULL;


    EventReader   *pEventReader          = NULL;
    EventReader   *pEventReaderBdSignal  = NULL;
    EventReader   *pEventReaderW         = NULL;
    CDatabase     *pCDatabase            = NULL;

    m_pEventLogger          = NULL;
    m_pEventLoggerPD        = NULL;
    m_pEventLoggerBdSignal  = NULL;
 



    LOG_MESSAGE(LM_INFO,"Starting data capture.");

    pCDatabase    = new CDatabase(
            " user = postgres"
            " password = its4me@1234"
            //" hostaddr = 10.95.28.103"
            " hostaddr = 127.0.0.1"
            " connect_timeout = 5.0"
            " dbname = viking_integration_test"
            );

    m_pEventLogger          = new EventLogger();
    m_pEventLoggerPD        = new EventLogger();
    m_pEventLoggerBdSignal  = new EventLogger();

	//disable db logging for event table and BDsignal
	//only m_pEventLoggerPD is enabled for patient data overwrite
	m_pEventLogger->disable( );
	m_pEventLoggerBdSignal->disable( );	


    //m_pSocketClient         = new SocketClient(m_ip.c_str(),"7003","UDP");

    char patientDataUdpPort[16];
    sprintf( patientDataUdpPort, "%d", PATIENT_DATA_UDPPORT );
    pSocketServer         = new SocketServer(patientDataUdpPort,"UDP");

    char waveformUdpPort[16];
    sprintf( waveformUdpPort, "%d", WAVEFORM_UDPPORT );
    pSocketServerW        = new SocketServer(waveformUdpPort, "UDP");

    char bdSignalUdpPort[16];
    sprintf( bdSignalUdpPort, "%d", BD_DEBUG_DEST_UDPPORT1 );
    pSocketServerBdSignal = new SocketServer(bdSignalUdpPort, "UDP");


    ////////////////////////
    // Register callbacks with EventLogger instances.
    //
    // Each event logger repersents a thread of execution.  Callbacks are 
    // called by the EventLogger when an message of a given type is posted
    // to the EventLogger an EventReader.
    //
    ////////////////////////

    //PatientData set is the class that gets data from Bd and sends it to gui.
    PatientDataUIHandler ::register_types (m_pEventLoggerPD      , m_pSocketClient  );  //PatientData for UI.
    PatientDataDBHandler ::register_types (m_pEventLogger        , pCDatabase       );  //PatientData for DB.
    BdSignalDataDBHandler::register_types (m_pEventLoggerBdSignal, pCDatabase       );  //Waveform for DB.


    ////////////////////////
    // Create EventReaders associating each EventReader with one or more EventLoggers. 
    ////////////////////////
    {
        //Set of EventReaders
    	EventLogger *d[]={
    		m_pEventLogger,
    		m_pEventLoggerPD,
    		NULL
    	};
    	pEventReader     =  new EventReader(d,pSocketServer);
    }

    {

        //Set of EventReaders
    	EventLogger *d[]={
    		m_pEventLogger,
    		m_pEventLoggerPD,
    		NULL
    	};
    	pEventReaderW        =  new EventReader(d,pSocketServerW);

    }
    pEventReaderBdSignal =  new EventReader(m_pEventLoggerBdSignal,pSocketServerBdSignal );


    // Run loop until signaled to stop.
    run_loop(pCDatabase);

    // Cleanup
    delete pEventReaderBdSignal;
    delete pEventReaderW;
    delete pEventReader;


    delete pSocketServerBdSignal;
    delete pSocketServerW;
    delete pSocketServer;

	//TODO[ME]:Why did I need to call delete here?
    delete m_pSocketClient;
    delete m_pEventLoggerBdSignal;
    delete m_pEventLoggerPD;
    delete m_pEventLogger;

    delete pCDatabase;

    LOG_MESSAGE(LM_INFO,"Exiting data capture.");



}




void *DataCapture::ThreadEntranceProc   (void *pVoid)
{
    ((DataCapture* )pVoid)->ThreadEntranceProc ();
    Thread::exit();
    return NULL;
}




DataCapture g_instance;











#define EOF (-1)
 
#ifdef __cplusplus    // If used by C++ code, 
extern "C" {          // we need to export the C interface
#endif
 
__declspec(dllexport) DataCaptureIf * __cdecl DataCapture()
{
    return &g_instance;
}
 
#ifdef __cplusplus
}
#endif


#if 0
int main(int argc, char **argv)
{


    DataCapture r;
    r.start();

    while(1)
    {

        Sleep(1000);
    }
    
}

#endif
















