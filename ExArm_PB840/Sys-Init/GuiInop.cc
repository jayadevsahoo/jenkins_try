#include "stdafx.h"
//=====================================================================
//   This is a proprietary work to which Puritan-Bennett corporation
//   of California claims exclusive right.  No part of this work may
//  be used, disclosed, reproduced, sorted in an information retrieval
//     system, or transmitted by any means, electronic, mechanical,
//   photocopying, recording, or otherwise without the prior written
//      permission of Puritan-Bennett Corporation of California.
//
//          Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//============================== C L A S S   D E S C R I P T I O N ====
//@ Class: GuiInop - GUI State: Inop.
//---------------------------------------------------------------------
//@ Interface-Description
//  The GuiInop class is a FsmState used by the GuiFsm finite state
//  machine.  The class represents the state that GUI Task Control
//  enters upon completing a GUI transition to the Inop state.  It
//  contains methods for processing events while in this state.
//---------------------------------------------------------------------
//@ Rationale
//  This class contains the methods for processing events while
//  in the GUI Inop state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  GuiInop is derived from the FsmState class.  It overrides the
//  virtual method receiveBdReady to provide state specific behavior
//  when it receives a BdReadyMessage.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//  none
//---------------------------------------------------------------------
//@ Invariants
//  none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Sys-Init/vcssrc/GuiInop.ccv   25.0.4.0   19 Nov 2013 14:33:52   pvcs  $
//
//@ Modification-Log
//
//   Revision 002   By:   sah      Date: 01/13/99    DR Number: 5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to a non-inlined method.
//
//   Revision 001   By:   gdc      Date: 07/06/95    DR Number:   n/a  
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//
//=====================================================================

//=====================================================================
//
//      Includes...
//
//=====================================================================

#include "GuiInop.hh"
#include "SigmaState.hh"
#include "TaskFlags.hh"

//@ Usage-Classes
#include "TaskControlAgent.hh"
#include "BdReadyMessage.hh"
#include "TaskAgentManager.hh"
#include "InitConditions.hh"
//TODO E600 port #include "Reset.hh"
//@ End-Usage

//=====================================================================
//
//      Static Data...
//
//=====================================================================

//@ Code...

//=====================================================================
//
//      Public Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiInop [default constructor]
//
//@ Interface-Description
//  GuiInop class default constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

GuiInop::GuiInop()
{
    CALL_TRACE("GuiInop()");
    // $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiInop [destructor]
//
//@ Interface-Description
//  GuiInop class destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

 
GuiInop::~GuiInop()
{
    CALL_TRACE("~GuiInop()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
// Method: nameOf
//
// Interface-Description
//  Returns the name of the class.
//---------------------------------------------------------------------
// Implementation-Description
//  Returns a pointer to the const string containing the class name.
//---------------------------------------------------------------------
// PreCondition
//  none
//---------------------------------------------------------------------
// PostCondition
//  none
// End-Method
//=====================================================================

const char*
GuiInop::nameOf(void) const
{
    CALL_TRACE("nameOf(void)");
    return "GuiInop";
    // $[TI1]
}
 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getSigmaState
//
//@ Interface-Description
//  The getSigmaState method returns the SigmaState associated with
//  this GuiInop state.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Returns a SigmaState enumeration for the INOP state.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SigmaState
GuiInop::getSigmaState(void) const
{
    CALL_TRACE("getSigmaState(void)");
    return STATE_INOP;
    // $[TI1]
}
 
//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: receiveBdReady
//
//@ Interface-Description
//  The receiveBdReady method processes a BdReadyMessage received
//  from BD task control by GUI task control in the GuiInop state.
//  The GUI has transitioned to this state for one of two reasons,
//  either TUV has declared a BD failure or the application
//  (specifically Background) detected a GUI failure and transitioned
//  the GUI to INOP.  If the BD is in INOP and TUV has not failed the
//  BD, this method forwards the BdReadyMessage to the application
//  tasks and reports ready to the BD.  For BD ready in SERVICE, it
//  resets the GUI attempting to resynchronize with the BD.  For other
//  valid BD states (ONLINE, SST, or START), if the BD is not failed
//  and GUI service is not required then this method will reset the GUI
//  attempting resychronization with the BD.  Invalid BD states cause
//  an assertion.
//
//---------------------------------------------------------------------
//@ Implementation-Description 
//  Sets the local BdState to the state contained in the BdReadyMessage.  
//  TaskAgentManager::Send forwards the BdReadyMessage to each 
//  managed task.  Incompatible BD states results in this method 
//  asserting causing the GUI to reboot.
//---------------------------------------------------------------------
//@ PreCondition 
//  none
//---------------------------------------------------------------------
//@ PostCondition 
//  none 
//@ End-Method
//=====================================================================

void GuiInop::receiveBdReady(const BdReadyMessage& message) const {
CALL_TRACE("receiveBdReady(const BdReadyMessage&)");

    FsmState::SetBdState(message);

    switch (message.getState())
    {
        case STATE_INOP:     // $[TI1]
            //  no need to acknowledge a failed BD even in INOP
            if ( FsmState::GetBdState() != STATE_FAILED )
            {
                // $[TI1.1]
                TaskAgentManager::Send(message);
                TaskControlAgent::ReportNodeReady();
            }
            // $[TI1.2]
            break;
        case STATE_SERVICE:  // $[TI2]
//TODO E600 define reset   Reset::Initiate(INTENTIONAL, Reset::GUIINOP);
            break;
        case STATE_ONLINE:
        case STATE_SST:
        case STATE_START:    // $[TI3]
            if (   !InitConditions::IsServiceRequired() 
                && FsmState::GetBdState() != STATE_FAILED )
            {
                // $[TI3.1]
//TODO E600 define reset  Reset::Initiate(INTENTIONAL, Reset::GUIINOP);
            }
            // $[TI3.2]
            break;
        case STATE_TIMEOUT:  
        default:             // $[TI4]
            AUX_CLASS_ASSERTION_FAILURE(message.getState());
            break;
    }
}

 

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//
//@ Interface-Description
//   This method is called when a software fault is detected by the
//   fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//   'lineNumber' are essential pieces of information.  The 'pFileName'
//   and 'pPredicate' strings may be defaulted in the macro to reduce
//   code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//   This method receives the call for the SoftFault, adds it sub-system
//   and class name ID and sends the message to the non-member function
//   SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//   none
//---------------------------------------------------------------------
//@ PostCondition
//   none
//@ End-Method
//=====================================================================
void
GuiInop::SoftFault(const SoftFaultID  softFaultID,
                    const Uint32       lineNumber,
                    const char*        pFileName,
                    const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
 
  FaultHandler::SoftFault(softFaultID, SYS_INIT,
                          Sys_Init::ID_GUIINOP,
                          lineNumber, pFileName, pPredicate);
}


//=====================================================================
//
//      Protected Methods...
//
//=====================================================================

//=====================================================================
//
//      Private Methods...
//
//=====================================================================




