
#ifndef VgaPoint_HH
#define VgaPoint_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: VgaPoint - Interface class for VGA point-drawing functions.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/VGA-Graphics-Driver/vcssrc/VgaPoint.hhv   25.0.4.0   19 Nov 2013 14:37:58   pvcs  $
//
//@ Modification-Log
//  
//  Revision: 002   By: gdc    Date:  27-Jan-2000    DCS Number: 5337
//  Project: 840 Neonatal
//  Description:
//	Optimized VGA library for Neonatal project to use BitBlt and Windows
//  Bitmap structures.
//
//  Revision: 001   By: sah    Date:  01-Aug-1997    DCS Number: <NONE>
//  Project: Sigma (R8027)
//  Description:
//	Integration baseline.
//
//====================================================================

#include "VgaGraphicsDriver.hh"

//@ Usage-Classes
//@ End-Usage


class DisplayContext;
class GraphicsContext;

class VgaPoint 
{
  public:
    
	static void  DrawPoint(
		 DisplayContext & rDc, GraphicsContext & rGc, Int x, Int y );

#if defined(SIGMA_UNIT_TEST)
    static Uint8  ReadPoint(const DisplayContext & rDc, Int x, Int y );
#endif // defined(SIGMA_UNIT_TEST)

    static void  SoftFault(const SoftFaultID softFaultID,
			   const Uint32      lineNumber,
			   const char*       pFileName  = NULL, 
			   const char*       pPredicate = NULL);
  private:
	VgaPoint();    // not implemented
	~VgaPoint();   // not implemented
};

#endif // VgaPoint_HH 
