/***************************************************************************
*
*		Copyright (c) 1995 MICROTEC RESEARCH INCORPORATED.
*
*	All rights reserved. MICROTEC RESEARCH's source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	MICROTEC RESEARCH. Any attempt or participation in deciphering,
*	decoding, reverse engineering or in any way altering the source code
*	is strictly prohibited, unless the prior written consent of
*	MICROTEC RESEARCH is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/

/******************************************************************************
 ** in8254a.c                                                             
 **                                                                           
 **    This file contains all of the device-dependent, board-independent      
 **    functions to control a hardware timer of name in8254.
 **
 **    in8254a.c can support the other counter channels(chanel 1 and 2) of
 **    in8254 timer chip although the original in8254.c is designed to control
 **    for counter channel 0 only.
 **    The configuration structure is slightly different from the one for
 **    in8254.c.  Refer in8254a.txt for detail information.
 **
 **    The prefixes of functions are same as in8254.c because it should be
 **    replaced by this timer driver in the future.
 **
 *****************************************************************************/

#include "in8254a.h"  /* specific definitions and prototypes */

/******************************************************************************
 ** in8254_install
 **
 **    This routine installs the data structures for the hardware timer
 **
 ** Inputs:
 **           cfg              timer device configuration.
 **           dev              timer device descriptor.
 ** Return codes:
 **           VBSP_SUCCESS     The call succeeded
 **
 *****************************************************************************/
vbsp_return_t
in8254_install(in8254_config_t *cfg, timer_dev_desc_t *dev)
{
    /* set up the config table */
    /* linked to the additional RAM space for this device */
    dev->config = cfg->base_config;  
    
    /* install the register map */
    dev->map = &cfg->regs;
    
    /* initialize the external board function fops entry */
    dev->externio = cfg->externio;
    
    /* initialize the specific variables */
    dev->config.specific = (void *)&cfg->specific_cfg;
    
    /* set up the fops table */
    /* These functions MUST be defined! */
    dev->fops.init         = in8254_init;
    dev->fops.test         = in8254_test;
    dev->fops.get_count    = in8254_get_ns_per_count;
    dev->fops.set_count    = in8254_set_ns_per_count;
    dev->fops.get_preload  = in8254_get_preload;
    dev->fops.set_preload  = in8254_set_preload;
    dev->fops.get_rollover = in8254_get_rollover;
    dev->fops.read_timer   = in8254_read_timer;
    dev->fops.int_enable   = in8254_enable;
    dev->fops.int_disable  = in8254_disable;
    dev->fops.int_info_get = in8254_interrupt_status;
    
    return(VBSP_SUCCESS);
}

/******************************************************************************
 ** in8254_init                                                    
 **                                                                           
 ** This routine initializes the timer for a preloadic          
 ** interrupt.  Interrupts are enabled by this routine.
 **
 ** Inputs:                                                                   
 **           dev            device descriptor                                
 **           cfg             configuration parameters                        
 ** Outputs:                                                                  
 **           none                                                            
 **                                                                           
 ** Return codes:                                                             
 **           VBSP_SUCCESS             The call succeeded                     
 **           VBSP_INVALID_PERIOD      bad timer preload                       
 **           VBSP_INVALID_PRIORITY    bad interrupt priority value           
 **           VBSP_INVALID_VECTOR      bad interrupt vector number            
 **           VBSP_ILLEGAL_CONFIG      bad counter channel
 **                                                                           
 *****************************************************************************/
vbsp_return_t
in8254_init(timer_dev_desc_t *dev)
{
    unsigned long cnt;
    in8254_reg_map_t *regs = (in8254_reg_map_t *)dev->map;
    in8254_specific_t *specific = (in8254_specific_t *)dev->config.specific;
    unsigned char channel, mode;

    /* check preload value */
    if (dev->config.preload > TIMER_MAX)
	cnt = TIMER_MAX + 1;
    else
	cnt = dev->config.preload;

    /* check channel value and make bit data in control word */
    if (specific->channel > PIT_COUNTER_2)
	return(VBSP_ILLEGAL_CONFIG);

    /* check mode and make bit data in control word */
    if (specific->mode > PIT_MODE_5)
	return(VBSP_ILLEGAL_CONFIG);

    /* store counter value */
    REG_WRITE_B(dev,regs->in8254_cntrl, (specific->channel | PIT_RW_BOTH |
					 specific->mode));
    REG_WRITE_B(dev,regs->in8254_cnt, cnt & 0x00ff);
    REG_WRITE_B(dev,regs->in8254_cnt, ((cnt & 0xff00) >> 8));

    return(VBSP_SUCCESS);
}

/******************************************************************************
 ** in8254_disable()
 **
 **    Disable interrupts from the timer.
 **
 **
 ** Inputs:                                                                   
 **           dev            device descriptor
 ** Outputs:                                                                  
 **           none.                                                           
 **                                                                           
 **                                                                           
 *****************************************************************************/
vbsp_return_t
in8254_disable(timer_dev_desc_t *dev)
{

    /*
     **    Unable to read interrupt status:
     **
     **    If the device is unable to return the status of the interrupts,
     **    the user should update the current status of the interupts in
     **    a device specific field in the timer structure.  i.e. *specific
     **    of the timer_config_t structure should point to a structure that
     **    contains a variable that indicates the  status of the interrupts
     **    (ENABLED or DISABLED). For 8254, PIC MASK could be used, however
     **    it varies depending on H/W. Hence shadow is used.
     ** --------------------------------------------------------------- */
    
    ((in8254_specific_t*)(dev->config.specific))->flag_int_enabled = LOGIO_INT_DISABLED;
    BOARD_SPEC_INT_DISABLE(dev, 0); 	
    return(VBSP_SUCCESS);
}

/******************************************************************************
 ** in8254_enable()
 **
 **    Enables interrupts from the timer.
 **
 ** Inputs:
 **           dev            device descriptor
 **           priority        interrupt priority 
 ** Outputs:
 **           none.
 ** Return codes:
 **           VBSP_SUCCESS    The call succeeded
 **
 *****************************************************************************/
vbsp_return_t in8254_enable(timer_dev_desc_t *dev)
{
    ((in8254_specific_t*)(dev->config.specific))->flag_int_enabled = LOGIO_INT_ENABLED;
    BOARD_SPEC_INT_ENABLE(dev, 0);
    return(VBSP_SUCCESS);
}

/******************************************************************************
 ** in8254_interrupt_status()
 **
 **    get interrupt status, returns either interrupts enabled or interrupts
 **    disabled.
 **
 ** Inputs:
 **           dev            device descriptor
 ** Outputs:
 **           *status
 **                LOGIO_INT_ENABLED    - if interrupts are enabled
 **                LOGIO_INT_DISABLED   - if interrupts are disabled
 ** Return codes:
 **           VBSP_SUCCESS    The call succeeded
 **
 *****************************************************************************/
vbsp_return_t
in8254_interrupt_status(timer_dev_desc_t *dev, int *status)
{
    /*------------------------------------------------------------------
     ** INSTRUCTION:
     **
     **    replace in8254_INTERRUPTS_ENABLED_TEST with device specific
     **    code that will test the state of the timer interrupts and
     **    return non-zero if the interrupts are enabled.
     **
     **    Unable to read interrupt status:
     **
     **    If the device is unable to return the status of the interrupts,
     **    the user should include the current status of the interupts in
     **    a device specific field in the timer structure.  i.e. *specific
     **    of the timer_config_t structure should point to a variable that
     **    contains the status of the interrupts, or *specific should point
     **    to a structure where one of the variables contains the status
     **    of the interrupts (ENABLED or DISABLED).  *status should be set
     **    to the current state of that variable.
     **
     **    i.e. replace in8254_INTERRUPTS_ENABLED_TEST with the code:
     **
     **         (dev->config.specific.flag_int_enabled==LOGIO_INT_ENABLED)
     **
     **         This will check the status of the specific shadow
     **         interrupt status register.
     **
     ** --------------------------------------------------------------- */
    
    if (((in8254_specific_t*)(dev->config.specific))->flag_int_enabled)
	*status = (unsigned char)LOGIO_INT_ENABLED;
    else
	*status = (unsigned char)LOGIO_INT_DISABLED;
    return(VBSP_SUCCESS);
}

/******************************************************************************
 ** in8254_set_ns_per_count
 **
 **    Set the timer count value
 **
 **    If this function is not supported, set *count = 0;
 **                                                                           
 ** Inputs:                                                                   
 **           dev            device descriptor                                
 **           count          configuration parameters                        
 ** Outputs:                                                                  
 **           none.
 ** Return codes:                                                             
 **           VBSP_SUCCESS
 **                                                                           
 *****************************************************************************/
vbsp_return_t in8254_set_ns_per_count(timer_dev_desc_t *dev,
				      unsigned long *ns_per_count)
{
    *ns_per_count = dev->config.clock_period;
    return(VBSP_SUCCESS);
}


/******************************************************************************
 ** in8254_get_ns_per_count
 **
 **    Get the timer count 
 **    This function returns the base count value in nanoseconds.
 **
 ** Inputs:
 **           dev            device descriptor                                
 **           count           configuration parameters                        
 ** Outputs:
 **           none.
 ** Return codes:
 **           VBSP_SUCCESS    The call succeeded
 **
 *****************************************************************************/
vbsp_return_t
in8254_get_ns_per_count(timer_dev_desc_t *dev,
			unsigned long *ns_per_count)
{
    *ns_per_count = dev->config.clock_period;
    return(VBSP_SUCCESS);
}


/******************************************************************************
 ** in8254_get_preload
 **
 **       This function returns the timer rollover value
 **
 ** Inputs:
 **           dev            device descriptor                                
 **           *rollover      configuration parameters                        
 ** Outputs:
 **           none.
 ** Return codes:
 **           VBSP_SUCCESS    The call succeeded
 **
 *****************************************************************************/
vbsp_return_t
in8254_get_preload(timer_dev_desc_t *dev, unsigned long *preload)
{
    *preload = dev->config.preload;
    return(VBSP_SUCCESS);
}


/******************************************************************************
 ** in8254_set_preload
 **
 **    This function sets the timer preload value
 **
 ** Inputs:
 **           dev            device descriptor                                
 **           preload         configuration parameters                        
 ** Outputs:
 **           none.
 ** Return codes:
 **           VBSP_SUCCESS    The call succeeded
 **
 *****************************************************************************/
vbsp_return_t
in8254_set_preload(timer_dev_desc_t *dev, unsigned long *preload)
{
    unsigned char ctrl;
    in8254_reg_map_t *regs = (in8254_reg_map_t *)dev->map;
    in8254_specific_t *specific =(in8254_specific_t *)(dev->config.specific);
    
    /* check preload value */
    if (*preload > TIMER_MAX)
	*preload = TIMER_MAX + 1;

    dev->config.preload = *preload;

    /* prepare control word to read both byte of counter */
    ctrl = specific->channel;
    ctrl |= specific->mode;
    ctrl |= PIT_RW_BOTH;

    /* set preload value */
    REG_WRITE_B(dev,regs->in8254_cntrl, ctrl);
    REG_WRITE_B(dev,regs->in8254_cnt, *preload & 0x00ff);
    REG_WRITE_B(dev,regs->in8254_cnt, (*preload >> 8));

    return(VBSP_SUCCESS);
}

/******************************************************************************
 ** in8254_get_rollover
 **
 **    Get the timer maximum value.
 **
 ** Inputs:
 **           dev            device descriptor                                
 **           max            preload value                        
 ** Outputs:
 **           none.
 ** Return codes:
 **           VBSP_SUCCESS    The call succeeded
 **
 *****************************************************************************/
vbsp_return_t
in8254_get_rollover(timer_dev_desc_t *dev, unsigned long *max)
{
    *max = TIMER_MAX;
    return(VBSP_SUCCESS);
}


/******************************************************************************
 ** in8254_read_timer
 **
 **    read the current count value
 **
 **    If this capability is not supported, return the count as
 **    preload - 2.
 **
 ** Inputs:
 **           dev            device descriptor                                
 **           count           count value                        
 ** Outputs:
 **           none.
 ** Return codes:
 **           VBSP_SUCCESS    The call succeeded
 **
 *****************************************************************************/
vbsp_return_t
in8254_read_timer(timer_dev_desc_t *dev, unsigned long *count)
{
    *count = dev->config.preload  - in8254_counter_get_current(dev);
    return(VBSP_SUCCESS);
}

/******************************************************************************
 ** in8254_set_timer
 **
 **    sets the current count value
 **
 **    If this capability is not supported, return the count as
 **    preload - 2.
 **
 ** Inputs:
 **           dev            device descriptor                                
 **           count           count value                        
 ** Outputs:
 **           none.
 ** Return codes:
 **           VBSP_SUCCESS    The call succeeded
 **
 *****************************************************************************/
vbsp_return_t
in8254_set_timer(timer_dev_desc_t *dev, unsigned long *count)
{
    *count = dev->config.preload  - in8254_counter_get_current(dev);
    return(VBSP_SUCCESS);
}

/******************************************************************************
 ** in8254_test
 **
 **    interrupt service routine for the timer
 **    This function returns true always if there is no test to return zero.
 **
 ** Return codes: 
 **           0 - no interrupt occured
 **           1 - interrupt occurred
 **
 *****************************************************************************/
int in8254_test(logio_interrupt_entry_t *entry)  
{
    INTERRUPT_END((timer_dev_desc_t *)entry->id->data, 0);
    return(1);
}

in8254_counter_get_current(timer_dev_desc_t *dev)
{
    in8254_reg_map_t *regs = (in8254_reg_map_t *)dev->map;
    unsigned short count;
    unsigned char lsb, ctrl;
    in8254_specific_t *specific = (in8254_specific_t*)(dev->config.specific);

    /* prepare control word for counter latch operation */
    ctrl = specific->channel | PIT_LATCH;

    /* read counter value */
    REG_WRITE_B(dev, regs->in8254_cntrl, ctrl);
    lsb   = REG_READ_B(dev, regs->in8254_cnt);
    count = REG_READ_B(dev, regs->in8254_cnt);
    count = (count << 8) + lsb;
    return(count);
}



