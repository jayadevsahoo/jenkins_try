#ifndef ExitServiceModeSubScreen_HH
#define ExitServiceModeSubScreen_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993-1995, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: ExitServiceModeSubScreen - Activated by selecting Exit Tab button
// at ServiceLowerSelectArea to exit Service mode.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/ExitServiceModeSubScreen.hhv   25.0.4.0   19 Nov 2013 14:07:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date:  07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreen.hh"
#include "AdjustPanelTarget.hh"
#include "GuiTestManagerTarget.hh"

//@ Usage-Classes
#include "GuiTestManager.hh"
#include "SmTestId.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class ExitServiceModeSubScreen : public SubScreen, 
							public AdjustPanelTarget,
							public GuiTestManagerTarget 
{
public:
	ExitServiceModeSubScreen(SubScreenArea *pSubScreenArea);
	~ExitServiceModeSubScreen(void);

	void startTest_(SmTestId::ServiceModeTestId currentTestId);
	void nextTest_(void);
	
	// Redefine SubScreen activation/deactivation methods
	virtual void activate(void);
	virtual void deactivate(void);

	// AdjustPanelTarget virtual methods
	virtual void adjustPanelAcceptPressHappened(void);
	virtual void adjustPanelRestoreFocusHappened(void);

	// GuiTestManagerTarget virtual methods
	virtual void processTestPrompt(Int command);
	virtual void processTestKeyAllowed(Int keyAllowed);
	virtual void processTestResultStatus(Int resultStatus, Int testResultId=-1);
	virtual void processTestResultCondition(Int resultCondition);
	virtual void processTestData(Int dataIndex, TestResult *pResult);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:

private:
	// these methods are purposely declared, but not implemented...
	ExitServiceModeSubScreen(void);						// not implemented...
	ExitServiceModeSubScreen(const ExitServiceModeSubScreen&);	// not implemented...
	void operator=(const ExitServiceModeSubScreen&);		// not implemented...

	//@ Data-Member: testManager_
	// Object instance of GuiTestManager
	GuiTestManager testManager_;

	//@ Data-Member: currentTestId_
	// Id of the current Service Mode test.
	SmTestId::ServiceModeTestId currentTestId_;

};


#endif // ExitServiceModeSubScreen_HH 
