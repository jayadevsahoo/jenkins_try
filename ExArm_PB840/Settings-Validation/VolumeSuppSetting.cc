#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//================= C L A S S   D E S C R I P T I O N =================
//@ Class: VolumeSuppSetting - Target Support Volume
//---------------------------------------------------------------------
//@ Interface-Description
//  This is a batch, bounded setting that is used to limit the inspired
//  volume support, during spontaneous, volume-supported breaths.  This class
//  inherits from 'BatchBoundedSetting' and provides the specific information
//  needed for representation of this limit.  This information includes the
//  interval and range of this setting's values, this batch setting's
//  new-patient value (see 'getNewPatientValue()'), and the constraints of
//  this setting.
//---------------------------------------------------------------------
//@ Rationale
//  The purpose of this class is to encapsulate all unique data and behavior
//  for this specific setting.  The general purpose methods in the base
//  classes are overridden with derived methods in this class.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This class has no dependent settings, therefore 'calcDependentValues_()'
//  is NOT overridden.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/Settings-Validation/vcssrc/VolumeSuppSetting.ccv   25.0.4.0   19 Nov 2013 14:27:50   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By: mnr    Date: 28-Dec-2009     SCR Number: 6437
//  Project:  NEO
//  Description:
//		Lower limit based on Advanced option now. 
//	
//  Revision: 008   By: mnr    Date: 23-Nov-2009     SCR Number: 6522
//  Project:  NEO
//  Description:
//		Resolution to tenths from 2mL to 5mL.
//
//  Revision: 007   By: mnr    Date: 20-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Unused includes removed. Min value lowered to 2mL for NeoModeUpdate Option
//
//  Revision: 006   By: mnr    Date: 10-Jul-2009    SCR Number: 6437
//  Project:  NEO
//  Description:
//		Using new #define for lower tidal vol bound. 
//
//  Revision: 005   By: gdc    Date: 02-Mar-2009    SCR Number: 6478
//  Project:  840S
//  Description:
//		Initialized highVtiBasedMax resolution using BoundedValue
//		assignment operator. The decrement operation that followed 
//		detected an uninitialized resolution value when development 
//		debugging was turned on.
//
//  Revision: 004   By: gdc    Date: 26-Jan-2009    SCR Number: 6461
//  Project:  840S
//  Description:
//      Implemented #define workaround for compiler bug.
//
//  Revision: 003   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//  Modified to support neonatal non-invasive CPAP.
//
//  Revision: 002 By: jja     Date:  21-Mar-2002    DR Number: 5790
//  Project:  VTPC
//  Description:
//	Added Vt/Vti/Vsupp setting interaction/limitations for VC+ and VS
//
//  Revision: 001   By: sah   Date:  09-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//	VTPC project-related changes:
//      *  added this new setting to support VS
//
//=====================================================================

#include "VolumeSuppSetting.hh"
#include "SettingConstants.hh"
#include "SupportTypeValue.hh"
#include "ModeValue.hh"
#include "PatientCctTypeValue.hh"

//@ Usage-Classes
#include "SettingsMgr.hh"
#include "SoftwareOptions.hh"
#include "ContextMgr.hh"
#include "AdjustedContext.hh"
//@ End-Usage


//@ Code...

//====================================================================
//
//  Static Data Definitions...
//
//====================================================================

//  $[VC02000] The setting's range ...
//  $[VC02003] The setting's resolution ...

// use #define values to initialize static const struct to avoid compiler bug
static const BoundedInterval  LAST_NODE_ =
{
	DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS,
	0.0f,		// unused...
	ONES,		// unused...
	NULL
};
static const BoundedInterval  NODE3_ =
{
	DEFINED_TIDAL_VOL_NEONATAL_MIN_VALUE_MISC, // change-point...5.0f
	0.10f,		// resolution...
	TENTHS,		// precision...
	&::LAST_NODE_
};
static const BoundedInterval  NODE2_ =
{
	100.0f,		// change-point...
	1.0f,		// resolution...
	ONES,		// precision...
	&::NODE3_
};
static const BoundedInterval  NODE1_ =
{
	400.0f,		// change-point...
	5.0f,		// resolution...
	ONES,		// precision...
	&::NODE2_
};
static const BoundedInterval  INTERVAL_LIST_ =
{
	2500.0f,		// absolute maximum...
	10.0f,		// resolution...
	TENS,		// precision...
	&::NODE1_
};


//====================================================================
//
//  Public Methods...
//
//====================================================================

//============= M E T H O D   D E S C R I P T I O N ==================
//@ Method: VolumeSuppSetting()  [Default Constructor]
//
//@ Interface-Description
//  Create a default Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VolumeSuppSetting::VolumeSuppSetting(void)
: BatchBoundedSetting(SettingId::VOLUME_SUPPORT,
					  Setting::NULL_DEPENDENT_ARRAY_,
					  &::INTERVAL_LIST_,
					  VOLUME_SUPP_MAX_ID,	 // maxConstraintId...
					  VOLUME_SUPP_MIN_ID)	 // minConstraintId...
{
	CALL_TRACE("VolumeSuppSetting()");

	// register this setting's soft bounds...
	registerSoftBound_(::VOLUME_SUPP_SOFT_MIN_BASED_IBW_ID, Setting::LOWER);
	registerSoftBound_(::VOLUME_SUPP_SOFT_MAX_BASED_IBW_ID, Setting::UPPER);
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N ==================
//@ Method:  ~VolumeSuppSetting()  [Virtual Destructor]
//
//@ Interface-Description
//  Destroy this Setting.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

VolumeSuppSetting::~VolumeSuppSetting(void)
{
	CALL_TRACE("~VolumeSuppSetting()");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  getApplicability(qualifierId)  [const, virtual]
//
//@ Interface-Description
//  This is a virtual method that is overridden to return a boolean
//  indicating whether this setting is currently applicable.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[01081] -- breath settings applicability
//---------------------------------------------------------------------
//@ PreCondition
//  (qualifierId == Notification::ACCEPTED  ||
//   qualifierId == Notification::ADJUSTED)
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Applicability::Id
	VolumeSuppSetting::getApplicability(
									   const Notification::ChangeQualifier qualifierId
									   ) const
{
	CALL_TRACE("getApplicability(qualifierId)");

	Applicability::Id  applicability;

	if ( SoftwareOptions::IsOptionEnabled(SoftwareOptions::VTPC) )
	{  // $[TI1] -- VTPC is an active option...
		const Setting*  pSupportType =
			SettingsMgr::GetSettingPtr(SettingId::SUPPORT_TYPE);

		if ( pSupportType->getApplicability(qualifierId) !=
			 Applicability::INAPPLICABLE )
		{  // $[TI1.1] -- support type is applicable...
			DiscreteValue  supportTypeValue;

			switch ( qualifierId )
			{
				case Notification::ACCEPTED :	  // $[TI1.1.1]
					supportTypeValue = pSupportType->getAcceptedValue();
					break;
				case Notification::ADJUSTED :	  // $[TI1.1.2]
					supportTypeValue = pSupportType->getAdjustedValue();
					break;
				default :
					AUX_CLASS_ASSERTION_FAILURE(qualifierId);
					break;
			}

			applicability = (supportTypeValue == SupportTypeValue::VSV_SUPPORT_TYPE)
							? Applicability::CHANGEABLE	 // $[TI1.1.3]
							: Applicability::INAPPLICABLE;	 // $[TI1.1.4]
		}
		else
		{  // $[TI1.2] -- support type is NOT applicable...
			// volume supp can't be applicable if support type isn't...
			applicability = Applicability::INAPPLICABLE;
		}
	}
	else
	{  // $[TI2] -- VTPC is not an active option...
		applicability = Applicability::INAPPLICABLE;
	}

	return(applicability);
}


//================== M E T H O D   D E S C R I P T I O N ==============
//@ Method: acceptTransition(settingId, newValue, currValue) [virtual]
//
//@ Interface-Description
//  This method is responsible for updating this setting based on the
//  the Main Control Setting identified by 'settingId' and the transition
//  of its value from 'currValue' to 'newValue'.
//
//  The setting is interested in the transition of mode to "SIMV" or "SPONT,
//  and of support type to "VSV".
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[VC02007]  -- support type to VS...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	VolumeSuppSetting::acceptTransition(const SettingId::SettingIdType settingId,
										const DiscreteValue            newValue,
										const DiscreteValue)
{
	CALL_TRACE("acceptTransition(settingId, newValue, currValue)");

	if ( getApplicability(Notification::ADJUSTED) !=
		 Applicability::INAPPLICABLE )
	{	// $[TI1] -- volume support is applicable...
		// use new-patient value...
		BoundedValue  volumeSupp = getNewPatientValue();

		BoundStatus  dummyStatus(getId());

		// calculate bound constraints...
		updateConstraints_();

		// use the calculation of the maximum bound in 'updateConstraints_()'
		// to limit the current adjusted value to a valid range...
		getBoundedRange_().testValue(volumeSupp, dummyStatus);

        // store the transition value into the adjusted context...
		setAdjustedValue(volumeSupp);

		// activation of transition rule must be italicized...
		setForcedChangeFlag();
	} // $[TI2] -- volume support is NOT applicable...
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getNewPatientValue()  [const, virtual]
//
//@ Interface-Description
//  Return the setting's new patient value.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

SettingValue
	VolumeSuppSetting::getNewPatientValue(void) const
{
	CALL_TRACE("getNewPatientValue()");

	const Setting*  pTidalVolume =
		SettingsMgr::GetSettingPtr(SettingId::TIDAL_VOLUME);

	// $[VC02002] -- this setting's new-patient calculation...
	return(pTidalVolume->getNewPatientValue());	// $[TI1]
}


//============ M E T H O D   D E S C R I P T I O N ====================
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//	      [static]
//
//@ Interface-Description
//  This method is called when a software fault is detected by the
//  fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//  'lineNumber' are essential pieces of information.  The 'pFileName'
//  and 'pPredicate' strings may be defaulted in the macro to reduce
//  code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This method receives the call for the SoftFault, adds it sub-system
//  and class name ID and sends the message to the non-member function
//  SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	VolumeSuppSetting::SoftFault(const SoftFaultID  softFaultID,
								 const Uint32       lineNumber,
								 const char*        pFileName,
								 const char*        pPredicate)
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

	FaultHandler::SoftFault(softFaultID, SETTINGS_VALIDATION,
							VOLUME_SUPPORT_SETTING, lineNumber, pFileName,
							pPredicate);
}


//====================================================================
//
//  Protected Methods...
//
//====================================================================

//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  updateConstraints_()  [virtual]
//
//@ Interface-Description
//  Update the dynamic bounds of this setting.  This method sets this
//  setting's maximum constraint to the most restrictive of this setting's
//  upper bounds, and, correspondingly, set this setting's minimum
//  constraint to its most restrictive lower bound.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	VolumeSuppSetting::updateConstraints_(void)
{
	CALL_TRACE("updateConstraints_()");

	const Setting*  pSetting = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pSetting->getAdjustedValue()).value;

	Real32  softMinValue, softMaxValue;


	findSoftMinMaxValues_(softMinValue, softMaxValue);

	//===================================================================
	// calculate volume support's most restrictive MAXIMUM bound...
	//===================================================================
	{
		BoundedRange::ConstraintInfo  maxConstraintInfo;

		//-----------------------------------------------------------------
		// determine which upper limit to apply...
		//-----------------------------------------------------------------

		BoundedValue  ibwBasedMax;

		const Real32  ABSOLUTE_MAX = getAbsoluteMaxValue_();

		BoundedValue  highVtiBasedMax;
		highVtiBasedMax.value = ABSOLUTE_MAX + 1.0f;  // Ensure a high value to start
		maxConstraintInfo.value = ABSOLUTE_MAX;
		BoundStatus dummyStatus(getId());
		Boolean useAbsoluteMax = TRUE;	   // assume absolue max will be most restrictive

		if ( isSoftBoundActive_(VOLUME_SUPP_SOFT_MAX_BASED_IBW_ID) )
		{	// $[TI1] -- volume support's soft maximum bound is currently active...
			// initialize to soft bound maximum bound id...
			ibwBasedMax.value = softMaxValue;

			maxConstraintInfo.id = VOLUME_SUPP_SOFT_MAX_BASED_IBW_ID;
		}
		else
		{	// $[TI2] -- volume support's soft bound is NOT active...
			// initialize to absolute maximum bound id...
			ibwBasedMax.value = (IBW_VALUE *
								 SettingConstants::MAX_TIDAL_VOL_IBW_SCALE);

			maxConstraintInfo.id = VOLUME_SUPP_MAX_BASED_IBW_ID;
		}


		//-----------------------------------------------------------------
		// determine which upper limit to apply...
		//-----------------------------------------------------------------

		// Start with the IBW based max.....
		// NOTE:  When soft bound is active, this branch is forced by 
		//        ibwBasedMax.value setting above; constraintId must not be 
		//        changed in this path!
		if ( ibwBasedMax.value < ABSOLUTE_MAX )
		{	// $[TI3]
			// reset this range's maximum constraint to allow for "warping" of a
			// new maximum value...
			getBoundedRange_().resetMaxConstraint();

			// warp the maximum value to a lower "click" boundary...
			getBoundedRange_().warpValue(ibwBasedMax, BoundedRange::WARP_DOWN);

			// the IBW-based maximum is more restrictive, therefore make it the
			// upper bound value...
			maxConstraintInfo.value = ibwBasedMax.value;
			useAbsoluteMax = FALSE;
		}

		pSetting = SettingsMgr::GetSettingPtr(SettingId::HIGH_INSP_TIDAL_VOL);
		highVtiBasedMax = pSetting->getAdjustedValue();   // assigns value and precision


		// Now test for the high inspired volume based limitation
		// (but only after all settings have been initialized)
		AdjustedContext&  rAdjContext = ContextMgr::GetAdjustedContext();
		if ( (highVtiBasedMax.value <= maxConstraintInfo.value) && rAdjContext.getAllSettingValuesKnown() )
		{	// $[TI1]
			// the Volume-based maximum is more restrictive, therefore make it the
			// upper bound value...

			// reset this range's maximum constraint to allow for "warping" of a
			// new maximum value...
			getBoundedRange_().resetMaxConstraint();

			maxConstraintInfo.id = VOL_SUP_MAX_BASED_HIGH_INSP_TIDAL_VOL_SPONT_ID;


			getBoundedRange_().decrement( 1, highVtiBasedMax, dummyStatus);
			maxConstraintInfo.value = highVtiBasedMax.value;
			useAbsoluteMax = FALSE;
		}

		if ( useAbsoluteMax )
		{	// $[TI4]
			// the absolute maximum is more restrictive, therefore make it the
			// upper bound value...
			maxConstraintInfo.id     = VOLUME_SUPP_MAX_ID;
			maxConstraintInfo.value  = ABSOLUTE_MAX;
		}

		// is set to soft bound?...
		maxConstraintInfo.isSoft =
		// $[TI5] (TRUE)  $[TI6] (FALSE)...
			(maxConstraintInfo.id == VOLUME_SUPP_SOFT_MAX_BASED_IBW_ID);

		getBoundedRange_().updateMaxConstraint(maxConstraintInfo);
	}


	//===================================================================
	// calculate volume support's most restrictive MINIMUM bound...
	//===================================================================
	{
		BoundedRange::ConstraintInfo  minConstraintInfo;

		//-----------------------------------------------------------------
		// determine which IBW-based lower limit to apply...
		//-----------------------------------------------------------------

		BoundedValue  ibwBasedMin;

		if ( isSoftBoundActive_(VOLUME_SUPP_SOFT_MIN_BASED_IBW_ID) )
		{	// $[TI7] -- volume support's soft minimum bound is currently active...
			// initialize to soft bound minimum bound id...
			ibwBasedMin.value    = softMinValue;
			minConstraintInfo.id = VOLUME_SUPP_SOFT_MIN_BASED_IBW_ID;
		}
		else
		{	// $[TI8] -- volume support's soft bound is NOT active...
			// initialize to absolute minimum bound id...
			ibwBasedMin.value = (IBW_VALUE *
								 SettingConstants::MIN_TIDAL_VOL_IBW_SCALE);

			minConstraintInfo.id = VOLUME_SUPP_MIN_BASED_IBW_ID;
		}

		//-----------------------------------------------------------------
		// determine which lower limit to apply (absolute or IBW-based?)...
		//-----------------------------------------------------------------

		const Real32  ABSOLUTE_MIN = getAbsoluteMinValue_();

		if ( ibwBasedMin.value > ABSOLUTE_MIN )
		{	// $[TI9]
			// reset this range's minimum constraint to allow for "warping" of a
			// new minimum value...
			getBoundedRange_().resetMinConstraint();

			// warp the minimum value to a upper "click" boundary...
			getBoundedRange_().warpValue(ibwBasedMin, BoundedRange::WARP_UP);

			// the IBW-based minimum is more restrictive, therefore make it the
			// upper bound value...
			minConstraintInfo.value = ibwBasedMin.value;
		}
		else
		{	// $[TI10]
			// the absolute minimum is more restrictive, therefore make it the
			// upper bound value...
			minConstraintInfo.id    = VOLUME_SUPP_MIN_ID;
			minConstraintInfo.value = ABSOLUTE_MIN;
		}

		// is set to soft bound?...
		minConstraintInfo.isSoft =
		// $[TI11] (TRUE)  $[TI12] (FALSE)...
			(minConstraintInfo.id == VOLUME_SUPP_SOFT_MIN_BASED_IBW_ID);

		getBoundedRange_().updateMinConstraint(minConstraintInfo);
	}
}


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)  [const]
//
//@ Interface-Description
//  Return, via 'rSoftMinValue' and 'rSoftMaxValue', the soft bound lower
//  and upper limit values, respectively.
//---------------------------------------------------------------------
//@ Implementation-Description
//  $[VC02001] -- volume support's soft range...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
	VolumeSuppSetting::findSoftMinMaxValues_(Real32& rSoftMinValue,
											 Real32& rSoftMaxValue) const
{
	CALL_TRACE("findSoftMinMaxValues_(rSoftMinValue, rSoftMaxValue)");

	const Setting*  pIbw = SettingsMgr::GetSettingPtr(SettingId::IBW);

	const Real32  IBW_VALUE = BoundedValue(pIbw->getAdjustedValue()).value;

	BoundedValue  boundedValue;

	//-------------------------------------------------------------------
	// determine lower soft bound limit value...
	//-------------------------------------------------------------------
	// calculate lower soft bound limit...
	boundedValue.value =
		(IBW_VALUE * SettingConstants::MIN_TIDAL_VOL_SOFT_IBW_SCALE);

	// warp the calculated value to the upper "click", with no clipping...
	getBoundedRange().warpValue(boundedValue, BoundedRange::WARP_UP, FALSE);

	rSoftMinValue = boundedValue.value;

	//-------------------------------------------------------------------
	// determine upper soft bound limit value...
	//-------------------------------------------------------------------

	// calculate upper soft bound limit...
	boundedValue.value =
		(IBW_VALUE * SettingConstants::MAX_TIDAL_VOL_SOFT_IBW_SCALE);

	// warp the calculated value to the lower "click", with no clipping...
	getBoundedRange().warpValue(boundedValue, BoundedRange::WARP_DOWN, FALSE);

	rSoftMaxValue = boundedValue.value;
}  // $[TI1]


//============== M E T H O D   D E S C R I P T I O N =================
//@ Method:  getAbsoluteMinValue_()  [const, virtual]
//
//@ Interface-Description
//  Return volume support's absolute minimum value.
//---------------------------------------------------------------------
//@ Implementation-Description
//  This is a virtual method that is overridden to support this setting's
//  dynamic minimum constraint.
//
//  $[VC02000] The setting's range ...
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

Real32
	VolumeSuppSetting::getAbsoluteMinValue_(void) const
{
	CALL_TRACE("getAbsoluteMinValue_()");

	Real32  absoluteMinValue;

	const Setting*  pPatientCctType =
		SettingsMgr::GetSettingPtr(SettingId::PATIENT_CCT_TYPE);

	const DiscreteValue  PATIENT_CCT_TYPE_VALUE =
		pPatientCctType->getAdjustedValue();

	Boolean isNeoModeAdvancedEnabled = 
		SoftwareOptions::IsOptionEnabled(SoftwareOptions::NEO_MODE_ADVANCED);

	switch ( PATIENT_CCT_TYPE_VALUE )
	{
		case PatientCctTypeValue::NEONATAL_CIRCUIT :	  // $[TI1]
			if( isNeoModeAdvancedEnabled )
			{
				absoluteMinValue = SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_VC_PLUS;
			}
			else
			{
				absoluteMinValue = SettingConstants::TIDAL_VOL_NEONATAL_MIN_VALUE_MISC;
			}
			break;
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :	  // $[TI2]
			absoluteMinValue = SettingConstants::TIDAL_VOL_STANDARD_MIN_VALUE;
			break;
		default :
			// unexpected patient circuit type value...
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE_VALUE);
			break;
	}

	return(absoluteMinValue);
}
