#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//     Copyright (c) 1995, Nellcor Puritan Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: LaptopEventRegistrar - The central registration, control, and
// dispatch point for Laptop request events received by GUI-Apps.  The
// laptop events are received from the GUI-Serial-Interfaces subsystem via the GUI
// task's User Annunciation queue.
//---------------------------------------------------------------------
//@ Interface-Description
// Objects which handle Laptop events use the RegisterTarget() method to
// register for a callback.  The callback can be either the virtual method
// LaptopEventTarget::LaptopFailureHappened() or LaptopRequestHappened()
// Therefore all objects which need to receive laptop callbacks are derived
// from the LaptopEventTarget class.  All unique laptop events are
// specified with the LaptopEventId class enum.
//
// When laptop events are received by the GUI Application's event loop
// this registrar is informed via LaptopRequestHappened() or LaptopFailureHappened() 
//  which will then look after informing the interested party (if any).
//
// Before registering of targets can occur, though, the Initialize() method
// must be called to do one-time initialization of this class.
//
//---------------------------------------------------------------------
//@ Rationale
// This class is necessary to register, set, and centrally dispatch laptop 
// events which are received from the GUI-Serial-Interfaces subsystem.
//---------------------------------------------------------------------
//@ Implementation-Description
// Only one instance of this class is created.  The class is implemented
// with the CascadeArray_[] as the centerpiece.  This is an array of
// LaptopEventCascades -- one for each unique event id.  The 
// LaptopEventId acts as the index into the CascadeArray_[].
// The RegisterTarget() method is used to initialize the pointer
// slots of the CascadeArray_[] -- this is done on-the-fly as needed.
//
// Laptop events are communicated to the registrar via the
// LaptopRequestHappened() or LaptopFailureHappened() method.  This then calls the
// laptopRequestHappened() or laptopFailureHappened() of the object which is 
// registered as a target of the particular laptop event.
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/LaptopEventRegistrar.ccv   25.0.4.0   19 Nov 2013 14:08:02   pvcs  $
//
//@ Modification-Log
//
//  Revision: 005  By: gdc    Date: 26-May-2007   SCR Number: 6330
//  Project:  Trend
//  Description:
//      Removed SUN prototype code.
//
//  Revision: 004  By: gdc    Date: 22-Jan-2001   DR Number: 5493
//  Project:  GuiComms
//  Description:
//      Implemented SMCP on additional serial ports.
//
//  Revision: 003  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 002  By: gdc      Date: 24-Nov-1997  DR Number: 2605
//    Project:  Sigma (R8027)
//    Description:
//     Changed interface to GUI-Serial-Interface as part of its 
//     restructuring.
//
//  Revision: 001  By:  mpm    Date:  16-OCT-95    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//	     Integration baseline.
//=====================================================================

#include "LaptopEventRegistrar.hh"

//@ Usage-Classes
#include "LaptopMessageHandler.hh"
#include "LaptopEventTarget.hh"
#include "LaptopEventCascade.hh"
#include "UserAnnunciationMsg.hh"
#include "SerialInterface.hh"
#include "SmcpPacket.hh"
//@ End-Usage

//@ Code...

//
// Word-aligned memory pools for static member data.
//
static Uint CascadeArrayMemory_ [
		((sizeof(LaptopEventCascade) + sizeof(Uint) - 1) / sizeof(Uint))
		* (LaptopEventId::NUMBER_OF_EVENT + 1)];

//
// Initializations of static member data.
//
LaptopEventCascade *LaptopEventRegistrar::CascadeArray_ =
								(LaptopEventCascade *)CascadeArrayMemory_;

Uint8 LaptopEventRegistrar::PMsg[ sizeof(SmcpMessage) ];

Int32 LaptopEventRegistrar::ControlPortNum_;

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LaptopFailureHappened
//
//@ Interface-Description
// Called due to a Laptop request event message from the GUI-Serial-Interfaces 
// subsystem.  It is passed the failure code sent to this task.  The
// objects associated with all event ids are then informed of the event by
// calling the object's corresponding laptopFailureHappened() method.
// The serialFailureCodes can be SERIAL_FAILURE_TOUT, or SERIAL_FAILURE_NAK.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is the central dispatch point for all Laptop Failure events received
// by the GUI-App task.  Since the laptop failure code is not event specific,
// all objects in the CascadeArray_[] array are informed of the event.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopEventRegistrar::LaptopFailureHappened(
	SerialInterface::SerialFailureCodes serialFailureCodes, Int32 portNum)
{
	CALL_TRACE("LaptopEventRegistrar::LaptopFailureHappened(SerialInterface::SerialFailureCodes)");

	if ( ControlPortNum_ == portNum )
	{ 											// $[TI1.1]
	  // This is the proposed code for the laptop communication to start

	  Int eventId;
	  Uint8 *pMsgPtr;

	  // When the GuiApp task is ready for the input message, it extracts
	  // the request from the SerialInterface buffer.
	  Int32 length = SerialInterface::Read( 
						  PMsg, sizeof(PMsg), 0, 
						  SerialInterface::PortNum(portNum) );
	  pMsgPtr = &PMsg[0];

	  eventId = (Int) *(pMsgPtr + LaptopEventId::INCOMING_LT_EVENT_IDX);

	  CLASS_ASSERTION(eventId > LaptopEventId::NULL_EVENT_ID  &&
					  eventId < LaptopEventId::NUMBER_OF_EVENT);

	  // Inform targets of event
	  CascadeArray_[eventId].laptopRequestHappened(
				  (LaptopEventId::LTEventId) eventId,	pMsgPtr);
	} 											// $[TI1.2]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: LaptopRequestHappened
//
//@ Interface-Description
// Called due to a Laptop request event message from the GUI-Serial-Interfaces
// subsystem.  It is passed the event message sent to this task.  We decode the
// request id (id of the Laptop data that was requestd) from this message.  The
// object associated with this request id is then informed of the event by
// calling the object's corresponding LaptopRequestHappened() method.
//---------------------------------------------------------------------
//@ Implementation-Description
// This is the central dispatch point for all Laptop request events received
// by the GUI-App task. We execute the callback
// to the target object.
//
// The request id is used as an index into the Targets_[] array to find the
// target object.
//
// The first request from a serial port sets the ControlPortNum_. 
// Subsequent requests must come from this control port. Requests
// from other ports are ignored after the control port number is set.
//---------------------------------------------------------------------
//@ PreCondition
// The target object for the given 'timerId' must exist.  The request id
// must be valid.
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================

void
LaptopEventRegistrar::LaptopRequestHappened(Int32 portNum)
{
    CALL_TRACE("LaptopEventRegistrar::LaptopRequestHappened(void)");

	if (ControlPortNum_ == -1)
	{
	  ControlPortNum_ = portNum;
	}
	
	if (ControlPortNum_ == portNum)
	{ 													// $[TI1.1]
	  // This is the proposed code for the laptop communication to start

	  Int eventId;
	  Uint8 *pMsgPtr;

	  // When the GuiApp task is ready for the input message, it extracts
	  // the request from the SerialInterface buffer.
	  Int32 length = SerialInterface::Read(
						  PMsg, sizeof(PMsg), 0, 
						  SerialInterface::PortNum(portNum) );
	  pMsgPtr = &PMsg[0];

	  eventId = (Int) *(pMsgPtr + LaptopEventId::INCOMING_LT_EVENT_IDX);

	  CLASS_ASSERTION(eventId > LaptopEventId::NULL_EVENT_ID  &&
					  eventId < LaptopEventId::NUMBER_OF_EVENT);

	  // Inform targets of event
	  CascadeArray_[eventId].laptopRequestHappened(
				  (LaptopEventId::LTEventId) eventId, pMsgPtr);
	} 													// $[TI1.2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: RegisterTarget [public, static]
//
//@ Interface-Description
// Objects needing to know when Laptop Request events occur use this method to
// register themselves for request notices.  The parameters are as follows:
// >Von
//	eventId		The enum id of the Laptop request data type. 
//	pTarget		ptr to the object to be notified when a laptop event given by
//				'eventId' occurs.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
// Registers the given target with the associated 'eventId' by storing the
// given pointer into the appropriate slot of the Targets_[] array.
//---------------------------------------------------------------------
//@ PreCondition
// eventId must be valid and pTarget must be non-null.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopEventRegistrar::RegisterTarget(LaptopEventId::LTEventId eventId,
				     LaptopEventTarget* pTarget)
{
	CALL_TRACE("LaptopEventRegistrar::RegisterTarget("
				"LaptopEventId::LTEventId eventId, LaptopEventTarget* pTarget)");

	CLASS_PRE_CONDITION(eventId >= 0 && eventId < LaptopEventId::NUMBER_OF_EVENT);
	CLASS_PRE_CONDITION(pTarget != NULL);

	CascadeArray_[eventId].addTarget(pTarget);		
														// $[TI1]

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: Initialize [public, static]
//
//@ Interface-Description
//	Called to construct all Laptop Event Cascades.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Called "new" to construct all possible LaptopEventCascades which
//	belong to the CascadeArray_ member of the LaptopEventRegistrar class.
//---------------------------------------------------------------------
//@ PreCondition
//	Total size of all possible LaptopEventCascades not to exceed static
//	memory reserved for them in CascadeArrayMemory_.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
LaptopEventRegistrar::Initialize(void)
{
	CALL_TRACE("LaptopEventRegistrar::Initialize(void)");

	CLASS_ASSERTION(sizeof(CascadeArrayMemory_) >=
				(sizeof(LaptopEventCascade) * (LaptopEventId::NUMBER_OF_EVENT + 1)));

	// Run placement news on each CascadeArray_[] element
	for (Uint16 i = 0; i < LaptopEventId::NUMBER_OF_EVENT; i++)
	{
		new (CascadeArray_ + i) LaptopEventCascade;
	}

	ControlPortNum_ = -1;
							     	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GetControlPort
//
//@ Interface-Description
// Returns the port number of the controlling serial port.
//---------------------------------------------------------------------
//@ Implementation-Description
// Returns the value of the static ControlPortNum_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SerialInterface::PortNum
LaptopEventRegistrar::GetControlPort(void)
{
// $[TI1]
  return SerialInterface::PortNum(ControlPortNum_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
LaptopEventRegistrar::SoftFault(const SoftFaultID  softFaultID,
						     const Uint32       lineNumber,
						     const char*        pFileName,
						     const char*        pPredicate)  
{
	CALL_TRACE("LaptopEventRegistrar::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, LAPTOPEVENTREGISTRAR,
							lineNumber, pFileName, pPredicate);
}
