/*****************************************************************************/
/*   Module Name:   		 PTS.h                                             
/*   Purpose:       		 Header for PTS Ventilator Serial Communications Module                               
/*
/*   Modification History:
/*   
/*   Description of Change			        By			      Date 
/*   ---------------------					-----   		  ------
/*   The Error codes in ErrorMessage 		Dwarkanath(CGS)   12 Dec,2000
/*   functions are changed.
/*
/*	 TestExecutive related code is removed 
/* 	 and replaced corresponding 			Hamsavally(CGS)   6 Oct,2000
/*	 functionality.
/*
/*   Symbol definitions for                 Dave Richardson   15 Nov, 2002
/*   AUTO_DETECT and NO_AUTO_DETECT
/*   are conditionally defined if
/*   not already defined
/*                                                                           
/*   Requirements:                       
/*                                                                           
/*   $Revision::   1.2      $                                                
/*                                                                          
/*****************************************************************************/
/*****************************************************************************
THIS IS A PROPRIETARY WORK TO WHICH PURITAN-BENNETT CORPORATION CLAIMS
EXCLUSIVE RIGHT.  NO PART OF THIS WORK MAY BE USED, DISCLOSED, REPRODUCED,
STORED IN AN INFORMATION RETIEVAL SYSTEM, OR TRANSMITTED, IN ANY FORM OR BY ANY
MEANS ELECTRONIC, MECHANICAL, PHOTOCOPYING, RECORDING OR OTHERWISE WITHOUT THE
PRIOR WRITTEN PERMISSION OF PURITAN-BENNETT CORPORATION.
 
Copyright (c) 1999-2002, Puritan-Bennett
All rights reserved.
*******************************************************************************/

#ifndef PTS_H
#define PTS_H

#include "com.h"

// Defines
#ifndef AUTO_DETECT
#define AUTO_DETECT 0
#endif
#ifndef NO_AUTO_DETECT
#define NO_AUTO_DETECT 1
#endif

// public functions
int IsPTSConnected(void);
int EstablishPTS(int autoDetect);
int SendPTS(char*, int);
int SendPTSByte(int byte);
int RecvPTSByte(void);
int RecvPTS(char*, int, int*);
int RecvPTS_CS(char*, int, int*);
int TerminatePTS(void);
int FlushInQPTS(void);
int FlushOutQPTS(void);
int CharsInPTSQ(void);
int ComBreakPTS(int breakTime);
void RegisterForPTSDetectCB(ComDetectStatusFuncPtr func);
void InitPTSComStruct(void);

// private functions
static int TestStrResponseValidFunc(char* str, int len);

// PTS send macro used to automate redundant code
// only for use within a test state, not allowed in test state
// support functions
#define SEND_PTS(func) \
{\
    if (func != 0) {\
        gErrorFlag = TRUE;\
		ErrorMessage(COM_PTS_ERROR);\
        return;\
    }\
}    

#endif //PTS_H
