
#ifndef ExhFlowSensorFlowTest_HH
#define ExhFlowSensorFlowTest_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
//  Class: ExhFlowSensorFlowTest - BD Safety Net Test for the detection
//                                 of exhalation flow sensor reading OOR
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/ExhFlowSensorFlowTest.hhv   25.0.4.0   19 Nov 2013 13:59:46   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 001  By:  by    Date:  05-Sep-1995    DR Number: None
//    Project:  Sigma (840)
//    Description:
//        Initial version.
//
//====================================================================

#include "Sigma.hh"
#include "Breath_Delivery.hh"
#include "BD_IO_Devices.hh"
#include "SafetyNetTest.hh"

//@ Usage-Classes
#include "Background.hh"
//@ End-Usage


class ExhFlowSensorFlowTest : public SafetyNetTest
{
    public:
        ExhFlowSensorFlowTest( void );
        ~ExhFlowSensorFlowTest( void );

        static void SoftFault( const SoftFaultID softFaultID,
			       const Uint32      lineNumber,
			       const char*       pFileName  = NULL,
			       const char*       pPredicate = NULL );

        virtual BkEventName checkCriteria( void );

    protected:

    private:

        // these methods are purposely declared, but not implemented...
        ExhFlowSensorFlowTest( const ExhFlowSensorFlowTest& ); // not implemented...
        void operator=( const ExhFlowSensorFlowTest& );        // not implemented...

        //@ Data-Member: numCyclesOorHigh_
        // The number of consecutive cycles that the check has indicated that
        // the exhalation flow sensor flow signal is out of range on the high
        // end; this test must differentiate between OOR-High and OOR-Low and
        // therefore does not use the numCyclesCriteriaMet_ data attribute of
        // the base class
        Int16 numCyclesOorHigh_;

        //@ Data-Member: numCyclesOorLow_
        // The number of consecutive cycles that the check has indicated that
        // the exhalation flow sensor flow signal is out of range on the low
        // end; this test must differentiate between OOR-High and OOR-Low and
        // therefore does not use the numCyclesCriteriaMet_ data attribute of
        // the base class
        Int16 numCyclesOorLow_;
};


#endif // ExhFlowSensorFlowTest_HH 
