#ifndef TrendDbTestDataAgent_HH
#define TrendDbTestDataAgent_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: Test Data Agent
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbTestDataAgent.hhv   25.0.4.0   19 Nov 2013 14:34:42   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  mjf    Date:  22-March-2007    SCR Number: 6237
//       Project:  Trend
//       Description:
//             Initial version
//=====================================================================

#include "TrendDataMgr.hh"
#include "TrendValue.hh"
#include "TrendDbPatientRecord.hh"
#include "TrendDbSettingRecord.hh"

#include "TimeStamp.hh"             // OS-Foundation
#include "TrendTimeScaleValue.hh"   // Setting-Validation
#include "TrendSelectValue.hh"      // Setting-Validation
#include "BitUtilities.hh"          // Utilities




class TrendDbTestDataAgent
{
public:    

	TrendDbTestDataAgent();
	~TrendDbTestDataAgent();


	static const TrendValue &
		NextSimulatedBoundedData( Int16 scale, const TrendValue & oldValue );

	static const TimeStamp &
		NextSimulatedTimeStamp( Int16 scale, const TimeStamp oldTime );

	static void
		FillSimulatedData( Uint32 numFrames, const TimeStamp & startTime );

	static void
		LoadSimulatedData( Uint32 numFrames = TrendDataMgr::TREND_PATIENT_DB_CAPACITY );


	void processTdgCmd( const char * pCmdString );

	static const char FIELD_SEPARATOR;
	static const Int16 NUMBER_BUFFER_LEN;


private:

	// no copy or assignment constructor
	TrendDbTestDataAgent( const TrendDbTestDataAgent& );	// not implemented
	void operator=( const TrendDbTestDataAgent& );			// not implemented


	//@ Data-Member: SimTimeStepSeconds_
	// Const values used to generate the simulated data, timestamp in seconds
	static const Uint8
		SimTimeStepSeconds_[ TrendSelectValue::TOTAL_TREND_SELECT_VALUES ];

	//@ Data-Member: SimTimeStepMinutes
	// Const values used to generate the simulated data, timestamp in minutes
	static const Uint8
		SimTimeStepMinutes_[ TrendSelectValue::TOTAL_TREND_SELECT_VALUES ];

	//@ Data-Member:
	// Const values used to generate simulated trend values
	static const Real32 
		SimBoundedStep_[ TrendSelectValue::TOTAL_TREND_SELECT_VALUES ];

	//@ Data-Member: SimMaxValue_
	// The const max simulated trend value
	static const Real32 SimMaxValue_;

	//@ Data-Member: SimMinValue_
	// The const min simulated trend value 
	static const Real32 SimMinValue_;

	//@ Data-Member: testPatientDataArray_
	// An array used to hold the simulated patient values
	TrendValue testPatientDataArray_[ TrendDataMgr::TREND_PATIENT_MAX_SLOTS ];

	//@ Data-Member: testSettingDataArray_
	// An array used to hold the simulated setting values
	TrendValue testSettingDataArray_[ TrendDataMgr::TREND_SETTING_MAX_SLOTS ];

	//@ Data-Member: testEvents_
	// Simulated events
	Uint32     testEvents_[ TrendEvents::TOTAL_EVENT_IDS / BITS_PER_LWORD ];

	//@ Data-Member: testSettingDataChange_
	// Flag used to know if the simulated settings values have changed and
	// therefore does the setting table need to be updated
	Boolean    isTestSettingDataChange_;

	//@ Data-Member: testTimeStamp_
	// The simulated timestamp
	TimeStamp  testTimestamp_;

	void processTdgSetCmd_( const char * pCmdArrgs );
	void processTdgSampleCmd_( const char * pCmdArrgs );
	void processTdgTimestampCmd_( const char * pCmdArrgs );
	void processTdgEventCmd_( const char * pCmdArrgs );
	void processTdgDisableCmd_( void );
	void processTdgCorruptCmd_( const char * pCmdArrgs );
	void processTdgResetCmd_( void );
	void processTdgSimCmd_( const char * pCmdArrgs );

	Int16 getFieldStr_( const char * pCmdString, char * pFieldStr, Int16 maxSize );
	Real32 getNumber_( const char * pNumString );

};



#endif // TrendDbTestDataAgent_HH
