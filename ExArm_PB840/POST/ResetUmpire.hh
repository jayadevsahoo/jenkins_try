#ifndef ResetUmpire_HH
#define ResetUmpire_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


//=====================================================================
// Class: ResetUmpire - Power On Self Test - Reset Umpire Test
//---------------------------------------------------------------------
//@ Modification-Log
//
//
//  Revision: 001    By: Gary Cederquist  Date: 04-MAR-1997  DR Number:
//    Project: Sigma (R8027)
//    Description:
//       Initial version
//
//=====================================================================

#include "Sigma.hh"
#include "Post.hh"

class ResetUmpire : public Post
{
  public:
    static Criticality Test(void);

  private:
    ResetUmpire();        // not implemented
    ~ResetUmpire();       // not implemented

    static void     LogStrike_(void);
    static Boolean  IsStrikedOut_(void);
};

#endif	// #ifndef ResetUmpire_HH
