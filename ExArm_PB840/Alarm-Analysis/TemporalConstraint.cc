#include "stdafx.h"
//====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: TemporalConstraint - Type "Detection criteria met for > 20 seconds".
//---------------------------------------------------------------------
//@ Interface-Description
// The VHMTemporal object attached to the child of the OperatingGroup
// associated with the TemporalConstraint starts the timers necessary to track
// the bounds specified in the TemporalConstraint objects.
//
// A method which does the Boolean comparisons is provided by the pure virtual
// base class Constraint.
//---------------------------------------------------------------------
//@ Rationale
// TemporalConstraint is used to model condition augmentations from the
// Technical and Non-Technical Alarm Matrices of the type "Detection criteria
// is met for > 20 seconds and <= 40 seconds".
//---------------------------------------------------------------------
//@ Implementation-Description
// TemporalConstraint defines base class Constraint's pure virtual method,
// evaluate.  This method takes an OperatingGroup as a parameter.  This
// OperatingGroup object is the only child of the OperatingGroup object to
// which the TemporalConstraint is attached.  The child OperatingGroup contains
// the detection criteria for an Alarm or an AlarmAugmentation and has a
// VHMTemporal object (derived from ViolationHistoryManager) attached to it.
// The VHMTemporal object keeps track of the timers that are used to determine
// when the parent OperatingGroup objects with TemporalConstraints are to be
// evaluated.  When the OperatingGroup attached to the VHMTemporal becomes
// active, the VHMTemporal object examines each parent OperatingGroup object's
// TemporalConstraint bounds and sets timers accordingly.  When a
// timer fires indicating one of the parent OperatingGroup objects needs to be
// evaluated, it's evaluate method is called.  The intervalTime_ field from the
// OperatingGroup attached to the VHMTemporal object is accessed and compared
// against the minimum and maximum derived from the TemporalConstraint 
// object.
//---------------------------------------------------------------------
//@ Fault-Handling
//  The standard class assertions are used to ensure correctness.
//---------------------------------------------------------------------
//@ Restrictions
//    none
//---------------------------------------------------------------------
//@ Invariants
//    none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Alarm-Analysis/vcssrc/TemporalConstraint.ccv   25.0.4.0   19 Nov 2013 13:51:24   pvcs  $
//
//@ Modification-Log

//  
//   Revision 001   By:   hct      Date: 01/01/95         DR Number:   n/a
//      Project:   Sigma   (R8027)
//      Description:
//         Initial version (Integration baseline)
//  
//   Revision 002   By:   hct      Date: 01/18/96         DR Number:   659
//      Project:   Sigma   (R8027)
//      Description:
//         Use OsTimeStamp for interval timing instead of TimeStamp.
//  
//   Revision 003   By:   gbs      Date: 05/17/96         DR Number:   996
//      Project:   Sigma   (R8027)
//      Description:
//         Modify [TI] numbers.
//  
//   Revision 004   By:   sah      Date: 01/12/99         DR Number:   5321
//      Project:   ATC
//      Description:
//         Changed 'SoftFault()' method to non-inlined method.
//  
//   Revision 005   By:   hct      Date: 04/22/99         DR Number:   5359
//      Project:   ATC
//      Description:
//         Added functionality to store values of pMinCalcFunction_ and
//         pMaxCalcFunction_ when associated OperatingGroup becomes active.
//  
//   Revision 006   By:   hct      Date: 06/08/99         DR Number:   5395
//      Project:   ATC
//      Description:
//         Corrected changes made for 5359 so that TemporalConstraint/
//         VHMTemporal objects work properly.
//  
//=====================================================================

#include "Sigma.hh"
#include "Alarm_Analysis.hh"
#include "TemporalConstraint.hh"

//@ Usage-Classes
#include "OperatingGroup.hh"
#include "TimeStamp.hh"

//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: TemporalConstraint
//
//@ Interface-Description
//   Constructor.
//
// >Von
//   minBooleanOp      Enumerator representing the Boolean operator used for 
//                     evaluating the Constraint minimum.
//   maxBooleanOp      Enumerator representing the Boolean operator used for 
//                     evaluating the Constraint maximum.
//   pMinCalcFunction  Pointer to a function which returns the minimum value.
//   pMaxCalcFunction  Pointer to a function which returns the maximum value.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
TemporalConstraint::TemporalConstraint(Constraint::BooleanOperator minBooleanOp,
                             Constraint::BooleanOperator maxBooleanOp,
                             Constraint::CalcFunctionPtrType pMinCalcFunction,
                             Constraint::CalcFunctionPtrType pMaxCalcFunction):
                             Constraint(minBooleanOp,
                                        maxBooleanOp,
                                        pMinCalcFunction,
                                        pMaxCalcFunction)
{
  CALL_TRACE("TemporalConstraint(minBooleanOp, maxBooleanOp, pMinCalcFunction, pMaxCalcFunction");

  if (pMinCalcFunction != NULL) // $[TI1]
  {
    minValue_ = (*pMinCalcFunction)();
  }
  else // $[TI2]
  {
    minValue_ = 0;
  }

  if (pMaxCalcFunction != NULL) // $[TI3]
  {
    maxValue_ = (*pMaxCalcFunction)();
  }
  else // $[TI4]
  {
    maxValue_ = 0;
  }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~TemporalConstraint
//
//@ Interface-Description
//    Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

TemporalConstraint::~TemporalConstraint(void)
{
  CALL_TRACE("~TemporalConstraint(void)");
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluate
//
//@ Interface-Description
//   This method is called when an OperatingGroup has an object derived from
//   Constraint associated with it.  When the OperatingGroup is asked to
//   evaluate itself, it messages its children to evaluate themselves.  After
//   control returns to the OperatingGroup, it messages this method, passing a
//   reference to its child OperatingGroup which will have an object derived 
//   from ViolationHistoryManager attached to it.  The child OperatingGroup is 
//   queried as to the time it became active and the length of time it has been
//   active is calculated.  This length of time is compared against the 
//   TemporalConstraint minimum and/or the TemporalConstraint maximum via the 
//   respective Boolean operators.  If the length of time falls within the 
//   ranges specified by the TemporalConstraint minimum and maximum as compared 
//   using the respective Boolean operators, this method returns TRUE otherwise
//   it will return FALSE.
//
// >Von
//   rChildGroup  Reference to the OperatingGroup which refers to the 
//                VHMTemporal object associated with this TemporalConstraint.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
TemporalConstraint::evaluate(OperatingGroup& rChildGroup) const
{
  CALL_TRACE("evaluate(rChildGroup)");

  Boolean state = TRUE;

  if (rChildGroup.getIsActive()) // $[TI1]
  {
    // If there is a minimum, compare the minimum with the length of time
    // the child OperatingGroup has been active using the Boolean operator
    // specified in minBooleanOp_.
    if (pMinCalcFunction_ != NULL) // $[TI1.1]
    {
      state = evaluateBoolean_(
                         rChildGroup.getIntervalTime().getPassedTime(), 
                         minValue_, 
                         minBooleanOp_);
    } // else $[TI1.2]

    // If there is no minimum OR if the length of time was within the minimum
    // constraint... AND
    // If there is a maximum, compare the maximum with the length of time
    // the child OperatingGroup has been active using the Boolean operator
    // specified in maxBooleanOp_
    if (state && pMaxCalcFunction_ != NULL) // $[TI1.3]
    {
      state = evaluateBoolean_(
                         rChildGroup.getIntervalTime().getPassedTime(), 
                         maxValue_, 
                         maxBooleanOp_);
    } // else $[TI1.4]
  }
  // If the child OperatingGroup object's state is inactive, return FALSE.
  else // $[TI2]
  {
    state = FALSE;
  }
  
  return(state);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: evaluateAutoReset()
//
//@ Interface-Description
//   This method may be called when an OperatingGroup has a TemporalConstraint 
//   associated with it.  When the OperatingGroup is asked to evaluate itself, 
//   it messages its children to evaluate themselves.  After control returns to
//   the OperatingGroup, it messages method TemporalConstraint::evaluate, 
//   passing a reference to its only child OperatingGroup.  If 
//   TemporalConstraint::evaluate returns FALSE, and the OperatingGroup is 
//   currently active and there is an autoreset constraint associated with the
//   OperatingGroup (pAutoResetConstraint_ does not equal NULL), the 
//   OperatingGroup will message this method, passing a reference to its only 
//   child OperatingGroup.  The child OperatingGroup will have an object derived
//   from ViolationHistoryManager attached to it.  The child OperatingGroup is 
//   queried as to the time it became inactive and the length of time it has 
//   been inactive is calculated.  This length of time is compared against the 
//   reset TemporalConstraint minimum and/or the reset TemporalConstraint 
//   maximum via the respective Boolean operators.  If the length of time falls 
//   within the ranges specified by the reset TemporalConstraint minimum and 
//   maximum, this method will return TRUE otherwise it will return FALSE.
//   As an example, delivered O2% must be high for 30 seconds in order to
//   declare the HIGH DELIVERED O2 alarm; however, the return of delivered O2%
//   to normal is not sufficient to reset the alarm.  The delivered O2% must be
//   normal for 30 seconds before the alarm auto-resets.
//
// >Von
//   rChildGroup  Reference to the OperatingGroup which refers to the 
//                VHMTemporal object associated with this TemporalConstraint.
// >Voff
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
Boolean
TemporalConstraint::evaluateAutoReset(OperatingGroup& rChildGroup) const
{
  CALL_TRACE("evaluateAutoReset(rChildGroup)");

  CLASS_ASSERTION(pMinCalcFunction_ != NULL);

  // compare the minimum with the length of time the child 
  // OperatingGroup has been inactive using the Boolean operator 
  // specified in minBooleanOp_.

  return( !rChildGroup.getIsActive() && evaluateBoolean_(
                         rChildGroup.getIntervalTime().getPassedTime(), 
                         minValue_, 
                         minBooleanOp_)
        );

  // $[TI1] $[TI2]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getMinNum
//
//@ Interface-Description
//   Return the value of the Constraint minimum returned by executing the
//   method pointed to by attribute pMinCalcFunction_.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//@ End-Method
//=====================================================================
Int32
TemporalConstraint::getMinNum(void) const
{
  CALL_TRACE("getMinNum(void)");
  const Int32 &rMinVal = minValue_;
  const Int32 &rMaxVal = maxValue_;

  if (pMinCalcFunction_ != NULL) // $[TI1]
  {
    (Int32&)rMinVal = (*pMinCalcFunction_)();
  } // $[TI2]

  if (pMaxCalcFunction_ != NULL) // $[TI3]
  {
    (Int32&)rMaxVal = (*pMaxCalcFunction_)();
  } // $[TI4]

  return(minValue_);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault
//
//@ Interface-Description
//    This method is called when a software fault is detected by the
//    fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//    'lineNumber' are essential pieces of information.  The 'pFileName'
//    and 'pPredicate' strings may be defaulted in the macro to reduce
//    code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This method receives the call for the SoftFault, adds it sub-system
//    and class name ID and sends the message to the non-member function
//    SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition    
//    none
//@ End-Method
//=====================================================================

void
TemporalConstraint::SoftFault(const SoftFaultID  softFaultID,
							  const Uint32       lineNumber,
							  const char*        pFileName,
							  const char*        pPredicate)  
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");

  FaultHandler::SoftFault(softFaultID, ALARM_ANALYSIS, 
                          Alarm_Analysis::TEMPORALCONSTRAINT, lineNumber,
                          pFileName, pPredicate);
}
