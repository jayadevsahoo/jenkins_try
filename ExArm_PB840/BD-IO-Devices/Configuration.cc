#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: Configuration - storage for ventilator specific information.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class stores the information that is in flash memory.  It provides
//		methods to obtain the serial number, exhalation valve signature
//		and CRC value, the flow sensor serial numbers and CRC values that
//		are stored in flash.
//---------------------------------------------------------------------
//@ Rationale
//---------------------------------------------------------------------
//@ Implementation-Description
//		Collection of functions that updates and returns the configuration
//		data from flash memory.
//---------------------------------------------------------------------
//@ Fault-Handling
//		n/a
//---------------------------------------------------------------------
//@ Restrictions
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version
// @(#) $Header:   /840/Baseline/BD-IO-Devices/vcssrc/Configuration.ccv   25.0.4.0   19 Nov 2013 13:54:00   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002 By: sah    Date: 13-Jan-1999   DR Number: 5321
//  Project:  ATC
//	Description:
//		Changed 'SoftFault()' method to non-inlined method.
//
//  Revision: 001  By:  syw    Date: 08-Nov-1996    DR Number: INITIAL RELEASE
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "Configuration.hh"

//@ Usage-Classes
//@ End-Usage

#ifdef SIGMA_DEBUG
#include <stdio.h>
#endif // SIGMA_DEBUG


//@ Code...


//=====================================================================
//
//  Public Methods...
//
//=====================================================================

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: Configuration
//
//@ Interface - Description
//  	Constructor.  This method has a pointer to flash memory as an argument
//		and has no return value.
//-----------------------------------------------------------------------
//@ Implementation - Description
//		Initialize the data members with the information in flash.
//-----------------------------------------------------------------------
//@ PreCondition
//		none
//-----------------------------------------------------------------------
//@ PostCondition
//		none
//@ End - Method
//=======================================================================
Configuration::Configuration( BDIOFlashMap *pFlash)
	: pFlash_( pFlash),											// $[TI1]
	  flashSerialNumber_( pFlash->serialNumber),
	  exhValveSignature_( pFlash->exhValveCalInfoSignature),
	  exhValveCrc_( pFlash->exhValveCalInfoCrc),
	  airFlowSensorSN_( pFlash->airCalInfo.getSerialNumber()),
	  airFlowSensorCrc_( pFlash->airCalInfoCrc),
	  o2FlowSensorSN_( pFlash->o2CalInfo.getSerialNumber()),
	  o2FlowSensorCrc_( pFlash->o2CalInfoCrc),
	  exhFlowSensorSN_( pFlash->exhCalInfo.getSerialNumber()),
      exhFlowSensorCrc_( pFlash->exhCalInfoCrc)
{
	CALL_TRACE("Configuration::Configuration( char *pFlash)") ;
}

// ============================= M E T H O D  D E S C R I P T I O N =====
//@ Method: ~Configuration
//
//@ Interface - Description
//-----------------------------------------------------------------------
//@ Implementation - Description
//-----------------------------------------------------------------------
//@ PreCondition
//  none
//-----------------------------------------------------------------------
//@ PostCondition
//  none
//@ End - Method
//=======================================================================
Configuration::~Configuration( void)
{
	CALL_TRACE("Configuration::~Configuration( void)") ;
}

#ifdef SIGMA_DEBUG

void
Configuration::dumpDataMembers( void)
{
	cout << "flashSerialNumber_ = " << flashSerialNumber_ << endl ;
	printf( "exhValveSignature_ = %X\n", exhValveSignature_) ;
	printf( "exhValveCrc_ = %X\n", exhValveCrc_) ;
	printf( "airFlowSensorSN_ = %X\n", airFlowSensorSN_) ;
	printf( "airFlowSensorCrc_ = %X\n", airFlowSensorCrc_) ;
	printf( "o2FlowSensorSN_ = %X\n", o2FlowSensorSN_) ;
	printf( "o2FlowSensorCrc_ = %X\n", o2FlowSensorCrc_) ;
	printf( "exhFlowSensorSN_ = %X\n", exhFlowSensorSN_) ;
	printf( "exhFlowSensorCrc_ = %X\n", exhFlowSensorCrc_) ;
	printf( "exhO2FlowSensorSN_ = %X\n", exhO2FlowSensorSN_) ;
	printf( "exhO2FlowSensorCrc_ = %X\n", exhO2FlowSensorCrc_) ;
}

#endif // SIGMA_DEBUG


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
Configuration::SoftFault( const SoftFaultID  softFaultID,
                   		const Uint32       lineNumber,
		   				const char*        pFileName,
		   				const char*        pPredicate)
{
	CALL_TRACE("Configuration::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  	FaultHandler::SoftFault( softFaultID, BD_IO_DEVICES, CONFIGURATION,
  							 lineNumber, pFileName, pPredicate) ;
}

#endif // defined(SIGMA_DEVELOPMENT)

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//=====================================================================
//
//  Private Methods...
//
//=====================================================================


