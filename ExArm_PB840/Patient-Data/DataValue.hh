
#ifndef DataValue_HH
#define DataValue_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class:  DataValue -  union of breath data item values.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header::   /840/Baseline/Patient-Data/vcssrc/DataValue.hhv   10.7   08/17/07 10:27:32   pvcs  
//
//@ Modification-Log
//  
//  Revision: 001  By:  jhv    Date:  26-Jan-94    DR Number:
//	Project:  Sigma (R8027)
//	Description:
//		Initial version (Integration baseline).
//
//====================================================================

#include "DiscreteValue.hh"

union DataValue
{
	Real32	realValue;
	Int32   discreteValue;
};

#endif  // DataValue_HH
