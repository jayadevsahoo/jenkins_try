
//*****************************************************************************
//
//  ExhCommunicationInterface - Header file for the ExhCommunicationInterface class.
//
//*****************************************************************************

#ifndef EXH_COMM_INTERFACE_H

#define EXH_COMM_INTERFACE_H

#include "BdSerialInterface.hh"
#include "BdSerialPort.hh"


class BigSerialNumber;
class SerialPort;


enum ExhCommState_t
{
	STATE_FEX_STATUS,
	STATE_FEX_MSB,
	STATE_FEX_LSB,
	STATE_AUTOZERO_STATUS,
	STATE_FEX_STATUS_BYTE1,
	STATE_FEX_SERIAL_NO,

};

enum ExhAutozeroState_t
{
	STATE_AUTOZERO_INIT,
	STATE_AUTOZERO_CLOSE_VALVE,
	STATE_AUTOZERO_WAIT,
	STATE_AUTOZERO_REQUEST,
	STATE_AUTOZERO_DELAY,
	STATE_AUTOZERO_EXTRA_CMD,
	STATE_AUTOZERO_OFFSET,
	STATE_AUTOZERO_PENDING,
	STATE_AUTOZERO_COMPLETED
};

enum ExhSensorBoard_t
{
	EXH_SENSOR_BOARD_OLD,
	EXH_SENSOR_BOARD_NEW,
};

class ExhCommunicationInterface : public BdSerialInterface
{

    public:
        
		//E600 TO DO
		virtual void newCycle(void);
		
        bool RequestAutozero();
        ExhAutozeroState_t GetAutozeroState()
		{
			return AutozeroRequestState;
		}

		bool readSerialNumber(UINT& serialNumber);

		static ExhCommunicationInterface* getInstance();

		ExhSensorBoard_t GetSensorBoardType();

    protected:

	    ExhCommunicationInterface( BdSerialPort & rSerialPort,
				BdSerialInterface::PortNum portNum );
		~ExhCommunicationInterface();

		virtual void processRequests_(void);
		Boolean SetSensorBoardBaudRate(Uint32 baudRate, Uint8 command);
		void detectSensorBoard(void);

		void retrieveSerialNumber(void);	

    private:

		static ExhCommunicationInterface* instance_;
		ExhSensorBoard_t		   exhSensorBoard;
        ExhCommState_t          ExhCommunicationState;
        ExhAutozeroState_t      AutozeroRequestState;
        Uint16				   FexRawData;
        Uint8				   FexStatus;
        bool                    bFexDataValid;
        bool                    bAutozeroRequested;
		Uint32                  serialNumber;
		bool                    bSerialNumberReadError;
		Uint8				   BytesToReceive;

	enum ExhSensorCommand_t
	{
		DATA_CMD_00 = 0x00,
		DATA_CMD_01	= 0x01,
		DATA_CMD_03	= 0x03,
		DATA_CMD_04	= 0x04,
		DATA_CMD_40 = 0x40,
		DATA_CMD_80 = 0x80
	};

};

#endif

