#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: GuiTimerTarget - A target to handle GUI Timer events.
// Classes can specify this class as an additional parent class register to be
// informed of timer events.
//---------------------------------------------------------------------
//@ Interface-Description
// Any classes which need to be informed of timer events (either one-shot or
// periodic events) need to multiply inherit from this class.  Then, when they
// register for timer events with the GuiTimerRegistrar class, they will be
// informed of events via the timerEventHappened() method.
//---------------------------------------------------------------------
//@ Rationale
// Used to implement the callback mechanism needed to communicate
// timer events to objects.
//---------------------------------------------------------------------
//@ Implementation-Description
// If classes need to be informed of timer events they must do 3
// things: a) multiply inherit from this class, b) define the
// timerEventHappened() method of this class and c) register for
// change notices via the GuiTimerRegistrar class.  They will then
// be informed of timer events via the timerEventHappened() method.
//---------------------------------------------------------------------
//@ Fault-Handling
//---------------------------------------------------------------------
//@ Restrictions
// This class should not be instantiated directly.  This class has use only
// when inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/GuiTimerTarget.ccv   25.0.4.0   19 Nov 2013 14:07:54   pvcs  $
//
//@ Modification-Log
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "GuiTimerTarget.hh"

//@ Usage-Classes
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timerEventHappened	[virtual]
//
//@ Interface-Description
// This method should be redefined in derived classes.  It is via this method
// that timer events are communicated.  It is passed one parameter, 'timerId',
// which identifies which timer fired.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
GuiTimerTarget::timerEventHappened(GuiTimerId::GuiTimerIdType timerId)
{
        CALL_TRACE("GuiTimerTarget::timerEventHappened(GuiTimerId::GuiTimerIdType"
                                                                                                                        "timerId)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: GuiTimerTarget()  [Default Constructor, Protected]
//
//@ Interface-Description
// The constructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiTimerTarget::GuiTimerTarget(void)
{
        CALL_TRACE("GuiTimerTarget::GuiTimerTarget(void)");

        // Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~GuiTimerTarget  [Destructor, Protected]
//
//@ Interface-Description
// Nothing to destruct!
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

GuiTimerTarget::~GuiTimerTarget(void)
{
        CALL_TRACE("GuiTimerTarget::~GuiTimerTarget(void)");

        // Do nothing
}


#if defined(SIGMA_DEVELOPMENT)

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
GuiTimerTarget::SoftFault(const SoftFaultID  softFaultID,
		   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)  
{
	CALL_TRACE("GuiTimerTarget::SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, GUITIMERTARGET,
							lineNumber, pFileName, pPredicate);
}

#endif // defined(SIGMA_DEVELOPMENT)
