// FixHTML.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "FixHTML.h"
#include "FixHTMLDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFixHTMLApp

BEGIN_MESSAGE_MAP(CFixHTMLApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CFixHTMLApp construction

CFixHTMLApp::CFixHTMLApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CFixHTMLApp object

CFixHTMLApp theApp;


// CFixHTMLApp initialization

BOOL CFixHTMLApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CFixHTMLDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


bool CFixHTMLApp::LoadConfig(CString strHTMLPathName)
{


	

	//CString str;
	//GetDlgItem(IDC_EDIT_ROOT_PATH)->GetWindowText(str);



	/*int nIndex = strHTMLPathName.ReverseFind('\\');
	CString strLanguage;
	strLanguage = strHTMLPathName.Right(strHTMLPathName.GetLength()-nIndex-1);

	CString strFileName;
	strFileName.Format(_T("%s\\_%s.enum"),strHTMLPathName, strLanguage); */



	int nIndex = strHTMLPathName.ReverseFind('\\');
	CString strPath = strHTMLPathName.Left(nIndex);

	nIndex = strPath.ReverseFind('\\');
	
	CString strLanName = strPath.Right(strPath.GetLength() - nIndex-1);

//	CString strPath2 = strPath.Left(nIndex-1);


	//strLanguage = strHTMLPathName.Right(strHTMLPathName.GetLength()-nIndex-1);

	CString strFileName;
	strFileName.Format(_T("%s\\_%s.enum"),strPath, strLanName);

/*////////////////////////////////////////////////////////////////
// Open the file with the specified encoding
FILE *fStream;
errno_t e = _tfopen_s(&fStream, _T("\test.txt"), _T("wt,ccs=UNICODE"));
if (e != 0) return; // failed..
CStdioFile f(fStream);  // open the file from this stream
f.WriteString(_T("Test"));
f.Close();
//
// For Reading
//
// Open the file with the specified encoding
FILE *fStream;
errno_t e = _tfopen_s(&fStream, _T("\test.txt"), _T("wt,ccs=UNICODE"));
if (e != 0) return; // failed..CString sRead;
CStdioFile f(fStream);  // open the file from this stream
CString sRead;
f.ReadString(sRead);
f.Close();
///////////////////////////////////////////////////////////////*/




//	strFileName = _T("\\\\racarl-rd01\\Viking_Software\\work\\kris.warren\\English\\_English.enum");
	

	structData data;

	try{
/*
FILE *fStream;
errno_t e = _tfopen_s(&fStream, strFileName, _T("wt,ccs=UNICODE"));
if (e != 0) return false; // failed..CString sRead;
CStdioFile file(fStream);  // open the file from this stream
*/

	
		CStdioFile file; 
		if(!file.Open( strFileName , CFile::modeRead | CFile::typeBinary ))
		{
			Log(_T("Unable to open enums file : ")+strFileName);
			return false;
		}

		Log(_T("Loaded enums file : ")+strFileName);
			

		CString strRead;
		BOOL b  = file.ReadString(strRead);//first string is invalid

	

		while(file.ReadString(strRead))
		{
			int nIndex = strRead.Find(_T("--||--")); 
			if(nIndex == -1)
			{
				strRead.Trim();
				if(!strRead.IsEmpty())
				{
					Log(_T("Following string didn't match guidance :"));
					Log(strRead);
				}
			}
			else
			{
				data.m_strToFind = strRead.Left(nIndex);
				data.m_strReplaceWith= strRead.Right(strRead.GetLength()-nIndex-6)  ;
				
				/*
				if(data.m_strToFind.Right(1) == _T(" "))
				{
					data.m_strToFind = data.m_strToFind.Left(data.m_strToFind.GetLength()-1);
				}*/

				m_listReplaceString.AddTail(data);
			}

		}
	}



	catch( CFileException* e )
	{
		TCHAR   szCause[300];
		e->GetErrorMessage(szCause, 255);
		CString str;
		str.Format(_T("Exception for %s : cause=%s"), strFileName, szCause);

	    Log(str);
	    
		e->Delete();

		return false;
	}



	return true;


	
}


void CFixHTMLApp::Log(CString str)
{
	CString* p = new CString(str);
	::PostMessage(m_pMainWnd->m_hWnd,WM_LOG, 0, reinterpret_cast<LPARAM>(p));
}