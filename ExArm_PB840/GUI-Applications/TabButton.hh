#ifndef TabButton_HH
#define TabButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TabButton - A SubScreenButton which displays a "tab" around
// it when depressed.  Used for the buttons in the Upper and Lower
// Screen Select areas.
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TabButton.hhv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 003  By:  gdc    Date:  10-Dec-2008    SCR Number: 6377
//  Project:  840S
//  Description:
//	Removed DELTA project single screen support.
//
//  Revision: 002  By: sah    Date: 07-Jan-1999   DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to be non-inlined.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "SubScreenButton.hh"

//@ Usage-Classes
#include "Box.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class TabButton : public SubScreenButton
{
public:
	TabButton(Uint16 xOrg, Uint16 yOrg, Uint16 width, Uint16 height,
			ButtonType buttonType, Uint8 bevelSize, CornerType cornerType,
			StringId title, SubScreen *pSubScreen);
	~TabButton(void);

	virtual void updateDisplay(void);

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

protected:
	virtual void downHappened_(Boolean byOperationAction);
	virtual void upHappened_(Boolean byOperatorAction);

	void setTopLineBoxColor_(Colors::ColorS boxColor);

private:
	// these methods are purposely declared, but not implemented...
	TabButton(const TabButton&);		// not implemented...
	void   operator=(const TabButton&);	// not implemented...

	//@ Data-Member: topLineBox_
	// Box used for displaying grey area at the top of the button
	// to make it look as if the button is attached to the subscreen.
	// This drawable is sort of like a line (it's long and thin) but
	// it's implemented as a box because of the necessity to easily
	// control its exact dimensions (the dimensions of a line are
	// difficult to control when the line is thicker than one pixel).
	Box topLineBox_;
};


#endif // TabButton_HH 
