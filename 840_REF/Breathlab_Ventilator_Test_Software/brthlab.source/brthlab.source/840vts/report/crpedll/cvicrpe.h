// cvicrpe.h : main header file for the CVICRPE DLL
//

#if !defined(AFX_CVICRPE_H__4DF9A288_4C86_11D2_A9E2_004005A2E411__INCLUDED_)
#define AFX_CVICRPE_H__4DF9A288_4C86_11D2_A9E2_004005A2E411__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


// Include The CRPE header from Seagate //
#include "crpe.h"

/////////////////////////////////////////////////////////////////////////////
// CCvicrpeApp
// See cvicrpe.cpp for the implementation of this class
//

class CCvicrpeApp : public CWinApp
{
public:
	CCvicrpeApp();
	~CCvicrpeApp();
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCvicrpeApp)
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCvicrpeApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CVICRPE_H__4DF9A288_4C86_11D2_A9E2_004005A2E411__INCLUDED_)
