#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PressureInspTrigger - Triggers if conditions for pressure 
//	triggering an inspiration are met.
//---------------------------------------------------------------------
//@ Interface-Description
//    This class triggers inspiration based on the measured circuit pressure 
//    and the operator set value of pressure sensitivity, as well as other 
//    qualifying factors.  This trigger is used as  the method of 
//    detecting patient inspiratory effort when pressure triggering has 
//    been selected.  It is enabled or disabled, depending upon the 
//    applicability of the trigger.  When the pressure condition calculated 
//    by this trigger is true, the trigger is considered to have "fired".  
//    This trigger is kept on a list contained in the BreathTriggerMediator.
//---------------------------------------------------------------------
//@ Rationale
//    This class implements the algorithm for detecting patient
//    inspiratory effort for pressure triggering.  Since the pressure
//    sensitivity for the backup pressure trigger and the user set
//    pressure trigger differ algorithmically, the virtual method
//    getPressureSens() provides for retrieving different pressure
//    sensitivities without restricting this class to its instantiated
//    objects.  The base class getPressureSens() method
//    returns a fixed pressure sensitivity established at construction.
//---------------------------------------------------------------------
//@ Implementation-Description
//    This class is derived from PatientTrigger. The pressure triggering
//    conditions are implemented in the method triggerCondition.
//---------------------------------------------------------------------
//@ Fault-Handling
//    n/a.
//---------------------------------------------------------------------
//@ Restrictions
//    None. Restrictions removed by Respiratory Mechanics modification.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PressureInspTrigger.ccv   25.0.4.0   19 Nov 2013 14:00:04   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 019   By: rhj     Date: 02-Apr-2009     SCR Number: 6493
//  Project:  840S2
//  Description:
//       Added an extra condition to the triggerCondition_() to handle
//       a transient drop in pressure during a backup pressure trigger 
//       check. 
// 
//  Revision: 018   By: rhj     Date: 07-Nov-2008     SCR Number: 6435
//  Project:  840S
//  Description:
//       Leak Compensation project-related changes.
// 
//  Revision: 017   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Added virtual method getPressureSens() to allow for derived
//		methods of setting the pressure sensitivity. Removed setExhTraj()
//		as dead code.
//
//  Revision: 016  By: cep     Date:  17-Apr-2002    DR Number: 5899
//  Project:  VCP
//  Description:
//		Eliminated underscore after referencePressure_ in modification log
//      revision 014.
//
//  Revision: 015  By: syw     Date:  31-Oct-2000    DR Number: 5793
//  Project:  Metabolics
//  Description:
//		Eliminate unused functionality because of new puffless controller
//		implementation.
//
//  Revision: 014  By: syw     Date:  14-Jun-1999    DR Number: 5573
//  Project:  ATC
//  Description:
//		Use referencePressure in triggering condition instead of peep.
//
//  Revision: 013  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//		ATC initial revision.
//      Changed 'SoftFault()' method to a non-inlined method.
//		Use valve command in triggering condition instead of peep.
//
//  Revision: 012  By:  iv    Date: 18-Sep-1997     DR Number:DCS 1649
//       Project:  Sigma (R8027)
//       Description:
//          Added a redundent mechanism to trigger based on Pi.
//
//  Revision: 011  By: iv    Date:  18-Jul-1997    DR Number: 2302
//  	Project:  Sigma (840)
//		Description:
//			Removed Pi slope condition.
//
//  Revision: 010  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 009  By:  sp    Date: 15-Apr-1997    DR Number: DCS 1074
//       Project:  Sigma (R8027)
//       Description:
//             For traceability.
//
//  Revision: 008  By:  sp    Date: 1-Apr-1997    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 007  By:  syw    Date: 13-Mar-1997    DR Number: DCS 1780
//       Project:  Sigma (R8027)
//       Description:
//			Changed rExhDryFlowSensor.getValue() to rExhFlowSensor.getDryValue().
//
//  Revision: 006  By:  sp    Date:  31-Oct-1996    DR Number: DCS NONE
//       Project:  Sigma (R8027)
//       Description:
//             Inspection rework.
//
//  Revision: 005  By:  sp    Date:  30-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Flow offsett is bounded by MAX_FLOW_OFFSET and MIN_FLOW_OFFSET.
//
//  Revision: 004  By:  sp    Date:  25-Sept-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Removed unit test methods and add unit test items.
//
//  Revision: 003  By:  sp    Date:  26-Jun-1996    DR Number:600
//       Project:  Sigma (R8027)
//       Description:
//             Implement new patient trigger's algorithm.
//
//  Revision: 002  By:  sp    Date:  4-Mar-1996    DR Number:674
//       Project:  Sigma (R8027)
//       Description:
//             Remove #ifndef.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================
#include "PressureInspTrigger.hh"

//@ Usage-Classes
#include "PhasedInContextHandle.hh"
#include "Peep.hh"
#  include "BreathMiscRefs.hh"
#  include "PressureSensor.hh"
#  include "ExhFlowSensor.h"
#  include "MainSensorRefs.hh"
#include "ExhalationValve.hh"
#include "ValveRefs.hh"
#include "LeakCompEnabledValue.hh"
#include "ModeValue.hh"
#include "BreathRecord.hh"
#include "LeakCompMgr.hh"
#include "Breath_Delivery.hh"
#include "ControllersRefs.hh"
#include "PeepController.hh"

//@ End-Usage

//@ Code...

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PressureInspTrigger
//
//@ Interface-Description
//      This constructor takes triggerId as an argument.
//---------------------------------------------------------------------
//@ Implementation-Description
//	Initialize triggerId_ using PatientTrigger's constructor.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureInspTrigger::PressureInspTrigger(const Trigger::TriggerId triggerId,
										 const Real32 pressureSens) 
: 
PatientTrigger(triggerId),
pressureSens_(pressureSens)
{
	CALL_TRACE("PressureInspTrigger(triggerId)");
	// $[TI1]
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PressureInspTrigger()  
//
//@ Interface-Description
//	Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

PressureInspTrigger::~PressureInspTrigger(void)
{
  CALL_TRACE("~PressureInspTrigger()");

}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enable()
//
//@ Interface-Description
//      This method enables the trigger. This method also initializes data
//      members that belong to patient trigger.
//---------------------------------------------------------------------
//@ Implementation-Description
//      The state data member isEnabled_ is set to true, activating this trigger.
//      The data members count_ and isTriggerActive_ are initialized.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
void
PressureInspTrigger::enable(void)
{
  CALL_TRACE("enable(void)");
 
   // $[TI1]
  isEnabled_ = TRUE;
  isTriggerActive_ = FALSE;
  isZeroExhFlow_ = FALSE;
  flowChangeCount_ = 0.0F;
  prevDryExhFlow_ = 0.0;
  exhTraj_ = 0.0;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: getPressureSens [virtual]
//
//@ Interface-Description
//  This method takes no parameters.  Returns the pressure 
//  sensitivity for this pressure trigger. 
//---------------------------------------------------------------------
//@ Implementation-Description
//  This base class implementation returns the value of the private 
//  data pressureSens_.
//---------------------------------------------------------------------
//@ PreCondition
//  none
//---------------------------------------------------------------------
//@ PostCondition
//  none
//@ End-Method
//=====================================================================
Real32
PressureInspTrigger::getPressureSens(void) const
{
  CALL_TRACE("getPressureSens()");

  return pressureSens_;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PressureInspTrigger::SoftFault(const SoftFaultID  softFaultID,
                   const Uint32       lineNumber,
		   const char*        pFileName,
		   const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, PRESSUREINSPTRIGGER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: triggerCondition_
//
//@ Interface-Description
//    This method takes no parameters.  If the trigger condition has occured, 
//    the method returns true, otherwise, the method returns false.
//---------------------------------------------------------------------
//@ Implementation-Description
//     This method checks for all of the following conditions:
//    1.    isTriggerActive_ is TRUE
// 
//    2.    The patient pressure measured by the expiratory pressure sensor 
//	    drops below the baseline (PEEP) by an amount equal to or greater 
//	    than the Pressure Sensitivity.  Pressure sensitivity for pressure 
//	    inspiration trigger is derived from setting.  Pressure sensitivity 
//	    for pressure backup inspiration trigger is fixed at 
//	    BACKUP_PRESSURE_SENS_VALUE cmH2O.
//          If Leak compensation is enabled, pressure sensitivity is 
//          the maximum value between PEEP and the pressure sensitivity
//          setting based on the following conditions:  Backup trigger 
//          is active, true exhalation time is less than 300ms, and 
//          the current mode setting is not bilevel.
// 
//    3.    The estimated exhaled flow is less than flow offset
//                    OR
//           The change in the exhalation flow measurement is less than 
//	     EXH_FLOW_SLOPE_TRIG_VALUE, for two consecutive control intervals.
//
//    4.    The sign of the expiratory pressure slope signal is NEGATIVE.
// $[04004]
//---------------------------------------------------------------------
//@ PreCondition
//      (triggerId_ == Trigger::PRESSURE_BACKUP_INSP ||
//	 triggerId_ == PRESSURE_INSP)
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================
static const Real32 EXH_FLOW_SLOPE_TRIG_VALUE = 0.01;
static const Int32 FLOW_CHANGE_COUNT_LIMIT = 2;
static const Real32 MAX_FLOW_OFFSET = 37.5F;
static const Real32 MIN_FLOW_OFFSET = .5F;
Boolean
PressureInspTrigger::triggerCondition_(void)
{
  CALL_TRACE("triggerCondition_()");

  Boolean rtnValue = FALSE;

  Real32 exhDryFlow = RExhFlowSensor.getDryValue();
  Real32 flowChange = exhDryFlow - prevDryExhFlow_;
  Real32 estimatedExhFlow = 3.0F * exhDryFlow - 2.0F * prevDryExhFlow_;
  Real32 peepValue = RPeep.getActualPeep();
  Real32 exhPressure = RExhPressureSensor.getValue(); 
  Real32 airwayPressure = BreathRecord::GetAirwayPressure();
  Real32 inspPressure = RInspPressureSensor.getValue(); 
  Real32 pressSensValue = 0.0f;
  Real32 flowOffset = 0.0f;
  prevDryExhFlow_ = exhDryFlow;


  const DiscreteValue MODE_VALUE = PhasedInContextHandle::GetDiscreteValue(SettingId::MODE);

  // Initially decrease the pressure sensitivity to prevent autocyclying under leak
  // conditions.
  // $[LC24043] -- The back up pressure trigger sensitivity under flow triggering... 
  if ( (RLeakCompMgr.isEnabled()) && 
	   (triggerId_ == Trigger::PRESSURE_BACKUP_INSP) && 
	   (BreathRecord::GetTrueExhTime() < 300) && 
	   (MODE_VALUE != ModeValue::BILEVEL_MODE_VALUE)
	  )
  {
	  pressSensValue = MAX_VALUE(RPeep.getActualPeep(), getPressureSens());
  }
  else 
  {
  
	  //  each type of pressure trigger has its own pressure sensitivity
	  //  get pressure sensitivity from the derived object
	  pressSensValue = getPressureSens();
  }


  if (!isTriggerActive_)
  {
    // $[TI4]
    activateTrigger_();
  }
  // $[TI5]
 
  if (isTriggerActive_) 
  {
    // $[TI12]
    flowOffset = 4.915F * (peepValue - pressSensValue - exhTraj_) / (.017F * exhTraj_ + .7F);
    flowOffset = MIN_VALUE( flowOffset, MAX_FLOW_OFFSET);
    flowOffset = MAX_VALUE( flowOffset, MIN_FLOW_OFFSET);
    if (estimatedExhFlow <= flowOffset)
    {
      // $[TI14]
      isZeroExhFlow_ = TRUE;
    }
    // $[TI15]
    
    if (flowChange >= 5.0F)
    {
      // $[TI16]
      isZeroExhFlow_ = FALSE;
    }
    // $[TI17]

    if (ABS_VALUE(flowChange) <= EXH_FLOW_SLOPE_TRIG_VALUE)
    {
      // $[TI18]
      flowChangeCount_++;
    }
    else
    {
      // $[TI19]
      flowChangeCount_ = 0;
    }
  }
  // $[TI13]

	Real32 referencePressure = peepValue;


	Boolean condition1 = TRUE;

	//  When Leak comp is enabled, delay backup pressure trigger to prevent 
	//  false triggering if there is a transient drop in pressure before 
	//  peep controller manages to maintain peep.
	if (RLeakCompMgr.isEnabled() && triggerId_ == Trigger::PRESSURE_BACKUP_INSP) 
	{
	    condition1 = (RLeakCompMgr.isPressureAndFlowSteady() || BreathRecord::GetTrueExhTime() > 450);
	}

	Real32 peepBaseErr = RPeepController.getPeepBaseline() - RPeep.getActualPeep();
	Real32 pressTrigAdjust;
	if ( peepBaseErr >= 0.0F)
	{
		if (RPeepController.isPeepChanged())
			pressTrigAdjust = 0.0 * peepBaseErr;  //do not remove, I will refine it further 
		else 
			pressTrigAdjust = 0.0 * peepBaseErr;  //do not remove, I will refine it further 
	}
	else
		pressTrigAdjust = 1.0 * peepBaseErr;
	
	if ( condition1  &&
	   isTriggerActive_ && 
	   ( (inspPressure <= referencePressure - (pressSensValue + 3.0F)) ||
		 ( (airwayPressure <= referencePressure - pressSensValue + pressTrigAdjust) &&
		   (isZeroExhFlow_ || ( flowChangeCount_ >= FLOW_CHANGE_COUNT_LIMIT)) &&
		   (RExhPressureSensor.getSlope() <= 0.0F) ) ) )
	{
		// $[TI8]
		rtnValue = TRUE;
	}
	// $[TI9]

  // TRUE: $[TI10]
  // FALSE: $[TI11]
  return (rtnValue);
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================

