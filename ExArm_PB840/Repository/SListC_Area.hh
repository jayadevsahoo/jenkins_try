 
#ifndef SListC_Area_HH
#define SListC_Area_HH

//====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//====================================================================

//====================================================================
// Class: SListC_Area - Singly-Linked List of 'Area' Items.
//--------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Foundation/vcssrc/AbstractSListC.H_v   25.0.4.0   19 Nov 2013 14:03:42   pvcs  $
//
//@ Modification-Log
//
//   Revision: 003   By: sah   Date: 07-Jan-1999   DR Number: 5321
//   Project:  ATC
//   Description:
//	Changed 'SoftFault()' method to an non-inlined method.
//  
//   Revision: 002   By: sah   Date: 06-Feb-1996   DCS Number: 674
//   Project:  Sigma (R8027)
//   Description:
//	Removed 'ifndef' and 'endif' from around includes.
//  
//   Revision: 001   By: sah   Date: 21-Jan-1995   DR Number: <NONE>
//   Project:  Sigma (R8027)
//   Description:
//	Initial version (Integration baseline).
//
//====================================================================

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
class  Ostream;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

//@ Usage-Classes
#include "SNodeC_Area.hh"
#include "Heap.hh"
//@ End-Usage


class SListC_Area
{
#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    // Friend:  operator<<
    // This facilitates debug-only printing of this class's instances.
    friend inline Ostream&  operator<<(Ostream&                  ostr,
				       const SListC_Area& sList);
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

    //@ Friend:  SIterC_Area
    // This list's corresponding iterator needs access to the protected
    // members of this list.
    friend class  SIterC_Area;

    //@ Friend:  SListR_Area
    // The singly-linked reference list uses a singly-linked list of
    // void*'s, and needs access to it's protected members.
    friend class  SListR_Area;

  public:
    virtual ~SListC_Area(void);

    inline Boolean  isEmpty(void) const;
    inline Boolean  isFull (void) const;

    inline Boolean  isAtFirst(void) const;
    inline Boolean  isAtLast (void) const;

    inline Uint32  getNumItems(void) const;
    inline Uint32  getMaxItems(void) const;

    inline SigmaStatus  goFirst(void);
    inline SigmaStatus  goNext (void);

    SigmaStatus  search(const Area&      item,
			const SearchFrom startFrom = FROM_FIRST);

    void  insert (const Area&          newItem,
		  const InsertionPlace insertPlace = AFTER_CURRENT);
    void  append (const Area& newItem);
    void  prepend(const Area& newItem);

    SigmaStatus  removeCurrent(void);
    SigmaStatus  removeItem(const Area& item);
    void         clearList(void);

    inline const Area&  currentItem(void) const;
    inline Area&        currentItem(void);

    void  operator=(const SListC_Area& sList);

    inline Boolean  operator==(const SListC_Area& sList) const;
    inline Boolean  operator!=(const SListC_Area& sList) const;

    static void  SoftFault(const SoftFaultID softFaultID,
                           const Uint32      lineNumber,
                           const char*       pFileName = NULL,
                           const char*       pBoolTest = NULL);

  protected:
    inline SListC_Area(Heap* pHeap);

    // return a pointer to the link that is equivalent to 'item', starting
    // the search from 'pStart'...
    const SNodeC_Area* findNode_(const Area&        item,
				 const SNodeC_Area* pStartNode) const;

    inline Boolean  isLegalNode_(const SNodeC_Area* pNode) const;

    inline const SNodeC_Area*  getFirst_  (void) const;
    inline const SNodeC_Area*  getCurrent_(void) const;

    inline SNodeC_Area*  getFirst_  (void);
    inline SNodeC_Area*  getCurrent_(void);

    inline void  setCurrent_(SNodeC_Area* pNewCurr);

    Boolean  isEquivTo_(const SListC_Area& sList) const;

#if defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)
    virtual Ostream&  print_(Ostream& ostr) const = 0;
#endif  // defined(SIGMA_UNIT_TEST)  &&  defined(SIGMA_DUMP_OUT)

  private:
    SListC_Area(const SListC_Area&);	// not implemented...
    SListC_Area(void);				// not implemented...

    inline void  deallocNode_(SNodeC_Area*& pOldNode);

    SNodeC_Area* getPrevNode_(const SNodeC_Area* pStart) const;

    //@ Data-Member:  rHeapOfNodes_
    // A reference to this list's heap of nodes.
    Heap&  rHeapOfNodes_;

    //@ Data-Member:  pFirst_
    // A pointer to the first node in this list.
    SNodeC_Area*  pFirst_;

    //@ Data-Member:  pCurrent_
    // A pointer to the current node of this list.
    SNodeC_Area*  pCurrent_;
};


// Inlined methods...
#include "SListC_Area.in"


#endif  // SListC_Area_HH
