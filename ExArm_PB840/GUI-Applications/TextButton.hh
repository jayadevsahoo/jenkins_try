#ifndef TextButton_HH
#define TextButton_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Class: TextButton - A button which displays a piece of text in its
// interior (in the button's Label container).
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/TextButton.hhv   25.0.4.0   19 Nov 2013 14:08:34   pvcs  $
//
//@ Modification-Log
//
//  Revision: 009   By:   rhj    Date: 07-July-2008      SCR Number: 6435
//  Project:  LEAK
//  Description:
//       Leak Compensation project-related changes.
//       
//  Revision: 008   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 007   By: rhj    Date:  13-Oct-2006   DCS Number: 6236
//  Project:  RESPM
//  Description:
//      Added a functionality to change the title text with an Align 
//      right position.
//
//  Revision: 006   By: gdc    Date:  2-Oct-2006   DCS Number: 6236
//  Project:  RESPM
//  Description:
//      Added push Button Type.
//
//  Revision: 004   By: rhj    Date:  20-April-2006   DCS Number: 6181 
//  Project:  PAV4
//  Description:
//      Added a functionality to change the auxillary title text.
//
//  Revision: 003   By: sah    Date:  20-Apr-2000    DCS Number: 5705
//  Project:  NeoMode
//  Description:
//      Modified constructor to take arguments for auxillary title text
//      and position.  Also, added new 'setAuxTitleShowFlag_()' method,
//      and new data members.  This functionality will be used to plug-in
//      the "above PEEP" phrases to the "Pi" and "Psupp" setting buttons.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//====================================================================

#include "Button.hh"

//@ Usage-Classes
#include "TextField.hh"
#include "GuiAppClassIds.hh"
//@ End-Usage

class TextButton : public Button
{
	public:
    	TextButton(Uint16 xOrg, Uint16 yOrg, Uint16 width, Uint16 height,
    			   ButtonType buttonType, Uint8 bevelSize, CornerType cornerType,
    			   BorderType borderType, StringId title,
    			   StringId auxTitleText = ::NULL_STRING_ID,
    			   Gravity  auxTitlePos  = ::GRAVITY_RIGHT,
    			   Button::PushButtonType pushButtonType = Button::PB_TOGGLE);
    	~TextButton(void);

		void setTitleText(StringId title, Gravity gravity=GRAVITY_CENTER);
		void setTitleColor(Colors::ColorS color);
		void setTitleHighlighted(Boolean var);
		void setTitleHighlightColor(Colors::ColorS color);
        void setAuxTitleText(StringId title);

		static void SoftFault(const SoftFaultID softFaultID,
				  const Uint32      lineNumber,
				  const char*       pFileName  = NULL, 
				  const char*       pPredicate = NULL);

	protected:
		void  setAuxTitleShowFlag_(const Boolean);

	private:
		// these methods are purposely declared, but not implemented...
		TextButton(const TextButton&);		// not implemented...
		void   operator=(const TextButton&);	// not implemented...

	    //@ Data-Member: titleText_
	    // A TextField which displays the title text of the button.
		TextField titleText_;

	    //@ Data-Member: auxTitleText_
	    // A TextField which displays the auxillary title text of the button,
		// if any.
		TextField auxTitleText_;
		
	    //@ Data-Member: AUX_TITLE_POS_
	    // Positioning info for the auxillary title text.
		const Gravity  AUX_TITLE_POS_;

	    //@ Data-Member: unitsText_
	    // A TextField which displays the units text of the button.
		TextField unitsText_;

        //@ Data-Member: titlePos_
	    // Positioning info for the title text.
		Gravity  titlePos_;

};

#endif // TextButton_HH 
