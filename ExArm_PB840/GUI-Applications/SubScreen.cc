#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, sorted in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1993, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SubScreen - A subscreen (or display screen).  Subscreens are
// displayed in a subscreen area (see SubScreenArea) and are normally activated
// by pressing subscreen buttons (see SubScreenButton).  This class is part
// of the Loadable Screens cluster.
//---------------------------------------------------------------------
//@ Interface-Description
// This class (derived from GUI-Foundation's Container class) is never
// instantiated directly.  It stores a pointer to the subscreen area in which
// this subscreen will be displayed and provides virtual methods common to all
// (or most) subscreens.
//
// Many subscreens need to trap changes to settings and button
// presses so SubScreen inherits from ButtonTarget
// and defines the required inherited virtual functions.  This is as
// a convenience for derived subscreens.
//
// There is also an acceptHappened() method which is called by Main
// Setting and Proceed buttons whenever the Accept key is pressed when the
// buttons have the Adjust Panel focus.
//
// Derived classes must define the activate() and deactivate() methods.
// These methods are called by the associated SubScreenArea class when
// this SubScreen is activated (displayed) and deactivated (removed
// from the display) allowing the subscreen to do any necessary
// screen setup or tidy up.
//---------------------------------------------------------------------
//@ Rationale
// This class is used to group functionality common to all subscreens,
// such as their ability to be activated/deactivated and their
// need to be targets of Gui Settings and Buttons.
//---------------------------------------------------------------------
//@ Implementation-Description
// Very little is provided in this class apart from some virtual methods and
// the multiple inheritance from Container and ButtonTarget.
// One data member is defined, a pointer to the subscreen area in which this
// subscreen will be displayed (which should also be the object which creates
// this subscreen).
//---------------------------------------------------------------------
//@ Fault-Handling
// No special fault handling apart from the usual assertions and pre-conditions
// to verify the validity of arguments.
//---------------------------------------------------------------------
//@ Restrictions
// SubScreen cannot be instantiated directly, it must be inherited from.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ Abstract-Representation
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/GUI-Applications/vcssrc/SubScreen.ccv   25.0.4.0   19 Nov 2013 14:08:32   pvcs  $
//
//@ Modification-Log
//
//  Revision: 004  By:  yyy	   Date:  29-Jun-1998    DCS Number: 5327
//  Project:  NeoMode
//  Description:
//		Initial version.
//
//  Revision: 003  By:  hhd	   Date:  27-Apr-1999    DCS Number:  5322
//  Project:  ATC
//  Description:
// 		Initial version.
//
//  Revision: 002  By:  gdc	   Date:  29-Jun-1998    DCS Number: 
//  Project:  Color
//  Description:
//		Initial version.
//
//  Revision: 001  By:  mpm    Date:  01-APR-94    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Integration baseline.
//=====================================================================

#include "SubScreen.hh"

//@ Usage-Classes
#include "SubScreenArea.hh"
#include "SettingButton.hh"
//@ End-Usage

//@ Code...

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SubScreen()  [Constructor]
//
//@ Interface-Description
// Creates a SubScreen instance.  It is passed a pointer to the subscreen
// area which displays this subscreen.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor simply stores the subscreen area pointer.
//---------------------------------------------------------------------
//@ PreCondition
// The SubScreenArea pointer must be non-NULL
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SubScreen::SubScreen(SubScreenArea *pSubScreenArea, Boolean isMainSetting) :
		pSubScreenArea_(pSubScreenArea),
		isMainSetting_(isMainSetting)
{
	CALL_TRACE("SubScreen::SubScreen(SubScreenArea *pSubScreenArea)");

	CLASS_PRE_CONDITION(pSubScreenArea != NULL);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SubScreen()  [Destructor]
//
//@ Interface-Description
// Called automatically on destruction of this object.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

SubScreen::~SubScreen(void)
{
	CALL_TRACE("SubScreen::~SubScreen(void)");

	// Do nothing
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: activate
//
//@ Interface-Description
// No arguments needed.  Called when this screen needs to be activated,
// normally by the subscreen area which displays this subscreen.
// This is a pure virtual function which is not implemented in this
// class.
//
// Typical actions would be to initialize the display, to make sure
// all drawables are on-screen in their appropriate positions and the
// correct prompts are displayed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: deactivate
//
//@ Interface-Description
// No arguments needed.  Called when this screen needs to be deactivated,
// normally by the subscreen area which displays this subscreen.
// This is a pure virtual function which is not implemented in this
// class.
//
// Allows subscreens to do any tidy up needed when a subscreen loses the
// display such as releasing the Adjust Panel focus or removing prompts.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

//@ Not-Implemented


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: acceptHappened
//
//@ Interface-Description
// This method is defined, but should never be called except in derived
// classes.  All derived classes which expect a valid Accept key press
// redefine this method to perform the proper "accept" notification to the
// Settings-Validation subsystem.
//
// Generally, this is called by setting buttons when the operator
// presses the Accept key while adjusting a main setting.  Also, called
// by the Proceed button when the same key is pressed.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//
// $[01050] All Settings changes must be confirmed by the Accept key ...
//---------------------------------------------------------------------
//@ PreCondition
//	Cannot be called.
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreen::acceptHappened(void)
{
	CALL_TRACE("SubScreen::acceptHappened(void)");

	//
	// This method is defined, but should never be called.  All derived
	// classes which expect a valid Accept key press must redefine this
	// method as described above.
	//
	CLASS_ASSERTION(FALSE);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: initializeMainSetting
//
//@ Interface-Description
// This method do nothing.
//---------------------------------------------------------------------
//@ Implementation-Description
//  none
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreen::initializeMainSetting(void)
{
	CALL_TRACE("SubScreen::initializeMainSetting(void)");
// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: isMainSetting
//
//@ Interface-Description
// This method returns the main setting's status.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Simply returns isMainSetting_.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

Boolean
SubScreen::isMainSetting(void)
{
	CALL_TRACE("SubScreen::isMainSetting(void)");

	return(isMainSetting_);
	// $[TI1]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//		  [static]
//
//@ Interface-Description
//	This method is called when a software fault is detected by the
//	fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID' and
//	'lineNumber' are essential pieces of information.  The 'pFileName'
//	and 'pPredicate' strings may be defaulted in the macro to reduce
//	code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//	This method receives the call for the SoftFault, adds it sub-system
//	and class name ID and sends the message to the non-member function
//	SoftFault. 
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition	
//	none
//@ End-Method
//=====================================================================

void
SubScreen::SoftFault(const SoftFaultID  softFaultID,
					 const Uint32       lineNumber,
					 const char*        pFileName,
					 const char*        pPredicate)  
{
	CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
	FaultHandler::SoftFault(softFaultID, GUI_APPLICATIONS, SUBSCREEN,
							lineNumber, pFileName, pPredicate);
}


// ============= M E T H O D   D E S C R I P T I O N ====
//@ Method: operateOnButtons_
//
//@ Interface-Description
//  Kicks off action given by operation method on array of setting buttons.
//---------------------------------------------------------------------
//@ Implementation-Description
//  Loop through the array of button pointers and invoke the operation method
//  on each setting button.
//---------------------------------------------------------------------
//@ PreCondition
//	none
//---------------------------------------------------------------------
//@ PostCondition
//	none
//@ End-Method
//=====================================================================

void
SubScreen::operateOnButtons_(SettingButton*                 arrButtonPtrs[],
							 SubScreen::OperationMethodPtr_ pOperationMethod)
{
	for (Uint idx = 0u; arrButtonPtrs[idx] != NULL; idx++)
	{
		 ((arrButtonPtrs[idx])->*pOperationMethod)();
	}
}  // $[TI1]
