#ifndef TrendDbPatientRepository_HH
#define TrendDbPatientRepository_HH


//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 2007, Puritan-Bennett Corporation
//=====================================================================

//=====================================================================
// Class: TrendDbPatientData
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Trend-Database/vcssrc/TrendDbPatientRepository.hhv   25.0.4.0   19 Nov 2013 14:34:38   pvcs  $
//
//@ Modification-Log
// 
//  Revision: 002   By: rpr    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support Leak Compensation
// 
//    Revision: 001  By:  ksg    Date:  4-Jun-2007    SCR Number: 6237
//       Project:  Trend
//       Description: Initial version
//=====================================================================

#include "TrendDataSet.hh"
#include "TrendDbSampleAvg.hh"
#include "TrendDbSampleNonAvg.hh"
#include "TrendDbSampleEvents.hh"
#include "TrendDbConfig.hh"
#include "TrendDbTable.hh"
#include "TrendDbPatientRecord.hh"
#include "TrendDbSampleRm.hh"
#include "BreathDatumHandle.hh"     // Patient-Data


// forward declare
class TimeStamp;

class TrendDbPatientRepository
{
public:

	enum TableScheduleTime
	{
		TABLE_1HR_TIMING    = 1,	// every "sample" cycle
		TABLE_2HR_TIMING    = 2,	// every other "sample" cycle
		TABLE_4HR_TIMING    = 4,	// etc ...
		TABLE_8HR_TIMING    = 8,
		TABLE_12HR_TIMING   = 12,
		TABLE_24HR_TIMING   = 24,
		TABLE_48HR_TIMING   = 48,
		TABLE_72HR_TIMING   = 72   
	};

	TrendDbPatientRepository();
	~TrendDbPatientRepository();

	void initialize( void );

	void changeHappened( const PatientDataId::PatientItemId dataId );

	void sample( Uint32 seqPosNumber, const TimeStamp & trendTimeStamp );

	Boolean isValidContext( const PatientDataId::PatientItemId dataId, BoundedBreathDatum boundedDatum );

	TrendDataSet::Status processRequest( TrendDataSet * pTrendDataSet );

	Uint16 getTableFrequency( TrendTimeScaleValue::TrendTimeScaleValueId timescale );

	void resetDatabase( void );

	void resyncAll( void );

	void sampleSimulatedData( Uint32 seqPosNumber,
							  const TimeStamp & trendTimeStamp,
							  const TrendValue * pTrendValues );

	static void SoftFault(const SoftFaultID softFaultID,
						  const Uint32      lineNumber,
						  const char*       pFileName  = NULL, 
						  const char*       pPredicate = NULL);

private:

	TrendDbPatientRepository( const TrendDbPatientRepository & );
	void operator=( const TrendDbPatientRepository & );

	SigmaStatus updateTable_( TrendDbTable * pTable,
							  TrendTimeScaleValue::TrendTimeScaleValueId timescale,
							  Uint32 seqPosNumber, const TimeStamp & trendTimeStmap );

	void scheduleTableUpdates_( Uint32 seqPosNumber,
								const TimeStamp & trendTimeStamp );

	Int16 loadRecordData_( TrendDbTableBuffer & inputBuffer,
						   TrendDataSet * pTrendDataSet,
						   Uint16 firstRow );

	//@ Data-Member: refreshArray_
	// Array used to know if a value is in need
	// of updating
	Boolean         isRefreshedArray_[PatientDataId::NUM_TOTAL_PATIENT_DATA];

	//@ Data-Member:: userEventsSample_
	// Trend sample for collection of the events
	TrendDbSampleEvents  userEventsSample_;

	//@ Data-Member:: autoEventsSample_
	// Trend sample for collection of the events
	TrendDbSampleEvents  autoEventsSample_;

	//@ Data-Member: dynComplianceSample_
	// Trend sample collection for Cdyn
	TrendDbSampleAvg   dynComplianceSample_;

	//@ Data-Member: pavLungCompSample_
	// Trend sample collection for Cpav
	TrendDbSampleAvg pavComplianceSample_;

	//@ Data-Member: statComplianceSample_
	// Trend sample collection for Cstat
	TrendDbSampleNonAvg statComplianceSample_;

	//@ Data-Member: pavElastanceSample_
	// Trend sample collection for Epav
	TrendDbSampleAvg pavElastanceSample_;

	//@ Data-Member: endExpFlowSample_
	// Trend sample collection for EEF
	TrendDbSampleAvg endExpFlowSample_;

	//@ Data-Member: peakExpFlowSample_
	// Trend sample collection for PEF
	TrendDbSampleAvg peakExpFlowSample_;

	//@ Data-Member: peakSpontFlowSample_
	// Trend sample collection for PSF
	TrendDbSampleAvg peakSpontFlowSample_;

	//@ Data-Member: totalRespRateSample_
	// Trend sample collection for Ftot
	TrendDbSampleAvg totalRespRateSample_;

	//@ Data-Member: ieRatioSample_
	// Trend sample collection for I:E
	TrendDbSampleAvg  ieRatioSample_;

	//@ Data-Member: nifPressSample_
	// Trend sample collection for NIF
	TrendDbSampleRm nifPressSample_;

	//@ Data-Member: deliveredO2Sample_
	// Trend sample collection for O2
	TrendDbSampleNonAvg deliveredO2Sample_;

	//@ Data-Member: p100PressSample_
	// Trend sample collection for p100
	TrendDbSampleRm p100PressSample_;

	//@ Data-Member: endExpPressSample_
	// Trend sample collection for PEEP
	TrendDbSampleAvg   endExpPressSample_;

	//@ Data-Member: intrinsicPeepSample_
	// Trend Samnple collection for PEEPi
	TrendDbSampleNonAvg  intrinsicPeepSample_;

	//@ Data-Member: pavIntrinsicPeepSample_
	// Trend Sample collection for PEEPi pav
	TrendDbSampleAvg  pavIntrinsicPeepSample_;

	//@ Data-Member: totalWorkOfBreathing
	// Trend Sample collection for WOBtot 
	TrendDbSampleAvg  totalWorkOfBreathingSample_;

	//@ Data-Member: totalPeepSample_
	// Trend Sample collection for PEEPtot
	TrendDbSampleNonAvg  totalPeepSample_;

	//@ Data-Member: meanCctPressSample_
	// Trend sample collection for Pmean
	TrendDbSampleAvg  meanCctPressSample_;

	//@ Data-Member: peakCctPressSample_
	// Trend sample collection for Ppeak
	TrendDbSampleAvg peakCctPressSample_;

	//@ Data-Member: plateauPressSample_
	// Trend sample collection for Pplat
	TrendDbSampleNonAvg plateauPressSample_;

	//@ Data-Member: spontRapidIndexSample_
	// Trend Sample collection for f/Vt
	TrendDbSampleAvg  spontRapidIndexSample_;

	//@ Data-Member: dynResistanceSample_
	// Trend sample collection for Rdyn
	TrendDbSampleAvg dynResistanceSample_;

	//@ Data-Member: pavResistanceSample_
	// Trend sample collection for Rpav
	TrendDbSampleAvg pavResistanceSample_;

	//@ Data-Member: statResistanceSample_
	// Trend sample collection for Rstat
	TrendDbSampleNonAvg statResistanceSample_;

	//@ Data-Member: pavTotalResistanceSample_
	// Trend sample collection for Rtot
	TrendDbSampleAvg  pavTotalResistanceSample_;

	//@ Data-Member: spontInspiratoryTimeSample_
	// Trend sample collection for Ti spont
	TrendDbSampleAvg  spontInspiratoryTimeSample_;

	//@ Data-Member: spontInspiratoryRatioSample_
	// Trend sample collection for Ti/Ttot
	TrendDbSampleAvg  spontInspiratoryRatioSample_;

	//@ Data-Member: slowVitalCapacitySample_
	// Trend sample collection for VC
	TrendDbSampleRm   slowVitalCapacitySample_;

	//@ Data-Member: exhMinuteVolSample_
	// Trend sample collection for VeTot
	TrendDbSampleAvg exhMinuteVolSample_;

	//@ Data-Member: exhSpontMinVolSample_
	// Trend sample collection for VeSpont
	TrendDbSampleAvg exhSpontMinVolSample_;

	//@ Data-Member: exhTidalVolSample
	// Trend sample collection for Vte
	TrendDbSampleAvg  exhTidalVolSample_;

	//@ Data-Member: exhSpontTidalVolSample_
	// Trend sample collection for VteSpont
	TrendDbSampleAvg exhSpontTidalVolSample_;

	//@ Data-Member: exhMandTidalVolSample_
	// Trend sample collection for VteMand
	TrendDbSampleAvg  exhMandTidalVolSample_;

	//@ Data-Member: inspTidalVolSample_
	// Trend sample collection for Vti
	TrendDbSampleAvg inspTidalVolSample_;

	//@ Data-Member: iLeakSample_
	// Trend sample collection for iLeak
	TrendDbSampleAvg iLeakSample_;

	//@ Data-Member: LeakSample_
	// Trend sample collection for Leak
	TrendDbSampleAvg LeakSample_;

	//@ Data-Member: PercentLeakSample_
	// Trend sample collection for Percent Leak
	TrendDbSampleAvg PercentLeakSample_;

	//@ Data-Member: dbConfig_
	// Configuration of the Patient Trend database
	TrendDbConfig    dbConfig_;

	//@ Data-Member: patientRecord_
	// Used to access the fields of the record
	TrendDbPatientRecord    patientRecord_;

	//@ Data-Member: compactFlashInterface_
	// Interface to the compact flash
	CompactFlash &  compactFlashInterface_;

	//@ Data-Member: table1Hr_
	//  Trend table for 1 hour
	TrendDbTable    table1Hr_;

	//@ Data-Member: table2hr_
	//  Trend table for 2 Hour
	TrendDbTable    table2Hr_;

	//@ Data-Member: table4Hr_
	//  Trend table for 4 Hour
	TrendDbTable    table4Hr_;

	//@ Data-Member: table8Hr_
	//  Trend table for 8 hour
	TrendDbTable    table8Hr_;

	//@ Data-Member: table12Hr_
	//  Trend table for 12 hour
	TrendDbTable    table12Hr_;

	//@ Data-Member: table24Hr_
	//  Trend table for 24 hour
	TrendDbTable    table24Hr_;

	//@ Data-Member: table48Hr_
	// Trebd table for 48 hour
	TrendDbTable    table48Hr_;

	//@ Data-Member: table72Hr_
	//  Trend table for 72 hour
	TrendDbTable    table72Hr_;

	//@ Type: TableSchedule
	// Used to schedule the storage of the tables
	struct TableSchedule
	{
		Uint8           factor;
		TrendDbTable *  ptrTable;
	};

	//@ Data-Member: tableSchedule_
	// An array of schedules for each table
	TableSchedule   tableSchedule_[ TrendTimeScaleValue::TOTAL_TREND_TIME_SCALE_VALUES ];
};

#endif // TrendDbPatientRepository_HH
