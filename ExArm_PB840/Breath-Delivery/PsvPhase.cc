#include "stdafx.h"

//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: PsvPhase - Implements pressure support inspiration ventilation.
//---------------------------------------------------------------------
//@ Interface-Description
//		This class is derived from PressureBasedPhase class.  No public
//		methods are defined by this class since base class methods are used.
//		Protected virtual methods are implemented to support the base class.
//---------------------------------------------------------------------
//@ Rationale
//		This class implements the algorithms for pressure support
//		inspirations.
//---------------------------------------------------------------------
//@ Implementation-Description
// 		The data members values from the base class are defined	in the pure
//		virtual methods.
//---------------------------------------------------------------------
//@ Fault-Handling
//		N/A
//---------------------------------------------------------------------
//@ Restrictions
//		none
//---------------------------------------------------------------------
//@ Invariants
//		none
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/PsvPhase.ccv   25.0.4.0   19 Nov 2013 14:00:06   pvcs  $
//
//@ Modification-Log
//
//  Revision: 015   By:   rhj    Date: 05-March-2007      DR Number: 6359
//  Project:  RESPM
//  Description:
//        Resolve merge conflict for RM to Baseline 
//
//  Revision: 014   By: gdc   Date:  22-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		removed getExhSens_() methid as dead code, never called.
//
//  Revision: 013   By: gdc   Date:  10-June-2005    DR Number: 6170
//  Project:  NIV2
//  Description:
//     DCS 6170 - NIV2
//     Max inspiratory time changed to setting instead of IBW-based calculation
//     when in NIV.  
//
//  Revision: 012  By: jja     Date:  31-Aug-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//      Added logic to handle a test breath for VS
//
//  Revision: 011  By: syw     Date:  14-Feb-2000    DR Number: 5638
//  Project:  NeoMode
//  Description:
//		Changed tTarget to 2/3* (383 + 133 * ibw) for neonatal cct types
//
//  Revision: 010  By: sah     Date:  22-Sep-1999    DR Number: 5327
//  Project:  NeoMode
//  Description:
//      NeoMode-specific changes:
//      *  added neonatal-specific calculation of 'timeToTarget_',
//         and optimized adult and pediatric calculations
//		*  added neonatal-specific calculation of maxInspTimeMs_
//
//  Revision: 009  By: yyy     Date:  5-May-1999    DR Number: 5322
//  Project:  ATC
//  Description:
//      Use symbolic name to replace the magic number for MIN_SUPPORT_PRESSURE.
//
//  Revision: 008  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//
//  Revision: 007  By:  syw    Date:  14-Jul-1998    DR Number: None
//       Project:  Sigma (840)
//       Description:
//          Bilevel initial version.
//
//  Revision: 006  By: syw   Date:  18-Jul-1997    DR Number: 2299, 2418
//  	Project:  Sigma (840)
//		Description:
//			Changed formula for time to target.
//
//  Revision: 005  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 004 By: sp    Date: 02-Jan-1997   DR Number: NONE
//  	Project:  Sigma (R8027)
//		Description:
//			Inspection rework.
//
//  Revision: 003 By: syw    Date: 06-May-1996   DR Number: DCS 600
//  	Project:  Sigma (R8027)
//		Description:
//			Added getExhSens_() and call
//			rDeliveredFlowExpTrigger.setExhalationSensitivty() in
//			enableExhalationTriggers_() and rDisconnectTrigger.setFlowCmdLimit()
//			rDisconnectTrigger.setTimeLimit().
//
//  Revision: 002 By: syw    Date: 21-Feb-1996   DR Number: DCS 674
//  	Project:  Sigma (R8027)
//		Description:
//			Removed #ifndef around #include.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "PsvPhase.hh"

#include "TriggersRefs.hh"
#include "BreathMiscRefs.hh"
#include "ControllersRefs.hh"
#include "ModeTriggerRefs.hh"

//@ Usage-Classes

#include "BdDiscreteValues.hh"
#include "TimerBreathTrigger.hh"
#include "PhasedInContextHandle.hh"
#include "O2Mixture.hh"
#include "MathUtilities.hh"
#include "DeliveredFlowExpTrigger.hh"
#include "PressureController.hh"
#include "DisconnectTrigger.hh"
#include "Peep.hh"

//@ End-Usage

//@ Code...

//@ Constant: MIN_SUPPORT_PRESSURE
// The minimum peep pressure to be maintained
extern const Real32 MIN_SUPPORT_PRESSURE ;
const Real32 MIN_SUPPORT_PRESSURE = 1.5f;

//@ Constant: PSV_THRESHOLD
//	pressure threshold where linear interpolation is used
static const Real32 PSV_THRESHOLD = 5.0F ;

//@ Constant: VSV_TEST_BREATH_LEVEL
//	pressure support level for VSV Test breath
static const Real32 VSV_TEST_BREATH_LEVEL = 15.0F ;

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: PsvPhase()
//
//@ Interface-Description
//		Constructor.  This method has no arguments and returns nothing.
//		The rO2Mixture object should be constructed before this class is
//		constructed.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The base class constructor is called.  Register breath phase to
//		O2Mixture.
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PsvPhase::PsvPhase( void)
 : PressureBasedPhase() 		// $[TI1]
{
	CALL_TRACE("PsvPhase::PsvPhase( void)") ;
	
	// $[TI2]
	RO2Mixture.registerBreathPhaseCallBack( this) ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~PsvPhase()
//
//@ Interface-Description
//		Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//		none
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

PsvPhase::~PsvPhase(void)
{
	CALL_TRACE("PsvPhase::~PsvPhase(void)") ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//--------------------------------------------------------------------- 
//@ Implementation-Description 
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault. 
//--------------------------------------------------------------------- 
//@ PreCondition
//      none 
//---------------------------------------------------------------------
//@ PostCondition 
//      none 
//@ End-Method 
//===================================================================== 

void
PsvPhase::SoftFault( const SoftFaultID  softFaultID,
                   	 const Uint32       lineNumber,
					 const char*        pFileName,
		   			 const char*        pPredicate)
{
	CALL_TRACE("SoftFault( softFaultID, lineNumber, pFileName, pPredicate)") ;
  	FaultHandler::SoftFault( softFaultID, BREATH_DELIVERY, PSVPHASE,
                             lineNumber, pFileName, pPredicate) ;
}

//=====================================================================
//
//  Protected Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineEffectivePressureAndBiasOffset_
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to determine the
//		effective pressure and the bias offset. 
//---------------------------------------------------------------------
//@ Implementation-Description
//		The bias offset = 1.5.  The effective pressure is the biasOffset_
//		if the pressure support level is zero, PSV_THRESHOLD if the pressure
//		support level is PSV_THRESHOLD, else the effective pressure is the support
//		pressure.  Linear interpolation is used to determine the
//		effective pressure if the support pressure is less than PSV_THRESHOLD. 
// 		$[04171] $[04172] $[04211]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PsvPhase::determineEffectivePressureAndBiasOffset_( void)
{
	CALL_TRACE("PsvPhase::determineEffectivePressureAndBiasOffset_( void)") ;
	
	const BoundedValue& rPressureSupportLevel =
			PhasedInContextHandle::GetBoundedValue( SettingId::PRESS_SUPP_LEVEL) ;

   	DiscreteValue supportType = 
					PhasedInContextHandle::GetDiscreteValue( SettingId::SUPPORT_TYPE) ;
		
	biasOffset_ = MIN_SUPPORT_PRESSURE;

	if (supportType == SupportTypeValue::OFF_SUPPORT_TYPE)
	{
		// $[TI1.1]
		effectivePressure_ = MIN_SUPPORT_PRESSURE;
	}
	else if (supportType == SupportTypeValue::PSV_SUPPORT_TYPE) 
	{
		// $[TI1.2]
		if (rPressureSupportLevel.value <= PSV_THRESHOLD)
		{
			// $[TI1.2.1]
			effectivePressure_ = rPressureSupportLevel.value * (PSV_THRESHOLD - biasOffset_)
							 / PSV_THRESHOLD + biasOffset_ ;
		}
		else
		{
			// $[TI1.2.2]
			effectivePressure_ = rPressureSupportLevel.value ;
		}
	}
	else if (supportType == SupportTypeValue::VSV_SUPPORT_TYPE) 
	{
		// $[TI1.4]
		// $[VC24016]
		//Do a PSV test breath before the VSV phase kicks in
		Real32 hcp = PhasedInContextHandle::GetBoundedValue( SettingId::HIGH_CCT_PRESS).value ;
		Real32 peep = RPeep.getActualPeep() ;
		
		effectivePressure_ = MIN_VALUE( VSV_TEST_BREATH_LEVEL, hcp - 3.0F - peep) ;
	}
	else
	{   // $[TI1.3]
        AUX_CLASS_ASSERTION_FAILURE(supportType);
	}
}
		

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineTimeToTarget_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to determine the
//		time to target.
//---------------------------------------------------------------------
//@ Implementation-Description
//
//  \a\ ttarget =  2/3 * (0.4 * Inspiration too long time) seconds for PS
//                       and circuit type = ADULT or PEDIATRIC
//
//	\c\ ttarget =  2/3 * (0.383 + 0.133 * IBW) seconds for PS
//                       and circuit type = NEONATAL
//
//  Maximum inspiratory time is calculated based on IBW for INVASIVE 
//  ventilation and set based on the High Ti,spont Limit setting for 
//  NIV ventilation. T-target, however, is calculated based on the 
//  IBW-based maximum inspiratory time for both INVASIVE and NIV.
//  
// 		$[04175]	$[04029]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PsvPhase::determineTimeToTarget_( void)
{
	CALL_TRACE("PsvPhase::determineTimeToTarget_( void)") ;
	
	// $[TI1]
	const Real32  IBW_VALUE =
				PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value ;

	const DiscreteValue  VENT_TYPE_VALUE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::VENT_TYPE) ;

	const DiscreteValue  PATIENT_CCT_TYPE =
		PhasedInContextHandle::GetDiscreteValue(SettingId::PATIENT_CCT_TYPE) ;

	switch (PATIENT_CCT_TYPE)
	{
		case PatientCctTypeValue::PEDIATRIC_CIRCUIT :
		case PatientCctTypeValue::ADULT_CIRCUIT :
			{
				// $[TI1]
				maxInspTimeMs_ = 1990 + (Int)(IBW_VALUE * 20.0F) ;

				static const Real32  FACTOR_ = ((2.0 / 3.0) * 0.4F) ;
				timeToTarget_ = maxInspTimeMs_ * FACTOR_ ;
			}
			break ;

		case PatientCctTypeValue::NEONATAL_CIRCUIT :
			{
				// $[TI2]
				maxInspTimeMs_ = 1000 + (Int)(IBW_VALUE * 100.0F) ;

				static const Real32  FACTOR1_ = ((2.0 / 3.0) * 383.0F) ;
				static const Real32  FACTOR2_ = ((2.0 / 3.0) * 133.0F) ;

				// T-targ = (2.0 / 3.0) * (383.0 + (133.0 * IBW)) milliseconds...
				timeToTarget_ = FACTOR1_ + (FACTOR2_ * IBW_VALUE) ;
			}
			break ;

		default :
			AUX_CLASS_ASSERTION_FAILURE(PATIENT_CCT_TYPE) ;
			break ;
	}

	if (VENT_TYPE_VALUE == VentTypeValue::NIV_VENT_TYPE)
	{
	  maxInspTimeMs_ = (Int32)
		(PhasedInContextHandle::GetBoundedValue(SettingId::HIGH_SPONT_INSP_TIME).value) ;
	}
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineFlowAccelerationPercent_( void)
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to determine the
//		flow acceleration percentage.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The flow acceleration percentage is obtained.
// 		$[BL04065]
//---------------------------------------------------------------------
//@ PreCondition
//		none
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PsvPhase::determineFlowAccelerationPercent_( void)
{
	CALL_TRACE("PsvPhase::determineFlowAccelerationPercent_( void)") ;
	
	// $[TI1]
	const BoundedValue& rFlowAccelPercentage =
			PhasedInContextHandle::GetBoundedValue( SettingId::FLOW_ACCEL_PERCENT) ;

	fap_ = rFlowAccelPercentage.value ;
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableExhalationTriggers_()
//
//@ Interface-Description
//		This method has no arguments and has no return value.  This method is
//		called by the base class, PressureBasePhase, to enable the
//		triggers that will terminate inspiration.
//---------------------------------------------------------------------
//@ Implementation-Description
//		The EnablePatientTriggers and rBackupTimeExpTrigger are enabled
//		on the very first cycle.  This method must be called BEFORE
//		elapsedTimeMs_ is updated in newCycle().
// 		$[04015] $[04018] $[04028] $[04029] 
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//		none
//@ End-Method
//=====================================================================

void
PsvPhase::enableExhalationTriggers_( void)
{
	CALL_TRACE("PsvPhase::enableExhalationTriggers_( void)") ;
	
	if (elapsedTimeMs_ == 0)
	{
		// $[TI1.1]
   		BreathPhase::EnablePatientTriggers( BreathPhaseType::INSPIRATION) ;
		((BreathTrigger&)RBackupTimeExpTrigger).enable();

        // the member maxInspTimeMs_ is set in the newBreath() method of the
        // base class when the method determineTimeToTarget_() is called.
		RBackupTimeExpTrigger.restartTimer( maxInspTimeMs_ ) ;
		RDisconnectTrigger.setTimeLimit( maxInspTimeMs_ ) ;

		RDisconnectTrigger.setFlowCmdLimit( RPressureController.getFlowCommandLimit()) ;
		
	}	// implied else $[TI1.2]
}

//=====================================================================
//
//  Private Methods...
//
//=====================================================================










