/***************************************************************************
*
*		Copyright (c) 1995 MICROTEC RESEARCH INCORPORATED.
*
*	All rights reserved. MICROTEC RESEARCH's source code is an unpublished
*	work and the use of a copyright notice does not imply otherwise.
*	This source code contains confidential, trade secret material of
*	MICROTEC RESEARCH. Any attempt or participation in deciphering,
*	decoding, reverse engineering or in any way altering the source code
*	is strictly prohibited, unless the prior written consent of
*	MICROTEC RESEARCH is obtained.
*
*
*	Module Name:		%M%
*
*	Identification:		%Z% %I% %M%
*
*	Date:			%G%  %U%
*
****************************************************************************
*/



/*****************************************************************************
 *
 * File:
 *
 *    timer.h
 *
 * Author:
 *
 *    Mike Milne
 *
 * Date:
 *
 *    8-19-92
 *
 * Contents:
 *
 *    This header file contains typedefs specific to all devices. 
 *
 * Revision History:
 *
 *    Name |   Date   | Description of modification
 *    -----+----------+-----------------------------------------------------
 *    DAR  | 08-18-92 | First version
 *    -----+----------+-----------------------------------------------------
 *
 */
#ifndef _TIMER_H_
#define _TIMER_H_

#include "ansiprot.h"
#include "compiler.h"
#include "logio.h"
#include "vbsp.h"
#include "extio_1.h"

typedef struct {
    unsigned char vector;
    unsigned long clock_period;
    unsigned long count;
    unsigned long preload;
    void *specific;
} timer_config_t;

struct timer_dev_desc_t;

typedef struct {
    vbsp_return_t (*init) (_ANSIPROT1 
			   (struct timer_dev_desc_t *));
    int (*test) (_ANSIPROT1 
		 (struct logio_interrupt_entry_t *));
    vbsp_return_t (*get_count) (_ANSIPROT2 
				(struct timer_dev_desc_t *, unsigned long *));
        vbsp_return_t (*set_count) (_ANSIPROT2 
				    (struct timer_dev_desc_t *,
				     unsigned long *));
    vbsp_return_t (*get_preload) (_ANSIPROT2 
				  (struct timer_dev_desc_t *,
				   unsigned long *));
    vbsp_return_t (*set_preload) (_ANSIPROT2 
				  (struct timer_dev_desc_t *,
				   unsigned long *));
    vbsp_return_t (*get_rollover) (_ANSIPROT2 
				   (struct timer_dev_desc_t *,
				    unsigned long *));
    vbsp_return_t (*read_timer) (_ANSIPROT2 
				 (struct timer_dev_desc_t *,
				  unsigned long *));
    vbsp_return_t (*int_enable) (_ANSIPROT1 
				 (struct timer_dev_desc_t *));
    vbsp_return_t (*int_disable) (_ANSIPROT1 
				  (struct timer_dev_desc_t *));
    vbsp_return_t (*int_info_get) (_ANSIPROT2 
				   (struct timer_dev_desc_t *, int *));
    void *specific;
} timer_fops_t;

typedef struct timer_dev_desc_t {
    void *map;
    timer_config_t config;
    timer_fops_t fops;
    extern_fops_t externio;
    void *specific;
} timer_dev_desc_t;

extern const logio_fops_t logio_timer_fops;

#endif
