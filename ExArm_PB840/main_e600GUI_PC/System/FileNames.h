//*****************************************************************************
//
//  FileNames.h - Header file containing standard file names.
//
//*****************************************************************************

#ifndef FILE_NAMES_H

#define FILE_NAMES_H

#define SETTINGS_DATA_FILE_NAME                 _T(".\\Settings.Dat")
#define CALIBRATION_DATA_FILE_NAME              _T(".\\Calibration.Dat")
#define CONFIGURATION_DATA_FILE_NAME            _T(".\\Configuration.Dat")
#define DUAL_DATA_FILE_NAME                     _T(".\\Dual.Dat")
#define SPEED_TO_DAC_TABLE_FILE_NAME            _T(".\\Spd2Dac.Tbl")
#define DAC_TO_SPEED_TABLE_FILE_NAME            _T(".\\Dac2Spd.Tbl")
#define DIFFERENTIAL_PRESSURE_TABLE_FILE_NAME   _T(".\\DiffPres.Tbl")

#define ENGLISH_FILE_NAME               _T(".\\English.Str")
#define ENGLISH_BACKUP_FILE_NAME        _T(".\\EnglishBackup.Str")
#define SPANISH_FILE_NAME               _T(".\\Spanish.Str")
#define CHINESE_FILE_NAME               _T(".\\Chinese.Str")
#define VIETNAMESE_FILE_NAME            _T(".\\Vietname.Str")
#define JAPANESE_FILE_NAME              _T(".\\Japanese.Str")
#define RUSSIAN_FILE_NAME               _T(".\\Russian.Str")
#define ITALIAN_FILE_NAME               _T(".\\Italian.Str")
#define POLISH_FILE_NAME                _T(".\\Polish.Str")
#define FRENCH_FILE_NAME                _T(".\\French.Str")
#define GERMAN_FILE_NAME                _T(".\\German.Str")
#define PORTUGUESE_FILE_NAME            _T(".\\Portugue.Str")
#define GREEK_FILE_NAME                 _T(".\\Greek.Str")

#define ARIAL_BD_FONT_FILE_NAME         _T(".\\arialbd.ttf")
#define ARIAL_1_30_FONT_FILE_NAME       _T(".\\arial_1_30.ttf")
#define MSGOTHIC_FONT_FILE_NAME         _T(".\\msgothic.ttf")
#define SIMSUN_FONT_FILE_NAME           _T(".\\simsun.ttf")
#define MEDICAL_FONT_FILE_NAME          _T(".\\medical.ttf")

#define DOWNLOAD_REQUEST_FILE           _T(".\\DOWNLOAD.REQ")

#define SYSTEM_RESTART_EXE              _T(".\\Windows.\\Restart.exe")

#define TREND_THREAD_DATA_FILE          _T(".\\Thread.trd")
#define TREND_FILE_PEAK_PRESSURE        _T(".\\PeakPres.trd")
#define TREND_FILE_MEAN_AW_PRESSURE     _T(".\\Map.trd")
#define TREND_FILE_PEAK_FLOW            _T(".\\PeakFlow.trd")
#define TREND_FILE_MINUTE_VOLUME        _T(".\\MinVol.trd")
#define TREND_FILE_TIDAL_VOLUME         _T(".\\TidalVol.trd")
#define TREND_FILE_FIO2                 _T(".\\FiO2.trd")
#define TREND_FILE_CASE_TEMPERATURE     _T(".\\CaseTemp.trd")
#define TREND_FILE_BREATH_RATE          _T(".\\BrthRate.trd")
#define TREND_FILE_BASELINE_PRESSURE    _T(".\\BasePres.trd")
#define TREND_FILE_INT_BATTERY_LEVEL    _T(".\\LevIbat.trd")
#define TREND_FILE_EXT_BATTERY_LEVEL    _T(".\\LevEbat.trd")
#define TREND_FILE_INT_BATTERY_TEMP     _T(".\\TemIbat.trd")
#define TREND_FILE_EXT_BATTERY_TEMP     _T(".\\TemEbat.trd")
#define TREND_FILE_TOTAL_PEEP		_T(".\\TotalPeep.trd")
#define TREND_FILE_NIF			_T(".\\NIF.trd")
#define TREND_FILE_AUTO_PEEP		_T(".\\AutoPeep.trd")
#define TREND_FILE_PLATEAU_PRESSURE	_T(".\\PlateauPres.trd")
#define TREND_FILE_PRESSURE_01	        _T(".\\Pressure01.trd")
#define TREND_FILE_EXP_TIDAL_VOLUME	_T(".\\ExpTidalV.trd")
#define TREND_FILE_EXP_MINUTE_VOLUME	_T(".\\ExpMinVol.trd")
#define TREND_FILE_SPON_EXP_MIN_V	_T(".\\SponExpMinVol.trd")
#define TREND_FILE_PEAK_EXP_FLOW	_T(".\\PeakExpFlow.trd")

#define TREND_FILE_SPON_BREATH_RATE 	_T(".\\SponBrthRate.trd")
#define TREND_FILE_END_TIDAL_CO2	_T(".\\EndTidalCo2.trd")
#define TREND_FILE_SPO2			_T(".\\SPO2.trd")
#define TREND_FILE_PULSE_RATE		_T(".\\PulseRate.trd")
#define TREND_FILE_RSBI			_T(".\\RSBI.trd")
#define TREND_FILE_DYNAMIC_COMP 	_T(".\\DynComp.trd")
#define TREND_FILE_STATIC_COMP		_T(".\\StaticComp.trd")
#define TREND_FILE_INSP_RESISTANCE	_T(".\\InspResisitance.trd")
#define TREND_FILE_EXP_RESISTANCE	_T(".\\ExpResisitance.trd")
#define TREND_FILE_EXP_TIME_CONSTANT	_T(".\\ExpTimeConstant.trd")

#define EXE_CRCS_FILE_NAME              _T(".\\ExArm.Crcs")
#define EXE_CRCS_BACKUP_FILE_NAME       _T(".\\ExArmBackup.Crcs")

#define ENGLISH_CRC_FILE_NAME           _T(".\\English.crc")
#define ENGLISH_BACKUP_CRC_FILE_NAME    _T(".\\EnglishBackup.crc")
#define SPANISH_CRC_FILE_NAME           _T(".\\Spanish.crc")
#define CHINESE_CRC_FILE_NAME           _T(".\\Chinese.crc")
#define VIETNAMESE_CRC_FILE_NAME        _T(".\\Vietname.crc")
#define JAPANESE_CRC_FILE_NAME          _T(".\\Japanese.crc")
#define RUSSIAN_CRC_FILE_NAME           _T(".\\Russian.crc")
#define ITALIAN_CRC_FILE_NAME           _T(".\\Italian.crc")
#define POLISH_CRC_FILE_NAME            _T(".\\Polish.crc")
#define FRENCH_CRC_FILE_NAME            _T(".\\French.crc")
#define GERMAN_CRC_FILE_NAME            _T(".\\German.crc")
#define PORTUGUESE_CRC_FILE_NAME        _T(".\\Portugue.crc")
#define GREEK_CRC_FILE_NAME             _T(".\\Greek.crc")

// Download to USB flash stick names
#define ROOT_DIRECTORY                                  _T(".\\");
#define SERIAL_NUMBER_STARTING_LETTER                   _T("N");
#define SERIAL_NUMBER_MODEL_NAME                        _T("HT70");
#define USB_FLASH_DRIVE_DIRECTORY_NAME                  _T(".\\Hard Disk");
#define DOWNLOAD_STORAGE_FOLDER_NAME                    _T("FolderId_")
#define DOWNLOAD_FOLDERID_FILE_NAME                     _T("DownloadFileFolderIdList.Dat")
#define DOWNLOAD_EVENTS_FILE_NAME                       _T("Events.Dat")
#define DOWNLOAD_EVENT_SETTINGS_FILE_NAME               _T("EventSet.Dat")
#define DOWNLOAD_SETTINGS_DATA_FILE_NAME                _T("Settings.Dat")
#define DOWNLOAD_CALIBRATION_DATA_FILE_NAME             _T("Calibration.Dat")
#define DOWNLOAD_DUAL_DATA_FILE_NAME                    _T("Dual.Dat")
#define DOWNLOAD_SPEED_TO_DAC_TABLE_FILE_NAME           _T("Spd2Dac.Tbl")
#define DOWNLOAD_DAC_TO_SPEED_TABLE_FILE_NAME           _T("Dac2Spd.Tbl")
#define DOWNLOAD_DIFFERENTIAL_PRESSURE_TABLE_FILE_NAME  _T("DiffPres.Tbl")
#define DOWNLOAD_CONFIGURATION_DATA_FILE_NAME           _T("Configuration.Dat")
#define DOWNLOAD_TREND_THREAD_DATA_FILE                 _T("Thread.trd")
#define DOWNLOAD_TREND_FILE_PEAK_PRESSURE               _T("PeakPressure.csv")
#define DOWNLOAD_TREND_FILE_MEAN_AW_PRESSURE            _T("MeanAirwayPressure.csv")
#define DOWNLOAD_TREND_FILE_PEAK_FLOW                   _T("PeakFlow.csv")
#define DOWNLOAD_TREND_FILE_MINUTE_VOLUME               _T("MinuteVolume.csv")
#define DOWNLOAD_TREND_FILE_VOLUME                      _T("Volume.csv")
#define DOWNLOAD_TREND_FILE_FIO2                        _T("FiO2.csv")
#define DOWNLOAD_TREND_FILE_CASE_TEMPERATURE            _T("CaseTemperature.csv")
#define DOWNLOAD_TREND_FILE_BREATH_RATE                 _T("BreathRate.csv")
#define DOWNLOAD_TREND_FILE_BASELINE_PRESSURE           _T("BaselinePressure.csv")
#define DOWNLOAD_TREND_FILE_INT_BATTERY_LEVEL           _T("InternalBattery.csv")
#define DOWNLOAD_TREND_FILE_EXT_BATTERY_LEVEL           _T("ExternalBattery.csv")
#define DOWNLOAD_TREND_FILE_INT_BATTERY_TEMP            _T("InternalBatteryTemperature.csv")
#define DOWNLOAD_TREND_FILE_EXT_BATTERY_TEMP            _T("ExternalBatteryTemperature.csv")
#define DOWNLOAD_EVENT_TEXT_FILE                        _T("EventHistoryData.csv");

#define SRCFLASH_PHOTO_PATH								_T("E:\\")
#define UPLOAD_PHOTO_PATH								_T(".\\Photos\\")
#define DEBUG_TIMING_LOG_FILE			                _T("ThreadTiming.csv")


#endif

