#ifndef SmConstants_HH
#define SmConstants_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1996, Puritan-Bennett Corporation
//=====================================================================

// ============================= C L A S S   D E S C R I P T I O N ====
// Class:  SmConstants - Service Mode global constants
//---------------------------------------------------------------------
//@ Version-Information
// @(#) $Header:   /840/Baseline/Service-Mode/vcssrc/SmConstants.hhv   25.0.4.0   19 Nov 2013 14:23:44   pvcs  $
//
//@ Modification-Log
//
//  Revision: 012  By: rhj    Date: 14-June-2010    SCR Number: 6436
//  Project:  PROX
//  Description:
//      Added PROX resistance and compliance.
// 
//  Revision: 011  By: quf    Date: 03-Jan-2001     DR Number: 5832
//  Project:  Baseline
//  Description:
//      Reduced value of MAX_LIFTOFF_COUNT.
//
//  Revision: 010  By: jja    Date: 31-May-2000     DR Number: 5736
//  Project:  NeoMode
//  Description:
//      Revise HIGH FAIL and HIGH ALERT values for neonatal circuit compliance
//      to be consistent with expected Neonatal values. 
//
//  Revision: 009  By: sah    Date: 30-Jun-1999     DR Number: 5327
//  Project:  NeoMode
//  Description:
//      Incorporation of NeoMode Project requirements.
//
//  Revision: 008  By: quf    Date: 13-Oct-1999     DR Number: 5540
//  Project:  ATC
//  Description:
//      Changed value of COMPRESSOR_FILL_TIME, added constants used for
//      altitude compensation.
//
//  Revision: 007  By:  dosman    Date: 26-Feb-1999    DR Number: DCS 5322
//       Project:  ATC
//       Description:
//	Implemented changes for ATC.
//	Added new constant (see REMOTE_TEST_MAX_COMPLIANCE) 
//	because it is believed that the maximum 
//	compliance needed during circuit pressure test
//	for external/remote test is much more than that needed for EST.
//
//  Revision: 006  By:  quf    Date: 09-Oct-1997    DR Number: DCS 1865
//       Project:  Sigma (840)
//       Description:
//			Added const EV_LOOPBACK_OFFSET.
//
//  Revision: 005  By:  quf    Date: 08-Oct-1997    DR Number: DCS 2407
//       Project:  Sigma (840)
//       Description:
//			Decreased adult and ped low compliance Failure and Alert
//			criteria by 10% to account for adiabatic compression.
//
//  Revision: 004  By:  quf    Date: 25-Sep-1997    DR Number: DCS 2410
//       Project:  Sigma (840)
//       Description:
//			Added ATM_PRESSURE_ERROR_MMHG, CMH20_PER_MMHG.
//
//  Revision: 003  By:  quf    Date: 06-Jun-1997    DR Number: DCS 1974
//       Project:  Sigma (840)
//       Description:
//            Changed exh valve gain constant from .985 to .988 per
//            the control spec.
//
//  Revision: 002  By:  quf    Date: 22-May-1997    DR Number: DCS 2131
//       Project:  Sigma (840)
//       Description:
//            Modification-Log fix only.
//
//  Revision: 001  By:  quf    Date:  13-Feb-1997    DR Number: 
//       Project:  Sigma (R8027)
//       Description:
//             Initial version (Integration baseline).
//
//=====================================================================

#include "Sigma.hh"

// ***** insp/exh pressure transducer constants *****
//@ Constant: REL_PRESSURE_ERROR
// pressure transducer relative error
const Real32 REL_PRESSURE_ERROR = 0.031;

//@ Constant: ABS_PRESSURE_ERROR
// pressure transducer absolute error
const Real32 ABS_PRESSURE_ERROR = 0.35;

//@ Constant: MAX_ZERO_COUNT
// pressure transducer maximum DAC count @ atmospheric pressure
// TODO E600 MS Revisit when P3.0 units come in
const Uint16 MAX_ZERO_COUNT = 2200;

//@ Constant: MIN_ZERO_COUNT
// pressure transducer minimum DAC count @ atmospheric pressure
// TODO E600 MS Revisit when P3.0 units come in
const Uint16 MIN_ZERO_COUNT = 1500;


// ***** atmospheric pressure transducer constants *****
//@ Constant: ATM_PRESSURE_ERROR_MMHG
// atmospheric pressure transducer error in mmHg
const Real32 ATM_PRESSURE_ERROR_MMHG = 44.0;

// minimum atmospheric pressure
const Real32 MIN_ATM_PRESSURE = 570.00F ;   // cmH2O = 8.1 psia

// maximum atmospheric pressure
const Real32 MAX_ATM_PRESSURE = 1160.00F;   // cmH2O = 16.5 psia


// ***** flow sensor constants *****
//@ Constant: MAX_INSP_FLOW_SENSOR_0_OFFSET
// maximum inspiratory flow sensor offset
const Real32 MAX_INSP_FLOW_SENSOR_0_OFFSET = 0.05; // lpm

//@ Constant: MAX_EXH_FLOW_SENSOR_0_OFFSET
// maximum expiratory flow sensor offset
const Real32 MAX_EXH_FLOW_SENSOR_0_OFFSET = 0.1;   // lpm

//@ Constant: MAX_INSP_FLOW_PSOL_OFF
// maximum inspiratory flow sensor reading, PSOL closed
const Real32 MAX_INSP_FLOW_PSOL_OFF = 0.153;  // lpm

//@ Constant: INSP_FLOW_ERROR_AT_1_LPM
// inspiratory flow sensor relative error @ 1 lpm
const Real32 INSP_FLOW_ERROR_AT_1_LPM = 0.132;

//@ Constant: EXP_FLOW_ERROR_AT_1_LPM
// expiratory flow sensor relative error @ 1 lpm
const Real32 EXP_FLOW_ERROR_AT_1_LPM = 0.211;

//@ Constant: INSP_FLOW_ERROR_AT_5_LPM
// inspiratory flow sensor relative error @ 5 lpm
const Real32 INSP_FLOW_ERROR_AT_5_LPM = 0.068;

//@ Constant: EXP_FLOW_ERROR_AT_5_LPM
// expiratory flow sensor relative error @ 5 lpm
const Real32 EXP_FLOW_ERROR_AT_5_LPM = 0.083;

//@ Constant: INSP_FLOW_ERROR_AT_10_LPM
// inspiratory flow sensor relative error @ 10 lpm
const Real32 INSP_FLOW_ERROR_AT_10_LPM = 0.060;

//@ Constant: EXP_FLOW_ERROR_AT_10_LPM
// expiratory flow sensor relative error @  10 lpm
const Real32 EXP_FLOW_ERROR_AT_10_LPM = 0.067;

//@ Constant: INSP_FLOW_ERROR_AT_60_LPM
// inspiratory flow sensor relative error @ 60 lpm
const Real32 INSP_FLOW_ERROR_AT_60_LPM = 0.053;

//@ Constant: EXP_FLOW_ERROR_AT_60_LPM
// expiratory flow sensor relative error @ 60 lpm
const Real32 EXP_FLOW_ERROR_AT_60_LPM = 0.054;

//@ Constant: INSP_FLOW_ERROR_AT_120_LPM
// inspiratory flow sensor relative error @ 120 lpm
const Real32 INSP_FLOW_ERROR_AT_120_LPM = 0.053; 

//@ Constant: EXP_FLOW_ERROR_AT_120_LPM
// expiratory flow sensor relative error @ 120 lpm
const Real32 EXP_FLOW_ERROR_AT_120_LPM = 0.052;


// ***** PSOL constants *****
//@ Constant: PSOL_COUNTS_TO_MA
// conversion factor for PSOL DAC counts to current in mA
const Real32 PSOL_COUNTS_TO_MA = 730.0 / 4095.0;  // mA per DAC count

//@ Constant: LIFTOFF_FLOW
// flow threshold for PSOL liftoff calibration
const Real32 LIFTOFF_FLOW = 0.3;  // lpm

//@ Constant: LIFTOFF_STARTING_COUNT
// initial PSOL DAC count for PSOL liftoff calibration
const Uint16 LIFTOFF_STARTING_COUNT = 448;

//@ Constant: MAX_LIFTOFF_COUNT
// maximum PSOL DAC count for PSOL liftoff calibration
const Uint16 MAX_LIFTOFF_COUNT = 909;


// ***** exhalation valve constants *****
//@ Constant: EV_P5_REPEATABILITY
// EV pressure repeatability @ 5 lpm flow
const Real32 EV_P5_REPEATABILITY = 0.15;  // cmH2O

//@ Constant: MIN_EV_MAGNET_TEMP
// minimum EV magnet temperature
const Real32 MIN_EV_MAGNET_TEMP = 10.0;  // deg C

//@ Constant: MAX_EV_MAGNET_TEMP
// maximum EV magnet temperature
const Real32 MAX_EV_MAGNET_TEMP = 100.0;  // deg C

//@ Constant: MIN_DAMPING_GAIN
// minimum EV damping gain
const Uint16 MIN_DAMPING_GAIN = 26;  // DAC counts

//@ Constant: EV_PRESSURE_CONVERSION_MULTIPLIER
// multiplier to convert from P5 to P0.1
const Real32 EV_PRESSURE_CONVERSION_MULTIPLIER = 0.988;

//@ Constant: EV_PRESSURE_CONVERSION_OFFSET
// offset to convert from P5 to P0.1
const Real32 EV_PRESSURE_CONVERSION_OFFSET = 0.68;

//@ Constant: EV_LOOPBACK_OFFSET
// max offset of EV loopback current
const Real32 EV_LOOPBACK_OFFSET = 20.0;  // mA


// ***** compliance constants *****
//@ Constant: EST_MAX_COMPLIANCE
// maximum compliance for EST 
// This constant was made larger than might be expected
// in order to take into account the increased compliance
// that is assumed needed for the remote external test setup.
// However, it might not be enough for the remote/external test setup (VIPER), 
// so a separate constant, REMOTE_TEST_MAX_COMPLIANCE, 
// can be used explicitly when desired.
const Real32 EST_MAX_COMPLIANCE =               2.0;  // ml per cmH2O

//@ Constant: REMOTE_TEST_MAX_COMPLIANCE
// maximum compliance for remote external tests.
const Real32 REMOTE_TEST_MAX_COMPLIANCE =               3.0;  // ml per cmH2O

//@ Constant: ADULT_COMPLIANCE_FAILURE_HIGH
// high compliance failure threshold, adult circuit
const Real32 ADULT_COMPLIANCE_FAILURE_HIGH =	12.0; // ml per cmH2O

//@ Constant: PED_COMPLIANCE_FAILURE_HIGH
// high compliance failure threshold, pediatric circuit
const Real32 PED_COMPLIANCE_FAILURE_HIGH =		9.0;  // ml per cmH2O

//@ Constant: NEO_COMPLIANCE_FAILURE_HIGH
// high compliance failure threshold, neonatal circuit
const Real32 NEO_COMPLIANCE_FAILURE_HIGH =	   3.0;  // ml per cmH2O

//@ Constant: PROX_NEO_COMPLIANCE_FAILURE_HIGH
// high compliance failure threshold with PROX, neonatal circuit
const Real32 PROX_NEO_COMPLIANCE_FAILURE_HIGH =	3.3;  // ml per cmH2O

//@ Constant: ADULT_COMPLIANCE_ALERT_HIGH
// high compliance alert threshold, adult circuit
const Real32 ADULT_COMPLIANCE_ALERT_HIGH =		6.0;  // ml per cmH2O

//@ Constant: PED_COMPLIANCE_ALERT_HIGH
// high compliance alert threshold, pediatric circuit
const Real32 PED_COMPLIANCE_ALERT_HIGH =		4.5;  // ml per cmH2O

//@ Constant: NEO_COMPLIANCE_ALERT_HIGH
// high compliance alert threshold, neonatal circuit
const Real32 NEO_COMPLIANCE_ALERT_HIGH =	 	2.5;  // ml per cmH2O

//@ Constant: PROX_NEO_COMPLIANCE_ALERT_HIGH
// high compliance alert threshold with PROX, neonatal circuit
const Real32 PROX_NEO_COMPLIANCE_ALERT_HIGH =	2.7;  // ml per cmH2O

//@ Constant: ADULT_COMPLIANCE_FAILURE_LOW
// low compliance failure threshold, adult circuit
// TODO E600 MS This value was changed because it has not yet been determined
// whether PB880 will use an expiratory filter or not. Having an exhlation fileter
// signifcantly affects the value of compliance since more volume needs to be delivered
// to reach the required pressure
const Real32 ADULT_COMPLIANCE_FAILURE_LOW =		0.5; // ml per cmH2O
//const Real32 ADULT_COMPLIANCE_FAILURE_LOW =		1.05; // ml per cmH2O

//@ Constant: PED_COMPLIANCE_FAILURE_LOW
// low compliance failure threshold, pediatric circuit
// TODO E600 MS This value was changed because it has not yet been determined
// whether PB880 will use an expiratory filter or not. Having an exhlation fileter
// signifcantly affects the value of compliance since more volume needs to be delivered
// to reach the required pressure
const Real32 PED_COMPLIANCE_FAILURE_LOW =		0.5; // ml per cmH2O
//const Real32 PED_COMPLIANCE_FAILURE_LOW =		1.05; // ml per cmH2O

//@ Constant: NEO_COMPLIANCE_FAILURE_LOW
// high compliance alert threshold, neonatal circuit
const Real32 NEO_COMPLIANCE_FAILURE_LOW =	 	0.25;  // ml per cmH2O

//@ Constant: ADULT_COMPLIANCE_ALERT_LOW
// low compliance alert threshold, adult circuit

// TODO E600 MS This value was changed because it has not yet been determined
// whether PB880 will use an expiratory filter or not. Having an exhlation fileter
// signifcantly affects the value of compliance since more volume needs to be delivered
// to reach the required pressure
const Real32 ADULT_COMPLIANCE_ALERT_LOW =		1.00; // ml per cmH2O
//const Real32 ADULT_COMPLIANCE_ALERT_LOW =		1.56; // ml per cmH2O

//@ Constant: PED_COMPLIANCE_ALERT_LOW
// low compliance alert threshold, pediatric circuit
// TODO E600 MS This value was changed because it has not yet been determined
// whether PB880 will use an expiratory filter or not. Having an exhlation fileter
// signifcantly affects the value of compliance since more volume needs to be delivered
// to reach the required pressure
const Real32 PED_COMPLIANCE_ALERT_LOW =			1.00; // ml per cmH2O
//const Real32 PED_COMPLIANCE_ALERT_LOW =			1.34; // ml per cmH2O

//@ Constant: NEO_COMPLIANCE_ALERT_LOW
// high compliance alert threshold, neonatal circuit
const Real32 NEO_COMPLIANCE_ALERT_LOW =		 	0.60;  // ml per cmH2O
	

// ***** compressor specs *****
//@ Constant: COMPRESSOR_FILL_TIME
// maximum time required to fill the compressor accumulator
const Uint32 COMPRESSOR_FILL_TIME = 18000;  // msec

//@ Constant: COMPRESSOR_BLEED_TIME
// maximum time to bleed the compressor accumulator
const Uint32 COMPRESSOR_BLEED_TIME = 10000;  // msec


// ***** resistance characterization constants *****
//@ Constant: ADULT_ALERT_MIN_PEAK_FLOW
// adult alert-level minimum peak flow
const Real32 ADULT_ALERT_MIN_PEAK_FLOW =   80.0;  // lpm

//@ Constant: ADULT_FAILURE_MIN_PEAK_FLOW
// adult failure-level minimum peak flow
const Real32 ADULT_FAILURE_MIN_PEAK_FLOW = 60.0;  // lpm

//@ Constant: PED_ALERT_MIN_PEAK_FLOW
// pediatric alert-level minimum peak flow
const Real32 PED_ALERT_MIN_PEAK_FLOW =   80.0;  // lpm

//@ Constant: PED_FAILURE_MIN_PEAK_FLOW
// pediatric failure-level minimum peak flow
const Real32 PED_FAILURE_MIN_PEAK_FLOW = 60.0;  // lpm

//@ Constant: NEO_ALERT_MIN_PEAK_FLOW
// neonatal alert-level minimum peak flow
const Real32 NEO_ALERT_MIN_PEAK_FLOW =   0.0;  // no alert for neonate...   failure only

//@ Constant: NEO_FAILURE_MIN_PEAK_FLOW
// neonatal failure-level minimum peak flow
const Real32 NEO_FAILURE_MIN_PEAK_FLOW = 20.0;  // lpm


// ***** misc constants *****
//@ Constant: MIN_DAC_COUNT
// minimum DAC count
const Uint16 MIN_DAC_COUNT = 0;

//@ Constant: PSI_TO_CMH20_FACTOR
// PSI to cmH2O conversion factor
const Real32 PSI_TO_CMH20_FACTOR = 70.31;  // cmH2O per PSI

//@ Constant: CMH2O_PER_MMHG
// mmHg to cmH2O conversion factor
const Real32 CMH2O_PER_MMHG = 1.359;  // cmH2O per mmHg

// constants for volume or flow conversion from slpm to btps
const Real32 VAPOR_PRESS_BTPS = 64.037F ; 	// cmH2O
const Real32 STD_ATM_PRESSURE = 1033.66F ; 	// cmH2O
const Real32 TEMPERATURE_SLPM = 294.25F ; 	// K
const Real32 TEMPERATURE_BTPS = 310.15F ; 	// K


#endif // SmConstants_HH 
