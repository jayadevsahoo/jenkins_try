#include "stdafx.h"
//----------------------------------------------------------------------------
//            Copyright (c) 2010 Covidien, Inc.
//
// This software is copyrighted by and is the sole property of Covidien. This
// is a proprietary work to which Covidien claims exclusive right.  No part
// of this work may be used, disclosed, reproduced, stored in an information
// retrieval system, or transmitted by any means, electronic, mechanical,
// photocopying, recording, or otherwise without the prior written permission
// of Covidien.
//----------------------------------------------------------------------------
/// @file MemoryHandle.cc

#include "MemoryHandle.hh"
#include "UtilityClassId.hh"
#include "CriticalSection.hh"
#include <string.h>


MemPtr MemoryHandle::m_PointerArray[MAX_HANDLES];
Boolean MemoryHandle::m_isInitialized = FALSE;

//-----------------------------------------------------------------------------
// MemoryHandle::CreateHandle
// The API checks if the input pointer is already stored in the internal
// array. If so, it returns the index. If not, it stores the pointer in the 
// first available empty slot in the array and returns its index. This implementation
// allows storing upto 64K active pointers at any time (unlike PB980 where it allowed
// storage of only the first 64K pointers, regardless of if they are in use)
//-----------------------------------------------------------------------------
MemHandle  MemoryHandle::CreateHandle( MemPtr memPtr )
{
	MemHandle allottedHandle = MAX_HANDLES;
	MemHandle emptySlot = MAX_HANDLES;

	CriticalSection cs(MEMORY_HANDLE_MT);
	//Check if this is the first time someone is requesting a
	//handle. If so, initialize the array first.
	if(m_isInitialized == FALSE)
	{
		memset(m_PointerArray, 0, sizeof(m_PointerArray));
		m_isInitialized = TRUE;
	}

	//Search in the array, and see if the input pointer is already
	//stored. While searching also note down the first available 
	//empty slot (this is where the pointer will be inserted if
	//it is not already stored)
	for(Uint32 i=0; i<MAX_HANDLES; i++)
	{
		if(m_PointerArray[i] == memPtr)
		{
			allottedHandle = (MemHandle) i;
			break;
		}
		else if(emptySlot == MAX_HANDLES && m_PointerArray[i] == NULL)
			emptySlot = (MemHandle)i;
	}

	if(allottedHandle == MAX_HANDLES && emptySlot != MAX_HANDLES)
	{
		//The pointer is not already stored, and we do have an empty
		//slot. So store it in the available slot.
		m_PointerArray[emptySlot] = memPtr;
		allottedHandle = emptySlot;
	}

	return allottedHandle;
}

//-----------------------------------------------------------------------------
// GetMemoryPtr
//-----------------------------------------------------------------------------
MemPtr MemoryHandle::GetMemoryPtr( MemHandle handle )
{
	CriticalSection cs(MEMORY_HANDLE_MT);

	CLASS_ASSERTION( handle < MAX_HANDLES );

	//return the memory pointer associated with the handle.
	return m_PointerArray[handle];
}


void MemoryHandle::DeleteMemoryPtr( MemHandle handle )
{
	CLASS_ASSERTION( handle < MAX_HANDLES );
	CriticalSection cs(MEMORY_HANDLE_MT);

	//Just set the memory pointer associated with the handle to NULL.
	//This allows reusing the slot when a new pointer is inserted.
	m_PointerArray[handle] = NULL;
}

void MemoryHandle::SoftFault(const SoftFaultID softFaultID,
								const Uint32 lineNumber,
								const char *pFileName,
								const char *pPredicate)
{
  FaultHandler::SoftFault(softFaultID, UTILITIES,
			  MemoryHandle::MEMORY_HANDLE, 
			  lineNumber, pFileName, pPredicate);
}
