//                           
#ifndef ComputedInspPauseDataPacket_HH
#define ComputedInspPauseDataPacket_HH

//=====================================================================
// This is a proprietary work to which Puritan-Bennett Corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================

//====================================================================
// Struct : ComputedInspPauseDataPacket - structure definition of patient
//   data packet containing static compliance and resistance computed after
//   an inspiratory pause.
//
//---------------------------------------------------------------------
//@ Version
// @(#) $Header:   /840/Baseline/Patient-Data/vcssrc/ComputedInspPauseDataPacket.hhv   25.0.4.0   19 Nov 2013 14:17:58   pvcs  $
//
//@ Modification-Log
//
//  Revision: 001  By:  hct    Date:  03-FEB-98    DR Number: 
//	Project:  Sigma (R8027)
//	Description:
//		Initial version for BiLevel.
//
//====================================================================

#include "Sigma.hh"
#include "PauseTypes.hh"
#include "SocketWrap.hh"

// @Type: ComputedInspPauseDataPacket
struct   ComputedInspPauseDataPacket
{
	Real32  staticCompliance;
	PauseTypes::ComplianceState staticComplianceValid;
	Real32  staticResistance;
	PauseTypes::ComplianceState staticResistanceValid;

	void convHtoN()
	{
		SocketWrap sock;
		staticCompliance = sock.HTONF(staticCompliance);
		staticComplianceValid = (PauseTypes::ComplianceState) sock.HTONL((Uint32)staticComplianceValid);
		staticResistance = sock.HTONF(staticResistance);
		staticResistanceValid = (PauseTypes::ComplianceState) sock.HTONL((Uint32)staticResistanceValid);
	}

	void convNtoH()
	{
		SocketWrap sock;
		staticCompliance = sock.NTOHF(staticCompliance);
		staticComplianceValid = (PauseTypes::ComplianceState) sock.NTOHL((Uint32)staticComplianceValid);
		staticResistance = sock.NTOHF(staticResistance);
		staticResistanceValid = (PauseTypes::ComplianceState) sock.NTOHL((Uint32)staticResistanceValid);
	}
};

#endif // ComputedInspPauseDataPacket_HH 

