#include "stdafx.h"
//=====================================================================
// This is a proprietary work to which Puritan-Bennett corporation of
// California claims exclusive right.  No part of this work may be used,
// disclosed, reproduced, stored in an information retrieval system, or
// transmitted by any means, electronic, mechanical, photocopying,
// recording, or otherwise without the prior written permission of
// Puritan-Bennett Corporation of California.
//
//            Copyright (c) 1995, Puritan-Bennett Corporation
//=====================================================================


// ============================= C L A S S   D E S C R I P T I O N ====
//@ Class: SimvScheduler - the active BreathPhaseScheduler when the
//  current mode is SIMV.
//---------------------------------------------------------------------
//@ Interface-Description
// This class is responsible for scheduling breath phases during SIMV mode.
// The class is also responsible for relinquishing control to the next valid
// scheduler and for assuming control when SIMV mode is requested.  SIMV mode
// can be requested by the operator or upon emergency mode recovery and
// power-up sequence, when transition to the previous mode is required. The
// Simv scheduler has a cycle that is determined by the respiratory rate setting.
// This scheduler is attempting to deliver a mandatory breath every simv
// cycle. To implement this cyclical behavior, the SimvScheduler is defined as
// a client of an IntervalTimer.  Note that mode transition, as a response for
// setting change, occurs once the new mode setting is accepted (by the
// operator).  This scheduler is responsible for
// enabling valid mode triggers for SIMV mode and for enabling breath triggers
// that are not under the breath phase direct control. The SimvScheduler is
// responsible for handling user events like 100% O2, manual inspiration
// request, etc. and for handling setting change events like rate and mode
// changes.
//---------------------------------------------------------------------
//@ Rationale
// This class encapsulates all the breathing rules specified for SIMV mode.
//---------------------------------------------------------------------
//@ Implementation-Description
// The class implements methods for breath and mode transitions and for
// breath data updates. The Simv scheduler is responsible for:
// - Determining the next breath when a breath trigger becomes active.
// - Initiating the activities required for the proper start of a new breath:
//    phase in settings, update breath record, set breath triggers,
//    enable breath triggers, and phase out the previous breath phase.
// - Relinquishing control when a mode trigger becomes active.
//    Activities while control is relinquished include:
//    disable out of date breath triggers, determine which is the next
//    valid scheduler, enable breath triggers for the next scheduler,
//    and instruct the next scheduler to take control.
// - Taking control when the previous scheduler relinquishing control.
//    Activities while taking control include:
//    register self with the BreathPhaseScheduler object,  and activate
//    the mode trigger list.
//---------------------------------------------------------------------
//@ Fault-Handling
// n/a
//---------------------------------------------------------------------
//@ Restrictions
//    Only one instance of this class may be created.
//---------------------------------------------------------------------
//@ Invariants
//---------------------------------------------------------------------
//@ End-Preamble
//
//@ Version-Information
// @(#) $Header:   /840/Baseline/Breath-Delivery/vcssrc/SimvScheduler.ccv   25.0.4.0   19 Nov 2013 14:00:08   pvcs  $
//
//@ Modification-Log
//
//  Revision: 031   By: rhj    Date: 05-Jan-2009    SCR Number: 6451
//  Project:  840S
//  Description:
//      Added missing net flow backup inspiratory triggers.
//
//  Revision: 030   By: rhj    Date: 07-Nov-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Added net flow backup inspiratory trigger.
//
//  Revision: 029   By: gdc    Date: 24-Sep-2008    SCR Number: 6435
//  Project:  840S
//  Description:
//      Modified to support neonatal non-invasive CPAP.
//
//  Revision: 028   By: gdc   Date:  25-Oct-2006    DR Number: 6293
//  Project:  RESPM
//  Description:
//		Changed to use PEEP recovery breath where applicable to restart
//      breath timers after an RM maneuver.
//
//  Revision: 027   By: gdc   Date:  26-Oct-2006    DR Number: 6289
//  Project:  RESPM
//  Description:
//		Corrected wrong scheduler ID in newBreathRecord call. Changed
//		per code review.
//
//  Revision: 036   By: gdc   Date:  25-Oct-2006    DR Number: 6296
//  Project:  RESPM
//  Description:
//		Changed to pressure expiratory trigger to cycle out of
// 		vital capacity inspiratory phase.
//
//  Revision: 035   By: gdc   Date:  13-Oct-2006    DR Number: 6236
//  Project:  RESPM
//  Description:
//		RESPM project-related changes:
//		Add RM maneuver handling.
//
//  Revision: 034   By: syw   Date:  24-May-2000    DR Number: 5313
//  Project:  PAV
//  Description:
//		PAV project-related changes:
//		Added pav pause handle during mode transition.
//
//  Revision: 033  By:  syw    Date:  08-Nov-2000    DR Number: 5794
//       Project:  VTPC
//       Description:
//			Added requirement tracings.
//
//  Revision: 032  By: jja     Date:  17-May-2000    DR Number: 5755
//  Project:  VTPC
//  Description:
//		VTPC project-related changes:
//		Added VTPC handle.
//
//  Revision: 031  By:  healey    Date:  14-Jan-99    DR Number: DCS 5322, 5367
//       Project:  ATC
//       Description:
//          Added checking for TC support type to set up corresponding phase
//          Added checking for PAUSE_PRESS trigger
//
//  Revision: 030  By: sah     Date:  12-Jan-1999    DR Number: 5321
//  Project:  ATC
//  Description:
//      Changed 'SoftFault()' method to a non-inlined method.
//          ATC initial revision
//
//  Revision: 029  By:  iv    Date:   13-Oct-1998    DR Number: DCS 5206
//       Project:  BiLevel
//       Description:
//			Removed the extension of the apnea interval for inspiratory
//          pause phase in determineBreathPhase().
//
//  Revision: 028  By:  syw    Date:  30-Sep-1998    DR Number: DCS 5188
//       Project:  Sigma (R8027)
//       Description:
//			Fixed the "logical or" to "Bitwised or" when presenting the
//			error code.
//
//  Revision: 027  By:  syw    Date:  17-Jul-1998    DR Number: DCS 5120
//       Project:  Sigma (R8027)
//       Description:
//			Call RPeep.setPeepChange if breath is to be Peep Recovery.
//
//  Revision: 026  By:  iv    Date:  31-Jan-1998    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Bilevel initial version.
//             Added handling for Inspiratory Pause event and
//             Inspiratoty Pause phase.
//
//  Revision: 025  By: iv    Date:  29-Oct-1997   DR Number: DCS 2594
//  	Project:  Sigma (R8027)
//		Description:
//          In determineBreathPhase(), added a check for the RPeepRecoveryMandInspTrigger
//          when state is EXHALATION and simvInterval_ is MANDATORY. This case is possible
//          on transition from disconnect to simv or on transition from disconnect to ac
//          and then to simv.  Also, changed class assertions to aux class assertions to add
//          more debugging information.
//
//  Revision: 024  By: iv    Date:  18-Jul-1997   DR Number: DCS 2298
//  	Project:  Sigma (R8027)
//		Description:
//          Added RPeepRecoveryMandInspTrigger.
//
//  Revision: 023  By: syw   Date:  23-May-1997    DR Number: 1656
//  	Project:  Sigma (840)
//		Description:
//			Capitalize all global references per coding standard.
//
//  Revision: 022  By:  iv    Date:  07-Feb-1997    DR Number: None
//       Project:  Sigma (840)
//       Description:
//             Rework per Breath Delivery formal code review.
//
//  Revision: 021  By:  iv    Date:  08-Jan-1996    DR Number: DCS 1339
//       Project:  Sigma (R8027)
//       Description:
//             Added a check during EXPIRATORY_PAUSE for HighCircuitPressureInspTrigger.
//
//  Revision: 020  By:  iv    Date:  18-Dec-1996    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Rework per formal code review.
//
//  Revision: 019  By:  iv    Date:  04-Dec-1996    DR Number: DCS 1597
//       Project:  Sigma (R8027)
//       Description:
//             Added isRateAdjustmentRequired_ flag to handle rate changes
//             during the simv mandatory intervals.
//
//  Revision: 018  By:  iv    Date:  03-Dec-1996    DR Number: DCS 1608, 1541
//       Project:  Sigma (R8027)
//       Description:
//             determineBreathPhase(), case = NON_BREATHING : eliminate assertion
//             and add initialization for peep recovery state and simv data members.
//             Prepare code for inspections.
//
//  Revision: 017  By:  iv    Date:  15-Nov-1996    DR Number: DCS 1559
//       Project:  Sigma (R8027)
//       Description:
//             Replaced safe class assertion with class assertion in constructor.
//
//  Revision: 016  By:  iv    Date:  13-Nov-1996    DR Number: DCS 1525, 1542
//       Project:  Sigma (R8027)
//       Description:
//             Fixed a problem that was introduced with the last change.
//
//  Revision: 015  By:  iv    Date:  06-Oct-1996    DR Number: DCS 1525
//       Project:  Sigma (R8027)
//       Description:
//             Changed startInspiration() to consider rateChanges, and if necessary, to
//             extend the simv cycle time so that the next breath won't be stacked
//             on top of the one just started.
//
//  Revision: 014  By:  iv    Date:  06-Oct-1996    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Deleted class assertion in determineBreathPhase() for NON_BREATHING
//             case.
//
//  Revision: 013  By:  sp    Date:  26-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//             Changed PeepRecoveryTrigger to PeepRecoveryInspTrigger.
//
//  Revision: 012 By:  iv   Date:   09-Sep-1996    DR Number: DCS 10035
//       Project:  Sigma (R8027)
//       Description:
//               Added a check for peep recovery trigger in determineBreathPhase(),
//             and disable the trigger on relinquishControl().
//
//  Revision: 011 By:  iv   Date:   13-Aug-1996    DR Number: DCS 1218
//       Project:  Sigma (R8027)
//       Description:
//               Changed the range of SettingId values as checked in a setting change
//             call back assertion.
//
//  Revision: 010 By:  iv   Date:   02-Aug-1996    DR Number: DCS 10035, 1094, 10028, 712
//       Project:  Sigma (R8027)
//       Description:
//             Incorporated PEEP recovery after OSC and Powerup.
//             Fixed an assertion problem in timeupHappened(), fixed a
//             timing problem that caused breath stacking.
//
//  Revision: 009 By:  iv   Date:   21-Jun-1996    DR Number: DCS 1127
//       Project:  Sigma (R8027)
//       Description:
//             Initialized isFirstBreath_ to FALSE on takeControl().
//
//  Revision: 008 By:  iv   Date:   21-Jun-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Added a Boolean isPeepRecovery to newBreathRecord(...) call.
//
//  Revision: 007 By:  iv   Date:   04-Jun-1996    DR Number: DCS 713, 804
//       Project:  Sigma (R8027)
//       Description:
//             Fixed call to base class takeControl() from takeControl().
//             Eliminated the call from timeupHappened() to the method
//             adjustIntervalForRateChange_(), replaced it with direct setting
//             of the simv cycle timer and insp timer.
//             Changed the method adjustIntervalForRateChange_() to consider
//             Spontaneous or Mandatory intervals.
//
//  Revision: 006 By:  iv   Date:   07-May-1996    DR Number: DCS 967
//       Project:  Sigma (R8027)
//       Description:
//             Changed takeControl(), determineBreathPhase(), and
//             startInspiration_() methods - to allow for a PEEP recovery
//             breath phase to take place on transitions from disconnect,
//             standby, and SVO.
//
//  Revision: 005 By:  iv   Date:   15-Apr-1996    DR Number:  DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Eliminated O2_MONITOR_CALIBRATE case in reportEventStatus_().
//
//  Revision: 004 By:  iv   Date:   15-Feb-1996    DR Number: DCS 674
//       Project:  Sigma (R8027)
//       Description:
//             Removed #ifndef directives surrounding #include directives.
//
//  Revision: 003 By:  kam   Date:  06-Nov-1995    DR Number:  DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated SST_CONFIRMATION case of reportEventStatus_() and
//             added O2_MONITOR_CALIBRATE.  Added call to base class
//             takeControl() from takeControl().
//
//  Revision: 002 By:  kam   Date:  25-Sep-1995    DR Number: DCS 600
//       Project:  Sigma (R8027)
//       Description:
//             Updated for interface with Alarm-Analysis subsystem
//             and for BD-GUI user event request and event status interface.
//
//  Revision: 001  By:  iv    Date:  10-Oct-1994    DR Number: None
//       Project:  Sigma (R8027)
//       Description:
//             Initial version
//
//=====================================================================

#include "SimvScheduler.hh"

#include "BreathMiscRefs.hh"
#include "PhaseRefs.hh"
#include "TriggersRefs.hh"
#include "ModeTriggerRefs.hh"
#include "SchedulerRefs.hh"
#include "BdDiscreteValues.hh"
#include "BD_IO_Devices.hh"

//@ Usage-Classes
#include "BreathSet.hh"
#include "BreathTriggerMediator.hh"
#include "UiEvent.hh"
#include "BdSystemStateHandler.hh"
#include "ModeTriggerMediator.hh"
#include "BreathTrigger.hh"
#include "ModeTrigger.hh"
#include "TimerBreathTrigger.hh"
#include "ApneaInterval.hh"
#include "IntervalTimer.hh"
#include "O2Mixture.hh"
#include "Peep.hh"
#include "PendingContextHandle.hh"
#include "PhasedInContextHandle.hh"
#include "PhaseInEvent.hh"
#include "BreathPhase.hh"
#include "BiLevelScheduler.hh"
#include "InspPauseManeuver.hh"
#include "ManeuverRefs.hh"
#include "VolumeTargetedManager.hh"
#include "NifManeuver.hh"
#include "P100Maneuver.hh"
#include "P100PausePhase.hh"
#include "VitalCapacityManeuver.hh"
#include "PressureInspTrigger.hh"
#include "SimvTimeInspTrigger.hh"
#include "AsapInspTrigger.hh"
#include "VcmPsvPhase.hh"
#include "NifPausePhase.hh"
#include "ManeuverTimes.hh"
//#include "Ostream.hh"

#ifdef INTEGRATION_TEST_ENABLE
#if defined (SIGMA_BD_CPU)
#include "EventManager.h"
#endif // SIGMA_BD_CPU
#endif // INTEGRATION_TEST_ENABLE

//@ End-Usage

//@ Code...

//@ Constant MAND_INTERVAL_FRACTION
// $[04237] The Simv mandatory interval as a fraction of the entire
// Simv cycle.
static const Real32 MAND_INTERVAL_FRACTION = 0.6F;

//@ Constant MAX_MANDATORY_INTERVAL
// $[04237] The maximum mandatory interval allowed
static const Int32 MAX_MANDATORY_INTERVAL = 10000;

//@ Constant: MAX_EXP_PAUSE_INTERVAL
// max time for expiratory pause in msec
#ifdef E600_840_TEMP_REMOVED
extern Int32 MAX_EXP_PAUSE_INTERVAL;

//@ Constant: MAX_INSP_PAUSE_INTERVAL
// max time for inspiratory pause in msec
extern Int32 MAX_INSP_PAUSE_INTERVAL;
#endif

//@ Constant: MAX_NIF_MANEUVER_TIME
// max time for NIF maneuver [msec]
extern Int32 MAX_NIF_MANEUVER_TIME;

//@ Constant: MAX_P100_MANEUVER_ACTIVE_TIME
// max time for P100 pause in msec
#ifdef E600_840_TEMP_REMOVED
extern Int32 MAX_P100_MANEUVER_ACTIVE_TIME;
#endif

//=====================================================================
//
//  Public Methods...
//
//=====================================================================

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: SimvScheduler [Constructor]
//
//@ Interface-Description
//        Constructor.
// $[04119] $[04125] $[04130] $[04141]
// The method takes a reference to a IntervalTimer. It calls the base class
// constructors and initializes some data members.
// The list of valid mode triggers, used by the mode trigger mediator is
// initialized as well. The triggers are ordered based on priority - the most urgent
// is first. The list also guarantees that when a trigger fires - the triggers
// that follow it do not have to be processed.
//---------------------------------------------------------------------
//@ Implementation-Description
// The constructor takes no arguments. It invokes the base class
// constructor with the SIMV id argument. It initializes the data
// member pModeTriggerList_.
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
// The number of elements on the trigger list is asserted
// to be MAX_NUM_SIMV_TRIGGERS and MAX_NUM_SIMV_TRIGGERS to be less than
// MAX_MODE_TRIGGERS.
//@ End-Method
//=====================================================================
static const Int32 MAX_NUM_SIMV_TRIGGERS = 5;
SimvScheduler::SimvScheduler(IntervalTimer& rTimer) :
    BreathPhaseScheduler(SchedulerId::SIMV),
    TimerTarget(),
    rCycleTimer_(rTimer),
    simvInterval_(NULL_INTERVAL),
    isRateAdjustmentRequired_(FALSE)
{
    // $[TI1]
    CALL_TRACE("SimvScheduler::SimvScheduler(IntervalTimer& rTimer)");

    Int32 ii = 0;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RSvoTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RDisconnectTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&ROcclusionTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RApneaTrigger;
    pModeTriggerList_[ii++] = (ModeTrigger*)&RSettingChangeModeTrigger;
    pModeTriggerList_[ii] = (ModeTrigger*)NULL;

    CLASS_ASSERTION(ii == MAX_NUM_SIMV_TRIGGERS &&
                        MAX_NUM_SIMV_TRIGGERS < MAX_MODE_TRIGGERS);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: ~SimvScheduler [Destructor]
//
//@ Interface-Description
//        Destructor.
//---------------------------------------------------------------------
//@ Implementation-Description
//---------------------------------------------------------------------
//@ PreCondition
//        none
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
SimvScheduler::~SimvScheduler()
{
    CALL_TRACE("SimvScheduler::~SimvScheduler()");
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: determineBreathPhase
//
//@ Interface-Description
// This method accepts a breath trigger as an argument and has no return value.
// It determines which breath phase to start, based on the trigger id,
// the current phase, and the vent settings.
// Once the next breath phase is determined, the following activities take place:
// - phase-in new settings for start of inspiration or start of exhalation,
//   and update the applied O2%, apnea interval and peep value.
// - Update the current breath record with new data.
// - Change the active breath trigger list, disabling the triggers on the
//   current list.
// - Instruct the current breath phase to relinquish control and setup
//   the new breath phase.
//---------------------------------------------------------------------
//@ Implementation-Description
// The activities handled by this method are dependent on the current breath
// phase. The current breath phase is retrieved from the BreathPhase object.
// Before any phase starts, the BreathTriggerMediator is used to reset the
// old phase trigger list, and to set the new phase trigger list,  the old
// phase relinquishes control, and the new phase registers itself with the
// BreathPhase object. New settings are phased in - when relevant, the applied
// O2 percent is updated if necessary, the apnea interval is updated for start
// of exhalation, and the breath record gets updated. Note that the above activities
// are either done in this method or in the method startInspiration_().
// A switch statement is implemented for the various breath phases which are:
//  NON_BREATHING, EXHALATION, INSPIRATION, EXPIRATORY_PAUSE and INSPIRATORY_PAUSE.
// Inspiration begins by calling the method startInspiration_(). Before that method is
// called, the breath type is set (e.g. CONTROL, ASSIST, etc.)
// When current phase is EXHALATION, a pause phase starts if time trigger is
// detected and pause request is pending.  If a peep reduction trigger is
// detected then exhalation (with null inspiration flag) is delivered, followed
// by enabling a asap insp. trigger. Otherwise the breath type  is determined
// and inspiration is delivered.
// When the breath phase is EXPIRATORY PAUSE, the expiratory pause event status is
// set to either COMPLETE or CANCEL, and the simv cycle is extended by the
// pause elapsed time.
// When current phase is INSPIRATION, a pause phase starts if immediate exp
// trigger is detected and pause request is pending. Otherwise exhalation phase
// is delivered. When the breath phase is INSPIRATORY PAUSE, the inspiratory pause event
// status is set to either COMPLETE or CANCEL, and the simv cycle is extended by the
// pause elapsed time.
// When current phase is NON_BREATHING, the breathType is set to SPONT to start peep
// recovery inspiration.
//---------------------------------------------------------------------
//@ PreCondition
// For each case in the switch statement, the expected range of valid
// triggers and/or previous schedulers is checked. The valid range for
// phase type is also checked. For the NON_BREATHING case, the member
// isFirstBreath_ is checked to be true.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
SimvScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)
{

    CALL_TRACE("SimvScheduler::determineBreathPhase(const BreathTrigger& breathTrigger)");

    // the type of breath to communicate to the user
    ::BreathType breathType;

    const Trigger::TriggerId triggerId = breathTrigger.getId();

    // Get the scheduler id from the current breath record. Note that the current
    // breath record stores the id of the scheduler that was active at the time the
    // record was created.
    const SchedulerId::SchedulerIdValue previousSchedulerId =
            (RBreathSet.getCurrentBreathRecord())->getSchedulerId();

    // A pointer to the current breath phase
    BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
    // determine the current phase type (e.g. EXHALATION, INSPIRATION)
    const BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();

	//cout << "P=" << phase << ",I=" << simvInterval_ << ",T=" << triggerId << endl;

    switch (phase)
    {
        case BreathPhaseType::NON_BREATHING:
		{
        // $[TI1]
            // $[04321] Deliver a peep recovery inspiration:
            BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
            simvInterval_ = SimvScheduler::NULL_INTERVAL;
            isFirstBreath_ = FALSE;
            startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
		}
        break;

        case BreathPhaseType::EXHALATION:
		{
        // $[TI2]
			if ( triggerId == Trigger::ARM_MANEUVER )
			{
				if (RNifManeuver.getManeuverState() == PauseTypes::PENDING ||
					RVitalCapacityManeuver.getManeuverState() == PauseTypes::PENDING)
				{
					// $[RM24010] \3\ During the exhalation phase recognize a NIF maneuver as pending (if not already pending), enable the backup pressure trigger but disable pressure and flow triggers.
                    // $[RM12205] When the VC maneuver is armed, only the backup pressure trigger shall be enabled.

					// this disables all triggers including the control breath trigger
					RBreathTriggerMediator.resetTriggerList();
					// retain expiratory triggers list
					RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);
					// enable pressure trigger for NIF breath phase -- all others disabled
					RPressureBackupInspTrigger.enable();
					// stop the cycle timer and breath timer until after the maneuver completes
					rCycleTimer_.stop();
					RSimvTimeInspTrigger.disable();

					// -- still in exhalation as required to activate a NIF/VC maneuver
				}
				else
				{
					// no maneuver pending --- the arm maneuver trigger wasn't disarmed properly
					CLASS_ASSERTION_FAILURE();
				}
			}
			else if ( triggerId == Trigger::DISARM_MANEUVER )
			{
                // $[RM12078] A NIF maneuver shall be cancelled...
                // $[RM12085] An armed Vital Capacity maneuver shall be aborted...
                // $[RM12209] In SIMV, the interval timers shall be reset like the mode was being entered when normal ventilation resumes.

				// restart breath timers by delivering a PEEP recovery breath
				BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
				simvInterval_ = SimvScheduler::NULL_INTERVAL;
				isFirstBreath_ = FALSE;
				startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
			}
			else if ( triggerId == Trigger::PRESSURE_BACKUP_INSP &&
					  RVitalCapacityManeuver.getManeuverState() == PauseTypes::PENDING)
			{
                // $[RM12207] The maneuver shall transition to active at the beginning of the next patient inspiratory effort signaled by the backup pressure trigger.

				// patient effort sensed with vital capacity maneuver pending
				RBreathTriggerMediator.resetTriggerList();
				// set up the trigger list for the VC inspiration
				RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::VCM_INSP_LIST);
				// current breath phase is given a chance to phase-out
				pBreathPhase->relinquishControl(breathTrigger);
				// register a Vital Capacity Maneuver PSV phase:
				BreathPhase::SetCurrentBreathPhase(RVcmPsvPhase, breathTrigger);
				//Create a new breath record
				RBreathSet.newBreathRecord(SchedulerId::SIMV, MandTypeValue::PCV_MAND_TYPE, ::SPONT,
										BreathPhaseType::VITAL_CAPACITY_INSP, FALSE);
				// start the vital capacity inspiratory phase
				BreathPhase::NewBreath() ;
			}
			else if ( triggerId == Trigger::PRESSURE_BACKUP_INSP
					  && RNifManeuver.getManeuverState() == PauseTypes::PENDING )
			{
				// $[RM12207] NIF shall transition to active at the beginning of the next patient inspiratory effort signaled by the backup pressure trigger.

				// this is a patient triggered breath with NIF pause pending
				// NIF pause is triggered on patient effort detected by backup pressure trigger (PEEP-2.0)

				//start NIF maneuver...

				// Reset currently active triggers
				RBreathTriggerMediator.resetTriggerList();
				// Change triggers to those active during an active NIF
				RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::NIF_PAUSE_LIST);
				// current breath phase is given a chance to phase-out
				pBreathPhase->relinquishControl(breathTrigger);
				// register a NIF pause phase:
				BreathPhase::SetCurrentBreathPhase(RNifPausePhase, breathTrigger);
				// update the breath record
				(RBreathSet.getCurrentBreathRecord())->startNifPause();
				// start the pause
				BreathPhase::NewBreath() ;
			}
			else if ( triggerId == Trigger::P100_MANEUVER )
			{
				// $[RM12306] P100 shall transition to active when the P100 inspiratory trigger conditions (see Control Specification) have been met.

				//start P100 maneuver...

				// Reset currently active triggers
				RBreathTriggerMediator.resetTriggerList();
				// Change triggers to those active during an active P100 (same as NIF)
				RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::NIF_PAUSE_LIST);

                // $[RM12308] If the ventilator is in SIMV, the cycle during which the maneuver becomes active (the cycle during which the next scheduled VIM will occur) is extended by the amount of time the maneuver is active.

                // Stop the simv cycle timer until the maneuver completes
                rCycleTimer_.stop();

                // Extend the simv cycle timer by the max P100 pause duration
			    RSimvTimeInspTrigger.extendInterval(MAX_P100_MANEUVER_ACTIVE_TIME);

				// current breath phase is given a chance to phase-out
				pBreathPhase->relinquishControl(breathTrigger);
				// register a P100 pause phase:
				BreathPhase::SetCurrentBreathPhase(RP100PausePhase, breathTrigger);
				// update the breath record
				(RBreathSet.getCurrentBreathRecord())->startP100Pause();
				// start the pause
				BreathPhase::NewBreath() ;
			}
            //$[04243] $[04216] $[04217]
            // Check if time trigger and expiratory pause is requested --
            // start an expiratory pause:

            // check conditions for expiratory pause
            else if( (triggerId == Trigger::SIMV_TIME_INSP) &&
                (RExpiratoryPauseEvent.getEventStatus() == EventData::PENDING) )
            {
             // $[TI2.1]
                //start expiratory pause ....
                // $[04221] for the sake of i:e and expiratory time calculation, the breath
                // record considers the expiratory pause phase as part of the expiratory
                // phase, therefore no new phase update for the breath record is necessary.
                // Reset currently active triggers and setup triggers that are active during
                // pause:
                RBreathTriggerMediator.resetTriggerList();
                RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_PAUSE_LIST);
                // Extend the simv cycle timer by the max pause duration
                rCycleTimer_.extendInterval(MAX_EXP_PAUSE_INTERVAL);
                // notify the user that expiratory pause is active:
                RExpiratoryPauseEvent.setEventStatus(EventData::ACTIVE);

                // current breath phase is given a chance to phase-out
                pBreathPhase->relinquishControl(breathTrigger);

                // register an expiratory pause:
                BreathPhase::SetCurrentBreathPhase((BreathPhase&)RExpiratoryPausePhase,
                                                    breathTrigger);
                // update the breath record
                (RBreathSet.getCurrentBreathRecord())->startExpiratoryPause();
                // start the pause
				BreathPhase::NewBreath() ;
            }
            else if( (triggerId == Trigger::SIMV_TIME_INSP) &&
                      (previousSchedulerId != SchedulerId::SIMV) )
            {
            // $[TI2.2]
                //previous scheduler is not SIMV, therefore the time trigger should have
                // been disabled:
                AUX_CLASS_ASSERTION_FAILURE(previousSchedulerId);
            }
            else if(triggerId == Trigger::PEEP_REDUCTION_EXP)
            {
                // $[TI2.4]
                startExhalation_(breathTrigger, pBreathPhase, TRUE);

                // After the peep reduction (peep low) we should enable the
                // inspiration trigger to start a inspiration.
                ((BreathTrigger&)RAsapInspTrigger).enable();

	        	// On Peep reduction transition mode, the apnea interval should be extended
    		    // if necessary
                RApneaInterval.extendIntervalForPeepReduction();
            }
            else // $[04233] $[04236] $[03008] $[04010] inspiration is to be delivered.
            {
				if (RVitalCapacityManeuver.getManeuverState() == PauseTypes::IDLE_PENDING)
				{
					// $[RM12208] The VC maneuver shall allow for a full exhalation effort after the inspiration.
					if ( (triggerId == Trigger::OPERATOR_INSP) ||
						 (triggerId == Trigger::ASAP_INSP) ||
						 (triggerId == Trigger::NET_FLOW_INSP) ||
						 (triggerId == Trigger::NET_FLOW_BACKUP_INSP) ||
						 (triggerId == Trigger::PRESSURE_BACKUP_INSP) ||
						 (triggerId == Trigger::PRESSURE_INSP) )
					{
						// $[RM12233] A manual inspiration request shall be accepted during the expiratory phase...
						// The operator insp trigger fires only when not in restricted phase of exhalation
						// $[RM12209] In SIMV, at the conclusion of the maneuver, the interval timers shall be reset like the mode was being entered.
						// $[RM12064] When a VC maneuver terminates, a PEEP Recovery breath shall be delivered, followed by a resumption of normal breath delivery.
						// $[RM12209] In SIMV, at the conclusion of the maneuver, the interval timers shall be reset like the mode was being entered.

						// restart the breath timers by delivering a PEEP recovery breath
						RVitalCapacityManeuver.complete();

						BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
						simvInterval_ = SimvScheduler::NULL_INTERVAL;
						isFirstBreath_ = FALSE;
						breathType = ::SPONT;
					}
					else
					{
						AUX_CLASS_ASSERTION_FAILURE(Uint32( ( Uint32(simvInterval_) << 4) | triggerId));
					}
				}
            // $[TI2.3]
                // set the breath type:
                else if( (simvInterval_ == SimvScheduler::SPONTANEOUS) &&
                    ( (triggerId == Trigger::NET_FLOW_INSP) ||
                      (triggerId == Trigger::PRESSURE_BACKUP_INSP) ||
					  (triggerId == Trigger::NET_FLOW_BACKUP_INSP) ||
                      (triggerId == Trigger::PRESSURE_INSP)
                    ) )
                {
                // $[TI2.3.1]
                    breathType = ::SPONT;
                }
                else if( ( (simvInterval_ == SimvScheduler::SPONTANEOUS) &&
                           (triggerId == Trigger::OPERATOR_INSP) )
                        ||
                         ( (simvInterval_ == SimvScheduler::MANDATORY) &&
                           ( (triggerId == Trigger::ASAP_INSP) ||
                             (triggerId == Trigger::SIMV_TIME_INSP) ||
                             (triggerId == Trigger::PEEP_RECOVERY_MAND_INSP) ||
                             (triggerId == Trigger::OPERATOR_INSP) ) )
                        ||
                         ( (BreathPhaseScheduler::PeepRecovery_ ==
                                 BreathPhaseScheduler::ACTIVE) &&
                           ( (triggerId == Trigger::PEEP_RECOVERY_MAND_INSP) ||
                             (triggerId == Trigger::OPERATOR_INSP) ) ) )
                {
                // $[TI2.3.2]
                        //This is a VIM breath
                        breathType = ::CONTROL;
                }
                else if( (simvInterval_ == SimvScheduler::MANDATORY ||
                           BreathPhaseScheduler::PeepRecovery_ ==
                                 BreathPhaseScheduler::ACTIVE)
                         &&
                         ( (triggerId == Trigger::NET_FLOW_INSP) ||
                           (triggerId == Trigger::PRESSURE_BACKUP_INSP) ||
						   (triggerId == Trigger::NET_FLOW_BACKUP_INSP) ||
                           (triggerId == Trigger::PRESSURE_INSP)
                         ) )
                    // this is a patient triggered breath -- PIM
                {
                // $[TI2.3.3]
                    breathType     = ::ASSIST;
                }
                else if( simvInterval_ == SimvScheduler::NULL_INTERVAL &&
                         triggerId == Trigger::PEEP_RECOVERY )
                {
                // $[TI2.3.5]
                    CLASS_ASSERTION( previousSchedulerId == SchedulerId::OCCLUSION &&
                         BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::START )
                    breathType = ::SPONT;
                }
                else
                {
                // $[TI2.3.4]
                    Uint32 errorCode = ( (Uint32)simvInterval_ << 4) | triggerId;

                    AUX_CLASS_ASSERTION_FAILURE(errorCode);
                }
                startInspiration_(breathTrigger, pBreathPhase, breathType);
            }
		}
        break;

        case BreathPhaseType::INSPIRATION:
        {
        // $[TI3]
            // Check the validity of the triggers. Once triggers are checked, settings for
            // new exhalation can be phased in.

            // The trigger should be one of the following legal triggers (note that
            // inspiratory phase can be a spont inspiration during mode transition):
            CLASS_PRE_CONDITION( (triggerId == Trigger::DELIVERED_FLOW_EXP) ||
			                (triggerId == Trigger::HIGH_PRESS_COMP_EXP) ||
			                (triggerId == Trigger::LUNG_FLOW_EXP) ||
            			    (triggerId == Trigger::LUNG_VOLUME_EXP) ||
                            (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) ||
                            (triggerId == Trigger::PRESSURE_EXP) ||
                            (triggerId == Trigger::BACKUP_TIME_EXP) ||
                            (triggerId == Trigger::IMMEDIATE_EXP) ||
                            (triggerId == Trigger::HIGH_VENT_PRESSURE_EXP) );

			RVolumeTargetedManager.setExhalationTrigger( triggerId, pBreathPhase) ;

            //Phase in new settings (for start of exhalation)
            PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_EXHALATION);

            // check conditions for inspiratory pause
            if( (triggerId == Trigger::IMMEDIATE_EXP) &&
            		RInspPauseManeuver.isManeuverAllowed() &&
                    (RInspiratoryPauseEvent.getEventStatus() == EventData::PENDING ||
                     RInspPauseManeuver.getManeuverState() == PauseTypes::ACTIVE) )
            {
                 // $[TI3.3]
                //start inspiratory pause when in mandatory breath....
                // $[VC24020]
			    // $[BL04005]
			    // $[BL04006]
			    // $[BL04009] :a transition to inspiratory pause phase
                // Reset currently active triggers and setup triggers that are active during
                // pause:
                RBreathTriggerMediator.resetTriggerList();
                RBreathTriggerMediator.changeBreathListName(
                                                BreathTriggerMediator::INSP_PAUSE_LIST);
                // Extend the simv cycle timer by the max pause duration
				rCycleTimer_.stop();
				// Extend the simv timer trigger by the max pause duration
			    ((TimerBreathTrigger&)RSimvTimeInspTrigger).extendInterval(MAX_INSP_PAUSE_INTERVAL);

                // notify the user that inspiratory pause is active:
                RInspiratoryPauseEvent.setEventStatus(EventData::ACTIVE);
                // current breath phase is given a chance to phase-out
                pBreathPhase->relinquishControl(breathTrigger);
                // register an inspiratory pause phase:
                BreathPhase::SetCurrentBreathPhase((BreathPhase&)RInspiratoryPausePhase,
                                                                        breathTrigger);
                // update the breath record
                (RBreathSet.getCurrentBreathRecord())->startInspiratoryPause();
                // start the pause
				BreathPhase::NewBreath() ;
            }
            else
            {
                // $[TI3.2]
                if(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::ACTIVE)
                {
                // $[TI3.2.1]
                // $[04323]
                    ((BreathTrigger&)RPeepRecoveryMandInspTrigger).enable();
                } // $[TI3.2.2]

                startExhalation_(breathTrigger, pBreathPhase);
            }
        }
		break;

        case BreathPhaseType::EXPIRATORY_PAUSE:
        {
        // $[TI4]
            // set the pause event status to notify user of exp pause status:
            EventData::EventStatus eventStatus;
            eventStatus = (triggerId==Trigger::PAUSE_COMPLETE) ?
                                EventData::COMPLETE:     // $[TI4.4]
                                EventData::CANCEL;       // $[TI4.5]
            RExpiratoryPauseEvent.setEventStatus(eventStatus);

        // $[01253] $[03008] $[04010] It's time to start new inspiration:

            //Determine the type of inspiration to be delivered:
            if( (triggerId == Trigger::PAUSE_TIMEOUT) ||
                (triggerId == Trigger::PAUSE_COMPLETE) ||
                (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_INSP) ||
                (triggerId == Trigger::IMMEDIATE_BREATH) )
            {
            // $[TI4.1]
                breathType = ::CONTROL;
            }
            else if(triggerId == Trigger::PAUSE_PRESS)
            {
            // $[TI4.2]
		 	// $[BL04071] :m terminate pause event and start inspiration
		    // $[04220] :m terminate active manual expiratory pause
                breathType = ::ASSIST;
            }
            else
            {
            // $[TI4.3]
                AUX_CLASS_ASSERTION_FAILURE(triggerId);
            }
            // re-adjust the cycle timer duration
            rCycleTimer_.extendInterval(-MAX_EXP_PAUSE_INTERVAL);
            // $[04244] extend the simv interval.  retain previous extentions
            // made possible by previous inspiratory pauses.
            // We can have multiple insp pauses in one simv cycle.
            simvIntervalExtension_ += pBreathPhase->getElapsedTimeMs();

            startInspiration_(breathTrigger, pBreathPhase, breathType);
        }
        break;

        case BreathPhaseType::INSPIRATORY_PAUSE:
	    case BreathPhaseType::BILEVEL_PAUSE_PHASE:
		{
            // $[TI6]
            // set the pause event status to notify user of insp pause status:
            EventData::EventStatus eventStatus;
                // $[TI6.1] COMPLETE
                // $[TI6.2] CANCEL
            eventStatus = (triggerId==Trigger::PAUSE_COMPLETE) ?
                                EventData::COMPLETE:EventData::CANCEL;
            RInspiratoryPauseEvent.setEventStatus(eventStatus);

            //The trigger should have been one of the above triggers
		    // $[BL04078] :m terminate active manual inspiratory pause
		    // $[BL04012] :m terminate active auto inspiratory pause
            AUX_CLASS_ASSERTION( (triggerId == Trigger::PAUSE_TIMEOUT) ||
                             (triggerId == Trigger::PAUSE_COMPLETE) ||
                             (triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP) ||
                             (triggerId == Trigger::PAUSE_PRESS) ||
                             (triggerId == Trigger::IMMEDIATE_BREATH),
                             triggerId );


            // Continue the current timer only when the previous
            // scheduler was SIMV, this is due to that only in SIMV mode the
            // TimeInspTrigger is applicable (generate a CONTROL breath).
            if (previousSchedulerId == SchedulerId::SIMV)
            {   // $[TI6.3.1]
				rCycleTimer_.start();

				// re-adjust  the simv timer trigger to include the insp. pause duration
		    	((TimerBreathTrigger&)RSimvTimeInspTrigger).
		    			extendInterval(-MAX_INSP_PAUSE_INTERVAL + pBreathPhase->getElapsedTimeMs());
		    }   // $[TI6.3.2]

            startExhalation_(breathTrigger, pBreathPhase);
		}
        break;

        case BreathPhaseType::PAV_INSPIRATORY_PAUSE:
		{
	        // $[TI7]
            AUX_CLASS_ASSERTION( triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP ||
                             	 triggerId == Trigger::IMMEDIATE_BREATH ||
                             	 triggerId == Trigger::PAUSE_COMPLETE,
                             	 triggerId ) ;
            startExhalation_(breathTrigger, pBreathPhase);
		}
        break ;

		case BreathPhaseType::NIF_PAUSE:
		{
			// $[RM24010] Upon NIF completion, resume normal ventilation
			// $[RM12110] An active NIF maneuver shall be successfully terminated upon detecting a Manual Inspiration request.

			AUX_CLASS_ASSERTION( triggerId == Trigger::PAUSE_COMPLETE ||
								 triggerId == Trigger::OPERATOR_INSP ||
								 triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_INSP ||
								 triggerId == Trigger::IMMEDIATE_BREATH,
								 triggerId );

			// $[RM12109] ...the interval timers shall be reset like the mode was being entered

			// restart the breath timers by delivering a PEEP recovery breath
			BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
			simvInterval_ = SimvScheduler::NULL_INTERVAL;
			isFirstBreath_ = FALSE;
			startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
		}
		break;

		case BreathPhaseType::P100_PAUSE:
		{
			// $[RM12096] When an active P100 maneuver terminates, breath delivery shall return to the previous scheduler.
			if ( triggerId == Trigger::P100_COMPLETE )
			{
				// Complete the inspiratory phase
				if( simvInterval_ == SimvScheduler::SPONTANEOUS )
				{
					// $[RM12096] if in the SPONT interval, deliver a SPONT breath
					breathType = SPONT;
				}
				else if( simvInterval_ == SimvScheduler::MANDATORY )
				{
					// $[RM12096] if in the ASSIST interval, deliver an ASSIST breath
					breathType = ASSIST;
				}
				else
				{
					AUX_CLASS_ASSERTION_FAILURE( simvInterval_ );
				}
			}
			else if ( triggerId == Trigger::OPERATOR_INSP ||
					  triggerId == Trigger::PAUSE_COMPLETE ||
					  triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_INSP ||
					  triggerId == Trigger::IMMEDIATE_BREATH)
			{
				// $[RM12094] A P100 maneuver shall be canceled for a manual insp request
				breathType = CONTROL;
			}
			else
			{
				// Invalid trigger
				AUX_CLASS_ASSERTION_FAILURE( triggerId );
			}

			// restart the cycle timer stopped when P100 was activated
			rCycleTimer_.start();

			// $[RM12308] If the ventilator is in SIMV, the cycle during which the maneuver becomes active (the cycle during which the next scheduled VIM will occur) is extended by the amount of time the maneuver is active.

			// re-adjust  the simv timer trigger to include the P100 pause duration
			RSimvTimeInspTrigger.extendInterval(-MAX_P100_MANEUVER_ACTIVE_TIME
												+ pBreathPhase->getElapsedTimeMs() );

			startInspiration_(breathTrigger, pBreathPhase, breathType);
		}
		break;

		case BreathPhaseType::VITAL_CAPACITY_INSP:
		{
			// Check the validity of the triggers.
			AUX_CLASS_PRE_CONDITION( triggerId == Trigger::PRESSURE_EXP ||
									 triggerId == Trigger::PAUSE_COMPLETE ||
									 triggerId == Trigger::HIGH_PRESS_COMP_EXP ||
									 triggerId == Trigger::LUNG_FLOW_EXP ||
									 triggerId == Trigger::LUNG_VOLUME_EXP ||
									 triggerId == Trigger::HIGH_CIRCUIT_PRESSURE_EXP ||
									 triggerId == Trigger::IMMEDIATE_EXP ||
									 triggerId == Trigger::HIGH_VENT_PRESSURE_EXP,
									 triggerId) ;

			// if this was a "normal" termination of inspiration then the
			// maneuver can proceed with data collection otherwise
			// cancel the maneuver.

			if (triggerId != Trigger::PRESSURE_EXP)
			{
				// $[RM12062] A Vital Capacity maneuver shall be canceled if...
                // $[RM12209] In SIMV, at the conclusion of the maneuver, the interval timers shall be reset like the mode was being entered.
				// If the VC inspiratory phase was not completed normally then
				// restart the breath timer here, otherwise the breath timer will
				// be restarted after the VitalCapacityManeuver collects data from
				// the exhalation phase and delivers a PEEP recovery breath.

				// restart the breath timers by delivering a PEEP recovery breath
				BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
				simvInterval_ = SimvScheduler::NULL_INTERVAL;
				isFirstBreath_ = FALSE;
				startInspiration_(breathTrigger, pBreathPhase, ::SPONT);
			}
			else
			{
				// $[RM12208] The VC maneuver shall allow for a full exhalation effort after the inspiration.
				startExhalation_(breathTrigger, pBreathPhase);
			}
		}
		break;

        default:
        // $[TI5]
            AUX_CLASS_ASSERTION_FAILURE(phase);
    } //switch
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: relinquishControl
//
//@ Interface-Description
// The method accepts a mode trigger reference as an argument and has no return
// value.  Before the new mode is instructed to take control, the simv time
// trigger and the peep recovery insp trigger are disabled, the asap trigger is
// enabled (on transition to AC, and Apnea modes), and all mode triggers on the
// current trigger list get reset.  Since Simv mode may follow SVO scheduler, a
// NON_BREATHING breath phase is possible. In that case, any breath delivery is
// avoided until the completion of the transitional NON_BREATHING phase that
// guarantees a complete closure of the safety valve.  Therefore, no breath
// triggers are enabled when a transition to a breathing mode is detected.  The
// immediate breath trigger is enabled on transition to emergency modes. No
// breath triggers are enabled on transition to Spont mode. When an immediate
// breath trigger is enabled, the rest of the triggers on the active list are
// disabled to ensure safe transition to the emergency mode.  The argument
// passed, provides the method with the information required to determine the
// next scheduler.
//---------------------------------------------------------------------
//@ Implementation-Description
// Individual breath triggers are enabled and disabled by directly
// accessing the trigger instance.
// Disabling all triggers on the current list is accomplished by
// using the only one instance of the breath trigger mediator,
// calling the method resetTriggerList().
// The mode trigger id passed as an argument is checked to
// determine the requested scheduler which then instructed to
// take control.
//---------------------------------------------------------------------
//@ PreCondition
// The mode setting is checked for valid range of values.
// The trigger id is checked for valid range of mode triggers.
// When the mode trigger is either apnea or mode change, the non breathing
// phase is checked to be SafetyValveClosePhase.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
SimvScheduler::relinquishControl(const ModeTrigger& modeTrigger)
{
    CALL_TRACE("SimvScheduler::relinquishControl(const ModeTrigger& modeTrigger)");

    const Trigger::TriggerId modeTriggerId = modeTrigger.getId();

    // get the type of the current breath phase:
    const BreathPhaseType::PhaseType phase = BreathRecord::GetPhaseType();

    // disable the inspiratory time trigger, to avoid simv inspiration trigger
    // during the new mode
    ((TimerBreathTrigger&)RSimvTimeInspTrigger).disable();
    // and make sure the simvCycleTimer does not timeout to re-enable that trigger:
    rCycleTimer_.stop();

    // disable the peep recovery trigger, to avoid its interference with
    // the next scheduler.
    ((BreathTrigger&)RPeepRecoveryInspTrigger).disable();

    // reset all mode triggers for this scheduler
    RModeTriggerMediator.resetTriggerList();

    // check what mode to switch to
    DiscreteValue mode = PendingContextHandle::GetDiscreteValue(SettingId::MODE);

    if( (modeTriggerId == Trigger::SETTING_CHANGE_MODE) ||
                        (modeTriggerId == Trigger::APNEA) )
    {
    // $[TI1]
        if (phase != BreathPhaseType::NON_BREATHING)
        {
           // $[TI1.1]
           //$[04263]
           // set the asap trigger only if the mode is not spont.
            if ( (mode != ModeValue::SPONT_MODE_VALUE) && (mode != ModeValue::CPAP_MODE_VALUE)
				 || (modeTriggerId == Trigger::APNEA))
            {
               // $[TI1.1.1]
                   ((BreathTrigger&)RAsapInspTrigger).enable();
            }
               // $[TI1.1.2]
        }
        else // phase == BreathPhaseType::NON_BREATHING
        {
            // $[TI1.2]
            // If the breath phase is a non breathing phase, it is the
            // safety valve closed phase that is in progress, and since a time trigger
            // is already set to terminate that phase, there is no need to set any
            // breath trigger.
            CLASS_PRE_CONDITION(BreathPhase::GetCurrentBreathPhase()
                                         == (BreathPhase*)&RSafetyValveClosePhase);
        }
    }
    else
    {
        // $[TI2]
        //Reset currently active triggers and then enable the only valid breath
        // trigger for that transition.
        RBreathTriggerMediator.resetTriggerList();
        ((BreathTrigger&)RImmediateBreathTrigger).enable();
        // make sure trigger is valid:
        CLASS_PRE_CONDITION(
            (modeTriggerId == Trigger::OCCLUSION) ||
            (modeTriggerId == Trigger::DISCONNECT) ||
            (modeTriggerId == Trigger::SVO) );
    }

    if(modeTriggerId == Trigger::SETTING_CHANGE_MODE)
    {
    // $[TI3]
        // $[04230] $[04231]
        if(mode == ModeValue::AC_MODE_VALUE)
        {
        // $[TI3.1]
            ((BreathPhaseScheduler&)RAcScheduler).takeControl(*this);
        }
        else if ((mode == ModeValue::SPONT_MODE_VALUE) || (mode == ModeValue::CPAP_MODE_VALUE))
        {
        // $[TI3.2]
            // $[04253]
            ((BreathPhaseScheduler&)RSpontScheduler).takeControl(*this);
        }
        else if(mode == ModeValue::BILEVEL_MODE_VALUE)
        {
        // $[TI3.4]
            // $[04253]
            ((BreathPhaseScheduler&)RBiLevelScheduler).takeControl(*this);
        }
        else
        {
        // $[TI3.3]
            // the only legal modes to  switch to from Simv are either SPONT
            //or AC:
			AUX_CLASS_ASSERTION_FAILURE(mode);
        }
    }
    else if(modeTriggerId == Trigger::APNEA)
    {
    // $[TI4]
        ((BreathPhaseScheduler&)RApneaScheduler).takeControl(*this);
    }
    else if(modeTriggerId == Trigger::OCCLUSION)
    {
    // $[TI5]
        ((BreathPhaseScheduler&)ROscScheduler).takeControl(*this);
    }
    else if(modeTriggerId == Trigger::DISCONNECT)
    {
    // $[TI6]
        ((BreathPhaseScheduler&)RDisconnectScheduler).takeControl(*this);
    }
    else if(modeTriggerId == Trigger::SVO)
    {
    // $[TI7]
        ((BreathPhaseScheduler&)RSvoScheduler).takeControl(*this);
    }
    // $[TI8]
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: takeControl
//
//@ Interface-Description
// The method is invoked by the method relinquishControl() from the scheduler
// instance that relinquishes control.  It accepts a breath phase scheduler
// reference as an argument and has no return value. The base class method for
// takeControl() is invoked to handle activities common to all schedulers.
// Peep recovery flag is set on transition from a non breathing mode. The
// method is responsible for extending the apnea interval when a transition to
// a breathing mode is in progress.  The NOV RAM instance rBdSystemState is
// updated with the new scheduler to enable BD system to properly recover from
// power interruption.  The Simv scheduler data members are initialized
// to indicate the start of a new Simv interval.  The scheduler registers with
// the BreathPhaseScheduler as the current scheduler, and the mode triggers
// relevant to that mode are set to be enabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// The scheduler reference passed as an argument is used to get the id of the
// current scheduler. The apnea interval is extended only if the id is SPONT,
// AC or APNEA. The apnea extension is accomplised by invoking the method
// extendIntervalForModeChange() in the apnea interval instance. The static
// method of BreathPhaseScheduler is used to register the new scheduler. The
// BdSystemStateHandler is used to update the NOV RAM object for BdSystemState
// with the new scheduler id.  The simv scheduler data members are
// initialized to indicate mandatory interval, first simv breath, and zero
// extension for the simv interval.  The data member pModeTriggerList_
// is passed as an argument to the method in the mode trigger mediator object
// that is responsible for changing the current mode trigger list.
//---------------------------------------------------------------------
//@ PreCondition
// The CLASS_PRE_CONDITION method is used to validate the IDs of the
// valid breath phase schedulers.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
SimvScheduler::takeControl(const BreathPhaseScheduler& scheduler)
{
    CALL_TRACE("SimvScheduler::takeControl(const BreathPhaseScheduler& scheduler)");

    // Invoke the base class takeControl method
    BreathPhaseScheduler::takeControl(scheduler);

    SchedulerId::SchedulerIdValue schedulerId = scheduler.getId();

    if( (schedulerId == SchedulerId::AC) ||
        (schedulerId == SchedulerId::SPONT) ||
        (schedulerId == SchedulerId::BILEVEL) ||
        (schedulerId == SchedulerId::APNEA) )
    {
    // $[TI1]
        // $[04120] On transition to SIMV mode, the apnea interval should be extended
        // if necessary.
        // Note that the initialization of the following data members might be overwritten
        // latter in determineBreathPhase() - if the transition is done during a NON_BREATHING
        // phase.
        RApneaInterval.extendIntervalForModeChange();
        // set the current SIMV interval to mandatory
        simvInterval_ = SimvScheduler::MANDATORY;
        // specify the next simv breath as the first breath in simv mode:
        isFirstBreath_ = TRUE;

        if (schedulerId == SchedulerId::APNEA)
        {
		    // $[TI4]
			RVolumeTargetedManager.reset() ;
		}
	    // $[TI5]
    }
    else if((schedulerId == SchedulerId::DISCONNECT) ||
            (schedulerId == SchedulerId::STANDBY) ||
            (schedulerId == SchedulerId::SVO) ||
            (schedulerId == SchedulerId::OCCLUSION) ||
            (schedulerId == SchedulerId::POWER_UP) )
    {
    // $[TI2]
        BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::START;
        // set the simv interval to null interval:
        simvInterval_ = SimvScheduler::NULL_INTERVAL;
        isFirstBreath_ = FALSE;

        if (schedulerId == SchedulerId::DISCONNECT)
        {
		    // $[TI6]
			// $[VC24013a]
			if (PhasedInContextHandle::GetBoundedValue(SettingId::IBW).value <= 7.0)
			{
			    // $[TI7]
				RVolumeTargetedManager.reset() ;
			}
		    // $[TI8]
        }
        else
        {
		    // $[TI9]
			// $[VC24013b]
			RVolumeTargetedManager.reset() ;
        }
    }
    else
    {
    // $[TI3]
        CLASS_ASSERTION(schedulerId == SchedulerId::SAFETY_PCV);
        // set the current SIMV interval to mandatory
        simvInterval_ = SimvScheduler::MANDATORY;
        // specify the next simv breath as the first breath in simv mode:
        isFirstBreath_ = TRUE;
		RVolumeTargetedManager.reset() ;
    }

    // update the reference to the newly active breath scheduler
    BreathPhaseScheduler::SetCurrentScheduler_((BreathPhaseScheduler&)(*this));

    // initialized the simv interval extension:
    simvIntervalExtension_ = 0;

    //The BreathRecord is not being updated with the new scheduler id, so that
    // the identity of the scheduler that originated this breath is maintained.
    // until a new inspiration starts.

    // Initialize the cycle time to an arbitrary value. This value does not effect
    // ventilation.
    simvCycleTimeMs_ = 10000;

    // update the BD state, in case of power interruption
    BdSystemStateHandler::UpdateSchedulerId(getId());

    // Set the mode triggers for that scheduler.
    enableTriggers_();
    RModeTriggerMediator.changeModeTriggerList(pModeTriggerList_);
}

//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: timeUpHappened
//
//@ Interface-Description
// $[04233]
// This method takes a reference to an IntervalTimer as an argument and has no
// return value. The Simv scheduler is a client of the timer server that is
// responsible to notify (by invoking this function) the end of the current
// simv interval.  This routine sets the start of the new mandatory interval
// and restarts the timer that signals the end of the simv interval. This
// method initializes the new Simv interval. If rate change is in progress and the
// current phase is not inspiration, the simv interval is adjusted for rate change.
//---------------------------------------------------------------------
//@ Implementation-Description
// The IntervalTimer reference passed in, rTimer, is that of the cycle timer
// server for the simv scheduler. To initialize the new interval, the following
// activities are taken place: the interval type is set to MANDATORY, any
// previous interval extensions is reset, the actual simv interval is set to either
// the pending breath period or the simvCycleTimeMs_. The simv time insp trigger
// (rSimvTimeInspTrigger) is set with the new target mandatory interval, the simv
// cycle timer (rTimer) is set with the re-adjusted actual simv interval
// duration.  The target inspiratory time and the simv cycle time may be
// affected by the new respiratory rate, if a rate setting change has been
// detected.
//---------------------------------------------------------------------
//@ PreCondition
// The id of the timer server is checked to match the simv cycle timer id.
// Peep recovery is off.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
SimvScheduler::timeUpHappened(const IntervalTimer& rTimer)
{
    CALL_TRACE("SimvScheduler::timeUpHappened(const IntervalTimer& rTimer)");

    // make sure the caller is the proper server
    CLASS_PRE_CONDITION(&rTimer == &rCycleTimer_);
    CLASS_ASSERTION(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::OFF);
    Int32 targetMandatoryInterval;
    simvInterval_ = SimvScheduler::MANDATORY;
    //reset the simv interval extension
    simvIntervalExtension_ = 0;
    if ( (BreathPhaseScheduler::RateChange_) &&
         (BreathRecord::GetPhaseType() != BreathPhaseType::INSPIRATION) )
    {
    // $[TI1]
    // The target mandatory interval should use the pending interval unless the target interval
    // is shorter because of apnea interval duration.
        actualSimvInterval_ = pendingBreathPeriod_;
        targetMandatoryInterval = MIN_VALUE(pendingMandatoryInterval_,targetMandatoryInterval_);
    }
    else
    {
    // $[TI2]
        actualSimvInterval_ = simvCycleTimeMs_;
        targetMandatoryInterval = targetMandatoryInterval_;
    }

    // $[04009] $[04234]
    // restart the simv cycle timer (simv elapsed time is set to 0)
    rCycleTimer_.restart();

    // set, enable and restart the simv inspiratory trigger.
    // Extend the target mandatory interval by CYCLE_TIME_MS to compensate for
    // incrementing the timer on this interval (as this timer is following the simv
    // cycle timer on the TimerMediator list).
    ((TimerBreathTrigger&)RSimvTimeInspTrigger).restartTimer(targetMandatoryInterval +
                                                CYCLE_TIME_MS);

    // adjust the target time for the set simv cycle time
    // and restart the timer
    rCycleTimer_.setTargetTime(actualSimvInterval_);
    ((TimerBreathTrigger&)RSimvTimeInspTrigger).enable();

}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method:  SoftFault(softFaultID, lineNumber, pFileName, pPredicate)
//                [static]
//
//@ Interface-Description
//      This method is called when a software fault is detected by the
//      fault macros ('CLASS_PRE_CONDITION()', etc.). The 'softFaultID'
//      and 'lineNumber' are essential pieces of information.  The
//      'pFileName' and 'pPredicate' strings may be defaulted in the macro
//      to reduce code space.
//---------------------------------------------------------------------
//@ Implementation-Description
//      This method receives the call for the SoftFault, adds it sub-system
//      and class name ID and sends the message to the non-member function
//      SoftFault.
//---------------------------------------------------------------------
//@ PreCondition
//      none
//---------------------------------------------------------------------
//@ PostCondition
//      none
//@ End-Method
//=====================================================================

void
SimvScheduler::SoftFault(const SoftFaultID  softFaultID,
			 const Uint32       lineNumber,
			 const char*        pFileName,
			 const char*        pPredicate)
{
  CALL_TRACE("SoftFault(softFaultID, lineNumber, pFileName, pPredicate)");
  FaultHandler::SoftFault(softFaultID, BREATH_DELIVERY, SIMVSCHEDULER,
                          lineNumber, pFileName, pPredicate);
}



//=====================================================================
//
//  Protected Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: settingChangeHappened_
//
//@ Interface-Description
// The method takes a SettingId id as an argument and has no return value.  The
// implementation of this method considers multiple rate change events for the
// same Simv interval. A mode change is checked first and if true, the setting
// change mode trigger is enabled. If only a rate change is detected, and the
// new rate is greater than the current, the simv interval is adjusted right
// away for an exhalation phase otherwise, it gets postponed by setting the rate
// change flag to true.
//---------------------------------------------------------------------
//@ Implementation-Description
// The client of this method may use only one SettingId as an argument. The
// method checks the id value and sets the static data members ModeChange_ and
// RateChange_ accordingly. For a mode change, the mode trigger instance:
// rSettingChangeModeTrigger is enabled. For rate change only, if the current
// breath phase is exhalation, the method AdjustIntervalForRateChange_
// is called otherwise, the rate change in progress is flagged by setting the
// BreathPhaseScheduler::RateChange_ to TRUE.
//---------------------------------------------------------------------
//@ PreCondition
// The setting id is within the folowing range:
//   id >= SettingId::LOW_BATCH_BD_ID_VALUE && id <= SettingId::HIGH_BATCH_BD_ID_VALUE
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
SimvScheduler::settingChangeHappened_(const SettingId::SettingIdType id)
{
    CALL_TRACE("SimvScheduler::settingChangeHappened_(const SettingId::SettingIdType id)");

    if(id == SettingId::MODE)
    {
    // $[TI1]
        // Check the mode value. Ignore any pending rate changes. Rate change, if any, will be
        // updated on the next inspiration.
        const DiscreteValue mode = PendingContextHandle::GetDiscreteValue(SettingId::MODE);
        if(mode != ModeValue::SIMV_MODE_VALUE)
        {
        // $[TI1.1]
            BreathPhaseScheduler::ModeChange_ = TRUE;
            BreathPhaseScheduler::RateChange_ = FALSE;
            ((Trigger&)RSettingChangeModeTrigger).enable();
        }
        // $[TI1.2]
    }
    else if( (id == SettingId::RESP_RATE) && (!BreathPhaseScheduler::ModeChange_) )
    {
    // $[TI2]
        // $[04245] $[04246]
        // pending respiratory rate setting
        const BoundedValue& rRespiratoryRate =
            PendingContextHandle::GetBoundedValue(SettingId::RESP_RATE);
        // pending breath period:
        Int32 pendingBreathPeriod = (Int32)(60.0F * 1000.0F / rRespiratoryRate.value);
        // A pointer to the current breath phase
        BreathPhase* pBreathPhase = BreathPhase::GetCurrentBreathPhase();
        // determine the current phase type (e.g. EXHALATION, INSPIRATION)
        BreathPhaseType::PhaseType phase = pBreathPhase->getPhaseType();
        if ( pendingBreathPeriod < simvCycleTimeMs_)
        {
        // $[TI2.1]
            pendingBreathPeriod_ = pendingBreathPeriod;
            BreathPhaseScheduler::RateChange_ = TRUE;
            if(phase == BreathPhaseType::EXHALATION)
            {
            // $[TI2.1.1]
                adjustIntervalForRateChange_();
            }
            // $[TI2.1.2]
        }
        else
        {
        // $[TI2.2]
            BreathPhaseScheduler::RateChange_ = FALSE;
        }
    }
    else
    {
    // $[TI3]
        CLASS_ASSERTION( id >= SettingId::LOW_BATCH_BD_ID_VALUE &&
                     id <= SettingId::HIGH_BATCH_BD_ID_VALUE );
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: reportEventStatus_
//
//@ Interface-Description
// The method accepts a EventId and a Boolean for the event status as
// arguments.  It returns a EventStatus. The method handles user events
// like manual inspiration, alarm reset, expiratory pause, etc.  The method
// allows for any user event to be either enabled or disabled.
//---------------------------------------------------------------------
//@ Implementation-Description
// $[01247] $[01255]
// The argument of type EventId specifies what is the active event.  The
// Boolean type argument, eventState, specifies whether user event is enabled
// (TRUE) or disabled (FALSE). A simple switch statement implements the
// response for the different user events. The manual inspiration event is accepted by
// invoking a call to the static method:
// BreathPhaseScheduler::AcceptManualInspiration_(eventStatus).
// The alarm reset event is ignored, while the eventStatus argument value is checked for
// TRUE.
// The expiratory and inspiratory pause events are accepted by invoking a call to the
// static methods:
// BreathPhaseScheduler::AcceptExpiratoryPause_(eventStatus).
// BreathPhaseScheduler::AcceptInspiratoryPause_(eventStatus) - respectively.
//---------------------------------------------------------------------
//@ PreCondition
// The user event id is checked to be within a valid range.
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================

EventData::EventStatus
SimvScheduler::reportEventStatus_(const EventData::EventId id,
                                                     const Boolean eventStatus)
{
    CALL_TRACE("SimvScheduler::reportEventStatus_(const EventData::EventId id,\
                                                     const Boolean eventStatus)");
    EventData::EventStatus rtnStatus = EventData::IDLE;

    switch (id)
    {
        case EventData::MANUAL_INSPIRATION:
        // $[TI1]
            rtnStatus =
                    BreathPhaseScheduler::AcceptManualInspiration_(eventStatus);
            break;

        case EventData::ALARM_RESET:
        // $[TI2]
            // ignore alarm reset
            CLASS_PRE_CONDITION(eventStatus);
            break;

        case EventData::EXPIRATORY_PAUSE:
        // $[TI3]
            rtnStatus =
                    BreathPhaseScheduler::AcceptExpiratoryPause_(eventStatus);
            break;

        case EventData::INSPIRATORY_PAUSE:
        // $[TI4]
            rtnStatus =
                    BreathPhaseScheduler::AcceptInspiratoryPause_(eventStatus);
            break;

		case EventData::NIF_MANEUVER:
		{
            rtnStatus = BreathPhaseScheduler::AcceptNifManeuver_(eventStatus);
		}
		break;

		case EventData::P100_MANEUVER:
		{
			rtnStatus = BreathPhaseScheduler::AcceptP100Maneuver_(eventStatus);
		}
		break;

		case EventData::VITAL_CAPACITY_MANEUVER:
		{
			rtnStatus = BreathPhaseScheduler::AcceptVitalCapacityManeuver_(eventStatus);
		}
		break;

        default:
        // $[TI6]
            AUX_CLASS_ASSERTION_FAILURE(id);
    }

    return (rtnStatus);
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: enableTriggers_
//
//@ Interface-Description
// $[04119] $[04125] $[04130] $[04141]
// The method takes no arguments and returns no value.  A request to enable
// SIMV scheduler valid mode triggers is issued whenever the Simv scheduler is
// taking control. Immediate triggers are not set here since by design they are
// only set once the trigger they instanciate becomes active.
//---------------------------------------------------------------------
//@ Implementation-Description
// The mode trigger instances are accessed directly with a call to their
// setIsEnableRequested() method.
//---------------------------------------------------------------------
//@ PreCondition
//    none
//---------------------------------------------------------------------
//@ PostCondition
//    none
//@ End-Method
//=====================================================================
void
SimvScheduler::enableTriggers_(void)
{
// $[TI1]
    CALL_TRACE("SimvScheduler::enableTriggers_(void)");

    ((ModeTrigger&)RApneaTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)ROcclusionTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RDisconnectTrigger).setIsEnableRequested(TRUE);
    ((ModeTrigger&)RSvoTrigger).setIsEnableRequested(TRUE);

    // Note that the immediate, or event driven triggers are not
    // enabled here: rSettingChangeModeTrigger
}


//=====================================================================
//
//  Private Methods...
//
//=====================================================================


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startInspiration_
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, and a
// breath type as arguments. It does not return any value.  When the next phase
// is determined to be inspiration, the method is called to perform all the
// standard procedures required to start inspiration: Any pending mode or rate
// change status is cleared, new settings are phased in, the scheduler is
// checked to be in sync with the mode setting. the breath phase to employ is
// determined based on the breath type and can be either a mandatory or a
// spontaneous.  a new breath record is created, the oxygen mix, the apnea
// interval, and peep get updated, the breath trigger list is changed for
// inspiration list, the simv cycle timer is set with the simv interval to
// determine when the next simv interval should start. The duration of the next
// mandatory interval is calculated. the old breath relinquishes control, and
// the breath phase is set with the reference to the breath phase to be
// delivered. If rate change is in process, Breath stacking is prevented by
// extending the simv cycle time.
//---------------------------------------------------------------------
//@ Implementation-Description
// The method collaborates with the handle PhasedInContextHandle to phase in
// new settings. A check that the scheduler matches the mode setting is done by
// calling the method VerifyModeSynchronization_().
// The method creates a new breath record using the object RBreathSet.
// The breathType argument, passed-in is checked to determine whether a
// mandatory or a spontaneous inspiration is due.  The actual oxygen mix, apnea
// interval, and peep are updated using the instances rO2Mixture,
// rApneaInterval, and rPeep.  The breath trigger mediator instance
// RBreathTriggerMediator is used to set the inspiratory breath trigger list.
// The simv cycle timer: rCycleTimer_ is set to establish the start of the next
// simv interval. The same timer restarts for the very first simv inspiration.
// The simv elapsed time is retrieved from the cycle timer, the apnea interval
// is retrieved from the phased in settings, from these two variables, the
// target mandatory interval is calculated.  The breath phase instance pointed
// to by argument pBreathPhase is instructed to phase out.  The static method
// BreathPhase::SetCurrentBreathPhase is used to set up the new mandatory
// breath type.  If isRateAdjustmentRequired_ is TRUE and the breath type is
// mandatory, the simv cycle time is adjusted to prevent breath stacking.
//---------------------------------------------------------------------
//@ PreCondition
// Check the mandatory type value to be within the expected range.
// Check the support type value to be within the expected range.
// Check simv interval and isFirstBreath_ for appropriate values.
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
SimvScheduler::startInspiration_(const BreathTrigger& breathTrigger,
                BreathPhase* pBreathPhase,  BreathType breathType)
{

    CALL_TRACE("SimvScheduler::startInspiration_(const BreathTrigger& breathTrigger,\
                BreathPhase* pBreathPhase,  BreathType breathType)");

    // pointer to a breath phase
    BreathPhase* pPhase = NULL;
    // variables to store setting values:
    DiscreteValue mandatoryType;
    // flags peep recovery in process
    Boolean isPeepRecovery = FALSE;
    // time elapsed since start of SIMV cycle
    Int32 simvElapsedTime;
    // the following is only used for rate change in progress:
    Int32 previousSimvCycleTimeMs = simvCycleTimeMs_;

    // Phase in new settings (for start of inspiration)
    PhasedInContextHandle::PhaseInPendingBatch(PhaseInEvent::START_OF_INSPIRATION);

#ifdef INTEGRATION_TEST_ENABLE
    // Signal Event for swat
    swat::EventManager::signalEvent(swat::EventManager::START_OF_INSP);
#endif // INTEGRATION_TEST_ENABLE

    // verify BD is in synch with GUI:
    BreathPhaseScheduler::VerifyModeSynchronization_(getId());
    // set the simv cycle time, use the respiratory rate setting
    const BoundedValue& rRespiratoryRate =
        PhasedInContextHandle::GetBoundedValue(SettingId::RESP_RATE);
    simvCycleTimeMs_ = (Int32)(60.0F * 1000.0F / rRespiratoryRate.value);

	DiscreteValue supportType =
			PhasedInContextHandle::GetDiscreteValue(SettingId::SUPPORT_TYPE);

	mandatoryType = PhasedInContextHandle::GetDiscreteValue(SettingId::MAND_TYPE);

	RVolumeTargetedManager.updateData( mandatoryType, supportType) ;

    if( (breathType == ::CONTROL) || (breathType == ::ASSIST) )
    {
    // $[TI1]
        isPeepRecovery = FALSE;
        // $[04241] set the mandatory type

		switch (mandatoryType)
		{
			case MandTypeValue::PCV_MAND_TYPE :
				// $[TI1.2]
				pPhase = (BreathPhase*)&RPcvPhase ;
				break ;

			case MandTypeValue::VCV_MAND_TYPE :
				// $[TI1.1]
				pPhase = (BreathPhase*)&RVcvPhase ;
	    		break ;

			case MandTypeValue::VCP_MAND_TYPE :
				// $[TI1.3]
				RVolumeTargetedManager.determineBreathType( pPhase, mandatoryType) ;
				break ;

			default :
				// unexpected mand type...
				AUX_CLASS_ASSERTION_FAILURE(mandatoryType) ;
				break ;
		}
    }
    else if (breathType == ::SPONT)
    {
    // $[TI2]
        // deliver a peep recovery phase for peep recovery state = start
        if(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::START)
        {
        // $[TI2.1]
            isPeepRecovery = TRUE;
            pPhase = (BreathPhase*)&RPeepRecoveryPhase;
        }
        else
        {
        // $[TI2.2]
            isPeepRecovery = FALSE;

			// get the support type
			if (supportType == SupportTypeValue::ATC_SUPPORT_TYPE)
			{   // $[TI2.2.1]
				// set the phase for ATC support type
				pPhase = (BreathPhase*)&RTcvPhase;
			}
			else if (supportType == SupportTypeValue::PSV_SUPPORT_TYPE ||
					 supportType == SupportTypeValue::OFF_SUPPORT_TYPE)
			{   // $[TI2.2.2]
				// set the phase for PSV support type
				pPhase = (BreathPhase*)&RPsvPhase;
			}
			else
			{   // $[TI2.2.3]
				AUX_CLASS_ASSERTION_FAILURE(supportType);
			}
        }
        mandatoryType = ::NULL_MANDATORY_TYPE;
    }
    else
    {
    // $[TI3]
        CLASS_PRE_CONDITION( (breathType == ::CONTROL) ||
                             (breathType == ::ASSIST) ||
                             (breathType == ::SPONT) );
    }

    //Reset currently active triggers and setup triggers that are active during
    //inspiration, note that the rSimvTimeInspTrigger is disabled here as well,
    // so that it won't fire during the spontaneous interval:
    RBreathTriggerMediator.resetTriggerList();
    RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::INSP_LIST);

    // $[04118] $[04122] update the apnea interval, and peep.
    // The apnea interval is used later to determine the targetMandatoryInterval_.
    RApneaInterval.newPhase(BreathPhaseType::INSPIRATION);
    RPeep.updatePeep(BreathPhaseType::INSPIRATION);

	if (pPhase == (BreathPhase*)&RPeepRecoveryPhase)
	{	// $[TI6]
		RPeep.setPeepChange( TRUE) ;
	}	// $[TI7]

    if(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::START)
    {
    // $[TI4]
        // Set the PeepRecovery_ variable to ACTIVE to flag the activation of the ASAP trigger
        // on the start of the next exhalation.
        BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::ACTIVE;
        CLASS_ASSERTION(simvInterval_ == SimvScheduler::NULL_INTERVAL);
        CLASS_ASSERTION(!isFirstBreath_);
    }
    else
    {
    // $[TI5]
        if(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::ACTIVE)
        {
        // $[TI5.1]
            BreathPhaseScheduler::PeepRecovery_ = BreathPhaseScheduler::OFF;
            CLASS_ASSERTION(simvInterval_ == SimvScheduler::NULL_INTERVAL);
            CLASS_ASSERTION(!isFirstBreath_);
            isFirstBreath_ = TRUE;
        }    // $[TI5.2]

        // the following adjustment is necessary to avoid breath stacking when rate changes
        // to a very high rate. It makes sure that the SIMV interval is not terminated too
        // soon.
        if(isRateAdjustmentRequired_ &&
            (breathType == ::ASSIST || breathType == ::CONTROL) )
        {
        // $[TI5.5]
            simvElapsedTime = rCycleTimer_.getCurrentTime();
            // figure out the proper simv interval to avoid breath stacking
            Real32 setInspTime = PhasedInContextHandle::GetBoundedValue(SettingId::INSP_TIME).value;
            // inspiratory time based for a transitional simv interval
            Int32 tiBasedSimvInterval = (Int32)(simvElapsedTime + 3.5 * setInspTime);
            // total breath based for a transitional simv interval
            Int32 tbBasedSimvInterval = simvCycleTimeMs_;
            // calculation for transitional simv interval
            Int32 transitionalSimvInterval = MAX_VALUE(tiBasedSimvInterval, tbBasedSimvInterval);
            actualSimvInterval_ = MIN_VALUE(transitionalSimvInterval, previousSimvCycleTimeMs)
                                    + simvIntervalExtension_;
        }
        else
        {
        // $[TI5.6]
            // $[04246]
            // set the actual simv interval:
            actualSimvInterval_ = simvCycleTimeMs_ + simvIntervalExtension_;
        }
        // $[04235] adjust the target time for the new simv cycle time (if any)
        rCycleTimer_.setTargetTime(actualSimvInterval_);

        // restart the simv cycle timer only if it is the first breath in simv mode.
        // Deliver a breath at the beginning of the first simv cycle. The mandatory
        // breath in SIMV is usually delivered at the end of the mandatory interval - if
        // no patient effort was detected earlier. The first breath in SIMV (excluding
        // the peep recovery breath) is delivered at the beginning of the SIMV cycle; the
        // purpose of the variable isFirstBreath_ is to flag the start of the first SIMV
        // breath so that the SIMV cycle timer can restart.
        if(isFirstBreath_)
        {
        // $[04247]
        // $[TI5.3]
            rCycleTimer_.restart();
            isFirstBreath_ = FALSE;
        }
        // $[TI5.4]

        simvElapsedTime = rCycleTimer_.getCurrentTime();
        // set the next mandatory interval, based on the apnea interval - if necessary.
        Int32 apneaInterval = RApneaInterval.getApneaInterval();
        // $[04237] $[04238] recalculate the simv max mandatory interval. The max mandatory
        // interval is based only on respiratory rate. This actual simv interval -
        // targetMandatoryInterval_ can't be greater than the max amndatory interval.
        maxMandatoryInterval_ = MIN_VALUE( (Int32)(simvCycleTimeMs_ * MAND_INTERVAL_FRACTION),
                                                    MAX_MANDATORY_INTERVAL );
        // the target mandatory interval is determine to prevent apnea from triggering
        // on the next simv cycle - if possible. Thats why the targetMandatoryInterval_ is
        // dependent on the apnea interval.
        targetMandatoryInterval_ =
            MAX_VALUE(0, (apneaInterval+simvElapsedTime-actualSimvInterval_) );
        targetMandatoryInterval_ = MIN_VALUE(targetMandatoryInterval_, maxMandatoryInterval_);

        // $[04233] once a mandatory breath is delivered, the simv interval is set to
        // spontaneous.
        simvInterval_ = SimvScheduler::SPONTANEOUS;
    }

    isRateAdjustmentRequired_ = FALSE;
    BreathPhaseScheduler::ModeChange_ = FALSE;
    BreathPhaseScheduler::RateChange_ = FALSE;

    // phase-out current breath phase
    pBreathPhase->relinquishControl(breathTrigger);

    // Once settings are phased in, determine the O2 mix for this scheduler:
    RO2Mixture.determineO2Mix(*this, (BreathPhase&)*pPhase);

    //  register new phase and prepare to deliver inspiration:
    BreathPhase::SetCurrentBreathPhase((BreathPhase&)*pPhase,
                                        breathTrigger);
    //Update the breath record
    RBreathSet.newBreathRecord(SchedulerId::SIMV, mandatoryType, breathType,
                                BreathPhaseType::INSPIRATION, isPeepRecovery);
    // set the breath record with the breath cycle time:
    BreathRecord::SetBreathPeriod((Real32)simvCycleTimeMs_);
    // start the mandatory inspiration
	BreathPhase::NewBreath() ;
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: adjustIntervalForRateChange_
//
//@ Interface-Description
// $[04246]
// The method accepts no arguments and has no return value.  The method is
// called during exhalation only. If rate change was detected during
// inspiration, the call to the method will be delayed until the start of
// exhalation. The method calculates a new simv cycle based on the proposed new
// rate and the length of the most recent inspiration.  It also calculates the
// pending mandatory interval - this is the interval that is based on the new
// respiratory rate. The method distinguishes between 2 cases: the first is for
// spontaneous simv interval and the second is for mandatory simv interval. For
// spontaneous mandatory interval, a new simv interval is calculated and the
// simv interval timer is set with the new value. The new simv interval is
// caluclated based on 2 requirements. One requirement is that the next simv
// cycle will not force an inspiration that will be delivered before exhalation
// elapsed time exceeded 2.5 times of the inspiratory time.  The second
// requirement is based on the new rate. The actual simv cycle is determined
// from the max calculation of the two values. For mandatory interval, a new,
// transitional, mandatory interval is calculated. The new mandatory interval
// is caluclated based on 2 requirements. One requirement is that the next
// inspiration will be delivered only after exhalation elapsed time exceeded
// 2.5 times of the inspiratory time.  The second requirement is based on the
// pending mandatory interval. The new mandatory interval is determined by
// finding the min value out of the two requirements.  A flag to signify that
// rate change is required is set so that if the next simv cycle happens before
// a new inspiration takes place, the calculations that determine the start of
// the next inspiration will consider avoiding breath stacking due to much
// higher breath rate.  for the next when The method allows for multiple rate
// changes to occur before the change takes effect.
//---------------------------------------------------------------------
//@ Implementation-Description
// The current breath record is used to get the breath duration and the
// inspiratory time. The simv cycle timer rCycleTimer_ is used to get the
// current simv elapsed time. A new breath cycle is calculated which allow for
// a minimum I:E ratio of 1:2.5.  A pending mandatory interval is calculated
// based on the new rate.  For a spontaneous simv interval, the simv cycle is
// adjusted by the difference between the old cycle and the new cycle duration;
// the simv cycle timer: rCycleTimer_ is adjusted for the actual simv interval.
// For a mandatory simv interval, the mandatory interval is adjusted to prevent
// breath stacking. The flag isRateAdjustmentRequired_ is set to signal a
// probable additional adjustment for the simv cycle on the start of the next
// inspiration.
//
//---------------------------------------------------------------------
//@ PreCondition
// The simv interval is checked to be either MANDATORY or SPONTANEOUS.
// The peep recovery state is checked to be equal to OFF.
//---------------------------------------------------------------------
//@ PostCondition
// none
//@ End-Method
//=====================================================================
void
SimvScheduler::adjustIntervalForRateChange_(void)
{
    CALL_TRACE("SimvScheduler::adjustIntervalForRateChange_(const Int32 pendingBreathPeriod)");

    CLASS_ASSERTION(BreathPhaseScheduler::PeepRecovery_ == BreathPhaseScheduler::OFF);
    Int32 inspTime = (Int32)BreathRecord::GetInspiratoryTime();
    Int32 breathDuration = (Int32)(RBreathSet.getCurrentBreathRecord())->getBreathDuration();
    // the minimum simv cycle time that allows for full exhalation:
    Int32 simvElapsedTime = rCycleTimer_.getCurrentTime();
    Int32 minCycleTime = (Int32)(simvElapsedTime - breathDuration + 3.5 * inspTime);
    Int32 newBreathCycle = MAX_VALUE(minCycleTime, pendingBreathPeriod_);
    // new max mandatory interval based on pending breath rate:
    pendingMandatoryInterval_ = (Int32)
            MIN_VALUE(pendingBreathPeriod_ * MAND_INTERVAL_FRACTION, MAX_MANDATORY_INTERVAL);
    if(simvInterval_ == SimvScheduler::SPONTANEOUS)
    {
    // $[TI1]
        Int32 delta = MAX_VALUE( (simvCycleTimeMs_ - newBreathCycle), 0);
        // set the actual simv interval to reflect the request for rate
        // change. The actualSimvInterval is not used on the RHS to avoid
        // accumulating the effect of successive rate changes.
        // $[04299]
        actualSimvInterval_ = simvCycleTimeMs_+simvIntervalExtension_-delta;
        // set the simv cycle timer to the rate dependent value
        // this is especially important when the mandatory breath is delayed due to
        // a longer than expected restricted phase.
        rCycleTimer_.setTargetTime(actualSimvInterval_);
    }
    else if(simvInterval_ == SimvScheduler::MANDATORY)
    {
    // $[TI2]
        // adjustment is required to the simv cycle on the next mandatory breath
        isRateAdjustmentRequired_ = TRUE;
        // $[04300] $[04324]
        // in the case of mandatory interval, minCycleTime is the minimum mandatory
        // interval to prevent breath stacking.
        Int32 transitionalMandatoryInterval = MAX_VALUE(pendingMandatoryInterval_,
                                                                        minCycleTime);
        transitionalMandatoryInterval = MIN_VALUE(transitionalMandatoryInterval,
                                                targetMandatoryInterval_);
        // set a new target for the simv insp trigger
        ((TimerBreathTrigger&)RSimvTimeInspTrigger).setTargetTime(
                                                    transitionalMandatoryInterval);
        // note that the targetMandatoryInterval is not set to the pending
        // value to maintain a valid reference when multiple rate change requests
        // accur.
    }
    else
    {
    // $[TI3]
        AUX_CLASS_ASSERTION_FAILURE(simvInterval_);
    }
}


//============================ M E T H O D   D E S C R I P T I O N ====
//@ Method: startExhalation_
//
//@ Interface-Description
// The method takes a breath trigger reference, a breath phase pointer, a
// breath type as arguments, and a null inspiration boolean flag. It
// does not return any value.
//---------------------------------------------------------------------
//@ Implementation-Description
// Reset currently active triggers and setup triggers that are active during
// inspiration.  Turn peep recovery off for peep recovery expiration.
// The applied O2 percent is updated if necessary, the apnea interval is
// updated for start of exhalation, and the breath phase gets updated.
// Create a new breath record with NULL_INSPIRATION phase type when peep
// reduction occures due to transition from high peep bilevel state to
// this scheduler.  Otherwise, update the breath record.
//---------------------------------------------------------------------
//@ PreCondition
//---------------------------------------------------------------------
//@ PostCondition
//        none
//@ End-Method
//=====================================================================
void
SimvScheduler::startExhalation_(const BreathTrigger& breathTrigger,
                              BreathPhase* pBreathPhase,
                              const Boolean noInspiration)
{
// $[TI1]
	CALL_TRACE("SimvScheduler::startExhalation_(const BreathTrigger& breathTrigger,\
                              BreathPhase* pBreathPhase,\
                              const Boolean noInspiration)");

#ifdef INTEGRATION_TEST_ENABLE
	// Signal Event for swat
	swat::EventManager::signalEvent(swat::EventManager::START_OF_EXH);
#endif // INTEGRATION_TEST_ENABLE

    //Reset currently active triggers and setup triggers that are active during
    //inspiration:
    RBreathTriggerMediator.resetTriggerList();
    RBreathTriggerMediator.changeBreathListName(BreathTriggerMediator::EXP_LIST);
    // phase-out current breath phase
    pBreathPhase->relinquishControl(breathTrigger);
     // determine the O2 mix, the apnea interval, and peep:
    RO2Mixture.determineO2Mix(*this, (BreathPhase&)RExhalationPhase);
    RApneaInterval.newPhase(BreathPhaseType::EXHALATION);
    RPeep.updatePeep(BreathPhaseType::EXHALATION);
     // register new phase:
    BreathPhase::SetCurrentBreathPhase((BreathPhase&)RExhalationPhase, breathTrigger);

    if(noInspiration)
    {
    // $[TI2]
        //Update the breath record
        RBreathSet.newBreathRecord(SchedulerId::SIMV,
               ::NULL_MANDATORY_TYPE,
               RBiLevelScheduler.getFirstBreathType(),
               BreathPhaseType::NULL_INSPIRATION,
               FALSE);
    }
    else
    {
    // $[TI1]
        //Update the breath record
        (RBreathSet.getCurrentBreathRecord())->newExhalation();
    }

    // start exhalation
	BreathPhase::NewBreath() ;

    if (BreathPhaseScheduler::RateChange_)
    {
    // $[TI3]
        adjustIntervalForRateChange_();
    }
    // $[TI4]
}


